// -*- Mode: C++; -*-
#ifndef TIMESTAMP_H_20141009
#define TIMESTAMP_H_20141009

/** \file timestamp.h time utilities */

#include <time.h>

#ifdef _VRXEVO
#include "time2.h"
#endif

#if (defined _VRXEVO || defined _WIN32)
#  if   defined VFI_GUIPRT_SHARED_EXPORT
#    define DllSpec __declspec(dllexport)
#  elif defined VFI_GUIPRT_STATIC_EXPORT || defined _WIN32  // dllimport not required for Windows
#    define DllSpec
#  else
#    define DllSpec __declspec(dllimport)
#  endif
#elif defined __GNUC__ && defined VFI_GUIPRT_SHARED_EXPORT
#  define DllSpec  __attribute__((visibility ("default")))
#else 
#  define DllSpec
#endif

#if defined(_WIN32) && !defined(_TIMESPEC_DEFINED)
#define _TIMESPEC_DEFINED
struct timespec {
        time_t tv_sec;
        long tv_nsec;
};
#endif /* _TIMESPEC_DEFINED */


namespace vfihtml {
#if 0
}
#endif

/** time utility class */
class DllSpec TimeStamp: public timespec 
{
 public:
   /** read realtime clock \return unix time */
   static TimeStamp Clock();      

   /** read monotonic clock \return monitonic time */
   static TimeStamp Monotonic();

   /** helper class for unit conversion */
   class Unit_MS {};

   /** helper class for unit conversion */
   class Unit_S {};

   /** Millisecond unit */
   static Unit_MS MS;

   /** Second unit */
   static Unit_S S;

   /** constuctor \param[in] sec seconds \param[in] nsec nanoseconds */
   TimeStamp(time_t sec, long nsec);

   /** constuctor \param[in] sec seconds */
   TimeStamp(char sec)           { tv_sec=sec; tv_nsec=0; }

   /** constuctor \param[in] sec seconds */
   TimeStamp(unsigned char sec)  { tv_sec=sec; tv_nsec=0; }

   /** constuctor \param[in] sec seconds */
   TimeStamp(short sec)          { tv_sec=sec; tv_nsec=0; }

   /** constuctor \param[in] sec seconds */
   TimeStamp(unsigned short sec) { tv_sec=sec; tv_nsec=0; }

   /** constuctor \param[in] sec seconds */
   TimeStamp(int sec)            { tv_sec=sec; tv_nsec=0; }

   /** constuctor \param[in] sec seconds */
   TimeStamp(unsigned sec)       { tv_sec=sec; tv_nsec=0; }

   /** constuctor \param[in] sec seconds */
   TimeStamp(long sec=0)         { tv_sec=sec; tv_nsec=0; }

   /** constuctor \param[in] sec seconds */
   TimeStamp(unsigned long sec)  { tv_sec=sec; tv_nsec=0; }

   /** constuctor \param[in] t seconds */
   TimeStamp(float t);

   /** constuctor \param[in] t seconds */
   TimeStamp(double t);

   /** assignment operator \param[in] sec seconds */
   TimeStamp &operator=(time_t sec) { tv_sec=sec; tv_nsec=0; return *this;}

   /** assignment operator \param[in] t seconds */
   TimeStamp &operator=(double t);

   TimeStamp &operator+=(const TimeStamp &o);
   TimeStamp &operator-=(const TimeStamp &o);

   /** get time */
   double get() const { return tv_sec+tv_nsec/1000000000.0; }

   /** set time */
   void set(double t) { operator=(t); }

   /** create second based time stamp \return time stamp */
   time_t s() const { return tv_sec; }

   /** create millisecond based time stamp \return time stamp */
   long ms() const { return tv_sec*1000+tv_nsec/1000000; }

   /** normalize time representation */
   void normalize();

   /** reset to 0 */
   void clear() { tv_sec=0; tv_nsec=0; }
};


/** add two time values \param[in] a first time value \param[in] b second time value \return result of a+b */
DllSpec TimeStamp operator+(const TimeStamp &a, const TimeStamp &b);

/** substract two time values \param[in] a first time value \param[in] b second time value \return result of a-b */
DllSpec TimeStamp operator-(const TimeStamp &a, const TimeStamp &b);

/** compare two time values \param[in] a first time value \param[in] b second time value \return result of a==b */
inline bool operator==(const TimeStamp &a, const TimeStamp &b) { return a.tv_sec==b.tv_sec && a.tv_nsec==b.tv_nsec; }

/** compare two time values \param[in] a first time value \param[in] b second time value \return result of a!=b */
inline bool operator!=(const TimeStamp &a, const TimeStamp &b) { return a.tv_sec!=b.tv_sec && a.tv_nsec!=b.tv_nsec; }

/** compare two time values \param[in] a first time value \param[in] b second time value \return result of a<b */
inline bool operator<(const TimeStamp &a, const TimeStamp &b)  { return a.tv_sec<b.tv_sec || (a.tv_sec==b.tv_sec && a.tv_nsec<b.tv_nsec); }

/** compare two time values \param[in] a first time value \param[in] b second time value \return result of a<=b */
inline bool operator<=(const TimeStamp &a, const TimeStamp &b) { return a.tv_sec<b.tv_sec || (a.tv_sec==b.tv_sec && a.tv_nsec<=b.tv_nsec); }

/** compare two time values \param[in] a first time value \param[in] b second time value \return result of a>b */
inline bool operator>(const TimeStamp &a, const TimeStamp &b)  { return a.tv_sec>b.tv_sec || (a.tv_sec==b.tv_sec && a.tv_nsec>b.tv_nsec); }

/** compare two time values \param[in] a first time value \param[in] b second time value \return result of a>=b */
inline bool operator>=(const TimeStamp &a, const TimeStamp &b) { return a.tv_sec>b.tv_sec || (a.tv_sec==b.tv_sec && a.tv_nsec>=b.tv_nsec); }

/** convert int to time value by multiplying with MS unit \param[in] t time in milliseconds \param[in] u Unit \return time value */
inline TimeStamp operator*(int t, TimeStamp::Unit_MS) {
   return TimeStamp(t/1000,t%1000*1000000);
}

/** convert int to time value by multiplying with S unit \param[in] t time in seconds \param[in] u Unit \return time value */
inline TimeStamp operator*(int t, TimeStamp::Unit_S) {
   return TimeStamp(t,0);
}


} // namespace 

#undef DllSpec

#endif
