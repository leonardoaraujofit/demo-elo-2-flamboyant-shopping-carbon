#ifndef HTML_TYPES_H_20150126 
#define HTML_TYPES_H_20150126 

#include <map>
#include <string>

#ifndef DOXYGEN
namespace vfihtml {
#endif

   /** map<string,string> */
   typedef std::map<std::string,std::string> stringmap;
}

#endif


