/*****************************************************************************/
/* This source code is confidential proprietary information of VeriFone Inc. */
/* and is an unpublished work or authorship protected by the copyright laws  */
/* of the United States. Unauthorized copying or use of this document or the */
/* program contained herein, in original or modified form, is a violation of */
/* Federal and State law.                                                    */
/*                                                                           */
/*                         Verifone Inc.                                     */
/*                        Copyright 2015                                     */
/*****************************************************************************/

/*
;|===========================================================================+
;| FILE NAME:  | ABS-Emv.h
;|-------------+-------------------------------------------------------------+
;| DEVICE:     | EMV Abstraction API defintion
;|-------------+-------------------------------------------------------------+
;| DESCRIPTION:| VERTEX EMV application interface
;|             | 
;|-------------+-------------------------------------------------------------+
;| TYPE:       |
;|-------------+-------------------------------------------------------------+
;| COMMENTS:   |
;|===========================================================================+
*/

#ifndef __ABS_EMV_H__
#define __ABS_EMV_H__

#ifdef __cplusplus
extern "C"
{
#endif

/// @addtogroup  ABSTRACTION_LAYER Vertex Abstraction Layer
/// @{


//---------------------------------------------------------------------------------
//   Abstraction Layer Definitions
//---------------------------------------------------------------------------------

typedef enum {
    CRD_STATUS_OK = 0,                          ///< Good Completion
    CRD_STATUS_ABSENT,                          ///< Card not present
    CRD_STATUS_PRESENT,                         ///< Card is present
    CRD_STATUS_ACTIVE,                          ///< Card is active
    CRD_STATUS_NOT_ACTIVE,                      ///< Unable to activate
    CRD_STATUS_REMOVED,                         ///< Card was removed
    CRD_STATUS_UNSUPPORTED,                     ///< Card is not supported
    CRD_ERR_EXCHG,                              ///< Error during exchange
    CRD_ERR_MULT_CARDS,                         ///< Mulitple cards in field
    CRD_ERR_READER_ID,                          ///< Invalid Reader Id
    CRD_ERR_POWER,                              ///< Error during activation
    CRD_ERR_INVALID_OPTION,                     ///< Invalid option selected
    CRD_ERR_OPTION_READ_ONLY,                   ///< Object is read only
    CRD_ERR_TIMEOUT,                            ///< Exchanged timed out
    CRD_ERR_INTERNAL_ERROR,                     ///< Failed during attempt to execute
    CRD_ERR_DATA_TOO_LARGE,                     ///< Data length parameter too large
} CRD_READER_STATUS_t;


///< CRD_DETECT options flags
#define CRD_DET_ACTIVATE        1               ///< check immediate for card present
#define CRD_DET_NO_ACTIVATE     2               ///< check card present but don't activate

///< CRC_Ident return values
#define CRD_IDENT_ERR_PARAM     -1              ///< Invalid parameter
#define CRD_IDENT_ERR_INIT      -2              ///< Readers not yet intialized

typedef struct STRUCT_CRD_CARD_PROTOCOL {
    unsigned long EMV_Contactless : 1;          ///< Bit Field CTLS - EMV
    unsigned long ISO_14443 : 1;                ///< Bit Field CTLS - ISO_14443
    unsigned long ISO_18092 : 1;                ///< Bit Field CTLS - ISO_18092bit field
    unsigned long Mifare : 1;                   ///< Bit Field CTLS - Mifare
    unsigned long EMV_SmartCard : 1;            ///< Bit Field Contact - EMV
    unsigned long ISO_Smartcard : 1;            ///< Bit Field Contact - ISO
    unsigned long SAM_slots : 1;                ///< Bit Field Contact - SAM Slots Present
    unsigned long ExternalReader : 1;           ///< Bit Field Contact - External Reader
} CRD_CARD_PROTOCOL_t;

typedef struct STRUCT_SW_STRINGS {
    unsigned long readerId;                     ///< Reader Indentifier
    unsigned long pcd;                          ///< PCD identifier
    CRD_CARD_PROTOCOL_t interfaceType;          ///< interfaceType
    unsigned long samSlots;                     ///< Number of SAM Slots (SAM_slots==1)
    unsigned char* emvVersionStr;               ///< Version String for EMV Driver
    unsigned char* osDriverVersionStr;          ///< Version string for the OS Driver
    unsigned char* fwNameStr;                   ///< Firmware Name String
    unsigned char* VendorVersionStr;            ///< Vendor Version String
    unsigned char* VendorNameStr;               ///< Vendor Name String
}CRD_IDENT_t;

typedef enum ENUM_CRD_EXCHANGE_STATUS {
    CRD_EXCHG_OK = 0,                           ///< Good Status
    CRD_EXCHG_ERR,                              ///< Error During Exchange
    CRD_EXCHG_BAD_HANDLE,                       ///< Bad Reader Id Provided
    CRD_EXCHG_NOT_POWERED,                      ///< Card not active
    CRD_EXCHG_INVALID_OPTION                    ///< Invalid Options
} CRD_EXCHANGE_STATUS_t;

typedef enum ENUM_CRD_EXCHAGE_OPTIONS {
    CRD_CARDTYPE_A = 0,                         ///< EMV CTLS Card Type A
    CRD_CARDTYPE_B,                             ///< EMV CTLS Card Type B
    CRD_EMV_APDU,                               ///< EMV Type APDU
    CRD_ISO_APDU,                               ///< ISO type APDU
    CRD_VENDOR_APDU                             ///< Vendor specific command
}CRD_EXCHANGE_OPTIONS_t;

typedef enum ENUM_CRD_POWER_OPTIONS {           ///< In CRD_POWER_t
    CRD_VCC_FIELD_OFF = 0,                      ///< Power off card Contact/CTLS
    CRD_ACTIVATE,                               ///< CTLS Field On/Contact Cold Reset
    CRD_STATUS,                                 ///< Activation status Active or not active
    CRD_REMOVE,                                 ///< Prepare card for removal
    CRD_CONTACT_WARM_RESET                      ///< Contact Warm Reset function
}CRD_POWER_OPT_t;

typedef enum ENUM_CRD_CONTACT_STANDARD {        ///< In CRD_POWER_t
    STD_EMV = 0,                                ///< Contact EMV Cold Reset
    STD_ISO,                                    ///< Contact ISO Mode Cold Reset
    STD_VISACASH,                               ///< Contact VISA Cash mode -VISA cash card type
    STD_NO_STANDARD,                            ///< Contact No protocol standard - deviating cards
    STD_NOT_ISO_38400,                          ///< Contact No Standard with 38400 Bit rate (special card)
}CRD_CT_STANDARD;

typedef enum ENUM_CRD_CONTACT_CLASS {           ///< In CRD_POWER_t
    CLASS_TYPE_A = 0,                           ///< 4.5V - 5.5V 60mA
    CLASS_TYPE_B,                               ///< 2.7V - 3.3V 50mA
    CLASS_TYPE_AB,                              ///< Automatic Class support
    VOLT_1v8,                                   ///< 1.8V
}CRD_CT_CLASS_t;

typedef enum ENUM_CRD_PTS_MGT {                 ///< In CRD_POWER_t
    PTS_MANUAL = 0,                             ///< PTS request is initiated by application
    PTS_AUTO,                                   ///< PTS request is automatic by reader (default)
    PTS_NOT_ALLOWED                             ///< PTS not allowed
}CRD_CT_PTS_t;

typedef struct {
    CRD_POWER_OPT_t pwrAction;                  ///< Power action to take
    CRD_CT_STANDARD contact_standard;           ///< Contact card protocol standard for power up
    CRD_CT_CLASS_t  contact_class;              ///< Contact card class management type
    CRD_CT_PTS_t    contact_pts_mode;           ///< Contact PTS management type
    unsigned long   card_remove_timeout;        ///< card remove wait period in microseconds.
}CRD_POWER_t;

///< STRUCT_PPS_REQUEST ProtocolType parameter definition
#define CRD_PROTOCOL_T0 1                       ///< ProtocolType - T=0 protocol selection
#define CRD_PROTOCOL_T1 2                       ///< ProtocolType - T=1 protocol selection
#define CRD_OVERRIDE_ATR 3                      ///< Override ATR to string at atrPtr

///< STRUCT_PPS_REQUEST Selection Flags parameter definition.  Bit ORed flags for each PPS byte
#define CRD_SELECT_PPS1 1                       ///< SelectionFlags - PPS1 byte present
#define CRD_SELECT_PPS2 2                       ///< SelectionFlags - PPS2 byte present
#define CRD_SELECT_PPS3 4                       ///< SelectionFlags - PPS3 byte present

typedef struct STRUCT_PPS_REQUEST {
    unsigned long ProtocolType;                 ///< ProtocolType Set CRD_PROTOCOL_T0, CRD_PROTOCOL_T1
    unsigned char SelectionFlags;               ///< SelectionFlags Bit ORed flags determine which PPS bytes are present
    unsigned char pps1;                         ///< PTS1 PPS1 parameter character
    unsigned char pps2;                         ///< PTS2 PPS2 parameter character
    unsigned char pps3;                         ///< PTS3 PPS2 parameter character
    unsigned char* atrPtr;                      ///< If ProtocolType set to CRD_OVERRIDE_ATR this points to ATR string.
}CRD_PPSREQ_t;

#define CRD_CTLS_NO_TYPE                    0x00000000
#define CRD_CTLS_ISO14443A                  0x00000001
#define CRD_CTLS_ISO14443B                  0x00000002
#define CRD_CTLS_NFC_F212                   0x00000004
#define CRD_CTLS_NFC_F424                   0x00000008
#define CRD_CTLS_MIFARE_1K                  0x00010000
#define CRD_CTLS_MIFARE_UL                  0x00020000
#define CRD_CTLS_MIFARE_AND_ISO14443A       0x00040000
#define CRD_CTLS_MIFARE_4K_AND_ISO14443A    0x00080000
#define CRD_CTLS_MIFARE_4K                  0x00100000
#define CRD_CONTACT_DEVICE                  0xFFFFFFFF



typedef struct {
    char apiName[48];                           ///< API Function Name for shared interface module
} CDR_RDR_API_t;


// Supported Readers
enum
{
    CRD_CTLS_READER = 0,                        ///< Contactless Reader
    CRD_CONTACT_SLOT1,                          ///< Customer facing card slot
    CRD_CONTACT_SLOT2,                          ///< MSAM 1
    CRD_CONTACT_SLOT3,                          ///< MSAM 2
    CRD_CONTACT_SLOT4,                          ///< MSAM 3
    CRD_CONTACT_SLOT5,                          ///< MSAM 4
    CRD_NUM_SUPPORTED_READERS
};


//---------------------------------------------------------------------------------
//   EMV Abstraction API
//---------------------------------------------------------------------------------

//****************************************************************************
/// @brief Identify reader support on this platform
/// Return reader information in the array of strutures pointed to by pRdr.
/// CRD_NUM_SUPPORTED_READERS defines the maximum number of readers supported.
///
/// @param[out] pRdr                - Where reader information is stored
/// @param[in]  iMaxReaders         - is the number of elements in pRdr
///
/// @returns >0                     - Number of readers resident
/// @returns =0                     - None avaiable
/// @returns CRD_IDENT_ERR_PARAM    - Invalid Parameter
/// @returns CRD_IDENT_ERR_INIT     - Must call CRD_Initialize first.
//****************************************************************************
int CRD_Ident(CRD_IDENT_t* pRdr, int iMaxReaders);

//****************************************************************************
/// @brief Returns the number of readers for CRD_ReadConfig and CRD_Ident
///  functions
///
/// @returns Number of readers resident
//****************************************************************************
int CRD_NumberConfigs(void);

//****************************************************************************
/// @brief Initialize Card Reader Abstraction
///
/// @param[in] ulOptions            - not currenly used
///
/// @returns CRD_STATUS_OK          - Reader initialization successful
/// @returns CRD_STATUS_UNSUPPORTED - Error Intializing Reader(s)
//****************************************************************************
CRD_READER_STATUS_t CRD_Initialize(unsigned long ulOptions);

//****************************************************************************
/// @brief Close all open readers
/// 
/// @param None
/// 
/// @returns None
//****************************************************************************
void CRD_Shutdown(void);

//****************************************************************************
/// @brief Return Device present
///
/// @param[in] ulReaderId           - Reader ID recevied by CRD_Ident
/// @param[in] ulOptions            - Options for Check Present (not currently used)
///
/// @returns CRD_STATUS_ABSENT      - Card Not found
/// @returns CRD_STATUS_PRESENT     - Card found
/// @returns CRD_ERR_READER_ID      - Invalid Reader ID
/// @returns CRD_ERR_INTERNAL_ERROR - Error during detect or activation
//****************************************************************************
CRD_READER_STATUS_t CRD_DevicePresent(unsigned long ulReaderId, unsigned long ulOptions);


//****************************************************************************
/// @brief Detect card holder device
///
/// @param[in] ulReaderId           - Reader ID recevied by CRD_Ident
/// @param[in] ulOptions            - Options for detect @n
///            CRD_DET_ACTIVATE     - CTLS Leave field active if card not found @n                                    
///            CRD_DET_NO_ACTIVATE  - Deactivate field if card not found @n
/// @param[in]  ulActTimoutUs       - Activation timeout (microseconds to wait for card)
///
/// @returns CRD_STATUS_ABSENT      - No card
/// @returns CRD_STATUS_PRESENT     - Card found
/// @returns CRD_STATUS_ACTIVE      - Card found and active
/// @returns CRD_STATUS_REMOVED     - Card removed
/// @returns CRD_ERR_EXCHG          - Exchange error
/// @returns CRD_ERR_MULT_CARDS     - Multiple cards
/// @returns CRD_ERR_READER_ID      - Invalid Reader ID
/// @returns CRD_ERR_POWER          - Reader not powered
/// @returns CRD_ERR_TIMEOUT        - Wait for Card Present time out
//****************************************************************************
CRD_READER_STATUS_t CRD_Detect(unsigned long ulReaderId, unsigned long ulOptions, unsigned long ulActTimoutUs);


//****************************************************************************
/// @brief Activate card holder device @n
/// CTLS: @n
/// - If not activated, activate device @n
/// - Perform Remove operation @n
/// - Perform Technology poll on activated Technology @n
/// - Perform AnitCollision @n
/// - Perform Technology Activate (Protocol Activation) - Device ready for APDU Exchange. @n
/// - Return ST_EMV_PICC_PARAMS_t data structure. @n
/// @n
/// Contact: @n
/// - Activate VCC and perform Reset sequence @n
/// - Return Answer To Reset (ATR) from card. @n
/// If previously activated then ATR is returned from prevous Reset. @n
/// 
/// @param[in] ulReaderId           - Reader ID recevied by CRD_Open
/// @param[in]  ulOptions           - Not currently Used
/// @param[out] pulCardType         - Card type Activated bit mask returned
///                                     CTLS_TECHTYPE_NONE  
///                                     CTLS_TECHTYPE_A     
///                                     CTLS_TECHTYPE_B     
///                                     CTLS_TECHTYPE_F212     
///                                     CTLS_TECHTYPE_F424     
///                                     CTLS_TECHTYPE_MIFARE
/// @param[out] pucCardInfo         - ATR or Ctls card info
/// @param[in] pulInfoLength        - Size of CardInfo buffer
/// @param[out] pulInfoLength       - Size of CardInfo returned data
/// @param[in]  ulActTimoutUs       - Activation timeout (microseconds to wait for card)
///
/// @returns CRD_STATUS_ABSENT      - No card
/// @returns CRD_STATUS_PRESENT     - Card was found
/// @returns CRD_STATUS_ACTIVE      - CTLS Card found and active
/// @returns CRD_STATUS_REMOVED     - Card was removed
/// @returns CRD_EXCHG_ERR          - Error executing protocol
/// @returns CRD_TWO_CARDS          - CTLS multiple cards detected
/// @returns CRD_ERR_READER_ID      - Invalid Reader ID
/// @returns CRD_ERR_POWER          - Error during power up sequence
//****************************************************************************
CRD_READER_STATUS_t  CRD_Activate(unsigned long ulReaderId, unsigned long ulOptions,
                                             unsigned long* pulCardType, unsigned char* pucCardInfo,
                                             unsigned long* pulInfoLength , unsigned long ulActTimoutUs);


//****************************************************************************
/// @brief Exchange data with card holder device
///
/// @param[in] ulReaderId           - Reader ID recevied by CRD_Open
/// @param[in] eOptions             - Type of Exchange @n
///                                     CRD_CARDTYPE_A @n
///                                     CRD_CARDTYPE_B @n
///                                     CRD_EMV_APDU @n
///                                     CRD_ISO_APDU @n
///                                     CRD_VENDOR_APDU @n
/// @param[in] usInDataLen          - Length of transmit data_exch_buff
/// @param[in] pucDataIn            - Transmit data pointer
/// @param[out] pusOutDataLen       - (Response Length)
/// @param[out] pucDataOut          - Response is stored at this location
/// @param[out] usDataOutSize       - Data out buffer size.  Maximum response size.
///
/// @returns CRD_EXCHG_OK           - No error
/// @returns CRD_EXCHG_ERR          - Exchange error
/// @returns CRD_ERR_READER_ID      - Invalid Reader ID
/// @returns CRD_EXCHG_INVALID_OPTION - Invalid parameter
/// @returns CRD_ERR_POWER          - Error card not powered
//****************************************************************************
CRD_EXCHANGE_STATUS_t CRD_Exchange(unsigned long ulReaderId, CRD_EXCHANGE_OPTIONS_t eOptions,
                                              unsigned short usInDataLen, unsigned char* pucDataIn,
                                              unsigned short* pusOutDataLen, unsigned char* pucDataOut,
                                              unsigned short usDataOutSize);


//****************************************************************************
/// @brief Modify power to card holder device
///
/// @param[in] ulReaderId           - Reader ID recevied by CRD_Initialize
/// @param[in] pOptions             - Power options for reader @n
///     pOPtions->pwrAction         - Power action to carry out @n
///                                     CRD_VCC_FIELD_OFF @n
///                                     CRD_ACTIVATE @n
///                                     CRD_STATUS @n
///                                     CRD_REMOVE: Performs an EMV Picc Removal procedure.@n
///                                         @note If PICC is not removed from the field, 
///                                         will return with a timeout status after a 
///                                         card_remove_timeout period in pOptions. @n
///                                     CRD_CONTACT_WARM_RESET @n
///     pOptions->contact_standard  - Contact reader protocol standard @n
///                                     STD_EMV @n
///                                     STD_ISO @n
///                                     STD_VISACASH @n
///                                     STD_NO_STANDARD @n
///                                     STD_NOT_ISO_38400 @n
///     pOptions->contact_class     - Contact reader power class @n
///                                     CLASS_TYPE_A @n
///                                     CLASS_TYPE_B @n
///                                     CLASS_TYPE_AB @n
///                                     VOLT_1v8 @n
///     pOptions->contact_pts_mode  - Contact reader PTS negotiaion mode @n
///                                     PTS_MANUAL @n
///                                     PTS_AUTO @n
///                                     PTS_NOT_ALLOWED @n
/// 
/// @returns CRD_STATUS_OK          - pOption CRD_ACTIVATE, CRD_REMOVE, CRD_VCC_FIELD_OFF: No error
/// @returns CRD_STATUS_ACTIVE      - pOption CRD_STATUS: Reader is active
/// @returns CRD_STATUS_NOT_ACTIVE  - pOption CRD_STATUS: Reader is inactive
/// @returns CRD_EXCHG_ERR          - Exchange error
/// @returns CRD_ERR_READER_ID      - Invalid Reader ID
/// @returns CRD_ERR_INVALID_OPTION - Invalid pOptions parameter
//****************************************************************************
CRD_READER_STATUS_t  CRD_Power(unsigned long ulReaderId, CRD_POWER_t* pOptions);


//****************************************************************************
/// @brief Request PPS exchange with current active card.
/// If Manual PPS request mode is required the PPS request can be generated
/// using this API.  PPS Request must immediately follow ATR.
///
/// @param[in] ulReaderId           - Reader ID recevied by CRD_Ident
/// @param[in] pPpsParams           - Pointer to parameters needed for PPS exchange
///
/// @returns CRD_STATUS_OK          - No error
/// @returns CRD_ERR_EXCHG          - Protocol error
/// @returns CRD_ERR_READER_ID      - Invalid Reader ID
//****************************************************************************
CRD_READER_STATUS_t  CRD_CtPpsRequest(unsigned long ulReaderId, CRD_PPSREQ_t* ppsParams);

//****************************************************************************
/// @brief Request Card Activation Information from specified reader.
/// 
/// Returns device/card information from prior activation
/// Expectation is that CRD_Activate was used.
///
/// @param[in]  ulReaderId          - Reader ID recevied by CRD_Open
/// @param[in]  ulOptions           - Not currently Used
/// @param[out] pulCardType         - Report Card type
/// @param[out] pucCardInfo         - ATR or Ctls card info
/// @param[in]  pulInfoLength       - Size of CardInfo buffer
/// @param[out] pulInfoLength       - Size of CardInfo returned data
///
/// @returns CRD_STATUS_OK          - No error
/// @returns CRD_ERR_EXCHG          - Card data not available
/// @returns CRD_ERR_READER_ID      - Invalid Reader ID
/// @returns CRD_ERR_INVALID_OPTION - Invalid parameter
//****************************************************************************
CRD_READER_STATUS_t CRD_GetCardInfo(unsigned long ulReaderId, unsigned long ulOptions,
                                    unsigned long* pulCardType, unsigned char* pucCardInfo,
                                    unsigned long* pulInfoLength);

/// @}

#ifdef __cplusplus
}   /* extern "C" */
#endif

#endif
