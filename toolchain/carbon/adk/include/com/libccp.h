#ifndef _LIBCCP_H_
#define _LIBCCP_H_

#ifdef __cplusplus
extern "C"
{
#endif

#if (defined _VRXEVO || defined _WIN32)
#  if   defined VFI_CCP_DLL_EXPORT
#    define DllSpecCCP __declspec(dllexport)
#  elif defined VFI_CCP_DLL_IMPORT && defined _VRXEVO
#    define DllSpecCCP __declspec(dllimport) // dllimport not required for Windows
#  else
#    define DllSpecCCP
#  endif
#elif defined __GNUC__ && defined VFI_CCP_DLL_EXPORT
#  define DllSpecCCP  __attribute__((visibility ("default")))
#else
#  define DllSpecCCP
#endif


enum CCPMenuName{
	CCP_MENU_MAIN = 0,
	CCP_MENU_MAIN_NO_PWD,
	CCP_MENU_CONFIGURATION,
	CCP_MENU_CONFIGURATION_NO_PWD,
	CCP_MENU_DIAGNOSTIC,
	CCP_MENU_INFORMATION
};

enum CCPErrorCodes {
	CCP_LIB_OK = 0,
	CCP_LIB_FAIL
};

/**
 * Show selected dialog from Com Control Panel to configure your communication device
 * 
 * When using this call on an unattended device (ux300, ux301, ux410) the GUI property UI_PROP_TIMEOUT
 * will be checked. If the value is positive, then this value will be respected. If not, then the property will be
 * set to 60000 milliseconds at the beginning of ccp_show and reverted back to its previous value when ccp_show returns.
 * 
 * 
 * @brief ccp_show					Show selected dialog from Com Control Panel to configure your
 *									communication device
 *
 * @param which						Which dialog to show. See CCPMenuName.
 * @param infodb					path to database for storing of the settings (eg. "flash/info.db")
 *									note: this path is relative to your binary directory
 * @param useInternalKeyTimeout		Use an ccp internal timeout which specifies how long it takes for a character in
 * 									a password dialog to transform from that character into a star (*).
 * 									See UI_PROP_PASSWORD_SHOW_CHAR in the vfigui documentation. The value of this
 * 									GUI parameter will be reset to its previous value once the function returns
 * 
 * @return							will return CCP_LIB_FAIL on errors, otherwise CCP_LIB_OK
 */
DllSpecCCP enum CCPErrorCodes ccp_show(enum CCPMenuName which, const char *infodb, bool useInternalKeyTimeout = true);

#ifdef __cplusplus
} // extern C
#endif

#endif //_LIBCCP_H_
