/*****************************************************************************/
/* This source code is confidential proprietary information of VeriFone Inc. */
/* and is an unpublished work or authorship protected by the copyright laws  */
/* of the United States. Unauthorized copying or use of this document or the */
/* program contained herein, in original or modified form, is a violation of */
/* Federal and State law.                                                    */
/*                                                                           */
/*                         Verifone Inc.                                     */
/*                        Copyright 2015                                     */
/*****************************************************************************/
/*
;|===========================================================================+
;| FILE NAME:  | cryptoAbstr.h
;|-------------+-------------------------------------------------------------+
;| DEVICE:     | Vertex Crypto Abstraction API defintions
;|-------------+-------------------------------------------------------------+
;| DESCRIPTION:| VERTEX EMV Crypto library
;|             | 
;|-------------+-------------------------------------------------------------+
;| TYPE:       |
;|-------------+-------------------------------------------------------------+
;| COMMENTS:   |
;|===========================================================================+
*/

#ifndef __ABS_EMV_CRYPTO_H__
#define __ABS_EMV_CRYPTO_H__

/// @addtogroup  ABSTRACTION_LAYER Vertex Crypto Library
/// @{

typedef enum 
{
    ACROPT_SHA1 = 1,                // Supported on all platforms
    ACROPT_SHA2,                    // Not currently supported on this platform
    ACROPT_SHA3,                    // Not currently supported on this platform
    ACROPT_SHA224,                  // Not currently supported on this platform
    ACROPT_SHA256,                  // Supported on all platforms
    ACROPT_SHA384,                  // Not currently supported on this platform
    ACROPT_SHA512,                  // Not currently supported on this platform
    ACROPT_SHA512_224,              // Not currently supported on this platform
    ACROPT_SHA512_256,              // Not currently supported on this platform
}ACR_SHA_OPTIONS_t;

typedef enum
{
    ACR_DESX1KE=0x02,               // (02h) DEAX encryption with single-length key
    ACR_DESX1KD,                    // (03h) DEAX encryption with single-length key
    ACR_DESX2KE,                    // (04h) DEAX encryption with double-length key
    ACR_DESX2KD,                    // (05h) DEAX encryption with double-length key
    ACR_DESX3KE,                    // (06h) DEAX encryption with triple-length key
    ACR_DESX3KD,                    // (07h) DEAX encryption with triple-length key
    ACR_DESE,                       // (08h) DEA encryption with single-length key
    ACR_DESD,                       // (09h) DEA encryption with single-length key
    ACR_TDES2KE=0x0C,               // (0Ch) TDEA encryption with double-length key
    ACR_TDES2KD,                    // (0Dh) TDEA encryption with double-length key
    ACR_TDES3KE,                    // (0Eh) TDEA encryption with triple-length key
    ACR_TDES3KD,                    // (0Fh) TDEA encryption with triple-length key
}ACR_ENUM_DES_OPTIONS_t;


typedef enum
{
    ACR_AES128E = 0x04,             //  (04h) AES encryption using a 128-bit key
    ACR_AES128D,                    //  (05h) AES decryption using a 128-bit key
    ACR_AES192E,                    //  (06h) AES encryption using a 192-bit key
    ACR_AES192D,                    //  (07h) AES decryption using a 192-bit key
    ACR_AES256E,                    //  (08h) AES encryption using a 256-bit key
    ACR_AES256D,                    //  (09h) AES decryption using a 256-bit key
}ACR_ENUM_AES_OPTIONS_t;


#define ACR_ERR_NONE    0
#define ACR_ERR_PARAM   -1

//****************************************************************************
/// @brief Returns library identification string
///
/// @param[out] pucVerString        - Buffer pointer where version string stored
/// @param[in]  ulVerLength         - Size of buffer in bytes
/// @param[out] ulVerLength         - Currently not used
///
/// @returns ACR_ERR_NONE			- Successfull completion
/// @returns ACR_ERR_PARAM          - Invalid Parameter
//****************************************************************************
unsigned int ACR_Ident(unsigned char * pucVerString, long lVerLength);

//****************************************************************************
/// @brief Generate Unpredictable Number of required length. UN must meet PCI @n
/// and EMV requirements on UN.
///
/// @param[out] puiRnd              - Unpredicatable number buffer (bytes)
/// @param[in] uiRndLen             - Required length of UN bytes
/// @param[in] ulOptions            - Currently not used
///
/// @returns >0                     - Length of UN returned
/// @returns =0                     - Zero for UN generation error
///
//****************************************************************************
unsigned int ACR_Random (unsigned int* puiRnd
                         , unsigned int uiRndLen
                         , unsigned long ulOptions);


//****************************************************************************
/// @brief Perform RSA certificate verification using Public keys.
///
/// @param[in] pucMsg               - Certificate/Msg to verify
/// @param[in] uiMsgBitlen			- Message Length in bits
/// @param[in] pucMod               - RSA Key Modulus
/// @param[in] uiModBitlen               - Modulus length in bits
/// @param[in] pucExp               - Public exponent
/// @param[in] uiExpBitlen          - Public exponent length in bits
/// @param[in] pucResult            - Verification result
/// @param[in] ulOptions            - Not Currently Used
///
/// @returns >0                     - Length of RSA Result
/// @returns =0                     - Zero for RSA generation error
//****************************************************************************
unsigned int ACR_RSA (unsigned char *pucMsg
                      , unsigned int uiMsgBitlen
                      , unsigned char *pucMod
                      , unsigned int uiModBitlen
                      , unsigned char *pucExp
                      , unsigned int uiExpBitlen
                      , unsigned char *pucResult
                      , unsigned long ulOptions);


//****************************************************************************
/// @brief Perform SHA operation on data.
///
/// @param[in] pucMsg               - Certificate/Msg to verify
/// @param[in] uiMlen               - Message length
/// @param[in] pucResult            - SHA  result
/// @param[in] ulOptions            - Options supported for SHA calculation
///                                     ACROPT_SHA1
///                                     ACROPT_SHA256
/// @returns ACR_ERR_NONE			- Option supported
/// @returns ACR_ERR_PARAM          - Option not supported
//****************************************************************************
unsigned int ACR_SHA (unsigned char *pucMsg
                      , unsigned int uiMlen
                      , unsigned char *pucResult
                      , ACR_SHA_OPTIONS_t ulOptions);


//****************************************************************************
/// @brief Perform AES calculation using supplied keys.
///
/// @param[in] pucKey               - Key to use - multiple of 8 bytes
/// @param[in] pucMsg               - Input data
/// @param[out] pucResult           - Output data
/// @param[in] ulOptions            - Options defined for use by AES function
///
/// @returns >0                     - Length of SHA returned
/// @returns =0                     - Zero for SHA generation error
//****************************************************************************
unsigned int ACR_AES (unsigned char *pucKey
                      , unsigned char *pucMsg
                      , unsigned char *pucResult
                      , ACR_ENUM_AES_OPTIONS_t ulOptions);


//****************************************************************************
/// @brief Perform DES calculation using supplied key.
///
/// @param[in] pucKey               - 8 byte key
/// @param[in] pucMsg               - pointer to input data
/// @param[out] pucResult           - pointer to output data
/// @param[in] ulOptions            - Options
///                                     ACR_DESX1KE
///                                     ACR_DESX1KD
///                                     ACR_DESX2KE
///                                     ACR_DESX2KD
///                                     ACR_DESX3KE
///                                     ACR_DESX3KD
///                                     ACR_DESE
///                                     ACR_DESD
///                                     ACR_TDES2KE
///                                     ACR_TDES2KD
///                                     ACR_TDES3KE
///                                     ACR_TDES3KD
/// 
/// @returns >0                     - Length of SHA returned
/// @returns =0                     - Zero for SHA generation error
//****************************************************************************
unsigned int ACR_DES (unsigned char *pucKey
                      , unsigned char *pucMsg
                      , unsigned char *pucResult
                      , ACR_ENUM_DES_OPTIONS_t tDesOptions);


/// @}
#endif

