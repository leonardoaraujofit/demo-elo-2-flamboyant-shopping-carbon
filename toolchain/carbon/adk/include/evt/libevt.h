#ifndef _LIBEVT_H_
#define _LIBEVT_H_

// --------------------------------------------------------------------------------------------------------------------


#ifdef _VRXEVO

#include <stdint.h>
//Inclusion of "libevt.h" header assumes app will be user of libevt and therefore imports functions from library by default.
#ifdef _EVT_API_EXPORT
#define EVT_API __declspec(dllexport)
#else
#define EVT_API __declspec(dllimport)
#endif

#ifndef ETIME //Seems like Verix errno.h file is non-standard and it has no such status.
#define ETIME 62  /* Timer expired */
#endif

#else

#include <stdint.h>

#ifndef EVT_API
#define EVT_API
#endif

#endif

// --------------------------------------------------------------------------------------------------------------------

/// Waiting hangle type
typedef unsigned long int WaiterHandle;

/// Timer Event mask that allow to receive all timer events
const long EVT_ANY_HANDLE = -1;

/// Invalid WaiterHandle value
const WaiterHandle EVT_INVALID_HANDLE = 0;

/// Timeout value for infinite wait
const int EVT_INFINITE_WAIT = -1;

/// Unique ID of Event
typedef uint64_t UID;

/// Size of Event additional data
const uint32_t EVT_DATA_SIZE = 64; // 64 bytes

/// Maximal event count to wait for
const uint32_t EVT_MAX_COUNT = 256;
 
// --------------------------------------------------------------------------------------------------------------------

/**
 * ADK Event structure.
 */
#ifdef __cplusplus
extern "C"
{
#endif

struct Event
{
    /// Event ID. Must be specified by both raise and wait sides.
    UID id;
    /// OS Event handle or EVT_ANY_HANDLE
    long handle;
    // 'raised_' attributes are set by raise side and read by wait side.
    /// Timestamp of raised event. Unix time in seconds.
    uint64_t raised_timestamp;
    /// Event version. Data is componentID/eventID specific.
    uint16_t raised_version;
    /// Raising flag. Indicates that Event was succesfully received.
    uint8_t raised_flag;
    /// OS Event return Handle.
    long raised_handle;
    /// Additional data for received Event.
    char raised_data[EVT_DATA_SIZE];
};


enum OSEvents
{
    OS_NOP = 0, /* non OS event */
    OS_EVT_TIMER, /* user-defined timer */
    OS_EVT_DOCK, /* terminal is docked */
    OS_EVT_UNDOCK, /* terminal is undocked */
    OS_EVT_SHUTDOWN, /* terminal is going to halt */ 

    /* Verix specific events */
    OS_EVT_KBD, 
    OS_EVT_PIPE,   
    OS_EVT_NETWORK,
    OS_EVT_COM1,   
    OS_EVT_COM2,   
    OS_EVT_COM3,   
    OS_EVT_COM4,   
    OS_EVT_COM5,   
    OS_EVT_COM6,   
    OS_EVT_COM7,   
    OS_EVT_COM8,   
    OS_EVT_MAG,
    OS_EVT_USB,
};

/**
 * Waiting policies for event groups.
 */
enum WaitPolicy
{
    WAIT_ANY, /// Stop waiting if at least 1 event from group is raised.
    WAIT_ALL, /// Stop waiting only when all events from group are raised.
};

enum EvtSide
{
    SIDE_RAISER = 0,
    SIDE_WAITER,
    SIDE_BOTH
};

/**
 * EVT engine options
 */
enum EvtOptions
{
    EVT_OPT_DEFAULT = 0,
    EVT_OPT_COPY_NEW_ONLY_EVENTS_IN_GET_BY_HANDLE_FUNC = 0x1 << 0, // ADKEVT-161
};

// --------------------------------------------------------------------------------------------------------------------

/** \brief Creates UID based on componentID and eventID.
 *  \details UID Creation helper function for user events.
 *  \param componentID System wide component ID.
 *  \param eventID Component wide event ID.
 *  \return Event UID.
 */
EVT_API UID evt_make_uid(uint16_t componentID, uint16_t eventID);

/** \brief Creates UID based on componentID and OS Event.
 *  \details UID Creation helper function for OS events.
 *  \param componentID System wide component ID.
 *  \param osEvent OS Event.
 *  \return Event UID.
 */
EVT_API UID evt_make_uid_os(uint16_t componentID, OSEvents osEvent);

/** \brief Returns componentID part of UID
 *  \param UID 
 *  \return componentID
 */ 
EVT_API uint16_t evt_get_component_id( UID );

/** \brief Starts a new timer.
 *  \details Schedules an event after a timeout.
 *  \param timeout Timeout in ms. Max value is 24h (86,400,000 ms)
 *  \return Negative: error with errno value. Non negative: timer handle.
 */
EVT_API long evt_start_timer(uint32_t timeout);

/** \brief Deletes a timer.
 *  \details Stops a timer and free associated resources.
 *  \param handle Timer handle acquired by evt_start_timer().
 */
EVT_API void evt_stop_timer(long handle);

/** \brief Initialize EVT library.
 *  \details Creates caching structures, sets ignore to SIGPIPE signal (V/OS - raiser side).
 *  \param side Side to be initialized.
 *  \param componentID System wide component ID.
 *  \param capacity Estimated number of unique events to be processed.
 *  \return Negative: error with errno value. 0: Library successfully initialized.
 */
EVT_API int evt_init(EvtSide side, uint16_t componentID, uint8_t capacity);

/** \brief De-initialize EVT library.
 *  \details Destroys caching structures, do cleanup. Make sure all waiters are stopped before this call.
 *  \return Negative: error with errno value. 0: Library successfully de-initialized.
 */
EVT_API int evt_destroy();

/** \brief Wait for a single event.
 *  \details Stops thread execution till 'event' is received or time specified by 'timeout' is up.
 *  \param event An Event to wait for.
 *  \param timeout A valid wait timeout (timeout > 0) in ms. EVT_INFINITE_WAIT - for infinite wait.
 *  \return Negative: error with errno value. 0: Event successfully received. Positive: ETIME - on timeout.
 */
EVT_API int evt_wait(Event *event, int timeout);

/** \brief Wait for a group of events.
 *  \details Stops thread execution till one or all (see 'policy') 'events' are received or time specified by 'timeout' is up.
 *  \param events Array of Events to wait for. Caller is responsible to provide only unique ID.
 *  \param count Number of events represented in 'events' param.
 *  \param timeout A valid wait timeout (timeout > 0) in ms. EVT_INFINITE_WAIT - for infinite wait.
 *  \param policy WaitPolicy for event group.
 *  \return Negative: error with errno value. 0: Event successfully received. Positive: ETIME - on timeout.
 */
EVT_API int evt_wait_group(Event *events, uint32_t count, int timeout, WaitPolicy policy);

/** \brief Raise an event.
 *  \details Raise an event specified by 'event'. Works synchronously and returns after event is sent.
 *  \param event An Event to raise.
 *  \return Negative: error with errno value. 0: Event successfully raised.
 */
EVT_API int evt_raise(Event *event);

/** \brief Initialize asynchronous waiter for a group of events.
 *  \param events Array of Events to wait for.  Caller is responsible to provide only unique ID.
 *  \param count Number of events represented in 'events' param.
 *  \return EVT_INVALID_HANDLE on failure or valid WaiterHandle otherwise.
 */
EVT_API WaiterHandle evt_init_waiter(Event* events, uint32_t count);

/** \brief Get events so far collected by asynchronous waiter.
 *  \details Function returns immediately. Returns either all or only 'new' events (collected from the last call), depends from EVT_OPT_COPY_NEW_ONLY_EVENTS_IN_GET_BY_HANDLE_FUNC compatibility option.
 *  \param waiter A handle created by evt_init_waiter(). Passing here a destroyed handle cause an undefined behavior.
 *  \param events Array of Events to fill.
 *  \param count Number of events represented in 'events' param. (Must be equal or more as passed to evt_init_waiter()).
 *  \return Negative: error with errno value. 0: success but there is no raised event (since last call). 
 *          Positive: returns number of raised events. Events are copied to 'events' member.
 */
EVT_API int evt_get_by_handle(WaiterHandle waiter, Event* events, uint32_t count);

/** \brief Peek events so far collected by asynchronous waiter.
 *  \details Function returns immediately. Returns either all or only 'new' events, depends from EVT_OPT_COPY_NEW_ONLY_EVENTS_IN_GET_BY_HANDLE_FUNC compatibility option.
 *  \param waiter A handle created by evt_init_waiter(). Passing here a destroyed handle cause an undefined behavior.
 *  \param events Array of Events to fill.
 *  \param count Number of events represented in 'events' param. (Must be equal or more as passed to evt_init_waiter()).
 *  \return Negative: error with errno value. 0: success but there is no raised event (since last call). 
 *          Positive: returns number of raised events. Events are copied to 'events' member.
 */
EVT_API int evt_peek_by_handle(WaiterHandle waiter, Event* events, uint32_t count);

/** \brief Blocks thread execution until waiter receive an event or timeout occurs
 *  \details Function returns only if event is received or timeout occurred
 *  \param waiter A handle created by evt_init_waiter(). Passing here a destroyed handle cause an undefined behavior.
 *  \param timeout A valid wait timeout (timeout > 0) in ms. EVT_INFINITE_WAIT - for infinite wait.
 *  \param policy WaitPolicy for event group.
 *  \return ETIME: timeout occurred
 *          0: event received
 */  
EVT_API int evt_wait_by_handle(WaiterHandle handle, int timeout, WaitPolicy policy);

/** \brief Stops and destroys the asynchronous waiter.
 *  \details Send waiter thread an exit signal then join the thread.
 *  \param waiter A handle created by evt_init_waiter(). Passing here already destroyed handle cause an undefined behavior.
 *  \return Negative: error with errno value. 0: on success.
 */
EVT_API int evt_destroy_waiter(WaiterHandle waiter);

/** \brief Set options changing behavior of ADK-EVT engine
 *  \param options The options are coded as bitmasks and can be combined by a logical or operation. All available options are collected in EvtOptions enum.
*/
EVT_API void evt_set_options(int options);

/** \brief Get options
 *  \return the options set
*/
EVT_API int evt_get_options();

/** \brief get version of a library
  * \return string with version
*/
EVT_API const char *evt_getVersion();

#ifndef _VRXEVO
/** \brief Returns unit dock status
 *  \return 0: Unit is docked -1: Unit is undocked.
*/ 
EVT_API int get_dock_sts();
#endif

#ifdef __cplusplus
} // extern C
#endif

#endif //_LIBEVT_H_
