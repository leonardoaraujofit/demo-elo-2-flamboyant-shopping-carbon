#ifndef CPLOG_H_20150811
#define CPLOG_H_20150811

/** \file cplog.h */

#include <time.h>
#include <string>
#include <vector>
#include <ipc/jsobject.h>

#if (defined _VRXEVO || defined _WIN32)
#  if   defined VFI_CPAPP_SHARED_EXPORT
#    define DllSpec __declspec(dllexport)
#  elif defined VFI_CPAPP_STATIC_EXPORT || defined _WIN32  // dllimport not required for Windows
#    define DllSpec
#  else
#    define DllSpec __declspec(dllimport)
#  endif
#elif defined __GNUC__ && defined VFI_CPAPP_SHARED_EXPORT
#  define DllSpec  __attribute__((visibility ("default")))
#else 
#  define DllSpec
#endif

namespace cpapp {
#if 0
}
#endif

/** severity, similar to syslog */
enum CPSeverity {
   CPS_EMERG, 
   CPS_ALERT, 
   CPS_CRIT,
   CPS_ERR,
   CPS_WARNING,
   CPS_NOTICE,
   CPS_INFO,
   CPS_DEBUG
};

/** message type */
enum CPMsgType {
   CPMT_APPLICATION,  /**< cp message type application */
   CPMT_ERROR,        /**< cp message type error */
   CPMT_APP_LOG       /**< CP app log */ 
};

struct DllSpec CPLogEntry {
   time_t timestamp;             /**< timestamp, automatically set in constructor => 't' */ 
   CPSeverity severity;          /**< severity => 's' */ 
   CPMsgType messageType;        /**< message type => 'msgT' */ 
   std::string message;          /**< message => 'msg' */ 
   std::string errorCode;        /**< error code, only for type CPMT_ERROR => 'eC' */ 
   std::string errorName;        /**< error name, only for type CPMT_ERROR => 'eN' */ 
   std::string errorMessage;     /**< error message, only for type CPMT_ERROR => 'eMsg' */ 
   std::string errorDetails;     /**< error details, only for type CPMT_ERROR => 'eD' */ 
   std::string applicationEvent; /**< error application event, only for type CPMT_APPLICATION , => 'aE' */
   std::string applicationDetails;/**< application details => 'aD' */

   /** constructor, optimized for message type CPMT_ERROR
    * \param[in] s severity
    * \param[in] t message type CPMT_ERROR
    * \param[in] msg message
    * \param[in] errCode error code
    * \param[in] errName error name
    * \param[in] errMessage error message
    * \param[in] errDetails error details
    */
 CPLogEntry(CPSeverity s, CPMsgType t, const std::string &msg,
            const std::string &errCode, const std::string &errName,
            const std::string &errMessage, const std::string &errDetails)
 :severity(s),messageType(t),message(msg),errorCode(errCode),errorName(errName),
      errorMessage(errMessage),errorDetails(errDetails)
      {
         time(&timestamp);
      }

   /** constructor, optimized for message type CPMT_APPLICATION
    * \param[in] s severity
    * \param[in] t message type CPMT_APPLICATION
    * \param[in] msg message
    * \param[in] appEvent application event
    */
 CPLogEntry(CPSeverity s, CPMsgType t, const std::string &msg,
            const std::string &appEvent)
 :severity(s),messageType(t),message(msg),applicationEvent(appEvent)
      {
         time(&timestamp);
      }

   /** constructor, optimized for message type CMPT_APP_LOG
    * \param[in] s severity
    * \param[in] t message type CMPT_APP_LOG
    * \param[in] msg log messages 
    */
   CPLogEntry(CPSeverity s, CPMsgType t, const std::vector<std::string> &msg);

   /** upper limit of the size of the JSON encoded data */
   int size() {
      return 10*16 // up to 16 bytes overhead per element
         +28       // timestamp
         +1        // severity
         +13       // messageType
         +message.length() 
         +errorCode.length()
         +errorName.length()
         +errorMessage.length()
         +errorDetails.length()
         +applicationEvent.length()
         +applicationDetails.length();
   }

};

/** send vector of log entries
 * \param[in] app_id application ID
 * \param[in] app_version application version
 * \param[in] logs vector of log entries
 */
DllSpec void cpLog(const std::string &app_id,
           const std::string &app_version,
           const std::vector<CPLogEntry> &logs);

/** send vector of log entries
 * \param[in] app_id application ID
 * \param[in] app_version application version
 * \param[in] merchant_id merchant ID, which should, which will be added to HTTP header X-VFI-MERCHANT-ID
 * \param[in] system system information (e.g. "ADK"), which will be added to HTTP header X-VFI-APP-ID and field "sys" of log message
 * \param[in] system system version (e.g. "1.0.0"), which will be added to HTTP header X-VFI-APP-VERSION
 * \param[in] logs vector of log entries
 */
DllSpec void cpLog(const std::string &app_id,
           const std::string &app_version,
           const std::string &merchant_id,
           const std::string &system,
           const std::string &system_version,
           const std::vector<CPLogEntry> &logs);

// private function, not exported
std::string terminal_serial();

} // namespace

#undef DllSpec

#endif
