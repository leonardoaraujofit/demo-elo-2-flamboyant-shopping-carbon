/****************************************************************************
*  Product:     InFusion
*  Company:     VeriFone
*  Author:      GSS R&D Germany
*  Content:     Definitions and functions for serial interface
****************************************************************************/


#ifndef EMV_CTLS_TLV_H   /* avoid double interface-includes */
  #define EMV_CTLS_TLV_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdarg.h>

// *************************
// defines for serialization
// *************************

#define HEADER_SIZE          (4)
#define MAX_MESSAGE_SIZE     (2048)
#define MAX_DATA_SIZE        (MAX_MESSAGE_SIZE-HEADER_SIZE)
#define CALLBACK_MESSAGE_SIZE     (2048)  ///< for unidirectional callback

#ifndef MIN
#define MIN(a,b) ((a)<(b)?(a):(b))
#endif

// ========================================================================================================
// === EMVCo and card issuer tags ===
// ========================================================================================================

/// @defgroup EMV_TAGS BER TLV tags used by Transaction Flow
/// @ingroup ADK_SERIALIZATION

/// @defgroup EMVCO_TAGS Tags defined by EMVCo
/// @ingroup EMV_TAGS
///@{
#define TAG_4F_APP_ID               0x4Fu     ///< Application Identifier (AID) - Card               (@b 4F)
#define TAG_50_APP_LABEL            0x50u     ///< Application Label                                 (@b 50)
#define TAG_52_CMD_TO_PERFORM       0x52u     ///< Command to Perform                                (@b 52)
#define TAG_56_TRACK1_EQUIVALENT    0x56u     ///< Track 1 Equivalent Data (MChip card)
#define TAG_57_TRACK2_EQUIVALENT    0x57u     ///< Track 2 Equivalent Data                           (@b 57) @n used as EMV_CTLS_TRANSRES_STRUCT::T_57_DataTrack2
#define TAG_5A_APP_PAN              0x5Au     ///< Application Primary Account Number                (@b 5A) @n used as EMV_CTLS_TRANSRES_STRUCT::T_5A_PAN
#define TAG_5F20_CARDHOLDER_NAME    0x5F20u   ///< Cardholder Name                                   (@b 5F20) @n used as EMV_CTLS_TRANSRES_STRUCT::T_5F20_Cardholder
#define TAG_5F24_APP_EXP_DATE       0x5F24u   ///< Application Expiration Data                       (@b 5F24) @n used as EMV_CTLS_TRANSRES_STRUCT::T_5F24_AppExpDate
#define TAG_5F25_APP_EFF_DATE       0x5F25u   ///< Application Effective Data                        (@b 5F25) @n used as EMV_CTLS_TRANSRES_STRUCT::T_5F25_AppEffDate
#define TAG_5F28_ISS_COUNTRY_CODE   0x5F28u   ///< Issuer Country Code                               (@b 5F28) @n used as EMV_CTLS_TRANSRES_STRUCT::T_5F28_IssCountryCode
#define TAG_5F2A_TRANS_CURRENCY     0x5F2Au   ///< Transaction Currency Code                         (@b 5F2A) @n Configured with EMV_CTLS_TERMDATA_STRUCT::CurrencyTrans (for DCC possibly in ::EMV_CTLS_START_STRUCT). Used as EMV_CTLS_TRANSRES_STRUCT::T_5F2A_CurrencyTrans
#define TAG_5F2D_LANGUAGE           0x5F2Du   ///< Language Preference                               (@b 5F2D) @n used as EMV_CTLS_TRANSRES_STRUCT::T_5F2D_Lang_Pref
#define TAG_5F30_SERVICE_CODE       0x5F30u   ///< Service Code                                      (@b 5F30)
#define TAG_5F34_PAN_SEQUENCE_NB    0x5F34u   ///< PAN Sequence Number                               (@b 5F34) @n used as EMV_CTLS_TRANSRES_STRUCT::T_5F34_PANSequenceNo
#define TAG_5F36_TRANS_CURRENCY_EXP 0x5F36u   ///< Transaction Currency Exponent                     (@b 5F36) @n Configured with EMV_CTLS_TERMDATA_STRUCT::Exp_Trans (for DCC possibly in ::EMV_CTLS_START_STRUCT). Used as EMV_CTLS_TRANSRES_STRUCT::T_5F36_Trx_Currency_Exp
#define TAG_5F57_ACCOUNT_TYPE       0x5F57u   ///< Account Type                                      (@b 5F57)
#define TAG_61_APP_TEMPLATE         0x61u     ///< Application Template                              (@b 61)
#define TAG_6F_FCI_TEMPLATE         0x6Fu     ///< File Control Information (FCI) Template           (@b 6F)
#define TAG_70_AEF_DATA_TEMPLATE    0x70u     ///< AEF Data Template                                 (@b 70)
#define TAG_71_ISS_SCRIPT_TPLT_1    0x71u     ///< Issuer Script Template 1                          (@b 71)
#define TAG_72_ISS_SCRIPT_TPLT_2    0x72u     ///< Issuer Script Template 2                          (@b 72)
#define TAG_73_DIR_DISCR_TEMPLATE   0x73u     ///< Dirctory Discretionary Template                   (@b 73)
#define TAG_77_RS_MSG_TPLT_FRMT_2   0x77u     ///< Response Message Template Format 2                (@b 77)
#define TAG_80_RS_MSG_TPLT_FRMT_1   0x80u     ///< Response Message Template Format 1                (@b 80)
#define TAG_81_BIN_AMOUNT_AUTH      0x81u     ///< Amount Authorization Binary                       (@b 81)
#define TAG_82_AIP                  0x82u     ///< Application Interchange Profile                   (@b 82) @n used as EMV_CTLS_TRANSRES_STRUCT::T_82_AIP
#define TAG_83_CMD_TPLT             0x83u     ///< Command Template                                  (@b 83)
#define TAG_84_DF_NAME              0x84u     ///< Dedicated File Name                               (@b 84) @n used as EMV_CTLS_TRANSRES_STRUCT::T_84_DFName
#define TAG_86_ISS_SCRIPT_CMD       0x86u     ///< Issuer Script Command                             (@b 86)
#define TAG_87_APP_PRIORITY_ID      0x87u     ///< Application Priority Indicator                    (@b 87)
#define TAG_88_SHORT_FILE_ID        0x88u     ///< Short File Identifier (SFI)                       (@b 88)
#define TAG_89_AUTH_CODE            0x89u     ///< Authorization Code                                (@b 89) @n used as EMV_CTLS_HOST_STRUCT::AuthorizationCode
#define TAG_8A_AUTH_RESP_CODE       0x8Au     ///< Authorization Response Code                       (@b 8A) @n used as EMV_CTLS_HOST_STRUCT::AuthResp
#define TAG_8C_CDOL_1               0x8Cu     ///< CDOL 1                                            (@b 8C)
#define TAG_8D_CDOL_2               0x8Du     ///< CDOL 2                                            (@b 8D)
#define TAG_8E_CVM_LIST             0x8Eu     ///< Cardholder Verification Method List               (@b 8E)
#define TAG_8F_CERTIF_AUTH_PK_ID    0x8Fu     ///< Certification Autority Public key Index           (@b 8F)
#define TAG_90_ISS_PK_CERTIF        0x90u     ///< Issuer Public Key Certificate                     (@b 90)
#define TAG_91_ISS_AUTH_DATA        0x91u     ///< Issuer Authentication Data                        (@b 91) @n used as EMV_CTLS_HOST_STRUCT::AuthData
#define TAG_92_ISS_PK_REMAINDER     0x92u     ///< Issuer Public Key Remainder                       (@b 92)
#define TAG_93_SGND_STAT_APP_DATA   0x93u     ///< Signed Static Application Data                    (@b 93)
#define TAG_94_AFL                  0x94u     ///< Application File Locator                          (@b 94)
#define TAG_95_TVR                  0x95u     ///< Terminal Verification Result                      (@b 95) @n used as EMV_CTLS_TRANSRES_STRUCT::T_95_TVR
#define TAG_97_TDOL                 0x97u     ///< TDOL                                              (@b 97)
#define TAG_98_TC_HASH_VALUE        0x98u     ///< Transaction Certificate Hash Value                (@b 98)
#define TAG_99_TRANS_PIN_DATA       0x99u     ///< Transaction PIN Data                              (@b 99)
#define TAG_9A_TRANS_DATE           0x9Au     ///< Transaction Date                                  (@b 9A) @n used as EMV_CTLS_TRANSRES_STRUCT::T_9A_Date, and EMV_CTLS_START_STRUCT::Date
#define TAG_9B_TSI                  0x9Bu     ///< Transaction Status Information                    (@b 9B) @n used as EMV_CTLS_TRANSRES_STRUCT::T_9B_TSI
#define TAG_9C_TRANS_TYPE           0x9Cu     ///< Transaction Type                                  (@b 9C) @n used as EMV_CTLS_START_STRUCT::TransType, EMV_CTLS_TRANSRES_STRUCT::T_9C_TransType
#define TAG_9D_DDF_NAME             0x9Du     ///< Directory Definition File Name                    (@b 9D)
#define TAG_9F01_ACQ_ID             0x9F01u   ///< Acquirer Identifier                               (@b 9F01)
#define TAG_9F02_NUM_AMOUNT_AUTH    0x9F02u   ///< Amount Authorized (Numeric)                       (@b 9F02) @n used as EMV_CTLS_START_STRUCT::Betrag_num
#define TAG_9F03_NUM_AMOUNT_OTHER   0x9F03u   ///< Amount Other (Numeric)                            (@b 9F03)
#define TAG_9F04_BIN_AMOUNT_OTHER   0x9F04u   ///< Amount Other (Binary)                             (@b 9F04)
#define TAG_9F05_APP_DISCR_DATA     0x9F05u   ///< Application Discretionary Data                    (@b 9F05)
#define TAG_9F06_AID                0x9F06u   ///< Application Identifier (AID)-terminal             (@b 9F06) @n used as EMV_CTLS_TRANSRES_STRUCT::T_9F06_AID
#define TAG_9F07_APP_USAGE_CONTROL  0x9F07u   ///< Application Usage Control                         (@b 9F07)
#define TAG_9F08_ICC_APP_VERSION_NB 0x9F08u   ///< Application Version Number - ICC                  (@b 9F08) @n used as EMV_CTLS_TRANSRES_STRUCT::T_9F08_ICC_Appli_Vers_No
#define TAG_9F09_TRM_APP_VERSION_NB 0x9F09u   ///< Application Version Number                        (@b 9F09)
#define TAG_9F0B_CARDHOLDER_NAME_XT 0x9F0Bu   ///< Cardholder Name Extended                          (@b 9F0B)
#define TAG_9F0D_IAC_DEFAULT        0x9F0Du   ///< Issuer Action Code - Default                      (@b 9F0D) @n used as EMV_CTLS_TRANSRES_STRUCT::T_9F0D_IACDefault
#define TAG_9F0E_IAC_DENIAL         0x9F0Eu   ///< Issuer Action Code - Denial                       (@b 9F0E) @n used as EMV_CTLS_TRANSRES_STRUCT::T_9F0E_IACDenial
#define TAG_9F0F_IAC_ONLINE         0x9F0Fu   ///< Issuer Action Code - Online                       (@b 9F0F) @n used as EMV_CTLS_TRANSRES_STRUCT::T_9F0F_IACOnline
#define TAG_9F10_ISS_APP_DATA       0x9F10u   ///< Issuer Application Data                           (@b 9F10) @n used as EMV_CTLS_TRANSRES_STRUCT::T_9F10_DataIssuer
#define TAG_9F11_ISS_CODE_TABLE_ID  0x9F11u   ///< Issuer Code Table Index                           (@b 9F11) @n used as EMV_CTLS_STARTRES_STRUCT::T_9F11_CodeTable
#define TAG_9F12_APP_PREFERRED_NAME 0x9F12u   ///< Application Preferred Name                        (@b 9F12) @n used as EMV_CTLS_STARTRES_STRUCT::T_9F12_PreferredName
#define TAG_9F13_LAST_ONLINE_ATC    0x9F13u   ///< Last Online ATC Register                          (@b 9F13)
#define TAG_9F14_LO_OFFLINE_LIMIT   0x9F14u   ///< Lower Consecutive Offline Limit                   (@b 9F14)
#define TAG_9F15_MERCH_CATEG_CODE   0x9F15u   ///< Merchant Category Code                            (@b 9F15)
#define TAG_9F16_MERCHANT_ID        0x9F16u   ///< Merchant Identifier                               (@b 9F16)
#define TAG_9F17_PIN_TRIES_LEFT     0x9F17u   ///< PIN Try Counter                                   (@b 9F17)
#define TAG_9F18_ISS_SCRIPT_ID      0x9F18u   ///< Issuer Script Identifier                          (@b 9F18)
#define TAG_9F1A_TRM_COUNTRY_CODE   0x9F1Au   ///< Terminal Country Code                             (@b 9F1A)
#define TAG_9F1B_TRM_FLOOR_LIMIT    0x9F1Bu   ///< Terminal Floor Limit                              (@b 9F1B)
#define TAG_9F1C_TRM_ID             0x9F1Cu   ///< Terminal Identification                           (@b 9F1C)
#define TAG_9F1D_TRM_RISK_MNGT_DATA 0x9F1Du   ///< Terminal Risk Management Data                     (@b 9F1D)
#define TAG_9F1E_IFD_SERIAL_NB      0x9F1Eu   ///< Interface Device (IFD) Serial Number              (@b 9F1E) @n used as EMV_CTLS_TERMDATA_STRUCT::SerNum, EMV_CTLS_TRANSRES_STRUCT::T_9F1E_SerNum
#define TAG_9F1F_TRACK_1_DISCR_DATA 0x9F1Fu   ///< Track 1 Discretionary Data                        (@b 9F1F)
#define TAG_9F20_TRACK_2_DISCR_DATA 0x9F20u   ///< Track 2 Discretionary Data                        (@b 9F20)
#define TAG_9F21_TRANS_TIME         0x9F21u   ///< Transaction Time                                  (@b 9F21) @n used as EMV_CTLS_START_STRUCT::Time, EMV_CTLS_TRANSRES_STRUCT::T_9F21_Time
#define TAG_9F22_CERTIF_AUTH_PK_ID  0x9F22u   ///< Certification Authority Public Key Index          (@b 9F22)
#define TAG_9F23_HI_OFFLINE_LIMIT   0x9F23u   ///< Upper Consecutive Offline Limit                   (@b 9F23)
#define TAG_9F26_APP_CRYPTOGRAM     0x9F26u   ///< Application Cryptogram                            (@b 9F26) @n used as EMV_CTLS_TRANSRES_STRUCT::T_9F26_Cryptogramm
#define TAG_9F27_CRYPT_INFO_DATA    0x9F27u   ///< Cryptogram Information Data                       (@b 9F27) @n used as EMV_CTLS_TRANSRES_STRUCT::T_9F27_CryptInfo
#define TAG_9F2D_ICC_PIN_PK_CERTIF  0x9F2Du   ///< ICC PIN Encipherment Public Key Certificate       (@b 9F2D)
#define TAG_9F2E_ICC_PIN_PK_EXP     0x9F2Eu   ///< ICC PIN Encipherment Public Key Exponent          (@b 9F2E)
#define TAG_9F2F_ICC_PIN_PK_REMAIN  0x9F2Fu   ///< ICC PIN Encipherment Public Key Remainder         (@b 9F2F)
#define TAG_9F32_ISS_PK_EXP         0x9F32u   ///< Issuer Public Key Exponent                        (@b 9F32)
#define TAG_9F33_TRM_CAPABILITIES   0x9F33u   ///< Terminal Capabilities                             (@b 9F33)
#define TAG_9F34_CVM_RESULTS        0x9F34u   ///< Cardholder Verification Method (CVM) Results      (@b 9F34) @n used as EMV_CTLS_TRANSRES_STRUCT::T_9F34_CVM_Res
#define TAG_9F35_TRM_TYPE           0x9F35u   ///< Terminal Type                                     (@b 9F35) @n used as EMV_CTLS_TERMDATA_STRUCT::TermTyp, EMV_CTLS_TRANSRES_STRUCT::T_9F35_TermTyp
#define TAG_9F36_ATC                0x9F36u   ///< Application Transaction Counter (ATC)             (@b 9F36) @n used as EMV_CTLS_TRANSRES_STRUCT::T_9F36_ATC
#define TAG_9F37_UNPREDICTABLE_NB   0x9F37u   ///< Unpredictable Number                              (@b 9F37) @n used as EMV_CTLS_TRANSRES_STRUCT::T_9F37_RandomNumber
#define TAG_9F38_PDOL               0x9F38u   ///< Processing Options Data Object List (PDOL)        (@b 9F38)
#define TAG_9F39_POS_ENTRY_MODE     0x9F39u   ///< Point-of-Service (POS) Entry Mode                 (@b 9F39)
#define TAG_9F3A_AMNT_REF_CURRENCY  0x9F3Au   ///< Amount, Reference Currency                        (@b 9F3A) @n used as EMV_CTLS_START_STRUCT::Betrag_num
#define TAG_9F3B_APP_REF_CURRENCY   0x9F3Bu   ///< Application Reference Currency                    (@b 9F3B)
#define TAG_9F3C_TRANS_REF_CURRENCY 0x9F3Cu   ///< Transaction Reference Currency Code               (@b 9F3C) @n same as #TAG_5F2A_TRANS_CURRENCY.
#define TAG_9F3D_TRANS_CURRENCY_EXP 0x9F3Du   ///< Transaction Reference Currency Exponent           (@b 9F3D) @n same as #TAG_5F36_TRANS_CURRENCY_EXP.
#define TAG_9F40_ADD_TRM_CAP        0x9F40u   ///< Additional Terminal Capabilities                  (@b 9F40)
#define TAG_9F41_TRANS_SEQUENCE_NB  0x9F41u   ///< Transaction Sequence Counter                      (@b 9F41) @n used as EMV_CTLS_TRANSAC_STRUCT::TransCount, EMV_CTLS_TRANSRES_STRUCT::T_9F41_TransCount
#define TAG_9F42_APP_CURRENCY_CODE  0x9F42u   ///< Application Currency Code                         (@b 9F42)
#define TAG_9F43_APP_REF_CURRCY_EXP 0x9F43u   ///< Application Reference Currency Exponent           (@b 9F43)
#define TAG_9F44_APP_CURRENCY_EXP   0x9F44u   ///< Application Currency Exponent                     (@b 9F44)
#define TAG_9F45_DATA_AUTHENT_CODE  0x9F45u   ///< Data Authentication Code                          (@b 9F45) @n used as EMV_CTLS_TRANSRES_STRUCT::T_9F45_DataAuthCode
#define TAG_9F46_ICC_PK_CERTIF      0x9F46u   ///< ICC Public Key Certificate                        (@b 9F46)
#define TAG_9F47_ICC_PK_EXP         0x9F47u   ///< ICC Public Key Exponent                           (@b 9F47)
#define TAG_9F48_ICC_PK_REMAINDER   0x9F48u   ///< ICC Public Key Remainder                          (@b 9F48)
#define TAG_9F49_DDOL               0x9F49u   ///< Dynamic Data Authentication Data Object List      (@b 9F49)
#define TAG_9F4A_SDA_TAG_LIST       0x9F4Au   ///< Static Data Authentication Tag List               (@b 9F4A)
#define TAG_9F4B_SDA_DATA           0x9F4Bu   ///< Signed Dynamic Application Data                   (@b 9F4B)
#define TAG_9F4C_ICC_DYNAMIC_NB     0x9F4Cu   ///< ICC Dynamic Number                                (@b 9F4C) @n used as EMV_CTLS_TRANSRES_STRUCT::T_9F4C_ICCDynNumber
#define TAG_9F4E_TAC_MERCHANTLOC    0x9F4Eu   ///< Merchant name and location (VISA VCPS 2.1.1)      (@b 9F4E)
#define TAG_A5_FCI_PROPR_TPLT       0xA5u     ///< FCI Proprietary Template                          (@b A5)
#define TAG_BF0C_FCI_ISS_DISCR      0xBF0Cu   ///< FCI Issuer Discretionary Data                     (@b BF0C)
///@}

/// @defgroup VISA_TAGS Propriertary tags used by Visa
/// @ingroup EMV_TAGS
///@{
#define TAG_9F51_APP_CURRENCY_CODE           0x9F51 ///< Application Currency Code
#define TAG_9F52_DEFAULT_ACTION              0x9F52 ///< Application Default Action (ADA)
#define TAG_9F53_CONSECUTIVE_LIM_TRANS_INT   0x9F53 ///< Consecutive Transaction Limit (International)
#define TAG_9F54_CUMULATIVE_TRANS_LIMET      0x9F54 ///< Cumulative Total Transaction Amount Limit
#define TAG_9F56_ISS_AUTH_INCATOR            0x9F56 ///< Issuer Authentication Indicator
#define TAG_9F5A_APP_PROGRAM_ID              0x9F5A ///< Application Program Identifier
#define TAG_9F5D_VISA_AOSA                   0x9F5D ///< Contactless: Available Offline Spending Amount (AOSA) @n used as @c T_9F5D_CL_VISA_AOSA in ::EMV_CTLS_TRANSRES_STRUCT
#define TAG_9F66_TTQ                         0x9F66 ///< VISA TTQ (Contactless only)
#define TAG_9F6C_VISA_CTQ                    0x9F6C ///< VISA CTQ (Contactless only) @n used as @c T_9F6C_CL_VISA_CTQ in ::EMV_CTLS_TRANSRES_STRUCT
#define TAG_9F6D_TRM_APP_VERSION_NB          0x9F6D ///< Terminal application version number
#define TAG_DF04_PK_CVM_REQUIREMENTS         0xDF04 ///< Visa Asia/Pacific: CVM requirements, see @ref EMV_CTLS_APPLIDATA_PK_STRUCT::CvmRequirements_DF04
#define TAG_DF02_PK_FLOOR_LIMIT              0xDF02 ///< Visa Asia/Pacific: Floor limit, see @ref EMV_CTLS_APPLIDATA_PK_STRUCT::ContactlessFloorLimit_DF02
#define TAG_DF01_PK_CVM_REQ_LIMIT            0xDF01 ///< Visa Asia/Pacific: CVM required limit, see @ref EMV_CTLS_APPLIDATA_PK_STRUCT::ContactlessCVMRequiredLimit_DF01
///@}

/// @defgroup MC_TAGS Proprietary tags used by MasterCard
/// @ingroup EMV_TAGS
///@{
#define TAG_9F53_TRANS_CATEGORY_CODE                     0x9F53    ///< Transaction Category Code
#define TAG_9F5E_DS_ID                                   0x9F5E    ///< Data Storage ID = Application PAN (without any 'F' padding) & Application PAN Sequence Number
#define TAG_9F6B_TRACK2_DATA                             0x9F6B    ///< Track 2 Data contains the data objects of the track 2 according to [ISO/IEC 7813], excluding start sentinel, end sentinel and LRC.
#define TAG_9F6D_TRM_APP_MSR_VERSION_NB                  0x9F6D    ///< Terminal application MSR version number
#define TAG_9F6E_DEVICE_TYPE_INDICATOR                   0x9F6E    ///< Device type indicator
#define TAG_DF8104_BALANCE_BEFORE                        0xDF8104  ///< Balance Read Before Gen AC @n used as @c T_DF8104_CL_MC_BALANCE in ::EMV_CTLS_TRANSRES_STRUCT
#define TAG_DF8105_BALANCE_AFTER                         0xDF8105  ///< Balance Read After Gen AC @n used as @c T_DF8105_CL_MC_BALANCE in ::EMV_CTLS_TRANSRES_STRUCT
#define TAG_DF8106_DE_DATA_NEEDED                        0xDF8106  ///< DataExchange, data needed, used in @ref EMV_CTLS_CALLBACK_FnT, #TAG_BF0E_CBK_DATA_EXCHANGE
#define TAG_DF810C_KERNEL_ID                             0xDF810C  ///< Kernel ID
#define TAG_DF8115_ERROR_INDICATION                      0xDF8115  ///< Error Indication (part of Discretionary Data)
#define TAG_DF8117_CARD_DATA_INPUT_CAPABILITY            0xDF8117  ///< Card data input capability (same as Terminal capability, byte 1)
#define TAG_DF8118_CVM_CAPABILITY__CVM_REQUIRED          0xDF8118  ///< CVM Capability - CVM Required
#define TAG_DF8119_CVM_CAPABILITY__NO_CVM_REQUIRED       0xDF8119  ///< CVM Capability - No CVM Required
#define TAG_DF811B_KERNEL_CONFIGURATION                  0xDF811B  ///< Kernel Configuration
#define TAG_DF811C_TRN_TXN_LIFETIME                      0xDF811C  ///< Torn transaction life time
#define TAG_DF811D_TRN_TXN_NUMBER                        0xDF811D  ///< Torn transaction max. number
#define TAG_DF811E_MSR_CVM_ABOVE_LIMIT                   0xDF811E  ///< Mag-stripe CVM Capability - CVM Required. Indicates the CVM capability of the Terminal/Reader in the case of a mag-stripe mode transaction when the Amount, Authorized (Numeric) is greater than the Reader CVM Required Limit.
#define TAG_DF811F_SECURITY_CAPABILITY                   0xDF811F  ///< Security Capability
#define TAG_DF8120_TAC_DEFAULT                           0xDF8120  ///< TAC Default
#define TAG_DF8121_TAC_DENIAL                            0xDF8121  ///< TAC Denial
#define TAG_DF8122_TAC_ONLINE                            0xDF8122  ///< TAC Online
#define TAG_DF8123_FLOOR_LIMIT                           0xDF8123  ///< Reader Contactless Floor Limit
#define TAG_DF8124_READER_CTLS_TRX_LIMIT__NO_ON_DEV_CVM  0xDF8124  ///< Reader Contactless Transaction Limit (No On-device CVM)
#define TAG_DF8125_READER_CTLS_TRX_LIMIT__ON_DEVICE_CVM  0xDF8125  ///< Reader Contactless Transaction Limit (On-device CVM)
#define TAG_DF8126_CVM_REQUIRED_LIMIT                    0xDF8126  ///< Reader Contactless CVM Required Limit
#define TAG_DF812C_MSR_CVM_BELOW_LIMIT                   0xDF812C  ///< Mag-stripe CVM Capability - No CVM Required. Indicates the CVM capability of the Terminal/Reader in the case of a mag-stripe mode transaction when the Amount, Authorized (Numeric) is less than or equal to the Reader CVM Required Limit.
#define TAG_DF812D_MESSAGE_HOLD_TIME                     0xDF812D  ///< Message Hold Time
#define TAG_DF8130_HOLD_TIME_VALUE                       0xDF8130  ///< Hold Time Value
#define TAG_DF8131_PHONE_MSG_TABLE                       0xDF8131  ///< Phone Message Table
#define TAG_DF8112_TAGS_TO_READ                          0xDF8112  ///< Tags To Read
#define TAG_FF8102_TAGS_TO_WRITE_BEFORE_GEN_AC           0xFF8102  ///< Tags To Write Before Gen AC
#define TAG_FF8103_TAGS_TO_WRITE_AFTER_GEN_AC            0xFF8103  ///< Tags To Write After Gen AC
#define TAG_DF8110_PROCEED_TO_FIRST_WRITE_FLAG           0xDF8110  ///< Proceed To First Write Flag
#define TAG_DF810D_DSVN_TERM                             0xDF810D  ///< DSVN Term
#define TAG_9F5C_DS_REQUESTED_OPERATOR_ID                0x9F5C    ///< DS Requested Operator ID
#define TAG_DF8127_DE_TIMEOUT_VALUE                      0xDF8127  ///< Data Exchange Time Out Value
#define TAG_DF8132_RR_MIN_GRACE_PERIOD                   0xDF8132  ///< Minimum Relay Resistance Grace Period [1/100 ms]
#define TAG_DF8133_RR_MAX_GRACE_PERIOD                   0xDF8133  ///< Maximum Relay Resistance Grace Period [1/100 ms]
#define TAG_DF8134_RR_TERM_EXPECTED_TRANS_TIME_CAPDU     0xDF8134  ///< Terminal Expected Transmission Time For Relay Resistance C-APDU [1/10 ms]
#define TAG_DF8135_RR_TERM_EXPECTED_TRANS_TIME_RAPDU     0xDF8135  ///< Terminal Expected Transmission Time For Relay Resistance R-APDU [1/10 ms]
#define TAG_DF8136_RR_ACCURACY_THRESHOLD                 0xDF8136  ///< Relay Resistance Accuracy Threshold [1/10 ms]
#define TAG_DF8137_RR_TRANS_TIME_MISMATCH_THRESHOLD      0xDF8137  ///< Relay Resistance Transmission Time Mismatch Threshold [%]
#define TAG_9F7C_MERCHANT_CUSTOM_DATA                    0x9F7C    ///< Merchant Custom Data

#define TAG_FF8104_DE_DATA_TO_SEND                       0xFF8104  ///< DataExchange, data to send, used in @ref EMV_CTLS_CALLBACK_FnT, #TAG_BF0E_CBK_DATA_EXCHANGE
#define TAG_FF8105_DATA_RECORD                           0xFF8105  ///< Data Record, The Data Record is a list of TLV encoded data objects returned with the Outcome Parameter Set on the completion of transaction processing.
///@}

/// @defgroup JK_TAGS Proprietary tags used by JCB
/// @ingroup EMV_TAGS
///@{
#define TAG_9F53_TRM_INTERCHANGE_PROFILE                 0x9F53    ///< Terminal Interchange Profile
///@}

/// @defgroup INTERAC_TAGS Proprietary tags used by Interac (Canada)
/// @ingroup EMV_TAGS
///@{
#define TAG_9F58_MERCH_TYPE_INDICATOR  0x9F58
#define TAG_9F59_TERM_TRANS_INFO       0x9F59
#define TAG_9F5A_TERM_TRANS_TYPE       0x9F5A
#define TAG_9F5E_TERM_OPTION_STATUS    0x9F5E
// Interac_1_5Changes: Tag 9F5F, 9F5D tag additions
#define TAG_9F5F_READER_CTLS_FLOOR_LIMIT    0x9F5F
#define TAG_9F5D_TERM_CTLS_RECEIPT_REQUIRED_LIMIT    0x9F5D
///@}

/// @defgroup EXPRESSPAY_TAGS Proprietary tags used by Amex
/// @ingroup EMV_TAGS
///@{
#define TAG_9F6D_AMEX_CAPABILITIES	0xDF9F6D
#define TAG_9F6E_AMEX_ENHANCED_CAPABILITIES	 0xDF9F6E
///@}

/// @defgroup GEMALTO_TAGS Proprietary tags used by Gemalto/Pure
/// @ingroup EMV_TAGS
///@{
#define TAG_9F71_PURE_GDDOL_RESULTS         0x9F71  ///< GDDOL Resulting Buffer. Its content is the result of the concatenation of the data elements listed in GDDOL. See #TAG_BF18_CBK_PURE_GET_PUT_DATA.
#define TAG_9F74_PURE_DATA_UPDATE_RESULT    0x9F74  ///< Data Elements Update Result. Result of the data element update requested by the reader using Memory Slot Update Template. See #TAG_BF18_CBK_PURE_GET_PUT_DATA.
#define TAG_9F76_PURE_TERM_TRX_DATA         0x9F76  ///< Terminal transaction data. Allows the Terminal Application to communicate transaction-related information to the Card Payment application. Depending on card payment application setting, this information may participate in the card payment application risk management or may be inserted inside the card payment application transaction log.
#define TAG_BF70_PURE_PUT_DATA_MSUT         0xBF70  ///< Memory Slot Update Template. This data element provides to the PURE kernel, the list of data elements to update using the PUT DATA. Used in #TAG_BF18_CBK_PURE_GET_PUT_DATA.
#define TAG_BF71_PURE_GET_DATA_MSRT         0xBF71  ///< Memory Slot Read Template. This data element provides to the PURE kernel, the list of data elements to read. Used in #TAG_BF18_CBK_PURE_GET_PUT_DATA.
///@}

// ========================================================================================================
// === Verifone internal tags ===
// ========================================================================================================

/// @defgroup VERI_TAGS Verifone internal tags
/// @ingroup ingroup EMV_TAGS
/// @brief There are many data objects, which do not have an EMVCo defined tags.
///        For usage of TLV interface it's necessary to define internal tags.
///@{
#define TAG_C0_TRM_CL_CVM_LIMIT     0xC0
#define TAG_C1_TRM_CL_CEIL_LIMIT    0xC1
#define TAG_C2_TRM_CL_MODES         0xC2  ///< EMV_CTLS_TERMDATA_STRUCT::CL_Modes_Supported
#define TAG_C3_INDEX                0xC3  ///< EMV_CTLS_VISA_DRL_STRUCT::Index
#define TAG_C4_APP_PRG_ID_LEN       0xC4  ///< EMV_CTLS_VISA_DRL_STRUCT::ucAppPrgIdLen
#define TAG_C5_APP_PRG_ID           0xC5  ///< EMV_CTLS_VISA_DRL_STRUCT::Application_PRG_ID
#define TAG_C6_TXN_LIMIT            0xC6  ///< EMV_CTLS_VISA_DRL_STRUCT::TXNlimit
#define TAG_C7_LED_ID               0xC7  ///< EMV_CTLS_LED(): @c ucLedId
#define TAG_C8_LED_STATE            0xC8  ///< EMV_CTLS_LED(): @c ucLedState resp. #TAG_BF10_CTLS_CBK_LEDS
#define TAG_C9_POLL_TIMEOUT         0xC9  ///< EMV_CTLS_START_STRUCT::ServerPollTimeout
#define TAG_CA_DRL_ON_OFF           0xCA  ///< EMV_CTLS_VISA_DRL_STRUCT::OnOffSwitch
#define TAG_CB_BEEP_SCENARIO        0xCB  ///< Buzzer scenario in callback #TAG_BF19_CTLS_CBK_BEEP
#define TAG_FA_VISA_DRL_RISK        0xFA
#define TAG_FB_PP3_PMSG_TABLE       0xFB
#define TAG_FC_EP_BF0C_FINAL_SELECT 0xFC  ///< BF0C from SELECT response is returned in this tag instead.
#define TAG_FD_AMEX_DRL_RISK        0xFD  ///< EMV_CTLS_APPLIDATA_TYPE::AmexDRLParams
///@}

/// @defgroup VERI_CONSTR_TAGS Constructed tags
/// @ingroup VERI_TAGS
///@{
#define TAG_E2_FORMAT_B             0xE2
#define TAG_E3_FORMAT_N             0xE3
#define TAG_E4_FORMAT_CN            0xE4
#define TAG_E5_FORMAT_A             0xE5
#define TAG_E6_FORMAT_AN            0xE6
#define TAG_E7_FORMAT_ANS           0xE7

#define TAG_F0_EMV_TEMPLATE         0xF0    ///< All BER TLV streams are packed in this tag.

#define TAG_DOM_CHIP                0xFF01  ///< constructed tag for domestic chip applications
#define TAG_FALLBACK_MSR            0xFF02  ///< constructed tag for MSR data
#define TAG_CAND_LIST               0xFF03  ///< constructed tag for candidate list
#define TAG_KEY                     0xFF04  ///< constructed tag for CAP keys
#define TAG_HOTLIST                 0xFF05  ///< constructed tag for hot list in contactless terminal data

#define TAG_FF10_DE_DET_DATA        0xFF10  ///< DataExchange, constructed tag for DET data, used in @ref EMV_CTLS_CALLBACK_FnT, #TAG_BF0E_CBK_DATA_EXCHANGE
///@}

/// @defgroup VERI_CONSTR_TAGS_CARD_LOG Card Transaction Log dump (China union pay)
/// @ingroup VERI_CONSTR_TAGS
///@{
#define TAG_FFDC01_LOG_ENTRY_1     0xFFDC01  ///< 1st Transaction log entry.  Used as input for EMV_CTLS_fetchTxnTags() after a transaction with #EMV_ADK_TRAN_TYPE_READ_CARD_LOG. @n See also @ref VERI_TRX_LOG_SUBFIELDS.
#define TAG_FFDC02_LOG_ENTRY_2     0xFFDC02  ///< 2nd Transaction log entry.  Used as input for EMV_CTLS_fetchTxnTags() after a transaction with #EMV_ADK_TRAN_TYPE_READ_CARD_LOG. @n See also @ref VERI_TRX_LOG_SUBFIELDS.
#define TAG_FFDC03_LOG_ENTRY_3     0xFFDC03  ///< 3rd Transaction log entry.  Used as input for EMV_CTLS_fetchTxnTags() after a transaction with #EMV_ADK_TRAN_TYPE_READ_CARD_LOG. @n See also @ref VERI_TRX_LOG_SUBFIELDS.
#define TAG_FFDC04_LOG_ENTRY_4     0xFFDC04  ///< 4th Transaction log entry.  Used as input for EMV_CTLS_fetchTxnTags() after a transaction with #EMV_ADK_TRAN_TYPE_READ_CARD_LOG. @n See also @ref VERI_TRX_LOG_SUBFIELDS.
#define TAG_FFDC05_LOG_ENTRY_5     0xFFDC05  ///< 5th Transaction log entry.  Used as input for EMV_CTLS_fetchTxnTags() after a transaction with #EMV_ADK_TRAN_TYPE_READ_CARD_LOG. @n See also @ref VERI_TRX_LOG_SUBFIELDS.
#define TAG_FFDC06_LOG_ENTRY_6     0xFFDC06  ///< 6th Transaction log entry.  Used as input for EMV_CTLS_fetchTxnTags() after a transaction with #EMV_ADK_TRAN_TYPE_READ_CARD_LOG. @n See also @ref VERI_TRX_LOG_SUBFIELDS.
#define TAG_FFDC07_LOG_ENTRY_7     0xFFDC07  ///< 7th Transaction log entry.  Used as input for EMV_CTLS_fetchTxnTags() after a transaction with #EMV_ADK_TRAN_TYPE_READ_CARD_LOG. @n See also @ref VERI_TRX_LOG_SUBFIELDS.
#define TAG_FFDC08_LOG_ENTRY_8     0xFFDC08  ///< 8th Transaction log entry.  Used as input for EMV_CTLS_fetchTxnTags() after a transaction with #EMV_ADK_TRAN_TYPE_READ_CARD_LOG. @n See also @ref VERI_TRX_LOG_SUBFIELDS.
#define TAG_FFDC09_LOG_ENTRY_9     0xFFDC09  ///< 9th Transaction log entry.  Used as input for EMV_CTLS_fetchTxnTags() after a transaction with #EMV_ADK_TRAN_TYPE_READ_CARD_LOG. @n See also @ref VERI_TRX_LOG_SUBFIELDS.
#define TAG_FFDC0A_LOG_ENTRY_A     0xFFDC0A  ///< 10th Transaction log entry. Used as input for EMV_CTLS_fetchTxnTags() after a transaction with #EMV_ADK_TRAN_TYPE_READ_CARD_LOG. @n See also @ref VERI_TRX_LOG_SUBFIELDS.
#define TAG_FFDC0B_LOG_ENTRY_B     0xFFDC0B  ///< 11th Transaction log entry. Used as input for EMV_CTLS_fetchTxnTags() after a transaction with #EMV_ADK_TRAN_TYPE_READ_CARD_LOG. @n See also @ref VERI_TRX_LOG_SUBFIELDS.
#define TAG_FFDC0C_LOG_ENTRY_C     0xFFDC0C  ///< 12th Transaction log entry. Used as input for EMV_CTLS_fetchTxnTags() after a transaction with #EMV_ADK_TRAN_TYPE_READ_CARD_LOG. @n See also @ref VERI_TRX_LOG_SUBFIELDS.
#define TAG_FFDC0D_LOG_ENTRY_D     0xFFDC0D  ///< 13th Transaction log entry. Used as input for EMV_CTLS_fetchTxnTags() after a transaction with #EMV_ADK_TRAN_TYPE_READ_CARD_LOG. @n See also @ref VERI_TRX_LOG_SUBFIELDS.
#define TAG_FFDC0E_LOG_ENTRY_E     0xFFDC0E  ///< 14th Transaction log entry. Used as input for EMV_CTLS_fetchTxnTags() after a transaction with #EMV_ADK_TRAN_TYPE_READ_CARD_LOG. @n See also @ref VERI_TRX_LOG_SUBFIELDS.
#define TAG_FFDC0F_LOG_ENTRY_F     0xFFDC0F  ///< 15th Transaction log entry. Used as input for EMV_CTLS_fetchTxnTags() after a transaction with #EMV_ADK_TRAN_TYPE_READ_CARD_LOG. @n See also @ref VERI_TRX_LOG_SUBFIELDS.
///@}


/// @defgroup VERI_PRIM_TAGS Primitive tags
/// @ingroup VERI_TAGS
///@{
#define TAG_ISO_DATA                0xDF01  ///< Tag for ISO data
#define TAG_TRACE                   0xDF02  ///< Tag for trace data
#define TAG_KEY_NUMBER              0xDF03  ///< CAP Key Number of Keys, 1 byte 0...255
#define TAG_DF04_AID                0xDF04  ///< AID in ::EMV_CTLS_APPLI_STRUCT, TRM_SetAppliData(), TRM_GetAppliData(), TRM_SelAppli(), TRM_ReduceCandidateList()
#define TAG_DF05_BUILD_APPLILIST    0xDF05  ///< not applicable for CTLS
#define TAG_DF06_CARDREADER_NUM     0xDF06  ///< L1 reader options @n@c ucOptions in EMV_CTLS_SmartISO()
#define TAG_DF07_UNCRIT_SCR         0xDF07  ///< Script results of uncritical scripts in response to EMV_CTLS_ContinueOnline() @n EMV_CTLS_TRANSRES_STRUCT::ScriptResults
#define TAG_DF08_CRIT_SCR           0xDF08  ///< Script results of uncritical scripts @n@c Future use
#define TAG_KEY_INDEX               0xDF09  ///< CAP Key Index @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_KEY_INDEX, @n Index in EMV_CTLS_CAPKEY_STRUCT::Index, @n XML Tag: #XML_TAG_CAP_KEYS_INDEX
#define TAG_KEY_RID                 0xDF0A  ///< CAP Key RID @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_KEY_RID, @n RID in EMV_CTLS_CAPKEY_STRUCT::RID, @n XML Tag: #XML_TAG_CAP_KEYS_RID
#define TAG_KEY_KEY                 0xDF0B  ///< CAP Key Modulus @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_KEY_KEY, @n Key in EMV_CTLS_CAPKEY_STRUCT::Key, @n XML Tag: #XML_TAG_CAP_KEYS_KEY
#define TAG_KEY_HASH                0xDF0C  ///< CAP Key Hash @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_KEY_HASH, @n Hash in EMV_CTLS_CAPKEY_STRUCT::Hash, @n XML Tag: #XML_TAG_CAP_KEYS_HASH
#define TAG_KEY_EXPONENT            0xDF0D  ///< CAP Key Exponent @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_KEY_EXPONENT, @n Exponent in EMV_CTLS_CAPKEY_STRUCT::Exponent, @n XML Tag: #XML_TAG_CAP_KEYS_EXPONENT
#define TAG_KEY_CRL                 0xDF0E  ///< CAP Key Certification Revocation List  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_KEY_CRL, @n RevocEntries in EMV_CTLS_CAPKEY_STRUCT::RevocEntries, @n XML Tag: #XML_TAG_CAP_KEYS_REVOC_LIST

#define TAG_DF10_MSR_CVM_BELOW      0xDF10
#define TAG_DF11_LIB_VERSION        0xDF11  ///< Library version given back from EMV_CTLS_GetTermData() @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF11_LIB_VERSION, @n FrameworkVersion in EMV_CTLS_TERMDATA_STRUCT::FrameworkVersion, @n XML Tag: #XML_TAG_TERMDATA_FRAMEWORK_VERSION
#define TAG_DF12_CHECKSUM           0xDF12  ///< EMVCo checksum given back from EMV_CTLS_GetTermData() @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF12_CHECKSUM, @n XML Tag: #XML_TAG_APPLIDATA_CHKSUM_ASCII_KERNEL
#define TAG_DF13_TERM_PARAM         0xDF13
#define TAG_DF14_ADD_TAGS_RES       0xDF14  ///< additional tag list requested for result data @n@c EMV_CTLS_PAYMENT_STRUCT::Additional_Result_Tags
#define TAG_DF15_OFFL_ONLY_PROCESS  0xDF15
#define TAG_KEY_KEYLEN              0xDF16  ///< CAP Key key length @n@c Struct, @c XML Reference: @n KeyLen in EMV_CTLS_CAPKEY_STRUCT::KeyLen, @n XML Tag: #XML_TAG_CAP_KEYS_KEYLEN
#define TAG_DF17_FALLBACK_MIDS      0xDF17
#define TAG_DF18_FALLABCK           0xDF18
#define TAG_DF19_PARAMETER_4        0xDF19  ///< universal tag no. 4
#define TAG_DF1A_PARAMETER_5        0xDF1A  ///< universal tag no. 5
#define TAG_DF1B_PARAMETER_6        0xDF1B  ///< universal tag no. 6
#define TAG_DF1C_SPECIAL_TRX        0xDF1C
#define TAG_DF1D_PRIO_APPLI         0xDF1D
#define TAG_DF1E_PARAMETER_7        0xDF1E  ///< universal tag no. 7
#define TAG_DF1F_PARAMETER_8        0xDF1F  ///< universal tag no. 8

#define TAG_DF20_ASI                0xDF20
#define TAG_DF21_TAC_DENIAL         0xDF21
#define TAG_DF21_TAC_ABLEHNUNG      TAG_DF21_TAC_DENIAL  ///< @deprecated use @ref TAG_DF21_TAC_DENIAL instead
#define TAG_DF22_TAC_ONLINE         0xDF22
#define TAG_DF23_TAC_DEFAULT        0xDF23
#define TAG_DF24_THRESHHOLD         0xDF24
#define TAG_DF25_MAXPERCENT_ONL     0xDF25
#define TAG_DF26_PERCENT_ONL        0xDF26
#define TAG_DF27_DEFAULT_TDOL       0xDF27
#define TAG_DF28_DEFAULT_DDOL       0xDF28
#define TAG_DF29_ADD_TAGS           0xDF29
#define TAG_DF2B_APP_FLOW_CAP       0xDF2B
#define TAG_DF2C_ADD_TAGS_CRD       0xDF2C
#define TAG_DF2D_EMV_APPLI          0xDF2D
#define TAG_DF2E_CVM_NOT_SUPP       0xDF2E
#define TAG_DF2F_MSR_CVM_ABOVE      0xDF2F

#define TAG_DF30_RETAP_FIELD_OFF    0xDF30
#define TAG_DF32_MC_BALANCE_8104    0xDF32  ///< see EMV_CTLS_TRANSRES_STRUCT::T_DF8104_CL_MC_BALANCE
#define TAG_DF33_APPLICATION_TAG_50 0xDF33  ///< EMV_CTLS_STARTRES_STRUCT::T_50_ApplicationName
#define TAG_DF36_TRX_OPTIONS        0xDF36  ///< EMV_CTLS_TRANSAC_STRUCT::TxnOptions
#define TAG_DF38_MC_BALANCE_8105    0xDF38  ///< see EMV_CTLS_TRANSRES_STRUCT::T_DF8105_CL_MC_BALANCE
#define TAG_DF3A_FB_MSR_OPTIONS     0xDF3A  ///< EMV_CTLS_APPS_SELECT_STRUCT::xFallback_MS (partially, together with #TAG_DF4C_FALLB_MSR_APPLI)
#define TAG_DF3B_PARAMETER_1        0xDF3B  ///< universal tag no. 1 @n used for @c numberOfAIDs in EMV_CTLS_Init_Framework(), respectively EMV_CTLS_Init_FrameworkClient() @n Also used for @c VirtualTermMapType in EMV_CTLS_MapVirtualTerminal()
#define TAG_DF3C_PARAMETER_2        0xDF3C  ///< universal tag no. 2 @n used for options in EMV_CTLS_Init_Framework(), respectively EMV_CTLS_Init_FrameworkClient() @n Also used for @c TLVSwitchValue in EMV_CTLS_MapVirtualTerminal()
#define TAG_DF3D_PARAMETER_3        0xDF3D  ///< universal tag no. 3 @n Used for @c VirtualTerminal in EMV_CTLS_MapVirtualTerminal()
#define TAG_DF3F_PP_PHONE_MSG       0xDF3F

#define TAG_DF40_FORCE_ONLINE       0xDF40  ///< EMV_CTLS_PAYMENT_STRUCT::Force_Online
#define TAG_DF42_STATUS             0xDF42  ///< EMV_CTLS_TRANSRES_STRUCT::StatusInfo
#define TAG_DF44_CHIP_CVM_BELOW     0xDF44
#define TAG_DF45_CHIP_TXN_LIFETIME  0xDF45
#define TAG_DF46_CHIP_TXN_NO        0xDF46
#define TAG_DF48_ONLINE_SWITCH      0xDF48  ///< EMV_CTLS_PAYMENT_STRUCT::Online_Switch
#define TAG_DF49_CEIL_LIMIT_MOBILE  0xDF49
#define TAG_DF4B_DOM_CHIP_APPLI     0xDF4B  ///< not used in CTLS environment
#define TAG_DF4C_FALLB_MSR_APPLI    0xDF4C  ///< not used in CTLS environment
#define TAG_DF4D_CARD_CONF          0xDF4D  ///< not used in CTLS environment
#define TAG_DF4E_PASSTROUGH         0xDF4E  ///< EMV_CTLS_START_STRUCT::passtroughCardTypes
#define TAG_DF4F_CARDTYPE           0xDF4F  ///< EMV_CTLS_TRANSRES_STRUCT::ctlsCardType

#define TAG_DF50_ONL_RES            0xDF50  ///< EMV_CTLS_HOST_STRUCT::OnlineResult
#define TAG_DF52_AUTH_DATA_CLESS    0xDF52  ///< EMV_CTLS_HOST_STRUCT::AuthData
#define TAG_DF54_SCRIPT_CLESS       0xDF54  ///< EMV_CTLS_HOST_STRUCT::LenScript and EMV_CTLS_HOST_STRUCT::ScriptData
#define TAG_DF57_AC_ADD_OK_CLESS    0xDF57  ///< EMV_CTLS_HOST_STRUCT::AuthResp_Positive
#define TAG_DF5D_CL_MAGSTRIPE_T2    0xDF5D  ///< reserved for CTLS magstripe data, duplicated to DF5E
#define TAG_DF5E_CL_MAGSTRIPE_T2    0xDF5E  ///< contactless magstripe data in ::EMV_CTLS_TRANSRES_STRUCT
#define TAG_DF5F_ADD_APP_VERSION    0xDF5F  ///< see EMV_CT_APPLIDATA_STRUCT::Additional_Versions_No

#define TAG_DF60_VELOCITY_ORIG_IDX  0xDF60  ///< original candidate index in the list of application before it was presented to the Velocity kernel for applications selection (The kernel changes the order of the list according to different "priority" / "partial select" rules, the index keeps the refrence to the original candidate list
#define TAG_DF61_INF_REC_DATA       0xDF61  ///< in EMV_CTLS_STARTRES_STRUCT::T_DF61_Info_Received_Data, in EMV_CTLS_TRANSRES_STRUCT::T_DF61_Info_Received_Data
#define TAG_DF62_BUZZER_VOLUME      0xDF62  ///< Buzzer Volume, EMV_CTLS_TERMDATA_STRUCT::BeepVolume
#define TAG_DF63_VELOCITY_EP_PRB    0xDF63  ///< processing result bitmap from the Entry point kernel
#define TAG_DF64_KERNEL_DEBUG       0xDF64  ///< EMV_CTLS_TRANSRES_STRUCT::T_DF64_KernelDebugData
#define TAG_KERNEL_VERSION          0xDF65  ///< kernel version, see EMV_CTLS_TERMDATA_STRUCT::KernelVersion
#define TAG_HANDLE_APPLI_TYPE       0xDF66  ///< Configuration mode for applications and CAP keys, see @ref APPLI_CONF_MODE @n Used in EMV_CTLS_SetAppliData(), EMV_CTLS_GetAppliData(), EMV_CTLS_StoreCAPKey()
#define TAG_SUPP_LANG               0xDF67  ///< supported languages, see EMV_CTLS_TERMDATA_STRUCT::SuppLang
#define TAG_ATR                     0xDF68  ///< not used in CTLS environment
#define TAG_EXCLUDE_AID             0xDF69  ///< AIDs to exclude from the configuration list for this transaction, EMV_CTLS_APPS_SELECT_STRUCT::ExcludeEmvAIDs
#define TAG_EMV_CONFORM             0xDF6A  ///< not used in CTLS environment
#define TAG_DF6B_TRACK1_EQUIVALENT  0xDF6B  ///< Track 1 Equivalent Data, n byte binary
#define TAG_DF6D_TRY_AGAIN_LIMIT    0xDF6D
#define TAG_DF6E_ADDITIONAL_UI_INFO 0xDF6E  ///< Tag for reading Interac additional UI info from Interac kernel via _EMVADK_fetchTxnTags (mapped to TAG_IK0A)
#define TAG_DF6F_L1DRIVER_VERSION   0xDF6F  ///< L1 driver version given back from EMV_CTLS_GetTermData() @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF6F_L1DRIVER_VERSION, @n L1DriverVersion in EMV_CTLS_TERMDATA_STRUCT::L1DriverVersion, @n XML Tag: #XML_TAG_TERMDATA_L1DRIVER_VERSION

#define TAG_DFD002_APPNAME_CHOSEN   0xDFD002  ///< chosen application name based on the combination 9F12, 9F11, 50, application default name, up to 16 byte ASCII string
#define TAG_DFD003_Kernel_ID        0xDFD003  ///< chosen (Velocity) kernel ID based on the EP processing in combination with probable domestic processing, 3 byte binary according to the EMVCo specified kernel ID

#define TAG_DFD004_TANSPARENT_SEND        0xDFD004 ///< for #EMV_CTLS_TransparentSend
#define TAG_DFD005_TANSPARENT_RECEIVE     0xDFD005 ///< for #EMV_CTLS_TransparentReceive
#define TAG_DFD006_BEEP_FREQ_SUCCESS      0xDFD006 ///< EMV_CTLS_TERMDATA_STRUCT::BeepFrequencySuccess
#define TAG_DFD007_BEEP_FREQ_ALERT        0xDFD007 ///< EMV_CTLS_TERMDATA_STRUCT::BeepFrequencyAlert

///@}


/// @defgroup TLV_CBCK_TLV tags for callback functions
/// @ingroup VERI_PRIM_TAGS
/// @brief Used in function EMV_CallbackFunction()
///@{
#define TAG_DF75_CBK_APP_NO         0xDF75  ///< If length == 1: Return value of terminal selection process == chosen candidate, otherwise reduced candidate list @n index is 1-based (1=first, 2=second, ...) @n also see #TAG_BF12_CBK_MODIFY_CAND
#define TAG_DF76_CBK_APP_POSTPROC   0xDF76  ///< Decide if post processing for the candidates applies (longest match, priority application, ...) or not @n (1 byte, 1=TRUE/0=FALSE) @n not supported for VFI reader @n see #TAG_BF12_CBK_MODIFY_CAND
#define TAG_DF7E_KERNEL_TO_USE      0xDF7E  ///< Set which Kernel should be used
#define TAG_DF7F_CBK_COMM_ERR       0xDF7F  ///< During execution of callback function a communication error occured. @n Indicator to be set by calling application (1 byte TRUE/FALSE value). @n Will result in abort of transaction (#EMV_ADK_INTERNAL)

#define TAG_DF8010_DE_REQUEST       0xDF8010  ///< DataExchange, type of request @ref CBCK_DE_REQUEST, used in @ref EMV_CTLS_CALLBACK_FnT, #TAG_BF0E_CBK_DATA_EXCHANGE
#define TAG_DF8011_DE_STATE         0xDF8011  ///< DataExchange, kernel state @ref CBCK_DE_STATE, used in @ref EMV_CTLS_CALLBACK_FnT, #TAG_BF0E_CBK_DATA_EXCHANGE
#define TAG_DF8012_DE_MODE          0xDF8012  ///< DataExchange, mode @ref CBCK_DE_MODE, used in @ref EMV_CTLS_CALLBACK_FnT, #TAG_BF0E_CBK_DATA_EXCHANGE
#define TAG_FF8013_SIGNAL_MSG       0xFF8013  ///< PayPass 3, tag for reading list of User Interface Request Signals with @ref EMV_CTLS_fetchTxnTags
#define TAG_FF8014_SIGNAL_OUT       0xFF8014  ///< PayPass 3, tag for reading list of Outcome Signals with @ref EMV_CTLS_fetchTxnTags

#define TAG_DF8020_RUPAY_GPO        0xDF8020  ///< RuPay DataExchange service shortlist before GPO, used in @ref EMV_CTLS_CALLBACK_FnT, #TAG_BF16_CBK_SERVICE_SHORTLIST
#define TAG_DF8021_RUPAY_GENAC      0xDF8021  ///< RuPay DataExchange Rupay additional CDOL Parameters, used in @ref EMV_CTLS_CALLBACK_FnT, #TAG_BF17_CBK_SERVICE_FOR_GENAC
///@}


#define TAG_1F70_PASS_TROUGH_CARD_TYPES 0x1F70
#define TAG_1F74_ADDITIONAL_ACTIVATE_OPTIONS_BITMAP 0x1F74

/// @defgroup PRIM_TAGS_3BYTE Three-byte-tags
/// @ingroup VERI_TAGS
///@{
#define TAG_DF8F0A_EXIT_OPTIONS             0xDF8F0A  ///< Options send with Exit_Framework, see @ref EXIT_FW_OPTIONS
#define TAG_DF8F0B_TERM_FLOW_OPTIONS        0xDF8F0B  ///< EMV_CTLS_TERMDATA_STRUCT::FlowOptions
#define TAG_DF8F0C_INIT_RESULT_FLAGS        0xDF8F0C  ///< Parameter @c ulResult in function EMV_CTLS_Init_Framework(), respectively EMV_CTLS_Init_FrameworkClient()
#define TAG_DF8F0D_SETUP_RES_TRXINFO        0xDF8F0D  ///< EMV_CTLS_STARTRES_STRUCT::TxnInformation
#define TAG_DF8F0E_TERM_MAXCTLS_TRANSLIMIT  0xDF8F0E  ///< EMV_CTLS_TERMDATA_STRUCT::MaxCTLSTranslimit
#define TAG_DF8F0F_APPLYCONFIG_OPTIONS  	  0xDF8F0F  ///< Options send with EMV_CTLS_ApplyConfiguration(), unsigned long in functional interface, please use 4 byte in the serialization

#define TAG_DF8F10_LED_DESIGN_WIDTH	      0xDF8F10 ///< LED width, parameter @c width in EMV_CTLS_LED_ConfigDesign()
#define TAG_DF8F11_LED_DESIGN_HEIGHT      0xDF8F11 ///< LED height, parameter @c height in EMV_CTLS_LED_ConfigDesign()
#define TAG_DF8F12_LED_DESIGN_OFF_COL     0xDF8F12 ///< color off LED when off, parameter @c color_off in EMV_CTLS_LED_ConfigDesign()
#define TAG_DF8F13_LED_DESIGN_ON_COL      0xDF8F13 ///< color off LED when on, parameter @c color_on in EMV_CTLS_LED_ConfigDesign()
#define TAG_DF8F14_LED_REGION_WIDTH	      0xDF8F14 ///< Region width, parameter @c wRegion in EMV_CTLS_LED_ConfigDesign()
#define TAG_DF8F15_LED_REGION_HEIGHT      0xDF8F15 ///< Region height, parameter @c hRegion in EMV_CTLS_LED_ConfigDesign()
#define TAG_DF8F16_LED_REGION_XCOORD      0xDF8F16 ///< Region upper left corner x-coordinate, parameter @c xRegion in EMV_CTLS_LED_ConfigDesign()
#define TAG_DF8F17_LED_REGION_YCOORD      0xDF8F17 ///< Region upper left corner y-coordinate, parameter @c yRegion in EMV_CTLS_LED_ConfigDesign()
#define TAG_DF8F18_LED_DESIGN_SHAPE       0xDF8F18 ///< Shape, parameter @c shape in EMV_CTLS_LED_ConfigDesign_Extended()
#define TAG_DF8F19_LED_DESIGN_SHAPE_PARAM 0xDF8F19 ///< Shape parameters, parameter @c shape_params in EMV_CTLS_LED_ConfigDesign_Extended()
#define TAG_DF8F1A_LED_DESIGN_OPTIONS     0xDF8F1A ///< options, parameter @c options in EMV_CTLS_LED_ConfigDesign_Extended()
#define TAG_DF8F1B_LED_DESIGN_BG_COL      0xDF8F1B ///< background color, parameter @c bg_color in EMV_CTLS_LED_ConfigDesign_Extended()


#define TAG_DF8F12_DISPLAY_TEXT          0xDF8F12  ///< used in @ref EMV_CTLS_CALLBACK_FnT, #TAG_BF14_CBK_TEXT_DISPLAY

#define TAG_DF8F20_CTLS_CARD_TPYE        0xDF8F20  ///< detailed CTLS card type, available if a CTLS non EMV card is detected
#define TAG_DF8F21_CTLS_MIFARE_SN        0xDF8F21  ///< Mifare Card Serial number, available if a CTLS non EMV card is detected AND the detected card turns our as Mifare AND the card incldues a serial number

#define TAG_DF8F30_REQUESTED_TAGS         0xDF8F30   ///< for requestedTags parameter in EMV_CTLS_fetchTxnTags()
///@}



/// @defgroup TAGS_NEW_CFG_INTF_PRIM Primitive tags for new scheme specific config interface
/// @ingroup VERI_PRIM_TAGS
/// @brief Used in functions EMV_CTLS_SetAppliDataSchemeSpecific()/EMV_CTLS_GetAppliDataSchemeSpecific
///@{
#define TAG_DFAB01_KERNEL_ID                0xDFAB01
#define TAG_DFAB02_ASI                      0xDFAB02
#define TAG_DFAB03_APP_FLOW_CAP             0xDFAB03
#define TAG_DFAB04_PRIO_APPS                0xDFAB04
#define TAG_DFAB05_SPECIAL_TRX_CFG          0xDFAB05
#define TAG_DFAB06_CHKSUM_EP                0xDFAB06
#define TAG_DFAB07_CHKSUM_KERNEL            0xDFAB07
#define TAG_DFAB08_RETAP_FIELD_OFF          0xDFAB08
#define TAG_DFAB20_ADD_TAGS_TRM             0xDFAB20
#define TAG_DFAB21_ADD_TAGS_CRD             0xDFAB21
#define TAG_DFAB22_DEF_APP_NAME             0xDFAB22

#define TAG_DFAB30_TEC_SUPPORT              0xDFAB30
#define TAG_DFAB31_APP_FLOW_CAP             0xDFAB31

#define TAG_DFAB40_CTLS_FLOOR_LIMIT         0xDFAB40
#define TAG_DFAB41_CTLS_TRX_LIMIT           0xDFAB41
#define TAG_DFAB42_CTLS_CVM_REQ_LIMIT       0xDFAB42
#define TAG_DFAB43_TAC_DEFAULT              0xDFAB43
#define TAG_DFAB44_TAC_DENIAL               0xDFAB44
#define TAG_DFAB45_TAC_ONLINE               0xDFAB45
#define TAG_DFAB46_RISK_MGMT_THRESHOLD      0xDFAB46
#define TAG_DFAB47_RISK_MGMT_TRGT_PRCT      0xDFAB47
#define TAG_DFAB48_RISK_MGMT_MAX_TRGT_PRCT  0xDFAB48
#define TAG_DFAB49_DRL_SWITCHES             0xDFAB49
#define TAG_DFAB4A_CTLS_TRX_LIMIT_CASH      0xDFAB4A
#define TAG_DFAB4B_COMBINATION_OPTIONS      0xDFAB4B
#define TAG_DFAB4C_REMOVAL_TIMEOUT          0xDFAB4C
#define TAG_DFAB4D_RUPAY_CALLBACK_TIMEOUT   0xDFAB4D
#define TAG_DFAB4E_RUPAY_TORN_TRX_INTERVAL  0xDFAB4E
#define TAG_DFAB4F_PURE_CTLS_APP_KERN_CAP   0xDFAB4F
#define TAG_DFAB50_PURE_MTOL                0xDFAB50
#define TAG_DFAB51_CTLS_DEFAULT_DDOL        0xDFAB51
#define TAG_DFAB52_AMEX_UN_RANGE            0xDFAB52
///@}


///@}

/// @defgroup TAGS_NEW_CFG_INTF_CONSTR Constructed tags for new scheme specific config interface
/// @ingroup VERI_CONSTR_TAGS
///
/// Used in functions EMV_CTLS_SetAppliDataSchemeSpecific()/EMV_CTLS_GetAppliDataSchemeSpecific
///@{
#define TAG_FFAB01_DRL_PARAMETER            0xFFAB01
///@}


// range 0xDFD9xx reserved for encryption manager
// range 0xDFDAxx reserved for msr
// range 0xDFDBxx reserved for technology selection

/// @defgroup VERI_TRX_LOG_SUBFIELDS Tags for Transaction Log subfields
/// @ingroup VERI_TAGS
///@{
#define TAG_DFDC01_SFI          0xDFDC01    ///< SFI from Transaction log entry. Used as output by EMV_CTLS_fetchTxnTags() after a transaction with #EMV_ADK_TRAN_TYPE_READ_CARD_LOG. See also @ref VERI_CONSTR_TAGS_CARD_LOG.
#define TAG_DFDC02_RECORD_NB    0xDFDC02    ///< Record no. from Transaction log entry. Used as output by EMV_CTLS_fetchTxnTags() after a transaction with #EMV_ADK_TRAN_TYPE_READ_CARD_LOG. See also @ref VERI_CONSTR_TAGS_CARD_LOG.
#define TAG_DFDC03_READ_STATUS  0xDFDC03    ///< Read status from Transaction log entry. Used as output by EMV_CTLS_fetchTxnTags() after a transaction with #EMV_ADK_TRAN_TYPE_READ_CARD_LOG. See also @ref VERI_CONSTR_TAGS_CARD_LOG.
#define TAG_DFDC04_SW12         0xDFDC04    ///< Status word (SW1/2)from Transaction log entry. Used as output by EMV_CTLS_fetchTxnTags() after a transaction with #EMV_ADK_TRAN_TYPE_READ_CARD_LOG. See also @ref VERI_CONSTR_TAGS_CARD_LOG.
#define TAG_DFDC05_RECORD       0xDFDC05    ///< Transaction log entry. Used as output by EMV_CTLS_fetchTxnTags() after a transaction with #EMV_ADK_TRAN_TYPE_READ_CARD_LOG. See also @ref VERI_CONSTR_TAGS_CARD_LOG.
#define TAG_DFDC06_LOG_FORMAT   0xDFDC06    ///< Format of Transaction log entry. Used as output by EMV_CTLS_fetchTxnTags() after a transaction with #EMV_ADK_TRAN_TYPE_READ_CARD_LOG. See also @ref VERI_CONSTR_TAGS_CARD_LOG.
///@}


/// @defgroup VERI_L1_DUMP_TAGS Primitive tags for L1 dump
/// @ingroup VERI_TAGS
///@{

// tags for L1 Status
#define TAG_DFDD01_L1_ISSUE    				    0xDFDD01
#define TAG_DFDD02_L1_ISSUE_REASON		    0xDFDD02
#define TAG_DFDD03_L1_ISSUE_COMMAND		    0xDFDD03
#define TAG_DFDD04_L1_LAST_COMMAND		    0xDFDD04
#define TAG_DFDD05_L1_LAST_SW12   		    0xDFDD05

// tags for PSE dump
#define NO_PSE_RECS                       10
#define TAG_DFDF01_PSE_1       				    0xDFDF01
#define TAG_DFDF02_PSE_2       				    0xDFDF02
#define TAG_DFDF03_PSE_3       				    0xDFDF03
#define TAG_DFDF04_PSE_4       				    0xDFDF04
#define TAG_DFDF05_PSE_5       				    0xDFDF05
#define TAG_DFDF06_PSE_6       				    0xDFDF06
#define TAG_DFDF07_PSE_7       				    0xDFDF07
#define TAG_DFDF08_PSE_8       				    0xDFDF08
#define TAG_DFDF09_PSE_9       				    0xDFDF09
#define TAG_DFDF10_PSE_10       				  0xDFDF0A

// tags for LOA processing
#define NO_LOA_RECS                       15
#define TAG_DFDF11_LOA_1       				    0xDFDF11
#define TAG_DFDF12_LOA_2       				    0xDFDF12
#define TAG_DFDF13_LOA_3       				    0xDFDF13
#define TAG_DFDF14_LOA_4       				    0xDFDF14
#define TAG_DFDF15_LOA_5       				    0xDFDF15
#define TAG_DFDF16_LOA_6       				    0xDFDF16
#define TAG_DFDF17_LOA_7       				    0xDFDF17
#define TAG_DFDF18_LOA_8       				    0xDFDF18
#define TAG_DFDF19_LOA_9       				    0xDFDF19
#define TAG_DFDF1A_LOA_10       				  0xDFDF1A
#define TAG_DFDF1B_LOA_11       				  0xDFDF1B
#define TAG_DFDF1C_LOA_12       				  0xDFDF1C
#define TAG_DFDF1D_LOA_13       				  0xDFDF1D
#define TAG_DFDF1E_LOA_14       				  0xDFDF1E
#define TAG_DFDF1F_LOA_15       				  0xDFDF1F

// tags for Final Select
#define NO_FS_RECS                        5
#define TAG_DFDF21_FINALS_1    				    0xDFDF21
#define TAG_DFDF22_FINALS_2     				  0xDFDF22
#define TAG_DFDF23_FINALS_3     				  0xDFDF23
#define TAG_DFDF24_FINALS_4     				  0xDFDF24
#define TAG_DFDF25_FINALS_5     				  0xDFDF25

// tags for GPO
#define NO_GETPROC_RECS                   5
#define TAG_DFDF29_GETPROC_1    				  0xDFDF29
#define TAG_DFDF2A_GETPROC_2    				  0xDFDF2A
#define TAG_DFDF2B_GETPROC_3    				  0xDFDF2B
#define TAG_DFDF2C_GETPROC_4    				  0xDFDF2C
#define TAG_DFDF2D_GETPROC_5    				  0xDFDF2D

// tags for ReadRecords
#define NO_RR_RECS                        15
#define TAG_DFDF31_RR_1       				    0xDFDF31
#define TAG_DFDF32_RR_2       				    0xDFDF32
#define TAG_DFDF33_RR_3         				  0xDFDF33
#define TAG_DFDF34_RR_4       		  		  0xDFDF34
#define TAG_DFDF35_RR_5       			  	  0xDFDF35
#define TAG_DFDF36_RR_6       				    0xDFDF36
#define TAG_DFDF37_RR_7       				    0xDFDF37
#define TAG_DFDF38_RR_8         				  0xDFDF38
#define TAG_DFDF39_RR_9       		  		  0xDFDF39
#define TAG_DFDF3A_RR_10       				    0xDFDF3A
#define TAG_DFDF3B_RR_11       				    0xDFDF3B
#define TAG_DFDF3C_RR_12       				    0xDFDF3C
#define TAG_DFDF3D_RR_13       				    0xDFDF3D
#define TAG_DFDF3E_RR_14       				    0xDFDF3E
#define TAG_DFDF3F_RR_15       				    0xDFDF3F

// tags for GetData (PINTry Counter, RiskManagement, ...)
#define NO_GETD_RECS                      8
#define TAG_DFDF41_GETD_1      				    0xDFDF41
#define TAG_DFDF42_GETD_2     			  	  0xDFDF42
#define TAG_DFDF43_GETD_3     				    0xDFDF43
#define TAG_DFDF44_GETD_4     				    0xDFDF44
#define TAG_DFDF45_GETD_5     				    0xDFDF45
#define TAG_DFDF46_GETD_6     	  			  0xDFDF46
#define TAG_DFDF47_GETD_7     		  		  0xDFDF47
#define TAG_DFDF48_GETD_8     			  	  0xDFDF48

// tags for other commands (of course no verify)
#define TAG_DFDF51_INTAUTH     				    0xDFDF51
#define TAG_DFDF52_EXTAUTH    			  	  0xDFDF52
#define TAG_DFDF53_GENAC1     				    0xDFDF53
#define TAG_DFDF54_GENAC2     				    0xDFDF54
#define TAG_DFDF55_CHALL      				    0xDFDF55
#define TAG_DFDF5F_UNKNOW_TAG_LIST  	    0xDFDF5F

// tags for Critical Scripts
#define NO_SCRIPT_RECS                    10
#define TAG_DFDF61_SCRIPT_1    		 		    0xDFDF61
#define TAG_DFDF62_SCRIPT_2     				  0xDFDF62
#define TAG_DFDF63_SCRIPT_3     				  0xDFDF63
#define TAG_DFDF64_SCRIPT_4     				  0xDFDF64
#define TAG_DFDF65_SCRIPT_5     				  0xDFDF65
#define TAG_DFDF69_SCRIPT_6    	 			    0xDFDF69
#define TAG_DFDF6A_SCRIPT_7     				  0xDFDF6A
#define TAG_DFDF6B_SCRIPT_8     				  0xDFDF6B
#define TAG_DFDF6C_SCRIPT_9     				  0xDFDF6C
#define TAG_DFDF6D_SCRIPT_10    				  0xDFDF6D

// tags for unknown commands
#define NO_UNKNOW_RECS                    15
#define TAG_DFDF71_UNKNOW_1   				    0xDFDF71
#define TAG_DFDF72_UNKNOW_2   				    0xDFDF72
#define TAG_DFDF73_UNKNOW_3     				  0xDFDF73
#define TAG_DFDF74_UNKNOW_4   		  		  0xDFDF74
#define TAG_DFDF75_UNKNOW_5   			  	  0xDFDF75
#define TAG_DFDF76_UNKNOW_6   				    0xDFDF76
#define TAG_DFDF77_UNKNOW_7   				    0xDFDF77
#define TAG_DFDF78_UNKNOW_8     				  0xDFDF78
#define TAG_DFDF79_UNKNOW_9   		  	    0xDFDF79
#define TAG_DFDF7A_UNKNOW_10   				    0xDFDF7A
#define TAG_DFDF7B_UNKNOW_11   				    0xDFDF7B
#define TAG_DFDF7C_UNKNOW_12   				    0xDFDF7C
#define TAG_DFDF7D_UNKNOW_13   				    0xDFDF7D
#define TAG_DFDF7E_UNKNOW_14   				    0xDFDF7E
#define TAG_DFDF7F_UNKNOW_15    			    0xDFDF7F
///@}



// ========================================================================================================
// === TLV tags for transport layer ===
// ========================================================================================================

/// @defgroup ADK_TRANSPORT_TAGS Tags for transport layer
/// @ingroup ADK_SERIALIZATION
/// @brief Used for enclosing TLV container at transport.
///        That's necessary in case EMV ADK and calling application are placed in separated entities.
///        Transport layer (e.g. PINPad ISO2) must have indicators to determine whether data is meant for EMV ADK or not.
///@{
#define CLA_EMV        0x39 ///< Class for requests to EMV ADK
#define CLA_EMV_ALT    0x40 ///< Alternative class for requests to EMV ADK
#define CLA_CRD        0x41 ///< Class for ICC functions
#define CLA_CRD_ALT    0x42 ///< Alternative class for ICC functions
#define CLA_LED        0x43 ///< Class for LED functions
#define EMV_CTLS_CLA_CBCK_REQ   0x91 ///< Class for callback requests from EMV ADK to calling application (see @ref CBCK_FCT_TAGS)
#define EMV_CTLS_CLA_CBCK_RESP  0x92 ///< Class for callback responses (see @ref CBCK_FCT_TAGS)

#define EMV_CTLS_CLA_RET_OK     0x90 ///< Class for Return with no error
#define EMV_CTLS_CLA_RET_ERROR  0x94 ///< Class for Return with error (function not available / parameter error, ...)

#define P2_SET         0x00  ///< Uaed for config command, e.g. @ref EMV_CTLS_SetTermData
#define P2_GET         0x01  ///< Uaed for config command, e.g. @ref EMV_CTLS_GetTermData

#define INS_CBCK_FCT        0x01 ///< Instruction byte used for callback functions, used with #EMV_CTLS_CLA_CBCK_REQ
#define INS_CBCK_CRD        0x02 ///< Instruction byte used for callbacks to cardreader, used with #EMV_CTLS_CLA_CBCK_REQ
#define INS_CBCK_TRACE      0xFF ///< Instruction byte used for trace output, used with #EMV_CTLS_CLA_CBCK_REQ
#define INS_INIT            0x00 ///< Instruction byte for @ref EMV_CTLS_Init_Framework, used with #CLA_EMV
#define INS_TERM_CFG        0x01 ///< Instruction byte for @ref EMV_CTLS_SetTermData, @ref EMV_CTLS_GetTermData, used with #CLA_EMV
#define INS_APPLI_CFG       0x02 ///< Instruction byte for @ref EMV_CTLS_SetAppliData, @ref EMV_CTLS_GetAppliData, used with #CLA_EMV
#define INS_CAPKEY_CFG      0x03 ///< Instruction byte for @ref EMV_CTLS_StoreCAPKey, @ref EMV_CTLS_ReadCAPKeys, used with #CLA_EMV
#define INS_APPLY_CFG       0x04 ///< Instruction byte for @ref EMV_CTLS_ApplyConfiguration, used with #CLA_EMV
#define INS_GET_VER         0x05 ///< Instruction byte for @ref EMV_CTLS_FRAMEWORK_GetVersion, used with #CLA_EMV
#define INS_VIRT_CFG        0x06 ///< Instruction byte for @ref EMV_CTLS_MapVirtualTerminal, used with #CLA_EMV
#define INS_SELECT          0x10 ///< Instruction byte for @ref EMV_CTLS_SetupTransaction, used with #CLA_EMV
#define INS_TRANSAC         0x11 ///< Instruction byte for @ref EMV_CTLS_ContinueOffline, used with #CLA_EMV
#define INS_ONLINE          0x12 ///< Instruction byte for @ref EMV_CTLS_ContinueOnline, used with #CLA_EMV
#define INS_RFU             0x13 ///< Instruction byte used in CT mode only
#define INS_FETCH_TAG       0x14 ///< Instruction byte for @ref EMV_CTLS_fetchTxnTags, used with #CLA_EMV
#define INS_END_TRX         0x15 ///< Instruction byte for @ref EMV_CTLS_EndTransaction, used with #CLA_EMV
#define INS_CND_DATA        0x16 ///< Instruction byte for @ref EMV_CTLS_GetCandidateData, used with #CLA_EMV
#define INS_BREAK           0x20 ///< Instruction byte for @ref EMV_CTLS_Break, used wih #CLA_EMV
#define INS_ICC_RESET       0x02 ///< Instruction byte for @ref EMV_CTLS_SmartReset, used with #CLA_CRD
#define INS_ICC_ISO         0x03 ///< Instruction byte for @ref EMV_CTLS_SmartISO, used with #CLA_CRD
#define INS_ICC_OFF         0x04 ///< Instruction byte for @ref EMV_CTLS_SmartPowerOff, used with #CLA_CRD
#define INS_ICC_TRANS       0x07 ///< Instruction byte for @ref EMV_CTLS_TransparentCommand, used with #CLA_CRD
#define INS_ICC_SEND        0x08 ///< Instruction byte for @ref EMV_CTLS_TransparentSend, used with #CLA_CRD
#define INS_ICC_RECEIVE     0x09 ///< Instruction byte for @ref EMV_CTLS_TransparentReceive, used with #CLA_CRD
#define INS_ICC_REMOVAL     0x0A ///< Instruction byte for @ref EMV_CTLS_CardRemoval, used with #CLA_CRD
#define INS_LED_SWITCH      0x03 ///< Instruction byte for @ref EMV_CTLS_LED, used with #CLA_LED
#define INS_LED_MODE        0x04 ///< Instruction byte for @ref EMV_CTLS_LED_SetMode, used with #CLA_LED
#define INS_LED_CONF_DESIGN 0x05 ///< Instruction byte for @ref EMV_CTLS_LED_ConfigDesign, used with #CLA_LED
///@}

#ifdef __cplusplus
}    // extern "C"
  #endif

#endif
