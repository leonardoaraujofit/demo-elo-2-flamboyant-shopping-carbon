/****************************************************************************
*  Product:     InFusion
*  Company:     VeriFone
*  Author:      GSS R&D Germany
*  Content:     Interface definitions and functions
****************************************************************************/


/**
* @file
* @brief Interface of CT-Framework
*
*/

#ifndef _EMV_CT_SER_INTERFACE_H_   /* avoid double interface-includes */
#define _EMV_CT_SER_INTERFACE_H_

#ifdef __cplusplus
extern "C" {
#endif


#ifdef _VRXEVO
  #ifdef EMV_CTF_EXPORT
    #define DLL_CTF __declspec(dllexport)
  #else
    #define DLL_CTF __declspec(dllimport)
  #endif
#else
  #define DLL_CTF __attribute__((visibility ("default")))
#endif


#include "EMV_Common_Interface.h"


/*****************************************************************************
*  EMV_CT_SerInterface
*****************************************************************************/
/// @brief Serial interface to EMV functions
///
/// @author GSS R&D Germany
///
/// @param[in] dataIn input TLV buffer
/// @param[in] dataInLen length of dataIn
/// @param[out] dataOut output TLV buffer
/// @param[in,out] dataOutLen length of dataOut
/// @returns EMV_ADK_OK, EMV_ADK_INTERNAL, EMV_ADK_PARAM
///
DLL_CTF EMV_ADK_INFO EMV_CT_SerInterface(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);


#ifdef __cplusplus
}     // extern "C"
#endif

#endif  // #ifndef _EMV_CT_SER_INTERFACE_H_
