/**
 * \file msr.h
 * Interface definitions for libmsr.
 * This file defines the API for the magnetic card reader library.
 * \author Thomas Buening, GSS
 */

#ifndef __MSR_LIB_INTERFACE__
#define __MSR_LIB_INTERFACE__

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _DOXYGEN_
  #ifdef _VRXEVO
    #ifdef MSR_SERVER
      #define MSR_EXP_DECL
    #else
      #ifdef MSR_EXPORT
        #define MSR_EXP_DECL __declspec(dllexport)
      #else
        #define MSR_EXP_DECL __declspec(dllimport)
      #endif
    #endif
  #else
    #define MSR_EXP_DECL
  #endif
#endif

/** \defgroup ERROR_CODES Error code
 * These error codes are returned by the various libmsr functions.
 * \{ */
#define MSR_OK       0  /**< No error, function executed successfully. */
#define MSR_ERROR   -1  /**< Device error, magnetic card reader not activated, or internal error. */
#define MSR_TIMEOUT -2  /**< Timeout occurred (returned by MSR_GetData()). */
#define MSR_ABORTED -3  /**< Aborted by user (returned by MSR_GetData()). */
#define MSR_ACTIVE  -4  /**< Already activated (returned by MSR_Activate()). */
#define MSR_PARAM   -5  /**< Parameter error (returned by MSR_GetData(), MSR_SetOptions() and MSR_SwitchLeds()). */
#define MSR_PROCESS -6  /**< Process error (returned by MSR_GetData() and MSR_DataAvailable()). */
/** \} */

/** \defgroup STATUS_CODES Status code
 * These values indicate the status of each read track.
 * \{ */
#define MSR_STATUS_NOERR  0  /**< Valid data. */
#define MSR_STATUS_NODATA 1  /**< No data. */
#define MSR_STATUS_NOSTX  2  /**< Missing start sentinel or insufficient data. */
#define MSR_STATUS_NOETX  3  /**< Missing end sentinel or excessive data. */
#define MSR_STATUS_BADLRC 4  /**< Missing LRC or LRC error. */
#define MSR_STATUS_PARITY 5  /**< Parity error. */
#define MSR_STATUS_REVETX 6  /**< Reversed end sentinel found. */
#define MSR_STATUS_BADJIS 7  /**< Insufficient printable characters. */
#define MSR_STATUS_BADTRK 8  /**< Insufficient characters. */
/** \} */

/** \defgroup CARD_CODES Card Type Code
 * These values indicate the type of msr read.
 * \{ */
#define MSR_TYPE_PHYSICAL  0  /**< Normal magstripe swipe. */
#define MSR_TYPE_SAMSUNG   1  /**< LoopPay or Samsung Phone swipe. */
/** \} */

/** \defgroup TRACK_TYPES Track Type Code
 * These values indicate the type of track data.
 * \{ */
#define MSR_CARD_UNKNOWN  0x00  /**< Unknown track data. */
#define MSR_CARD_ISO      0x01  /**< ISO track data. */
#define MSR_CARD_JIS_II   0x02  /**< JIS-II track data. */
/** \} */

/**
 * \defgroup OPTIONS options
 * These options configure the behavior of magnetic card reader library, set by MSR_SetOptions().
 * A combination is possible using | operator.
 * \{ */
#define MSR_UX_ENHANCEMENTS   0x01 /**< options[0] Relevant for UX, V/OS only:
                                        use improved noise filter and ignore card insertion, only read on card removal. */

#define MSR_SAMSUNG_TRACK1    0x02 /**< options[0] Relevant for Samsung LoopPay only:
                                        continuous polling of Samsung LoopPay device to fetch track 1 data, if not set will deliver track 2 as soon as any data is read. */

#define MSR_ONE_SWIPE         0x04 /**< options[0] In opposite to default behavior only read first swiped card after each call of MSR_Activate().
                                        Subsequently swiped cards will not be read and will not overwrite the stored data.
                                        The stored data is deleted as usually upon call of MSR_GetData() or MSR_Deactivate().
                                        To read the next card application has to call MSR_Deactivate() and MSR_Activate(). */

#define MSR_LEDS              0x08 /**< options[0] Light MSR LEDs while card swipe is possible. */

#define MSR_DETECT_ISO        0x01 /**< options[1] Detect ISO cards, this is the default and presumed if options[1] == 0. */

#define MSR_DETECT_JIS_II     0x02 /**< options[1] Detect JIS-II cards. Currently only supported on V/OS2. */

#define MSR_DETECT_ALL        0x03 /**< options[1] Detect both ISO and JIS-II cards. JIS-II cards currently only supported on V/OS2. */
/** \} */

/**
 * \defgroup LED_STATES LED states
 * These are the states of the three MSR LEDs which can be set by MSR_SwitchLeds().
 * On devices that don't have multicolored LEDs, all "color defines" are equivalent and imply "LED on".
 * As there is currently no device with multicolored LEDs, multicolor feature is not implemented yet.
 * \{ */
#define MSR_LED_OFF     0x0000 /**< LED is switched off. */
#define MSR_LED_WHITE   0x0001 /**< LED shines white. */
#define MSR_LED_RED     0x0002 /**< LED shines red. */
#define MSR_LED_GREEN   0x0003 /**< LED shines green. */
#define MSR_LED_BLUE    0x0004 /**< LED shines blue. */
#define MSR_LED_YELLOW  0x0005 /**< LED shines yellow. */
#define MSR_LED_BLINK   0x0100 /**< LED blinks (500ms on, 500ms off), must be combined with one of the "color defines". */
/** \} */


/** Track data of track 1. */
typedef struct
{
  unsigned char status;     /**< \ref STATUS_CODES. */
           char data[80];   /**< Track data including start sentinel, end sentinel and LRC, ASCII null-terminated. */
} MSR_Track_1;

/** Track data of track 2. */
typedef struct
{
  unsigned char status;     /**< \ref STATUS_CODES. */
           char data[41];   /**< Track data including start sentinel, end sentinel and LRC, ASCII null-terminated. */
} MSR_Track_2;

/** Track data of track 3. */
typedef struct
{
  unsigned char status;     /**< \ref STATUS_CODES. */
           char data[108];  /**< Track data, ASCII null-terminated. */
} MSR_Track_3;

/** Track data. */
typedef struct
{
  MSR_Track_1 t1;  /**< Track data of track 1. */
  MSR_Track_2 t2;  /**< Track data of track 2. */
  MSR_Track_3 t3;  /**< Track data of track 3. */
} MSR_TrackData;

/** Track data including card type. */
typedef struct
{
  unsigned char card_type; /**< \ref CARD_CODES. */
  MSR_Track_1   t1;        /**< Track data of track 1. */
  MSR_Track_2   t2;        /**< Track data of track 2. */
  MSR_Track_3   t3;        /**< Track data of track 3. */
} MSR_TrackData2;

/** Track data including additional information. */
typedef struct
{
  unsigned char add_info[8]; /**< additional information, add_info[0]: \ref CARD_CODES, add_info[1]: \ref TRACK_TYPES, add_info[2..7]: RFU. */
  MSR_Track_1   t1;          /**< Track data of track 1. */
  MSR_Track_2   t2;          /**< Track data of track 2. */
  MSR_Track_3   t3;          /**< Track data of track 3. */
} MSR_TrackData3;

/** Decoded data of track 1. */
typedef struct
{
  unsigned char valid;            /**< 1: data valid, 0: data invalid. */
           char pan[20];          /**< PAN, ASCII null-terminated. */
           char name[27];         /**< Cardholder name, ASCII null-terminated. */
           char exp_date[5];      /**< Expiry date, YYMM, ASCII null terminated. */
           char service_code[4];  /**< Service code, ASCII null terminated. */
           char disc_data[72];    /**< Discretionary data, ASCII null terminated. */
} MSR_Decoded_Track_1;

/** Decoded data of track 2. */
typedef struct
{
  unsigned char valid;            /**< 1: data valid, 0: data invalid. */
           char pan[20];          /**< PAN, ASCII null-terminated. */
           char exp_date[5];      /**< Expiry date, YYMM, ASCII null terminated. */
           char service_code[4];  /**< Service code, ASCII null terminated. */
           char disc_data[35];    /**< Discretionary data, ASCII null terminated. */
} MSR_Decoded_Track_2;

/** Decoded track data. */
typedef struct
{
  MSR_Decoded_Track_1 t1;  /**< Decoded data of track 1. */
  MSR_Decoded_Track_2 t2;  /**< Decoded data of track 2. */
} MSR_DecodedData;


/**
 * Type of function that is called after magnetic card has been swiped, see MSR_Activate().
 * \param[in] data : Data pointer provided by the application.
 */
typedef void (* MSR_Callback) (void *data);

/**
 * Type of function that is called for traces, see MSR_SetTraceCallback();
 * \param[in] str : Trace message.
 * \param[in] data : Data pointer provided by the application.
 */
typedef void (* MSR_TraceCallback) (const char *str, void *data);

/**
 * Get version of libmsr.
 * \param[out] version : Buffer to store null-terminated version string.
 * \param[in] len : Size of buffer version.
 */
MSR_EXP_DECL void MSR_Version(char *version, unsigned char len);

/**
 * Set callback function for trace output.
 * \param[in] cbf : Callback function for trace messages, may be NULL.
 * \param[in] cb_data : Data pointer that is passed on to the callback function cbf, may be NULL.
 */
MSR_EXP_DECL void MSR_SetTraceCallback(MSR_TraceCallback cbf, void *cb_data);

/**
 * Set options.
 * This function has to be called while MSR is deactivated.
 * \param[in] options : data pointer, see \ref OPTIONS
 * \param[in] options_len : length of options
 * \return
 * - \ref MSR_OK : No error
 * - \ref MSR_PARAM : options == NULL or options_len == 0
 * - \ref MSR_ERROR : MSR is activated, call MSR_Deactivate() first.
 */
MSR_EXP_DECL int MSR_SetOptions(unsigned char *options, unsigned char options_len);

/**
 * Activate magnetic card reader interface.
 * After successful execution of this function every swiped card is read and its data is stored internally.
 * Upon storing card data any previously stored data is erased, so only the data of the last swiped card remains available.
 * To obtain the stored data, call MSR_GetData(). After doing so, the stored data is erased as well, so that card data can only be obtained once.
 * \param[in] cbf : Callback function that is called every time a card has been swiped, may be NULL. Within the callback function card data can be obtained by MSR_GetData().
 * \param[in] cb_data : Data pointer that is passed on to the callback function cbf, may be NULL.
 * \return
 * - \ref MSR_OK : Successfully activated.
 * - \ref MSR_ERROR : No reader present, device could not be opened, or internal error.
 * - \ref MSR_ACTIVE : Interface is already activated, nothing done.
 */
MSR_EXP_DECL int MSR_Activate(MSR_Callback cbf, void *cb_data);

/**
 * Deactivate magnetic card reader interface.
 * After deactivating the card reader interface no more swiped cards are read and no data is stored. Any previously stored card data will be erased.
 * \return
 * - \ref MSR_OK : Successfully deactivated or not activated before.
 * - \ref MSR_ERROR : Internal error.
 */
MSR_EXP_DECL int MSR_Deactivate(void);

/**
 * Check if magnetic card has been swiped and its data is stored. After obtaining the stored data with MSR_GetData()
 * or deactivating the card reader interface with MSR_Deactivate() the stored data is erased and not available any longer.
 * \return
 * - 1 : Data available.
 * - 0 : No data available.
 * - \ref MSR_ERROR : Magnetic card reader interface not activated.
 * - \ref MSR_PROCESS : MSR_ONE_SWIPE was set and card data has already been obtained by MSR_GetData(). Reactivation needed before next card swipe.
 */
MSR_EXP_DECL int MSR_DataAvailable(void);

/**
 * Obtain magnetic card data.
 * With this function the internally stored card data can be obtained. In doing so, the internally stored data is erased, so that data of each card swipe can only be obtained once.
 * If no card data is stored at the moment, the function waits for a specified time for a card swipe.
 * If a timeout is supplied, the function is blocking and can be aborted by MSR_AbortGetData().
 * If a callback function is supplied to MSR_Activate(), setting a timeout != 0 is not allowed.
 * MSR_GetData() can be called from within a callback function supplied to MSR_Activate() but there's no need to do so.
 * \param[in] timeout_msec : Time (in milliseconds, < 0 : infinite) to wait for a card swipe if no card data has been stored previously.
 * \param[out] tracks : Track data, may be NULL.
 * \param[out] data : Decoded track data, may be NULL.
 * \return
 * - \ref MSR_OK : Magnetic card data is available of became available within the supplied timeout period.
 * - \ref MSR_ERROR : Magnetic card reader interface not activated or internal error.
 * - \ref MSR_TIMEOUT : No magnetic card data available within the specified timeout.
 * - \ref MSR_ABORTED : Aborted by MSR_AbortGetData().
 * - \ref MSR_PARAM : Callback function supplied to MSR_Activate() and timeout != 0.
 * - \ref MSR_PROCESS : MSR_ONE_SWIPE was set and card data has already been obtained. Reactivation needed before next card swipe.
 */
MSR_EXP_DECL int MSR_GetData(int timeout_msec, MSR_TrackData *tracks, MSR_DecodedData *data);

/**
 * Obtain magnetic card data including card type.
 * This is the same as MSR_GetData() but additionally offers \ref CARD_CODES.
 * With this function the internally stored card data can be obtained. In doing so, the internally stored data is erased, so that data of each card swipe can only be obtained once.
 * If no card data is stored at the moment, the function waits for a specified time for a card swipe.
 * If a timeout is supplied, the function is blocking and can be aborted by MSR_AbortGetData().
 * If a callback function is supplied to MSR_Activate(), setting a timeout != 0 is not allowed.
 * MSR_GetData2() can be called from within a callback function supplied to MSR_Activate() but there's no need to do so.
 * \param[in] timeout_msec : Time (in milliseconds, < 0 : infinite) to wait for a card swipe if no card data has been stored previously.
 * \param[out] tracks : Track data, may be NULL.
 * \param[out] data : Decoded track data, may be NULL.
 * \return
 * - \ref MSR_OK : Magnetic card data is available of became available within the supplied timeout period.
 * - \ref MSR_ERROR : Magnetic card reader interface not activated or internal error.
 * - \ref MSR_TIMEOUT : No magnetic card data available within the specified timeout.
 * - \ref MSR_ABORTED : Aborted by MSR_AbortGetData().
 * - \ref MSR_PARAM : Callback function supplied to MSR_Activate() and timeout != 0.
 * - \ref MSR_PROCESS : MSR_ONE_SWIPE was set and card data has already been obtained. Reactivation needed before next card swipe.
 */
MSR_EXP_DECL int MSR_GetData2(int timeout_msec, MSR_TrackData2 *tracks, MSR_DecodedData *data);

/**
 * Obtain magnetic card data including additional information.
 * This is the same as MSR_GetData() but additionally offers \ref CARD_CODES and \ref TRACK_TYPES.
 * With this function the internally stored card data can be obtained. In doing so, the internally stored data is erased, so that data of each card swipe can only be obtained once.
 * If no card data is stored at the moment, the function waits for a specified time for a card swipe.
 * If a timeout is supplied, the function is blocking and can be aborted by MSR_AbortGetData().
 * If a callback function is supplied to MSR_Activate(), setting a timeout != 0 is not allowed.
 * MSR_GetData2() can be called from within a callback function supplied to MSR_Activate() but there's no need to do so.
 * \param[in] timeout_msec : Time (in milliseconds, < 0 : infinite) to wait for a card swipe if no card data has been stored previously.
 * \param[out] tracks : Track data, may be NULL.
 * \param[out] data : Decoded track data, may be NULL.
 * \return
 * - \ref MSR_OK : Magnetic card data is available of became available within the supplied timeout period.
 * - \ref MSR_ERROR : Magnetic card reader interface not activated or internal error.
 * - \ref MSR_TIMEOUT : No magnetic card data available within the specified timeout.
 * - \ref MSR_ABORTED : Aborted by MSR_AbortGetData().
 * - \ref MSR_PARAM : Callback function supplied to MSR_Activate() and timeout != 0.
 * - \ref MSR_PROCESS : MSR_ONE_SWIPE was set and card data has already been obtained. Reactivation needed before next card swipe.
 */
MSR_EXP_DECL int MSR_GetData3(int timeout_msec, MSR_TrackData3 *tracks, MSR_DecodedData *data);

/**
 * Abort blocking function MSR_GetData().
 * If MSR_GetData() is waiting for a card swipe, the blocking function can be aborted by invoking this function from another thread.
 */
MSR_EXP_DECL void MSR_AbortGetData(void);

/**
 * Set state of MSR LEDs.
 * On Verix devices switching LEDs is only possible while magnetic card reader interface is activated, see MSR_Activate().
 * \param[in] led1 : state of LED 1 (top), see \ref LED_STATES.
 * \param[in] led2 : state of LED 2 (mid), see \ref LED_STATES, not supported on Verix devices.
 * \param[in] led3 : state of LED 3 (bottom), see \ref LED_STATES, not supported on Verix devices.
 * \param[in] duration : if > 0: all LEDs are switched off after this time (in seconds).
 * \return
 * - \ref MSR_OK    : Successfully set LED state.
 * - \ref MSR_PARAM : Invalid parameter.
 * - \ref MSR_ERROR : Internal error.
 */
MSR_EXP_DECL int MSR_SwitchLeds(int led1, int led2, int led3, int duration);


#ifdef __cplusplus
}
#endif

#endif
