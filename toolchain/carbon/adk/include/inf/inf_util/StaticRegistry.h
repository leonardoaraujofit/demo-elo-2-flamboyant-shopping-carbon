/**
 * @ingroup inf_util_backend_api
 * @file
 * @brief Declaration of template class StaticRegistry
 */

#ifndef STATICREGISTRY_H_
#define STATICREGISTRY_H_

#include <assert.h>
#include <cstddef>

/**
 * @brief Statically (at compile-time) generated registry of objects.
 * @tparam T the type of objects stored in the registry
 * @details
 * This facility is here to ease conditional inclusion of program features based on a preprocessor symbol.
 * @ingroup inf_util_backend_api
 */
template<class T>
class StaticRegistry
{
public:

    /**
     * @brief Helper for static registration of objects
     *
     * This class implements both automatic registration (done in constructor) and a singly linked list node
     * used to populate the list of statically available objects. This is how it works:
     * -# use macro REGISTER_STATIC_OBJECT(ClassName) in a .cpp file, wrapped with conditional compilation (\#if.../\#endif);
     * -# macro defines a static instance of ClassName;
     * -# macro defines a static instance of StaticRegistry<ClassName>::Registrar and passes pointer to static instance of ClassName to its constructor;
     * -# constructor saves the pointer and enters @c this (the static instance of StaticRegistry<ClassName>::Registrar) as a next node in the static
     *      singly linked list of Registrar's;
     * -# when StaticRegistry<ClassName>::get_objects() is called later on in the program, it traverses the singly linked list and copies
     *      StaticRegistry<ClassName>::Registrar#m_obj pointers to the supplied container.
     */
    class Registrar
    {
    public:
        typedef T Object;
        typedef StaticRegistry<Object> Registry;
        friend class StaticRegistry<Object> ;

        /**
         * @brief Constructor which enters provided pointer into the static list
         * @param[in] obj pointer to static object to register
         */
        Registrar(Object* obj) :
            m_obj(obj), m_next(NULL)
        {
            Registry::register_object(this);
        }

    private:
        Object* m_obj;    ///< Pointer to the object being registered
        Registrar* m_next;    ///< Pointer to the next object in the singly linked list
    };

public:
    friend class Registrar;

    /**
     * @brief Get list of statically available objects
     * @tparam Index class of container. Must provide methods @c clear() and @c push_back()
     * @param[out] index container to fill
     *
     * This method fills the supplied container with pointers to static objects of type T
     * that were registered with this registry. This registration is static,
     * i.e. it is prepared at compile time and executed at static initialization time, which is right
     * before main() is entered.
     *
     * This function traverses the internal singly linked list formed by static Registrar instances.
     * @see Registrar, REGISTER_STATIC_OBJECT()
     */
    template<class Index>
    static void get_objects(Index& index)
    {
        index.clear();
        for (Registrar* registrar = registry(); registrar != NULL; registrar = registrar->m_next)
        {
            assert(registrar->m_obj != NULL);
            if (registrar->m_obj)
                index.push_back(registrar->m_obj);
        }
    }

private:
    /**
     * @brief Enter a registrar into the registry at static init time
     * @param registrar address of a registrar object to link into the registry list
     */
    static void register_object(Registrar* registrar)
    {
        Registrar** pp = &registry();
        while (*pp)
            pp = &(*pp)->m_next;

        *pp = registrar;
        std::size_t& num = num_objects();
        num++;
    }

    /**
     * @brief Accessor for static registry list head pointer
     * @return reference to list head pointer
     * @details
     * This method is needed in order to overcome static initialization order errors.
     */
    static Registrar*& registry()
    {
        // head of singly-linked list of statically registered objects
        static Registrar* registry = NULL;
        return registry;
    }

    /**
     * @brief Accessor for static counter of registered objects
     * @return reference to counter
     * @details
     * This method is needed in order to overcome static initialization order errors.
     */
    static std::size_t& num_objects()
    {
        static std::size_t num_objects = 0;
        return num_objects;
    }
};

/**
 * @brief Define static instance of given class
 * @param __class class of the object to make statically available
 * @param __reg_class class of the objects in static registry. Can be the same as @a __class, or its superclass.
 *      @a __class* should be convertible to @a __reg_class*.
 *
 * @warning This macro should be used in a .cpp file, not in .h file.
 *
 * This macro defines a function that initializes and returns static instance of class @a __class. Usage of function
 * helps prevent the Static Initialization Order Fiasco problem.
 * @note class @a __class must have a public default constructor
 * @ingroup inf_util_backend_api
 */
#define DEFINE_STATIC_FACTORY( __class, __reg_class )\
__reg_class* get_##__class##_inst()\
{\
    static __class inst;\
    return static_cast<__reg_class*>(&inst);\
}

/**
 * @brief Register static instance of given class with the static registry
 * @param __class class of the object to make statically available
 * @param __reg_class class of the objects in static registry. Can be the same as @a __class, or its superclass.
 *      @a __class* should be convertible to @a __reg_class*.
 *
 * @warning This macro should be used in a .cpp file, not in .h file.
 *
 * This macro defines an instance of class StaticRegistry<ClassName>::Registrar, which enters the pointer to the @a __class
 * instance into the static registry of type StaticRegistry<__reg_class> in its constructor. Use DEFINE_STATIC_FACTORY()
 * to actually define static instance of @a __class and to make it available for static registration.
 * @ingroup inf_util_backend_api
 */
#define DEFINE_STATIC_REGISTRAR( __class, __reg_class )\
StaticRegistry<__reg_class>::Registrar s_##__class##_registrar(get_##__class##_inst())

/**
 * @brief Define static instance and register it in one shot
 * @param __class class of the object to make statically available
 * @param __reg_class class of the objects in static registry. Can be the same as @a __class, or its superclass.
 *      @a __class* should be convertible to @a __reg_class*.
 *
 * @warning This macro should be used in a .cpp file, not in .h file.
 *
 * This macro both defines the instance of @a __class and registers it in static registry in one shot.
 * @ingroup inf_util_backend_api
 */
#define REGISTER_STATIC_OBJECT( __class, __reg_class )\
DEFINE_STATIC_FACTORY( __class, __reg_class );\
DEFINE_STATIC_REGISTRAR( __class, __reg_class )

/**
 * @brief Register static object defined in another translation unit
 * @param __class class of the object to make statically available
 * @param __reg_class class of the objects in static registry. Can be the same as @a __class, or its superclass.
 *      @a __class* should be convertible to @a __reg_class*.
 *
 * @warning This macro should be used in a .cpp file, not in .h file.
 *
 * This macro allows to register static objects that are defined in another .cpp file or even separate static library.
 * You do not need to include declaration of @a __class to use this macro.
 * @ingroup inf_util_backend_api
 */
#define REGISTER_EXTERNAL_STATIC_OBJECT( __class, __reg_class )\
extern __reg_class* get_##__class##_inst();\
DEFINE_STATIC_REGISTRAR( __class, __reg_class)

#endif /* STATICREGISTRY_H_ */
