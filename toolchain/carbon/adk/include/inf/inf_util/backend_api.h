/**
 * @ingroup inf_util
 * @defgroup inf_util_backend_api Internal backend API
 * @brief Used by developers to implement new backends
 */

/**
 * @ingroup inf_util_backend_api
 * @file
 * @brief Top-level header for internal API for backends
 */

#ifndef BACKEND_API_H_
#define BACKEND_API_H_

#include <string>

#include "../inf_util.h"
#include "types.h"
#include "StaticRegistry.h"

struct Error;
struct Schemata;
class RecordProducer;
class RecordConsumer;
class XMLHandler;

/**
 * @brief A collection of functions and methods passed to backends
 * @ingroup inf_util_backend_api
 */
struct BackendAPI
{
    /**
     * @brief Invoke XML parser
     * @param[in] handler the XMLHandler object to use for XML processing
     * @param[in] fileName name of XML file to parse
     * @param[in] useNameSpaces whether parser should translate XML namespaces
     * @param[in] defNSPrefix namespace prefix to use whenever it is not specified explicitly in XML
     * @param[out] error structure for collecting extended error information
     * @return one of #ErrorCode values, #INFO_E_OK on success.
     * @details
     * This function creates and intializes XML parser and starts parsing in one shot.
     * @ingroup inf_util_backend_api
     */
    ErrorCode (*parse_xml)(XMLHandler& handler, const std::string& fileName, bool useNameSpaces,
                           const std::string& defNSPrefix, Error& error);

};

/**
 * @brief Interface for backend factory
 * @details
 * Every backend provides an instance of a factory class that implements this interface. The factory instance should be
 * allocated statically (i.e., should not need to be deleted). If a backend allows static linking, it should use macro
 * #DEFINE_STATIC_BACKEND_FACTORY() to implement mechanism for registration of static backends.
 * @ingroup inf_util_backend_api
 */
class BackendInit
{
public:
    /**
     * @brief Get the name of the backend
     * @return name of the backend
     * @details
     * This name is the same as used in file and database schema definition XML files
     */
    virtual std::string name() const = 0;

    /**
     * @brief Create object that can read data records from data source
     * @param[in] imex current operation parameters, import or export, full or partial
     * @param[in] api backend API
     * @param[in] schemata reference to parsed schema collection
     * @param[in] srcName name of the data source (e.g. file)
     * @param[out] producer pointer that receives the address of created reader object on success
     * @param[in,out] error reference to Error structure to capture error information
     * @return one of #ErrorCode values, #INFO_E_OK on success
     */
    virtual ErrorCode createReader(ImExPar &imex, const BackendAPI* api, const Schemata& schemata, const std::string& srcName,
                                   RecordProducer*& producer, Error& error) = 0;

    /**
     * @brief Create object that can write data records to data store
     * @param[in] imex current operation parameters, import or export, full or partial
     * @param[in] api backend API
     * @param[in] schemata reference to parsed schema collection
     * @param[in] destName name of the data store (e.g. file)
     * @param[out] consumer pointer that receives the address of created writer object on success
     * @param[in,out] error reference to Error structure to capture error information
     * @return one of #ErrorCode values, #INFO_E_OK on success
     */
    virtual ErrorCode createWriter(ImExPar &imex, const BackendAPI* api, const Schemata& schemata, const std::string& destName,
                                   RecordConsumer*& consumer, Error& error) = 0;
};

/**
 * @ingroup inf_util_backend_api
 */
typedef StaticRegistry<BackendInit> StaticBackendRegistry;

/**
 * @brief Macro for adding static backend to the static backend registry
 * @param __class name of backend's factory class (the one that implements interface BackendInit)
 * @details
 * This macro should be used in the code that *uses* the backend. Typically it is invoked from a .cpp file
 * in the program that links the backends and InfoSvc libraries statically. Without this macro, there would be
 * no reference to backend code and therefore it would be simply removed by the linker.
 * @ingroup inf_util_backend_api
 */
#define REGISTER_STATIC_BACKEND( __class )\
REGISTER_EXTERNAL_STATIC_OBJECT( __class, BackendInit)

/**
 * @brief Macro for making backend available for static linkage
 * @param __class name of backend's factory class (the one that implements interface BackendInit)
 * @details
 * This macro should be used in one of the .cpp files of the backend implementation. It defines a function
 * that will be called from the code generated from the expansion of macro #REGISTER_STATIC_BACKEND().
 * @ingroup inf_util_backend_api
 */
#define DEFINE_STATIC_BACKEND_FACTORY( __class )\
DEFINE_STATIC_FACTORY( __class, BackendInit)

#endif /* BACKEND_API_H_ */
