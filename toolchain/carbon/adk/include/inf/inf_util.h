/**
 * @defgroup inf_util Import/Export Utility
 * @brief Allows configurable data translation from a file into a database and vice versa.
 */

/**
 * @ingroup inf_util
 * @defgroup inf_util_public Public API
 * @brief API for the users of the library
 */

/**
 * @file
 * @ingroup inf_util_public
 * @brief Import/Export Utility API
 */

#ifndef INF_UTIL_H_
#define INF_UTIL_H_

#ifndef INF_UTIL_API
#ifdef _VRXEVO
#   ifdef _INF_UTIL_API_EXPORT
#       define INF_UTIL_API __declspec(dllexport)
#   else
#       define INF_UTIL_API __declspec(dllimport)
#   endif
#else
#   define INF_UTIL_API
#endif // #ifdef _VRXEVO
#endif // #ifndef INF_UTIL_API

#include "inf_util/types.h"
/**
 * @brief Error codes for the API functions
 * @ingroup inf_util_public
 */
typedef enum
{
    INFO_E_OK = 0,    ///< No errors

    // File errors
    INFO_E_BAD_DB_SCHEMA_FILE = -1,    ///< DB schema file either not found or malformed
    INFO_E_BAD_DATA_SCHEMA_FILE = -2,    ///< Data schema file either not found or malformed
    INFO_E_BAD_DB = -3,    ///< DB cannot be opened
    INFO_E_BAD_DATA_FILE = -4,    ///< Data file is improperly formatted or corrupt
    INFO_E_FILE_IO = -5,    ///< File I/O operation failed

    // Database errors
    INFO_E_DB_FAILURE = -10,    ///< Internal DB failure (e.g. cannot open/close db file)
    INFO_E_DB_INSERT = -11,    ///< DB insert operation failed
    INFO_E_DB_READ = -12,    ///< DB read (select?) operation failed
    INFO_E_DB_TRANSACTION = -13,    ///< DB transaction start or finish failed
    INFO_E_DB_DELETE = -14, ///< DB delete operation failed

    // Parser errors
    INFO_E_PARSE_ERROR = -100,    ///< Error occured in parser

    // Data processing
    INFO_E_NO_MORE = -200,    ///< No more data
    INFO_E_UNKNOWN = -201,    ///< An object is not in the schema

    // Misc
    INFO_E_NOT_IMPL = -999,    ///< Not implemented
    INFO_E_INTERNAL_ERROR = -1000,    ///< Some internal error
    INFO_E_PARAM = -1001, ///< Invalid arguments to function
    INFO_E_BACKEND = -1002, ///< Backend is unknown
    INFO_E_BACKEND_ROLE = -1003, ///< Backend is used in the wrong role
} ErrorCode;


/**
 * @brief Operation sub type
 * @ingroup inf_util_public
 */
typedef enum
{
    INFO_OP_FULL,   ///< delete destination prior operation
    INFO_OP_PARTIAL,///< update destination
    INFO_OP_MERGE   ///< insert data which not exist in destination
}OperationSubType;
/**
 * @brief Structure to tune up import /export operations
 * @ingroup inf_util_public
 */
typedef struct
{
    Operation           op;    ///<Top-level operation codes
    OperationSubType    subop; ///<Operation sub type
} ImExPar;

/**
 * @brief Structure to capture error information
 * @details This structure can be used by the caller of Info Svc to receive extended error information in the case of error
 * @ingroup inf_util_public
 */
typedef struct
{
    ErrorCode errorCode;    ///< final error code
    char* shortMessage;    ///< short message that can be shown to the end user
    char* location;    ///< location of the faulty object. Can be file name, file/line, or database table.
    char* extendedMessage;    ///< message that provides details of the failure
} ErrorInfo;

/** \brief get version of a library
  * \return string with version
*/
INF_UTIL_API const char *infoutil_getVersion(void);

/**
 * @brief Deallocate resources referenced by a ErrorInfo structure
 * @param[in] error address of the structure
 * @details
 * This function releases all dynamically allocated strings behind the @a error structure. Note that it does not deallocate the structure itself.
 * If @a error is @c NULL, then this function is a no-op.
 * @ingroup inf_util_public
 */
INF_UTIL_API void error_info_free(ErrorInfo* error);

/**
 * @brief Import data from datafile to database
 * @param[in] dataSchemaFile name of XML file containing schema of the data file
 * @param[in] dbSchemaFile name of XML file containing schema of the database
 * @param[in] dbName name of a database to which data will be imported
 * @param[in] inputFileName name of data file to be loaded
 * @param[out] error pointer to struct ErrorInfo. Note, the initial contents of this structure will be overwritten!
 * @return one of #ErrorCode values, #INFO_E_OK on success. The same error code will be duplicated in @a error->errorCode, if @a error is not @c NULL.
 * @note target tables are deleted before importing data from datafile
 * @ingroup inf_util_public
 */
INF_UTIL_API ErrorCode info_import(const char *dataSchemaFile, const char *dbSchemaFile, const char *dbName,
                                  const char *inputFileName, ErrorInfo* error);

/**
* @brief Import data from datafile to database. Data is inserted or updated, depends from destination DB.
* @param[in] dataSchemaFile name of XML file containing schema of the data file
* @param[in] dbSchemaFile name of XML file containing schema of the database
* @param[in] dbName name of a database to which data will be imported
* @param[in] inputFileName name of data file with new/updated records
* @param[out] error pointer to struct ErrorInfo. Note, the initial contents of this structure will be overwritten!
* @return one of #ErrorCode values, #INFO_E_OK on success. The same error code will be duplicated in @a error->errorCode, if @a error is not @c NULL.
* @ingroup inf_util_public
*/
INF_UTIL_API ErrorCode  info_partial_import(const char *dataSchemaFile, const char *dbSchemaFile, const char *dbName,
                                  const char *inputFileName, ErrorInfo* error);
/**
 * @brief Export data from database to datafile
 * @param[in] dataSchemaFile name of XML file containing schema of the data file
 * @param[in] dbSchemaFile name of XML file containing schema of the database
 * @param[in] dbName name of a database that contains the data to be exported
 * @param[in] outputFileName name of data file to be created
 * @param[out] error pointer to struct ErrorInfo. Note, the initial contents of this structure will be overwritten!
 * @return one of #ErrorCode values, #INFO_E_OK on success. The same error code will be duplicated in @a error->errorCode, if @a error is not @c NULL.
 * @ingroup inf_util_public
 */
INF_UTIL_API ErrorCode info_export(const char *dataSchemaFile, const char *dbSchemaFile, const char *dbName,
                                  const char *outputFileName, ErrorInfo* error);


/**
 * @brief Import data from datafile to property database
 * @param[in] dbName name of a database to which data will be imported
 * @param[in] inputFileName name of data file to be loaded
 * @param[out] error pointer to struct ErrorInfo. Note, the initial contents of this structure will be overwritten!
 * @return one of #ErrorCode values, #INFO_E_OK on success. The same error code will be duplicated in @a error->errorCode, if @a error is not @c NULL.
 * @note target tables are deleted before importing data from datafile
 * @ingroup inf_util_public
 */
INF_UTIL_API ErrorCode info_import_property_db(const char *dbName, const char *inputFileName, ErrorInfo* error);

/**
 * @brief Import data from datafile to property database. Data is inserted or updated, depends from destination DB.
 * @param[in] dbName name of a database to which data will be imported
 * @param[in] inputFileName name of data file to be loaded
 * @param[out] error pointer to struct ErrorInfo. Note, the initial contents of this structure will be overwritten!
 * @return one of #ErrorCode values, #INFO_E_OK on success. The same error code will be duplicated in @a error->errorCode, if @a error is not @c NULL.
 * @ingroup inf_util_public
 */
INF_UTIL_API ErrorCode info_partial_import_property_db(const char *dbName, const char *inputFileName, ErrorInfo* error);


/**
 * @brief Merge data from source XML file to destination DB. Data is inserted if not existed in destination DB.
 * @param[in] dbName name of a database to which data will be imported
 * @param[in] inputFileName name of data file to be loaded
 * @param[out] error pointer to struct ErrorInfo. Note, the initial contents of this structure will be overwritten!
 * @return one of #ErrorCode values, #INFO_E_OK on success. The same error code will be duplicated in @a error->errorCode, if @a error is not @c NULL.
 * @ingroup inf_util_public
 */
INF_UTIL_API ErrorCode info_merge_import_property_db(const char *dbName, const char *inputFileName, ErrorInfo* error);


/**
 * @brief Export data from database to datafile
 * @param[in] dbName name of a database that contains the data to be exported
 * @param[in] outputFileName name of data file to be created
 * @param[out] error pointer to struct ErrorInfo. Note, the initial contents of this structure will be overwritten!
 * @return one of #ErrorCode values, #INFO_E_OK on success. The same error code will be duplicated in @a error->errorCode, if @a error is not @c NULL.
 * @ingroup inf_util_public
 */
INF_UTIL_API ErrorCode info_export_property_db(const char *dbName, const char *outputFileName, ErrorInfo* error);

#endif /* INF_UTIL_H_ */
