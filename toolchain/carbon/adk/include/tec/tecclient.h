/**
 * \file tecclient.h
 * Interface definitions for libtecclient.
 * This file defines the API for the technology selection client library.
 * \author Thomas Buening, GSS
 */

#ifndef __TEC_LIB_CLIENT_INTERFACE__
#define __TEC_LIB_CLIENT_INTERFACE__

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _DOXYGEN_
  #ifdef _VRXEVO
    #ifdef TEC_EXPORT
      #define TEC_EXP_DECL __declspec(dllexport)
    #else
      #define TEC_EXP_DECL __declspec(dllimport)
    #endif
  #else
    #define TEC_EXP_DECL
  #endif
#endif

/**
 * \defgroup TECHNOLOGIES technology code
 * These codes are used to specify supported technologies.
 * A combination is possible using | operator.
 * \{ */
#define CTS_CHIP 1
#define CTS_MSR  2
#define CTS_CTLS 4
/** \} */

#define CTS_DATA_TLV 0x80 /**< if set in usedTechnology parameter of cts_WaitSelection(), dataBuffer is in TLV format (this is only the case if NFC or VAS are used) */

/**
 * \defgroup RETURN_CODES return code
 * These return codes are returned by the various libtec functions.
 * \{ */
#define CTS_OK                0  /**< No error, function executed successfully. */
#define CTS_NO_CHIP           1  /**< Card without chip or with broken chip is inserted or card is inserted and \ref CTS_NO_POWERON is set (returned by cts_WaitSelection()). */
#define CTS_IN_PROGRESS       2  /**< Operation in progress (returned by cts_StartSelection(), cts_WaitSelection(), cts_WaitCardRemoval()). */
#define CTS_TIMEOUT           3  /**< Timeout occurred (returned by cts_WaitSelection(), cts_WaitCardRemoval2()). */
#define CTS_PARAM             4  /**< Parameter error (returned by cts_SetOptions(), cts_WaitSelection(), cts_WaitCardRemoval()). */
#define CTS_NOT_STARTED       5  /**< Technology selection is not running (returned by cts_WaitSelection(), cts_StopSelection(), cts_RemoveTechnologies()). */
#define CTS_STOPPED           6  /**< Technology selection was aborted by cts_StopSelection() (returned by cts_WaitSelection()). */
#define CTS_CTLS_INIT         7  /**< Contactless transaction was not set up correctly (returned by cts_WaitSelection()). */
#define CTS_ERROR             8  /**< Internal error. */
#define CTS_CTLS_NOT_ALLOWED  9  /**< VFI-Reader has not yet finished previous transaction (returned by cts_WaitSelection()). */
#define CTS_CTLS_EMV_NO_CARD 10  /**< ADK-EMV has detected no card after ADK-NFC said there is one, only relevant if \ref CTS_EMV_AFTER_NFC_ISO was set (returned by cts_WaitSelection()). */
#define CTS_ERR_LOAD         11  /**< Error loading dynamic library (NFC_Client.so/NFC_Client.vsl). */

#define CTS_FORMAT         -101  /**< Malformed message. */
#define CTS_TLV            -102  /**< Error parsing TLV input buffer. */
#define CTS_PARAMETER      -103  /**< Missing parameter.  */
#define CTS_UNKNOWN_CLA    -104  /**< Unknown class. */
#define CTS_UNKNOWN_INS    -105  /**< Unknown instruction.  */
#define CTS_BLOCKED        -106  /**< TEC service is blocked by another client/function call. */

#define CTS_NO_SERVER      -201  /**< No server configured. */
#define CTS_IPC            -202  /**< IPC error (e.g. connection to server lost). */
/** \} */

/**
 * \defgroup OPTIONS options
 * These options configure the behavior of technology selection, set by cts_StartSelection().
 * A combination is possible using | operator.
 * \{ */
#define CTS_PURE_CARD_DETECTION         0x01 /**< options[0] Relevant for CTLS only: only detect card, do not perform transaction */
#define CTS_NO_POWERON                  0x02 /**< options[0] Relevant for CT only: do not power on inserted card */

#define CTS_NFC_ENABLE                  0x01 /**< options[1] Relevant for CTLS only (must not be set in conjunction with \ref CTS_VAS_ENABLE): ADK-NFC is used for detection of contactless cards, tech_bitmap is filled with options[12..15]. */
#define CTS_VAS_ENABLE                  0x02 /**< options[1] Relevant for CTLS only (must not be set in conjunction with \ref CTS_NFC_ENABLE): ADK-NFC is used for Wallet processing. */
#define CTS_EMV_AFTER_NFC_ISO           0x04 /**< options[1] Relevant only if \ref CTS_NFC_ENABLE is set. Perform an EMV transaction if NFC has detected one of the following cards:
                                                             I_ISO14443A, I_ISO14443B */
#define CTS_VAS_HANDLE_LED_BUZZ         0x08 /**< options[1] Relevant only if \ref CTS_VAS_ENABLE is set: Handle LEDs and buzzer if VAS processing failed or VAS only (no subsequent EMV transaction). */
/** \} */

/**
 * \defgroup DATA_TAGS data tags
 * These tags are used in dataBuffer of cts_WaitSelection() if \ref CTS_DATA_TLV is set in usedTechnology
 * \{ */
#define CTS_DATA_TAG_NFC_RESULT         0xDFDB20 /**< return code of NFC_PT_Polling(). */
#define CTS_DATA_TAG_CARD               0xFFDB20 /**< card detected by NFC_PT_Polling(), may occur several times. */
#define CTS_DATA_TAG_CARD_TYPE          0xDFDB21 /**< card type, included in \ref CTS_DATA_TAG_CARD. */
#define CTS_DATA_TAG_CARD_INFO          0xDFDB22 /**< card info, included in \ref CTS_DATA_TAG_CARD. */
#define CTS_DATA_TAG_VAS_RESULT         0xDFDB23 /**< return code of NFC_VAS_Activate(). */
#define CTS_DATA_TAG_VAS_DATA           0xDFDB24 /**< output data of NFC_VAS_Activate(). */
#define CTS_DATA_TAG_EMV_RESULT         0xDFDB25 /**< return code of EMV_CTLS_ContinueOffline() / EMV_CTLS_SmartReset(). */
/** \} */

/**
 * \defgroup OPTION_TAGS option tags
 * These options can be set by cts_SetOptions().
 * \{ */
#define CTS_OPTION_TAG_APP_ID           0xDFDB40 /**< application ID, passed on to NFC_VAS_Activate(). */
/** \} */

/** Server configuration. */
typedef struct {
  const char *hostname;     /**< host name of server, NULL or empty string means localhost */
  unsigned short port;      /**< port to connect to, 0 means default port (5825) */
} cts_ServerConfig;


/**
 *  Type of function that is called after technology selection has been finished (see cts_StartSelection()) or removed (see cts_WaitCardRemoval()).
 * \param[in] server_idx : index of server on which technology selection has been finished.
 * \param[in] data : Data pointer provided by the application.
 */
typedef void (* cts_Callback) (unsigned char server_idx, void *data);

/**
 * Type of function that is called for traces, see cts_SetTraceCallback();
 * \param[in] str : Trace message.
 * \param[in] data : Data pointer provided by the application.
 */
typedef void (* cts_TraceCallback) (const char *str, void *data);

/**
 * Configure servers.
 * Set servers and connect to them.
 * \param[in] server_cnt number of servers. Set to 0 to disconnect from all servers.
 * \param[in] server_cfg list of server configurations
 * \return
 * - \ref CTS_OK : Successfully connected to all servers
 * - \ref CTS_IPC : Could not connect to one or more servers
 * - \ref CTS_ERROR : Internal error
 */
TEC_EXP_DECL int cts_ConfigureServer(unsigned char server_cnt, const cts_ServerConfig *server_cfg);

/**
 * Get versions of libtecclient and all connected servers.
 * \param[out] version : Buffer to store null-terminated version string.
 * \param[in] len : Size of buffer version.
 */
TEC_EXP_DECL void cts_Version(char *version, unsigned char len);

/**
 * Set callback function for trace output.
 * \param[in] cbf : Callback function for trace messages, may be NULL.
 * \param[in] cb_data : Data pointer that is passed on to the callback function cbf, may be NULL.
 */
TEC_EXP_DECL void cts_SetTraceCallback(cts_TraceCallback cbf, void *cb_data);

/**
 * Set additional options on specified server.
 * This function must not be called while technology selection is running.
 * \param[in] server_idx  : index of server in server configuration (see cts_ConfigureServer()).
 * \param[in] options     : data pointer, TLV format, see \ref OPTION_TAGS
 * \param[in] options_len : length of options
 * \return
 * - \ref CTS_OK : No error
 * - \ref CTS_PARAM : options == NULL or options_len == 0 or TLV error
 * - \ref CTS_ERROR : technology selection is currently running.
 */
TEC_EXP_DECL int cts_SetOptions(unsigned char server_idx, const unsigned char *options, unsigned char options_len);

/**
 * This function starts an asynchronous card reader monitoring on specified server. The monitoring ends if
 * - magstripe card is swiped or inserted
 * - or chip card is inserted
 * - or contactless card is tapped
 * - or the timeout occurs
 * - or cts_StopSelection() is called
 * - or error occurred.
 *
 * \param[in] server_idx            : index of server in server configuration (see cts_ConfigureServer()).
 * \param[in] supportedTechnologies : supported technologies: combination of \ref TECHNOLOGIES, any additional bits are reserved for future use and are currently ignored.
 *                                    Supplying none of \ref TECHNOLOGIES is allowed. In this case cts_WaitSelection() will never return \ref CTS_OK or \ref CTS_NO_CHIP of course.
 * \param[in] timeout_sec           : timeout in seconds to wait for card insertion/swipe/tap; if this timeout expires while a timeout defined in options[6..7] or options[8..9] is running, latter timeouts have higher priority, they are not aborted.
 * \param[in] cbf                   : callback function that is called when technology selection has been finished, that means a (positive or negative) result != \ref CTS_IN_PROGRESS can be obtained with cts_WaitSelection(), may be NULL
 * \param[in] cb_data               : data pointer that is passed on to the callback function, may be NULL.
 * \param[in] options               : data pointer, options[0..1] : see \ref OPTIONS,
 *                                                  options[2..3] : msrserver port, tecserver connects msrserver on this port, 2 byte binary, big-endian, 0 = use default port,
 *                                                  options[4] is passed to CT ICC functions,
 *                                                  options[5] is passed to CTLS ICC functions,
 *                                                  options[6..7] : time in ms to wait for MSR after CTLS has been detected, 2 byte binary, big-endian,
 *                                                  options[8..9] : time in ms to wait for MSR after card without chip or with broken chip is inserted, relevant for UX only, 2 byte binary, big-endian,
 *                                                  options[10..11] : reserved,
 *                                                  options[12..15] : NFC technologies, this parameter is passed to NFC poll function, 4 byte binary, big-endian, only relevant if \ref CTS_NFC_ENABLE is set,
 *                                    may be NULL (default = {0})
 * \param[in] options_len           : length of options
 * \return
 * - \ref CTS_OK : Success
 * - \ref CTS_IN_PROGRESS : Monitoring is already active. Call cts_WaitSelection() until it returns != \ref CTS_IN_PROGRESS first.
 * - \ref CTS_PARAM : both \ref CTS_NFC_ENABLE and \ref CTS_VAS_ENABLE are set
 * - \ref CTS_ERR_LOAD : \ref CTS_NFC_ENABLE or \ref CTS_VAS_ENABLE are set but NFC client library could not be loaded.
 * - \ref CTS_ERROR : Failure
 * - \ref CTS_FORMAT , \ref CTS_TLV , \ref CTS_PARAMETER , \ref CTS_UNKNOWN_CLA , \ref CTS_UNKNOWN_INS , \ref CTS_BLOCKED , \ref CTS_NO_SERVER , \ref CTS_IPC
 */
TEC_EXP_DECL int cts_StartSelection(unsigned char server_idx,
                                    unsigned char supportedTechnologies, unsigned short timeout_sec,
                                    cts_Callback cbf, void *cb_data,
                                    unsigned char *options, unsigned char options_len);

/**
 * This function stops a technology selection started via cts_StartSelection().
 * It will be called by application if waiting for a card is canceled by user or ECR break.
 * Keep in mind that the technology selection may not be stopped immediately.
 * Call cts_WaitSelection() to wait for termination of technology selection.
 * After cts_WaitSelection() returns != \ref CTS_IN_PROGRESS, it is safe to call cts_StartSelection() again.
 * \param[in] server_idx : index of server in server configuration (see cts_ConfigureServer()).
 * \return
 * - \ref CTS_OK : Stopping technology selection is successfully requested
 * - \ref CTS_NOT_STARTED : Technology selection is not running
 * - \ref CTS_FORMAT , \ref CTS_TLV , \ref CTS_PARAMETER , \ref CTS_UNKNOWN_CLA , \ref CTS_UNKNOWN_INS , \ref CTS_BLOCKED , \ref CTS_NO_SERVER , \ref CTS_IPC
 */
TEC_EXP_DECL int cts_StopSelection(unsigned char server_idx);

/**
 * This function waits for technology selection to finish.
 * \param[in]     server_idx       : index of server in server configuration (see cts_ConfigureServer()).
 * \param[out]    usedTechnology   : technology that has been selected, only set if \ref CTS_OK is returned.
 *                                   If \ref CTS_DATA_TLV is set dataBuffer is in TLV format (this is the case if NFC or VAS was detected).
 *                                   In certain circumstances ('MSR after CTLS timeout' is set; UX MSR enhancements not enabled) it is possible that usedTechnology contains more than one technology at once, see documentation.
 * \param[out]    dataBuffer       : reference to buffer for output data.
 *                                   a) \ref CTS_DATA_TLV is set in usedTechnology:
 *                                     contains tags, see \ref DATA_TAGS.
 *                                   b) \ref CTS_DATA_TLV is not set in usedTechnology:
 *                                     If *usedTechnology & \ref CTS_CHIP : contains ATR.
 *                                     If *usedTechnology & \ref CTS_CTLS and \ref CTS_PURE_CARD_DETECTION was set as option to cts_StartSelection() : contains card info delivered by EMV_CTLS_SmartReset().
 *                                     If *usedTechnology & \ref CTS_CTLS and \ref CTS_PURE_CARD_DETECTION was not set : contains return value of EMV_CTLS_ContinueOffline().
 * \param[in,out] dataBufferLength : buffer size for output data, return data length;
 *                                   if the size of dataBuffer is too small to hold the whole output data,
 *                                   no special error code is returned, the return code is as usual, but the output buffer will be empty, dataBufferLength is set to 0.
 * \param[in]     timeout_msec     : timeout in milliseconds to wait for card insertion or swipe.
 *                                   If a callback function is supplied to cts_StartSelection(), setting a timeout != 0 is not allowed.
 * \return
 * - \ref CTS_OK               : Successful completion, card was detected. Not possible if \ref CTS_NO_POWERON is set.
 * - \ref CTS_NO_CHIP          : Card without chip or with broken chip is inserted or card is inserted and \ref CTS_NO_POWERON is set.
 * - \ref CTS_IN_PROGRESS      : Technology selection not completed. Another call of cts_StartSelection() will return \ref CTS_IN_PROGRESS in this case.
 * - \ref CTS_TIMEOUT          : Timeout occurred, no card detected.
 * - \ref CTS_PARAM            : - usedTechnology is NULL pointer; - callback function supplied to cts_StartSelection() and timeout != 0.
 * - \ref CTS_NOT_STARTED      : cts_StartSelection() was not called previously.
 * - \ref CTS_STOPPED          : Technology selection was aborted by cts_StopSelection().
 * - \ref CTS_CTLS_INIT        : Contactless transaction was not set up correctly.
 * - \ref CTS_ERROR            : Internal error occurred.
 * - \ref CTS_CTLS_NOT_ALLOWED : VFI-Reader has not yet finished previous transaction, e.g. still waiting for ContinueOnline.
 * - \ref CTS_CTLS_EMV_NO_CARD : ADK-EMV has detected no card after ADK-NFC said there is one, only relevant if \ref CTS_EMV_AFTER_NFC_ISO was set.
 * - \ref CTS_FORMAT , \ref CTS_TLV , \ref CTS_PARAMETER , \ref CTS_UNKNOWN_CLA , \ref CTS_UNKNOWN_INS , \ref CTS_BLOCKED , \ref CTS_NO_SERVER , \ref CTS_IPC
 */
TEC_EXP_DECL int cts_WaitSelection(unsigned char server_idx,
                                   unsigned char *usedTechnology, unsigned char *dataBuffer,
                                   unsigned short *dataBufferLength, unsigned short timeout_msec);

/**
 * This function removes technologies from currently running technology selection.
 * This can be useful to remove contact and magstripe technologies after the application was informed by EMV-ADK that a ctls retap scenario is running.
 * \param[in] server_idx : index of server in server configuration (see cts_ConfigureServer()).
 * \param[in] technologies : technologies to remove from running technology selection: combination of \ref TECHNOLOGIES, any additional bits are reserved for future use and are currently ignored.
 *                           Supplying none of \ref TECHNOLOGIES is allowed. In this case this function does actually nothing.
 * \return
 * - \ref CTS_OK : Removing technologies is successfully requested.
 * - \ref CTS_NOT_STARTED : Technology selection is not running.
 * - \ref CTS_FORMAT , \ref CTS_TLV , \ref CTS_PARAMETER , \ref CTS_UNKNOWN_CLA , \ref CTS_UNKNOWN_INS , \ref CTS_BLOCKED , \ref CTS_NO_SERVER , \ref CTS_IPC
 */
TEC_EXP_DECL int cts_RemoveTechnologies(unsigned char server_idx, unsigned char technologies);

/**
 * This function registers callback for card removal.
 * The function returns immediately with one of the return values stated below.
 * The callback is invoked as soon as the inserted card is removed or immediately if no card is inserted.
 * Keep in mind that the callback is only invoked once. If you want to be informed about the next card removal as well, call this function again, even from within the callback function.
 * Attention: Do not call this function as long as other TEC/EMV functions are running and do not call other TEC/EMV functions until the callback has been invoked!
 * \param[in] server_idx : index of server in server configuration (see cts_ConfigureServer()).
 * \param[in] cbf : callback function that is called when a card has been removed, must not be NULL.
 * \param[in] cb_data : data pointer that is passed on to the callback function.
 * \return
 * - \ref CTS_OK : Success.
 * - \ref CTS_IN_PROGRESS : Waiting for card removal is already active.
 * - \ref CTS_PARAM : Missing parameter.
 * - \ref CTS_ERROR : Internal error.
 * - \ref CTS_FORMAT , \ref CTS_TLV , \ref CTS_PARAMETER , \ref CTS_UNKNOWN_CLA , \ref CTS_UNKNOWN_INS , \ref CTS_BLOCKED , \ref CTS_NO_SERVER , \ref CTS_IPC
 */
TEC_EXP_DECL int cts_WaitCardRemoval(unsigned char server_idx, cts_Callback cbf, void *cb_data);

/**
 * This function waits for card removal.
 * The function does not return until card has been removed or timeout has occurred.
 * Attention: Do not call this function as long as other TEC/EMV functions are running and do not call other TEC/EMV functions until the function has returned!
 * \param[in] server_idx : index of server in server configuration (see cts_ConfigureServer()).
 * \param[in] timeout_sec : timeout in seconds to wait for card removal.
 * \return
 * - \ref CTS_OK : Card has been removed.
 * - \ref CTS_TIMEOUT : Timeout occurred.
 * - \ref CTS_ERROR : Internal error.
 * - \ref CTS_FORMAT , \ref CTS_TLV , \ref CTS_PARAMETER , \ref CTS_UNKNOWN_CLA , \ref CTS_UNKNOWN_INS , \ref CTS_BLOCKED , \ref CTS_NO_SERVER , \ref CTS_IPC
 */
TEC_EXP_DECL int cts_WaitCardRemoval2(unsigned char server_idx, unsigned short timeout_sec);

/**
 * Function to test round trip performance
 * \param[in] server_idx : index of server in server configuration (see cts_ConfigureServer()).
 * \param[in] byteCount : number of bytes send for ping-pong, for instance up to 2048 byte supported
 * \return
 * - \ref CTS_OK : ping ping successful
 * - \ref CTS_PARAM : parameter byte count too high
 * - \ref CTS_FORMAT , \ref CTS_TLV , \ref CTS_PARAMETER , \ref CTS_UNKNOWN_CLA , \ref CTS_UNKNOWN_INS , \ref CTS_BLOCKED , \ref CTS_NO_SERVER , \ref CTS_IPC
 */
TEC_EXP_DECL int cts_Ping(unsigned char server_idx, unsigned short byteCount);

#ifdef __cplusplus
}
#endif

#endif
