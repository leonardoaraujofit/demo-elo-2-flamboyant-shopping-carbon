/**
 * \file pintransfer.h
 * Interface definitions for libtecclient.
 * This file defines the API for inter-VAULT PIN transfer.
 */

#ifndef __PINTRANSFER_CLIENT_INTERFACE__
#define __PINTRANSFER_CLIENT_INTERFACE__

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _DOXYGEN_
  #ifdef _VRXEVO
    #ifdef TEC_EXPORT
      #define PINTRANSFER_EXP_DECL __declspec(dllexport)
    #else
      #define PINTRANSFER_EXP_DECL __declspec(dllimport)
    #endif
  #else
    #define PINTRANSFER_EXP_DECL
  #endif
#endif

/**
 * Check the link status and if applicable do the Needham-Schroeder process between two terminals
 * \param[in] from_server_idx : index of server (terminal 1) in server configuration (see cts_ConfigureServer()).
 * \param[in] to_server_idx : index of server (terminal 2) in server configuration (see cts_ConfigureServer()).
 * \return
 * - \ref CTS_OK : Link status ok
 * - \ref CTS_IPC : Could not connect to one or more servers
 * - \ref CTS_ERROR : Error link creation
 */
PINTRANSFER_EXP_DECL int pintransfer_Pairing(unsigned char from_server_idx, unsigned char to_server_idx);

/**
 * Transfer an entered PIN from one terminal to another one
 * \param[in] from_server_idx : index of server in server configuration (see cts_ConfigureServer()).
 * \param[in] to_server_idx : index of server in server configuration (see cts_ConfigureServer()).
 * \return
 * - \ref CTS_OK : Successfully moved PIN
 * - \ref CTS_IPC : Could not connect to one or more servers
 * - \ref CTS_ERROR : Error during PIN export or import
 */
PINTRANSFER_EXP_DECL int pintransfer_MovePin(unsigned char from_server_idx, unsigned char to_server_idx);

#ifdef __cplusplus
}
#endif

#endif // __PINTRANSFER_CLIENT_INTERFACE__
