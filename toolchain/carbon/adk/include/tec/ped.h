/**
 * \file ped.h
 * This file defines the API for the PP1000 pairing and PIN transfer functionality which is part of technology selection library.
 * \author Thomas Buening, GSS
 */

#ifndef __TEC_LIB_PED_INTERFACE__
#define __TEC_LIB_PED_INTERFACE__

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _DOXYGEN_
  #ifdef _VRXEVO
    #ifdef TEC_EXPORT
      #define PED_EXP_DECL __declspec(dllexport)
    #else
      #define PED_EXP_DECL __declspec(dllimport)
    #endif
  #else
    #define PED_EXP_DECL
  #endif
#endif


/**
 * \defgroup RETURN_CODES return code
 * These return codes are returned by the ped functions.
 * \{ */
#define PED_OK                0  /**< No error, function executed successfully */
#define PED_ERROR            -1  /**< Error */
#define PED_ERR_LOAD         -2  /**< Error loading dynamic library (pp1000.so/pp1000.vsl) */
/** \} */

/**
 * \defgroup STATUS_CODES status codes
 * These codes are set by ped_Pairing().
 * \{ */
#define PED_LINK_OK           0  /**< Set by ped_Pairing(): Link verification successful, no pairing needed. */
#define PED_NS_DONE           1  /**< Set by ped_Pairing(): Link verification failed, pairing (Needham-Schroeder) successful. */
#define PED_CERT_ERR          2  /**< Set by ped_Pairing(): Error during certificate exchange. */
#define PED_NS_ERR            3  /**< Set by ped_Pairing(): Error during pairing (Needham-Schroeder). */
/** \} */


/**
 *  Callback function application has to implement for sending data to PED.
 * \param[in] data : Data to be sent
 * \param[in] len : Length of data
 * \return number of bytes sent, -1: failure
 */
typedef int (* ped_SendCallback) (const unsigned char *data, int len);

/**
 * Callback function application has to implement for receiving data from PED.
 * If the received data is not a complete packet, the callback will be invoked again until the whole packet is received.
 * \param[out] data : received data
 * \param[in] len : size of data buffer
 * \return number of bytes read, -1: failure
 */
typedef int (* ped_RcvCallback) (unsigned char *data, int len);


/**
 * Set send and receive callback functions for communication with PED.
 * This function is for convenience. You can choose if to use this or directly use the equivalent function from pp1000 library, don't call both!
 * - \ref PED_OK : Callback functions successfully registered
 * - \ref PED_ERROR : Invalid parameter
 * - \ref PED_ERR_LOAD : Error loading dynamic library
 * \param[in] scbf : Send callback function
 * \param[in] rcbf : Receive callback function
 */
PED_EXP_DECL int ped_SetSendRcvCb(ped_SendCallback scbf, ped_RcvCallback rcbf);

/**
 * Execute pairing between CTP and PED.
 * \param[out] status : additional status information, see \ref STATUS_CODES, may be NULL
 * \return
 * - \ref PED_OK : No error, pairing successful
 * - \ref PED_ERROR : Pairing failed
 * - \ref PED_ERR_LOAD : Error loading dynamic library
 */
PED_EXP_DECL int ped_Pairing(int *status);

/**
 * Move PIN from PED to CTP.
 * \return
 * - \ref PED_OK : No error, PIN successfully moved
 * - \ref PED_ERROR : Failure
 * - \ref PED_ERR_LOAD : Error loading dynamic library
 */
PED_EXP_DECL int ped_MovePin(void);


#ifdef __cplusplus
}
#endif

#endif
