#ifndef UNITTEST_TESTRUNNER_H
#define UNITTEST_TESTRUNNER_H

#include <set>
#include <memory>

#include "Test.h"
#include "TestList.h"
#include "CurrentTest.h"

namespace UnitTest {

class TestReporter;
class TestResults;
class Timer;

int RunAllTests(int argc, char ** argv);
int RunTheseSuites(int argc, char ** argv, const std::set<std::string> & suites);

struct True
{
	bool operator()(const Test* const) const
	{
		return true;	
	}
};

class TestRunner
{
public:
	explicit TestRunner(std::auto_ptr<TestReporter> reporter);
	~TestRunner();

	template <class Predicate>
	int RunTestsIf(TestList const& list, char const* suiteName, 
				   const Predicate& predicate, int maxTestTimeInMs) const
	{
	    Test* curTest = list.GetHead();

	    while (curTest != 0)
	    {
		    if (IsTestInSuite(curTest,suiteName) && predicate(curTest))
			{
				RunTest(m_result, curTest, maxTestTimeInMs);
			}

			curTest = curTest->next;
	    }

	    return Finish();
	}	

private:
	std::auto_ptr<TestReporter> m_reporter;
	TestResults* m_result;
	Timer* m_timer;

	int Finish() const;
	bool IsTestInSuite(const Test* const curTest, char const* suiteName) const;
	void RunTest(TestResults* const result, Test* const curTest, int const maxTestTimeInMs) const;

	// Non-copyable.
	TestRunner(const TestRunner &);
	TestRunner & operator=(const TestRunner &);
};

}

#endif
