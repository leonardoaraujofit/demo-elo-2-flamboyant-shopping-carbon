#ifndef UNITTEST_MEMORYOUTSTREAM_H
#define UNITTEST_MEMORYOUTSTREAM_H

#include "Config.h"

#ifndef UNITTEST_USE_CUSTOM_STREAMS

#ifdef _VRXEVO
#pragma diag_suppress 69, 161
#include <limits>
#pragma diag_warning 69, 161
#endif

#include <sstream>
#include <stdio.h>

namespace UnitTest
{

class MemoryOutStream : public std::ostringstream
{
public:
    MemoryOutStream() {}
    char const* GetText() const;

private:
    MemoryOutStream(MemoryOutStream const&);
    void operator =(MemoryOutStream const&);

    mutable std::string m_text;
};

// Workaround for Verix compiler, which is missing uint64_t overload.
template <typename CharT, typename traits>
std::basic_ostream<CharT,traits> & operator<< (std::basic_ostream<CharT,traits> & out, const unsigned long long & t)
{
    char buf[20] = {};
    sprintf(buf, "%llu", t);
    out << buf;
    return out;
}

}

#else

#include <cstddef>

namespace UnitTest
{

class MemoryOutStream
{
public:
    explicit MemoryOutStream(int const size = 256);
    ~MemoryOutStream();

    char const* GetText() const;

    MemoryOutStream& operator << (char const* txt);
    MemoryOutStream& operator << (int n);
    MemoryOutStream& operator << (long n);
    MemoryOutStream& operator << (unsigned long n);
    MemoryOutStream& operator << (float f);
    MemoryOutStream& operator << (double d);
    MemoryOutStream& operator << (void const* p);
    MemoryOutStream& operator << (unsigned int s);

    enum { GROW_CHUNK_SIZE = 32 };
    int GetCapacity() const;

private:
    void operator= (MemoryOutStream const&);
    void GrowBuffer(int capacity);

    int m_capacity;
    char* m_buffer;
};

}

#endif

#endif
