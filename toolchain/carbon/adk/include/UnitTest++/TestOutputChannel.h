#ifndef UNITTEST_TESTOUTPUTCHANNEL_H
#define UNITTEST_TESTOUTPUTCHANNEL_H

#include <stdio.h>
#include <stdarg.h>

#include "TestReporter.h"

namespace UnitTest {

struct TestOutputChannel
{
    virtual void Print(const char * str, ...) = 0;
    virtual void PrintLine(const char * str, ...) = 0;
    virtual void PrintLineV(const char * str, va_list args) = 0;
};

//------------------------------------------------------------------------------

struct TestOutputStdout : TestOutputChannel
{
    virtual void Print(const char * str, ...);
    virtual void PrintLine(const char * str, ...);
    virtual void PrintLineV(const char * str, va_list args);
};

//------------------------------------------------------------------------------

struct TestOutputOslog : TestOutputChannel
{
    virtual void Print(const char * str, ...);
    virtual void PrintLine(const char * str, ...);
    virtual void PrintLineV(const char * str, va_list args);
};

//------------------------------------------------------------------------------

struct TestOutputFile : TestOutputChannel
{
    explicit TestOutputFile(const char * filename);
    virtual void Print(const char * str, ...);
    virtual void PrintLine(const char * str, ...);
    virtual void PrintLineV(const char * str, va_list args);
    ~TestOutputFile();

private:
    FILE * m_file;
};

} // namespace UnitTest

#endif // UNITTEST_TESTOUTPUTCHANNEL_H
