#ifndef UNITTEST_TESTREPORTERSTDOUT_H
#define UNITTEST_TESTREPORTERSTDOUT_H

#include <memory>
#include <stdarg.h>

#include "TestReporter.h"
#include "TestOutputChannel.h"

namespace UnitTest {

class TestReporterPlainText : public TestReporter
{
public:
    explicit TestReporterPlainText(std::auto_ptr<TestOutputChannel> output)
        : m_output(output)
    { }

private:
    virtual void ReportTestStart(TestDetails const& test);
    virtual void ReportFailure(TestDetails const& test, char const* failure);
	virtual void ReportSkipped(TestDetails const& test, char const* reason);
    virtual void ReportTestFinish(TestDetails const& test, float secondsElapsed);
    virtual void ReportSummary(int totalTestCount, int failedTestCount, int failureCount, int skippedTestCount, float secondsElapsed);
    virtual void ReportTestLog(TestDetails const& test, const char * fmt, va_list args);

    std::auto_ptr<TestOutputChannel> m_output;

    // Non-copyable.
    TestReporterPlainText(const TestReporterPlainText &);
    TestReporterPlainText & operator= (const TestReporterPlainText &);
};

}

#endif 
