#ifndef UNITTEST_XMLTESTREPORTER_H
#define UNITTEST_XMLTESTREPORTER_H

#include <memory>

#include "DeferredTestReporter.h"
#include "TestOutputChannel.h"

#ifdef _VRXEVO
#pragma diag_suppress 69, 161
#include <limits>
#pragma diag_warning 69, 161
#endif

namespace UnitTest
{

class TestReporterXml : public DeferredTestReporter
{
public:
    explicit TestReporterXml(std::auto_ptr<TestOutputChannel> output);

    virtual void ReportSkipped(TestDetails const& test, char const* reason);
    virtual void ReportSummary(int totalTestCount, int failedTestCount, int failureCount, int skippedTestCount, float secondsElapsed);
    virtual void ReportTestLog(TestDetails const& test, const char * fmt, va_list args);

private:
    TestReporterXml(TestReporterXml const&);
    TestReporterXml& operator=(TestReporterXml const&);

    void AddXmlElement(char const* encoding);
    void BeginResults(int totalTestCount, int failedTestCount, int failureCount, float secondsElapsed);
    void EndResults();
    void BeginTest(DeferredTestResult const& result);
    void AddFailure(DeferredTestResult const& result);
    void EndTest(DeferredTestResult const& result);

    std::auto_ptr<TestOutputChannel> m_output;
};

}

#endif
