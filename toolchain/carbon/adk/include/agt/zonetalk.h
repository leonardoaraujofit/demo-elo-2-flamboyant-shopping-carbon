#ifndef ZONETALK_EXT_H
#define ZONETALK_EXT_H

/** This function is used by the application to perform local or remote ZONTALK operations.  Zontalk is a Verifone proprietary protocol
*	used on older verifone terminals, and it has been incorporated into many users business processes.
*
* @param[in] downloadType = the type of download to perform.  A single char, it can be one of:<BR>
*							o	�F� for a full download.<BR>
*							o	�P� or �p� for partial downloads.<BR>
*							o	�R� like F except that all application files in all groups in both RAM (I:) and flash (F:) are deleted, and the flash is coalesced. Only Group 1 tasks are allowed to do this.<BR>
*							o	�r� like R except that flash is not coalesced.* 
* @param[in] pszAppName = Name of the app to request from the server.  On verix this was obtained from the environment variable
*	*ZA. <BR>
* @param[in] pszTermID = Terminal ID to send to Vericenter Server or zontalk peer.<BR>
*
* @param[in] pszComPort = Device name of serial port.  Use something like "/dev/ttyAMA0"<BR>
* @param[in] address = IP address of zontalk server.  Use something like "192.168.1.110"<BR>
* @param[in] port = TCPIP port.  Vericenter and other zontalk servers generally use 8013, and you probably should too.<BR>
*
*
* @return int - 
*							o	0 = Success
*							o	1 = Init failed
*							o	2 = Stopped
*							o	3 = IOError
*							o	4 = Buffer Overflow
*							o	5 = Operation Failed
*							o	6 = OutOfResource

*
* IMPORTANT NOTES:
* @li The service will identify the caller of the registration function so no
*     indentification parameters are necessary
*/
/*SVC_PROTOTYPE*/ int SVC_ZONTALK(char downloadType, char *pszAppName, char *pszTermID, char *pszComPort);
/*SVC_PROTOTYPE*/ int SVC_ZONTALK_NET(char downloadType, char *pszAppName, char *pszTermID, char *address, unsigned short port);


#endif
