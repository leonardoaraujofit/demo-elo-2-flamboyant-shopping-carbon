/*
 *  ADK TMS Agent API for application integration with the TMS Agent
 *
 *	Copyright, 2017 VeriFone Inc.
 *	88 W Plumeria Dr
 *	San Jose, CA 95134
 *
 *  All Rights Reserved. No part of this software may be reproduced,
 *  transmitted, transcribed, stored in a retrieval system, or
 *  translated into any language or computer language, in any form
 *  or by any means electronic, mechanical, magnetic, optical,
 *  chemical, manual or otherwise, without the prior permission of
 *  VeriFone Inc.
 */
#ifndef SVC_TMS_H
#define SVC_TMS_H

#include "tms_status.h"

#ifdef _VRXEVO
struct version {
	int major;       /**< Major Version */
	int minor;       /**< Minor Version */
	int maint;       /**< Maintenance Version */
	char build[16];  /**< Optional build string (i.e. rc1) */
};
#else
/* VOS and VOS2 have this header under SDK release */
#include "svcmgrSvcDef.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

/*SVC_SERVICE:tms*/

#if (defined _VRXEVO || defined _WIN32)
#  if   defined TMS_DLL_EXPORT
#    define TMS_API __declspec(dllexport)
#  elif defined TMS_DLL_IMPORT && defined _VRXEVO
#    define TMS_API __declspec(dllimport) // dllimport not required for Windows
#  else
#    define TMS_API
#  endif
#elif defined __GNUC__ && defined TMS_DLL_EXPORT
#  define TMS_API  __attribute__((visibility ("default")))
#else
#  define TMS_API
#endif

#ifdef _VRXEVO
#define TMS_MAX_APP_NAME_LENGTH				12
#define TMS_MAX_APP_PARAMETERS				200

#define	TMS_IPC_NOWAIT	04000	/* error if request must wait */
#else
#define TMS_MAX_APP_NAME_LENGTH				32
#define TMS_MAX_APP_PARAMETERS				10000
#endif

/* Allow for 32 user registered applications */
#define TMS_MAX_REGISTERED_APPS		32
#define TMS_REG_TIMEOUT_SECS		30



// defines for tms status
#define TMS_STATUS_SUCCESS								0 /**< successful status */
#define TMS_STATUS_ERROR								5000 /**< error status */
#define TMS_STATUS_UNSUPPORTED_FEATURE					5001 /**< unsupported feature */
#define TMS_STATUS_CONTENT_AVAILABLE					5002 /**< content available status */
#define TMS_STATUS_NO_CONSUMER							5003 /**< no consumer for this content */
#define TMS_STATUS_FILENAME_ERROR						5004 /**< filename is too long */
#define TMS_STATUS_CONTENT_FAILURE						5005 /**< content failed to update */
#define TMS_STATUS_MSGQ_FAILURE							5006 /**< message queue failure */
#define TMS_STATUS_MSGSND_FAILURE						5007 /**< sending message to TMS agent failed */
#define TMS_STATUS_MSGRCV_FAILURE						5008 /**< receiving message from TMS agent failed */
#define TMS_STATUS_APP_EVENT_AVAIL						5009 /**< app event is available */
#define TMS_STATUS_REQUESTED							5010 /**< app event is available */
#define TMS_STATUS_REGISTER_FAIL						5011 /**< app registration failure */
#define TMS_STATUS_REGISTER_NAME_TOO_LONG				5012 /**< app registration name too long */
#define TMS_STATUS_EINVAL								5013 /**< error in a parameter value */
#define TMS_STATUS_AGENT_NOT_RUNNING					5014 /**< the agent is not running */
#define TMS_STATUS_SERVER_INSTANCE_ERROR				5015 /**< generic error with server instance */
#define TMS_STATUS_SERVER_INSTANCE_LOCK_ERROR			5016 /**< server instance lock error */
#define TMS_STATUS_SERVER_INSTANCE_LOCK_TIMEOUT_ERROR	5017 /**< server instance lock timeout error */
#define TMS_STATUS_SERVER_INSTANCE_ALREADY_LOCKED		5018 /**< server instance already locked */
#define TMS_STATUS_SERVER_INSTANCE_MAX_LOCK_REQ_EXCEEDED	5019 /**< too many lock requests */
#define TMS_STATUS_SERVER_INSTANCE_UNLOCK_ERROR			5020 /**< server instance unlock error */
#define TMS_STATUS_SERVER_INSTANCE_CONFIG_LOAD_ERROR	5021 /**< error loading server instance config */
#define TMS_STATUS_AGENT_BUSY_ERROR						5022 /**< agent is busy and can't perform request */
#define TMS_STATUS_POSTPONED							5023 /**< app can use this to postpone a PUT_FILE request */
#define TMS_STATUS_CANCELLED							5024 /**< vhq got the request to cancel current task */
#define TMS_STATUS_AGENT_CONTACTSRV_ERROR				5025 /**< vhq server doesn't ready to process requests from device yet */
#define TMS_STATUS_API_SYNC_ERROR						5026 /**< app and Agent lost sync in messages */


/*SVC_STRUCT*/
/**
* The structure returned in response to some TMS APIs.  This structure is returned when communication with the TMS Agent is required and an immediate response may not be available. In these cases, the status field will indicate TMS_STATUS_REQUESTED, and the handle will be used to track the ultimate response from the TMS Agent.  When the agent is able to respond to the request, it will send the response in a tmsEventData structure with the handle of that structure set to the same value as the handle returned in this structure.
*/
struct tmsReturn {
	int status;		/**< Return state value - TMS_STATUS_REQUESTED indicates the request was successfully made to the TMS Agent */
	int handle;		/**< Handle for tracking purposes - valid handle is > 0 */
};


/* The maximum length for file names or paths */
#define MAX_TMS_FILENAME_LENGTH				4096

/* The maximum length a transaction type string can be */
#define MAX_TRANSACTION_TYPE_STR_LEN		128

/* The maximum length of a server instance name */
#define MAX_SERVER_INSTANCE_NAME_LEN		128

/* The maximum number of lock requests at a time */
#define MAX_SERVER_INSTANCE_LOCK_REQ		10

/* The name of the primary server instance */
#define PRIMARY_SERVER_INSTANCE_NAME	"primary"


/* The RESPONSE events come back from TMS Agent in response to requests from the application (tmsEventData.evtType) */
#define TMS_EVT_REGISTER_APP_RESPONSE			1
#define TMS_EVT_UNREGISTER_APP_RESPONSE			2
#define TMS_EVT_CALL_SERVER_RESPONSE			3
#define TMS_EVT_GET_TMS_CONFIG_RESPONSE			4
#define TMS_EVT_SET_TMS_CONFIG_RESPONSE			5

/* The rest of these events are the TMS Agent notifying the app it needs to do something (tmsEventData.evtType) */
#define TMS_EVT_SET_APP_STATE					6
#define TMS_EVT_SET_APP_INFO					7
#define TMS_EVT_SET_PARM_LIST					8
#define TMS_EVT_GET_FILE						9
#define TMS_EVT_PUT_FILE						10
#define TMS_EVT_DEL_FILE						11
#define TMS_EVT_DO_TRANSACTION					12

/* This event notifies the app of an event it requested notification for when it registered (tmsEventData.evtType) */
#define TMS_EVT_NOTIFICATION					13

/* These events are for the app to manage server instances */
#define TMS_EVT_GET_SERVER_INSTANCE				14
#define TMS_EVT_LOCK_SERVER_INSTANCE			15
#define TMS_EVT_RELEASE_SERVER_INSTANCE			16

/* This event is for app generated alerts */
#define TMS_EVT_APP_ALERT_RESULT				17

/* This event provides the final result of tms_clearApplicationInfo() call */
#define TMS_EVT_CLEAR_APP_INFO_RESULT			18

/* This event is for notification app in case of API usage errors */
#define TMS_EVT_API_ERRORS						19



// defines for put file type (tmsEventData.putFileType)
#define TMS_PUT_FILE_TYPE_UNSIGNED_BUNDLE					1 /**< an unsigned bundle type */
#define TMS_PUT_FILE_TYPE_UNSIGNED_FILE						2 /**< a single unsigned file */
#define TMS_PUT_FILE_TYPE_MIXED_BUNDLE						3 /**< a mixed bundle type (unsigned and signed packages) */
#define TMS_PUT_FILE_TYPE_SIGNED_BUNDLE						4 /**< a signed bundle type */
#define TMS_PUT_FILE_TYPE_FULL_PARAM_FILE					5 /**< a full parameter file type */
#define TMS_PUT_FILE_TYPE_PARTIAL_PARAM_FILE_FLAT			6 /**< a flat partial parameter file type */
#define TMS_PUT_FILE_TYPE_PARTIAL_PARAM_FILE_HIERARCHICAL	7 /**< a hierarchical partial parameter file type */
#define TMS_PUT_FILE_TYPE_FULL_PARAM_FILE_CONFIG_SYS		8 /**< a full parameter file type, automatically merged into the environment */


// defines for postInstallAction notification data (tmsEventData.notificationData.postInstallAction)
#define TMS_POST_INSTALL_ACTION_NONE						0
#define TMS_POST_INSTALL_ACTION_REBOOT						1
#define TMS_POST_INSTALL_ACTION_RESTART_APPS				3


/*SVC_STRUCT*/
/**
* The TMS Event structure that needs to be handled by the callback function provided in the applications tms_registerApplication() call.  The strcuture has detailed information about the events that the application gets notified of.
*/
struct tmsEventData {
	int status;		/**< status of the event */
	int evtType;	/**< type of event */
	int handle;	    /**< a handle that can be used for tracking purposes - gets passed back in certain response functions */
	int putFileType;	/**< type of file to process for TMS_EVT_PUT_FILE events */
	int numFileEntries;  /**< number of entries in a file (applies to TMS_EVT_SET_APP_INFO/TMS_EVT_SET_PARM_LIST response) */
	unsigned char removeFile;  /**< whether to remove file after it is received by the agent (applies to TMS_EVT_GET_FILE response) */
	int appStatus; /**< status of application in response to TMS_EVT_SET_APP_STATE request */
	char filename[MAX_TMS_FILENAME_LENGTH];	/**< filename to process (may be empty) - used for TMS_EVT_GET_FILE, TMS_EVT_PUT_FILE and TMS_EVT_DEL_FILE events */
	char filepath[MAX_TMS_FILENAME_LENGTH];	/**< Path where the file will go (may be empty) - used for TMS_EVT_GET_FILE, TMS_EVT_PUT_FILE and TMS_EVT_DEL_FILE events */
	int eventMask; /**< mask of events for TMS_EVT_NOTIFICATION event */
	char transactionType[MAX_TRANSACTION_TYPE_STR_LEN]; /**< type of transaction to perform in TMS_EVT_DO_TRANSACTION event */
	union{
		int downloadPercent; /**< percentage of download that is complete for TMS_EVT_NOTIFICATION / TMS_EVENT_NOTIFY_DOWNLOAD_PERCENT event */
		int postInstallAction; /**< post install action configured by server for TMS_EVT_NOTIFICATION / TMS_EVENT_NOTIFY_INSTALL_DOWNLOAD event */
	}notificationData;
};



/***************************************************************************************************
* These APIs do not require an application to be registered to use
****************************************************************************************************/



/** This function is used by an application to get the version of TMS library that the application
* is linked with, or the shared version of the library that the application is using.
*
* @return version struct defined in svcmgrSvcDef.h
*
* IMPORTANT NOTES: This function could return incomplete Maintenance versions
*/
/*SVC_PROTOTYPE*/ TMS_API struct version tms_getVersion(void);

/** This function is used by an application to get the version of TMS library that the application
* is linked with, or the shared version of the library that the application is using.
*
* @return version string
*/
/*SVC_PROTOTYPE*/ TMS_API char* tms_GetVersion(void);






/** This function is used by an application to get the version of the ADK TMS Agent that is running
* on the device.
*
* @return version struct defined in svcmgrSvcDef.h
*
* IMPORTANT NOTES:
* @li This function could return incomplete Maintenance versions
* @li This function will communicate to the ADK TMS Agent via pipes/message queues so it may block
*     for a short period of time
*/
/*SVC_PROTOTYPE*/ TMS_API struct version tms_getAgentVersion(void);

/** This function is used by an application to get the version of the ADK TMS Agent that is running
* on the device.
*
* @return version string
*
* IMPORTANT NOTES:
* @li This function will communicate to the ADK TMS Agent via pipes/message queues so it may block
*     for a short period of time
*/
/*SVC_PROTOTYPE*/ TMS_API char* tms_GetAgentVersion(void);






/** This function is used by the application to call the management server. The app does not
* need to be registered in order to request a TMS call. If the app is registered, it will get
* status information about the server communication in its callback.
*
* @param[in] secondsToWait = the number of seconds from now to contact the server.  This can
*				be used to schedule server contacts for the future.  A value of 0 indicates to
*				contact the server right now.
*
* @return struct tmsReturn = a tmsReturn structure<BR>
*					tmsReturn.status = the immediate library status response.  TMS_STATUS_REQUESTED
*					indicates the library was able to successfully send the contact server request
*					to the VHQ Agent.  All other return values indicate a failure.<BR><BR>
*					tmsReturn.handle = the handle used for further status tracking in the callback.
*					The callback status events will only be sent to registered applications.
*
*/
/*SVC_PROTOTYPE*/ TMS_API struct tmsReturn tms_callServer(int secondsToWait /* 0 */);


#define TMS_API_CALL_SERVER_UPDATE_CHECK_ONLY_FLAG		0x1
#define TMS_API_CALL_SERVER_FORCE_MAINTENANCE_FLAG		0x2


/** This function is used by the application to call the management server. The app does not
* need to be registered in order to request a TMS call. If the app is registered, it will get
* status information about the server communication in its callback.
*
* @param[in] secondsToWait = the number of seconds from now to contact the server.  This can
*				be used to schedule server contacts for the future.  A value of 0 indicates to
*				contact the server right now.
*
* @return struct tmsReturn = a tmsReturn structure<BR>
*					tmsReturn.status = the immediate library status response.  TMS_STATUS_REQUESTED
*					indicates the library was able to successfully send the contact server request
*					to the VHQ Agent.  All other return values indicate a failure.<BR><BR>
*					tmsReturn.handle = the handle used for further status tracking in the callback.
*					The callback status events will only be sent to registered applications.
*
*/
/*SVC_PROTOTYPE*/ TMS_API struct tmsReturn tms_callServer2(int secondsToWait /* 0 */, unsigned int iFlags);










/** This function can be used by the application to tell the VHQ agent to load and run with a new
* configuration.  When using this function, the application should first call the
* tms_getConfigLocation() function to get the location of the current VHQ configuration.  The
* application should then make a temporary copy of the current VHQ configuration and edit the
* temporary copy for any configuration updates the application wants to make.  After making the
* changes to the temporary file, the tms_setNewConfigAvailable() function should be called and the
* temporary file with the requested changes should be passed to this function.  The agent will load
* the new changes and store them in its permanent configuration.  The configuration changes will occur
* in run-time and no reboot is required. The App does not need to be registered in order to request
* a TMS config update
*
* @param[in] newTMSConfig = the full path to the new TMS configuration file.<BR>
*			<B>NOTE: The agent must have read permissions on the temporary file for this to work correctly.</B>
* @param[in] timeout = time to wait (in seconds) before giving up because the agent might be busy.<BR>
*			<B>NOTE: This parameter only applies to tms_setNewConfigAvailableWithTimeout API.</B>
*
* @return struct tmsReturn = a tmsReturn structure<BR>
*					tmsReturn.status = the immediate library status response.  TMS_STATUS_REQUESTED
*					indicates the library was able to successfully send the contact server request
*					to the VHQ Agent.  All other return values indicate a failure.<BR><BR>
*					tmsReturn.handle = the handle used for further status tracking in the callback.
*					The callback status events will only be sent to registered applications.
*/
/*SVC_PROTOTYPE*/ TMS_API struct tmsReturn tms_setNewConfigAvailable(const char* newTMSConfig /*REQ*/);
/*SVC_PROTOTYPE*/ TMS_API struct tmsReturn tms_setNewConfigAvailableWithTimeout(const char* newTMSConfig /*REQ*/, int timeout /* 0 */);






/***************************************************************************************************
* These APIs involve apps registering with the agent for status notifications, file updates, etc.
****************************************************************************************************/



/* These masks can be used in the eventNotifyMask parameter of tms_registerApplication.  These masks should be ORd
together by the application to tell the TMS Agent which events the application wants to be notified about */
#define TMS_EVENT_NOTIFY_REBOOT_DEVICE				0x00000001
#define TMS_EVENT_NOTIFY_RESTART_APPS				0x00000002
#define TMS_EVENT_NOTIFY_DOWNLOAD_STARTED			0x00000004
#define TMS_EVENT_NOTIFY_DOWNLOAD_PERCENT			0x00000008
#define TMS_EVENT_NOTIFY_DOWNLOAD_COMPLETE			0x00000010
#define TMS_EVENT_NOTIFY_INSTALL_DOWNLOAD			0x00000020
#define TMS_EVENT_NOTIFY_MAINTENANCE_END			0x00000040
#define TMS_EVENT_NOTIFY_HEARTBEAT_RESULT			0x00000080
#define TMS_EVENT_NOTIFY_CONTACT_SERVER_START		0x00000100
#define TMS_EVENT_NOTIFY_CONTACT_SERVER_END			0x00000200
#define TMS_EVENT_NOTIFY_CONTACT_SERVER_APPROVAL	0x00000400
#define TMS_EVENT_NOTIFY_CONTENT_UPDATES			0x00000800
#define TMS_EVENT_NOTIFY_CLOCK_UPDATE				0x00001000
#define TMS_EVENT_NOTIFY_SERVER_ERRORS				0x00002000
#define TMS_EVENT_NOTIFY_INSTALL_STATUS				0x00004000



typedef int(*tmsEventCallback)(struct tmsEventData eventData);
typedef int(*tmsEventCallback2)(struct tmsEventData* eventData, void* data);



/** This function is used by the application to register itself with the VHQ agent.  When
* registering, the application provides its name, a mask specifying the types of events the
* application would like to be notified of, and a callback function for handling events sent
* to the app from the VHQ agent.
*
* @param[in] appName = A name used by the VHQ agent to help identify the different applications that
*						are registered.  The app name is used for identification purposes, as well as
*						parameter management.The registered app name MUST match the application name
*						specified in Secure Installer bundle for V/OS devices, or the application name
*						specified in Manifest files for VX devices.  This name is case sensitive so it must
*						match exactly.<BR>
* @param[in] eventNotifyMask = A mask provided by the application to configure the events that the
*						application wants to be notified of by the VHQ agent.  The application can
*						request notification for these types of events:<BR>
*							o	Reboot (0x00000001) = Notify the application before the agent performs
*								a reboot.  If registered for this notification, the application will
*								have to approve a reboot which will give the app time to perform any
*								required tasks before the reboot occurs.<BR>
*							o	Restart Applications (0x00000002) = Notify the application before the
*								agent performs a restart of all applications.  If registered for this
*								notification, the application will have to approve the application
*								restart which will give the app time to perform any required tasks
*								before the application restarts.<BR>
*							o	Download Started (0x00000004) = Notify the application when the agent
*								begins a download.<BR>
*							o	Download Percent (0x00000008) = Notify the application with updates
*								about the progress of an ongoing download (this is not available in
*								the current version of the VHQ Agent but will be added in future versions).<BR>
*							o	Download Completed (0x00000010) = Notify the application when the agent
*								completes a download.<BR>
*							o	Install Download (0x00000020) = Notify the application before a download
*								is installed by the agent.  If registered for this notification, the
*								application will have to approve the install which will give the app
*								time to perform any required tasks before the device is possibly
*								restarted as part of the installation.<BR>
*							o	Maintenance End (0x00000040) = Notify the application when all
*								maintenance activities for a given heartbeat have been completed.<BR>
*							o	Heartbeat Result (0x00000080) = Notify the application with the final
*								result of heartbeat messages to the server<BR>
*							o	Contact Server Start (0x00000100) = Notify the application when contact
*								to the server is beginning.<BR>
*							o	Contact Server End (0x00000200) = Notify the application when all contact
*								to the server for a given session has completed.<BR>
*							o	Contact Server Approval (0x00000400) = When registered for this
*								notification, the application will have to approve the agent contacting
*								the server.  This is useful for dial-up connections so that the agent will
*								not try to use the modem while the application is using it.<BR>
*							o	Application Content Update Available (0x00000800) = Notify the application
*								when content is available for the application.  This notification must be
*								enabled for the application to support VHQ content management.<BR>
*							o	Clock Update (0x00001000) = Notify the application before the agent performs
*								a clock update.  If registered for this notification, the application will
*								have to approve the clock update so the app can adjust any timers, etc.<BR>
*							o	Server Errors (0x00002000) = Notify the application
*								when a communication with the server fails, including an error code.<BR>
*
* @param[in] tmsEvtCb = A callback function provided by the application to handle events sent from the agent
*						to the application.  The prototype for the callback function is:
*							<CODE>int tmsEventCallback(struct tmsEventData eventData);</CODE>
*						The following events can be sent from the agent to the application and need to be
*						handled by the application in the callback:<BR>
*						<B>All registered applications must handle these events</B><BR>
*							o	SET_APP_INFO (eventData.evtType = 7) = event used by the agent to request
*								application specific parameters to be sent up in VHQ messages to the VHQ server.
*								The application should call tms_setApplicationInfo() in response to this message.  DeviceId would be an example of a parameter passed from the application to the agent<BR>
*							o	SET_PARM_LIST (eventData.evtType = 8) = event used by the agent to request a
*								list of application files that can be managed through VHQ.  The
*								application should call tms_setApplicationParameterList() in response to
*								this message.  The list of files provided is mainly used for parameter management but can be used for more than that. At a minimum, the app should call tms_setApplicationParameterList() with numParamFiles=0 and paramInfoFile=NULL<BR>
*						<B>If the app registers with any of these notification flags set in its register call(TMS_EVENT_NOTIFY_REBOOT_DEVICE, TMS_EVENT_NOTIFY_RESTART_APPS, TMS_EVENT_NOTIFY_INSTALL_DOWNLOAD, TMS_EVENT_NOTIFY_CONTACT_SERVER_APPROVAL or TMS_EVENT_NOTIFY_CLOCK_UPDATE), then it needs to handle this event:</B><BR>
*							o	SET_APP_STATE (eventData.evtType = 6) = event used by the agent to request the
*								application state (whether it is busy or free).  The application should call
*								tms_setApplicationState() in response to this message.<BR>
*						<B>If content management, parameter management, or being able to upload app files to the server is desired, then these events should be handled:</B><BR>
*							o	GET_FILE (eventData.evtType = 9) = event used by the agent to retrieve a file
*								from the application.  This event is used for parameter management.  The
*								application should call tms_getApplicationFileAvailable() with the shared
*								location of the parameter file in response to this message.<BR>
*							o	PUT_FILE (eventData.evtType = 10) = event used by the agent to send a file to
*								the application.  This event is used for parameter management and content
*								management.  The application should call tms_setFileOperationResult() in
*								response to this message.<BR>
*							o	DEL_FILE (eventData.evtType = 11) = event used by the agent to request the
*								application to delete one of its files.  The application should call
*								tms_setFileOperationResult() in response to this message.  It is up to the
*								application as to whether it will actually delete the file.  For instance,
*								the app should respond with an error if the DEL_FILE event is trying to
*								delete a file that should not be deleted.<BR>
*							o	DO_TRANSACTION (eventData.evtType = 12) = event used by the agent to request
*								the application to do a transaction (such as a daily clearance or diagnostics).
*								The application should call tms_setTransactionResult() in response to this message.<BR>
*						<B>These events can be handled by the application if it wants to keep up to date with the status of the TMS Agent, and what it is doing.  No response is required from the app for these events</B><BR>
*							o	REGISTER_APP_RESPONSE (eventData.evtType = 1) = event to provide the result
*								of the tms_registerApplication()  request.<BR>
*							o	UNREGISTER_APP_RESPONSE (eventData.evtType = 2) = event to provide the result
*								of the tms_unregisterApplication()  request.  This event will only get sent if
*								the unregister call fails.<BR>
*							o	CALL_SERVER_RESPONSE (eventData.evtType = 3) = event to provide the result of
*								a tms_callServer() request.<BR>
*							o	GET_TMS_CONFIG_RESPONSE (eventData.evtType = 4) = event to provide the result
*								of a tms_getConfigLocation() request.<BR>
*							o	SET_TMS_CONFIG_RESPONSE (eventData.evtType = 5) = event to provide the result
*								of a tms_setNewConfigAvailable() request.<BR>
*							o	EVENT_NOTIFICATION (eventData.evtType = 13) = event sent by the agent to notify
*								the application of the different events the app requested notificaction for in its
*								register call.<BR><BR>
*							*NOTE: For the events that the agent expects a response from, the agent will timeout
*							after 30 seconds of waiting for the response.  The events that the agent expects a
*							response from are: SET_APP_INFO, SET_PARM_LIST, GET_FILE, PUT_FILE, DEL_FILE and
*							DO_TRANSACTION.  So if the application is not calling response APIs directly in the
*							callback, it must call the response API within 30 seconds of receiving the event in the
*							callback.  SET_APP_STATE expects a response as well but the agent will wait 5 minutes for
*							the application to respond to a SET_APP_STATE event.<BR>
*							*NOTE2: The 30 second timeout and 5 minute timeout are default timeout values.  These are
*							configurable via the "AppIfc Event Response Timeout" and "AppIfc SetAppState Timeout"
*                           VHQ parameters (respectively).
*
*
*
* @return struct tmsReturn - a tmsReturn structure<BR>
*								tmsReturn.status = the immediate library status response.  TMS_STATUS_REQUESTED
*								indicates the library was able to successfully send the register request to the
*								VHQ Agent.  All other return values indicate a failure.<BR><BR>
*								tmsReturn.handle = the handle used for further status tracking in the callback
*
*
* IMPORTANT NOTES:
* @li The service will identify the caller of the registration function so no
*     indentification parameters are necessary
*/
/*SVC_PROTOTYPE*/ TMS_API struct tmsReturn tms_registerApplication(const char* appName /*REQ*/, int eventNotifyMask /* 0 */, unsigned int tmsEvtCb /* 0 */);
/*SVC_PROTOTYPE*/ TMS_API struct tmsReturn tms_registerApplication2(const char* appName /*REQ*/, int eventNotifyMask /* 0 */, tmsEventCallback2 tmsEvtCb /* 0 */, void* cbData /* 0 */);


/** This function is used by an application to unregister itself with the TMS Agent
*
* @param[in] appName = The name the app provided in its registration call.
*
*@return struct tmsReturn = a tmsReturn structure<BR>
					tmsReturn.status = the immediate library status response.  TMS_STATUS_REQUESTED indicates the
					library was able to successfully send the unregister request to the VHQ Agent.  All other
					return values indicate a failure.<BR><BR>
					tmsReturn.handle = the handle used for further status tracking in the callback.  The callback
					will only get status messages with this handle only in the case where the unregistration fails.
					Upon successful unregistration, the only response the application will get is TMS_STATUS_REQUESTED
					status in response to this call.

*/
/*SVC_PROTOTYPE*/ TMS_API struct tmsReturn tms_unregisterApplication(const char* appName /*REQ*/);







/** This function is called by the application to request the location of the VHQ configuration
* file.
*
* @return struct tmsReturn = a tmsReturn structure<BR>
*					tmsReturn.status = the immediate library status response.  TMS_STATUS_REQUESTED
*					indicates the library was able to successfully send the contact server request
*					to the VHQ Agent.  All other return values indicate a failure.<BR><BR>
*					tmsReturn.handle = the handle used for further status tracking in the callback.
*					The callback status events will only be sent to registered applications.
*
*/
/*SVC_PROTOTYPE*/ TMS_API struct tmsReturn tms_getConfigLocation(void);





/* These defines are used for appState in tms_setAppState() call */
#define APP_STATUS_FREE							1
#define APP_STATUS_BUSY							2
#define APP_STATUS_POSTPONE						3
#define APP_STATUS_CANCEL						4		// cancel operation in progress if possible (Vericenter modes only for now)



/** The application will call this function in response to a SET_APP_STATE event received in its callback.
* If the application is not busy, it will respond with a FREE status.  If the application is busy, it can
* respond with a BUSY status which will prevent certain agent operations (like a reboot) from taking place.
* After the application completes what is causing it to respond with BUSY, it will call this function again
* with the FREE status.  The application will have 5 minutes to respond with the FREE status after responding
* with the BUSY status.  If the application does not respond with a FREE status within 5 minutes of responding
* with BUSY status, the agent will continue with its planned operation.<BR><BR>
* This function can also be called at any time by the app to set its BUSY or FREE state.  This will allow the
* app to pre-emtively cause certain operations (like reboot) from occurring.  So if the app sets its state to
* BUSY without a SET_APP_STATE event being received in the callback, the agent will already know the app is
* BUSY and wont send the SET_APP_STATE in situations when it normally would (like just before rebooting the
* terminal).  Instead the agent will wait for the app to set its state to FREE before proceeding.  For instance,
* before the app does its daily clearance, it can pre-emtively use this function to set its state to BUSY.  When
* the app is complete with the daily clearance, it can the use this function to set its state to FREE again.<BR>
* NOTE: The 5 minute timeout to set FREE status is the default value, but is configurable in the
* "AppIfc Busy Response Timeout" VHQ parameter.

*
* @param[in] handle = this should be the same handle provided in the callback event.  If using this function
*						pre-emtively, the handle should be set to -1.
* @param[in] appState = the state of the app.  APP_STATUS_FREE if the application is free, or APP_STATUS_BUSY
*						if the application is busy.  In Vericentre modes, APP_STATUS_CANCEL is also supported which will cancel the operation in progress, if possible.
*
* @return int - status of the call. TMS_STATUS_REQUESTED indicates the call was successful.  All other values indicate a failure.
*/
/*SVC_PROTOTYPE*/ TMS_API int tms_setApplicationState(int handle /*REQ*/, int appState /*REQ*/);










// defines for custom info type (see struct tmsAppParameter)
#define TMS_PARAMETER_TYPE_IDENTIFIER			0x01 /**< a parameter to send with identifier information */
#define TMS_PARAMETER_TYPE_DEVICE_PROFILE		0x02 /**< a parameter to send with device profile information */
#define TMS_PARAMETER_TYPE_DIAGNOSTIC			0x04 /**< a parameter to send with diagnostic information */
#define TMS_PARAMETER_TYPE_VERICENTER			0x08 /**< a parameter to send with diagnostic information */


/*SVC_STRUCT*/
/**
* Application Parameter Structure used by the application to pass an application parameter to TMS.
* The application will setup one of these structures for each custom parameter it would like sent
* to the TMS server.  An array of these structures is then pointed to in the tmsAppInfo structure
* that gets passed to tms_setApplicationInfo() in response to a TMS_EVT_SET_APP_INFO event.
*/
struct tmsAppParameter {
	char parameterName[64];  /**< NULL terminated parameter name - i.e. "DeviceId" for VX terminals */
	char parameterValue[256];  /**< NULL terminated parameter Value - i.e. Device ID value for VX terminals */
	int parameterType; /**< Parameter type (i.e. TMS_PARAMETER_TYPE_IDENTIFIER, TMS_PARAMETER_TYPE_DEVICE_PROFILE, or TMS_PARAMETER_TYPE_DIAGNOSTIC) */
};


/*SVC_STRUCT*/
/**
* Application Information Structure used by the application to pass application parameters to TMS.  This structure is filled out by the application and passed to tms_setApplicationInfo() in response to a TMS_EVT_SET_APP_INFO event.
*/
struct tmsAppInfo {
	struct tmsAppParameter* parameterList; /**< Pointer to an array of tmsAppParameter[] values to be sent to the server */
	int parameterCount; /**< number of tmsAppParameter values in the parameterList array */
};




/** The application will call this function in response to a SET_APP_INFO event received in its callback.
* This will allow the application to send application specific parameters to the agent that can be sent
* in VHQ messages to the server.  There are three types of parameters that the application can send to the
* agent: IDENTIFIER, DEVICE_PROFILE and DIAGNOSTIC parameters.  The eventMask field of the event will
* contain the mask of the parameters the agent is requesting.  The application should provide only the type
* of parameters requested by the agent in the eventMask field.  One example of this would be the Device ID
* that is controlled by the application.  Another example would be the Lane/Store info that is controlled
* by the POS system , but can be retrieved by the application.
*
* @param[in] handle = this should be the same handle provided in the callback event.
* @param[in] appInfo = a structure containing all of the application specific parameters that can be sent
*						to the VHQ server.  There are 3 types of parameters that can be sent to the server:
*	TMS_PARAMETER_TYPE_IDENTIFIER = IDENTIFIER parameters are included in every message that the agent sends
*		to the server.  Examples include "DeviceId" or "StoreId".
*	TMS_PARAMETER_TYPE_DEVICE_PROFILE = DEVICE_PROFILE parameters are not currently sent to the server
*	TMS_PARAMETER_TYPE_DIAGNOSTIC = DIAGNOSTIC parameters are sent to the server in response to a special
*		GetDiagData request made by the server.  The server currently makes the special GetDiagData request
*		once per day.  Examples would be statistics collected by the application.
*
* @return int = status of the call. TMS_STATUS_SUCCESS indicates the call was successful.  All other values
*				indicate a failure.
*
* IMPORTANT NOTES:  This is used by the application to respond to TMS_EVT_SET_APP_INFO
* request from the agent.
*
*
*/
/*SVC_PROTOTYPE*/ TMS_API int tms_setApplicationInfo(int handle /*REQ*/, struct tmsAppInfo appInfo /*REQ*/);




/** The application can call this function to clear any parameters the app has sent to the agent that
* the agent has cached.  Since the agent will only request application info periodically, this API can
* be used when the app needs to change a parameter it has already provided to the agent.  After calling
* this API, the agent cached information for the app will be cleared, and the agent will be forced to
* request the application info the next time it needs it with the TMS_EVT_SET_APP_INFO event
*
*
* @return int = status of the call. TMS_STATUS_REQUESTED indicates the call was successful (but not necessarily
*               the clearing of the cached data).  The final result of the call will come in the registered
*               callback handler with TMS_EVT_CLEAR_APP_INFO_RESULT. All other values indicate a failure
*
*
*/
/*SVC_PROTOTYPE*/ TMS_API struct tmsReturn tms_clearApplicationInfo(void);





/** The application will call this function in response to a SET_PARM_LIST event received in its callback.
* The application can provide a filename that contains a list of parameter files that can be managed through
* VHQ in its response.
*
* @param[in] handle = this should be the same handle provided in the callback event.
* @param[in] numParamFiles = the number of parameter files described in paramInfoFile.
* @param[in] paramInfoFile = a file that lists all of the applications parameter files, as well as a hash value for each of the parameter files.
*							  this file will contain a line for each paramter file
*							  each line will have 3 whitespace separated fields - Name, Hash Algo, Hash
*							  i.e. "TMS.ini SHA1 0ad19e6f4424a459b625fb38de402226b012e8d7"
*
* @return int = status of the call. TMS_STATUS_SUCCESS indicates the call was successful.  All other values indicate a failure.
*
* IMPORTANT NOTES:  This is used by the application to respond to TMS_EVT_SET_PARM_LIST
* request from the agent
*/
/*SVC_PROTOTYPE*/ TMS_API int tms_setApplicationParameterList(int handle /*REQ*/, int numParamFiles /*REQ*/, const char* paramInfoFile /*REQ*/);








/** The application will call this function in response to a GET_FILE event received in its callback.  When
* the agent requests a file with GET_FILE, the application will put the file in a shared location and use this
* call to notify the agent that the file is available.<BR>
* <B>NOTE: the shared location file must have read permissions for the agent.  Also, this is currently only
* used for parameter management.</B>

*
* @param[in] handle = this should be the same handle provided in the callback event.
* @param[in] result = the status of the application getting the file.  If the agent requests a file that the
*						application should not share, the app should respond with an error.
*						TMS_STATUS_SUCCESS indicates getting the file was successful, any other value indicates
*						an error
* @param[in] fileLocation = the shared location where the application put the requested file.  This only needs
*							to be set if the file retrieval was successful.
* @param[in] removeFile = a boolean telling the agent whether it should delete the file when it is finished with
*							it.  For cases where the app copies a file from its private area to a shared area,
*							this should be set to TRUE.
*
* @return int = status of the call. TMS_STATUS_SUCCESS indicates the call was successful.  All other values indicate
* a failure.
*
* IMPORTANT NOTES:  This is used by the application to respond to TMS_EVT_GET_FILE
* request from the agent
*/
/*SVC_PROTOTYPE*/ TMS_API int tms_getApplicationFileAvailable(int handle /*REQ*/, int result /*REQ*/, const char* fileLocation /*REQ*/, unsigned char removeFile /*REQ*/);








/** The application will call this function in response to a PUT_FILE or DEL_FILE event received in its callback.
* When the agent requests a PUT_FILE, the application should copy the file from the location provided by the event,
* into its own directory (if allowed).  When the agent requests a DEL_FILE, the application should delete the
* specified file (if allowed).  If the PUT_FILE or DEL_FILE is trying to operate on a file that should not be
* operated on, then the app should respond with an error.
*
* @param[in] handle = this should be the same handle provided in the callback event.
* @param[in] fileOpResult = the status of the PUT_FILE or DEL_FILE request.  TMS_STATUS_SUCCESS should be used to
*							indicate the operation was successful.  Any other status response indicates to the agent
*							that the file operation failed.
* @param[in] fileOpType = this should be the same event type provided in the callback event.  For PUT_FILE requests,
*							it should be PUT_FILE and for DEL_FILE requests it should be DEL_FILE.
* @param[in] additional_info = this is a string, mostly used for error cases, that can be relayed to the server with the result.
*							Maximum length of this string should be 256 characters.
*
* @return int = status of the call. TMS_STATUS_SUCCESS indicates the call was successful.  All other values indicate a failure.
*
* IMPORTANT NOTES:  This is used by the application to respond to TMS_EVT_PUT_FILE or TMS_EVT_DEL_FILE
* request from the agent
*/
/*SVC_PROTOTYPE*/ TMS_API int tms_setFileOperationResult(int handle /*REQ*/, int fileOpResult /*REQ*/, int fileOpType /*REQ*/);
/*SVC_PROTOTYPE*/ TMS_API int tms_setFileOperationResultWithDescription(int handle /*REQ*/, int fileOpResult /*REQ*/, int fileOpType /*REQ*/, char* additional_info /*REQ*/);










/** The application will call this function in response to a DO_TRANSACTION event received in its callback function.
* The application will report the result of the requested transaction in this function call.
*
* @param[in] handle = this should be the same handle provided in the callback event.
* @param[in] transactionResult = the status of the DO_TRANSACTION request.  TMS_STATUS_SUCCESS should be used to
*									indicate the transaction was successful.  Any other status response indicates
*									to the agent that the transaction failed.
*
* @return int = status of the call. TMS_STATUS_SUCCESS indicates the call was successful.  All other values indicate a failure.
*
* IMPORTANT NOTES:  This is used by the application to respond to TMS_EVT_DO_TRANSACTION
* request from the agent
*/
/*SVC_PROTOTYPE*/ TMS_API int tms_setTransactionResult(int handle /*REQ*/, int transactionResult /*REQ*/);




/** The application will call this function to get the name of the current server instance being used
*
* @return struct tmsReturn = a tmsReturn structure<BR>
*					tmsReturn.status = the immediate library status response.  TMS_STATUS_REQUESTED
*					indicates the library was able to successfully send the get server instance request
*					to the VHQ Agent.  All other return values indicate a failure.<BR><BR>
*					tmsReturn.handle = the handle used for further status tracking in the callback.
*					The callback will provide further status including the name of the current server
*					instance.
*
*
*/
/*SVC_PROTOTYPE*/ TMS_API struct tmsReturn tms_getServerInstance(void);



/** The application will call this function to lock the server instance to the provided instanceId. A timeout
* can be provided and if the lock can't be obtained in the time period specified, the call will fail.
*
* @param[in] instanceId = the name of the server instance to set the agent to if the lock is obtained successfully
* @param[in] mlSecTimeout = number of milliseconds to wait for the lock.  If the lock can't be obtained in the time
*							specified, an error will be returned.  A value of 0 for this means to wait forever.
*
* @return struct tmsReturn = a tmsReturn structure<BR>
*					tmsReturn.status = the immediate library status response.  TMS_STATUS_REQUESTED
*					indicates the library was able to successfully send the lock server instance request
*					to the VHQ Agent.  All other return values indicate a failure.<BR><BR>
*					tmsReturn.handle = the handle used for further status tracking in the callback.
*					The callback will provide further status including the status of the lock
*					and the name of the current instanceID.
*
* IMPORTANT NOTES:  The current instanceID being used will be returned in the callback.  That name can be used in the
* tms_releaseServerLock() call to restore the instance that was in use before the app requested the lock on the new server
*/
/*SVC_PROTOTYPE*/ TMS_API struct tmsReturn tms_lockServerInstance(char* instanceId /*REQ*/, int mlSecTimeout /*REQ*/);



/** The application will call this function to release a lock it has obtained on server instances
*
* @param[in] restoreInstanceId = the name of the server instance to restore when the lock is released (obtained
*								in the callback response to tms_lockServerInstance() call)
*
* @return struct tmsReturn = a tmsReturn structure<BR>
*					tmsReturn.status = the immediate library status response.  TMS_STATUS_REQUESTED
*					indicates the library was able to successfully send the release server lock request
*					to the VHQ Agent.  All other return values indicate a failure.<BR><BR>
*					tmsReturn.handle = the handle used for further status tracking in the callback.
*					The callback will provide final status of releasing the lock
*
* IMPORTANT NOTES:  If the app wants to restore the previous instance, it can pass the instanceID that
* was returned from the lock call to this API.  The agent will restore the previous instance after
* the unlock request is processed.
*/
/*SVC_PROTOTYPE*/ TMS_API struct tmsReturn tms_releaseServerLock(char* restoreInstanceId /*REQ*/);






#define TMS_ALERT_SEVERITY_LOW		1
#define TMS_ALERT_SEVERITY_MEDIUM	2
#define TMS_ALERT_SEVERITY_HIGH		3

/** The application can call this function to generate an application based alert on the server
*
* @param[in] description = A short description of the alert (MAX 256 characters)
* @param[in] severity = The severity of the alert (valid values are TMS_ALERT_SEVERITY_LOW, TMS_ALERT_SEVERITY_MEDIUM or TMS_ALERT_SEVERITY_HIGH)
* @param[in] details = Optional string to provide details about the alert to the server (No enforced maximum but 4096 characters is the recommended limit)
*
* @return struct tmsReturn = a tmsReturn structure<BR>
*					tmsReturn.status = the immediate library status response.  TMS_STATUS_REQUESTED
*					indicates the library was able to successfully send the alert request
*					to the VHQ Agent.  All other return values indicate a failure.<BR><BR>
*					tmsReturn.handle = the handle used for further status tracking in the callback.
*					The callback will provide further status including the status of sending the alert to the server
*
*/
/*SVC_PROTOTYPE*/ TMS_API struct tmsReturn tms_sendApplicationAlert(char* description /*REQ*/, int severity /*REQ*/, char* details /*REQ*/);




/** This function is called by the application to request information about POS Agent information. Has processing only under Verix OS
* file.
*
* @param[in] buffer = pointer to buffer for result
* @param[in] size = size of mentioned buffer
*
* @return struct tmsReturn = a tmsReturn structure<BR>
*					tmsReturn.status = the blocking library status response.  TMS_STATUS_REQUESTED
*					indicates the library was able to successfully receive POS Agent information.
*					All other return values indicate a failure.<BR><BR>
*					tmsReturn.handle = the handle used for further status tracking in the callback.
*					The callback is ignored in this case.
*
*/
TMS_API struct tmsReturn tms_blgetPOSAgetnInfo(char * buffer, unsigned int size);

#ifdef __cplusplus
}

#endif
#endif //SVC_TMS_H
