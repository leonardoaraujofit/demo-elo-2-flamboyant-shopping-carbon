#ifndef ATSLIB_H
#define ATSLIB_H

/*****************************************************************************/
/**
 * \file atslib.h
 *
 * \author GSS R&D Germany
 *
 * Company: VeriFone GmbH
 *
 * Product: Verifone Automated Test System (VATS)
 *
 *       (c) Copyright 2013 - 2016
 */
/*****************************************************************************/

// *****************************************************************************
// P i p e  c o m m u n i c a t i o n :
// *****************************************************************************
//
//
// *****************************************************************************
//                  A T T E N T I T I O N !!!
//
// For VATS release > 1.2.0-rc3 the interprocess communication has been 
// changed. Now the IPC from GUI / PRT system is used. By means of this
// conflicts with pipe events and timers will be avoided. IPC is using
// separate threads.
//
// Instead of EVT_PIPE the application now has to wait for EVT_USER. The
// user_event_mask can be examined in detail by meas of read_user_event ().
// The user_event_mask has to be specified by the application.
// *****************************************************************************
//
//
// *****************************************************************************
// Please refer to:
//                  index.html
//                  page_Programmers_Guide_PC.html
//                  page_Programmers_Guide_Terminal.html
// *****************************************************************************


#ifdef _VOS
#include <svc.h>

#define VATS_SOCKET                "/tmp/vatsR"      // VX520 V/OS
#define VATS_SOCKET_GUI            "/tmp/vatsRGui"   // VX520 V/OS
#define VATS_SOCKET_LEN            6                 // max. message size (characters) on socket interface
#define ENV_SECTION                "reg"
#else
#define VATS_APP_PIPENAME          "VATSAppPipe"     // IPC
#define VATS_PRT_PIPENAME          "VATSPrtPipe"     // IPC
#endif                             

#define ENV_NAME                   "VATS_HDLR_VER"
#define PIPE_MAX_MSG_LEN           2000              // max. length for pipe buffer (printer test long data: 37)
#define VATS_PIPE_OVERHEAD         7                 // pipe overhead: event, etc.
#define APPNAME_MAX                21                // including \0
#define VER_LEN_MAX                32                // including \0
                                                     
#define VATS_IPC_PREFIX            0x164D5347        // SYN A T S
#define VATS_CHAINING_HEADER_SIZE  10                // ADKVATS-334: header size for chained messages PC -> terminal

/** VATS event IDs */
enum AtsEvent
{
  // general events
  ATSEVENT_NONE,                            /**< no event                                                                          */
  ATSEVENT_ANY,                             /**< an arbitrary event                                                                */
  ATSEVENT_ACK_ATSHANDLER,                  /**< internal: ACK sent from VATS Handler                                              */
  ATSEVENT_ACK_ATSLIB,                      /**< internal: ACK send from VATS Library                                              */
  ATSEVENT_IDENTIFICATION,                  /**< internal: identification of APP  towards VATS Handler (VOS only)                  */
  ATSEVENT_REGISTER_EVENT,                  /**< internal: registration of events towards VATS Handler (VOS only)                  */
  ATSEVENT_LIBVERSION,                      /**< internal: VATS library version to be able to check the versions of the components */
  ATSEVENT_VATSOS_STATUS,                   /**< internal: determine if VATSOS is activer (VOS only)                               */
  ATSEVENT_PRTLIBVERSION,                   /**< internal: VATS library version of HTML PRT to be able to check the versions of the components (ADKVATS-382) */
  ATSEVENT_GUIPRTVERSION,                   /**< internal: version of GUIPRT server (ADKVATS-401)                                  */
  ATSEVENT_EMV_ACTIVE,                      /**< internal: EMV application is active now (ADKVATS-442)                             */
  ATSEVENT_EMV_ACTIVE_RESP,                 /**< internal: response on EMV application is active now (ADKVATS-442)                 */

  // standard events: terminal -> PC
  ATSEVENT_TERMINAL_DISPLAY       = 100,    /**< text display                           */
  ATSEVENT_TERMINAL_PRINTER,                /**< text printout                          */
  ATSEVENT_TERMINAL_TRACE,                  /**< trace output                           */
  ATSEVENT_TERMINAL_DISPLAY_HTML,           /**< HTML display                           */
  ATSEVENT_TERMINAL_PRINT_HTML,             /**< HTML printout                          */
  ATSEVENT_TERMINAL_DISPLAY_REGIONS,        /**< HTML display regions                   */
  ATSEVENT_TERMINAL_DISPLAY_SNAPSHOT,       /**< display snapshot data                  */
  ATSEVENT_TERMINAL_EMV_CT_RECORDING,       /**< EMV: record CT responses/callbacks     */
  ATSEVENT_TERMINAL_EMV_CTLS_RECORDING,     /**< EMV: record CTLS responses/callbacks   */
  ATSEVENT_TERMINAL_EMV_CT_TAGS_RESULT,     /**< EMV: response of CT fetch tag          */
  ATSEVENT_TERMINAL_EMV_CTLS_TAGS_RESULT,   /**< EMV: response of CTLS fetch tag        */

  // standard events: PC -> terminal
  ATSEVENT_USER_KEYBOARD          = 200,    /**< keyboard press                         */
  ATSEVENT_USER_MAGSTRIPE,                  /**< magstripe card swiped                  */
  ATSEVENT_USER_ICCCARD_IN,                 /**< card inserted in card slot             */
  ATSEVENT_USER_TOUCH,                      /**< touchscreen press / release            */
  ATSEVENT_HTML_DISPLAY_RESULT,             /**< HTML display result data               */
  ATSEVENT_HTML_PRINT_RESULT,               /**< HTML print result data                 */
  ATSEVENT_EMV_CT_DATA,                     /**< EMV CT response data                   */
  ATSEVENT_EMV_CTLS_DATA,                   /**< EMV CTLS response data                 */
  ATSEVENT_COM_ERROR_DATA,                  /**< COM error data                         */
  ATSEVENT_GUI_SNAPSHOT,                    /**< take a display snapshot                */
  ATSEVENT_EMV_CT_TAGS,                     /**< request EMV CT tags                    */
  ATSEVENT_EMV_CTLS_TAGS,                   /**< request EMV CTLS tags                  */
  ATSEVENT_CHAINED_DATA,                    /**< chained data, real event type inside the message (ADKVATS-334) */

  // ID range for customized events
  ATSEVENT_CUSTOM_START           = 1000,   /**< customized events: ID must be greater than this value  */
  ATSEVENT_CUSTOM_END             = 65535   /**< customized events: ID must be lower than this value    */
  // Do not assign a new enum value behind ATSEVENT_CUSTOM_END because ID is casted to
  // unsigned short on sending over pipe
};

/** VATS error codes */
enum AtsError
{
   ATSERROR_OK                     = 0,   /**< no error                       */
   ATSERROR_INIT_FAILED,                  /**< initialization error           */
   ATSERROR_INVALID_PARAMETER,            /**< wrong input parameter detected */
   ATSERROR_FAILED,                       /**< general error                  */
   ATSERROR_BUFFER_TOO_SMALL,             /**< given buffer is too small      */
   ATSERROR_TIMEOUT,                      /**< timeout expired                */
   ATSERROR_WAITING,                      /**< task has to wait for response  */
   ATSERROR_NO_DATA,                      /**< no data available              */
   ATSERROR_NOT_ENABLED,                  /**< request not enabled            */
   ATSERROR_NOT_ACTIVE                    /**< VATS is not active             */
};

// combination of events is supported - registration from another task overwrites
// previous registrations
#define ATSREG_NONE        ((int)(0x00000000))      /**< no event                                               */
#define ATSREG_GUI         ((int)(0x00000001))      /**< GUI events                                             */
#define ATSREG_PRINTER     ((int)(0x00000002))      /**< HTML printer event                                     */
#define ATSREG_KEYBOARD    ((int)(0x00000004))      /**< keyboard press                                         */
#define ATSREG_MAGSTRIPE   ((int)(0x00000008))      /**< magstripe card swiped                                  */
#define ATSREG_ICCCARD_IN  ((int)(0x00000010))      /**< card inserted in card slot                             */
#define ATSREG_USER_TOUCH  ((int)(0x00000020))      /**< touchscreen press                                      */
#define ATSREG_CUSTOM      ((int)(0x00000040))      /**< customized events                                      */
#define ATSREG_EMV_CT      ((int)(0x00000080))      /**< EMV contact data messages                              */
#define ATSREG_EMV_CTLS    ((int)(0x00000100))      /**< EMV contactless data messages                          */
#define ATSREG_COM_ERROR   ((int)(0x00000200))      /**< COM error messages                                     */
#define ATSREG_LAST        (ATSREG_COM_ERROR + 1)   /**< last value to be able to recognize the end of the list */

#define ATS_CHAINED_EVENT_CHUNK_TIMOUT    (2000)    // milliseconds

struct AtsPipeRegister
{
   int  taskID;                 // application --> VATS Handler
   int  DQDriverActive;         // application <-- VATS Handler
   char appname [APPNAME_MAX];  // application --> VATS Handler
   int  dispCharWidth;          // application <-- VATS Handler
   int  dispCharHeight;         // application <-- VATS Handler
   int  regEvents;              // application --> VATS Handler
};

// =================================================================================================
// === Functions
// =================================================================================================

#ifdef __cplusplus
extern "C" {
#endif

/** Ats is active
 *  This function returns an indication whether VATS is active or not.
 * \return  0=inactive, 1=active */
int AtsIsActive (void);


#ifndef _VOS
/** Initialize VATS Library
 *  This function registers the caller to the VATS handler.
 *  It has to be called for every task/thread that need to use VATS
 *  functions. During this process two pipes are created for sending events
 *  towards VATS handler and for receiving events from VATS handler.
 *
 *  Function has to be called on application or thread startup.
 * \param[in]  app_name      unique application name, zero terminated string, max. len: 20
 * \                         (VATS PC can identify the application)
 * \param[in]  taskId        task ID to which event mask has to be posted
 * \param[in]  eventMask     event mask to be posted to task ID
 * \return  ATSERROR_OK on success, else error */
int AtsInit (const char* app_name, int taskId, int eventMask);
#endif


#ifndef _VOS
/** Initialize VATS Library extended
 *  This function registers the caller to the VATS handler.
 *  It has to be called for every task/thread that need to use VATS
 *  functions. During this process two pipes are created for sending events
 *  towards VATS handler and for receiving events from VATS handler.
 *
 *  Function has to be called on application or thread startup.
 * \param[in]  app_name      unique application name, zero terminated string, max. len: 20
 * \                         (VATS PC can identify the application)
 * \param[in]  regEvents     bitmap of events that application registers for (these
 *                           events are handled by this application)
 * \param[in]  taskId        task ID to which event mask has to be posted
 * \param[in]  eventMask     event mask to be posted to task ID
 * \return  ATSERROR_OK on success, else error */
int AtsInitExt (const char* app_name, int regEvents, int taskId, int eventMask);
#endif


/** Initialize VATS Library on V/OS
 *  This function registers the caller to the VATS handler.
 *
 *  It has to be called for every task that need to use VATS functions.
 *  During this process a socket communication is established for sending
 *  events towards VATS handler and for receiving events from VATS handler.
 *
 *  Function has to be called on application or thread startup.
 * \param[in]  app_name      unique application name, zero terminated string, max. len: 20
 * \                         (VATS PC can identify the application)
 * \param[in]  regEvents     bitmap of events that application registers for (these
 *                           events are handled by this application)
 * \return  ATSERROR_OK on success, else error */
#ifdef DOXYGEN_SHOULD_USE_THIS
int AtsInitVOS (const char *app_name, int regEvents);
#endif  // #ifdef DOXYGEN_SHOULD_USE_THIS
#ifdef _VOS
int AtsInitVOS (const char *app_name, int regEvents);
#endif

/** Initialize VATS Library from GUI
 *  This function registers the caller (GUI system) to the VATS handler. A socket is
 *  created between GUI and VATS handler in order to exchange GUI events.
 *  This function has to be called on startup from GUI lib / GUI server.
 * \param[in]  taskId        task ID to which event mask has to be posted
 * \param[in]  eventMask     event mask to be posted to task ID
 * \param[out] FdGui         descriptor of the GUI socket
 * \param[in]  GUIVersion    version of guiprtservervats
 * \return  ATSERROR_OK on success, else error */
int AtsInitGui (int *FdGui, int taskId, int eventMask, char* GUIVersion);


/** Send VATS event
 *  This function sends the specified VATS event and data towards the VATS Handler.
 *  Reception has to be confirmed by an ACK event from VATS Handler.
 * \param[in]  event    event ID (see enum AtsEvent)
 * \param[in]  datalen  length of data [bytes]
 * \param[in]  data     buffer with data to be sent
 * \return  ATSERROR_OK on success, else error */
int AtsSendEvent (unsigned short event, int datalen, const void *data);


/** Send VATS display text
 *  This function uses AtsSendEvent () in order to handle the ATSEVENT_TERMINAL_DISPLAY event.
 *  The data to be transferred are prepended by x and y position.
 * \param[in]  datalen  length of data, size in bytes
 * \param[in]  data     buffer with data to be sent
 * \param[in]  xpos     display column x, 1 <= x <= 22
 * \param[in]  ypos     display line   y, 1 <= y <=  8
 * \return  ATSERROR_OK on success, else error */
int AtsSendDisplayText (int datalen, const void *data, unsigned char xpos, unsigned char ypos);


/** Send VATS trace
 *  This function sends trace data towards the VATS PC by means of the VATS Handler.
 * \param[in]  source        ID for source device (arbitrary value)
 * \param[in]  channel       ID for the channel (arbitrary value, could be e.g. 1=send, 2=receive)
 * \param[out] datalen       length of trace data in parameter data
 * \param[out] data          traced data to be sent
 * \return  ATSERROR_OK on success, else error */
int AtsTraceEvent (unsigned char source, unsigned char channel, int datalen, const void *data);


/** Receive VATS event
 *  This function tries to retrieve an event and data possibly sent by the VATS Handler.
 *  If a timeout value > 0 is specified the function waits for an event.
 * \param[out] event         received event ID (see enum AtsEvent)
 * \param[in]  timeout_msec  timeout in milliseconds, 0: don't wait for event, just read data
 *                           timeout > 0: wait up to timeout msecs for event
 * \param[in,out]   datalen  length of allocated buffer "data" / length of received data
 * \param[out] data          data read from pipe
 * \return  ATSERROR_OK on success, else error */
int AtsReceiveEvent (enum AtsEvent *event, int timeout_msec, int *datalen, void *data);


/** Register for event
 *  This Function registers an application for all events given by the parameter
 *  eventMask. If another task has already registered for such an event the new
 *  assignment becomes valid.
 * \return  ATSERROR_OK on success, else error */
#ifdef DOXYGEN_SHOULD_USE_THIS
int AtsRegisterForEvent (int eventMask);
#endif  // #ifdef DOXYGEN_SHOULD_USE_THIS
#ifdef _VOS
int AtsRegisterForEvent (int eventMask);
#endif


/** ATS server
 *  This function implements the server part of the VATS socket communication between VATS
 *  handler and VATS library.
 *
 *  This function is  N O T  to be called by an application.
 *
 * \param[out] listenFd      allocated descriptor of the socket
 * \param[out] listenFdGui   allocated descriptor of the socket towards GUI server
 * \return  ATSERROR_OK on success, else ATSERROR_INIT_FAILED, ATSERROR_FAILED */
#ifdef DOXYGEN_SHOULD_USE_THIS
int AtsServer (int *listenFd, int *listenFdGui);
#endif  // #ifdef DOXYGEN_SHOULD_USE_THIS
#ifdef _VOS
int AtsServer (int *listenFd, int *listenFdGui);
#endif


/** Send VATS event specific
 *  This function sends the specified event and data towards a specific socket descriptor.
 *
 *  This function is  N O T  to be called by an application.
 *
 * \param[in]  socketFd      socket file descriptor to be used
 * \param[in]  event         event ID (see enum AtsEvent)
 * \param[in]  datalen       length of data [bytes]
 * \param[in]  data          buffer with data to be sent
 * \param[in]  useSemaphore  flag to indicate whether a semaphore should be applied
 * \return  ATSERROR_OK on success, else ATSERROR_INIT_FAILED, ATSERROR_INVALID_PARAMETER,
 *          ATSERROR_TIMEOUT, ATSERROR_FAILED */
#ifdef DOXYGEN_SHOULD_USE_THIS
int AtsSendEventSpec (int socketFd, unsigned short event, int datalen, const void *data, unsigned char useSemaphore);
#endif  // #ifdef DOXYGEN_SHOULD_USE_THIS
#ifdef _VOS
int AtsSendEventSpec (int socketFd, unsigned short event, int datalen, const void *data, unsigned char useSemaphore);
#endif


/** Ats receive event specific
 *  This function tries to receive an event for the time given by parameter timeout_msec.
 *  If an event is received within this period of time the event and its data are returned.
 *
 *  This function is  N O T  to be called by an application.
 *
 * \param[in]  socketFd      socket descriptor to be used
 * \param[out] event         pointer to received event ID (see enum AtsEvent)
 * \param[in]  timeout_msec  timeout in milliseconds, 0: don't wait for an event, just read data
 *                           timeout > 0: wait up to timeout msecs for an event
 * \param[in,out]   datalen  length of allocated buffer "data" / length of received data
 * \param[out] data          pointer to received data
 * \param[in]  useSemaphore  flag to indicate whether a semaphore should be applied
 * \return  ATSERROR_OK on success, else error */
#ifdef DOXYGEN_SHOULD_USE_THIS
int AtsReceiveEventSpec (int socketFd, enum AtsEvent *event, int timeout_msec, int *datalen, void *data, unsigned char useSemaphore);
#endif  // #ifdef DOXYGEN_SHOULD_USE_THIS
#ifdef _VOS
int AtsReceiveEventSpec (int socketFd, enum AtsEvent *event, int timeout_msec, int *datalen, void *data, unsigned char useSemaphore);
#endif


/** Receive VATS event for GUI
 *  This function tries to retrieve an ATSEVENT_HTML_DISPLAY_RESULT event and data possibly
 *  sent by the VATS Handler. If a timeout value > 0 is given the function waits for an
 *  event according to the time specified. If a timeout value = 0 is given the function
 *  returns at once.
 *
 *  This function is  N O T  to be called by an application.
 *
 * \param[out] event         received event ID (see enum AtsEvent)
 * \param[in]  timeout_msec  timeout in milliseconds, 0: don't wait for pipe event, just read pipe data
 *                           timeout > 0: wait up to timeout msecs for pipe event
 * \param[in,out]   datalen  length of allocated buffer "data" / length of received data
 * \param[out] data          data read from pipe
 * \return  ATSERROR_OK on success, else error */
int AtsReceiveEventGui (enum AtsEvent *event, int timeout_msec, int *datalen, void *data);


/** Receive VATS printer event
 *  This function tries to retrieve an ATSEVENT_HTML_PRINT_RESULT sent by the VATS Handler.
 *  If a timeout value > 0 is specified the function waits for an event.
 *
 *  This function is  N O T  to be called by an application.
 *
 * \param[out] event         received event ID (see enum AtsEvent)
 * \param[in]  timeout_msec  timeout in milliseconds, 0: don't wait for event, just read data
 *                           timeout > 0: wait up to timeout msecs for event
 * \param[in,out]   datalen  length of allocated buffer "data" / length of received data
 * \param[out] data          data read from pipe
 * \return  ATSERROR_OK on success, else error */
int AtsReceiveEventPrinter (enum AtsEvent *event, int timeout_msec, int *datalen, void *data);


/** Send display HTML event
 *  This functions sends JSON data from the GUI system as an ATSEVENT_TERMINAL_DISPLAY_HTML
 *  event on the terminal towards the VATS PC by means of the VATS Handler.
 *
 *  This function is  N O T  to be called by an application.
 *
 * \param[in] data          string to be sent, i.e. JSON stuff ('\0' terminated)
 * \return  ATSERROR_OK on success, else error */
int AtsSendDisplayHtml (const char *data);


/** Send display regions change event
 *  This functions sends JSON data from the GUI system as an ATSEVENT_TERMINAL_DISPLAY_REGIONS
 *  event on the terminal towards the VATS PC by means of the VATS Handler.
 *
 *  This function is  N O T  to be called by an application.
 *
 * \param[in] data          string to be sent, i.e. JSON stuff ('\0' terminated)
 * \return  ATSERROR_OK on success, else error */
int AtsSendDisplayRegions (const char *data);


/** Send print HTML event
 *  This functions sends JSON data from the PRT system as an ATSEVENT_TERMINAL_PRINT_HTML
 *  event on the terminal towards the VATS PC by means of the VATS Handler.
 *
 *  This function is  N O T  to be called by an application.
 *
 * \param[in] data          string to be sent, i.e. JSON stuff ('\0' terminated)
 * \return  ATSERROR_OK on success, else error */
int AtsSendPrintHtml ( const char *data );

/** Ats VATSOS Active
 *  This function returns an indication whether VATSOS is active or not.
 * \return  0=inactive, 1=active */
#ifdef DOXYGEN_SHOULD_USE_THIS
bool AtsVATSOSActive (void);
#endif  // #ifdef DOXYGEN_SHOULD_USE_THIS
#ifdef _VOS // ADKVATS-149
bool AtsVATSOSActive (void);
#endif

/** Send chained event with ID
 *  This functions sends string data with the desired event code and the
 *  desired ID towards the VATS Handler.
 *
 *  This function is  N O T  to be called by an application.
 *
 * \param[in] usEvent  event code, e.g. ATSEVENT_TERMINAL_DISPLAY_SNAPSHOT
 * \param[in] ulId     unique ID for the chained message (has to be changed after each
 *                     message to be able to differentiate between two succeeding messages)
 * \param[in] ulLength string length (without the terminating '\0')
 * \param[in] cData    string to be sent ('\0' terminated)
 * \return  ATSERROR_OK on success, else error */
int sendChainedEventWithId( unsigned short usEvent, unsigned long ulId, unsigned long ulLength, const char *cData );

/** ADK version info API
 *  Each ADK component needs to provide a \<component>_GetVersion() API.
 *  The format of the version info needs to follow the common numbering scheme:
 *  \<major>.\<minor>.\<patch>-\<build>; e.g. 1.2.3-4
 *
 * \return const char* with version info of the linked library */
const char *vats_GetVersion( void );

/** ADK version info API
 *  Each ADK component needs to provide a \<component>_GetSvcVersion() API.
 *  The format of the version info needs to follow the common numbering scheme:
 *  \<major>.\<minor>.\<patch>-\<build>; e.g. 1.2.3-4
 *
 * \return const char* with version info of the service, resp. demon */
const char *vats_GetSvcVersion( void );

/** Send VATS event chained
 *  This function sends the specified event and data towards a specific socket descriptor or pipe.
 *
 *  This function is  N O T  to be called by an application.
 *
 * \param[in]  p             V/OS: socket file descriptor to be used
 *                           VERIX: pipe
 * \param[in]  event         event ID (see enum AtsEvent)
 * \param[in]  datalen       length of data [bytes]
 * \param[in]  data          buffer with data to be sent
 * \param[in]  useSemaphore  flag to indicate whether a semaphore should be applied (V/OS only)
 * \param[in]  curLibVers    current version of VATS library according to registration
 * \return  ATSERROR_OK on success, else ATSERROR_INIT_FAILED, ATSERROR_INVALID_PARAMETER,
 *          ATSERROR_TIMEOUT, ATSERROR_FAILED */
int AtsSendEventChained (void *p, unsigned short event, unsigned long datalen, const void *data, unsigned char useSemaphore, const char *curLibVers);

/** Receive VATS event chained
 *  This function tries to retrieve an event and data possibly sent by the VATS Handler.
 *  The data might be sent within a single message or it might consist of several chunks.
 *  In this case the complete message is assembled by this function.
 *
 *  !!! The memory allocated to store the message has to be released by the caller !!!
 *
 *  If a timeout value > 0 is specified the function waits for an event.
 * \param[out] event         received event ID (see enum AtsEvent)
 * \param[in]  timeout_msec  timeout in milliseconds, 0: don't wait for event, just read data
 *                           timeout > 0: wait up to timeout msecs for event
 *                           Note: In the case the first chunk of a chained message was received
 *                                 within the timeout, internally the timeout is increased to
 *                                 ATS_CHAINED_EVENT_CHUNK_TIMOUT for all subsequent chunks to
 *                                 ensure all chunks of a chained message will be received
 *                                 successfully.
 * \param[in,out]   datalen  length of received data
 * \param[out] data          pointer to the address of received data
 * \return  ATSERROR_OK on success, else error */
int AtsReceiveEventChained (enum AtsEvent *event, int timeout_msec, int *datalen, void **data);

#ifdef __cplusplus
}  // extern "C"
#endif

#endif // ATSLIB
