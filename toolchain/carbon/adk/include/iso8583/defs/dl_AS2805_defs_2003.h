/* Created for Bendigo support*/

/******************************************************************************/
/*                                                                            */
/* dl_AS2805_defs_2003.h - AS2805 2003 Definitions                          */
/*                                                                            */
/******************************************************************************/

#ifndef __INC_DL_AS2805_DEFS_2003
#define __INC_DL_AS2805_DEFS_2003

#include "../dl_base.h"
#include "../dl_iso8583_common.h"

/******************************************************************************/

// sets the specified handler
void DL_AS2805_DEFS_2003_GetHandler ( DL_ISO8583_HANDLER *oHandler );

/******************************************************************************/

#endif /* __INC_DL_ISO8583_DEFS_2003 */
