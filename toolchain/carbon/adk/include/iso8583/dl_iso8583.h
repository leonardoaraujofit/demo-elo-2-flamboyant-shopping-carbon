/******************************************************************************/
/*                                                                            */
/* Copyright (C) 2005-2007 Oscar Sanderson                                    */
/*                                                                            */
/* This software is provided 'as-is', without any express or implied          */
/* warranty.  In no event will the author(s) be held liable for any damages   */
/* arising from the use of this software.                                     */
/*                                                                            */
/* Permission is granted to anyone to use this software for any purpose,      */
/* including commercial applications, and to alter it and redistribute it     */
/* freely, subject to the following restrictions:                             */
/*                                                                            */
/* 1. The origin of this software must not be misrepresented; you must not    */
/*    claim that you wrote the original software. If you use this software    */
/*    in a product, an acknowledgment in the product documentation would be   */
/*    appreciated but is not required.                                        */
/*                                                                            */
/* 2. Altered source versions must be plainly marked as such, and must not be */
/*    misrepresented as being the original software.                          */
/*                                                                            */
/* 3. This notice may not be removed or altered from any source distribution. */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* dl_iso8583.c - An implementation of the ISO-8583 message protocol          */
/*                                                                            */
/******************************************************************************/

#ifndef __INC_DL_ISO8583
#define __INC_DL_ISO8583

#include "dl_base.h"
#include "dl_err.h"
#include "dl_str.h"

#include "ConstData.h"
#include "SafeBuffer.hpp"

#include "dl_iso8583_common.h"
#include "dl_iso8583_fields.h"




class ISO8583_MSG
{
public:
    /******************************************************************************/

// Initialises an ISO8583 message
// iMsgHandler - ISO8583 Msg handler instance

// returns: none
    ISO8583_MSG ( DL_ISO8583_MSG_HANDLER *iMsgHandler,
                  DL_UINT8       *_iStaticBuf = NULL,
                  DL_UINT16       _iStaticBufSize = 0);

// Frees initialised ISO8583 message, de-allocating memory as req
// returns: none
    virtual ~ISO8583_MSG ( );

    /******************************************************************************/

// allocates and sets the specified (string) field
// NB iField range is 0..kDL_ISO8583_MAX_FIELD_IDX
// returns: error code
    DL_ERR SetField_Str ( DL_UINT16       iField,
                          const DL_SINT8 *iDataStr );

// allocates and sets the specified field
// NB iField range is 0..kDL_ISO8583_MAX_FIELD_IDX
// NB iDataLen indicates the length of the data (0..n)
// NB iData can be NULL if iDataLen is 0
// returns: error code
    DL_ERR SetField_Bin ( DL_UINT16       iField,
                          const DL_UINT8 *_iData,
                          DL_UINT16       _iDataLen );

// allocates and sets the specified fields
// NB tlvFields
// returns: error code
    DL_ERR SetFields_TLV ( com_verifone_TLVLite::ConstData_t tlvFields );
                          
    /******************************************************************************/

// NB iField range is 0..kDL_ISO8583_MAX_FIELD_IDX
// returns: 1 if the field is set / 0 otherwise
    int HaveField ( DL_UINT16             iField);

    /******************************************************************************/

// NB iField range is 0..kDL_ISO8583_MAX_FIELD_IDX
// outputs: oPtr - static pointer to field data
// returns: error code
    DL_ERR GetField_Str ( DL_UINT16              iField,
                          DL_UINT8             **oPtr );

// NB iField range is 0..kDL_ISO8583_MAX_FIELD_IDX
// outputs: oPtr     - static pointer to field data
//			oByteLen - byte length of field data
// returns: error code
    DL_ERR GetField_Bin ( DL_UINT16              iField,
                          DL_UINT8             **oPtr,
                          DL_UINT16             *oByteLen );

// NB Gets back data in TLV format
// outputs: tlvFields
// returns: error code
    DL_ERR GetFields_TLV ( com_verifone_TLVLite::SafeBuffer & tlvFields );
                          
    /******************************************************************************/

// NB Pack ISO message 
// returns: error code
    DL_ERR Pack ( DL_UINT8                 *ioByteArr,
                  DL_UINT16                ioByteArrSize,
                  DL_UINT16                *oNumBytes,
                  DL_UINT16                *errFieldIdx=NULL );

// NB Unpack ISO message 
// returns: error code
    DL_ERR Unpack ( const DL_UINT8           *iByteArr,
                    DL_UINT16                 iByteArrSize);

    /******************************************************************************/
#if defined(_VRXEVO) || defined(_VOS)
// outputs the contents of the ISO8583 message to the log
// returns: none
    void Dump ( );

#else
// outputs the contents of the ISO8583 message to the specified file
// inputs : iOutFile - output stream
//          _iEolStr - EOL terminator string, defaults to '\n' if NULL
// returns: none
    void Dump ( FILE                     *iOutFile,
                const char               *_iEolStr);
#endif

    /******************************************************************************/

private:
    DL_ISO8583_MSG iMsg;
    
    DL_ISO8583_MSG_HANDLER *iMsgHandler;

};

#endif /* __INC_DL_ISO8583 */
