/**
  * @file logapi_common.h
 * @note Define <b>LOGAPI_ENABLE_DEBUG</b> to enable DBG*() macros
 *
 * Public API:
 *     - LOGAPI_ENABLE_DEBUG - define macro in preprocessor to enable DEBUG level
 *
 *   Initialization:
 *     - LOGAPI_INIT( channel )
 *     - LOGAPI_DEINIT( )
 *
 *   Set maximal log level
 *     - LOGAPI_SETLEVEL( level )
 *
 *   Misc. API
 *    - LOGAPI_DUMP_SYS_INFO()
 */
#ifndef _LOGAPI_COMMON_H_
#define _LOGAPI_COMMON_H_

#ifdef _VRXEVO
#   ifndef LOG_API
#       ifdef _LOG_API_EXPORT
#           define LOG_API __declspec(dllexport)
#       else
#           define LOG_API __declspec(dllimport)
#       endif
#   endif
#else
#   define LOG_API
#endif

#ifdef _VRXEVO
#   ifndef LOG_STREAM_API
#       ifdef _LOG_STREAM_API_EXPORT
#           define LOG_STREAM_API __declspec(dllexport)
#       else
#           define LOG_STREAM_API __declspec(dllimport)
#       endif
#   endif
#else
#   define LOG_STREAM_API
#endif

#if defined( __GNUC__ ) || defined( __GNUG__ )
#   define LOGAPI_GCC

#   define LOGAPI_GCC_ATTR( x ) __attribute__(x)
#else
#   define LOGAPI_GCC_ATTR( x )
#endif

#ifndef _VRXEVO
#   define LOGAPI_PUBLIC LOGAPI_GCC_ATTR(( visibility("default") ))
#else
#   define LOGAPI_PUBLIC LOG_API
#endif

#ifndef _VRXEVO
#   define LOGSTREAM_API_PUBLIC LOGAPI_GCC_ATTR(( visibility("default") ))
#else
#   define LOGSTREAM_API_PUBLIC LOG_STREAM_API
#endif

#if defined( __cplusplus )
extern "C" {
#endif


// Message size 1K + placeholder for formatting
#define MAX_ARRIVING_MSG 1100

/**
 * @brief Sane limits
 */
enum
{
#ifdef _VRXEVO
    //syslog daemon will cut messages in a proper way
    LOGAPI_MSG_MAX_LEN = MAX_ARRIVING_MSG, 
    LOGAPI_ENV_NAME_MAX_LEN = 40,
#else
    //syslog does not cut messages, so high-level API should take care of that
    //currently busybox restricts messages to 250 chars
    LOGAPI_MSG_MAX_LEN = 150, 
    LOGAPI_CONFIG_NAME_MAX_LEN = 40,
#endif
    LOGAPI_FILTER_MAX_LEN = 255,
    LOGAPI_CHANNEL_MAX_LEN = 32,
    LOGAPI_SOURCE_MAX_LEN = 16,
};

#define LOGAPI_FILE __FILE__
#define LOGAPI_LINE __LINE__

/* Required due to concern raised in certain unsafe coding style situations */
#define LOGAPI_EMPTYSTMT do {} while(0)

#ifndef _LIBLOG_GET_VER
#   define _LIBLOG_GET_VER
/**
  * @brief returns release version of a library 
  */
    LOGAPI_PUBLIC const char * log_getVersion(void);
#endif

/*
* Message log API
*/

/**
 * @brief Basic Logging API levels with strictly defined IDs
 * @note In most cases, use DBG*_ERROR() ...DBG*_TRACE() macros
 * @see severity levels in syslog.h
 */
typedef enum
{
    LOGAPI_OFF = 0,
    LOGAPI_EMERG = 1,	
    LOGAPI_ALERT = 2,	
    LOGAPI_CRIT = 3,	
    LOGAPI_ERROR = 4,
    LOGAPI_WARN = 5,	
    LOGAPI_NOTICE = 6,	
    LOGAPI_INFO = 7,	
    LOGAPI_TRACE = 8	
} LogAPI_Levels;


/**
 * @brief Low level message data
 * @note It's designed for easy extensions and backward compatibility in shared libraries
 *
 */
typedef struct
{
    const char *msg; /**< Pre-formatted message of LOGAPI_MSG_MAX_LEN characters max + 1 for zero termination */
    unsigned int msg_len; /**< don't depend on zero termination here */
    
    const char *channel; /**< LOGAPI_CHANNEL_MAX_LEN characters max + 1 for zero termination */
    
    const char *source_file; /**< Use LOGAPI_FILE macro here */
    unsigned int source_line; /**< Use LOGAPI_LINE macro here */
    /** @note "source_file:source_line" is truncated to last LOGAPI_SOURCE_MAX_LEN characters */
    
    LogAPI_Levels log_level; /**< Log level */
    
    unsigned int struct_end_indicator; /**< Should always be 0, unless the structure is extended */
} LogAPI_MessageData;


/**
 * @brief Platform-specific message logging implementation
 * @warning Use DBGF_*() or DBGS_*() macros
 */
LOGAPI_PUBLIC void LogAPI_os_message( const LogAPI_MessageData* msg_data );


/**
  * @brief Message destination
  */
enum LOGAPI_DESTINATION
{
    /** 
      * @brief outputs to console
      * @details on V/OS messages would go to <b>stderr</b>.
      * On Verix they would go to internal Verix buffer
      * @warning in order for this to work on Verix, <b>*DEBUG</b> environment variable must be set to a valid serial port
     */
    LOGAPI_CONSOLE = 0x1,

    /** @brief outputs to syslog daemon
      * @note this is default value
     */
    LOGAPI_SYSLOG = 0x2,
    /** @brief outputs both to syslog daemon and console */
    LOGAPI_ALL = LOGAPI_CONSOLE | LOGAPI_SYSLOG,
};

/**
  * @brief Sets destination of LogAPI
  * @note @see LOGAPI_DESTINATION
  * @note use LOGAPI_SETOUTPUT macro
  */
LOGAPI_PUBLIC void LogAPI_setoutput(enum LOGAPI_DESTINATION);

/** @brief Public macro for calling LogAPI_setoutput */
#define LOGAPI_SETOUTPUT( dest ) \
    LogAPI_setoutput( dest )

/*
* Configure log API
*/

/**
 * @brief Platform-specific initialization
 * @warning Do not use directly, not exported
 * @note Must be protected inside from repeated calls
 */
void LogAPI_os_system_init( void );

/**
 * @brief Initialize logging library with a specified channel
 * @param channel Application name or anything meaningful of up to LOGAPI_CHANNEL_MAX_LEN
 * @note will prepend FILE:NUMBER to a message
 * @warning Use LOGAPI_INIT() macro
 */
LOGAPI_PUBLIC void LogAPI_Init( const char *channel);

/**
 *@brief Macro to be used for initialization
 *@note will prepend FILE:NUMBER to a message
 *@see LogAPI_Init
*/
#define LOGAPI_INIT( channel) \
    LogAPI_Init( channel)



/**
 * @brief Platform-specific deinitialization
 */
void LogAPI_os_system_deinit( void );

/**
 * @brief Deinitialize logging library 
 * @warning Use LOGAPI_DEINIT() macro
 */
LOGAPI_PUBLIC void LogAPI_Deinit(void);

/**
 *@brief Macro to be used for deinitialization
 *@see LogAPI_Deinit
*/
#define LOGAPI_DEINIT( ) \
    LogAPI_Deinit( )

typedef enum
{
    /** Turn off verbose mode. The same effect as if LOGAPI_SET_VERBOSITY() would not get called at all*/
    LOGAPI_VERB_OFF = 0x0,
    /** Prepend FILE:LINE to a log message */
    LOGAPI_VERB_FILE_LINE = 0x1,
    /** Prepend [PROCESS ID] to a log message */
    LOGAPI_VERB_PID = 0x2,
    /** Prepend [THREAD ID] to a log message */
    LOGAPI_VERB_TID = 0x4,
} LogAPI_Verbosity;

/**
 *@brief Sets verbosity of log messages
 *@param verbosity can be either of
 - LOGAPI_VERB_OFF 
 - LOGAPI_VERB_FILE_LINE 
 - LOGAPI_VERB_PID 
 *@see LogAPI_Verbosity
 *@attention This function must be called strictly before LOGAPI_INIT() to work correctly
*/
LOGAPI_PUBLIC void LogAPI_Set_Verbosity(int verbosity);


/**
 *@brief Macro to be used to call LogAPI_Set_Verbosity()
*/
#define LOGAPI_SET_VERBOSITY(verbosity) \
    LogAPI_Set_Verbosity(verbosity)

/**
 * @brief Get current log channel
 */
LOGAPI_PUBLIC const char *LogAPI_GetChannel( void );

/**
 * @brief Set log level filter
 * @warning Use LOGAPI_SETLEVEL() macro
 */
LOGAPI_PUBLIC void LogAPI_SetLevel( LogAPI_Levels log_level );

#define LOGAPI_SETLEVEL( level ) \
    LogAPI_SetLevel( level )

/**
 * @brief Get current filter's log level
 */
LOGAPI_PUBLIC LogAPI_Levels LogAPI_GetLevel( void );



/**
* Misc. API
**/

/**
 * @brief Expand to statement, if LogAPI debug is enabled
 */
#if defined( LOGAPI_ENABLE_DEBUG )
#   define LOGAPI_WHEN_DEBUG( stmt )    stmt
#else
/** @warning don't use LOGAPI_EMPTYSTMT as this macro can be used in very hackish
 * form like "if ( LOGAPI_WHEN_DEBUG( test1 && ) test2 )"*/
#   define LOGAPI_WHEN_DEBUG( stmt )
#endif

/**
 * @brief Platform-specific system information dump implementation
 * @warning Use LOGAPI_DUMP_SYS_INFO() macro
 */
LOGAPI_PUBLIC void LogAPI_os_dump_sysinfo( LogAPI_Levels log_level );

/**
 * @brief Dump system information at info level
 */
#define LOGAPI_DUMP_SYS_INFO() \
    LogAPI_os_dump_sysinfo( LOGAPI_INFO )

/**
 * @brief Platform-specific callstack dump implementation
 * @warning Use LOGAPI_DUMP_SYS_INFO() macro
 */
LOGAPI_PUBLIC void LogAPI_os_print_callstack( LogAPI_Levels log_level );

/**
 * @brief Dump callstack 
 */
#define LOGAPI_PRINT_CALLSTACK( level ) \
    LogAPI_os_print_callstack( level )

/**
 * @brief Platform-specific callstack dump implementation
 * @warning Use LOGAPI_DUMP_SYS_INFO() macro
 */
LOGAPI_PUBLIC void LogAPI_VerboseAbort( LogAPI_Levels log_level );

/**
 * @brief Dump callstack 
 */
#define LOGAPI_VERBOSE_ABORT() \
    LogAPI_VerboseAbort( LOGAPI_INFO )

/**
 * @brief Get Syslog daemon version
 */
LOGAPI_PUBLIC const char *Log_GetSvcVersion(void);

/**
 * @brief Set semicolon delimited filtering tags
 */
LOGAPI_PUBLIC void LogAPI_Set_FilterList( const char * filter_list );

#define LOGAPI_SETFILTERLIST( filter_list ) \
        LogAPI_Set_FilterList( filter_list )


/**
 * @brief Check internal filter for tag presence
 */
LOGAPI_PUBLIC int LogAPI_IsInFilterList( const char * tag );


#if defined( __cplusplus )
} // extern "C" 
#endif

#endif //_LOGAPI_COMMON_H_
