/**
 * @file logapi_printf.h
 * @note Define <b>LOGAPI_ENABLE_DEBUG</b> to enable DBG*() macros
 * @note Enable <b>-Wall</b> for GCC compiler to check formatting
 *
 * Public API:
 *   printf()-like message log
 *    - LOGF_EMERG(format, ...)
 *    - LOGF_ALERT(format, ...)
 *    - LOGF_CRIT(format, ...)
 *    - LOGF_ERROR(format, ...)
 *    - LOGF_WARN(format, ...)
 *    - LOGF_NOTICE(format, ...)
 *    - LOGF_INFO(format, ...)
 *    - LOGF_TRACE(format, ...)
 * 
 *    - DBGF_EMERG(format, ...)
 *    - DBGF_ALERT(format, ...)
 *    - DBGF_CRIT(format, ...)
 *    - DBGF_ERROR(format, ...)
 *    - DBGF_WARN(format, ...)
 *    - DBGF_NOTICE(format, ...)
 *    - DBGF_INFO(format, ...)
 *    - DBGF_TRACE(format, ...)
 *
 *    - LOGF_TAG_EMERG(tag,format, ...)
 *    - LOGF_TAG_ALERT(tag,format, ...)
 *    - LOGF_TAG_CRIT(tag,format, ...)
 *    - LOGF_TAG_ERROR(tag,format, ...)
 *    - LOGF_TAG_WARN(tag,format, ...)
 *    - LOGF_TAG_NOTICE(tag,format, ...)
 *    - LOGF_TAG_INFO(tag,format, ...)
 *    - LOGF_TAG_TRACE(tag,format, ...)
 * 
 *    - DBGF_TAG_EMERG(tag,format, ...)
 *    - DBGF_TAG_ALERT(tag,format, ...)
 *    - DBGF_TAG_CRIT(tag,format, ...)
 *    - DBGF_TAG_ERROR(tag,format, ...)
 *    - DBGF_TAG_WARN(tag,format, ...)
 *    - DBGF_TAG_NOTICE(tag,format, ...)
 *    - DBGF_TAG_INFO(tag,format, ...)
 *    - DBGF_TAG_TRACE(tag,format, ...)      
 */
#ifndef _LOGAPI_PRINTF_H_
#define _LOGAPI_PRINTF_H_

#include "logapi_common.h"

#if defined( __cplusplus )
extern "C"
{
#endif

#include "stdarg.h"

/**
 * @brief Log variable argument helper
 * @note Should be use in legacy logging API wrappers only
 */
LOGAPI_PUBLIC void LogAPI_vprintf(
    LogAPI_Levels log_level, const char *file, unsigned int line,
    const char* format, va_list args
);

/**
 * @brief Log message in printf-like style
 * @warning Avoid direct use
 */
LOGAPI_PUBLIC void LogAPI_printf(
    LogAPI_Levels log_level, const char *file, unsigned int line,
    const char* format, ...
) LOGAPI_GCC_ATTR(( format( printf, 4, 5 ) ));

#   define LOGAPI_PRINTF( level, ... ) \
        LogAPI_printf( level, LOGAPI_FILE, LOGAPI_LINE, __VA_ARGS__ )

#   define LOGF_EMERG( ... )  LOGAPI_PRINTF( LOGAPI_EMERG, __VA_ARGS__ )
#   define LOGF_ALERT( ... ) LOGAPI_PRINTF( LOGAPI_ALERT, __VA_ARGS__ )
#   define LOGF_CRIT( ... ) LOGAPI_PRINTF( LOGAPI_CRIT, __VA_ARGS__ )
#   define LOGF_ERROR( ... ) LOGAPI_PRINTF( LOGAPI_ERROR, __VA_ARGS__ )
#   define LOGF_WARN( ... ) LOGAPI_PRINTF( LOGAPI_WARN, __VA_ARGS__ )
#   define LOGF_NOTICE( ... ) LOGAPI_PRINTF( LOGAPI_NOTICE, __VA_ARGS__ )
#   define LOGF_INFO( ... ) LOGAPI_PRINTF( LOGAPI_INFO, __VA_ARGS__ )
#   define LOGF_TRACE( ... ) LOGAPI_PRINTF( LOGAPI_TRACE, __VA_ARGS__ ) 

/**
 * @brief Log message in printf-like style
 * @warning Avoid direct use
 */
LOGAPI_PUBLIC void LogAPI_tag_printf(
    LogAPI_Levels log_level, const char * tag, const char *file, unsigned int line,
    const char* format, ...
) LOGAPI_GCC_ATTR(( format( printf, 5, 6 ) ));
 
#   define LOG_CONVERT_TAG( x ) (";" x ";")

#   define LOGAPI_TAG_PRINTF( level, tag, ... ) \
        LogAPI_tag_printf( level, LOG_CONVERT_TAG( tag ), LOGAPI_FILE, LOGAPI_LINE, __VA_ARGS__ )

#   define LOGF_TAG_EMERG( tag, ... )  LOGAPI_TAG_PRINTF( LOGAPI_EMERG, tag, __VA_ARGS__ )
#   define LOGF_TAG_ALERT( tag, ... )  LOGAPI_TAG_PRINTF( LOGAPI_ALERT, tag, __VA_ARGS__ )
#   define LOGF_TAG_CRIT( tag, ... )   LOGAPI_TAG_PRINTF( LOGAPI_CRIT, tag, __VA_ARGS__ )
#   define LOGF_TAG_ERROR( tag, ... )  LOGAPI_TAG_PRINTF( LOGAPI_ERROR, tag, __VA_ARGS__ )
#   define LOGF_TAG_WARN( tag, ... )   LOGAPI_TAG_PRINTF( LOGAPI_WARN, tag, __VA_ARGS__ )
#   define LOGF_TAG_NOTICE( tag, ... ) LOGAPI_TAG_PRINTF( LOGAPI_NOTICE, tag, __VA_ARGS__ )
#   define LOGF_TAG_INFO( tag, ... )   LOGAPI_TAG_PRINTF( LOGAPI_INFO, tag, __VA_ARGS__ )
#   define LOGF_TAG_TRACE( tag, ... )  LOGAPI_TAG_PRINTF( LOGAPI_TRACE, tag, __VA_ARGS__ ) 
  
#if defined( LOGAPI_ENABLE_DEBUG )
    #define DBG_PRINTF LOGAPI_PRINTF
    #define DBGF_EMERG LOGF_EMERG	
    #define DBGF_ALERT LOGF_ALERT	
    #define DBGF_CRIT LOGF_CRIT	
    #define DBGF_ERROR LOGF_ERROR
    #define DBGF_WARN LOGF_WARN	
    #define DBGF_NOTICE LOGF_NOTICE	
    #define DBGF_INFO LOGF_INFO	
    #define DBGF_TRACE LOGF_TRACE 	

    #define DBG_TAG_PRINTF LOGAPI_TAG_PRINTF
    #define DBGF_TAG_EMERG LOGF_TAG_EMERG
    #define DBGF_TAG_ALERT LOGF_TAG_ALERT
    #define DBGF_TAG_CRIT LOGF_TAG_CRIT
    #define DBGF_TAG_ERROR LOGF_TAG_ERROR
    #define DBGF_TAG_WARN LOGF_TAG_WARN
    #define DBGF_TAG_NOTICE LOGF_TAG_NOTICE
    #define DBGF_TAG_INFO LOGF_TAG_INFO
    #define DBGF_TAG_TRACE LOGF_TAG_TRACE
#else
    #define DBG_PRINTF(...) LOGAPI_EMPTYSTMT
    #define DBGF_EMERG(...) LOGAPI_EMPTYSTMT	
    #define DBGF_ALERT(...) LOGAPI_EMPTYSTMT	
    #define DBGF_CRIT(...) LOGAPI_EMPTYSTMT	
    #define DBGF_ERROR(...) LOGAPI_EMPTYSTMT
    #define DBGF_WARN(...) LOGAPI_EMPTYSTMT	
    #define DBGF_NOTICE(...) LOGAPI_EMPTYSTMT	
    #define DBGF_INFO(...) LOGAPI_EMPTYSTMT	
    #define DBGF_TRACE(...) LOGAPI_EMPTYSTMT 	

    #define DBG_TAG_PRINTF(...) LOGAPI_EMPTYSTMT
    #define DBGF_TAG_EMERG(...) LOGAPI_EMPTYSTMT
    #define DBGF_TAG_ALERT(...) LOGAPI_EMPTYSTMT
    #define DBGF_TAG_CRIT(...) LOGAPI_EMPTYSTMT
    #define DBGF_TAG_ERROR(...) LOGAPI_EMPTYSTMT
    #define DBGF_TAG_WARN(...) LOGAPI_EMPTYSTMT
    #define DBGF_TAG_NOTICE(...) LOGAPI_EMPTYSTMT
    #define DBGF_TAG_INFO(...) LOGAPI_EMPTYSTMT
    #define DBGF_TAG_TRACE(...) LOGAPI_EMPTYSTMT
#endif

#if defined( __cplusplus )
} // extern "C"
#endif

#endif //_LOGAPI_PRINTF_H_
