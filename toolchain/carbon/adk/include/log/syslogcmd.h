/**
  * @file 
*/
#ifndef _SYSLOGCMD_H
#define _SYSLOGCMD_H

#include "logapi_common.h"

#if defined( __cplusplus )
extern "C"
{
#endif

/** codes returned by syslcmd_ API */
enum SYSL_ERROR
{
    /** operation succeeded */
    SYSL_E_OK = 0,

    /** one or more paramteres are invalid*/
    SYSL_E_PARAM = -1,

    /** no connection to daemon */
    SYSL_E_CONN = -2,

    /** unexpected error while executing a function*/
    SYSL_E_UNEXP = -3,

    /** not implemented */
    SYSL_E_NOT_IMPL = -99
};

/** Denotes a source of log messages*/
enum SYSL_SRC
{
    SSRC_VERIX_BUF = 0x2,
    SSRC_APPS = 0x4,
    SSRC_ALL = 0x6,
};

/** Enum denotes an upper level of log messages*/
enum SYSL_LVL
{
    SYSL_LVL_EMERG,	
    SYSL_LVL_ALERT,	
    SYSL_LVL_CRIT,	
    SYSL_LVL_ERROR,
    SYSL_LVL_WARN,	
    SYSL_LVL_NOTICE,	
    SYSL_LVL_INFO,	
    SYSL_LVL_TRACE 
};

enum config_option
{
    ENABLED = 0,
    DESTINATION = 1,
    SERIAL_COM = 2,
    UDP_HOST = 3,
    LOG_LEVEL = 4,
    UDP_PORT = 5,
    FILE_NAME = 6,
    FILE_BUF_SIZE = 7,
    LOG_SOURCE = 8,
    SEND_RETRIES = 9,
    MAX_LOGFILE_SIZE = 10,
    NUMBER_OF_FILES = 11,
    LOG_FORMAT = 12,
    FILTER_OUT_CONSOLE_LOGS = 13,
    MAX_ENUM_COUNT = 14,
};

enum COMMUNICATION
{
    COM,
    ETH,
    FS,
    USB
};

/** 
* @brief Sets level on syslog daemon side 
* @param[in] lvl upper level of log messages
*/
LOGAPI_PUBLIC enum SYSL_ERROR syslcmd_set_level(enum SYSL_LVL lvl);

/** 
* @briefSets source of log messages 
* @param[in] src source of log messages
*/
LOGAPI_PUBLIC enum SYSL_ERROR syslcmd_set_source(enum SYSL_SRC src);

/**
 * @brief Sets UPD as destination for messages 
 * @param[in] addr address of target host
 * @param[in] port port of target host
*/
LOGAPI_PUBLIC enum SYSL_ERROR syslcmd_set_dest_UDP(const char *addr, int port);

/** 
* @brief Sets serial as destination for messages 
* @param[in] serial_port COM port to use
*/
LOGAPI_PUBLIC enum SYSL_ERROR syslcmd_set_dest_COM(int serial_port);

/** 
* @brief Sets file as destination for messages 
* @param[in] filename file to write logs to
*/
LOGAPI_PUBLIC enum SYSL_ERROR syslcmd_set_dest_FILE(const char *filename);

/**
* @brief apply configuration to syslog daemon
*/
LOGAPI_PUBLIC enum SYSL_ERROR syslcmd_apply_config(void);

/**
* @brief Sets string options(UDP host and file name) on syslog daemon side
* @param[in] option configuration option to set
* @param[in] sval value to set
*/
LOGAPI_PUBLIC enum SYSL_ERROR syslcmd_set_cfg_str(enum config_option option, const char *sval);

/**
* @brief Sets integer options(others than UDP host and file name) on syslog daemon side
* @param[in] option configuration option to set
* @param[in] ival value to set
*/
LOGAPI_PUBLIC enum SYSL_ERROR syslcmd_set_cfg_int(enum config_option option, int ival);

/**
* @brief Returns string options(UDP host and file name) from syslog daemon side
* @param[in] option configuration option to get
* @param[out] sval configuration options value
*/
LOGAPI_PUBLIC enum SYSL_ERROR syslcmd_get_cfg_str(enum config_option option, char * sval);

/**
* @brief Returns integer options(others than UDP host and file name) on syslog daemon side
* @param[in] option configuration option to get
*/
LOGAPI_PUBLIC enum SYSL_ERROR syslcmd_get_cfg_int(enum config_option option, int *ival);

#if defined( __cplusplus )
}
#endif

#endif // _SYSLOGCMD_H
