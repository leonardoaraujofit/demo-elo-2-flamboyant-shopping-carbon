/**
  * @file logapi_hexdump.h
 * @note Define <b>LOGAPI_ENABLE_DEBUG</b> to enable DBG*() macros
 *
 *   Dump title + address + size, start separator HEX and ASCII representation, end separator
 *    - LOGAPI_HEXDUMP_EMERG(title, data, size)
 *    - LOGAPI_HEXDUMP_ALERT(title, data, size)
 *    - LOGAPI_HEXDUMP_CRIT(title, data, size)
 *    - LOGAPI_HEXDUMP_ERROR(title, data, size)
 *    - LOGAPI_HEXDUMP_WARN(title, data, size)
 *    - LOGAPI_HEXDUMP_NOTICE(title, data, size)
 *    - LOGAPI_HEXDUMP_INFO(title, data, size)
 *    - LOGAPI_HEXDUMP_TRACE(title, data, size)
 *    - DBG_HEXDUMP_EMERG(title, data, size)
 *    - DBG_HEXDUMP_ALERT(title, data, size)
 *    - DBG_HEXDUMP_CRIT(title, data, size)
 *    - DBG_HEXDUMP_ERROR(title, data, size)
 *    - DBG_HEXDUMP_WARN(title, data, size)
 *    - DBG_HEXDUMP_NOTICE(title, data, size)
 *    - DBG_HEXDUMP_INFO(title, data, size)
 *    - DBG_HEXDUMP_TRACE(title, data, size)
 *
 *   HEXDUMP API with filtering support
 *    - LOGAPI_HEXDUMP_TAG_EMERG(tag, title, data, size)
 *    - LOGAPI_HEXDUMP_TAG_ALERT(tag, title, data, size)
 *    - LOGAPI_HEXDUMP_TAG_CRIT(tag, title, data, size)
 *    - LOGAPI_HEXDUMP_TAG_ERROR(tag, title, data, size)
 *    - LOGAPI_HEXDUMP_TAG_WARN(tag, title, data, size)
 *    - LOGAPI_HEXDUMP_TAG_NOTICE(tag, title, data, size)
 *    - LOGAPI_HEXDUMP_TAG_INFO(tag, title, data, size)
 *    - LOGAPI_HEXDUMP_TAG_TRACE(tag, title, data, size)
 *    - DBG_HEXDUMP_TAG_EMERG(tag, title, data, size) 
 *    - DBG_HEXDUMP_TAG_ALERT(tag, title, data, size) 
 *    - DBG_HEXDUMP_TAG_CRIT(tag, title, data, size) 
 *    - DBG_HEXDUMP_TAG_ERROR(tag, title, data, size)  
 *    - DBG_HEXDUMP_TAG_WARN(tag, title, data, size)   
 *    - DBG_HEXDUMP_TAG_NOTICE(tag, title, data, size)   
 *    - DBG_HEXDUMP_TAG_INFO(tag, title, data, size)   
 *    - DBG_HEXDUMP_TAG_TRACE(tag, title, data, size)   

 *   Dump HEX and ASCII representation only
 *    - LOGAPI_HEXDUMP_RAW_EMERG(data, size)
 *    - LOGAPI_HEXDUMP_RAW_ALERT(data, size)
 *    - LOGAPI_HEXDUMP_RAW_CRIT(data, size)
 *    - LOGAPI_HEXDUMP_RAW_ERROR(data, size)
 *    - LOGAPI_HEXDUMP_RAW_WARN(data, size)
 *    - LOGAPI_HEXDUMP_RAW_NOTICE(data, size)
 *    - LOGAPI_HEXDUMP_RAW_INFO(data, size)
 *    - LOGAPI_HEXDUMP_RAW_TRACE(data, size)
 *    - DBG_HEXDUMP_RAW_EMERG(data, size)
 *    - DBG_HEXDUMP_RAW_ALERT(data, size)
 *    - DBG_HEXDUMP_RAW_CRIT(data, size)
 *    - DBG_HEXDUMP_RAW_ERROR(data, size)
 *    - DBG_HEXDUMP_RAW_WARN(data, size)
 *    - DBG_HEXDUMP_RAW_NOTICE(data, size)
 *    - DBG_HEXDUMP_RAW_INFO(data, size)
 *    - DBG_HEXDUMP_RAW_TRACE(data, size)
 *
 *   HEXDUMP RAW API with filtering support
 *    - LOGAPI_HEXDUMP_RAW_TAG_EMERG(tag, data, size)
 *    - LOGAPI_HEXDUMP_RAW_TAG_ALERT(tag, data, size)
 *    - LOGAPI_HEXDUMP_RAW_TAG_CRIT(tag, data, size) 
 *    - LOGAPI_HEXDUMP_RAW_TAG_ERROR(tag, data, size)
 *    - LOGAPI_HEXDUMP_RAW_TAG_WARN(tag, data, size) 
 *    - LOGAPI_HEXDUMP_RAW_TAG_NOTICE(tag, data, size
 *    - LOGAPI_HEXDUMP_RAW_TAG_INFO(tag, data, size) 
 *    - LOGAPI_HEXDUMP_RAW_TAG_TRACE(tag, data, size)
 *    - DBG_HEXDUMP_RAW_TAG_EMERG(tag, data, size) 
 *    - DBG_HEXDUMP_RAW_TAG_ALERT(tag, data, size) 
 *    - DBG_HEXDUMP_RAW_TAG_CRIT(tag, data, size) 
 *    - DBG_HEXDUMP_RAW_TAG_ERROR(tag, data, size) 
 *    - DBG_HEXDUMP_RAW_TAG_WARN(tag, data, size) 
 *    - DBG_HEXDUMP_RAW_TAG_NOTICE(tag, data, size) 
 *    - DBG_HEXDUMP_RAW_TAG_INFO(tag, data, size) 
 *    - DBG_HEXDUMP_RAW_TAG_TRACE(tag, data, size) 
 */

#ifndef _LOGAPI_HEXDUMP_H_
#define _LOGAPI_HEXDUMP_H_

#include "logapi_common.h"

#if defined( __cplusplus )
extern "C" {
#endif

/**
* Hex dump API
***/

/**
 * @brief Universal hexdump
 * @warning Use LOGAPI_HEX() macro
 *
 * Dump title + address + size, start separator HEX and ASCII representation, end separator
 */
LOGAPI_PUBLIC void LogAPI_hexdump(
    LogAPI_Levels log_level, const char *title, const void *data, unsigned int size,
    const char *file, unsigned int line
);

/**
 * @brief helper macro
 * @warning Avoid direct use
 */
#define LOGAPI_HEXDUMP( level, title, data, size ) \
    LogAPI_hexdump( level, title, data, size, LOGAPI_FILE, LOGAPI_LINE )

#define LOGAPI_HEXDUMP_EMERG(title, data, size) LOGAPI_HEXDUMP(LOGAPI_EMERG, title, data, size)	
#define LOGAPI_HEXDUMP_ALERT(title, data, size) LOGAPI_HEXDUMP(LOGAPI_ALERT, title, data, size)	
#define LOGAPI_HEXDUMP_CRIT(title, data, size) LOGAPI_HEXDUMP(LOGAPI_CRIT, title, data, size)	
#define LOGAPI_HEXDUMP_ERROR(title, data, size) LOGAPI_HEXDUMP(LOGAPI_ERROR, title, data, size)
#define LOGAPI_HEXDUMP_WARN(title, data, size) LOGAPI_HEXDUMP(LOGAPI_WARN, title, data, size)	
#define LOGAPI_HEXDUMP_NOTICE(title, data, size) LOGAPI_HEXDUMP(LOGAPI_NOTICE, title, data, size)	
#define LOGAPI_HEXDUMP_INFO(title, data, size) LOGAPI_HEXDUMP(LOGAPI_INFO, title, data, size)	
#define LOGAPI_HEXDUMP_TRACE(title, data, size) LOGAPI_HEXDUMP(LOGAPI_TRACE, title, data, size) 

#if defined( LOGAPI_ENABLE_DEBUG )

    #define DBG_HEXDUMP         LOGAPI_HEXDUMP
    #define DBG_HEXDUMP_EMERG   LOGAPI_HEXDUMP_EMERG	
    #define DBG_HEXDUMP_ALERT   LOGAPI_HEXDUMP_ALERT	
    #define DBG_HEXDUMP_CRIT    LOGAPI_HEXDUMP_CRIT	
    #define DBG_HEXDUMP_ERROR   LOGAPI_HEXDUMP_ERROR
    #define DBG_HEXDUMP_WARN    LOGAPI_HEXDUMP_WARN	
    #define DBG_HEXDUMP_NOTICE  LOGAPI_HEXDUMP_NOTICE	
    #define DBG_HEXDUMP_INFO    LOGAPI_HEXDUMP_INFO	
    #define DBG_HEXDUMP_TRACE   LOGAPI_HEXDUMP_TRACE 	
#else
    #define DBG_HEXDUMP(...)        LOGAPI_EMPTYSTMT
    #define DBG_HEXDUMP_OFF(...)    LOGAPI_EMPTYSTMT
    #define DBG_HEXDUMP_EMERG(...)  LOGAPI_EMPTYSTMT	
    #define DBG_HEXDUMP_ALERT(...)  LOGAPI_EMPTYSTMT	
    #define DBG_HEXDUMP_CRIT(...)   LOGAPI_EMPTYSTMT	
    #define DBG_HEXDUMP_ERROR(...)  LOGAPI_EMPTYSTMT
    #define DBG_HEXDUMP_WARN(...)   LOGAPI_EMPTYSTMT	
    #define DBG_HEXDUMP_NOTICE(...) LOGAPI_EMPTYSTMT	
    #define DBG_HEXDUMP_INFO(...)   LOGAPI_EMPTYSTMT	
    #define DBG_HEXDUMP_TRACE(...)  LOGAPI_EMPTYSTMT 	
#endif

/**
 * @brief Universal hexdump
 * @warning Use LOGAPI_HEX_TAG() macro
 *
 * Dump title + address + size, start separator HEX and ASCII representation, end separator
 */
LOGAPI_PUBLIC void LogAPI_hexdump_tag(
    LogAPI_Levels log_level, const char * tag, const char *title, const void *data, unsigned int size,
    const char *file, unsigned int line
);

/**
 * @brief helper macro
 * @warning Avoid direct use
 */
#define LOGAPI_HEXDUMP_TAG( level, tag, title, data, size ) \
    LogAPI_hexdump_tag( level, tag, title, data, size, LOGAPI_FILE, LOGAPI_LINE )

#define LOGAPI_HEXDUMP_TAG_EMERG(tag, title, data, size) LOGAPI_HEXDUMP_TAG(LOGAPI_EMERG, tag, title, data, size)	
#define LOGAPI_HEXDUMP_TAG_ALERT(tag, title, data, size) LOGAPI_HEXDUMP_TAG(LOGAPI_ALERT, tag, title, data, size)	
#define LOGAPI_HEXDUMP_TAG_CRIT(tag, title, data, size) LOGAPI_HEXDUMP_TAG(LOGAPI_CRIT, tag, title, data, size)	
#define LOGAPI_HEXDUMP_TAG_ERROR(tag, title, data, size) LOGAPI_HEXDUMP_TAG(LOGAPI_ERROR, tag, title, data, size)
#define LOGAPI_HEXDUMP_TAG_WARN(tag, title, data, size) LOGAPI_HEXDUMP_TAG(LOGAPI_WARN, tag, title, data, size)	
#define LOGAPI_HEXDUMP_TAG_NOTICE(tag, title, data, size) LOGAPI_HEXDUMP_TAG(LOGAPI_NOTICE, tag, title, data, size)	
#define LOGAPI_HEXDUMP_TAG_INFO(tag, title, data, size) LOGAPI_HEXDUMP_TAG(LOGAPI_INFO, tag, title, data, size)	
#define LOGAPI_HEXDUMP_TAG_TRACE(tag, title, data, size) LOGAPI_HEXDUMP_TAG(LOGAPI_TRACE, tag, title, data, size) 

#if defined( LOGAPI_ENABLE_DEBUG )
    #define DBG_HEXDUMP_TAG         LOGAPI_HEXDUMP_TAG
    #define DBG_HEXDUMP_TAG_EMERG   LOGAPI_HEXDUMP_TAG_EMERG	
    #define DBG_HEXDUMP_TAG_ALERT   LOGAPI_HEXDUMP_TAG_ALERT	
    #define DBG_HEXDUMP_TAG_CRIT    LOGAPI_HEXDUMP_TAG_CRIT	
    #define DBG_HEXDUMP_TAG_ERROR   LOGAPI_HEXDUMP_TAG_ERROR
    #define DBG_HEXDUMP_TAG_WARN    LOGAPI_HEXDUMP_TAG_WARN	
    #define DBG_HEXDUMP_TAG_NOTICE  LOGAPI_HEXDUMP_TAG_NOTICE	
    #define DBG_HEXDUMP_TAG_INFO    LOGAPI_HEXDUMP_TAG_INFO	
    #define DBG_HEXDUMP_TAG_TRACE   LOGAPI_HEXDUMP_TAG_TRACE 	
#else
    #define DBG_HEXDUMP_TAG(...)        LOGAPI_EMPTYSTMT
    #define DBG_HEXDUMP_TAG_OFF(...)    LOGAPI_EMPTYSTMT
    #define DBG_HEXDUMP_TAG_EMERG(...)  LOGAPI_EMPTYSTMT	
    #define DBG_HEXDUMP_TAG_ALERT(...)  LOGAPI_EMPTYSTMT	
    #define DBG_HEXDUMP_TAG_CRIT(...)   LOGAPI_EMPTYSTMT	
    #define DBG_HEXDUMP_TAG_ERROR(...)  LOGAPI_EMPTYSTMT
    #define DBG_HEXDUMP_TAG_WARN(...)   LOGAPI_EMPTYSTMT	
    #define DBG_HEXDUMP_TAG_NOTICE(...) LOGAPI_EMPTYSTMT	
    #define DBG_HEXDUMP_TAG_INFO(...)   LOGAPI_EMPTYSTMT	
    #define DBG_HEXDUMP_TAG_TRACE(...)  LOGAPI_EMPTYSTMT 	
#endif
   

/**
 * @brief Universal hexdump
 * @warning Use LOGAPI_HEX_RAW_TAG() macro
 *
 * Dump HEX and ASCII representation only
 */
LOGAPI_PUBLIC void LogAPI_hexdump_raw_tag(
        LogAPI_Levels log_level, const char * tag, const void *data, unsigned int size,
        const char *file, unsigned int line
);

/**
 * @brief helper macro
 * @warning Avoid direct use
 */
#define LOGAPI_HEXDUMP_RAW_TAG( level, tag, data, size ) \
    LogAPI_hexdump_raw_tag( level, tag, data, size, LOGAPI_FILE, LOGAPI_LINE )
  
#define LOGAPI_HEXDUMP_RAW_TAG_EMERG(tag, data, size)  LOGAPI_HEXDUMP_RAW_TAG(LOGAPI_EMERG, tag, data, size)	
#define LOGAPI_HEXDUMP_RAW_TAG_ALERT(tag, data, size)  LOGAPI_HEXDUMP_RAW_TAG(LOGAPI_ALERT, tag, data, size)	
#define LOGAPI_HEXDUMP_RAW_TAG_CRIT(tag, data, size)   LOGAPI_HEXDUMP_RAW_TAG(LOGAPI_CRIT, tag, data, size)	
#define LOGAPI_HEXDUMP_RAW_TAG_ERROR(tag, data, size)  LOGAPI_HEXDUMP_RAW_TAG(LOGAPI_ERROR, tag, data, size)
#define LOGAPI_HEXDUMP_RAW_TAG_WARN(tag, data, size)   LOGAPI_HEXDUMP_RAW_TAG(LOGAPI_WARN, tag, data, size)	
#define LOGAPI_HEXDUMP_RAW_TAG_NOTICE(tag, data, size) LOGAPI_HEXDUMP_RAW_TAG(LOGAPI_NOTICE, tag, data, size)	
#define LOGAPI_HEXDUMP_RAW_TAG_INFO(tag, data, size)   LOGAPI_HEXDUMP_RAW_TAG(LOGAPI_INFO, tag, data, size)	
#define LOGAPI_HEXDUMP_RAW_TAG_TRACE(tag, data, size)  LOGAPI_HEXDUMP_RAW_TAG(LOGAPI_TRACE, tag, data, size) 

#if defined( LOGAPI_ENABLE_DEBUG )
    #define DBG_HEXDUMP_RAW_TAG         LOGAPI_HEXDUMP_RAW_TAG
    #define DBG_HEXDUMP_RAW_TAG_EMERG   LOGAPI_HEXDUMP_RAW_TAG_EMERG	
    #define DBG_HEXDUMP_RAW_TAG_ALERT   LOGAPI_HEXDUMP_RAW_TAG_ALERT	
    #define DBG_HEXDUMP_RAW_TAG_CRIT    LOGAPI_HEXDUMP_RAW_TAG_CRIT	
    #define DBG_HEXDUMP_RAW_TAG_ERROR   LOGAPI_HEXDUMP_RAW_TAG_ERROR
    #define DBG_HEXDUMP_RAW_TAG_WARN    LOGAPI_HEXDUMP_RAW_TAG_WARN	
    #define DBG_HEXDUMP_RAW_TAG_NOTICE  LOGAPI_HEXDUMP_RAW_TAG_NOTICE	
    #define DBG_HEXDUMP_RAW_TAG_INFO    LOGAPI_HEXDUMP_RAW_TAG_INFO	
    #define DBG_HEXDUMP_RAW_TAG_TRACE   LOGAPI_HEXDUMP_RAW_TAG_TRACE 	
#else
    #define DBG_HEXDUMP_RAW_TAG(...)        LOGAPI_EMPTYSTMT
    #define DBG_HEXDUMP_RAW_TAG_OFF(...)    LOGAPI_EMPTYSTMT
    #define DBG_HEXDUMP_RAW_TAG_EMERG(...)  LOGAPI_EMPTYSTMT	
    #define DBG_HEXDUMP_RAW_TAG_ALERT(...)  LOGAPI_EMPTYSTMT	
    #define DBG_HEXDUMP_RAW_TAG_CRIT(...)   LOGAPI_EMPTYSTMT	
    #define DBG_HEXDUMP_RAW_TAG_ERROR(...)  LOGAPI_EMPTYSTMT
    #define DBG_HEXDUMP_RAW_TAG_WARN(...)   LOGAPI_EMPTYSTMT	
    #define DBG_HEXDUMP_RAW_TAG_NOTICE(...) LOGAPI_EMPTYSTMT	
    #define DBG_HEXDUMP_RAW_TAG_INFO(...)   LOGAPI_EMPTYSTMT	
    #define DBG_HEXDUMP_RAW_TAG_TRACE(...)  LOGAPI_EMPTYSTMT 	
#endif
 

/**
 * @brief Universal hexdump
 * @warning Use LOGAPI_HEX_RAW() macro
 *
 * Dump HEX and ASCII representation only
 */
LOGAPI_PUBLIC void LogAPI_hexdump_raw(
        LogAPI_Levels log_level, const void *data, unsigned int size,
        const char *file, unsigned int line
);

/**
 * @brief helper macro
 * @warning Avoid direct use
 */
#define LOGAPI_HEXDUMP_RAW( level, data, size ) \
    LogAPI_hexdump_raw( level, data, size, LOGAPI_FILE, LOGAPI_LINE )

#define LOGAPI_HEXDUMP_RAW_EMERG(data, size) LOGAPI_HEXDUMP_RAW(LOGAPI_EMERG, data, size)	
#define LOGAPI_HEXDUMP_RAW_ALERT(data, size) LOGAPI_HEXDUMP_RAW(LOGAPI_ALERT, data, size)	
#define LOGAPI_HEXDUMP_RAW_CRIT(data, size) LOGAPI_HEXDUMP_RAW(LOGAPI_CRIT, data, size)	
#define LOGAPI_HEXDUMP_RAW_ERROR(data, size) LOGAPI_HEXDUMP_RAW(LOGAPI_ERROR, data, size)
#define LOGAPI_HEXDUMP_RAW_WARN(data, size) LOGAPI_HEXDUMP_RAW(LOGAPI_WARN, data, size)	
#define LOGAPI_HEXDUMP_RAW_NOTICE(data, size) LOGAPI_HEXDUMP_RAW(LOGAPI_NOTICE, data, size)	
#define LOGAPI_HEXDUMP_RAW_INFO(data, size) LOGAPI_HEXDUMP_RAW(LOGAPI_INFO, data, size)	
#define LOGAPI_HEXDUMP_RAW_TRACE(data, size) LOGAPI_HEXDUMP_RAW(LOGAPI_TRACE, data, size) 

#if defined( LOGAPI_ENABLE_DEBUG )
    #define DBG_HEXDUMP_RAW         LOGAPI_HEXDUMP_RAW
    #define DBG_HEXDUMP_RAW_EMERG   LOGAPI_HEXDUMP_RAW_EMERG	
    #define DBG_HEXDUMP_RAW_ALERT   LOGAPI_HEXDUMP_RAW_ALERT	
    #define DBG_HEXDUMP_RAW_CRIT    LOGAPI_HEXDUMP_RAW_CRIT	
    #define DBG_HEXDUMP_RAW_ERROR   LOGAPI_HEXDUMP_RAW_ERROR
    #define DBG_HEXDUMP_RAW_WARN    LOGAPI_HEXDUMP_RAW_WARN	
    #define DBG_HEXDUMP_RAW_NOTICE  LOGAPI_HEXDUMP_RAW_NOTICE	
    #define DBG_HEXDUMP_RAW_INFO    LOGAPI_HEXDUMP_RAW_INFO	
    #define DBG_HEXDUMP_RAW_TRACE   LOGAPI_HEXDUMP_RAW_TRACE 	
#else
    #define DBG_HEXDUMP_RAW(...)        LOGAPI_EMPTYSTMT
    #define DBG_HEXDUMP_RAW_OFF(...)    LOGAPI_EMPTYSTMT
    #define DBG_HEXDUMP_RAW_EMERG(...)  LOGAPI_EMPTYSTMT	
    #define DBG_HEXDUMP_RAW_ALERT(...)  LOGAPI_EMPTYSTMT	
    #define DBG_HEXDUMP_RAW_CRIT(...)   LOGAPI_EMPTYSTMT	
    #define DBG_HEXDUMP_RAW_ERROR(...)  LOGAPI_EMPTYSTMT
    #define DBG_HEXDUMP_RAW_WARN(...)   LOGAPI_EMPTYSTMT	
    #define DBG_HEXDUMP_RAW_NOTICE(...) LOGAPI_EMPTYSTMT	
    #define DBG_HEXDUMP_RAW_INFO(...)   LOGAPI_EMPTYSTMT	
    #define DBG_HEXDUMP_RAW_TRACE(...)  LOGAPI_EMPTYSTMT 	
#endif

#if defined( __cplusplus )
}
#endif
#endif  //_LOGAPI_HEXDUMP_H_
