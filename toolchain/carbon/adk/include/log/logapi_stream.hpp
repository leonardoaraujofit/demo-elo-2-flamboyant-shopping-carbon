/**
 * @file logapi_stream.hpp
*/

/* Public API:
   C++ iostream-like message log
     LOGS_ERROR( right_expr_to_<<_op )
     LOGS_WARN( right_expr_to_<<_op )
     LOGS_INFO( right_expr_to_<<_op )
     LOGS_TRACE( right_expr_to_<<_op )

     DBGS_ERROR( right_expr_to_<<_op )
     DBGS_WARN( right_expr_to_<<_op )
     DBGS_INFO( right_expr_to_<<_op )
     DBGS_TRACE( right_expr_to_<<_op )
*/
#ifndef _LOGAPI_STREAM_H
#define _LOGAPI_STREAM_H

#include "logapi_common.h"
#include <streambuf>
#include <ostream>
#include <vector>


namespace LogAPI
{

/** @brief Helper class
  * @warning Do not use directly, unless you know what are you doing
*/


class MsgStreamBuf : public std::streambuf
{
    public:
            LOGAPI_PUBLIC explicit MsgStreamBuf(std::size_t buff_sz = LOGAPI_MSG_MAX_LEN);
            LOGAPI_PUBLIC void setOpts(LogAPI_Levels level, const char *sourceFile, unsigned int sourceLine);
            LOGAPI_PUBLIC int isInFilterList( const char * tag );

    private:
        virtual int_type overflow(int_type ch);
        virtual int sync();

        MsgStreamBuf(const MsgStreamBuf &);
        MsgStreamBuf &operator= (const MsgStreamBuf &);

    private:
        bool cap_next_;
        std::vector<char> buffer_;
        std::string source_file_;
        size_t source_line_;
        LogAPI_Levels level_;
};

LOGSTREAM_API_PUBLIC extern std::ostream g_stream;
LOGSTREAM_API_PUBLIC extern MsgStreamBuf g_buff;

/** @brief helper macro
  * @warning Avoid direct use
*/
#define LOGAPI_STREAM( level, ... ) \
    do \
    {\
        if ( int( level ) <= int(LogAPI_GetLevel())) \
        {\
            LogAPI::g_buff.setOpts(level, LOGAPI_FILE, LOGAPI_LINE);\
            LogAPI::g_stream << __VA_ARGS__ << std::flush;\
        }\
    } while (0)

/** @brief helper macro
  * @warning Avoid direct use
*/
#define LOGAPI_TAG_STREAM( level, tag, ... ) \
    do \
    {\
        if ( int( level ) <= int(LogAPI_GetLevel())) \
        {\
            if ( LogAPI::g_buff.isInFilterList( tag ) )\
            {\
                LogAPI::g_buff.setOpts(level, LOGAPI_FILE, LOGAPI_LINE);\
                LogAPI::g_stream << __VA_ARGS__ << std::flush;\
            }\
        }\
    } while (0)
 
#   define LOGS_EMERG( ... )  LOGAPI_STREAM( LOGAPI_EMERG, __VA_ARGS__ )
#   define LOGS_ALERT( ... ) LOGAPI_STREAM( LOGAPI_ALERT, __VA_ARGS__ )
#   define LOGS_CRIT( ... ) LOGAPI_STREAM( LOGAPI_CRIT, __VA_ARGS__ )
#   define LOGS_ERROR( ... ) LOGAPI_STREAM( LOGAPI_ERROR, __VA_ARGS__ )
#   define LOGS_WARN( ... ) LOGAPI_STREAM( LOGAPI_WARN, __VA_ARGS__ )
#   define LOGS_NOTICE( ... ) LOGAPI_STREAM( LOGAPI_NOTICE, __VA_ARGS__ )
#   define LOGS_INFO( ... ) LOGAPI_STREAM( LOGAPI_INFO, __VA_ARGS__ )
#   define LOGS_TRACE( ... ) LOGAPI_STREAM( LOGAPI_TRACE, __VA_ARGS__ ) 

#   define LOGS_TAG_EMERG( tag, ... )  LOGAPI_TAG_STREAM( LOGAPI_EMERG, tag, __VA_ARGS__ )
#   define LOGS_TAG_ALERT( tag, ... ) LOGAPI_TAG_STREAM( LOGAPI_ALERT, tag, __VA_ARGS__ )
#   define LOGS_TAG_CRIT( tag, ... ) LOGAPI_TAG_STREAM( LOGAPI_CRIT, tag, __VA_ARGS__ )
#   define LOGS_TAG_ERROR( tag, ... ) LOGAPI_TAG_STREAM( LOGAPI_ERROR, tag, __VA_ARGS__ )
#   define LOGS_TAG_WARN( tag, ... ) LOGAPI_TAG_STREAM( LOGAPI_WARN, tag, __VA_ARGS__ )
#   define LOGS_TAG_NOTICE( tag, ... ) LOGAPI_TAG_STREAM( LOGAPI_NOTICE, tag, __VA_ARGS__ )
#   define LOGS_TAG_INFO( tag, ... ) LOGAPI_TAG_STREAM( LOGAPI_INFO, tag, __VA_ARGS__ )
#   define LOGS_TAG_TRACE( tag, ... ) LOGAPI_TAG_STREAM( LOGAPI_TRACE, tag, __VA_ARGS__ ) 

#if defined( LOGAPI_ENABLE_DEBUG )
    #define DBG_STREAM  LOGAPI_STREAM
    #define DBGS_EMERG LOGS_EMERG	
    #define DBGS_ALERT LOGS_ALERT	
    #define DBGS_CRIT LOGS_CRIT	
    #define DBGS_ERROR LOGS_ERROR
    #define DBGS_WARN LOGS_WARN	
    #define DBGS_NOTICE LOGS_NOTICE	
    #define DBGS_INFO LOGS_INFO	
    #define DBGS_TRACE LOGS_TRACE 	

    #define DBG_TAG_STREAM  LOGAPI_TAG_STREAM
    #define DBGS_TAG_EMERG LOGS_TAG_EMERG	
    #define DBGS_TAG_ALERT LOGS_TAG_ALERT	
    #define DBGS_TAG_CRIT LOGS_TAG_CRIT	
    #define DBGS_TAG_ERROR LOGS_TAG_ERROR
    #define DBGS_TAG_WARN LOGS_TAG_WARN	
    #define DBGS_TAG_NOTICE LOGS_TAG_NOTICE	
    #define DBGS_TAG_INFO LOGS_TAG_INFO	
    #define DBGS_TAG_TRACE LOGS_TAG_TRACE 	 
#else
    #define DBG_STREAM(...) LOGAPI_EMPTYSTMT
    #define DBGS_EMERG(...) LOGAPI_EMPTYSTMT	
    #define DBGS_ALERT(...) LOGAPI_EMPTYSTMT	
    #define DBGS_CRIT(...) LOGAPI_EMPTYSTMT	
    #define DBGS_ERROR(...) LOGAPI_EMPTYSTMT
    #define DBGS_WARN(...) LOGAPI_EMPTYSTMT	
    #define DBGS_NOTICE(...) LOGAPI_EMPTYSTMT	
    #define DBGS_INFO(...) LOGAPI_EMPTYSTMT	
    #define DBGS_TRACE(...) LOGAPI_EMPTYSTMT 	 

    #define DBG_TAG_STREAM(...) LOGAPI_EMPTYSTMT
    #define DBGS_TAG_EMERG(...) LOGAPI_EMPTYSTMT	
    #define DBGS_TAG_ALERT(...) LOGAPI_EMPTYSTMT	
    #define DBGS_TAG_CRIT(...) LOGAPI_EMPTYSTMT	
    #define DBGS_TAG_ERROR(...) LOGAPI_EMPTYSTMT
    #define DBGS_TAG_WARN(...) LOGAPI_EMPTYSTMT	
    #define DBGS_TAG_NOTICE(...) LOGAPI_EMPTYSTMT	
    #define DBGS_TAG_INFO(...) LOGAPI_EMPTYSTMT	
    #define DBGS_TAG_TRACE(...) LOGAPI_EMPTYSTMT 	
#endif

} // LogAPI

#endif //_LOGAPI_STREAM_H
