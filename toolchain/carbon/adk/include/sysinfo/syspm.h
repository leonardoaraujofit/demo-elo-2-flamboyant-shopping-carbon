#ifndef SYSPM_H_2015_02_25
#define SYSPM_H_2015_02_25

#if (defined _VRXEVO || defined _WIN32)
#  if   defined VFI_SYSINFO_DLL_EXPORT
#    define SYS_INFO_API __declspec(dllexport)
#  elif defined VFI_SYSINFO_DLL_IMPORT && defined _VRXEVO
#    define SYS_INFO_API __declspec(dllimport) // dllimport not required for Windows
#  else
#    define SYS_INFO_API
#  endif
#elif defined __GNUC__ && defined VFI_SYSINFO_DLL_EXPORT
#  define SYS_INFO_API  __attribute__((visibility ("default")))
#else
#  define SYS_INFO_API
#endif
 
/** \file syspm.h 
 * \defgroup syspm System power management functions
 * @{
 */

#include <map>
#include <string>

#ifndef DOXYGEN
/** Verifone system information (vfisysinfo) namespace */
namespace vfisysinfo {
#endif

/** Reboot terminal
 * \return error code
 */
SYS_INFO_API int sysReboot();

/** Puts terminal in sleep mode
 * \return error code
 */
SYS_INFO_API int sysSleep();

/** Shut down terminal
 * \return error code
 */
SYS_INFO_API int sysShutdown();

#ifndef DOXYGEN
} // namespace vfisysinfo
#endif
/** @}*/

#endif
