/*************************************************************
* FILE NAME:   NFC_Interface.h                               *
* MODULE NAME: NFC Framework                                 *
* PROGRAMMER:  Evgeny Sokolovsky                             *
* DESCRIPTION:                                               *
* REVISION:                                                  *
*************************************************************/
#ifndef __NFCINTERFACE_H__
#define __NFCINTERFACE_H__

#ifdef __cplusplus
extern "C" {
#endif


/* Global defines */
//#define _NFC_SERIAL_INTERFACE_
//#define _DEBUG_



#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif


 /*==========================================*
  *             RESPONSE CODES               *
  *==========================================*/

typedef enum
{
	EMB_APP_OK=0,//!< EMB_APP_OK
	EMB_APP_INCORRECT_HEADER,				// 0x01
	EMB_APP_UNKNOWN_COMMAND,				// 0x02
	EMB_APP_UNKNOWN_SUBCOMMAND,         	// 0x03
	EMB_APP_COMMAND_NOT_SUPPORTED,      	// 0x04
	EMB_APP_SUBCOMMAND_NOT_SUPPORTED,   	// 0x05
	EMB_APP_CRC_ERROR,                  	// 0x06
	EMB_APP_FAILED,                     	// 0x07
	EMB_APP_TIMEOUT,                   	 	// 0x08
	EMB_APP_UNKNOWN_APP_NAME,           	// 0x09
	EMB_APP_PARAMETER_NOT_SUPPORTED,    	// 0x0A
	EMB_APP_OTHER_APP_RUNNING,          	// 0x0B
	EMB_APP_AUTO_RUN,						// 0x0C
	EMB_APP_MULTI_CARDS,					// 0x0D
	EMB_APP_START_PAYMENT,					// 0x0E
	EMB_APP_COMM_ERROR,						// 0x0F
	EMB_APP_DATA_CRC_ERROR,					// 0x10
	EMB_APP_INCORRECT_DATA,					// 0x11
	EMB_APP_CANCEL_DONE,					// 0x12
	EMB_APP_CANCEL_IRRELEVANT,				// 0x13
	EMB_APP_CANCEL_NOT_ALLOWED,				// 0x14
	EMB_APP_DISCOVERY_CANCELED,				// 0x15
	EMB_APP_UNSUPPORTED_CARD,				// 0x16
	EMB_APP_SECOND_TAP_FAILED,				// 0x17
	EMB_APP_OUT_OF_ORDER_COMMAND,			// 0x18
	EMB_APP_2ND_AUTHENTICATION_FAILED,		// 0x19
	EMB_APP_NFC_GROUP_RET_FAIL,				// 0x1A
	EMB_APP_INCORRECT_LEN,			    	// 0x1B
	EMB_APP_TO_MANY_WALLETS,				// 0x1C
	EMB_APP_COMMAND_NOT_ALLOWED,			// 0x1D
	EMB_APP_MISSING_MANDATORY_DATA,			// 0x1E
	EMB_APP_TWO_SNEP_DEFAULT_WALLET,		// 0x1F
	EMB_APP_INCORRECT_APP_NAME,				// 0x20
	EMB_APP_APPLICATION_DATA_NOT_ALLOWED,	// 0x21
	EMB_APP_APPLICATION_NOT_FOUND,			// 0X22
	EMB_APP_MISMATHCED_UID,					// 0x23
	EMB_APP_WALLET_NOT_EXIST,				// 0x24
	EMB_APP_NFCUTIL_ERROR,					// 0x25
	EMB_APP_USER_ACTION_REQUIRED_UI,		// 0x26
	EMB_APP_VAS_DATA_NOT_ACTIVATED,			// 0x27
	EMB_APP_BAD_SYNTAX,						// 0x28
	EMB_APP_INTERNAL_ERROR,					// 0x29
	EMB_APP_CARD_NOT_FOUND,					// 0x2A
	EMB_APP_CARD_ERROR,						// 0x2B
	EMB_APP_L1_CARD_PROTOCOL_ERROR,			// 0x2C
	EMB_APP_L1_CARD_NOT_ACTIVE,				// 0x2D
	EMB_APP_DUMMY_FUNC_CALL,                // 0x2E
    EMB_APP_CALLBACK_SET_ERROR              // 0x2F
}ResponseCodes;

typedef enum
{

	VAS_OK =0,
	VAS_DO_PAY,
	VAS_FAIL,
	VAS_ERR_CTLS_DRIVER_CLOSE,
	VAS_CANCEL,
	VAS_TIME_OUT,
	VAS_ERR_CONFIG,
	VAS_DUMMY_CALL,
	VAS_CANCEL_NOT_ALLOWED,
	VAS_CANCEL_IRRELEVANT,
	VAS_COMM_ERR,

}VasStatus;


typedef struct
{
	unsigned char*		buffer;
	unsigned int		bufferLen;
	unsigned int		bufferMaxSize;
}rawData;

typedef struct
{
	unsigned int 	pollTime;
	unsigned char	technology;
	unsigned char	cardType;
	unsigned char	collision;
	unsigned char	activation;
	unsigned char	customPollParams[1];
	unsigned char	customPollParamsSize;
}discoveryParams;

typedef struct
{
	unsigned char	technology;
}activateParams;


typedef struct
{
	unsigned char	numOfCards;
	unsigned char	numOfCardsA;
	unsigned char	cardTypeA;
	unsigned char	sak;
	unsigned char	uidA_Size;
	unsigned char	uidA[1];
	unsigned char	numOfCardsB;
	unsigned char	cardTypeB;
	unsigned char	uidB_Size;
	unsigned char	uidB[1];
	unsigned char	numOfCardsF;
	unsigned char	cardTypeF;
	unsigned char	uidF_Size;
	unsigned char	uidF[1];
	unsigned char	pollData[1];
	unsigned char	pollDataSize;
	unsigned char	status;
}discoveryResult;

typedef enum
{
	I_ISO14443A             = 1,
	I_JEWEL                 = 2,
	I_ISO14443B             = 3,
	I_ISO14443BI            = 4, // pre-ISO14443B aka ISO/IEC 14443 B' or Type B'
	I_ISO14443B2SR          = 5, // ISO14443-2B ST SRx
	I_ISO14443B2CT          = 6, // ISO14443-2B ASK CTx
	I_DEP                   = 7,
	I_FELICA                = 8,
	I_ISO14443A_MIFARE_MINI = 9,
	I_ISO14443A_MIFARE_1K   = 10,
	I_ISO14443A_MIFARE_4K   = 11,
	I_ISO14443A_MIFARE_DES  = 12,
	I_ISO14443A_MIFARE_PLUS = 13,
	I_ISO14443A_MIFARE_UL   = 14,
	I_ISO14443A_MIFARE_UL_C = 15,
	I_UNKNOWN_CARD_TYPE     = 16

}NFC_CARD_TYPE;

typedef enum
{
	I_MIFARE_TYPE_CLASSIC,
	I_MIFARE_TYPE_ULTRALIGHT,
}I_MIFARE_CARD_TYPE;

typedef enum
{
	NFC_POLL_PARAM_TECH_A		=	0x01,
	NFC_POLL_PARAM_TECH_B		=	0x02,
	NFC_POLL_PARAM_TECH_AB		=	0x03,
	NFC_POLL_PARAM_TECH_F_DEP	=	0x04,
	NFC_POLL_PARAM_TECH_FELICA	=	0x08,
	NFC_POLL_PARAM_TECH_F		= 	0x0C,
	NFC_POLL_PARAM_TECH_AF		=	0x05,
	NFC_POLL_PARAM_TECH_BF		=	0x06,
	NFC_POLL_PARAM_TECH_ABF		=	0x07,
	NFC_POLL_PARAM_CUSTOM		=	0x10,
}NFC_POLL_PARAM_TECH;


typedef struct _cardInfo
{
	rawData				card_info;
	NFC_CARD_TYPE		cardType;

}cardInfo;

typedef struct _pollRes
{
	unsigned int    m_foundTargetsTotalCount;
	unsigned int    m_foundTargetsA;
	unsigned int    m_foundTargetsB;
	unsigned int    m_foundTargetsF;
	cardInfo        *cards_info_arr;
	rawData         custom_poll_result;
}pollRes;

typedef struct _pollReq
{
	unsigned int 		tech_bitmap;
	unsigned int		poll_timeout;
	rawData 			customData;
}pollReq;

typedef struct
{
	unsigned char	status;
	rawData	        rdWalletName;
	rawData	        rdWalletAuthResult;
}authenticationResult;

typedef struct
{
	unsigned char	status;
	rawData	        rdWalletName;
	rawData	        rdWalletServices;
}getServicesResult;

typedef struct
{
	unsigned char	cardType;
	unsigned char	tech;
	rawData	        rdWalletName;
	rawData	        rdWalletAuthParams;
	unsigned int	expectedResultMaxSize;
}authenticationParams;

typedef struct
{
	rawData	        rdWalletName;
	rawData	        rdWalletGetServicesParams;
	unsigned int	expectedResultMaxSize;
}getServicesParams;

typedef struct
{
	authenticationParams	*pAuthenticationParams;
	getServicesParams		*pGetServicesParams;
}doVASInput;

typedef struct
{
	authenticationResult	*pAuthenticationResult;
	getServicesResult		*pGetServicesResult;
}doVASOutput;

typedef struct
{
	unsigned int clas;
	unsigned int instruction;
	unsigned int param1;
	unsigned int param2;
	unsigned int expectedResponseLen;
	rawData txData;
}apduTxData;

typedef struct
{
	unsigned int    sw1;
	unsigned int    sw2;
	rawData			rxData;
}apduRxData;

typedef enum
{
	INF_BAUD212,
	INF_BAUD424
}NFC_F_BAUD;

typedef enum
{
	MIFARE_KEY_TYPE_A=1,
	MIFARE_KEY_TYPE_B
}MIFARE_KEY_TYPE;

typedef struct
{
	unsigned char clas;
	unsigned char instraction;
	unsigned char param1;
	unsigned char param2;
	unsigned char expectResLen;
}apduCommand;

typedef enum
{
	IDLE = 1,
	BUSY = 2,
}FrameworkState;

typedef struct
{
     unsigned int   timeOut;
     rawData txData;
}felicaTxData;

typedef struct
{
     rawData rxData;
}felicaRxData;

typedef struct
{
	unsigned int 	reciveTimeOUt;
	unsigned char 	systemCode[2];
	unsigned char       requestCode;
	unsigned char       TimeSlot;
}felicaPolling;

typedef struct{
	unsigned char UID[8];
	unsigned char pad[8];
	unsigned char sysCode[2];
}felicaPollingOutput;

/*
 * callbackFlags, this bit field tells what info is present in this callback
 * Multiple info might be present in one callback
 */
typedef struct
{
    unsigned int     init           : 1;
    unsigned int     textPresent    : 1;
    unsigned int     ledsPresent    : 1;
    unsigned int     buzzerPresent  : 1;
} callbackFlags;

/*
 * Text info
 */
#define CALLBACK_MAX_TEXT_SIZE         (64)
typedef struct
{
    // Null terminated string
    char            text[CALLBACK_MAX_TEXT_SIZE];
}callbackText;

/*
 * LED info, this bit field tells the state each LED should take:
 *  0-> Turn LED off
 *  1-> Turn LED on
 *  2-> Blink LED
 */
typedef struct
{
    unsigned int     led1           : 2;
    unsigned int     led2           : 2;
    unsigned int     led3           : 2;
    unsigned int     led4           : 2;
}callbackLeds;

/*
 * Buzzer frequencies used by callbackBuzzer
 */
typedef enum
{
    CALLBACK_BUZZER_FREQUENCY_LOW   = 0xFFFF,
    CALLBACK_BUZZER_FREQUENCY_HIGH  = 0xFFFE
} callbackBuzzerFrequency;

/*
 * Buzzer info
 */
typedef struct
{
    // Frequency
    callbackBuzzerFrequency         frequency;

    // Time in milliseconds the buzzer should be ON
    unsigned int                    on_ms;

    // Time in milliseconds the buzzer should be OFF
    unsigned int                    off_ms;

    // Number of times the buzzer cycle(ON-OFF) should repeat
    unsigned int                    nTimes;
}callbackBuzzer;

/*
 * Callback info structure
 */
typedef struct
{
    // Flags tells what info is present
    callbackFlags                   flags;

    // Text info
    callbackText                    text;

    // Leds info
    callbackLeds                    leds;

    // Buzzer info
    callbackBuzzer                  buzzer;
}callbackInfo;

/*
 *  NFC callback prototype
 *
 * (in ) data - Data associated with callback reason. It should be fed into a
 *              CallbackParseData function to have the callbackInfo structure populated.
 * (in ) dataSizeBytes - Size of data buffer.
 *
 */
typedef void (NfcCallbackFunction) (unsigned char *data, size_t dataSizeBytes);

/*
 *  This function is responsible to parse the callback data and populate the callbackInfo structure
 *
 * (in ) data - Data associated with callback.
 * (in ) dataSizeBytes - Size of data buffer.
 * (out) pCallbackInfo - Pointer to callbackBuzzer structure to be populated by this function
 *
 */
ResponseCodes       CallbackParseData(unsigned char *data, size_t dataSizeBytes, callbackInfo *pCallbackInfo );


ResponseCodes 		NFC_Config_Init(void);
ResponseCodes		NFC_Get_Version(rawData *output);
ResponseCodes		NFC_Ping(rawData *output);
ResponseCodes       NFC_Set_Callback_Function(rawData* id, NfcCallbackFunction *callbackFunction );
ResponseCodes       NFC_Callback_Test(void);

// Pass Through Command
ResponseCodes 		NFC_PT_Open(void);
ResponseCodes 		NFC_PT_Close(void);
ResponseCodes 		NFC_PT_FieldOn(void);
ResponseCodes 		NFC_PT_FieldOff(void);
ResponseCodes		NFC_PT_Polling(pollReq *in_pull_req, pollRes *out_pull_res);
void 				NFC_Free_Poll_Data(pollRes *out_pull_res);
ResponseCodes    	NFC_PT_Cancel_Polling(void);
ResponseCodes		NFC_PT_Activation(NFC_CARD_TYPE cardType, rawData* rd_activationData);
ResponseCodes		NFC_PT_TxRx(NFC_CARD_TYPE cardType, rawData* in_buff, rawData* out_buff);
ResponseCodes		NFC_PT_FtechBaud(NFC_F_BAUD baud);

//Mifare Command
ResponseCodes		NFC_Mifare_Authenticate(unsigned char blockNumber, MIFARE_KEY_TYPE keyType, rawData*	Key);
ResponseCodes		NFC_Mifare_Read (I_MIFARE_CARD_TYPE m_cardType, unsigned int StartBlockNum, unsigned int blockAmount, rawData* out_buff);
ResponseCodes		NFC_Mifare_Write(I_MIFARE_CARD_TYPE m_cardType, unsigned int StartBlockNum, unsigned int blockAmount, rawData* in_buff);

//Felica Command
ResponseCodes      	NFC_Felica_Exchange(felicaTxData* in_buff, felicaRxData* out_buff);
ResponseCodes	   	NFC_Felica_Polling(unsigned int pollTimeout ,felicaPolling* inData , felicaPollingOutput* outData);

//APDU Command
ResponseCodes		NFC_APDU_Exchange(apduTxData* txData, apduRxData* rxData);

VasStatus	 		NFC_Terminal_Config(rawData *input, rawData *output);
VasStatus 			NFC_TERMINAL_ReadConfig(rawData* id, rawData *output);


//VAS Command
VasStatus		NFC_VAS_ReadConfig(rawData* id, rawData *output);
VasStatus		NFC_VAS_Activate(rawData* id,rawData *input, rawData *output);
VasStatus		NFC_VAS_Cancel(void);

VasStatus 		NFC_VAS_UpdateConfig(rawData* id, rawData *input, rawData *output);
VasStatus		NFC_VAS_CancelConfig(rawData*  id);


VasStatus 		NFC_VAS_PreLoad(rawData* id, rawData *input, rawData *output);
VasStatus		NFC_VAS_CancelPreLoad(rawData*  id);
VasStatus		NFC_VAS_Continue(rawData* id, rawData *output);


#ifdef __cplusplus
}//extern "C"
#endif

#endif //__NFCINTERFACE_H__
