/*
 * utils.h
 *
 *  Created on: Mar 10, 2016
 *      Author: idano2
 */

#ifndef SRC_UTILS_H_
#define SRC_UTILS_H_

#define	JASON_KEY_TERM_CONF		"Terminal_Configuration"
#define JSON_KEY_LOG			"Log"
#define JSON_KEY_LOG_LEVEL		"Log_Level_Bit_Map"

#define	LOG_ERROR_MASK		0x00000001
#define	LOG_WARNING_MASK	0x00000002
#define	LOG_INFO_MASK		0x00000004
#define	LOG_DEBUG_MASK		0x00000008
#define	LOG_PERFORMANCE_MASK	0x00000010

extern usint logBitMap;

//Macro
#define MAX(a,b) \
      ({ typeof (a) _a = (a); \
          typeof (b) _b = (b); \
        _a > _b ? _a : _b; })

#define MIN(a,b) \
      ({ typeof (a) _a = (a); \
          typeof (b) _b = (b); \
        _a < _b ? _a : _b; })

#define LOG(logMask, ...)  if ((logMask&logBitMap) != 0) {printf("NFC-ADK : "); printf(__VA_ARGS__);}
#define	LOG_DEBUG(...) LOG(LOG_DEBUG_MASK, __VA_ARGS__)
#define	LOG_ERR(...) LOG(LOG_ERROR_MASK, __VA_ARGS__)

#define	LOG_W_NAME(logMask, ...) if ((logMask&logBitMap) != 0) {printf("NFC-ADK : "); printf("%s - ", __func__); printf(__VA_ARGS__);}
#define	LOG_ERR_W_NAME(...) LOG_W_NAME(LOG_ERROR_MASK, __VA_ARGS__)
#define	LOG_DBG_W_NAME(...) LOG_W_NAME(LOG_DEBUG_MASK, __VA_ARGS__)
#define	LOG_INFO_W_NAME(...) LOG_W_NAME(LOG_INFO_MASK, __VA_ARGS__)
#define	LOG_PERF_W_NAME(...) LOG_W_NAME(LOG_PERFORMANCE_MASK, __VA_ARGS__)
#define	SET_T(t) if ((LOG_PERFORMANCE_MASK&logBitMap) != 0) {t=clock();}
#define	PRINT_PERF(str, t1, t2)  if ((LOG_PERFORMANCE_MASK&logBitMap) != 0) {std::cout << str << " clocks: " <<  (t2-t1) << " time: " << (((float)(t2-t1))/1000.0) << "ms"<< std::endl;}

#define	DEBUG_START_FUNC()	LOG(LOG_DEBUG_MASK, "%s - start\n", __func__)
#define	DEBUG_END_FUNC()	LOG(LOG_DEBUG_MASK, "%s - finish\n", __func__)

#define	IS_DEBUG() ((LOG_DEBUG_MASK&logBitMap) ? 1 : 0)

#define FOR_EACH_IN_VEC(index, vector) for(usint index=0; index < vector.size(); index++)
#define	IS_VAL_IN_VECTOR(vec, val) (std::find(vec.begin(),vec.end(),val)!=vec.end() ? true:false)

#define ARRAY_ENTRIES(a) (sizeof(a)/sizeof(a[0]))


void dumpBuff(rawData& buff, char* title=NULL);
void dumpBuff(buffData& buff, char* title=NULL);
void dumpBuff(char* buff, usint len, char* title=NULL);
void dumpBuff(byte* buff, usint len, char* title);
void dumpBuff(std::string& str, char* title);
void dumpBuff(std::vector<uint8_t>& vector, char* title);

#define	DEBUG_DUMP_BUFF(buff, title) if ((LOG_DEBUG_MASK&logBitMap) != 0) dumpBuff(buff, (char*)title)
#define	DEBUG_DUMP_BUFF_LEN(buff, len, title) if ((LOG_DEBUG_MASK&logBitMap) != 0) dumpBuff(buff, len, (char*)title)

void buffToHexString(byte* buff, usint len, std::string& outStr);
void buffToHexString(buffData& buff, std::string& outStr);
void buffToHexString(usint& buff, std::string& outStr);

buffData stringHexToBuffData(const std::string& stringIn);
buffData stringHexToBuffData(const char* stringIn, const usint stringInLen);

bool		strToUsint(const std::string& stringIn, usint& outVal, bool isHex=false);
bool		strToBcd(const std::string& stringIn, buffData& outVal);

bool configUtils(void* configData);
bool configUtils(rawData* configData);
usint getLogBitMap();



ulint binaryBuffToUlint(buffData& binaryBuff);
usint binaryBuffToUsint (buffData& binaryBuff);

std::string  	ulintToString(ulint val);
std::string  	usintToString(usint val);

std::string& 	IntToHexStr(int val, std::string& outStr);
ulint 			bcdToUlint(buffData& bcdBuff);




ulint get_ms();

char* prv_itoa(int num, char* str, int base);

unsigned long read_ticks(void);

#endif /* SRC_UTILS_H_ */
