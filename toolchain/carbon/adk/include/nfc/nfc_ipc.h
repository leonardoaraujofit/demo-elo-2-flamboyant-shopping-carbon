/*************************************************************
* FILE NAME:   NFC_IPC.h                                     *
* MODULE NAME: NFC IPC                                       *
* PROGRAMMER:  Evgeny Sokolovsky                             *
* DESCRIPTION:                                               *
* REVISION:                                                  *
*************************************************************/
#ifndef _NFC_IPC_H_
#define _NFC_IPC_H_


typedef enum
{
  NFC_IPC,
  NFC_IPC_CANCEL,
  NFC_IPC_CALLBACK
} IPC_TYPE;


//Communication
//#define HEADER_SIZE         		4
//#define MAX_MESSAGE_SIZE     		2048
//#define MAX_DATA_SIZE        		(MAX_MESSAGE_SIZE-HEADER_SIZE)
//#define CALLBACK_MESSAGE_SIZE     	2048  ///< for unidirectional callback
#define	VFI_MAX_MSG_SIZE			10240+200 //10KB + possible overhead.
#define	MAX_PIPE_NAME				256
#define	PORT_BASE					5900
#define	LOOP_BACK_ADDR				"127.0.0.1"
#define MAX_CONECTION_RETRIES		10

/** start server and wait for client to connect
 * \param[in] type one of IPC_EMV_CT, IPC_EMV_CTLS
 * \return 0 if client has connected, -1 in case of error
 */
int ipc_waitConnect(IPC_TYPE type);

/** connect to server
 * \param[in] type one of IPC_EMV_CT, IPC_EMV_CTLS
 * \return 0 on success, -1 in case of error
 */
int ipc_connect(IPC_TYPE type);

/** send message
 * \param[in] msg message to be sent
 * \param[in] size size of message
 * \return 0 on success, -1 in case of error
 */
int ipc_send(byte *msg, usint size, IPC_TYPE type=NFC_IPC);

/** receive message
 * \param[out] rsp buffer for received message
 * \param[in] size in: size of buffer, out: size of received message
 * \return 0 on success, -1 in case of error
 */
int ipc_receive(byte *rsp, usint *size,IPC_TYPE type=NFC_IPC);

int ipc_receive(buffData &outBuff, IPC_TYPE type=NFC_IPC);

/** send unidirectional callback
 * \param[in] msg message to be sent
 * \param[in] size size of message
 * \return 0 on success, -1 in case of error
 */
int ipc_sendCallback(byte *msg, usint size);


/** receive unidirectional callback
 * \param[out] rsp buffer for received message
 * \param[in] size in: size of buffer, out: size of received message
 * \return 0 on success, -1 in case of error
 */
int ipc_receiveCallback(byte *rsp, usint *size);

void ipc_init(void);

void ipc_close_connection(IPC_TYPE type);

void ipc_close_all_connection(void);

#endif//_NFC_IPC_H_
