/*
 * VASkernel.h
 *
 *  Created on: 30 Jul 2015
 *      Author: SagiT1
 */

#ifndef SRC_VASKERNEL_H_
#define SRC_VASKERNEL_H_

#define MAX_APDU_BUFF_SIZE 255

typedef enum
{
	VASKERNEL_STATUS_SUCCESS	= 0,
	VASKERNEL_STATUS_DATA_ERROR	= -1,
	VASKERNEL_STATUS_COMM_ERROR = -2,
	VASKERNEL_STATUS_ERROR      = -3
}VASKERNEL_STATUS;

typedef enum
{
	TRANSMIT_APDU,

}VASKERNEL_ACTIONS;

#ifdef __cplusplus
extern "C" {
#endif
void getVersion(rawData* rdOutVersion);
#ifdef __cplusplus
}
#endif

class actionParams
{
public:
	byte	m_maxAttempts;
	rawData	m_rdInput;
	virtual ~actionParams();
};

class actionResults
{
public:
	buffData	m_bfResult;
	virtual ~actionResults();
};

class APDUparams: public actionParams
{

#define DEFAULT_CHUNK_SIZE	0xFF

public:
	APDUparams(const byte Class, const byte Inst, const byte P1, const byte P2, const rawData rdData, const byte expectedRetSize, const byte maxAtempts = 1);
	byte	m_Class;
	byte	m_Instruction;
	byte	m_P1;
	byte	m_P2;
	byte	m_expectedSize;
	byte	chunkSize;
};

class APDUresults: public actionResults
{
public:
	APDUresults();
	void operator=(APDU::APDU_response& apduRes);
	byte		m_sw1;
	byte		m_sw2;
	uint16_t	sw;
};

VASKERNEL_STATUS	vasKernelDoAction(VASKERNEL_ACTIONS action, actionParams& indata, actionResults& outdata);

VASKERNEL_STATUS	vasKernelDoAction(VASKERNEL_ACTIONS action, actionParams& actionParams,rawData& rd_dataToSend, buffData& bddataReseived, actionResults& outdata);







VASKERNEL_STATUS	transmitAPDU(APDUparams& inParams, APDUresults& outResults);

/*
typedef enum
{
	VASKERNEL_STATUS_SUCCESS	= 0,
	VASKERNEL_STATUS_DATA_ERROR	= -1,
	VASKERNEL_STATUS_COMM_ERROR = -2
}VASKERNEL_STATUS;

typedef enum
{
	COMMAND_CREATE_AND_SEND_APDU,
	COMMAND_CREATE_APDU_SELECT,//create APDU select Command into buffer
	COMMAND_SEND_APDU_MSG,//try to send & receive APDU message
	COMMAND_DO_APDU_SELECT,//create, send select & receive APDU message
	COMMAND_CREATE_APDU,//createAPDU command into buffer
}VASKERNEL_COMMAND;

typedef struct
{
	byte cla;
	byte ins;
	byte p1;
	byte p2;
}apduHeader;

typedef struct
{
	byte sw1;
	byte sw2;
}apduRetStatus;

#ifdef __cplusplus
extern "C" {
#endif

VASKERNEL_STATUS	vasKernelDoAction(VASKERNEL_COMMAND command, void* protocolHeader, rawData* rdDataIn, rawData* rdDataOut, void* protocolReturnStatus);

#include <ctlsl1/picc.h>

#ifdef __cplusplus
}
#endif
*/
#endif /* SRC_VASKERNEL_H_ */
