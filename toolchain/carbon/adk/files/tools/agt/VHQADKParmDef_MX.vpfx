<?xml version="1.0" encoding="utf-8"?>
<vhq:ParameterElements xmlns:vhq="http://www.verifone.com/VHQ/Parameter/schema" version="1.0">
	<ParameterFile Name="VHQ_InfoSvc_CFG.xml" Format="ADKH">
		<Container Name="Server" DisplayName="Server" Access="Basic">
			<Param Name="URL_Root" DisplayName="URL Root" Default="https://vhq.verifone.com/MessagingServer/MessageHandler.asmx" Description="URL or IP address of the VHQ server's webservice the agent will send its messages to" Access="Basic">
				<ValueType Type="String">
					<String MinLen="0" MaxLen="255" ValidChars="([a-zA-Z0-9.:\/@#$%*_-]+$)" ErrorOnValidationFailure="Use valid HTTPS URL. Examples: https://domain.com or https://127.0.0.1:port"/>
				</ValueType>
			</Param>
			<Param Name="SSL_Validate_Hostname" DisplayName="SSL Validate Hostname" Default="1" Description="When this is TRUE, the agent will validate the hostname in the SSL certificate received from the server matches the hostname that it is actually talking to. NOTE: Set to FALSE if using a host IP address rather than a URL." Access="Basic">
				<ValueType Type="Enumeration">
					<Enumeration>
						<EnumItem Name="Don't Validate Hostname" Value="0"/>
						<EnumItem Name="Validate Hostname" Value="1"/>
					</Enumeration>
				</ValueType>
			</Param>
			<Param Name="Validate_Protocol_Hostname" DisplayName="Validate Protocol Hostname" Default="1" Description="If this is TRUE, then the device will verify that the common name (or Subject Alternative Name) contained in VHQSrvPubKey.crt matches the server name specified in URL Root parameter when exchanging keys.  The device will not exchange keys if this is set and the common name (or SAN) in VHQSrvPubKey.crt does not match the server name in URL Root parameter." Access="Basic">
				<ValueType Type="Enumeration">
					<Enumeration>
						<EnumItem Name="Don't Validate Protocol Hostname" Value="0"/>
						<EnumItem Name="Validate Protocol Hostname" Value="1"/>
					</Enumeration>
				</ValueType>
			</Param>
			<Param Name="Validate_Peer_Certificate" DisplayName="Validate Peer Certificate" Default="1" Description="When this is TRUE, the agent will validate the SSL certificate received from the server against the CA certificates stored with the agent." Access="Basic">
				<ValueType Type="Enumeration">
					<Enumeration>
						<EnumItem Name="Don't Validate Peer Certificate" Value="0"/>
						<EnumItem Name="Validate Peer Certificate" Value="1"/>
					</Enumeration>
				</ValueType>
			</Param>
			<Param Name="Contact_server_via_proxy" DisplayName="Contact server via proxy" Default="0" Description="Use proxy server for communicating to webservice" Access="Basic">
				<ValueType Type="Enumeration">
					<Enumeration>
						<EnumItem Name="Don't Contact Server via Proxy" Value="0"/>
						<EnumItem Name="Contact Server via Proxy" Value="1"/>
					</Enumeration>
				</ValueType>
			</Param>
			<Param Name="WebProxyURL" DisplayName="WebProxyURL" Default="" Description="URL or IP address for web proxy" Access="Basic">
				<ValueType Type="String">
					<String MinLen="0" MaxLen="255" ValidChars="([a-zA-Z0-9.:\/@#$%*_-]+$)" ErrorOnValidationFailure="Use valid URL. Examples: https://domain.com or wwww.domian.com or 127.0.0.1:port"/>
				</ValueType>
			</Param>
			<Param Name="WebProxyTunnelEnable" DisplayName="WebProxyTunnelEnable" Default="0" Description="Setting this to FALSE allows for alternate SSL certificate for communication with proxy. Setting this to TRUE will allow communication to the proxy in the clear until after the HTTP connect and it switches to HTTPS." Access="Basic">
				<ValueType Type="Enumeration">
					<Enumeration>
						<EnumItem Name="Disable Web Procy Tunnel" Value="0"/>
						<EnumItem Name="Enable Web Proxy Tunnel" Value="1"/>
					</Enumeration>
				</ValueType>
			</Param>
			<Param Name="FTPProxyURL" DisplayName="FTPProxyURL" Default="" Description="Proxy server address for file transfers.  Used for all file transfers, not just via FTP protocol (this is what's used for https download, for example)." Access="Basic">
				<ValueType Type="String">
					<String MinLen="0" MaxLen="255" ValidChars="([a-zA-Z0-9.:\/@#$%*_-]+$)" ErrorOnValidationFailure="Use valid URL. Examples: ftps://wwww.domian.com or wwww.domian.com or 127.0.0.1:port"/>
				</ValueType>
			</Param>
			<Param Name="FtpForcePASV" DisplayName="FtpForcePASV" Default="1" Description="Only applicable for FTP downloads (currently not supported)." Access="Basic">
				<ValueType Type="Enumeration">
					<Enumeration>
						<EnumItem Name="Don't force FTP PASV Mode" Value="0"/>
						<EnumItem Name="Force FTP PASV mode" Value="1"/>
					</Enumeration>
				</ValueType>
			</Param>
			<Param Name="VCZontalkPort" DisplayName="VCZontalkPort" Default="8013" Description="Zontalk TCPIP port" Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="0" MaxVal="65535"/>
				</ValueType>
			</Param>
		</Container>
		<Container Name="VHQ" DisplayName="VHQ" Access="Basic">
			<Param Name="Operating_Mode" DisplayName="Operating Mode" Default="Direct" Description="'Direct' for terminal Ethernet; 'UseProxy' for proxy connected; 'UseCradle' for PWM on charge cradle; 'ADK' for communications via ADK comm component: requires additional config files. This value only applies to VX and V/OS devices; 'Dialup' for modem communications, other parameters are required. This value only applies to VX devices" Access="Basic">
				<ValueType Type="Enumeration">
					<Enumeration>
						<EnumItem Name="Direct Mode" Value="Direct"/>
						<EnumItem Name="Proxy Connected" Value="UseProxy"/>
						<EnumItem Name="PWM on Charge Cradle" Value="UseCradle"/>
						<EnumItem Name="ADK Mode" Value="ADK"/>
						<EnumItem Name="Dial (Modem) Mode" Value="Dialup"/>
					</Enumeration>
				</ValueType>
			</Param>
			<Param Name="ConnectWhileDockedOnly" DisplayName="ConnectWhileDockedOnly" Default="0" Description="'0' to allow communications anytime required; '1' to allow communications only when device is in docking charger station. This value only applies to mobile devices." Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="0" MaxVal="1"/>
				</ValueType>
			</Param>
			<Param Name="Alert_Mask" DisplayName="Alert Mask" Default="0" Description="Bit Fields to Enable Different Device Alerts/Events: 0x2 - RAM Low Event Enable; 0x4 - Flash Low Event Enable; 0x8 - Reboot Event Enable; 0x10 - Low Battery Event Enable; 0x20 - Dock In Event Enable; 0x80 - VSP Encryption Status Change Event Enable; 0x100 - Dock Out Event Enable;  Note: 0x1 bit used to control the tamper event.In release 1.3.20 and later tamper events will ALWAYS be reported because they require a new key exchange with the server.So the 0x1 bit is unused in the Alert Mask for release 1.3.20 and later." Access="Basic">
				<ValueType Type="Enumeration">
					<Enumeration>
						<EnumItem Name="Enable Tamper Event" Value="1"/>
						<EnumItem Name="Enable Low RAM Event" Value="2"/>
						<EnumItem Name="Enable Low Flash Event" Value="4"/>
						<EnumItem Name="Enable Reboot Event" Value="8"/>
						<EnumItem Name="Enable Low Battery Event" Value="16"/>
						<EnumItem Name="Enable DockIn Event" Value="32"/>
						<EnumItem Name="Reserved - internal use" Value="64"/>
						<EnumItem Name="Enable VSP Encryption Status Change Event" Value="128"/>
						<EnumItem Name="Enable Dock Out Event" Value="256"/>
						<EnumItem Name="Enable All Events" Value="511"/>
					</Enumeration>
				</ValueType>
			</Param>
			<Param Name="Free_Ram_Alert_Threshold" DisplayName="Free Ram Alert Threshold" Default="15" Description="Percent of free RAM for when to send RAM Low Event" Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="0" MaxVal="99"/>
				</ValueType>
			</Param>
			<Param Name="Free_Flash_Alert_Threshold" DisplayName="Free Flash Alert Threshold" Default="15" Description="Percent of free FLASH for when to send FLASH Low Event" Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="0" MaxVal="99"/>
				</ValueType>
			</Param>
			<Param Name="Low_Battery_Threshold" DisplayName="Low Battery Threshold" Default="27" Description="The voltage level (multiplied by 10) to send the low battery event.  If the voltage level drops to this level or below, the low battery event will be sent." Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="0" MaxVal="40"/>
				</ValueType>
			</Param>
			<Param Name="Safemode" DisplayName="Safemode" Default="0" Description="Can be used to disable certain ADK VHQ features. This should only be changed in rare instances. Please contact ADK VHQ development team for more info." Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="0" MaxVal="8192"/>
				</ValueType>
			</Param>
			<Param Name="MaintenanceStart" DisplayName="MaintenanceStart" Default="2200" Description="Maintenance window start time in the following format: HHMM; Where HH = 00 - 23; MM = 00 - 59. Note that if MaintenanceStart equals MaintenanceEnd, that indicates a 24 hour Maintenance Window." Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="0" MaxVal="2359"/>
				</ValueType>
			</Param>
			<Param Name="MaintenanceEnd" DisplayName="MaintenanceEnd" Default="0600" Description="Maintenance window end time in the following format: HHMM; Where	HH = 00 - 23; MM = 00 - 59. Note that if MaintenanceStart equals MaintenanceEnd, that indicates a 24 hour Maintenance Window." Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="0" MaxVal="2359"/>
				</ValueType>
			</Param>
			<Param Name="MaintenanceDays" DisplayName="MaintenanceDays" Default="*" Description="Maintenance Days for the device.  Concatenate the following strings together or use * for all days: Su = Sunday; M = Monday; Tu = Tuesday; W = Wednesday; Th = Thursday; F = Friday; Sa = Saturday; So for example, Maintenance days on Monday, Tuesday, Thursday and Friday would set this value to 'MTuThF'; For a monthly maintenance window, you would use 'm' + the day of the month followed by 'm' again.For instance, 'm1m' in this field would indicate the first of each month. Month days can be concatenated together with spaces in between so 'm1m m15m' would mean the 1st and 15th of each month are maintenance days. NOTE: this value is case sensitive so 'M' should be used for Monday, while 'm' should be used for month days. Note that by leaving this field blank, you can disable the Maintenance Window" Access="Basic">
				<ValueType Type="String">
					<String MinLen="0" MaxLen="15" ValidChars="(^[SuMTWhFam0-9 ]+$)" ErrorOnValidationFailure="Use: Su = Sunday; M = Monday; Tu = Tuesday; W = Wednesday; Th = Thursday; F = Friday; Sa = Saturday; m15m for days; Spaces to separate"/>
				</ValueType>
			</Param>
			<Param Name="CustomerId" DisplayName="CustomerId" Default="Verifone-home" Description="The Customer ID assigned to the customer" Access="Basic">
				<ValueType Type="String">
					<String MinLen="3" MaxLen="15" ValidChars="(^[a-zA-Z0-9_-]+$)" ErrorOnValidationFailure="Use ASCII characters and '_' '-'"/>
				</ValueType>
			</Param>
			<Param Name="HeartbeatFreq" DisplayName="HeartbeatFreq" Default="3600" Description="This specifies the heartbeat frequency in seconds to be used for all communication interfaces except for Dial-Up and GPRS/3G." Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="0" MaxVal="99999999"/>
				</ValueType>
			</Param>
			<Param Name="Dial_HeartbeatFreq" DisplayName="Dial HeartbeatFreq" Default="86400" Description="When Dial-Up communication interface is used, then this indicates the  frequency in seconds to be used." Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="0" MaxVal="99999999"/>
				</ValueType>
			</Param>
			<Param Name="GPRS3G_HeartbeatFreq" DisplayName="GPRS3G HeartbeatFreq" Default="86400" Description="When GPRS/3G communication interface is used, then this indicates the  frequency in seconds to be used." Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="0" MaxVal="99999999"/>
				</ValueType>
			</Param>
			<Param Name="HBFailureRetryThreshold" DisplayName="HBFailureRetryThreshold" Default="7200" Description="The HB frequency threshold in seconds, at which the agent will retry a HB failure instead of skipping it and waiting for the next HB." Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="300" MaxVal="99999999"/>
				</ValueType>
			</Param>
			<Param Name="HBFailureRetryFreq" DisplayName="HBFailureRetryFreq" Default="300" Description="The rate (in seconds) at which a failed HB will be retried.  This is also the rate at which a failed event due to network connection failure will be retried.  This does not apply if a HB or an event is successful, and does not apply for heartbeat retries if HeartbeatFreq is less than HBFailureRetryThreshold" Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="60" MaxVal="99999999"/>
				</ValueType>
			</Param>
			<Param Name="EventLoopRate" DisplayName="EventLoopRate" Default="10" Description="ADK VHQ Event Scheduler Polling interval, in seconds; NOTE: This parameter only applies to VX devices for now. V/OS and RFS devices will not use this parameter" Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="1" MaxVal="300"/>
				</ValueType>
			</Param>
			<Param Name="TerminalID" DisplayName="TerminalID" Default="" Description="Can be used in conjunction with DeviceIDBackupSource to define the DeviceId that will get sent to the server.  If DeviceIDBackupSource is set to 'VHQConfig', then this parameter will be sent as the DeviceId; Note: for backward compatibility, this is TerminalID and not DeviceID." Access="Basic">
				<ValueType Type="String">
					<String MinLen="0" MaxLen="63" ValidChars="(^[a-zA-Z0-9]+$)" ErrorOnValidationFailure="Use any ASCII characters and numbers"/>
				</ValueType>
			</Param>
			<Param Name="DeviceIDBackupSource" DisplayName="DeviceIDBackupSource" Default="Disabled" Description="Where to get the DeviceId parameter from if it is not provided by an application.  If an application provides the DeviceId, then this parameter is ignored; 'Disabled' - Do not send DeviceId unless provided by the application; 'IPAddress' - Send the IP Address of the device as the DeviceId; 'ZT' - Send the *ZT configuration parameter as the DeviceId;  'VHQConfig' - The TerminalId parameter described above will be sent as the DeviceId;  Note: 'ZT' only applies to VX devices and will be ignored on other platforms." Access="Basic">
				<ValueType Type="Enumeration">
					<Enumeration>
						<EnumItem Name="Disabled" Value="Disabled"/>
						<EnumItem Name="IP Address is backup device ID" Value="IPAddress"/>
						<EnumItem Name="*ZT parameter is backup device ID" Value="ZT"/>
						<EnumItem Name="VHQ TerminalID is backup device ID" Value="VHQConfig"/>
					</Enumeration>
				</ValueType>
			</Param>
			<Param Name="AppRegWaitTime" DisplayName="AppRegWaitTime" Default="0" Description="The amount of time (in seconds) for ADK VHQ agent to wait for applications to register with it before sending its first heartbeat. When applications are registered, they can provide application specific data to the agent that can be sent to the server in the agent's messages.  Examples of this would be the terminal ID or the lane/store information which are controlled by the applications." Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="0" MaxVal="300"/>
				</ValueType>
			</Param>
			<Param Name="MaintenanceDownloadStaggering" DisplayName="MaintenanceDownloadStaggering" Default="1" Description="Controls whether maintenance download staggering is enabled on the agent or not.  When enabled, maintenance downloads will be scheduled to randomly start sometime before the end of the maintenance window.  When disabled, the maintenance download will start as specified by the server." Access="Basic">
				<ValueType Type="Enumeration">
					<Enumeration>
						<EnumItem Name="Don't Stagger Maintenance Downloads" Value="0"/>
						<EnumItem Name="Stagger Maintenance Downloads" Value="1"/>
					</Enumeration>
				</ValueType>
			</Param>
			<Param Name="Download_Maximum_Attempts" DisplayName="Download Maximum Attempts" Default="60" Description="How many times to attempt a file download to the device, including the first initial download attempt." Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="1" MaxVal="9999"/>
				</ValueType>
			</Param>
			<Param Name="Download_Network_Timeout" DisplayName="Download Network Timeout" Default="30" Description="How long in seconds to wait for no packets to be sent or received over the network before trying to recover and retrying the download." Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="1" MaxVal="30"/>
				</ValueType>
			</Param>
			<Param Name="Download_Retry_Timeout" DisplayName="Download Retry Timeout" Default="15" Description="Time in seconds between the Network Timeout and when to try and recover communication to continue with the download" Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="1" MaxVal="72000"/>
				</ValueType>
			</Param>
			<Param Name="Download_Receive_Buffer_Size" DisplayName="Download Receive Buffer Size" Default="0" Description="Indicates the Receive Buffer Size to be used when receiving data during downloads. If the value is 0 then the default Receive Buffer Size will not be changed from the default in libCURL which is 16384 bytes (16K). If the value is non-zero and in the valid range, the Receive Buffer Size will be changed to the new value.  Since the default size is the maximum value of 16K, then it only makes sense to change this value if you want the Receive Buffer Size to be smaller than 16K.  This is necessary sometimes when the communication interface is USB or Serial through the IP over USB bridge.  Reducing the size of the Receive Buffer Size effectively reduces the TCP Window size during downloads so timeouts don't occur from the server side." Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="512" MaxVal="16384"/>
				</ValueType>
			</Param>
			<Param Name="AppIfc_Event_Response_Timeout" DisplayName="AppIfc Event Response Timeout" Default="30" Description="The timeout in seconds, for the agent when waiting for a response to an event sent to an application via the App Interface.  This timeout applies to these events which the agent waits for a response for:  TMS_EVT_SET_PARM_LIST,  TMS_EVT_SET_APP_INFO,  TMS_EVT_GET_FILE,  TMS_EVT_PUT_FILE, TMS_EVT_DO_TRANSACTION, TMS_EVT_DEL_FILE." Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="5" MaxVal="600"/>
				</ValueType>
			</Param>
			<Param Name="AppIfc_SetAppState_Timeout" DisplayName="AppIfc SetAppState Timeout" Default="300" Description="The timeout in seconds, for the agent when waiting for a response to the TMS_EVT_SET_APP_STATE event sent to an application via the App Interface." Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="5" MaxVal="3600"/>
				</ValueType>
			</Param>
			<Param Name="AppIfc_Busy_Response_Timeout" DisplayName="AppIfc Busy Response Timeout" Default="300" Description="The timeout in seconds, for the agent when it is waiting for the application to set its state to free, after the application sets it state to busy." Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="5" MaxVal="50000"/>
				</ValueType>
			</Param>
			<Param Name="AppIfc_Proceed_On_Failure" DisplayName="AppIfc Proceed On Failure" Default="1" Description="If the agent is unable to get approval for an event from an application due to the application timing out or some other error, should the agent continue with the event or fail the event." Access="Basic">
				<ValueType Type="Enumeration">
					<Enumeration>
						<EnumItem Name="Don't Proceed on App Interface Failure" Value="0"/>
						<EnumItem Name="Proceed on App Interface Failure" Value="1"/>
					</Enumeration>
				</ValueType>
			</Param>
			<Param Name="EventPostponedRetryDelay" DisplayName="EventPostponedRetryDelay" Default="1800" Description="If an Operation gets 'POSTPONED' (most likely by an application), this is the ammount of time the agent will wait before retrying the Operation" Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="300" MaxVal="14400"/>
				</ValueType>
			</Param>
			<Param Name="EventPostponedRetryTimeout" DisplayName="EventPostponedRetryTimeout" Default="43200" Description="If an Operation gets 'POSTPONED' (most likely by an application), this is the MAXIMUM ammount of time the agent will continue to retry the operation before sending a FAILURE response" Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="3600" MaxVal="86400"/>
				</ValueType>
			</Param>
			<Param Name="NoIPAddrOK" DisplayName="NoIPAddrOK" Default="0" Description="If set to 1, it will allow the Key Exchange Message (KEM) to be sent even if no ip address has been retrieved.  It is set to 0 by default and requires an ip address for the KEM to be sent.  This is normally used for testing purposes" Access="Basic">
				<ValueType Type="Enumeration">
					<Enumeration>
						<EnumItem Name="No IP Address causes FAILURE" Value="0"/>
						<EnumItem Name="No IP Address is OK" Value="1"/>
					</Enumeration>
				</ValueType>
			</Param>
			<Param Name="AgentLoggingLevel" DisplayName="AgentLoggingLevel" Default="Standard" Description="Used to adjust what messages get logged to the ADK VHQ logs.  By default, only some messages will get logged to the ADK VHQ logs.  If users are running into issues, they can change this level to try and help debug what is happening." Access="Basic">
				<ValueType Type="Enumeration">
					<Enumeration>
						<EnumItem Name="Standard Logging Level" Value="Standard"/>
						<EnumItem Name="Alert Logging Level" Value="Alert"/>
						<EnumItem Name="Critical Logging Level" Value="Critical"/>
						<EnumItem Name="Error Logging Level" Value="Error"/>
						<EnumItem Name="Warning Logging Level" Value="Warning"/>
						<EnumItem Name="Notice Logging Level" Value="Notice"/>
						<EnumItem Name="Info Logging Level" Value="Info"/>
						<EnumItem Name="Full Debug Logging Level" Value="Debug"/>
					</Enumeration>
				</ValueType>
			</Param>
			<Param Name="AgentMaxLogSize" DisplayName="AgentMaxLogSize" Default="30" Description="The maximum size in KB that the ADK VHQ logs grow to before being rotated out.  Keep in mind that the agent rotates its log files from vhqlog to vhqlog.1 to vhqlog.2 in addition to keeping 2 error logs (vhqlog.error and vhqlog.error.1) of this size so the total log size of ADK VHQ logs can be 5 times this value.; NOTE: This parameter does not apply to VX devices.The log size is configured in syslog.conf for VX devices." Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="30" MaxVal="2000"/>
				</ValueType>
			</Param>
			<Param Name="ResendInvalidNonceMessages" DisplayName="ResendInvalidNonceMessages" Default="1" Description="If true the agent will update the NONCE in a message and resend it if the server responds with E_INVALID_NONCE" Access="Basic">
				<ValueType Type="Enumeration">
					<Enumeration>
						<EnumItem Name="Don't Resend messages in response to INVALID_NONCE" Value="0"/>
						<EnumItem Name="Resend messages in response to INVALID_NONCE" Value="1"/>
					</Enumeration>
				</ValueType>
			</Param>
			<Param Name="DisconnectOverrideWaitSeconds" DisplayName="DisconnectOverrideWaitSeconds" Default="30" Description="Maximum time the agent will allow to override the disconnect of the agent.  If the next agent event is scheduled to run within this timeframe, the agent will not disconnect its connection upon completion of the current event" Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="0" MaxVal="3600"/>
				</ValueType>
			</Param>
			<Param Name="ConnectRetries" DisplayName="ConnectRetries" Default="3" Description="How many times to retry during connection establishment.  Retry is per connection type.  If multiple connection types are in the network profile, etc type is retried this number of times, before the next connection type is tried." Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="1" MaxVal="10"/>
				</ValueType>
			</Param>
			<Param Name="VHQParamFileReporting" DisplayName="VHQParamFileReporting" Default="Current" Description="This setting determines how the agent will report its parameter files, especially when using multiple server instances." Access="Basic">
				<ValueType Type="Enumeration">
					<Enumeration>
						<EnumItem Name="Current Instance Only" Value="Current"/>
						<EnumItem Name="Current and Primary Instance" Value="Current+"/>
						<EnumItem Name="Primary Instance Only" Value="Primary"/>
						<EnumItem Name="Current Instance with Primary File Names" Value="PrimaryOverride"/>
						<EnumItem Name="All parameter files" Value="All"/>
						<EnumItem Name="No Parameter Reporting" Value="None"/>
					</Enumeration>
				</ValueType>
			</Param>
			<Param Name="NoDataTransmit" DisplayName="NoDataTransmit" Default="120" Description="This setting determines libCURL connection timeout. Slow connections may require large value. DO NOT CHANGE THIS VALUE FROM DEFAULT VALE UNLESS INSTRUCTED TO DO SO BY VERIONE." Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="30" MaxVal="300"/>
				</ValueType>
			</Param>
			<Param Name="PinpadComport" DisplayName="PinpadComport" Default="usb" Description="Type of communication port to which Pinpad is connected.  Settings: usb, serial" Access="Basic">
				<ValueType Type="String">
					<String MinLen="0" MaxLen="31" ValidChars="(^[a-zA-Z0-9]+$)" ErrorOnValidationFailure="Use any ASCII characters and numbers"/>
				</ValueType>
			</Param>
			<Param Name="CURLErrorRetries" DisplayName="CURLErrorRetries" Default="2" Description="If a libCURL error occurs when contacting the server, this is the number of times the message will be retried" Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="0" MaxVal="50"/>
				</ValueType>
			</Param>
			<Param Name="CURLRetryDelay" DisplayName="CURLRetryDelay" Default="10" Description="If a libCURL error occurs when contacting the server, and CURLErrorRetries is not 0, this is the number of seconds to delay before retrying the message. Configured value can be reduced by up to 20% randomly on each retry attempt" Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="0" MaxVal="300"/>
				</ValueType>
			</Param>
			<Param Name="VCCallVHQHomeFreq" DisplayName="VCCallVHQHomeFreq" Default="86400" Description="When in VeriCenter mode, this parameter controls how often the agent will ping Verifone-Home VHQ Instance" Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="0" MaxVal="99999999"/>
				</ValueType>
			</Param>
			<Param Name="EventWatchdogTime" DisplayName="EventWatchdogTime" Default="1200" Description="The ammount of time (in seconds) before an unresponsive operation will be cancelled.  Note that as long as the operation is responsive, this timer will get periodically reset.  If an operation becomes unresponsive, this is how long before event scheduler's failsafe mechanisms kick in. NOTE: This value MUST be greater than all 3 AppIfc timeout values or the watchdog may kill processes that are still in a known good state" Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="600" MaxVal="3600"/>
				</ValueType>
			</Param>
			<Param Name="MaxDLFlashUtilization" DisplayName="MaxDLFlashUtilization" Default="90" Description="The maximum percentage of available flash that the agent should use for download storage" Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="1" MaxVal="100"/>
				</ValueType>
			</Param>
			<Param Name="TSCalLoggingEnabled" DisplayName="TSCalLoggingEnabled" Default="0" Description="Flag to tell whether to log touchscreen calibration values during the maintenance window,  Note that this applies to V/OS and V/OS2 devices only, and more specifcally should only be enabled on Carbon devices when directed by Verifone" Access="Basic">
				<ValueType Type="Enumeration">
					<Enumeration>
						<EnumItem Name="Don't log touchscreen calibration values during maintenance window" Value="0"/>
						<EnumItem Name="Enable logging of touchscreen calibration values during maintenance window" Value="1"/>
					</Enumeration>
				</ValueType>
			</Param>
			<Param Name="UseTimeZoneFromServer" DisplayName="UseTimeZoneFromServer" Default="1" Description="Allows to apply timezone information sent by server" Access="Basic">
				<ValueType Type="Enumeration">
					<Enumeration>
						<EnumItem Name="Use local settings" Value="0"/>
						<EnumItem Name="Use timezone form server" Value="1"/>
					</Enumeration>
				</ValueType>
			</Param>
			<Param Name="AppApprovalPerOpSet" DisplayName="AppApprovalPerOpSet" Default="1" Description="Flag to tell whether application approval applies to an entire Download Operation Set" Access="Basic">
				<ValueType Type="Enumeration">
					<Enumeration>
						<EnumItem Name="Application approval is required for all downloads/installations in an OperationSet" Value="0"/>
						<EnumItem Name="Application approval of the first download/install applies to the whole Operaiton Set" Value="1"/>
					</Enumeration>
				</ValueType>
			</Param>
			<Param Name="SetAppInfoReqPeriod" DisplayName="SetAppInfoReqPeriod" Default="86400" Description="The ammount of time (in seconds) between TMS_EVT_SET_APP_INFO agent requests to application.  Whatever parameters the app supplies to the agent will be cached in between the requests" Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="0" MaxVal="86400"/>
				</ValueType>
			</Param>
			<Param Name="Skip_connect_for_ADK" DisplayName="Skip connect for ADK" Default="0" Description="If true the agent will not call com_AttachNetwork() and com_DetachNetwork() for operating mode=ADK" Access="Basic">
				<ValueType Type="Enumeration">
					<Enumeration>
						<EnumItem Name="Call com_AttachNetwork() and com_DetachNetwork()" Value="0"/>
						<EnumItem Name="Don't call com_AttachNetwork() and com_DetachNetwork()" Value="1"/>
					</Enumeration>
				</ValueType>
			</Param>
			<Param Name="NetworkConfigWaitTime" DisplayName="NetworkConfigWaitTime" Default="30" Description="Wait time (in seconds) to allow communication interface obtain network information. WiFi network may require big value" Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="0" MaxVal="300"/>
				</ValueType>
			</Param>
		</Container>
		<Container Name="Proxy_Agent" DisplayName="Proxy Agent" Access="Basic">
			<Param Name="SerialRouterLogging" DisplayName="SerialRouterLogging" Default="0" Description="0= logging off, otherwise 1-3 is logging level.  3 is more detail; OBSOLETE / UNUSED" Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="0" MaxVal="3"/>
				</ValueType>
			</Param>
			<Param Name="Physical_Comm_Port" DisplayName="Physical Comm Port" Default="/dev/ttyGS0" Description="For OperatingMode=UseProxy, which port is the physical port to use" Access="Basic">
				<ValueType Type="String">
					<String MinLen="0" MaxLen="31" ValidChars="(^[a-zA-Z0-9\/_-]+$)" ErrorOnValidationFailure="Use ASCII characters and '/' '_' '-'"/>
				</ValueType>
			</Param>
			<Param Name="Enable_Port_Sharing_Protocol" DisplayName="Enable Port Sharing Protocol" Default="0" Description="Share the serial port" Access="Basic">
				<ValueType Type="Enumeration">
					<Enumeration>
						<EnumItem Name="Don't Share the Serial Port" Value="0"/>
						<EnumItem Name="Share the Serial Port" Value="1"/>
					</Enumeration>
				</ValueType>
			</Param>
			<Param Name="PosAgentBaudRate" DisplayName="PosAgentBaudRate" Default="115200" Description="If using POS agent, what baud rate" Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="0" MaxVal="99999999"/>
				</ValueType>
			</Param>
			<Param Name="PPPSetting" DisplayName="PPPSetting" Default="persist noauth lock novj defaultroute usepeerdns mtu 1400 mru 1400 %s" Description="Config options for PPPD.  Note that the last item must be %s for formatting in other parameters" Access="Basic">
				<ValueType Type="String">
					<String MinLen="0" MaxLen="319" ValidChars="(^[a-zA-Z0-9\(\)\[\]\/\\| _%?,.:;*$%^#@`!~+=-]+$)" ErrorOnValidationFailure="Use ASCII characters"/>
				</ValueType>
			</Param>
			<Param Name="ConnCheckInterval" DisplayName="ConnCheckInterval" Default="90" Description="How often to check ppp connection status when in UseProcy OperatingMode" Access="Basic">
				<ValueType Type="Numeric">
					<Numeric MinVal="0" MaxVal="1000"/>
				</ValueType>
			</Param>
			<Param Name="PPPUsername" DisplayName="PPPUsername" Default="" Description="Username for Dialup PPP account" Access="Basic">
				<ValueType Type="String">
					<String MinLen="0" MaxLen="31" ValidChars="(^[a-zA-Z0-9\(\)\[\]\/\\| _%?,.:;*$%^#@`!~+=-]+$)" ErrorOnValidationFailure="Use ASCII characters"/>
				</ValueType>
			</Param>
			<Param Name="PPPpassword" DisplayName="PPPpassword" Default="" Description="Password for Dialup PPP account" Access="Basic">
				<ValueType Type="String">
					<String MinLen="0" MaxLen="31" ValidChars="(^[a-zA-Z0-9\(\)\[\]\/\\| _%?,.:;*$%^#@`!~+=-]+$)" ErrorOnValidationFailure="Use ASCII characters"/>
				</ValueType>
			</Param>
			<Param Name="PPPphone" DisplayName="PPPphone" Default="" Description="Phone number for Dialup PPP" Access="Basic">
				<ValueType Type="String">
					<String MinLen="0" MaxLen="31" ValidChars="(^[0-9\(\)+-]+$)" ErrorOnValidationFailure="Use phone number in 0000000 or +0(000)000-000 format"/>
				</ValueType>
			</Param>
			<Param Name="PPPphoneALT" DisplayName="PPPphoneALT" Default="" Description="Alternate number" Access="Basic">
				<ValueType Type="String">
					<String MinLen="0" MaxLen="31" ValidChars="(^[0-9\(\)+-]+$)" ErrorOnValidationFailure="Use phone number in 0000000 or +0(000)000-000 format"/>
				</ValueType>
			</Param>
			<Param Name="PPPauthtype" DisplayName="PPPauthtype" Default="1" Description="Authorization method, 0=none, 1=PAP, 2=CHAP, 3=MSCHAP" Access="Basic">
				<ValueType Type="Enumeration">
					<Enumeration>
						<EnumItem Name="No Auth" Value="0"/>
						<EnumItem Name="PAP" Value="1"/>
						<EnumItem Name="CHAP" Value="2"/>
						<EnumItem Name="MSCHAP" Value="3"/>
					</Enumeration>
				</ValueType>
			</Param>
		</Container>
		<OutFileFormat>
			<f:format xmlns:f="http://www.verifone.com/adk/information-service/schema/datafile" filter="xmlfile" library_version="0.1.0">
				<record_type name="registry" topology="hierarchical" parent_id="id" child_link="parent_id">
					<xattr name="path" value="/data/vhq/key" />
					<xattr name="child_path" value="key" />
					<field name="id" type="INTEGER">
						<xattr name="path" value="id" />
					</field>
					<field name="parent_id" type="INTEGER" >
						<xattr name="path" value="parent_id" />
					</field>
					<field name="node_name" type="TEXT" >
						<xattr name="path" value="node_name" />
					</field>
					<field name="value" type="TEXT" >
						<xattr name="path" value="value" />
					</field>
				</record_type>
			</f:format>
		</OutFileFormat>
	</ParameterFile>
</vhq:ParameterElements>