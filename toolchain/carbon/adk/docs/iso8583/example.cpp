#include "dl_iso8583.h"

int main(int argc, char *argv[])
{
  DL_ISO8583_FIELD_DEF fields[] = { {kDL_ISO8583_ANS, 10, kDL_ISO8583_LLVARA},
                                    {kDL_ISO8583_ANS, 10, kDL_ISO8583_LLVAR },
                                    {kDL_ISO8583_ANS,  3, kDL_ISO8583_FIXED } };
  DL_ISO8583_HANDLER iso_handler = { fields, 3 };
  DL_ISO8583_MSG_HANDLER msg_handler = { iso_handler, NULL, 0 };
  ISO8583_MSG *msg = new ISO8583_MSG(&msg_handler);
  unsigned char buf[200];
  unsigned short len = 0;
  unsigned short errFieldIdx = 0;
  int ret = 0;
  unsigned char *ptr;
  
  ret = msg->SetField_Str(0, "12345");
  printf("msg->SetField_Str: %d\n", ret);

  ret = msg->SetField_Str(1, "1234");
  printf("msg->SetField_Str: %d\n", ret);

  ret = msg->SetField_Str(2, "123");
  printf("msg->SetField_Str: %d\n", ret);
  
  ret = msg->Pack(buf, sizeof(buf), &len, &errFieldIdx);
  printf("msg->Pack: %d\n", ret);
  
  if (ret == kDL_ERR_NONE)
  {
    printf("msg [%d]: ", len);
    for (int i = 0; i < len; ++i)
      printf("%02x", buf[i]);
    printf("\n");

    ret = msg->Unpack(buf, len);
    printf("msg->Unpack: %d\n", ret);

    if (ret == kDL_ERR_NONE)
    {
      ret = msg->GetField_Str(0, &ptr);
      if (ret == kDL_ERR_NONE)
      {
        printf("GetField_Str: %s \n", (char *) ptr);
      }
      ret = msg->GetField_Str(1, &ptr);
      if (ret == kDL_ERR_NONE)
      {
        printf("GetField_Str: %s \n", (char *) ptr);
      }
      ret = msg->GetField_Str(2, &ptr);
      if (ret == kDL_ERR_NONE)
      {
        printf("GetField_Str: %s \n", (char *) ptr);
      }
    }
  }
  else
  {
    printf("error in field %d\n", errFieldIdx);
  }
 
  delete msg;
  
  return 0;
}
