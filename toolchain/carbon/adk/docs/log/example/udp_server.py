import socket
import sys

def main():
    argc = len(sys.argv)
    PORT = 801
    if(argc > 1): 
        PORT = int(sys.argv[1])
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((socket.gethostbyname(socket.gethostname()), PORT))
    print(socket.gethostbyname(socket.gethostname()))
    
    while True:
        sock.settimeout(None)
        print("Standing by")
        data, addr = sock.recvfrom(1024)
        if not data:
            break
        print("Data: ",data.decode("UTF-8"), " from ", addr)

if __name__ == "__main__":
    main()
