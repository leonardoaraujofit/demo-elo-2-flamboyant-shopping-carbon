@echo OFF
set PORT=%1
if "%PORT%" == "" (
echo "Usage: %0% <PORT>"
set PORT=12
)
@echo ON
make vrx-release
make vrx-release-load -f Makefile.test vrx_DDL_PORT=%PORT%
