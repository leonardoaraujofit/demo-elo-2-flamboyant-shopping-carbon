#ifdef _VRXEVO
#   include <svc.h>
#   define sleep(x) SVC_WAIT(x*1000)
#   include <log/syslog.h>
#else
#   include<syslog.h>
#endif

#include <stdio.h>
#include <log/liblog.h>
#include <log/logapi_stream.hpp>
#include <log/syslogcmd.h>

#include <string.h>
#include <pthread.h>


char const  *NAME = "LoggingAppLoggingAppLogging1";
int const STRESS_MSG_NUM = 50;
const char * IP = "127.0.0.1";
const int PORT = 0;

void *stress_logger(void *p)
{
    char buf[50] = {0};
    for(int i = 0; i < STRESS_MSG_NUM; i++)
    {
#ifdef _VRXEVO
        dbprintf("Just some OS message to keep syslog busy");
#endif
        snprintf(buf, sizeof(buf), "T%d iter. %d. Some random gibberish 0123456789!", (int) pthread_self(), i);

        
        LOGF_TRACE(buf);
    }
    return NULL;
}

const int NUM_THREADS = 5;
void stress_test() 
{
    LOGAPI_SET_VERBOSITY(LOGAPI_VERB_OFF);
    LOGAPI_INIT(NAME);
    LOGAPI_SETOUTPUT(LOGAPI_ALL);
    LOGAPI_SETLEVEL(LOGAPI_TRACE);

    pthread_t threads[NUM_THREADS];
    for(int i = 0; i < NUM_THREADS; i++)
        pthread_create(threads + i, NULL, &stress_logger, NULL);

    stress_logger(NULL);

    for(int i = 0; i < NUM_THREADS; i++)
        pthread_join(threads[i], NULL);
    LOGAPI_DEINIT();
}

void test_syslog_API()
{
    openlog(NAME, LOG_NDELAY | LOG_PID, LOG_USER);
    syslog(LOG_DEBUG, "Random stuff");
    closelog();

    openlog(NAME, LOG_NDELAY | LOG_PID, LOG_USER);
    int i =10;
    while(i-- > 0) 
    {
        syslog(LOG_INFO, "Note: there is a distinction between width (which is a minimum field width) and precision (which gives the maximum number of characters to be printed). %%s specifies the width,%%.s specifies the precision. (and you can also use %%.* but then you need two parameters, one for the width one for the precision) See also the printf man page (man 3 printf under linux) and especially the sections on field width and precision.  [%d]", (int)pthread_self());

    }
    closelog();

}

void test_stream_API()
{
#ifdef __cplusplus
    LOGAPI_SET_VERBOSITY(LOGAPI_VERB_PID | LOGAPI_VERB_FILE_LINE);
    LOGAPI_INIT(NAME);
    LOGAPI_SETLEVEL(LOGAPI_TRACE);

    const char * msg = 
    "Character traits classes specify character properties and provide specific semantics for certain operations on characters and sequences of characters."
    "The standard library includes a standard set of character traits classes that can be instantiated from the char_traits template, and which are used by default both for the basic_string objects and for the input/output stream objects. But any other class that follows the requirements of a character traits class can be used instead. This reference attempts to describe both the definition of the standard char_traits and the requirements for custom character traits classes.";

    for(int i = 0; i < 20; i ++)
    {
        LOGS_TRACE("Using " << "streams " << "to " << "log " << "this");
    }
    LOGS_TRACE("Using " << "streams " << "to " << "log " << "msg of length " << strlen(msg) <<": " << msg);

    LOGAPI_DEINIT();
#endif
}

void test_setoutput()
{
    LOGAPI_SET_VERBOSITY(LOGAPI_VERB_PID | LOGAPI_VERB_FILE_LINE);
    LOGAPI_INIT(NAME);
    LOGAPI_SETLEVEL(LOGAPI_TRACE);
    LOGAPI_SETOUTPUT(LOGAPI_ALL);

    const char *ver = log_getVersion();
    LOGF_TRACE("Version: %s", ver);

    LOGAPI_SETOUTPUT(LOGAPI_CONSOLE);
    LOGF_TRACE("To console only");

    LOGAPI_SETOUTPUT(LOGAPI_SYSLOG);
    LOGF_TRACE("To syslog only");

    LOGAPI_DEINIT();
}

void test_hexdump()
{
    LOGAPI_INIT(NAME);
    LOGAPI_SETLEVEL(LOGAPI_TRACE);

    LOGAPI_HEXDUMP_TRACE("\"DATA\" string hex", "DATA", 5);

    LOGAPI_DEINIT();
}

void test_from_doc()
{
    LOGAPI_SET_VERBOSITY(LOGAPI_VERB_PID | LOGAPI_VERB_FILE_LINE);
    LOGAPI_INIT("TESTD");
    LOGAPI_SETLEVEL(LOGAPI_TRACE);
    LOGAPI_SETOUTPUT(LOGAPI_ALL);

    LOGAPI_DUMP_SYS_INFO();
#ifdef __cplusplus
    DBGS_TRACE( "Trace Stream " << 1 << " test" );
#endif
    DBGF_TRACE( "Trace Printf %d test", 1 );
#ifdef __cplusplus
    DBGS_INFO( "Info Stream " << 1 << " test" );
#endif
    DBGF_INFO( "Info Printf %d test", 1 );
#ifdef __cplusplus
    DBGS_WARN( "Warn Stream " << 1 << " test" );
#endif
    DBGF_WARN( "Warn Printf %d test", 1 );
#ifdef __cplusplus
    DBGS_ERROR( "Error Stream " << 1 << " test" );
#endif
    DBGF_ERROR( "Error Printf %d test", 1 );


    const char buf[] =
        "12345678901234567890123456789012345678901234567890\x00\x01\xFF";
    ( void ) buf;

    DBG_HEXDUMP_RAW_TRACE( buf, sizeof( buf ) );
    DBG_HEXDUMP_RAW_INFO( buf, sizeof( buf ) );
    DBG_HEXDUMP_RAW_WARN(  buf, sizeof( buf ) );
    DBG_HEXDUMP_RAW_ERROR( buf, sizeof( buf ) );

    DBG_HEXDUMP_TRACE( "Buffer", buf, sizeof( buf ) );
    DBG_HEXDUMP_INFO( "Buffer", buf, sizeof( buf ) );
    DBG_HEXDUMP_WARN( "Buffer", buf, sizeof( buf ) );
    DBG_HEXDUMP_ERROR( "Buffer", buf, sizeof( buf ) );

    //Gets executed only when LOGAPI_ENABLE_DEBUG is defined
    LOGAPI_WHEN_DEBUG( LOGAPI_SETLEVEL( LOGAPI_WARN ) );
    DBGF_ERROR( "Dump this error f" );
#ifdef __cplusplus
    DBGS_ERROR( "Dump this error s" );
#endif
    DBGF_WARN( "Dump this warning f" );
#ifdef __cplusplus
    DBGS_WARN( "Dump this warning s" );
    DBGF_INFO( "DON'T dump this info f" );
    DBGS_INFO( "DON'T dump this info s" );
    DBGF_INFO( "DON'T dump this debug f" );
    DBGS_INFO( "DON'T dump this debug s" );
#endif
    LOGAPI_DEINIT();
}

void test_levels()
{
    LOGAPI_SET_VERBOSITY(LOGAPI_VERB_PID | LOGAPI_VERB_FILE_LINE);
    LOGAPI_INIT("TESTD");
    LOGAPI_SETLEVEL(LOGAPI_TRACE);

    LOGF_EMERG("EMERG msg");
	DBGF_EMERG("DBGF EMERG msg");
	
    LOGF_ALERT("ALERT msg");
	DBGF_ALERT("DBGF ALERT msg");
	
    LOGF_CRIT("CRIT msg");
	DBGF_CRIT("DBGF CRIT msg");
	
    LOGF_ERROR("ERROR msg");
	DBGF_ERROR("DBGF ERROR msg");

    LOGF_WARN("WARN msg");
	DBGF_WARN("DBGF WARN msg");
	
    LOGF_NOTICE("NOTICE msg");
	DBGF_NOTICE("DBGF NOTICE msg");
	
    LOGF_INFO("INFO msg");
	DBGF_INFO("DBGF INFO msg");
	
    LOGF_TRACE("TRACE msg");
	DBGF_TRACE("DBGF TRACE msg");


    LOGAPI_SETLEVEL(LOGAPI_EMERG);
    LOGF_ALERT("Won't output: ALERT msg");
	DBGF_ALERT("Won't output: DBGF ALERT msg");
	
    LOGF_CRIT("Won't output: CRIT msg");
	DBGF_CRIT("Won't output: DBGF CRIT msg");
	
    LOGF_ERROR("Won't output: ERROR msg");
	DBGF_ERROR("Won't output: DBGF ERROR msg");

    LOGF_WARN("Won't output: WARN msg");
	DBGF_WARN("Won't output: DBGF WARN msg");
	
    LOGF_NOTICE("Won't output: NOTICE msg");
	DBGF_NOTICE("Won't output: DBGF NOTICE msg");
	
    LOGF_INFO("Won't output: INFO msg");
	DBGF_INFO("Won't output: DBGF INFO msg");
	
    LOGF_TRACE("Won't output: TRACE msg");
	DBGF_TRACE("Won't output: DBGF TRACE msg");

    LOGAPI_SETLEVEL(LOGAPI_TRACE);
    //test stream API now
#ifdef __cplusplus
    LOGS_EMERG("EMERG msg" << " added via <<");
	DBGS_EMERG("DBGS EMERG msg" << " added via <<");
	
    LOGS_ALERT("ALERT msg" << " added via <<");
	DBGS_ALERT("DBGS ALERT msg" << " added via <<");
	
    LOGS_CRIT("CRIT msg" << " added via <<");
	DBGS_CRIT("DBGS CRIT msg" << " added via <<");
	
    LOGS_ERROR("ERROR msg" << " added via <<");
	DBGS_ERROR("DBGS ERROR msg" << " added via <<");

    LOGS_WARN("WARN msg" << " added via <<");
	DBGS_WARN("DBGS WARN msg" << " added via <<");
	
    LOGS_NOTICE("NOTICE msg" << " added via <<");
	DBGS_NOTICE("DBGS NOTICE msg" << " added via <<");
	
    LOGS_INFO("INFO msg" << " added via <<");
	DBGS_INFO("DBGS INFO msg" << " added via <<");
	
    LOGS_TRACE("TRACE msg" << " added via <<");
	DBGS_TRACE("DBGS TRACE msg" << " added via <<");
#endif
    LOGAPI_DEINIT();
}

void test_levels_hex()
{
    LOGAPI_SET_VERBOSITY(LOGAPI_VERB_PID | LOGAPI_VERB_FILE_LINE);
    LOGAPI_INIT("TESTD");
    LOGAPI_SETLEVEL(LOGAPI_TRACE);

    LOGAPI_HEXDUMP_EMERG("Level EMERG", "DATA STRING", strlen("DATA STRING"));	
    LOGAPI_HEXDUMP_ALERT("Level ALERT", "DATA STRING", strlen("DATA STRING"));	
    LOGAPI_HEXDUMP_CRIT("Level CRIT", "DATA STRING", strlen("DATA STRING"));	
    LOGAPI_HEXDUMP_ERROR("Level ERROR", "DATA STRING", strlen("DATA STRING"));
    LOGAPI_HEXDUMP_WARN("Level WARN", "DATA STRING", strlen("DATA STRING"));	
    LOGAPI_HEXDUMP_NOTICE("Level NOTICE", "DATA STRING", strlen("DATA STRING"));	
    LOGAPI_HEXDUMP_INFO("Level INFO", "DATA STRING", strlen("DATA STRING"));	
    LOGAPI_HEXDUMP_TRACE("Level TRACE", "DATA STRING", strlen("DATA STRING")); 	

    LOGAPI_HEXDUMP_RAW_EMERG("DATA STRING", strlen("DATA STRING"));	
    LOGAPI_HEXDUMP_RAW_ALERT("DATA STRING", strlen("DATA STRING"));	
    LOGAPI_HEXDUMP_RAW_CRIT("DATA STRING", strlen("DATA STRING"));	
    LOGAPI_HEXDUMP_RAW_ERROR("DATA STRING", strlen("DATA STRING"));
    LOGAPI_HEXDUMP_RAW_WARN("DATA STRING", strlen("DATA STRING"));	
    LOGAPI_HEXDUMP_RAW_NOTICE("DATA STRING", strlen("DATA STRING"));	
    LOGAPI_HEXDUMP_RAW_INFO("DATA STRING", strlen("DATA STRING"));	
    LOGAPI_HEXDUMP_RAW_TRACE("DATA STRING", strlen("DATA STRING")); 	

    LOGAPI_DEINIT();
}

void test_long_msg()
{
    LOGAPI_SET_VERBOSITY(LOGAPI_VERB_PID | LOGAPI_VERB_FILE_LINE);
    LOGAPI_INIT("JSONTEST");
    LOGAPI_SETOUTPUT(LOGAPI_ALL);
    const char *msg = "[  {    \"_id\": \"543e1042e3ca0370f283e141\",    \"index\": 0,    \"guid\": \"c0708547-31fa-4963-b55b-d7e312d212bb\",    \"isActive\": true,    \"balance\": \"$2,617.53\",    \"picture\": \"http://placehold.it/32x32\",    \"age\": 0,    \"eyeColor\": \"blue\",    \"name\": \"Shelia Mosley\",    \"gender\": \"female\",    \"company\": \"SURETECH\",    \"email\": \"sheliamosley@suretech.com\",    \"phone\": \"+1 (945) 526-2896\",    \"address\": \"499 Mill Road, Greenfields, Delaware, 4248\",    \"about\": \"Sit consectetur laborum consectetur consectetur mollit dolor ex pariatur ut cupidatat dolore elit adipisicing. Ad irure voluptate nisi ullamco eiusmod. Aliqua ea aute anim qui laboris aliquip dolor commodo minim minim dolore. Cupidatat nostrud ex laboris occaecat cupidatat. Excepteur et do nisi amet mollit ea nulla. Aliquip aliquip ex nostrud sunt ea irure consectetur culpa.\r\n\",    \"registered\": \"2014-06-02T11:05:41 -03:00\",    \"latitude\": 55.356534,    \"longitude\": -157.025974,    \"tags\": [      \"dolore\",      \"velit\",      \"excepteur\",      \"pariatur\",      \"esse\",      \"commodo\",      \"veniam\"    ],    \"friends\": [      {        \"id\": 0,        \"name\": \"Bray Merritt\"      },      {        \"id\": 0}]";
    LOGF_TRACE("Some large(%d) message %s, %s", strlen(msg), __func__, msg);
    LOGAPI_DEINIT();
}

void test_command_COM()
{
    syslcmd_set_source(SSRC_APPS);
    syslcmd_set_level(SYSL_LVL_TRACE);
    syslcmd_set_dest_COM(1);
}

void test_command_FS()
{
    syslcmd_set_source(SSRC_ALL);
    syslcmd_set_level(SYSL_LVL_WARN);
    syslcmd_set_dest_FILE("log.txt");
}

void test_command_UDP()
{
    syslcmd_set_source(SSRC_ALL);
    syslcmd_set_level(SYSL_LVL_WARN);
    syslcmd_set_dest_UDP(IP,PORT);
}

void test_print_callstack()
{
    LOGAPI_INIT("CALLSTACK");
    LOGAPI_SETOUTPUT(LOGAPI_ALL);
    LOGAPI_PRINT_CALLSTACK( LOGAPI_EMERG );
    LOGAPI_DEINIT();

    LOGF_TRACE("No logs should bee allowed after deinit!");
}

void test_log_config()
{
    LOGAPI_INIT("example");
    LOGAPI_SETOUTPUT(LOGAPI_ALL);
    LOGF_TRACE("Should see this log");
    LOGAPI_DEINIT();

    LOGAPI_INIT("example1");
    LOGAPI_SETOUTPUT(LOGAPI_ALL);
    LOGF_TRACE("Shouldn't see this one though");
    LOGAPI_DEINIT();                      

    LOGAPI_INIT("example");
    LOGAPI_SETOUTPUT(LOGAPI_ALL);
    LOGF_TRACE("Should see this one too");
    LOGAPI_DEINIT();
} 

void test_tag_api()
{ 
    LOGAPI_INIT("example");
    LOGAPI_SETLEVEL(LOGAPI_TRACE);
    LOGAPI_SETOUTPUT(LOGAPI_ALL);

    LOGF_TAG_EMERG( "TEST", "Tag test is enabled" );
    LOGF_TAG_ALERT( "TEST1", "Tag test1 is disabled" ); 
    LOGF_TAG_CRIT( "TEST2", "Tag test2 is enabled" );     
    LOGF_TAG_ERROR( "TEST3", "Tag test3 is disabled" );     
    LOGF_TAG_WARN( "TEST4", "Tag test4 is enabled" );     
    LOGF_TAG_NOTICE( "TEST5", "Tag test5 is disabled" );     
    LOGF_TAG_INFO( "TEST6", "Tag test6 is enabled" );     
    LOGF_TAG_TRACE( "TEST7", "Tag test7 is disabled" );     

    LOGF_TAG_TRACE( "EST", "est tag should be ignored" );     
    LOGF_TAG_TRACE( "", "empty string tag should be ignored" );     

#ifdef __cplusplus
    LOGS_TAG_EMERG( "TEST", "TEST msg" << " added via <<");
    DBGS_TAG_EMERG( "TEST", "DBGS EMERG TEST msg" << " added via <<");
    LOGS_TAG_TRACE( "TEST1", "TRACE TEST1 msg" << " added via <<");
    DBGS_TAG_TRACE( "TEST1", "DBGS TRACE TEST1 msg" << " added via <<");
    LOGS_TAG_ALERT( "TEST2", "ALERT TEST2 msg" << " added via <<");
    DBGS_TAG_ALERT( "TEST2", "DBGS ALERT TEST2 msg" << " added via <<");
    LOGS_TAG_CRIT( "TEST3", "CRIT TEST3 msg" << " added via <<");
    DBGS_TAG_CRIT( "TEST3", "DBGS CRIT TEST3 msg" << " added via <<");
    LOGS_TAG_ERROR( "TEST4", "ERROR TEST4 msg" << " added via <<");
    DBGS_TAG_ERROR( "TEST4", "DBGS ERROR TEST4 msg" << " added via <<");
    LOGS_TAG_WARN( "TEST5", "WARN TEST5 msg" << " added via <<");
    DBGS_TAG_WARN( "TEST5", "DBGS WARN TEST5 msg" << " added via <<");
    LOGS_TAG_NOTICE( "TEST6", "NOTICE TEST6 msg" << " added via <<");
    DBGS_TAG_NOTICE( "TEST6", "DBGS NOTICE TEST6 msg" << " added via <<");
    LOGS_TAG_INFO( "TEST7", "INFO TEST7 msg" << " added via <<");
    DBGS_TAG_INFO( "TEST7", "DBGS INFO TEST7 msg" << " added via <<");
#endif    
    LOGAPI_HEXDUMP_TAG_EMERG("TEST","Level EMERG tag:TEST", "DATA STRING", strlen("DATA STRING"));	
    LOGAPI_HEXDUMP_TAG_ALERT("TEST1","Level ALERT tag:TEST1", "DATA STRING", strlen("DATA STRING"));	
    LOGAPI_HEXDUMP_TAG_CRIT("TEST2","Level CRIT tag:TEST2", "DATA STRING", strlen("DATA STRING"));	
    LOGAPI_HEXDUMP_TAG_ERROR("TEST3","Level ERROR tag:TEST3", "DATA STRING", strlen("DATA STRING"));
    LOGAPI_HEXDUMP_TAG_WARN("TEST4","Level WARN tag:TEST4", "DATA STRING", strlen("DATA STRING"));	
    LOGAPI_HEXDUMP_TAG_NOTICE("TEST5","Level NOTICE tag:TEST5", "DATA STRING", strlen("DATA STRING"));	
    LOGAPI_HEXDUMP_TAG_INFO("TEST6","Level INFO tag:TEST6", "DATA STRING", strlen("DATA STRING"));	
    LOGAPI_HEXDUMP_TAG_TRACE("TEST7","Level TRACE tag:TEST7", "DATA STRING", strlen("DATA STRING")); 	

    LOGAPI_HEXDUMP_RAW_TAG_TRACE("TEST","DATA STRING tag:TEST", strlen("DATA STRING")); 	
    LOGAPI_HEXDUMP_RAW_TAG_EMERG("TEST1","DATA STRING tag:TEST1", strlen("DATA STRING"));	
    LOGAPI_HEXDUMP_RAW_TAG_ALERT("TEST2","DATA STRING tag:TEST2", strlen("DATA STRING"));	
    LOGAPI_HEXDUMP_RAW_TAG_CRIT("TEST3","DATA STRING tag:TEST3", strlen("DATA STRING"));	
    LOGAPI_HEXDUMP_RAW_TAG_ERROR("TEST4","DATA STRING tag:TEST4", strlen("DATA STRING"));
    LOGAPI_HEXDUMP_RAW_TAG_WARN("TEST5","DATA STRING tag:TEST5", strlen("DATA STRING"));	
    LOGAPI_HEXDUMP_RAW_TAG_NOTICE("TEST6","DATA STRING tag:TEST6", strlen("DATA STRING"));	
    LOGAPI_HEXDUMP_RAW_TAG_INFO("TEST7","DATA STRING tag:TEST7", strlen("DATA STRING"));	

    LOGAPI_DEINIT();
}

void test_cmd_API()
{
    char buf[50] = {0};
    int i, ret;
    LOGAPI_INIT("TEST_CMD_API");
    ret = syslcmd_get_cfg_int(ENABLED, &i);
    LOGF_TRACE("ENABLED %d; ret = %d", i, ret);
    ret = syslcmd_get_cfg_int(DESTINATION, &i);
    LOGF_TRACE("DESTINATION %d; ret = %d", i, ret);
    ret = syslcmd_get_cfg_int(SERIAL_COM, &i);
    LOGF_TRACE("SERIAL_COM %d; ret = %d", i, ret);
    ret = syslcmd_get_cfg_str(UDP_HOST, buf);
    LOGF_TRACE("UDP_HOST %s; ret = %d", buf, ret);
    ret = syslcmd_get_cfg_int(UDP_PORT, &i);
    LOGF_TRACE("UDP_PORT %d; ret = %d", i, ret);
    ret = syslcmd_get_cfg_int(LOG_LEVEL, &i);
    LOGF_TRACE("LOG_LEVEL %d; ret = %d", i, ret);
    ret = syslcmd_get_cfg_str(FILE_NAME, buf);
    LOGF_TRACE("FILE_NAME %s; ret = %d", buf, ret);
    ret = syslcmd_get_cfg_int(FILE_BUF_SIZE, &i);
    LOGF_TRACE("FILE_BUF_SIZE %d; ret = %d", i, ret);
    ret = syslcmd_get_cfg_int(LOG_SOURCE, &i);
    LOGF_TRACE("LOG_SOURCE %d; ret = %d", i, ret);
    ret = syslcmd_get_cfg_int(SEND_RETRIES, &i);
    LOGF_TRACE("SEND_RETRIES %d; ret = %d", i, ret);
    ret = syslcmd_get_cfg_int(MAX_LOGFILE_SIZE, &i);
    LOGF_TRACE("MAX_LOGFILE_SIZE %d; ret = %d", i, ret);
    ret = syslcmd_get_cfg_int(NUMBER_OF_FILES, &i);
    LOGF_TRACE("NUMBER_OF_FILES %d; ret = %d", i, ret);
    ret = syslcmd_get_cfg_int(LOG_FORMAT, &i);
    LOGF_TRACE("LOG_FORMAT %d; ret = %d", i, ret);

    syslcmd_set_cfg_str(UDP_HOST, IP);
    syslcmd_set_cfg_int(UDP_PORT, PORT);
    syslcmd_set_cfg_int(DESTINATION, 1);

    syslcmd_apply_config();

    for(int i = 0; i < 50; i++)
    {
        snprintf(buf, sizeof(buf), "iter. %d. Some random gibberish 0123456789!", i);

        LOGF_TRACE(buf);
    }

    syslcmd_set_dest_COM(1);
    syslcmd_apply_config();

    for(int i = 0; i < 50; i++)
    {
        snprintf(buf, sizeof(buf), "iter. %d. Some random gibberish 0123456789!", i);

        LOGF_TRACE(buf);
    }

    LOGAPI_DEINIT();
}

void all_tests()
{
    stress_test();
    test_long_msg();
    test_syslog_API();
    test_stream_API();
    test_setoutput();
    test_hexdump();
    test_from_doc();
    test_levels_hex();
    test_levels();
    test_print_callstack();
    test_log_config();
    test_tag_api();
    test_cmd_API();
}

int main(int argc, char* argv[])
{
    all_tests();
}
