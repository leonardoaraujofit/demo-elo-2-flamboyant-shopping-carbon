@echo off
echo CP Signer
echo --------------------------
set appdir=%1
set tempdir=%appdir%\pkg
set script_path=%~dp0
set bin_signer="%script_path%\vfisigner_win32.exe"
for /d %%D in (%appdir%/mp/*) do set appname=%%~nxD
set file_mpmanifest="%appdir%\mp\%appname%\mpmanifest.mft"
set file_confowner="%appdir%\mp\%appname%\confOwner.json"
set file_appuninstall="%appdir%\mp\%appname%\appuninstall.mft"
set file_cert_approval="%script_path%\vs208928-cpapproval.crt"
set file_cert_manifest="%script_path%\vs208937-cpmanifest.crt"
set file_key_approval="%script_path%\vs208928-cpapproval.key"
set file_key_manifest="%script_path%\vs208937-cpmanifest.key"


echo input dir: %appdir%
echo app name : %appname%
echo script_path: %script_path%
echo --------------------------
type %file_mpmanifest%
type %file_appuninstall%
echo --------------------------

REM **** a few checks and warnings
if "%1"=="" goto err_param
if not exist "%appdir%\mp\%appname%" goto err_mp_folder
if not exist "%appdir%\www\%appname%" goto err_www_folder
if not exist %file_mpmanifest% goto err_mp_manifest
if not exist %file_confowner% echo WARNING: no confOwner.json found
if not exist %file_appuninstall% echo WARNING: no appuninstall.mfg found

:packaging
REM **** delete old tempdir recursively and create freshly
rmdir "%tempdir%" /s /q > NUL 2>&1

REM **** create the application package
mkdir "%tempdir%\zip"
CScript %script_path%zip.vbs %appdir%\www %tempdir%\zip\%appname%.zip > NUL 2>&1
copy %file_cert_approval% "%tempdir%\zip" > NUL 2>&1
%bin_signer% --cert %file_cert_approval% --key %file_key_approval% %tempdir%\zip\%appname%.zip
move "%tempdir%\zip\%appname%.zip.p7s" "%tempdir%\zip\%appname%.p7s" > NUL 2>&1
CScript %script_path%zip.vbs %tempdir%\zip %tempdir%\pkg-%appname%.zip > NUL 2>&1
mkdir "%tempdir%\all"
move "%tempdir%\zip\*" "%tempdir%\all" > NUL 2>&1
rmdir "%tempdir%\zip" /s /q


REM **** create the MP manifest package
mkdir "%tempdir%\zip"
mkdir "%tempdir%\files\%appname%"
copy %file_confowner%  "%tempdir%\files\%appname%" > NUL 2>&1
copy %file_mpmanifest% "%tempdir%\files\%appname%" > NUL 2>&1
CScript %script_path%zip.vbs "%tempdir%\files" "%tempdir%\zip\%appname%-o.zip" > NUL 2>&1
copy %file_cert_manifest% "%tempdir%\zip" > NUL 2>&1
%bin_signer% --cert %file_cert_manifest% --key %file_key_manifest% %tempdir%\zip\%appname%-o.zip
move "%tempdir%\zip\%appname%-o.zip.p7s" "%tempdir%\zip\%appname%-o.p7s" > NUL 2>&1
CScript %script_path%zip.vbs %tempdir%\zip %tempdir%\pkg-%appname%-o.zip > NUL 2>&1
move "%tempdir%\zip\*" "%tempdir%\all" > NUL 2>&1
rmdir "%tempdir%\files" /s /q
rmdir "%tempdir%\zip" /s /q

REM **** create the combination installation package
CScript %script_path%zip.vbs %tempdir%\all %tempdir%\pkg-%appname%-inst.zip > NUL 2>&1
rmdir "%tempdir%\all" /s /q


REM **** create the uninstall packages
if not exist %file_appuninstall% goto skip_uninstall
mkdir "%tempdir%\zip"
mkdir "%tempdir%\files\%appname%"
copy %file_appuninstall% "%tempdir%\files\%appname%" > NUL 2>&1
CScript %script_path%zip.vbs %tempdir%\files %tempdir%\zip\%appname%-u.zip > NUL 2>&1
rmdir "%tempdir%\files" /s /q
copy %file_cert_manifest% "%tempdir%\zip" > NUL 2>&1
%bin_signer% --cert %file_cert_manifest% --key %file_key_manifest% %tempdir%\zip\%appname%-u.zip
move "%tempdir%\zip\%appname%-u.zip.p7s" "%tempdir%\zip\%appname%-u.p7s" > NUL 2>&1
CScript %script_path%zip.vbs %tempdir%\zip %tempdir%\pkg-%appname%-remove.zip > NUL 2>&1
rmdir "%tempdir%\zip" /s /q

goto end

:skip_uninstall
echo WARNING: Skipping uninstall package creation.
goto end

:err_param
echo ERROR: wrong parameter.
echo usage cpsign.bat appdir
goto end

:err_mp_manifest
echo ERROR: marketplace manifest %file_mpmanifest% does not exist.
goto end

:err_mp_folder
echo ERROR: marketplace (MP) files folder %appdir%\mp\%appname% does not exist.
goto end

:err_www_folder
echo ERROR: application files (WWW) folder %appdir%\www\%appname% does not exist.
goto end

:end
echo done.
echo --------------------------