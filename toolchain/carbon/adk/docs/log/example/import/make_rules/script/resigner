#!/usr/bin/env bash

##
# File:    resigner
# Author:  F. Wiechers
# Created: Fri, 25 Apr 2014 15:57:44 +0200
# Changed: Mon, 01 Dec 2014 16:39:08 +0100
##

cpopts="r"
# setting owner and group to 0 is more compatible using cygwin
taropts="--owner=0 --group=0"
user="usr*"

PRODUCTIVE_SIGNING=1
verbose=false
# exit on no action
enoact=false
outdir="."

while getopts :vdo:u:e opt
do
  case $opt in
    v) cpopts+=$opt; verbose=true;;
    o) outdir="$OPTARG";;
    d) PRODUCTIVE_SIGNING=0;;
    u) user="$OPTARG";;
    e) enoact=true;;
    *) echo "
usage: $0 [-vd] [--] <dl.xyz.tar ...>
options:
  v verbose mode
  o output dir (default: current directory)
  d development signing
  u user for signing (if wildcard is used it needs to be escaped or quoted; eg. -u \"sys*\")
  e exit if no bundle/package got resigned. Exits with error code 2.
    Only reliable with one dl file.
";;
  esac
done

scriptdir=${0%/*}

vos_SIGN_TOOL_PRD="${vos_SIGN_TOOL_PRD:-$(printenv "ProgramFiles(x86)")/VeriFone/FST/FileSignature}"
vos_SIGN_TOOL_DEV=${scriptdir}/vfisigner
if test "$(uname -o)" = "Msys" -o "$(uname -o)" = "Cygwin"
then
  vos_SIGN_TOOL_DEV=${scriptdir}/vfisigner_win32.exe
fi


shift $((OPTIND - 1))

if test ! -e $outdir
then
  mkdir -p $outdir
  if test "$?" != "0"
  then
    exit 1
  fi
else
  if test ! -d $outdir
  then
    echo "\"$outdir\" is not a directory" >&2
    exit 1
  fi
fi
outdir="$(readlink -f "$outdir")"

# sign_file
# @param fname: filename
# @param crtpath: path to the certificate
# @param username: username (needed to determine cert name for development
#  signing.

sign_file ()
{
  fname=$1
  crtpath=$2
  username=$3

  crtdir=$(dirname "$crtpath")
  if ! test -d "$crtdir"
  then
    mkdir -p "$crtdir"
  fi

  if test "$(uname -o)" = "Cygwin"
  then
    fname=$(cygpath -m "$fname")
    crtpath=$(cygpath -m "$crtpath")
  fi

  if test "$PRODUCTIVE_SIGNING" == "1"
  then
    if test ! -e "${vos_SIGN_TOOL_PRD}"
    then
      echo "No sign tool found!" >&2
      exit 1
    fi
    if $verbose
    then
      echo "DEBUG: $vos_SIGN_TOOL_PRD -F $fname -C $crtpath -SM"
    fi
    msg=$("$vos_SIGN_TOOL_PRD" -F "$fname" -C "$crtpath" -SM 2>&1)
    if [[ $msg != *"... OK"* ]]
    then
      echo "sign file \"$fname\" failed!" >&2
      echo $msg
      exit 1
    fi
  else
    case $username in
      usr*) key=${scriptdir}/appsignerA1.key; cert=${scriptdir}/appsignerA1.crt;;
      sys*) key=${scriptdir}/sysappsign1.key; cert=${scriptdir}/sysappsign1.crt;;
      os) key=${scriptdir}/ossigner1.key; cert=${scriptdir}/ossigner1.crt;;
      *) key=${scriptdir}/ossigner1.key; cert=${scriptdir}/ossigner1.crt;;
    esac
    cp $cert "$crtpath"
    if $verbose
    then
      echo "DEBUG: ${vos_SIGN_TOOL_DEV} --key $key --cert $cert $fname"
    fi
    msg=$("${vos_SIGN_TOOL_DEV}" --key $key --cert $cert "$fname" 2>&1)
    if [[ $? != "0" ]]
    then
      echo "sign file \"$fname\" failed!" >&2
      echo $msg
      exit 1
    fi
  fi
}

tmpdir=$(mktemp -d)

#echo "Created \"${tmpdir}\""
# clean up on exit
trap "rm -rf $tmpdir; exit" EXIT

for i in $@
do
  dlfilename=${i##*/}
  # create a new directory for the download file
  extdir=$tmpdir/${dlfilename}
  mkdir $extdir
  tar -axf "$i" -C "$extdir"
  if test "$?" != "0"
  then
    continue
  fi
  # walk through all bundles in the download file
  for j in "$extdir/"*
  do
    # file extension can be .tar .tgz .tbz2 tar.gz .tar.bz2
    if [[ $j =~ \.(tar|tgz|tbz2|tar\.gz|tar\.bz2)$ ]]
    then
      bdlfile="${j}"
      bdldir="${bdlfile%.t*}"
      hasPkgs=false

      mkdir "${bdldir}"
      # extract the bundle tar file
      tar -axf "$bdlfile" -C "${bdldir}"
      # extract the user name form bundle control file
      username=$(grep -i User: < ${bdldir}/CONTROL/control | sed -e s/User:[[:space:]]*//)
      if $verbose
      then
        echo "DEBUG: found bundle file: ${j/$extdir\/} (User: $username)"
      fi
      # we only sign user bundles/packages in productive mode
      if [[ $PRODUCTIVE_SIGNING == "1" && $username != ${user} ]]
      then
        echo "bundle ${bdlfile/$extdir\/} is no ${user} bundle.. won't resign" >&2
        rm -r "${bdldir}"
        continue
      fi
      # we'll sign so don't exit
      enoact=false
      # walk through all packages in the bundle
      for k in "${bdldir}/"*
      do
        # package files should not be compressed, hence we need only .tar
        if [[ $k =~ \.tar$ ]]
        then
          # remove old package signature
          rm ${k}.[pP]7[sS] &>/dev/null
          if $verbose
          then
            echo "DEBUG: found package file: ${k/$bdldir\/}"
          fi
          hasPkgs=true
          # no need to repack the package file, only signing is needed
          echo -en "Signing package tar file (${k/$bdldir\/})... "
          sign_file $k "$bdldir/crt/Certif.crt" $username
          echo "done."
        fi
      done
      echo -en "Signing bundle tar file (${bdlfile/$extdir\/})... "
      # sign dummy file if bundle contains no pkgs to get the certificate
      if ! $hasPkgs
      then
        dummy="${bdldir}/dummy"
        touch "${dummy}"
        sign_file "$dummy" "$bdldir/crt/Certif.crt" ${currusr}
        rm ${dummy} ${dummy}.[Pp]7[Ss]
      fi
      # remove old bundle signature
      rm ${j}.[pP]7[sS] &>/dev/null
      # repack and resign the bundle
      (cd "${bdldir}"; tar -acf "$bdlfile" *)
      sign_file $bdlfile "$bdldir/crt/Certif.crt" $username
      echo "done."
      rm -r "${bdldir}"
    fi
  done
  # repack the download file
  echo -en "Creating tar file (${outdir}/${dlfilename})... "
  (cd $extdir; tar -acf "${outdir}/$dlfilename" *)
  echo "done."

  $enoact && exit 2
  exit 0
done

