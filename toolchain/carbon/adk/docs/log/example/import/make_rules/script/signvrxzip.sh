#!/bin/bash

argv0=$0
scriptdir=${0%/*}

bundle=""
ziplist=""
outdir=""
outdir_created=false
suffix=""
fstfile="syssign.fst"
verbose=false
silent=false
keep_fst=false
enoact=false
testmode=false

function error_cleanup {
  # remove partial filled output directory
  if $outdir_created; then rm -rf $outdir 2>/dev/null; fi
}

function usage {
   echo "Usage: $argv0 [-f <fst file>] [-v] [-S] [-o <dir>] [-s <suffix>]"
   echo "       [-b dl.bundle.zip] [-k] [-e] [-t] <file.zip ...>"
   echo "Sign a Verix eVo ZIP installation package containing a FST file (option -f)."
   echo "For productive signing (without option -t) make sure that signing service with"
   echo "File Signing Tool (FST) runs on your PC when calling this script."
   echo "Description of options:"
   echo "-f  Relative path to FST file in the ZIP. This file contains the certificate location"
   echo "    the list of files to be signed. Option -f is useful to handle packages for different"
   echo "    signing cards, e.g. \"-f sign.fst\" usually contains files for user signing."
   echo "    Default is \"syssign.fst\", which is generally used for EOS singing."
   echo "-v  Verbose mode"
   echo "-S  Silent mode, do not prompt user"
   echo "-o  Output directory for signed packages. If not specified, ZIP files are replaced."
   echo "    Note: Skipped packages (that were not signed) are not stored to this directory."
   echo "-s  Name suffix for signed packages to avoid that origin packages are replaced,"
   echo "    e.g. \"-s -signed\" creates dl.package-signed.zip for dl.package.zip".
   echo "    Note: Skipped packages (that were not signed) do not get this suffix."
   echo "-b  If this file name is set, a loadable bundle is created from the ZIP file list."
   echo "    The specified bundle must have extension .zip, e.g. \"-b bundle.zip\"".
   echo "-k  Keep FST file (specified by option -f) after signing. Default: FST file"
   echo "    will be deleted."
   echo "-e  Exits with error code 2, if the specified installation package got not signed."
   echo "    This option only is reliable with one installation package."
   echo "-t  Enable signing for test mode devices. Requires test signing tool Signtool.exe"
   echo "    to be available beside this script."
   error_cleanup
   exit 1
}

function debug {
  if $verbose; then echo -e "DEBUG: $1"; fi
}

while getopts :f:vSo:s:b:ket opt
do
  case $opt in
    f) fstfile="$OPTARG";;
    v) verbose=true;;
    S) silent=true;;
    o) outdir="$OPTARG";;
    s) suffix="$OPTARG";;
    b) bundle="$OPTARG";;
    k) keep_fst=true;;
    e) enoact=true;;
    t) testmode=true;;
    *) usage;;
  esac
done

shift $((OPTIND - 1))

if [ $# -lt 1 ]; then usage; fi

# check existence signing tools
if $testmode; then
  if [ ! -f "${scriptdir}/FileSign" ]; then
    echo "FileSign Tool for test mode does not exist in script folder, exit." >&2
    exit 1
  fi
else
  # check existence of FileSignature Tool
  fst=
  for i in "${PROGRAMFILES}" "${ProgramW6432}" "C:\Program Files (x86)"
  do
    if [ -f "${i}/VeriFone/FST/FileSignature" ]; then
      fst="${i}/VeriFone/FST/FileSignature"
    break
    fi
  done
  
  if [ -z "$fst" ]; then
    echo "Verifone FileSignature Tool (FST) is not installed, exit." >&2
    exit 1
  fi
fi

tmpdir=$(mktemp -d)
trap "rm -r $tmpdir" EXIT
# extract folder for ZIP contents
mkdir ${tmpdir}/zip

# setup the output directory (if any)
if [ -n "$outdir" ]; then
  if [ ! -e "$outdir" ]; then
    mkdir -p $outdir
    if [ "$?" != "0" ]; then
      echo "Output directory \"$outdir\" cannot be created, exit." >&2
      exit 1
    fi
    outdir_created=true
    debug "created output directory \"$outdir\"."
  else
    if [ ! -d "$outdir" ]; then
      echo "Output directory \"$outdir\" is not a directory, exit." >&2
      exit 1
    fi
  fi
fi

# setup the bundle directory
if [ -n "$bundle" ]; then
  bname=$(basename "$bundle")
  bext="${bname##*.}"
  if [ "$bext" != "zip" ]; then
    echo "Invalid bundle file extension specified by option \"-b\", exit." >&2
    error_cleanup
    exit 1
  fi
  if [ -z "${bname%.*}" ]; then
    echo "Invalid bundle file name specified by option \"-b\", exit." >&2
    error_cleanup
    exit 1
  fi
  bdir=$(dirname "$bundle")
  if [ ! -d "$bdir" ]; then
    echo "Invalid bundle file path specified by option \"-b\", exit." >&2
    error_cleanup
    exit 1
  fi
  mkdir ${tmpdir}/bundle
  # add VFI installer files
  (cd "$scriptdir"; cp pkg_inst.sdm vfi.p7s vfi.ped vxossign.crt ${tmpdir}/bundle 2>/dev/null)
  if [ "$?" != "0" ]; then
    echo "VFI installer files not available, exit." >&2
    error_cleanup
    exit 1
  fi
fi

if ! $verbose; then zipopts="-q"; fi

if [ ! $testmode -a ! $silent ]; then
  read -p "Please start FST signing service and press ENTER to continue."
fi

# process the ZIP file list
for i in $@; do
  filename=$(basename "$i")
  extension="${filename##*.}"
  if [ "$extension" != "zip" ]; then usage; fi
  if [ -z "${filename%.*}" ]; then usage; fi
  if [ ! -f "$i" ]; then
    echo "File \"$i\" does not exist, exit." >&2; 
    error_cleanup
    exit 1;
  fi
  echo "Processing \"$i\"..."
  debug "unzip $zipopts $i -d ${tmpdir}/zip"
  unzip $zipopts $i -d ${tmpdir}/zip
  if [ "$?" != "0" ]; then
    echo "File \"$i\" not found or not a valid ZIP format, exit." >&2
    error_cleanup
    exit 1
  fi
  
  # check, if there is a FST file
  if [ -f "${tmpdir}/zip/${fstfile}" ]; then
    # FileSignature tool will fail, if FST file contains
    # a certificate location folder, which does not exist.
    # Therefore, we create it to ship around this problem.    
    crt_location=$(awk '/^[ \t]*\[/{z=0} {if(z==1 && NF!=0) {print $0; z=0} } /^[ \t]*\[Certificate name & location\]/{z=1}' ${tmpdir}/zip/${fstfile})
    # remove " characters (if any)
    crt_location=${crt_location#*\"}
    crt_location=${crt_location%\"*}
    # replace \ by / and remove leading "./" if any
    crt_location=$(echo $crt_location | sed -e 's/\\/\//g' -e 's/^\.\///g')
    # save file name for test mode signing (see below), remove leading "./" if any
    rel_path_crt_file="$crt_location"
    # create the absolute temp dir path
    crt_location=$(dirname $crt_location)
    crt_location=$(echo "${tmpdir}/zip/${crt_location}" | sed s#//*#/#g)
    mkdir -p "$crt_location" 2>/dev/null
    if [ "$?" != "0" ]; then
      echo Could not create certificate location directory \"$crt_location\", exit. >&2
      error_cleanup
      exit 1
    fi
  else # no FST file found
    # skip this package, if no bundle should be created
    if [ -z "$bundle" ]; then
      echo File \"$fstfile\" not found. Skipped signing \"$i\".
      rm -rf $tmpdir/zip* 2>/dev/null
      continue
    fi
    # bundle mode: if ZIP already contains certificates, signing can be skipped
    any_file=$(find ${tmpdir}/zip -type f -iname "*.crt" 2>/dev/null)
    if [ -n "$any_file" ]; then
      debug "cp $i ${tmpdir}/bundle"
      cp "$i" "${tmpdir}/bundle"
      echo Package \"$i\" already signed, added to bundle.
      rm -rf $tmpdir/zip* 2>/dev/null
      continue
    fi
    # workaround for bundle mode:
    # we need to sign at least one file of the ZIP, since terminal
    # will abort installation after a ZIP not containing a certificate.
    # Search for any file to sign (ignore files on root level)
    any_file=$(find ${tmpdir}/zip -mindepth 2 -type f -print 2>/dev/null | head -n 1)
    if [ -z "$any_file" ]; then
      echo Invalid package format. No files found in \"$i\", exit. >&2
      error_cleanup
      exit 1
    fi
    rel_path=$(dirname $any_file)
    # get the relative path to the file
    rel_path=${rel_path#${tmpdir}/zip/}
     # save file path for test mode signing (see below)
    rel_path_crt_file=${rel_path}/Certif.crt
    # replace '/' by '\'
    rel_path=${rel_path////\\}
    # remove pending path to get the path prefix, e.g. "f1" or "1"
    path_prefix=${rel_path%%\\*}
    flash_drive=0
    if [ -n "$(echo "$path_prefix" | grep -E "[f,F][0-9]*$")" ]; then flash_drive=1; fi
    # reduce filpath to filename
    any_file=$(basename $any_file)
    # create a temporary fst file
    echo "[Files to sign]/1">>${tmpdir}/zip/${fstfile}
    echo "${flash_drive} \".\\${rel_path}\\${any_file}\"">>${tmpdir}/zip/${fstfile}
    echo "">>${tmpdir}/zip/${fstfile}
    echo "[Security Level]/2">>${tmpdir}/zip/${fstfile}
    echo "0">>${tmpdir}/zip/${fstfile}
    echo "" >>${tmpdir}/zip/${fstfile}
    echo "[Certificate name & location]/3">>${tmpdir}/zip/${fstfile}
    echo "\".\\${rel_path}\Certif.crt\"">>${tmpdir}/zip/${fstfile}
    echo "">>${tmpdir}/zip/${fstfile}
    debug "Created dummy \"${fstfile}\" for package \"$filename\"."
  fi
  
  # do signing
  if $testmode; then
    # test mode signing
    awk '/^[ \t]*;/{next} /^[ \t]*\[/{z=0} {if(z==1 && NF!=0) print $0; } /^[ \t]*\[Files to sign\]/{z=1}' ${tmpdir}/zip/${fstfile} | while read -r flash_flag file_to_sign; do
      # remove " characters (if any)
      file_to_sign=${file_to_sign#*\"}
      file_to_sign=${file_to_sign%\"*}
      # replace \ by / and remove leading "./" if any
      file_to_sign=$(echo $file_to_sign | sed -e 's/\\/\//g' -e 's/^\.\///g')
      # create the absolute temp dir path
      file_to_sign=$(echo "${tmpdir}/zip/${file_to_sign}" | sed s#//*#/#g)
      # file path must be converted to windows style, otherwise FileSign fails
      if [ "$(uname -o)" = "Cygwin" ]; then
        file_to_sign=$(cygpath -w "$file_to_sign")
      else 
        file_to_sign=$(msyspath -w "$file_to_sign")
      fi
      flashopt=""
      if [ "$flash_flag" != "0" ]; then flashopt="-L"; fi
      "${scriptdir}/FileSign" -C "${scriptdir}/vs090400.crt" -K "${scriptdir}/vs090400.key" -F "$file_to_sign" -S "$file_to_sign.p7s" $flashopt 2>/dev/null
      if [ "$?" != 0 ]; then exit 1; fi
    done
    if [ "$?" != 0 ]; then
      echo "Signing failed, exit."
#      read -p 'Press [Enter] key to continue...'
      error_cleanup
      exit 1
    fi
    # create absolute tempdir path of certificate file
    rel_path_crt_file=$(echo "${tmpdir}/zip/${rel_path_crt_file}" | sed s#//*#/#g)
    # copy certificate file
    cp "${scriptdir}/vs090400.crt" "$rel_path_crt_file"
    # copy sponsor certificate
    rel_path_crt_file=$(dirname $rel_path_crt_file)/sponsorCert.crt
    cp "${scriptdir}/VS208736.crt" "$rel_path_crt_file"
  else
    # productive signing
    debug "Signing \"$filename\"... "
    debug "(cd ${tmpdir}/zip; \$($fst ${fstfile} -nogui 2>&1 >../fst_result))"
    ( cd ${tmpdir}/zip; $("$fst" "${fstfile}" -nogui 2>&1 >../fst_result))
    msg=$(cat ${tmpdir}/fst_result)
    rm -f ${tmpdir}/fst_result
    if [[ $msg != *"... OK"* ]]; then
      echo $msg >&2
      echo "Signing failed, exit."
      error_cleanup
      exit 1
    fi
    echo "$msg"
  fi
  
  enoact=false  # do not return 2, if at least one file signed 
  # delete fst file after signing
  if ! $keep_fst; then rm -f "${tmpdir}/zip/${fstfile}" 2>/dev/null; fi
  
  # move signature files (not already located on drive i) to drive i in a folder having the same group "i<gid>"
  for j in $(find ${tmpdir}/zip -type f -iname "*.p7s" -printf '%P\n' 2>/dev/null | xargs); do
    fpath=$(dirname "$j")
    fname=$(basename "$j")
    
    # get the path prefix, e.g. "1" of "1/a/b" . Rest of path "a/b" is stored in path_suffix
    path_prefix=${fpath%%/*}
    path_suffix=""
    if [ "$fpath" != "$path_prefix" ]; then path_suffix=${fpath#*/}; fi
    
    nodigits=$(echo "$path_prefix" | sed "s/[[:digit:]]//g")
    if [ -n "$nodigits" ] ; then
      # folder is not a number and contains drive prefix
      drive=${path_prefix:0:1}
      group=${path_prefix:1}
    else
      # already a group folder without drive prefix
      drive=""
      group="$path_prefix"
    fi
    if [ -n "$group" ]; then
      if [ "$drive" = "I" ]; then drive="i"; fi
      if [ "$drive" != "i" ]; then
        destdir="${tmpdir}/zip/i${group}"
        # if there is already a folder with capital 'I', 
        # use it instead of creating a new one
        if [ -d "${tmpdir}/zip/I${group}" ]; then
          destdir="${tmpdir}/zip/I${group}"
        fi
        # append the path suffix
        if [ -n "$path_suffix" ]; then destdir=${destdir}/${path_suffix}; fi
        mkdir -p "$destdir"
        mv "${tmpdir}/zip/$j" "${destdir}/${fname}" 2>/dev/null
        debug "Moved signature file ${fname}:\nmv ${tmpdir}/zip/$j ${destdir}/${fname} 2>/dev/null"
      fi
    fi
  done
  
  # default: replace the existing ZIP file
  zipdest="$i"
  # prepare the output ZIP file
  zipfile=$(basename "$i")
  # ZIP file has a file suffix (set by option -s)
  if [ -n "$suffix" ]; then
    ext=${i##*.}
    name=$(basename "$i" .$ext)
    zipfile=${name}${suffix}.${ext}
  fi
  
  if [ -n "$bundle" ]; then  # bundle mode
    # just move to temporary bundle dir
    zipdest=${tmpdir}/bundle/${zipfile}
  else # no bundle mode
    if [ -n "$outdir" ]; then
      # store ZIP file to output directory (having the same basename)
      zipdest=$(echo "${outdir}/${zipfile}" | sed s#//*#/#g) 
    else
      # if a suffix was set, new ZIP file is added on same level as origin one
      dir=$(dirname "$i")
      if [ -n "$dir" ]; then
        zipdest=$(echo "${dir}/${zipfile}" | sed s#//*#/#g) 
      fi
    fi
  fi
  rm -f $zipdest 2>/dev/null
    
  debug "(cd $tmpdir/zip; zip $zipopts -r ../$zipfile *)"
  (cd $tmpdir/zip; zip $zipopts -r ../$zipfile *)
  debug "${tmpdir}/${zipfile} $zipdest 2>/dev/null"
  mv "${tmpdir}/${zipfile}" "$zipdest" 2>/dev/null
  if [ "$?" != "0" ]; then
    echo "Couldn't create package \"$zipdest\", exit". >&2
    error_cleanup
    exit 1
  fi
  if [ -n "$bundle" ]; then
    echo Added package \"$(basename $zipdest)\" to bundle.
  else
    if [ "$zipdest" = "$i" ]; then
      echo Replaced package \"$zipdest\" successfully.
    else
      echo Created package \"$zipdest\" successfully.
    fi
  fi
  rm -rf $tmpdir/zip* 2>/dev/null
done

if $enoact; then exit 2; fi

# create the bundle
if [ -n "$bundle" ]; then
  (cd $tmpdir/bundle; 
   for i in $(find . -type f -iname "*.zip" -printf '%P\n' 2>/dev/null | xargs); do
     # add filename to package list
     echo "I:1/${i}">> pkg_inst.man
     debug "Added \"${i}\" to bundle list \"pkg_inst.man\"."
   done
  debug "zip $zipopts -r ../bundle.zip *"
  zip $zipopts -r ../bundle.zip *)
  mv "${tmpdir}/bundle.zip" "$bundle" 2>/dev/null
  if [ "$?" != "0" ]; then
    echo "Couldn't create bundle \"$bundle\", exit." >&2
    error_cleanup
    exit 1
  fi
  echo Created bundle \"$bundle\" successfully.
fi

exit 0
