@ECHO OFF
SET sign_file=%1
echo sign_file is %sign_file%
set sign_file=%sign_file:unsigned=signed%
echo sign_file is now %sign_file%
rm %sign_file%
rm -rf sign_temp
mkdir sign_temp
unzip %1 -d sign_temp
"C:\Program Files (x86)\VeriFone\FST\filesignature" vrx_development.fst -nogui
echo copying %2\VXEOSDEV.DAT{! to sign_temp\f1
cp "%2\VXEOSDEV.DAT{!" sign_temp\f1
echo copying %2\VXEOSC.MAN to sign_temp\1
cp "%2\VXEOSC.MAN" sign_temp\1
echo copying %2\VXEOSC2.MAN to sign_temp\1
cp "%2\VXEOSC2.MAN" sign_temp\1
echo copying %2\VXEOSDEV.DAT{!.P7S to sign_temp\1
cp "%2\VXEOSDEV.DAT{!.P7S" sign_temp\1
cd sign_temp
zip -r ..\%sign_file% *
cd ..
rm -rf sign_temp




