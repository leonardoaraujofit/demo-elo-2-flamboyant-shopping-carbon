Follow these instructions to sign and load the VHQ nightly build on an
"unlocked" VX device


	1. Unzip tms-agent-vrx-<VERSION>.zip
	2. cd to where step 1 was unzipped
	3. Start File Signing Tool in dual user mode so that Sign File
		option is enabled.
	4. Make sure your VX app signing card is inserted
	5. run "sign_vrx_dev.bat <VHQ development ZIP file to sign>
		<Directory where device unlock files are located>"

The steps about will generate a "signed" ZIP file with the VHQ Agent that
can be used on "unlocked" VX terminals.  The ZIP file can be included
with your application download (with *unzip set to the name of the ZIP).
If you want to use the svc_tms_tester application included in the dist
package, continue to the steps below.

	6. Use FST to sign test\svc_tms_tester.vsa
	7. Add a VHQconfig.ini file to downloads_svc_tms_tester.txt (if
		you want to update the VHQ configuration)
	8. run "load_svc_tms_tester_w_agent.bat <VX device port number>
		<ZIP file from step 5>"


NOTE: The output from step 5 is for the agent talking to VHQ production
server.  If you are talking to a server other than the VHQ production
server, you will need to edit the ZIP file from step 5 and replace the
following files with the ones for your server:

	<Step 5 ZIP File>\15\VHQFTP.pem
	<Step 5 ZIP File>\15\VHQSrvSSL.pem
	<Step 5 ZIP File>\15\VHQSrvPubKey.vrt

Note that the VHQSrvPubKey.crt file gets renamed with a .vrt extension
instead of a .crt extension.