#include <stdio.h>
#include <string.h>
#include <errno.h>
#if 0 //JB
#include <fcntl.h>
#include <grp.h>
#include <libgen.h>
#endif
#include <stdarg.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#if 0 //JB
#include <pwd.h>
#endif
#include <unistd.h>
#include "svc_tms.h"

#include <log/syslog.h>
#include <log/liblog.h>
#include <log/syslogcmd.h>
#include <eoslog.h>

#if 0 //JB
#include <fancypants/Evas.h>
#include <fancypants/Ecore.h>
#include <fancypants/Ecore_Evas.h>

#include <fancypants/libfp/fp.h>
#include <fancypants/libfp/fp_theme.h>
#include <fancypants/libfp/fp_image.h>
#include <fancypants/libfp/fp_container.h>
#include <fancypants/libfp/fp_button.h>
#include <fancypants/libfp/fp_checkbox.h>
#include <fancypants/libfp/fp_text.h>
#include <fancypants/libfp/fp_list.h>
#include <fancypants/libfp/fp_multivalue.h>
#include <fancypants/libfp/fp_multiselect.h>
#include <fancypants/libfp/fp_scrollbar.h>

#include <fancypants/libfp/media/fp_media.h>
#include <fancypants/libfp/media/fp_media_enum.h>

#include "vf_sig_capture.h"
#include "vf_textbox.h"
#include "vf_dialog.h"
#endif

#define CONFIG_BUTTON_IS_GET_CONFIG		0

typedef unsigned int uint32;

#if 0 //JB
/* The Application GUI */
struct appdata
{
	struct fp *fp;
	Evas_Object *bg;
	Evas_Object *titleText;
	Evas_Object *textRect;
	Evas_Object *labelText1;
	Evas_Object *labelText2;
	Evas_Object *horizLine1;
	Evas_Object *horizLine2;
	Evas_Object *vertLine;
	Evas_Object *appStatus;
	Evas_Object *appStatus2;

	Evas_Object *acceptButton;
	Evas_Object *rejectButton;
	Evas_Object *viewlogButton;
	Evas_Object *contactServerButton;
	Evas_Object *getConfigButton;

	Evas_Object *logDeleteButton;
	Evas_Object *logExitButton;

	Evas_Object *logContainer;
};

struct appdata *app;


void CreateGradient(struct appdata *app);
void CreateControls(struct appdata *app);
void CreateText(struct appdata *app, Evas_Object **pObj, const char *pFontFace, int iSize, const char *pText, int iX, int iY, int iRed, int iGreen, int iBlue);
void CreateRectangle(struct appdata *app, Evas_Object **pObj, int iY, int iH, int iRed, int iGreen, int iBlue, int iAlpha);
void CreateLine(struct appdata *app, Evas_Object **pObj, int x1, int y1, int x2, int y2, int iRed, int iGreen, int iBlue);
void CreateButton(struct appdata *app, Evas_Object **pObj, const char *pText, int iX, int iY, int iWidth, int iHeight, fp_button_clicked_cb cb);
void Clicked_AcceptButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event);
void Clicked_RejectButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event);
void Clicked_ViewLogButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event);
void Clicked_ContactServerButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event);
void Clicked_GetConfigButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event);
void Clicked_DeleteButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event);
void Clicked_ExitButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event);
#endif

char *readFile(char *fn, int mode);

#define PAD_SPACE 	3
#define ALIGN_LEFT		-1000
#define ALIGN_CENTER 	-2000
#define ALIGN_RIGHT 	-3000


#ifndef boolean
#define boolean unsigned char
#define TRUE 1
#define FALSE 0
#endif



/* Allow 20 Kb for the log file */
#define SVC_TMS_TESTER_LOG_FILE						"svc_tms_tester_log"
#define SVC_TMS_TESTER_LOG_FILE_SIZE_LIMIT			(20 * 1024)
#define printf(fmt, ...)	LOG_PRINTFF(1, fmt, ##__VA_ARGS__)
#define LogMsg(fmt, ...)	LOG_PRINTFF(1, fmt, ##__VA_ARGS__)

boolean bVerbose = TRUE;

static boolean backup_requested = FALSE;
static boolean backup_active = FALSE;
static boolean third_active = FALSE;
static char prevInstance1[MAX_SERVER_INSTANCE_NAME_LEN];
static char prevInstance2[MAX_SERVER_INSTANCE_NAME_LEN];


typedef enum
{
	VERIFY_READ_PERMISSION,
	VERIFY_WRITE_PERMISSION,
	VERIFY_EXECUTE_PERMISSION,
	VERIFY_RW_PERMISSION,
	VERIFY_RWX_PERMISSION,
}verify_permission_t;

bool manualStateControl   = FALSE;
bool manualStateBUSY      = FALSE;
bool manualStatePOSTPONED = FALSE;


#if 0 //MM
int LogMsgEx(const char *fname, const char *string, va_list ap)
{
	int iFile;
	int r = 0;
#if 0 //JB
	struct stat log_stat;
#endif
	struct timeval cur_time;
	struct tm local;
	time_t t;

	gettimeofday(&cur_time, NULL);
	t = cur_time.tv_sec;
	linux_localtime(&t, &local);

	while ((*string == '\n') || (*string == '\r'))
	{
		LOG_PRINTF("%s", *string);
		string++;
	}

	//printf("%02d:%02d:%02d:%02d: ", local.tm_hour, local.tm_min, local.tm_sec, (int)cur_time.tv_usec/1000);
	LOG_PRINTF("%02d:%02d:%02d: ", local.tm_hour, local.tm_min, local.tm_sec);

	r = vprintf (string, ap);

#if 0 //JB
	/* If the logs are too big, rename them to svc_tms_tester_log1, svc_tms_tester_log2*/
	if (stat(fname, &log_stat) == 0)
	{
		if (log_stat.st_size > SVC_TMS_TESTER_LOG_FILE_SIZE_LIMIT)
		{
			char szName1[256], szName2[256];
			sprintf(szName1, "%s.1", fname);
			sprintf(szName2, "%s.2", fname);
			rename(szName1, szName2);
			rename(SVC_TMS_TESTER_LOG_FILE, szName1);
		}
	}
#else
	if (dir_get_file_size(fname) > SVC_TMS_TESTER_LOG_FILE_SIZE_LIMIT)
	{
		char szName1[256], szName2[256];
		sprintf(szName1, "%s.1", fname);
		sprintf(szName2, "%s.2", fname);
		_rename(szName1, szName2);
		_rename(SVC_TMS_TESTER_LOG_FILE, szName1);
	}
#endif

#if 0 //JB
#ifndef WIN32
	iFile= open(fname, O_CREAT | O_APPEND | O_RDWR, S_IRWXU);
#else
	iFile= open(fname, O_CREAT | O_APPEND | O_RDWR, _S_IREAD | _S_IWRITE);
#endif
#else
	iFile = open(fname, O_CREAT | O_APPEND | O_RDWR);
#endif
	if (iFile!=-1)
	{
		char* szMsg;
		char* szMsgWithTime;


		szMsg = malloc(25 * 1024);
		szMsgWithTime = malloc((25 * 1024) + 50);

		r = 0;
		if (szMsg)
		{
			r = vsprintf (szMsg, string, ap);
		}

		if (szMsgWithTime && (r>0))
		{
			sprintf(szMsgWithTime, "%d/%d/%d %02d:%02d:%02d: %s", local.tm_mon+1, local.tm_mday, (local.tm_year % 100), local.tm_hour, local.tm_min, local.tm_sec, szMsg);
			write(iFile, szMsgWithTime, strlen(szMsgWithTime));
			LOG_PRINTF("%s\n", szMsgWithTime);
		}

		LOG_PRINTF("%s\n", szMsgWithTime);
		if (szMsg)
		{
			//DebugMsg("%s: Freeing szMsg @ 0x%08x\n", __FUNCTION__, szMsg);
			free(szMsg);
			szMsg = NULL;
		}
		if (szMsgWithTime)
		{
			//DebugMsg("%s: Freeing szMsgWithTime @ 0x%08x\n", __FUNCTION__, szMsgWithTime);
			free(szMsgWithTime);
			szMsgWithTime = NULL;
		}

		close(iFile);

#if 0 //JB
		chmod(fname, 0666 );
#endif
	}


	return r;
}

int LogMsg(const char *string, ...)
{
	int ret_val;

	va_list ap;
	va_start (ap, string);
	ret_val = LogMsgEx(SVC_TMS_TESTER_LOG_FILE, string, ap);
	va_end(ap);

	return ret_val;
}
#endif


unsigned char FileExists(const char *pszName)
{
	// this is slight odd, but if the error on this function is anything but ENOENT, then the file exists but we can access it...
	// but it exists, so return true in that case
	int iSize;
	LOG_PRINTF("checking existence of file \"%s\"\n", pszName);
	errno = 0;
	iSize = dir_get_file_sz(pszName);
	if (-1 == iSize && errno == ENOENT)
	{
		LOG_PRINTF("apparently, \"%s\" doesn't exist (iSize= %d, errno=%d)\n", pszName, iSize, errno);
		return FALSE;
	}
	LOG_PRINTF("\"%s\" exists\n", pszName);
	return TRUE;
}



///////////////////////////////////////////////////////////////////////////////
///
///  \fn       void VerifyFilePermissions()
///  \brief    Verifies the agent has permissions to the file in question
///
///  Returns TRUE if the permissions seem correct, FALSE if not
////////////////////////////////////////////////////////////////////////////////
boolean VerifyFilePermissions(char* pszConfigFile, verify_permission_t permission_type)
{
#if 0 //JB
	uid_t my_uid = getuid();
	int num_groups;
	gid_t my_gids[64];
	struct stat file_stat;
	char permissions_str[16];

	/* Make sure the file exists */
	if (stat(pszConfigFile, &file_stat) != 0)
	{
		LogMsg("%s: %s does not exist - returning FALSE\n", __FUNCTION__, pszConfigFile);
		return FALSE;
	}


	memset(permissions_str, 0, sizeof(permissions_str));

	/* Setup directory */
	if (S_ISDIR(file_stat.st_mode))
		strcpy(permissions_str, "d");
	else
		strcpy(permissions_str, "-");

	/* Setup owner permissions */
	if (file_stat.st_mode & S_IRUSR)
		strcat(permissions_str, "r");
	else
		strcat(permissions_str, "-");
	if (file_stat.st_mode & S_IWUSR)
		strcat(permissions_str, "w");
	else
		strcat(permissions_str, "-");
	if (file_stat.st_mode & S_IXUSR)
		strcat(permissions_str, "x");
	else
		strcat(permissions_str, "-");


	/* Setup group permissions */
	if (file_stat.st_mode & S_IRGRP)
		strcat(permissions_str, "r");
	else
		strcat(permissions_str, "-");
	if (file_stat.st_mode & S_IWGRP)
		strcat(permissions_str, "w");
	else
		strcat(permissions_str, "-");
	if (file_stat.st_mode & S_IXGRP)
		strcat(permissions_str, "x");
	else
		strcat(permissions_str, "-");


	/* Setup other permissions */
	if (file_stat.st_mode & S_IROTH)
		strcat(permissions_str, "r");
	else
		strcat(permissions_str, "-");
	if (file_stat.st_mode & S_IWOTH)
		strcat(permissions_str, "w");
	else
		strcat(permissions_str, "-");
	if (file_stat.st_mode & S_IXOTH)
		strcat(permissions_str, "x");
	else
		strcat(permissions_str, "-");

	LogMsg("%s: permissions of %s - %s\n", __FUNCTION__, pszConfigFile, permissions_str);


	if (permission_type == VERIFY_READ_PERMISSION)
	{
		if (file_stat.st_mode & S_IROTH)
		{
			LogMsg("%s: we have 'other' read priveleges - returning TRUE\n", __FUNCTION__);
			return TRUE;
		}

		if (file_stat.st_mode & S_IRGRP)
		{
			int i;
			num_groups = getgroups(64, my_gids);

			for (i = 0; i < num_groups; i++)
			{
				if (my_gids[i] == file_stat.st_gid)
				{
					LogMsg("%s: we have group read priveleges as gid %d - returning TRUE\n", __FUNCTION__, my_gids[i]);
					return TRUE;
				}
				LogMsg("%s: no group read priveleges in gid %d (file gid = %d) - returning TRUE\n", __FUNCTION__, my_gids[i], file_stat.st_gid);
			}
		}

		if ((my_uid == file_stat.st_uid) && (file_stat.st_mode & S_IRUSR))
		{
			LogMsg("%s: we have owner read priveleges - returning TRUE\n", __FUNCTION__);
			return TRUE;
		}

		LogMsg("%s: no read priveleges - setting up permission string\n", __FUNCTION__);
	}
	else if (permission_type == VERIFY_WRITE_PERMISSION)
	{
		if (file_stat.st_mode & S_IWOTH)
		{
			LogMsg("%s: we have 'other' write priveleges - returning TRUE\n", __FUNCTION__);
			return TRUE;
		}

		if (file_stat.st_mode & S_IWGRP)
		{
			int i;
			num_groups = getgroups(64, my_gids);

			for (i = 0; i < num_groups; i++)
			{
				if (my_gids[i] == file_stat.st_gid)
				{
					LogMsg("%s: we have group write priveleges as gid %d - returning TRUE\n", __FUNCTION__, my_gids[i]);
					return TRUE;
				}
				LogMsg("%s: no group write priveleges in gid %d (file gid = %d)\n", __FUNCTION__, my_gids[i], file_stat.st_gid);
			}
		}

		if ((my_uid == file_stat.st_uid) && (file_stat.st_mode & S_IWUSR))
		{
			LogMsg("%s: we have owner write priveleges - returning TRUE\n", __FUNCTION__);
			return TRUE;
		}

		LogMsg("%s: no write priveleges - setting up permission string\n", __FUNCTION__);
	}
	else
		LogMsg("%s: permission type %d not supported yet\n", __FUNCTION__, permission_type);


	LogMsg("%s: Invalid permissions on %s - %s (Need read priveleges)\n", __FUNCTION__, pszConfigFile, permissions_str);
#endif
	return FALSE;
}





static int CopyFile(const char *pszSrc, const char *pszDest)
{
	int fpSrc, fpDst;
	char buf[8*1024];
	int n_read;

	LogMsg("Copyfile: copying %s to %s\r\n", pszSrc, pszDest);
	fpSrc= open(pszSrc, O_RDONLY);
	fpDst = open(pszDest, O_RDWR | O_CREAT | O_TRUNC);

	// return an error if we can't open source and dest files
	if ((fpSrc == -1) || (fpDst == -1))
	{
		if (fpSrc == -1)
			LogMsg("%s: could not open souce %s\n", __FUNCTION__, pszSrc);
		else
			close(fpSrc);

		if (fpDst == -1)
			LogMsg("%s: could not open dest %s\n", __FUNCTION__, pszDest);
		else
			close(fpDst);

		return -1;
	}

	LogMsg("Copyfile: files opened, src: %lx dst:%lx\r\n", (long)fpSrc, (long)fpDst);
	//if ((fpSrc > 0) && (fpDst > 0))
	{
		while ((n_read = read(fpSrc, buf, sizeof(buf))) > 0)
		{
			if (write(fpDst, buf, n_read) < 0) {
//				g_warning(_("writing to %s failed.\n"), dest);
				close(fpDst);
				close(fpSrc);
				_remove(pszDest);
				return -1;
			}
		}
	}

	LogMsg("Copyfile: closing files\r\n");
	close(fpSrc);
	close(fpDst);
	LogMsg("Copyfile: done\r\n");
	return 0;
}




int GetSerialNumber(char *pszSerialNumber)
{
	int sn_len;
	char szSerialNum[32 + 1];

	memset(szSerialNum, 0, sizeof(szSerialNum));
	SVC_INFO_SERLNO(szSerialNum);		// this call cannot fail, apparently.

	printf("%s: Serial number is %s\n", __FUNCTION__, szSerialNum);
	if (strlen(szSerialNum) == 0)
	{
		memset(szSerialNum, 0, sizeof(szSerialNum));
		strcpy(szSerialNum, "123-456-789");
		printf("%s: Replaced missing Serial number with %s\n", __FUNCTION__, szSerialNum);
	}

	sn_len = strlen(szSerialNum);
	memcpy(pszSerialNumber, szSerialNum, sn_len + 1);

	return sn_len;
}



static char* status_to_string(int status)
{
	switch (status)
	{
	case TMS_STATUS_SUCCESS:
		return "Success";
	case TMS_STATUS_ERROR:
		return "Error";
	case TMS_STATUS_UNSUPPORTED_FEATURE:
		return "Unsupported Feature";
	case TMS_STATUS_CONTENT_AVAILABLE:
		return "Content Avaliable";
	case TMS_STATUS_NO_CONSUMER:
		return "No Consumer";
	case TMS_STATUS_FILENAME_ERROR:
		return "Filename ERROR";
	case TMS_STATUS_CONTENT_FAILURE:
		return "Status Content Failure";

	case TMS_STATUS_MSGQ_FAILURE:
		return "Message Queue FAILURE";
	case TMS_STATUS_MSGSND_FAILURE:
		return "Message Send FAILURE";
	case TMS_STATUS_MSGRCV_FAILURE:
		return "Message Receive FAILURE";
	case TMS_STATUS_APP_EVENT_AVAIL:
		return "App Event Available";

	case TMS_STATUS_REQUESTED:
		return "Requested";

	case TMS_STATUS_REGISTER_FAIL:
		return "Register Fail";
	case TMS_STATUS_REGISTER_NAME_TOO_LONG:
		return "Register Name Too Long";
	case TMS_STATUS_EINVAL:
		return "Error in Value";
	case TMS_STATUS_AGENT_NOT_RUNNING:
		return "Agent Not Running";

	case TMS_STATUS_SERVER_INSTANCE_ERROR:
		return "Srv Inst Error";
	case TMS_STATUS_SERVER_INSTANCE_LOCK_ERROR:
		return "Srv Inst Lock Error";
	case TMS_STATUS_SERVER_INSTANCE_LOCK_TIMEOUT_ERROR:
		return "Srv Inst Lock Timeout Error";
	case TMS_STATUS_SERVER_INSTANCE_ALREADY_LOCKED:
		return "Srv Inst Already Locked";
	case TMS_STATUS_SERVER_INSTANCE_MAX_LOCK_REQ_EXCEEDED:
		return "Too Many SrvInst Lock Requests";
	case TMS_STATUS_SERVER_INSTANCE_UNLOCK_ERROR:
		return "Srv Inst Unlock Error";
	case TMS_STATUS_SERVER_INSTANCE_CONFIG_LOAD_ERROR:
		return "Srv Inst Config Load Error";

	case TMS_STATUS_AGENT_BUSY_ERROR:
		return "Agent Busy Error";
	case TMS_STATUS_POSTPONED:
		return (char*) "Postponed";
	case TMS_STATUS_CANCELLED:
		return (char*) "Cancelled";
	case TMS_STATUS_AGENT_CONTACTSRV_ERROR:
		return "Agent not registered on the server yet";
	case TMS_STATUS_API_SYNC_ERROR:
		return "Lost SYNC of TMS exchange";

	default:
		return "Unknown";
	}

	return "Unknown";
}

static char* msgType_to_string(int msgType)
{
	switch (msgType)
	{
	case TMS_EVT_CALL_SERVER_RESPONSE:
		return "Call Server";
	case TMS_EVT_SET_APP_STATE:
		return "Get APP State";
	case TMS_EVT_GET_TMS_CONFIG_RESPONSE:
		return "Get TMS Config";
	case TMS_EVT_SET_TMS_CONFIG_RESPONSE:
		return "Set TMS Config";
	case TMS_EVT_SET_APP_INFO:
		return "Get APP Info";
	case TMS_EVT_SET_PARM_LIST:
		return "Get Parameter List";
	case TMS_EVT_GET_FILE:
		return "Get File";
	case TMS_EVT_PUT_FILE:
		return "Put File";
	case TMS_EVT_DEL_FILE:
		return "Delete File";
	case TMS_EVT_NOTIFICATION:
		return "Event Notfication";
	case TMS_EVT_DO_TRANSACTION:
		return "Do Transaction";
	case TMS_EVT_REGISTER_APP_RESPONSE:
		return "Register";
	case TMS_EVT_UNREGISTER_APP_RESPONSE:
		return "Un-Register";

	case TMS_EVT_GET_SERVER_INSTANCE:
		return "Get Srv Inst";
	case TMS_EVT_LOCK_SERVER_INSTANCE:
		return "Lock Srv Inst";
	case TMS_EVT_RELEASE_SERVER_INSTANCE:
		return "Release Srv Inst";

	case TMS_EVT_APP_ALERT_RESULT:
		return "App Event Result";

	case TMS_EVT_CLEAR_APP_INFO_RESULT:
		return "Clear App Info Result";

	case TMS_EVT_API_ERRORS:
		return "API Usage Error";

	default:
		return "Unknown";
	}

	return "Unknown";
}


#define DELETE_BUTTONS_IN_CB	0



struct tmsEventData* appEvent;
boolean waiting_for_usr_response = FALSE;



#if 0

// Simple exe to call tms service to deliver a test content update to an app
// Makes interface from WDE PHP simple.
// Note this will setup the content registration and wait for content
void* MainLoopContent(void* pvData)
{
	struct tmsRegisterReturn* regRtn;
	struct passwd *psPasswd;
	char contentEventKey[32];

	printf("%s: MainLoopContent started\n", __FUNCTION__);
	SVC_WAIT(5 * 1000);

	psPasswd = getpwuid(getuid());

	LogMsg("svc_tms_tester: Registering for content as %s\n", psPasswd->pw_name);
	regRtn = tms_registerForContent();
	if (regRtn && (regRtn->status == TMS_STATUS_SUCCESS))
	{

		memcpy(contentEventKey, regRtn->eventkey, 32);
		LogMsg("svc_tms_tester: Registered for content - Event Key = %s - freeing regRtn\n", contentEventKey);

		free(regRtn);
		regRtn = NULL;

		while (1)
		{

			if (app->appStatus2)
			{
				evas_object_hide(app->appStatus2);
				evas_object_del(app->appStatus2);
				app->appStatus2 = NULL;
			}
			evas_object_text_text_set(app ->appStatus, "Waiting For Content");
			evas_render(app->fp->e);

			LogMsg("svc_tms_tester: Waiting for content on Key %s\n", contentEventKey);
			contentEvent = tms_getContentEvent(contentEventKey, 0);

			if (contentEvent)
			{
				if (app->appStatus2)
				{
					evas_object_hide(app->appStatus2);
					evas_object_del(app->appStatus2);
					app->appStatus2 = NULL;
				}
				evas_object_text_text_set(app ->appStatus, "Received Content Update - Accept or Reject");

				LogMsg("svc_tms_tester: Got a content event\n\tStatus = %s\n\tType = %d\n\tContent ID = %d\n\tFilename = %s\n\tfilepath = %s\n",
					status_to_string(contentEvent->status), contentEvent->contentType, contentEvent->contentID, contentEvent->filename, contentEvent->filepath);

				CreateButton(app, &app ->acceptButton, "Accept Content", (PAD_SPACE * 2), 185, 140, 40, Clicked_AcceptButtonCB);
				CreateButton(app, &app ->rejectButton, "Reject Content", 2 * (PAD_SPACE * 2) + 140, 185, 140, 40, Clicked_RejectButtonCB);

				evas_render(app->fp->e);

				waiting_for_usr_response = TRUE;

				while(waiting_for_usr_response)
				{
					printf("%s: Waiting for user response\n", __FUNCTION__);
					SVC_WAIT(3 * 1000);
				}


#if (!DELETE_BUTTONS_IN_CB)
				printf("%s: Got user response - deleting buttons\n", __FUNCTION__);

				/* We can remove the accept button */
				if (app->acceptButton)
				{
					evas_object_hide(app->acceptButton);
					evas_object_del(app->acceptButton);
					app->acceptButton = NULL;
				}

				/* We can remove the reject button */
				if (app->rejectButton)
				{
					evas_object_hide(app->rejectButton);
					evas_object_del(app->rejectButton);
					app->rejectButton = NULL;
				}
#else
				printf("%s: Got user response\n", __FUNCTION__);
#endif
			}
		}

	}
	else
	{
		LogMsg("svc_tms_tester ERROR: tms_registerForContent() returned an error - regRtn = 0x%08x, regRtn->status = %s, errno = %d\n",
			(int)regRtn, regRtn ? status_to_string(regRtn->status) : "NULL", errno);
	}

	if (regRtn)
		free(regRtn);

	return 0;

}

#endif


#ifdef VHQ_VX
#define APP_PARM_DATA_FILE					"I:/AppParmFile.dat"
#else
#define APP_PARM_DATA_FILE					"/tmp/share/AppParmFile.dat"
#endif
#define APP_PARM_DATA_FILE_TEST_DATA		"test.ini SHA-1 1234567890123456789012345678901234567890"


struct tmsAppParameter custom_parameters[] =
{
	{"DeviceId", "10896028", TMS_PARAMETER_TYPE_IDENTIFIER},

	{" 1CardInsertCount", "30", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 2CardSwipeCount", "6", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 3ContactlessTapCount", "20", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 4RestartCount", "7", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{ " 5191115053913035_ConnectionType", {0}, TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{" 6191115053913035_DataEntryTimeInMs", "-19852", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 7191115053913035_EMVApp", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 8191115053913035_HostConnectTimeInMs", "", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 9191115053913035_HostResponseTimeInMs", "4664", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 10191115053913035_PrintingTimeInMs", "", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 11191115053913035_TotalCommsTimeInMs", "6458", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 12191115053913035_TxnDateTime", "150219000539", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 13191115053913035_TxnStatus", "Unknown", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 14191115053913035_TxnTotalTimeInSec", "159", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 15191115053913035_TxnType", "Logon", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 1617111523362211552_ConnectionType", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 1717111523362211552_DataEntryTimeInMs", "-7545", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 1817111523362211552_EMVApp", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 1917111523362211552_HostConnectTimeInMs", "1097", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 2017111523362211552_HostResponseTimeInMs", "6448", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 2117111523362211552_TotalCommsTimeInMs", "7545", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 2217111523362211552_TxnDateTime", "150217233622", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 2317111523362211552_TxnStatus", "Unknown", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 2417111523362211552_TxnTotalTimeInSec", "2", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 2517111523362211552_TxnType", "Settlement", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 261811153342011968_ConnectionType", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 271811153342011968_DataEntryTimeInMs", "-3860", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 281811153342011968_EMVApp", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 291811153342011968_HostConnectTimeInMs", "920", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 301811153342011968_HostResponseTimeInMs", "2940", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 311811153342011968_TotalCommsTimeInMs", "3860", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 321811153342011968_TxnDateTime", "150218033420", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 331811153342011968_TxnStatus", "Unknown", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 341811153342011968_TxnTotalTimeInSec", "2", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 351811153342011968_TxnType", "Settlement", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 3617111523362918691_ConnectionType", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 3717111523362918691_DataEntryTimeInMs", "-7316", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 3817111523362918691_EMVApp", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 3917111523362918691_HostConnectTimeInMs", "868", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 4017111523362918691_HostResponseTimeInMs", "6448", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 4117111523362918691_TotalCommsTimeInMs", "7316", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 4217111523362918691_TxnDateTime", "150217233629", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 4317111523362918691_TxnStatus", "Unknown", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 4417111523362918691_TxnTotalTimeInSec", "2", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 4517111523362918691_TxnType", "Settlement", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 461811153342721410_ConnectionType", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 471811153342721410_DataEntryTimeInMs", "-3772", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 481811153342721410_EMVApp", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 491811153342721410_HostConnectTimeInMs", "832", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 501811153342721410_HostResponseTimeInMs", "2940", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 511811153342721410_TotalCommsTimeInMs", "3772", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 521811153342721410_TxnDateTime", "150218033427", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 531811153342721410_TxnStatus", "Unknown", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 541811153342721410_TxnTotalTimeInSec", "2", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 551811153342721410_TxnType", "Settlement", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 5617111523363619755_ConnectionType", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 5717111523363619755_DataEntryTimeInMs", "-7300", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 5817111523363619755_EMVApp", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 5917111523363619755_HostConnectTimeInMs", "852", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 6017111523363619755_HostResponseTimeInMs", "6448", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 6117111523363619755_TotalCommsTimeInMs", "7300", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 6217111523363619755_TxnDateTime", "150217233636", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 6317111523363619755_TxnStatus", "Unknown", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 6417111523363619755_TxnTotalTimeInSec", "2", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 6517111523363619755_TxnType", "Settlement", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 661811153343519448_ConnectionType", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 671811153343519448_DataEntryTimeInMs", "-3777", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 681811153343519448_EMVApp", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 691811153343519448_HostConnectTimeInMs", "837", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 701811153343519448_HostResponseTimeInMs", "2940", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 711811153343519448_TotalCommsTimeInMs", "3777", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 721811153343519448_TxnDateTime", "150218033435", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 731811153343519448_TxnStatus", "Unknown", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 741811153343519448_TxnTotalTimeInSec", "2", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 751811153343519448_TxnType", "Settlement", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 761811153344216709_ConnectionType", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 771811153344216709_DataEntryTimeInMs", "-3611", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 781811153344216709_EMVApp", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 791811153344216709_HostConnectTimeInMs", "671", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 801811153344216709_HostResponseTimeInMs", "2940", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 811811153344216709_TotalCommsTimeInMs", "3611", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 821811153344216709_TxnDateTime", "150218033442", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 831811153344216709_TxnStatus", "Unknown", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 841811153344216709_TxnTotalTimeInSec", "2", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 851811153344216709_TxnType", "Settlement", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 8617111523365130822_ConnectionType", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 8717111523365130822_DataEntryTimeInMs", "-7121", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 8817111523365130822_EMVApp", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 8917111523365130822_HostConnectTimeInMs", "673", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 9017111523365130822_HostResponseTimeInMs", "6448", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 9117111523365130822_TotalCommsTimeInMs", "7121", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 9217111523365130822_TxnDateTime", "150217233651", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 9317111523365130822_TxnStatus", "Unknown", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 9417111523365130822_TxnTotalTimeInSec", "2", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 9517111523365130822_TxnType", "Settlement", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 9617111523365816936_ConnectionType", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 9717111523365816936_DataEntryTimeInMs", "-7340", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 9817111523365816936_EMVApp", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 9917111523365816936_HostConnectTimeInMs", "892", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 10017111523365816936_HostResponseTimeInMs", "6448", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 10117111523365816936_TotalCommsTimeInMs", "7340", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 10217111523365816936_TxnDateTime", "150217233658", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 10317111523365816936_TxnStatus", "Unknown", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 10417111523365816936_TxnTotalTimeInSec", "2", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 10517111523365816936_TxnType", "Settlement", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 106201502171130_BatteryCapacity", "1701 mAH", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 107201502171130_BatteryRemainingLevel", "71 % ", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 108201502171130_BatterySerialNumber", "BA1439298535", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 109201502171130_BatteryVoltage", "8110 mV", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 110201502171130_DeviceImei", "013454000647580", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 111201502171130_IPAddress", "10.241.165.150", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 112201502171130_IPDNS1", "139.130.4.4", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 113201502171130_IPDNS2", "203.50.2.71", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 114201502171130_IPGatewayAddress", "10.241.165.151", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 115201502171130_IPSubnetMask", "255.255.255.255", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 116201502171130_PPID", "0112060010896028", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 117201502171130_RadioAccessTechnology", "3G", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 118201502171130_SignalFrequency", "850 MHz", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 119201502171130_SignalStrength", "23 RSSI", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 120201502171130_SimImsi", "505013499210493", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 121201502181243_BatteryCapacity", "1700 mAH", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 122201502181243_BatteryRemainingLevel", "99 % ", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 123201502181243_BatterySerialNumber", "BA1439298535", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 124201502181243_BatteryVoltage", "8280 mV", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 125201502181243_DeviceImei", "013454000647580", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 126201502181243_IPAddress", "10.241.6.57", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 127201502181243_IPDNS1", "139.130.4.4", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 128201502181243_IPDNS2", "203.50.2.71", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 129201502181243_IPGatewayAddress", "10.241.6.58", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 130201502181243_IPSubnetMask", "255.255.255.255", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 131201502181243_PPID", "0112060010896028", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 132201502181243_RadioAccessTechnology", "3G", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 133201502181243_SignalFrequency", "2100 MHz", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 134201502181243_SignalStrength", "15 RSSI", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 135201502181243_SimImsi", "505013499210493", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 136201502181248_BatteryCapacity", "1700 mAH", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 137201502181248_BatteryRemainingLevel", "99 % ", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 138201502181248_BatterySerialNumber", "BA1439298535", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 139201502181248_BatteryVoltage", "8280 mV", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 140201502181248_DeviceImei", "013454000647580", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 141201502181248_IPAddress", "10.241.6.57", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 142201502181248_IPDNS1", "139.130.4.4", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 143201502181248_IPDNS2", "203.50.2.71", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 144201502181248_IPGatewayAddress", "10.241.6.58", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 145201502181248_IPSubnetMask", "255.255.255.255", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 146201502181248_PPID", "0112060010896028", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 147201502181248_RadioAccessTechnology", "3G", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 148201502181248_SignalFrequency", "2100 MHz", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 149201502181248_SignalStrength", "17 RSSI", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 150201502181248_SimImsi", "505013499210493", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 151201502181546_BatteryCapacity", "1700 mAH", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 152201502181546_BatteryRemainingLevel", "98 % ", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 153201502181546_BatterySerialNumber", "BA1439298535", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 154201502181546_BatteryVoltage", "8210 mV", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 155201502181546_DeviceImei", "013454000647580", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 156201502181546_IPAddress", "10.241.154.58", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 157201502181546_IPDNS1", "139.130.4.4", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 158201502181546_IPDNS2", "203.50.2.71", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 159201502181546_IPGatewayAddress", "10.241.154.59", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 160201502181546_IPSubnetMask", "255.255.255.255", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 161201502181546_PPID", "0112060010896028", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 162201502181546_RadioAccessTechnology", "3G", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 163201502181546_SignalFrequency", "2100 MHz", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 164201502181546_SignalStrength", "14 RSSI", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 165201502181546_SimImsi", "505013499210493", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 166201502181551_BatteryCapacity", "1700 mAH", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 167201502181551_BatteryRemainingLevel", "98 % ", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 168201502181551_BatterySerialNumber", "BA1439298535", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 169201502181551_BatteryVoltage", "8210 mV", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 170201502181551_DeviceImei", "013454000647580", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 171201502181551_IPAddress", "10.241.154.58", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 172201502181551_IPDNS1", "139.130.4.4", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 173201502181551_IPDNS2", "203.50.2.71", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 174201502181551_IPGatewayAddress", "10.241.154.59", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 175201502181551_IPSubnetMask", "255.255.255.255", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 176201502181551_PPID", "0112060010896028", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 177201502181551_RadioAccessTechnology", "3G", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 178201502181551_SignalFrequency", "2100 MHz", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 179201502181551_SignalStrength", "18 RSSI", TMS_PARAMETER_TYPE_DIAGNOSTIC},

	{" 180201502181551_SimImsi", "505013499210493", TMS_PARAMETER_TYPE_DIAGNOSTIC},



};




#define SEND_DEFINED_NUM_PARAMS			0
#define NUM_DIAG_PARAMS_TO_SEND			200

void createParamList(struct tmsAppInfo *appInfo, uint32 param_mask)
{
	int i, entry_index;
	int num_params = 0;
	struct tmsAppParameter* parameter_array = NULL;


	/* Find out how many parameters to send */
	for (i = 0; i < (sizeof(custom_parameters) / sizeof(struct tmsAppParameter)); i++)
	{
		if (custom_parameters[i].parameterType & param_mask)
		{
			num_params++;
			LogMsg("%s: custom_parameter[%d] (type = %d) matches mask 0x%08x - num_params incremented to %d\n", __FUNCTION__, i, custom_parameters[i].parameterType, param_mask, num_params);
		}
	}


#if SEND_DEFINED_NUM_PARAMS
	if (param_mask & TMS_PARAMETER_TYPE_DIAGNOSTIC)
	{
		LogMsg("%s: Allocating %d bytes for %d parameters\n", __FUNCTION__, NUM_DIAG_PARAMS_TO_SEND * sizeof(struct tmsAppParameter), NUM_DIAG_PARAMS_TO_SEND);
		parameter_array = malloc(NUM_DIAG_PARAMS_TO_SEND * sizeof(struct tmsAppParameter));
	}
	else
#endif
	{
		LogMsg("%s: Allocating %d bytes for %d parameters\n", __FUNCTION__, num_params * sizeof(struct tmsAppParameter), num_params);
		parameter_array = (tmsAppParameter *)malloc(num_params * sizeof(struct tmsAppParameter));
	}
	if (parameter_array)
	{
		memset(parameter_array, 0, (num_params * sizeof(struct tmsAppParameter)));
		for (i = 0, entry_index = 0; ((i < (sizeof(custom_parameters) / sizeof(struct tmsAppParameter))) && (entry_index < num_params)); i++)
		{
			if (custom_parameters[i].parameterType & param_mask)
			{
				LogMsg("%s: custom_parameter[%d] (type = %d) matches mask 0x%08x - adding to index %d\n", __FUNCTION__, i, custom_parameters[i].parameterType, param_mask, entry_index);
				memcpy(&parameter_array[entry_index], &custom_parameters[i], sizeof(struct tmsAppParameter));
				entry_index++;
			}
		}
		appInfo->parameterCount = num_params;
	}
	else
		appInfo->parameterCount = 0;


#if SEND_DEFINED_NUM_PARAMS
	if (param_mask & TMS_PARAMETER_TYPE_DIAGNOSTIC)
	{
		for (entry_index = num_params; entry_index < NUM_DIAG_PARAMS_TO_SEND; entry_index++)
		{
			struct tmsAppParameter temp_param;

			sprintf(temp_param.parameterName, "temp_parm_%d", entry_index);
			sprintf(temp_param.parameterValue, "%d", entry_index);
			temp_param.parameterType = TMS_PARAMETER_TYPE_DIAGNOSTIC;

			LogMsg("%s: adding temp_parm %s to index %d\n", __FUNCTION__, temp_param.parameterName, entry_index);
			memcpy(&parameter_array[entry_index], &temp_param, sizeof(struct tmsAppParameter));
		}
		appInfo->parameterCount = NUM_DIAG_PARAMS_TO_SEND;
	}
#endif



	appInfo->parameterList = parameter_array;
}


static int HandleApplicationEvent(struct tmsEventData eventData)
{
	int param_count;
	int ret_val, result;
	int fd;
	char* pszBaseName;
	char disp_str[256];
	char param_file_copy_name[MAX_TMS_FILENAME_LENGTH];
	struct tmsAppInfo appInfo;


	appEvent = &eventData;

#if 1

#if 0 //JB
	if (app->appStatus)
	{
		evas_object_hide(app->appStatus);
		evas_object_del(app->appStatus);
		app->appStatus = NULL;
	}
	if (app->appStatus2)
	{
		evas_object_hide(app->appStatus2);
		evas_object_del(app->appStatus2);
		app->appStatus2 = NULL;
	}
#endif
	memset(disp_str, 0, sizeof(disp_str));
	LOG_PRINTF("%s: Received App Message - %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
	sprintf(disp_str, "Received App Message - %s", msgType_to_string(appEvent->evtType));
#if 0 //JB
	CreateText(app, &app->appStatus, "Vera", 20, disp_str, ALIGN_CENTER, (PAD_SPACE + 55), 255, 192, 192);
#endif
	LogMsg("%s: Got an application event\n\tStatus = %s", __FUNCTION__, status_to_string(appEvent->status));
	LogMsg("\n\tType = %s", msgType_to_string(appEvent->evtType));
	LogMsg("\n\tHandle = %d", appEvent->handle);
	LogMsg("\n\tFilename = %s", appEvent->filename);
	LogMsg("\n\tFileLocation = %s", appEvent->filepath);
	LogMsg("\n\tEvent Mask = 0x%08x", appEvent->eventMask);
	LogMsg("\n\tTransaction Type = %s\n", appEvent->transactionType);

#if 0 //JB
	evas_render(app->fp->e);
#endif
#endif





#if 0 //JB
	if (app->appStatus2)
	{
		evas_object_hide(app->appStatus2);
		evas_object_del(app->appStatus2);
		app->appStatus2 = NULL;
	}
#endif

	switch (appEvent->evtType)
	{
	case TMS_EVT_CALL_SERVER_RESPONSE:
		memset(disp_str, 0, sizeof(disp_str));
		if (appEvent->status == TMS_STATUS_SUCCESS)
		{
			SVC_WAIT(2 * 1000);
			printf("Call Server SUCCESSFUL");
#if 0 //MM
#if 0 //JB
			CreateText(app, &app ->appStatus, "Vera", 20, disp_str, ALIGN_CENTER, (PAD_SPACE + 55), 255, 192, 192);
#endif

			if (strlen(appEvent->filename))
			{

				/* Probably want to register for notifications on this file with inotify since the file will be updated
				a couple of times during a heartbeat. */
				unsigned char resFileFound = FALSE;
				int retries = 20;
				char* tptr = NULL;

				while(retries)
				{
#if 0 //JB
					struct stat resFileStat;

					if (stat(appEvent->filename, &resFileStat) == 0)
					{
						LogMsg("%s: Result file found\n", __FUNCTION__);
						resFileFound = TRUE;
						break;
					}
#endif
					SVC_WAIT(3 * 1000);
					LOG_PRINTF("%s: waiting for heartbeat result file\n", __FUNCTION__);
				}

				if (resFileFound)
					tptr=readFile(appEvent->filename,0);
				if (tptr)
				{
					sprintf(disp_str, "Result=%s", tptr);
					free(tptr);
				}
				else
				{
					sprintf(disp_str, "Error reading result file");
				}
			}
			else
			{
				sprintf(disp_str, "No result file to read");
			}

#if 0 //JB
			CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
#endif
#endif //MM
		}
		else
		{
			sprintf(disp_str, "Call Server FAILED");
#if 0 //JB
			CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
#endif
		}

		LOG_PRINTF("%s: adding contact server button back\n", __FUNCTION__);
#if 0 //JB
		if (app->contactServerButton)
		{
			evas_object_show(app->contactServerButton);
		}
#endif
		break;

	case TMS_EVT_SET_APP_STATE:
		LogMsg("%s: Handling %s - eventMask = 0x%08x\n", __FUNCTION__, msgType_to_string(appEvent->evtType), appEvent->eventMask);

		/* Call service to set app state */
		if (manualStateControl)
		{
			LogMsg("%s: Intercept processing of STATE control, manualStateBUSY = %s, manualStatePOSTPONED = %s\n", __FUNCTION__, manualStateBUSY?"TRUE":"FALSE", manualStatePOSTPONED?"Postponed":"Not Postponed");
			if (manualStatePOSTPONED && (appEvent->eventMask & TMS_EVENT_NOTIFY_INSTALL_DOWNLOAD))
			{
				LogMsg("%s: Calling tms_setApplicationState(%d, APP_STATUS_POSTPONE)\n", __FUNCTION__, appEvent->handle);
				ret_val = tms_setApplicationState(appEvent->handle, APP_STATUS_POSTPONE);
			} else if (manualStateBUSY)
			{
				LogMsg("%s: Calling tms_setApplicationState(%d, APP_STATUS_BUSY)\n", __FUNCTION__, appEvent->handle);
				ret_val = tms_setApplicationState(appEvent->handle, APP_STATUS_BUSY);
			} else {
				LogMsg("%s: Calling tms_setApplicationState(%d, APP_STATUS_FREE)\n", __FUNCTION__, appEvent->handle);
				ret_val = tms_setApplicationState(appEvent->handle, APP_STATUS_FREE);
			}
		} else {
			LogMsg("%s: Calling tms_setApplicationState(%d, APP_STATUS_BUSY)\n", __FUNCTION__, appEvent->handle);
			ret_val = tms_setApplicationState(appEvent->handle, APP_STATUS_BUSY);
			LogMsg("%s: tms_setApplicationState() returned %s - sleeping for 2 seconds\n", __FUNCTION__, status_to_string(ret_val));
			SVC_WAIT(2 * 1000);
			LogMsg("%s: Calling tms_setApplicationState(%d, APP_STATUS_FREE)\n", __FUNCTION__, appEvent->handle);
			ret_val = tms_setApplicationState(appEvent->handle, APP_STATUS_FREE);
		}
		memset(disp_str, 0, sizeof(disp_str));
		sprintf(disp_str, "tms_setApplicationState() returned %s", status_to_string(ret_val));
#if 0 //JB
		CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
#endif
		break;

	case TMS_EVT_GET_TMS_CONFIG_RESPONSE:
		memset(disp_str, 0, sizeof(disp_str));
		if (appEvent->status == TMS_STATUS_SUCCESS)
		{
			SVC_WAIT(2 * 1000);
			sprintf(disp_str, "Get Config Location SUCCESSFUL");
#if 0 //JB
			CreateText(app, &app ->appStatus, "Vera", 20, disp_str, ALIGN_CENTER, (PAD_SPACE + 55), 255, 192, 192);
#endif


			if (strlen(appEvent->filename))
			{
				sprintf(disp_str, "Config file located at %s", appEvent->filename);
			}
			else
			{
				sprintf(disp_str, "No Config File Location Returned");
			}

#if 0 //JB
			CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
#endif
		}
		else
		{
			sprintf(disp_str, "Get Config Location FAILED");
#if 0 //JB
			CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
#endif
		}

		LOG_PRINTF("%s: adding get config button back\n", __FUNCTION__);
#if 0 //JB
		if (app->getConfigButton)
		{
			evas_object_show(app->getConfigButton);
		}
#endif
		break;

	case TMS_EVT_SET_TMS_CONFIG_RESPONSE:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		/* Call service to set app state */

		sprintf(disp_str, "Config Update returned %s", status_to_string(appEvent->status));
#if 0 //JB
		CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
#endif

		LOG_PRINTF("%s: adding set config button back\n", __FUNCTION__);
#if 0 //JB
		if (app->getConfigButton)
		{
			evas_object_show(app->getConfigButton);
		}
#endif
		break;

	case TMS_EVT_SET_APP_INFO:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		/* Call service to set app state */

		memset(&appInfo, 0, sizeof(struct tmsAppInfo));

		createParamList(&appInfo, appEvent->eventMask);

		LogMsg("%s: Calling tms_setApplicationInfo(%d) - appInfo.parameterCount = %d, appInfo.parameterList = 0x%08x\n", __FUNCTION__, appEvent->handle,
			appInfo.parameterCount, appInfo.parameterList);
		for (param_count = 0; param_count < appInfo.parameterCount; param_count++)
		{
			LogMsg("\tParameter %d: name = %s, value = %s, type = %d\n", param_count, appInfo.parameterList[param_count].parameterName,
				appInfo.parameterList[param_count].parameterValue, appInfo.parameterList[param_count].parameterType);
		}

		ret_val = tms_setApplicationInfo(appEvent->handle, appInfo);
		free(appInfo.parameterList);

		memset(disp_str, 0, sizeof(disp_str));
		sprintf(disp_str, "tms_setApplicationInfo() returned %s", status_to_string(ret_val));
		LogMsg("%s: tms_setApplicationInfo() returned %s (%d)", __FUNCTION__, status_to_string(ret_val), ret_val);
#if 0 //JB
		CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
#endif
		break;

	case TMS_EVT_SET_PARM_LIST:

		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		/* Call service to set parameter list */
		fd = open(APP_PARM_DATA_FILE, O_RDWR | O_CREAT | O_TRUNC);

		if (fd != -1)
		{
			write(fd, APP_PARM_DATA_FILE_TEST_DATA, strlen(APP_PARM_DATA_FILE_TEST_DATA));
			close(fd);
		}

		LogMsg("%s: Calling tms_setApplicationParameterList(%d, 1, %s)\n", __FUNCTION__, appEvent->handle, APP_PARM_DATA_FILE);
		ret_val = tms_setApplicationParameterList(appEvent->handle, 1, APP_PARM_DATA_FILE);

		memset(disp_str, 0, sizeof(disp_str));
		sprintf(disp_str, "tms_setApplicationParameterList() returned %s", status_to_string(ret_val));
#if 0 //JB
		CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
#endif
		break;

	case TMS_EVT_GET_FILE:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		memset(param_file_copy_name, 0, sizeof(param_file_copy_name));
		/* Make sure filename is valid */
		if (strcmp(appEvent->filename, "test.ini") == 0)
		{
			int copy_result;
			int chmod_ret, chown_ret;
			char groupname[16];
			struct group *psGroup;
			struct passwd *psPasswd;


#if 0 //JB
			psPasswd = getpwuid(getuid());
#endif
			sprintf(param_file_copy_name, "I:/test.ini");
			LogMsg("%s: Copying %s to %s\n", __FUNCTION__, appEvent->filename, param_file_copy_name);
			copy_result = CopyFile("test.ini", param_file_copy_name);
			LogMsg("%s: Copy result = %d\n", __FUNCTION__, copy_result);

#if 0 //JB
			sprintf(groupname, "%ssys", psPasswd->pw_name);
			psGroup = getgrnam(groupname);

			if (psGroup)
			{
				chmod_ret = chmod(param_file_copy_name, 0640 );
				chown_ret = chown(param_file_copy_name, getuid(), psGroup->gr_gid);
				LogMsg("%s: chmod to 640 returned (%d) - chown to %s group returned %d\n", __FUNCTION__, chmod_ret, groupname, chown_ret);
			}
			else
				chmod_ret = chmod(param_file_copy_name, 0644);
#endif
			if (copy_result == 0)
				result = TMS_STATUS_SUCCESS;
			else
			{
				sprintf(disp_str, "Error %d copying test.ini to %s", copy_result, param_file_copy_name);
				result = TMS_STATUS_ERROR;
			}
		}
		else
		{
			sprintf(disp_str, "Invalid Parameter File to Get-%s", appEvent->filename);
			LogMsg("%s: Invalid parameter filename - %s\n", __FUNCTION__, appEvent->filename);
			result = TMS_STATUS_FILENAME_ERROR;
		}


		LogMsg("%s: Calling tms_getApplicationFileAvailable(%d, %s, %s, TRUE)\n", __FUNCTION__, appEvent->handle, status_to_string(result), param_file_copy_name);
		ret_val = tms_getApplicationFileAvailable(appEvent->handle, result, param_file_copy_name, TRUE);

		memset(disp_str, 0, sizeof(disp_str));
		sprintf(disp_str, "tms_getApplicationFileAvailable() returned %s", status_to_string(ret_val));

#if 0 //JB
		CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
#endif
		break;

	case TMS_EVT_PUT_FILE:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));

		/* Process received file */
		if (manualStateControl)
		{
			LogMsg("%s: Intercept processing of STATE control, manualStateBUSY = %s, manualStatePOSTPONED = %s\n", __FUNCTION__, manualStateBUSY?"TRUE":"FALSE", manualStatePOSTPONED?"Postponed":"Not Postponed");
			if (manualStatePOSTPONED)
			{
				result = TMS_STATUS_POSTPONED;
				LogMsg("%s: Calling tms_setFileOperationResult(%d, %s, TMS_EVT_PUT_FILE)\n", __FUNCTION__, appEvent->handle, status_to_string(result));
				ret_val = tms_setFileOperationResultWithDescription(appEvent->handle, result, TMS_EVT_PUT_FILE, NULL);

				memset(disp_str, 0, sizeof(disp_str));
				sprintf(disp_str, "tms_setFileOperationResult() returned %s", status_to_string(ret_val));
				break;
			}
		}

		memset(param_file_copy_name, 0, sizeof(param_file_copy_name));
		strcpy(param_file_copy_name, appEvent->filename);

		pszBaseName = param_file_copy_name;

		/* Make sure filename is valid */
		if (strcmp(pszBaseName, "I:/test.ini") == 0)
		{
			int copy_result;

			LogMsg("%s: Copying %s to test.ini\n", __FUNCTION__, appEvent->filename);
			copy_result = CopyFile(appEvent->filename, "test.ini");
			LogMsg("%s: Copy result = %d\n", __FUNCTION__, copy_result);

			if (copy_result == 0)
				result = TMS_STATUS_SUCCESS;
			else
				result = TMS_STATUS_ERROR;

			LogMsg("%s: Calling tms_setFileOperationResult(%d, %s, TMS_EVT_PUT_FILE)\n", __FUNCTION__, appEvent->handle, status_to_string(result));
			ret_val = tms_setFileOperationResultWithDescription(appEvent->handle, result, TMS_EVT_PUT_FILE, NULL);

			memset(disp_str, 0, sizeof(disp_str));
			sprintf(disp_str, "tms_setFileOperationResult() returned %s", status_to_string(ret_val));
		}
		else
		{
			result = TMS_STATUS_ERROR;
#if 0 //MM
			if (app->appStatus2)
			{
				evas_object_hide(app->appStatus2);
				evas_object_del(app->appStatus2);
				app->appStatus2 = NULL;
			}
			evas_object_text_text_set(app->appStatus, "Received Content Update - Accept or Reject");

			LogMsg("svc_tms_tester: Got a content event\n\tStatus = %s\n\tType = %d\n\tContent ID = %d\n\tFilename = %s\n\tfilepath = %s\n",
				status_to_string(appEvent->status), appEvent->putFileType, appEvent->handle, appEvent->filename, appEvent->filepath);

			CreateButton(app, &app->acceptButton, "Accept Content", (PAD_SPACE * 2), 185, 140, 40, Clicked_AcceptButtonCB);
			CreateButton(app, &app->rejectButton, "Reject Content", 2 * (PAD_SPACE * 2) + 140, 185, 140, 40, Clicked_RejectButtonCB);

			evas_render(app->fp->e);

			waiting_for_usr_response = TRUE;

			while (waiting_for_usr_response)
			{
				printf("%s: Waiting for user response\n", __FUNCTION__);
				SVC_WAIT(3 * 1000);
			}


#if (!DELETE_BUTTONS_IN_CB)
			printf("%s: Got user response - deleting buttons\n", __FUNCTION__);

			/* We can remove the accept button */
			if (app->acceptButton)
			{
				evas_object_hide(app->acceptButton);
				evas_object_del(app->acceptButton);
				app->acceptButton = NULL;
			}

			/* We can remove the reject button */
			if (app->rejectButton)
			{
				evas_object_hide(app->rejectButton);
				evas_object_del(app->rejectButton);
				app->rejectButton = NULL;
			}
#else
			printf("%s: Got user response\n", __FUNCTION__);
#endif
#endif
		}
		break;

	case TMS_EVT_DEL_FILE:
		LogMsg("%s: Handling %s - Delete Filename = %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType), appEvent->filename);
		/* Call service and just respond with a SUCCESS message */

		LogMsg("%s: Calling tms_setFileOperationResult(%d, TMS_STATUS_ERROR, TMS_EVT_DEL_FILE) - svc_tms_tester does not delete files\n", __FUNCTION__, appEvent->handle);
		ret_val = tms_setFileOperationResultWithDescription(appEvent->handle, TMS_STATUS_ERROR, TMS_EVT_DEL_FILE, "App DEL not supported");

		memset(disp_str, 0, sizeof(disp_str));
		sprintf(disp_str, "tms_setFileOperationResult() returned %s", status_to_string(ret_val));
#if 0 //JB
		CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
#endif
		break;

	case TMS_EVT_NOTIFICATION:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));

		/* Just display a message with the notification event */
		memset(disp_str, 0, sizeof(disp_str));
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_REBOOT_DEVICE)
		{
			strcat(disp_str, "Device Rebooting ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_RESTART_APPS)
		{
			struct tmsReturn tms_ret;
			strcat(disp_str, "Restarting Apps ");

			LogMsg("svc_tms_tester: Un-Registering App %s\n", "svc_tms_tester");
			tms_ret = tms_unregisterApplication("tms_tester12");
			if (tms_ret.status == TMS_STATUS_REQUESTED)
				LogMsg("svc_tms_tester: Un-Register requested\n");


		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_DOWNLOAD_STARTED)
		{
			strcat(disp_str, "Download Started-file=");
			strcat(disp_str, appEvent->filename);
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_DOWNLOAD_PERCENT)
		{
			char percent_str[16];

			memset(percent_str, 0, sizeof(percent_str));
			sprintf(percent_str, "%d", appEvent->notificationData.downloadPercent);

			strcat(disp_str, "Download Percent Update-file=");
			strcat(disp_str, appEvent->filename);
			strcat(disp_str, "at ");
			strcat(disp_str, percent_str);
			strcat(disp_str, " percent ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_DOWNLOAD_COMPLETE)
		{
			strcat(disp_str, "Download Complete-file=");
			strcat(disp_str, appEvent->filename);
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_INSTALL_DOWNLOAD)
		{
			strcat(disp_str, "Installing downloaded file - ");
			strcat(disp_str, appEvent->filename);
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_MAINTENANCE_END)
		{
			strcat(disp_str, "Maintenance Completed");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_HEARTBEAT_RESULT)
		{
			strcat(disp_str, "Heartbeat Completed - ");
			if (appEvent->status == TMS_STATUS_SUCCESS)
				strcat(disp_str, "SUCCESSFUL");
			else
				strcat(disp_str, "FAILURE");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_CONTACT_SERVER_START)
		{
			strcat(disp_str, "Contacting Server");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_CONTACT_SERVER_END)
		{
			strcat(disp_str, "Server Contact Complete");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_CONTACT_SERVER_APPROVAL)
		{
			/* Should never get this notification.  It is a duplicate of CONTACT_SERVER_START, but when registering
			for this notifcation, it means the app wants to approve server contact */
			strcat(disp_str, "Contact Server Approval");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_CONTENT_UPDATES)
		{
			strcat(disp_str, "Content Update");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_CLOCK_UPDATE)
		{
			strcat(disp_str, "Clock Updated ");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_SERVER_ERRORS)
		{
			char temp_str[128];

			LogMsg("%s: Server Error Notification - Error = %s (%d)\n", __FUNCTION__, appEvent->filename, appEvent->status);
			sprintf(temp_str, "Server Error %d", appEvent->status);
			strcat(disp_str, temp_str);
			strcat(disp_str, " ");
		}
#if 0 //JB
		CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
#endif
		break;

	case TMS_EVT_DO_TRANSACTION:
		LogMsg("%s: Handling %s - transaction = %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType), appEvent->transactionType);
		/* Call service and just respond with a SUCCESS message */

		LogMsg("%s: Calling tms_setTransactionResult(%d, TMS_STATUS_SUCCESS)\n", __FUNCTION__, appEvent->handle);
		ret_val = tms_setTransactionResult(appEvent->handle, TMS_STATUS_SUCCESS);

		memset(disp_str, 0, sizeof(disp_str));
		sprintf(disp_str, "tms_setTransactionResult() returned %s", status_to_string(ret_val));
#if 0 //JB
		CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
#endif
		break;

	case TMS_EVT_REGISTER_APP_RESPONSE:
		memset(disp_str, 0, sizeof(disp_str));
		if (appEvent->status == TMS_STATUS_SUCCESS)
		{
			sprintf(disp_str, "Registration SUCCESSFUL");
#if 0 //JB
			CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
#endif
			//tms_setApplicationState(-1, APP_STATUS_BUSY);
		}
		else
		{
			sprintf(disp_str, "Registration FAILED");
#if 0 //JB
			CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
#endif
		}
		break;

	case TMS_EVT_UNREGISTER_APP_RESPONSE:
		memset(disp_str, 0, sizeof(disp_str));
		if (appEvent->status == TMS_STATUS_SUCCESS)
		{
			sprintf(disp_str, "Un-Registration SUCCESSFUL");
#if 0 //JB
			CreateText(app, &app->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
#endif
		}
		else
		{
			sprintf(disp_str, "Un-Registration FAILED");
#if 0 //JB
			CreateText(app, &app->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
#endif
		}

	case TMS_EVT_GET_SERVER_INSTANCE:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		if (appEvent->status == TMS_STATUS_SUCCESS)
			sprintf(disp_str, "GetServerInstance returned %s SUCCESSFULLY", appEvent->filename);
		else
			sprintf(disp_str, "GetServerInstance returned ERROR %s", status_to_string(appEvent->status));

		break;

	case TMS_EVT_LOCK_SERVER_INSTANCE:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		if (appEvent->status == TMS_STATUS_SUCCESS)
		{
			if (backup_requested)
			{
				backup_requested = FALSE;
				backup_active = TRUE;
				strcpy(prevInstance1, appEvent->filename);
				sprintf(disp_str, "LockServerInstance 'backup' SUCCESSFUL - Old Instance = %s", prevInstance1);
				LogMsg("LockServerInstance 'backup' SUCCESSFUL - Old Instance = %s\n", prevInstance1);
			}
			else
			{
				third_active = TRUE;
				strcpy(prevInstance2, appEvent->filename);
				sprintf(disp_str, "LockServerInstance 'third' SUCCESSFUL - Old Instance = %s", prevInstance2);
				LogMsg("LockServerInstance 'third' SUCCESSFUL - Old Instance = %s\n", prevInstance2);
			}
		}
		else
		{
			sprintf(disp_str, "LockServerInstance returned ERROR %s", status_to_string(appEvent->status));
			LogMsg("LockServerInstance returned ERROR %s\n", status_to_string(appEvent->status));
		}

		break;

	case TMS_EVT_RELEASE_SERVER_INSTANCE:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		if (appEvent->status == TMS_STATUS_SUCCESS)
		{
			if (backup_active)
			{
				backup_active = FALSE;
				sprintf(disp_str, "ReleaseServerInstance 'backup' SUCCESSFUL");
				LogMsg("ReleaseServerInstance 'backup' SUCCESSFUL - %s restored\n", prevInstance1);
			}
			else if (third_active)
			{
				third_active = FALSE;
				sprintf(disp_str, "ReleaseServerInstance 'third' SUCCESSFUL");
				LogMsg("ReleaseServerInstance 'third' SUCCESSFUL - %s restored\n", prevInstance2);
			}
		}
		else
		{
			sprintf(disp_str, "ReleaseServerInstance returned ERROR %s", status_to_string(appEvent->status));
			LogMsg("ReleaseServerInstance returned ERROR %s\n", status_to_string(appEvent->status));
		}

		break;

	case TMS_EVT_APP_ALERT_RESULT:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		if (appEvent->status == TMS_STATUS_SUCCESS)
			sprintf(disp_str, "App Alert sent SUCCESSFULLY");
		else
			sprintf(disp_str, "App Alert ERROR %s", status_to_string(appEvent->status));

		LogMsg("%s\n", disp_str);
		break;

	case TMS_EVT_CLEAR_APP_INFO_RESULT:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		if (appEvent->status == TMS_STATUS_SUCCESS)
			sprintf(disp_str, "Clear App Info SUCCESSFUL");
		else
			sprintf(disp_str, "Clear App Info ERROR %s", status_to_string(appEvent->status));

		LogMsg("%s\n", disp_str);
		break;

	case TMS_EVT_API_ERRORS:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		/*
		 * This is critical ERROR, VHQ Agent can't trust data exchange with application start from this moment.
		 * Verify your code and provide required changes!
		 *
		 * Do not call any TMS APIs here. This restriction is related to Verix OS only.
		 * HandleApplicationEvent() is a separate thread. You can set flag here and process it later by
		 * tms_sendApplicationAlert(), for example, in the same thread where tms_registerApplication2() was called.
		 *
		 * Any call of single-thread compatible calls, like next:
		 * - tms_setTransactionResult()
		 * - tms_setApplicationState(with handle)
		 * - tms_setApplicationInfo()
		 * - tms_setApplicationParameterList()
		 * - tms_getApplicationFileAvailable()
		 * - tms_setFileOperationResult()
		 * - tms_setFileOperationResultWithDescription()
		 * - tms_setTransactionResult()
		 * will cause closed loop behavior, closed loop up to timeout on VHQ Agent side. Don't use them here.
		 */
		if (appEvent->status == TMS_STATUS_API_SYNC_ERROR)
			sprintf(disp_str, "API processing error %s: %s", status_to_string(appEvent->status), appEvent->filename);
		else
			sprintf(disp_str, "API processing error: %s (%d)", status_to_string(appEvent->status), appEvent->status);
	}

	clrscr();
	write_at(disp_str, strlen(disp_str), 1, 1);
	
	LogMsg("%s: returning from handler\n", __FUNCTION__);

	return 0;
}



static int HandleApplicationEvent_New(struct tmsEventData* eventData, void* data)
{
	int ret_val;
	tmsEventCallback cb_func = (tmsEventCallback)data;

	printf("HandleApplicationEvent_New: +\n");
	LogMsg("%s: Callback data cb_func = 0x%08x\n", __FUNCTION__, (int)cb_func);
	ret_val = cb_func(*eventData);

	printf("HandleApplicationEvent_New: returning %d\n", ret_val);
	return ret_val;
}

static void processManualStateControl(int handle_console)
{
	int read_val;
	char key;
	char line[64];

	clrscr();

	write_at("Change APP state:", 17, 1, 1);
	write_at("1 - BUSY", 8, 2, 2);
	write_at("2 - FREE", 8, 2, 3);
	write_at("3 - POSTPONED", 13, 2, 4);
	write_at("4 - NOT POSTPONED", 17, 2, 5);

	while (1) {
		snprintf(line, sizeof(line), "Status: %s, %s", manualStatePOSTPONED?"PS":"nP", manualStateBUSY?"BUSY":"FREE");
		write_at(line, strlen(line), 1, 6);

		wait_evt(EVT_KBD);
		read_val = read(handle_console, &key, 1);
		if(read_val > 0)
		{
			LOG_PRINTF("svc_tms_tester: Key pressed = %d\n", key);

			switch(key)
			{
			case 0xB1:
				manualStateBUSY = TRUE;
				break;
			case 0xB2:
				manualStateBUSY = FALSE;
				break;
			case 0xB3:
				manualStatePOSTPONED = TRUE;
				break;
			case 0xB4:
				manualStatePOSTPONED = FALSE;
				break;
			case 0x9B:
				return;
			}
		}
	}
}

#define SYSLOG_OUTPUT_FILE		"syslog.log"

#if 0 //JB
// Makes interface from WDE PHP simple.
// Note this loop/thread will do the app integration to TMS
void* MainLoopApp(void* pvData)
{
#else
int main(int argc, char **argv)
{
#endif
	struct tmsReturn regRtn;
	struct passwd *psPasswd;
	int handle_console = 0;
	char key;
	int tid;
	int read_val;
	char serial_num[32];
	char line2[256];

	handle_console = open(DEV_CONSOLE, O_RDWR);
	SVC_WAIT(30000);
	LOGAPI_INIT("TMSTEST");
	LOG_INIT("TMSTEST", LOGSYS_OS, 0x00000000);

#if 0 //JB
	psPasswd = getpwuid(getuid());
#endif

	memset(serial_num, 0, sizeof(serial_num));
	GetSerialNumber(serial_num);
	if (strlen(serial_num))
	{
		strcpy(custom_parameters[0].parameterValue, serial_num);
		printf("svc_tms_tester: Set %s to %s\n", custom_parameters[0].parameterName, custom_parameters[0].parameterValue);
	}

	while (1)
	{
		LogMsg("svc_tms_tester: TMS App Ifc Version (tms_GetVersion) = %s\n", tms_GetVersion());

#if 0 //JB
		LogMsg("svc_tms_tester: Registering App %s as %s\n", "svc_tms_tester", psPasswd->pw_name);
#endif

		//regRtn = tms_registerApplication("tms_tester12", 0xffffffff, (unsigned int)HandleApplicationEvent);
		regRtn = tms_registerApplication2("tms_tester12", 0xffffffff, HandleApplicationEvent_New, HandleApplicationEvent);
		if (regRtn.status == TMS_STATUS_REQUESTED)
		{
			LogMsg("svc_tms_tester: Register App REQUESTED\n");

			LogMsg("svc_tms_tester: TMS Agent Version (tms_GetAgentVersion) = %s\n", tms_GetAgentVersion());
			if (errno != 0)
			{
				LogMsg("svc_tms_tester ERROR: tms_GetAgentVersion() returned errno %d\n", errno);
			}

#if 0 //JB
			if (app->appStatus2)
			{
				evas_object_hide(app->appStatus2);
				evas_object_del(app->appStatus2);
				app->appStatus2 = NULL;
			}

			evas_object_text_text_set(app ->appStatus, "Waiting For APP Event");
			evas_render(app->fp->e);
#endif
#if 0
			/* Sleep for 5 minutes and then say we are free */
			LogMsg("svc_tms_tester: sleeping 5 minutes\n");
			SVC_WAIT(60 * 1000);
			SVC_WAIT(60 * 1000);
			SVC_WAIT(60 * 1000);
			SVC_WAIT(60 * 1000);
			SVC_WAIT(60 * 1000);
			LogMsg("svc_tms_tester: setting app state to FREE\n");
			tms_setApplicationState(-1, APP_STATUS_FREE);
#endif

			/*while (handle_console <= 0)
			{
				//handle_console = open(DEV_CONSOLE, O_RDWR);
				handle_console = get_console(read_val);
				LOG_PRINTF("svc_tms_tester: Open Console %d\n", handle_console);
			}*/
			memset(line2, 0, sizeof(line2));

			while(1)
			{

				LogMsg("svc_tms_tester: Wait key press\n");
				
				//SVC_WAIT(1000);
				//set_timer(1000, EVT_TIMER);
				clrscr();
				write_at("Wait Key Press", 14, 1, 1);
				if (strlen(line2))
					write_at(line2, strlen(line2), 2, 2);
				wait_evt(EVT_KBD);
				read_val = read(handle_console, &key, 1);
				clrscr();
				if(read_val > 0)
				{
					LOG_PRINTF("svc_tms_tester: Key pressed = %d\n", key);
					memset(line2, 0, sizeof(line2));
					if (key == 0xB1) //Key 1
					{
						struct tmsReturn tms_ret;

						/* Sleep for 5 minutes and then call the server */
						//LogMsg("svc_tms_tester: sleeping 5 minutes\n");
						//SVC_WAIT(60 * 5 * 1000);
						LogMsg("svc_tms_tester: calling server\n");
						tms_ret = tms_callServer2(0, TMS_API_CALL_SERVER_FORCE_MAINTENANCE_FLAG);
						if (tms_ret.status == TMS_STATUS_REQUESTED)
							LogMsg("svc_tms_tester: server call requested\n");
						else
							LogMsg("svc_tms_tester: server call request FAILED (tms_ret.status = %d)!!!!\n", errno, strerror(errno));
					}
					else if (key == 0xB2) //Key 2
					{
						struct tmsReturn tms_ret;

						//LogMsg("svc_tms_tester: sleeping 5 minutes\n");
						//SVC_WAIT(60 * 5 * 1000);
						LogMsg("svc_tms_tester: Un-Registering App %s\n", "tms_tester12");
						tms_ret = tms_unregisterApplication("tms_tester12");
						if (tms_ret.status == TMS_STATUS_REQUESTED)
							LogMsg("svc_tms_tester: Un-Register requested\n");
					}
					else if (key == 0xB3) //Key 3
					{
						struct tmsReturn tms_ret;

						tms_ret = tms_getConfigLocation();

						LogMsg("%s: tms_getConfigLocation result = %s\n", __FUNCTION__, status_to_string(tms_ret.status));
						if (tms_ret.status == TMS_STATUS_REQUESTED)
						{
							  LogMsg("%s: Get Config Location REQUESTED", __FUNCTION__);
						}
						else
						{
							  LogMsg("%s: Get Config Location request FAILED", __FUNCTION__);
						}
					}
					else if (key == 0xB4) //Key 4
					{
						struct tmsReturn tms_ret;

						tms_ret = tms_setNewConfigAvailable("I:/tms_config.ini");

						LogMsg("%s: tms_setNewConfigAvailable result = %s\n", __FUNCTION__, status_to_string(tms_ret.status));
						if (tms_ret.status == TMS_STATUS_REQUESTED)
						{
							LogMsg("%s: Set Config REQUESTED", __FUNCTION__);
						}
						else
						{
							LogMsg("%s: Set Config request FAILED", __FUNCTION__);
						}
					}
					else if (key == 0xB5) //Key 5
					{
						int file_num = 0;
						char log_file[256];

						LOGAPI_DUMP_SYS_INFO();
						syslog(LOG_CRIT, "Testing syslog\n");
						LOG_PRINTF("svc_tms_tester: running in group %d\n", get_group());

						for (file_num = 0; file_num < 10; file_num++)
						{
							sprintf(log_file, "%s%d", SYSLOG_OUTPUT_FILE, file_num);
							if (FileExists(log_file))
							{
								int md_handle;

								sprintf(line2, "%s found", log_file);
								syslog(LOG_CRIT, "%s exists - printing contents\n", log_file);
								LOG_PRINTF("%s exists - printing contents\n", log_file);
								md_handle = open(log_file, O_RDONLY);
								if (md_handle != -1)
								{
									int bytes_read = 0;
									char md_contents[95];

									LOG_PRINTF("%s opened\n", log_file);
									LOG_PRINTF("\n\n\n\n\n%s contents:\n[", log_file);

									do
									{
										memset(md_contents, 0, sizeof(md_contents));
										bytes_read = read(md_handle, md_contents, sizeof(md_contents) - 1);
										LOG_PRINTF("%s", md_contents);
									} while (bytes_read > 0);

									close(md_handle);
								}
							}
							else
							{
								sprintf(line2, "%s DOES NOT EXIST", log_file);
							}
							clrscr();
							write_at(line2, strlen(line2), 2, 2);
							SVC_WAIT(1000);
						}
					}
					else if (key == 0xB6) //Key 6
					{
						char disp_str1[256];
						struct tmsReturn tms_ret;

						if (backup_active)
						{
							backup_requested = FALSE;
							LogMsg("Restoring previous instance\n");
							SVC_WAIT(1000);

							LogMsg("%s: Restoring server instance %s\n", __FUNCTION__, prevInstance1);
							tms_ret = tms_releaseServerLock(prevInstance1);


							memset(disp_str1, 0, sizeof(disp_str1));
							LogMsg("%s: tms_releaseServerLock result = %s\n", __FUNCTION__, status_to_string(tms_ret.status));
							if (tms_ret.status == TMS_STATUS_REQUESTED)
							{
								sprintf(disp_str1, "Release Server Lock 'backup' REQUESTED");
							}
							else
							{
								sprintf(disp_str1, "FAILURE to send release server lock request to agent");
							}
						}
						else
						{
							LogMsg("Setting server instance to backup\n");
							SVC_WAIT(1000);

							LogMsg("%s: Setting backup server instance\n", __FUNCTION__);
							tms_ret = tms_lockServerInstance("backup", 0);


							memset(disp_str1, 0, sizeof(disp_str1));
							LogMsg("%s: tms_lockServerInstance result = %s\n", __FUNCTION__, status_to_string(tms_ret.status));
							if (tms_ret.status == TMS_STATUS_REQUESTED)
							{
								backup_requested = TRUE;
								sprintf(disp_str1, "Server Lock 'backup' REQUESTED");
							}
							else
							{
								sprintf(disp_str1, "FAILURE to send server lock request to agent");
							}
						}

						clrscr();
						write_at(disp_str1, strlen(disp_str1), 2, 2);
						SVC_WAIT(1000);

					}
					else if (key == 0xB7) //Key 7
					{
						char disp_str1[256];
						struct tmsReturn tms_ret;

						if (third_active)
						{
							backup_requested = FALSE;
							LogMsg("Restoring previous instance\n");
							SVC_WAIT(1000);

							LogMsg("%s: Restoring server instance %s\n", __FUNCTION__, prevInstance2);
							tms_ret = tms_releaseServerLock(prevInstance2);


							memset(disp_str1, 0, sizeof(disp_str1));
							LogMsg("%s: tms_releaseServerLock result = %s\n", __FUNCTION__, status_to_string(tms_ret.status));
							if (tms_ret.status == TMS_STATUS_REQUESTED)
							{
								sprintf(disp_str1, "Release Server Lock 'third' REQUESTED");
							}
							else
							{
								sprintf(disp_str1, "FAILURE to send release server lock request to agent");
							}
						}
						else
						{
							LogMsg("Setting server instance to third\n");
							SVC_WAIT(1000);

							LogMsg("%s: Setting third server instance\n", __FUNCTION__);
							tms_ret = tms_lockServerInstance("third", 60000);


							memset(disp_str1, 0, sizeof(disp_str1));
							LogMsg("%s: tms_lockServerInstance result = %s\n", __FUNCTION__, status_to_string(tms_ret.status));
							if (tms_ret.status == TMS_STATUS_REQUESTED)
							{
								backup_requested = FALSE;
								sprintf(disp_str1, "Server Lock 'third' REQUESTED");
							}
							else
							{
								sprintf(disp_str1, "FAILURE to send server lock request to agent");
							}
						}

						clrscr();
						write_at(disp_str1, strlen(disp_str1), 2, 2);
						SVC_WAIT(1000);
					}
					else if (key == 0xB8) //Key 8
					{
						struct tmsReturn tms_ret;

						tms_ret = tms_sendApplicationAlert("App Test Event", TMS_ALERT_SEVERITY_MEDIUM, "The details of the VX test App alert could include a lot of information like statistics, etc.");

						LogMsg("%s: tms_sendApplicationAlert result = %s\n", __FUNCTION__, status_to_string(tms_ret.status));
						if (tms_ret.status == TMS_STATUS_REQUESTED)
						{
							LogMsg("%s: App Alert REQUESTED", __FUNCTION__);
						}
						else
						{
							LogMsg("%s: App Alert request FAILED", __FUNCTION__);
						}
					}
					else if (key == 0xB9) //Key 9
					{
						LogMsg("%s: Manual state control started", __FUNCTION__);
						manualStateControl = TRUE;
						processManualStateControl(handle_console);
						LogMsg("%s: Manual state control finished", __FUNCTION__);
						manualStateControl = FALSE;
					}
				}
				else
					LOG_PRINTF("svc_tms_tester: EVT_TIMER %d, %d\n", read_val, errno);
			}
			LOG_PRINTF("!!!svc_tms_tester: END OF WHILE \n");
#if 0
#if 1
			{
				struct tmsReturn tms_ret;

				/* Sleep for 5 minutes and then call the server */
				LogMsg("svc_tms_tester: sleeping 5 minutes\n");
				SVC_WAIT(60 * 1000);
				SVC_WAIT(60 * 1000);
				SVC_WAIT(60 * 1000);
				SVC_WAIT(60 * 1000);
				SVC_WAIT(60 * 1000);
				LogMsg("svc_tms_tester: calling server\n");
				tms_ret = tms_callServer2(0, TMS_API_CALL_SERVER_FORCE_MAINTENANCE_FLAG);
				if (tms_ret.status == TMS_STATUS_REQUESTED)
					LogMsg("svc_tms_tester: server call requested\n");
				else
					LogMsg("svc_tms_tester: server call request FAILED (tms_ret.status = %d)!!!!\n", tms_ret.status);
			}
#endif
#if 1
			{
				struct tmsReturn tms_ret;

				LogMsg("svc_tms_tester: sleeping 3 minutes\n");
				SVC_WAIT(60 * 1000);
				SVC_WAIT(60 * 1000);
				SVC_WAIT(60 * 1000);
				LogMsg("svc_tms_tester: Getting Config Location\n");
				tms_ret = tms_getConfigLocation();
				if (tms_ret.status == TMS_STATUS_REQUESTED)
					LogMsg("svc_tms_tester: GetConfig requested\n");
				else
					LogMsg("svc_tms_tester: GetConfig request FAILED (tms_ret.status = %d)!!!!\n", tms_ret.status);
			}
#endif
#if 1
			{
				struct tmsReturn tms_ret;

				LogMsg("svc_tms_tester: sleeping 1 minute\n");
				SVC_WAIT(60 * 1000);
				LogMsg("svc_tms_tester: Un-Registering App %s\n", "tms_tester12");
				tms_ret = tms_unregisterApplication("tms_tester12");
				if (tms_ret.status == TMS_STATUS_REQUESTED)
					LogMsg("svc_tms_tester: Un-Register requested\n");
				else
					LogMsg("svc_tms_tester: Un-Register request FAILED (tms_ret.status = %d)!!!!\n", tms_ret.status);
			}
#endif

			while (1)
			{
				int loop_count;
				LogMsg("svc_tms_tester: still running - sleeping for an hour\n", errno, strerror(errno));
				for (loop_count = 0; loop_count < 60; loop_count++)
					SVC_WAIT(60 * 1000);
			}
#endif
		}
		else
		{
			LogMsg("svc_tms_tester ERROR: tms_registerApplication() returned an error - regRtn->status = %s (%d), errno = %d\n\tRetrying in 30 seconds",
				status_to_string(regRtn.status), regRtn.status, errno);
			SVC_WAIT(30 * 1000);
		}


	}

	return 0;

}















/*******************************************************************
* Used for the GUI version
*******************************************************************/




#ifndef DATAPATH
#define DATAPATH	"./flash"
#endif

#ifndef THEMES_DATAPATH
#define THEMES_DATAPATH	 DATAPATH "/themes"
#endif



#if 0 //JB
static Evas_Object *textbx;
#endif


#if 0 //JB
// Main function
int fp_main(int argc, char **argv)
{
	int MainloopAppRetVal;
	pthread_t MainLoopAppThread;
	struct appdata app_obj;

	memset(&app_obj, 0, sizeof(struct appdata));
	app = &app_obj;

	app ->fp = fp_init(&argc, argv);

	if (!app ->fp) return -1;
	if (fp_window_init(app ->fp, DATAPATH) < 0) return -1;

	ecore_evas_title_set(app ->fp ->ee, "Content Test App");
	printf("%s: calling evas_font_path_append(%s)\n", __FUNCTION__, THEMES_DATAPATH);
	evas_font_path_append(app ->fp ->e, THEMES_DATAPATH);

	// Create Background Gradient
	printf("%s: calling CreateGradient\n", __FUNCTION__);
	CreateGradient(app);
	printf("%s: calling CreateControls\n", __FUNCTION__);
	CreateControls(app);


	printf("%s: Starting MainLoopApp on a separate thread\n", __FUNCTION__);
	MainloopAppRetVal = pthread_create(&MainLoopAppThread, NULL, MainLoopApp, NULL);
	if (MainloopAppRetVal != 0)
	{
		printf("%s ERROR: Could not start MainLoopApp thread - EXITING\n", __FUNCTION__);
		exit(-1);
	}
	else
		SVC_WAIT(1 * 1000);


	// Begin Main Loop
	fp_begin(app ->fp);

	printf("svc_tms_tester: reached end of fp_main - returning -1\n");
	return -1;
}

void CreateGradient(struct appdata *app)
{
	// Create Background Gradient
	app ->bg = evas_object_gradient_add(app ->fp ->e);

	evas_object_gradient_color_add(app ->bg, 0, 0, 255, 255, 1);
	evas_object_gradient_color_add(app ->bg, 0, 0, 128, 255, 2);
	evas_object_gradient_color_add(app ->bg, 0, 0, 0, 255, 3);
	evas_object_gradient_color_add(app ->bg, 0, 0, 128, 255, 4);
	evas_object_gradient_angle_set(app ->bg, 45.0);
	evas_object_resize(app ->bg, app ->fp ->w, app ->fp ->h);
	evas_object_show(app ->bg);
}
void CreateControls(struct appdata *app)
{
	//printf("%s: +\n", __FUNCTION__);
	// Title
	//printf("%s: creating title text\n", __FUNCTION__);
	CreateText(app, &app ->titleText, "Vera", 12, "MX9 TMS Interface Test App", ALIGN_CENTER, PAD_SPACE, 255, 255, 255);

	// App Status
	//printf("%s: creating labeltext1\n", __FUNCTION__);
	CreateText(app, &app ->labelText1, "Vera", 11, "App Status", ALIGN_CENTER, PAD_SPACE + 20, 224, 224, 224);
	//printf("%s: creating textrect\n", __FUNCTION__);
	CreateRectangle(app, &app ->textRect, PAD_SPACE + 30, (app ->fp ->h / 2) + 10 + PAD_SPACE, 192, 192, 192, 64);

	// App Status
	//printf("%s: creating appstatus\n", __FUNCTION__);
	CreateText(app, &app ->appStatus, "Vera", 25, "Waiting for event", ALIGN_CENTER, (PAD_SPACE + 55), 255, 192, 192);

	//printf("%s: creating horizline1\n", __FUNCTION__);
	CreateLine(app, &app ->horizLine1, PAD_SPACE, (PAD_SPACE + 180), (app ->fp ->w - PAD_SPACE), (PAD_SPACE + 180), 128, 128, 128);
	//printf("%s: creating horizline2\n", __FUNCTION__);
	CreateLine(app, &app ->horizLine2, PAD_SPACE, (PAD_SPACE + 223), (app ->fp ->w - PAD_SPACE), (PAD_SPACE + 223), 128, 128, 128);
	//printf("%s: creating vertline\n", __FUNCTION__);
	CreateLine(app, &app ->vertLine, 205, (PAD_SPACE + 180), 205, (PAD_SPACE + 223), 128, 128, 128);
	//CreateButton(app, &app ->acceptButton, "Accept Content", (PAD_SPACE * 2), 185, 140, 40, Clicked_AcceptButtonCB);
	//CreateButton(app, &app ->rejectButton, "Reject Content", 2 * (PAD_SPACE * 2) + 140, 185, 140, 40, Clicked_RejectButtonCB);
	//printf("%s: creating view log button\n", __FUNCTION__);
	CreateButton(app, &app ->viewlogButton, "View Log", app ->fp ->w - 140 - (PAD_SPACE * 4), 185, 140, 40, Clicked_ViewLogButtonCB);
	//printf("%s: creating contact server button\n", __FUNCTION__);
	CreateButton(app, &app ->contactServerButton, "Contact Server", app ->fp ->w - 140 - (PAD_SPACE * 4), 140, 140, 40, Clicked_ContactServerButtonCB);
	//printf("%s: creating get config button\n", __FUNCTION__);
#if CONFIG_BUTTON_IS_GET_CONFIG
	CreateButton(app, &app ->getConfigButton, "Get Config", app ->fp ->w - 280 - (PAD_SPACE * 5), 140, 140, 40, Clicked_GetConfigButtonCB);
#else
	CreateButton(app, &app ->getConfigButton, "Set Config", app ->fp ->w - 280 - (PAD_SPACE * 5), 140, 140, 40, Clicked_GetConfigButtonCB);
#endif
	//printf("%s: +\n", __FUNCTION__);
}

void CreateText(struct appdata *app, Evas_Object **pObj, const char *pFontFace, int iSize, const char *pText, int iX, int iY, int iRed, int iGreen, int iBlue)
{
	Evas_Object *obj;
	Evas_Coord w, h;

	if ((*pObj) != NULL)
	{
		printf("%s: *pObj exists - deleting\n", __FUNCTION__);
		evas_object_hide(*pObj);
		evas_object_del(*pObj);
		*pObj = NULL;
	}

	obj = evas_object_text_add(app ->fp ->e);
	//printf("%s: calling evas_object_text_font_source_set(%s)\n", __FUNCTION__, THEMES_DATAPATH);
	evas_object_text_font_source_set(obj, THEMES_DATAPATH);
	evas_object_text_font_set(obj, pFontFace, iSize);
	evas_object_text_text_set(obj, pText);
	evas_object_color_set(obj, iRed, iGreen, iBlue, 255);
	evas_object_geometry_get(obj, 0, 0, &w, &h);
	evas_object_resize(obj, app ->fp ->w, h);

	switch (iX)
	{
		case ALIGN_LEFT: iX = PAD_SPACE; break;
		case ALIGN_CENTER: iX = (app ->fp ->w / 2) - (w / 2); break;
   		case ALIGN_RIGHT: iX = (app ->fp ->w - w - PAD_SPACE); break;
	}

	evas_object_move(obj, iX, iY);
	evas_object_show(obj);

	*pObj = obj;
}

void CreateRectangle(struct appdata *app, Evas_Object **pObj, int iY, int iH, int iRed, int iGreen, int iBlue, int iAlpha)
{
	Evas_Object *obj;
	Evas_Coord iX = PAD_SPACE;
	Evas_Coord iW = app ->fp ->w - PAD_SPACE;

	obj = evas_object_rectangle_add(app ->fp ->e);
	evas_object_move(obj, iX, iY);
	evas_object_resize(obj, iW, iH);
	evas_object_color_set(obj, iRed, iGreen, iBlue, iAlpha);
	evas_object_show(obj);
	*pObj = obj;
}

void CreateLine(struct appdata *app, Evas_Object **pObj, int x1, int y1, int x2, int y2, int iRed, int iGreen, int iBlue)
{
	Evas_Object *obj;

	obj = evas_object_line_add(app ->fp ->e);
	evas_object_line_xy_set(obj, x1, y1, x2, y2);
	evas_object_color_set(obj, iRed, iGreen, iBlue, 255);
	evas_object_show(obj);
	*pObj = obj;
}

void CreateButton(struct appdata *app, Evas_Object **pObj, const char *pText, int iX, int iY, int iWidth, int iHeight, fp_button_clicked_cb cb)
{
	Evas_Object *obj;

	obj = fp_button_add_text(app ->fp, FP_TEXT_NORMAL, pText);
	fp_button_clicked_callback_add(obj, cb, app);
	fp_button_enable(obj);
	evas_object_move(obj, iX, iY);
	evas_object_resize(obj, iWidth, iHeight);
	evas_object_show(obj);
	*pObj = obj;
}


/*****************************************************************
 createScreen()
 create a empty screen containing only background

 Parameters:

 Returns:
 Evas Object
 *****************************************************************/
Evas_Object *createScreen(Evas_Object *screen, int xoffset, int yoffset, int width, int height)
{
	Evas_Object *sc;

	if (screen) {
		evas_object_hide(screen);
		evas_object_del(screen);
	}

	sc = fp_container_add(app->fp);
	evas_object_move(sc, xoffset, yoffset);
	if ( 0 >= width ) {
		width = app->fp->w - xoffset;
	}
	if ( 0 >= height ) {
		height = app->fp->h - yoffset;
	}
	evas_object_resize(sc, width, height);

	evas_object_show(sc);

	return sc;
}
#endif



char *readFile(char *fn, int mode)
{
#if 0 //JB
	struct stat st;
#endif
	char *tptr, *memptr;
	int h,count;
	long st_size;

	memptr=NULL;
#if 0 //JB
	stat(fn,&st);
	if (st.st_size>0) {
#endif
	st_size=dir_get_file_size(fn);
	if (st_size>0)
	{
		memptr=(char *)malloc(st_size+1);
		if (memptr) {
			tptr=memptr;
			h=open(fn,O_RDONLY);
			if (h>=0) {
				if (!mode) {
					read(h,tptr,st_size);
				tptr[st_size] = 0; // null terminate
				} else {
					count=st_size;
					while (count) {
						read(h,tptr,1);
						if ( !((*tptr < ' ') && (*tptr != '\n')) )
							tptr++;
						count--;
					}
					tptr[0] = 0; // null terminate
				}
				close(h);
			} else {
				free(tptr);
				memptr=NULL;
			}
		}
	}
//#endif
	return memptr;
}



#if 0 //JB
/*****************************************************************
 createTabBackground()
 create the basic background for tab views

 Parameters:
 tab_container - Container in which background is placed

 Returns:
 0 = success
 *****************************************************************/
int createTabBackground(void)
{
	Evas_Object *obj;
	int x, y, w, h;
	int containerX, containerY, containerW, containerH;

	evas_object_geometry_get(app->textRect, &containerX, &containerY, &containerW, &containerH);

	printf("%s: containerx=%d, containery=%d, containerw=%d, containerh=%d\r\n",__FUNCTION__,containerX,containerY,containerW,containerH);

	app->logContainer = createScreen(app->logContainer, containerX, containerY, containerW, containerH);

    // create the background
	obj = evas_object_rectangle_add( app->fp->e );
	evas_object_color_set(obj, 245, 246, 247, 255);
	evas_object_layer_set(obj, 75 - 1);
	evas_object_resize(obj, containerW, containerH);
	printf("%s: fp_container_object_add1(container = 0x%08x, obj = 0x%08x)\r\n",__FUNCTION__, (unsigned int)app->logContainer, (unsigned int)obj);
	fp_container_object_add(app->logContainer, obj, 0, 0); // relative to container x,y
	evas_object_show(obj);

	// add the watermark
	obj = fp_image_add(app->fp);
	fp_image_file_set(obj, "./flash/images/watermark.png");
	evas_object_layer_set(obj, 75); // on top of overlay
	fp_image_size_get(obj, &w, &h);
	x = (containerW - w)/2;
	y = (containerH - h)/2;
	evas_object_resize( obj, w, h );
	printf("%s: fp_container_object_add2(container = 0x%08x, obj = 0x%08x)\r\n",__FUNCTION__,(unsigned int)app->logContainer, (unsigned int)obj);
	fp_container_object_add(app->logContainer, obj, x, y);
	evas_object_show(obj);

	return 0;
}

Evas_Object *createMessageBox(char* msg, enum vf_dialog_icon icon, enum vf_dialog_buttons buttons, void (*msgButtonCb)(void *data, Evas_Object *o, const char *button, const char *input))
{
	Evas_Object *msgBox;

	msgBox = vf_dialog_add(app->fp, msg, icon, buttons, 0);
	if (msgButtonCb) {
		vf_dialog_callback_set(msgBox, msgButtonCb, (void *)msgBox);
	}
	evas_object_focus_set(msgBox, 1);
	evas_object_show(msgBox);
	return msgBox;
}

void destroyMessageBox(Evas_Object* msgBox)
{
    evas_object_del(msgBox);
}


Evas_Object *createText_wrap(Evas_Object *container, int textX, int textY, int textW, const char *msg)
{
	Evas_Object *o;
    int textH;
	int containerX, containerY, containerW, containerH;

	o = fp_text_add( app->fp );
	fp_text_fonts_set(o, "Vera", "Vera", "Vera", NULL, 10);
	evas_object_color_set(o, 0, 0, 0, 255);
	//fp_text_text_style_set(o, pTextDef->style);
	/* resize to requested width, and just use a dummy height */
	textH = 480; // just picked a number...
	evas_object_resize(o, textW, textH);
	if ( msg && strlen(msg) ) {
		fp_text_text_set(o, msg);
		/* now get real required height */
		textH = fp_text_height(o);
		/* now resize */
		evas_object_resize(o, textW, textH);
	}
	evas_object_layer_set( o, 90 );
	evas_object_show( o );
	if (container) {
		if ( 0 == textX ) {
			/* center text in container */
			evas_object_geometry_get(container, &containerX, &containerY, &containerW, &containerH);
			textX = (containerW - textW)/2;
		}
		fp_container_object_add(container, o, textX, textY);
	}

	return o;
}


void Clicked_AcceptButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event)
{
	int set_resRes;
	int process_result = TMS_STATUS_CONTENT_FAILURE;

#if DELETE_BUTTONS_IN_CB
	if (app->acceptButton)
	{
		evas_object_hide(app->acceptButton);
		evas_object_del(app->acceptButton);
		app->acceptButton = NULL;
	}

	if (app->rejectButton)
	{
		evas_object_hide(app->rejectButton);
		evas_object_del(app->rejectButton);
		app->rejectButton = NULL;
	}
#endif

	evas_object_text_text_set(app ->appStatus, "Accpeting Content");
	evas_render(app ->fp ->e);
	SVC_WAIT(2 * 1000);

	/* If we have the source and destination, lest copy the files */
	if (strlen(appEvent->filename) && strlen(appEvent->filepath))
	{
		char* pszBaseName;
		char tempname[256];
		char copyFileDest[512];
		int copy_result;

		strcpy(tempname, appEvent->filename);
		pszBaseName = basename(tempname);

		sprintf(copyFileDest, "%s/%s", appEvent->filepath, pszBaseName);
		if (VerifyFilePermissions(appEvent->filename, VERIFY_READ_PERMISSION))
		{
			if (VerifyFilePermissions(appEvent->filepath, VERIFY_WRITE_PERMISSION))
			{
				LogMsg("svc_tms_tester: Copying %s to %s\n", appEvent->filename, copyFileDest);
				copy_result = CopyFile(appEvent->filename, copyFileDest);
				LogMsg("svc_tms_tester: Copy result = %d\n", copy_result);

				if (copy_result == 0)
					process_result = TMS_STATUS_SUCCESS;
				else
					process_result = TMS_STATUS_CONTENT_FAILURE;
			}
		}
	}


	/* If we have a content handle we can set the result */
	if (appEvent->handle > 0)
	{
		LogMsg("svc_tms_tester: Content Accepted - Setting result of ID %d to %s\n", appEvent->handle, status_to_string(process_result));
		set_resRes = tms_setFileOperationResultWithDescription(appEvent->handle, process_result, TMS_EVT_PUT_FILE, NULL);
	}

	LogMsg("svc_tms_tester: freeing appEvent @ 0x%08x\n", (int)appEvent);
	//free(appEvent);
	//appEvent = NULL;

	evas_object_text_text_set(app ->appStatus, "Content Update Accepted");
	evas_render(app ->fp ->e);
	SVC_WAIT(2 * 1000);

	waiting_for_usr_response = FALSE;

	LogMsg("%s: returning\n", __FUNCTION__);
}

void Clicked_RejectButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event)
{
	int set_resRes;
	int process_result = TMS_STATUS_CONTENT_FAILURE;

#if DELETE_BUTTONS_IN_CB
	if (app->acceptButton)
	{
		evas_object_hide(app->acceptButton);
		evas_object_del(app->acceptButton);
		app->acceptButton = NULL;
	}

	if (app->rejectButton)
	{
		evas_object_hide(app->rejectButton);
		evas_object_del(app->rejectButton);
		app->rejectButton = NULL;
	}
#endif

	evas_object_text_text_set(app ->appStatus, "Rejecting Content");
	evas_render(app ->fp ->e);
	SVC_WAIT(2 * 1000);

	/* If we have a content handle we can set the result */
	if (appEvent->handle)
	{
		LogMsg("svc_tms_tester: Content Rejected - Setting result of ID %d to %s\n", appEvent->handle, status_to_string(process_result));
		set_resRes = tms_setFileOperationResultWithDescription(appEvent->handle, process_result, TMS_EVT_PUT_FILE, "App content REJECTED by user");
	}

	LogMsg("svc_tms_tester: freeing appEvent @ 0x%08x\n", (int)appEvent);
	//free(appEvent);
	//appEvent = NULL;

	evas_object_text_text_set(app ->appStatus, "Content Update Rejected");
	evas_render(app ->fp ->e);
	SVC_WAIT(2 * 1000);

	waiting_for_usr_response = FALSE;
}

void Clicked_ContactServerButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event)
{
	struct tmsReturn tms_ret;
	char disp_str1[256];

	if (app->contactServerButton)
	{
		evas_object_hide(app->contactServerButton);
	}
	if (app->appStatus2)
	{
		evas_object_hide(app->appStatus2);
		evas_object_del(app->appStatus2);
		app->appStatus2 = NULL;
	}

	evas_object_text_text_set(app ->appStatus, "Contacting Server");
	evas_render(app ->fp ->e);
	SVC_WAIT(1 * 1000);



	LogMsg("%s: Contacting server\n", __FUNCTION__);
	tms_ret = tms_callServer2(0, TMS_API_CALL_SERVER_FORCE_MAINTENANCE_FLAG);


	memset(disp_str1, 0, sizeof(disp_str1));
	LogMsg("%s: contact server result = %s\n", __FUNCTION__, status_to_string(tms_ret.status));
	if (tms_ret.status == TMS_STATUS_REQUESTED)
	{
		sprintf(disp_str1, "Call Server REQUESTED");
	}
	else
	{
		sprintf(disp_str1, "FAILURE to send contact server to agent");
	}

	if (app->appStatus)
	{
		evas_object_hide(app->appStatus);
		evas_object_del(app->appStatus);
		app->appStatus = NULL;
	}

	CreateText(app, &app ->appStatus, "Vera", 15, disp_str1, ALIGN_CENTER, (PAD_SPACE + 55), 255, 192, 192);

	LogMsg("%s: re-rendering\n", __FUNCTION__);
	evas_render(app ->fp ->e);
}




void Clicked_GetConfigButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event)
{
#if (!CONFIG_BUTTON_IS_GET_CONFIG)
	int copy_result;
#endif
	char disp_str1[256];
	struct tmsReturn tms_ret;

	if (app->getConfigButton)
	{
		evas_object_hide(app->getConfigButton);
	}
	if (app->appStatus2)
	{
		evas_object_hide(app->appStatus2);
		evas_object_del(app->appStatus2);
		app->appStatus2 = NULL;
	}

#if CONFIG_BUTTON_IS_GET_CONFIG

	evas_object_text_text_set(app ->appStatus, "Getting Config Location");
	evas_render(app ->fp ->e);
	SVC_WAIT(1 * 1000);



	LogMsg("%s: Getting Config Location\n", __FUNCTION__);
	tms_ret = tms_getConfigLocation();


	memset(disp_str1, 0, sizeof(disp_str1));
	LogMsg("%s: tms_getConfigLocation result = %s\n", __FUNCTION__, status_to_string(getConfigRes));
	if (tms_ret.status == TMS_STATUS_REQUESTED)
	{
		sprintf(disp_str1, "Get Config Location REQUESTED");
	}
	else
	{
		sprintf(disp_str1, "FAILURE to send get config location request to agent");
	}
#else
	evas_object_text_text_set(app ->appStatus, "Requesting Config Update");
	evas_render(app ->fp ->e);
	SVC_WAIT(1 * 1000);

	LogMsg("%s: Copying %s to %s\n", __FUNCTION__, "./tms_config.ini", "/tmp/share/tms_config.ini");
	copy_result = CopyFile("./tms_config.ini", "/tmp/share/tms_config.ini");
	LogMsg("%s: Copy result = %d\n", __FUNCTION__, copy_result);
	chmod("/tmp/share/tms_config.ini", 0666 );

	LogMsg("%s: Requesting Config Update\n", __FUNCTION__);
	tms_ret = tms_setNewConfigAvailable("/tmp/share/tms_config.ini");


	memset(disp_str1, 0, sizeof(disp_str1));
	LogMsg("%s: tms_newConfigAvail result = %s\n", __FUNCTION__, status_to_string(tms_ret.status));
	if (tms_ret.status == TMS_STATUS_REQUESTED)
	{
		sprintf(disp_str1, "Config Update REQUESTED");
	}
	else
	{
		sprintf(disp_str1, "FAILURE to send config update request to agent");
	}
#endif

	if (app->appStatus)
	{
		evas_object_hide(app->appStatus);
		evas_object_del(app->appStatus);
		app->appStatus = NULL;
	}

	CreateText(app, &app ->appStatus, "Vera", 15, disp_str1, ALIGN_CENTER, (PAD_SPACE + 55), 255, 192, 192);

	LogMsg("%s: re-rendering\n", __FUNCTION__);
	evas_render(app ->fp ->e);
}




void Clicked_ViewLogButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event)
{
	struct appdata *loc_app = (struct appdata *)data;
	Evas_Object *obj, *obj2, *o2;
	int coverX, coverY, coverW, coverH;
	char *tptr;
	struct tmsReturn tms_ret;


	evas_object_geometry_get(loc_app->textRect, &coverX, &coverY, &coverW, &coverH);

	createTabBackground();

	o2=createMessageBox("Please Wait...\nReading log file!",VF_DIALOG_ICON_INFO , VF_DIALOG_BUTTONS_CUSTOM, NULL);
	evas_render(app->fp->e);

	LogMsg("svc_tms_tester: Un-Registering App %s\n", "svc_tms_tester");
	tms_ret = tms_unregisterApplication("tms_tester12");
	if (tms_ret.status == TMS_STATUS_REQUESTED)
		LogMsg("svc_tms_tester: Un-Register requested\n");



	tptr=readFile(SVC_TMS_TESTER_LOG_FILE,0);
	if (tptr)
	{
		textbx = obj=createText_wrap(loc_app->logContainer, 0,0, coverW-18,tptr);
		free(tptr);
		obj2=fp_scrollbar_add(app->fp,obj);
		fp_scrollbar_bars_show_set(obj2,FP_SCROLLBAR_SHOW_AUTO,FP_SCROLLBAR_SHOW_NEVER);
        fp_scrollbar_dragging_set(obj2,1);
        fp_scrollbar_fading_set(obj2,1);
		evas_object_resize(obj2,coverW-15,coverH-15);
		fp_container_object_add(loc_app->logContainer, obj2, 10, 10);
		evas_object_show(obj2);
	}

	destroyMessageBox(o2);

	CreateButton(app, &app ->logDeleteButton, "Delete Log", (PAD_SPACE * 2), ((app ->fp ->h) - 60), 140, 40, Clicked_DeleteButtonCB);
	CreateButton(app, &app ->logExitButton, "Exit", 2 * (PAD_SPACE * 2) + 140, ((app ->fp ->h) - 60), 140, 40, Clicked_ExitButtonCB);
}

void Clicked_DeleteButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event)
{
	remove(SVC_TMS_TESTER_LOG_FILE);

	Clicked_ExitButtonCB(data, button, mouse_event);
	Clicked_ViewLogButtonCB(data, button, mouse_event);
}

void Clicked_ExitButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event)
{
	if (app->logContainer)
	{
		evas_object_hide(app->logContainer);
		evas_object_del(app->logContainer);
		app->logContainer = NULL;
	}

	if (app->logDeleteButton)
	{
		evas_object_hide(app->logDeleteButton);
		evas_object_del(app->logDeleteButton);
		app->logDeleteButton = NULL;
	}

	if (app->logExitButton)
	{
		evas_object_hide(app->logExitButton);
		evas_object_del(app->logExitButton);
		app->logExitButton = NULL;
	}
}
#endif

//	Make Enable / Disable Togglable
//	fp_button_background_normal
//	fp_button_background_pressed
//	fp_button_metric_image_set(app ->enableButton, "fp_button_background_pressed", "fp_button_background_pressed");
//	fp_button_metric_image_set(app ->disableButton, "fp_button_background_normal", "fp_button_background_pressed");
//	fp_button_metric_image_set(app ->enableButton, "fp_button_background_normal", "fp_button_background_pressed");
//	fp_button_metric_image_set(app ->disableButton, "fp_button_background_pressed", "fp_button_background_pressed");
