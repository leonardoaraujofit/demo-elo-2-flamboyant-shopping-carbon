#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdarg.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>


extern "C"
{
#include "svc_tms.h"

#include <fcntl.h>
#include <pwd.h>
#include <grp.h>

#include <syslog.h>
};
#include <FL/Fl.H>
#include <FL/Enumerations.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Tabs.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Multiline_Output.H>
#include <FL/Fl_Text_Display.H>
#include "log\logapi_common.h"
#ifdef _VOS2
#include <string>
#include <log/liblog.h>
#include <inf/infodb.h>
#endif

#define CONFIG_BUTTON_IS_GET_CONFIG		0

typedef unsigned int uint32;

char *readFile(char *fn, int mode);


struct passwd *psPasswd;
struct tmsReturn regRtn;


Fl_Text_Display *g_disp = NULL;
Fl_Text_Buffer *g_buff= NULL;



void WriteToOutput(char *pszTxt)
{
//	Fl::lock();

	g_buff->append(pszTxt);
	// Go to end of line
	g_disp->insert_position(g_disp->buffer()->length());
	g_disp->scroll(g_disp->count_lines(0, g_disp->buffer()->length(), 1), 0);

	Fl::awake();
}


void
clrscr(void)
{
}

void
write_at(char *pszFoo, int len, int x, int y)
{
	WriteToOutput(pszFoo);
}

#define dbprintf printf

void SVC_WAIT(int x)
{
	usleep(x * 10);
}


/* Allow 20 Kb for the log file */
#define SVC_TMS_TESTER_LOG_FILE						"svc_tms_tester_log"
#define SVC_TMS_TESTER_LOG_FILE_SIZE_LIMIT			(20 * 1024)
//#define printf(fmt, ...)	printf(  fmt, ##__VA_ARGS__)
//#define printf(fmt, ...)	printf(  fmt, ##__VA_ARGS__)

bool bVerbose = true;

static bool backup_requested = false;
static bool backup_active = false;
static bool third_active = false;
static char prevInstance1[MAX_SERVER_INSTANCE_NAME_LEN];
static char prevInstance2[MAX_SERVER_INSTANCE_NAME_LEN];


typedef enum
{
	VERIFY_READ_PERMISSION,
	VERIFY_WRITE_PERMISSION,
	VERIFY_EXECUTE_PERMISSION,
	VERIFY_RW_PERMISSION,
	VERIFY_RWX_PERMISSION,
}verify_permission_t;


unsigned char FileExists(const char *pszName)
{
	return false;
}



///////////////////////////////////////////////////////////////////////////////
///
///  \fn       void VerifyFilePermissions()
///  \brief    Verifies the agent has permissions to the file in question
///
///  Returns true if the permissions seem correct, false if not
////////////////////////////////////////////////////////////////////////////////
bool VerifyFilePermissions(char* pszConfigFile, verify_permission_t permission_type)
{
#if 0 //JB
	uid_t my_uid = getuid();
	int num_groups;
	gid_t my_gids[64];
	struct stat file_stat;
	char permissions_str[16];

	/* Make sure the file exists */
	if (stat(pszConfigFile, &file_stat) != 0)
	{
		printf("%s: %s does not exist - returning false\n", __FUNCTION__, pszConfigFile);
		return false;
	}


	memset(permissions_str, 0, sizeof(permissions_str));

	/* Setup directory */
	if (S_ISDIR(file_stat.st_mode))
		strcpy(permissions_str, "d");
	else
		strcpy(permissions_str, "-");

	/* Setup owner permissions */
	if (file_stat.st_mode & S_IRUSR)
		strcat(permissions_str, "r");
	else
		strcat(permissions_str, "-");
	if (file_stat.st_mode & S_IWUSR)
		strcat(permissions_str, "w");
	else
		strcat(permissions_str, "-");
	if (file_stat.st_mode & S_IXUSR)
		strcat(permissions_str, "x");
	else
		strcat(permissions_str, "-");


	/* Setup group permissions */
	if (file_stat.st_mode & S_IRGRP)
		strcat(permissions_str, "r");
	else
		strcat(permissions_str, "-");
	if (file_stat.st_mode & S_IWGRP)
		strcat(permissions_str, "w");
	else
		strcat(permissions_str, "-");
	if (file_stat.st_mode & S_IXGRP)
		strcat(permissions_str, "x");
	else
		strcat(permissions_str, "-");


	/* Setup other permissions */
	if (file_stat.st_mode & S_IROTH)
		strcat(permissions_str, "r");
	else
		strcat(permissions_str, "-");
	if (file_stat.st_mode & S_IWOTH)
		strcat(permissions_str, "w");
	else
		strcat(permissions_str, "-");
	if (file_stat.st_mode & S_IXOTH)
		strcat(permissions_str, "x");
	else
		strcat(permissions_str, "-");

	printf("%s: permissions of %s - %s\n", __FUNCTION__, pszConfigFile, permissions_str);


	if (permission_type == VERIFY_READ_PERMISSION)
	{
		if (file_stat.st_mode & S_IROTH)
		{
			printf("%s: we have 'other' read priveleges - returning true\n", __FUNCTION__);
			return true;
		}

		if (file_stat.st_mode & S_IRGRP)
		{
			int i;
			num_groups = getgroups(64, my_gids);

			for (i = 0; i < num_groups; i++)
			{
				if (my_gids[i] == file_stat.st_gid)
				{
					printf("%s: we have group read priveleges as gid %d - returning true\n", __FUNCTION__, my_gids[i]);
					return true;
				}
				printf("%s: no group read priveleges in gid %d (file gid = %d) - returning true\n", __FUNCTION__, my_gids[i], file_stat.st_gid);
			}
		}

		if ((my_uid == file_stat.st_uid) && (file_stat.st_mode & S_IRUSR))
		{
			printf("%s: we have owner read priveleges - returning true\n", __FUNCTION__);
			return true;
		}

		printf("%s: no read priveleges - setting up permission string\n", __FUNCTION__);
	}
	else if (permission_type == VERIFY_WRITE_PERMISSION)
	{
		if (file_stat.st_mode & S_IWOTH)
		{
			printf("%s: we have 'other' write priveleges - returning true\n", __FUNCTION__);
			return true;
		}

		if (file_stat.st_mode & S_IWGRP)
		{
			int i;
			num_groups = getgroups(64, my_gids);

			for (i = 0; i < num_groups; i++)
			{
				if (my_gids[i] == file_stat.st_gid)
				{
					printf("%s: we have group write priveleges as gid %d - returning true\n", __FUNCTION__, my_gids[i]);
					return true;
				}
				printf("%s: no group write priveleges in gid %d (file gid = %d)\n", __FUNCTION__, my_gids[i], file_stat.st_gid);
			}
		}

		if ((my_uid == file_stat.st_uid) && (file_stat.st_mode & S_IWUSR))
		{
			printf("%s: we have owner write priveleges - returning true\n", __FUNCTION__);
			return true;
		}

		printf("%s: no write priveleges - setting up permission string\n", __FUNCTION__);
	}
	else
		printf("%s: permission type %d not supported yet\n", __FUNCTION__, permission_type);


	printf("%s: Invalid permissions on %s - %s (Need read priveleges)\n", __FUNCTION__, pszConfigFile, permissions_str);
#endif
	return false;
}





static int CopyFile(const char *pszSrc, const char *pszDest)
{
	// wda: revisit
	return 0;
}






static char* status_to_string(int status)
{
	switch (status)
	{
	case TMS_STATUS_SUCCESS:
		return "Success";
	case TMS_STATUS_ERROR:
		return "Error";
	case TMS_STATUS_UNSUPPORTED_FEATURE:
		return "Unsupported Feature";
	case TMS_STATUS_CONTENT_AVAILABLE:
		return "Content Avaliable";
	case TMS_STATUS_NO_CONSUMER:
		return "No Consumer";
	case TMS_STATUS_FILENAME_ERROR:
		return "Filename ERROR";
	case TMS_STATUS_CONTENT_FAILURE:
		return "Status Content Failure";

	case TMS_STATUS_MSGQ_FAILURE:
		return "Message Queue FAILURE";
	case TMS_STATUS_MSGSND_FAILURE:
		return "Message Send FAILURE";
	case TMS_STATUS_MSGRCV_FAILURE:
		return "Message Receive FAILURE";
	case TMS_STATUS_APP_EVENT_AVAIL:
		return "App Event Available";

	case TMS_STATUS_REQUESTED:
		return "Requested";

	case TMS_STATUS_REGISTER_FAIL:
		return "Register Fail";
	case TMS_STATUS_REGISTER_NAME_TOO_LONG:
		return "Register Name Too Long";
	case TMS_STATUS_EINVAL:
		return "Error in Value";
	case TMS_STATUS_AGENT_NOT_RUNNING:
		return "Agent Not Running";

	case TMS_STATUS_SERVER_INSTANCE_ERROR:
		return "Srv Inst Error";
	case TMS_STATUS_SERVER_INSTANCE_LOCK_ERROR:
		return "Srv Inst Lock Error";
	case TMS_STATUS_SERVER_INSTANCE_LOCK_TIMEOUT_ERROR:
		return "Srv Inst Lock Timeout Error";
	case TMS_STATUS_SERVER_INSTANCE_ALREADY_LOCKED:
		return "Srv Inst Already Locked";
	case TMS_STATUS_SERVER_INSTANCE_MAX_LOCK_REQ_EXCEEDED:
		return "Too Many SrvInst Lock Requests";
	case TMS_STATUS_SERVER_INSTANCE_UNLOCK_ERROR:
		return "Srv Inst Unlock Error";
	case TMS_STATUS_SERVER_INSTANCE_CONFIG_LOAD_ERROR:
		return "Srv Inst Config Load Error";

	case TMS_STATUS_AGENT_BUSY_ERROR:
		return "Agent Busy Error";
	case TMS_STATUS_POSTPONED:
		return (char*) "Postponed";
	case TMS_STATUS_CANCELLED:
		return (char*) "Cancelled";
	case TMS_STATUS_AGENT_CONTACTSRV_ERROR:
		return "Agent not registered on the server yet";
	case TMS_STATUS_API_SYNC_ERROR:
		return "Lost SYNC of TMS exchange";

	default:
		return "Unknown";
	}

	return "Unknown";
}

static char* msgType_to_string(int msgType)
{
	switch (msgType)
	{
	case TMS_EVT_CALL_SERVER_RESPONSE:
		return "Call Server";
	case TMS_EVT_SET_APP_STATE:
		return "Get APP State";
	case TMS_EVT_GET_TMS_CONFIG_RESPONSE:
		return "Get TMS Config";
	case TMS_EVT_SET_TMS_CONFIG_RESPONSE:
		return "Set TMS Config";
	case TMS_EVT_SET_APP_INFO:
		return "Get APP Info";
	case TMS_EVT_SET_PARM_LIST:
		return "Get Parameter List";
	case TMS_EVT_GET_FILE:
		return "Get File";
	case TMS_EVT_PUT_FILE:
		return "Put File";
	case TMS_EVT_DEL_FILE:
		return "Delete File";
	case TMS_EVT_NOTIFICATION:
		return "Event Notfication";
	case TMS_EVT_DO_TRANSACTION:
		return "Do Transaction";
	case TMS_EVT_REGISTER_APP_RESPONSE:
		return "Register";
	case TMS_EVT_UNREGISTER_APP_RESPONSE:
		return "Un-Register";

	case TMS_EVT_GET_SERVER_INSTANCE:
		return "Get Srv Inst";
	case TMS_EVT_LOCK_SERVER_INSTANCE:
		return "Lock Srv Inst";
	case TMS_EVT_RELEASE_SERVER_INSTANCE:
		return "Release Srv Inst";

	case TMS_EVT_APP_ALERT_RESULT:
		return "App Event Result";

	case TMS_EVT_CLEAR_APP_INFO_RESULT:
		return "Clear App Info Result";

	case TMS_EVT_API_ERRORS:
		return "API Usage Error";

	default:
		return "Unknown";
	}

	return "Unknown";
}


#define DELETE_BUTTONS_IN_CB	0



struct tmsEventData* appEvent;
bool waiting_for_usr_response = false;



#ifdef VHQ_VX
#define APP_PARM_DATA_FILE					"I:/AppParmFile.dat"
#else
#define APP_PARM_DATA_FILE					"/tmp/share/AppParmFile.dat"
#endif
#define APP_PARM_DATA_FILE_TEST_DATA		"test.ini SHA-1 1234567890123456789012345678901234567890"


struct tmsAppParameter custom_parameters[] =
{
	{"DeviceId", "555666", TMS_PARAMETER_TYPE_IDENTIFIER},
	{"StoreId", "Store11", TMS_PARAMETER_TYPE_IDENTIFIER},
	{"LaneId", "Lane5", TMS_PARAMETER_TYPE_IDENTIFIER},
	{"DevProfileTestData", "false", TMS_PARAMETER_TYPE_DEVICE_PROFILE},
	{"DiagProfileTestData", "true", TMS_PARAMETER_TYPE_DIAGNOSTIC},
};

void createParamList(struct tmsAppInfo *appInfo, uint32 param_mask)
{
	int i, entry_index;
	int num_params = 0;
	struct tmsAppParameter* parameter_array = NULL;

	/* Find out how many parameters to send */
	for (i = 0; i < (sizeof(custom_parameters) / sizeof(struct tmsAppParameter)); i++)
	{
		if (custom_parameters[i].parameterType & param_mask)
		{
			num_params++;
			printf("%s: custom_parameter[%d] (type = %d) matches mask 0x%08x - num_params incremented to %d\n", __FUNCTION__, i, custom_parameters[i].parameterType, param_mask, num_params);
		}
	}

	printf("%s: Allocating %d bytes for %d parameters\n", __FUNCTION__, num_params * sizeof(struct tmsAppParameter), num_params);
	parameter_array = (struct tmsAppParameter* ) malloc(num_params * sizeof(struct tmsAppParameter));
	memset(parameter_array, 0, (num_params * sizeof(struct tmsAppParameter)));
	for (i = 0, entry_index = 0; ((i < (sizeof(custom_parameters) / sizeof(struct tmsAppParameter))) && (entry_index < num_params)); i++)
	{
		if (custom_parameters[i].parameterType & param_mask)
		{
			printf("%s: custom_parameter[%d] (type = %d) matches mask 0x%08x - adding to index %d\n", __FUNCTION__, i, custom_parameters[i].parameterType, param_mask, entry_index);
			memcpy(&parameter_array[entry_index], &custom_parameters[i], sizeof(struct tmsAppParameter));
			entry_index++;
		}
	}


	appInfo->parameterCount = num_params;
	appInfo->parameterList = parameter_array;
}



#define SYSLOG_OUTPUT_FILE		"syslog.log"
#ifndef DATAPATH
#define DATAPATH	"./flash"
#endif






char *readFile(char *fn, int mode)
{
	return NULL;
}




#if 1
int LogMsgEx(const char *fname, const char *string, va_list ap)
{
	int r = 0;
	struct timeval cur_time;
	struct tm local;
	time_t t;

	gettimeofday(&cur_time, NULL);
	t = cur_time.tv_sec;
	
#if 0
	while ((*string == '\n') || (*string == '\r'))
	{
		printf("%c", *string);
		string++;
	}
#endif
		
	if (localtime_r(&t, &local))
		printf("%02d:%02d:%02d: ", local.tm_hour, local.tm_min, local.tm_sec);
	else
		printf("%s: errno set in localtime_r() = %d\n", __FUNCTION__, errno);

	r = vprintf(string, ap);

	{
		char szMsg[256];
		vsnprintf(szMsg, sizeof(szMsg) - 1, string, ap);
		WriteToOutput(szMsg);
	}

	return r;
}

int LogMsg(const char *string, ...)
{
	int ret_val;

	va_list ap;
	va_start(ap, string);
	ret_val = LogMsgEx(SVC_TMS_TESTER_LOG_FILE, string, ap);
	va_end(ap);

	return ret_val;
}
#else
#define LogMsg printf
#endif 

static int HandleApplicationEvent(struct tmsEventData eventData)
{
	int param_count;
	int ret_val, result;
	FILE *fd;
	char* pszBaseName;
	char disp_str[256];
	char param_file_copy_name[MAX_TMS_FILENAME_LENGTH];
	struct tmsAppInfo appInfo;


	appEvent = &eventData;


	memset(disp_str, 0, sizeof(disp_str));
	printf("%s: Received App Message - %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
	sprintf(disp_str, "Received App Message - %s", msgType_to_string(appEvent->evtType));
	WriteToOutput(disp_str);
	printf("%s: at %d\r", __FUNCTION__, __LINE__);

	switch (appEvent->evtType)
	{
	case TMS_EVT_CALL_SERVER_RESPONSE:
		printf("%s: at %d\r", __FUNCTION__, __LINE__);

		if (appEvent->status == TMS_STATUS_SUCCESS)
		{
			sleep(2);
			sprintf(disp_str, "Call Server SUCCESSFUL");
			WriteToOutput(disp_str);


		}
		else
		{
			sprintf(disp_str, "Call Server FAILED");
			WriteToOutput(disp_str);
		}

		printf("%s: adding contact server button back\n", __FUNCTION__);
		break;

	case TMS_EVT_SET_APP_STATE:
		printf("%s: at %d\r", __FUNCTION__, __LINE__);
		LogMsg("%s: Handling %s - eventMask = 0x%08x\n", __FUNCTION__, msgType_to_string(appEvent->evtType), appEvent->eventMask);
		/* Call service to set app state */
		printf("%s: at %d\r", __FUNCTION__, __LINE__);

		LogMsg("%s: Calling tms_setApplicationState(%d, APP_STATUS_FREE)\n", __FUNCTION__, appEvent->handle);
		printf("%s: at %d\r", __FUNCTION__, __LINE__);
			ret_val = tms_setApplicationState(appEvent->handle, APP_STATUS_FREE);

		printf("%s: at %d\r", __FUNCTION__, __LINE__);


		sprintf(disp_str, "tms_setApplicationState() returned %s", status_to_string(ret_val));
		WriteToOutput(disp_str);
		break;

	case TMS_EVT_GET_TMS_CONFIG_RESPONSE:
		printf("%s: at %d\r", __FUNCTION__, __LINE__);

		if (appEvent->status == TMS_STATUS_SUCCESS)
		{
			sleep(2);
			sprintf(disp_str, "Get Config Location SUCCESSFUL");
			WriteToOutput(disp_str);


			if (strlen(appEvent->filename))
			{
				sprintf(disp_str, "Config file located at %s", appEvent->filename);
			}
			else
			{
				sprintf(disp_str, "No Config File Location Returned");
			}

			WriteToOutput(disp_str);
		}
		else
		{
			sprintf(disp_str, "Get Config Location FAILED");
			WriteToOutput(disp_str);
		}

		printf("%s: adding get config button back\n", __FUNCTION__);
		break;

	case TMS_EVT_SET_TMS_CONFIG_RESPONSE:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		/* Call service to set app state */

		sprintf(disp_str, "Config Update returned %s", status_to_string(appEvent->status));
		WriteToOutput(disp_str);

		printf("%s: adding set config button back\n", __FUNCTION__);
		break;

	case TMS_EVT_SET_APP_INFO:
		LogMsg("%s: Handling %s (mask = 0x%08x)\n", __FUNCTION__, msgType_to_string(appEvent->evtType), appEvent->eventMask);
		/* Call service to set app state */

		memset(&appInfo, 0, sizeof(struct tmsAppInfo));

		createParamList(&appInfo, appEvent->eventMask);

		LogMsg("%s: Calling tms_setApplicationInfo(%d) - appInfo.parameterCount = %d, appInfo.parameterList = 0x%08x\n", __FUNCTION__, appEvent->handle,
			appInfo.parameterCount, appInfo.parameterList);
		for (param_count = 0; param_count < appInfo.parameterCount; param_count++)
		{
			LogMsg("\tParameter %d: name = %s, value = %s, type = %d\n", param_count, appInfo.parameterList[param_count].parameterName,
				appInfo.parameterList[param_count].parameterValue, appInfo.parameterList[param_count].parameterType);
		}

		ret_val = tms_setApplicationInfo(appEvent->handle, appInfo);

		free(appInfo.parameterList);


		sprintf(disp_str, "tms_setApplicationInfo() returned %s", status_to_string(ret_val));
		WriteToOutput(disp_str);
		LogMsg("%s: tms_setApplicationInfo() returned %s (%d)", __FUNCTION__, status_to_string(ret_val), ret_val);
		break;

	case TMS_EVT_SET_PARM_LIST:

		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		/* Call service to set parameter list */
		fd = fopen(APP_PARM_DATA_FILE, "w");

		if (fd)
		{
			fwrite(APP_PARM_DATA_FILE_TEST_DATA, strlen(APP_PARM_DATA_FILE_TEST_DATA), 1, fd);
			fclose(fd);
		}
		else
			LogMsg("%s: errno set in opening file (%s) = %d\n", __FUNCTION__, APP_PARM_DATA_FILE, errno);

		LogMsg("%s: Calling tms_setApplicationParameterList(%d, 1, %s)\n", __FUNCTION__, appEvent->handle, APP_PARM_DATA_FILE);
		ret_val = tms_setApplicationParameterList(appEvent->handle, 1, APP_PARM_DATA_FILE);


		sprintf(disp_str, "tms_setApplicationParameterList() returned %s", status_to_string(ret_val));
		WriteToOutput(disp_str);
		break;

	case TMS_EVT_GET_FILE:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		memset(param_file_copy_name, 0, sizeof(param_file_copy_name));
		/* Make sure filename is valid */
		if (strcmp(appEvent->filename, "test.ini") == 0)
		{
			int copy_result;
			int chmod_ret, chown_ret;
			char groupname[16];
			struct group *psGroup;
			struct passwd *psPasswd;


			psPasswd = getpwuid(getuid());

			sprintf(param_file_copy_name, "/tmp/share/test.ini");
			LogMsg("%s: Copying %s to %s\n", __FUNCTION__, appEvent->filename, param_file_copy_name);
			copy_result = CopyFile("./test.ini", param_file_copy_name);
			LogMsg("%s: Copy result = %d\n", __FUNCTION__, copy_result);

			if (psPasswd)
				sprintf(groupname, "%ssys", psPasswd->pw_name);
			else
				sprintf(groupname, "sys");
			psGroup = getgrnam(groupname);

			if (psGroup)
			{
				chmod_ret = chmod(param_file_copy_name, 0640);
				chown_ret = chown(param_file_copy_name, getuid(), psGroup->gr_gid);
				LogMsg("%s: chmod to 640 returned (%d) - chown to %s group returned %d\n", __FUNCTION__, chmod_ret, groupname, chown_ret);
			}
			else
				chmod_ret = chmod(param_file_copy_name, 0644);

			if (copy_result == 0)
				result = TMS_STATUS_SUCCESS;
			else
			{
				sprintf(disp_str, "Error %d copying test.ini to %s", copy_result, param_file_copy_name);
				result = TMS_STATUS_ERROR;
			}
		}
		else
		{
			sprintf(disp_str, "Invalid Parameter File to Get-%s", appEvent->filename);
			LogMsg("%s: Invalid parameter filename - %s\n", __FUNCTION__, appEvent->filename);
			result = TMS_STATUS_FILENAME_ERROR;
		}


		LogMsg("%s: Calling tms_getApplicationFileAvailable(%d, %s, %s, true)\n", __FUNCTION__, appEvent->handle, status_to_string(result), param_file_copy_name);
		ret_val = tms_getApplicationFileAvailable(appEvent->handle, result, param_file_copy_name, true);


		sprintf(disp_str, "tms_getApplicationFileAvailable() returned %s", status_to_string(ret_val));

		WriteToOutput(disp_str);
		break;

	case TMS_EVT_PUT_FILE:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		memset(param_file_copy_name, 0, sizeof(param_file_copy_name));
		strcpy(param_file_copy_name, appEvent->filename);
		pszBaseName = basename(param_file_copy_name);

		/* Make sure filename is valid */
		if (strcmp(pszBaseName, "test.ini") == 0)
		{
			int copy_result;

			LogMsg("%s: Copying %s to ./test.ini\n", __FUNCTION__, appEvent->filename);
			copy_result = CopyFile(appEvent->filename, "./flash/test.ini");
			LogMsg("%s: Copy result = %d\n", __FUNCTION__, copy_result);

			if (copy_result == 0)
				result = TMS_STATUS_SUCCESS;
			else
			{
				sprintf(disp_str, "Error %d copying %s to ./flash/test.ini", copy_result, appEvent->filename);
				result = TMS_STATUS_ERROR;
			}

			LogMsg("%s: Calling tms_setFileOperationResult(%d, %s, TMS_EVT_PUT_FILE)\n", __FUNCTION__, appEvent->handle, status_to_string(result));
			ret_val = tms_setFileOperationResultWithDescription(appEvent->handle, result, TMS_EVT_PUT_FILE, "App PUT successful");

	
			if (result == TMS_STATUS_SUCCESS)
				sprintf(disp_str, "tms_setFileOperationResult() returned %s", status_to_string(ret_val));
			else
				WriteToOutput(disp_str);
		}
		else
		{
#if 0
//			evas_object_text_text_set(app->appStatus, "Received Content Update - Accept or Reject");

			LogMsg("svc_tms_tester: Got a content event\n\tStatus = %s\n\tType = %d\n\tContent ID = %d\n\tFilename = %s\n\tfilepath = %s\n",
				status_to_string(appEvent->status), appEvent->putFileType, appEvent->handle, appEvent->filename, appEvent->filepath);

			CreateButton(app, &app->acceptButton, "Accept Content", (PAD_SPACE * 2), 185, 140, 40, Clicked_AcceptButtonCB);
			CreateButton(app, &app->rejectButton, "Reject Content", 2 * (PAD_SPACE * 2) + 140, 185, 140, 40, Clicked_RejectButtonCB);

			evas_render(app->fp->e);

			waiting_for_usr_response = true;

			while (waiting_for_usr_response)
			{
				printf("%s: Waiting for user response\n", __FUNCTION__);
				sleep(3);
			}


#if (!DELETE_BUTTONS_IN_CB)
			printf("%s: Got user response - deleting buttons\n", __FUNCTION__);

			/* We can remove the accept button */
			if (app->acceptButton)
			{
				evas_object_hide(app->acceptButton);
				evas_object_del(app->acceptButton);
				app->acceptButton = NULL;
			}

			/* We can remove the reject button */
			if (app->rejectButton)
			{
				evas_object_hide(app->rejectButton);
				evas_object_del(app->rejectButton);
				app->rejectButton = NULL;
			}
#else
			printf("%s: Got user response\n", __FUNCTION__);
#endif
#endif
		}
		break;

	case TMS_EVT_DEL_FILE:
		LogMsg("%s: Handling %s - Delete Filename = %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType), appEvent->filename);
		/* Call service and just respond with a SUCCESS message */

		LogMsg("%s: Calling tms_setFileOperationResult(%d, TMS_STATUS_ERROR, TMS_EVT_DEL_FILE) - svc_tms_tester does not delete files\n", __FUNCTION__, appEvent->handle);
		ret_val = tms_setFileOperationResultWithDescription(appEvent->handle, TMS_STATUS_ERROR, TMS_EVT_DEL_FILE, "App DELETE not supported");


		sprintf(disp_str, "tms_setFileOperationResult() returned %s", status_to_string(ret_val));
		WriteToOutput(disp_str);
		break;

	case TMS_EVT_NOTIFICATION:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));

		/* Just display a message with the notification event */

		if (appEvent->eventMask & TMS_EVENT_NOTIFY_REBOOT_DEVICE)
		{
			strcat(disp_str, "Device Rebooting ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_RESTART_APPS)
		{
			struct tmsReturn tms_ret;
			strcat(disp_str, "Restarting Apps ");

			LogMsg("svc_tms_tester: Un-Registering App %s\n", "svc_tms_tester");
			tms_ret = tms_unregisterApplication("svc_tms_tester");
			if (tms_ret.status == TMS_STATUS_REQUESTED)
				LogMsg("svc_tms_tester: Un-Register requested\n");


		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_DOWNLOAD_STARTED)
		{
			strcat(disp_str, "Download Started-file=");
			strcat(disp_str, appEvent->filename);
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_DOWNLOAD_PERCENT)
		{
			char percent_str[16];

			memset(percent_str, 0, sizeof(percent_str));
			sprintf(percent_str, "%d", appEvent->notificationData.downloadPercent);

			strcat(disp_str, "Download Percent Update-file=");
			strcat(disp_str, appEvent->filename);
			strcat(disp_str, "at ");
			strcat(disp_str, percent_str);
			strcat(disp_str, " percent ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_DOWNLOAD_COMPLETE)
		{
			strcat(disp_str, "Download Complete-file=");
			strcat(disp_str, appEvent->filename);
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_INSTALL_DOWNLOAD)
		{
			strcat(disp_str, "Installing downloaded file - ");
			strcat(disp_str, appEvent->filename);
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_MAINTENANCE_END)
		{
			strcat(disp_str, "Maintenance Completed");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_HEARTBEAT_RESULT)
		{
			strcat(disp_str, "Heartbeat Completed - ");
			if (appEvent->status == TMS_STATUS_SUCCESS)
				strcat(disp_str, "SUCCESSFUL");
			else
				strcat(disp_str, "FAILURE");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_CONTACT_SERVER_START)
		{
			strcat(disp_str, "Contacting Server");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_CONTACT_SERVER_END)
		{
			strcat(disp_str, "Server Contact Complete");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_CONTACT_SERVER_APPROVAL)
		{
			/* Should never get this notification.  It is a duplicate of CONTACT_SERVER_START, but when registering
			for this notifcation, it means the app wants to approve server contact */
			strcat(disp_str, "Contact Server Approval");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_CONTENT_UPDATES)
		{
			strcat(disp_str, "Content Update");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_CLOCK_UPDATE)
		{
			strcat(disp_str, "Clock Updated");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_SERVER_ERRORS)
		{
			char temp_str[128];

			LogMsg("%s: Server Error Notification - Error = %s (%d)\n", __FUNCTION__, appEvent->filename, appEvent->status);
			sprintf(temp_str, "Server Error %d", appEvent->status);
			strcat(disp_str, temp_str);
			strcat(disp_str, " ");
			WriteToOutput(appEvent->filename);
		}
		WriteToOutput(disp_str);
		break;

	case TMS_EVT_DO_TRANSACTION:
		LogMsg("%s: Handling %s - transaction = %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType), appEvent->transactionType);
		/* Call service and just respond with a SUCCESS message */

		LogMsg("%s: Calling tms_setTransactionResult(%d, TMS_STATUS_SUCCESS)\n", __FUNCTION__, appEvent->handle);
		ret_val = tms_setTransactionResult(appEvent->handle, TMS_STATUS_SUCCESS);


		sprintf(disp_str, "tms_setTransactionResult() returned %s", status_to_string(ret_val));
		WriteToOutput(disp_str);
		break;

	case TMS_EVT_REGISTER_APP_RESPONSE:

		if (appEvent->status == TMS_STATUS_SUCCESS)
		{
			sprintf(disp_str, "Registration SUCCESSFUL");
			WriteToOutput(disp_str);

			//tms_setApplicationState(-1, APP_STATUS_BUSY);
		}
		else
		{
			sprintf(disp_str, "Registration FAILED");
			WriteToOutput(disp_str);
		}
		break;

	case TMS_EVT_UNREGISTER_APP_RESPONSE:

		if (appEvent->status == TMS_STATUS_SUCCESS)
		{
			sprintf(disp_str, "Un-Registration SUCCESSFUL");
			WriteToOutput(disp_str);
		}
		else
		{
			sprintf(disp_str, "Un-Registration FAILED");
			WriteToOutput(disp_str);
		}
		break;


	case TMS_EVT_GET_SERVER_INSTANCE:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		if (appEvent->status == TMS_STATUS_SUCCESS)
			sprintf(disp_str, "GetServerInstance returned %s SUCCESSFULLY", appEvent->filename);
		else
			sprintf(disp_str, "GetServerInstance returned ERROR %s", status_to_string(appEvent->status));

		WriteToOutput(disp_str);
		break;

	case TMS_EVT_LOCK_SERVER_INSTANCE:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		if (appEvent->status == TMS_STATUS_SUCCESS)
		{
			if (backup_requested)
			{
				backup_requested = false;
				backup_active = true;
				strcpy(prevInstance1, appEvent->filename);
				sprintf(disp_str, "LockServerInstance 'backup' SUCCESSFUL - Old Instance = %s", prevInstance1);
				LogMsg("LockServerInstance 'backup' SUCCESSFUL - Old Instance = %s\n", prevInstance1);
			}
			else
			{
				third_active = true;
				strcpy(prevInstance2, appEvent->filename);
				sprintf(disp_str, "LockServerInstance 'third' SUCCESSFUL - Old Instance = %s", prevInstance2);
				LogMsg("LockServerInstance 'third' SUCCESSFUL - Old Instance = %s\n", prevInstance2);
			}
		}
		else
			sprintf(disp_str, "LockServerInstance returned ERROR %s", status_to_string(appEvent->status));
		WriteToOutput(disp_str);

		printf("%s: adding swap config1 button back\n", __FUNCTION__);
#if 0
		if (app->swapConfig1Button)
		{
			evas_object_show(app->swapConfig1Button);
		}

		printf("%s: adding swap config2 button back\n", __FUNCTION__);
		if (app->swapConfig2Button)
		{
			evas_object_show(app->swapConfig2Button);
		}
#endif
		break;


	case TMS_EVT_RELEASE_SERVER_INSTANCE:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		if (appEvent->status == TMS_STATUS_SUCCESS)
		{
			if (backup_active)
			{
				backup_active = false;
				sprintf(disp_str, "ReleaseServerInstance 'backup' SUCCESSFUL");
				LogMsg("ReleaseServerInstance 'backup' SUCCESSFUL - %s restored\n", prevInstance1);
			}
			else if (third_active)
			{
				third_active = false;
				sprintf(disp_str, "ReleaseServerInstance 'third' SUCCESSFUL");
				LogMsg("ReleaseServerInstance 'third' SUCCESSFUL - %s restored\n", prevInstance2);
			}
		}
		else
			sprintf(disp_str, "ReleaseServerInstance returned ERROR %s", status_to_string(appEvent->status));
		WriteToOutput(disp_str);

		printf("%s: adding swap config1 button back\n", __FUNCTION__);
#if 0
		if (app->swapConfig1Button)
		{
			evas_object_show(app->swapConfig1Button);
		}

		printf("%s: adding swap config2 button back\n", __FUNCTION__);
		if (app->swapConfig2Button)
		{
			evas_object_show(app->swapConfig2Button);
		}
#endif
		break;

	case TMS_EVT_APP_ALERT_RESULT:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		if (appEvent->status == TMS_STATUS_SUCCESS)
			sprintf(disp_str, "App Alert sent SUCCESSFULLY");
		else
			sprintf(disp_str, "App Alert ERROR %s", status_to_string(appEvent->status));

		WriteToOutput(disp_str);
		break;

	case TMS_EVT_CLEAR_APP_INFO_RESULT:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		if (appEvent->status == TMS_STATUS_SUCCESS)
			sprintf(disp_str, "Clear App Info SUCCESSFUL");
		else
			sprintf(disp_str, "Clear App Info ERROR %s", status_to_string(appEvent->status));

		WriteToOutput(disp_str);
		break;

	case TMS_EVT_API_ERRORS:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		/*
		 * This is critical ERROR, VHQ Agent can't trust data exchange with application start from this moment.
		 * Verify your code and provide required changes!
		 *
		 * Future TMS processing with VHQ Agent is not safe. The best case here is to unregister and close/restart application.
		 * You can use tms_sendApplicationAlert() to notify server about problems. Please do not use it for production build.
		 *
		 * Do not use next TMS APIs here:
		 * - tms_setTransactionResult()
		 * - tms_setApplicationState(with handle)
		 * - tms_setApplicationInfo()
		 * - tms_setApplicationParameterList()
		 * - tms_getApplicationFileAvailable()
		 * - tms_setFileOperationResult()
		 * - tms_setFileOperationResultWithDescription()
		 * - tms_setTransactionResult()
		 * since it will cause closed loop behavior, closed loop up to timeout on VHQ Agent side.
		 */
		if (appEvent->status == TMS_STATUS_API_SYNC_ERROR)
		{
			struct tmsReturn tms_ret;
#ifdef DEBUG_BUILD
			/* Let's notify server that we have some problems */
			sprintf(disp_str, "API processing error %s: %s", status_to_string(appEvent->status), appEvent->filename);
			tms_ret = tms_sendApplicationAlert(status_to_string(appEvent->status), TMS_ALERT_SEVERITY_HIGH, appEvent->filename);
			if (tms_ret.status == TMS_STATUS_REQUESTED)
				LogMsg("svc_tms_tester: Alert requested\n");
#endif
			/* Unregister application */
			LogMsg("svc_tms_tester: Un-Registering App %s\n", "svc_tms_tester");
			tms_ret = tms_unregisterApplication("svc_tms_tester");
			if (tms_ret.status == TMS_STATUS_REQUESTED)
				LogMsg("svc_tms_tester: Un-Register requested\n");
			/* Close application */
			exit(0);
		} else
			sprintf(disp_str, "API processing error: %s (%d)", status_to_string(appEvent->status), appEvent->status);

		WriteToOutput(disp_str);
		break;
	}

	//evas_render(app->fp->e);

	return 0;
}



void
Register(void)
{
	LogMsg("svc_tms_tester: TMS App Ifc Version (tms_GetVersion) returns %s\n", tms_GetVersion());

	psPasswd = getpwuid(getuid());
	if (psPasswd)
		LogMsg("svc_tms_tester: Registering App %s as %s\n", "svc_tms_tester", psPasswd->pw_name);

	regRtn = tms_registerApplication("svc_tms_tester", 0xffffffff, (unsigned int)HandleApplicationEvent);
	printf("%s: at %d\r\n", __FUNCTION__, __LINE__);

	if (regRtn.status == TMS_STATUS_REQUESTED)
	{
		LogMsg("svc_tms_tester: Register App REQUESTED\n");

		LogMsg("svc_tms_tester: TMS Agent Version (tms_GetAgentVersion) = %s\n", tms_GetAgentVersion());
		printf("%s: at %d\r\n", __FUNCTION__, __LINE__);
		if (errno != 0)
		{
			LogMsg("svc_tms_tester ERROR: tms_GetAgentVersion() returned errno %d\n", errno);
		}
	}

	printf("%s: at %d\r", __FUNCTION__, __LINE__);

}



void RequestHB(Fl_Widget *widget, void* pv)
{
	struct tmsReturn retval;
	retval= tms_callServer(0);

	if (retval.status == TMS_STATUS_REQUESTED)
		WriteToOutput("HB Successfully Requested\n");
	else
		WriteToOutput("HB not successfully Requested\n");
}


void ClearAgentCache(Fl_Widget *widget, void* pv)
{
	struct tmsReturn retval;
	retval = tms_clearApplicationInfo();

	if (retval.status == TMS_STATUS_REQUESTED)
		WriteToOutput("HB Successfully Requested\n");
	else
		WriteToOutput("HB not successfully Requested\n");
}


#ifdef _VOS2

#define CHECK(x) if((x)<INFO_OK) {LOGF_ERROR("Failed %s %d, code %d",__FILE__,__LINE__,(x)); info_closedb(db); return 1; }

unsigned int timestamp() {
	struct timeval tv;
	gettimeofday(&tv, 0);
	return (unsigned int)(tv.tv_sec * 1000 + tv.tv_usec / 1000);
}

int create_db(Fl_Widget *widget, void* pv) {
	int rc;
	mkdir("flash", 0755);
	std::string infodb_file = "/tmp/conn_info.db";

	struct stat buf;


	if (stat(infodb_file.c_str(), &buf) == 0) {
		/*Infodb exists. Nothing to do*/
		return 0;
	}

	InfoDB *db = info_opendb(infodb_file.c_str(), INFO_READ_WRITE);

	if (!db) {
		LOGF_ERROR("Cannot open database");
		return 1;
	}

	LOGF_TRACE("Opened DB\n");

#ifdef _VATS
	// subset 1
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_A/0/type", "TCP");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_A/0/network", "com/networks/mynetwork");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_A/0/address", "vhqtest.verifone.com");
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_A/0/port", 443);
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_A/0/timeout", 60000);
	CHECK(rc);

	// subset 2 - fallback
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_A/1/type", "TCP");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_A/1/network", "com/networks/mynetwork");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_A/1/address", "10.217.12.110");
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_A/1/port", 443);
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_A/1/timeout", 60000);
	CHECK(rc);

#else	
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_A/0/type", "TCP");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_A/0/network", "com/networks/mynetwork");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_A/0/address", "vhqtest.verifone.com");
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_A/0/port", 443);
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_A/0/timeout", 60000);
	CHECK(rc);
#endif

	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_B/0/type", "TCP");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_B/0/network", "com/networks/mynetwork");
	CHECK(rc);
	/*Same as bhelvbuild2.verifone.com*/
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_B/0/address", "10.217.146.4");
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_B/0/port", 1200);
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_B/0/timeout", 60000);
	CHECK(rc);

	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_A_FALLBACK/0/type", "TCP");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_A_FALLBACK/0/network", "com/networks/mynetwork");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_A_FALLBACK/0/address", "vhqtest.verifone.com");
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_A_FALLBACK/0/port", 443);
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_A_FALLBACK/0/timeout", 60000);
	CHECK(rc);

	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_A_FALLBACK/1/type", "TCP");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_A_FALLBACK/1/network", "com/networks/mynetwork");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_A_FALLBACK/1/address", "vhqtest.verifone.com");
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_A_FALLBACK/1/port", 443);
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_A_FALLBACK/1/timeout", 60000);
	CHECK(rc);

	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_WLAN/0/type", "TCP");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_WLAN/0/network", "com/networks/mywlannetwork");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_WLAN/0/address", "192.168.1.2");
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_WLAN/0/port", 1200);
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_WLAN/0/timeout", 60000);
	CHECK(rc);


	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_GPRS/0/type", "TCP");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_GPRS/0/network", "com/networks/mynetwork_gprs");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_GPRS/0/address", "195.138.223.22");
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_GPRS/0/port", 1819);
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_GPRS/0/timeout", 60000);
	CHECK(rc);

	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_RAW_MODEM/0/type", "RAW_MODEM");
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_RAW_MODEM/0/timeout", 120000);
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_RAW_MODEM/0/device_name", "MDM_INT");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_RAW_MODEM/0/address", "2184917");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_RAW_MODEM/0/dial_type", "tone");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_RAW_MODEM/0/dial_mode", "async");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_RAW_MODEM/0/blind_dialing", "yes");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_RAW_MODEM/0/modulation", "v92");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_RAW_MODEM/0/compression", "none");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_RAW_MODEM/0/error_correction", "v42_mnp");
	CHECK(rc);


	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_PPPMODEM/0/type", "TCP");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_PPPMODEM/0/network", "com/networks/mynetwork_pppmodem");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_PPPMODEM/0/address", "172.16.0.1");
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_PPPMODEM/0/port", 1200);
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_PPPMODEM/0/timeout", 60000);
	CHECK(rc);

	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_RAW_SERIAL/0/type", "RAW_SERIAL");
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_RAW_SERIAL/0/timeout", 6000);
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_RAW_SERIAL/0/serial_port", "USBD");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_RAW_SERIAL/0/baudrate", "115200");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_RAW_SERIAL/0/parity", "none");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_RAW_SERIAL/0/databits", "8");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_RAW_SERIAL/0/stopbits", "1");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_RAW_SERIAL/0/flowcontrol", "none");
	CHECK(rc);

	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_SSL_SINGLE/0/type", "SSL");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_SSL_SINGLE/0/network", "com/networks/mynetwork");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_SSL_SINGLE/0/address", "10.217.146.4");
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_SSL_SINGLE/0/port", 1300);
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_SSL_SINGLE/0/ssl_prot_version", "TLS1_0");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_SSL_SINGLE/0/ssl_ca_cert_file", "ADKCOM_CA.pem");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_SSL_SINGLE/0/ssl_own_cert", "ADKCOM_Client.pem");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_SSL_SINGLE/0/ssl_ca_own_pkey", "ADKCOM_Client.key");
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_SSL_SINGLE/0/timeout", 60000);
	CHECK(rc);

	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_SSL_MUTUAL_PKCS12/0/type", "SSL");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_SSL_MUTUAL_PKCS12/0/network", "com/networks/mynetwork");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_SSL_MUTUAL_PKCS12/0/address", "10.217.146.4");
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_SSL_MUTUAL_PKCS12/0/port", 1400);
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_SSL_MUTUAL_PKCS12/0/ssl_prot_version", "TLS1_0");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_SSL_MUTUAL_PKCS12/0/ssl_ca_cert_file", "ADKCOM_CA.pem");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_SSL_MUTUAL_PKCS12/0/ssl_pkcs12_file", "ADKCOM_Client.p12");
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_SSL_MUTUAL_PKCS12/0/timeout", 60000);
	CHECK(rc);


	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_SSL_POLICY99/0/type", "SSL");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_SSL_POLICY99/0/network", "com/networks/mynetwork");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_SSL_POLICY99/0/address", "10.217.146.4");
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_SSL_POLICY99/0/port", 1500);
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_SSL_POLICY99/0/ssl_prot_version", "TLS1_2");
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_SSL_POLICY99/0/timeout", 60000);
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_SSL_POLICY99/0/ssl_policy", 99);
	CHECK(rc);


	/*******************************NETWORKS***************************/

	/*LAN*/
	rc = info_writeString(db, INFO_ROOT, "com/networks/mynetwork/type", "LAN");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mynetwork/device_name", "eth0");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mynetwork/startup_mode", "auto");
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/networks/mynetwork/timeout", 60000);
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/networks/mynetwork/dhcp_enabled", 1);
	CHECK(rc);

	/*WLAN*/
	rc = info_writeString(db, INFO_ROOT, "com/networks/mywlannetwork/type", "WLAN");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mywlannetwork/device_name", "wlan0");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mywlannetwork/startup_mode", "on-demand");
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/networks/mywlannetwork/timeout", 60000);
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/networks/mywlannetwork/dhcp_enabled", 1);
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mywlannetwork/wlan_node/0/ssid", "OpenWrt_WPA2");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mywlannetwork/wlan_node/0/auth_alg", "OPEN");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mywlannetwork/wlan_node/0/proto", "WPA2");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mywlannetwork/wlan_node/0/psk", "5a32664551672c27497a4f7d27");
	CHECK(rc);

	/*PPP-GPRS*/
	rc = info_writeString(db, INFO_ROOT, "com/networks/mynetwork_gprs/type", "GPRS");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mynetwork_gprs/device_name", "gprs0");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mynetwork_gprs/startup_mode", "on-demand");
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/networks/mynetwork_gprs/timeout", 120000);
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/networks/mynetwork_gprs/idle_timeout", 0);
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mynetwork_gprs/username", "tm");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mynetwork_gprs/password", "tm");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mynetwork_gprs/apn", "internet.t-mobile");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mynetwork_gprs/authentication", "pap");
	CHECK(rc);

	/*PPP-Modem*/
	rc = info_writeString(db, INFO_ROOT, "com/networks/mynetwork_pppmodem/type", "MODEM");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mynetwork_pppmodem/device_name", "MDM_INT");
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/networks/mynetwork_pppmodem/timeout", 120000);
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/networks/mynetwork_pppmodem/idle_timeout", 0);
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mynetwork_pppmodem/username", "adktest");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mynetwork_pppmodem/password", "adktest");
	CHECK(rc);
	// rc=info_writeString(db,INFO_ROOT,"com/networks/mynetwork_pppmodem/pabx_code", "8");
	//CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mynetwork_pppmodem/dial_number", "2184916");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mynetwork_pppmodem/dial_type", "tone");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mynetwork_pppmodem/dial_mode", "async");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mynetwork_pppmodem/authentication", "pap");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mynetwork_pppmodem/blind_dialing", "yes");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mynetwork_pppmodem/modulation", "v92");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mynetwork_pppmodem/compression", "none");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mynetwork_pppmodem/error_correction", "none");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mynetwork_pppmodem/startup_mode", "on-demand");
	CHECK(rc);

	info_closedb(db);
	LOGF_TRACE("Closed DB\n");
	return 0;
}

int create_cache_db(int port) {
	int rc;
	std::string infodb_file = "flash/infocache.db";

	mkdir("flash", 0755);
	InfoDB *db = info_opendb(infodb_file.c_str(), INFO_READ_WRITE);

	if (!db) {
		LOGF_ERROR("Cannot open database");
		return 1;
	}

	LOGF_TRACE("Opened DB\n");

	// subset 2 - fallback
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_A/1/type", "TCP");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_A/1/network", "com/networks/mynetwork");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/con/HOST_A/1/address", "vhqtest.verifone.com");
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_A/1/port", 443);
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/con/HOST_A/1/timeout", 60000);
	CHECK(rc);

	/*******************************NETWORKS***************************/

	/*LAN*/
	rc = info_writeString(db, INFO_ROOT, "com/networks/mynetwork/type", "LAN");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mynetwork/device_name", "eth0");
	CHECK(rc);
	rc = info_writeString(db, INFO_ROOT, "com/networks/mynetwork/startup_mode", "auto");
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/networks/mynetwork/timeout", 60000);
	CHECK(rc);
	rc = info_writeInt(db, INFO_ROOT, "com/networks/mynetwork/dhcp_enabled", 1);
	CHECK(rc);

	info_closedb(db);
	LOGF_TRACE("Closed DB\n");
	return 0;
}
#endif

int main(int argc, char *argv[])
{
	Fl_Multiline_Output *output= NULL;
	Fl_Window *win = new Fl_Window(320, 480, "SVC_TMS_TESTER");

	

	FL_NORMAL_SIZE = 16;
	{
		Fl_Tabs *tabs = new Fl_Tabs(10, 10, 320 - 10, 480 - 10);
		{
			// Aaa tab
			Fl_Group *aaa = new Fl_Group(40, 35, 320 - 20, 480 - 65, "Status");
			{
				Fl_Button *b1 = new Fl_Button(10, 60, 150, 30, "RequestHB"); b1->color(88 + 1);
				b1->callback((Fl_Callback*)RequestHB);
				b1->shortcut(FL_Enter);

				Fl_Button *b3 = new Fl_Button(10, 95, 150, 30, "ClearCache"); b3->color(88 + 1);
				b3->shortcut(FL_Escape);
				b3->callback((Fl_Callback*)ClearAgentCache);

				Fl_Button *b2 = new Fl_Button(10, 150, 150, 30, "View log"); b2->color(88 + 2);
//				output = new Fl_Multiline_Output(15, 230, 320-30, 480-260);
//				g_disp = output;
#ifdef _VOS2
				Fl_Button *b4 = new Fl_Button(10, 185, 150, 30, "Create ConnDB"); b4->color(88 + 2);
				b4->shortcut('9');
				b4->callback((Fl_Callback*)create_db);
#endif

				g_buff = new Fl_Text_Buffer();
				Fl_Text_Display *disp = new Fl_Text_Display(10, 230, 320 - 30, 480-260, "");
				g_disp = disp;
				disp->buffer(g_buff);
				g_buff->text("");
				//			Fl_Multiline_Output *disp = new Fl_Multiline_Output(20, 480 - 65 + 35, 320 - 20, 40);
				//			g_disp = disp;
				//			disp->value("");

			}
			aaa->end();

			// Bbb tab
			Fl_Group *bbb = new Fl_Group(40, 35, 320 - 20, 480 - 65, "Test");
			{
				Fl_Button *b1 = new Fl_Button(10, 60, 150, 75, "Button B1"); b1->color(88 + 1);
				Fl_Button *b2 = new Fl_Button(10, 150, 150, 75, "Button B2"); b2->color(88 + 3);
				Fl_Button *b3 = new Fl_Button(10, 240, 150, 75, "Button B3"); b3->color(88 + 5);
				Fl_Button *b4 = new Fl_Button(180, 60, 180, 75, "Button B4"); b4->color(88 + 2);
				Fl_Button *b5 = new Fl_Button(180, 150, 180, 75, "Button B5"); b5->color(88 + 4);
				Fl_Button *b6 = new Fl_Button(180, 240, 180, 75, "Button B6"); b6->color(88 + 6);
			}
			bbb->end();

		}
		tabs->end();
	}

	win->end();
	win->show();
	sleep(30);
	Register();
	return(Fl::run());
}

//	Make Enable / Disable Togglable
//	fp_button_background_normal
//	fp_button_background_pressed
//	fp_button_metric_image_set(app ->enableButton, "fp_button_background_pressed", "fp_button_background_pressed");
//	fp_button_metric_image_set(app ->disableButton, "fp_button_background_normal", "fp_button_background_pressed");
//	fp_button_metric_image_set(app ->enableButton, "fp_button_background_normal", "fp_button_background_pressed");
//	fp_button_metric_image_set(app ->disableButton, "fp_button_background_pressed", "fp_button_background_pressed");
