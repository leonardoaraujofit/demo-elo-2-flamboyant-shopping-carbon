#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <grp.h>
#include <libgen.h>
#include <stdarg.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <pwd.h>
#include <unistd.h>
#include "svc_tms.h"



#include <fancypants/Evas.h>
#include <fancypants/Ecore.h>
#include <fancypants/Ecore_Evas.h>

#include <fancypants/libfp/fp.h>
#include <fancypants/libfp/fp_theme.h>
#include <fancypants/libfp/fp_image.h>
#include <fancypants/libfp/fp_container.h>
#include <fancypants/libfp/fp_button.h>
#include <fancypants/libfp/fp_checkbox.h>
#include <fancypants/libfp/fp_text.h>
#include <fancypants/libfp/fp_list.h>
#include <fancypants/libfp/fp_multivalue.h>
#include <fancypants/libfp/fp_multiselect.h>
#include <fancypants/libfp/fp_scrollbar.h>

#include <fancypants/libfp/media/fp_media.h>
#include <fancypants/libfp/media/fp_media_enum.h>

#include "vf_sig_capture.h"
#include "vf_textbox.h"
#include "vf_dialog.h"


#define CONFIG_BUTTON_IS_GET_CONFIG		1

typedef unsigned int uint32;


/* The Application GUI */
struct appdata
{
	struct fp *fp;
	Evas_Object *bg;
	Evas_Object *titleText;
	Evas_Object *textRect;
	Evas_Object *labelText1;
	Evas_Object *labelText2;
	Evas_Object *horizLine1;
	Evas_Object *horizLine2;
	Evas_Object *vertLine;	
	Evas_Object *appStatus;
	Evas_Object *appStatus2;

	Evas_Object *acceptButton;
	Evas_Object *rejectButton;
	Evas_Object *viewlogButton;
	Evas_Object *contactServerButton;
	Evas_Object *getConfigButton;

	Evas_Object *logDeleteButton;
	Evas_Object *logExitButton;

	Evas_Object *logContainer;
};

struct appdata *app;


void CreateGradient(struct appdata *app);
void CreateControls(struct appdata *app);
void CreateText(struct appdata *app, Evas_Object **pObj, const char *pFontFace, int iSize, const char *pText, int iX, int iY, int iRed, int iGreen, int iBlue);
void CreateRectangle(struct appdata *app, Evas_Object **pObj, int iY, int iH, int iRed, int iGreen, int iBlue, int iAlpha);
void CreateLine(struct appdata *app, Evas_Object **pObj, int x1, int y1, int x2, int y2, int iRed, int iGreen, int iBlue);
void CreateButton(struct appdata *app, Evas_Object **pObj, const char *pText, int iX, int iY, int iWidth, int iHeight, fp_button_clicked_cb cb);
void Clicked_AcceptButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event);
void Clicked_RejectButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event);
void Clicked_ViewLogButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event);
void Clicked_ContactServerButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event);
void Clicked_GetConfigButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event);
void Clicked_DeleteButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event);
void Clicked_ExitButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event);


char *readFile(char *fn, int mode);

#define PAD_SPACE 	3
#define ALIGN_LEFT		-1000
#define ALIGN_CENTER 	-2000
#define ALIGN_RIGHT 	-3000


#ifndef boolean
#define boolean unsigned char
#define TRUE 1
#define FALSE 0
#endif



/* Allow 20 Kb for the log file */
#define SVC_TMS_TESTER_LOG_FILE						"./logs/svc_tms_tester_log"
#define SVC_TMS_TESTER_LOG_FILE_SIZE_LIMIT			(20 * 1024)


boolean bVerbose = TRUE;


typedef enum
{
	VERIFY_READ_PERMISSION,
	VERIFY_WRITE_PERMISSION,
	VERIFY_EXECUTE_PERMISSION,
	VERIFY_RW_PERMISSION,
	VERIFY_RWX_PERMISSION,
}verify_permission_t;




int LogMsgEx(const char *fname, const char *string, va_list ap)
{
	int iFile;
	int r = 0;
	struct stat log_stat;
	struct timeval cur_time;
	struct tm local;
	time_t t;

	gettimeofday(&cur_time, NULL);
	t = cur_time.tv_sec;

	while ((*string == '\n') || (*string == '\r'))
	{
		printf("%c", *string);
		string++;
	}

	if (localtime_r(&t, &local))
		printf("%02d:%02d:%02d: ", local.tm_hour, local.tm_min, local.tm_sec);
	else
		printf("%s: errno set in localtime_r() = %d\n", __FUNCTION__, errno);

	r = vprintf (string, ap);


	/* If the logs are too big, rename them to svc_tms_tester_log1, svc_tms_tester_log2*/
	if (stat(fname, &log_stat) == 0)
	{
		if (log_stat.st_size > SVC_TMS_TESTER_LOG_FILE_SIZE_LIMIT)
		{
			char szName1[256], szName2[256];
			sprintf(szName1, "%s.1", fname);
			sprintf(szName2, "%s.2", fname);
			if (rename(szName1, szName2) != 0)
				printf("%s: errno set in renaming file (%s.1) = %d\n", __FUNCTION__, fname, errno);
			if (rename(SVC_TMS_TESTER_LOG_FILE, szName1) != 0)
				printf("%s: errno set in renaming file (%s) = %d\n", __FUNCTION__, SVC_TMS_TESTER_LOG_FILE, errno);
		}
	}



#ifndef WIN32
	iFile= open(fname, O_CREAT | O_APPEND | O_RDWR, S_IRWXU);
#else
	iFile= open(fname, O_CREAT | O_APPEND | O_RDWR, _S_IREAD | _S_IWRITE);
#endif
	if (iFile!=-1)
	{
		char* szMsg;
		char* szMsgWithTime;


		szMsg = malloc(25 * 1024);
		szMsgWithTime = malloc((25 * 1024) + 50);

		r = 0;
		if (szMsg)
		{
			r = vsprintf (szMsg, string, ap);
		}

		if (szMsgWithTime && (r>0))
		{
			sprintf(szMsgWithTime, "%d/%d/%d %02d:%02d:%02d: %s", local.tm_mon+1, local.tm_mday, (local.tm_year % 100), local.tm_hour, local.tm_min, local.tm_sec, szMsg);
			write(iFile, szMsgWithTime, strlen(szMsgWithTime));
		}

		if (szMsg)
		{
			//DebugMsg("%s: Freeing szMsg @ 0x%08x\n", __FUNCTION__, szMsg);
			free(szMsg);
			szMsg = NULL;
		}
		if (szMsgWithTime)
		{
			//DebugMsg("%s: Freeing szMsgWithTime @ 0x%08x\n", __FUNCTION__, szMsgWithTime);
			free(szMsgWithTime);
			szMsgWithTime = NULL;
		}

		close(iFile);

		if (chmod(fname, 0666 ) != 0)
			printf("%s: errno set in chmod() = %d\n", __FUNCTION__, errno);
	}


	return r;
}

int LogMsg(const char *string, ...)
{
	int ret_val;

	va_list ap;
	va_start (ap, string);
	ret_val = LogMsgEx(SVC_TMS_TESTER_LOG_FILE, string, ap);
	va_end(ap);

	return ret_val;
}



///////////////////////////////////////////////////////////////////////////////
///
///  \fn       void VerifyFilePermissions()
///  \brief    Verifies the agent has permissions to the file in question
///
///  Returns TRUE if the permissions seem correct, FALSE if not
////////////////////////////////////////////////////////////////////////////////
boolean VerifyFilePermissions(char* pszConfigFile, verify_permission_t permission_type)
{
	uid_t my_uid = getuid();
	int num_groups;
	gid_t my_gids[64];
	struct stat file_stat;
	char permissions_str[16];

	/* Make sure the file exists */
	if (stat(pszConfigFile, &file_stat) != 0)
	{
		LogMsg("%s: %s does not exist - returning FALSE\n", __FUNCTION__, pszConfigFile);
		return FALSE;
	}


	memset(permissions_str, 0, sizeof(permissions_str));

	/* Setup directory */
	if (S_ISDIR(file_stat.st_mode))
		strcpy(permissions_str, "d");
	else
		strcpy(permissions_str, "-");

	/* Setup owner permissions */
	if (file_stat.st_mode & S_IRUSR)
		strcat(permissions_str, "r");
	else
		strcat(permissions_str, "-");
	if (file_stat.st_mode & S_IWUSR)
		strcat(permissions_str, "w");
	else
		strcat(permissions_str, "-");
	if (file_stat.st_mode & S_IXUSR)
		strcat(permissions_str, "x");
	else
		strcat(permissions_str, "-");


	/* Setup group permissions */
	if (file_stat.st_mode & S_IRGRP)
		strcat(permissions_str, "r");
	else
		strcat(permissions_str, "-");
	if (file_stat.st_mode & S_IWGRP)
		strcat(permissions_str, "w");
	else
		strcat(permissions_str, "-");
	if (file_stat.st_mode & S_IXGRP)
		strcat(permissions_str, "x");
	else
		strcat(permissions_str, "-");


	/* Setup other permissions */
	if (file_stat.st_mode & S_IROTH)
		strcat(permissions_str, "r");
	else
		strcat(permissions_str, "-");
	if (file_stat.st_mode & S_IWOTH)
		strcat(permissions_str, "w");
	else
		strcat(permissions_str, "-");
	if (file_stat.st_mode & S_IXOTH)
		strcat(permissions_str, "x");
	else
		strcat(permissions_str, "-");

	LogMsg("%s: permissions of %s - %s\n", __FUNCTION__, pszConfigFile, permissions_str);


	if (permission_type == VERIFY_READ_PERMISSION)
	{
		if (file_stat.st_mode & S_IROTH)
		{
			LogMsg("%s: we have 'other' read priveleges - returning TRUE\n", __FUNCTION__);
			return TRUE;
		}

		if (file_stat.st_mode & S_IRGRP)
		{
			int i;
			num_groups = getgroups(64, my_gids);

			for (i = 0; i < num_groups; i++)
			{
				if (my_gids[i] == file_stat.st_gid)
				{
					LogMsg("%s: we have group read priveleges as gid %d - returning TRUE\n", __FUNCTION__, my_gids[i]);
					return TRUE;
				}
				LogMsg("%s: no group read priveleges in gid %d (file gid = %d) - returning TRUE\n", __FUNCTION__, my_gids[i], file_stat.st_gid);
			}
		}
		
		if ((my_uid == file_stat.st_uid) && (file_stat.st_mode & S_IRUSR))
		{
			LogMsg("%s: we have owner read priveleges - returning TRUE\n", __FUNCTION__);
			return TRUE;
		}

		LogMsg("%s: no read priveleges - setting up permission string\n", __FUNCTION__);
	}
	else if (permission_type == VERIFY_WRITE_PERMISSION)
	{
		if (file_stat.st_mode & S_IWOTH)
		{
			LogMsg("%s: we have 'other' write priveleges - returning TRUE\n", __FUNCTION__);
			return TRUE;
		}

		if (file_stat.st_mode & S_IWGRP)
		{
			int i;
			num_groups = getgroups(64, my_gids);

			for (i = 0; i < num_groups; i++)
			{
				if (my_gids[i] == file_stat.st_gid)
				{
					LogMsg("%s: we have group write priveleges as gid %d - returning TRUE\n", __FUNCTION__, my_gids[i]);
					return TRUE;
				}
				LogMsg("%s: no group write priveleges in gid %d (file gid = %d)\n", __FUNCTION__, my_gids[i], file_stat.st_gid);
			}
		}

		if ((my_uid == file_stat.st_uid) && (file_stat.st_mode & S_IWUSR))
		{
			LogMsg("%s: we have owner write priveleges - returning TRUE\n", __FUNCTION__);
			return TRUE;
		}

		LogMsg("%s: no write priveleges - setting up permission string\n", __FUNCTION__);
	}
	else
		LogMsg("%s: permission type %d not supported yet\n", __FUNCTION__, permission_type);


	LogMsg("%s: Invalid permissions on %s - %s (Need read priveleges)\n", __FUNCTION__, pszConfigFile, permissions_str);
	return FALSE;
}





static int CopyFile(const char *pszSrc, const char *pszDest)
{
	FILE *fpSrc, *fpDst;
	unsigned char buf[8*1024];
	int n_read;

	LogMsg("Copyfile: copying %s to %s\r\n", pszSrc, pszDest);
	fpSrc= fopen(pszSrc, "r");
	fpDst= fopen(pszDest, "w");

	// return an error if we can't open source and dest files
	if ((fpSrc == NULL) || (fpDst == NULL))
	{
		if (fpSrc == NULL)
			LogMsg("%s: could not open souce %s\n", __FUNCTION__, pszSrc);
		else
			fclose(fpSrc);

		if (fpDst == NULL)
			LogMsg("%s: could not open dest %s\n", __FUNCTION__, pszDest);
		else
			fclose(fpDst);

		return -1;
	}
	
	LogMsg("Copyfile: files opened, src: %lx dst:%lx\r\n", (long)fpSrc, (long)fpDst);
	if (fpSrc!=NULL && fpDst!=NULL)
	{
		while ((n_read = fread(buf, sizeof(char), sizeof(buf), fpSrc)) > 0) 
		{
			if (n_read < sizeof(buf) && ferror(fpSrc))
				break;
			if ((fwrite(buf, n_read, 1, fpDst) < 1) && ferror(fpDst)) {
//				g_warning(_("writing to %s failed.\n"), dest);
				fclose(fpDst);
				fclose(fpSrc);
				unlink(pszDest);
				return -1;
			}
		}
	}

	LogMsg("Copyfile: closing files\r\n");
	if (fpSrc) fclose(fpSrc);
	if (fpDst) fclose(fpDst);
	LogMsg("Copyfile: done\r\n");
	return 0;
}






static char* status_to_string(int status)
{
	switch (status)
	{
	case TMS_STATUS_SUCCESS:
		return "Success";
	case TMS_STATUS_ERROR:
		return "Error";
	case TMS_STATUS_UNSUPPORTED_FEATURE:
		return "Unsupported Feature";
	case TMS_STATUS_CONTENT_AVAILABLE:
		return "Content Avaliable";
	case TMS_STATUS_NO_CONSUMER:
		return "No Consumer";
	case TMS_STATUS_FILENAME_ERROR:
		return "Filename ERROR";
	case TMS_STATUS_CONTENT_FAILURE:
		return "Status Content Failure";

	case TMS_STATUS_MSGQ_FAILURE:
		return "Message Queue FAILURE";
	case TMS_STATUS_MSGSND_FAILURE:
		return "Message Send FAILURE";
	case TMS_STATUS_MSGRCV_FAILURE:
		return "Message Receive FAILURE";
	case TMS_STATUS_APP_EVENT_AVAIL:
		return "App Event Available";

	case TMS_STATUS_REQUESTED:
		return "Requested";

	case TMS_STATUS_REGISTER_FAIL:
		return "Register Fail";
	case TMS_STATUS_REGISTER_NAME_TOO_LONG:
		return "Register Name Too Long";
	case TMS_STATUS_EINVAL:
		return "Error in Value";
	case TMS_STATUS_AGENT_NOT_RUNNING:
		return "Agent Not Running";

	case TMS_STATUS_SERVER_INSTANCE_ERROR:
		return "Srv Inst Error";
	case TMS_STATUS_SERVER_INSTANCE_LOCK_ERROR:
		return "Srv Inst Lock Error";
	case TMS_STATUS_SERVER_INSTANCE_LOCK_TIMEOUT_ERROR:
		return "Srv Inst Lock Timeout Error";
	case TMS_STATUS_SERVER_INSTANCE_ALREADY_LOCKED:
		return "Srv Inst Already Locked";
	case TMS_STATUS_SERVER_INSTANCE_MAX_LOCK_REQ_EXCEEDED:
		return "Too Many SrvInst Lock Requests";
	case TMS_STATUS_SERVER_INSTANCE_UNLOCK_ERROR:
		return "Srv Inst Unlock Error";
	case TMS_STATUS_SERVER_INSTANCE_CONFIG_LOAD_ERROR:
		return "Srv Inst Config Load Error";

	case TMS_STATUS_AGENT_BUSY_ERROR:
		return "Agent Busy Error";
	case TMS_STATUS_POSTPONED:
		return (char*) "Postponed";
	case TMS_STATUS_CANCELLED:
		return (char*) "Cancelled";
	case TMS_STATUS_AGENT_CONTACTSRV_ERROR:
		return "Agent not registered on the server yet";
	case TMS_STATUS_API_SYNC_ERROR:
		return "Lost SYNC of TMS exchange";

	default:
		return "Unknown";
	}

	return "Unknown";
}

static char* msgType_to_string(int msgType)
{
	switch (msgType)
	{
	case TMS_EVT_CALL_SERVER_RESPONSE:
		return "Call Server";
	case TMS_EVT_SET_APP_STATE:
		return "Get APP State";
	case TMS_EVT_GET_TMS_CONFIG_RESPONSE:
		return "Get TMS Config";
	case TMS_EVT_SET_TMS_CONFIG_RESPONSE:
		return "Set TMS Config";
	case TMS_EVT_SET_APP_INFO:
		return "Get APP Info";
	case TMS_EVT_SET_PARM_LIST:
		return "Get Parameter List";
	case TMS_EVT_GET_FILE:
		return "Get File";
	case TMS_EVT_PUT_FILE:
		return "Put File";
	case TMS_EVT_DEL_FILE:
		return "Delete File";
	case TMS_EVT_NOTIFICATION:
		return "Event Notfication";
	case TMS_EVT_DO_TRANSACTION:
		return "Do Transaction";
	case TMS_EVT_REGISTER_APP_RESPONSE:
		return "Register";
	case TMS_EVT_UNREGISTER_APP_RESPONSE:
		return "Un-Register";

	case TMS_EVT_GET_SERVER_INSTANCE:
		return "Get Srv Inst";
	case TMS_EVT_LOCK_SERVER_INSTANCE:
		return "Lock Srv Inst";
	case TMS_EVT_RELEASE_SERVER_INSTANCE:
		return "Release Srv Inst";

	case TMS_EVT_APP_ALERT_RESULT:
		return "App Event Result";

	case TMS_EVT_CLEAR_APP_INFO_RESULT:
		return "Clear App Info Result";

	case TMS_EVT_API_ERRORS:
		return "API Usage Error";

	default:
		return "Unknown";
	}

	return "Unknown";
}


#define DELETE_BUTTONS_IN_CB	0



struct tmsEventData* appEvent;
boolean waiting_for_usr_response = FALSE;



#if 0

// Simple exe to call tms service to deliver a test content update to an app
// Makes interface from WDE PHP simple.
// Note this will setup the content registration and wait for content
void* MainLoopContent(void* pvData)
{
	struct tmsRegisterReturn* regRtn;
	struct passwd *psPasswd;
	char contentEventKey[32];

	printf("%s: MainLoopContent started\n", __FUNCTION__);
	sleep(5);

	psPasswd = getpwuid(getuid());
	
	LogMsg("svc_tms_tester: Registering for content as %s\n", psPasswd->pw_name);
	regRtn = tms_registerForContent();
	if (regRtn && (regRtn->status == TMS_STATUS_SUCCESS))
	{

		memcpy(contentEventKey, regRtn->eventkey, 32);
		LogMsg("svc_tms_tester: Registered for content - Event Key = %s - freeing regRtn\n", contentEventKey);

		free(regRtn);
		regRtn = NULL;

		while (1)
		{

			if (app->appStatus2)
			{
				evas_object_hide(app->appStatus2);
				evas_object_del(app->appStatus2);
				app->appStatus2 = NULL;
			}
			evas_object_text_text_set(app ->appStatus, "Waiting For Content");
			evas_render(app->fp->e);

			LogMsg("svc_tms_tester: Waiting for content on Key %s\n", contentEventKey);
			contentEvent = tms_getContentEvent(contentEventKey, 0);

			if (contentEvent)
			{
				if (app->appStatus2)
				{
					evas_object_hide(app->appStatus2);
					evas_object_del(app->appStatus2);
					app->appStatus2 = NULL;
				}
				evas_object_text_text_set(app ->appStatus, "Received Content Update - Accept or Reject");

				LogMsg("svc_tms_tester: Got a content event\n\tStatus = %s\n\tType = %d\n\tContent ID = %d\n\tFilename = %s\n\tfilepath = %s\n",
					status_to_string(contentEvent->status), contentEvent->contentType, contentEvent->contentID, contentEvent->filename, contentEvent->filepath);

				CreateButton(app, &app ->acceptButton, "Accept Content", (PAD_SPACE * 2), 185, 140, 40, Clicked_AcceptButtonCB);
				CreateButton(app, &app ->rejectButton, "Reject Content", 2 * (PAD_SPACE * 2) + 140, 185, 140, 40, Clicked_RejectButtonCB);

				evas_render(app->fp->e);

				waiting_for_usr_response = TRUE;

				while(waiting_for_usr_response)
				{
					printf("%s: Waiting for user response\n", __FUNCTION__);
					sleep(3);
				}


#if (!DELETE_BUTTONS_IN_CB)
				printf("%s: Got user response - deleting buttons\n", __FUNCTION__);

				/* We can remove the accept button */
				if (app->acceptButton)
				{
					evas_object_hide(app->acceptButton);
					evas_object_del(app->acceptButton);
					app->acceptButton = NULL;
				}

				/* We can remove the reject button */
				if (app->rejectButton)
				{
					evas_object_hide(app->rejectButton);
					evas_object_del(app->rejectButton);
					app->rejectButton = NULL;
				}
#else
				printf("%s: Got user response\n", __FUNCTION__);
#endif
			}
		}

	}
	else
	{
		LogMsg("svc_tms_tester ERROR: tms_registerForContent() returned an error - regRtn = 0x%08x, regRtn->status = %s, errno = %d\n",
			(int)regRtn, regRtn ? status_to_string(regRtn->status) : "NULL", errno);
	}

	if (regRtn)
		free(regRtn);

	return 0;

}

#endif


#define APP_PARM_DATA_FILE					"/tmp/share/AppParmFile.dat"
#define APP_PARM_DATA_FILE_TEST_DATA		"test.ini SHA-1 1234567890123456789012345678901234567890"


struct tmsAppParameter custom_parameters[] =
{
	{"StoreId", "Store8", TMS_PARAMETER_TYPE_IDENTIFIER},
	{"LaneId", "Lane1", TMS_PARAMETER_TYPE_IDENTIFIER},
	{"DevProfileTestData_U2", "TRUE", TMS_PARAMETER_TYPE_DEVICE_PROFILE},
	{"DiagProfileTestData_U2", "FALSE", TMS_PARAMETER_TYPE_DIAGNOSTIC},
};

void createParamList(struct tmsAppInfo *appInfo, uint32 param_mask)
{
	int i, entry_index;
	int num_params = 0;
	struct tmsAppParameter* parameter_array = NULL;

	/* Find out how many parameters to send */
	for (i = 0; i < (sizeof(custom_parameters) / sizeof(struct tmsAppParameter)); i++)
	{
		if (custom_parameters[i].parameterType & param_mask)
		{
			num_params++;
			LogMsg("%s: custom_parameter[%d] (type = %d) matches mask 0x%08x - num_params incremented to %d\n", __FUNCTION__, i, custom_parameters[i].parameterType, param_mask, num_params);
		}
	}

	LogMsg("%s: Allocating %d bytes for %d parameters\n", __FUNCTION__, num_params * sizeof(struct tmsAppParameter), num_params);
	parameter_array = malloc(num_params * sizeof(struct tmsAppParameter));
	if (parameter_array)
	{
		memset(parameter_array, 0, (num_params * sizeof(struct tmsAppParameter)));
		for (i = 0, entry_index = 0; ((i < (sizeof(custom_parameters) / sizeof(struct tmsAppParameter))) && (entry_index < num_params)); i++)
		{
			if (custom_parameters[i].parameterType & param_mask)
			{
				LogMsg("%s: custom_parameter[%d] (type = %d) matches mask 0x%08x - adding to index %d\n", __FUNCTION__, i, custom_parameters[i].parameterType, param_mask, entry_index);
				memcpy(&parameter_array[entry_index], &custom_parameters[i], sizeof(struct tmsAppParameter));
				entry_index++;
			}
		}
		appInfo->parameterCount = num_params;
	}
	else
		appInfo->parameterCount = 0;
	
	appInfo->parameterList = parameter_array;
}


static int HandleApplicationEvent(struct tmsEventData eventData)
{
	int param_count;
	int ret_val, result;
	FILE *fd;
	char* pszBaseName;
	char disp_str[256];
	char param_file_copy_name[MAX_TMS_FILENAME_LENGTH];
	struct tmsAppInfo appInfo;


	appEvent = &eventData;

#if 1

	if (app->appStatus)
	{
		evas_object_hide(app->appStatus);
		evas_object_del(app->appStatus);
		app->appStatus = NULL;
	}
	if (app->appStatus2)
	{
		evas_object_hide(app->appStatus2);
		evas_object_del(app->appStatus2);
		app->appStatus2 = NULL;
	}
	memset(disp_str, 0, sizeof(disp_str));
	printf("%s: Received App Message - %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
	sprintf(disp_str, "Received App Message - %s", msgType_to_string(appEvent->evtType));
	CreateText(app, &app->appStatus, "Vera", 20, disp_str, ALIGN_CENTER, (PAD_SPACE + 55), 255, 192, 192);

	LogMsg("%s: Got an application event\n\tStatus = %s\n\tType = %s\n\tMessage ID = %d\n\tFilename = %s\n\tFileLocation = %s\n\tEvent Mask = 0x%08x\n\tTransaction Type = %s\n",
		__FUNCTION__, status_to_string(appEvent->status), msgType_to_string(appEvent->evtType), appEvent->handle, appEvent->filename,
		appEvent->filepath, appEvent->eventMask, appEvent->transactionType);

	evas_render(app->fp->e);
#endif



	

	if (app->appStatus2)
	{
		evas_object_hide(app->appStatus2);
		evas_object_del(app->appStatus2);
		app->appStatus2 = NULL;
	}

	switch (appEvent->evtType)
	{
	case TMS_EVT_CALL_SERVER_RESPONSE:
		memset(disp_str, 0, sizeof(disp_str));
		if (appEvent->status == TMS_STATUS_SUCCESS)
		{
			sleep(2);
			sprintf(disp_str, "Call Server SUCCESSFUL");
			CreateText(app, &app ->appStatus, "Vera", 20, disp_str, ALIGN_CENTER, (PAD_SPACE + 55), 255, 192, 192);


			if (strlen(appEvent->filename))
			{

				/* Probably want to register for notifications on this file with inotify since the file will be updated
				a couple of times during a heartbeat. */
				unsigned char resFileFound = FALSE;
				int retries = 20;
				char* tptr = NULL;

				while(retries)
				{
					struct stat resFileStat;

					if (stat(appEvent->filename, &resFileStat) == 0)
					{
						LogMsg("%s: Result file found\n", __FUNCTION__);
						resFileFound = TRUE;
						break;
					}
					sleep(3);
					printf("%s: waiting for heartbeat result file\n", __FUNCTION__);
				}

				if (resFileFound)
					tptr=readFile(appEvent->filename,0);
				if (tptr)
				{
					sprintf(disp_str, "Result=%s", tptr);
					free(tptr);
				}
				else
				{
					sprintf(disp_str, "Error reading result file");
				}
			}
			else
			{
				sprintf(disp_str, "No result file to read");
			}

			CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		}
		else
		{
			sprintf(disp_str, "Call Server FAILED");
			CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		}

		printf("%s: adding contact server button back\n", __FUNCTION__);
		if (app->contactServerButton)
		{
			evas_object_show(app->contactServerButton);
		}
		break;

	case TMS_EVT_SET_APP_STATE:
		LogMsg("%s: Handling %s - eventMask = 0x%08x\n", __FUNCTION__, msgType_to_string(appEvent->evtType), appEvent->eventMask);
		/* Call service to set app state */

		LogMsg("%s: Calling tms_setApplicationState(%d, APP_STATUS_BUSY)\n", __FUNCTION__, appEvent->handle);
		ret_val = tms_setApplicationState(appEvent->handle, APP_STATUS_BUSY);
		LogMsg("%s: tms_setApplicationState() returned %s - sleeping for 2 seconds\n", __FUNCTION__, status_to_string(ret_val));

		sleep(2);


		LogMsg("%s: Calling tms_setApplicationState(%d, APP_STATUS_FREE)\n", __FUNCTION__, appEvent->handle);
		ret_val = tms_setApplicationState(appEvent->handle, APP_STATUS_FREE);

		memset(disp_str, 0, sizeof(disp_str));
		sprintf(disp_str, "tms_setApplicationState() returned %s", status_to_string(ret_val));
		CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		break;

	case TMS_EVT_GET_TMS_CONFIG_RESPONSE:
		memset(disp_str, 0, sizeof(disp_str));
		if (appEvent->status == TMS_STATUS_SUCCESS)
		{
			sleep(2);
			sprintf(disp_str, "Get Config Location SUCCESSFUL");
			CreateText(app, &app ->appStatus, "Vera", 20, disp_str, ALIGN_CENTER, (PAD_SPACE + 55), 255, 192, 192);


			if (strlen(appEvent->filename))
			{
				sprintf(disp_str, "Config file located at %s", appEvent->filename);
			}
			else
			{
				sprintf(disp_str, "No Config File Location Returned");
			}

			CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		}
		else
		{
			sprintf(disp_str, "Get Config Location FAILED");
			CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		}

		printf("%s: adding get config button back\n", __FUNCTION__);
		if (app->getConfigButton)
		{
			evas_object_show(app->getConfigButton);
		}
		break;

	case TMS_EVT_SET_TMS_CONFIG_RESPONSE:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		/* Call service to set app state */

		sprintf(disp_str, "Config Update returned %s", status_to_string(appEvent->status));
		CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);

		printf("%s: adding set config button back\n", __FUNCTION__);
		if (app->getConfigButton)
		{
			evas_object_show(app->getConfigButton);
		}
		break;

	case TMS_EVT_SET_APP_INFO:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		/* Call service to set app state */

		memset(&appInfo, 0, sizeof(struct tmsAppInfo));

		createParamList(&appInfo, appEvent->eventMask);

		LogMsg("%s: Calling tms_setApplicationInfo(%d) - appInfo.parameterCount = %d, appInfo.parameterList = 0x%08x\n", __FUNCTION__, appEvent->handle,
			appInfo.parameterCount, appInfo.parameterList);
		for (param_count = 0; param_count < appInfo.parameterCount; param_count++)
		{
			LogMsg("\tParameter %d: name = %s, value = %s, type = %d\n", param_count, appInfo.parameterList[param_count].parameterName,
				appInfo.parameterList[param_count].parameterValue, appInfo.parameterList[param_count].parameterType);
		}

		ret_val = tms_setApplicationInfo(appEvent->handle, appInfo);

		free(appInfo.parameterList);

		memset(disp_str, 0, sizeof(disp_str));
		sprintf(disp_str, "tms_setApplicationParameterList() returned %s", status_to_string(ret_val));
		CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		break;

	case TMS_EVT_SET_PARM_LIST:

		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		/* Call service to set parameter list */
		fd = fopen(APP_PARM_DATA_FILE, "w");

		if (fd)
		{
			fwrite(APP_PARM_DATA_FILE_TEST_DATA, strlen(APP_PARM_DATA_FILE_TEST_DATA), 1, fd);
			fclose(fd);
		}
		else
			LogMsg("%s: errno set in opening file (%s) = %d\n", __FUNCTION__, APP_PARM_DATA_FILE, errno);

		LogMsg("%s: Calling tms_setApplicationParameterList(%d, 1, %s)\n", __FUNCTION__, appEvent->handle, APP_PARM_DATA_FILE);
		ret_val = tms_setApplicationParameterList(appEvent->handle, 1, APP_PARM_DATA_FILE);

		memset(disp_str, 0, sizeof(disp_str));
		sprintf(disp_str, "tms_setApplicationParameterList() returned %s", status_to_string(ret_val));
		CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		break;

	case TMS_EVT_GET_FILE:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		memset(param_file_copy_name, 0, sizeof(param_file_copy_name));
		/* Make sure filename is valid */
		if (strcmp(appEvent->filename, "test.ini") == 0)
		{
			int copy_result;
			int chmod_ret, chown_ret;
			char groupname[16];
			struct group *psGroup;
			struct passwd *psPasswd;


			psPasswd = getpwuid(getuid());

			sprintf(param_file_copy_name, "/tmp/share/test.ini");
			LogMsg("%s: Copying %s to %s\n", __FUNCTION__, appEvent->filename, param_file_copy_name);
			copy_result = CopyFile("./test.ini", param_file_copy_name);
			LogMsg("%s: Copy result = %d\n", __FUNCTION__, copy_result);

			if (psPasswd)
				sprintf(groupname, "%ssys", psPasswd->pw_name);
			else
				sprintf(groupname, "sys");
			psGroup = getgrnam(groupname);

			if (psGroup)
			{
				chmod_ret = chmod(param_file_copy_name, 0640 );
				chown_ret = chown(param_file_copy_name, getuid(), psGroup->gr_gid);
				LogMsg("%s: chmod to 640 returned (%d) - chown to %s group returned %d\n", __FUNCTION__, chmod_ret, groupname, chown_ret);
			}
			else
				chmod_ret = chmod(param_file_copy_name, 0644);

			if (copy_result == 0)
				result = TMS_STATUS_SUCCESS;
			else
			{
				sprintf(disp_str, "Error %d copying test.ini to %s", copy_result, param_file_copy_name);
				result = TMS_STATUS_ERROR;
			}
		}
		else
		{
			sprintf(disp_str, "Invalid Parameter File to Get-%s", appEvent->filename);
			LogMsg("%s: Invalid parameter filename - %s\n", __FUNCTION__, appEvent->filename);
			result = TMS_STATUS_FILENAME_ERROR;
		}


		LogMsg("%s: Calling tms_getApplicationFileAvailable(%d, %s, %s, TRUE)\n", __FUNCTION__, appEvent->handle, status_to_string(result), param_file_copy_name);
		ret_val = tms_getApplicationFileAvailable(appEvent->handle, result, param_file_copy_name, TRUE);

		memset(disp_str, 0, sizeof(disp_str));
		sprintf(disp_str, "tms_getApplicationFileAvailable() returned %s", status_to_string(ret_val));

		CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		break;

	case TMS_EVT_PUT_FILE:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		memset(param_file_copy_name, 0, sizeof(param_file_copy_name));
		strcpy(param_file_copy_name, appEvent->filename);
		pszBaseName = basename(param_file_copy_name);

		/* Make sure filename is valid */
		if (strcmp(pszBaseName, "test.ini") == 0)
		{
			int copy_result;

			LogMsg("%s: Copying %s to ./test.ini\n", __FUNCTION__, appEvent->filename);
			copy_result = CopyFile(appEvent->filename, "./flash/test.ini");
			LogMsg("%s: Copy result = %d\n", __FUNCTION__, copy_result);

			if (copy_result == 0)
				result = TMS_STATUS_SUCCESS;
			else
			{
				sprintf(disp_str, "Error %d copying %s to ./flash/test.ini", copy_result, appEvent->filename);
				result = TMS_STATUS_ERROR;
			}

			LogMsg("%s: Calling tms_setFileOperationResult(%d, %s, TMS_EVT_PUT_FILE)\n", __FUNCTION__, appEvent->handle, status_to_string(result));
			ret_val = tms_setFileOperationResultWithDescription(appEvent->handle, result, TMS_EVT_PUT_FILE, NULL);

			memset(disp_str, 0, sizeof(disp_str));
			if (result == TMS_STATUS_SUCCESS)
				sprintf(disp_str, "tms_setFileOperationResult() returned %s", status_to_string(ret_val));
			else
				CreateText(app, &app->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		}
		else
		{
			if (app->appStatus2)
			{
				evas_object_hide(app->appStatus2);
				evas_object_del(app->appStatus2);
				app->appStatus2 = NULL;
			}
			evas_object_text_text_set(app->appStatus, "Received Content Update - Accept or Reject");

			LogMsg("svc_tms_tester: Got a content event\n\tStatus = %s\n\tType = %d\n\tContent ID = %d\n\tFilename = %s\n\tfilepath = %s\n",
				status_to_string(appEvent->status), appEvent->putFileType, appEvent->handle, appEvent->filename, appEvent->filepath);

			CreateButton(app, &app->acceptButton, "Accept Content", (PAD_SPACE * 2), 185, 140, 40, Clicked_AcceptButtonCB);
			CreateButton(app, &app->rejectButton, "Reject Content", 2 * (PAD_SPACE * 2) + 140, 185, 140, 40, Clicked_RejectButtonCB);

			evas_render(app->fp->e);

			waiting_for_usr_response = TRUE;

			while (waiting_for_usr_response)
			{
				printf("%s: Waiting for user response\n", __FUNCTION__);
				sleep(3);
			}


#if (!DELETE_BUTTONS_IN_CB)
			printf("%s: Got user response - deleting buttons\n", __FUNCTION__);

			/* We can remove the accept button */
			if (app->acceptButton)
			{
				evas_object_hide(app->acceptButton);
				evas_object_del(app->acceptButton);
				app->acceptButton = NULL;
			}

			/* We can remove the reject button */
			if (app->rejectButton)
			{
				evas_object_hide(app->rejectButton);
				evas_object_del(app->rejectButton);
				app->rejectButton = NULL;
			}
#else
			printf("%s: Got user response\n", __FUNCTION__);
#endif
		}

		break;

	case TMS_EVT_DEL_FILE:
		LogMsg("%s: Handling %s - Delete Filename = %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType), appEvent->filename);
		/* Call service and just respond with a SUCCESS message */

		LogMsg("%s: Calling tms_setFileOperationResult(%d, TMS_STATUS_ERROR, TMS_EVT_DEL_FILE) - svc_tms_tester does not delete files\n", __FUNCTION__, appEvent->handle);
		ret_val = tms_setFileOperationResultWithDescription(appEvent->handle, TMS_STATUS_ERROR, TMS_EVT_DEL_FILE, "App DEL not supported");

		memset(disp_str, 0, sizeof(disp_str));
		sprintf(disp_str, "tms_setFileOperationResult() returned %s", status_to_string(ret_val));
		CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		break;

	case TMS_EVT_NOTIFICATION:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));

		/* Just display a message with the notification event */
		memset(disp_str, 0, sizeof(disp_str));
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_REBOOT_DEVICE)
		{
			strcat(disp_str, "Device Rebooting ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_RESTART_APPS)
		{
			struct tmsReturn tms_ret;
			strcat(disp_str, "Restarting Apps ");

			LogMsg("svc_tms_tester: Un-Registering App %s\n", "svc_tms_tester_u2");
			tms_ret = tms_unregisterApplication("svc_tms_tester_u2");
			if (tms_ret.status == TMS_STATUS_REQUESTED)
				LogMsg("svc_tms_tester: Un-Register requested\n");


		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_DOWNLOAD_STARTED)
		{
			strcat(disp_str, "Download Started-file=");
			strcat(disp_str, appEvent->filename);
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_DOWNLOAD_PERCENT)
		{
			char percent_str[16];

			memset(percent_str, 0, sizeof(percent_str));
			sprintf(percent_str, "%d", appEvent->notificationData.downloadPercent);

			strcat(disp_str, "Download Percent Update-file=");
			strcat(disp_str, appEvent->filename);
			strcat(disp_str, "at ");
			strcat(disp_str, percent_str);
			strcat(disp_str, " percent ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_DOWNLOAD_COMPLETE)
		{
			strcat(disp_str, "Download Complete-file=");
			strcat(disp_str, appEvent->filename);
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_INSTALL_DOWNLOAD)
		{
			strcat(disp_str, "Installing downloaded file - ");
			strcat(disp_str, appEvent->filename);
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_MAINTENANCE_END)
		{
			strcat(disp_str, "Maintenance Completed");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_HEARTBEAT_RESULT)
		{
			strcat(disp_str, "Heartbeat Completed - ");
			if (appEvent->status == TMS_STATUS_SUCCESS)
				strcat(disp_str, "SUCCESSFUL");
			else
				strcat(disp_str, "FAILURE");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_CONTACT_SERVER_START)
		{
			strcat(disp_str, "Contacting Server");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_CONTACT_SERVER_END)
		{
			strcat(disp_str, "Server Contact Complete");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_CONTACT_SERVER_APPROVAL)
		{
			/* Should never get this notification.  It is a duplicate of CONTACT_SERVER_START, but when registering
			for this notifcation, it means the app wants to approve server contact */
			strcat(disp_str, "Contact Server Approval");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_CONTENT_UPDATES)
		{
			strcat(disp_str, "Content Update");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_CLOCK_UPDATE)
		{
			strcat(disp_str, "Clock Updated");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_SERVER_ERRORS)
		{
			char temp_str[128];

			LogMsg("%s: Server Error Notification - Error = %s (%d)\n", __FUNCTION__, appEvent->filename, appEvent->status);
			sprintf(temp_str, "Server Error %d", appEvent->status);
			strcat(disp_str, temp_str);
			strcat(disp_str, " ");
		}
		CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		break;

	case TMS_EVT_DO_TRANSACTION:
		LogMsg("%s: Handling %s - transaction = %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType), appEvent->transactionType);
		/* Call service and just respond with a SUCCESS message */

		LogMsg("%s: Calling tms_setTransactionResult(%d, TMS_STATUS_SUCCESS)\n", __FUNCTION__, appEvent->handle);
		ret_val = tms_setTransactionResult(appEvent->handle, TMS_STATUS_SUCCESS);

		memset(disp_str, 0, sizeof(disp_str));
		sprintf(disp_str, "tms_setTransactionResult() returned %s", status_to_string(ret_val));
		CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		break;

	case TMS_EVT_REGISTER_APP_RESPONSE:
		memset(disp_str, 0, sizeof(disp_str));
		if (appEvent->status == TMS_STATUS_SUCCESS)
		{
			sprintf(disp_str, "Registration SUCCESSFUL");
			CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);

			tms_setApplicationState(-1, APP_STATUS_BUSY);
		}
		else
		{
			sprintf(disp_str, "Registration FAILED");
			CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		}
		break;

	case TMS_EVT_UNREGISTER_APP_RESPONSE:
		memset(disp_str, 0, sizeof(disp_str));
		if (appEvent->status == TMS_STATUS_SUCCESS)
			sprintf(disp_str, "Un-Registration SUCCESSFUL");
		else
			sprintf(disp_str, "Un-Registration FAILED");

		CreateText(app, &app->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		break;

	case TMS_EVT_APP_ALERT_RESULT:
		memset(disp_str, 0, sizeof(disp_str));
		if (appEvent->status == TMS_STATUS_SUCCESS)
			sprintf(disp_str, "App Alert sent SUCCESSFULLY");
		else
			sprintf(disp_str, "App Alert ERROR %s", status_to_string(appEvent->status));

		CreateText(app, &app->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		break;

	case TMS_EVT_CLEAR_APP_INFO_RESULT:
		memset(disp_str, 0, sizeof(disp_str));
		if (appEvent->status == TMS_STATUS_SUCCESS)
			sprintf(disp_str, "Clear App Info SUCCESSFUL");
		else
			sprintf(disp_str, "Clear App Info ERROR %s", status_to_string(appEvent->status));

		CreateText(app, &app->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		break;

	case TMS_EVT_API_ERRORS:
		/*
		 * This is critical ERROR, VHQ Agent can't trust data exchange with application start from this moment.
		 * Verify your code and provide required changes!
		 *
		 * Future TMS processing with VHQ Agent is not safe. The best case here is to unregister and close/restart application.
		 * You can use tms_sendApplicationAlert() to notify server about problems. Please do not use it for production build.
		 *
		 * Do not use next TMS APIs here:
		 * - tms_setTransactionResult()
		 * - tms_setApplicationState(with handle)
		 * - tms_setApplicationInfo()
		 * - tms_setApplicationParameterList()
		 * - tms_getApplicationFileAvailable()
		 * - tms_setFileOperationResult()
		 * - tms_setFileOperationResultWithDescription()
		 * - tms_setTransactionResult()
		 * since it will cause closed loop behavior, closed loop up to timeout on VHQ Agent side.
		 */
		memset(disp_str, 0, sizeof(disp_str));
		if (appEvent->status == TMS_STATUS_API_SYNC_ERROR)
		{
			struct tmsReturn tms_ret;
#ifdef DEBUG_BUILD
			/* Let's notify server that we have some problems */
			sprintf(disp_str, "API processing error %s: %s", status_to_string(appEvent->status), appEvent->filename);
			tms_ret = tms_sendApplicationAlert(status_to_string(appEvent->status), TMS_ALERT_SEVERITY_HIGH, appEvent->filename);
			if (tms_ret.status == TMS_STATUS_REQUESTED)
				LogMsg("svc_tms_tester: Alert requested\n");
#endif
			/* Unregister application */
			LogMsg("svc_tms_tester: Un-Registering App %s\n", "svc_tms_tester_u2");
			tms_ret = tms_unregisterApplication("svc_tms_tester_u2");
			if (tms_ret.status == TMS_STATUS_REQUESTED)
				LogMsg("svc_tms_tester: Un-Register requested\n");
			/* Close application */
			exit(0);
		} else
			sprintf(disp_str, "API processing error: %s (%d)", status_to_string(appEvent->status), appEvent->status);

		CreateText(app, &app->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		break;
	}

	evas_render(app->fp->e);

	return 0;
}




// Makes interface from WDE PHP simple.
// Note this loop/thread will do the app integration to TMS
void* MainLoopApp(void* pvData)
{
	struct tmsReturn regRtn;
	struct passwd *psPasswd;

	printf("%s: MainLoopApp started\n", __FUNCTION__);

	psPasswd = getpwuid(getuid());
	
	while (1)
	{
		LogMsg("svc_tms_tester: TMS App Ifc Version (tms_GetVersion) = %s\n", tms_GetVersion());

		if (psPasswd)
			LogMsg("svc_tms_tester: Registering App %s as %s\n", "svc_tms_tester_u2", psPasswd->pw_name);
		regRtn = tms_registerApplication("svc_tms_tester_u2", 0xffffffff, (unsigned int)HandleApplicationEvent);
		if (regRtn.status == TMS_STATUS_REQUESTED)
		{

			LogMsg("svc_tms_tester: Register App REQUESTED\n");
			tms_setApplicationState(-1, APP_STATUS_BUSY);

			if (app->appStatus2)
			{
				evas_object_hide(app->appStatus2);
				evas_object_del(app->appStatus2);
				app->appStatus2 = NULL;
			}

			evas_object_text_text_set(app ->appStatus, "Waiting For APP Event");
			evas_render(app->fp->e);

			/* Sleep for 5 minutes and then say we are free */
			LogMsg("svc_tms_tester: sleeping 2 minutes\n", errno, strerror(errno));
			sleep(60 * 2);
			LogMsg("svc_tms_tester: setting app state to FREE\n", errno, strerror(errno));
			tms_setApplicationState(-1, APP_STATUS_FREE);

			while (1)
			{
				LogMsg("svc_tms_tester: still running - sleeping for an hour\n", errno, strerror(errno));
				sleep(60 * 60);
			}

		}
		else
		{
			LogMsg("svc_tms_tester ERROR: tms_registerApplication() returned an error - regRtn->status = %s, errno = %d\n\tRetrying in 30 seconds",
				status_to_string(regRtn.status), errno);
			sleep(30);
		}


	}

	return 0;

}















/*******************************************************************
* Used for the GUI version
*******************************************************************/




#ifndef DATAPATH
#define DATAPATH	"./flash"
#endif

#ifndef THEMES_DATAPATH
#define THEMES_DATAPATH	 DATAPATH "/themes"
#endif



static Evas_Object *textbx;



// Main function
int fp_main(int argc, char **argv)
{
	int MainloopAppRetVal;
	pthread_t MainLoopAppThread;
	struct appdata app_obj;	

	memset(&app_obj, 0, sizeof(struct appdata));
	app = &app_obj;

	app ->fp = fp_init(&argc, argv);

	if (!app ->fp) return -1;
	if (fp_window_init(app ->fp, DATAPATH) < 0) return -1;

	ecore_evas_title_set(app ->fp ->ee, "Content Test App");	
	printf("%s: calling evas_font_path_append(%s)\n", __FUNCTION__, THEMES_DATAPATH);
	evas_font_path_append(app ->fp ->e, THEMES_DATAPATH);
	
	// Create Background Gradient
	printf("%s: calling CreateGradient\n", __FUNCTION__);
	CreateGradient(app);
	printf("%s: calling CreateControls\n", __FUNCTION__);
	CreateControls(app);


	printf("%s: Starting MainLoopApp on a separate thread\n", __FUNCTION__);
	MainloopAppRetVal = pthread_create(&MainLoopAppThread, NULL, MainLoopApp, NULL);
	if (MainloopAppRetVal != 0)
	{
		printf("%s ERROR: Could not start MainLoopApp thread - EXITING\n", __FUNCTION__);
		exit(-1);
	}
	else
		sleep(1);


	// Begin Main Loop
	fp_begin(app ->fp);

	printf("svc_tms_tester: reached end of fp_main - returning -1\n");
	return -1;
}

void CreateGradient(struct appdata *app)
{	
	// Create Background Gradient
	app ->bg = evas_object_gradient_add(app ->fp ->e);
	
	evas_object_gradient_color_add(app ->bg, 0, 0, 255, 255, 1);
	evas_object_gradient_color_add(app ->bg, 0, 0, 128, 255, 2);
	evas_object_gradient_color_add(app ->bg, 0, 0, 0, 255, 3);
	evas_object_gradient_color_add(app ->bg, 0, 0, 128, 255, 4);
	evas_object_gradient_angle_set(app ->bg, 45.0);
	evas_object_resize(app ->bg, app ->fp ->w, app ->fp ->h);
	evas_object_show(app ->bg);
}

void CreateControls(struct appdata *app)
{	
	//printf("%s: +\n", __FUNCTION__);
	// Title
	//printf("%s: creating title text\n", __FUNCTION__);
	CreateText(app, &app ->titleText, "Vera", 12, "MX9 TMS Interface Test App", ALIGN_CENTER, PAD_SPACE, 255, 255, 255);
	
	// App Status
	//printf("%s: creating labeltext1\n", __FUNCTION__);
	CreateText(app, &app ->labelText1, "Vera", 11, "App Status", ALIGN_CENTER, PAD_SPACE + 20, 224, 224, 224);	
	//printf("%s: creating textrect\n", __FUNCTION__);
	CreateRectangle(app, &app ->textRect, PAD_SPACE + 30, (app ->fp ->h / 2) + 10 + PAD_SPACE, 192, 192, 192, 64);	
	
	// App Status
	//printf("%s: creating appstatus\n", __FUNCTION__);
	CreateText(app, &app ->appStatus, "Vera", 25, "Waiting for event", ALIGN_CENTER, (PAD_SPACE + 55), 255, 192, 192);

	//printf("%s: creating horizline1\n", __FUNCTION__);
	CreateLine(app, &app ->horizLine1, PAD_SPACE, (PAD_SPACE + 180), (app ->fp ->w - PAD_SPACE), (PAD_SPACE + 180), 128, 128, 128);
	//printf("%s: creating horizline2\n", __FUNCTION__);
	CreateLine(app, &app ->horizLine2, PAD_SPACE, (PAD_SPACE + 223), (app ->fp ->w - PAD_SPACE), (PAD_SPACE + 223), 128, 128, 128);
	//printf("%s: creating vertline\n", __FUNCTION__);
	CreateLine(app, &app ->vertLine, 205, (PAD_SPACE + 180), 205, (PAD_SPACE + 223), 128, 128, 128);	
	//CreateButton(app, &app ->acceptButton, "Accept Content", (PAD_SPACE * 2), 185, 140, 40, Clicked_AcceptButtonCB);
	//CreateButton(app, &app ->rejectButton, "Reject Content", 2 * (PAD_SPACE * 2) + 140, 185, 140, 40, Clicked_RejectButtonCB);	
	//printf("%s: creating view log button\n", __FUNCTION__);
	CreateButton(app, &app ->viewlogButton, "View Log", app ->fp ->w - 140 - (PAD_SPACE * 4), 185, 140, 40, Clicked_ViewLogButtonCB);
	//printf("%s: creating contact server button\n", __FUNCTION__);
	CreateButton(app, &app ->contactServerButton, "Contact Server", app ->fp ->w - 140 - (PAD_SPACE * 4), 140, 140, 40, Clicked_ContactServerButtonCB);
	//printf("%s: creating get config button\n", __FUNCTION__);
#if CONFIG_BUTTON_IS_GET_CONFIG
	CreateButton(app, &app ->getConfigButton, "Get Config", app ->fp ->w - 280 - (PAD_SPACE * 5), 140, 140, 40, Clicked_GetConfigButtonCB);
#else
	CreateButton(app, &app ->getConfigButton, "Set Config", app ->fp ->w - 280 - (PAD_SPACE * 5), 140, 140, 40, Clicked_GetConfigButtonCB);
#endif
	//printf("%s: +\n", __FUNCTION__);
}

void CreateText(struct appdata *app, Evas_Object **pObj, const char *pFontFace, int iSize, const char *pText, int iX, int iY, int iRed, int iGreen, int iBlue)
{
	Evas_Object *obj;
	Evas_Coord w, h;
	
	if ((*pObj) != NULL)
	{
		printf("%s: *pObj exists - deleting\n", __FUNCTION__);
		evas_object_hide(*pObj);
		evas_object_del(*pObj);
		*pObj = NULL;
	}

	obj = evas_object_text_add(app ->fp ->e);
	//printf("%s: calling evas_object_text_font_source_set(%s)\n", __FUNCTION__, THEMES_DATAPATH);
	evas_object_text_font_source_set(obj, THEMES_DATAPATH);
	evas_object_text_font_set(obj, pFontFace, iSize);		
	evas_object_text_text_set(obj, pText);
	evas_object_color_set(obj, iRed, iGreen, iBlue, 255);	
	evas_object_geometry_get(obj, 0, 0, &w, &h);
	evas_object_resize(obj, app ->fp ->w, h);	
	
	switch (iX)
	{
		case ALIGN_LEFT: iX = PAD_SPACE; break;
		case ALIGN_CENTER: iX = (app ->fp ->w / 2) - (w / 2); break;
   		case ALIGN_RIGHT: iX = (app ->fp ->w - w - PAD_SPACE); break;
	}
	
	evas_object_move(obj, iX, iY);
	evas_object_show(obj);
	
	*pObj = obj;
}

void CreateRectangle(struct appdata *app, Evas_Object **pObj, int iY, int iH, int iRed, int iGreen, int iBlue, int iAlpha)
{
	Evas_Object *obj;
	Evas_Coord iX = PAD_SPACE;
	Evas_Coord iW = app ->fp ->w - PAD_SPACE;

	obj = evas_object_rectangle_add(app ->fp ->e);
	evas_object_move(obj, iX, iY);
	evas_object_resize(obj, iW, iH);
	evas_object_color_set(obj, iRed, iGreen, iBlue, iAlpha);
	evas_object_show(obj);
	*pObj = obj;	
}

void CreateLine(struct appdata *app, Evas_Object **pObj, int x1, int y1, int x2, int y2, int iRed, int iGreen, int iBlue)
{
	Evas_Object *obj;
	
	obj = evas_object_line_add(app ->fp ->e);
	evas_object_line_xy_set(obj, x1, y1, x2, y2);
	evas_object_color_set(obj, iRed, iGreen, iBlue, 255);
	evas_object_show(obj);		
	*pObj = obj;	
}

void CreateButton(struct appdata *app, Evas_Object **pObj, const char *pText, int iX, int iY, int iWidth, int iHeight, fp_button_clicked_cb cb)
{
	Evas_Object *obj;
	
	obj = fp_button_add_text(app ->fp, FP_TEXT_NORMAL, pText);
	fp_button_clicked_callback_add(obj, cb, app);
	fp_button_enable(obj);
	evas_object_move(obj, iX, iY);
	evas_object_resize(obj, iWidth, iHeight);	
	evas_object_show(obj);
	*pObj = obj;	
}


/*****************************************************************
 createScreen()
 create a empty screen containing only background

 Parameters:

 Returns:
 Evas Object
 *****************************************************************/
Evas_Object *createScreen(Evas_Object *screen, int xoffset, int yoffset, int width, int height)
{
	Evas_Object *sc;

	if (screen) {
		evas_object_hide(screen);
		evas_object_del(screen);
	}

	sc = fp_container_add(app->fp);
	evas_object_move(sc, xoffset, yoffset);
	if ( 0 >= width ) {
		width = app->fp->w - xoffset;
	}
	if ( 0 >= height ) {
		height = app->fp->h - yoffset;
	}
	evas_object_resize(sc, width, height);

	evas_object_show(sc);

	return sc;
}



char *readFile(char *fn, int mode)
{
	struct stat st;
	char *tptr, *memptr;
	int h,count;

	memptr=NULL;
	if (stat(fn,&st) == 0) {
		if (st.st_size>0) {
			memptr=malloc(st.st_size+1);
			if (memptr) {
				tptr=memptr;
				h=open(fn,O_RDONLY);
				if (h>=0) {
					if (!mode) {
						read(h,tptr,st.st_size);
						tptr[st.st_size] = 0; // null terminate
					} else {
						count=st.st_size;
						while (count) {
							read(h,tptr,1);
							if ( !((*tptr < ' ') && (*tptr != '\n')) )
								tptr++;
							count--;
						}
						tptr[0] = 0; // null terminate
					}
					close(h);
				} else {
					free(tptr);
					memptr=NULL;
				}
			}
		}
	}
	return memptr;
}



/*****************************************************************
 createTabBackground()
 create the basic background for tab views

 Parameters:
 tab_container - Container in which background is placed

 Returns:
 0 = success
 *****************************************************************/
int createTabBackground(void)
{
	Evas_Object *obj;
	int x, y, w, h;
	int containerX, containerY, containerW, containerH;

	evas_object_geometry_get(app->textRect, &containerX, &containerY, &containerW, &containerH);

	printf("%s: containerx=%d, containery=%d, containerw=%d, containerh=%d\r\n",__FUNCTION__,containerX,containerY,containerW,containerH);

	app->logContainer = createScreen(app->logContainer, containerX, containerY, containerW, containerH);

    // create the background
	obj = evas_object_rectangle_add( app->fp->e );
	evas_object_color_set(obj, 245, 246, 247, 255);
	evas_object_layer_set(obj, 75 - 1);
	evas_object_resize(obj, containerW, containerH);
	printf("%s: fp_container_object_add1(container = 0x%08x, obj = 0x%08x)\r\n",__FUNCTION__, (unsigned int)app->logContainer, (unsigned int)obj);
	fp_container_object_add(app->logContainer, obj, 0, 0); // relative to container x,y
	evas_object_show(obj);

	// add the watermark
	obj = fp_image_add(app->fp);
	fp_image_file_set(obj, "./flash/images/watermark.png");
	evas_object_layer_set(obj, 75); // on top of overlay
	fp_image_size_get(obj, &w, &h);
	x = (containerW - w)/2;
	y = (containerH - h)/2;
	evas_object_resize( obj, w, h );
	printf("%s: fp_container_object_add2(container = 0x%08x, obj = 0x%08x)\r\n",__FUNCTION__,(unsigned int)app->logContainer, (unsigned int)obj);
	fp_container_object_add(app->logContainer, obj, x, y);
	evas_object_show(obj);

	return 0;
}

Evas_Object *createMessageBox(char* msg, enum vf_dialog_icon icon, enum vf_dialog_buttons buttons, void (*msgButtonCb)(void *data, Evas_Object *o, const char *button, const char *input))
{
	Evas_Object *msgBox;

	msgBox = vf_dialog_add(app->fp, msg, icon, buttons, 0);
	if (msgButtonCb) {
		vf_dialog_callback_set(msgBox, msgButtonCb, (void *)msgBox);
	}
	evas_object_focus_set(msgBox, 1);
	evas_object_show(msgBox);
	return msgBox;
}

void destroyMessageBox(Evas_Object* msgBox)
{
    evas_object_del(msgBox);
}


Evas_Object *createText_wrap(Evas_Object *container, int textX, int textY, int textW, const char *msg)
{
	Evas_Object *o;
    int textH;
	int containerX, containerY, containerW, containerH;

	o = fp_text_add( app->fp );
	fp_text_fonts_set(o, "Vera", "Vera", "Vera", NULL, 10);
	evas_object_color_set(o, 0, 0, 0, 255);
	//fp_text_text_style_set(o, pTextDef->style);
	/* resize to requested width, and just use a dummy height */
	textH = 480; // just picked a number...
	evas_object_resize(o, textW, textH);
	if ( msg && strlen(msg) ) {
		fp_text_text_set(o, msg);
		/* now get real required height */
		textH = fp_text_height(o);
		/* now resize */
		evas_object_resize(o, textW, textH);
	}
	evas_object_layer_set( o, 90 );
	evas_object_show( o );
	if (container) {
		if ( 0 == textX ) {
			/* center text in container */
			evas_object_geometry_get(container, &containerX, &containerY, &containerW, &containerH);
			textX = (containerW - textW)/2;
		}
		fp_container_object_add(container, o, textX, textY);
	}

	return o;
}


void Clicked_AcceptButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event)
{
	int set_resRes;
	int process_result = TMS_STATUS_CONTENT_FAILURE;

#if DELETE_BUTTONS_IN_CB
	if (app->acceptButton)
	{
		evas_object_hide(app->acceptButton);
		evas_object_del(app->acceptButton);
		app->acceptButton = NULL;
	}

	if (app->rejectButton)
	{
		evas_object_hide(app->rejectButton);
		evas_object_del(app->rejectButton);
		app->rejectButton = NULL;
	}
#endif

	evas_object_text_text_set(app ->appStatus, "Accpeting Content");
	evas_render(app ->fp ->e);
	sleep(2);

	/* If we have the source and destination, lest copy the files */
	if (strlen(appEvent->filename) && strlen(appEvent->filepath))
	{
		char* pszBaseName;
		char tempname[256];
		char copyFileDest[512];
		int copy_result;

		strcpy(tempname, appEvent->filename);
		pszBaseName = basename(tempname);

		sprintf(copyFileDest, "%s/%s", appEvent->filepath, pszBaseName);
		if (VerifyFilePermissions(appEvent->filename, VERIFY_READ_PERMISSION))
		{
			if (VerifyFilePermissions(appEvent->filepath, VERIFY_WRITE_PERMISSION))
			{
				LogMsg("svc_tms_tester: Copying %s to %s\n", appEvent->filename, copyFileDest);
				copy_result = CopyFile(appEvent->filename, copyFileDest);
				LogMsg("svc_tms_tester: Copy result = %d\n", copy_result);

				if (copy_result == 0)
					process_result = TMS_STATUS_SUCCESS;
				else
					process_result = TMS_STATUS_CONTENT_FAILURE;
			}
		}
	}


	/* If we have a content handle we can set the result */
	if (appEvent->handle > 0)
	{
		LogMsg("svc_tms_tester: Content Accepted - Setting result of ID %d to %s\n", appEvent->handle, status_to_string(process_result));
		set_resRes = tms_setFileOperationResultWithDescription(appEvent->handle, process_result, TMS_EVT_PUT_FILE, NULL);
	}

	LogMsg("svc_tms_tester: freeing appEvent @ 0x%08x\n", (int)appEvent);
	//free(appEvent);
	//appEvent = NULL;

	evas_object_text_text_set(app ->appStatus, "Content Update Accepted");
	evas_render(app ->fp ->e);
	sleep(2);

	waiting_for_usr_response = FALSE;

	LogMsg("%s: returning\n", __FUNCTION__);
}

void Clicked_RejectButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event)
{
	int set_resRes;
	int process_result = TMS_STATUS_CONTENT_FAILURE;

#if DELETE_BUTTONS_IN_CB
	if (app->acceptButton)
	{
		evas_object_hide(app->acceptButton);
		evas_object_del(app->acceptButton);
		app->acceptButton = NULL;
	}

	if (app->rejectButton)
	{
		evas_object_hide(app->rejectButton);
		evas_object_del(app->rejectButton);
		app->rejectButton = NULL;
	}
#endif

	evas_object_text_text_set(app ->appStatus, "Rejecting Content");
	evas_render(app ->fp ->e);
	sleep(2);

	/* If we have a content handle we can set the result */
	if (appEvent->handle)
	{
		LogMsg("svc_tms_tester: Content Rejected - Setting result of ID %d to %s\n", appEvent->handle, status_to_string(process_result));
		set_resRes = tms_setFileOperationResultWithDescription(appEvent->handle, process_result, TMS_EVT_PUT_FILE, "App content REJECTED by user");
	}

	LogMsg("svc_tms_tester: freeing appEvent @ 0x%08x\n", (int)appEvent);
	//free(appEvent);
	//appEvent = NULL;

	evas_object_text_text_set(app ->appStatus, "Content Update Rejected");
	evas_render(app ->fp ->e);
	sleep(2);

	waiting_for_usr_response = FALSE;
}

void Clicked_ContactServerButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event)
{
	struct tmsReturn tms_ret;
	char disp_str1[256];

	if (app->contactServerButton)
	{
		evas_object_hide(app->contactServerButton);
	}
	if (app->appStatus2)
	{
		evas_object_hide(app->appStatus2);
		evas_object_del(app->appStatus2);
		app->appStatus2 = NULL;
	}

	evas_object_text_text_set(app ->appStatus, "Contacting Server");
	evas_render(app ->fp ->e);
	sleep(1);



	LogMsg("%s: Contacting server\n", __FUNCTION__);
	tms_ret = tms_callServer(0);
	

	memset(disp_str1, 0, sizeof(disp_str1));
	LogMsg("%s: contact server result = %s\n", __FUNCTION__, status_to_string(tms_ret.status));
	if (tms_ret.status == TMS_STATUS_REQUESTED)
	{
		sprintf(disp_str1, "Call Server REQUESTED");
	}
	else
	{
		sprintf(disp_str1, "FAILURE to send contact server to agent");
	}

	if (app->appStatus)
	{
		evas_object_hide(app->appStatus);
		evas_object_del(app->appStatus);
		app->appStatus = NULL;
	}

	CreateText(app, &app ->appStatus, "Vera", 15, disp_str1, ALIGN_CENTER, (PAD_SPACE + 55), 255, 192, 192);
	
	LogMsg("%s: re-rendering\n", __FUNCTION__);
	evas_render(app ->fp ->e);
}




void Clicked_GetConfigButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event)
{
#if (!CONFIG_BUTTON_IS_GET_CONFIG)
	int copy_result;
#endif
	char disp_str1[256];
	struct tmsReturn tms_ret;

	if (app->getConfigButton)
	{
		evas_object_hide(app->getConfigButton);
	}
	if (app->appStatus2)
	{
		evas_object_hide(app->appStatus2);
		evas_object_del(app->appStatus2);
		app->appStatus2 = NULL;
	}

#if CONFIG_BUTTON_IS_GET_CONFIG

	evas_object_text_text_set(app ->appStatus, "Getting Config Location");
	evas_render(app ->fp ->e);
	sleep(1);



	LogMsg("%s: Getting Config Location\n", __FUNCTION__);
	tms_ret = tms_getConfigLocation();
	

	memset(disp_str1, 0, sizeof(disp_str1));
	LogMsg("%s: tms_getConfigLocation result = %s\n", __FUNCTION__, status_to_string(tms_ret.status));
	if (tms_ret.status == TMS_STATUS_REQUESTED)
	{
		sprintf(disp_str1, "Get Config Location REQUESTED");
	}
	else
	{
		sprintf(disp_str1, "FAILURE to send get config location request to agent");
	}
#else
	evas_object_text_text_set(app ->appStatus, "Requesting Config Update");
	evas_render(app ->fp ->e);
	sleep(1);

	LogMsg("%s: Copying %s to %s\n", __FUNCTION__, "./tms_config.ini", "/tmp/share/tms_config.ini");
	copy_result = CopyFile("./tms_config.ini", "/tmp/share/tms_config.ini");
	LogMsg("%s: Copy result = %d\n", __FUNCTION__, copy_result);
	chmod("/tmp/share/tms_config.ini", 0666 );

	LogMsg("%s: Requesting Config Update\n", __FUNCTION__);
	tms_ret = tms_setNewConfigAvailable("/tmp/share/tms_config.ini");
	

	memset(disp_str1, 0, sizeof(disp_str1));
	LogMsg("%s: tms_newConfigAvail result = %s\n", __FUNCTION__, status_to_string(tms_ret.status));
	if (tms_ret.status == TMS_STATUS_REQUESTED)
	{
		sprintf(disp_str1, "Config Update REQUESTED");
	}
	else
	{
		sprintf(disp_str1, "FAILURE to send config update request to agent");
	}
#endif

	if (app->appStatus)
	{
		evas_object_hide(app->appStatus);
		evas_object_del(app->appStatus);
		app->appStatus = NULL;
	}

	CreateText(app, &app ->appStatus, "Vera", 15, disp_str1, ALIGN_CENTER, (PAD_SPACE + 55), 255, 192, 192);
	
	LogMsg("%s: re-rendering\n", __FUNCTION__);
	evas_render(app ->fp ->e);
}




void Clicked_ViewLogButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event)
{
	struct appdata *loc_app = (struct appdata *)data;
	Evas_Object *obj, *obj2, *o2;
	int coverX, coverY, coverW, coverH;
	char *tptr;
	struct tmsReturn tms_ret;


	evas_object_geometry_get(loc_app->textRect, &coverX, &coverY, &coverW, &coverH);

	createTabBackground();

	o2=createMessageBox("Please Wait...\nReading log file!",VF_DIALOG_ICON_INFO , VF_DIALOG_BUTTONS_CUSTOM, NULL);
	evas_render(app->fp->e);

	LogMsg("svc_tms_tester: Un-Registering App %s\n", "svc_tms_tester_u2");
	tms_ret = tms_unregisterApplication("svc_tms_tester_u2");
	if (tms_ret.status == TMS_STATUS_REQUESTED)
		LogMsg("svc_tms_tester: Un-Register requested\n");



	tptr=readFile(SVC_TMS_TESTER_LOG_FILE,0);
	if (tptr)
	{
		textbx = obj=createText_wrap(loc_app->logContainer, 0,0, coverW-18,tptr);
		free(tptr);
		obj2=fp_scrollbar_add(app->fp,obj);
		fp_scrollbar_bars_show_set(obj2,FP_SCROLLBAR_SHOW_AUTO,FP_SCROLLBAR_SHOW_NEVER);
        fp_scrollbar_dragging_set(obj2,1);
        fp_scrollbar_fading_set(obj2,1);        
		evas_object_resize(obj2,coverW-15,coverH-15);
		fp_container_object_add(loc_app->logContainer, obj2, 10, 10);
		evas_object_show(obj2);
	}

	destroyMessageBox(o2);

	CreateButton(app, &app ->logDeleteButton, "Delete Log", (PAD_SPACE * 2), ((app ->fp ->h) - 60), 140, 40, Clicked_DeleteButtonCB);
	CreateButton(app, &app ->logExitButton, "Exit", 2 * (PAD_SPACE * 2) + 140, ((app ->fp ->h) - 60), 140, 40, Clicked_ExitButtonCB);
}

void Clicked_DeleteButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event)
{
	if (remove(SVC_TMS_TESTER_LOG_FILE) != 0)
		LogMsg("%s: errno set in removing file (%s) = %d\n", __FUNCTION__, SVC_TMS_TESTER_LOG_FILE, errno);

	Clicked_ExitButtonCB(data, button, mouse_event);
	Clicked_ViewLogButtonCB(data, button, mouse_event);
}

void Clicked_ExitButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event)
{
	if (app->logContainer)
	{
		evas_object_hide(app->logContainer);
		evas_object_del(app->logContainer);
		app->logContainer = NULL;
	}

	if (app->logDeleteButton)
	{
		evas_object_hide(app->logDeleteButton);
		evas_object_del(app->logDeleteButton);
		app->logDeleteButton = NULL;
	}

	if (app->logExitButton)
	{
		evas_object_hide(app->logExitButton);
		evas_object_del(app->logExitButton);
		app->logExitButton = NULL;
	}
}

//	Make Enable / Disable Togglable
//	fp_button_background_normal
//	fp_button_background_pressed	
//	fp_button_metric_image_set(app ->enableButton, "fp_button_background_pressed", "fp_button_background_pressed");
//	fp_button_metric_image_set(app ->disableButton, "fp_button_background_normal", "fp_button_background_pressed");
//	fp_button_metric_image_set(app ->enableButton, "fp_button_background_normal", "fp_button_background_pressed");
//	fp_button_metric_image_set(app ->disableButton, "fp_button_background_pressed", "fp_button_background_pressed");
