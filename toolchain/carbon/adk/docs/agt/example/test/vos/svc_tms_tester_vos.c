#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <grp.h>
#include <libgen.h>
#include <stdarg.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <pwd.h>
#include <unistd.h>
#include <ucontext.h>
#include "svc_tms.h"



#include <fancypants/Evas.h>
#include <fancypants/Ecore.h>
#include <fancypants/Ecore_Evas.h>

#include <fancypants/libfp/fp.h>
#include <fancypants/libfp/fp_theme.h>
#include <fancypants/libfp/fp_image.h>
#include <fancypants/libfp/fp_container.h>
#include <fancypants/libfp/fp_button.h>
#include <fancypants/libfp/fp_checkbox.h>
#include <fancypants/libfp/fp_text.h>
#include <fancypants/libfp/fp_list.h>
#include <fancypants/libfp/fp_multivalue.h>
#include <fancypants/libfp/fp_multiselect.h>
#include <fancypants/libfp/fp_scrollbar.h>

#include <fancypants/libfp/media/fp_media.h>
#include <fancypants/libfp/media/fp_media_enum.h>

#include "vf_sig_capture.h"
#include "vf_textbox.h"
#include "vf_dialog.h"


#define CONFIG_BUTTON_IS_GET_CONFIG		0

typedef unsigned int uint32;


/* The Application GUI */
struct appdata
{
	struct fp *fp;
	Evas_Object *bg;
	Evas_Object *titleText;
	Evas_Object *textRect;
	Evas_Object *labelText1;
	Evas_Object *labelText2;
	Evas_Object *horizLine1;
	Evas_Object *horizLine2;
	Evas_Object *vertLine;	
	Evas_Object *appStatus;
	Evas_Object *appStatus2;

	Evas_Object *acceptButton;
	Evas_Object *postponeButton;
	Evas_Object *errorButton;
	Evas_Object *viewlogButton;
	Evas_Object *contactServerButton;
	Evas_Object *getConfigButton;
	Evas_Object *swapConfig1Button;
	Evas_Object *swapConfig2Button;

	Evas_Object *logDeleteButton;
	Evas_Object *logExitButton;

	Evas_Object *logContainer;
};

struct appdata *app;


void CreateGradient(struct appdata *app);
void CreateControls(struct appdata *app);
void CreateText(struct appdata *app, Evas_Object **pObj, const char *pFontFace, int iSize, const char *pText, int iX, int iY, int iRed, int iGreen, int iBlue);
void CreateRectangle(struct appdata *app, Evas_Object **pObj, int iY, int iH, int iRed, int iGreen, int iBlue, int iAlpha);
void CreateLine(struct appdata *app, Evas_Object **pObj, int x1, int y1, int x2, int y2, int iRed, int iGreen, int iBlue);
void CreateButton(struct appdata *app, Evas_Object **pObj, const char *pText, int iX, int iY, int iWidth, int iHeight, fp_button_clicked_cb cb);
void Clicked_AcceptButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event);
void Clicked_PostponeButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event);
void Clicked_ErrorButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event);
void Clicked_ViewLogButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event);
void Clicked_ContactServerButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event);
void Clicked_SwapConfig1ButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event);
void Clicked_SwapConfig2ButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event);
void Clicked_GetConfigButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event);
void Clicked_DeleteButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event);
void Clicked_ExitButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event);


char *readFile(char *fn, int mode);

#define PAD_SPACE 	3
#define ALIGN_LEFT		-1000
#define ALIGN_CENTER 	-2000
#define ALIGN_RIGHT 	-3000


#ifndef boolean
#define boolean unsigned char
#define TRUE 1
#define FALSE 0
#endif



/* Allow 20 Kb for the log file */
#define SVC_TMS_TESTER_LOG_FILE						"./logs/svc_tms_tester_log"
#define SVC_TMS_TESTER_LOG_FILE_SIZE_LIMIT			(20 * 1024)


static boolean backup_requested = FALSE;
static boolean backup_active = FALSE;
static boolean third_active = FALSE;
static char prevInstance1[MAX_SERVER_INSTANCE_NAME_LEN];
static char prevInstance2[MAX_SERVER_INSTANCE_NAME_LEN];

static pthread_mutex_t evas_mutex;

static boolean postpone_installs = FALSE;
static boolean delay_installs = FALSE;
static int delay_time = 900;
static time_t delay_time_expire = 0;


typedef enum
{
	VERIFY_READ_PERMISSION,
	VERIFY_WRITE_PERMISSION,
	VERIFY_EXECUTE_PERMISSION,
	VERIFY_RW_PERMISSION,
	VERIFY_RWX_PERMISSION,
}verify_permission_t;



static void sighandler(int signum, siginfo_t *siginfo, void *context)
{
	if ((signum != SIGALRM) && (signum != SIGCHLD))
		printf("Signal [%d] in thread %u (PID %d) received.", signum, (unsigned int)pthread_self(), getpid());
	if (signum == SIGSEGV)
	{
		ucontext_t *ucontext = (ucontext_t*)context;

		printf("SIGSEGV data: si_errno = %d, si_pid = %u, si_uid = %u, si_addr = 0x%08x\n",
			siginfo->si_errno, (unsigned int)siginfo->si_pid, (unsigned int)siginfo->si_uid, (unsigned int)siginfo->si_addr);


		printf("ip = 0x%08x, pc = 0x%08x, sp = 0x%08x\n",
			(unsigned int)ucontext->uc_mcontext.arm_ip, (unsigned int)ucontext->uc_mcontext.arm_pc, (unsigned int)ucontext->uc_mcontext.arm_sp);

		exit(-1);

	}
}


int LogMsgEx(const char *fname, const char *string, va_list ap)
{
	int iFile;
	int r = 0;
	struct stat log_stat;
	struct timeval cur_time;
	struct tm local;
	time_t t;

	gettimeofday(&cur_time, NULL);
	t = cur_time.tv_sec;

	while ((*string == '\n') || (*string == '\r'))
	{
		printf("%c", *string);
		string++;
	}

	if (localtime_r(&t, &local))
		printf("%02d:%02d:%02d: ", local.tm_hour, local.tm_min, local.tm_sec);
	else
		printf("%s: errno set in localtime_r() = %d\n", __FUNCTION__, errno);

	r = vprintf (string, ap);


	/* If the logs are too big, rename them to svc_tms_tester_log1, svc_tms_tester_log2*/
	if (stat(fname, &log_stat) == 0)
	{
		if (log_stat.st_size > SVC_TMS_TESTER_LOG_FILE_SIZE_LIMIT)
		{
			char szName1[256], szName2[256];
			sprintf(szName1, "%s.1", fname);
			sprintf(szName2, "%s.2", fname);
			if (rename(szName1, szName2) != 0)
				printf("%s: errno set in renaming file (%s.1) = %d\n", __FUNCTION__, fname, errno);
			if (rename(SVC_TMS_TESTER_LOG_FILE, szName1) != 0)
				printf("%s: errno set in renaming file (%s) = %d\n", __FUNCTION__, SVC_TMS_TESTER_LOG_FILE, errno);
		}
	}



#ifndef WIN32
	iFile= open(fname, O_CREAT | O_APPEND | O_RDWR, S_IRWXU);
#else
	iFile= open(fname, O_CREAT | O_APPEND | O_RDWR, _S_IREAD | _S_IWRITE);
#endif
	if (iFile!=-1)
	{
		char* szMsg;
		char* szMsgWithTime;


		szMsg = malloc(25 * 1024);
		szMsgWithTime = malloc((25 * 1024) + 50);

		r = 0;
		if (szMsg)
		{
			r = vsprintf (szMsg, string, ap);
		}

		if (szMsgWithTime && (r>0))
		{
			sprintf(szMsgWithTime, "%d/%d/%d %02d:%02d:%02d: %s", local.tm_mon+1, local.tm_mday, (local.tm_year % 100), local.tm_hour, local.tm_min, local.tm_sec, szMsg);
			write(iFile, szMsgWithTime, strlen(szMsgWithTime));
		}

		if (szMsg)
		{
			//DebugMsg("%s: Freeing szMsg @ 0x%08x\n", __FUNCTION__, szMsg);
			free(szMsg);
			szMsg = NULL;
		}
		if (szMsgWithTime)
		{
			//DebugMsg("%s: Freeing szMsgWithTime @ 0x%08x\n", __FUNCTION__, szMsgWithTime);
			free(szMsgWithTime);
			szMsgWithTime = NULL;
		}

		close(iFile);

		if (chmod(fname, 0666 ) != 0)
			printf("%s: errno set in chmod() = %d\n", __FUNCTION__, errno);
	}

	return r;
}

int LogMsg(const char *string, ...)
{
	int ret_val;

	va_list ap;
	va_start (ap, string);
	ret_val = LogMsgEx(SVC_TMS_TESTER_LOG_FILE, string, ap);
	va_end(ap);

	return ret_val;
}



///////////////////////////////////////////////////////////////////////////////
///
///  \fn       void VerifyFilePermissions()
///  \brief    Verifies the agent has permissions to the file in question
///
///  Returns TRUE if the permissions seem correct, FALSE if not
////////////////////////////////////////////////////////////////////////////////
boolean VerifyFilePermissions(char* pszConfigFile, verify_permission_t permission_type)
{
	uid_t my_uid = getuid();
	int num_groups;
	gid_t my_gids[64];
	struct stat file_stat;
	char permissions_str[16];

	/* Make sure the file exists */
	if (stat(pszConfigFile, &file_stat) != 0)
	{
		LogMsg("%s: %s does not exist - returning FALSE\n", __FUNCTION__, pszConfigFile);
		return FALSE;
	}


	memset(permissions_str, 0, sizeof(permissions_str));

	/* Setup directory */
	if (S_ISDIR(file_stat.st_mode))
		strcpy(permissions_str, "d");
	else
		strcpy(permissions_str, "-");

	/* Setup owner permissions */
	if (file_stat.st_mode & S_IRUSR)
		strcat(permissions_str, "r");
	else
		strcat(permissions_str, "-");
	if (file_stat.st_mode & S_IWUSR)
		strcat(permissions_str, "w");
	else
		strcat(permissions_str, "-");
	if (file_stat.st_mode & S_IXUSR)
		strcat(permissions_str, "x");
	else
		strcat(permissions_str, "-");


	/* Setup group permissions */
	if (file_stat.st_mode & S_IRGRP)
		strcat(permissions_str, "r");
	else
		strcat(permissions_str, "-");
	if (file_stat.st_mode & S_IWGRP)
		strcat(permissions_str, "w");
	else
		strcat(permissions_str, "-");
	if (file_stat.st_mode & S_IXGRP)
		strcat(permissions_str, "x");
	else
		strcat(permissions_str, "-");


	/* Setup other permissions */
	if (file_stat.st_mode & S_IROTH)
		strcat(permissions_str, "r");
	else
		strcat(permissions_str, "-");
	if (file_stat.st_mode & S_IWOTH)
		strcat(permissions_str, "w");
	else
		strcat(permissions_str, "-");
	if (file_stat.st_mode & S_IXOTH)
		strcat(permissions_str, "x");
	else
		strcat(permissions_str, "-");

	LogMsg("%s: permissions of %s - %s\n", __FUNCTION__, pszConfigFile, permissions_str);


	if (permission_type == VERIFY_READ_PERMISSION)
	{
		if (file_stat.st_mode & S_IROTH)
		{
			LogMsg("%s: we have 'other' read priveleges - returning TRUE\n", __FUNCTION__);
			return TRUE;
		}
		
		if (file_stat.st_mode & S_IRGRP)
		{
			int i;
			num_groups = getgroups(64, my_gids);

			for (i = 0; i < num_groups; i++)
			{
				if (my_gids[i] == file_stat.st_gid)
				{
					LogMsg("%s: we have group read priveleges as gid %d - returning TRUE\n", __FUNCTION__, my_gids[i]);
					return TRUE;
				}
				LogMsg("%s: no group read priveleges in gid %d (file gid = %d) - returning TRUE\n", __FUNCTION__, my_gids[i], file_stat.st_gid);
			}
		}
		
		if ((my_uid == file_stat.st_uid) && (file_stat.st_mode & S_IRUSR))
		{
			LogMsg("%s: we have owner read priveleges - returning TRUE\n", __FUNCTION__);
			return TRUE;
		}

		LogMsg("%s: no read priveleges - setting up permission string\n", __FUNCTION__);
	}
	else if (permission_type == VERIFY_WRITE_PERMISSION)
	{
		if (file_stat.st_mode & S_IWOTH)
		{
			LogMsg("%s: we have 'other' write priveleges - returning TRUE\n", __FUNCTION__);
			return TRUE;
		}

		if (file_stat.st_mode & S_IWGRP)
		{
			int i;
			num_groups = getgroups(64, my_gids);

			for (i = 0; i < num_groups; i++)
			{
				if (my_gids[i] == file_stat.st_gid)
				{
					LogMsg("%s: we have group write priveleges as gid %d - returning TRUE\n", __FUNCTION__, my_gids[i]);
					return TRUE;
				}
				LogMsg("%s: no group write priveleges in gid %d (file gid = %d)\n", __FUNCTION__, my_gids[i], file_stat.st_gid);
			}
		}

		if ((my_uid == file_stat.st_uid) && (file_stat.st_mode & S_IWUSR))
		{
			LogMsg("%s: we have owner write priveleges - returning TRUE\n", __FUNCTION__);
			return TRUE;
		}

		LogMsg("%s: no write priveleges - setting up permission string\n", __FUNCTION__);
	}
	else
		LogMsg("%s: permission type %d not supported yet\n", __FUNCTION__, permission_type);


	LogMsg("%s: Invalid permissions on %s - %s (Need read priveleges)\n", __FUNCTION__, pszConfigFile, permissions_str);
	return FALSE;
}





static int CopyFile(const char *pszSrc, const char *pszDest)
{
	FILE *fpSrc, *fpDst;
	unsigned char buf[8*1024];
	int n_read;

	LogMsg("Copyfile: copying %s to %s\r\n", pszSrc, pszDest);
	fpSrc= fopen(pszSrc, "r");
	fpDst= fopen(pszDest, "w");

	// return an error if we can't open source and dest files
	if ((fpSrc == NULL) || (fpDst == NULL))
	{
		if (fpSrc == NULL)
			LogMsg("%s: could not open souce %s\n", __FUNCTION__, pszSrc);
		else
			fclose(fpSrc);

		if (fpDst == NULL)
			LogMsg("%s: could not open dest %s\n", __FUNCTION__, pszDest);
		else
			fclose(fpDst);

		return -1;
	}

	LogMsg("Copyfile: files opened, src: %lx dst:%lx\r\n", (long)fpSrc, (long)fpDst);
	if (fpSrc!=NULL && fpDst!=NULL)
	{
		while ((n_read = fread(buf, sizeof(char), sizeof(buf), fpSrc)) > 0) 
		{
			if (n_read < sizeof(buf) && ferror(fpSrc))
				break;
			if ((fwrite(buf, n_read, 1, fpDst) < 1) && ferror(fpDst)) {
//				g_warning(_("writing to %s failed.\n"), dest);
				fclose(fpDst);
				fclose(fpSrc);
				unlink(pszDest);
				return -1;
			}
		}
	}

	LogMsg("Copyfile: closing files\r\n");
	if (fpSrc) fclose(fpSrc);
	if (fpDst) fclose(fpDst);
	LogMsg("Copyfile: done\r\n");
	return 0;
}





char complex_status_str[256];
static const char* status_to_string(int status)
{
	memset(complex_status_str, 0, sizeof(complex_status_str));
	switch (status)
	{
	case TMS_STATUS_SUCCESS:
		return "Success";
	case TMS_STATUS_ERROR:
		return "Error";
	case TMS_STATUS_UNSUPPORTED_FEATURE:
		return "Unsupported Feature";
	case TMS_STATUS_CONTENT_AVAILABLE:
		return "Content Avaliable";
	case TMS_STATUS_NO_CONSUMER:
		return "No Consumer";
	case TMS_STATUS_FILENAME_ERROR:
		return "Filename ERROR";
	case TMS_STATUS_CONTENT_FAILURE:
		return "Status Content Failure";

	case TMS_STATUS_MSGQ_FAILURE:
		return "Message Queue FAILURE";
	case TMS_STATUS_MSGSND_FAILURE:
		return "Message Send FAILURE";
	case TMS_STATUS_MSGRCV_FAILURE:
		return "Message Receive FAILURE";
	case TMS_STATUS_APP_EVENT_AVAIL:
		return "App Event Available";

	case TMS_STATUS_REQUESTED:
		return "Requested";

	case TMS_STATUS_REGISTER_FAIL:
		return "Register Fail";
	case TMS_STATUS_REGISTER_NAME_TOO_LONG:
		return "Register Name Too Long";
	case TMS_STATUS_EINVAL:
		return "Error in Value";
	case TMS_STATUS_AGENT_NOT_RUNNING:
		return "Agent Not Running";

	case TMS_STATUS_SERVER_INSTANCE_ERROR:
		return "Srv Inst Error";
	case TMS_STATUS_SERVER_INSTANCE_LOCK_ERROR:
		return "Srv Inst Lock Error";
	case TMS_STATUS_SERVER_INSTANCE_LOCK_TIMEOUT_ERROR:
		return "Srv Inst Lock Timeout Error";
	case TMS_STATUS_SERVER_INSTANCE_ALREADY_LOCKED:
		return "Srv Inst Already Locked";
	case TMS_STATUS_SERVER_INSTANCE_MAX_LOCK_REQ_EXCEEDED:
		return "Too Many SrvInst Lock Requests";
	case TMS_STATUS_SERVER_INSTANCE_UNLOCK_ERROR:
		return "Srv Inst Unlock Error";
	case TMS_STATUS_SERVER_INSTANCE_CONFIG_LOAD_ERROR:
		return "Srv Inst Config Load Error";

	case TMS_STATUS_AGENT_BUSY_ERROR:
		return "Agent Busy Error";
	case TMS_STATUS_POSTPONED:
		return (char*) "Postponed";
	case TMS_STATUS_CANCELLED:
		return (char*) "Cancelled";
	case TMS_STATUS_AGENT_CONTACTSRV_ERROR:
		return "Agent not registered on the server yet";
	case TMS_STATUS_API_SYNC_ERROR:
		return "Lost SYNC of TMS exchange";

	default:
		sprintf(complex_status_str, "Unknown (%d)", status);
		return complex_status_str;
	}

	sprintf(complex_status_str, "Unknown (%d)", status);
	return complex_status_str;
}

static char* msgType_to_string(int msgType)
{
	switch (msgType)
	{
	case TMS_EVT_CALL_SERVER_RESPONSE:
		return "Call Server";
	case TMS_EVT_SET_APP_STATE:
		return "Get APP State";
	case TMS_EVT_GET_TMS_CONFIG_RESPONSE:
		return "Get TMS Config";
	case TMS_EVT_SET_TMS_CONFIG_RESPONSE:
		return "Set TMS Config";
	case TMS_EVT_SET_APP_INFO:
		return "Get APP Info";
	case TMS_EVT_SET_PARM_LIST:
		return "Get Parameter List";
	case TMS_EVT_GET_FILE:
		return "Get File";
	case TMS_EVT_PUT_FILE:
		return "Put File";
	case TMS_EVT_DEL_FILE:
		return "Delete File";
	case TMS_EVT_NOTIFICATION:
		return "Event Notfication";
	case TMS_EVT_DO_TRANSACTION:
		return "Do Transaction";
	case TMS_EVT_REGISTER_APP_RESPONSE:
		return "Register";
	case TMS_EVT_UNREGISTER_APP_RESPONSE:
		return "Un-Register";

	case TMS_EVT_GET_SERVER_INSTANCE:
		return "Get Srv Inst";
	case TMS_EVT_LOCK_SERVER_INSTANCE:
		return "Lock Srv Inst";
	case TMS_EVT_RELEASE_SERVER_INSTANCE:
		return "Release Srv Inst";

	case TMS_EVT_APP_ALERT_RESULT:
		return "App Event Result";

	case TMS_EVT_CLEAR_APP_INFO_RESULT:
		return "Clear App Info Result";

	case TMS_EVT_API_ERRORS:
		return "API Usage Error";

	default:
		return "Unknown";
	}

	return "Unknown";
}


#define DELETE_BUTTONS_IN_CB	0



struct tmsEventData* appEvent;
boolean waiting_for_usr_response = FALSE;



#if 0

// Simple exe to call tms service to deliver a test content update to an app
// Makes interface from WDE PHP simple.
// Note this will setup the content registration and wait for content
void* MainLoopContent(void* pvData)
{
	struct tmsRegisterReturn* regRtn;
	struct passwd *psPasswd;
	char contentEventKey[32];

	printf("%s: MainLoopContent started\n", __FUNCTION__);
	sleep(5);

	psPasswd = getpwuid(getuid());
	
	LogMsg("svc_tms_tester: Registering for content as %s\n", psPasswd->pw_name);
	regRtn = tms_registerForContent();
	if (regRtn && (regRtn->status == TMS_STATUS_SUCCESS))
	{

		memcpy(contentEventKey, regRtn->eventkey, 32);
		LogMsg("svc_tms_tester: Registered for content - Event Key = %s - freeing regRtn\n", contentEventKey);

		free(regRtn);
		regRtn = NULL;

		while (1)
		{

			if (app->appStatus2)
			{
				evas_object_hide(app->appStatus2);
				evas_object_del(app->appStatus2);
				app->appStatus2 = NULL;
			}
			evas_object_text_text_set(app ->appStatus, "Waiting For Content");
			evas_render(app->fp->e);

			LogMsg("svc_tms_tester: Waiting for content on Key %s\n", contentEventKey);
			contentEvent = tms_getContentEvent(contentEventKey, 0);

			if (contentEvent)
			{
				if (app->appStatus2)
				{
					evas_object_hide(app->appStatus2);
					evas_object_del(app->appStatus2);
					app->appStatus2 = NULL;
				}
				evas_object_text_text_set(app ->appStatus, "Received Content Update - Accept or Reject");

				LogMsg("svc_tms_tester: Got a content event\n\tStatus = %s\n\tType = %d\n\tContent ID = %d\n\tFilename = %s\n\tfilepath = %s\n",
					status_to_string(contentEvent->status), contentEvent->contentType, contentEvent->contentID, contentEvent->filename, contentEvent->filepath);

				CreateButton(app, &app ->acceptButton, "Accept Content", (PAD_SPACE * 2), 185, 140, 40, Clicked_AcceptButtonCB);
				CreateButton(app, &app ->rejectButton, "Reject Content", 2 * (PAD_SPACE * 2) + 140, 185, 140, 40, Clicked_RejectButtonCB);

				evas_render(app->fp->e);

				waiting_for_usr_response = TRUE;

				while(waiting_for_usr_response)
				{
					printf("%s: Waiting for user response\n", __FUNCTION__);
					sleep(3);
				}


#if (!DELETE_BUTTONS_IN_CB)
				printf("%s: Got user response - deleting buttons\n", __FUNCTION__);

				/* We can remove the accept button */
				if (app->acceptButton)
				{
					evas_object_hide(app->acceptButton);
					evas_object_del(app->acceptButton);
					app->acceptButton = NULL;
				}

				/* We can remove the reject button */
				if (app->rejectButton)
				{
					evas_object_hide(app->rejectButton);
					evas_object_del(app->rejectButton);
					app->rejectButton = NULL;
				}
#else
				printf("%s: Got user response\n", __FUNCTION__);
#endif
			}
		}

	}
	else
	{
		LogMsg("svc_tms_tester ERROR: tms_registerForContent() returned an error - regRtn = 0x%08x, regRtn->status = %s, errno = %d\n",
			(int)regRtn, regRtn ? status_to_string(regRtn->status) : "NULL", errno);
	}

	if (regRtn)
		free(regRtn);

	return 0;

}

#endif


#define APP_PARM_DATA_FILE					"/tmp/share/AppParmFile.dat"
#define APP_PARM_DATA_FILE_TEST_DATA		"test.ini SHA-1 1234567890123456789012345678901234567890"


struct tmsAppParameter custom_parameters[] =
{
	{ "Usr1AppId", "Test Identifier 1", TMS_PARAMETER_TYPE_IDENTIFIER },

	{ " 1CardInsertCount", "30", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 2CardSwipeCount", "6", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 3ContactlessTapCount", "20", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 4RestartCount", "7", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 5191115053913035_ConnectionType", { 0 }, TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 6191115053913035_DataEntryTimeInMs", "-19852", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 7191115053913035_EMVApp", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 8191115053913035_HostConnectTimeInMs", "", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 9191115053913035_HostResponseTimeInMs", "4664", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 10191115053913035_PrintingTimeInMs", "", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 11191115053913035_TotalCommsTimeInMs", "6458", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 12191115053913035_TxnDateTime", "150219000539", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 13191115053913035_TxnStatus", "Unknown", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 14191115053913035_TxnTotalTimeInSec", "159", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 15191115053913035_TxnType", "Logon", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 1617111523362211552_ConnectionType", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 1717111523362211552_DataEntryTimeInMs", "-7545", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 1817111523362211552_EMVApp", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 1917111523362211552_HostConnectTimeInMs", "1097", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 2017111523362211552_HostResponseTimeInMs", "6448", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 2117111523362211552_TotalCommsTimeInMs", "7545", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 2217111523362211552_TxnDateTime", "150217233622", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 2317111523362211552_TxnStatus", "Unknown", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 2417111523362211552_TxnTotalTimeInSec", "2", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 2517111523362211552_TxnType", "Settlement", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 261811153342011968_ConnectionType", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 271811153342011968_DataEntryTimeInMs", "-3860", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 281811153342011968_EMVApp", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 291811153342011968_HostConnectTimeInMs", "920", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 301811153342011968_HostResponseTimeInMs", "2940", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 311811153342011968_TotalCommsTimeInMs", "3860", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 321811153342011968_TxnDateTime", "150218033420", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 331811153342011968_TxnStatus", "Unknown", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 341811153342011968_TxnTotalTimeInSec", "2", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 351811153342011968_TxnType", "Settlement", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 3617111523362918691_ConnectionType", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 3717111523362918691_DataEntryTimeInMs", "-7316", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 3817111523362918691_EMVApp", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 3917111523362918691_HostConnectTimeInMs", "868", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 4017111523362918691_HostResponseTimeInMs", "6448", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 4117111523362918691_TotalCommsTimeInMs", "7316", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 4217111523362918691_TxnDateTime", "150217233629", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 4317111523362918691_TxnStatus", "Unknown", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 4417111523362918691_TxnTotalTimeInSec", "2", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 4517111523362918691_TxnType", "Settlement", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 461811153342721410_ConnectionType", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 471811153342721410_DataEntryTimeInMs", "-3772", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 481811153342721410_EMVApp", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 491811153342721410_HostConnectTimeInMs", "832", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 501811153342721410_HostResponseTimeInMs", "2940", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 511811153342721410_TotalCommsTimeInMs", "3772", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 521811153342721410_TxnDateTime", "150218033427", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 531811153342721410_TxnStatus", "Unknown", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 541811153342721410_TxnTotalTimeInSec", "2", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 551811153342721410_TxnType", "Settlement", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 5617111523363619755_ConnectionType", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 5717111523363619755_DataEntryTimeInMs", "-7300", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 5817111523363619755_EMVApp", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 5917111523363619755_HostConnectTimeInMs", "852", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 6017111523363619755_HostResponseTimeInMs", "6448", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 6117111523363619755_TotalCommsTimeInMs", "7300", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 6217111523363619755_TxnDateTime", "150217233636", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 6317111523363619755_TxnStatus", "Unknown", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 6417111523363619755_TxnTotalTimeInSec", "2", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 6517111523363619755_TxnType", "Settlement", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 661811153343519448_ConnectionType", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 671811153343519448_DataEntryTimeInMs", "-3777", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 681811153343519448_EMVApp", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 691811153343519448_HostConnectTimeInMs", "837", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 701811153343519448_HostResponseTimeInMs", "2940", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 711811153343519448_TotalCommsTimeInMs", "3777", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 721811153343519448_TxnDateTime", "150218033435", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 731811153343519448_TxnStatus", "Unknown", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 741811153343519448_TxnTotalTimeInSec", "2", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 751811153343519448_TxnType", "Settlement", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 761811153344216709_ConnectionType", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 771811153344216709_DataEntryTimeInMs", "-3611", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 781811153344216709_EMVApp", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 791811153344216709_HostConnectTimeInMs", "671", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 801811153344216709_HostResponseTimeInMs", "2940", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 811811153344216709_TotalCommsTimeInMs", "3611", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 821811153344216709_TxnDateTime", "150218033442", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 831811153344216709_TxnStatus", "Unknown", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 841811153344216709_TxnTotalTimeInSec", "2", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 851811153344216709_TxnType", "Settlement", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 8617111523365130822_ConnectionType", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 8717111523365130822_DataEntryTimeInMs", "-7121", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 8817111523365130822_EMVApp", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 8917111523365130822_HostConnectTimeInMs", "673", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 9017111523365130822_HostResponseTimeInMs", "6448", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 9117111523365130822_TotalCommsTimeInMs", "7121", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 9217111523365130822_TxnDateTime", "150217233651", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 9317111523365130822_TxnStatus", "Unknown", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 9417111523365130822_TxnTotalTimeInSec", "2", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 9517111523365130822_TxnType", "Settlement", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 9617111523365816936_ConnectionType", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 9717111523365816936_DataEntryTimeInMs", "-7340", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 9817111523365816936_EMVApp", "GPRS", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 9917111523365816936_HostConnectTimeInMs", "892", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 10017111523365816936_HostResponseTimeInMs", "6448", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 10117111523365816936_TotalCommsTimeInMs", "7340", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 10217111523365816936_TxnDateTime", "150217233658", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 10317111523365816936_TxnStatus", "Unknown", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 10417111523365816936_TxnTotalTimeInSec", "2", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 10517111523365816936_TxnType", "Settlement", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 106201502171130_BatteryCapacity", "1701 mAH", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 107201502171130_BatteryRemainingLevel", "71 % ", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 108201502171130_BatterySerialNumber", "BA1439298535", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 109201502171130_BatteryVoltage", "8110 mV", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 110201502171130_DeviceImei", "013454000647580", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 111201502171130_IPAddress", "10.241.165.150", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 112201502171130_IPDNS1", "139.130.4.4", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 113201502171130_IPDNS2", "203.50.2.71", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 114201502171130_IPGatewayAddress", "10.241.165.151", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 115201502171130_IPSubnetMask", "255.255.255.255", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 116201502171130_PPID", "0112060010896028", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 117201502171130_RadioAccessTechnology", "3G", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 118201502171130_SignalFrequency", "850 MHz", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 119201502171130_SignalStrength", "23 RSSI", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 120201502171130_SimImsi", "505013499210493", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 121201502181243_BatteryCapacity", "1700 mAH", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 122201502181243_BatteryRemainingLevel", "99 % ", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 123201502181243_BatterySerialNumber", "BA1439298535", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 124201502181243_BatteryVoltage", "8280 mV", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 125201502181243_DeviceImei", "013454000647580", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 126201502181243_IPAddress", "10.241.6.57", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 127201502181243_IPDNS1", "139.130.4.4", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 128201502181243_IPDNS2", "203.50.2.71", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 129201502181243_IPGatewayAddress", "10.241.6.58", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 130201502181243_IPSubnetMask", "255.255.255.255", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 131201502181243_PPID", "0112060010896028", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 132201502181243_RadioAccessTechnology", "3G", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 133201502181243_SignalFrequency", "2100 MHz", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 134201502181243_SignalStrength", "15 RSSI", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 135201502181243_SimImsi", "505013499210493", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 136201502181248_BatteryCapacity", "1700 mAH", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 137201502181248_BatteryRemainingLevel", "99 % ", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 138201502181248_BatterySerialNumber", "BA1439298535", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 139201502181248_BatteryVoltage", "8280 mV", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 140201502181248_DeviceImei", "013454000647580", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 141201502181248_IPAddress", "10.241.6.57", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 142201502181248_IPDNS1", "139.130.4.4", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 143201502181248_IPDNS2", "203.50.2.71", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 144201502181248_IPGatewayAddress", "10.241.6.58", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 145201502181248_IPSubnetMask", "255.255.255.255", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 146201502181248_PPID", "0112060010896028", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 147201502181248_RadioAccessTechnology", "3G", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 148201502181248_SignalFrequency", "2100 MHz", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 149201502181248_SignalStrength", "17 RSSI", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 150201502181248_SimImsi", "505013499210493", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 151201502181546_BatteryCapacity", "1700 mAH", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 152201502181546_BatteryRemainingLevel", "98 % ", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 153201502181546_BatterySerialNumber", "BA1439298535", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 154201502181546_BatteryVoltage", "8210 mV", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 155201502181546_DeviceImei", "013454000647580", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 156201502181546_IPAddress", "10.241.154.58", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 157201502181546_IPDNS1", "139.130.4.4", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 158201502181546_IPDNS2", "203.50.2.71", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 159201502181546_IPGatewayAddress", "10.241.154.59", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 160201502181546_IPSubnetMask", "255.255.255.255", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 161201502181546_PPID", "0112060010896028", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 162201502181546_RadioAccessTechnology", "3G", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 163201502181546_SignalFrequency", "2100 MHz", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 164201502181546_SignalStrength", "14 RSSI", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 165201502181546_SimImsi", "505013499210493", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 166201502181551_BatteryCapacity", "1700 mAH", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 167201502181551_BatteryRemainingLevel", "98 % ", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 168201502181551_BatterySerialNumber", "BA1439298535", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 169201502181551_BatteryVoltage", "8210 mV", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 170201502181551_DeviceImei", "013454000647580", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 171201502181551_IPAddress", "10.241.154.58", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 172201502181551_IPDNS1", "139.130.4.4", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 173201502181551_IPDNS2", "203.50.2.71", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 174201502181551_IPGatewayAddress", "10.241.154.59", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 175201502181551_IPSubnetMask", "255.255.255.255", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 176201502181551_PPID", "0112060010896028", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 177201502181551_RadioAccessTechnology", "3G", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 178201502181551_SignalFrequency", "2100 MHz", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 179201502181551_SignalStrength", "18 RSSI", TMS_PARAMETER_TYPE_DIAGNOSTIC },

	{ " 180201502181551_SimImsi", "505013499210493", TMS_PARAMETER_TYPE_DIAGNOSTIC },

};


#define SEND_DEFINED_NUM_PARAMS			0
#define NUM_DIAG_PARAMS_TO_SEND			10000

void createParamList(struct tmsAppInfo *appInfo, uint32 param_mask)
{
	int i, entry_index;
	int num_params = 0;
	struct tmsAppParameter* parameter_array = NULL;


	/* Find out how many parameters to send */
	for (i = 0; i < (sizeof(custom_parameters) / sizeof(struct tmsAppParameter)); i++)
	{
		if (custom_parameters[i].parameterType & param_mask)
		{
			num_params++;
			//LogMsg("%s: custom_parameter[%d] (type = %d) matches mask 0x%08x - num_params incremented to %d\n", __FUNCTION__, i, custom_parameters[i].parameterType, param_mask, num_params);
		}
	}


#if SEND_DEFINED_NUM_PARAMS
	if (param_mask & TMS_PARAMETER_TYPE_DIAGNOSTIC)
	{
		LogMsg("%s: Allocating %d bytes for %d parameters\n", __FUNCTION__, NUM_DIAG_PARAMS_TO_SEND * sizeof(struct tmsAppParameter), NUM_DIAG_PARAMS_TO_SEND);
		parameter_array = malloc(NUM_DIAG_PARAMS_TO_SEND * sizeof(struct tmsAppParameter));
	}
	else
#endif
	{
		LogMsg("%s: Allocating %d bytes for %d parameters\n", __FUNCTION__, num_params * sizeof(struct tmsAppParameter), num_params);
		parameter_array = malloc(num_params * sizeof(struct tmsAppParameter));
	}
	if (parameter_array)
	{
		memset(parameter_array, 0, (num_params * sizeof(struct tmsAppParameter)));
		for (i = 0, entry_index = 0; ((i < (sizeof(custom_parameters) / sizeof(struct tmsAppParameter))) && (entry_index < num_params)); i++)
		{
			if (custom_parameters[i].parameterType & param_mask)
			{
				//LogMsg("%s: custom_parameter[%d] (type = %d) matches mask 0x%08x - adding to index %d\n", __FUNCTION__, i, custom_parameters[i].parameterType, param_mask, entry_index);
				memcpy(&parameter_array[entry_index], &custom_parameters[i], sizeof(struct tmsAppParameter));
				entry_index++;
			}
		}
		appInfo->parameterCount = num_params;
	}
	else
		appInfo->parameterCount = 0;


#if SEND_DEFINED_NUM_PARAMS
	if (param_mask & TMS_PARAMETER_TYPE_DIAGNOSTIC)
	{
		for (entry_index = num_params; entry_index < NUM_DIAG_PARAMS_TO_SEND; entry_index++)
		{
			struct tmsAppParameter temp_param;

			sprintf(temp_param.parameterName, "temp_parm_%d", entry_index);
			sprintf(temp_param.parameterValue, "%d", entry_index);
			temp_param.parameterType = TMS_PARAMETER_TYPE_DIAGNOSTIC;

			//LogMsg("%s: adding temp_parm %s to index %d\n", __FUNCTION__, temp_param.parameterName, entry_index);
			memcpy(&parameter_array[entry_index], &temp_param, sizeof(struct tmsAppParameter));
		}
		appInfo->parameterCount = NUM_DIAG_PARAMS_TO_SEND;
	}
#endif



	appInfo->parameterList = parameter_array;
}


static int HandleApplicationEvent(struct tmsEventData eventData)
{
	int param_count;
	int ret_val, result;
	FILE *fd;
	char* pszBaseName;
	char disp_str[256];
	char param_file_copy_name[MAX_TMS_FILENAME_LENGTH];
	struct tmsAppInfo appInfo;


	appEvent = &eventData;

#if 1

	if (app->appStatus)
	{
		evas_object_hide(app->appStatus);
		evas_object_del(app->appStatus);
		app->appStatus = NULL;
	}
	if (app->appStatus2)
	{
		evas_object_hide(app->appStatus2);
		evas_object_del(app->appStatus2);
		app->appStatus2 = NULL;
	}
	memset(disp_str, 0, sizeof(disp_str));
	printf("%s: Received App Message - %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
	sprintf(disp_str, "Received App Message - %s", msgType_to_string(appEvent->evtType));
	CreateText(app, &app->appStatus, "Vera", 20, disp_str, ALIGN_CENTER, (PAD_SPACE + 55), 255, 192, 192);

	LogMsg("%s: Got an application event\n\tStatus = %s\n\tType = %s\n\tMessage ID = %d\n\tFilename = %s\n\tFileLocation = %s\n\tEvent Mask = 0x%08x\n\tTransaction Type = %s\n",
		__FUNCTION__, status_to_string(appEvent->status), msgType_to_string(appEvent->evtType), appEvent->handle, appEvent->filename,
		appEvent->filepath, appEvent->eventMask, appEvent->transactionType);

	printf("handler: evas_render() on tid %u, pid %d\n", (unsigned int)pthread_self(), getpid());
	evas_render(app->fp->e);
	printf("handler: evas_render() returned on tid %u, pid %d\n", (unsigned int)pthread_self(), getpid());
#endif


	if (app->appStatus2)
	{
		evas_object_hide(app->appStatus2);
		evas_object_del(app->appStatus2);
		app->appStatus2 = NULL;
	}

	switch (appEvent->evtType)
	{
	case TMS_EVT_CALL_SERVER_RESPONSE:
		memset(disp_str, 0, sizeof(disp_str));
		if (appEvent->status == TMS_STATUS_SUCCESS)
		{
			sleep(2);
			sprintf(disp_str, "Call Server SUCCESSFUL");
			CreateText(app, &app ->appStatus, "Vera", 20, disp_str, ALIGN_CENTER, (PAD_SPACE + 55), 255, 192, 192);
		}
		else
		{
			sprintf(disp_str, "Call Server FAILED");
			CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		}

		printf("%s: adding contact server button back\n", __FUNCTION__);
		if (app->contactServerButton)
		{
			evas_object_show(app->contactServerButton);
		}
		break;

	case TMS_EVT_SET_APP_STATE:
		LogMsg("%s: Handling %s - eventMask = 0x%08x\n", __FUNCTION__, msgType_to_string(appEvent->evtType), appEvent->eventMask);
		/* Call service to set app state */

		if (appEvent->eventMask & TMS_EVENT_NOTIFY_CLOCK_UPDATE)
		{
#if 0
			LogMsg("%s: Delaying for CLOCK_UPDATE approval (0x%08x)\n", __FUNCTION__, appEvent->eventMask);
			LogMsg("%s: Calling tms_setApplicationState(%d, APP_STATUS_BUSY)\n", __FUNCTION__, appEvent->handle);
			ret_val = tms_setApplicationState(appEvent->handle, APP_STATUS_BUSY);
			LogMsg("%s: tms_setApplicationState() returned %s - sleeping for 240 seconds\n", __FUNCTION__, status_to_string(ret_val));

			sleep(240);

			LogMsg("%s: Calling tms_setApplicationState(%d, APP_STATUS_BUSY) AGAIN\n", __FUNCTION__, appEvent->handle);
			ret_val = tms_setApplicationState(appEvent->handle, APP_STATUS_BUSY);
			LogMsg("%s: tms_setApplicationState() returned %s - sleeping for 240 seconds AGAIN\n", __FUNCTION__, status_to_string(ret_val));

			sleep(240);
#endif
		}

		if (postpone_installs && (appEvent->eventMask & TMS_EVENT_NOTIFY_INSTALL_DOWNLOAD))
		{
			LogMsg("%s: Calling tms_setApplicationState(%d, APP_STATUS_POSTPONE)\n", __FUNCTION__, appEvent->handle);
			ret_val = tms_setApplicationState(appEvent->handle, APP_STATUS_POSTPONE);
			LogMsg("%s: tms_setApplicationState() returned %s\n", __FUNCTION__, status_to_string(ret_val));
		}
		else if (delay_installs && (appEvent->eventMask & TMS_EVENT_NOTIFY_INSTALL_DOWNLOAD))
		{
			time_t cur_time = time(NULL);

			if (delay_time_expire == 0)
			{
				delay_time_expire = cur_time + delay_time;
			}

			if (delay_time_expire <= cur_time)
			{
				LogMsg("%s: delay_time is EXPIRED - Calling tms_setApplicationState(%d, APP_STATUS_FREE)\n", __FUNCTION__, appEvent->handle);
				ret_val = tms_setApplicationState(appEvent->handle, APP_STATUS_FREE);
				LogMsg("%s: tms_setApplicationState() returned %s\n", __FUNCTION__, status_to_string(ret_val));
				delay_time_expire = 0;
			}
			else
			{
				LogMsg("%s: delay_time (%d) expires in %u seconds Calling tms_setApplicationState(%d, APP_STATUS_BUSY)\n", __FUNCTION__, delay_time, (delay_time_expire - cur_time), appEvent->handle);
				ret_val = tms_setApplicationState(appEvent->handle, APP_STATUS_BUSY);
				LogMsg("%s: tms_setApplicationState() returned %s\n", __FUNCTION__, status_to_string(ret_val));
			}
		}
		else
		{
			LogMsg("%s: Calling tms_setApplicationState(%d, APP_STATUS_FREE)\n", __FUNCTION__, appEvent->handle);
			ret_val = tms_setApplicationState(appEvent->handle, APP_STATUS_FREE);
		}

		memset(disp_str, 0, sizeof(disp_str));
		sprintf(disp_str, "tms_setApplicationState() returned %s", status_to_string(ret_val));
		CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		break;

	case TMS_EVT_GET_TMS_CONFIG_RESPONSE:
		memset(disp_str, 0, sizeof(disp_str));
		if (appEvent->status == TMS_STATUS_SUCCESS)
		{
			sleep(2);
			sprintf(disp_str, "Get Config Location SUCCESSFUL");
			CreateText(app, &app ->appStatus, "Vera", 20, disp_str, ALIGN_CENTER, (PAD_SPACE + 55), 255, 192, 192);


			if (strlen(appEvent->filename))
			{
				sprintf(disp_str, "Config file located at %s", appEvent->filename);
			}
			else
			{
				sprintf(disp_str, "No Config File Location Returned");
			}

			CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		}
		else
		{
			sprintf(disp_str, "Get Config Location FAILED");
			CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		}

		printf("%s: adding get config button back\n", __FUNCTION__);
		if (app->getConfigButton)
		{
			evas_object_show(app->getConfigButton);
		}
		break;

	case TMS_EVT_SET_TMS_CONFIG_RESPONSE:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		/* Call service to set app state */

		sprintf(disp_str, "Config Update returned %s", status_to_string(appEvent->status));
		CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);

		printf("%s: adding set config button back\n", __FUNCTION__);
		if (app->getConfigButton)
		{
			evas_object_show(app->getConfigButton);
		}
		break;

	case TMS_EVT_SET_APP_INFO:
		LogMsg("%s: Handling %s (mask = 0x%08x)\n", __FUNCTION__, msgType_to_string(appEvent->evtType), appEvent->eventMask);
		/* Call service to set app state */

		memset(&appInfo, 0, sizeof(struct tmsAppInfo));

		createParamList(&appInfo, appEvent->eventMask);

		LogMsg("%s: Calling tms_setApplicationInfo(%d) - appInfo.parameterCount = %d, appInfo.parameterList = 0x%08x\n", __FUNCTION__, appEvent->handle,
			appInfo.parameterCount, appInfo.parameterList);
		for (param_count = 0; param_count < appInfo.parameterCount; param_count++)
		{
			LogMsg("\tParameter %d: name = %s, value = %s, type = %d\n", param_count, appInfo.parameterList[param_count].parameterName,
				appInfo.parameterList[param_count].parameterValue, appInfo.parameterList[param_count].parameterType);
		}

		ret_val = tms_setApplicationInfo(appEvent->handle, appInfo);

		free(appInfo.parameterList);

		memset(disp_str, 0, sizeof(disp_str));
		sprintf(disp_str, "tms_setApplicationInfo() returned %s", status_to_string(ret_val));
		CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		LogMsg("%s: tms_setApplicationInfo() returned %s (%d)", __FUNCTION__, status_to_string(ret_val), ret_val);
		break;

	case TMS_EVT_SET_PARM_LIST:

		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		/* Call service to set parameter list */
		fd = fopen(APP_PARM_DATA_FILE, "w");

		if (fd)
		{
			fwrite(APP_PARM_DATA_FILE_TEST_DATA, strlen(APP_PARM_DATA_FILE_TEST_DATA), 1, fd);
			fclose(fd);
		}
		else
			LogMsg("%s: errno set in opening file (%s) = %d\n", __FUNCTION__, APP_PARM_DATA_FILE, errno);

		LogMsg("%s: Calling tms_setApplicationParameterList(%d, 1, %s)\n", __FUNCTION__, appEvent->handle, APP_PARM_DATA_FILE);
		ret_val = tms_setApplicationParameterList(appEvent->handle, 1, APP_PARM_DATA_FILE);

		memset(disp_str, 0, sizeof(disp_str));
		sprintf(disp_str, "tms_setApplicationParameterList() returned %s", status_to_string(ret_val));
		CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		break;

	case TMS_EVT_GET_FILE:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		memset(param_file_copy_name, 0, sizeof(param_file_copy_name));
		/* Make sure filename is valid */
		if (strcmp(appEvent->filename, "test.ini") == 0)
		{
			int copy_result;
			int chmod_ret, chown_ret;
			char groupname[16];
			struct group *psGroup;
			struct passwd *psPasswd;


			psPasswd = getpwuid(getuid());

			sprintf(param_file_copy_name, "/tmp/share/test.ini");
			LogMsg("%s: Copying %s to %s\n", __FUNCTION__, appEvent->filename, param_file_copy_name);
			copy_result = CopyFile("./test.ini", param_file_copy_name);
			LogMsg("%s: Copy result = %d\n", __FUNCTION__, copy_result);

			if (psPasswd)
				sprintf(groupname, "%ssys", psPasswd->pw_name);
			else
				sprintf(groupname, "sys");
			psGroup = getgrnam(groupname);

			if (psGroup)
			{
				chmod_ret = chmod(param_file_copy_name, 0640 );
				chown_ret = chown(param_file_copy_name, getuid(), psGroup->gr_gid);
				LogMsg("%s: chmod to 640 returned (%d) - chown to %s group returned %d\n", __FUNCTION__, chmod_ret, groupname, chown_ret);
			}
			else
				chmod_ret = chmod(param_file_copy_name, 0644);

			if (copy_result == 0)
				result = TMS_STATUS_SUCCESS;
			else
			{
				sprintf(disp_str, "Error %d copying test.ini to %s", copy_result, param_file_copy_name);
				result = TMS_STATUS_ERROR;
			}
		}
		else
		{
			sprintf(disp_str, "Invalid Parameter File to Get-%s", appEvent->filename);
			LogMsg("%s: Invalid parameter filename - %s\n", __FUNCTION__, appEvent->filename);
			result = TMS_STATUS_FILENAME_ERROR;
		}


		LogMsg("%s: Calling tms_getApplicationFileAvailable(%d, %s, %s, TRUE)\n", __FUNCTION__, appEvent->handle, status_to_string(result), param_file_copy_name);
		ret_val = tms_getApplicationFileAvailable(appEvent->handle, result, param_file_copy_name, TRUE);

		memset(disp_str, 0, sizeof(disp_str));
		sprintf(disp_str, "tms_getApplicationFileAvailable() returned %s", status_to_string(ret_val));

		CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		break;

	case TMS_EVT_PUT_FILE:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		memset(param_file_copy_name, 0, sizeof(param_file_copy_name));
		strcpy(param_file_copy_name, appEvent->filename);
		pszBaseName = basename(param_file_copy_name);

		/* Make sure filename is valid */
		if (strcmp(pszBaseName, "test.ini") == 0)
		{
			int copy_result;

			LogMsg("%s: Copying %s to ./test.ini\n", __FUNCTION__, appEvent->filename);
			copy_result = CopyFile(appEvent->filename, "./flash/test.ini");
			LogMsg("%s: Copy result = %d\n", __FUNCTION__, copy_result);

			if (copy_result == 0)
				result = TMS_STATUS_SUCCESS;
			else
			{
				sprintf(disp_str, "Error %d copying %s to ./flash/test.ini", copy_result, appEvent->filename);
				result = TMS_STATUS_ERROR;
			}

			LogMsg("%s: Calling tms_setFileOperationResult(%d, %s, TMS_EVT_PUT_FILE)\n", __FUNCTION__, appEvent->handle, status_to_string(result));
			ret_val = tms_setFileOperationResultWithDescription(appEvent->handle, result, TMS_EVT_PUT_FILE, "App PUT successful");

			memset(disp_str, 0, sizeof(disp_str));
			if (result == TMS_STATUS_SUCCESS)
				sprintf(disp_str, "tms_setFileOperationResult() returned %s", status_to_string(ret_val));
			else
				CreateText(app, &app->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		}
		else
		{
			if (app->appStatus2)
			{
				evas_object_hide(app->appStatus2);
				evas_object_del(app->appStatus2);
				app->appStatus2 = NULL;
			}
			evas_object_text_text_set(app->appStatus, "Received Content Update - Accept or Reject");

			LogMsg("svc_tms_tester: Got a content event\n\tStatus = %s\n\tType = %d\n\tContent ID = %d\n\tFilename = %s\n\tfilepath = %s\n",
				status_to_string(appEvent->status), appEvent->putFileType, appEvent->handle, appEvent->filename, appEvent->filepath);

			CreateButton(app, &app->acceptButton, "Accept Content", (PAD_SPACE * 2), 185, 140, 40, Clicked_AcceptButtonCB);
			CreateButton(app, &app->postponeButton, "Postpone Content", 2 * (PAD_SPACE * 2) + 140, 185, 140, 40, Clicked_PostponeButtonCB);
			CreateButton(app, &app->errorButton, "Error", 2 * (PAD_SPACE * 2) + (140 * 3), 185, 140, 40, Clicked_ErrorButtonCB);

			evas_render(app->fp->e);

			waiting_for_usr_response = TRUE;

			while (waiting_for_usr_response)
			{
				printf("%s: Waiting for user response\n", __FUNCTION__);
				sleep(3);
			}


#if (!DELETE_BUTTONS_IN_CB)
			printf("%s: Got user response - deleting buttons\n", __FUNCTION__);

			/* We can remove the accept button */
			if (app->acceptButton)
			{
				evas_object_hide(app->acceptButton);
				evas_object_del(app->acceptButton);
				app->acceptButton = NULL;
			}

			/* We can remove the postpone button */
			if (app->postponeButton)
			{
				evas_object_hide(app->postponeButton);
				evas_object_del(app->postponeButton);
				app->postponeButton = NULL;
			}

			if (app->errorButton)
			{
				evas_object_hide(app->errorButton);
				evas_object_del(app->errorButton);
				app->errorButton = NULL;
			}
#else
			printf("%s: Got user response\n", __FUNCTION__);
#endif
		}
		break;

	case TMS_EVT_DEL_FILE:
		LogMsg("%s: Handling %s - Delete Filename = %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType), appEvent->filename);
		/* Call service and just respond with a SUCCESS message */

		LogMsg("%s: Calling tms_setFileOperationResult(%d, TMS_STATUS_ERROR, TMS_EVT_DEL_FILE) - svc_tms_tester does not delete files\n", __FUNCTION__, appEvent->handle);
		ret_val = tms_setFileOperationResultWithDescription(appEvent->handle, TMS_STATUS_ERROR, TMS_EVT_DEL_FILE, "App DELETE not supported");

		memset(disp_str, 0, sizeof(disp_str));
		sprintf(disp_str, "tms_setFileOperationResult() returned %s", status_to_string(ret_val));
		CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		break;

	case TMS_EVT_NOTIFICATION:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));

		/* Just display a message with the notification event */
		memset(disp_str, 0, sizeof(disp_str));
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_REBOOT_DEVICE)
		{
			strcat(disp_str, "Device Rebooting ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_RESTART_APPS)
		{
			struct tmsReturn tms_ret;
			strcat(disp_str, "Restarting Apps ");

			LogMsg("svc_tms_tester: Un-Registering App %s\n", "svc_tms_tester");
			tms_ret = tms_unregisterApplication("svc_tms_tester");
			if (tms_ret.status == TMS_STATUS_REQUESTED)
				LogMsg("svc_tms_tester: Un-Register requested\n");


		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_DOWNLOAD_STARTED)
		{
			strcat(disp_str, "Download Started-file=");
			strcat(disp_str, appEvent->filename);
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_DOWNLOAD_PERCENT)
		{
			char percent_str[16];

			memset(percent_str, 0, sizeof(percent_str));
			sprintf(percent_str, "%d", appEvent->notificationData.downloadPercent);

			strcat(disp_str, "Download Percent Update-file=");
			strcat(disp_str, appEvent->filename);
			strcat(disp_str, "at ");
			strcat(disp_str, percent_str);
			strcat(disp_str, " percent ");
			LogMsg("%s: %s download at %d percent\n", __FUNCTION__, appEvent->filename, appEvent->notificationData.downloadPercent);
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_DOWNLOAD_COMPLETE)
		{
			strcat(disp_str, "Download Complete-file=");
			strcat(disp_str, appEvent->filename);
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_INSTALL_DOWNLOAD)
		{
			strcat(disp_str, "Installing downloaded file - ");
			strcat(disp_str, appEvent->filename);
			strcat(disp_str, " ");
			LogMsg("%s: Installing download file %s - Post Install Action = %d\n", __FUNCTION__, appEvent->filename, appEvent->notificationData.postInstallAction);
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_MAINTENANCE_END)
		{
			strcat(disp_str, "Maintenance Completed");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_HEARTBEAT_RESULT)
		{
			strcat(disp_str, "Heartbeat Completed - ");
			if (appEvent->status == TMS_STATUS_SUCCESS)
				strcat(disp_str, "SUCCESSFUL");
			else
				strcat(disp_str, "FAILURE");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_CONTACT_SERVER_START)
		{
			strcat(disp_str, "Contacting Server");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_CONTACT_SERVER_END)
		{
			strcat(disp_str, "Server Contact Complete");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_CONTACT_SERVER_APPROVAL)
		{
			/* Should never get this notification.  It is a duplicate of CONTACT_SERVER_START, but when registering
			for this notifcation, it means the app wants to approve server contact */
			strcat(disp_str, "Contact Server Approval");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_CONTENT_UPDATES)
		{
			strcat(disp_str, "Content Update");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_CLOCK_UPDATE)
		{
			strcat(disp_str, "Clock Updated");
			strcat(disp_str, " ");
		}
		if (appEvent->eventMask & TMS_EVENT_NOTIFY_SERVER_ERRORS)
		{
			char temp_str[128];

			LogMsg("%s: Server Error Notification - Error = %s (%d)\n", __FUNCTION__, appEvent->filename, appEvent->status);
			sprintf(temp_str, "Server Error %d", appEvent->status);
			strcat(disp_str, temp_str);
			strcat(disp_str, " ");
		}
		CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		break;

	case TMS_EVT_DO_TRANSACTION:
		LogMsg("%s: Handling %s - transaction = %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType), appEvent->transactionType);
		/* Call service and just respond with a SUCCESS message */

		LogMsg("%s: Calling tms_setTransactionResult(%d, TMS_STATUS_SUCCESS)\n", __FUNCTION__, appEvent->handle);
		ret_val = tms_setTransactionResult(appEvent->handle, TMS_STATUS_SUCCESS);

		memset(disp_str, 0, sizeof(disp_str));
		sprintf(disp_str, "tms_setTransactionResult() returned %s", status_to_string(ret_val));
		CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		break;

	case TMS_EVT_REGISTER_APP_RESPONSE:
		memset(disp_str, 0, sizeof(disp_str));
		if (appEvent->status == TMS_STATUS_SUCCESS)
		{
			sprintf(disp_str, "Registration SUCCESSFUL");
			CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);

			//tms_setApplicationState(-1, APP_STATUS_BUSY);
		}
		else
		{
			sprintf(disp_str, "Registration FAILED");
			CreateText(app, &app ->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		}
		break;

	case TMS_EVT_UNREGISTER_APP_RESPONSE:
		memset(disp_str, 0, sizeof(disp_str));
		if (appEvent->status == TMS_STATUS_SUCCESS)
		{
			sprintf(disp_str, "Un-Registration SUCCESSFUL");
			CreateText(app, &app->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		}
		else
		{
			sprintf(disp_str, "Un-Registration FAILED");
			CreateText(app, &app->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		}
		break;


	case TMS_EVT_GET_SERVER_INSTANCE:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		if (appEvent->status == TMS_STATUS_SUCCESS)
			sprintf(disp_str, "GetServerInstance returned %s SUCCESSFULLY", appEvent->filename);
		else
			sprintf(disp_str, "GetServerInstance returned ERROR %s", status_to_string(appEvent->status));

		CreateText(app, &app->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		break;

	case TMS_EVT_LOCK_SERVER_INSTANCE:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		if (appEvent->status == TMS_STATUS_SUCCESS)
		{
			if (backup_requested)
			{
				backup_requested = FALSE;
				backup_active = TRUE;
				strcpy(prevInstance1, appEvent->filename);
				sprintf(disp_str, "LockServerInstance 'backup' SUCCESSFUL - Old Instance = %s", prevInstance1);
				LogMsg("LockServerInstance 'backup' SUCCESSFUL - Old Instance = %s\n", prevInstance1);
			}
			else
			{
				third_active = TRUE;
				strcpy(prevInstance2, appEvent->filename);
				sprintf(disp_str, "LockServerInstance 'third' SUCCESSFUL - Old Instance = %s", prevInstance2);
				LogMsg("LockServerInstance 'third' SUCCESSFUL - Old Instance = %s\n", prevInstance2);
			}
		}
		else
			sprintf(disp_str, "LockServerInstance returned ERROR %s", status_to_string(appEvent->status));
		CreateText(app, &app->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);

		printf("%s: adding swap config1 button back\n", __FUNCTION__);
		if (app->swapConfig1Button)
		{
			evas_object_show(app->swapConfig1Button);
		}

		printf("%s: adding swap config2 button back\n", __FUNCTION__);
		if (app->swapConfig2Button)
		{
			evas_object_show(app->swapConfig2Button);
		}

		break;


	case TMS_EVT_RELEASE_SERVER_INSTANCE:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		if (appEvent->status == TMS_STATUS_SUCCESS)
		{
			if (backup_active)
			{
				backup_active = FALSE;
				sprintf(disp_str, "ReleaseServerInstance 'backup' SUCCESSFUL");
				LogMsg("ReleaseServerInstance 'backup' SUCCESSFUL - %s restored\n", prevInstance1);
			}
			else if (third_active)
			{
				third_active = FALSE;
				sprintf(disp_str, "ReleaseServerInstance 'third' SUCCESSFUL");
				LogMsg("ReleaseServerInstance 'third' SUCCESSFUL - %s restored\n", prevInstance2);
			}
		}
		else
			sprintf(disp_str, "ReleaseServerInstance returned ERROR %s", status_to_string(appEvent->status));
		CreateText(app, &app->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);

		printf("%s: adding swap config1 button back\n", __FUNCTION__);
		if (app->swapConfig1Button)
		{
			evas_object_show(app->swapConfig1Button);
		}

		printf("%s: adding swap config2 button back\n", __FUNCTION__);
		if (app->swapConfig2Button)
		{
			evas_object_show(app->swapConfig2Button);
		}

		break;

	case TMS_EVT_APP_ALERT_RESULT:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		if (appEvent->status == TMS_STATUS_SUCCESS)
			sprintf(disp_str, "App Alert sent SUCCESSFULLY");
		else
			sprintf(disp_str, "App Alert ERROR %s", status_to_string(appEvent->status));

		CreateText(app, &app->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		break;

	case TMS_EVT_CLEAR_APP_INFO_RESULT:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		if (appEvent->status == TMS_STATUS_SUCCESS)
			sprintf(disp_str, "Clear App Info SUCCESSFUL");
		else
			sprintf(disp_str, "Clear App Info ERROR %s", status_to_string(appEvent->status));

		CreateText(app, &app->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		break;

	case TMS_EVT_API_ERRORS:
		LogMsg("%s: Handling %s\n", __FUNCTION__, msgType_to_string(appEvent->evtType));
		/*
		 * This is critical ERROR, VHQ Agent can't trust data exchange with application start from this moment.
		 * Verify your code and provide required changes!
		 *
		 * Future TMS processing with VHQ Agent is not safe. The best case here is to unregister and close/restart application.
		 * You can use tms_sendApplicationAlert() to notify server about problems. Please do not use it for production build.
		 *
		 * Do not use next TMS APIs here:
		 * - tms_setTransactionResult()
		 * - tms_setApplicationState(with handle)
		 * - tms_setApplicationInfo()
		 * - tms_setApplicationParameterList()
		 * - tms_getApplicationFileAvailable()
		 * - tms_setFileOperationResult()
		 * - tms_setFileOperationResultWithDescription()
		 * - tms_setTransactionResult()
		 * since it will cause closed loop behavior, closed loop up to timeout on VHQ Agent side.
		 */
		if (appEvent->status == TMS_STATUS_API_SYNC_ERROR)
		{
			struct tmsReturn tms_ret;
#ifdef DEBUG_BUILD
			/* Let's notify server that we have some problems */
			sprintf(disp_str, "API processing error %s: %s", status_to_string(appEvent->status), appEvent->filename);
			tms_ret = tms_sendApplicationAlert(status_to_string(appEvent->status), TMS_ALERT_SEVERITY_HIGH, appEvent->filename);
			if (tms_ret.status == TMS_STATUS_REQUESTED)
				LogMsg("svc_tms_tester: Alert requested\n");
#endif
			/* Unregister application */
			LogMsg("svc_tms_tester: Un-Registering App %s\n", "svc_tms_tester");
			tms_ret = tms_unregisterApplication("svc_tms_tester");
			if (tms_ret.status == TMS_STATUS_REQUESTED)
				LogMsg("svc_tms_tester: Un-Register requested\n");
			/* Close application */
			exit(0);
		} else
			sprintf(disp_str, "API processing error: %s (%d)", status_to_string(appEvent->status), appEvent->status);

		CreateText(app, &app->appStatus2, "Vera", 10, disp_str, ALIGN_CENTER, (PAD_SPACE + 90), 255, 192, 192);
		break;
	}

	printf("%s: evas_render(app = 0x%08x, app->fp = 0x%08x, app->fp->e = 0x%08x)\n", __FUNCTION__, (int)app, app ? (int)app->fp : 0, app->fp ? (int)app->fp->e : 0);
	evas_render(app->fp->e);

	printf("HandleApplicationEvent: returning 0\n");
	return 0;
}

static int HandleApplicationEvent_New(struct tmsEventData* eventData, void* data)
{
	int ret_val;
	tmsEventCallback cb_func = (tmsEventCallback)data;

	pthread_mutex_lock(&evas_mutex);
	printf("HandleApplicationEvent_New: +\n");
	LogMsg("%s: Callback data cb_func = 0x%08x\n", __FUNCTION__, (int)cb_func);
	ret_val = cb_func(*eventData);

	printf("HandleApplicationEvent_New: returning %d\n", ret_val);
	pthread_mutex_unlock(&evas_mutex);
	return ret_val;
}




// Makes interface from WDE PHP simple.
// Note this loop/thread will do the app integration to TMS
void* MainLoopApp(void* pvData)
{
	struct tmsReturn regRtn;
	struct passwd *psPasswd;

	printf("%s: MainLoopApp started\n", __FUNCTION__);

	psPasswd = getpwuid(getuid());
	
	while (1)
	{
		LogMsg("svc_tms_tester: TMS App Ifc Version (tms_GetVersion) = %s\n", tms_GetVersion());

		if (psPasswd)
			LogMsg("svc_tms_tester: Registering App %s as %s - HandleApplicationEvent (@ 0x%08x) = 0x%08x\n", "svc_tms_tester", psPasswd->pw_name, &HandleApplicationEvent, HandleApplicationEvent);
		//regRtn = tms_registerApplication("svc_tms_tester", 0xffffffff, (unsigned int)HandleApplicationEvent);
		regRtn = tms_registerApplication2("svc_tms_tester", 0xffffffff, HandleApplicationEvent_New, HandleApplicationEvent);
		if (regRtn.status == TMS_STATUS_REQUESTED)
		{
			LogMsg("svc_tms_tester: Register App REQUESTED\n");

			LogMsg("svc_tms_tester: TMS Agent Version (tms_GetAgentVersion) = %s\n", tms_GetAgentVersion());
			if (errno != 0)
			{
				LogMsg("svc_tms_tester ERROR: tms_GetAgentVersion() returned errno %d\n", errno);
			}

			pthread_mutex_lock(&evas_mutex);

			if (app->appStatus2)
			{
				evas_object_hide(app->appStatus2);
				evas_object_del(app->appStatus2);
				app->appStatus2 = NULL;
			}

			evas_object_text_text_set(app ->appStatus, "Waiting For APP Event");
			printf("main_thread: evas_render() on tid %d, pid %d\n", (unsigned int)pthread_self(), getpid());
			evas_render(app->fp->e);
			printf("main_thread: evas_render() returned on tid %d, pid %d\n", (unsigned int)pthread_self(), getpid());

			pthread_mutex_unlock(&evas_mutex);

#if 0
			/* Sleep for 5 minutes and then say we are free */
			LogMsg("svc_tms_tester: sleeping 5 minutes\n", errno, strerror(errno));
			sleep(60 * 5);
			LogMsg("svc_tms_tester: setting app state to FREE\n", errno, strerror(errno));
			tms_setApplicationState(-1, APP_STATUS_FREE);
#endif

			while (1)
			{
				int min_counter;

				LogMsg("svc_tms_tester: still running - sleeping for an hour\n", errno, strerror(errno));
				for (min_counter = 0; min_counter < 60; min_counter++)
				{
					//printf("sleep (60)\n");
					sleep(60);
					//printf("sleep returned\n");
				}
			}

		}
		else
		{
			LogMsg("svc_tms_tester ERROR: tms_registerApplication() returned an error - regRtn->status = %s, errno = %d\n\tRetrying in 30 seconds",
				status_to_string(regRtn.status), errno);
			sleep(30);
		}


	}

	return 0;

}















/*******************************************************************
* Used for the GUI version
*******************************************************************/




#ifndef DATAPATH
#define DATAPATH	"./flash"
#endif

#ifndef THEMES_DATAPATH
#define THEMES_DATAPATH	 DATAPATH "/themes"
#endif



static Evas_Object *textbx;



// Main function
int fp_main(int argc, char **argv)
{
	int MainloopAppRetVal;
	pthread_t MainLoopAppThread;
	struct appdata app_obj;	
	struct sigaction sa;

	sa.sa_sigaction = &sighandler;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART | SA_SIGINFO;
	sigemptyset(&sa.sa_mask);
	if (sigaction(SIGSEGV, &sa, NULL) == -1)
		printf("unable to setup sigaction on SIGSEGV\n");

	if (argc > 1)
	{
		if (strcmp(argv[1], "-p") == 0)
		{
			printf("svc_tms_tester: -p flag set - setting postpone_installs\n");
			postpone_installs = TRUE;
		}
		else if (strcmp(argv[1], "-d") == 0)
		{
			printf("svc_tms_tester: -d flag set - setting delay_installs\n");
			delay_installs = TRUE;
			if (argc > 2)
			{
				delay_time = atoi(argv[2]);
				printf("svc_tms_tester: delay time set to %d\n", delay_time);
			}
		}
	}

	memset(&app_obj, 0, sizeof(struct appdata));
	app = &app_obj;

	app ->fp = fp_init(&argc, argv);

	if (!app ->fp) return -1;
	if (fp_window_init(app ->fp, DATAPATH) < 0) return -1;

	ecore_evas_title_set(app ->fp ->ee, "Content Test App");	
	printf("%s: calling evas_font_path_append(%s)\n", __FUNCTION__, THEMES_DATAPATH);
	evas_font_path_append(app ->fp ->e, THEMES_DATAPATH);
	
	// Create Background Gradient
	printf("%s: calling CreateGradient\n", __FUNCTION__);
	CreateGradient(app);
	printf("%s: calling CreateControls\n", __FUNCTION__);
	CreateControls(app);


	/* Setup the evas mutex */
	while (pthread_mutex_init(&evas_mutex, NULL) != 0)
	{
		LogMsg("%s: Error creating evas mutex - RETRYING in 5 seconds!!!\n", __FUNCTION__);
		sleep(5);
	}


	printf("%s: Starting MainLoopApp on a separate thread - running on tid %d, pid %d\n", __FUNCTION__, (unsigned int)pthread_self(), getpid());
	MainloopAppRetVal = pthread_create(&MainLoopAppThread, NULL, MainLoopApp, NULL);
	if (MainloopAppRetVal != 0)
	{
		printf("%s ERROR: Could not start MainLoopApp thread - EXITING\n", __FUNCTION__);
		exit(-1);
	}
	else
		sleep(1);


	// Begin Main Loop
	fp_begin(app ->fp);

	printf("svc_tms_tester: reached end of fp_main - returning -1\n");
	return -1;
}

void CreateGradient(struct appdata *app)
{	
	// Create Background Gradient
	app ->bg = evas_object_gradient_add(app ->fp ->e);
	
	evas_object_gradient_color_add(app ->bg, 0, 0, 255, 255, 1);
	evas_object_gradient_color_add(app ->bg, 0, 0, 128, 255, 2);
	evas_object_gradient_color_add(app ->bg, 0, 0, 0, 255, 3);
	evas_object_gradient_color_add(app ->bg, 0, 0, 128, 255, 4);
	evas_object_gradient_angle_set(app ->bg, 45.0);
	evas_object_resize(app ->bg, app ->fp ->w, app ->fp ->h);
	evas_object_show(app ->bg);
}

void CreateControls(struct appdata *app)
{	
	//printf("%s: +\n", __FUNCTION__);
	// Title
	//printf("%s: creating title text\n", __FUNCTION__);
	CreateText(app, &app ->titleText, "Vera", 12, "MX9 TMS Interface Test App", ALIGN_CENTER, PAD_SPACE, 255, 255, 255);
	
	// App Status
	//printf("%s: creating labeltext1\n", __FUNCTION__);
	CreateText(app, &app ->labelText1, "Vera", 11, "App Status", ALIGN_CENTER, PAD_SPACE + 20, 224, 224, 224);	
	//printf("%s: creating textrect\n", __FUNCTION__);
	CreateRectangle(app, &app ->textRect, PAD_SPACE + 30, (app ->fp ->h / 2) + 10 + PAD_SPACE, 192, 192, 192, 64);	
	
	// App Status
	//printf("%s: creating appstatus\n", __FUNCTION__);
	CreateText(app, &app ->appStatus, "Vera", 25, "Waiting for event", ALIGN_CENTER, (PAD_SPACE + 55), 255, 192, 192);

	//printf("%s: creating horizline1\n", __FUNCTION__);
	CreateLine(app, &app ->horizLine1, PAD_SPACE, (PAD_SPACE + 180), (app ->fp ->w - PAD_SPACE), (PAD_SPACE + 180), 128, 128, 128);
	//printf("%s: creating horizline2\n", __FUNCTION__);
	CreateLine(app, &app ->horizLine2, PAD_SPACE, (PAD_SPACE + 223), (app ->fp ->w - PAD_SPACE), (PAD_SPACE + 223), 128, 128, 128);
	//printf("%s: creating vertline\n", __FUNCTION__);
	CreateLine(app, &app ->vertLine, 205, (PAD_SPACE + 180), 205, (PAD_SPACE + 223), 128, 128, 128);	
	//CreateButton(app, &app ->acceptButton, "Accept Content", (PAD_SPACE * 2), 185, 140, 40, Clicked_AcceptButtonCB);
	//CreateButton(app, &app ->rejectButton, "Reject Content", 2 * (PAD_SPACE * 2) + 140, 185, 140, 40, Clicked_RejectButtonCB);	
	//printf("%s: creating view log button\n", __FUNCTION__);
	CreateButton(app, &app ->viewlogButton, "View Log", app ->fp ->w - 140 - (PAD_SPACE * 4), 185, 140, 40, Clicked_ViewLogButtonCB);
	//printf("%s: creating contact server button\n", __FUNCTION__);
	CreateButton(app, &app ->contactServerButton, "Contact Server", app ->fp ->w - 140 - (PAD_SPACE * 4), 140, 140, 40, Clicked_ContactServerButtonCB);
	//printf("%s: creating get config button\n", __FUNCTION__);
#if CONFIG_BUTTON_IS_GET_CONFIG
	CreateButton(app, &app ->getConfigButton, "Get Config", app ->fp ->w - 280 - (PAD_SPACE * 5), 140, 140, 40, Clicked_GetConfigButtonCB);
#else
	CreateButton(app, &app ->getConfigButton, "Set Config", app ->fp ->w - 280 - (PAD_SPACE * 5), 140, 140, 40, Clicked_GetConfigButtonCB);
#endif
	CreateButton(app, &app->swapConfig1Button, "Swap Config 1", app->fp->w - 420 - (PAD_SPACE * 6), 140, 140, 40, Clicked_SwapConfig1ButtonCB);
	CreateButton(app, &app->swapConfig2Button, "Swap Config 2", app->fp->w - 560 - (PAD_SPACE * 7), 140, 140, 40, Clicked_SwapConfig2ButtonCB);
	//printf("%s: +\n", __FUNCTION__);
}

void CreateText(struct appdata *app, Evas_Object **pObj, const char *pFontFace, int iSize, const char *pText, int iX, int iY, int iRed, int iGreen, int iBlue)
{
	Evas_Object *obj;
	Evas_Coord w, h;

	if (pObj == NULL)
		return;
	
	if ((*pObj) != NULL)
	{
		printf("%s: *pObj exists - deleting\n", __FUNCTION__);
		evas_object_hide(*pObj);
		evas_object_del(*pObj);
		*pObj = NULL;
	}

	obj = evas_object_text_add(app ->fp ->e);
	//printf("%s: calling evas_object_text_font_source_set(%s)\n", __FUNCTION__, THEMES_DATAPATH);
	evas_object_text_font_source_set(obj, THEMES_DATAPATH);
	evas_object_text_font_set(obj, pFontFace, iSize);		
	evas_object_text_text_set(obj, pText);
	evas_object_color_set(obj, iRed, iGreen, iBlue, 255);	
	evas_object_geometry_get(obj, 0, 0, &w, &h);
	evas_object_resize(obj, app ->fp ->w, h);	
	
	switch (iX)
	{
		case ALIGN_LEFT: iX = PAD_SPACE; break;
		case ALIGN_CENTER: iX = (app ->fp ->w / 2) - (w / 2); break;
   		case ALIGN_RIGHT: iX = (app ->fp ->w - w - PAD_SPACE); break;
	}
	
	evas_object_move(obj, iX, iY);
	evas_object_show(obj);
	
	*pObj = obj;
}

void CreateRectangle(struct appdata *app, Evas_Object **pObj, int iY, int iH, int iRed, int iGreen, int iBlue, int iAlpha)
{
	Evas_Object *obj;
	Evas_Coord iX = PAD_SPACE;
	Evas_Coord iW = app ->fp ->w - PAD_SPACE;

	obj = evas_object_rectangle_add(app ->fp ->e);
	evas_object_move(obj, iX, iY);
	evas_object_resize(obj, iW, iH);
	evas_object_color_set(obj, iRed, iGreen, iBlue, iAlpha);
	evas_object_show(obj);
	*pObj = obj;	
}

void CreateLine(struct appdata *app, Evas_Object **pObj, int x1, int y1, int x2, int y2, int iRed, int iGreen, int iBlue)
{
	Evas_Object *obj;
	
	obj = evas_object_line_add(app ->fp ->e);
	evas_object_line_xy_set(obj, x1, y1, x2, y2);
	evas_object_color_set(obj, iRed, iGreen, iBlue, 255);
	evas_object_show(obj);		
	*pObj = obj;	
}

void CreateButton(struct appdata *app, Evas_Object **pObj, const char *pText, int iX, int iY, int iWidth, int iHeight, fp_button_clicked_cb cb)
{
	Evas_Object *obj;
	
	obj = fp_button_add_text(app ->fp, FP_TEXT_NORMAL, pText);
	fp_button_clicked_callback_add(obj, cb, app);
	fp_button_enable(obj);
	evas_object_move(obj, iX, iY);
	evas_object_resize(obj, iWidth, iHeight);	
	evas_object_show(obj);
	*pObj = obj;	
}


/*****************************************************************
 createScreen()
 create a empty screen containing only background

 Parameters:

 Returns:
 Evas Object
 *****************************************************************/
Evas_Object *createScreen(Evas_Object *screen, int xoffset, int yoffset, int width, int height)
{
	Evas_Object *sc;

	if (screen) {
		evas_object_hide(screen);
		evas_object_del(screen);
	}

	sc = fp_container_add(app->fp);
	evas_object_move(sc, xoffset, yoffset);
	if ( 0 >= width ) {
		width = app->fp->w - xoffset;
	}
	if ( 0 >= height ) {
		height = app->fp->h - yoffset;
	}
	evas_object_resize(sc, width, height);

	evas_object_show(sc);

	return sc;
}



char *readFile(char *fn, int mode)
{
	struct stat st;
	char *tptr, *memptr;
	int h,count;

	memptr=NULL;
	if (stat(fn,&st) == 0) {
		if (st.st_size>0) {
			memptr=malloc(st.st_size+1);
			if (memptr) {
				tptr=memptr;
				h=open(fn,O_RDONLY);
				if (h>=0) {
					if (!mode) {
						read(h,tptr,st.st_size);
						tptr[st.st_size] = 0; // null terminate
					} else {
						count=st.st_size;
						while (count) {
							read(h,tptr,1);
							if ( !((*tptr < ' ') && (*tptr != '\n')) )
								tptr++;
							count--;
						}
						tptr[0] = 0; // null terminate
					}
					close(h);
				} else {
					free(tptr);
					memptr=NULL;
				}
			}
		}
	}
	return memptr;
}



/*****************************************************************
 createTabBackground()
 create the basic background for tab views

 Parameters:
 tab_container - Container in which background is placed

 Returns:
 0 = success
 *****************************************************************/
int createTabBackground(void)
{
	Evas_Object *obj;
	int x, y, w, h;
	int containerX, containerY, containerW, containerH;

	evas_object_geometry_get(app->textRect, &containerX, &containerY, &containerW, &containerH);

	printf("%s: containerx=%d, containery=%d, containerw=%d, containerh=%d\r\n",__FUNCTION__,containerX,containerY,containerW,containerH);

	app->logContainer = createScreen(app->logContainer, containerX, containerY, containerW, containerH);

    // create the background
	obj = evas_object_rectangle_add( app->fp->e );
	evas_object_color_set(obj, 245, 246, 247, 255);
	evas_object_layer_set(obj, 75 - 1);
	evas_object_resize(obj, containerW, containerH);
	printf("%s: fp_container_object_add1(container = 0x%08x, obj = 0x%08x)\r\n",__FUNCTION__, (unsigned int)app->logContainer, (unsigned int)obj);
	fp_container_object_add(app->logContainer, obj, 0, 0); // relative to container x,y
	evas_object_show(obj);

	// add the watermark
	obj = fp_image_add(app->fp);
	fp_image_file_set(obj, "./flash/images/watermark.png");
	evas_object_layer_set(obj, 75); // on top of overlay
	fp_image_size_get(obj, &w, &h);
	x = (containerW - w)/2;
	y = (containerH - h)/2;
	evas_object_resize( obj, w, h );
	printf("%s: fp_container_object_add2(container = 0x%08x, obj = 0x%08x)\r\n",__FUNCTION__,(unsigned int)app->logContainer, (unsigned int)obj);
	fp_container_object_add(app->logContainer, obj, x, y);
	evas_object_show(obj);

	return 0;
}

Evas_Object *createMessageBox(char* msg, enum vf_dialog_icon icon, enum vf_dialog_buttons buttons, void (*msgButtonCb)(void *data, Evas_Object *o, const char *button, const char *input))
{
	Evas_Object *msgBox;

	msgBox = vf_dialog_add(app->fp, msg, icon, buttons, 0);
	if (msgButtonCb) {
		vf_dialog_callback_set(msgBox, msgButtonCb, (void *)msgBox);
	}
	evas_object_focus_set(msgBox, 1);
	evas_object_show(msgBox);
	return msgBox;
}

void destroyMessageBox(Evas_Object* msgBox)
{
    evas_object_del(msgBox);
}


Evas_Object *createText_wrap(Evas_Object *container, int textX, int textY, int textW, const char *msg)
{
	Evas_Object *o;
    int textH;
	int containerX, containerY, containerW, containerH;

	o = fp_text_add( app->fp );
	fp_text_fonts_set(o, "Vera", "Vera", "Vera", NULL, 10);
	evas_object_color_set(o, 0, 0, 0, 255);
	//fp_text_text_style_set(o, pTextDef->style);
	/* resize to requested width, and just use a dummy height */
	textH = 480; // just picked a number...
	evas_object_resize(o, textW, textH);
	if ( msg && strlen(msg) ) {
		fp_text_text_set(o, msg);
		/* now get real required height */
		textH = fp_text_height(o);
		/* now resize */
		evas_object_resize(o, textW, textH);
	}
	evas_object_layer_set( o, 90 );
	evas_object_show( o );
	if (container) {
		if ( 0 == textX ) {
			/* center text in container */
			evas_object_geometry_get(container, &containerX, &containerY, &containerW, &containerH);
			textX = (containerW - textW)/2;
		}
		fp_container_object_add(container, o, textX, textY);
	}

	return o;
}


void Clicked_AcceptButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event)
{
	int set_resRes;
	int process_result = TMS_STATUS_CONTENT_FAILURE;

#if DELETE_BUTTONS_IN_CB
	if (app->acceptButton)
	{
		evas_object_hide(app->acceptButton);
		evas_object_del(app->acceptButton);
		app->acceptButton = NULL;
	}

	/* We can remove the postpone button */
	if (app->postponeButton)
	{
		evas_object_hide(app->postponeButton);
		evas_object_del(app->postponeButton);
		app->postponeButton = NULL;
	}


	if (app->errorButton)
	{
		evas_object_hide(app->errorButton);
		evas_object_del(app->errorButton);
		app->errorButton = NULL;
	}
#endif

	evas_object_text_text_set(app ->appStatus, "Accpeting Content");
	evas_render(app ->fp ->e);
	sleep(2);

	/* If we have the source and destination, lest copy the files */
	if (strlen(appEvent->filename))
	{
		char* pszBaseName;
		char tempname[256];
		char filepath[512];
		char copyFileDest[512];
		int copy_result;

		if (strlen(appEvent->filepath))
			strcpy(filepath, appEvent->filepath);
		else
			strcpy(filepath, "./");

		strcpy(tempname, appEvent->filename);
		pszBaseName = basename(tempname);

		sprintf(copyFileDest, "%s/%s", filepath, pszBaseName);
		if (VerifyFilePermissions(appEvent->filename, VERIFY_READ_PERMISSION))
		{
			if (VerifyFilePermissions(filepath, VERIFY_WRITE_PERMISSION))
			{
				LogMsg("svc_tms_tester: Copying %s to %s\n", appEvent->filename, copyFileDest);
				copy_result = CopyFile(appEvent->filename, copyFileDest);
				LogMsg("svc_tms_tester: Copy result = %d\n", copy_result);

				if (copy_result == 0)
					process_result = TMS_STATUS_SUCCESS;
				else
					process_result = TMS_STATUS_CONTENT_FAILURE;
			}
		}
	}


	/* If we have a content handle we can set the result */
	if (appEvent->handle > 0)
	{
		LogMsg("svc_tms_tester: Content Accepted - Setting result of ID %d to %s\n", appEvent->handle, status_to_string(process_result));
		set_resRes = tms_setFileOperationResultWithDescription(appEvent->handle, process_result, TMS_EVT_PUT_FILE, NULL);
	}

	LogMsg("svc_tms_tester: freeing appEvent @ 0x%08x\n", (int)appEvent);
	//free(appEvent);
	//appEvent = NULL;

	evas_object_text_text_set(app ->appStatus, "Content Update Accepted");
	evas_render(app ->fp ->e);
	sleep(2);

	waiting_for_usr_response = FALSE;

	LogMsg("%s: returning\n", __FUNCTION__);
}

void Clicked_PostponeButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event)
{
	int set_resRes;
	int process_result = TMS_STATUS_POSTPONED;

#if DELETE_BUTTONS_IN_CB
	if (app->acceptButton)
	{
		evas_object_hide(app->acceptButton);
		evas_object_del(app->acceptButton);
		app->acceptButton = NULL;
	}

	/* We can remove the postpone button */
	if (app->postponeButton)
	{
		evas_object_hide(app->postponeButton);
		evas_object_del(app->postponeButton);
		app->postponeButton = NULL;
	}

	if (app->errorButton)
	{
		evas_object_hide(app->errorButton);
		evas_object_del(app->errorButton);
		app->errorButton = NULL;
	}
#endif

	evas_object_text_text_set(app ->appStatus, "Rejecting Content");
	evas_render(app ->fp ->e);
	sleep(2);

	/* If we have a content handle we can set the result */
	if (appEvent->handle)
	{
		LogMsg("svc_tms_tester: Content Postponed - Setting result of ID %d to %s\n", appEvent->handle, status_to_string(process_result));
		set_resRes = tms_setFileOperationResult(appEvent->handle, process_result, TMS_EVT_PUT_FILE);
	}

	LogMsg("svc_tms_tester: freeing appEvent @ 0x%08x\n", (int)appEvent);
	//free(appEvent);
	//appEvent = NULL;

	evas_object_text_text_set(app ->appStatus, "Content Update Rejected");
	evas_render(app ->fp ->e);
	sleep(2);

	waiting_for_usr_response = FALSE;
}



void Clicked_ErrorButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event)
{
	int set_resRes;
	int process_result = TMS_STATUS_CONTENT_FAILURE;

#if DELETE_BUTTONS_IN_CB
	if (app->acceptButton)
	{
		evas_object_hide(app->acceptButton);
		evas_object_del(app->acceptButton);
		app->acceptButton = NULL;
	}

	/* We can remove the postpone button */
	if (app->postponeButton)
	{
		evas_object_hide(app->postponeButton);
		evas_object_del(app->postponeButton);
		app->postponeButton = NULL;
	}

	if (app->errorButton)
	{
		evas_object_hide(app->errorButton);
		evas_object_del(app->errorButton);
		app->errorButton = NULL;
	}
#endif

	evas_object_text_text_set(app->appStatus, "Rejecting Content");
	evas_render(app->fp->e);
	sleep(2);

	/* If we have a content handle we can set the result */
	if (appEvent->handle)
	{
		LogMsg("svc_tms_tester: Content Rejected - Setting result of ID %d to %s\n", appEvent->handle, status_to_string(process_result));
		set_resRes = tms_setFileOperationResultWithDescription(appEvent->handle, process_result, TMS_EVT_PUT_FILE, "App content REJECTED by user");
	}

	LogMsg("svc_tms_tester: freeing appEvent @ 0x%08x\n", (int)appEvent);
	//free(appEvent);
	//appEvent = NULL;

	evas_object_text_text_set(app->appStatus, "Content Update Rejected");
	evas_render(app->fp->e);
	sleep(2);

	waiting_for_usr_response = FALSE;
}


void Clicked_ContactServerButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event)
{
	struct tmsReturn tms_ret;
	char disp_str1[256];

	if (app->contactServerButton)
	{
		evas_object_hide(app->contactServerButton);
	}
	if (app->appStatus2)
	{
		evas_object_hide(app->appStatus2);
		evas_object_del(app->appStatus2);
		app->appStatus2 = NULL;
	}

	evas_object_text_text_set(app ->appStatus, "Contacting Server");
	evas_render(app ->fp ->e);
	sleep(1);



	LogMsg("%s: Contacting server\n", __FUNCTION__);
	tms_ret = tms_callServer2(0, TMS_API_CALL_SERVER_FORCE_MAINTENANCE_FLAG);
	

	memset(disp_str1, 0, sizeof(disp_str1));
	LogMsg("%s: contact server result = %s\n", __FUNCTION__, status_to_string(tms_ret.status));
	if (tms_ret.status == TMS_STATUS_REQUESTED)
	{
		sprintf(disp_str1, "Call Server REQUESTED");
	}
	else
	{
		sprintf(disp_str1, "FAILURE to send contact server to agent");
	}

	if (app->appStatus)
	{
		evas_object_hide(app->appStatus);
		evas_object_del(app->appStatus);
		app->appStatus = NULL;
	}

	CreateText(app, &app ->appStatus, "Vera", 15, disp_str1, ALIGN_CENTER, (PAD_SPACE + 55), 255, 192, 192);
	
	LogMsg("%s: re-rendering\n", __FUNCTION__);
	evas_render(app ->fp ->e);
}




void Clicked_GetConfigButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event)
{
#if (!CONFIG_BUTTON_IS_GET_CONFIG)
	int copy_result;
#endif
	char disp_str1[256];
	struct tmsReturn tms_ret;

	if (app->getConfigButton)
	{
		evas_object_hide(app->getConfigButton);
	}
	if (app->appStatus2)
	{
		evas_object_hide(app->appStatus2);
		evas_object_del(app->appStatus2);
		app->appStatus2 = NULL;
	}

#if CONFIG_BUTTON_IS_GET_CONFIG

	evas_object_text_text_set(app ->appStatus, "Getting Config Location");
	evas_render(app ->fp ->e);
	sleep(1);



	LogMsg("%s: Getting Config Location\n", __FUNCTION__);
	tms_ret = tms_getConfigLocation();
	

	memset(disp_str1, 0, sizeof(disp_str1));
	LogMsg("%s: tms_getConfigLocation result = %s\n", __FUNCTION__, status_to_string(getConfigRes));
	if (tms_ret.status == TMS_STATUS_REQUESTED)
	{
		sprintf(disp_str1, "Get Config Location REQUESTED");
	}
	else
	{
		sprintf(disp_str1, "FAILURE to send get config location request to agent");
	}
#else
	evas_object_text_text_set(app ->appStatus, "Requesting Config Update");
	evas_render(app ->fp ->e);
	sleep(1);

	LogMsg("%s: Copying %s to %s\n", __FUNCTION__, "./tms_config.ini", "/tmp/share/tms_config.ini");
	copy_result = CopyFile("./tms_config.ini", "/tmp/share/tms_config.ini");
	LogMsg("%s: Copy result = %d\n", __FUNCTION__, copy_result);
	if (chmod("/tmp/share/tms_config.ini", 0666 ) != 0)
		LogMsg("%s: errno set in chmod() = %d\n", __FUNCTION__, errno);

	LogMsg("%s: Requesting Config Update\n", __FUNCTION__);
	tms_ret = tms_setNewConfigAvailable("/tmp/share/tms_config.ini");
	

	memset(disp_str1, 0, sizeof(disp_str1));
	LogMsg("%s: tms_newConfigAvail result = %s\n", __FUNCTION__, status_to_string(tms_ret.status));
	if (tms_ret.status == TMS_STATUS_REQUESTED)
	{
		sprintf(disp_str1, "Config Update REQUESTED");
	}
	else
	{
		sprintf(disp_str1, "FAILURE to send config update request to agent");
	}
#endif

	if (app->appStatus)
	{
		evas_object_hide(app->appStatus);
		evas_object_del(app->appStatus);
		app->appStatus = NULL;
	}

	CreateText(app, &app ->appStatus, "Vera", 15, disp_str1, ALIGN_CENTER, (PAD_SPACE + 55), 255, 192, 192);
	
	LogMsg("%s: re-rendering\n", __FUNCTION__);
	evas_render(app ->fp ->e);
}




void Clicked_SwapConfig1ButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event)
{
	char disp_str1[256];
	struct tmsReturn tms_ret;

	if (app->swapConfig1Button)
	{
		evas_object_hide(app->swapConfig1Button);
	}
	if (app->appStatus2)
	{
		evas_object_hide(app->appStatus2);
		evas_object_del(app->appStatus2);
		app->appStatus2 = NULL;
	}

	if (backup_active)
	{
		backup_requested = FALSE;
		evas_object_text_text_set(app->appStatus, "Restoring previous instance");
		evas_render(app->fp->e);
		sleep(1);

		LogMsg("%s: Restoring server instance %s\n", __FUNCTION__, prevInstance1);
		tms_ret = tms_releaseServerLock(prevInstance1);


		memset(disp_str1, 0, sizeof(disp_str1));
		LogMsg("%s: tms_releaseServerLock result = %s\n", __FUNCTION__, status_to_string(tms_ret.status));
		if (tms_ret.status == TMS_STATUS_REQUESTED)
		{
			sprintf(disp_str1, "Release Server Lock 'backup' REQUESTED");
		}
		else
		{
			sprintf(disp_str1, "FAILURE to send release server lock request to agent");
		}
	}
	else
	{
		evas_object_text_text_set(app->appStatus, "Setting server instance to backup");
		evas_render(app->fp->e);
		sleep(1);

		LogMsg("%s: Setting backup server instance\n", __FUNCTION__);
		tms_ret = tms_lockServerInstance("backup", 0);


		memset(disp_str1, 0, sizeof(disp_str1));
		LogMsg("%s: tms_lockServerInstance result = %s\n", __FUNCTION__, status_to_string(tms_ret.status));
		if (tms_ret.status == TMS_STATUS_REQUESTED)
		{
			backup_requested = TRUE;
			sprintf(disp_str1, "Server Lock 'backup' REQUESTED");
		}
		else
		{
			sprintf(disp_str1, "FAILURE to send server lock request to agent");
		}
	}

	if (app->appStatus)
	{
		evas_object_hide(app->appStatus);
		evas_object_del(app->appStatus);
		app->appStatus = NULL;
	}

	CreateText(app, &app->appStatus, "Vera", 15, disp_str1, ALIGN_CENTER, (PAD_SPACE + 55), 255, 192, 192);

	LogMsg("%s: re-rendering\n", __FUNCTION__);
	evas_render(app->fp->e);
}



void Clicked_SwapConfig2ButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event)
{
	char disp_str1[256];
	struct tmsReturn tms_ret;

	if (app->swapConfig2Button)
	{
		evas_object_hide(app->swapConfig2Button);
	}
	if (app->appStatus2)
	{
		evas_object_hide(app->appStatus2);
		evas_object_del(app->appStatus2);
		app->appStatus2 = NULL;
	}

	if (third_active)
	{
		backup_requested = FALSE;
		evas_object_text_text_set(app->appStatus, "Restoring previous instance");
		evas_render(app->fp->e);
		sleep(1);

		LogMsg("%s: Restoring server instance %s\n", __FUNCTION__, prevInstance2);
		tms_ret = tms_releaseServerLock(prevInstance2);


		memset(disp_str1, 0, sizeof(disp_str1));
		LogMsg("%s: tms_releaseServerLock result = %s\n", __FUNCTION__, status_to_string(tms_ret.status));
		if (tms_ret.status == TMS_STATUS_REQUESTED)
		{
			sprintf(disp_str1, "Release Server Lock 'third' REQUESTED");
		}
		else
		{
			sprintf(disp_str1, "FAILURE to send release server lock request to agent");
		}
	}
	else
	{
		evas_object_text_text_set(app->appStatus, "Setting server instance to third");
		evas_render(app->fp->e);
		sleep(1);

		LogMsg("%s: Setting third server instance\n", __FUNCTION__);
		tms_ret = tms_lockServerInstance("third", 60000);


		memset(disp_str1, 0, sizeof(disp_str1));
		LogMsg("%s: tms_lockServerInstance result = %s\n", __FUNCTION__, status_to_string(tms_ret.status));
		if (tms_ret.status == TMS_STATUS_REQUESTED)
		{
			backup_requested = FALSE;
			sprintf(disp_str1, "Server Lock 'third' REQUESTED");
		}
		else
		{
			sprintf(disp_str1, "FAILURE to send server lock request to agent");
		}
	}

	if (app->appStatus)
	{
		evas_object_hide(app->appStatus);
		evas_object_del(app->appStatus);
		app->appStatus = NULL;
	}

	CreateText(app, &app->appStatus, "Vera", 15, disp_str1, ALIGN_CENTER, (PAD_SPACE + 55), 255, 192, 192);

	LogMsg("%s: re-rendering\n", __FUNCTION__);
	evas_render(app->fp->e);
}



void Clicked_ViewLogButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event)
{
	struct appdata *loc_app = (struct appdata *)data;
	Evas_Object *obj, *obj2, *o2;
	int coverX, coverY, coverW, coverH;
	char *tptr;
	struct tmsReturn tms_ret;


	evas_object_geometry_get(loc_app->textRect, &coverX, &coverY, &coverW, &coverH);

	createTabBackground();

	o2=createMessageBox("Please Wait...\nReading log file!",VF_DIALOG_ICON_INFO , VF_DIALOG_BUTTONS_CUSTOM, NULL);
	evas_render(app->fp->e);

	LogMsg("svc_tms_tester: Un-Registering App %s\n", "svc_tms_tester");
	tms_ret = tms_sendApplicationAlert("ViewLogEvent", TMS_ALERT_SEVERITY_HIGH, "fake details of the view log event can be quite long with newlines\nand tabs\n\ttab test");
	if (tms_ret.status == TMS_STATUS_REQUESTED)
	LogMsg("svc_tms_tester: Un-Register requested\n");



	tptr=readFile(SVC_TMS_TESTER_LOG_FILE,0);
	if (tptr)
	{
		textbx = obj=createText_wrap(loc_app->logContainer, 0,0, coverW-18,tptr);
		free(tptr);
		obj2=fp_scrollbar_add(app->fp,obj);
		fp_scrollbar_bars_show_set(obj2,FP_SCROLLBAR_SHOW_AUTO,FP_SCROLLBAR_SHOW_NEVER);
        fp_scrollbar_dragging_set(obj2,1);
        fp_scrollbar_fading_set(obj2,1);
		evas_object_resize(obj2,coverW-15,coverH-15);
		fp_container_object_add(loc_app->logContainer, obj2, 10, 10);
		evas_object_show(obj2);
	}
	destroyMessageBox(o2);

	CreateButton(app, &app ->logDeleteButton, "Delete Log", (PAD_SPACE * 2), ((app ->fp ->h) - 60), 140, 40, Clicked_DeleteButtonCB);
	CreateButton(app, &app ->logExitButton, "Exit", 2 * (PAD_SPACE * 2) + 140, ((app ->fp ->h) - 60), 140, 40, Clicked_ExitButtonCB);
}

void Clicked_DeleteButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event)
{
	if (remove(SVC_TMS_TESTER_LOG_FILE) != 0)
		LogMsg("%s: errno set in removing file (%s) = %d\n", __FUNCTION__, SVC_TMS_TESTER_LOG_FILE, errno);

	Clicked_ExitButtonCB(data, button, mouse_event);
	Clicked_ViewLogButtonCB(data, button, mouse_event);
}

void Clicked_ExitButtonCB(void *data, Evas_Object *button, Evas_Event_Mouse_Up *mouse_event)
{
	if (app->logContainer)
	{
		evas_object_hide(app->logContainer);
		evas_object_del(app->logContainer);
		app->logContainer = NULL;
	}

	if (app->logDeleteButton)
	{
		evas_object_hide(app->logDeleteButton);
		evas_object_del(app->logDeleteButton);
		app->logDeleteButton = NULL;
	}

	if (app->logExitButton)
	{
		evas_object_hide(app->logExitButton);
		evas_object_del(app->logExitButton);
		app->logExitButton = NULL;
	}
}

//	Make Enable / Disable Togglable
//	fp_button_background_normal
//	fp_button_background_pressed	
//	fp_button_metric_image_set(app ->enableButton, "fp_button_background_pressed", "fp_button_background_pressed");
//	fp_button_metric_image_set(app ->disableButton, "fp_button_background_normal", "fp_button_background_pressed");
//	fp_button_metric_image_set(app ->enableButton, "fp_button_background_normal", "fp_button_background_pressed");
//	fp_button_metric_image_set(app ->disableButton, "fp_button_background_pressed", "fp_button_background_pressed");
