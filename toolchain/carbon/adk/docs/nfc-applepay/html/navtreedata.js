var NAVTREE =
[
  [ "APPLEPAY-VAS-Kernel", "index.html", [
    [ "ApplePay VAS Kernel", "index.html", [
      [ "PREFACE", "index.html#sec_Preface", [
        [ "Audience", "index.html#subsec_Audience", null ],
        [ "Organization", "index.html#subsec_Organization", null ]
      ] ],
      [ "INTRODUCTION", "index.html#sec_Introduction", null ],
      [ "ARCHITECTURE", "index.html#sec_applepay_vas_kern_Architecture", null ],
      [ "APPLEPAY VAS KERNEL INTERFACE", "index.html#sec_applepay_vas_kern_interface", null ],
      [ "Return Values", "index.html#sec_applepay_vas_ret_val", null ],
      [ "SW1 SW2", "index.html#sec_applepay_vas_sw1sw2", null ],
      [ "TYPES", "index.html#sec_applepay_vas_kern_types", [
        [ "List Of Known JSON Objects", "index.html#subsec_adk_nfc_vas_wallet_json_objects", null ]
      ] ],
      [ "VAS Wallet Configuration Parameters", "index.html#sec_adk_nfc_vas_config_types", null ],
      [ "Read VAS Wallet Configuration Parameters", "index.html#sec_adk_nfc_vas_config_read", null ],
      [ "VAS Wallet Dynamic Configuration Parameters", "index.html#sec_applepay_vas_dynamic_config_types", null ],
      [ "VAS Wallet PreLoad Configuration Parameters", "index.html#sec_applepay_vas_preload_config_types", null ],
      [ "VAS Wallet Response Format", "index.html#sec_applepay_vas_response", null ],
      [ "Customer Data Functions", "index.html#sec_applepay_vas_customer_data_func", [
        [ "Dynamic URL", "index.html#sec_applepay_vas_customer_data_du_func", null ]
      ] ],
      [ "VAS Wallet JSON Examples", "index.html#sec_applepay_vas_json_examples", [
        [ "VAS Wallets Configuration", "index.html#subsec_adk_nfc_VAS_Wallets_Json_VASConfig", null ],
        [ "VAS Wallets Configuration Response", "index.html#subsec_adk_nfc_VAS_Wallets_Json_VASConfigResponse", null ],
        [ "Dynamic Configuration", "index.html#subsec_adk_nfc_VAS_Wallets_Json_DynamicConfig", null ],
        [ "PreLoad Configuration", "index.html#subsec_adk_nfc_VAS_Wallets_Json_PreLoadConfig", null ],
        [ "VAS Wallet Response", "index.html#subsec_adk_nfc_VAS_Wallets_Json_Response", null ]
      ] ],
      [ "EXAMPLES", "index.html#sec_applepay_vas_kern_examples", null ],
      [ "HISTORY", "index.html#sec_applepay_vas_history", null ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"index.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';