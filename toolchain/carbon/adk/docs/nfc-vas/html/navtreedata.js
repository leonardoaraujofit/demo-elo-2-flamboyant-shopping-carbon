var NAVTREE =
[
  [ "VAS-Kernel", "index.html", [
    [ "VAS Kernel", "index.html", [
      [ "PREFACE", "index.html#sec_Preface", [
        [ "Audience", "index.html#subsec_Audience", null ],
        [ "Organization", "index.html#subsec_Organization", null ]
      ] ],
      [ "INTRODUCTION", "index.html#sec_Introduction", null ],
      [ "ARCHITECTURE", "index.html#sec_vas_kern_Architecture", null ],
      [ "VAS KERNEL INTERFACE", "index.html#sec_vas_kern_interface", [
        [ "VAS KERNEL INTERFACE - General", "index.html#subsec_vas_kern_interface", null ],
        [ "VAS KERNEL INTERFACE - Parameters and Data", "index.html#subsec_vas_kern_interface_gen_data", null ],
        [ "TRANSIVE APDU", "index.html#subsec_vas_kern_interface_apdu", null ]
      ] ],
      [ "TYPES", "index.html#sec_vas_kern_types", [
        [ "VASKERNEL_STATUS", "index.html#subsec_vas_kern_status", null ],
        [ "VASKERNEL_ACTIONS", "index.html#subsec_vas_kern_actions", null ],
        [ "actionParams", "index.html#subsec_vas_kern_actionParams", null ],
        [ "actionResults", "index.html#subsec_vas_kern_actionResults", null ],
        [ "APDUparams", "index.html#subsec_vas_kern_APDUparams", null ],
        [ "APDUresults", "index.html#subsec_vas_kern_APDUresults", null ],
        [ "rawData", "index.html#subsec_vas_kern_rawData", null ]
      ] ],
      [ "EXAMPLES", "index.html#sec_vas_kern_examples", null ],
      [ "HISTORY", "index.html#sec_applepay_vas_history", null ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"index.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';