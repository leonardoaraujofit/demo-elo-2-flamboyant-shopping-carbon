#ifndef _COMMON_H
#define _COMMON_H

#include "logging.h"
#ifdef _VRXEVO
#include <SVC.H>
inline void remove_file(const char *name)
{
	_remove(name);
}
#else
#include <unistd.h>
inline void remove_file(const char *name)
{
	unlink(name);
}
#endif

#endif //COMMON_H
