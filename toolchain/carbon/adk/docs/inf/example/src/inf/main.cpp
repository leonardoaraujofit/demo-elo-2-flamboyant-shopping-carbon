#include <inf/infodb.h>

#include <stdlib.h>
#include <stdio.h>

#include "common.h"

InfoDBHandle *g_handle = NULL;

char const *DB_NAME = "prop.db";

///////////// UTILITY ////////////////////////


const char *translate_type(int type)
{
    switch(type)
    {

        case INFO_VOID: return "INFO_VOID";
        case INFO_INTEGER: return "INFO_INTEGER";
        case INFO_NUMBER: return "INFO_NUMBER";
        case INFO_STRING: return "INFO_STRING";
        case INFO_BINARY: return "INFO_BINARY";
        default: return "(unrecognized)";
    }
}
const char *translate_error(int err)
{
    switch(err)
    {
        case 0: return "INFO_OK";
        case -1: return "INFO_FAIL";
        case -2: return "INFO_NO_DB";
        case -3: return "INFO_NOT_FOUND";
        case -4: return "INFO_INVALID_PARAM";
        case -5: return "INFO_BUSY";
        case -6: return "INFO_MEM";
        case -7: return "INFO_BREAK";
        case -8: return "INFO_NODE_NAME_INVALID";
        case -900: return "INFO_NOT_IMPL";
        case -901: return "INFO_NOT_SUPP";
        default: return "(unknown)";
    }

}

static int title_n = 0;
#define TITLE(x) do { title_n++; LOG(" "); LOG("--------"); LOG("%d)"x, title_n); LOG("--------"); } while(0)
int open_db()
{
    int res = 0;
    g_handle = info_opendb(DB_NAME, INFO_READ_WRITE);
    if(!g_handle)
    {
        LOG_ERR("Could not open database %s", DB_NAME);
        res = -1;
    }
    return res;
}

void close_db()
{
    info_closedb(g_handle);
    g_handle = NULL;
}

int callback(void *data, const char *path, struct InfoReference *info)
{
    InfoType type;
    int rc = info_nodeGetType(info, &type);
    if(rc != INFO_OK)
        LOG_ERR("info_nodeGetType returned %s", translate_error(rc));
    switch(type)
    {
    case INFO_VOID:
        break;
    case INFO_INTEGER:
    {
        int i;
        rc = info_nodeReadInt(info, &i);
        if(rc == INFO_OK)
            LOG("Int: %d", i);
        else
            LOG_ERR("nodeReadInt returned %s", translate_error(rc));
        break;
    }
    case INFO_NUMBER:
    {
        double d;
        rc = info_nodeReadDouble(info, &d);
        if(rc == INFO_OK)
            LOG("Double: %f", d);
        else
            LOG_ERR("nodeReadDouble returned %s", translate_error(rc));
        break;
    }
    case INFO_STRING:
    {
        char *s;
        rc = info_nodeReadString(info, &s);
        if(rc == INFO_OK)
        {
            LOG("String: %s", s);
            free(s);
        }
        else
            LOG_ERR("nodeReadString returned %s", translate_error(rc));
        break;
    }
    case INFO_BINARY:
    {
        char *s;
        int i;
        rc = info_nodeReadBinary(info, (void **)&s, &i);
        if(rc == INFO_OK)
        {
            LOG("Binary");
            free(s);
        }
        else
            LOG_ERR("nodeReadBinary returned %s", translate_error(rc));
        break;
    }
    }
    return 0;
}

///////////// UTILITY ////////////////////////
void write_read_example(InfoNode node, int value)
{
    TITLE("Will write and read int/double/string/binary values");
    open_db();
    int rc = 0;
    char s_val[10] = { 0 };
    snprintf(s_val, sizeof(s_val), "%d", value);

    rc = info_writeInt(g_handle, node, "integer", value); 
    if(rc != INFO_OK)
        LOG("writeInt() returned %s", translate_error(rc));

    rc = info_writeDouble(g_handle, node, "double", (double) value); 
    if(rc != INFO_OK)
        LOG("writeDouble() returned %s", translate_error(rc));

    rc = info_writeString(g_handle, node, "string", s_val); 
    if(rc != INFO_OK)
        LOG("writeString() returned %s", translate_error(rc));

    rc = info_writeBinary(g_handle, node, "binary", s_val, 3); 
    if(rc != INFO_OK)
        LOG("writeBinary() returned %s", translate_error(rc));

    int i = 0;
    double d = 0;
    char *s = NULL;
    rc = info_readInt(g_handle, node, "integer", &i); 
    if(rc == INFO_OK)
        LOG("Read int: %d", i);
    else
        LOG("Error: %s", translate_error(rc));

    //increment and read again
    rc = info_incrementInt(g_handle, node, "integer", 10, &i);
    if(rc != INFO_OK)
        LOG_ERR("info_incrementInt() returned %s", translate_error(rc));

    rc = info_readInt(g_handle, node, "integer", &i); 
    if(rc == INFO_OK)
        LOG("Read int after incrementing by 10: %d", i);
    else
        LOG("Error: %s", translate_error(rc));

    rc = info_readDouble(g_handle, node, "double", &d); 
    if(rc == INFO_OK)
        LOG("Read double: %.1f", d);
    else
        LOG("Error: %s", translate_error(rc));

    rc = info_readString(g_handle, node, "string", &s); 
    if(rc == INFO_OK)
    {
        LOG("Read string: %s", s);
        free(s);
    }
    else
        LOG("Error: %s", translate_error(rc));

    rc = info_readBinary(g_handle, node, "binary", (void **)&s, &i); 
    if(rc == INFO_OK)
    {
        LOG("Read binary: %s, len %d", s, i);
        free(s);
    }
    else
        LOG("Error: %s", translate_error(rc));

    close_db();    
}

void find_example()
{
    TITLE("Will execute info_find() on existing and non-existing node");
    open_db();

    
    //let's find previously written node
    InfoNode node = info_find(g_handle, INFO_ROOT, "string");
    LOG("\"/string\" is node %u", (unsigned) node);

    //works in the same way, think of it as a path
    node = info_find(g_handle, node, "");
    //Should output the same thing
    LOG("\"/string\" is node %u", (unsigned) node);

    //let's try to find non-existent node
    node = info_find(g_handle, (InfoNode) 12345, "");
    //bear in mind that node can be a return code, so always check against negative values
    LOG("info_find() returned %s for non-existing node ", translate_error(node));


    close_db();
}

void output_children_types(InfoNode parentNode)
{
    InfoChildren *i_chld;
    int rc = info_list(g_handle, parentNode, "", &i_chld);
    LOG("info_list returned %s", translate_error(rc));
    if(INFO_OK == rc)
    {
        LOG("Node contains %d immediate children", i_chld->count);

        InfoType type;
        for(int i = 0; i < i_chld->count; i++)
        {
            //get a InfoNode for the current child
            InfoNode node = info_find(g_handle, parentNode, i_chld->name[i]);
            if(rc != INFO_OK)
            {
                LOG_ERR("info_find() returned %s for \"%s\"", translate_error(node), i_chld->name[i]);
                continue;
            }

            char *path = NULL;
            //gets absolute path of a current child
            rc = info_getPath(g_handle, node, &path);
            if(rc != INFO_OK)
            {
                LOG_ERR("info_getPath() returned %s", translate_error(rc));
                continue;
            }

            //use INFO_ROOT as starting node,
            //as "path" contains absolute path to root
            rc = info_getType(g_handle, INFO_ROOT, path, &type);
            if(rc < 0)
                LOG_ERR("info_getType() returned %s", translate_error(rc));
            LOG("Node's name: %s, path: %s, type :%s", i_chld->name[i], path, translate_type(type));
            free(path);
        }
        free(i_chld);
    }
}

void list_gettype_example()
{

    open_db();
    TITLE("Will perfrom info_list() on a node \"double\" and call info_getPath() and info_getType() on each");

    InfoNode node = info_find(g_handle, INFO_ROOT, "double");
    if(node < 0)
        LOG_ERR("info_find returned %s", translate_error(node));

    int rc = info_writeInt(g_handle, node, "first_child", 0);
    if(rc != INFO_OK)
        LOG_ERR("info_writeInt returned %s", translate_error(rc));

    rc = info_writeInt(g_handle, node, "second_child", 0);
    if(rc != INFO_OK)
        LOG_ERR("info_writeInt returned %s", translate_error(rc));

    output_children_types(node);

    close_db();
}

void delete_example()
{
    TITLE("Will delete INFO_ROOT's children and call info_list() afterwards");
    open_db();
    int rc = info_delete(g_handle, INFO_ROOT, "");
    LOG("info_delete() returned %s", translate_error(rc));
    output_children_types(INFO_ROOT);
    close_db();

}

void walk_tree_example()
{
    TITLE("Will populate a DB and call walkTree");
    open_db();
    int rc;

    rc = info_writeInt(g_handle, INFO_ROOT, "1st_level_int", 1);
    if(rc < 0)
        LOG_ERR("info_writeInt returned %s", translate_error(rc));
    
    rc = info_writeString(g_handle, INFO_ROOT, "1st_level_string", "1");
    if(rc < 0)
        LOG_ERR("info_writeString returned %s", translate_error(rc));


    rc = info_writeInt(g_handle, INFO_ROOT, "1st_level_int/2nd_level_int", 2);
    if(rc < 0)
        LOG_ERR("info_writeInt returned %s", translate_error(rc));

    rc = info_writeString(g_handle, INFO_ROOT, "1st_level_string/2nd_level_string", "2");
    if(rc < 0)
        LOG_ERR("info_writeString returned %s", translate_error(rc));

    rc = info_walkTree(g_handle, INFO_ROOT, "", callback, NULL);

    close_db();
}

int main(int argc, char** argv)
{
#ifdef _VRXEVO
    EOSLOG_INIT("INF", LOGSYS_OS, 0xFFFFFFFF);
#endif
    //remove old db file
    remove_file(DB_NAME);

    //execute examples
    write_read_example(INFO_ROOT, 8);
    find_example();
    list_gettype_example();
    delete_example();
    walk_tree_example();
}
