#include <inf/infodb.h>
#include <inf/inf_util.h>
#include <sqlite/sqlite3.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "common.h"

#ifdef TEST_STATIC
#include "inf/inf_util/backend_api.h"

#ifndef OMIT_BACKEND_XMLFILE
REGISTER_STATIC_BACKEND(XMLFileBackend);
#endif

#ifndef OMIT_BACKEND_SQLITE
REGISTER_STATIC_BACKEND(SQLiteBackend);
#endif

#endif
////////////////////    UTILITY    //////////////////////////////////////////

//for convenient error dumping
#define FIX_NULL(__exp, __val) (__exp ? __exp : __val)
#define DUMP_ERROR_INFO(__var) LOG("ErrorInfo: { %d / %s / %s / %s }", __var.errorCode, FIX_NULL(__var.shortMessage, "(NULL)"), FIX_NULL(__var.location, "(NULL)"), FIX_NULL(__var.extendedMessage,"(NULL)"))

//columns is an array of column names
//argv is an array of values 
//argc is the length of these arrays
int callback(void *param, int argc, char **argv, char **columns)
{
    LOG("----------------");
    for(int i = 0; i < argc; i++)
        LOG("[%s]: %s", columns[i], (argv[i] == NULL) ? "(null)" : argv[i]);
    //return -1;
    //returning -1 will result in aborting query execution
    return 0;
}

void execute_query(sqlite3 *handle, const char *query, void *param)
{ LOG(" ");
    LOG("Executing query %s", query);
    int rc = sqlite3_exec(handle, query, callback, param, 0);
    if(rc != SQLITE_OK)
    {
        LOG_ERR("Executing  %s", query);
        LOG_ERR("Details: %s", sqlite3_errmsg(handle));
    }
}

sqlite3_stmt* prepare_statement(sqlite3 *handle, const char *stmt)
{
    sqlite3_stmt *st;
    //safe to use v2 here, as they differ by encoding used for stmt(UTF-8 in this case)
    //-1 tells sqlite to treat stmt as a c-string
    int rc = sqlite3_prepare_v2(handle, stmt, -1, &st, 0);
    if(rc != SQLITE_OK)
    {
        LOG_ERR("Preparing %s. Details: %s", stmt, sqlite3_errmsg(handle));
    }
    return st;
}

sqlite3* open_db(const char *name)
{
    sqlite3 *handle = NULL;
    const char *db_name = "first.db";

    int rc = sqlite3_open(db_name, &handle);
    if(rc)
    {
        LOG_ERR("Opening DB %s", db_name);
        LOG_ERR("Details: %s", sqlite3_errmsg(handle));
    }
    return handle;
}

////////////////////////////////////// //////////////////////////////////////

void syntax1_impexp()
{
    char const *db_name ="first.db";
    char const *db_sch = "sample_prop_db_schema.xml";
    char const *xml_fmt = "sample_prop_xml_format.xml";
    char const *data = "sample_prop_data.xml";
    char const *out_xml = "exp1.xml";
    ErrorInfo err;
    LOG("--Hierarchical structure");

    LOG("Starting import");
    info_import(xml_fmt, db_sch, db_name, data, &err); 
    DUMP_ERROR_INFO(err);

    LOG("Starting export");
    info_export(xml_fmt, db_sch, db_name, out_xml, &err); 
    DUMP_ERROR_INFO(err);

}

void syntax2_impexp()
{
    char const *db_name ="second.db";
    char const *db_sch = "sample_prop_db_schema.xml";
    char const *xml_fmt = "sample_prop_xml_format_2.xml";
    char const *data = "sample_prop_data_2.xml";
    char const *out_xml = "exp2.xml";
    ErrorInfo err;
    LOG("--Hierarchical compound name");
    LOG("Starting import");
    info_import(xml_fmt, db_sch, db_name, data, &err); 
    DUMP_ERROR_INFO(err);

    LOG("Starting export");
    info_export(xml_fmt, db_sch, db_name, out_xml, &err); 
    DUMP_ERROR_INFO(err);
}

void syntax3_impexp()
{
    char const *db_name ="third.db";
    char const *db_sch = "sample_prop_db_schema.xml";
    char const *xml_fmt = "sample_prop_xml_format_2.xml";
    char const *data = "sample_prop_data_2.xml";
    char const *out_xml = "exp31.xml";
    char const *out_xml2 = "exp32.xml";
    ErrorInfo err;
    LOG("--Mix of hierarchical compound name and simple format");
    LOG("Starting import compound name");
    info_import(xml_fmt, db_sch, db_name, data, &err);
    DUMP_ERROR_INFO(err);

    LOG("Starting export simple format");
    info_export_property_db(db_name, out_xml, &err);
    DUMP_ERROR_INFO(err);

    LOG("Starting import simple import");
    info_import_property_db(db_name, out_xml, &err);
    DUMP_ERROR_INFO(err);

    LOG("Starting export compound name");
    info_export(xml_fmt, db_sch, db_name, out_xml2, &err);
    DUMP_ERROR_INFO(err);

}

void import_update()
{
    ErrorInfo err;
    ErrorCode rc;
    InfoResult res;
    char* str_value = NULL;
    InfoDBHandle *handle = NULL;

    char const *db_name ="partial.db";
    char const *db_sch = "sample_prop_db_schema.xml";
    char const *xml_fmt = "sample_prop_xml_format.xml";
    char const *data = "sample_prop_data.xml";

    LOG("--Start test of partial import");
    LOG("Insert initial two records");
    // write to properties DB two records
    handle = info_opendb(db_name, INFO_READ_WRITE);
    res = info_writeString(handle, INFO_ROOT, "com/verifone/adk/version", "0.9");
    LOG("info_writeString rc: %s", (res == INFO_OK) ? "ok": "fail");
    res = info_writeString(handle, INFO_ROOT, "just_name", "just_value");
    LOG("info_writeString rc: %s", (res == INFO_OK) ? "ok": "fail");
    info_closedb(handle);

    LOG("Starting import");
    info_partial_import(xml_fmt, db_sch, db_name, data, &err);
    DUMP_ERROR_INFO(err);
    error_info_free(&err);

    LOG("Test initial two records");
    // one record should be overwritten by import
    handle = info_opendb(db_name, INFO_READ_WRITE);
    res = info_readString(handle, INFO_ROOT, "com/verifone/adk/version", &str_value);
    LOG("info_readString rc: %s", (res == INFO_OK && str_value) ? "ok": "fail");
    if(res == INFO_OK)
    {
        LOG("read \"%s\", expected \"%s\" - %s", str_value, "1.5", 0 == strcmp(str_value, "1.5") ? "ok" : "fail");
        free(str_value);
    }

    // second record should remain unchanged
    res = info_readString(handle, INFO_ROOT, "just_name", &str_value);
    LOG("info_readString rc: %s", (res == INFO_OK && str_value) ? "ok": "fail");
    if(res == INFO_OK)
    {
        LOG("read \"%s\", expected \"%s\" - %s", str_value, "just_value", 0 == strcmp(str_value, "just_value") ? "ok" : "fail");
        free(str_value);
    }
    info_closedb(handle);

}

void err_reporting()
{
    char const *db_name ="scond.db";
    char const *db_sch = "sample_prop_db_schema.xml";
    char const *xml_fmt = "sample_prop_xml_format_2.xml";
    char const *data = "bad_data.xml";
    ErrorInfo err;
    LOG("--Start test where we do error reporting");
    LOG("Starting import");
    info_import(xml_fmt, db_sch, db_name, data, &err); 
    DUMP_ERROR_INFO(err);
}

void select_recs()
{
    sqlite3* handle = open_db("first.db");
    execute_query(handle, "SELECT * FROM sqlite_master;", NULL);
    execute_query(handle, "SELECT * FROM "PROP_NAME";", NULL);

    sqlite3_close(handle);
}

void insert_prepared()
{

    sqlite3* handle = open_db("first.db");

    int rc = sqlite3_exec(handle, "BEGIN TRANSACTION;", NULL, NULL, NULL);
    if(rc != SQLITE_OK)
    {
        LOG_ERR("failed to start a transaction[%d]: %s", rc, sqlite3_errmsg(handle));
        return;
    }

    const char *query = "INSERT INTO "PROP_NAME"(PARENT_ID, NODE_NAME, VALUE) VALUES (?1, ?2, ?3);";
    sqlite3_stmt* st = prepare_statement(handle, query);

    //need two buffers for binding, as this memory won't get copied on bind()
    char name[30] = {0};
    char value[30] = {0};
    for(int i = 0; i < 20; i++)
    {
        rc = sqlite3_bind_int(st, 1, 1);     
        if(rc != SQLITE_OK)
            LOG_ERR("sqlite3_bind_int: %d", rc);

        snprintf(name, sizeof(name), "Name%d", i);
        rc = sqlite3_bind_text(st, 2, name, strlen(name), NULL);
        if(rc != SQLITE_OK)
            LOG_ERR("sqlite3_bind_text: %d", rc);

        snprintf(value, sizeof(value), "Value%d", i);
        rc = sqlite3_bind_text(st, 3, value, strlen(value), NULL);
        if(rc != SQLITE_OK)
            LOG_ERR("sqlite3_bind_text: %d", rc);

        //step through statement
        rc = sqlite3_step(st);
        if(rc != SQLITE_DONE)
            LOG_ERR("step returned %d", rc);

        //reset, so it can be reused
        rc = sqlite3_reset(st);
        if(rc != SQLITE_OK)
            LOG_ERR("sqlite3_reset: %d", rc);

        if(rc != SQLITE_OK)
        {
            sqlite3_exec(handle, "ROLLBACK;", NULL, NULL, NULL);
            LOG_ERR("Rolling back and returning");
            return;
        }
    }

    //always finalize prepared statements,
    //otherwise there will be a resource leak
    sqlite3_finalize(st);

    rc = sqlite3_exec(handle, "COMMIT;", NULL, NULL, NULL);
    if(rc != SQLITE_OK)
    {
        LOG_ERR("failed to commit a transaction[%d]: %s", rc, sqlite3_errmsg(handle));
        return;
    }

    //were they inserted?
    LOG("Executing SELECT after inserting some records");
    execute_query(handle, "SELECT * FROM "PROP_NAME";", NULL);
    sqlite3_close(handle);
}

int main(int argc, char **argv)
{
#ifdef _VRXEVO
    EOSLOG_INIT("INFUTIL", LOGSYS_OS, 0xFFFFFFFF);
#endif
    syntax1_impexp();
    syntax2_impexp();
    syntax3_impexp();
    import_update();
    err_reporting();

    //SQL queries
    select_recs();
    insert_prepared();
}
