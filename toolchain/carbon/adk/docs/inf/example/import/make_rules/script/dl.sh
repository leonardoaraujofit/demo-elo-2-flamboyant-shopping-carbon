#!/bin/bash

argv0=$0

function usage {
   echo "Usage: $argv0 <httplink> [--component <name>] [--tag <tag>] [--targets=<targets>]"
   echo "                         [--user=<user>] [--password=<password>] [--dir=<dir>]"
   exit 1
}

function clean_up {
  # housekeeping
  if [ -d $STAGING_DIR ]; then rm -rf $STAGING_DIR; fi
  trap 0  # reset to default action
  exit
}

if [ $# -lt 1 ]; then usage; fi

LINK=$1
shift 1

# set defaults for some variables
COMPONENT=$(echo $LINK | sed "s@.*/ADK/\(.*\)/\(.*\)/.*@\1@g" | tr [:upper:] [:lower:])
TAG=$(echo $LINK | sed "s@.*/ADK/\(.*\)/\(.*\)/.*@\1-\2@g")
TARGETS="vos vrx"
USERNAME=adk_automation_ro
PASSWORD=Y39aZ5
DOWNLOAD_DIR=dl

while [ $# -gt 1 -a "${1:0:2}" = "--" ] ; do
    case "$1" in
        --component)
            COMPONENT="$2"
            shift 2
            ;;
        --tag)
            TAG="$2"
            shift 2
            ;;
        --targets)
            TARGETS="$2"
            shift 2
            ;;
        --user)
            USERNAME="$2"
            shift 2
            ;;
        --password)
            PASSWORD="$2"
            shift 2
            ;;
        --dir)
            DOWNLOAD_DIR="$2"
            shift 2
            ;;
        *)
            usage
            ;;
    esac
done


STAGING_DIR=$DOWNLOAD_DIR/staging_dir
EXTRACT_DIR=$STAGING_DIR/extract

trap clean_up 0 1 2 3 15
mkdir -p $STAGING_DIR

proceed=0

ZIP_FILE=`basename $LINK | tr [:upper:] [:lower:]`
echo "checking for $ZIP_FILE"

if [ ! -f $DOWNLOAD_DIR/$ZIP_FILE ]; then
  (cd $STAGING_DIR; wget --http-user=$USERNAME --http-password=$PASSWORD $LINK)
  FILE=`ls $STAGING_DIR`
  mv $STAGING_DIR/$FILE $STAGING_DIR/$ZIP_FILE 2>/dev/null
  if [ ! -f $STAGING_DIR/*.zip ]; then
    echo "Couldn't download $LINK, exit."
    exit 1
  fi
  mv $STAGING_DIR/*.zip $DOWNLOAD_DIR/$ZIP_FILE
  proceed=1
fi

if [ $proceed -eq 0 ]; then
  for target in $TARGETS;
  do
    # proceed, if no tag exists and target was not extracted before
    if [ ! -f $DOWNLOAD_DIR/$target/$COMPONENT/$TAG ]; then
      proceed=1
      break
      fi
    # proceed, if ZIP file is newer than the tag file
    if [ $DOWNLOAD_DIR/$ZIP_FILE -nt $DOWNLOAD_DIR/$target/$COMPONENT/$TAG ]; then
      proceed=1
      break
    fi
  done
fi

# we are done
if [ $proceed -eq 0 ]; then exit 0; fi

mkdir -p $EXTRACT_DIR
#extract distribution bundles into staging directory
(cd $EXTRACT_DIR; unzip ../../$ZIP_FILE)

# delete previous tags
for target in $TARGETS;
do 
  for tagfile in `ls $DOWNLOAD_DIR/$target/$COMPONENT | xargs`;
  do
    if [ -f $DOWNLOAD_DIR/$target/$COMPONENT/$tagfile ];
    then
      for line in $(cat ${DOWNLOAD_DIR}/${target}/${COMPONENT}/${tagfile});
      do
        # remove the file (if any)
        if [ -f import/$target/$line ];
        then
          rm -f import/$target/$line
          # empty folder left -> remove it
          folder=`dirname import/$target/$line`
          folderfiles=`ls -A $folder`
          if [ -z "$folderfiles" ]; then rm -rf $folder; fi
        fi
        
        # remove folder, if it is empty
        if [ -d import/$target/$line ];
        then 
          folderfiles=`ls -A import/$target/$line`
          if [ -z "$folderfiles" ]; then rm -rf import/$target/$line; fi
        fi
      done
      rm -f $DOWNLOAD_DIR/$target/$COMPONENT/$TAG 2>/dev/null
    fi
  done
done

mkdir -p $STAGING_DIR/vrx
mkdir -p $STAGING_DIR/vos

#check if the files are the correct format, copy properly
if [ -d $EXTRACT_DIR/VOS ] || [ -d $EXTRACT_DIR/vos ] || [ -d $EXTRACT_DIR/VRX ] || [ -d $EXTRACT_DIR/vrx ]
then
  if [ -d $EXTRACT_DIR/VOS ]
  then
    rmdir $STAGING_DIR/vos
    mv $EXTRACT_DIR/VOS $STAGING_DIR/vos
  fi
  
  if [ -d $EXTRACT_DIR/vos ]
  then
    rmdir $STAGING_DIR/vos
    mv $EXTRACT_DIR/vos $STAGING_DIR/vos
  fi
  
  if [ -d $EXTRACT_DIR/VRX ]
  then
    rmdir $STAGING_DIR/vrx
    mv $EXTRACT_DIR/VRX $STAGING_DIR/vrx
  fi
  
  if [ -d $EXTRACT_DIR/vrx ]
  then
    rmdir $STAGING_DIR/vrx
    mv $EXTRACT_DIR/vrx $STAGING_DIR/vrx
  fi
  
  # If the contents are already extracted, then zip again
  # We check if there is a .zip in the folder, if yes we assume it is not extracted, but well-packaged
  # We need to delete everything which is not a zip, as not to confuse the final code-block
  if [ -d $STAGING_DIR/vos ]
  then
    if [ -z "$(ls $STAGING_DIR/vos/ | grep .zip)" ];
    then
      (cd $STAGING_DIR/vos; zip -r $COMPONENT-$TAG-vos-dev.zip *)
      mv $STAGING_DIR/vos/$COMPONENT-$TAG-vos-dev.zip $STAGING_DIR
      rm -Rf $STAGING_DIR/vos/
      mkdir $STAGING_DIR/vos/
      mv $STAGING_DIR/$COMPONENT-$TAG-vos-dev.zip $STAGING_DIR/vos/
    fi
  fi
  
  if [ -d $STAGING_DIR/vrx ]
  then
    if [ -z "$(ls $STAGING_DIR/vrx/ | grep .zip)" ]
    then
      (cd $STAGING_DIR/vrx; zip -r $COMPONENT-$TAG-vrx-dev.zip *)
      mv $STAGING_DIR/vrx/$COMPONENT-$TAG-vrx-dev.zip $STAGING_DIR
      rm -Rf $STAGING_DIR/vrx/
      mkdir $STAGING_DIR/vrx/
      mv $STAGING_DIR/$COMPONENT-$TAG-vrx-dev.zip $STAGING_DIR/vrx/
    fi
  fi
else
  #Single platform package
  if [ -d $EXTRACT_DIR/include ] || [ -d $EXTRACT_DIR/lib ]
  then
    if [ -n "$(echo $LINK | awk -F/ '{print $NF}' | grep vos)" ]
    then
      (cd $EXTRACT_DIR; zip -r $COMPONENT-$TAG-vos-dev.zip *)
      mv $EXTRACT_DIR/$COMPONENT-$TAG-vos-dev.zip $STAGING_DIR/vos
    else
      if [ -n "$(echo $LINK | awk -F/ '{print $NF}' | grep vrx)" ]
      then
        (cd $EXTRACT_DIR; zip -r $COMPONENT-$TAG-vrx-dev.zip *)
        mv $EXTRACT_DIR/$COMPONENT-$TAG-vrx-dev.zip $STAGING_DIR/vrx
      else
        echo "Did not recognize format"
      fi
    fi
  fi
fi

# extract libraries
for target in $TARGETS;
do   
  #folders are named VOS and VRX
  target_upper=$(echo $target | tr [:lower:] [:upper:])
  
  # handle captial letters of folders
  mv $STAGING_DIR/$target_upper $STAGING_DIR/$target 2>/dev/null
  
  for file in `ls $STAGING_DIR/$target/`;
  do
    if [[ "$file" == *"$target"*"barcodelibs"* ]] \
    || [[ "$file" == *"$target"*"dev.zip"* ]] \
    || [[ "$file" == *"$target"*"vats.zip"* ]] \
    || [[ "$target" == "vrx" ]];
    then
      rm -rf $DOWNLOAD_DIR/$target/$COMPONENT
      mkdir -p $DOWNLOAD_DIR/$target/$COMPONENT
      # add the file list to tag file and extract the archive to import
      unzip -l $STAGING_DIR/$target/$file | cut -c29- | tail -n +4 | head -n -2 | tr -d '\r' > $DOWNLOAD_DIR/$target/$COMPONENT/$TAG      
      unzip -o $STAGING_DIR/$target/$file -d import/$target
    fi
  done
done

