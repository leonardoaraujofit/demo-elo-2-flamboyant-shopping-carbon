# see also http://mad-scientist.net/make/autodep.html

# $(NAME) name of the program/library to be created
# $(TARGET) architecture, e.g. "x86"
# $(VARIANT) variant, one of  "release" or "debug"
# $(TYPE) one of "program", "staticlib" or "sharedlib"


ifeq "$(VARIANT)" "debug"
$(TARGET)_LDFLAGS_debug:=-Wl,-rpath,'$$$$ORIGIN' $($(TARGET)_LDFLAGS_debug)  $($(TARGET)_LDFLAGS) $(LDFLAGS)
ifeq "$(TYPE)" "sharedlib"
ifneq (,$(findstring --version-script=,$(TARGET)_LDFLAGS_debug))
$(TARGET)_SHAREDLIB:=-fvisibility=hidden
endif
endif
$(TARGET)_CFLAGS_debug:=-g3 -D_DEBUG $($(TARGET)_SHAREDLIB) $($(TARGET)_CFLAGS_debug) $($(TARGET)_CFLAGS)  $(CFLAGS)
else
$(TARGET)_LDFLAGS_release:=-Wl,-s -Wl,-rpath,'$$$$ORIGIN' $($(TARGET)_LDFLAGS_release) $($(TARGET)_LDFLAGS) $(LDFLAGS) -Wl,--gc-sections
ifeq "$(TYPE)" "sharedlib"
ifneq (,$(findstring --version-script=,$(TARGET)_LDFLAGS_release))
$(TARGET)_SHAREDLIB:=-fvisibility=hidden
endif
endif
$(TARGET)_CFLAGS_release:=-Os $($(TARGET)_SHAREDLIB) $($(TARGET)_CFLAGS_release) $($(TARGET)_CFLAGS)  $(CFLAGS) -ffunction-sections -fdata-sections
endif

ifeq "$(TYPE)" "sharedlib"
$(TARGET)_CFLAGS_$(VARIANT):=-fPIC $($(TARGET)_CFLAGS_$(VARIANT))
$(TARGET)_LDFLAGS_$(VARIANT):=-fPIC $($(TARGET)_LDFLAGS_$(VARIANT))
endif

$(TARGET)_ALLSOURCE:=$(SOURCE) $($(TARGET)_SOURCE)

$(TARGET)_OBJ_$(VARIANT):=$(patsubst %,obj/$(NAME)/$(TARGET)-$(VARIANT)/%,$(patsubst %.cpp,%.o,$(patsubst %.cxx,%.o,$($(TARGET)_ALLSOURCE:%.c=%.o))))
$(TARGET)_DEP_$(VARIANT):=$(patsubst %,obj/$(NAME)/$(TARGET)-$(VARIANT)/%,$(patsubst %.cpp,%.d,$(patsubst %.cxx,%.d,$($(TARGET)_ALLSOURCE:%.c=%.d))))

$(TARGET)_LOCAL_LIBS_:=$(foreach z,$(LOCAL_LIBS),$(TARGET)-$(VARIANT)/$(z)) $(foreach z,$($(TARGET)_LOCAL_LIBS),$(TARGET)-$(VARIANT)/$(z))
$(TARGET)_LOCAL_LIBS_2:=$(patsubst $(TARGET)-$(VARIANT)/lib%,-l%,$(patsubst %.a,%,$(patsubst %.so,%,$($(TARGET)_LOCAL_LIBS_))))
$(TARGET)_SYSLIBS:=$($(TARGET)_LIBS) $($(TARGET)_LIBS_$(VARIANT)) $(LIBS) 

# to enforce immediate expansion first define as macro and then use $(eval $(call macro_template)) 
# note that $ needs to be duplicated for all variables but the ones that should be replaced immediately
define macro_gcc_rules

.PHONY: $(TARGET)-$(VARIANT) $(TARGET)-$(VARIANT)-build

$(TARGET)-$(VARIANT): $(TARGET)-$(VARIANT)-build
$(TARGET)-$(VARIANT)-build: $(TARGET)-$(VARIANT)/$(NAME)

$(TARGET)-$(VARIANT)/$(NAME): $($(TARGET)_OBJ_$(VARIANT)) $($(TARGET)_LOCAL_LIBS_)
	mkdir -p $(TARGET)-$(VARIANT)
ifeq "$(TYPE)" "program"
	$($(TARGET)_LN) -o $$@ $($(TARGET)_OBJ_$(VARIANT)) $($(TARGET)_LDFLAGS_$(VARIANT)) -L $(TARGET)-$(VARIANT) $($(TARGET)_LOCAL_LIBS_2) $($(TARGET)_SYSLIBS)
else
ifeq "$(TYPE)" "sharedlib"
	$($(TARGET)_LN) -shared -o $$@ $($(TARGET)_OBJ_$(VARIANT)) $($(TARGET)_LDFLAGS_$(VARIANT)) -L $(TARGET)-$(VARIANT) $($(TARGET)_LOCAL_LIBS_2) $($(TARGET)_SYSLIBS)
else
ifeq "$(TYPE)" "staticlib"
	-rm -f $$@ 2>/dev/null
	$($(TARGET)_AR) qcs $$@ $($(TARGET)_OBJ_$(VARIANT))
endif
endif
endif

obj/$(NAME)/$(TARGET)-$(VARIANT)/%.o: %.c
	@(z=$$@; mkdir -p $$$${z%/*})
	$($(TARGET)_CC) $($(TARGET)_CFLAGS_$(VARIANT)) -MMD -c -o $$@ $$<
	@cp $$(@:.o=.d) $$(@:.o=.d2); \
	sed -e 's/#.*//' -e 's/^[^:]*: *//' -e 's/ *\\$$$$//' \
		-e '/^$$$$/ d' -e 's/$$$$/ :/' < $$(@:.o=.d) >> $$(@:.o=.d2); \
	mv $$(@:.o=.d2) $$(@:.o=.d)

obj/$(NAME)/$(TARGET)-$(VARIANT)/%.o: %.cpp
	@(z=$$@; mkdir -p $$$${z%/*})
	$($(TARGET)_CXX) $($(TARGET)_CFLAGS_$(VARIANT)) $($(TARGET)_CPPFLAGS_$(VARIANT)) -MMD -c -o $$@ $$<
	@cp $$(@:.o=.d) $$(@:.o=.d2); \
	sed -e 's/#.*//' -e 's/^[^:]*: *//' -e 's/ *\\$$$$//' \
		-e '/^$$$$/ d' -e 's/$$$$/ :/' < $$(@:.o=.d) >> $$(@:.o=.d2); \
	mv $$(@:.o=.d2) $$(@:.o=.d)

obj/$(NAME)/$(TARGET)-$(VARIANT)/%.o: %.cxx
	@(z=$$@; mkdir -p $$$${z%/*})
	$($(TARGET)_CXX) $($(TARGET)_CFLAGS_$(VARIANT)) -MMD -c -o $$@ $$<
	@cp $$(@:.o=.d) $$(@:.o=.d2); \
	sed -e 's/#.*//' -e 's/^[^:]*: *//' -e 's/ *\\$$$$//' \
		-e '/^$$$$/ d' -e 's/$$$$/ :/' < $$(@:.o=.d) >> $$(@:.o=.d2); \
	mv $$(@:.o=.d2) $$(@:.o=.d)

endef
$(eval $(call macro_gcc_rules))


-include $($(TARGET)_DEP_DEBUG) $($(TARGET)_DEP_$(VARIANT))
