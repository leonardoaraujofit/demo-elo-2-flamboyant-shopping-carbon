@set DDL_PORT=%1
@if "%DDL_PORT%"=="" (
@set DDL_PORT=9
echo "Usage: %0 <DDL_PORT>"
)
make vrx-debug-load -f Makefile.test vrx_DDL_PORT=%DDL_PORT%