#ifndef LOGGING_H_
#define LOGGING_H_

#ifdef _VRXEVO
#   include <eoslog.h>
#   define LOG_ERR(format, ...) LOG_PRINTFF(1, "ERROR: "format" [%s:%d]", ## __VA_ARGS__,__FILE__,__LINE__)
#elif defined _VOS
#	  include <stdio.h>
#   define LOG_ERR(format, ...) do { printf("ERROR: "format" [%s:%d]\n", ## __VA_ARGS__,__FILE__,__LINE__); fflush(stdout); } while(0)
#else
#	include <stdio.h>
#   define LOG_ERR(format, ...) do { printf("ERROR: "format" [%s:%d]\n", ## __VA_ARGS__,__FILE__,__LINE__); fflush(stdout); } while(0)
#endif

#ifdef _VRXEVO
#   include <eoslog.h>
#   define LOG(format, ...) LOG_PRINTFF(1, format, ## __VA_ARGS__)
#elif defined _VOS
#   include <stdio.h>
#   define LOG(format, ...) do { printf(format"\n", ## __VA_ARGS__); fflush(stdout); } while(0)
#else
#   include <stdio.h>
#   define LOG(format, ...) do { printf(format"\n", ## __VA_ARGS__); fflush(stdout); } while(0)
#endif

#endif //LOGGING_H_
