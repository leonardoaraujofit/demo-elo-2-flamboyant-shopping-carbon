//pthread includes time.h, use this define,
//to avoid redeclaration errors
#define _USE_TIMEVAL_FROM_SVC_NET_H_

#include <libevt.h>

#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <string>
#include <errno.h>
#include <sys/time.h>

#ifdef _VRXEVO
#   include <ceif.h>
#   include <ceifConst.h>
#   include <SVC.h>
#   include <SVC_NET.h>
#   include <eoslog.h>
#   define sleep(x) SVC_WAIT(x * 1000)
#   define usleep SVC_WAIT
#else
#   include <fcntl.h>//open for V/OS
#   include <stdio.h> //remove for V/OS
#   include <ifaddrs.h>
#   include <arpa/inet.h> //in_addr, htons, inet_ntoa
#endif

#include "logging.h"



//////////////////   UTILITY  //////////////////////////////////

//for outputting UIDs with %s format specifier
std::string ulltostr(uint64_t ull)
{
    std::string res;
    if (!ull)
        res = "0";
    else
    {
        const int num_digits = 1 + (int) floor(log10((double)ull));
        res.reserve(num_digits);

        for (int i = num_digits - 1; i >= 0; i--)
        {
            uint64_t base = pow((double)10, i);
            uint64_t q = ull / base;

            res += ('0' + (char)q);
            ull = ull % base;
        }
    }
    return res;
}

//for measuring time
int64_t get_time_ms()
{
    struct timeval start_time;
    gettimeofday(&start_time, 0);
    int64_t time_msec = start_time.tv_sec;
    time_msec *= 1000;
    long int ms = start_time.tv_usec / 1000;
    time_msec += ms;

    return time_msec;
}

//for outputting title at the start of each example
static int title_n = 0;
#define TITLE(x, ...) do { ++title_n; LOG(" "); LOG("-------------------------------"); LOG("%d) "x, title_n, ##__VA_ARGS__); LOG("-------------------------------"); } while(0)

//to get length of non-char arrays
#define lenof(x) sizeof(x)/sizeof(x[0])

#ifdef _VRXEVO
    void get_ip_address(int ceHandle, std::string &out)
    {
        unsigned int bogus;
        stNI_IPConfig ipconf;
        ip_addr  addr;

        //get IP info
        ceGetNWParamValue(ceHandle, IP_CONFIG,  &ipconf, sizeof(ipconf), &bogus);

        //32-bit IPv4 to dotted string
        addr = ipconf.ncIPAddr;
        char buf[17] = { 0 };
        inet_ntoa(addr, buf);
        out = buf;
    }

    int try_starting_eth()
    {
        //Register daemon with CommEngine
        ceRegister();
        ceEnableEventNotification();

        int eth_handle = -1;

        //get number of network interfaces
        unsigned int count = ceGetNWIFCount();
        stNIInfo *info_array = new stNIInfo[count];
        LOG("Number of interfaces: %d", count);
        memset(info_array, 0, count);

        //get network interface information
        ceGetNWIFInfo(info_array, count, &count);

        for(unsigned int i = 0; i < count; i++)
        {
            //search for ethernet interface
            LOG("DeviceDriveType: %d (%s)", info_array[i].niDeviceDriverType, info_array[i].niDeviceDriverName);
            if(info_array[i].niDeviceDriverType == CE_DRV_TYPE_ETHERNET)
            {
                eth_handle = info_array[i].niHandle;
            }
        }
        delete [] info_array;

        //start ethernet interface  
        //second param: full connect (see Verix eVo Volume II *ceStartNWIF*)
        ceStartNWIF(eth_handle, CE_CONNECT);

        int retries = 5;
        while(retries-- > 0)
        {
            if(ceGetEventCount() > 0)
            {

                stceNWEvt evt;
                ceGetEvent(&evt, 0, NULL, NULL);

                if(evt.neEvt == CE_EVT_NET_UP)
                    break;
            }
            sleep(5);
        }
        return eth_handle;
    }

#else

    void get_ip_address(std::string &out)
    {

        int rc = system("udhcpc");
        if(rc < 0 )
            LOG("Invoking \"udhcpc\" returned %d, errno %d", rc, errno);

        struct ifaddrs *ifaddr, *ifa;

        if (getifaddrs(&ifaddr) == -1) 
        {
            LOG("call to getifaddrs() failed");
            return;
        }

        ifa = ifaddr;
        do
        {
            struct sockaddr_in *sock = (struct sockaddr_in *) ifa->ifa_addr;
            const char *addr = inet_ntoa(sock->sin_addr);
            int family = ifa->ifa_addr->sa_family;

            if( !strcmp(ifa->ifa_name, "eth0") && (family == AF_INET) )
            {
                out = addr;
                break;
            }
        }
        while((ifa = ifa->ifa_next) != NULL);
        freeifaddrs(ifaddr);
    }

#endif

///////////////// END OF UTILITY ///////////////////////////////

int const EVENT_ID = 12;
const char DATA = 'd';
const int DATA_IDX = 10;
const int COMPONENT_ID = 1;

void* waiting_thread(void *p)
{
    Event event;

    //null structure, so raised_* are all zeroes
    memset(&event, 0, sizeof(Event));

    //make UID for the event
    event.id = evt_make_uid(COMPONENT_ID, EVENT_ID);
    LOG("Waiter: created event with id %s", ulltostr(event.id).c_str());

    LOG("Waiter: raised_flag before evt_wait() %d", event.raised_flag);
    int ret = evt_wait(&event, 5000);
    LOG("Waiter: evt_wait returned %d", ret);
    LOG("Waiter: raised_flag after evt_wait() %d", event.raised_flag);
	LOG("Waiter: Expected data: %d, actual data: %d", DATA, event.raised_data[DATA_IDX]);
    return NULL;
}

void wait_raise_sync()
{
    TITLE("Wait/Raise using synchronous API");

    //Create waiting thread
    pthread_t t1;
    pthread_create(&t1, NULL, &waiting_thread, NULL);

    //wait for waiting_thread to start
    //sleep(3);

	Event event;
    //null structure, so raised_* are all zeroes
	memset(&event, 0, sizeof(Event));
    //Make UID for the event (same as in waiting_thread, because we use the same IDs)
	event.id = evt_make_uid(COMPONENT_ID, EVENT_ID);
    LOG("Raiser: created event with id %s", ulltostr(event.id).c_str());

    //put payload into structure
	event.raised_data[DATA_IDX] = DATA;
	//event.raised_handle = 1;

    LOG("Raiser: raised_flag before evt_raise() %d", event.raised_flag);

	// Raise an event
	int res = evt_raise(&event);
    LOG("Raiser: evt_raise() returned %d", res);

	pthread_join(t1, NULL);
}

void wait_raise_async()
{
    TITLE("Wait/Raise using asynchronous API");

    Event event;
    //null structure, so raised_* are all zeroes
    memset(&event, 0, sizeof(Event));

    //Make UID for the event 
    event.id = evt_make_uid(COMPONENT_ID, EVENT_ID);
    LOG("Created event with id %s for waiter", ulltostr(event.id).c_str());

    //Create async waiter
	WaiterHandle hdl = evt_init_waiter(&event, 1);

	// Raise an event
	Event event_r;
    //null structure, so raised_* are all zeroes
	memset(&event_r, 0, sizeof(Event));

    //Make UID for the event 
	event_r.id = evt_make_uid(COMPONENT_ID, EVENT_ID);
    LOG("Created event with id %s for evt_raise()", ulltostr(event.id).c_str());

	event_r.raised_data[DATA_IDX] = DATA;
    LOG("raised_flag before evt_raise() %d", event.raised_flag);
    int res = evt_raise(&event_r);
    LOG("evt_raise() returned %d", res);


    LOG("raised_flag before evt_wait_by_handle() %d", event.raised_flag);

    //now we wait (blocking) 5 sec
    //WAIT_ANY that first raised event will stop waiting call
    res = evt_wait_by_handle(hdl, 5, WAIT_ANY);
    LOG("evt_wait_by_handle() returned %d", res);
    if(res == 0)
    {
        LOG("raised_flag before evt_peek_by_handle() %d", event.raised_flag);
        //now we peek event information ( events are left raised )
        res = evt_peek_by_handle(hdl, &event, 1);
        LOG("evt_peek_by_handle() returned %d", res);
        if(res == 0)
        {
            LOG("raised_flag before evt_get_by_handle() %d", event.raised_flag);
        }
        //now we get event information ( events are unmarked )
        res = evt_get_by_handle(hdl, &event, 1);
        LOG("evt_get_by_handle() returned %d", res);
        if(res == 0)
        {
            LOG("raised_flag after evt_wait_by_handle() %d", event.raised_flag);
        }
    }
	LOG("Expected data: %d, actual data: %d", DATA, event.raised_data[DATA_IDX]);
    
}

void wait_raise_multiple_waiters()
{
    TITLE("Wait/Raise with multiple waiters");

    Event event1;
    Event event2;

    //null structure, so raised_* are all zeroes
    memset(&event1, 0, sizeof(Event));
    memset(&event2, 0, sizeof(Event));

    //Make UID for the event 
    event1.id = evt_make_uid(COMPONENT_ID, EVENT_ID);
    LOG("Created event with id %s for waiter", ulltostr(event1.id).c_str());

    //Make UID for the event 
    event2.id = evt_make_uid(COMPONENT_ID, EVENT_ID);
    LOG("Created event with id %s for waiter", ulltostr(event2.id).c_str());

    //Create async waiter
	WaiterHandle hdl1 = evt_init_waiter(&event1, 1);
	WaiterHandle hdl2 = evt_init_waiter(&event2, 1);

	// Raise an event
	Event event_r;
    //null structure, so raised_* are all zeroes
	memset(&event_r, 0, sizeof(Event));

    //Make UID for the event 
	event_r.id = evt_make_uid(COMPONENT_ID, EVENT_ID);
    LOG("Created event with id %s for evt_raise()", ulltostr(event_r.id).c_str());

	event_r.raised_data[DATA_IDX] = DATA;
    int res = evt_raise(&event_r);
    LOG("evt_raise() returned %d", res);

    LOG("----Results----");
    res = evt_wait_by_handle(hdl1, 5, WAIT_ANY);
    LOG("evt_wait_by_handle(hdl1) returned %d", res);
    res = evt_get_by_handle(hdl1, &event1, 1);
    LOG("evt_get_by_handle(hdl1) returned %d", res);
    LOG("raised_flag after evt_get_by_handle(hdl1) %d", event1.raised_flag);
	LOG("Expected data: %d, actual data: %d", DATA, event1.raised_data[DATA_IDX]);

    res = evt_wait_by_handle(hdl2, 5, WAIT_ANY);
    LOG("evt_wait_by_handle(hdl2) returned %d", res);
    res = evt_get_by_handle(hdl2, &event2, 1);
    LOG("evt_get_by_handle(hdl2) returned %d", res);
    LOG("raised_flag after evt_get_by_handle(hdl2) %d", event2.raised_flag);
	LOG("Expected data: %d, actual data: %d", DATA, event2.raised_data[DATA_IDX]);
}

void wait_raise_timers()
{
    TITLE("Start and wait for multiple timer events (with policy WAIT_ALL)");

    //will wait for total of two events
    Event events[2];

    //null structure, so raised_* are all zeroes
    memset(events, 0, sizeof(events));

    //Make UIDs for the events
    events[0].id = evt_make_uid_os(1, OS_EVT_TIMER);
    events[1].id = evt_make_uid_os(1, OS_EVT_TIMER);
    LOG("Created event with id %s", ulltostr(events[0].id).c_str());
    LOG("Created event with id %s", ulltostr(events[1].id).c_str());

    //10sec 
    int hdl_10s = evt_start_timer(10000);
    //3sec
    int hdl_3s = evt_start_timer(3000);

    //assign handles to structures
    events[0].handle = hdl_10s;
    events[1].handle = hdl_3s;

    //Create async waiter
    WaiterHandle hdl = evt_init_waiter(events, 2);

    LOG("First timer's handle: %d", hdl_10s);
    LOG("Second timer's handle: %d", hdl_3s);

    uint64_t start = get_time_ms();
    int res1 = evt_wait_by_handle(hdl, EVT_INFINITE_WAIT, WAIT_ALL);
    int res2 = evt_get_by_handle(hdl, events, 2);
    uint64_t delta = get_time_ms() - start;
    LOG("----Results----");
    LOG("evt_wait_by_handle() returned %d", res1);
    LOG("evt_get_by_handle() returned %d", res2);
    LOG("First event's raised_handle %d", (int) events[0].raised_handle);
    LOG("Seconds event's raised_handle %d", (int) events[1].raised_handle);
    LOG("First event's raised_flag %d", events[0].raised_flag);
    LOG("Seconds event's raised_flag %d", events[1].raised_flag);
    LOG("Time passed: %.4fs", delta/1000.0);

}

void wait_raise_any_timers()
{
    TITLE("Start and wait for multiple timer events by using EVT_ANY_HANDLE");

    //will wait for total of two events
    Event event;

    //null structure, so raised_* are all zeroes
    memset(&event, 0, sizeof(event));

    //Make UIDs for the events
    event.id = evt_make_uid_os(1, OS_EVT_TIMER);
    LOG("Created event with id %s", ulltostr(event.id).c_str());

    //10sec 
    int hdl_10s = evt_start_timer(10000);
    //1sec
    int hdl_1s = evt_start_timer(1000);
    //3sec
    int hdl_3s = evt_start_timer(3000);

    //assign EVT_ANY_HANDLE to struct's handle 
    event.handle = EVT_ANY_HANDLE;

    LOG("10sec timer's handle: %d", hdl_10s);
    LOG("3sec timer's handle: %d", hdl_3s);
    LOG("1sec timer's handle: %d", hdl_1s);

    uint64_t start = get_time_ms();
    int res = evt_wait(&event, EVT_INFINITE_WAIT);
    uint64_t delta = get_time_ms() - start;
    LOG("----Results----");
    LOG("evt_wait_group() returned %d", res);
    LOG("Raised handle: %d (should be equal to [%d])", (int) event.raised_handle, hdl_1s); 
    LOG("Time passed: %.4fs (should be around 1s)", delta/1000.0);

    res = evt_wait(&event, EVT_INFINITE_WAIT);
    delta = get_time_ms() - start;
    LOG("----Results----");
    LOG("evt_wait_group() returned %d", res);
    LOG("Raised handle: %d (should be equal to [%d])", (int) event.raised_handle, hdl_3s); 
    LOG("Time passed: %.4fs (should be around 3s)", delta/1000.0);

    res = evt_wait(&event, EVT_INFINITE_WAIT);
    delta = get_time_ms() - start;
    LOG("----Results----");
    LOG("evt_wait_group() returned %d", res);
    LOG("Raised handle: %d (should be equal to [%d])", (int) event.raised_handle, hdl_10s); 
    LOG("Time passed: %.4fs (should be around 10s)", delta/1000.0);

}

void wait_network_evt()
{
    TITLE("Wait for network event (socket). Attended test -- data must be sent externally.");
    int const port = 801;
    std::string out;
#ifdef _VRXEVO
    LOG("----Network householding----");

    //start ETH interface
    //get handle to CommEngine Interface
    int eth_handle = try_starting_eth();


    //get an IP address
    get_ip_address(eth_handle, out);
#else
    get_ip_address(out);
#endif

    //open a socket
    int fd = socket(AF_INET, SOCK_DGRAM, 0);
    LOG("Socket's fd: %d", fd);

    struct sockaddr_in sock_addr;
    memset(&sock_addr, 0, sizeof(sock_addr));
    sock_addr.sin_family = AF_INET;
    sock_addr.sin_port = htons(port);
    in_addr_t addr_t = inet_addr(out.c_str());
    sock_addr.sin_addr.s_addr = addr_t;

    int res = bind(fd, (struct sockaddr *) &sock_addr, sizeof(sock_addr));
    LOG("Bind returned: %d", res);

    LOG("Send a UDP message to %s:%d", out.c_str(), port);

    Event event;
    //null fields of a struct
    memset(&event, 0, sizeof(event)); 
    event.id = evt_make_uid_os(COMPONENT_ID, OS_EVT_NETWORK);

    //assign socket's fd to event's handle
    event.handle = fd;
    res = evt_wait(&event, 30000);

    LOG("----Results----");
    LOG("evt_wait() returned :%d", res);
    LOG("raised_handle: %d, should be equal to socket's [%d]", (int) event.raised_handle, fd);
    LOG("raised_flag: %d", event.raised_flag);
}

int main(int argc, char **argv)
{
    //initialize both logging and evt
    //NOTE: always use component ID provided to evt_init()
    //in calls to evt_make_uid() and evt_make_uid_os()
	evt_init(SIDE_BOTH, COMPONENT_ID, 0);
#ifdef _VRXEVO
    EOSLOG_INIT("EVTTEST", LOGSYS_OS, 0xFFFFFFFF);
#endif

    //examples
    //single user events
    wait_raise_sync();
    wait_raise_async();
    wait_raise_multiple_waiters();

    //group timer events
    wait_raise_timers();
    wait_raise_any_timers();

    //OS event with handles
    wait_network_evt();

    //deinitialize evt
	evt_destroy();
}
