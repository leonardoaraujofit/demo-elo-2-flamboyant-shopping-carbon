import socket
import time
import sys

def main():
    argc = len(sys.argv)
    send_iter = 1
    addr = "127.0.0.1"
    if(argc > 1):
        addr = sys.argv[1]
    if(argc > 2):
        send_iter = int(sys.argv[2])
        
    print(addr)
    HOST = socket.gethostbyname(addr)
    PORT = 801
    print("Will send to {0}:{1}".format(HOST, PORT))
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    for i in range(0, send_iter):
        try:
            print("Sending")
            sock.sendto("Hi there".encode("UTF-8"), (HOST, PORT))
            time.sleep(1)
        except socket.error as msg:
            sock.close()
    
    

if __name__ == "__main__":
    main()
