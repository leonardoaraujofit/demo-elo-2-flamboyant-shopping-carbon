#!/bin/bash

argv0=$0

function usage {
   echo "Usage: $argv0 [--zipfile <name.zip>] [<files>] [--drive <drive> | --group <group>]"
   echo "   Use '--zipfile' to specify the name of ZIP file for output. Default is 'a.zip'."
   echo "   Several destination drives may be separated by '--drive' on the command line. Default is 'i'."
   echo "   Several destination groups may be separated by '--group' on the command line. Default is '1'."
   echo "   Additional files may be renamed while importing using <newname>=<oldname>"
   exit 1
}

# set to 1 to enable debug outputs
enable_debug=0
function debug {
  if [ "$enable_debug" -gt 0 ]; then echo "debug: $1"; fi
}

if [ $# = 0 ]; then usage; fi

tempdir=""
drive=""
group=""
zipfile="a.zip"

while true; do
       
    while [ $# -gt 1 -a "${1:0:2}" = "--" ] ; do
        debug "parameter $1"
        case "$1" in
            --drive)
                drive="$2"
                if [ -n "$drive" ]; then
                  # check if we have a valid drive
                  drive=`echo "$drive" | tr '[:upper:]' '[:lower:]'`
                  if [ "$drive" != "i" -a "$drive" != "f" -a "$drive" != "n" ] ; then usage; fi
                fi
                debug "drive is $drive"
                shift 2
                ;;
            --group)
                group="$2"
                if [ -n "$group" ]; then
                  # check if group is a digit between 1 and 15
                  nodigits=`echo "$group" | sed "s/[[:digit:]]//g"`
                  if [ -n "$nodigits" ] ; then usage; fi
                  if [ "$group" -gt 15 -o "$group" -lt 1 ]; then usage; fi
                fi
                debug "group is $group"
                shift 2
                ;;
            --zipfile)
                zipfile="$2"
                ext="${zipfile##*.}"
                # check for file extension "zip"
                if [ "$ext" != "zip" ]; then usage; fi
                shift 2
                ;;
            *) 
                debug "1. usage"
                usage
                ;;
        esac
    done

    if [ $# = 0 ]; then 
      debug "2. usage"
      usage;
    fi

    # generate temp folder from zip name, e.g. a.zip will be a_zip
    if [ -z "$tempdir" ]; then
       tempdir="${zipfile##*/}"
       tempdir=`echo "$tempdir" | sed "s/\./_/g"`
       debug "tempdir=$tempdir"
       if [ -z "$tempdir" ] ; then usage; fi
       if [ -d "$tempdir" ] ; then
          echo "Folder \"$tempdir\" already exists. Please remove or use another filename for the zip."
          exit 1
       fi
       echo mkdir "$tempdir"
       mkdir "$tempdir"
    fi
    
    dest="$tempdir"
    subdest="/"
    if [ "$drive" != "i" ]; then subdest="$subdest$drive"; fi
    if [ -n "$group" -o -n "$drive" ]; then subdest="$subdest$group"; debug "use subdest=$subdest"; fi
    if [ "$subdest" = "/" ]; then subdest=""; fi
    dest="$dest$subdest"
    
    x=0
    for i in "$@"; do
        
        debug "i=$i"
        
        if [ "$i" = "--drive" ]; then
            debug "--drive detected"
            shift $x 
            continue 2
        elif [ "$i" = "--group" ]; then
            debug "--group detected"
            shift $x
            continue 2
        fi
        x=$(($x+1))
        
        echo mkdir -p "$dest"
        mkdir -p "$dest"
        
        right="${i#*=}"
        if [ "$i" = "$right" ] ; then
            echo cp -r "$i" $dest
            cp -r "$i" $dest
        else
            left="${i%%=*}"
            debug "left=$left"
            # if source is a folder
            if [ -d "$right" ]; then
               echo cp -r "$right" "$dest"
               cp -r "$right" "$dest"
               rightdir="${right##*/}"
               debug "rightdir=$rightdir"
               leftdir="${left%%/*}"
               debug "leftdir=$leftdir"
               debug "$leftdir == $left"
               if [ "$leftdir" != "$left" ] ; then
                  echo mkdir -p "$dest/${left%%/*}"
                  mkdir -p "$dest/${left%%/*}"
               fi
               if [ "$rightdir" != "$left" ] ; then
                  echo mv "$dest/$rightdir" "$dest/$left"
                  mv "$dest/$rightdir" "$dest/$left"
               fi
            # else source is a file
            else
               leftdir="${left%/*}"
               debug "leftdir=$leftdir"
               if [ "$leftdir" != "$left" ] ; then
                  echo mkdir -p "$dest/$leftdir"
                  mkdir -p "$dest/$leftdir"
               fi
               echo cp "$right" "$dest/$left"
               cp "$right" "$dest/$left"
            fi
        fi
    done

    break
done

find "$tempdir" -name .svn -print0 | xargs -0 rm -rf
-rm "$zipfile" 2>/dev/null
(cd "$tempdir"; zip -r ../"$tempdir".zip *)
mv "$tempdir".zip "$zipfile"
rm -rf $tempdir

