#!/bin/bash

argv0=$0

function usage {
   echo "Usage: $argv0 [--ignore_bdls <bdlname,...>] [--ignore_usrs <usr1,...>] [--o <output file>] dlfile"
   echo "Read all bundles from a specified download package and create"
   echo "a removal package for all the bundles (development signed)."
   echo "Parameters:"
   echo "--ignore_bdls: comma separated list of bundle names, which should be"
   echo "               ignored for the removal package."
   echo "--ignore_usrs: comma separated list of users, which bundles should be ignored"
   echo "               for the remove package. If wildcard is used it needs to be"
   echo "               escaped or quoted; e. g. \"usr*\")."
   echo "--o: output file name, default is dl.remove.tar. Always a tar file is created,"
   echo "     disregarding from specified file extension"
   exit 1
}

ignore_bdls=""
ignore_usrs=""
dest="dl.remove.tar"
scriptdir=${0%/*}

while [ $# -gt 1 -a "${1:0:2}" = "--" ] ; do
    case "$1" in
        --ignore_bdls)
            ignore_bdls="$2"
            shift 2
            ;;
        --ignore_usrs)
            ignore_usrs="$2"
            shift 2
            ;;
        --o)
            dest="$2"
            shift 2
            ;;
        *)
            usage
            ;;
    esac
done

destfile=$(basename $dest)
dlname="${destfile%.*}"

if [ $# = 0 ]; then usage; fi

cnt=0

rm $dest &>/dev/null

cmdline="--dlname $dlname"

for file in $(tar -atf "$1")
do 
  if [[ "$file" = *.tgz ]]; then 
    ctrlfile=$(tar -O -axf "$1" $file | tar --no-anchored -O -zxf - CONTROL/control)
    bdl_name=$(echo "$ctrlfile" | grep -e Name -e Package | sed -e "s/Name://g" -e "s/^[ \t]*//")
    bdl_usr=$(echo "$ctrlfile" | grep -e User -e Package | sed -e "s/User://g" -e "s/^[ \t]*//")
    if [ -z $bdl_usr ]; then bdl_usr=usr1; fi # if no user is set, default is usr1
    
    echo "$ignore_bdls" | awk -F, '{for (i=1;i<=NF;i++)print $i}' | while read line
    do
      if [[ $bdl_name = ${line} ]]; then exit 1; fi
    done
    if [ "$?" = "1" ]; then 
      echo Skipped bundle \"$bdl_name\" according \"--ignore_bdls\".
      continue
    fi
    
    echo "$ignore_usrs" | awk -F, '{for (i=1;i<=NF;i++)print $i}' | while read line
    do
      if [[ $bdl_usr = ${line} ]]; then exit 1; fi
    done
    if [ "$?" = "1" ]; then
      echo Skipped bundle \"$bdl_name\" \(user: \"$bdl_usr\"\) according \"--ignore_usrs\".
      continue
    fi
    
    unset cert
    # use correct signing parameters depending on user
    case $bdl_usr in
      usr*) cert="${scriptdir}/appsignerA1.crt";;
      sys*) cert="${scriptdir}/sysappsign1.crt";;
      os)   cert="${scriptdir}/ossigner1.crt";;
    esac
        
    if [ -z $cert ]; then 
      echo "Found invalid username \"$bdl_usr\" in bundle \"$bdl_name\", exit."
      exit 1
    fi
    
    echo Found bundle \"$bdl_name\" \(user: \"$bdl_usr\"\).
    
    if [ $cnt -gt 0 ]; then cmdline="$cmdline --newbndl"; fi
    cmdline="$cmdline --user $bdl_usr --bndlname ${bdl_name}-remove --addcert $cert --remove_bdls $bdl_name --removeonly"
    cnt=$((cnt+1))
  fi
done

echo ${scriptdir}/makepackage $cmdline
${scriptdir}/makepackage $cmdline
# move package to destination folder
if [ -f $dlname.tar ]; then
  mv $dlname.tar $dest
  echo Removal package \"$dest\" successfully created.
else
  echo Failed to create removal package \"$dest\".
  exit 1
fi
