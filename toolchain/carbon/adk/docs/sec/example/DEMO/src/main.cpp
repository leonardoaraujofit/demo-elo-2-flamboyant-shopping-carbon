#include <stdio.h>
#include <string.h>
#include <svc.h>

#ifdef _VRXEVO
//#include <eoslog.h>
#include <svc_sec.h>
#else
#include <unistd.h>
#include <vficom.h>
#include <svcsec.h>
#endif

#include <log/liblog.h>

#include "scdemo_version.h"
#include <sec/libseccmd.h>


#ifdef _VRXEVO
#define sleep(a)    SVC_WAIT(a*1000)
#define usleep(a)   SVC_WAIT(a/1000)
#endif

#define BUFF_OUT_SIZE 1024
// host ids
#define HID01       1
#define HID02       2
#define HID03       3
#define HID04       4
#define HID05       5

// KeySetIDs
#define KEYSET01    1
#define KEYSET02    2
#define KEYSET03    3
#define KEYSET04    4
#define KEYSET05    5
#define KEYSET06    6
#define KEYSET07    7
#define KEYSET08    8

// Encryption/Decryption Data Flag
#define ENC        1
#define DEC        2

// Generate/Verify MAC Flag
#define GEN        1
#define VRF        2


unsigned char data_buf[BUFF_OUT_SIZE] = {0};
unsigned char res_buf[BUFF_OUT_SIZE]  = {0};

using namespace std;
using namespace com_verifone_seccmd;

/***************************************************************************
 * LOGAPI init
 ***************************************************************************/
void LOGAPI_init(void)
{
  LOGAPI_SET_VERBOSITY(LOGAPI_VERB_FILE_LINE); // prefix File:Line
  LOGAPI_INIT("DEMOAPP");
  LOGAPI_SETLEVEL(LOGAPI_OFF);
  
#ifdef LOGAPI_ENABLE_DEBUG
  LOGAPI_SETLEVEL(LOGAPI_TRACE);
  LOGAPI_SETOUTPUT(LOGAPI_CONSOLE);
#else
  LOGAPI_SETLEVEL(LOGAPI_ERROR);
  LOGAPI_SETOUTPUT(LOGAPI_SYSLOG);

  char* pLogLevel = getenv ("SEC_LOGMASK");
  if (pLogLevel == NULL)
    return;

  int i = atoi(pLogLevel);
  if (i < 1 || i > 8)
    return;
    
  switch (i)
  {
    case (1):
      LOGAPI_SETLEVEL(LOGAPI_EMERG);
      break;
    case (2):
      LOGAPI_SETLEVEL(LOGAPI_ALERT);
      break;
    case (3):
      LOGAPI_SETLEVEL(LOGAPI_CRIT);
      break;
    case (4):
      LOGAPI_SETLEVEL(LOGAPI_ERROR);
      break;
    case (5):
      LOGAPI_SETLEVEL(LOGAPI_WARN);
      break;
    case (6):
      LOGAPI_SETLEVEL(LOGAPI_NOTICE);
      break;
    case (7):
      LOGAPI_SETLEVEL(LOGAPI_INFO);
      break;
    case (8):
      LOGAPI_SETLEVEL(LOGAPI_TRACE);
      break;
  }     
#endif

  return;
}


bool h2bCH(char in, unsigned char *out)
{
  if( in>=0x30 && in<=0x39 )
    *out = (unsigned char)(in-0x30);
  else
    if( in>='A' && in<='F' )
       *out = (unsigned char)(in - 'A' + 10);
    else
       if( in>='a' && in<='f' )
          *out = (unsigned char)(in - 'a' + 10);
       else
          return false;
  return true;
}

int convertDsp2Hex(const char *dsp, size_t dsp_len, char *hex, const size_t max_hex_len)
{
  if (!dsp || !hex || dsp == hex || dsp_len == 0) return -1;
  if (dsp_len % 2) --dsp_len; // skip one byte
  dsp_len >>= 1;

  int i;
  unsigned char t1=0, t2=0;
  int end = (dsp_len>max_hex_len)?(max_hex_len):(dsp_len);
  for(i = 0; i < end; i++)
  {
    if ( !h2bCH( *dsp, &t1 ) )
         return i;
    if ( !h2bCH( *(dsp+1), &t2 ) )
         return i;
    hex[i] = (char)((t1<<4)|t2);
    dsp += 2;
  }
  return end;
}

static int testGetVersions()
{
  int ret;
  uint8_t errCode=0;
  std::string versions;
  const char *APIVersion;

  ret = Sec_GetVersions(&versions, &errCode, 5000);
  LOGF_TRACE("\tSec_GetVersions() returns %d, err code=%X", ret, errCode);
  if(ret == 0)
  {
    LOGF_TRACE("\tVersion Info: \"%s\"", versions.c_str());
  }

  APIVersion = Sec_GetVersion();
  LOGF_TRACE("\tAPI Lib Version: \"%s\"", APIVersion);

  return ((int)errCode+ret);
}

static int testUpdateKey(u_char HostId, uint8_t KeySetId, u_char KeyType, std::string keyDataStr)
{
  int ret;
  uint8_t errCode=0;

  LOGF_TRACE("\tInput Parameters: HostId = %d, KeySetID = %d, KeyType = %d", HostId, KeySetId, KeyType);
  Sec_SetKSId(HostId, KeySetId);

  errCode=0;
  convertDsp2Hex(keyDataStr.c_str(), keyDataStr.size(), reinterpret_cast<char *>(data_buf), BUFF_OUT_SIZE);
  ret = Sec_UpdateKey(HostId, KeyType, (char *)data_buf, keyDataStr.size()/2, &errCode, 5000);
  LOGF_TRACE("\tSec_UpdateKey() returns %x, err code=%d", ret, errCode);

  return ((int)errCode+ret);
}

int Enc_dec_call(u_char HostId, uint8_t KeySetId, std::string plainTestData, std::string encTestData, int flag, std::string IVData = "")
{
  unsigned char p_res[BUFF_OUT_SIZE] = {0};
  unsigned char p_IV[64] = {0};
  EncDecDataIV Data;
  uint8_t errCode=0;
  int ret;
  int dataOk = 0;

  Sec_SetKSId(HostId, KeySetId);
  convertDsp2Hex(plainTestData.c_str(), plainTestData.size(), reinterpret_cast<char *>(data_buf), BUFF_OUT_SIZE);
  convertDsp2Hex(encTestData.c_str(), encTestData.size(), reinterpret_cast<char *>(p_res), BUFF_OUT_SIZE);
  Data.pInData = data_buf;
  Data.uiInLen = encTestData.size()/2;
  Data.pOutData= data_buf;
  Data.uiOutLen = BUFF_OUT_SIZE;
  if( IVData.size() != 0 )
  {
    convertDsp2Hex(IVData.c_str(), IVData.size(), reinterpret_cast<char *>(p_IV), BUFF_OUT_SIZE);
    Data.IV       = p_IV;
    Data.uiIVLen  = IVData.size()/2;
  }
  else
  {
    Data.IV       = NULL;
    Data.uiIVLen  = 0;
  }
  errCode=0;
  if ( flag == 1 )
  { // encryption call
    ret = Sec_EncryptData(HostId, &Data, &errCode, 5000);
    LOGF_TRACE("\tSec_EncryptData() returns %x, err code %d, inp=res len -> %ld=%ld", ret, errCode, Data.uiInLen, Data.uiOutLen );
  }
  else
  { // decryption call
    ret = Sec_DecryptData(HostId, &Data, &errCode, 5000);
    LOGF_TRACE("\tSec_DecryptData() returns %x, err code %d, inp=res len -> %ld=%ld", ret, errCode, Data.uiInLen, Data.uiOutLen );
  }
  dataOk = (memcmp(Data.pOutData, p_res, encTestData.size()/2) == 0);
  return dataOk;
}

static int MAC_gen_ver_call(u_char HostId, uint8_t KeySetId, std::string macTestData, std::string macResData, int flag)
{
  MACData Data;
  genMAC  MAC;
  unsigned char p_res[24] = {0};
  uint8_t errCode=0;
  int ret;
  int dataOk = 0;

  Sec_SetKSId(HostId, KeySetId);
  convertDsp2Hex(macTestData.c_str(), macTestData.size(), reinterpret_cast<char *>(data_buf), BUFF_OUT_SIZE);
  convertDsp2Hex(macResData.c_str(), macResData.size(), reinterpret_cast<char *>(p_res), BUFF_OUT_SIZE);

  Data.pData = data_buf;
  Data.uiLen = macTestData.size()/2;
  errCode=0;
  MAC.ucMACLen = 4;
  MAC.pMAC = (uint8_t *)malloc(MAC.ucMACLen);
  if( MAC.pMAC == NULL )
  {
    return dataOk;
  }

  if ( flag == 1 )
  { // gen
    memset(MAC.pMAC, 0, MAC.ucMACLen);
    ret = Sec_GenerateMAC(HostId, 0, 1, &Data, &MAC, &errCode, 5000);
    LOGF_TRACE("\tSec_GenerateMAC() returns %x, err code %d", ret, errCode );
    dataOk = (MAC.ucMACLen == 4 && memcmp(MAC.pMAC, p_res, 4) == 0);
  }
  else
  {
    memcpy(MAC.pMAC, p_res, MAC.ucMACLen);
    ret = Sec_VerifyMAC(HostId, 0, 1, &Data, MAC, &errCode, 5000);
    LOGF_TRACE("\tSec_VerifyMAC() returns %x, err code %d", ret, errCode);
    dataOk = 1;
  }

  free(MAC.pMAC);
  return dataOk;
}

static int testSec_EnterAndEncryptPIN(u_char HostId, uint8_t KeySetId, u_char PINEntryType, uint16_t Flags, const char *PIN, const char *ExpectedPINBlock)
{
  int ret;
  int tmsk_dataOK = 0;
  uint8_t errCode=0;
  PINParams_v2    pinPara;
  PINBlockParams  pinBlockPara;
  PropData        propData;
  EncPINBlock     pinBlock;
  std::string     PINMessageText;
  std::string     AmountText;
  std::string     AddScreenText;
  std::string     AddScreenText2;

  LOGF_TRACE("\tInput Parameters: KeySetID = %d, HostId = %d", HostId, KeySetId);
  Sec_SetKSId(HostId, KeySetId);

  memset(&pinPara,      0x00, sizeof(PINParams_v2));
  memset(&pinBlockPara, 0x00, sizeof(pinBlockPara));
  memset(&propData, 0x00, sizeof(propData));
  memset(&pinBlock, 0x00, sizeof(pinBlock));

  // PAN
  pinBlockPara.pPAN = (unsigned char *)"4549493220438410";
  // STAN
  pinBlockPara.pSTAN = (unsigned char *)"1";
  // HTML file name
  std::string PinTextHtmlPath = "EnterPIN.html";
  pinPara.pPinTextHtmlPath = (unsigned char *)PinTextHtmlPath.c_str();
  // Amount
  std::string amt = "000000012300";
  convertDsp2Hex(amt.c_str(), amt.size(), reinterpret_cast<char *>(pinPara.TransAmount), TRANS_AMOUNT_SIZE);
  // PIN Entry Type
  pinPara.PinEntryType = PINEntryType;
  // PIN Try Flag
  pinPara.PinTryFlag = 0;

  PINMessageText.assign("Enter PIN");
  AmountText.assign("USD");
  char pin_text[20];
  sprintf(pin_text, "(PIN: %s)", PIN);
  AddScreenText.assign(pin_text);
  AddScreenText2.assign("Add Text 2");

  // Minimum PIN length
  pinPara.MinPINLength = 4;
  // Maximum PIN length
  pinPara.MaxPINLength = 12;
  // PIN cancel
  pinPara.PinCancel = 0;

  // test dependent values are modified here
   switch (pinPara.PinEntryType)
  {
     case 0:
       break;
     case 1:
       PINMessageText = "Enter PIN or easy bypass it with OK";
       break;
     case 2:
       PINMessageText = "Enter PIN or OK";
       break;
     case 3:
       PINMessageText = "Null PIN";
       AddScreenText  = "Add Text 1";
       break;
     default:
       break;
  }

  // PIN Message Text
  pinPara.PINMessageText = (u_char *)PINMessageText.c_str();
  // Amount Text
  pinPara.AmountText     = (u_char *)AmountText.c_str();
  // Additional Screen Text
  pinPara.AddScreenText  = (u_char *)AddScreenText.c_str();
  // Additional Screen Text 2
  pinPara.AddScreenText2 = (u_char *)AddScreenText2.c_str();
  // PIN Block Format
  pinBlockPara.PinBlockFormat = 0;
  // Propriety Data
  std::string propTestData = "00006666777766667777";
  convertDsp2Hex(propTestData.c_str(), propTestData.size(), reinterpret_cast<char *>(data_buf), BUFF_OUT_SIZE);
  propData.pPropData = data_buf;
  propData.uiPropDataLen = propTestData.size()/2;

  errCode=0;
  pinBlock.ucPINBlockLen = 8;
  pinBlock.pPINBlock = (uint8_t *)malloc(pinBlock.ucPINBlockLen);
  if( pinBlock.pPINBlock == NULL )
  {
    return 1;
  }

  memset(pinBlock.pPINBlock, 0, pinBlock.ucPINBlockLen);


  ret = Sec_EnterAndEncryptPIN(HostId, &pinPara, &pinBlockPara, 1, propData, &pinBlock, NULL, &errCode, 60000);
  LOGF_TRACE("\tSec_EnterAndEncryptPIN() returns %x, err code=%d ", ret, errCode);

  // result evaluation
  if(PINEntryType < 3)
  {
    if (errCode == 0 || (PINEntryType == 2 && errCode == 21))
    {
      // PIN Block check
      if(pinBlock.ucPINBlockLen == 8 && memcmp(pinBlock.pPINBlock, ExpectedPINBlock, 8) == 0)
        tmsk_dataOK = 0;
      else
        tmsk_dataOK = 1;
    }
    else // PIN bypassed i.e.
      tmsk_dataOK = 0;
  }
  else
  {
      tmsk_dataOK = 0; // configure for test pass
  }
  //sprintf( tmp, "%2d  %20.20s  %6s", command, "EnterAndEncryptPIN", ((errCode+ret)==0&&tmsk_dataOK ? "passed" : "fail") );
  free(pinBlock.pPINBlock);
  return ((int)errCode+ret+tmsk_dataOK);
}


/*************************************************************************************/
static int dukpt_LoadDUKPTKey(u_char g_HostId, uint8_t KeySetId, u_char KeyType, std::string InitKSN, std::string InitKey)
/*************************************************************************************/
{
  int      ret=0;
  uint8_t  errCode=0;
  Ksn      IKSN;

  Sec_SetKSId(g_HostId, KeySetId);
  convertDsp2Hex(InitKSN.c_str(), InitKSN.size(), reinterpret_cast<char *>(IKSN.KSN), KSN_SIZE);
  convertDsp2Hex(InitKey.c_str(), InitKey.size(), reinterpret_cast<char *>(data_buf), BUFF_OUT_SIZE);
  ret = Sec_UpdateKey(g_HostId, KEY_TYPE_DUKPT, 0, (char *)data_buf, InitKey.size()/2, &IKSN, &errCode, 5000);
  LOGF_TRACE("\tSec_UpdateKey() returns %x, err code=%d", ret, errCode);
  return ((int)errCode+ret);
}

/*********************************************************************************************************/
static int dukpt_SetDUKPTmask(u_char g_HostId, uint8_t KeySetId, std::string DUKPTmaskPIN, std::string DUKPTmaskMAC, std::string DUKPTmaskENC)
/*********************************************************************************************************/
{
  int         ret=0;
  uint8_t     errCode=0;
  DUKPTmask   DUKPTMask;
  u_char *dukptbuf = NULL; //[3*DOUBLE_KEY_SIZE];

  Sec_SetKSId(g_HostId, KeySetId);
  dukptbuf = (u_char*)malloc(DUKPTmaskPIN.size() + DUKPTmaskMAC.size() + DUKPTmaskENC.size());
  if( dukptbuf==NULL )
    return -5;

  DUKPTMask.maskPIN = dukptbuf;
  DUKPTMask.maskMAC = dukptbuf + DUKPTmaskPIN.size()/2;
  DUKPTMask.maskENC = dukptbuf + DUKPTmaskPIN.size()/2 + DUKPTmaskMAC.size()/2;

  convertDsp2Hex(DUKPTmaskPIN.c_str(), DUKPTmaskPIN.size(), reinterpret_cast<char *>(DUKPTMask.maskPIN), DUKPTmaskPIN.size()/2);
  convertDsp2Hex(DUKPTmaskMAC.c_str(), DUKPTmaskMAC.size(), reinterpret_cast<char *>(DUKPTMask.maskMAC), DUKPTmaskMAC.size()/2);
  convertDsp2Hex(DUKPTmaskENC.c_str(), DUKPTmaskENC.size(), reinterpret_cast<char *>(DUKPTMask.maskENC), DUKPTmaskENC.size()/2);
  DUKPTMask.sizeP = DUKPTmaskPIN.size()/2;
  DUKPTMask.sizeM = DUKPTmaskMAC.size()/2;
  DUKPTMask.sizeE = DUKPTmaskENC.size()/2;
  ret = Sec_SetDUKPTMask(g_HostId, &DUKPTMask, &errCode, 5000);
  LOGF_TRACE("\tSetDUKPTmask() returns %x, err code=%d", ret, errCode);
  free(dukptbuf);
  return ((int)errCode+ret);
}

/**************************************************************************/
static int dukpt_IncrementKSN(u_char g_HostId, uint8_t KeySetId)
/**************************************************************************/
{
  int      ret=0;
  uint8_t  errCode=0;
  Ksn      incKSN;

  Sec_SetKSId(g_HostId, KeySetId);
  ret = Sec_IncrementKSN(g_HostId, &incKSN, &errCode, 5000);
  LOGF_TRACE("\tIncrementKSN() returns %x, err code=%d", ret, errCode);
  LOGF_TRACE("\t\t\t>>>>>>>KSN=%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            incKSN.KSN[0], incKSN.KSN[1], incKSN.KSN[2], incKSN.KSN[3], incKSN.KSN[4],
            incKSN.KSN[5], incKSN.KSN[6], incKSN.KSN[7], incKSN.KSN[8], incKSN.KSN[9]);

   return ((int)errCode+ret);
}

/*************************************************************************************************************************/
static int dukpt_EncryptDecryptData(u_char g_HostId, uint8_t KeySetId, std::string TestData, std::string encTestData, int EncDecFlag, std::string ExpectedKSN)
/*************************************************************************************************************************/
{
  int            ret=0;
  uint8_t        errCode=0;
  EncDecDataIV   Data;
  Ksn            incKSN;
  unsigned char  p_res_ksn[24] = {0};
  int            dataOK = 1;

  Sec_SetKSId(g_HostId, KeySetId);
  convertDsp2Hex(TestData.c_str(), TestData.size(), reinterpret_cast<char *>(data_buf), BUFF_OUT_SIZE);
  convertDsp2Hex(encTestData.c_str(), encTestData.size(), reinterpret_cast<char *>(res_buf), BUFF_OUT_SIZE);
  convertDsp2Hex(ExpectedKSN.c_str(), ExpectedKSN.size(), reinterpret_cast<char *>(p_res_ksn), 24);

  Data.pInData   = data_buf;
  Data.uiInLen   = TestData.size()/2;
  Data.pOutData  = data_buf;
  Data.uiOutLen  = BUFF_OUT_SIZE;
  Data.IV = NULL;
  Data.uiIVLen  = 0;
  memset(incKSN.KSN, 0x00, KSN_SIZE);

  if(EncDecFlag == ENC)
  {
    ret = Sec_EncryptData(g_HostId, &Data, &incKSN, &errCode, 5000);
    LOGF_TRACE("\tSec_EncryptData returns %x, err code %d, inp=res len -> %ld=%ld", ret, errCode, Data.uiInLen, Data.uiOutLen);
  }
  else
  {
    ret = Sec_DecryptData(g_HostId, &Data, &incKSN, &errCode, 5000);
    LOGF_TRACE("\tSec_DecryptData returns %x, err code %d, inp=res len -> %ld=%ld", ret, errCode, Data.uiInLen, Data.uiOutLen);
  }

//  LOG_HEX_PRINTF("\tOutData", Data.pOutData, Data.uiOutLen);
//  LOG_HEX_PRINTF("\tKSN", incKSN.KSN, KSN_SIZE);
  if((memcmp(Data.pOutData, res_buf, encTestData.size()/2) == 0) && (memcmp(incKSN.KSN, p_res_ksn, KSN_SIZE) == 0))
    dataOK = 0;
  else
    dataOK = 1;
  return ((int)errCode+ret+dataOK);
}

/**************************************************************************************************/
static int dukpt_GenerateMAC(u_char g_HostId, uint8_t KeySetId, std::string TestData, std::string ExpectedMAC, std::string ExpectedKSN)
/**************************************************************************************************/
{
  int            ret=0;
  uint8_t        errCode=0;
  MACData        Data;
  u_char         MACMode = 1;
  unsigned char  p_res[24] = {0};
  unsigned char  p_res_ksn[24] = {0};
  genMAC         MAC;
  Ksn            incKSN;
  int            dataOK = 0;

  Sec_SetKSId(g_HostId, KeySetId);
  convertDsp2Hex(TestData.c_str(), TestData.size(), reinterpret_cast<char *>(data_buf), BUFF_OUT_SIZE);
  convertDsp2Hex(ExpectedMAC.c_str(), ExpectedMAC.size(), reinterpret_cast<char *>(p_res), 24);
  convertDsp2Hex(ExpectedKSN.c_str(), ExpectedKSN.size(), reinterpret_cast<char *>(p_res_ksn), 24);

  Data.pData = data_buf;
  Data.uiLen = TestData.size()/2;
  MAC.ucMACLen = 4;
  MAC.pMAC = (uint8_t *)malloc(MAC.ucMACLen);
  if( MAC.pMAC == NULL )
  {
    return 1;
  }
  memset(MAC.pMAC, 0, MAC.ucMACLen);
  memset(incKSN.KSN, 0x00, KSN_SIZE);

  ret = Sec_GenerateMAC(g_HostId, 0, MACMode, &Data, &MAC, &incKSN, &errCode, 5000);
  LOGF_TRACE("\tSec_GenerateMAC returns %x, err code %d", ret, errCode);
//  LOG_HEX_PRINTF("\tMAC", MAC.pMAC, 4);
//  LOG_HEX_PRINTF("\tKSN", incKSN.KSN, KSN_SIZE);

  if (!((MAC.ucMACLen == 4) && (memcmp(MAC.pMAC, p_res, 4) == 0) && (memcmp(incKSN.KSN, p_res_ksn, KSN_SIZE) == 0)))
    dataOK = 1;

  free(MAC.pMAC);
  return ((int)errCode+ret+dataOK);
}

/**************************************************************************/
static int dukpt_VerifyMAC(u_char g_HostId, uint8_t KeySetId, std::string TestData, std::string vrfMAC)
/**************************************************************************/
{
  int            ret=0;
  uint8_t        errCode=0;
  MACData        Data;
  u_char         MACMode = 1;
  unsigned char  p_res[24] = {0};
  genMAC         MAC;
  Ksn            incKSN;

  Sec_SetKSId(g_HostId, KeySetId);
  convertDsp2Hex(TestData.c_str(), TestData.size(), reinterpret_cast<char *>(data_buf), BUFF_OUT_SIZE);
  convertDsp2Hex(vrfMAC.c_str(), vrfMAC.size(), reinterpret_cast<char *>(p_res), 24);

  Data.pData = data_buf;
  Data.uiLen = TestData.size()/2;
  MAC.ucMACLen = 4;
  MAC.pMAC = (uint8_t *)malloc(MAC.ucMACLen);
  if( MAC.pMAC == NULL )
  {
    return 1;
  }
  memset(MAC.pMAC, 0, MAC.ucMACLen);
  memset(incKSN.KSN, 0x00, KSN_SIZE);

  memcpy(MAC.pMAC, p_res, MAC.ucMACLen);
  ret = Sec_VerifyMAC(g_HostId, 0, MACMode, &Data, MAC, &incKSN, &errCode, 5000);
  LOGF_TRACE("\tSec_VerifyMAC returns %x, err code %d", ret, errCode);
  free(MAC.pMAC);

  // sleep(1); // wait until last command was finished
  return ((int)errCode+ret);
}




////////////////////////////////////////////////////////////////////////////////

int main (int argc, char *argv[])
{
  // init logging
  LOGAPI_init();
  LOGF_INFO("*****%s (%s.%s.%s) has STARTED*****", "DemoApp", KEY_MAJOR_VERSION, KEY_MINOR_VERSION, KEY_BUGFIX_VERSION);

#ifdef _VRXEVO
  // launch necessary services
  static const char * appToStart[] = {
      "F:1/SYSOUT.OUT",
      "F:GUIPRTSERVER.OUT",
      "F:SCAPP.OUT",
  };
  for (int i = 0; i < sizeof(appToStart)/sizeof(appToStart[0]); ++i)
  {
      LOGF_TRACE("Starting app %s", appToStart[i]);
      int res = run(appToStart[i], NULL);
      LOGF_TRACE("Startup result %d", res);
      if (res < 0) LOGF_ERROR(" --> means FAILURE! errno %d", errno);
      else SVC_WAIT(100); // give some time to the task to initialise!
  }
#endif

  for (int l=6; l>0; l--)
  {
    LOGF_TRACE("Test starts in %d sec", l);
    sleep(1);
  }


  Sec_Init();
  testGetVersions();




  LOGF_TRACE("********** MSK test **********");
  /*
    Plain text keys:
    TMK     = 69C924585873862317FDA37882ECE732
      PINK    = 0A0AD4E1619EF28603E1C373E67339F6
      MACGenK = 871D7A0F037494C24199B0938C781452
      DataEnK = 4F4F0B48163DB6BC201D57ECA5957B52
      MACVerK = 871D7A04DB6BC201D579B0938C781452
      DataDeK = 4FD7A0F0374DB6BC201D57ECA5957B52
      KEK     = A9D511B8D827E09F3D87113CD68CD90D

    KEY_TYPE_TPK_FOR_PIN       = 1,  /PIN                   encPINK (1)     0DA7AFF46A0B5B9C53E166A0B8B62618
    KEY_TYPE_TAK_FOR_GEN_MAC   = 2,  /MAC Generation        encMACGenK (2)  3F399522C692A6EF10F06A82037BF693
    KEY_TYPE_TPK_FOR_ENC_DATA  = 3,  /Data Encryption       encDataEnK (3)  1D95143DDDAB03901666934B43ACBD3F
    KEY_TYPE_TAK_FOR_VER_MAC   = 19, /MAC Verification      encMACVerK (19) 84903A31F46A69A0242C7FA816AE28AC
    KEY_TYPE_TPK_FOR_DEC_DATA  = 20, /Data Decryption       encDataDeK (20) D66A042A7CB37A9F1666934B43ACBD3F
    KEY_TYPE_TPK_FOR_KEK       = 21, /KEK                   encKEK     (21) 6C6DA0F69181B244C0C315F8D2AF787E
  */
  testUpdateKey(HID01, KEYSET01, KEY_TYPE_TPK_FOR_PIN,      "0DA7AFF46A0B5B9C53E166A0B8B62618"); // HostID, KeySetID, KeyType, KeyData
  testUpdateKey(HID01, KEYSET01, KEY_TYPE_TAK_FOR_GEN_MAC,  "3F399522C692A6EF10F06A82037BF693"); // HostID, KeySetID, KeyType, KeyData
  testUpdateKey(HID01, KEYSET01, KEY_TYPE_TAK_FOR_VER_MAC,  "84903A31F46A69A0242C7FA816AE28AC"); // HostID, KeySetID, KeyType, KeyData
  testUpdateKey(HID01, KEYSET01, KEY_TYPE_TPK_FOR_ENC_DATA, "1D95143DDDAB03901666934B43ACBD3F"); // HostID, KeySetID, KeyType, KeyData
  testUpdateKey(HID01, KEYSET01, KEY_TYPE_TPK_FOR_DEC_DATA, "1D95143DDDAB03901666934B43ACBD3F"); // HostID, KeySetID, KeyType, KeyData
  testUpdateKey(HID01, KEYSET01, KEY_TYPE_TPK_FOR_KEK,      "6C6DA0F69181B244C0C315F8D2AF787E"); // HostID, KeySetID, KeyType, KeyData

  // ***** DATA Enc/dec *************************************************************************
  std::string plainTestData = "00001111000011110000111100001111222233332222333322223333222233334444555544445555444455554444555566667777666677776666777766667777";
  std::string encTestData   = "A710AF4E3F500CF2B862C8419A49998470E70CA3C5EB4EEF89F832CCB1420555E90DF336EC3B7D4A0FED72435AF46D8336993B3015163372EF3D4B801D88CE22";
  std::string IVData        = "F9D3BFC0B69C7189";
  Enc_dec_call(HID01, KEYSET01, plainTestData, encTestData, ENC);
  encTestData   = "4E5352D706E5AE904E5343C606E5BF81A069D350CF43A70C824BF172ED61852E78BAA7F317DD97651EDCC19571BBF103EE9E7DE55F07C011CCBC5FC77D25E233";
  Enc_dec_call(HID01, KEYSET01, plainTestData, encTestData, DEC);
  encTestData   = "FC1D51409AEA4C2BBD58E9A689AF2725FA93FFFEE2C4CCFF0C70F61CB93FC0F13B7FC4B2C6E9C48C00FB7750191EA51475403E9B461DE3A3318E669D93E2320E";
  Enc_dec_call(HID01, KEYSET01, plainTestData, encTestData, ENC, IVData);
  encTestData   = "B780ED17B079DF194E5343C606E5BF81A069D350CF43A70C824BF172ED61852E78BAA7F317DD97651EDCC19571BBF103EE9E7DE55F07C011CCBC5FC77D25E233";
  Enc_dec_call(HID01, KEYSET01, plainTestData, encTestData, DEC, IVData);
  // ***** gen/verify MAC *************************************************************************
  std::string macResData  = "59F44F2413668989";
  MAC_gen_ver_call(HID01, KEYSET01, plainTestData,macResData,GEN);
  macResData  = "A65BA07017A58E71";
  MAC_gen_ver_call(HID01, KEYSET01, plainTestData,macResData,VRF);
  // ***** PIN entry ******************************************************************************
  testSec_EnterAndEncryptPIN(HID01, KEYSET01, 0, 0, "1234C", "\x49\xF7\xC0\x0F\x3B\x53\x30\x26");  // HostID, KeySetID, PINEntryType, Flags, PIN, ExpectedPINBlock




  LOGF_TRACE("********** DUKPT test **********");

  // ***** DATA Enc/dec *************************************************************************
  dukpt_LoadDUKPTKey(HID03, KEYSET01, 0, "FFFF0013010000200000", "066E0D5E928D51C7C7B937C34C6153BA"); // HostID, KeySetID, KeyType (0=DUKPT, 1=EXXON), IKSN, Key
  dukpt_IncrementKSN(HID03, KEYSET01); //Increment KSN to ...0001
  dukpt_IncrementKSN(HID03, KEYSET01); //Increment KSN to ...0002
  dukpt_IncrementKSN(HID03, KEYSET01); //Increment KSN to ...0003
  dukpt_EncryptDecryptData(HID03, KEYSET01, "041255EDDCCBBEDC", "898193D055E2058E", ENC, "FFFF0013010000200003"); // HostID, KeySetID, Data,EncData,Encryption Flag,ExpectedKSN
  dukpt_IncrementKSN(HID03, KEYSET01); //Increment KSN to ...0004
  dukpt_EncryptDecryptData(HID03, KEYSET01,
                           "00001111000011110000111100001111222233332222333322223333222233334444555544445555444455554444555566667777666677776666777766667777",
                           "EF06EEB3FDF21408EF06EEB3FDF21408796845EE9E58969E796845EE9E58969EE1379144579992A9E1379144579992A9760509184E34CC53760509184E34CC53",
                           ENC, "FFFF0013010000200004"); // Data,EncData,Encryption Flag,ExpectedKSN

  // ***** generate MAC *************************************************************************
  dukpt_LoadDUKPTKey(HID03, KEYSET01, 0, "FFFF0013010000200000", "066E0D5E928D51C7C7B937C34C6153BA"); // HostID, KeySetID, KeyType (0=DUKPT, 1=EXXON), IKSN, Key
  dukpt_IncrementKSN(HID03, KEYSET01); //Increment KSN to ...0001
  dukpt_IncrementKSN(HID03, KEYSET01); //Increment KSN to ...0002
  dukpt_IncrementKSN(HID03, KEYSET01); //Increment KSN to ...0003
  dukpt_GenerateMAC(HID03, KEYSET01,
                    "00001111000011110000111100001111222233332222333322223333222233334444555544445555444455554444555566667777666677776666777766667777",
                    "96BFCCA37B65F0B9", "FFFF0013010000200003"); // data, expected CBC-MAC, expected KSN


  // ***** verify MAC *************************************************************************
  dukpt_LoadDUKPTKey(HID03, KEYSET01, 0, "FFFF0013010000200000", "066E0D5E928D51C7C7B937C34C6153BA"); // HostID, KeySetID, KeyType (0=DUKPT, 1=EXXON), IKSN, Key
  dukpt_IncrementKSN(HID03, KEYSET01); //Increment KSN to ...0001
  dukpt_IncrementKSN(HID03, KEYSET01); //Increment KSN to ...0002
  dukpt_IncrementKSN(HID03, KEYSET01); //Increment KSN to ...0003
  dukpt_VerifyMAC(HID03, KEYSET01,
                  "00001111000011110000111100001111222233332222333322223333222233334444555544445555444455554444555566667777666677776666777766667777",
                  "9010319AC491D491"); // data, expected CBC-MAC-Retail


  Sec_Destroy();
  LOGF_TRACE("test complete");



#if defined (_VRXEVO) && defined (_DEBUG)  // <<-- due to automatic test agent
  // open the USB port for loading
  int hUsb = -1;
  if ((hUsb = open(DEV_USBD, O_RDWR)) >= 0)
  {
    struct Opn_Blk oblk;
    oblk.rate = Rt_115200;
    oblk.format = Fmt_A8N1;
    oblk.protocol = P_char_mode;
    oblk.parameter = 0;
    set_opn_blk(hUsb, &oblk);
  }
  // process events as required
  while (1)
  {
      LOGF_TRACE("waiting for download");
      unsigned long events = wait_event();
      if (events & EVT_USB_CLIENT) // USB port
      {
          char ch;
          int ret;

          if (hUsb >= 0)
          {
              while ((ret = read(hUsb, &ch, 1)) > 0)
              {
                  if (ret == 1 && ch == 0x05)
                  {
                      LOGF_TRACE("performing download...");

                      SVC_WAIT(1000);

                      download(hUsb, "P");
                  }
              }
          }
      }
  }
#else
  LOGAPI_DEINIT();

  return 0;
#endif

  return 0;
}

////////////////////////////////////////////////////////////////////////////////

