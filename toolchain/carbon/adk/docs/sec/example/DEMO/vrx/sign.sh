
VARIANT=$1
TOOL=$2

echo "Variant" $VARIANT
echo "File to sign" $FILE

if [ -z $TOOL ]; then
	TOOL="FileSignature.exe"
fi

rm -rf $VARIANT/Certif.crt 2>/dev/null
cd $VARIANT
$TOOL demo.fst -nogui
cd ..

