'Get command-line arguments.
Set objArgs = WScript.Arguments
'The folder the contents should be zipped.
InputFolder = objArgs(0)
'The location of the zip file.
ZipFile = objArgs(1)

'Create empty ZIP file.
CreateObject("Scripting.FileSystemObject").CreateTextFile(ZipFile, True).Write "PK" & Chr(5) & Chr(6) & String(18, vbNullChar)

Set objShell = CreateObject("Shell.Application")
Set fso = CreateObject("Scripting.FileSystemObject")

in_folder = fso.GetAbsolutePathName(InputFolder)
zip_file = fso.GetAbsolutePathName(ZipFile)

Set source = objShell.NameSpace(in_folder).Items
objShell.NameSpace(zip_file).CopyHere(source)

do until objShell.NameSpace(in_folder).Items.Count = objShell.NameSpace(zip_file).Items.Count
  wscript.Sleep 100
loop

Set fso = Nothing
Set objShell = Nothing
