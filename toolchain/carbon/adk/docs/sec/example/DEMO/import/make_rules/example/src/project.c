#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
  // this is common code
  printf("This is an example!\n");
 
  // use _VRXEVO to seperate Verix specific code from common code
#ifdef _VRXEVO
  printf("This is Verix platform!\n");
#endif
  // use _VOS to seperate V/OS specific code from common code 
#ifdef _VOS
  printf("This is V/OS platform!\n");
#endif
  // use _VOS2 to seperate V/OS2 specific code from common code 
#ifdef _VOS2
  printf("This is V/OS2 platform!\n");
#endif
  // use _SVOS to seperate sV/OS specific code from common code 
#ifdef _SVOS
  printf("This is sV/OS platform!\n");
#endif
  // use _X5000 to seperate X5000 specific code from common code 
#ifdef _X5000
  printf("This is X5000 platform!\n");
#endif

  return EXIT_SUCCESS;
}
