#!/usr/bin/python3

from artifactory import ArtifactoryPath
import sys, os, atexit, tempfile, shutil, getpass

TARGET_VOS  = 1
TARGET_VOS2 = 2
TARGET_VRX  = 3

########################################################################
RO_USER='adk_automation_ro'
RO_PASSWD='Y39aZ5'
########################################################################

workdir = tempfile.mkdtemp()
RW_USER = ''
RW_PASSWD = ''

def printhelp():
    print(str("Usage: python {0} [<component name>] [<component version>]").format(arg_list[0]))
    print()
    print("This script tries to download the ADK ZIP packages specified by")
    print("    Parameter 1:  ADK component name")
    print("    Parameter 2:  ADK component version")
    print("from Verifone's Artifactory, calls the production sign script")
    print("and stores the signed packages on Artifactory.")

    exit(1)

def cleanup():
    shutil.rmtree(workdir)

arg_list = sys.argv
if len(sys.argv) != 3:
    printhelp()

def do_signing(target, filenames_full, filenames_load):
    if target == TARGET_VOS:
        printstr = "V/OS (Trident) SYS12/13-SIGNER CARD"
        full_subdir = "vos/load"
    elif target == TARGET_VOS2:
        printstr = "V/OS2 (Raptor) SYS12/13-SIGNER CARD"
        full_subdir = "vos2/load"
    elif target == TARGET_VRX:
        printstr = "VERIX EOS-SIGNER CARD"
        full_subdir = "vrx/load"
    else:
        print("Error: Unknown target type")
        exit(1)
    
    print("\n\n")
    print("****************************************************************")
    print("****************************************************************")
    print("***")
    print(str("*** PLEASE INSERT THE {0}").format(printstr))
    print("***")
    print("****************************************************************")
    print("****************************************************************")
    
    while True:
        PROCEED = input("DO YOU WANT TO PROCEED? [Y,N] ")
        if str.upper(PROCEED) == 'N':
            exit(1)
        elif str.upper(PROCEED) == 'Y':
            break

        print()
    
    if target == TARGET_VOS or target == TARGET_VOS2:
        for f in filenames_load:
            ret = os.system("./load_package_signer -u \"sys*\" -s \"-prod\" %s" % f)
            if ret != 0 or not os.path.isfile(f):
                print("Error: a signing issue occured")
                exit(1)
                
    if target == TARGET_VRX:
        for f in filenames_load:
            ret = os.system("./vrx_load_package_signer -f syssign.fst %s" % f)
            if ret != 0 or not os.path.isfile(f):
                print("Error: a signing issue occured")
                exit(1)
            
    for f in filenames_full:
        if target == TARGET_VOS or target == TARGET_VOS2:    
            ret = os.system(str("./load_package_signer -u \"sys*\" -s \"-prod\" -f {0} {1}").format(full_subdir, f))
        elif target == TARGET_VRX:
            ret = os.system(str("./vrx_load_package_signer -f syssign.fst -d {0} {1}").format(full_subdir, f))
        else:
            print("Error: Unknown target type")
            exit(1)
        
        if ret != 0 or not os.path.isfile(f):
            print("Error: a signing issue occured")
            exit(1)


def ask_for_password():
    global RW_USER
    global RW_PASSWD
    
    print("\n!!! In order to upload the signed packages to Artificatory a priviledged user account is required. !!!\n")
    RW_USER = input("Artifactory Account Name: ")
    RW_PASSWD = getpass.getpass()

    
def deploy_files(filelist, upload_path):
    global RW_USER
    global RW_PASSWD
    for f in filelist:
        print("Deploying [%s] ... " % f, end='')
        sys.stdout.flush()
        
        full_art_filepath = upload_path + '/' + f.split('/')[-1]
        path = ArtifactoryPath(full_art_filepath, auth=(RW_USER, RW_PASSWD))
        properties = path.properties
        path.unlink()
        
        path = ArtifactoryPath(upload_path, auth=(RW_USER, RW_PASSWD))
        path.deploy_file(f)
        
        # restore the old properties on file
        path = ArtifactoryPath(full_art_filepath, auth=(RW_USER, RW_PASSWD))
        path.set_properties(properties)
        
        print("done")

        
def do_download(pkglist):
    filenames_full = []
    for s in pkglist:
        filename = s.split('/')[-1]
        file_tmp = os.path.join(workdir, filename)
        filenames_full.append(file_tmp)
        
        print("Downloading [%s] ... " % filename, end='')
        sys.stdout.flush()
        
        path = ArtifactoryPath(s, auth=(RO_USER, RO_PASSWD))
        with path.open() as fd:
            with open(file_tmp, "wb") as out:
                out.write(fd.read())
        print("done")
        
    return filenames_full

def main():
    
    PKG_VOS_LOAD = []
    PKG_VOS2_LOAD = []
    PKG_VRX_LOAD = []
    PKG_FULL = []
    CMP_NAME = str.upper(arg_list[1])
    CMP_VERSION = arg_list[2]

    atexit.register(cleanup)
    
    
    ART_DL_PATH = str("http://artifactory.verifone.com:8081/artifactory/RMS_Snapshots_CLW/ADK/{0}/{1}").format(CMP_NAME, CMP_VERSION)
    ART_UL_PATH = str("http://artifactory.verifone.com:8081/artifactory/RMS_Snapshots_CLW/ADK/{0}/{1}").format(CMP_NAME, CMP_VERSION)

    print("Querying Artifactory ... ", end='')
    sys.stdout.flush()
    path = ArtifactoryPath(ART_DL_PATH, auth=(RO_USER, RO_PASSWD))
    for p in path.glob("*-full-*.zip"):
        PKG_FULL.append(str(p))

    for p in path.glob("*-vos-load-*.zip"):
        PKG_VOS_LOAD.append(str(p))

    for p in path.glob("*-vos2-load-*.zip"):
        PKG_VOS2_LOAD.append(str(p))

    for p in path.glob("*-vrx-load-*.zip"):
        PKG_VRX_LOAD.append(str(p))
        
    print("done")
    
    plist =((PKG_FULL, "FULL"),
            (PKG_VOS_LOAD, "V/OS (Trident) LOAD"),
            (PKG_VOS_LOAD, "V/OS2 (Raptor) LOAD"),
            (PKG_VOS_LOAD, "VERIX LOAD"))
    
    for i in plist:
        if len(i[0]) == 0:
            print("\n\n")
            print("#############################################################")
            print(str("ADK {0} {1} package under << {2} >> not found").format(CMP_NAME, i[1], ART_DL_PATH))
            print("#############################################################")
            exit(1)

    filenames_full = do_download(PKG_FULL)
    filenames_vos  = do_download(PKG_VOS_LOAD)
    filenames_vos2 = do_download(PKG_VOS2_LOAD)
    filenames_vrx  = do_download(PKG_VRX_LOAD)
    
    # call the resign script for all found packages for vos, vos2 and vrx independently as all use different signer cards
    do_signing(TARGET_VOS,  filenames_full, filenames_vos)
    do_signing(TARGET_VOS2, filenames_full, filenames_vos2)
    do_signing(TARGET_VRX, filenames_full, filenames_vrx)

    # finally ask for appropriate Artifactory login to upload the signed packages to Artifactory and do the actual deployment
    ask_for_password()

    # Print a warning
    print("\n\nFollowing files will be uploaded to %s:\n" % ART_UL_PATH)
    for f in filenames_full:
        print(f)
    for f in filenames_vos:
        print(f)
    for f in filenames_vos2:
        print(f)
    for f in filenames_vrx:
        print(f)
    print()
    print("\n\nThe next step replaces the according files irreversibly on Artifactory !!")
    
    while True:
        PROCEED = input("DO YOU WANT TO PROCEED? [Y,N] ")
        if str.upper(PROCEED) == 'N':
            exit(1)
        elif str.upper(PROCEED) == 'Y':
            break
    
    print()
    
    i = 2  # max two retries when getting wrong credentials
    while i >= 0:
        try:
            deploy_files(filenames_full, ART_UL_PATH)
            deploy_files(filenames_vos, ART_UL_PATH)
            deploy_files(filenames_vos2, ART_UL_PATH)
            deploy_files(filenames_vrx, ART_UL_PATH)
            break
        except OSError as oserr:
            print(oserr.strerror)
            exit(1)
        except RuntimeError as runerr:
            if str(runerr).find("Bad credentials") >= 0 and i > 0:
                print("\n\n")
                print(" >>>> Bad credentials found. Please enter again. <<<<\n")
                ask_for_password()
                i = i - 1
                continue
            print(runerr)
            exit(1)

if __name__ == "__main__":
    main()
