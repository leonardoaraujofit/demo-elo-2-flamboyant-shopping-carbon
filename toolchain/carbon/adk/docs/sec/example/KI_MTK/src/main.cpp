#include <stdio.h>
#include <string.h>
#include <svc.h>

#ifdef _VRXEVO
#include <eoslog.h>
#include <svc_sec.h>
#else
#include <unistd.h>
#include <vficom.h>
#include <svcsec.h>
#endif

#include "key_version.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef _VRXEVO
#define sleep(a)    SVC_WAIT(a*1000)
#define usleep(a)   SVC_WAIT(a/1000)
#endif

//using namespace std;

#define E99_LOAD_KEYS 3
#define E99_DEL_KEYS  4

char MSK_Script_name[] = "MSK10112.VSO";


static void KI_MTK(int what)
{
  int hCrypto;
  int ret;

  LOG_PRINTF("%s::Run", __FUNCTION__);

  // **************************************************************************************/
  #ifdef _VRXEVO
  LOG_PRINTF("%s::open the terminal crypto device", __FUNCTION__);
  // open the terminal crypto device
  hCrypto = open("/DEV/CRYPTO", 0);
  if (hCrypto < 0)
  {
    LOG_PRINTF("%s::Unable to open Crypto Device [ret %d] [err %d]", __FUNCTION__, hCrypto, errno);
    return;
  }
  LOG_PRINTF("%s::crypto device open", __FUNCTION__);
  #endif

  if (what == E99_LOAD_KEYS)
  {
    LOG_PRINTF("%s::Run iPS_LoadSysClearKey", __FUNCTION__);
    errno = 0;
    ret = iPS_LoadSysClearKey((unsigned char ) 0x00, (unsigned char *) "\x12\x34\x56\x78\x90\x12\x34\x56\xAB\xCD\xEF\xAB\xCD\xEF\xAB\xCD"); // or any random value
    LOG_PRINTF("%s::iPS_LoadSysClearKey [0x%x - errno 0x%x]", __FUNCTION__, ret, errno);

    if (ret != 0)
    {
      LOG_PRINTF("%s::Unable to set system key [ret %d] [err %d]", __FUNCTION__, ret, errno);
      return;
    }

    #ifdef _VRXEVO
    LOG_PRINTF("%s::Run iPS_InstallScript [%s]", __FUNCTION__, MSK_Script_name);
    errno = 0;
    ret = iPS_InstallScript(MSK_Script_name);
    LOG_PRINTF("%s::iPS_InstallScript [%s] [ret 0x%x - errno 0x%x]", __FUNCTION__, MSK_Script_name, ret, errno);
    #else
    ret = iPS_InstallScript();
    #endif
    if (ret != 0)
    {
      LOG_PRINTF("%s::Unable to install script [%s] [ret %d] [err %d]", __FUNCTION__, MSK_Script_name, ret, errno);
      #ifdef _VRXEVO
      char pathScript_Name[50];
      sprintf(pathScript_Name, "I:%s", MSK_Script_name);
      errno = 0;
      ret = iPS_InstallScript(pathScript_Name);
      LOG_PRINTF("%s::iPS_InstallScript [%s] [ret 0x%x - errno 0x%x]", __FUNCTION__, pathScript_Name, ret, errno);
      #endif
    }

    // an example of TMK
    unsigned char c_mtk[16] = {0x69, 0xC9, 0x24, 0x58, 0x58, 0x73, 0x86, 0x23,
                               0x17, 0xFD, 0xA3, 0x78, 0x82, 0xEC, 0xE7, 0x32};

    LOG_PRINTF("%s::Run iPS_LoadMasterClearKey", __FUNCTION__);
    errno = 0;
    ret = iPS_LoadMasterClearKey(0, 0, c_mtk);
    LOG_PRINTF("%s::iPS_LoadMasterClearKey(0, 0 of TMK [ret 0x%x - errno 0x%x]", __FUNCTION__, ret, errno);
    errno = 0;
    ret = iPS_LoadMasterClearKey(0, 1, &c_mtk[8]);
    LOG_PRINTF("%s::iPS_LoadMasterClearKey(0, 1 of TMK [ret 0x%x - errno 0x%x]", __FUNCTION__, ret, errno);
  }
  else if (what == E99_DEL_KEYS)
  {
    ret = iPS_DeleteKeys(DEL_ALL);
    LOG_PRINTF("%s::iPS_DeleteKeys ret %d", __FUNCTION__, ret);
    LOG_PRINTF("%s::iPS_LoadSysClearKey [ret 0x%x - errno 0x%x]", __FUNCTION__, ret, errno);
  }

  #ifdef _VRXEVO
  close(hCrypto);
  hCrypto = -1;
  LOG_PRINTF("%s::crypto device closed", __FUNCTION__);
  #endif
  // **************************************************************************************/

  return;
}




////////////////////////////////////////////////////////////////////////////////

int main (int argc, char *argv[])
{
  sleep(3);
  // initialise for system logging
  LOG_INIT("KEYINJECT", LOGSYS_COMM, 0xFFFFFFFF);

  LOG_PRINTF("MSK Key Injector Client (%s.%s.%s), Test key injection", KEY_MAJOR_VERSION, KEY_MINOR_VERSION, KEY_BUGFIX_VERSION);

  KI_MTK( E99_LOAD_KEYS );

  LOG_PRINTF("Key injection complete");

  return 0;
}

////////////////////////////////////////////////////////////////////////////////

