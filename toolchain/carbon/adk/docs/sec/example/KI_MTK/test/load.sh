
VARIANT=$1
TOOL=$2
echo "Variant" $VARIANT

if [ -z $TOOL ]; then
	TOOL="c:/eVoAps/SDK/1.1.0/VRXSDK/bin/ddl.exe"
fi

MY_PATH=`pwd`
cd $VARIANT

$TOOL -p9 -c -r*:*/ -rconfig.sys -fkeyinjector.dld
cd $MY_PATH
