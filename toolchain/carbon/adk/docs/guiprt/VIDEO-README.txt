Video package contents:
=======================

Just extract the content of this ZIP file into extracted doc folder of documentation package (guiprt-doc-X.X.X-X.zip).
These subfolders contain the following files:

   1. doc/guiprt:
      - avsdk-verix-readme-X.X.X.X.txt: README of AV Codec SDK (Verix eVo) with version information
      - avsdk-verix-programmers-guide-X.X.X.X.pdf: Documentation of AV Codec SDK (Verix eVo)
      - VIDEO-README.txt: This file

   2. doc/guiprt/example/load/videos: 
      - video.avi: AVI video file with resolution 582x388, which can be used for terminals 
                   with high resolution displays (e.g. MX9)
      - verifone.ogg: OGG video file with resolution 240x206, which should be used for Verix eVo
                      terminals with low resolution displays (e.g. vx820, vx680, vx675 or vx690).
      - sound.ogg:    OGG sound file, which should be used for Verix eVo terminals having a speaker (e.g. vx690 and vx6803G).
      - verifone.avi: Same video as verifone.ogg in AVI format. This video can be used for V/OS 
                      devices having low resolution displays.
      - vrx_demovideo_load.bat: Batch file for download Verix eVo demo video verifone.ogg

Video file installation instructions for GUIPRT demo:
=====================================================

The installation of video example files for GUIPRT demo is platform specific:

   1. V/OS:
   For V/OS (e.g. MX9 demo) just plug in USB device containing video file "video.avi".

   2. Verix eVo:
   Execute vrx_demovideo_load.bat to download verifone.ogg and sound.ogg for Verix GUIPRT demo.
   Just select COM port for DDL tool and follow instructions. Please note that the
   batch files uses DDL download tool, which is looked up under "%VRXSDK%\bin".
   For terminals having a USB interface it is also possible to plug in an USB device
   containing the video file verifone.ogg. Please note that HTML files (video.html 
   and video2.html found in resource folders) must refer to USB drive M: as follows:
   <video width="240" height="206" ...>
     <source src="file://M:/verifone.ogg">
     ...
   </video>

Prerequisites for video and audio playback:
===========================================

Video playback support requires installation of additional components depending on the platform:
   
   1. V/OS:
   V/OS terminals use the service libsvc_mplayer for that provides hardware accelerated video playback.
   Therefore, video support is only available on those platforms that contain that service. 
   
   2. Verix eVo:
   For video and audio playback on Verix the terminal must have installed AV codec component. The download package 
   of AV codec (dl.avcodec-X.X.X.X.zip) comes along with GUIPRT load package (guiprt-vrx-load-X.X.X-X.zip).
   Please note that GUIPRT server needs at least AV codec 1.1.1.2.

