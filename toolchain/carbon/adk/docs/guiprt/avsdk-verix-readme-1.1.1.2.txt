                        Verix eVo AVSDK Release Notes
                                Version 1.1.1.2
                              Dec 21, 2016

This is the Verix eVo software development kit for developing applications 
to play media files. AVSDK along with VRXSDK and EOSSDK, lets you build eVo
applications, that can play media files which will run on ARM11-based 
eVo platforms.  



Minimum Requirements
--------------------

1. Hardware Requirements:
-------------------------
    Vx680 3G		-- OS QT6G012L
    Vx680 GPRS		-- OS QT680109
    Vx680 BT Wi-Fi	-- OS QT6B0105
    Vx680 CDMA		-- OS QT6C0002
    Vx820 		-- OS QT820109
    Vx820 Duet 		-- OS QT820109
    Vx825 		-- OS QT830109
    Vx675		-- OS QT650103


2. Software Requirements:
-------------------------
    VRXSDK 3.9.6 or later
    EOSSDK 1.8.0.x or later
    RVDS 4.0 (bin: 902; inc & lib: 902)


SDK Installation
----------------
- Create a sub folder AVSDK in Verix eVo SDK folder
- Copy the provided file to this directory