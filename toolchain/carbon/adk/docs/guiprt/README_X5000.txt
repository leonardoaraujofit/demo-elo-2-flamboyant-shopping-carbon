
Notes for using HTMLPrinter on X5000

HTMLPrinter consists of two parts:
   - prtserver - the print server
   - libvfiprt.a - the import library for linking with programs that wish to print
   
prtserver is considered to be started on system startup. As long as it processes printing jobs it 
needs access to /dev/lp. Therefore, if another process is using /dev/lp it has to close that device first
before prtserver may be used for printing.

HTML receipt files and the configuration file prt.ini are read from folder www/print relative to the 
current working directory of the program that links libvfiprt.a. If an HTML file references images, then
these images must be readable by the prtserver process. If all processes use the same user, then this should
be the case but if prtserver runs as a different user care has to be taken so that it has sufficient access
rights.

Fonts are searched in folder /usr/lib/fonts, which is the default font directory on X5000.

