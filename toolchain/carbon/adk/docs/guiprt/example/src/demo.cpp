
#include <string>
#include <list>
#include <string.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include "html/gui.h"
#include "ipc/notify.h"
#ifndef NOPRINT
#   include "html/prt.h"
#endif
#include "html/jsobject.h"
#ifdef JS_SUPPORT
#  include "html/scriptproc.h"
#  include "html/jsproc.h"
#endif

#ifdef _WIN32
#  include <winsock2.h>
#  include <windows.h>
#  define sleep(x) Sleep(1000*(x))
#  define usleep(x) Sleep((x)/1000)
#endif

#include <sys/types.h>
#include <sys/timeb.h>
#if defined (linux) || defined (__CYGWIN__)
#   include <unistd.h>
#   include <dlfcn.h>
#endif

#ifdef _WIN32
#   define EMV_PIN 0x0A

#elif defined _VOS
#   include <svcsec.h>
#elif defined _VRXEVO
#   include <svc.h>
#   include <svc_sec.h>
// EMV_PIN not available in svc_sec.h on Verix
#   define EMV_PIN 0x0A
#   include <unistd.h>
#   include "html/vrx_printf.h"

static void verix_init()
{
   // try to enable fast free() on Verix, which implies a much better performance !!!
#ifdef FREE_TYPE_NO_VALIDATE  /* setFree() only available since SDK 3.8.1 providing define FREE_TYPE_NO_VALIDATE and setFree().
                               * Not enough to check _SYS_VERSION only, since this will throw compiler error for older SDKs (missing setFree()) */
   if(  (_SYS_VERSION>=0x301)       // SDK version
      &&(_syslib_version()>=0x301)) // OS version
   {
      setFree(FREE_TYPE_NO_VALIDATE);
   }
#endif
}

#else
#   define EMV_PIN 0x0A
#endif

using namespace std;
using namespace vfigui;
#ifndef NOPRINT
using namespace vfiprt;
#endif

enum StatusbarMode { NONE, STATUSBAR, KEYBOARD, STATUSBAR_KEYBOARD};

template<class T> class MinMsgQueue {

   std::list<T> queue;

   pthread_mutex_t mutex;
   pthread_cond_t  cond;

   MinMsgQueue(const MinMsgQueue&);
   void operator=(const MinMsgQueue &);

 public:
   MinMsgQueue() {
      pthread_mutex_init(&mutex,0);
      pthread_cond_init(&cond,0);
   }

   virtual ~MinMsgQueue() {
      pthread_cond_destroy(&cond);
      pthread_mutex_destroy(&mutex);
   }

   virtual void send(const T &msg) {
      pthread_mutex_lock(&mutex);
      queue.push_back(msg);
      pthread_cond_signal(&cond);
      pthread_mutex_unlock(&mutex);
   }

   virtual void receive(T &msg) {
      pthread_mutex_lock(&mutex);
      while(queue.empty()) pthread_cond_wait(&cond,&mutex);
      msg=queue.front();
      queue.pop_front();
      pthread_mutex_unlock(&mutex);
   }

   virtual bool empty() {
      bool r;
      pthread_mutex_lock(&mutex);
      r=queue.empty();
      pthread_mutex_unlock(&mutex);
      return r;
   }
};

bool cb_cancel(void *) { return false; }


MinMsgQueue<bool> statusbarQueue;

void *statusbar(void *p)
{
   bool active=false;
   bool immediate=false;

   while(1) {
      if(!active) { statusbarQueue.receive(active); immediate=true; }
      else if(!statusbarQueue.empty()) statusbarQueue.receive(active);
      if(!active) continue;

      struct timeb tp;
      ftime(&tp);
      if(!immediate) {
         usleep((1000-tp.millitm)*1000); // wait for beginning of next second
         tp.time++;
      }
      immediate=false;

      char buf[64]={0};
      struct tm *tm=localtime(&tp.time);
      if(tm) strftime(buf,sizeof(buf),"%d.%m.%Y - %H:%M:%S",tm);

      uiDisplay(1,uiPrint("<div style='background-color:black;color:white;height:100%%;text-align:center;overflow:hidden'>%S</div>",buf));
   }
   return 0;
}

void async_callback(void *data, UICBType type, UICBData &uidata)
{
   MinMsgQueue<int> *q=(MinMsgQueue<int> *)data;
   fprintf(stderr,"callback invoked\n");

   if(type==UI_CB_RESULT) q->send(uidata.result()); // signal main application that the dialog has finished
}

void async_cb2(void *data, UICBType type, UICBData &uidata)
{
   if(type!=UI_CB_RESULT && type!=UI_CB_UPDATE) return;

   MinMsgQueue<stringmap> *q=(MinMsgQueue<stringmap> *)data;
   stringmap v;
   v.swap(uidata.value());
   if(type==UI_CB_RESULT) {
      char buffer[16];
      sprintf(buffer,"%d",uidata.result());
      v["result"]=buffer;
   }
   q->send(v);
}



void updateStatusbar(enum StatusbarMode statusbar_mode, unsigned &transtype)
{
   switch(statusbar_mode) {
      case NONE:
         uiLayout("layout");
         uiSetTransition(transtype,500);
         break;
      case STATUSBAR:
         uiLayout("layout-status");
         uiSetTransition(transtype,500);
         break;
      case KEYBOARD:
         uiLayout("layout-kbd");
         uiSetTransition(transtype,500);
         uiInvokeURLDetached(2,"kbd.html");
         break;
      case STATUSBAR_KEYBOARD:
         uiLayout("layout-status-kbd");
         uiSetTransition(transtype,500);
         uiInvokeURLDetached(2,"kbd.html");
         break;
   }
   statusbarQueue.send(statusbar_mode==STATUSBAR || statusbar_mode==STATUSBAR_KEYBOARD);
}

void *led_test(void *)
{
   for(int i=0;i<3;i++) {
      if(i<2) uiConfigLEDs(i==0?UI_SHAPE_RECTANGLE:UI_SHAPE_ELLIPSE,40,20,0xff,0xff0000ff,0xffff00ff,0x00ff00ff,0x0000ffff);
      else uiConfigLEDs("led_config.png");
      uiShowLEDArea(UI_EDGE_TOP,200,24,0x808080ff);
      sleep(1);
      uiSetLED(0,1);
      uiSetLED(1,1);
      uiSetLED(2,1);
      uiSetLED(3,1);
      sleep(1);
      uiHideLEDArea();

      if(i<2) uiConfigLEDs(i==0?UI_SHAPE_RECTANGLE:UI_SHAPE_ELLIPSE,20,40,0xff,0xff0000ff,0xffff00ff,0x00ff00ff,0x0000ffff);
      else uiConfigLEDs("led_config.png");
      uiShowLEDArea(UI_EDGE_RIGHT,24,200,0x808080ff);
      sleep(1);
      uiSetLED(0,1);
      uiSetLED(1,1);
      uiSetLED(2,1);
      uiSetLED(3,1);
      sleep(1);
      uiHideLEDArea();

      if(i<2) uiConfigLEDs(i==0?UI_SHAPE_RECTANGLE:UI_SHAPE_ELLIPSE,40,20,0xff,0xff0000ff,0xffff00ff,0x00ff00ff,0x0000ffff);
      else uiConfigLEDs("led_config.png");
      uiShowLEDArea(UI_EDGE_BOTTOM,200,24,0x808080ff);
      sleep(1);
      uiSetLED(0,1);
      uiSetLED(1,1);
      uiSetLED(2,1);
      uiSetLED(3,1);
      sleep(1);
      uiHideLEDArea();

      if(i<2) uiConfigLEDs(i==0?UI_SHAPE_RECTANGLE:UI_SHAPE_ELLIPSE,20,40,0xff,0xff0000ff,0xffff00ff,0x00ff00ff,0x0000ffff);
      else uiConfigLEDs("led_config.png");
      uiShowLEDArea(UI_EDGE_LEFT,24,200,0x808080ff);
      sleep(1);
      uiSetLED(0,1);
      uiSetLED(1,1);
      uiSetLED(2,1);
      uiSetLED(3,1);
      sleep(1);
      uiHideLEDArea();
   }
   return 0;
}

void canvas_test();

// Verix applications using shared library of libvfigui should use main(), since main cannot be exported from shared object
int main(int argc, char *argv[])
{
#ifdef _WIN32
   {
      WSADATA d;
      WSAStartup(MAKEWORD(2,2),&d);
   }
#endif

#ifdef _VRXEVO
   verix_init();
#endif

#ifdef JS_SUPPORT
   htmlSetScriptProcessor("js",js::jsProcessorExt,0);
#endif

#if 0 // setting the backlight configuration is task of the power manager, activate just for testing.
   static const UIBacklight backlight_init[]={
      {100,5000}, // full brightness for 5s
      {50,10000}, // reduced brightness for 10s
      {0,0}       // minimum brightness
   };
   vector<UIBacklight> backlight_config(backlight_init,
                                        backlight_init+sizeof(backlight_init)/sizeof(backlight_init[0]));
   uiSetBacklightConfig(0,backlight_config);
#endif

   uiLayout("layout");

   uiSetPropertyInt(UI_PROP_CIRCULAR_MENU,1);
   uiSetPropertyInt(UI_PROP_TOUCH_ACTION_BEEP,1);
   uiSetPropertyInt(UI_PROP_INPUT_ERROR_BEEP,1);
   uiSetPropertyInt(UI_PROP_STATISTICS,1);

   // read some device properties
   int hasTouch;
   uiGetPropertyInt(UI_DEVICE_SUPPORTS_TOUCH, &hasTouch);
   int hasColorDisplay;
   uiGetPropertyInt(UI_DEVICE_SUPPORTS_COLOR_DISPLAY, &hasColorDisplay);
   int disp_w=-1,disp_h=-1;
   uiGetPropertyInt(UI_DEVICE_WIDTH, &disp_w);
   uiGetPropertyInt(UI_DEVICE_HEIGHT, &disp_h);
   int hasVideoSupport;
   uiGetPropertyInt(UI_DEVICE_SUPPORTS_VIDEO, &hasVideoSupport);
   int hasAudioSupport;
   uiGetPropertyInt(UI_DEVICE_SUPPORTS_AUDIO, &hasAudioSupport);

   // display platform info for 1 second
   {
     char rp[1024]="";
     uiGetPropertyString(UI_PROP_RESOURCE_PATH,rp,sizeof(rp));
     char *p=rp+strlen(rp)-1;
     while(p>rp && *p!='/' && *p!='\\') p--;
     if(p>rp) p++; else p=(char *)"none";
     uiDisplay(uiPrint("Platform: %s<br>Display: %dx%d %s<br>Resource path:<br>%s<br>Version: %s<br>SvcVersion: %s<br>",
                       p,disp_w,disp_h,hasColorDisplay?"color":"b/w",rp,gui_GetVersion(),gui_GetSvcVersion()));
     sleep(2);
   }

   // select image folder
   const char *images="images";             // colored images
   if(!hasColorDisplay) images="images_bw"; // b/w images

   enum StatusbarMode statusbar_mode=NONE;


   pthread_t statusthread;
   pthread_create(&statusthread,0,statusbar,0);

   unsigned transtype=UI_TRANS_NONE;
   uiSetTransition(transtype,500);

   struct UIMenuEntry menu[]={      {"Display",0,0},
                                    {"Confirm",1,0},
                                    {"Confirm/Cancel",2,0},
                                    {"Input Name",3,0},
                                    {"Input Amount",4,0},
                                    {"Input Password",5,0},
                                    {"Input Mask",6,0},
                                    {"Input Multi",7,0},
                                    {"Input Signature",23,0},
                                    {"Input Signature PNG",24,0},
                                    {"Input Signature ScrShot",25,0},
                                    {"YesNo",8,0},
                                    {"YesNoCancel",9,0},
                                    {"Form Input",10,0},
                                    {"HTML file",11,0},
                                    {"HTML sequence",12,0},
                                    {"Menu",13,0},
                                    {"PIN Input",14,0},
                                    {"PIN Check",15,0},
                                    {"Prefix=de-",16,0},
                                    {"Prefix=",17,0},
                                    {"Display Table",18,0},
                                    {"Font",19,0},
                                    {"Callback",20,0},
                                    {"Statusbar/Keyboard",21,0},
                                    {"Timeout demo",22,0},
                                    {"Grid",26,0},
                                    {"QR-Code",27,0},
                                    {"Multilanguage",28,0},
                                    {"Default dialog",29,0},
                                    {"Cancel dialog",30,0},
                                    {"Comparisons",31,0},
                                    {"Transitions",32,0},
                                    {"Popup",33},
#ifdef JS_SUPPORT
                                    {"JS script",34,0},
#endif
#ifndef NOPRINT
                                    {"Print receipt",35,0},
#endif
                                    {"Display layout",36,0},
                                    {"Long key press",37,0},
                                    {"Dual key press",38,0},
                                    {"Animate regions",39,0},
                                    {"Play video loop",102,0},
                                    {"Play video controls",103,0},
                                    {"Play music",104,0},
                                    {"Focus navigation",105,0},
#ifdef _VRXEVO
                                    {"Obtain console",200,0},
                                    {"Wait event",201,0},
#endif
//                                  {"Javascript",202,0},
#ifdef JS_SUPPORT
                                    {"Game of Dice",203,0},
                                    {"Weather report",204,0},
#endif
                                    {"CSS position",250,0},
                                    {"Second display",251,0},
                                    {"Enable logging",252,0},
                                    {"Disable logging",253,0},
                                    {"Performance test",300,0},
                                    {"LED test",301,0},
                                    {"Canvas test",302,0},
                                    {"Exit",303,0}
                                   };


   for(unsigned i=0;i<sizeof(menu)/sizeof(menu[0]);i++) {
      if(!hasTouch) { // disable features for devices not having a touch display
         if(menu[i].value==23) menu[i].options|=UI_MENU_DISABLED;
         if(menu[i].value==24) menu[i].options|=UI_MENU_DISABLED;
         if(menu[i].value==25) menu[i].options|=UI_MENU_DISABLED;
         if(menu[i].value==26) menu[i].options|=UI_MENU_DISABLED;
         if(menu[i].value==36) menu[i].options|=UI_MENU_DISABLED;
         if(menu[i].value==37) menu[i].options|=UI_MENU_DISABLED;
         if(menu[i].value==100) menu[i].options|=UI_MENU_DISABLED;
         if(menu[i].value==101) menu[i].options|=UI_MENU_DISABLED;
         if(menu[i].value==202) menu[i].options|=UI_MENU_DISABLED;
         if(menu[i].value==203) menu[i].options|=UI_MENU_DISABLED;
      }
      if(!hasColorDisplay) { // disable features for devices not having a color display
         if(menu[i].value==27) menu[i].options|=UI_MENU_DISABLED;
         if(menu[i].value==32) menu[i].options|=UI_MENU_DISABLED;
         if(menu[i].value==301) menu[i].options|=UI_MENU_DISABLED;
      }
      if(!hasVideoSupport) {
         if(menu[i].value==102) menu[i].options|=UI_MENU_DISABLED;
         if(menu[i].value==103) menu[i].options|=UI_MENU_DISABLED;
      }
      if(!hasAudioSupport) {
         if(menu[i].value==104) menu[i].options|=UI_MENU_DISABLED;
      }
      if(disp_h<64) { // disable features for devices having a small display
         if(menu[i].value==10) menu[i].options|=UI_MENU_DISABLED;
         if(menu[i].value==18) menu[i].options|=UI_MENU_DISABLED;
      }
      if(menu[i].value==251) {
         if(uiDisplayCount()<2) menu[i].options|=UI_MENU_DISABLED;
      }
#ifndef NOPRINT
      if(menu[i].value==35) { // disable features for printing, if there is no printer
         int res, state;
         res=prtGetPropertyInt(PRT_PROP_STATE,&state);
         if(res!=PRT_OK) state=res;
         if(state==PRT_NO_PRINTER) {
            menu[i].options|=UI_MENU_DISABLED;
         }
      }
#endif
#ifdef JS_SUPPORT
      if(menu[i].value==204) {
        string url=uiGetURLPath("weather.html");
        FILE *f=fopen(url.c_str(),"rb");
        if(f) fclose(f);
        else  menu[i].options|=UI_MENU_DISABLED;
      }
#endif
   }

   if(argc!=1) {
      string s="Parameters:<br>";
      for(int i=1;i<argc;i++) s+=string(argv[i])+"<br>";

      uiDisplay(s);
      sleep(3);
   }

   int r,s=0;
   bool exit_flag=false;
   while(!exit_flag) {
         s=uiMenu("mainmenu","Menu",menu,sizeof(menu)/sizeof(menu[0]),s>=0?s:0);
         if(s==UI_ERR_CONNECTION_LOST) break;

      switch(s) {
         /********************* Display *********************/
         case 0:
            uiDisplay("bgimage","<h4>Display</h4>Hello world!");
            sleep(2);
            uiDisplay("Line1<br>Line2<br>Line3<br>Line4<br>Line5<br>Line6<br>Line7<br>Line8<br>Line9<br>Line10<br>"
                      "Line1<br>Line2<br>Line3<br>Line4<br>Line5<br>Line6<br>Line7<br>Line8<br>Line9<br>Line10");
            sleep(2);
            break;

         /********************* Confirm *********************/
         case 1:{
            MinMsgQueue<int> q;
            uiConfirmAsync(0,"confirm","<h4>Confirmation</h4>Please press OK",async_callback,&q);
            int r;
            q.receive(r);
            break;
         }

         /********************* Confirm/Cancel *********************/
         case 2: {
            string result_text="<h4>Confirmation</h4>";
            string result_image;

            r=uiConfirm("confirm-cancel","<h4>Confirmation</h4>Please press OK or Cancel");
            if(r==UI_ERR_OK) {
               result_text+="You have pressed OK";
               result_image="<center><img src='../"+string(images)+"/confirmation.png' style='margin:auto;display:block'></center>";
            }
            else {
               result_text+="You have pressed Cancel";
               result_image=string("<center><img src='../"+string(images)+"/stop.png' style='margin:auto;display:block'></center>");
            }
            // show result with statistics (only for higher resolution displays)
            if(disp_h>=64) {
               const UIStatistics &s=uiStatistics();
               // add statistics result
               result_text+=uiPrint("<br><br>Statistics:<br>load css %ldms<br>load html %ldms<br>draw %ldms",
                                     (s.css-s.start).ms(),
                                     (s.html-s.css).ms(),
                                     (s.draw-s.html).ms());
               uiConfirm("confirm",result_text+"<br>"+result_image);
            } else { // for small screens, don't show statistics and display text in image in 2 steps
               uiDisplay(result_text);
               sleep(2);
               uiDisplay(result_image);
               sleep(2);
            }
            break;
         }

         /********************* Input Name *********************/
         case 3: {
            vector<string> value(1);
            value[0]="Test";
            MinMsgQueue<stringmap> q;

            uiSetPropertyInt(UI_PROP_UPDATE_EVENTS,1);
            uiInputAsync("input",value,
                         "<h4>Input</h4>Please enter your name <input style='width:100%' type='text' maxlength='20'>Footer",
                          async_cb2,&q);

            stringmap val;
            int r;
            while(1) {
               q.receive(val);
               if(val.count("result")) { r=atoi(val["result"].c_str()); break; }
               printf("update: %s\n",val["in0"].c_str());
            }

            if(r==UI_ERR_OK) {
               uiDisplay(string("<h4>Input</h4>Your name is:<br><span style='color:#ff0000'>")+val["in0"]);
               sleep(2);
            }
            else {
               uiDisplay(uiPrint("Result: %d<br>",r));
               sleep(2);
            }
            uiSetPropertyInt(UI_PROP_UPDATE_EVENTS,0);
            break;
         }

         /********************* Input Amount *********************/
         case 4: {
            vector<string> value(1);
            value[0]="123";

            if(disp_h>128) {
               r=uiInput("input",value,"<h4>Input</h4>Please enter Amount <input type='number' precision='2' autosize style='height:80px'>Footer");
            } else {
               r=uiInput("input",value,"<h4>Input</h4>Please enter Amount <input type='number' precision='2'>Footer");
            }

            uiDisplay(uiPrint("Result: %d<br>Input Amount:<br><span style='color:#ff0000'>%s</span>",r,value[0].c_str()));
            sleep(2);

            break;
         }

         /********************* Input Password *********************/
         case 5: {
            uiSetPropertyInt(UI_PROP_PASSWORD_CHAR,0x2022);
            uiSetPropertyInt(UI_PROP_PASSWORD_SHOW_CHAR,1000); // show last entered digit for 1000ms
            vector<string> value(1);
            r=uiInput("input",value,"<h4>Input</h4>Please enter Password ('0'-'9') <input size='6' type='password' maxlength='6' allowed_chars='0123456789'>Footer");
            if(r==UI_ERR_OK) {
               uiDisplay(string("<h4>Input</h4>Password was:<br><span style='color:#ff0000'>")+value[0]);
               sleep(2);
            }
            break;
         }

         /********************* Input Mask *********************/
         case 6: {
            vector<string> value(1);
            value[0]="ab";
            r=uiInput("input",value,"<h4>Input</h4>Please enter your string value <input type='mask' mask='*** ***'>Footer");
            if(r==UI_ERR_OK) {
               uiDisplay(string("<h4>Input</h4>Your value is:<br><span style='color:#ff0000'>")+value[0]);
               sleep(2);
            }
            break;
         }

         /********************* Input Multi *********************/
         case 7: {
            vector<string> value(2);
            value[0]="123";
            value[1]="test";
            r=uiInput("input",value,
                      "Please enter amount <input type='number' precision='2'><br>"
                      "Please enter name <input type='text' maxlength='20'>");
            if(r==UI_ERR_OK) {
               uiConfirm("confirm","<h4>Input</h4>Result:<br><span style='color:#ff0000'>"+value[0]+"<br>"+value[1]);
            }
            break;
         }

         /********************* YesNo *********************/
         case 8:
            r=uiConfirm("yesno","<h4>YesNo</h4>Select Yes or No");
            if(r==1) uiDisplay("<h4>YesNo</h4>You have selected Yes");
            else     uiDisplay("<h4>YesNo</h4>You have selected No");
            sleep(2);
            break;


         /********************* YesNoCancel *********************/
         case 9:
            r=uiConfirm("yesno-cancel","<h4>YesNo</h4>Select Yes/No/Cancel");
            if(r==1)      uiDisplay("<h4>YesNo</h4>You have selected Yes");
            else if(r==0) uiDisplay("<h4>YesNo</h4>You have selected No");
            else          uiDisplay("<h4>YesNo</h4>You have selected Cancel");
            sleep(2);
            break;

         /********************* Form Input *********************/
         case 10: {
            string html;
            map<string,string> value;

            value["name"]="Testuser";
            value["gender"]="male";
            value["age"]="1";
            value["subst"]="&<>\"'`!@$%()=+-{}[]";

            if(!hasTouch){
               html="Name: <input accesskey='&#13;' action='return 0' type='text' name='name'><br>"
                    "<table>"
                    "<tr><td>Gender:</td><td><input accesskey='1' type='radio' name='gender' value='male'> male (1)</td></tr>"
                    "<tr><td></td><td><input accesskey='2' type='radio' name='gender' value='female'> female (2)</td></tr>"
                    "</table>"
                    "Substituted value: <?var subst?>"
                    "<table>"
                    "<tr><td>18+</td><td><input accesskey='3' type='checkbox' name='age'>(3)</td></tr>"
                    "<tr><td>married</td><td><input accesskey='4' type='checkbox' name='married'>(4)</td></tr>"
                    "</table>"
                    "<button action='return 0' accesskey='&#13;' style='visibility:hidden'></button>"  // add a hidden ok button
                    "<button action='return -1' accesskey='&#27;' style='visibility:hidden'></button>"  // add a hidden cancel button
                    "<button accesskey='&#129;' action='call document.up()' style='visibility:hidden'></button>" // add hidden buttons for scrolling the page
                    "<button accesskey='&#132;' action='call document.down()' style='visibility:hidden'></button>"
                    "<button accesskey='&#138;' action='call focus.previous()' style='visibility:hidden'></button>" // add hidden buttons for focus selection
                    "<button accesskey='&#139;' action='call focus.next()' style='visibility:hidden'></button>";
            }else{
               html="<div style='background-color:yellow'>"
                    "Name: <input accesskey='&#13;' action='return 0' type='text' name='name'><br>"
                    "<table>"
                    "<tr><td>Gender:</td><td><input type='radio' name='gender' value='male'> male</td></tr>"
                    "<tr><td></td><td><input type='radio' name='gender' value='female'> female</td></tr>"
                    "</table>"
                    "Substituted value: <?var subst?>"
                    "<table>"
                    "<tr><td>18+</td><td><input type='checkbox' name='age'></td></tr>"
                    "<tr><td>married</td><td><input type='checkbox' name='married'></td></tr>"
                    "</table>"
                    "<button accesskey='&#13;' action='return 0'>submit</button>" // add submit button
                    "<button accesskey='&#27;' action='return -1'>cancel</button>" // add cancel button
                    "<button accesskey='&#129;' action='call document.up()' style='visibility:hidden'></button>" // add hidden buttons for scrolling the page
                    "<button accesskey='&#132;' action='call document.down()' style='visibility:hidden'></button>"
                    "</div>";
            }

            r=uiInvoke(value,html);

            string v=uiPrint("Result: %d<br>",r);
            map<string,string>::iterator i;
            for(i=value.begin();i!=value.end();++i) {
               v+=uiPrint("%S=%S<br>",i->first.c_str(),i->second.c_str());
            }

            uiConfirm("confirm",v);
            break;
         }

         /********************* HTML file *********************/
         case 11: {
            map<string,string> value;

            value["input"]="some text";

            r=uiInvokeURL(value,"test.html");

            string v=uiPrint("Result: %d<br>",r);
            map<string,string>::iterator i;
            for(i=value.begin();i!=value.end();++i) {
               v+=uiPrint("%S=%S<br>",i->first.c_str(),i->second.c_str());
            }
            // show result with statistics (only for higher resolution displays)
            if(disp_h>=64) {
               const UIStatistics &s=uiStatistics();
               // add statistics result
               v+=uiPrint("<br>Statistics:<br>load css %ldms<br>load html %ldms<br>draw %ldms",
                          (s.css-s.start).ms(),
                          (s.html-s.css).ms(),
                          (s.draw-s.html).ms());
            }
            uiConfirm("confirm",v);
            break;
         }

         /********************* HTML sequence *********************/
         case 12: {
            map<string,string> value;

            value["value"]="Testuser";
            value["confirm_label"]="Your name is:";

            string html="<h4>Input form</h4>"
                        "Name: <input action='load confirm.html' accesskey='&#13;' type='text' name='value'><br>";

            if (!hasTouch) {
               html+="<button action='return -1' accesskey='&#27;' style='visibility:hidden'></button>";  // add a hidden cancel button
            }
            else html+="<button accesskey='&#13;' action='load confirm.html'>submit</button>" // add submit button
                       "<button accesskey='&#27;' action='return -1'>cancel</button>"; // add cancel button

            html+="<button accesskey='&#129;' action='call document.up()' style='visibility:hidden'></button>" // add hidden buttons for scrolling the page
                  "<button accesskey='&#132;' action='call document.down()' style='visibility:hidden'></button>"
                  "<button accesskey='&#139;' action='call document.up()' style='visibility:hidden'></button>" // add hidden buttons for scrolling the page
                  "<button accesskey='&#138;' action='call document.down()' style='visibility:hidden'></button>"
                  "<button accesskey='&#130;' action='call cursor.left()' style='visibility:hidden'></button>" // add hidden buttons for moving the cursor
                  "<button accesskey='&#131;' action='call cursor.right()' style='visibility:hidden'></button>";

            r=uiInvoke(value,html);

            string v=uiPrint("Result: %d<br>",r);
            map<string,string>::iterator i;
            for(i=value.begin();i!=value.end();++i) {
               v+=uiPrint("%S=%S<br>",i->first.c_str(),i->second.c_str());
            }

            uiConfirm("confirm",v);
            break;
         }

         /********************* Menu *********************/
         case 13: {
            map<string,string> value;
            r=uiInvokeURL(value,"mainmenu.html");
            string v=uiPrint("Result: %d<br>",r);
            map<string,string>::iterator i;
            for(i=value.begin();i!=value.end();++i) {
               v+=uiPrint("%S=%S<br>",i->first.c_str(),i->second.c_str());
            }
            uiConfirm("confirm",v);
            break;
         }

         /********************* PIN Input *********************/
         case 14: {
            map<string,string> value;

            // modify this values to change default behaviour of PIN entry
            uiSetPropertyInt(UI_PROP_PASSWORD_CHAR,'*');
            uiSetPropertyInt(UI_PROP_PIN_AUTO_ENTER,0);  // 1 -> enable
            uiSetPropertyInt(UI_PROP_PIN_CLEAR_ALL,0);   // 1 -> enable
            uiSetPropertyInt(UI_PROP_PIN_BYPASS_KEY,8);  // 13 -> enter key, 8 -> clear key
            uiSetPropertyInt(UI_PROP_PIN_ALGORITHM,EMV_PIN);
            uiSetPropertyInt(UI_PROP_PIN_INTERCHAR_TIMEOUT,5000); // 5 seconds interchar timeout;

            r=uiInvokeURL(value,"pin.html");

            string v=uiPrint("Result: %d<br>",r);
            map<string,string>::iterator i;
            for(i=value.begin();i!=value.end();++i) {
               v+=uiPrint("%S=%S<br>",i->first.c_str(),i->second.c_str());
            }
            uiConfirm("confirm",v);
            break;
         }

         /********************* PIN Check *********************/
         case 15:
            uiSetPropertyInt(UI_PROP_PASSWORD_CHAR,'*');
            uiSetPropertyInt(UI_PROP_PASSWORD_SHOW_CHAR,0);
            r=uiInputPINCheck("pin-check","1234","<h4>PINCheck</h4>Guess my PIN <input type='password' maxlength='4' allowed_chars='0123456789'>Footer");
            if(r==UI_ERR_OK) uiDisplay("<h4>PINCheck</h4>The PIN was valid, you're a clairvoyant!");
            else if(r==UI_ERR_WRONG_PIN) uiDisplay("<h4>PINCheck</h4>Your entered PIN wasn't valid.");
            else uiDisplay("<h4>PINCheck</h4>Input aborted or general error");
            sleep(2);
            break;

         /********************* Prefix=de- *********************/
         case 16:
            uiSetPropertyString(UI_PROP_FILE_PREFIX,"de-");
            break;

         /********************* Prefix= *********************/
         case 17:
            uiSetPropertyString(UI_PROP_FILE_PREFIX,0);
            break;

         /********************* Display Table *********************/
         case 18: {
            JSObject l;
            l[0]("name")="item1";
            l[0]("value")=1;
            l[1]("name")="item2";
            l[1]("value")=2;
            l[2]("name")="item3";
            l[2]("value")=3;
            map<string,string> m;
            m["test"]=l.dump();

            string ok_button="<button action='return 0' accesskey='&#13;'>OK</button>";
            if(!hasTouch) ok_button="<button action='return 0' accesskey='&#13;' style='visibility:hidden'></button>";

            uiInvoke(m,"Table"
                     "<?foreach test"
                     "|(table border=1)(tr)(th)JSON(/th)(th)Name(/th)(th)Value(/th)(/tr)"
                     "|(tr)(td)[](/td)(td)[name](/td)(td)[value](/td)(/tr)"
                     "|(/table)"
                     "|Empty table(br)"
                     "?>"
                     +ok_button+
                     "<button accesskey='&#129;' action='call document.up()' style='visibility:hidden'></button>" // add hidden buttons for scrolling the page
                     "<button accesskey='&#132;' action='call document.down()' style='visibility:hidden'></button>"
                     "<button accesskey='&#139;' action='call document.up()' style='visibility:hidden'></button>" // add hidden buttons for scrolling the page
                     "<button accesskey='&#138;' action='call document.down()' style='visibility:hidden'></button>"
                     "<button accesskey='&#130;' action='call document.left()' style='visibility:hidden'></button>" // add hidden buttons for scrolling the page
                     "<button accesskey='&#131;' action='call document.right()' style='visibility:hidden'></button>"
                     "<button accesskey='2;' action='call document.up()' style='visibility:hidden'></button>" // add hidden buttons for scrolling the page
                     "<button accesskey='8;' action='call document.down()' style='visibility:hidden'></button>"
                     "<button accesskey='4' action='call document.left()' style='visibility:hidden'></button>" // add hidden buttons for scrolling the page
                     "<button accesskey='6;' action='call document.right()' style='visibility:hidden'></button>");
            m["test"]="";
            uiInvoke(m,"Table"
                     "<?foreach test"
                     "|(table border=1)(tr)(th)JSON(/th)(th)Name(/th)(th)Value(/th)(/tr)"
                     "|(tr)(td)[](/td)(td)[name](/td)(td)[value](/td)(/tr)"
                     "|(/table)"
                     "|(br)Empty table(br)"
                     "?>"
                     +ok_button+
                     "<button accesskey='&#129;' action='call document.up()' style='visibility:hidden'></button>" // add hidden buttons for scrolling the page
                     "<button accesskey='&#132;' action='call document.down()' style='visibility:hidden'></button>"
                     "<button accesskey='&#139;' action='call document.up()' style='visibility:hidden'></button>" // add hidden buttons for scrolling the page
                     "<button accesskey='&#138;' action='call document.down()' style='visibility:hidden'></button>"
                     "<button accesskey='&#130;' action='call document.left()' style='visibility:hidden'></button>" // add hidden buttons for scrolling the page
                     "<button accesskey='&#131;' action='call document.right()' style='visibility:hidden'></button>"
                     "<button accesskey='2;' action='call document.up()' style='visibility:hidden'></button>" // add hidden buttons for scrolling the page
                     "<button accesskey='8;' action='call document.down()' style='visibility:hidden'></button>"
                     "<button accesskey='4' action='call document.left()' style='visibility:hidden'></button>" // add hidden buttons for scrolling the page
                     "<button accesskey='6;' action='call document.right()' style='visibility:hidden'></button>");
            l.clear();
            l[0]="item1";
            l[1]="item2";
            l[2]="item3";
            m["test"]=l.dump();
            uiInvoke(m,"List:<br><?foreach test||[](br)|?>"+ok_button);
            break;
         }

         /********************* Font *********************/
         case 19:
         {
            while(1) {
               int i;
               char font[256]="";
               const struct UIMenuEntry fmenu[]={{"dejavu sans mono",0,0},
                                                 {"dejavu sans"     ,1,0},
                                                 {"dejavu serif"    ,2,0},
                                                 {"tiresias pcfont" ,3,0},
                                                 {"artema"          ,4,0}};
               int menusize=(sizeof(fmenu)/sizeof(fmenu[0]));

               uiGetPropertyString(UI_PROP_DEFAULT_FONT,font,sizeof(font));

               for(i=0;i<menusize;i++) if(fmenu[i].text==font) break;

               i=uiMenu("menu","<h4>Select font</h4>",fmenu,menusize,i>=menusize?0:i);
               if(i>=0) {
                  // configure new font
                  uiSetPropertyString(UI_PROP_DEFAULT_FONT,fmenu[i].text.c_str());

                  int fontsize;
                  const struct UIMenuEntry smenu[]={{"8",8,0},
                                                    {"9",9,0},
                                                    {"10",10,0},
                                                    {"11",11,0},
                                                    {"12",12,0},
                                                    {"13",13,0},
                                                    {"14",14,0},
                                                    {"16",16,0},
                                                    {"18",18,0},
                                                    {"20",20,0},
                                                    {"24",24,0},
                                                    {"28",28,0}};
                  int menusize=(sizeof(smenu)/sizeof(smenu[0]));

                  uiGetPropertyInt(UI_PROP_DEFAULT_FONT_SIZE,&fontsize);
                  for(i=0;i<menusize;i++) if(smenu[i].value==fontsize) break;
                  i=uiMenu("menu","<h4>Select font size</h4>",smenu,menusize,i>=menusize?0:smenu[i].value);
                  if(i>=0) uiSetPropertyInt(UI_PROP_DEFAULT_FONT_SIZE,i);
                  else if(i==UI_ERR_BACK) continue;
               }
               break;
            }
            break;
         }

         /********************* Callback *********************/
         case 20:
            r=uiConfirm("confirm","Dummy text",cb_cancel);
            uiConfirm("confirm",uiPrint("Cancelled dialog result: %d<br>",r));
            break;

         /********************* Statusbar/Keyboard *********************/
         case 21: {
            int tout;
            int max_opts=4;
            if(!hasTouch) max_opts=2;
            uiGetPropertyInt(UI_PROP_TIMEOUT,&tout);
            uiSetPropertyInt(UI_PROP_TIMEOUT,-1);
            statusbar_mode=(StatusbarMode)((statusbar_mode+1)%max_opts);
            updateStatusbar(statusbar_mode,transtype);

            uiSetPropertyInt(UI_PROP_TIMEOUT,tout);
            break;
         }

         /********************* Timeout demo *********************/
         case 22: {
            uiInvokeURL("timeout0.html");
            break;
         }


         /********************* Input Signature *********************/
         case 23: {
            map<string,string> value;

            if((r=uiInvokeURL(value,"signature.html"))!=UI_ERR_OK) {
               uiDisplay(uiPrint("Result: %d<br>",r));
               sleep(3);
               break;
            }

            JSObject v;
            v.load(value["signature"]);
            if(v.isArray()) {
               for(unsigned i=0;i<v.size();i++) {
                  printf("%d %d\n",int(v[i]("x")),int(v[i]("y")));
               }
            }
            break;
         }

         case 24:
         case 25: {
            stringmap value;
            const char *sightml="";

            switch(s) {
               default:
               case 24: sightml="signature2.html"; break; // PNG
               case 25: sightml="signature3.html"; break; // screenshot
            }

            // If an application wants to be signalled when a signature is provided,
            // update events may be used:
            MinMsgQueue<stringmap> q;
            uiSetPropertyInt(UI_PROP_UPDATE_EVENTS,1);
            if((r=uiInvokeURLAsync(value,sightml,async_cb2,&q))<UI_ERR_OK) {
               uiDisplay(uiPrint("Result: %d<br>",r));
               sleep(3);
               break;
            }
            uiSetPropertyInt(UI_PROP_UPDATE_EVENTS,0);

            while(1) {
               q.receive(value);
               if(value.count("result")) { r=atoi(value["result"].c_str()); break; }
               printf("signature started: %s\n",value["signature"].c_str());
            }


            JSObject v;
            v.load(value["signature"]);

            string sigfile=value["signature"];
            if(sigfile=="") {
               uiDisplay("No signature was given");
               sleep(3);
               break;
            }
            FILE *fp=fopen(sigfile.c_str(),"rb");
            if(!fp) {
               uiDisplay(uiPrint("Couldn't open file %s!<br>",sigfile.c_str()));
               sleep(3);
               break;
            }
            fclose(fp);
            uiDisplay(uiPrint("file=%s<br>",value["signature"].c_str()));
            sleep(3);
            break;
         }

         /********************* Grid *********************/
         case 26: {
            map<string,string> value;
            uiInvokeURL(value,"grid.html");
            break;
         }

         /********************* QR-Code *********************/
         case 27:
            uiInvokeURL("qrcode.html");
            break;

         /********************* Multilanguage *********************/
         case 28: {
            string lookup_en, lookup_de;
            map<string,string> value;
            value["sample"]="multilang.html";
            value["hl"]="headline"; // variable name for HTML catalog lookup
            printf("Catalog '%s' => ",uiGetCatalog().c_str());
            uiSetCatalog("en.ctlg");
            printf("'%s'\n",uiGetCatalog().c_str());
            lookup_en=uiGetText("text3","not found");
            uiInvokeURL(value,"multilang.html");
            uiSetCatalog("de.ctlg");
            lookup_de=uiGetText("text3","not found");
            uiInvokeURL(value,"multilang.html");
            uiSetCatalog(""); // unload
            uiDisplay(uiPrint("Lookup results:<br>lookup_en=%s<br>lookup_de=%s",lookup_en.c_str(),lookup_de.c_str()));
            sleep(3);
            break;
         }

         /********************* Default dialog *********************/
         case 29:
            uiInvokeURL("default.html");
            break;

         /********************* Cancel dialog *********************/
         case 30: {
            int txn_id;
            txn_id=uiConfirmAsync("confirm","<h4>Cancel dialog</h4>Press OK or dialog is cancelled after 5s.");
            if(txn_id>=0) {
               r=uiConfirmWait(txn_id,5000);
               if(r==UI_ERR_WAIT_TIMEOUT) {
                  r=uiInvokeCancel(txn_id);
                  uiDisplay(uiPrint("Dialog cancelled<br>Result: %d<br>",r));
               }
               else {
                  uiDisplay(uiPrint("Dialog confirmed<br>Result: %d<br>",r));
               }
               sleep(3);
            }
            break;
         }

         /********************* Comparisons *********************/
         case 31:
            uiInvokeURL("compare.html");
            break;


         /********************* Transitions *********************/
         case 32: {
            UIMenuEntry typemenu[]={  {"None",      UI_TRANS_NONE,0},
                                       {"Slide",     UI_TRANS_SLIDE,0},
                                       {"Slide on",  UI_TRANS_SLIDEON,0},
                                       {"Swap",      UI_TRANS_SWAP,0},
                                       {"Crossfade", UI_TRANS_CROSSFADE,0}
            };

            int type=uiMenu("mainmenu","<h4>Select transition</h4>",typemenu,5,transtype & UI_TRANS_TYPE_MASK);
            if(type<UI_ERR_OK) break;
            if(type==UI_TRANS_NONE) {
               transtype=type;
               uiSetTransition(transtype,500);
               break;
            }

            UIMenuEntry dirmenu[]={  {"Left", UI_TRANS_LEFT,0},
                                     {"Up",   UI_TRANS_UP,0},
                                     {"Right",UI_TRANS_RIGHT,0},
                                     {"Down", UI_TRANS_DOWN,0}
            };

            int dir;
            if(type==UI_TRANS_CROSSFADE) dir=0;
            else dir=uiMenu("mainmenu","<h4>Select direction</h4>",dirmenu,4,transtype & UI_TRANS_DIRECTION_MASK);
            if(dir<UI_ERR_OK) break;

            UIMenuEntry easemenu[]={ {"Linear",  0,          0},
                                     {"Ease in", UI_TRANS_EASE_IN,0},
                                     {"Ease out", UI_TRANS_EASE_OUT,0},
                                     {"Ease inout", UI_TRANS_EASE_INOUT,0}
            };

            int ease=uiMenu("mainmenu","<h4>Select direction</h4>",easemenu,4,transtype & UI_TRANS_EASE_MASK);
            if(ease<UI_ERR_OK) break;

            transtype=type+dir+ease;
            uiSetTransition(transtype,500);
            break;
         }

         /********************* Comparisons *********************/
         case 33:
            uiInvokeURL("popup.html");
            break;

         /********************* JS *********************/
#ifdef JS_SUPPORT
         case 34: {
            stringmap value;
            value["lines"]="10";
            r=uiInvokeTemplate(value,"confirm-cancel","Enter number of lines:<br><input type='number' name='lines' precision='0'>");
            if(r>=UI_ERR_OK) {
               r=uiInvokeTemplate(value,"confirm","<?js for(i=0;i<ARGV.lines;i++) print('Line ',i+1,'<br>') ?>");
               if(r==UI_ERR_SCRIPT) {
                  uiDisplay(uiPrint("Script Error: %s",uiScriptError().c_str()));
                  sleep(10);
               }
            }
            break;
         }
#endif
#ifndef NOPRINT
         case 35: {
            map<string,string> value;

            // set the content for variable data in the receipt
            value["data"]="http://www.verifone.com";
            value["amount"]="USD 123.41";
            // fill the data for the table (<?foreach ...?> processing instruction)
            JSObject jsobj;
            jsobj[0]("name")="item1";
            jsobj[0]("value")=1;
            jsobj[1]("name")="item2";
            jsobj[1]("value")=2;
            jsobj[2]("name")="item3";
            jsobj[2]("value")=3;
            value["test"]=jsobj.dump();
            value["lines"]="5";  // print 5 lines with js (if supported)

            // catalog file
            value["sample"]="en.ctlg"; // value for built-in variable
            value["hl"]="headline"; // variable name for HTML catalog lookup
            prtSetCatalog("en.ctlg"); // load the catalog file

            // print the receipt with a synchronous call

            struct timeb begin,end;
            ftime(&begin);
            int err=prtURL(value,"demo.html");
            ftime(&end);
            unsigned elapsed=(unsigned)end.time*1000u+(unsigned)end.millitm-(unsigned)begin.time*1000u-(unsigned)begin.millitm;
            uiDisplay(uiPrint("Printer result: %d<br>%s<br>Time: %u.%03us",err,err==PRT_SCRIPT_ERROR ? prtScriptError().c_str():"",elapsed/1000,elapsed%1000));
            sleep(5);
            break;
         }
#endif

         case 36: {
            vector<UIRegion> reg;
            int r=uiGetLayout(reg);

            string s=uiPrint("result: %d<br>",r);
            for(unsigned i=0;i<reg.size();i++) {
               s+=uiPrint("%d: %d,%d,%d,%d<br>",reg[i].id,reg[i].left,reg[i].top,reg[i].right,reg[i].bottom);
            }

            uiDisplay(s);
            sleep(5);
            break;
         }

         case 37: {
            int r=uiInvoke("Press button (short or long)<br><button action='return 1' action2='return 2'>OK</button>");
            if(r==1) uiDisplay("short button press");
            else if(r==2) uiDisplay("long button press");
            else uiDisplay("timeout");
            sleep(5);
            break;
         }
         case 38: {

            int r=uiInvoke("Press key combination Enter+9 to abort the dialog<br>"
                           "<button accesskey='&#13;+9' action='return 1' style='visibility:hidden'></button>");
            if(r==1) uiDisplay("dialog aborted");
            else uiDisplay("timeout");
            sleep(5);
            break;
         }
         case 39: {
            int id;
            UIRegion reg[2]={
               {0,0,0,-1,29,0},
               {3,0,30,-1,-1,0}
            };

            uiLayout(reg,2);

            string button;
            if(hasTouch) {
               button="<button accesskey='&#13;' action='return 1'>OK</button>";
            } else {
               button="Press OK.<button action='return 1' accesskey='&#13;' style='visibility:hidden'></button>";  // add a text and a hidden ok button
            }
            uiDisplay(0,"<table style='height:100%;width:100%;background-color:#ff0;border:1px solid #000'><tr><td>Region 1</table>");
            id=uiInvokeAsync(3,"<table style='height:100%;width:100%;background-color:#0ff;border:1px solid #000'><tr><td>Region 2<br>"+button+"</table>");

            r=0;
            for(int i=0;i<30;i++) {
               reg[0].bottom++;
               reg[1].top++;
               uiLayout(reg,2);
               r=uiInvokeWait(id,100);
               if(r==1) break;
            }
            if(r!=1) uiInvokeCancel(id);

            updateStatusbar(statusbar_mode,transtype);

            uiDisplay(uiPrint("OK %s pressed",r==1 ? "" : "not"));
            sleep(2);

            break;
         }
         /********************* Play Mx9-Video loop *********************/
         case 102: {
            uiInvokeURL("video.html");
            const vector<UIErrorEntry> &err=uiErrorList();
            string s;
            for(unsigned i=0;i<err.size();i++) {
               s+=uiPrint("%d %S<br>",err[i].type, err[i].name.c_str());
            }
            if(s.length()) {
               uiDisplay(s);
               sleep(10);
            }
            break;
         }

         /********************* Play Mx9-Video controls *********************/
         case 103: {
            stringmap value;
            value["audiosupport"]=hasAudioSupport?"1":"0";
            uiInvokeURL(value,"video2.html");
            break;
         }
         /********************* Play Mx9-Audio controls *********************/
         case 104:
            uiInvokeURL("audio.html");
            break;

         case 105:
            uiInvokeURL("focusnav.html");
            break;

#ifdef _VRXEVO
         /********************* Obtain console  *********************/
         case 200: {
            uiDisplay("");
            uiObtainConsole();
            clrscr();
            write_at("Obtained console!",17, 0, 0);
            sleep(2);
            clrscr();
            uiReleaseConsole();
            uiDisplay("Released console!");
            sleep(2);
            break;
         }

         /********************* Wait event *********************/
         case 201: {
            int txn_id;
            // enable Verix event notification
            uiSetVerixNotification(get_task_id(),1);
            txn_id=uiConfirmAsync(0,"confirm","<h4>Wait for result</h4>Please press OK");
            if(txn_id>=0) {
               while(1) {
                  wait_evt(EVT_USER);
                  int r=uiConfirmWait(txn_id,0);
                  if(r!=UI_ERR_WAIT_TIMEOUT) break;
               }
               uiDisplay(uiPrint("user_bits=%d<br>",read_user_event()));
            }
            // disable Verix event notification
            uiSetVerixNotification(0,0);
            sleep(3);
            break;
         }
#endif

         /********************* Javascript test *********************/
         case 202: {
            map<string,string> value;
            value["cnt"]="10";
            int r=uiInvokeURL(value,"jstest.html");
            uiDisplay(uiPrint("return=%d<br>cnt=%s<br>test=%s",r,value["cnt"].c_str(),value["test"].c_str()));
            sleep(3);
            break;
         }

         case 203: {
            map<string,string> value;
            int r=uiInvokeURL(value,"dice.html");
            if(r==UI_ERR_SCRIPT) {
               uiDisplay(uiPrint("Script Error: %s",uiScriptError().c_str()));
               sleep(10);
            }
            break;
         }

         case 204: {
            map<string,string> value;
            value["cityName"]="Bad Hersfeld";
            string css_save;
            uiGetPropertyString(UI_PROP_CSS,css_save);
            uiSetPropertyString(UI_PROP_CSS,"weather.css");
            r=uiInvokeURL(value,"weather.html");
            uiSetPropertyString(UI_PROP_CSS,css_save);
            if(r==UI_ERR_SCRIPT) {
               uiDisplay(uiPrint("Script Error: %s",uiScriptError().c_str()));
               sleep(10);
            }
            break;
         }
         /********************* CSS position test *********************/
         case 250:
            uiInvokeURL("position.html");
            break;

         /********************* Second display *********************/
         case 251: {
            MinMsgQueue<int> queue;

            uiInvokeURLAsync("display1.html",async_callback,&queue);
            uiInvokeURLAsync(1,UI_REGION_DEFAULT,"display2.html",async_callback,&queue);

            int r;
            queue.receive(r);
            uiDisplay(uiPrint("OK has been pressed on display %d",r));
            uiDisplay(1,UI_REGION_DEFAULT,uiPrint("OK has been pressed on display %d",r));
            sleep(3);

            break;
         }

         case 252:
            uiSetLogMask(64);
            break;

         case 253:
            uiSetLogMask(0);
            break;

         /********************* Performance test *********************/
         case 300: { // performance test
            int timeout,test=0;
            const int loop=20;
#define RUN_TEST(cmd) do { struct timeb tp_start, tp_stop; \
                           unsigned long current_ms,total_ms=0; \
                           int i=0; \
                           test++; \
                           for(i=0; i<loop; i++) { \
                             ftime(&tp_start); \
                             r=UI_ERR_FAIL; \
                             cmd; \
                             if(r!=UI_ERR_OK && r!=UI_ERR_TIMEOUT) break; \
                             ftime(&tp_stop); \
                             current_ms=(unsigned long)((long)(tp_stop.time-tp_start.time)*1000+((long)tp_stop.millitm-(long)tp_start.millitm)); \
                             total_ms+=current_ms; \
                           } \
                           if(i==loop) uiDisplay(uiPrint("Test %d<br>Loop: %d<br>Total time: %ld ms<br>Time per Dialog: %ld ms",test,i,total_ms,total_ms/(unsigned long)i)); \
                           sleep(3); \
                         } while(0)

            uiGetPropertyInt(UI_PROP_TIMEOUT, &timeout);
            // set timeout to 0 to force dialog to return directly
            uiSetPropertyInt(UI_PROP_TIMEOUT, 0);

            // TEST 1: Display a text (without input)
            RUN_TEST(r=uiDisplay(uiPrint("uiDisplay(): %d",i+1)));
            // TEST 2: same as TEST 1, but use asynchronous function
            int txn_id;
            RUN_TEST(txn_id=uiDisplayAsync(uiPrint("uiDisplayAsync(): %d",i+1));if(txn_id>=0) r=uiDisplayWait(txn_id));
            // TEST 3: same as TEST 1, but add an input control (hidden button)
            const string button="<button action='return 0' accesskey='&#13;' style='visibility:hidden'></button>";
            RUN_TEST(r=uiDisplay(uiPrint("uiDisplay()/Button: %d",i+1)+button));
            // TEST 4: same as TEST 1, but add 7 input control (hidden buttons)
            const string buttons="<button accesskey='&#13;' style='visibility:hidden' action='return 0'></button>"
                                 "<button accesskey='&#129;' action='call document.up()' style='visibility:hidden'></button>"
                                 "<button accesskey='&#132;' action='call document.down()' style='visibility:hidden'></button>"
                                 "<button accesskey='&#130;' action='call document.left()' style='visibility:hidden'></button>"
                                 "<button accesskey='&#131;' action='call document.right()' style='visibility:hidden'></button>"
                                 "<button accesskey='&#138;' action='call focus.previous()' style='visibility:hidden'></button>"
                                 "<button accesskey='&#139;' action='call focus.next()' style='visibility:hidden'></button>";
            RUN_TEST(r=uiDisplay(uiPrint("uiDisplay()/7Buttons: %d",i+1)+buttons));
            // TEST 5: same as TEST 4, but use a template to display the text with 7 hidden buttons
            RUN_TEST(r=uiDisplay("confirm",uiPrint("uiDisplay()/Template: %d",i+1)));

            // restore timeout
            uiSetPropertyInt(UI_PROP_TIMEOUT,timeout);
            break;
         }
         case 301: {
            pthread_t thread;
            pthread_attr_t attr;
            pthread_attr_init(&attr);
            pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
            pthread_create(&thread,&attr,led_test,0);
            pthread_attr_destroy(&attr);
            break;
         }
         case 302: {
            canvas_test();
            break;
         }
         case 303: {
          JSObject o;
          o("libmac_cmd")="libmac_show_desktop";
          o("libmac_version")="3.16.5-79";
          ipcNotify("_mac","_mac_control_trigger",o,0,"gui-demo");
          exit_flag=true;
          break;
        }
      }
   }
}



static int current_color;
static int current_x,current_y;
static MinMsgQueue<bool> canvas_queue;

static void draw_colormap()
{
   UIDrawing d;
   d.color(0xff0000);
   d.rectf(0,0,20,20);
   d.color(0x00ff00);
   d.rectf(20,0,20,20);
   d.color(0x0000ff);
   d.rectf(40,0,20,20);
   d.color(0);
   d.rect(20*current_color,0,20,20);
   d.rect(20*current_color+1,1,18,18);

   uiDraw(0,d);
}

void event_callback(void *data, UICanvasEvent event, int x, int y, int key, unsigned flags)
{
   const static int color[]={0xff0000,0x00ff00,0x0000ff};

   if(event==UI_PUSH) {
      if(x<60 && y<20) {
         current_color=x/20;
         draw_colormap();
         current_x=current_y=-1;
      }
      else {
         UIDrawing d;
         d.color(color[current_color]);
         d.pixel(x,y);
         uiDraw(0,d);
         current_x=x;
         current_y=y;
      }
   }
   else if((event==UI_RELEASE || event==UI_DRAG) && current_x>=0 && current_y>=0 ) {
      UIDrawing d;
      d.color(color[current_color]);
      d.line(current_x,current_y,x,y);
      uiDraw(0,d);
      if(event==UI_DRAG) {
         current_x=x;
         current_y=y;
      } else {
         current_x=current_y=-1;
      }
   }
   else if(event==UI_KEYUP && key==13) {
      canvas_queue.send(true);
   }

   //printf("event: %s x=%d y=%d key=%d\n",event.c_str(),x,y,key);
}

void canvas_test()
{
   current_color=0;
   int width,height;
   uiGetRegionSize(0,width,height);

   int txn_id=uiCanvas(0,event_callback,0);
   if(txn_id>=UI_ERR_OK) {

      // determine size of texts
      vector<UITextWidth> w;
      w.push_back(UITextWidth("Paiting"));
      w.push_back(UITextWidth("demo"));

      int fheight,descent;
      uiTextMetrics(0,"dejavu sans",24,0,w,fheight,descent);

      UIDrawing d;
      d.clear(0xffffff);
      d.color(0);
      d.font("dejavu sans",24);
      d.color(0xffff00);
      d.trif(0,height/2,width-1,height/2,width/2,height-1);

      d.color(0x808080);
      d.rectf(0,20,60,10);
      d.image("../images/confirmation.png",0,20);

      static const unsigned char rgb[]={255,0,0, 255,0,0, 255,0,0, 255,0,0,
                                        255,0,0, 0,255,0, 0,255,0, 0,255,0,
                                        255,0,0, 0,255,0, 0,0,255, 0,0,255,
                                        255,0,0, 0,255,0, 0,0,255, 0,0,255};
      d.image(UIDrawing::IMG_RAW,rgb,sizeof(rgb),0,20,4,4);

      // print Painting\ndemo centered on screen
      d.color(0);
      d.text(w[0].text,(width-w[0].width)/2,height/2-descent);
      d.text(w[1].text,(width-w[1].width)/2,height/2-descent+fheight);

      uiDraw(0,d);
      draw_colormap();
      bool done;
      canvas_queue.receive(done);
   }
}
