#ifndef VRX_PRINTF
#define VRX_PRINTF

#ifdef _VRXEVO

#  include <stdarg.h>
#  include <stdio.h>

#  ifdef VFI_GUIPRT_SHARED_EXPORT
#    define DllSpec __declspec(dllexport)   // used for VSL symbol export
#  elif defined VFI_GUIPRT_STATIC_EXPORT
#    define DllSpec                         // used for static libraries
#  else
#    define DllSpec __declspec(dllimport)   // used for VSA symbol import (also used for static linking)
#  endif

namespace vfihtml {
#  if 0 
}   // for automatic indentation
#  endif
    
#  define printf(a,...)    ::vfihtml::vrx_fprintf(stdout,a,##__VA_ARGS__)
#  define fprintf(a,b,...) ::vfihtml::vrx_fprintf(a,b,##__VA_ARGS__)
#  define vprintf(a,b)     ::vfihtml::vrx_vfprintf(stdout,a,b)
#  define vfprintf(a,b,c)  ::vfihtml::vrx_vfprintf(a,b,c)

DllSpec int vrx_vfprintf(FILE *stream, const char *format, va_list ap);
DllSpec int vrx_fprintf(FILE *stream, const char *format, ...);
/* Specifies the component name used as line prefix for logging (default is: "GUIPRT").
 * Call this function before first usage of dbglogm()/vdbglogm(), otherwise the name is ignored and not set. */
DllSpec void vrx_set_component_name(const char *cn);

} // namespace vfihtml

#undef DllSpec

#endif // _VRXEVO
#endif // VRX_PRINTF
