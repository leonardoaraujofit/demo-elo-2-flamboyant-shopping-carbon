#!/bin/bash

# usage
function usage {
 echo "Usage: $argv0 (-w) NAME..."
 echo ""
 echo "Convert Unix and Windows format paths"
 echo ""
 echo "Output type options:"
 echo ""
 echo "  -w, --windows         print Windows form of NAMEs (C:\WINNT)"
 exit 1
}

function winpath() {
    if [ ${#} -eq 0 ]; then
        : skip
    elif [ -f "$1" ]; then
        local dirname=$(dirname "$1")
        local basename=$(basename "$1")
        echo "$(cd "$dirname" && pwd -W)/$basename" \
        | sed \
          -e 's|/|\\|g';
    elif [ -d "$1" ]; then
        echo "$(cd "$1" && pwd -W)" \
        | sed \
          -e 's|/|\\|g';
    else
        echo "$1" \
        | sed \
          -e 's|^/\(.\)/|\1:\\|g' \
          -e 's|/|\\|g'
    fi
}

function unixpath() {
    echo "$1" \
    | sed -r \
      -e 's/\\/\//g' \
      -e 's/^([^:]+):/\/\1/'
}

towin=false;

while getopts :w opt
do
  case $opt in
    w) towin=true;;
    *) usage;;
  esac
done

shift $((OPTIND - 1))

if [ $# -ne 1 ]; then usage; fi

if $towin; then 
  winpath "$1"
else 
  unixpath "$1"
fi

