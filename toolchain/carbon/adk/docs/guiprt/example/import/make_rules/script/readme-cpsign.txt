usage:
1. put you CP application into the required directory structure
   <app-folder>
   |-<mp>
   |   |-<app-id>
   |      |-mpmanifest.mft     (need adaption of sponsor=<your terminal sponsor>)
   |      |-appuninstall.mft   (need adaption of sponsor=<your terminal sponsor>)
   |      |-confOwner.json
   |-<www>
   |   |-<app-id>.mft
   |   |-<app-id>
   |      |-confApp.json
   |      |-<platform>
   |      |   |-app.css
   |      |   |-*.html
   
2. adapt sponsor ID in the mpmanifest.mft and appuninstall.mft
   
3. call cpsign.bat <app-folder>

4. find the generated packages
   <app-folder>
   |-<pkg>
     |-<os>
       |-pkg-<app-id>-inst.zip    (includes pkg-<app-id>.zip and pkg-<app-id>-o.zip in a single bundle, ready to install)
       |-pkg-<app-id>-remove.zip  (removal package for the application
       |-pkg-<app-id>.zip         (the CP application package)
       |-pkg-<app-id>-o.zip       (the Market place pacakge for the CP application)
