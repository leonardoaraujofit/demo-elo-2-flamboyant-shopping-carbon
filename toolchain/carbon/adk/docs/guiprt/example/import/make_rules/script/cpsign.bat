@echo off
echo CP Signer
echo --------------------------
set appdir=%1

FOR /F %%i IN ("%appdir%") do set appdir=%%~fi
set tempdir=%appdir%\pkg
set script_path=%~dp0
set bin_signer="%script_path%\vfisigner_win32.exe"
for /d %%D in (%appdir%/mp/*) do set appname=%%~nxD
set file_mpmanifest="%appdir%\mp\%appname%\mpmanifest.mft"
set file_confowner="%appdir%\mp\%appname%\confOwner.json"
set file_appuninstall="%appdir%\mp\%appname%\appuninstall.mft"
set ziptool="%script_path%\7z.exe" a -r

REM echo input dir: %appdir%
REM echo app name : %appname%
REM echo script_path: %script_path%
REM echo --------------------------
type %file_mpmanifest%
type %file_appuninstall%
echo --------------------------

REM **** a few checks and warnings
if "%1"=="" goto err_param
if not exist "%appdir%\mp\%appname%" goto err_mp_folder
if not exist "%appdir%\www\%appname%" goto err_www_folder
if not exist %file_mpmanifest% goto err_mp_manifest
if not exist %file_confowner% echo WARNING: no confOwner.json found
if not exist %file_appuninstall% echo WARNING: no appuninstall.mfg found

:verix-cpdev
echo VERIX-CPDEV:
set file_cert_approval="%script_path%\vs208928-cpapproval.crt"
set file_cert_manifest="%script_path%\vs208937-cpmanifest.crt"
set file_key_approval="%script_path%\vs208928-cpapproval.key"
set file_key_manifest="%script_path%\vs208937-cpmanifest.key"
set tempdir=%appdir%\pkg\vrx-cpdev
echo   output: %tempdir% 
call :packaging

:vos-osdev
echo VOS-OSDEV
set file_cert_approval="%script_path%\vs200047-sys14signer.crt"
set file_cert_manifest="%script_path%\vs200046-sys11signer.crt"
set file_key_approval="%script_path%\vs200047-sys14signer.key"
set file_key_manifest="%script_path%\vs200046-sys11signer.key"
set tempdir=%appdir%\pkg\vos-osdev
echo   output: %tempdir% 
call :packaging

:vos-cpdev
echo VOS-CPDEV
set file_cert_approval="%script_path%\vs208730-sys14signer.crt"
set file_cert_manifest="%script_path%\vs208729-sys11signer.crt"
set file_key_approval="%script_path%\vs208730-sys14signer.key"
set file_key_manifest="%script_path%\vs208729-sys11signer.key"
set tempdir=%appdir%\pkg\vos-cpdev
echo   output: %tempdir% 
call :packaging

goto :end


:packaging
REM **** delete old tempdir recursively and create freshly
rmdir "%tempdir%" /s /q > NUL 2>&1

REM **** create the application package
echo   create application package...
mkdir "%tempdir%\zip"
%ziptool% "%tempdir%\zip\%appname%.zip" %appdir%\www\* > NUL 2>&1
copy %file_cert_approval% "%tempdir%\zip" > NUL 2>&1
%bin_signer% --cert %file_cert_approval% --key %file_key_approval% %tempdir%\zip\%appname%.zip > NUL 2>&1
move "%tempdir%\zip\%appname%.zip.p7s" "%tempdir%\zip\%appname%.p7s" > NUL 2>&1
%ziptool% "%tempdir%\pkg-%appname%.zip" %tempdir%\zip\* > NUL 2>&1
mkdir "%tempdir%\all"
move "%tempdir%\zip\*" "%tempdir%\all" > NUL 2>&1
rmdir "%tempdir%\zip" /s /q

REM **** create the MP manifest package
echo   create marketplace package...
mkdir "%tempdir%\zip"
mkdir "%tempdir%\files\%appname%"
copy %file_confowner%  "%tempdir%\files\%appname%" > NUL 2>&1
copy %file_mpmanifest% "%tempdir%\files\%appname%" > NUL 2>&1
%ziptool% "%tempdir%\zip\%appname%-o.zip" %tempdir%\files\* > NUL 2>&1
copy %file_cert_manifest% "%tempdir%\zip" > NUL 2>&1
%bin_signer% --cert %file_cert_manifest% --key %file_key_manifest% %tempdir%\zip\%appname%-o.zip > NUL 2>&1
move "%tempdir%\zip\%appname%-o.zip.p7s" "%tempdir%\zip\%appname%-o.p7s" > NUL 2>&1
%ziptool% "%tempdir%\pkg-%appname%-o.zip" %tempdir%\zip\* > NUL 2>&1
move "%tempdir%\zip\*" "%tempdir%\all" > NUL 2>&1
rmdir "%tempdir%\files" /s /q
rmdir "%tempdir%\zip" /s /q

REM **** create the combination installation package
echo   create combined installation package...
%ziptool% "%tempdir%\pkg-%appname%-inst.zip" %tempdir%\all\* > NUL 2>&1
rmdir "%tempdir%\all" /s /q


REM **** create the uninstall packages
echo   create uninstall package...
if not exist %file_appuninstall% goto skip_uninstall
mkdir "%tempdir%\zip"
mkdir "%tempdir%\files\%appname%"
copy %file_appuninstall% "%tempdir%\files\%appname%" > NUL 2>&1
%ziptool% "%tempdir%\zip\%appname%-u.zip" %tempdir%\files\* > NUL 2>&1
rmdir "%tempdir%\files" /s /q
copy %file_cert_manifest% "%tempdir%\zip" > NUL 2>&1
%bin_signer% --cert %file_cert_manifest% --key %file_key_manifest% %tempdir%\zip\%appname%-u.zip > NUL 2>&1
move "%tempdir%\zip\%appname%-u.zip.p7s" "%tempdir%\zip\%appname%-u.p7s" > NUL 2>&1
%ziptool% "%tempdir%\pkg-%appname%-u.zip" %tempdir%\zip\* > NUL 2>&1
rmdir "%tempdir%\zip" /s /q

exit /b



:skip_uninstall
echo WARNING: Skipping uninstall package creation.
goto end

:err_param
echo ERROR: wrong parameter.
echo usage cpsign.bat appdir
goto end

:err_mp_manifest
echo ERROR: marketplace manifest %file_mpmanifest% does not exist.
goto end

:err_mp_folder
echo ERROR: marketplace (MP) files folder %appdir%\mp\%appname% does not exist.
goto end

:err_www_folder
echo ERROR: application files (WWW) folder %appdir%\www\%appname% does not exist.
goto end

:end
echo done.
echo --------------------------