GUI and HTMLPrinter demo application
====================================

This GUI and HTMLPrinter demo application is provided as

I)  Downloadable/pre-compiled version with packages/binaries for V/OS, Raptor and Verix
II) Development version with Makefiles and source files

I) Downloadable version:
========================

   V/OS and Raptor platform:
   -------------------------
   Use the MX9 downloader to download the V/OS demo download packages.
   Please note that demo packages are development signed and need to be resigned with a productive card and 
   tools "Packager Manager Production" and "File Signature Tool", before downloading them to a productive device. 
   Download packages for GUIPRT demo:
    - load/dl.democlient-X.X.X-X.tar (demo application package for V/OS, development signed version, part of this ZIP package)
    - load/dl.vos2-democlient-X.X.X-X.tar (demo application package for Raptor, development signed version, part of this ZIP package)
    - load/dl.demofonts-X.X.X-X.tar  (font package for V/OS and Raptor, development signed version, part of this ZIP package)
    - dl.guiserver-prod-X.X.X-X.tar  (guiserver package for V/OS, productive signed version, part of package guiprt-vos-load-X.X.X-X.zip)
    - dl.guiserver-prod-X.X.X-X.tar  (guiserver package for Raptor, productive signed version, part of package guiprt-vos2-load-X.X.X-X.zip)
   
   
   Verix platform:
   ---------------
   Downloadable binaries of GUI demo are found in this ZIP package:
    - load/guiprt-vrx-demo-X.X.X-X.zip
   For installation please follow instructions of README.TXT that comes along with the archive. 
   
   
   Video playback support:
   -----------------------
   Since the size of sample videos would blow up this documentation package, an additional video distribution package
   is provided for this demo (guiprt-doc-video-X.X.X-X.zip). Just extract the ZIP file on the same folder level,
   where documentation package (guiprt-doc-X.X.X-X.zip) was extracted before. The video package contains additional 
   documentation and some video files with differnt resulotions for different displays. Please refer to the enclosed
   VIDEO-README.txt that contains more details about required components to meet the applicable prerequisites for video 
   playback. In addition, the file contains helpful instructions for installation of sample video files for this demo 
   application.
   
   
   JavaScript processor support:
   -----------------------------
   Since ADKGUI 2.1.1 JS (JavaScript) scripting processor is part of standard GUIPRT distribution package. 
   The library libjsproc is provided as static or as dynamic library as a kind of plugin for V/OS, Raptor
   and Verix platform. Following files and packages are available:
   
   a) Developement files for compiling and linking:
      V/OS:   jsproc.h, libjsproc.a, libjsproc.so                (part of guiprt-vos-dev-X.X.X-X.zip)
      Raptor: jsproc.h, libjsproc.a, libjsproc.so                (part of guiprt-vos2-dev-X.X.X-X.zip)
      Verix:  jsproc.h, libjsproc.a, libjsproc.so, libjsproc.vsl (part of guiprt-vrx-dev-X.X.X-X.zip)
      Instructions for using libjsproc with the demo application can be found in chapter II.
   
   b) Download packages for shared library:
      V/OS:   dl.libjsproc-X.X.X-X.tar (part of guiprt-vos-load-X.X.X-X.zip)
      Raptor: dl.libjsproc-X.X.X-X.tar (part of guiprt-vos2-load-X.X.X-X.zip)
      Verix:  dl.libjsproc-X.X.X-X.zip (part of guiprt-vrx-load-X.X.X-X.zip)
      Installation instructions:
      For V/OS and Raptor:
        Use the MX9 downloader to download the package dl.libjsproc-X.X.X-X-prod.tar found in subfolder load/guiprt
        of guiprt-vos-load-X.X.X-X.zip/guiprt-vos2-load-X.X.X-X.zip.
      For Verix eVo:
        Use the FST (File signature tool) and DDL download tool to sign and download the package dl.libjsproc-X.X.X-X.zip
        found in subfolder load/guiprt of guiprt-vrx-load-X.X.X-X.zip. More details and installation instructions can be 
        found README.txt that comes along with the ZIP archive.

II) Development version
=======================

Prerequisites for compiling the examples:

1) Installed Cygwin shell (http://www.cygwin.com)
   
2) Installed compilers for the desired platform. 
   Note: Path configurations for compiler and toolchain are located in following files:
   V/OS:   import/make_rules/vos_config
   Raptor: import/make_rules/vos2_config
   Verix:  import/make_rules/vrx_config
   x86:    import/make_rules/x86_config

Steps for compiling and running the example:

1) install the GUIPRT development package in import/<platform>, e.g.
   a) V/OS:   cd import/vos
              unzip guiprt-vos-dev-X.X.X-X.zip
   b) Raptor: cd import/vos2
              unzip guiprt-vos2-dev-X.X.X-X.zip
   c) Verix:  cd import/vrx
              unzip guiprt-vrx-dev-X.X.X-X.zip
   d) x86:    cd import/x86
              unzip guiprt-<distribution>-dev-X.X.X-X.zip
              <distirbution>: your linux distribution, e.g. "cygwin-i686-gcc-5.3.0"

2) install libposix full package for Verix eVo in import/vrx as follows: 
   a) download posix-full-X.X.X-X.zip from 
      http://artifactory.verifone.com:8081/artifactory/RMS_Release_CLW/ADK/POSIX
      Please note that GUI libraries at least require version 1.8.0 of libposix (posix-full-1.8.0-1.zip).
      It is recommended always to use the newest version of libposix.
   b) cd import
      unzip posix-full-X.X.X-X.zip

3) install the IPC full package in import/<platform>, e.g.
   a) download ipc-full-X.X.X-X.zip from 
      http://artifactory.verifone.com:8081/artifactory/RMS_Release_CLW/ADK/IPC
      Please note that GUI libraries at least require version 1.9.0 of libvfiipc (ipc-full-1.9.0-1.zip).
      It is recommended always to use the newest version of libvfiipc.
   b) cd import
      unzip ipc-full-X.X.X-X.zip
   For x86 platforms IPC is provided as builtin, therefore this step is obsolete here.

4) invoke make for the corresponding <platform>-<variant> using the corresponding make file, e.g.:
   a) V/OS release:
      make vos-release USE_SHARED=<shared> JS_SUPPORT=<js>
   b) Raptor release:
      make vos2-release USE_SHARED=<shared> JS_SUPPORT=<js>
   c) Verix release: 
      make vrx-release USE_SHARED=<shared> JS_SUPPORT=<js>
   d) x86 release: 
      make x86-release USE_SHARED=<shared> JS_SUPPORT=<js>
      
      Parameter description:
      <shared> set to 1 to use shared library (libvfiguiprt.so/vsl) or set to 0 
               for static linking with libvfiguiprt.a. Removing USE_SHARED variable
               also implies static linking with libvfiguiprt.a.
      <js>     set to 1 to add JS script processor support with library (libjsproc) 
               for the demo. Set to 0 or remove JS_SUPPORT variable disables JS support.
               Depending on value of USE_SHARED the Makefile uses libjsproc.a (for static
               linking) or libjsproc.so (for dynamic linking).
   
5) for installing Verix demo, invoke DDL tool with make as follows:
   make vrx-release-load vrx_DDL_PORT=<port> USE_SHARED=<shared> JS_SUPPORT=<js>
   <port> means COM port that is used by DDL tool
   <shared> see step 4
   <js>     see step 4
   If <shared> is set to 1, the Makefile looks up Verix download package dl.libvfiguiprt-X.X.X-X.zip in 
   folder import/vrx/load/guiprt.  If <shared> and <js> both are set to 1, the Makefile looks up Verix 
   download package dl.libjsproc-X.X.X-X.zip in folder import/vrx/load/guiprt. 
   Both download packages come along with load package guiprt-vrx-load-X.X.X-X.zip of this distribution,
   which needs to be installed as follows:
      cd import/vrx
      unzip guiprt-vrx-load-X.X.X-X.zip
   
   In addition, Makefile looks up Verix download pacakges dl.libvfiipc-X.X.X-X.zip and dl.libposix.zip.
   Both packages were installed with by step 2 and 3.
   
6) for installing demo for V/OS and Raptor, install the following packages with MX9 Downloader tool:
   a) V/OS:   vos-release/dl.democlient.tar
      Raptor: vos2-release/dl.democlient.tar
   b) ./dl.demofonts.tar     (generated by step 4, also found here: load/dl.demofonts-X.X.X-X.tar)
   c) dl.guiprtserver-X.X.X-X-prod.tar (guiprtserver package, productive signed version)
      For V/OS this package is part of package guiprt-vos-load-X.X.X-X.zip.
      For Raptor this package is part of package guiprt-vos2-load-X.X.X-X.zip.
   d) Since ADKGUI 2.14.0 guiprtserver was added dependency on the shared object libvfiipc.so.
      Therefore, dl.libvfiipc-X.X.X-X.tar must be installed,
      which is available import folder (by step 3) as follows:
      V/OS:   import/vrx/load/ipc/vos/load/ipc
      Raptor: import/vrx/load/ipc/vos2/load/ipc
   e) If the demo was built and dynamically linked with USE_SHARED=1, 
      packages containing the shared libraries needs to be installed, too:
      - dl.libvfiguiprt-X.X.X-X-prod.tar
        V/OS: see folder load/guiprt of guiprt-vos-load-X.X.X-X.zip.
        Raptor: see folder load/guiprt of guiprt-vos2-load-X.X.X-X.zip.
      - dl.libjsproc-X.X.X-X-prod.tar (if JS_SUPPORT=1, see step 4)
        V/OS: see folder load/guiprt of guiprt-vos-load-X.X.X-X.zip.
        Raptor: see folder load/guiprt of guiprt-vos2-load-X.X.X-X.zip.
   Please note that demo packages (dl.democlient.tar, dl.demofonts.tar) need to be resigned 
   with a productive card and tools "Packager Manager Production" and "File Signature Tool", 
   before downloading them to a productive device.



Steps for compiling with VeriFone DTK (Sourcery CodeBench) the example for V/OS:

1) install the development package in import/<platform>, e.g.
      V/OS:  cd import/vos
             unzip guiprt-vos-dev-X.X.X-X.zip

2) install the IPC full package in import/<platform>, e.g.
   a) download ipc-full-X.X.X-X.zip from 
      http://artifactory.verifone.com:8081/artifactory/RMS_Release_CLW/ADK/IPC/X.X.X-X
      Please note that GUI libraries at least require version 1.4.0 of libvfiipc (ipc-full-1.4.0-1.zip).
      It is recommended always to use the newest version of libvfiipc.
   b) cd import
      unzip ipc-full-X.X.X-X.zip
       
3) import eclipse project to VeriFone DTK (Sourcery CodeBench):
   a) File -> Import -> General/Existing Project into Workspace
   b) Select root directory: Browse to folder eclipse/democlient
   c) Click Finish
   
4) build variant "Release"

5) download/install the packages:
   a) eclipse/democlient/Release/dl.democlient.tar
   b) load/dl.eclipse-demo-resources-X.X.X-X.tar
   b) load/dl.demofonts-X.X.X-X.tar
   c) load/dl.guiprtserver-X.X.X-X-prod.tar (productive signed version, part of package guiprt-vos-load-X.X.X-X.zip)
   Please note that demo packages need to be resigned with a productive card and tools 
   "Packager Manager Production" and "File Signature Tool", before downloading them to 
   a productive device.
   
Please note: VeriFone DTK (Sourcery CodeBench) is not supported for Raptor so far.
