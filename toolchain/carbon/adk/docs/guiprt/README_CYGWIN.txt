How to build and run ADKGUIPRT on Windows with Cygwin
=====================================================

1. Install Cygwin with Unattended VFIADK setup available on stash:
   git clone ssh://git@stash.verifone.com:7999/ifadk/dev-adk-build-cygwin-setup.git
   Follow instructions of README.TXT.

2. Install following additional packages with Cygwin setup (installed by step 1 under c:\cygwin\setup-x86.exe):
   fltk-devel
   libfreetype-devel
   libpng-devel
   libboost-devel
   libgif-devel
   libtiff-devel
   libX11-devel
   dejavu-fonts
   xinit
   xlaunch

3. Build the project components with GNU make:
   make x86-release -j4
   For building demo application from documenation package, 
   please follow instructions of example/README.TXT to install
   ADKGUIPRT headers and libraries to import folder.

4. Run the demo application with guiserver/guiprtserver as follows:
   a) Start Cygwin XServer with command from cygwin bash:
      /usr/bin/startxwin &
   b) Set display variable in cygwin bash:
      export DISPLAY=:0
   c) Run guiprtserver as follows:
      x86-release/guiprtserver.exe &
      Or from documenation package example folder run it as follows:
      import/x86/bin/guiprt/guiprtserver.exe &
      -> confirm windows firewall settings with access for all networks (allow private, public etc.)
   d) Run democlient as follows:
      x86-release/democlient &
      
Have fun.

