var NAVTREE =
[
  [ "ADK-NFC", "index.html", [
    [ "Programmers Guide", "index.html", [
      [ "PREFACE", "index.html#sec_Preface", [
        [ "Audience", "index.html#subsec_Audience", null ],
        [ "Organization", "index.html#subsec_Organization", null ]
      ] ],
      [ "INTRODUCTION", "index.html#sec_Introduction", null ],
      [ "ARCHITECTURE", "index.html#sec_adk_nfc_Architecture", [
        [ "User Application", "index.html#subsec_adk_nfc_UserApplication", null ],
        [ "NFC Client", "index.html#subsec_adk_nfc_NfcClient", [
          [ "Communication Client (IPC)", "index.html#subsubsec_adk_nfc_NfcClientCom", null ],
          [ "Functional Client", "index.html#subsubsec_adk_nfc_NfcClientFun", null ]
        ] ],
        [ "NFC Server", "index.html#subsec_adk_nfc_NfcServer", null ],
        [ "NFC Framework", "index.html#subsec_adk_nfc_Framework", null ],
        [ "Pass Through", "index.html#subsec_adk_nfc_PassThrough", null ],
        [ "Mifare", "index.html#subsec_adk_nfc_Mifare", null ],
        [ "APDU", "index.html#subsec_adk_nfc_APDU", null ],
        [ "Felica", "index.html#subsec_adk_nfc_Felica", null ],
        [ "Technology Discovery", "index.html#subsec_adk_nfc_TechDiscovery", null ],
        [ "Wallet Manager", "index.html#subsec_adk_nfc_WalletMgr", null ]
      ] ],
      [ "GET STARTED", "index.html#sec_GetStarted", null ],
      [ "GENERAL INTERFACE", "index.html#sec_ADK_NFC_Gen_Interface", null ],
      [ "PASS THROUGH", "index.html#sec_ADK_NFC_PassThrough", [
        [ "Pass Through", "index.html#subsec_adk_nfc_fun_table_General", [
          [ "Pass Through TxRx Flow", "index.html#subsubsec_adk_nfc_flow_PassThrough", null ]
        ] ],
        [ "NFC Mifare", "index.html#subsec_adk_nfc_fun_table_Mifare", [
          [ "Mifare Flow", "index.html#subsubsec_adk_nfc_flow_Mifare", null ]
        ] ],
        [ "NFC APDU Exchange", "index.html#subsec_adk_nfc_fun_table_APDU", [
          [ "APDU Exchange Flow", "index.html#subsubsec_adk_nfc_flow_APDU", null ]
        ] ],
        [ "NFC Felica Polling", "index.html#subsec_adk_nfc_fun_table_FelicaPolling", null ],
        [ "NFC Felica Exchange", "index.html#subsec_adk_nfc_fun_table_Felica", [
          [ "Felica Exchange Flow", "index.html#subsubsec_adk_nfc_flow_Felica", null ]
        ] ],
        [ "VFI1 PROTOCOL INTERFACE", "index.html#sec_ADK_NFC_VFI1_Table", [
          [ "VFI1 Request Packet Format", "index.html#subsec_adk_nfc_vfi1_Req_packet_format", null ],
          [ "VFI1 Response Packet Format", "index.html#subsec_adk_nfc_vfi1_res_packet_format", null ],
          [ "VFI1 CRC", "index.html#subsec_adk_nfc_vfi1_CRC_packet_format", null ],
          [ "VFI1 Command Types", "index.html#subsec_adk_nfc_vfi1_CommandTypes", null ]
        ] ],
        [ "TAGS", "index.html#sec_ADK_NFC_Tags", [
          [ "Polling Request TAGS", "index.html#sec_ADK_NFC_PollReqTags", null ],
          [ "Polling Response TAGS", "index.html#sec_ADK_NFC_PollResTags", null ],
          [ "Activation Request TAGS", "index.html#sec_ADK_NFC_ActivationTags", null ],
          [ "Tx-Rx (send-Recieve) TAGS", "index.html#sec_ADK_NFC_TxRxTags", null ],
          [ "Mifare TAGS", "index.html#sec_ADK_NFC_Mifare", null ],
          [ "APDU TAGS", "index.html#sec_ADK_NFC_APDU", null ],
          [ "Felica TAGS", "index.html#sec_ADK_NFC_Felica", null ],
          [ "Set F Baud Rate TAGS", "index.html#sec_ADK_NFC_SetFbaud", null ]
        ] ]
      ] ],
      [ "VAS WALLETS", "index.html#sec_ADK_NFC_Wallets", [
        [ "VAS Wallet Functional Interface", "index.html#subsec_adk_nfc_vas_wallet_func_interface", null ],
        [ "Terminal Configuration Parameters", "index.html#subsec_adk_nfc_vas_terminal_config_types", null ],
        [ "Read Terminal Configuration Parameters", "index.html#subsec_adk_nfc_vas_read_terminal_config_types", null ],
        [ "VAS Wallet PreLoad/Dynamic Configuration Parameters", "index.html#subsec_adk_nfc_vas_preload_dynamic_config_types", [
          [ "PreLoad/Dynamic jSON objects", "index.html#subsubsec_adk_nfc_vas_prl_dyn", null ]
        ] ],
        [ "Select OSE response", "index.html#subsec_adk_nfc_vas_Select_OSe_Response", null ],
        [ "List Of Known JSON Objects", "index.html#subsec_adk_nfc_vas_wallet_json_objects", null ],
        [ "VAS Wallet Flowcharts", "index.html#subsec_adk_nfc_VAS_Wallets_Flows", [
          [ "NFC_Acitvate flowchart", "index.html#subsubsec_adk_nfc_VAS_Wallets_NFC_Activate", null ],
          [ "NFC_TERMINAL_Config flowchart", "index.html#subsubsec_adk_nfc_NFC_TERMINAL_Config", null ],
          [ "NFC_VAS_UpdateConfig flowchart", "index.html#subsubsec_adk_nfc_NFC_VAS_UpdateConfig", null ],
          [ "Preload configuration flowchart", "index.html#subsubsec_adk_nfc_Preload_configuration", null ],
          [ "NFC_VAS_Activate  with Dynamic Configuration flowchart", "index.html#subsubsec_adk_nfc_NFC_VAS_Activate_with_Dynamic_Configuration", null ],
          [ "Update and PreLoad configuration flow.", "index.html#subsubsec_adk_nfc_Update_vs_PreLoad", null ]
        ] ],
        [ "VAS Wallet JSON Examples", "index.html#subsec_adk_nfc_VAS_Wallets_Json", [
          [ "Terminal Configuration", "index.html#subsec_adk_nfc_VAS_Wallets_Json_TerminalConfig", null ],
          [ "Read Terminal Configuration", "index.html#subsec_adk_nfc_VAS_Wallets_Json_GetTerminalConfig", null ],
          [ "Dynamic Configuration", "index.html#subsec_adk_nfc_VAS_Wallets_Json_DynamicConfig", null ],
          [ "PreLoad Configuration", "index.html#subsec_adk_nfc_VAS_Wallets_Json_PreLoadConfig", null ]
        ] ]
      ] ],
      [ "USER INTERFACE", "index.html#sec_ADK_NFC_UI", [
        [ "UI Callback prototypes", "index.html#sec_ADK_NFC_UI_prototypes", null ],
        [ "UI types definition", "index.html#sec_ADK_NFC_UI_types", [
          [ "UI Callback flags", "index.html#sec_ADK_NFC_UI_types_flags", null ],
          [ "Text", "index.html#sec_ADK_NFC_UI_types_text", null ],
          [ "LED", "index.html#sec_ADK_NFC_UI_types_leds", null ],
          [ "Buzzer", "index.html#sec_ADK_NFC_UI_types_buzzer", null ],
          [ "General Callback Info", "index.html#sec_ADK_NFC_UI_types_callback_info", null ]
        ] ]
      ] ],
      [ "TYPES", "index.html#sec_ADK_NFC_Types", [
        [ "VasStatus", "index.html#subsec_adk_nfc_VasStatus", null ],
        [ "ResponseCodes", "index.html#subsec_adk_nfc_ResponseCodes", null ],
        [ "CL_STATUS", "index.html#subsec_adk_nfc_client_status", null ],
        [ "Polling Technologies Bitmap", "index.html#subsec_adk_nfc_poll_tech", null ],
        [ "NFC Framework State", "index.html#subsec_adk_nfc_frm_state", null ],
        [ "rawData", "index.html#subsec_adk_nfc_rawData", null ],
        [ "Polling time definition", "index.html#subsec_adk_nfc_polltime_calc", null ],
        [ "pollReq", "index.html#subsec_adk_nfc_poll_req", null ],
        [ "pollRes", "index.html#subsec_adk_nfc_poll_res", null ],
        [ "Card Information", "index.html#subsec_adk_nfc_card_info", null ],
        [ "cardInfo", "index.html#subsec_adk_nfc_cardInfo", null ],
        [ "NFC_CARD_TYPE", "index.html#subsec_adk_nfc_card_types", null ],
        [ "I_MIFARE_CARD_TYPE", "index.html#subsec_adk_nfc_mifare_card_types", null ],
        [ "MIFARE_KEY_TYPE", "index.html#subsec_adk_nfc_mifare_key_types", null ],
        [ "apduTxData", "index.html#subsec_adk_nfc_apdu_TxData", null ],
        [ "apduRxData", "index.html#subsec_adk_nfc_apdu_RxData", null ],
        [ "FeliCa TxData", "index.html#subsec_adk_nfc_felica_TxData", null ],
        [ "FeliCa Polling Parameters", "index.html#subsec_adk_nfc_felica_polling_param", null ],
        [ "FeliCa Polling Result", "index.html#subsec_adk_nfc_felica_polling_result", null ],
        [ "FeliCa RxData", "index.html#subsec_adk_nfc_felica_RxData", null ],
        [ "CONNECTION_TYPE", "index.html#subsec_adk_nfc_conn_type", null ],
        [ "NFC_F_BAUD", "index.html#subsec_adk_nfc_F_Baud", null ],
        [ "LOG BIT MASK", "index.html#subsec_adk_nfc_log_bitmask", null ]
      ] ],
      [ "MEMORY ALLOCATION", "index.html#sec_ADK_NFC_MemoryAllocation", null ],
      [ "EXAMPLES", "index.html#sec_ADK_NFC_Example", [
        [ "How to initiate polling - functional interface", "index.html#subsec_adk_nfc_examples_how_to_poll", null ],
        [ "How to initiate polling - serial interface", "index.html#subsec_adk_nfc_examples_how_to_poll_serial", null ]
      ] ],
      [ "DOCUMENTATION AND LINKS", "index.html#sec_ADK_NFC_AdditionalDocumentation", [
        [ "Links", "index.html#subsec_adk_nfc_additional_links", null ],
        [ "Documentation", "index.html#subsec_adk_nfc_additional_docs", null ]
      ] ],
      [ "KNOWN ISSUES", "index.html#sec_ADK_NFC_KnownIssues", null ],
      [ "HISTORY", "index.html#sec_ADK_NFC_History", null ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"index.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';