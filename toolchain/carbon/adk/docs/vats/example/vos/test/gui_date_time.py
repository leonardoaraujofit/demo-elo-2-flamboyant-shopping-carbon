#----------------------------------------------------------------------------
#
#    File : gui_date_time.py
#
#    VATS demo : GUI date and time testing
#
#    Author : Reinhard Lieberum
#
#    Creation date : 2014-Feb
#
#    Description:
'''Test module: GUI date and time test'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *
from vats_html import *
from datetime import datetime

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def gui_datetime_1():
    '''GUI date and time test, function 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = gui_prepare_test()

    # Choose menu item 1: "Miscellaneous"
    myMenu = vats_json.html_gui_selectMenu( 'Miscellaneous' )
    # Wait until headline indicates "Miscellaneous Menu"
    vats_json.html_waitDisplayContains( back_to_main_menu_item, AppName = 'vats-demo' )
    if( ats.check( vats_json.html_doesDisplayContain( misc_menu_title_gui, AppName = 'vats-demo' ), 'Select "Miscellaneous" menu failed!!!' ) ):

        # Choose menu item "date and time test" from menu to generate a result automatically
        myMenu = vats_json.html_gui_selectMenu( 'date and time test' )

        #----------------------------------------------------------------------------
        # Wait until headline indicates test start
        if ats.check( vats_json.html_waitDisplayContains( 'Date and Time', AppName = 'vats-demo' ), 'Test start failed!!!' ):

            #----------------------------------------------------------------------------
            # set date and time in terminal
            dt = datetime.now()
            # format string from example at http://docs.python.org/3/library/string.html#formatspec
            ats.log_info( "Now       : " + '{:%Y-%m-%d %H:%M:%S}'.format( dt ) )
            if( ( 29 == dt.day ) and ( 2 == dt.month ) ):
                # leap year
                # year, month, day, hour, minute, second
                dt = datetime( (dt.year - 1), dt.month, (dt.day - 1), dt.hour, dt.minute, 0 )
            else:
                # year, month, day, hour, minute, second
                dt = datetime( (dt.year - 1), dt.month, dt.day, dt.hour, dt.minute, 0 )
            # format string from example at http://docs.python.org/3/library/string.html#formatspec
            ats.log_info( "Changed to: " + '{:%Y-%m-%d %H:%M:%S}'.format( dt ) )
            user.set_DateAndTime( dt.year, dt.month, dt.day, dt.hour, dt.minute, 0 )

            # format string from example at http://docs.python.org/3/library/string.html#formatspec
            time_str = '{:%Y%m%d%H%M}'.format( dt )
            # Update display until expected time string is shown
            vats_json.html_waitDisplayContains( time_str, AppName = 'vats-demo' )
            # Check contents of display
            ats.check( vats_json.html_doesDisplayContain( time_str, AppName = 'vats-demo' ), "html_doesDisplayContain() failed!" )

            #----------------------------------------------------------------------------
            # reset date and time in terminal
            dt = datetime.now()
            # format string from example at http://docs.python.org/3/library/string.html#formatspec
            ats.log_info( "Now       : " + '{:%Y-%m-%d %H:%M:%S}'.format( dt ) )
            user.set_DateAndTime( dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second )

            # format string from example at http://docs.python.org/3/library/string.html#formatspec
            time_str = '{:%Y%m%d%H%M}'.format( dt )
            # Update display until expected time string is shown
            vats_json.html_waitDisplayContains( time_str, AppName = 'vats-demo' )

            # Check contents of display
            if( not vats_json.html_doesDisplayContain( time_str, AppName = 'vats-demo' ) ):
                # perhaps a minute / hour / day / month / year change occured:
                # e.g. datetime.now() was '2013-12-31 23:59:59' and now the terminal time is one
                # second later resulting in '2014-01-01 00:00:00'
                # so try once again with the current time
                dt = datetime.now()
                # format string from example at http://docs.python.org/3/library/string.html#formatspec
                ats.log_info( "Now       : " + '{:%Y-%m-%d %H:%M:%S}'.format( dt ) )
                user.set_DateAndTime( dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second )

                # format string from example at http://docs.python.org/3/library/string.html#formatspec
                time_str = '{:%Y%m%d%H%M}'.format( dt )
                # Update display until expected time string is shown
                vats_json.html_waitDisplayContains( time_str, AppName = 'vats-demo' )

            # Check contents of display
            ats.check( vats_json.html_doesDisplayContain( time_str, AppName = 'vats-demo' ), "html_doesDisplayContain() failed!" )

    # back to main menu
    gui_back_to_main_menu( response_time_gui = 700 )

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.7" )
ats.set_script_summary( "GUI date and time testing" )
ats.set_recovery_function( gui_back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( gui_datetime_1, "GUI date and time test 1" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
