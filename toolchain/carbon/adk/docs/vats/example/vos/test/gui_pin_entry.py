#----------------------------------------------------------------------------
#
#    File : gui_pin_entry.py
#
#    VATS demo : Keyboard testing
#
#    Author : Reinhard Lieberum
#
#    Creation date : 2013-Dec
#
#    Description:
'''Test module: PIN entry test'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *
from vats_html import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#
def wait_for_keypad_if_necessary( terminalIndex ):
    # e265, e315 and e335 seem to be slow while activating PIN entry
    if( ( 13 == terminalIndex ) or ( 15 == terminalIndex ) or ( 16 == terminalIndex ) ):
        user.wait( 1000 )

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def gui_pin_entry_1():
    '''GUI pin entry 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = gui_prepare_test()

    # Choose menu item 3: "User input"
    myMenu = vats_json.html_gui_selectMenu( 'User input' )
    # Wait until headline indicates "User Input Menu"
    vats_json.html_waitDisplayContains( back_to_main_menu_item, AppName = 'vats-demo' )
    if( ats.check( vats_json.html_doesDisplayContain( user_menu_title, AppName = 'vats-demo' ), 'Select "User input" menu failed!!!' ) ):

        # Choose menu item "PIN entry  test  1" from menu
        myMenu = vats_json.html_gui_selectMenu( 'PIN entry  test  1' )

        #---------------------------------------------------------------------------------------------------
        # Wait until GUI display indicates start of test
        if ats.check( vats_json.html_waitDisplayContains( 'PIN test', AppName = 'vats-demo' ), "Test start failed!!!" ):

            # Apply "4" "5" "6" "CORRECT" "7" "8" "9" "CONFIRM" key press sequence
            wait_for_keypad_if_necessary( myIndex )
            ats.check( user.enter_keys( "4" ), 'Input failed!!!' )
            ats.check( user.enter_keys( "5" ), 'Input failed!!!' )
            ats.check( user.enter_keys( "6" ), 'Input failed!!!' )
            ats.check( user.enter_keys( "<CORRECT>" ), 'Input failed!!!' )
            ats.check( user.enter_keys( "7" ), 'Input failed!!!' )
            ats.check( user.enter_keys( "8" ), 'Input failed!!!' )
            ats.check( user.enter_keys( "9" ), 'Input failed!!!' )
            ats.check( user.enter_keys( "<CONFIRM>" ), 'Input failed!!!' )

            # Wait until GUI display indicates end of test and check contents of display
            ats.check( vats_json.html_waitDisplayContains( 'Result: 0\ninput=5', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'html_waitDisplayContains() failed!' )

    # back to main menu
    gui_back_to_main_menu()


def gui_pin_entry_2():
    '''GUI pin entry 2'''

    # Wait for idle screen and set terminal specific values
    myIndex = gui_prepare_test()

    # Choose menu item 3: "User input"
    myMenu = vats_json.html_gui_selectMenu( 'User input' )
    # Wait until headline indicates "User Input Menu"
    vats_json.html_waitDisplayContains( back_to_main_menu_item, AppName = 'vats-demo' )
    if( ats.check( vats_json.html_doesDisplayContain( user_menu_title, AppName = 'vats-demo' ), 'Select "User input" menu failed!!!' ) ):

        # Choose menu item "PIN entry  test  1" from menu
        myMenu = vats_json.html_gui_selectMenu( 'PIN entry  test  1' )

        #---------------------------------------------------------------------------------------------------
        # Wait until GUI display indicates start of test
        if ats.check( vats_json.html_waitDisplayContains( 'PIN test', AppName = 'vats-demo' ), "Test start failed!!!" ):

            # Apply "1" "2" "4" "CANCEL" key press sequence
            wait_for_keypad_if_necessary( myIndex )
            ats.check( user.enter_keys ( "124<CANCEL>" ), 'Input failed!!!' )

            # Wait until GUI display indicates start of display test and check contents of display
            ats.check( vats_json.html_waitDisplayContains( 'Result: -1\ninput=cancel', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'html_waitDisplayContains() failed!' )

    # back to main menu
    gui_back_to_main_menu()

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.10" )
ats.set_script_summary( "GUI pin testing" )
ats.set_recovery_function( gui_back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( gui_pin_entry_1, "GUI pin entry 1" )
ats.add_test( gui_pin_entry_2, "GUI pin entry 2" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
