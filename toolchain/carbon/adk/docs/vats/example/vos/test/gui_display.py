#----------------------------------------------------------------------------
#
#    File : gui_display.py
#
#    VATS demo : Display testing
#
#    Author : Achim Groennert
#
#    Creation date : 2013-May
#
#    Description:
'''Test module: GUI display test'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *
from vats_html import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def gui_display_1():
    '''GUI display test with line breaks in the text 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = gui_prepare_test()

    # Choose menu item 2: "Display"
    myMenu = vats_json.html_gui_selectMenu( 'Display' )
    # Wait until headline indicates "Display Menu"
    vats_json.html_waitDisplayContains( back_to_main_menu_item, AppName = 'vats-demo' )
    if( ats.check( vats_json.html_doesDisplayContain( disp_menu_title, AppName = 'vats-demo' ), 'Select "Display" menu failed!!!' ) ):

        #----------------------------------------------------------------------------
        # Choose menu item "display    test  1" from menu
        myMenu = vats_json.html_gui_selectMenu( 'display    test  1' )
        # Wait until GUI display indicates start of display test and check contents of display
        ats.check( vats_json.html_waitDisplayContains( 'VATS test', AppName = 'vats-demo' ), 'Test start failed!' )

    # back to main menu
    gui_back_to_main_menu()

def gui_display_2():
    '''GUI display test with automatic line breaks 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = gui_prepare_test()

    # Choose menu item 2: "Display"
    myMenu = vats_json.html_gui_selectMenu( 'Display' )
    # Wait until headline indicates "Display Menu"
    vats_json.html_waitDisplayContains( back_to_main_menu_item, AppName = 'vats-demo' )
    if( ats.check( vats_json.html_doesDisplayContain( disp_menu_title, AppName = 'vats-demo' ), 'Select "Display" menu failed!!!' ) ):

        #----------------------------------------------------------------------------
        # Choose menu item "display    test  2" from menu
        myMenu = vats_json.html_gui_selectMenu( 'display    test  2' )

        # Wait a little bit to let trace finish
        if( ats.check( vats_json.html_waitDisplayContains( "1VATS Screen", AppName = 'vats-demo' ), "Test start failed!!!" ) ):

            # Check contents of display
            ats.check( vats_json.html_doesDisplayContain( "Display", AppName = 'vats-demo' ),                "html_doesDisplayContain() failed!" )
            ats.check( vats_json.html_doesDisplayContain( "1VATS Screen 123456782", AppName = 'vats-demo' ), "html_doesDisplayContain() failed!" )
            ats.check( vats_json.html_doesDisplayContain( "VATS Screen 123456783", AppName = 'vats-demo' ),  "html_doesDisplayContain() failed!" )
            ats.check( vats_json.html_doesDisplayContain( "8VATS Screen 12345678", AppName = 'vats-demo' ),  "html_doesDisplayContain() failed!" )

    # back to main menu
    gui_back_to_main_menu()

def gui_display_regions_1():
    '''GUI display test with display regions 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = gui_prepare_test()

    # Choose menu item 2: "Display"
    myMenu = vats_json.html_gui_selectMenu( 'Display' )
    # Wait until headline indicates "Display Menu"
    vats_json.html_waitDisplayContains( back_to_main_menu_item, AppName = 'vats-demo' )
    if( ats.check( vats_json.html_doesDisplayContain( disp_menu_title, AppName = 'vats-demo' ), 'Select "Display" menu failed!!!' ) ):

        #----------------------------------------------------------------------------
        # Choose menu item "disp.regs. test  1" from menu
        myMenu = vats_json.html_gui_selectMenu( 'disp.regs. test  1' )

        # Wait a little bit to let trace finish
        if( ats.check( vats_json.html_waitDisplayContains( "Region test", AppName = 'vats-demo' ), "Test start failed!!!" ) ):

            # Check contents of display
            ats.check( vats_json.html_waitDisplayContains( "01.11.2013 12:45", "/1", (default_menu_wait * 2), AppName = 'vats-demo' ), "Statusbar test 1 failed!!!" )
            if( ( 13 == myIndex ) or ( 14 == myIndex ) or ( 15 == myIndex ) ):
                # e315, e315M and e335 have small displays
                ats.check( vats_json.html_waitDisplayContains( "Update 1.", response_time = (default_menu_wait * 2), AppName = 'vats-demo' ), "Update test 1 failed!!!" )
            else:
                ats.check( vats_json.html_waitDisplayContains( "Main screen update 1.", response_time = (default_menu_wait * 2), AppName = 'vats-demo' ), "Update test 1 failed!!!" )
            ats.check( vats_json.html_waitDisplayContains( "01.11.2013 12:55", "/1", (default_menu_wait * 2), AppName = 'vats-demo' ), "Statusbar test 2 failed!!!" )
            if( ( 13 == myIndex ) or ( 14 == myIndex ) or ( 15 == myIndex ) ):
                # e315, e315M and e335 have small displays
                ats.check( vats_json.html_waitDisplayContains( "Update 2.", response_time = (default_menu_wait * 2), AppName = 'vats-demo' ), "Update test 2 failed!!!" )
            else:
                ats.check( vats_json.html_waitDisplayContains( "Main screen update 2.", response_time = (default_menu_wait * 2), AppName = 'vats-demo' ), "Update test 2 failed!!!" )
            ats.check( vats_json.html_waitDisplayContains( "01.11.2013 12:55", "/1", (default_menu_wait * 2), AppName = 'vats-demo' ), "Statusbar test 3 failed!!!" )
            if( ( 13 == myIndex ) or ( 14 == myIndex ) or ( 15 == myIndex ) ):
                # e315, e315M and e335 have small displays
                ats.check( vats_json.html_waitDisplayContains( "Update 3.", response_time = (default_menu_wait * 2), AppName = 'vats-demo' ), "Update test 3 failed!!!" )
            else:
                ats.check( vats_json.html_waitDisplayContains( "Main screen update 3.", response_time = (default_menu_wait * 2), AppName = 'vats-demo' ), "Update test 3 failed!!!" )
            ats.check( vats_json.html_waitDisplayContains( "status bar removal", response_time = (default_menu_wait * 2), AppName = 'vats-demo' ), "Test end failed!!!" )

    # back to main menu
    gui_back_to_main_menu()

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.11" )
ats.set_script_summary( "GUI display testing" )
ats.set_recovery_function( gui_back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( gui_display_1, "GUI display test with line breaks in the text 1" )
ats.add_test( gui_display_2, "GUI display test with automatic line breaks 1" )
ats.add_test( gui_display_regions_1, "GUI display test with display regions 1" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
