#----------------------------------------------------------------------------
#
#    File : gui_vatsemv_vos_ctls.py
#
#    VATS demo : EMV VOS CTLS testing
#
#    Author : Reinhard Lieberum
#
#    Creation date : 2014-Nov
#
#    Description:
#    with MasterCard Test Card ETEC Subset 8 - PayPass, PPC MAP 01, Version 2.0
'''Test module: GUI VATS EMV VOS CTLS test'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *
from vats_html import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def gui_vatsemv_vos_ctls():
    '''GUI VATS EMV VOS CTLS test'''

    # Wait for idle screen and set terminal specific values
    myIndex = gui_prepare_test()
    hasPrinter = vats_json.html_doesDisplayContain( 'Printer', AppName = 'vats-demo' )

    # Set timeout values
    responsetime_ms = 5000   # latest after this response time the desired display must appear
    holdtime_ms     = 500    # for minimum this hold time the display must be stable

    # Choose menu item "Card reader"
    myMenu = vats_json.html_gui_selectMenu( 'Card reader' )
    # Wait until headline indicates "Card Reader Menu"
    vats_json.html_waitDisplayContains( back_to_main_menu_item, AppName = 'vats-demo' )
    if( ats.check( vats_json.html_doesDisplayContain( card_menu_title, AppName = 'vats-demo' ), 'Select "Card reader" menu failed!!!' ) ):
        if( not vats_json.html_doesDisplayContain( 'EMV not supported', AppName = 'vats-demo' ) ):

            # Choose menu item "EMV           test" from menu
            myMenu = vats_json.html_gui_selectMenu( 'EMV           test' )

            # configure EMV CTLS
            ats.log_info( 'EMV CTLS simulation messages transfer started.' )
            # VATS clean-up
            user.send_emv_ctls_data ("FFFFFF")
            user.send_emv_ctls_data ("90000000F000")
            user.send_emv_ctls_data ("90DF0000F007DF420400008000")
            user.send_emv_ctls_data ("90DF0000F007DF420400008000")
            user.send_emv_ctls_data ("90DF0000F007DF420400008000")
            user.send_emv_ctls_data ("90DF0000F007DF420400008000")
            user.send_emv_ctls_data ("90E20000F0820168DF4204000088009F2701809F360202959F2608E3B2A7A123C0E3975A0A5413330089601018FFFF9F3901075F24031412319F4104000000005F34010057115413330089601018D141222001234091729F10120110A04009240000000000000000000000FF9F3704C1D2B0DB950500008080009A031408019F21031200009C01005F2A020036820259809F1A0200369F34033F00019F3303E040089F3501229F1E083432342D383032208407A00000000410109F090200029B02A8009F0607A00000000410109F5301529F02060000000200009F03060000000000009F120F505043204D41502030312020763230DFD0020F505043204D415020303120207632305F25030401019F4C082B111AEE93403505DF21050000000000DF2205FC509C8800DF2305FC509C88009F0E0500000000009F0F0500000000009F0D0500000000009F40056000B0A0019F160F202020202020202020202020202020C204000000039F660436204000")
            user.send_emv_ctls_data ("90000000F00CDF81290830F0F00038F0FF00")
            user.send_emv_ctls_data ("90E30000F0820168DF4204000008009F2701809F360202959F2608E3B2A7A123C0E3975A0A5413330089601018FFFF9F3901075F24031412319F4104000000005F34010057115413330089601018D141222001234091729F10120110A04009240000000000000000000000FF9F3704C1D2B0DB950500008080009A031408019F21031200009C01005F2A020036820259809F1A0200369F34033F00019F3303E040089F3501229F1E083432342D383032208407A00000000410109F090200029B02A8009F0607A00000000410109F5301529F02060000000200009F03060000000000009F120F505043204D41502030312020763230DFD0020F505043204D415020303120207632305F25030401019F4C082B111AEE93403505DF21050000000000DF2205FC509C8800DF2305FC509C88009F0E0500000000009F0F0500000000009F0D0500000000009F40056000B0A0019F160F202020202020202020202020202020C204000000039F660436204000")
            user.send_emv_ctls_data ("90000000F0098407A0000000041010")
            # indicates end of EMV input via VATS
            user.send_emv_ctls_data ("FFFF")
            ats.log_info( 'EMV CTLS simulation messages transfer ended.' )

            #---------------------------------------------------------------------------------------------------
            # Wait until GUI display indicates start of test
            if vats_json.html_waitDisplayContains( 'Insert or Tap', response_time = (default_menu_wait * 2), AppName = 'vats-demo' ):
                if( ats.check( vats_json.html_waitDisplayContains( 'Host Connection', response_time = 10000, AppName = 'vats-demo' ), 'Host Connection failed!!!' ) ):
                    # enter host result
                    myMenu = vats_json.html_gui_selectMenu( 'AC=00 (OK)' )
                    if( ats.check( vats_json.html_waitDisplayContains( "Online Response\nreceived", response_time = 10000, AppName = 'vats-demo' ), 'Online Response received missing!!!' ) ):
                        ats.check( vats_json.html_waitDisplayContains( "Transaction\napproved", response_time = 10000, AppName = 'vats-demo' ), 'Transaction approved missing!!!' )
            else:
                ats.log_info( 'EMV CTLS seems not to be possible with this terminal.' )

            # VATS clean-up
            user.send_emv_ctls_data ("FFFFFF")

        else:
            ats.log_warning( 'EMV not supported' )

    # back to main menu
    gui_back_to_main_menu()

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.6" )
ats.set_script_summary( "GUI VATS EMV VOS CTLS testing" )
ats.set_recovery_function( gui_back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( gui_vatsemv_vos_ctls, "GUI VATS EMV VOS CTLS test" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
