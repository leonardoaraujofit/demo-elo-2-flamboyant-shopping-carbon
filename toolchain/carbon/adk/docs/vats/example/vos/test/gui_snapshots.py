#----------------------------------------------------------------------------
#
#    File : gui_snapshots.py
#
#    VATS demo : Display snapshot testing
#
#    Author : Achim Groennert
#
#    Creation date : 2014-Oct
#
#    Description:
'''Test module: GUI display snapshot test'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *
from vats_html import *
from os import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#
def delete_existing_file( fileName = None ):
    if( None != fileName ):
        if( os.access( fileName, os.W_OK ) ):
            os.remove( fileName )

def check_if_file_exists( fileName = None, maxTimeout = 0 ):
    if( None != fileName ):
        start = user.get_reference_time()
        now   = user.get_reference_time()
        while( ( now - start ) <= maxTimeout ):
            if( os.access( fileName, os.R_OK ) ):
                return True
            user.wait_event()
            now = user.get_reference_time()
    return False

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def gui_snapshot_regions_1():
    '''GUI display snapshot test with display regions 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = gui_prepare_test()

    working_directory = user.get_working_directory()
    file1 = working_directory + 'regions_01.png'
    file2 = working_directory + 'regions_02.png'
    # Delete old files, if existent
    delete_existing_file( file1 )
    delete_existing_file( file2 )

    # Choose menu item 2: "Display"
    myMenu = vats_json.html_gui_selectMenu( 'Display' )
    # Wait until headline indicates "Display Menu"
    vats_json.html_waitDisplayContains( back_to_main_menu_item, AppName = 'vats-demo' )
    if( ats.check( vats_json.html_doesDisplayContain( disp_menu_title, AppName = 'vats-demo' ), 'Select "Display" menu failed!!!' ) ):

        #----------------------------------------------------------------------------
        # Choose menu item "disp.regs. test  1" from menu
        myMenu = vats_json.html_gui_selectMenu( 'disp.regs. test  1' )

        # Wait a little bit to let trace finish
        ats.check( vats_json.html_waitDisplayContains( "Region test", AppName = 'vats-demo' ), "Test start failed!!!" )

        # Check contents of display
        ats.check( vats_json.html_waitDisplayContains( "01.11.2013 12:45", "/1", (default_menu_wait * 2), (default_hold_time_gui // 2), AppName = 'vats-demo' ), "Statusbar test 1 failed!!!" )
        if( ( 13 == myIndex ) or ( 14 == myIndex ) or ( 15 == myIndex ) ):
            # e315, e315M and e335 have small displays
            ats.check( vats_json.html_waitDisplayContains( "Update 1.", response_time = (default_menu_wait * 2), hold_time = (default_hold_time_gui // 2), AppName = 'vats-demo' ), "Update test 1 failed!!!" )
        else:
            ats.check( vats_json.html_waitDisplayContains( "Main screen update 1.", response_time = (default_menu_wait * 2), hold_time = (default_hold_time_gui // 2), AppName = 'vats-demo' ), "Update test 1 failed!!!" )
        vats_json.take_terminal_display_snapshot( file1 )
        ats.check( vats_json.html_waitDisplayContains( "01.11.2013 12:55", "/1", (default_menu_wait * 2), (default_hold_time_gui // 2), AppName = 'vats-demo' ), "Statusbar test 2 failed!!!" )
        if( ( 13 == myIndex ) or ( 14 == myIndex ) or ( 15 == myIndex ) ):
            # e315, e315M and e335 have small displays
            ats.check( vats_json.html_waitDisplayContains( "Update 2.", response_time = (default_menu_wait * 2), hold_time = (default_hold_time_gui // 2), AppName = 'vats-demo' ), "Update test 2 failed!!!" )
        else:
            ats.check( vats_json.html_waitDisplayContains( "Main screen update 2.", response_time = (default_menu_wait * 2), hold_time = (default_hold_time_gui // 2), AppName = 'vats-demo' ), "Update test 2 failed!!!" )
        ats.check( vats_json.html_waitDisplayContains( "01.11.2013 12:55", "/1", (default_menu_wait * 2), (default_hold_time_gui // 2), AppName = 'vats-demo' ), "Statusbar test 3 failed!!!" )
        if( ( 13 == myIndex ) or ( 14 == myIndex ) or ( 15 == myIndex ) ):
            # e315, e315M and e335 have small displays
            ats.check( vats_json.html_waitDisplayContains( "Update 3.", response_time = (default_menu_wait * 2), hold_time = (default_hold_time_gui // 2), AppName = 'vats-demo' ), "Update test 3 failed!!!" )
        else:
            ats.check( vats_json.html_waitDisplayContains( "Main screen update 3.", response_time = (default_menu_wait * 2), hold_time = (default_hold_time_gui // 2), AppName = 'vats-demo' ), "Update test 3 failed!!!" )
        vats_json.take_terminal_display_snapshot( file2 )
        ats.check( vats_json.html_waitDisplayContains( "status bar removal", response_time = (default_menu_wait * 2), hold_time = (default_hold_time_gui // 2), AppName = 'vats-demo' ), "Test end failed!!!" )

        ats.check( check_if_file_exists( file1, default_print_wait ), "Snapshot 1 failed!" )
        ats.check( check_if_file_exists( file2, default_print_wait ), "Snapshot 2 failed!" )
        ats.log_warning( "Currently there is no mechanism for automated image compare available! Please compare manually the following files: " + file1 + " and " + file2 )

    # back to main menu
    gui_back_to_main_menu()

def gui_snapshot_menus_1():
    '''GUI display snapshot test with different menu selections 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = gui_prepare_test()

    working_directory = user.get_working_directory()
    file1 = working_directory + 'menus_01.png'
    file2 = working_directory + 'menus_02.png'
    # Delete old files, if existent
    delete_existing_file( file1 )
    delete_existing_file( file2 )

    # Choose menu item 2: "Display"
    myMenu = vats_json.html_gui_selectMenu( 'Display' )
    # Wait until headline indicates "Display Menu"
    vats_json.html_waitDisplayContains( back_to_main_menu_item, AppName = 'vats-demo' )
    if( ats.check( vats_json.html_doesDisplayContain( disp_menu_title, AppName = 'vats-demo' ), 'Select "Display" menu failed!!!' ) ):

        # back to main menu
        gui_back_to_main_menu()
        vats_json.take_terminal_display_snapshot( file1 )

        # Choose menu item 3: "User input"
        myMenu = vats_json.html_gui_selectMenu( 'User input' )
        # Wait until headline indicates "User Input Menu"
        vats_json.html_waitDisplayContains( back_to_main_menu_item, AppName = 'vats-demo' )
        if( ats.check( vats_json.html_doesDisplayContain( user_menu_title, AppName = 'vats-demo' ), 'Select "User input" menu failed!!!' ) ):

            # back to main menu
            gui_back_to_main_menu()
            vats_json.take_terminal_display_snapshot( file2 )

            ats.check( check_if_file_exists( file1, default_print_wait ), "Snapshot 1 failed!" )
            ats.check( check_if_file_exists( file2, default_print_wait ), "Snapshot 2 failed!" )
            ats.log_warning( "Currently there is no mechanism for automated image compare available! Please compare manually the following files: " + file1 + " and " + file2 )


#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.8" )
ats.set_script_summary( "GUI display snapshot testing" )
ats.set_recovery_function( gui_back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( gui_snapshot_regions_1, "GUI display snapshot test with display regions 1" )
ats.add_test( gui_snapshot_menus_1, "GUI display snapshot test with different menu selections 1" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
