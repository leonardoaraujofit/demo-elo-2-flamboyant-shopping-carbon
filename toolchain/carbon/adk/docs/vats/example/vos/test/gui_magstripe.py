#----------------------------------------------------------------------------
#
#    File : gui_magstripe.py
#
#    VATS demo : Magnetic stripe testing
#
#    Author : Achim Groennert
#
#    Creation date : 2013-May
#
#    Description:
'''Test module: GUI magnetic stripe test'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *
from vats_html import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def gui_magstripe_1():
    '''GUI Magnetic stripe test 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = gui_prepare_test()

    # Set timeout values
    responsetime_ms = 7000   # latest after this response time the desired display must appear
    holdtime_ms     = 500    # for minimum this hold time the display must be stable

    # Choose menu item "Card reader"
    myMenu = vats_json.html_gui_selectMenu( 'Card reader' )
    # Wait until headline indicates "Card Reader Menu"
    vats_json.html_waitDisplayContains( back_to_main_menu_item, AppName = 'vats-demo' )
    if( ats.check( vats_json.html_doesDisplayContain( card_menu_title, AppName = 'vats-demo' ), 'Select "Card reader" menu failed!!!' ) ):

        # Choose menu item "MSR read      test" from menu
        myMenu = vats_json.html_gui_selectMenu( 'MSR read      test' )

        #---------------------------------------------------------------------------------------------------
        # Wait until GUI display indicates start of test
        if ats.check( vats_json.html_waitDisplayContains( 'Magstripe test', AppName = 'vats-demo' ), 'Test start failed!!!' ):

            # Check that no magnetic stripe data is already read
            ats.check( not vats_json.html_doesDisplayContain( 'Total Bytes:', AppName = 'vats-demo' ), 'NOT html_doesDisplayContain() failed!' )

            #---------------------------------------------------------------------------------------------------
            # Swipe Card
            track1 = ""
            track2 = "4761739001010010=15122011143857589"
            track3 = ""
            user.swipe_magcard( track1 + "|" + track2 + "|" + track3 )

            # Check displayed magnetic stripe data
            pattern = "TK#1:%03d 00-NO ERR\s+TK#2:%03d 00-NO ERR\s+TK#3:%03d 00-NO ERR" % ( len( track1 ), len( track2 ), len( track3 ) )
            # take care: responsetime_ms must be large enough to include holdtime_ms
            ats.check( vats_json.html_waitDisplayContains( pattern, response_time = responsetime_ms, hold_time = holdtime_ms, AppName = 'vats-demo' ), 'Card swipe failed!!!' )

            # Check that magnetic stripe data is read
            ats.check( vats_json.html_doesDisplayContain( 'Total Bytes:   40', AppName = 'vats-demo' ), 'html_doesDisplayContain() failed!' )

            #---------------------------------------------------------------------------------------------------
            # Swipe next Card
            track1 = "11003132333D4A4F484E3D534D4954487802010201"
            track2 = "6726672910070115390D07091010274904490F"
            track3 = "015966666666D10070115392D2809540000000007001003012749020000007090232483411DD1D0000449000005020F"
            user.insert_hybrid_card( track1, track2, track3 )
            user.engage_hybrid_card( False, True )

            # Check displayed magnetic stripe data
            pattern = "TK#1:%03d 00-NO ERR\s+TK#2:%03d 00-NO ERR\s+TK#3:%03d 00-NO ERR" % ( len( track1 ), len( track2 ), len( track3 ) )
            # take care: responsetime_ms must be large enough to include holdtime_ms
            ats.check( vats_json.html_waitDisplayContains( pattern, response_time = responsetime_ms, hold_time = holdtime_ms, AppName = 'vats-demo' ), 'Card swipe failed!!!' )

            # Check that magdata is read
            ats.check( vats_json.html_doesDisplayContain( 'Total Bytes:  181', AppName = 'vats-demo' ), 'html_doesDisplayContain() failed!' )

            # Remove next card
            user.disengage_hybrid_card()
            user.remove_hybrid_card()

    # back to main menu
    gui_back_to_main_menu()

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.10" )
ats.set_script_summary( "GUI magnetic stripe testing" )
ats.set_recovery_function( gui_back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( gui_magstripe_1, "GUI Magnetic stripe test 1" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
