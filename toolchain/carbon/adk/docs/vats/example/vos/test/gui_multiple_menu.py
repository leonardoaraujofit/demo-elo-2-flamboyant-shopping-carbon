#----------------------------------------------------------------------------
#
#    File : gui_multiple_menu.py
#
#    VATS demo : Multiple menu testing
#
#    Author : Achim Groennert
#
#    Creation date : 2014-Jun
#
#    Description:
'''Test module: GUI multiple menu testing'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *
from vats_html import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def gui_multiple_menu_1():
    '''GUI multiple menu test 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = gui_prepare_test()

    # Choose menu item 3: "User input"
    myMenu = vats_json.html_gui_selectMenu( 'User input' )
    # Wait until headline indicates "User Input Menu"
    vats_json.html_waitDisplayContains( back_to_main_menu_item, AppName = 'vats-demo' )

    #---------------------------------------------------------------------------------------------------
    if( ats.check( vats_json.html_doesDisplayContain( user_menu_title, AppName = 'vats-demo' ), 'Select "User input" menu failed!!!' ) ):

        # Choose menu item "Multiple menus" from menu
        myMenu = vats_json.html_gui_selectMenu( 'Multiple menus' )

        # Wait until GUI display indicates start of test
        if ats.check( vats_json.html_waitDisplayContains( 'Menu 2', AppName = 'vats-demo' ), 'Test start failed!!!' ):

            # search for 'Test item' and get the menu name of the first appearance in any menu
            menuName = vats_json.get_gui_menu_name( 'Test item' )
            ats.check( ( 'testmenu1' == menuName ), 'Internal menu name check failed!' )

            # search for 'Test item' and get the menu name of the fourth appearance in any menu
            menuName = vats_json.get_gui_menu_name( 'Test item', 4 )
            ats.check( ( 'testmenu2' == menuName ), 'Internal menu name check failed!' )

            # search for 'Test item' and get the action of the second appearance in any menu
            menuAction = vats_json.get_gui_menu_item_action( 'Test item', None, 2 )
            ats.check( ( 'return 2' == menuAction ), 'Menu item action check failed!' )

            # search for 'Test item' and get the action of the second appearance in testmenu2 menu
            menuAction = vats_json.get_gui_menu_item_action( 'Test item', menuName, 2 )
            ats.check( ( 'return 8' == menuAction ), 'Menu item action check failed!' )

            # select the third 'Test item' menu item
            vats_json.html_gui_selectMenu( 'Test item', None, 3 )
            ats.check( vats_json.html_waitDisplayContains( "Third 'Test item' in second", response_time = ( 3 * default_menu_wait ), AppName = 'vats-demo' ), 'Menu select failed!!!' )

        #---------------------------------------------------------------------------------------------------
        if( ats.check( vats_json.html_waitDisplayContains( user_menu_title, AppName = 'vats-demo' ), 'Back to "User input" menu failed!!!' ) ):

            # Choose menu item "Multiple menus" from menu again
            myMenu = vats_json.html_gui_selectMenu( 'Multiple menus' )

            # Wait until GUI display indicates restart of test
            if ats.check( vats_json.html_waitDisplayContains( 'Menu 2', AppName = 'vats-demo' ), 'Test start failed!!!' ):

                # search for 'Who am I' and get the menu name of the first appearance in any menu
                menuName = vats_json.get_gui_menu_name( 'Who am I', 1 )
                ats.check( ( 'testmenu1' == menuName ), 'Internal menu name check failed!' )

                # search for 'Who am I' and get the menu name of the second appearance in any menu
                menuName = vats_json.get_gui_menu_name( 'Who am I', 2 )
                ats.check( ( 'testmenu2' == menuName ), 'Internal menu name check failed!' )

                # search for 'Who am I' and get the action of the second appearance in any menu
                menuAction = vats_json.get_gui_menu_item_action( 'Who am I', None, 2 )
                ats.check( ( 'return 6' == menuAction ), 'Menu item action check failed!' )

                # search for 'Who am I' and get the action of the first appearance in testmenu2 menu
                menuAction = vats_json.get_gui_menu_item_action( 'Who am I', menuName )
                ats.check( ( 'return 6' == menuAction ), 'Menu item action check failed!' )

                # select the first 'Other item' menu item in testmenu2 menu
                vats_json.html_gui_selectMenu( 'Other item', menuName )
                ats.check( vats_json.html_waitDisplayContains( "Second 'Other item' in second", response_time = ( 3 * default_menu_wait ), AppName = 'vats-demo' ), 'Menu select failed!!!' )

    # back to main menu
    gui_back_to_main_menu()

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.3" )
ats.set_script_summary( "GUI multiple menu testing" )
ats.set_recovery_function( gui_back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( gui_multiple_menu_1, "GUI multiple menu test 1" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
