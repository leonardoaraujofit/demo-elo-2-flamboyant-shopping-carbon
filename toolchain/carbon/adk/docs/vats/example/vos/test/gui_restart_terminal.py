#----------------------------------------------------------------------------
#
#    File : gui_restart_terminal.py
#
#    VATS demo : Restart terminal
#
#    Author : Achim Groennert
#
#    Creation date : 2013-May
#
#    Description:
'''Test module: GUI restart terminal test'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *
from vats_html import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def gui_restart_terminal_1():
    '''GUI restart terminal test, function 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = gui_prepare_test()

    # Choose menu item 1: "Miscellaneous"
    myMenu = vats_json.html_gui_selectMenu( 'Miscellaneous' )
    # Wait until headline indicates "Miscellaneous Menu"
    user.wait_display_contains( misc_menu_title_gui )

    # restart terminal
    ats.log_info( "restarting terminal now..." )
    user.restart_terminal()

    # Check that terminal returns to idle screen
    ats.check( vats_json.html_waitDisplayContains( main_menu_title, response_time = 420000, AppName = 'vats-demo' ), "html_waitDisplayContains() failed!" )

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.6" )
ats.set_script_summary( "GUI restart terminal testing" )
ats.set_recovery_function( gui_back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( gui_restart_terminal_1, "GUI restart terminal test 1" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
