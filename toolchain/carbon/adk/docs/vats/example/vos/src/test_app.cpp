/*****************************************************************************/
/**
 * \file test_app.cpp
 *
 * \author GSS R&D Germany
 *
 * Company: VeriFone GmbH
 *
 * Product: Verifone Automated Test System (VATS)
 *
 *       (c) Copyright 2014
 */
/*****************************************************************************/


#ifdef _VOS

#ifndef _VOS2
#define _EMV
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <libvoy.h>
#include <svc.h>
#include <errno.h>
#include <svcsec.h>                     // pin entry
#include "atsmisc.h"
#include "atslib.h"
#include "gui.h"
#include "fcntl.h"                      // O_NONBLOCK
#include "msrDevice.h"                  // magstripe data
#include "prt.h"                        // HTML printer
// #include "XHelperDemoHWDependencies.h"
#include <time.h>                       // time_t, struct tm, time, localtime, strftime
extern "C" {
#include <platforminfo_api.h>
#include <buzzer.h>                     // buzzer, libos.a
}

#include "test_app_emv.h"               // ADKVATS-215
#include "all_test_apps.h"

#ifdef _VOS2
#include <sys/stat.h>
#include <unistd.h>
#endif  // #ifdef _VOS2


#define __MODULE__                 __FILE__            // due to vDebugOutVats
#define APPNAME                    "TESTAPP"
#define EMV_PIN                    0x0A


#define MSR_T1_MAX_LEN 79
#define MSR_T2_MAX_LEN 40
#define MSR_T3_MAX_LEN 107


enum ATS_CARD_STATE
{
  CARD_ABSENT = 0,
  CARD_PRESENT
};


#ifndef MIN
#define MIN(a,b) ((a)<(b)?(a):(b))
#endif
#ifndef POST_PROC_PIN
#define POST_PROC_PIN 0x0B
#endif
#define MAXAPPS                       EMV_ADK_MAX_AIDSUPP // max number of applications
#define UI_PIN_TIMEOUT                30000               // Enter PIN dialog timeout in msec

#define UC_READER_0                   0

#define PRINTER_LINE_LENGTH           24

#define EMV_TRACE_MAX_LINE_LEN        238


enum IccState
{
  ICC_NONE,
  ICC_IN,
  ICC_OUT
};


struct customEventDisplay
{
  char *line1;
  char *line2;
  char *line3;
  char *line4;
  char *line5;
  char *line6;
  char *line7;
  char *line8;
  int   lineLength;
};


static IccState cardInStatus        = ICC_NONE;
static int      physicalCardIn      = 0;
static int      printerAvailable    = 999;

static char     magDataBuffer [500] = {0};
static int      magDataBufferLen    = 0;

static int      magCard             = 0;
static int      ErrNo               = -1;
static bool     msrDataAvailable    = false;
static MSR_DATA msrDataInsert;
static char     msrData[ (MSR_T1_MAX_LEN + MSR_T2_MAX_LEN + MSR_T3_MAX_LEN + 3) + 200 ];

static bool doMagDataTest = false;  // ADKVATS-164
static bool doPINTest     = false;  // ADKVATS-164
static bool doCardinTest  = false;  // ADKVATS-164

static const int mainMenuItems[] = {
  1,
  2,
  3,
  4,
  5,
  0                               // this item has to remain the last one, as this value is used to mark the end of the array
};

static const int miscMenuItems[] = {
  VATS_GUI_TEST_ID_CUSTOM1,
  VATS_GUI_TEST_ID_CUSTOM2,
  VATS_GUI_TEST_ID_TRACE1,
  VATS_GUI_TEST_ID_DATE_TIME1,
  VATS_GUI_TEST_ID_BUZZER,
  VATS_GUI_TEST_ID_UNIT_TEST,
  VATS_GUI_TEST_ID_NONE           // this item has to remain the last one, as this value is used to mark the end of the array
};

static const int dispMenuItems[] = {
  VATS_GUI_TEST_ID_DISPLAY1,
  VATS_GUI_TEST_ID_DISPLAY2,
  VATS_GUI_TEST_ID_DISPLAY3,
  VATS_GUI_TEST_ID_NONE           // this item has to remain the last one, as this value is used to mark the end of the array
};

static const int userMenuItems[] = {
  VATS_GUI_TEST_ID_KEYBOARD1,
  VATS_GUI_TEST_ID_KEYBOARD2,
  VATS_GUI_TEST_ID_KEYBOARD3,
  VATS_GUI_TEST_ID_TOUCH1,
  VATS_GUI_TEST_ID_MULTI_MENU1,
  VATS_GUI_TEST_ID_NONE           // this item has to remain the last one, as this value is used to mark the end of the array
};

static const int printMenuItems[] = {
  VATS_GUI_TEST_ID_PRINTER1,
  VATS_GUI_TEST_ID_NONE           // this item has to remain the last one, as this value is used to mark the end of the array
};

static const int cardMenuItems[] = {
  VATS_GUI_TEST_ID_ICC_CARDIN1,
  VATS_GUI_TEST_ID_MSR1,
  VATS_GUI_TEST_ID_MSR2,
  VATS_GUI_TEST_ID_EMV1,
  VATS_GUI_TEST_ID_NONE           // this item has to remain the last one, as this value is used to mark the end of the array
};

static const int tutMenuItems[] = {
  VATS_GUI_TEST_ID_UNIT_MAGDATA,
  VATS_GUI_TEST_ID_UNIT_PININPUT,
  VATS_GUI_TEST_ID_UNIT_CARDIN,
  VATS_GUI_TEST_ID_NONE           // this item has to remain the last one, as this value is used to mark the end of the array
};

void menu_terminal_unit_test (void);

using namespace std;
using namespace vfigui;


// =================================================================================================
// read manufacturing block in order to determine if a printer is available
int PrinterAvailable( void )
{
  if( 999 == printerAvailable )
  {
    struct stat dummy;


    if( 0 == stat( "/dev/lp0", &dummy ) )
    {
      printerAvailable = 1;
    }
    else
    {
      printerAvailable = 0;
    }
    if( 0 == printerAvailable )
    {
      int                myResult;
      unsigned long      myLength;
      PI_printer_info_st myPrinterInfo;


      myLength = 0;
      memset( &myPrinterInfo, 0, sizeof( myPrinterInfo ) );
      myResult = platforminfo_get( PI_PRINTER_INFO, &myPrinterInfo, sizeof( myPrinterInfo ), &myLength );
      if( ( ( PI_OK == myResult ) || ( PI_OK_LAST_ENTRY == myResult ) ) && ( sizeof( myPrinterInfo ) >= myLength ) && ( 0 < myLength ) )
      {
        printerAvailable = myPrinterInfo.exist;
      }
    }

#ifdef _VOS2
    if (0 == printerAvailable)
    {
      int           i;
      unsigned long rsize;
      char          model [PI_CIB_MODEL_ID_SIZE];


      if ( PI_OK == platforminfo_get(PI_CIB_MODEL_ID_STR, model, sizeof(model), &rsize) )
      {
        for (i=0; model [i] && model [i] != ' '; i++);
        model [i] = '\0';
        VatsLogging (VATS_LOG_INFO, __MODULE__, __LINE__, "VATS Handler: PrinterAvailable: original: model=%s\n", model);
        if ( (0 == strncmp (model, "X10", strlen ("X10") )) || (0 == strncmp (model, "SF-PAY", strlen ("SF-PAY") )) )
        {
          printerAvailable = 1;
        }
      }
    }
#endif
  }
  return printerAvailable;
}


// =================================================================================================
void terminal_info( void )
{
  PI_display_info_st      myDisplayInfo;
  PI_ethernet_info_st     myEthernetInfo;
  PI_printer_info_st      myPrinterInfo;
  PI_modem_info_st        myModemInfo;
  PI_msr_info_st          myMsrInfo;
  PI_touch_screen_info_st myTouchInfo;
  PI_ctls_info_st         myCtlsInfo;
  PI_smart_card_info_st   mySmartCardInfo;
  PI_wrenchman_info_st    myWrenchmanInfo;
  PI_keypad_info_st       myKeypadInfo;
  PI_gprs_info_st         myGprsInfo;
  int                     myResult;
  unsigned long           myLength;


  myLength = 0;
  memset( &myDisplayInfo, 0, sizeof( myDisplayInfo ) );
  myResult = platforminfo_get( PI_DISPLAY_INFO, &myDisplayInfo, sizeof( myDisplayInfo ), &myLength );
  if( ( ( PI_OK == myResult ) || ( PI_OK_LAST_ENTRY == myResult ) ) && ( sizeof( myDisplayInfo ) >= myLength ) && ( 0 < myLength ) )
  {
    uiConfirm ("confirm-cancel", uiPrint( "<h4>Display</h4>data length %d<br>color %d<br>dimensions %dx%d<br>type %d<br>'%s'<br>IF type %d<input type='timeout' value='5' style='visibility:hidden' action='return 500'>",
                        myLength, myDisplayInfo.color, myDisplayInfo.pixels_per_row, myDisplayInfo.pixels_per_col, myDisplayInfo.type, myDisplayInfo.name, myDisplayInfo.interface_type ) );
  }
  else
  {
    uiConfirm ("confirm-cancel", uiPrint( "<h4>Display</h4>failed.<br>error code %d<br>data length %d<input type='timeout' value='5' style='visibility:hidden' action='return 501'>", myResult, myLength ) );
  }
  myLength = 0;
  memset( &myEthernetInfo, 0, sizeof( myEthernetInfo ) );
  myResult = platforminfo_get( PI_ETHERNET_INFO, &myEthernetInfo, sizeof( myEthernetInfo ), &myLength );
  if( ( ( PI_OK == myResult ) || ( PI_OK_LAST_ENTRY == myResult ) ) && ( sizeof( myEthernetInfo ) >= myLength ) && ( 0 < myLength ) )
  {
    uiConfirm ("confirm-cancel", uiPrint( "<h4>Ethernet</h4>data length %d<br>exist %d<br>type %d<br>link up %d<br>'%s'<input type='timeout' value='5' style='visibility:hidden' action='return 502'>",
                        myLength, myEthernetInfo.exist, myEthernetInfo.type, myEthernetInfo.link_up, myEthernetInfo.mac ) );
  }
  else
  {
    uiConfirm ("confirm-cancel", uiPrint( "<h4>Ethernet</h4>failed.<br>error code %d<br>data length %d<input type='timeout' value='5' style='visibility:hidden' action='return 503'>", myResult, myLength ) );
  }
  myLength = 0;
  memset( &myPrinterInfo, 0, sizeof( myPrinterInfo ) );
  myResult = platforminfo_get( PI_PRINTER_INFO, &myPrinterInfo, sizeof( myPrinterInfo ), &myLength );
  if( ( ( PI_OK == myResult ) || ( PI_OK_LAST_ENTRY == myResult ) ) && ( sizeof( myPrinterInfo ) >= myLength ) && ( 0 < myLength ) )
  {
    uiConfirm ("confirm-cancel", uiPrint( "<h4>Printer</h4>data length %d<br>exist %d<br>type %d<input type='timeout' value='5' style='visibility:hidden' action='return 504'>",
                        myLength, myPrinterInfo.exist, myPrinterInfo.type ) );
  }
  else
  {
    uiConfirm ("confirm-cancel", uiPrint( "<h4>Printer</h4>failed.<br>error code %d<br>data length %d<input type='timeout' value='5' style='visibility:hidden' action='return 505'>", myResult, myLength ) );
  }
  myLength = 0;
  memset( &myModemInfo, 0, sizeof( myModemInfo ) );
  myResult = platforminfo_get( PI_MODEM_INFO, &myModemInfo, sizeof( myModemInfo ), &myLength );
  if( ( ( PI_OK == myResult ) || ( PI_OK_LAST_ENTRY == myResult ) ) && ( sizeof( myModemInfo ) >= myLength ) && ( 0 < myLength ) )
  {
    uiConfirm ("confirm-cancel", uiPrint( "<h4>Modem</h4>data length %d<br>exist %d<br>type %d<br>port %d<input type='timeout' value='5' style='visibility:hidden' action='return 506'>",
                        myLength, myModemInfo.exist, myModemInfo.type, myModemInfo.port ) );
  }
  else
  {
    uiConfirm ("confirm-cancel", uiPrint( "<h4>Modem</h4>failed.<br>error code %d<br>data length %d<input type='timeout' value='5' style='visibility:hidden' action='return 507'>", myResult, myLength ) );
  }
  myLength = 0;
  memset( &myMsrInfo, 0, sizeof( myMsrInfo ) );
  myResult = platforminfo_get( PI_MSR_INFO, &myMsrInfo, sizeof( myMsrInfo ), &myLength );
  if( ( ( PI_OK == myResult ) || ( PI_OK_LAST_ENTRY == myResult ) ) && ( sizeof( myMsrInfo ) >= myLength ) && ( 0 < myLength ) )
  {
    uiConfirm ("confirm-cancel", uiPrint( "<h4>Msr</h4>data length %d<br>exist %d<br>type %d<br>port %d<input type='timeout' value='5' style='visibility:hidden' action='return 508'>",
                        myLength, myMsrInfo.exist, myMsrInfo.type, myMsrInfo.port ) );
  }
  else
  {
    uiConfirm ("confirm-cancel", uiPrint( "<h4>Msr</h4>failed.<br>error code %d<br>data length %d<input type='timeout' value='5' style='visibility:hidden' action='return 509'>", myResult, myLength ) );
  }
  myLength = 0;
  memset( &myTouchInfo, 0, sizeof( myTouchInfo ) );
  myResult = platforminfo_get( PI_TOUCH_SCREEN_INFO, &myTouchInfo, sizeof( myTouchInfo ), &myLength );
  if( ( ( PI_OK == myResult ) || ( PI_OK_LAST_ENTRY == myResult ) ) && ( sizeof( myTouchInfo ) >= myLength ) && ( 0 < myLength ) )
  {
    uiConfirm ("confirm-cancel", uiPrint( "<h4>Touch</h4>data length %d<br>exist %d<br>type %d<br>port %d<input type='timeout' value='5' style='visibility:hidden' action='return 510'>",
                        myLength, myTouchInfo.exist, myTouchInfo.type, myTouchInfo.port ) );
  }
  else
  {
    uiConfirm ("confirm-cancel", uiPrint( "<h4>Touch</h4>failed.<br>error code %d<br>data length %d<input type='timeout' value='5' style='visibility:hidden' action='return 511'>", myResult, myLength ) );
  }
  myLength = 0;
  memset( &myCtlsInfo, 0, sizeof( myCtlsInfo ) );
  myResult = platforminfo_get( PI_CTLS_INFO, &myCtlsInfo, sizeof( myCtlsInfo ), &myLength );
  if( ( ( PI_OK == myResult ) || ( PI_OK_LAST_ENTRY == myResult ) ) && ( sizeof( myCtlsInfo ) >= myLength ) && ( 0 < myLength ) )
  {
    uiConfirm ("confirm-cancel", uiPrint( "<h4>CTLS</h4>data length %d<br>exist %d<br>type %d<br>port %d<br> config ID %d<input type='timeout' value='5' style='visibility:hidden' action='return 512'>",
                        myLength, myCtlsInfo.exist, myCtlsInfo.type, myCtlsInfo.port, myCtlsInfo.config_id ) );
  }
  else
  {
    uiConfirm ("confirm-cancel", uiPrint( "<h4>CTLS</h4>failed.<br>error code %d<br>data length %d<input type='timeout' value='5' style='visibility:hidden' action='return 513'>", myResult, myLength ) );
  }
  myLength = 0;
  memset( &mySmartCardInfo, 0, sizeof( mySmartCardInfo ) );
  myResult = platforminfo_get( PI_SMART_CARD_INFO, &mySmartCardInfo, sizeof( mySmartCardInfo ), &myLength );
  if( ( ( PI_OK == myResult ) || ( PI_OK_LAST_ENTRY == myResult ) ) && ( sizeof( mySmartCardInfo ) >= myLength ) && ( 0 < myLength ) )
  {
    uiConfirm ("confirm-cancel", uiPrint( "<h4>Smart card</h4>data length %d<br>exist %d<br>type %d<br>port %d<input type='timeout' value='5' style='visibility:hidden' action='return 514'>",
                        myLength, mySmartCardInfo.exist, mySmartCardInfo.type, mySmartCardInfo.port ) );
  }
  else
  {
    uiConfirm ("confirm-cancel", uiPrint( "<h4>Smart card</h4>failed.<br>error code %d<br>data length %d<input type='timeout' value='5' style='visibility:hidden' action='return 515'>", myResult, myLength ) );
  }
  myLength = 0;
  memset( &myWrenchmanInfo, 0, sizeof( myWrenchmanInfo ) );
  myResult = platforminfo_get( PI_WRENCHMAN_INFO, &myWrenchmanInfo, sizeof( myWrenchmanInfo ), &myLength );
  if( ( ( PI_OK == myResult ) || ( PI_OK_LAST_ENTRY == myResult ) ) && ( sizeof( myWrenchmanInfo ) >= myLength ) && ( 0 < myLength ) )
  {
    uiConfirm ("confirm-cancel", uiPrint( "<h4>Wrenchman</h4>data length %d<br>exist %d<br>type %d<br>port %d<input type='timeout' value='5' style='visibility:hidden' action='return 516'>",
                        myLength, myWrenchmanInfo.exist, myWrenchmanInfo.type, myWrenchmanInfo.port ) );
  }
  else
  {
    uiConfirm ("confirm-cancel", uiPrint( "<h4>Wrenchman</h4>failed.<br>error code %d<br>data length %d<input type='timeout' value='5' style='visibility:hidden' action='return 517'>", myResult, myLength ) );
  }
  myLength = 0;
  memset( &myKeypadInfo, 0, sizeof( myKeypadInfo ) );
  myResult = platforminfo_get( PI_KEYPAD_INFO, &myKeypadInfo, sizeof( myKeypadInfo ), &myLength );
  if( ( ( PI_OK == myResult ) || ( PI_OK_LAST_ENTRY == myResult ) ) && ( sizeof( myKeypadInfo ) >= myLength ) && ( 0 < myLength ) )
  {
    uiConfirm ("confirm-cancel", uiPrint( "<h4>Keypad</h4>data length %d<br>exist %d<br>type %d<br>port %d<br>standard keys %d<br>keypad exist %d<input type='timeout' value='5' style='visibility:hidden' action='return 518'>",
                        myLength, myKeypadInfo.exist, myKeypadInfo.type, myKeypadInfo.port, myKeypadInfo.num_standard_keys, myKeypadInfo.func_keypad_exist ) );
  }
  else
  {
    uiConfirm ("confirm-cancel", uiPrint( "<h4>Keypad</h4>failed.<br>error code %d<br>data length %d<input type='timeout' value='5' style='visibility:hidden' action='return 519'>", myResult, myLength ) );
  }
  myLength = 0;
  memset( &myGprsInfo, 0, sizeof( myGprsInfo ) );
  myResult = platforminfo_get( PI_GPRS_INFO, &myGprsInfo, sizeof( myGprsInfo ), &myLength );
  if( ( ( PI_OK == myResult ) || ( PI_OK_LAST_ENTRY == myResult ) ) && ( sizeof( myGprsInfo ) >= myLength ) && ( 0 < myLength ) )
  {
    uiConfirm ("confirm-cancel", uiPrint( "<h4>GPRS</h4>data length %d<br>exist %d<br>type %d<br>port %d<br>ctrl ID %d<input type='timeout' value='5' style='visibility:hidden' action='return 520'>",
                        myLength, myGprsInfo.exist, myGprsInfo.type, myGprsInfo.port, myGprsInfo.ctrl_id ) );
  }
  else
  {
    uiConfirm ("confirm-cancel", uiPrint( "<h4>GPRS</h4>failed.<br>error code %d<br>data length %d<input type='timeout' value='5' style='visibility:hidden' action='return 521'>", myResult, myLength ) );
  }
} // terminal_info


// =================================================================================================
int touchScreenAvailable( void )
{
  int devices = 0;
  int retVal  = 0;


  devices = svcInfoPlatform( PI_LEGACY_UNIT_CONFIG );

  if( ( devices & PI_LEGACY_UNIT_CONFIG_TOUCHPANEL ) != 0 )
  {
    retVal = 1;
  }

  return retVal;
}


// =================================================================================================
void display_test (void)
{
  vDebugOutVats( __MODULE__, __LINE__, "test_app: display_test called\n" );
  uiConfirm ("confirm-cancel", "VATS test<br>&nbsp;VATS test<br>&nbsp;&nbsp;VATS test<br>&nbsp;&nbsp;&nbsp;VATS test<br>&nbsp;&nbsp;&nbsp;&nbsp;VATS test<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VATS test<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VATS test<input type='timeout' value='3' style='visibility:hidden' action='return 100'>");
}


// =================================================================================================
void keyboard_test (void)
{
  int r = UI_ERR_OK;
  vector<string> value(1);


  vDebugOutVats( __MODULE__, __LINE__, "test_app: keyboard_test called\n" );

  while( UI_ERR_ABORT != r )
  {
    value[0] = "1";
    r = uiInput ("input", value, "<h4>Input test</h4>Enter amount <input type='number' precision='0' maxlength='3'><br>CANCEL to abort");

    vDebugOutVats( __MODULE__, __LINE__, "test_app: keyboard_test: r=%d\n", r );
    if (r == UI_ERR_OK)
    {
      uiDisplay (string("<h4>Input test</h4>Amount:<br><input type='timeout' value='2' style='visibility:hidden' action='return 200'>")+value[0]);
    }
  }
} // keyboard_test


// =================================================================================================
unsigned long SmartOpen (unsigned char ucReaderNum)
{
  unsigned long res = 1; // IFD_Failure
  unsigned char CardBuffer [64];


  /* ADKVATS-313: unused parameter */
  ucReaderNum = ucReaderNum;

  // convert to Big Endian
  CardBuffer[0] = 0;
  CardBuffer[1] = 0;
  CardBuffer[2] = 0;
  CardBuffer[3] = CUSTOMER_CARD;

  // open the icc
  res = IFD_Set_Capabilities (Tag_Open_ICC, CardBuffer);
  if (res != 0)
    vDebugOutVats( __MODULE__, __LINE__, "test_app: SmartOpen: Tag_Open_ICC: ERROR: res=%ld\n", res );

  return res;
} // SmartOpen


// =================================================================================================
unsigned long SmartCardPresent (unsigned char ucReaderNum)
{
  unsigned long res;
  unsigned char CardBuffer [64];


  /* ADKVATS-313: unused parameter */
  ucReaderNum = ucReaderNum;

  CardBuffer[0] = 0; // convert to Big Endian
  CardBuffer[1] = 0;
  CardBuffer[2] = 0;
  CardBuffer[3] = CUSTOMER_CARD;

  res = IFD_Set_Capabilities (Tag_Select_ICC, CardBuffer);
  if (res != 0)
    vDebugOutVats( __MODULE__, __LINE__, "  test_app: SmartCardPresent: IFD_Set_Capabilities: res=%ld\n", res );

  res = IFD_Is_ICC_Present ();
  (res == IFD_Success) ? (res = 1) : (res = 0);
  return res;
} // SmartCardPresent


// =================================================================================================
// callback function for uiConfirm () that handles a cardin event or a physically inserted/removed
// card
bool cb_cardin (void *)
{
  int           iRet;
  enum AtsEvent ats_event = ATSEVENT_NONE;
  char          dataBuf [500] = {0};
  int           datalen   = sizeof (dataBuf);


  if ( ATSERROR_OK == AtsReceiveEvent (&ats_event, 0, &datalen, (void*) dataBuf) )
  {
    vDebugOutVats( __MODULE__, __LINE__, "     test_app: cb_cardin: AtsReceiveEvent: ats_event=%d dataBuf[0]=%d\n", ats_event, dataBuf [0] );
    if (ats_event == ATSEVENT_USER_ICCCARD_IN)
    {
      if (dataBuf [0] == 1)
        cardInStatus = ICC_IN;
      else
        cardInStatus = ICC_OUT;
      return false; // terminate uiConfirm ()
    }
    else
    {
      vDebugOutVats( __MODULE__, __LINE__, "     test_app: cb_cardin: AtsReceiveEvent: E R R O R\n" );
    }
  }

  // examine physical card_in
  iRet = SmartCardPresent (CUSTOMER_CARD);
  if (iRet != physicalCardIn)
  {
    vDebugOutVats( __MODULE__, __LINE__, "     test_app: cb_cardin: physical card detected\n" );
    physicalCardIn = iRet;
    return false; // terminate uiConfirm ()
  }
  return true;    // continue uiConfirm ()
} // cb_cardin


// =================================================================================================
int prepareIccDisplay (IccState iccStat, int *smartOpen)
{
  int  iRet;
  char lines [7][21]; // VX520


  vDebugOutVats( __MODULE__, __LINE__, "  test_app: prepareIccDisplay\n" );
  memset (lines, 0x00, sizeof (lines));
  sprintf (lines [0], "Card in test");
  if (AtsIsActive())
    sprintf (lines [1], "VATS active");
  else
    sprintf (lines [1], "VATS inactive");

  if (iccStat == ICC_IN)
    sprintf (lines [2], "VATS cardin: in");
  else if (iccStat == ICC_OUT)
    sprintf (lines [2], "VATS cardin: out");
  else
    sprintf (lines [2], "VATS cardin: ???");

  if (*smartOpen == 0) // open ICC
  {
    iRet = SmartOpen (1);
    if (iRet == 0)
    {
      sprintf (lines [3], "Phys. init:   ok");
      *smartOpen = iRet;
    }
    else
    {
      sprintf (lines [3], "Phys. init: %d", iRet);
      *smartOpen = iRet;
    }
  }
  else
  {
    sprintf (lines [3], "Phys. init:   ok");
  }

  physicalCardIn = SmartCardPresent (CUSTOMER_CARD);
  if (physicalCardIn == CARD_PRESENT)
    sprintf (lines [4], "Phys. cardin: in");
  else if (physicalCardIn == CARD_ABSENT)
    sprintf (lines [4], "Phys. cardin: out");
  else
  {
    vDebugOutVats( __MODULE__, __LINE__, "  test_app: prepareIccDisplay: Get_Card_State 1: %d\n", physicalCardIn );
    sprintf (lines [4], "Phys. cardin: error");
  }

  sprintf (lines [5], "Please insert card");
  sprintf (lines [6], "CANCEL to abort");

  iRet = uiConfirm ("confirm-cancel", uiPrint ("%s<br>%s<br>%s<br>%s<br>%s<br>%s<br>%s",
                                      lines [0], lines [1], lines [2], lines [3], lines [4], lines [5], lines [6]), cb_cardin );

  vDebugOutVats( __MODULE__, __LINE__, "  test_app: prepareIccDisplay: DONE\n" );
  return iRet;
} // prepareIccDisplay


// =================================================================================================
void cardin_test (void)
{
  int  smartOpen = 0;
  int  r         = UI_ERR_OK;


  vDebugOutVats( __MODULE__, __LINE__, "  test_app: cardin_test\n" );

  while( UI_ERR_ABORT != r )
  {
    r = prepareIccDisplay (cardInStatus, &smartOpen);
  } // while
} // cardin_test


// =================================================================================================
bool cb_magdata (void *)
{
  int           res;
  enum AtsEvent ats_event = ATSEVENT_NONE;


  magDataBufferLen = 0;
  // handle physical mag data
  memset (magDataBuffer, 0x00, sizeof (magDataBuffer));
  res = msrRead (magDataBuffer, sizeof (magDataBuffer));
  if (res < 0)
  {
    vDebugOutVats( __MODULE__, __LINE__, "  test_app: cb_magdata: msrRead: ERROR: res=%d\n", res );
  }

  if (res > 0)
  {
    vDebugOutVats( __MODULE__, __LINE__, "  test_app: cb_magdata: msrRead: physical data available: res=%d\n", res );
    magDataBufferLen = res;
    return false;   // terminate uiConfirm ()
  }

  magDataBufferLen = sizeof (magDataBuffer);
  if ( ATSERROR_OK == AtsReceiveEvent (&ats_event, 0, &magDataBufferLen, (void*) magDataBuffer) ) // timeout = 0
  {
    if (ats_event == ATSEVENT_USER_MAGSTRIPE)
    {
      vDebugOutVats( __MODULE__, __LINE__, "  test_app: cb_magdata: AtsReceiveEvent: ats_event=%d magDataBufferLen=%d\n", ats_event, magDataBufferLen );
      return false;   // terminate uiConfirm ()
    }
  }

  return true;    // continue uiConfirm ()
} // cb_magdata


// =================================================================================================
int DisplayMagData2 (char *dataBuf, int dataLen, int *swipeCount)
{
  int ret;
  char lines [7][21]; // VX520


  vDebugOutVats( __MODULE__, __LINE__, "  test_app: DisplayMagData2: dataLen=%d\n", dataLen );

  memset (lines, 0x00, sizeof (lines));
  sprintf (lines [0], "Magstripe test");

  if (dataLen > 0)
  {
    (*swipeCount)++;
  }
  sprintf (lines [1], "No. of swipes: %3d", *swipeCount);
  sprintf (lines [6], "CANCEL to abort");

  errno = 0;
  if (dataLen > 0)
  {
    int i,j;


    sprintf (lines [2], "Total Bytes: %4d", dataLen);
    for (i = 1, j = 0; i <= 3; i++) // i: line, j: bytes
    {
      // vDebugOutVats( __MODULE__, __LINE__, "  test_app: DisplayMagData2: i=%d j=%d <%s>\n", i, j, dataBuf );
      // size1  state1 (data1) size2  state2 (data2) size3  state3 (data3)
      sprintf ( lines [2+i], "TK#%d:%03d %02X-%.8s", i,
                ((dataBuf[j] > 0) ? (dataBuf[j] - 2) : 0),
                dataBuf[j + 1],
                ((dataBuf[j + 1] >= MAX_ERR) ? "UNKNOWN" : VATScardErrTbl[int (dataBuf[j + 1])]) );
      j += dataBuf[j]; // increase j by size of track data
    }

    vDebugOutVats( __MODULE__, __LINE__, "  test_app: DisplayMagData2: Magdata: %s %s %s\n", lines [3], lines [4], lines [5]);
    ret = uiConfirm ( "confirm-cancel", uiPrint ("%s<br>%s<br>%s<br>%s<br>%s<br>%s<br>%s",
                                        lines [0], lines [1], lines [2], lines [3], lines [4], lines [5], lines [6]), cb_magdata);
  }
  else
  {
    sprintf (lines [2], "ERR: %-3d / %-3d", dataLen, errno);
    ret = uiConfirm ( "confirm-cancel", uiPrint ("%s<br>%s<br>%s<br><br><br><br>%s", lines [0], lines [1], lines [2], lines [6]), cb_magdata);
  }
  return ret;
} // DisplayMagData2


// =================================================================================================
void magstripe_test (void)
{
  int  res;
  int  swipeCount = 0;


  vDebugOutVats( __MODULE__, __LINE__, "  test_app: magstripe_test\n" );
  res = msrOpen( O_NONBLOCK, NULL );    // do not block msr operations and use callbacks

  if( res < 0 )
  {
    vDebugOutVats( __MODULE__, __LINE__, "  test_app: magstripe_test: Can't open //DEV//MAG (error %d)\n", res );
    return;
  }
  vDebugOutVats( __MODULE__, __LINE__, "  test_app: magstripe_test: Opened //DEV//MAG (result %d)\n", res );

  // dummy read before test start (in the case the ICC card in test with hybrid functions and empty MSR data was already performed)
  magDataBufferLen = (int)cb_magdata( NULL );
  // reset the variables
  magDataBufferLen = 0;
  memset (magDataBuffer, 0x00, sizeof (magDataBuffer));

  res = UI_ERR_OK;
  while( UI_ERR_ABORT != res )
  {
    vDebugOutVats( __MODULE__, __LINE__, "  test_app: magstripe_test: magDataBufferLen=%d\n", magDataBufferLen );
    res = DisplayMagData2 (magDataBuffer, magDataBufferLen, &swipeCount);
    svcSleep (100);
  } // while
  msrClose ();
} // magstripe_test


// =================================================================================================
void trace_event_test( void )
{
  char  txtTrace[]   = "VATS trace testing!";
  char *trc;
  char  txtPattern[] = "test ...";
  int   patternSize  = strlen( txtPattern );
  int   siz          = ( (PIPE_MAX_MSG_LEN - VATS_PIPE_OVERHEAD) / ( patternSize + 2 ) ) * patternSize;     // don't exceed max. pipe message length, and have a multiple of patternSize
  int   i            = 0;


  vDebugOutVats( __MODULE__, __LINE__, "  test_app: trace_event_test\n" );

  // 1st trace: very short
  AtsTraceEvent( /*source=*/7, /*channel=*/8, strlen( txtTrace ), txtTrace );

  // 2nd trace: very long, will be chained on PC interface
  trc = (char *)malloc( siz );
  if( NULL != trc )
  {
    for( i = 0; i < ( siz / patternSize ); i++ )
    {
      memcpy( (trc + ( i * patternSize ) ), txtPattern, patternSize );
    }
    AtsTraceEvent( /*source=*/5, /*channel=*/6, siz, trc );
    free( trc );
    trc = NULL;
  }
  uiConfirm( "confirm-cancel", "<h4>Trace</h4>Trace messages have been sent to PC.<input type='timeout' value='3' style='visibility:hidden' action='return 600'>" );
} // trace_event_test


// =================================================================================================
void screen_test (void)
{
  vDebugOutVats( __MODULE__, __LINE__, "  test_app: screen_test: called\n" );
  uiConfirm ("confirm-cancel", "<h4>Display</h4>1VATS Screen 123456782<br>VATS Screen 123456783<br>VATS Screen 123456784<br>VATS Screen 123456785<br>VATS Screen 123456786<br>VATS Screen 123456787<br>VATS Screen 12345678<br>8VATS Screen 12345678<input type='timeout' value='3' style='visibility:hidden' action='return 300'>");
} // screen_test


// =================================================================================================
void custom_event_test (void)
{
  // send a custom event
  char dat[] = "Hello World! \x01\x02\x03\x04";


  vDebugOutVats( __MODULE__, __LINE__, "  test_app: custom_event_test\n" );
  AtsSendEvent( ATSEVENT_CUSTOM_START+1, sizeof (dat), (const void*) dat );
  uiConfirm( "confirm-cancel", "<h4>DUT to PC Test</h4>Custom event has been sent to PC.<input type='timeout' value='3' style='visibility:hidden' action='return 500'>" );
} // custom_event_test


// =================================================================================================
int convertBufferSliceToVisibleStringLine( char *destination, int destinationMaxCharLen, char *source, int sourceMaxLen )
{
  int sourceOffset = 0;


  if( ( NULL != destination ) && ( 0 < destinationMaxCharLen ) && ( NULL != source ) && ( 0 < sourceMaxLen ) )
  {
    int  destinationOffset = 0;
    char nibble            = ' ';


    while( ( destinationMaxCharLen > destinationOffset ) && ( sourceMaxLen > sourceOffset ) )
    {
      if(
          !(
                (
                      ( (signed char)source[sourceOffset] >= 0 )
                  &&  ( source[sourceOffset] <  ' ' )
                )
            ||  ( source[sourceOffset] == 127 )
            ||  ( (signed char)source[sourceOffset] == (-1) )
          )
        )
      {
        destination[destinationOffset++] = source[sourceOffset];
      }
      else if( source[sourceOffset] == '\n' )
      {
        sourceOffset++;
        break;
      }
      else
      {
        if( (destinationMaxCharLen - 2) > destinationOffset )
        {
          destination[destinationOffset++] = '\\';
          nibble = (source[sourceOffset] >> 4) & 0xF;
          destination[destinationOffset++] = (nibble < 10) ? (nibble + '0') : (nibble - 10 + 'A');
          nibble = source[sourceOffset] & 0xF;
          destination[destinationOffset++] = (nibble < 10) ? (nibble + '0') : (nibble - 10 + 'A');
        }
        else
        {
          break;
        }
      }
      sourceOffset++;
    }
    destination[destinationOffset] = '\0';
  }
  return sourceOffset;
} // convertBufferSliceToVisibleStringLine


// =================================================================================================
bool customEvent_callBack( void *pDisplay )
{
  enum AtsEvent       ats_event    = ATSEVENT_NONE;
  char               *eventData    = NULL;
  int                 eventDataLen = 0;
  customEventDisplay *myDisplay    = (customEventDisplay *)pDisplay;


  if( ATSERROR_OK == AtsReceiveEventChained( &ats_event, 0, &eventDataLen, (void **)(&eventData) ) )   // timeout = 0
  {
    if( ( ats_event > ATSEVENT_CUSTOM_START ) && ( ats_event < ATSEVENT_CUSTOM_END ) )
    {
      int   offset  = 0;
      int   myCount = 0;
      int   i       = 0;
      char *buf     = NULL;


      vDebugOutVats( __MODULE__, __LINE__, "test_app: customEvent_callBack: ats_event=%d eventDataLen=%d\n", ats_event, eventDataLen );
      sprintf( myDisplay->line1, "Custom event: %d", ats_event );
      sprintf( myDisplay->line2, "Data length: %d", eventDataLen );
      memset( myDisplay->line3, 0, myDisplay->lineLength );
      memset( myDisplay->line4, 0, myDisplay->lineLength );
      memset( myDisplay->line5, 0, myDisplay->lineLength );
      memset( myDisplay->line6, 0, myDisplay->lineLength );
      memset( myDisplay->line7, 0, myDisplay->lineLength );
      sprintf( myDisplay->line8, "CANCEL to abort" );
      for( i = 3; i <= 7; i++ )
      {
        myCount = 0;
        switch( i )
        {
          case 3:
            buf = myDisplay->line3;
            break;
          case 4:
            buf = myDisplay->line4;
            break;
          case 5:
            buf = myDisplay->line5;
            break;
          case 6:
            buf = myDisplay->line6;
            break;
          case 7:
          default:  /* ADKVATS-313: default case added */
            buf = myDisplay->line7;
            break;
        }
        memset( buf, 0, myDisplay->lineLength );
        myCount = convertBufferSliceToVisibleStringLine( buf, ( myDisplay->lineLength - (myDisplay->lineLength % 2) ), &eventData[offset], (eventDataLen - offset) );
        if( 0 == myCount )
        {
          break;
        }
        offset += myCount;
        if( offset >= eventDataLen )
        {
          break;
        }
      }
      if( NULL != eventData )
      {
        free( eventData );
        eventData = NULL;
      }
      return false;     // terminate uiConfirm ()
    }
  }
  if( NULL != eventData )
  {
    free( eventData );
    eventData = NULL;
  }
  return true;          // continue uiConfirm ()
} // customEvent_callBack


// =================================================================================================
void custom_event_test2( void )
{
  int                ret = UI_ERR_OK;
  char               lines[8][CE2_MAX_LINE_LEN];
  customEventDisplay myDisplay;


  memset( lines, 0x00, sizeof( lines ) );
  myDisplay.line1 = &lines[0][0];
  myDisplay.line2 = &lines[1][0];
  myDisplay.line3 = &lines[2][0];
  myDisplay.line4 = &lines[3][0];
  myDisplay.line5 = &lines[4][0];
  myDisplay.line6 = &lines[5][0];
  myDisplay.line7 = &lines[6][0];
  myDisplay.line8 = &lines[7][0];
  myDisplay.lineLength = CE2_MAX_LINE_LEN;

  sprintf( lines[0], "AppName: '%s'", APPNAME );
  sprintf( lines[1], "Waiting for custom" );
  sprintf( lines[2], "event from test" );
  sprintf( lines[3], "script." );
  sprintf( lines[7], "CANCEL to abort" );
  while( UI_ERR_ABORT != ret )
  {
    ret = uiConfirm( "confirm-cancel", uiPrint( "%s<br>%s<br>%s<br>%s<br>%s<br>%s<br>%s<br>%s", lines[0], lines[1], lines[2], lines[3], lines[4], lines[5], lines[6], lines[7] ), customEvent_callBack, &myDisplay );
  }
} // custom_event_test2


// =================================================================================================
void html_printer_test (void)
{
#if 0 // long duration test for HTML printer available

  bool               landscape = false;
  map<string,string> value;
  int                r         = vfiprt::PRT_OK;


  vDebugOutVats( __MODULE__, __LINE__, "  test_app: html_printer_test: called\n" );

  for( int i = 0; i < 2000; i++ )
  {
    value.clear();
    value["receipt_n"] = "001";
    value["itemlist"]  = "name:value";
    vDebugOutVats( __MODULE__, __LINE__, "  test_app: html_printer_test: i=%d\n", i );
    uiConfirm( "confirm-cancel", uiPrint( "<h4>Printer test</h4>%s<br>%d<input type='timeout' value='1' style='visibility:hidden' action='return 400'>", value["receipt_n"].c_str(), i ) );
    r = vfiprt::prtURL( value, "receipt.html", landscape );
    if( vfiprt::PRT_OK != r )
    {
      vDebugOutVats( __MODULE__, __LINE__, "  test_app: html_printer_test: ERROR: r=%d\n", r );
      return;
    }

#else   // #if 0

  bool               landscape = false;
  JSObject           jHelp;
  map<string,string> value;
  int                r         = vfiprt::PRT_OK;


  vDebugOutVats( __MODULE__, __LINE__, "  test_app: html_printer_test: called\n" );

  // first receipt
  value.clear();
  value["receipt_n"]="007";
  jHelp[0]("name")="my name";
  jHelp[0]("value")="VATS test";
  jHelp[1]("name")="high score";
  jHelp[1]("value")=7000;
  value["itemlist"]=jHelp.dump();
  uiConfirm ("confirm-cancel", uiPrint( "<h4>Printer test</h4>%s<input type='timeout' value='5' style='visibility:hidden' action='return 400'>", value["receipt_n"].c_str() ) );
  r = vfiprt::prtURL( value, "receipt.html", landscape );
  vDebugOutVats( __MODULE__, __LINE__, "  test_app: html_printer_test: receipt.html=%d\n", r );
  uiConfirm ("confirm-cancel", uiPrint( "<h4>Printer test</h4>%s<br>Result: %d<input type='timeout' value='5' style='visibility:hidden' action='return 401'>", value["receipt_n"].c_str(), r ) );
  if( vfiprt::PRT_OK != r )
  {
    uiConfirm ("confirm-cancel", uiPrint( "<h4>Printer test</h4>%s<br>Reprinting...<input type='timeout' value='5' style='visibility:hidden' action='return 402'>", value["receipt_n"].c_str() ) );
    r = vfiprt::prtURL( value, "receipt.html", landscape );
    uiConfirm ("confirm-cancel", uiPrint( "<h4>Printer test</h4>%s<br>Result: %d<input type='timeout' value='5' style='visibility:hidden' action='return 403'>", value["receipt_n"].c_str(), r ) );
  }
  // second receipt
  value.clear();
  value["receipt_n"]="0815";
  value["itemlist"]="";
  uiConfirm ("confirm-cancel", uiPrint( "<h4>Printer test</h4>%s<input type='timeout' value='5' style='visibility:hidden' action='return 404'>", value["receipt_n"].c_str() ) );
  r = vfiprt::prtURL( value, "receipt.html", landscape );
  uiConfirm ("confirm-cancel", uiPrint( "<h4>Printer test</h4>%s<br>Result: %d<input type='timeout' value='5' style='visibility:hidden' action='return 405'>", value["receipt_n"].c_str(), r ) );
  if( vfiprt::PRT_OK != r )
  {
    uiConfirm ("confirm-cancel", uiPrint( "<h4>Printer test</h4>%s<br>Reprinting...<input type='timeout' value='5' style='visibility:hidden' action='return 406'>", value["receipt_n"].c_str() ) );
    r = vfiprt::prtURL( value, "receipt.html", landscape );
    uiConfirm ("confirm-cancel", uiPrint( "<h4>Printer test</h4>%s<br>Result: %d<input type='timeout' value='5' style='visibility:hidden' action='return 407'>", value["receipt_n"].c_str(), r ) );
  }

#endif
} // html_printer_test


// =================================================================================================
void buzzer_test (void)
{
  uiConfirm ("confirm-cancel", "Buzzer test start<br><input type='timeout' value='1' style='visibility:hidden' action='return 1201'>");
  vDebugOutVats (__MODULE__, __LINE__, "test_app: buzzer_test: Buzzer_Open\n");
  int ret = Buzzer_Open();

  for (int i = 0; i < 5; i++)
  {
    Buzzer_Play (2000+i, 500+i, 50+i);
    if (i == 0)
      sleep (3);
    else
      usleep (600000); // 600 msec
  }
  Buzzer_Close (ret);

  uiConfirm ("confirm-cancel", "Buzzer test stop<br><input type='timeout' value='1' style='visibility:hidden' action='return 1202'>");
}


// =================================================================================================
void msrCallback( void )
{
  if( msrDataAvailable )
  {
    return; // do not destroy data from an earlier call
  }
  else
  {
    magCard = msrMagneticCardPresent();
    if( 1 == magCard )
    {
      memset( msrData, 0, sizeof( msrData ) );
      msrDataInsert.stTrack1.ucCount  = MSR_T1_MAX_LEN + 10;
      msrDataInsert.stTrack1.ucStatus = 0xFF;
      msrDataInsert.stTrack2.ucCount  = MSR_T2_MAX_LEN + 10;
      msrDataInsert.stTrack2.ucStatus = 0xFF;
      msrDataInsert.stTrack3.ucCount  = MSR_T3_MAX_LEN + 10;
      msrDataInsert.stTrack3.ucStatus = 0xFF;
      ErrNo = msrStructured( &msrDataInsert );
      vDebugOutVats( __MODULE__, __LINE__, "  test_app: msrCallback: msrStructured: ErrNo=%d\n", ErrNo );
      msrDataAvailable = true;
    }
  }
}


// =================================================================================================
bool cb_magdata2( void * )
{
  enum AtsEvent ats_event = ATSEVENT_NONE;


  // handle physical mag data
  if( msrDataAvailable )
  {
    vDebugOutVats (__MODULE__, __LINE__, "  test_app: cb_magdata2: msrDataAvailable\n");
    return false;   // terminate uiConfirm ()
  }

  magDataBufferLen = sizeof (magDataBuffer);
  if ( ATSERROR_OK == AtsReceiveEvent (&ats_event, 0, &magDataBufferLen, (void*) magDataBuffer) )
  {
    if (ats_event == ATSEVENT_USER_MAGSTRIPE)
    {
      int j = 0;


      vDebugOutVats( __MODULE__, __LINE__, "  test_app: cb_magdata2: AtsReceiveEvent: ats_event=%d magDataBufferLen=%d\n", ats_event, magDataBufferLen );
      // convert the msrRead data type to MSR_DATA structure
      memset( msrData, 0, sizeof( msrData ) );
      msrDataInsert.stTrack1.ucCount  = MSR_T1_MAX_LEN + 10;
      msrDataInsert.stTrack1.ucStatus = 0xFF;
      msrDataInsert.stTrack2.ucCount  = MSR_T2_MAX_LEN + 10;
      msrDataInsert.stTrack2.ucStatus = 0xFF;
      msrDataInsert.stTrack3.ucCount  = MSR_T3_MAX_LEN + 10;
      msrDataInsert.stTrack3.ucStatus = 0xFF;
      ErrNo = 0;
      // size1  state1 (data1) size2  state2 (data2) size3  state3 (data3)
      if( 0 < magDataBuffer[j] )
      {
        if( ( magDataBuffer[j] - 2 ) > msrDataInsert.stTrack1.ucCount )
        {
          ErrNo = -1;
        }
        else
        {
          msrDataInsert.stTrack1.ucCount = magDataBuffer[j] - 2;
          memcpy( msrDataInsert.stTrack1.cpData, &magDataBuffer[j + 2], msrDataInsert.stTrack1.ucCount );
        }
      }
      else
      {
        msrDataInsert.stTrack1.ucCount = 0;
      }
      msrDataInsert.stTrack1.ucStatus = magDataBuffer[j + 1];
      j += magDataBuffer[j];                                        // increase j by size of track data
      if( 0 < magDataBuffer[j] )
      {
        if( ( magDataBuffer[j] - 2 ) > msrDataInsert.stTrack2.ucCount )
        {
          ErrNo = -1;
        }
        else
        {
          msrDataInsert.stTrack2.ucCount = magDataBuffer[j] - 2;
          memcpy( msrDataInsert.stTrack2.cpData, &magDataBuffer[j + 2], msrDataInsert.stTrack2.ucCount );
        }
      }
      else
      {
        msrDataInsert.stTrack2.ucCount = 0;
      }
      msrDataInsert.stTrack2.ucStatus = magDataBuffer[j + 1];
      j += magDataBuffer[j];                                        // increase j by size of track data
      if( 0 < magDataBuffer[j] )
      {
        if( ( magDataBuffer[j] - 2 ) > msrDataInsert.stTrack3.ucCount )
        {
          ErrNo = -1;
        }
        else
        {
          msrDataInsert.stTrack3.ucCount = magDataBuffer[j] - 2;
          memcpy( msrDataInsert.stTrack3.cpData, &magDataBuffer[j + 2], msrDataInsert.stTrack3.ucCount );
        }
      }
      else
      {
        msrDataInsert.stTrack3.ucCount = 0;
      }
      msrDataInsert.stTrack3.ucStatus = magDataBuffer[j + 1];
      msrDataAvailable = true;
      return false;   // terminate uiConfirm ()
    }
  }
  return true;    // continue uiConfirm ()
}


// =================================================================================================
int DisplayMagData3( int *swipeCount )
{
  int ret;
  char lines [8][21];


  vDebugOutVats( __MODULE__, __LINE__, "  test_app: DisplayMagData3: ErrNo=%d\n", ErrNo );

  memset (lines, 0x00, sizeof (lines));
  sprintf (lines [0], "Magstripe test");

  if( msrDataAvailable )
  {
    msrDataAvailable = false;
    (*swipeCount)++;
    sprintf (lines [3], "Total Bytes: %4d", msrDataInsert.stTrack1.ucCount + msrDataInsert.stTrack2.ucCount + msrDataInsert.stTrack3.ucCount);

    sprintf ( lines [4], "TK#1:%03d %02X-%.8s",
              msrDataInsert.stTrack1.ucCount,
              msrDataInsert.stTrack1.ucStatus,
              ((msrDataInsert.stTrack1.ucStatus >= MAX_ERR) ? "UNKNOWN" : VATScardErrTbl[msrDataInsert.stTrack1.ucStatus]) );
    sprintf ( lines [5], "TK#2:%03d %02X-%.8s",
              msrDataInsert.stTrack2.ucCount,
              msrDataInsert.stTrack2.ucStatus,
              ((msrDataInsert.stTrack2.ucStatus >= MAX_ERR) ? "UNKNOWN" : VATScardErrTbl[msrDataInsert.stTrack2.ucStatus]) );
    sprintf ( lines [6], "TK#3:%03d %02X-%.8s",
              msrDataInsert.stTrack3.ucCount,
              msrDataInsert.stTrack3.ucStatus,
              ((msrDataInsert.stTrack3.ucStatus >= MAX_ERR) ? "UNKNOWN" : VATScardErrTbl[msrDataInsert.stTrack3.ucStatus]) );
  }
  else
  {
    sprintf ( lines [3], "ERR: %-3d / %-3d", (msrDataInsert.stTrack1.ucCount + msrDataInsert.stTrack2.ucCount + msrDataInsert.stTrack3.ucCount), ErrNo );
    strcpy( lines [4], "" );
    strcpy( lines [5], "" );
    strcpy( lines [6], "" );
  }
  sprintf (lines [1], "No. of swipes: %3d", *swipeCount);
  sprintf (lines [2], "Card %d, data %d", magCard, msrDataAvailable);
  magCard = 0;
  sprintf (lines [7], "CANCEL to abort");
  ret = uiConfirm( "confirm-cancel", uiPrint ("%s<br>%s<br>%s<br>%s<br>%s<br>%s<br>%s<br>%s",
                                     lines [0], lines [1], lines [2], lines [3], lines [4], lines [5], lines [6], lines [7]), cb_magdata2 );
  return ret;
}


// =================================================================================================
void magstripe_test2( void )
{
  int      res;
  int      swipeCount = 0;
  unsigned offset     = 0;


  vDebugOutVats( __MODULE__, __LINE__, "  test_app: magstripe_test2\n" );
  memset( msrData, 0, sizeof( msrData ) );
  msrDataInsert.stTrack1.ucCount  = MSR_T1_MAX_LEN + 10;
  msrDataInsert.stTrack1.ucStatus = 0xFF;
  msrDataInsert.stTrack1.cpData = msrData + offset;
  offset += MSR_T1_MAX_LEN + 10; // +10 for too long tracks
  msrDataInsert.stTrack2.ucCount  = MSR_T2_MAX_LEN + 10;
  msrDataInsert.stTrack2.ucStatus = 0xFF;
  msrDataInsert.stTrack2.cpData = msrData + offset;
  offset += MSR_T2_MAX_LEN + 10; // +10 for too long tracks
  msrDataInsert.stTrack3.ucCount  = MSR_T3_MAX_LEN + 10;
  msrDataInsert.stTrack3.ucStatus = 0xFF;
  msrDataInsert.stTrack3.cpData = msrData + offset;

  res = msrOpen( O_NONBLOCK, (void *)msrCallback );   // do not block msr operations and use callbacks

  vDebugOutVats( __MODULE__, __LINE__, "test_app: magstripe_test2: msrOpen()=%d\n", res );

  if( res < 0 )
  {
    vDebugOutVats( __MODULE__, __LINE__, "  test_app: magstripe_test2: Can't open //DEV//MAG (error %d)\n", res );
    return;
  }

  res = UI_ERR_OK;
  while( UI_ERR_ABORT != res )
  {
    res = DisplayMagData3( &swipeCount );
    svcSleep (100);
  } // while
  msrClose ();
}


// =================================================================================================
void disp_region_test( void )
{
  const struct UIRegion reg_no_statusbar[]=
  {
    {UI_REGION_DEFAULT, 0, 0,-1,-1,0}   // main screen     ADKVATS-313: missing initialization parameter added
  };
  const struct UIRegion reg_statusbar[]=
  {
    {1,                 0, 0,-1,31,0},  // status bar      ADKVATS-313: missing initialization parameter added
    {UI_REGION_DEFAULT, 0,32,-1,-1,0}   // main screen     ADKVATS-313: missing initialization parameter added
  };


  // show main screen
  uiDisplay( "<h4>Region test</h4>Main screen for display regions test." );
  // wait some time before changing the display regions
  svcSleep( 5000 );                   // msec
  // add status bar
  uiLayout( reg_statusbar, sizeof( reg_statusbar ) / sizeof( reg_statusbar[0] ) );
  uiDisplay( 1, "statusbar", "01.11.2013 12:45" );
  // wait some time before updating the main screen
  svcSleep( 3000 );                   // msec
  uiDisplay( "<h4>Region test</h4>Main screen update 1." );
  // wait some time before updating the display regions
  svcSleep( 3000 );                   // msec
  uiDisplay( 1, "statusbar", "01.11.2013 12:55" );
  uiDisplay( "<h4>Region test</h4>Main screen update 2." );
  // wait some time before updating the display regions
  svcSleep( 3000 );                   // msec
  uiDisplay( "<h4>Region test</h4>Main screen update 3." );
  uiDisplay( 1, "statusbar", "01.11.2013 13:05" );
  // wait some time before removing the status bar
  svcSleep( 3000 );                   // msec
  // remove the status bar
  uiLayout( reg_no_statusbar, sizeof( reg_no_statusbar ) / sizeof( reg_no_statusbar[0] ) );
  uiConfirm ("confirm-cancel", "<h4>Region test</h4>Main screen update after status bar removal.<input type='timeout' value='3' style='visibility:hidden' action='return 998'>" );
} // disp_region_test


// =================================================================================================
void pin_test( void )
{
  int                  r = UI_ERR_OK;
  map <string, string> value;
#ifdef _VOS2
  int                  j;
  unsigned long        rsize;
  char                 model [PI_CIB_MODEL_ID_SIZE];
#endif


  vDebugOutVats( __MODULE__, __LINE__, "test_app: pin_test: called\n" );
  // modify these values to change default behaviour of PIN entry
  (void)uiSetPropertyInt( UI_PROP_PASSWORD_CHAR,  '*' );
  (void)uiSetPropertyInt( UI_PROP_PIN_AUTO_ENTER, 0 );        // 1 -> enable
  (void)uiSetPropertyInt( UI_PROP_PIN_CLEAR_ALL,  0 );        // 1 -> enable
  (void)uiSetPropertyInt( UI_PROP_PIN_BYPASS_KEY, 8 );        // 13 -> enter key, 8 -> clear key
  (void)uiSetPropertyInt( UI_PROP_PIN_ALGORITHM,  EMV_PIN );

#ifdef _VOS2
  if ( PI_OK == platforminfo_get(PI_CIB_MODEL_ID_STR, model, sizeof(model), &rsize) )
  {
    for (j=0; model [j] && model [j] != ' '; j++);
    model [j] = '\0';
    VatsLogging (VATS_LOG_INFO, __MODULE__, __LINE__, "test_app: pin_test: original: model=%s\n", model);
    if ( (0 == strncmp (model, "X10", strlen ("X10") )) || (0 == strncmp (model, "SF-PAY", strlen ("SF-PAY") )) )
      r = uiInvokeURL (value, "pin_x10.html");
    else
      r = uiInvoke( value, "<h4>PIN test</h4>PIN <input type='pin' name='input' minlength='4' maxlength='6' action='return 0'><br>(4 to 6 digits)" );
  }
#else
  r = uiInvoke( value, "<h4>PIN test</h4>PIN <input type='pin' name='input' minlength='4' maxlength='6' action='return 0'><br>(4 to 6 digits)" );
#endif

  vDebugOutVats( __MODULE__, __LINE__, "test_app: pin_test: r=%d\n", r );
  string v = uiPrint( "Result: %d<br>", r );
  map <string, string>::iterator i;
  for( i = value.begin(); i != value.end(); ++i )
  {
    v += uiPrint( "%S=%S<br>", i->first.c_str(), i->second.c_str() );
  }
  v.append( "<input type='timeout' value='2' style='visibility:hidden' action='return -999'>" );
  uiConfirm( "confirm-cancel", v );
  vDebugOutVats( __MODULE__, __LINE__, "test_app: pin_test: done\n" );
} // pin_test


// =================================================================================================
void vats_key_inject_test (void)
{
  int r = UI_ERR_OK;


  vDebugOutVats( __MODULE__, __LINE__, "test_app: vats_key_inject_test called\n" );
  while( 20 != r )
  {
    r = uiConfirm( "key-inject", "Press any key<br>CANCEL to abort" );
    uiConfirm( "confirm-cancel", uiPrint( "Entered: %d<input type='timeout' value='2' style='visibility:hidden' action='return -999'>", r ) );
  }

  vDebugOutVats( __MODULE__, __LINE__, "test_app: vats_key_inject_test done\n" );
} // vats_key_inject_test


// =================================================================================================
void datetime_test( void )
{
  int        i = 1;
  int        r = UI_ERR_OK;
  char       dateAndTime[16];
  time_t     rawtime;
  struct tm *timeinfo;
  string myDisplay;


  vDebugOutVats( __MODULE__, __LINE__, "test_app: datetime_test called\n" );

  while( UI_ERR_ABORT != r )
  {
    memset( dateAndTime, 0x00, sizeof( dateAndTime ) );
    time( &rawtime );
    timeinfo = localtime( &rawtime );
    strftime( dateAndTime, ( sizeof( dateAndTime ) - 2 ), "%Y%m%d%H%M%S", timeinfo ); // yyyymmddhhmmss
    vDebugOutVats( __MODULE__, __LINE__, "test_app: datetime_test i=%d %s\n", i, dateAndTime );

    myDisplay.assign( uiPrint( "<h4>Date and Time</h4>date and time %d:<br>%s<br><input type='timeout' value='2' style='visibility:hidden' action='return 1100'>", i, dateAndTime ) );
    r = uiConfirm( "confirm-cancel", myDisplay );
    i++;
  }

  vDebugOutVats( __MODULE__, __LINE__, "test_app: datetime_test done\n" );
} // datetime_test


// =================================================================================================
void vats_touch_test (void)
{
  int r = UI_ERR_OK;


  vDebugOutVats( __MODULE__, __LINE__, "test_app: vats_touch_test called\n" );
  while( 20 != r )
  {
    r = uiConfirm( "touch-inject", "Touch any button<br>CANCEL to abort" );
    uiConfirm( "confirm-cancel", uiPrint( "Touched: %d<input type='timeout' value='2' style='visibility:hidden' action='return -999'>", r ) );
  }

  vDebugOutVats( __MODULE__, __LINE__, "test_app: vats_touch_test done\n" );
}


// =================================================================================================
void multi_menu_test()
{
  int ret = UI_ERR_OK;
  string myFirst1    = "First ";
  string mySecond1   = "Second ";
  string myThird     = "Third ";
  string myFourth    = "Fourth ";
  string myTestItem1 = "'Test item'";
  string myTestItem2 = "'Other item'";
  string myTestItem3 = "'Who am I'";
  string myIn        = " in ";
  string myFirst2    = "first";
  string mySecond2   = "second";
  string myEnd       = " menu was selected.<input type='timeout' value='2' style='visibility:hidden' action='return -999'>";
  string myOutput;


  ret = uiConfirm( "confirm-cancel", "<table border='0' cellspacing='0' cellpadding='0' style='height:100%;width:100%'><tr><td style='height:1px;vertical-align:top'><h4>Multiple menu</h4></td></tr><tr><td style='height:100%'>Menu 1<select name='testmenu1' size='4' style='width:100%'><option action='return 4' selected>Test item</option><option action='return 2'>Test item</option><option action='return 3'>Other item</option><option action='return 1'>Who am I</option></select>Menu 2<select name='testmenu2' size='4' style='width:100%'><option action='return 5'>Other item</option><option action='return 7'>Test item</option><option action='return 8' selected>Test item</option><option action='return 6'>Who am I</option></select></td></tr></table><button accesskey='1' action='call testmenu1.action()' style='visibility:hidden'></button><button accesskey='2' action='call testmenu1.up()' style='visibility:hidden'></button><button accesskey='3' action='call testmenu1.down()' style='visibility:hidden'></button><button accesskey='4' action='call testmenu2.action()' style='visibility:hidden'></button><button accesskey='5' action='call testmenu2.up()' style='visibility:hidden'></button><button accesskey='6' action='call testmenu2.down()' style='visibility:hidden'></button><input type='timeout' value='15' style='visibility:hidden' action='return 100'>" );
  myOutput.clear();
  switch( ret )
  {
    case 4:
      myOutput.append( myFirst1 );
      myOutput.append( myTestItem1 );
      myOutput.append( myIn );
      myOutput.append( myFirst2 );
      myOutput.append( myEnd );
      break;
    case 2:
      myOutput.append( mySecond1 );
      myOutput.append( myTestItem1 );
      myOutput.append( myIn );
      myOutput.append( myFirst2 );
      myOutput.append( myEnd );
      break;
    case 3:
      myOutput.append( myFirst1 );
      myOutput.append( myTestItem2 );
      myOutput.append( myIn );
      myOutput.append( myFirst2 );
      myOutput.append( myEnd );
      break;
    case 1:
      myOutput.append( myFirst1 );
      myOutput.append( myTestItem3 );
      myOutput.append( myIn );
      myOutput.append( myFirst2 );
      myOutput.append( myEnd );
      break;
    case 5:
      myOutput.append( mySecond1 );
      myOutput.append( myTestItem2 );
      myOutput.append( myIn );
      myOutput.append( mySecond2 );
      myOutput.append( myEnd );
      break;
    case 7:
      myOutput.append( myThird );
      myOutput.append( myTestItem1 );
      myOutput.append( myIn );
      myOutput.append( mySecond2 );
      myOutput.append( myEnd );
      break;
    case 8:
      myOutput.append( myFourth );
      myOutput.append( myTestItem1 );
      myOutput.append( myIn );
      myOutput.append( mySecond2 );
      myOutput.append( myEnd );
      break;
    case 6:
      myOutput.append( mySecond1 );
      myOutput.append( myTestItem3 );
      myOutput.append( myIn );
      myOutput.append( mySecond2 );
      myOutput.append( myEnd );
      break;
    default:
      myOutput.append( uiPrint( "Result: %d<input type='timeout' value='2' style='visibility:hidden' action='return -999'>", ret ) );
      break;
  }
  uiConfirm( "confirm-cancel", myOutput );
}


// =================================================================================================
int checkMenuItems( const int *menuItemValues, int menuItem )
{
  int        retVal = 0;
  const int *myMenu = menuItemValues;
  bool       bFound = false;


  if( NULL != menuItemValues )
  {
    retVal = *menuItemValues;
    while( ( VATS_GUI_TEST_ID_NONE != *myMenu ) && ( !bFound ) )
    {
      if( *myMenu == menuItem )
      {
        retVal = menuItem;
        bFound = true;
      }
      else
      {
        myMenu++;
      }
    }
  }
  else
  {
    retVal = menuItem;
  }
  return retVal;
}


// =================================================================================================
void menu_card_handling( void )
{
  int    ret                = cardMenuItems[ 0];
  int    iContinue          = 1;
  struct UIMenuEntry menu[] = {
    { "ICC card in   test", cardMenuItems[ 0], 0 },
    { "MSR read      test", cardMenuItems[ 1], 0 },
    { "msrStructured test", cardMenuItems[ 2], 0 },
    { "EMV           test", cardMenuItems[ 3], 0 },
    { "back to main menu", VATS_GUI_MENU_BACK, 0 }
  };

#ifdef _VOS2
  menu[3].text.assign( "EMV not supported" );
  menu[3].value   = VATS_GUI_TEST_ID_NONE;
  menu[3].options = 0;
#endif  // #ifdef _VOS2

  while( 1 == iContinue )
  {
    ret = checkMenuItems( cardMenuItems, ret );
    ret = uiMenu( "mainmenu", "<h4>Card Reader Menu</h4>", menu, (sizeof( menu ) / sizeof( menu[0] )), ret > VATS_GUI_TEST_ID_NONE ? ret : cardMenuItems[ 0] );

    if( ( UI_ERR_ABORT == ret ) || ( UI_ERR_BACK == ret ) || ( UI_ERR_TIMEOUT == ret ) )
    {
      // back to main menu
      iContinue = 0;
    }
    else if( cardMenuItems[ 0] == ret ) cardin_test();
    else if( cardMenuItems[ 1] == ret ) magstripe_test();
    else if( cardMenuItems[ 2] == ret ) magstripe_test2();
#ifdef _EMV
#ifndef _VOS2
    else if( cardMenuItems[ 3] == ret ) EMV_Test_VATS();
#endif  // #ifndef _VOS2
#endif
  }
}


// =================================================================================================
void menu_print_handling( void )
{
  int    ret                = printMenuItems[ 0];
  int    iContinue          = 1;
  struct UIMenuEntry menu[] = {
    { "printer    test  1", printMenuItems[ 0], 0 },
    { "back to main menu", VATS_GUI_MENU_BACK, 0 }
  };


  while( 1 == iContinue )
  {
    ret = checkMenuItems( printMenuItems, ret );
    ret = uiMenu( "mainmenu", "<h4>Printer Menu</h4>", menu, (sizeof( menu ) / sizeof( menu[0] )), ret > VATS_GUI_TEST_ID_NONE ? ret : printMenuItems[ 0] );

    if( ( UI_ERR_ABORT == ret ) || ( UI_ERR_BACK == ret ) || ( UI_ERR_TIMEOUT == ret ) )
    {
      // back to main menu
      iContinue = 0;
    }
    else if( printMenuItems[ 0] == ret ) html_printer_test();
  }
}


// =================================================================================================
void menu_user_handling( void )
{
  int    ret                = userMenuItems[ 0];
  int    iContinue          = 1;
  struct UIMenuEntry menu[] = {
    { "keyboard   test  1", userMenuItems[ 0], 0 },
    { "keyboard   test  2", userMenuItems[ 1], 0 },
    { "PIN entry  test  1", userMenuItems[ 2], 0 },
    { "touch      test  1", userMenuItems[ 3], 0 },
    { "Multiple menus", userMenuItems[ 4], 0 },
    { "back to main menu", VATS_GUI_MENU_BACK, 0 }
  };


// ************************************************************************************************
// *  EventViaDQDriver() does not work for V/OS; perhaps check of /etc/vats.cfg is possible
// ************************************************************************************************
//  if( 0 == EventViaDQDriver( ATSEVENT_USER_KEYBOARD ) )
//  {
//    menu[1].text.assign( "no DQ driver active" );
//    menu[1].value   = VATS_GUI_TEST_ID_NONE;
//    menu[1].options = 0;
//    menu[2].text.assign( "no DQ driver active" );
//    menu[2].value   = VATS_GUI_TEST_ID_NONE;
//    menu[2].options = 0;
//  }
  if( 1 != touchScreenAvailable() )
  {
    menu[3].text.assign( "no touch available" );
    menu[3].value   = VATS_GUI_TEST_ID_NONE;
    menu[3].options = 0;
  }
  while( 1 == iContinue )
  {
    ret = checkMenuItems( userMenuItems, ret );
    ret = uiMenu( "mainmenu", "<h4>User Input Menu</h4>", menu, (sizeof( menu ) / sizeof( menu[0] )), ret > VATS_GUI_TEST_ID_NONE ? ret : userMenuItems[ 0] );

    if( ( UI_ERR_ABORT == ret ) || ( UI_ERR_BACK == ret ) || ( UI_ERR_TIMEOUT == ret ) )
    {
      // back to main menu
      iContinue = 0;
    }
    else if(   userMenuItems[ 0] == ret )                                                                keyboard_test();
    else if( ( userMenuItems[ 1] == ret ) /* && ( 0 != EventViaDQDriver( ATSEVENT_USER_KEYBOARD ) ) */ ) vats_key_inject_test();
    else if( ( userMenuItems[ 2] == ret ) /* && ( 0 != EventViaDQDriver( ATSEVENT_USER_KEYBOARD ) ) */ ) pin_test();
    else if( ( userMenuItems[ 3] == ret ) && ( 1 == touchScreenAvailable() ) )                           vats_touch_test();
    else if(   userMenuItems[ 4] == ret )                                                                multi_menu_test();
  }
}


// =================================================================================================
void menu_disp_handling( void )
{
  int    ret                = dispMenuItems[ 0];
  int    iContinue          = 1;
  struct UIMenuEntry menu[] = {
    { "display    test  1", dispMenuItems[ 0], 0 },
    { "display    test  2", dispMenuItems[ 1], 0 },
    { "disp.regs. test  1", dispMenuItems[ 2], 0 },
    { "back to main menu", VATS_GUI_MENU_BACK, 0 }
  };


  while( 1 == iContinue )
  {
    ret = checkMenuItems( dispMenuItems, ret );
    ret = uiMenu( "mainmenu", "<h4>Display Menu</h4>", menu, (sizeof( menu ) / sizeof( menu[0] )), ret > VATS_GUI_TEST_ID_NONE ? ret : dispMenuItems[ 0] );

    if( ( UI_ERR_ABORT == ret ) || ( UI_ERR_BACK == ret ) || ( UI_ERR_TIMEOUT == ret ) )
    {
      // back to main menu
      iContinue = 0;
    }
    else if( dispMenuItems[ 0] == ret ) display_test();
    else if( dispMenuItems[ 1] == ret ) screen_test();
    else if( dispMenuItems[ 2] == ret ) disp_region_test();
  }
}


// =================================================================================================
void menu_misc_handling( void )
{
  int    ret                = miscMenuItems[ 0];
  int    iContinue          = 1;
  struct UIMenuEntry menu[] = {
    { "DUT to PC cust.ev.", miscMenuItems[ 0], 0 },
    { "PC to DUT cust.ev.", miscMenuItems[ 1], 0 },
    { "trace         test", miscMenuItems[ 2], 0 },
    { "date and time test", miscMenuItems[ 3], 0 },
    { "buzzer        test", miscMenuItems[ 4], 0 },
    { "terminal unit test", miscMenuItems[ 5], 0 },
    { "back to main menu", VATS_GUI_MENU_BACK, 0 }
  };


  while( 1 == iContinue )
  {
    ret = checkMenuItems( miscMenuItems, ret );
    ret = uiMenu( "mainmenu", "<h4>Miscellaneous</h4>", menu, (sizeof( menu ) / sizeof( menu[0] )), ret > VATS_GUI_TEST_ID_NONE ? ret : miscMenuItems[ 0] );

    if( ( UI_ERR_ABORT == ret ) || ( UI_ERR_BACK == ret ) || ( UI_ERR_TIMEOUT == ret ) )
    {
      // back to main menu
      iContinue = 0;
    }
    else if( miscMenuItems[ 0] == ret ) custom_event_test();
    else if( miscMenuItems[ 1] == ret ) custom_event_test2();
    else if( miscMenuItems[ 2] == ret ) trace_event_test();
    else if( miscMenuItems[ 3] == ret ) datetime_test();
    else if( miscMenuItems[ 4] == ret ) buzzer_test();
    else if( miscMenuItems[ 5] == ret ) menu_terminal_unit_test();
  }
}


// =================================================================================================
void menu_main_handling( void )
{
  int                ret    = mainMenuItems[ 0];
  int    iContinue          = 1;
  struct UIMenuEntry menu[] = {
    { "Miscellaneous", mainMenuItems[ 0], 0 },
    { "Display",       mainMenuItems[ 1], 0 },
    { "User input",    mainMenuItems[ 2], 0 },
    { "Printer",       mainMenuItems[ 3], 0 },
    { "Card reader",   mainMenuItems[ 4], 0 },
    // here special menu items are listed which are no tests
    // they shall remain at the end of the list
    { "terminal info", mainMenuItems[ 5], 0 }             // this menu item shall remain at the end of the list
  };


  if( 1 != PrinterAvailable() )
  {
    menu[3].text.assign( "no printer on tap" );
    menu[3].value   = VATS_GUI_TEST_ID_NONE;
    menu[3].options = 0;
  }
  while( 1 == iContinue )
  {
    ret = checkMenuItems( mainMenuItems, ret );
    ret = uiMenu( "mainmenu", "<h4>VATS Main Menu</h4>", menu, (sizeof( menu ) / sizeof( menu[0] )), ret > VATS_GUI_TEST_ID_NONE ? ret : mainMenuItems[ 0] );

    if( UI_ERR_CONNECTION_LOST == ret )
    {
      vDebugOutVats( __MODULE__, __LINE__, "  test_app: menu_main_handling: ERROR: UI_ERR_CONNECTION_LOST\n" );
      iContinue = 0;
    }
    else if(   mainMenuItems[ 0] == ret )                                   menu_misc_handling();
    else if(   mainMenuItems[ 1] == ret )                                   menu_disp_handling();
    else if(   mainMenuItems[ 2] == ret )                                   menu_user_handling();
    else if( ( mainMenuItems[ 3] == ret ) && ( 1 == PrinterAvailable() ) )  menu_print_handling();
    else if(   mainMenuItems[ 4] == ret )                                   menu_card_handling();
    else if(   mainMenuItems[ 5] == ret )                                   terminal_info();
  }
}


// =================================================================================================
// ADKVATS-164
void *unitTestThread (void *p)
{
  int  i;
  int  ret;
  char data [2];
  int  pos;
  char magdata [512];

  /* ADKVATS-313: unused parameter */
  p = p;

  vDebugOutVats (__MODULE__, __LINE__, "  test_app: unitTestThread called\n");
  ret = AtsInitVOS ("unitTestThread", 1);
  vDebugOutVats (__MODULE__, __LINE__, "  test_app: unitTestThread: AtsInitVOS: ret=%d\n", ret);

  //          VX520   UX300
  // CONFIRM: 13      13
  // CANCEL : 27      27
  // DOWN   : 125     99
  // UP:              98
  while (1)
  {
    if (doPINTest)
    {
      doPINTest = false;

      i = 1;
      data [0] = 99; // DOWN
      ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: unitTestThread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
      i = 2;
      data [0] = 13; // CONFIRM
      ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: unitTestThread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
      i = 3;
      data [0] = '1';
      ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: unitTestThread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
      svcSleep (1000);
      i = 4;
      data [0] = '2';
      ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: unitTestThread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
      svcSleep (1000);
      i = 5;
      data [0] = '3';
      ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: unitTestThread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
      svcSleep (1000);
      i = 6;
      data [0] = '4';
      ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: unitTestThread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
      svcSleep (1000);
      i = 7;
      data [0] = 13; // CONFIRM
      ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: unitTestThread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
      i = 8;
      svcSleep (2000);
      data [0] = 27; // CANCEL
      ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: unitTestThread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
    }

    if (doMagDataTest)
    {
      doMagDataTest = false;

      i = 1;
      data [0] = 13; // CONFIRM
      ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: unitTestThread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
      svcSleep (1000);

      i = 2;
      // here without sentinels and LRC:
      // Magdata stripe 1, len=42, dat=11003132333D4A4F484E3D534D4954487802010201
      // Magdata stripe 2, len=38, dat=6726672910070115390D07091010274904490F
      // Magdata stripe 3, len=95, dat=015966666666D10070115392D2809540000000007001003012749020000007090232483411DD1D0000449000005020F
      pos = 0;
      magdata [pos++] = 44;
      magdata [pos++] = 0;
      memcpy (&magdata [pos], "11003132333D4A4F484E3D534D4954487802010201", 42);
      pos += 42;
      magdata [pos++] = 40;
      magdata [pos++] = 0;
      memcpy (&magdata [pos], "6726672910070115390D07091010274904490F", 38);
      pos += 38;
      magdata [pos++] = 97;
      magdata [pos++] = 0;
      memcpy (&magdata [pos], "015966666666D10070115392D2809540000000007001003012749020000007090232483411DD1D0000449000005020F", 95);
      pos += 95;

      ret = AtsSendEvent (ATSEVENT_USER_MAGSTRIPE, pos, (const void*) magdata);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: unitTestThread: AtsSendEvent: ATSEVENT_USER_MAGSTRIPE: i=%d ret=%d pos=%d\n", i, ret, pos);
      svcSleep (1000);
      i = 3;
      data [0] = 27; // CANCEL
      ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: unitTestThread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
      svcSleep (2000);
    }

    if (doCardinTest)
    {
      doCardinTest = false;

      i = 1;
      data [0] = 13; // CONFIRM
      ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: unitTestThread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
      svcSleep (1000);
      i = 2;
      data [0] = 1;  // Card_in set
      ret = AtsSendEvent (ATSEVENT_USER_ICCCARD_IN, 1, (const void*) data);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: unitTestThread: AtsSendEvent: ATSEVENT_USER_ICCCARD_IN: i=%d ret=%d\n", i, ret);
      svcSleep (2000);
      i = 3;
      data [0] = 0;  // Card_in set
      ret = AtsSendEvent (ATSEVENT_USER_ICCCARD_IN, 1, (const void*) data);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: unitTestThread: AtsSendEvent: ATSEVENT_USER_ICCCARD_IN: i=%d ret=%d\n", i, ret);
      svcSleep (2000);
      i = 4;
      data [0] = 27; // CANCEL
      ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: unitTestThread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
      svcSleep (1000);
    }

    svcSleep (1000);
  }

  vDebugOutVats (__MODULE__, __LINE__, "  test_app: unitTestThread: EXIT\n");
  return 0;
} // unitTestThread


// =================================================================================================
// ADKVATS-164:
void menu_terminal_unit_test ( void )
{
  int    ret                = tutMenuItems[ 0];
  int    iContinue          = 1;
  struct UIMenuEntry menu[] =
  {
    { "magdata unit test",   tutMenuItems[ 0], 0 },
    { "PIN input unit test", tutMenuItems[ 1], 0 },
    { "Card_in unit test",   tutMenuItems[ 2], 0 },
    { "back to main menu",   VATS_GUI_MENU_BACK, 0 }
  };


  while( 1 == iContinue )
  {
    ret = checkMenuItems( tutMenuItems, ret );
    ret = uiMenu( "mainmenu", "<h4>Terminal Unit Tests</h4>", menu, (sizeof( menu ) / sizeof( menu[0] )), ret > VATS_GUI_TEST_ID_NONE ? ret : tutMenuItems[ 0] );

    vDebugOutVats (__MODULE__, __LINE__, "test_app: menu_terminal_unit_test: ret=%d\n", ret); 
    if( ( UI_ERR_ABORT == ret ) || ( UI_ERR_BACK == ret ) || ( UI_ERR_TIMEOUT == ret ) )
    {
      iContinue = 0; // back to upper menu
    }
    else if( tutMenuItems[ 0] == ret )
    {
      doMagDataTest = true; // activate one pass in the unit test thread in order to provide data input
      magstripe_test ();
    }
    else if( tutMenuItems[ 1] == ret )
    {
      doPINTest = true; // activate one pass in the unit test thread in order to provide data input
      pin_test ();
    }
    else if( tutMenuItems[ 2] == ret )
    {
      doCardinTest = true; // activate one pass in the unit test thread in order to provide data input
      cardin_test ();
    }
  }
} // menu_terminal_unit_test


// =================================================================================================
// void uiMain (int argc, char *argv[])
int main ()
{
  int           propertyValue;
  pthread_t     unitTestThr; // ADKVATS-164
#ifndef _VOS2
  unsigned long r = EMV_INIT_ERR_NONE;
#endif  // #ifndef _VOS2


  // set resource subfolder for this application (if not already done by MAC)
  setenv( "GUIPRT_APPNAME", "vats-demo", 1 );

  vDebugOutVats( __MODULE__, __LINE__, "test_app: uiMain: START: 16.04.2014\n" );
  // register events to be handled by this application
  if ( 0 != AtsInitVOS( APPNAME, (int)( ATSREG_ICCCARD_IN | ATSREG_MAGSTRIPE | ATSREG_CUSTOM ) ) )
  {
    vDebugOutVats( __MODULE__, __LINE__, "  test_app: AtsInit: FAILED\n" );
  }

  // ADKVATS-164
  pthread_create (&unitTestThr, NULL, unitTestThread, (void *) "unitTestThread");

  // GUI init
  uiReadConfig();
  propertyValue = 60000;
  (void)uiSetPropertyInt( UI_PROP_TIMEOUT, propertyValue );
  propertyValue = 0;
  (void)uiGetPropertyInt( UI_PROP_TIMEOUT, &propertyValue );
  vDebugOutVats( __MODULE__, __LINE__, "  test_app: UI_PROP_TIMEOUT=%d\n", propertyValue );

#ifdef _EMV
#ifndef _VOS2
  // EMV init
  uiDisplay( "<h4>VATS</h4>Initializing EMV..." );
  r = EMV_InitEMV_VATS (0);
  if( EMV_INIT_ERR_NONE != r )
  {
    vDebugOutVats( __MODULE__, __LINE__, "test_app: main: EMV_InitEMV: %d.", r );
  }
  EMV_CTLS_ApplyConf_VATS ();
#endif  // #ifndef _VOS2
#endif

  // some debug information before starting the loop
  vDebugOutVats( __MODULE__, __LINE__, "test_app: main: VATS is active: %d.", AtsIsActive() );

  // infinite loop handling the menus and test cases
  menu_main_handling();
  vDebugOutVats( __MODULE__, __LINE__, "test_app: uiMain: Loop end\n" );
}


#endif  // #ifdef _VOS

