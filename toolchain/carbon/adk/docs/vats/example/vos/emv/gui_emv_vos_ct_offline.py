#----------------------------------------------------------------------------
#
#    File : gui_emv_vos_ct_offline.py
#
#    VATS demo : EMV testing
#
#    Author : Reinhard Lieberum
#
#    Creation date : 2014-May
#                    2014-June: - add responses of 39 14 (fetch)
#                               - does not depend on insert/remove_smartcard any longer
#                    2014-Oct:  - apply vats recording
#
#    Description:
'''Test module: GUI EMV test VOS CT offline'''

#    CLA_EMV, INS_TERM_CFG :  39 01: don't care, are really executed by EMV
#    CLA_EMV, INS_APPLI_CFG:  39 02: don't care, are really executed by EMV
#    VOS, CT, MasterCard Test Card 20.00, without PIN
#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *
from vats_html import *


#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def gui_emv_test():
    '''GUI EMV test VOS CT offline'''

    # set keyboard mapping
    terminalIndex = user.setTerminalSpecificValues()
    holdtime_ms     =   50  # for minimum this holdtime the display must be stable
    responsetime_ms = 3000  # latest after this response time the desired display must appear


    # Remove card to have a defined starting point
    user.remove_smartcard()

    # tear off last receipt in the case there is any
    user.tearoff_printout()

    # Wait until headline indicates "Welcome"
    ats.check( vats_json.html_waitDisplayContains( "Welcome", 0, default_menu_wait ), 'Welcome failed!!!' )

    vats_json.html_gui_setInput( 'number', '2000', 'in0' )
    myButton = vats_json.html_gui_pressButton( '', vats_json.VATS_KEYB_ENTER )

    #---------------------------------------------------------------------------------------------------
    # Wait until GUI display prompts for card insertion or tapping
    if ats.check( vats_json.html_waitDisplayContains( 'Insert or Tap', 0, 10000 ), 'Insert card failed!!!' ):

        # VATS cleanup
        user.send_emv_ct_data ("FFFFFF")
        user.send_emv_ct_data ("90010000")
        user.send_emv_ct_data ("90010000")
        user.send_emv_ct_data ("90010000")
        user.send_emv_ct_data ("90010000")
        user.send_emv_ct_data ("90000000")
        user.send_emv_ct_data ("90000000F015DF68123BE900008131FE454A434F503331563232A0")
        user.send_emv_ct_data ("91010000F007BF0104DF700107")
        user.send_emv_ct_data ("90000000F0588407A0000000041010500F505043204D41502030312020763230DF330F505043204D41502030312020763230DF4E0100FF031BDF0407A0000000041010500F505043204D41502030312020763230DF0407A0000000041010")
        user.send_emv_ct_data ("91010000F007BF0104DF70010B")
        user.send_emv_ct_data ("91010000F007BF0104DF700181")
        user.send_emv_ct_data ("91010000F007BF0104DF700103")
        user.send_emv_ct_data ("91010000F017BF03145A0854133300896010185F3401005F2403141231")
        user.send_emv_ct_data ("91010000F0819BBF0681979F420209785A0854133300896010189F02060000000020009F1B04000013885F280200565F2403141231DF5B0100DF5C03000000DF21050400000000DF2205F850ACF800DF2305FC50ACA000DF770600000000000057115413330089601018D14122200123409172500F505043204D415020303120207632308E1C00000000000000005E031F0300000000000000000000000000000000")
        user.send_emv_ct_data ("91010000F05ABF0757DF7D01009F420209785A0854133300896010189F02060000000020005F2A0200365F3601029F1B0400001388DF2404000001F48E1C00000000000000005E031F0300000000000000000000000000000000DF3903000000")
        user.send_emv_ct_data ("91010000F007BF0104DF700104")
        user.send_emv_ct_data ("91010000F007BF0104DF700102")
        user.send_emv_ct_data ("91010000F007BF0104DF700105")
        user.send_emv_ct_data ("90E30000F082019BDF4204000000029F2701409F360202539F260851C3DBD726D8329E5A0A5413330089601018FFFF9F3901055F24031412319F4104000000015F34010057115413330089601018D141222001234091729F10120110904009248000000000000000000027FF9F3704EF6F9B28950500000000009A031401019F21031200009C01005F2A020036820259009F1A0200369F34035E03009F3303E0F0C89F3501229F1E0831323334353637388407A00000000410109F090200029B02E8009F0607A0000000041010DF29099F3303E0F0C88F01FA9F5301529F02060000000020009F03060000000000009F120F505043204D41502030312020763230DFD0020F505043204D415020303120207632305F25030401015F280200569F4C08353BF1E1C690D5CBDF21050400000000DF2205F850ACF800DF2305FC50ACA0009F0E0500000000009F0F0500000000009F0D0500000000009F4005F000B0A0019F160F202020202020202020202020202020DF6301039F080200025F360102DF5901008E1C00000000000000005E031F0300000000000000000000000000000000")
        user.send_emv_ct_data ("90000000F0098407A0000000041010")
        user.send_emv_ct_data ("90000000F01F9F5301528E0C00000000000000005E031F039F3303E0F0C89F3501229F6600")
        user.send_emv_ct_data ("90030000")
        user.send_emv_ct_data ("90000000")
        user.send_emv_ct_data ("90000000")
        user.send_emv_ct_data ("90000000")
        user.send_emv_ct_data ("90000000")
        user.send_emv_ct_data ("90000000")
        user.send_emv_ct_data ("90000000")
        user.send_emv_ct_data ("90000000")
        user.send_emv_ct_data ("90010000")
        # indicates end of EMV input via VATS
        user.send_emv_ct_data ("FFFF")


        ats.check( vats_json.html_waitDisplayContains( 'Please wait', 0, 20000, 10), 'Please wait failed!!!' )
        ats.check( vats_json.html_waitDisplayContains( 'Signature needed', 0, 90000), 'Signature needed failed!!!' )
        ats.check( vats_json.html_waitDisplayContains( 'Transaction approved', 0, 30000), 'Transaction approved failed!!!' )
        # printing ?
        # ats.check( user.enter_keys( '<CANCEL>' ), 'input transaction appr. failed!!!' )
        myResult = vats_json.html_gui_pressButton( 'CAN1' )

        # Wait until headline indicates "Welcome"
        vats_json.html_waitDisplayContains( "Welcome", 0, 10000 )
        user.send_emv_ct_data ("FFFFFF")

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "2.1" )
ats.set_script_summary( "GUI EMV test VOS CT offline" )
ats.set_recovery_function( gui_back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( gui_emv_test, "GUI EMV test VOS CT offline" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
