#----------------------------------------------------------------------------
#
#    File : gui_emv_vos_ctls_no_host_resp.py
#
#    VATS demo : EMV CTLS testing 
#
#    Author : Reinhard Lieberum
#
#    Creation date : 2014-June
#                    2014-Oct:  - apply vats recording
#
#    Description:
'''Test module: GUI EMV test VOS CTLS no host resp'''

#    CLA_EMV, INS_TERM_CFG :  39 10: don't care, are really executed by EMV
#    CLA_EMV, INS_APPLI_CFG:  39 11: don't care, are really executed by EMV
#    MasterCard Test Card, 100.00, no PIN, no host resp, with host resp
#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *
from vats_html import *


#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def gui_emv_test_ctls1():
    '''GUI EMV test VOS CTLS no host resp'''

    # set keyboard mapping
    terminalIndex   = user.setTerminalSpecificValues()
    holdtime_ms     =   50  # for minimum this holdtime the display must be stable
    responsetime_ms = 3000  # latest after this response time the desired display must appear

    # Remove card to have a defined starting point
    user.remove_smartcard()

    # tear off last receipt in the case there is any
    user.tearoff_printout()

    # Wait until headline indicates "SALE Enter amount:"
    ats.check( vats_json.html_waitDisplayContains( "Welcome1\nEnter amount", 0, default_menu_wait ), 'start failed!!!' )

    vats_json.html_gui_setInput( 'number', '10000', 'in0' )


    #---------------------------------------------------------------------------------------------------
    # Wait until GUI display prompts for card insertion
    if ats.check ( vats_json.html_gui_pressButton( '', vats_json.VATS_KEYB_ENTER ), 'amount input failed!!!'):

        # VATS cleanup
        user.send_emv_ctls_data ("FFFFFF")
        user.send_emv_ctls_data ("90000000F000")
        user.send_emv_ctls_data ("91010000F003BF0C00")
        user.send_emv_ctls_data ("90000000F003DF6100")
        user.send_emv_ctls_data ("90E20000F08201A4DF4204000088029F2701809F3602025E9F26087C59A448C187170F5A0A5413330089601018FFFF9F3901075F24031412319F4104000000005F34010057115413330089601018D141222001234091729F10120110A04009240000000000000000000000FF9F37041373F1DD950500000080009A031408019F21031200009C01005F2A020036820259809F1A0200369F34035E03009F3303E060089F3501229F1E0830323334353637388407A00000000410109F090200029F0607A00000000410109F5301529F02060000000100009F03060000000000009F120F505043204D41502030312020763230DFD0020F505043204D415020303120207632305F25030401015F280200569F4C08FC48AD5A16E7EC0EDF21050000000000DF2205FC509C8800DF2305FC509C88009F0E0500000000009F0F0500000000009F0D0500000000009F40056000B0A0019F160F2020202020202020202020202020205F200C455445432F504159504153539F080200025F360102C204000000039F660436204000DF6420900000913EAB44009080FFFF006390901180020A0000001403000000000004D2")
        user.send_emv_ctls_data ("90000000F045DF81162C1E0400000000000000000000000000000000000000001B000000000000000000000000000000000000000000DF811B0120DF81290830F0F01028F0FF009F7E0101")
        user.send_emv_ctls_data ("90EA0000F08201A4DF4204000008029F2701809F3602025E9F26087C59A448C187170F5A0A5413330089601018FFFF9F3901075F24031412319F4104000000005F34010057115413330089601018D141222001234091729F10120110A04009240000000000000000000000FF9F37041373F1DD950500000080009A031408019F21031200009C01005F2A020036820259809F1A0200369F34035E03009F3303E060089F3501229F1E0830323334353637388407A00000000410109F090200029F0607A00000000410109F5301529F02060000000100009F03060000000000009F120F505043204D41502030312020763230DFD0020F505043204D415020303120207632305F25030401015F280200569F4C08FC48AD5A16E7EC0EDF21050000000000DF2205FC509C8800DF2305FC509C88009F0E0500000000009F0F0500000000009F0D0500000000009F40056000B0A0019F160F2020202020202020202020202020205F200C455445432F504159504153539F080200025F360102C204000000039F660436204000DF6420900000913EAB44009080FFFF006390901180020A0000001403000000000001F0")
        user.send_emv_ctls_data ("90000000F0098407A0000000041010")
        # indicates end of EMV input via VATS
        user.send_emv_ctls_data ("FFFF")

        ats.check( vats_json.html_waitDisplayContains( 'Host Connection', 0, 10000), 'Host Connection failed!!!' )
        # enter host result
        myMenu = vats_json.html_gui_selectMenu( 'No Host Resp' )

        ats.check( vats_json.html_waitDisplayContains( 'No Online\nresponse\nreceived', 0, 20000), 'No Online response received failed!!!' )
        ats.check( vats_json.html_waitDisplayContains( 'Transaction\nterminated', 0, 20000), 'Transaction terminated failed!!!' )
        myResult = vats_json.html_gui_pressButton( 'CAN1' )

        ats.check( vats_json.html_waitDisplayContains( "Welcome", 0, 20000 ), 'return to start failed!!!' )
        # VATS cleanup
        user.send_emv_ctls_data ("FFFFFF")


#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "2.1" )
ats.set_script_summary( "GUI EMV test VOS CTLS no host resp" )
ats.set_recovery_function( gui_back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( gui_emv_test_ctls1, "GUI EMV test VOS CTLS no host resp" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
