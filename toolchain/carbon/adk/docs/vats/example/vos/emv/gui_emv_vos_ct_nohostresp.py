#----------------------------------------------------------------------------
#
#    File : gui_emv_vos_ct_nohostresp.py
#
#    VATS demo : EMV testing
#
#    Author : Reinhard Lieberum
#
#    Creation date : 2014-June
#                    2014-Oct:  - apply vats recording
#
#    Description:
'''Test module: GUI EMV test VOS CT no host resp'''

#    CLA_EMV, INS_TERM_CFG :  39 01: don't care, are really executed by EMV
#    CLA_EMV, INS_APPLI_CFG:  39 02: don't care, are really executed by EMV
#    MasterCard Test Card, 300.00, no PIN, no host response
#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *
from vats_html import *


#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def gui_emv_test():
    '''GUI EMV test VOS CT no host resp'''

    # set keyboard mapping
    terminalIndex = user.setTerminalSpecificValues()
    holdtime_ms     =   50  # for minimum this holdtime the display must be stable
    responsetime_ms = 3000  # latest after this response time the desired display must appear


    # Remove card to have a defined starting point
    user.remove_smartcard()

    # tear off last receipt in the case there is any
    user.tearoff_printout()

    # Wait until headline indicates "Welcome"
    ats.check( vats_json.html_waitDisplayContains( "Welcome", 0, default_menu_wait ), 'Welcome failed!!!' )

    vats_json.html_gui_setInput( 'number', '30000', 'in0' )
    myButton = vats_json.html_gui_pressButton( '', vats_json.VATS_KEYB_ENTER )

    #---------------------------------------------------------------------------------------------------
    # Wait until GUI display prompts for card insertion or tapping
    if ats.check( vats_json.html_waitDisplayContains( 'Insert or Tap\nCard', 0, 10000 ), 'Insert card failed!!!' ):

        # VATS cleanup
        user.send_emv_ct_data ("FFFFFF")
        user.send_emv_ct_data ("90010000")
        user.send_emv_ct_data ("90010000")
        user.send_emv_ct_data ("90010000")
        user.send_emv_ct_data ("90010000")
        user.send_emv_ct_data ("90010000")
        user.send_emv_ct_data ("90010000")
        user.send_emv_ct_data ("90010000")
        user.send_emv_ct_data ("90000000")
        user.send_emv_ct_data ("90000000F015DF68123BE900008131FE454A434F503331563232A0")
        user.send_emv_ct_data ("91010000F007BF0104DF700107")
        user.send_emv_ct_data ("90000000F0588407A0000000041010500F505043204D41502030312020763230DF330F505043204D41502030312020763230DF4E0100FF031BDF0407A0000000041010500F505043204D41502030312020763230DF0407A0000000041010")
        user.send_emv_ct_data ("91010000F007BF0104DF70010B")
        user.send_emv_ct_data ("91010000F007BF0104DF700181")
        user.send_emv_ct_data ("91010000F007BF0104DF700103")
        user.send_emv_ct_data ("91010000F017BF03145A0854133300896010185F3401005F2403141231")
        user.send_emv_ct_data ("91010000F0819BBF0681979F420209785A0854133300896010189F02060000000300009F1B04000013885F280200565F2403141231DF5B0100DF5C03000000DF21050400000000DF2205F850ACF800DF2305FC50ACA000DF770600000000000057115413330089601018D14122200123409172500F505043204D415020303120207632308E1C00000000000000005E031F0300000000000000000000000000000000")
        user.send_emv_ct_data ("91010000F05ABF0757DF7D01009F420209785A0854133300896010189F02060000000300005F2A0200365F3601029F1B0400001388DF2404000001F48E1C00000000000000005E031F0300000000000000000000000000000000DF3903000000")
        user.send_emv_ct_data ("91010000F007BF0104DF700104")
        user.send_emv_ct_data ("91010000F007BF0104DF700102")
        user.send_emv_ct_data ("91010000F007BF0104DF700105")
        user.send_emv_ct_data ("90E20000F082019BDF4204000000029F2701809F360202579F26085B57C78DD26BC1115A0A5413330089601018FFFF9F3901055F24031412319F4104000000015F34010057115413330089601018D141222001234091729F10120110A04009240000000000000000000000FF9F370443524A24950500000080009A031401019F21031200009C01005F2A020036820259009F1A0200369F34035E03009F3303E0F0C89F3501229F1E0831323334353637388407A00000000410109F090200029B02E8009F0607A0000000041010DF29099F3303E0F0C88F01FA9F5301529F02060000000300009F03060000000000009F120F505043204D41502030312020763230DFD0020F505043204D415020303120207632305F25030401015F280200569F4C0887A7430B11A71E9CDF21050400000000DF2205F850ACF800DF2305FC50ACA0009F0E0500000000009F0F0500000000009F0D0500000000009F4005F000B0A0019F160F202020202020202020202020202020DF6301009F080200025F360102DF5901008E1C00000000000000005E031F0300000000000000000000000000000000")
        user.send_emv_ct_data ("91010000F007BF0104DF700106")
        user.send_emv_ct_data ("90E40000F08201ADDF4204000000029F2701009F360202579F260855BF454E2E0010DD5A0A5413330089601018FFFF9F3901055F24031412319F4104000000015F34010057115413330089601018D141222001234091729F1012011020400964000087A700000000000000FF9F3704DF716E5C950500000080009A031401019F21031200009C01005F2A020036820259009F1A0200369F34035E03009F3303E0F0C89F3501229F1E0831323334353637388407A00000000410109F090200029B02E8009F0607A0000000041010DF29099F3303E0F0C88F01FA9F5301529F02060000000300009F03060000000000009F120F505043204D41502030312020763230DFD0020F505043204D415020303120207632305F25030401015F280200569F4C0887A7430B11A71E9CDF21050400000000DF2205F850ACF800DF2305FC50ACA0009F0E0500000000009F0F0500000000009F0D0500000000009F4005F000B0A001DF620F90029000035A330000008000E800019F160F202020202020202020202020202020DF6301049F080200025F360102DF5901008E1C00000000000000005E031F0300000000000000000000000000000000")
        user.send_emv_ct_data ("90000000F0098407A0000000041010")
        user.send_emv_ct_data ("90000000F01F9F5301528E0C00000000000000005E031F039F3303E0F0C89F3501229F6600")
        user.send_emv_ct_data ("90030000")
        user.send_emv_ct_data ("90000000")
        user.send_emv_ct_data ("90000000")
        user.send_emv_ct_data ("90000000")
        user.send_emv_ct_data ("90000000")
        user.send_emv_ct_data ("90000000")
        user.send_emv_ct_data ("90000000")
        user.send_emv_ct_data ("90000000")
        user.send_emv_ct_data ("90000000")
        user.send_emv_ct_data ("90000000")
        user.send_emv_ct_data ("90000000")
        user.send_emv_ct_data ("90000000")
        user.send_emv_ct_data ("90000000")
        user.send_emv_ct_data ("90000000")
        user.send_emv_ct_data ("90010000")
        # indicates end of EMV input via VATS
        user.send_emv_ct_data ("FFFF")


        ats.check( vats_json.html_waitDisplayContains( 'Please wait', 0, 30000, 10), 'Please wait failed!!!' )
        ats.check( vats_json.html_waitDisplayContains( 'Host Connection', 0, 30000), 'Host Connection failed!!!' )
        # enter host result
        myMenu = vats_json.html_gui_selectMenu( 'No Host Resp' )
        ats.check( vats_json.html_waitDisplayContains( 'No Online\nresponse\nreceived', 0, 20000), 'No Online response received failed!!!' )

        ats.check( vats_json.html_waitDisplayContains( 'Transaction declined', 0, 20000), 'Transaction declined failed!!!' )
        # ats.check( user.enter_keys( '<CANCEL>' ), 'input transaction declined. failed!!!' )
        myResult = vats_json.html_gui_pressButton( 'CAN1' )

        # Wait until headline indicates "Welcome"
        vats_json.html_waitDisplayContains( "Welcome", 0, 10000 )
        # VATS cleanup
        user.send_emv_ct_data ("FFFFFF")

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "2.1" )
ats.set_script_summary( "GUI EMV test VOS CT no host resp" )
ats.set_recovery_function( gui_back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( gui_emv_test, "GUI EMV test VOS CT no host resp" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
