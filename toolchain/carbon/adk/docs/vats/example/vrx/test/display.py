#----------------------------------------------------------------------------
#
#    File : display.py
#
#    VATS demo : Display testing
#
#    Author : Dirk Germann
#
#    Creation date : 2012-Oct
#
#    Description:
'''Test module: Text display test'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def display_1():
    '''Text display test with line breaks in the text 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = prepare_test()

    # Choose menu item 2: "Display"
    user.enter_keys( "2" )
    # Wait until headline indicates "Display Menu"
    user.wait_display_contains( disp_menu_title )

    # Choose menu item 1: "display    test  1"
    user.enter_keys( "1" )
    # Wait until headline indicates test start
    user.wait_display_contains( "display test" )

    # Check contents of display
    ats.check( user.does_display_contain( "VATS test" ), "does_display_contain() failed!" )

    # back to main menu
    back_to_main_menu()

def display_2():
    '''Text display test with automatic line breaks 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = prepare_test()

    # Choose menu item 2: "Display"
    user.enter_keys( "2" )
    # Wait until headline indicates "Display Menu"
    user.wait_display_contains( disp_menu_title )

    # Choose menu item 2: "display    test  2"
    user.enter_keys( "2" )
    # Wait until headline indicates test start
    user.wait_display_contains( "1VATS" )

    # Check contents of display
    if( 0 == myIndex ) or ( 11 == myIndex ):
        #----------------------------------------------------------------------------
        # VX520
        ats.check( user.does_display_contain( "1VATS Screen 12345678\n" ), "does_display_contain() failed!" )
        ats.check( user.does_display_contain( "2VATS Screen 12345678\n" ), "does_display_contain() failed!" )
        ats.check( user.does_display_contain( "3VATS Screen 12345678\n" ), "does_display_contain() failed!" )
        ats.check( user.does_display_contain( "4VATS Screen 12345678\n" ), "does_display_contain() failed!" )
        ats.check( user.does_display_contain( "5VATS Screen 12345678\n" ), "does_display_contain() failed!" )
        ats.check( user.does_display_contain( "6VATS Screen 12345678\n" ), "does_display_contain() failed!" )
        ats.check( user.does_display_contain( "7VATS Screen 12345678\n" ), "does_display_contain() failed!" )
        ats.check( user.does_display_contain( "8VATS Screen 12345678\n" ), "does_display_contain() failed!" )
    else:
        #----------------------------------------------------------------------------
        # VX680 and VX820 and VX825 and VX690 and VX685
        if( ( 1 == myIndex ) or ( 2 == myIndex ) or ( 7 == myIndex ) or ( 8 == myIndex ) or ( 19 == myIndex ) ):
            ats.check( user.does_display_contain( "1VATS Screen 123456782VATS Scr\n" ), "does_display_contain() failed!" )
            ats.check( user.does_display_contain( "een 123456783VATS Screen 12345\n" ), "does_display_contain() failed!" )
            ats.check( user.does_display_contain( "6784VATS Screen 123456785VATS\n" ), "does_display_contain() failed!" )
            ats.check( user.does_display_contain( "Screen 123456786VATS Screen 12\n" ), "does_display_contain() failed!" )
            ats.check( user.does_display_contain( "3456787VATS Screen 123456788VA\n" ), "does_display_contain() failed!" )
            ats.check( user.does_display_contain( "TS Screen 12345678\n" ), "does_display_contain() failed!" )
        else:
            #----------------------------------------------------------------------------
            # VX520C, VX675 and e355
            if( ( 4 == myIndex ) or ( 5 == myIndex ) or ( 12 == myIndex ) ):
                ats.check( user.does_display_contain( "1VATS Screen 123456782VATS Screen 123456\n" ), "does_display_contain() failed!" )
                ats.check( user.does_display_contain( "783VATS Screen 123456784VATS Screen 1234\n" ), "does_display_contain() failed!" )
                ats.check( user.does_display_contain( "56785VATS Screen 123456786VATS Screen 12\n" ), "does_display_contain() failed!" )
                ats.check( user.does_display_contain( "3456787VATS Screen 123456788VATS Screen\n" ), "does_display_contain() failed!" )
                ats.check( user.does_display_contain( "12345678\n" ), "does_display_contain() failed!" )
            else:
                #----------------------------------------------------------------------------
                # e315, e315M and e335
                if( ( 13 == myIndex ) or ( 14 == myIndex ) or ( 15 == myIndex ) ):
                    ats.check( user.does_display_contain( "1VATS Screen 12345678\n" ), "does_display_contain() failed!" )
                    ats.check( user.does_display_contain( "2VATS Screen 12345678\n" ), "does_display_contain() failed!" )
                    ats.check( user.does_display_contain( "7VATS Screen 12345678\n" ), "does_display_contain() failed!" )
                    ats.check( user.does_display_contain( "8VATS Screen 12345678\n" ), "does_display_contain() failed!" )
                else:
                    #----------------------------------------------------------------------------
                    # e265
                    if( ( 16 == myIndex ) ):
                        ats.check( user.does_display_contain( "1VATS Screen 123456782VATS\n" ), "does_display_contain() failed!" )
                        ats.check( user.does_display_contain( " Screen 123456783VATS Scre\n" ), "does_display_contain() failed!" )
                        ats.check( user.does_display_contain( "en 123456784VATS Screen 12\n" ), "does_display_contain() failed!" )
                        ats.check( user.does_display_contain( "3456785VATS Screen 1234567\n" ), "does_display_contain() failed!" )
                        ats.check( user.does_display_contain( "86VATS Screen 123456787VAT\n" ), "does_display_contain() failed!" )
                        ats.check( user.does_display_contain( "S Screen 123456788VATS Scr\n" ), "does_display_contain() failed!" )
                        ats.check( user.does_display_contain( "een 12345678\n" ), "does_display_contain() failed!" )
                    else:
                        #----------------------------------------------------------------------------
                        # Unknown terminal
                        ats.check( False, "Unknown terminal" )

    # back to main menu
    back_to_main_menu()

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.12" )
ats.set_script_summary( "Text display testing" )
ats.set_recovery_function( back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( display_1, "Text display test with line breaks in the text 1" )
ats.add_test( display_2, "Text display test with automatic line breaks 1" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
