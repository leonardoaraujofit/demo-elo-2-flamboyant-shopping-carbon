#----------------------------------------------------------------------------
#
#    File : date_time.py
#
#    VATS demo : date and time testing
#
#    Author : Reinhard Lieberum
#
#    Creation date : 2014-Feb
#
#    Description:
'''Test module: Date and time test'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *
from datetime import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#
def update_display( pattern, countdown = 5 ):
    status = False

    # Update display until expected time string is shown or countdown reaches 0
    while( ( countdown > 0 ) and ( not status ) ):
        user.enter_keys( "0" )
        status = user.wait_display_contains( pattern, 0, ( default_wait // 2 ) )
        countdown -= 1

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def datetime_1():
    '''Date and time test 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = prepare_test()

    # Choose menu item 1: "Miscellaneous"
    user.enter_keys( "1" )
    # Wait until headline indicates "Miscellaneous Menu"
    user.wait_display_contains( misc_menu_title )

    # Choose menu item 4: "date and time test"
    user.enter_keys( "4" )
    # Wait until headline indicates test start
    user.wait_display_contains( "Date and Time test" )

    #----------------------------------------------------------------------------
    # set date and time in terminal
    dt = datetime.now()
    # format string from example at http://docs.python.org/3/library/string.html#formatspec
    ats.log_info( "Now       : " + '{:%Y-%m-%d %H:%M:%S}'.format( dt ) )
    if( ( 29 == dt.day ) and ( 2 == dt.month ) ):
        # leap year
        # year, month, day, hour, minute, second
        dt = datetime( (dt.year - 1), dt.month, (dt.day - 1), dt.hour, dt.minute, 0 )
    else:
        # year, month, day, hour, minute, second
        dt = datetime( (dt.year - 1), dt.month, dt.day, dt.hour, dt.minute, 0 )
    # format string from example at http://docs.python.org/3/library/string.html#formatspec
    ats.log_info( "Changed to: " + '{:%Y-%m-%d %H:%M:%S}'.format( dt ) )
    user.set_DateAndTime( dt.year, dt.month, dt.day, dt.hour, dt.minute, 0 )

    # format string from example at http://docs.python.org/3/library/string.html#formatspec
    time_str = '{:%Y%m%d%H%M}'.format( dt )
    # Update display until expected time string is shown
    update_display( time_str )
    # Check contents of display
    ats.check( user.wait_display_contains( time_str ), "wait_display_contains() failed!" )

    #----------------------------------------------------------------------------
    # reset date and time in terminal
    dt = datetime.now()
    # format string from example at http://docs.python.org/3/library/string.html#formatspec
    ats.log_info( "Now       : " + '{:%Y-%m-%d %H:%M:%S}'.format( dt ) )
    user.set_DateAndTime( dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second )

    # format string from example at http://docs.python.org/3/library/string.html#formatspec
    time_str = '{:%Y%m%d%H%M}'.format( dt )
    # Update display until expected time string is shown
    update_display( time_str )
    # Check contents of display
    if( not user.wait_display_contains( time_str ) ):
        # perhaps a minute / hour / day / month / year change occurred:
        # e.g. datetime.now() was '2013-12-31 23:59:59' and now the terminal time is one
        # second later resulting in '2014-01-01 00:00:00'
        # so try once again with the current time
        dt = datetime.now()
        # format string from example at http://docs.python.org/3/library/string.html#formatspec
        ats.log_info( "Now       : " + '{:%Y-%m-%d %H:%M:%S}'.format( dt ) )
        user.set_DateAndTime( dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second )

        # format string from example at http://docs.python.org/3/library/string.html#formatspec
        time_str = '{:%Y%m%d%H%M}'.format( dt )
        # Update display until expected time string is shown
        update_display( time_str )

    # Check contents of display
    ats.check( user.wait_display_contains( time_str ), "wait_display_contains() failed!" )

    # back to main menu
    back_to_main_menu()

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.2" )
ats.set_script_summary( "Date and time testing" )
ats.set_recovery_function( back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( datetime_1, "date and time test 1" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
