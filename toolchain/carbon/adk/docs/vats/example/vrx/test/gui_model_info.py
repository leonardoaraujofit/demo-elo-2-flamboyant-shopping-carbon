#----------------------------------------------------------------------------
#
#    File : gui_model_info.py
#
#    VATS demo : Model info testing
#
#    Author : Achim Groennert
#
#    Creation date : 2013-May
#
#    Description:
'''Test module: GUI model info test'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *
from vats_html import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def gui_model_info_1():
    '''GUI model info test 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = gui_prepare_test()

    # model info list
    #             VX520, VX680, VX820, UX300, VX520C, VX675, MX925, VX825, VX690, V200, P400, VX805, e355, e315, e315M, e335, e265, P200, UX410, VX685, M400, X10, MX915
    my_models = [     0,     1,     2,     3,      4,     5,     6,     7,     8,    9,   10,    11,   12,   13,    14,   15,   16,   17,    18,    19,   20,  21,    22 ]
    # Check if model info was found
    myFound = myIndex in my_models
    if( myFound ):
        ats.log_info( 'Terminal list index: ' + str( myIndex ) + ', "' + user.known_terminals[myIndex][0] + '"' )
    ats.check( myFound, 'Error: Terminal model index (' + str( myIndex ) + ') is unknown!' )

    # back to main menu
    gui_back_to_main_menu()

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.19" )
ats.set_script_summary( "GUI model info testing" )
ats.set_recovery_function(gui_back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( gui_model_info_1, "GUI model info test 1" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
