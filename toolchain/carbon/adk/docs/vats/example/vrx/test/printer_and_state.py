#----------------------------------------------------------------------------
#
#    File : printer_and_state.py
#
#    VATS demo : Printer testing
#
#    Author : Reinhard Lieberum
#
#    Creation date : 2015-Jan
#
#    Description:
'''Test module: Printer and printer state test via DQ driver'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def printer_and_state():
    '''Printer and printer state test'''

    # Wait for idle screen and set terminal specific values
    myIndex = prepare_test()

    #----------------------------------------------------------------------------
    # Check if a printer test is available
    if( user.does_display_contain( "4: Printer" ) ):
        holdTime = 100
        waitTime = 15000

        # in the case of VX820 DUET we have to configure the printer manually
        if( 2 == myIndex ):
            user.configure_text_printer( user.VX820DUET_vrx_default_printer_width, user.VX820DUET_vrx_default_printer_tearbar, user.VX820DUET_vrx_default_printer_lines )

        # Choose menu item 4: "Printer"
        user.enter_keys( "4" )
        # Wait until headline indicates "Printer Menu"
        user.wait_display_contains( print_menu_title )

        # Choose menu item 1: "printer    test  1"
        user.enter_keys( "1" )
        # Wait until headline indicates test start
        user.wait_display_contains( "Printer test" )

        ats.log_info( "set printer state: PC_TEXT_PRT_BUSY" )
        user.set_text_printer_state( PC_TEXT_PRT_BUSY )
        ats.check( user.wait_display_contains( "Status 1: 33", waitTime, holdTime ), "Printer status check 1 failed!" )

        ats.log_info( "set printer state: PC_TEXT_PRT_OKAY" )
        user.set_text_printer_state( PC_TEXT_PRT_OKAY )
        ats.check( user.wait_display_contains( "Status 2: 32", waitTime, holdTime ), "Printer status check 2 failed!" )

        # wait for printing
        user.wait_display_contains( "start printing" )
        user.wait_display_contains( "text printed", waitTime, holdTime )

        # Check contents of receipt
        ats.check( user.does_printout_contain( "VATS END" ), "does_printout_contain() txtPrint3 failed!" )
        ats.check( user.does_printout_contain( "012345678901234567890123456789012345678901\n000000000011111111112222222222333333333344\n" ), "does_printout_contain() txtPrint1 failed!" )
        ats.check( user.does_printout_contain( "VATS printer test!" ), "does_printout_contain() txtPrint2 failed!" )

        # tear off test receipt
        user.tearoff_printout()

        # Wait until back in "Printer Menu"
        user.wait_display_contains( print_menu_title )

        # back to main menu
        back_to_main_menu()

    #----------------------------------------------------------------------------
    else:
        # In the case no printer is available the test fails
        ats.check( False, "No printer available!" )

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.3" )
ats.set_script_summary( "Printer and printer state testing" )
ats.set_recovery_function( back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( printer_and_state, "Printer and printer state test" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
