#----------------------------------------------------------------------------
#
#    File : trace.py
#
#    VATS demo : Trace testing
#
#    Author : Dirk Germann
#
#    Creation date : 2013-Jan
#
#    Description:
'''Test module: Trace test'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def trace_1():
    '''Trace test 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = prepare_test()

    # Choose menu item 1: "Miscellaneous"
    user.enter_keys( "1" )
    # Wait until headline indicates "Miscellaneous Menu"
    user.wait_display_contains( misc_menu_title )

    # clear and activate traces (for all sources)
    user.trace_record()

    # Choose menu item 3: "trace         test"
    user.enter_keys( "3" )
    # Wait until headline indicates test start
    if( ats.check( user.wait_display_contains( "Trace test" ), "Trace test start failed!" ) ):

        # Wait until trace is finished
        if( ats.check( user.wait_display_contains( "Done." ), "Tracing not yet finished!" ) ):

            #----------------------------------------------------------------------------
            # Check traces received from terminal
            ats.check( ( user.get_trace_entries() == 2 ), "Wrong number of trace entries" )
            ats.check( ( user.get_trace_size( index = 0 ) == 19 ), "First trace wrong length" )
            ats.check( ( user.get_trace_string( index = 0 ) == "VATS trace testing!" ), "First trace wrong contents" )

            #----------------------------------------------------------------------------
            # other possibilities to fetch trace content:
            # as string in hex presentation: user.get_trace_hex()
            # as list of bytes: user.get_trace_data()
            ats.check ( ( user.get_trace_size( index = 1 ) == 1592 ), "Second trace wrong length" )

    # clear and deactivate traces (for all sources)
    user.trace_reset()

    # back to main menu
    back_to_main_menu()

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.6" )
ats.set_script_summary( "Trace testing" )
ats.set_recovery_function( back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( trace_1, "Trace test 1" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
