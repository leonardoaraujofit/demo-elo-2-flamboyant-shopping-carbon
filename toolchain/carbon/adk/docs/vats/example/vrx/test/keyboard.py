#----------------------------------------------------------------------------
#
#    File : keyboard.py
#
#    VATS demo : Keyboard testing
#
#    Author : Dirk Germann
#
#    Creation date : 2012-Oct
#
#    Description:
'''Test module: Keyboard test'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def keyboard_1():
    '''Keyboard test, function 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = prepare_test()

    # Choose menu item 3: "User input"
    user.enter_keys( "3" )
    # Wait until headline indicates "Miscellaneous Menu"
    user.wait_display_contains( user_menu_title )

    # Check if the DQ driver is active
    dq_driver_active = False
    if( user.does_display_contain( "PIN entry" ) ):
        dq_driver_active = True

    # Choose menu item 1: "keyboard   test  1"
    user.enter_keys( "1" )
    # Wait until headline indicates test start
    user.wait_display_contains( "keyboard test" )

    #----------------------------------------------------------------------------
    # a to d, alpha and F1 to F4 keys are only available on VX520 and VX805
    if( ( 0 == myIndex ) or ( 11 == myIndex ) ):
        # Press special key "a"
        ats.check( user.enter_keys( "<A>" ), "enter_keys() failed!" )
        # Wait until correct key code is displayed
        ats.check( user.wait_display_contains( "0xe1" ), "wait_display_contains() failed!" )

        # Press special key "b"
        ats.check( user.enter_keys( "<B>" ), "enter_keys() failed!" )
        # Wait until correct key code is displayed
        ats.check( user.wait_display_contains( "0xe2" ), "wait_display_contains() failed!" )

        # Press special key "alpha"
        ats.check( user.enter_keys( "<ALPHA>" ), "enter_keys() failed!" )
        # Wait until correct key code is displayed
        ats.check( user.wait_display_contains( "0x8f" ), "wait_display_contains() failed!" )

        # Press special key "c"
        ats.check( user.enter_keys( "<C>" ), "enter_keys() failed!" )
        # Wait until correct key code is displayed
        ats.check( user.wait_display_contains( "0xe3" ), "wait_display_contains() failed!" )

        # Press special key "d"
        ats.check( user.enter_keys( "<D>" ), "enter_keys() failed!" )
        # Wait until correct key code is displayed
        ats.check( user.wait_display_contains( "0xe4" ), "wait_display_contains() failed!" )

        # Press special key "F1"
        ats.check( user.enter_keys( "<F1>" ), "enter_keys() failed!" )
        # Wait until correct key code is displayed
        ats.check( user.wait_display_contains( "0xfa" ), "wait_display_contains() failed!" )

        # Press special key "F2"
        ats.check( user.enter_keys( "<F2>" ), "enter_keys() failed!" )
        # Wait until correct key code is displayed
        ats.check( user.wait_display_contains( "0xfb" ), "wait_display_contains() failed!" )

        # Press special key "F3"
        ats.check( user.enter_keys( "<F3>" ), "enter_keys() failed!" )
        # Wait until correct key code is displayed
        ats.check( user.wait_display_contains( "0xfc" ), "wait_display_contains() failed!" )

        # Press special key "F4"
        ats.check( user.enter_keys( "<F4>" ), "enter_keys() failed!" )
        # Wait until correct key code is displayed
        ats.check( user.wait_display_contains( "0xfd" ), "wait_display_contains() failed!" )

        # Press key pair 'd + 6'
        ats.check( user.enter_keys( "<KP_D_6>" ), "enter_keys() failed!" )
        # Wait until correct key code is displayed
        ats.check( user.wait_display_contains( "0xd6" ), "wait_display_contains() failed!" )

        # Press key pair 'ALPHA + 6'
        ats.check( user.enter_keys( "<KP_AL_6>" ), "enter_keys() failed!" )
        # Wait until correct key code is displayed
        ats.check( user.wait_display_contains( "0xf6" ), "wait_display_contains() failed!" )

        # Press key pair 'F2 + F4'
        ats.check( user.enter_keys( "<KP_F2_F4>" ), "enter_keys() failed!" )
        if( dq_driver_active == True ):
            # Press "cancel" to leave the password screen
            ats.check( user.enter_keys( cancel_key ), "enter_keys() failed!" )
        else:
            # Wait until correct key code is displayed
            ats.check( user.wait_display_contains( "0xfe" ), "wait_display_contains() failed!" )

        # wait some time to let the terminal process the key presses
        user.wait( 1000 )

    #----------------------------------------------------------------------------
    # a to d and navigation keys are only available on VX520C and VX675
    if( ( 4 == myIndex ) or ( 5 == myIndex ) ):
        # Press special key "a"
        ats.check( user.enter_keys( "<A>" ), "enter_keys() failed!" )
        # Wait until correct key code is displayed
        ats.check( user.wait_display_contains( "0xe1" ), "wait_display_contains() failed!" )

        # Press special key "b"
        ats.check( user.enter_keys( "<B>" ), "enter_keys() failed!" )
        # Wait until correct key code is displayed
        ats.check( user.wait_display_contains( "0xe2" ), "wait_display_contains() failed!" )

        # Press special key "left"
        ats.check( user.enter_keys( "<LEFT>" ), "enter_keys() failed!" )
        # Wait until correct key code is displayed
        ats.check( user.wait_display_contains( "0xdc" ), "wait_display_contains() failed!" )

        # Press special key "up"
        ats.check( user.enter_keys( "<UP>" ), "enter_keys() failed!" )
        # Wait until correct key code is displayed
        ats.check( user.wait_display_contains( "0xda" ), "wait_display_contains() failed!" )

        # Press special key "center"
        ats.check( user.enter_keys( "<CENTER>" ), "enter_keys() failed!" )
        # Wait until correct key code is displayed
        ats.check( user.wait_display_contains( "0xde" ), "wait_display_contains() failed!" )

        # Press special key "down"
        ats.check( user.enter_keys( "<DOWN>" ), "enter_keys() failed!" )
        # Wait until correct key code is displayed
        ats.check( user.wait_display_contains( "0xdb" ), "wait_display_contains() failed!" )

        # Press special key "right"
        ats.check( user.enter_keys( "<RIGHT>" ), "enter_keys() failed!" )
        # Wait until correct key code is displayed
        ats.check( user.wait_display_contains( "0xdd" ), "wait_display_contains() failed!" )

        # Press special key "c"
        ats.check( user.enter_keys( "<C>" ), "enter_keys() failed!" )
        # Wait until correct key code is displayed
        ats.check( user.wait_display_contains( "0xe3" ), "wait_display_contains() failed!" )

        # Press special key "d"
        ats.check( user.enter_keys( "<D>" ), "enter_keys() failed!" )
        # Wait until correct key code is displayed
        ats.check( user.wait_display_contains( "0xe4" ), "wait_display_contains() failed!" )

        # Press key pair 'd + 6'
        ats.check( user.enter_keys( "<KP_D_6>" ), "enter_keys() failed!" )
        # Wait until correct key code is displayed
        ats.check( user.wait_display_contains( "0xd6" ), "wait_display_contains() failed!" )

        # wait some time to let the terminal process the key presses
        user.wait( 1000 )

    #----------------------------------------------------------------------------
    # all following keys are available on all terminals

    # Press special key "*"
    ats.check( user.enter_keys( "<STAR>" ), "enter_keys() failed!" )
    # Wait until correct key code is displayed
    ats.check( user.wait_display_contains( "0xaa" ), "wait_display_contains() failed!" )

    # Press numeric key "4"
    ats.check( user.enter_keys( "4" ), "enter_keys() failed!" )
    # Wait until correct key code is displayed
    ats.check( user.wait_display_contains( "0xb4" ), "wait_display_contains() failed!" )

    # Press special key "correction"
    ats.check( user.enter_keys( "<CORRECT>" ), "enter_keys() failed!" )
    # Wait until correct key code is displayed
    ats.check( user.wait_display_contains( "0x88" ), "wait_display_contains() failed!" )

    #----------------------------------------------------------------------------
    # double key presses and long key presses are not possible for e315 and e335
    if( ( 13 != myIndex ) and ( 15 != myIndex ) ):
        # Press key pair 'CANCEL + 6'
        ats.check( user.enter_keys( "<KP_CAN_6>" ), "enter_keys() failed!" )
        # Wait until correct key code is displayed
        ats.check( user.wait_display_contains( "0xc6" ), "wait_display_contains() failed!" )

        # Press key pair 'CORRECT + 6'
        ats.check( user.enter_keys( "<KP_COR_6>" ), "enter_keys() failed!" )
        # Wait until correct key code is displayed
        ats.check( user.wait_display_contains( "0xa6" ), "wait_display_contains() failed!" )

        # Press key pair 'CONFIRM + 6'
        ats.check( user.enter_keys( "<KP_CON_6>" ), "enter_keys() failed!" )
        # Wait until correct key code is displayed
        ats.check( user.wait_display_contains( "0xe6" ), "wait_display_contains() failed!" )

        # Long press of 'CORRECT'
        ats.check( user.enter_keys( "<BKSP>" ), "enter_keys() failed!" )
        # Wait until correct key code is displayed
        ats.check( user.wait_display_contains( "0x8e" ), "wait_display_contains() failed!" )

        # Press key pair 'CONFIRM + 7'
        ats.check( user.enter_keys( "<KP_CON_7>" ), "enter_keys() failed!" )
        if( dq_driver_active == True ):
            # Press "cancel" to leave the password screen
            ats.check( user.enter_keys( cancel_key ), "enter_keys() failed!" )
        else:
            # Wait until correct key code is displayed
            ats.check( user.wait_display_contains( "0xe7" ), "wait_display_contains() failed!" )

    # back to main menu
    back_to_main_menu()

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.8" )
ats.set_script_summary( "Non-secure keyboard testing" )
ats.set_recovery_function( back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( keyboard_1, "Non-secure keyboard test 1" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
