#----------------------------------------------------------------------------
#
#    File : custom_event.py
#
#    VATS demo : Custom event testing
#
#    Author : Reinhard Lieberum
#
#    Creation date : 2012-Dec
#
#    Description:
'''Test module: Custom event test'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def custom_event_1():
    '''Custom event DUT to PC test 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = prepare_test()

    # Choose menu item 1: "Miscellaneous"
    user.enter_keys( "1" )
    # Wait until headline indicates "Miscellaneous Menu"
    user.wait_display_contains( misc_menu_title )

    #----------------------------------------------------------------------------
    # Clear Custom Event list and check it
    user.clear_custom_event()
    cust_eve1 = user.get_custom_event( 1000 )
    ats.check( len( cust_eve1 ) == 0, "Clear Custom Event list failed !!!" )

    # Choose menu item 1: "DUT to PC cust.ev."
    user.enter_keys( "1" )
    # Wait until headline indicates test start
    user.wait_display_contains( "Custom event test\n Done" )

    #----------------------------------------------------------------------------
    # Check contents of custom event
    cust_eve = user.get_custom_event( 1000 )
    if( ats.check( len( cust_eve ) != 0, "Get Custom Event failed !!!" ) ):
        eve_id = ( cust_eve[0] * 256 ) + cust_eve[1]
        ats.check( eve_id == 1001, "Wrong Custom Event ID !!!" )

    # back to main menu
    back_to_main_menu()

def custom_event_2():
    '''Custom event PC to DUT test 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = prepare_test()

    # Destination application name for the custom event messages
    myAppName = 'TESTAPP'

    # Choose menu item 1: "Miscellaneous"
    user.enter_keys( "1" )
    # Wait until headline indicates "Miscellaneous Menu"
    user.wait_display_contains( misc_menu_title )

    #----------------------------------------------------------------------------
    # Choose menu item 2: "PC to DUT cust.ev."
    user.enter_keys( "2" )
    # Wait until headline indicates test start
    if( ats.check( user.wait_display_contains( 'Waiting for custom' ), "Test start failed !!!" ) ):

        # binary data has to be converted to string
        # in C syntax it would be 'char myData[] = "Hello Terminal!\x00\x01\x02\x04\x03";'
        myData   = 'Hello Terminal! ' + chr( 0 ) + chr( 1 ) + chr( 2 ) + chr( 4 ) + chr( 3 )
        cust_eve = user.set_custom_event( myAppName, 2002, len( myData ), myData )

        # wait for test result screen
        ats.check( user.wait_display_contains( 'Custom event: ' ), "No custom event !!!" )

        # check display content
        ats.check( user.wait_display_contains( r'\\01\\02\\04\\03' ), "Wrong data !!!" )
        ats.check( user.does_display_contain( 'Hello Terminal!' ), "Wrong data !!!" )
        ats.check( user.does_display_contain( 'Data length: ' + str( len( myData ) ) ), "Wrong data !!!" )
        ats.check( user.does_display_contain( 'Custom event: 2002' ), "Wrong data !!!" )


    # back to main menu
    back_to_main_menu()

    # Choose menu item 1: "Miscellaneous"
    user.enter_keys( "1" )
    # Wait until headline indicates "Miscellaneous Menu"
    user.wait_display_contains( misc_menu_title )

    #----------------------------------------------------------------------------
    # Choose menu item 2: "PC to DUT cust.ev."
    user.enter_keys( "2" )
    # Wait until headline indicates test start
    if( ats.check( user.wait_display_contains( 'Waiting for custom' ), "Test start failed !!!" ) ):

        # binary data has to be converted to string
        # in C syntax it would be 'char myData[] = "Second try:\nblablabla";'
        myData   = 'Second try:\nblabla'
        cust_eve = user.set_custom_event( myAppName, 2003, len( myData ), myData )

        # wait for test result screen
        ats.check( user.wait_display_contains( 'Custom event: ' ), "No custom event !!!" )

        # check display content
        ats.check( user.wait_display_contains('Second try:\nblabla'), "Wrong data !!!" )
        ats.check( user.does_display_contain( 'Data length: ' + str( len( myData ) ) ), "Wrong data !!!" )
        ats.check( user.does_display_contain( 'Custom event: 2003' ), "Wrong data !!!" )

    # back to main menu
    back_to_main_menu()

    # Choose menu item 1: "Miscellaneous"
    user.enter_keys( "1" )
    # Wait until headline indicates "Miscellaneous Menu"
    user.wait_display_contains( misc_menu_title )

    #----------------------------------------------------------------------------
    # Choose menu item 2: "PC to DUT cust.ev."
    user.enter_keys( "2" )
    # Wait until headline indicates test start
    if( ats.check( user.wait_display_contains( 'Waiting for custom' ), "Test start failed !!!" ) ):

        # binary data has to be converted to string
        # in C syntax it would be 'char myData[] = "Second try:\nblablabla";'
        myData = '12cond try:\nblabla1\nwokgnwokgnowkgnognqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaiqweifnpwagunpwiounga49nwgriogt9048rg9pwuigrnp98r4paw3n8rg9ihnaw489pa<h9p4h9p8thwokgnwokgnowkgnognqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaiqweifnpwagunpwiounga49nwgriogt9048rg9pwuigrnp98r4paw3n8rg9ihnaw489pa<h9p4h9p8thwokgnwokgnowkgnognqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaiqweifnpwagunpwiounga49nwgriogt9048rg9pwuigrnp98r4paw3n8rg9ihnaw489pa<h9p4h9p8thwokgnwokgnowkgnognqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaiqweifnpwagunpwiounga49nwgriogt9048rg9pwuigrnp98r4paw3n8rg9ihnaw489pa<h9p4h9p8thwokgnwokgnowkgnognqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaiqweifnpwagunpwiounga49nwgriogt9048rg9pwuigrnp98r4paw3n8rg9ihnaw489pa<h9p4h9p8thwokgnwokgnowkgnognqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaiqweifnpwagunpwiounga49nwgriogt9048rg9pwuigrnp98r4paw3n8rg9ihnaw489pa<h9p4h9p8thwokgnwokgnowkgnognqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaiqweifnpwagunpwiounga49nwgriogt9048rg9pwuigrnp98r4paw3n8rg9ihnaw489pa<h9p4h9p8thwokgnwokgnowkgnognqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaiqweifnpwagunpwiounga49nwgriogt9048rg9pwuigrnp98r4paw3n8rg9ihnaw489pa<h9p4h9p8thwokgnwokgnowkgnognqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaiqweifnpwagunpwiounga49nwgriogt9048rg9pwuigrnp98r4paw3n8rg9ihnaw489pa<h9p4h9p8thwokgnwokgnowkgnognqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaiqweifnpwagunpwiounga49nwgriogt9048rg9pwuigrnp98r4paw3n8rg9ihnaw489pa<h9p4h9p8thwokgnwokgnowkgnognqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaiqweifnpwagunpwiounga49nwgriogt9048rg9pwuigrnp98r4paw3n8rg9ihnaw489pa<h9p4h9p8th4321'
        cust_eve = user.set_custom_event( myAppName, 25004, len( myData ), myData )

        # wait for test result screen
        ats.check( user.wait_display_contains( 'Custom event: ' ), "No custom event !!!" )

        # check display content
        if( ( 13 == myIndex ) or ( 14 == myIndex ) or ( 15 == myIndex ) ):
            # e315, e315M and e335 have small displays
            ats.check( user.wait_display_contains('12cond try:\nblabla1'), "Wrong data !!!" )
        else:
            ats.check( user.wait_display_contains('12cond try:\nblabla1\nwokgnwokgnow'), "Wrong data !!!" )
        ats.check( user.does_display_contain( 'Data length: ' + str( len( myData ) ) ), "Wrong data !!!" )
        ats.check( user.does_display_contain( 'Custom event: 25004' ), "Wrong data !!!" )

    # back to main menu
    back_to_main_menu()

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.6" )
ats.set_script_summary( "Custom event testing" )
ats.set_recovery_function( back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( custom_event_1, "Custom event DUT to PC test 1" )
ats.add_test( custom_event_2, "Custom event PC to DUT test 1" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
