#----------------------------------------------------------------------------
#
#    File : model_info.py
#
#    VATS demo : Model info testing
#
#    Author : Achim Groennert
#
#    Creation date : 2013-Jan
#
#    Description:
'''Test module: Terminal model info test'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def model_info_1():
    '''Terminal model info test 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = prepare_test()

    # Check if model info was found
    myFound = ( 0 == myIndex ) or ( 1 == myIndex ) or ( 2 == myIndex ) or ( 4 == myIndex ) or ( 5 == myIndex ) or ( 7 == myIndex ) or ( 8 == myIndex ) or ( 11 == myIndex ) or ( 12 == myIndex ) or ( 13 == myIndex ) or ( 14 == myIndex ) or ( 15 == myIndex ) or ( 16 == myIndex )or ( 19 == myIndex )
    if( myFound ):
        ats.log_info( 'Terminal list index: ' + str( myIndex ) + ', "' + user.known_terminals[myIndex][0] + '"' )
    ats.check( myFound, 'Error: Terminal model index (' + str( myIndex ) + ') is unknown!' )

    # back to main menu
    back_to_main_menu()

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.14" )
ats.set_script_summary( "Terminal model info testing" )
ats.set_recovery_function( back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( model_info_1, "Terminal model info test 1" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
