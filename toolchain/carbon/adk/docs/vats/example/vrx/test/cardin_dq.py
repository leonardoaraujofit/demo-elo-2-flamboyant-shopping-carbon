#----------------------------------------------------------------------------
#
#    File : cardin.py
#
#    VATS demo : ICC card in testing
#
#    Author : Dirk Germann
#
#    Creation date : 2014-Aug
#
#    Description:
'''Test module: ICC card in test'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def cardin_1():
    '''ICC card in test with DQ'''

    # Wait for idle screen and set terminal specific values
    myIndex = prepare_test()

    # Choose menu item 5: "Card reader"
    user.enter_keys( "5" )
    # Wait until headline indicates "Card Reader Menu"
    user.wait_display_contains( card_menu_title )

    # Choose menu item 1: "ICC card in   test"
    user.enter_keys( "1" )
    # Wait until headline indicates test start
    user.wait_display_contains( "Card in test" )

    # Check that no errors occured
    ats.check( not user.does_display_contain( "error" ), "NOT does_display_contain() failed!" )

    #----------------------------------------------------------------------------
    # Insert Card
    user.insert_smartcard()

    # Check displayed cardin state
    ats.check( user.wait_display_contains( "Phys. cardin: in" ), "Insert card failed!!!" )

    #----------------------------------------------------------------------------
    # Remove card
    user.remove_smartcard()

    # Check displayed cardin state
    ats.check( user.wait_display_contains( "Phys. cardin: out" ), "Remove card failed!!!" )

    # Insert Card
    user.insert_hybrid_card( "", "", "" )
    user.engage_hybrid_card( True, False )

    # Check displayed cardin state
    ats.check( user.wait_display_contains( "Phys. cardin: in" ), "Insert card failed!!!" )

    # Remove card
    user.disengage_hybrid_card()
    user.remove_hybrid_card()

    # Check displayed cardin state
    ats.check( user.wait_display_contains( "Phys. cardin: out" ), "Remove card failed!!!" )

    # back to main menu
    back_to_main_menu()

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.1" )
ats.set_script_summary( "ICC card in testing with DQ" )
ats.set_recovery_function( back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( cardin_1, "ICC card in test with DQ" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
