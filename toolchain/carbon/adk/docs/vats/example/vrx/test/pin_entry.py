#----------------------------------------------------------------------------
#
#    File : pin_entry.py
#
#    VATS demo : PIN entry testing
#
#    Author : Achim Groennert
#
#    Creation date : 2013-Jul
#
#    Description:
'''Test module: PIN entry test'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def pin_entry_1():
    '''PIN entry test 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = prepare_test()

    # Choose menu item 3: "User input"
    user.enter_keys( "3" )
    # Wait until headline indicates "Miscellaneous Menu"
    user.wait_display_contains( user_menu_title )

    #----------------------------------------------------------------------------
    if( user.does_display_contain( "PIN entry" ) ):
        # Choose menu item 2: "PIN entry  test  1"
        user.enter_keys( "2" )
        # Wait until headline indicates test start
        if( ats.check( user.wait_display_contains( "PIN entry test:" ), "test start failed!" ) ):
            # Enter 1234 as PIN
            user.enter_keys( "1234<CONFIRM>" )

            # Wait until all digits were read
            user.wait_display_contains( "PIN is" )

            # Check PIN
            ats.check( user.does_display_contain( "1234" ), "does_display_contain() failed!" )

    #----------------------------------------------------------------------------
    else:
        # In the case no DQ driver is available the test fails
        ats.check( False, "No DQ driver available for PIN entry test!" )

    # back to main menu
    back_to_main_menu()

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.2" )
ats.set_script_summary( "PIN entry testing" )
ats.set_recovery_function( back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( pin_entry_1, "PIN entry test 1" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
