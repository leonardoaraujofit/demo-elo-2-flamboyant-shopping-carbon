#----------------------------------------------------------------------------
#
#    File : touch.py
#
#    VATS demo : Touch testing
#
#    Author : Achim Groennert
#
#    Creation date : 2014-Apr
#
#    Description:
'''Test module: Touch test'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def touch_1():
    '''Touch test 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = prepare_test()

    # Choose menu item 3: "User input"
    user.enter_keys( "3" )
    # Wait until headline indicates "Miscellaneous Menu"
    user.wait_display_contains( user_menu_title )

    # Check if the touch is available
    touch_active = False
    if( user.wait_display_contains( "touch      test  1" ) ):
        touch_active = True
    term_type = user.is_touch_display()

    if( touch_active and term_type ):
        #----------------------------------------------------------------------------
        # release touch to be sure the VATS is synchronized
        user.touchRelease()

        # Choose menu item 3: "touch     test  1"
        user.enter_keys( "3" )
        # Wait until headline indicates test start
        user.wait_display_contains( "Touch testing" )

        # Check contents of display
        ats.check( user.does_display_contain( "pen up\n" ), "touchRelease() failed!" )

        #----------------------------------------------------------------------------
        # touch at 42,42
        user.touchCoordinate( 42, 42 )
        # Wait a little bit to let terminal finish
        ats.check( user.wait_display_contains( "y pos:  42\n" ), "touchCoordinate(42, 42) failed!" )

        # Check contents of display
        ats.check( user.does_display_contain( "pen down\n" ), "touchCoordinate(42, 42) failed!" )
        ats.check( user.does_display_contain( "x pos:  42\n" ), "touchCoordinate(42, 42) failed!" )

        #----------------------------------------------------------------------------
        # move to 120,60
        user.touchCoordinate( 120, 60 )
        # Wait a little bit to let terminal finish
        ats.check( user.wait_display_contains( "y pos:  60\n" ), "touchCoordinate(120, 60) failed!" )

        # Check contents of display
        ats.check( user.does_display_contain( "pen down\n" ), "touchCoordinate(120, 60) failed!" )
        ats.check( user.does_display_contain( "x pos: 120\n" ), "touchCoordinate(120, 60) failed!" )

        #----------------------------------------------------------------------------
        # release touch
        user.touchRelease()
        # Wait a little bit to let terminal finish
        ats.check( user.wait_display_contains( "pen up\n" ), "touchRelease() failed!" )

        # Check contents of display
        ats.check( user.does_display_contain( "x pos: 120\n" ), "touchRelease() failed!" )
        ats.check( user.does_display_contain( "y pos:  60\n" ), "touchRelease() failed!" )

    else:
        if( ( not touch_active ) and ( not term_type ) ):
            # terminal does not support touch screen, so mark test as passed
            ats.log_info( "Test case succeeded, as no touch device is available!" )
            ats.check( True, "Test case succeeded, as no touch device is available!" )
        else:
            # something is wrong, as both variables should state the same result
            ats.check( False, "Test case failed due to terminal type, resp. application menu output mismatch!" )
            ats.check( not touch_active, "Terminal application states no touch test available!" )
            ats.check( not term_type, "Terminal type states no touch device available!" )

    # back to main menu
    back_to_main_menu()

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.5" )
ats.set_script_summary( "Touch testing" )
ats.set_recovery_function( back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( touch_1, "Touch test 1" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
