#----------------------------------------------------------------------------
#
#    File : gui_trace.py
#
#    VATS demo : Trace testing
#
#    Author : Achim Groennert
#
#    Creation date : 2013-May
#
#    Description:
'''Test module: GUI trace test'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *
from vats_html import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def gui_trace_1():
    '''GUI trace test, function 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = gui_prepare_test()

    # Choose menu item 1: "Miscellaneous"
    myMenu = vats_json.html_gui_selectMenu( 'Miscellaneous' )
    # Wait until headline indicates "Miscellaneous Menu"
    vats_json.html_waitDisplayContains( back_to_main_menu_item, AppName = 'vats-demo' )
    if( ats.check( vats_json.html_doesDisplayContain( misc_menu_title_gui, AppName = 'vats-demo' ), 'Select "Miscellaneous" menu failed!!!' ) ):

        #----------------------------------------------------------------------------
        # clear and activate traces (for all sources)
        user.trace_record()

        # Choose menu item "trace test" from menu to generate a result automatically
        myMenu = vats_json.html_gui_selectMenu( 'trace         test' )

        #----------------------------------------------------------------------------
        # Wait display indicates test end
        if( ( 13 == myIndex ) or ( 14 == myIndex ) or ( 15 == myIndex ) ):
            # e315, e315M and e335 have small displays
            ats.check( vats_json.html_waitDisplayContains( 'Sent to PC.', hold_time = (default_hold_time_gui // 2 ), AppName = 'vats-demo' ), 'Test end failed!!!' )
        else:
            ats.check( vats_json.html_waitDisplayContains( 'have been sent', hold_time = (default_hold_time_gui // 2 ), AppName = 'vats-demo' ), 'Test end failed!!!' )
        # Check traces received from terminal
        ats.check ( ( user.get_trace_entries() == 2 ), "Wrong number of trace entries" )
        ats.check ( ( user.get_trace_size( index = 0 ) == 19 ), "First trace wrong length" )
        ats.check ( ( user.get_trace_string( index = 0 ) == "VATS trace testing!" ), "First trace wrong content" )
        # other possibilities to fetch trace content:
        # as string in hex presentation: user.get_trace_hex()
        # as list of bytes: user.get_trace_data()
        ats.check ( ( user.get_trace_size( index = 1 ) == 1592 ), "Second trace wrong length" )

    # clear and deactivate traces (for all sources)
    user.trace_reset()

    # back to main menu
    gui_back_to_main_menu()

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.11" )
ats.set_script_summary( "GUI trace testing" )
ats.set_recovery_function( gui_back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( gui_trace_1, "GUI trace test 1" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
