#----------------------------------------------------------------------------
#
#    File : snapshots.py
#
#    VATS demo : Display snapshot testing
#
#    Author : Achim Groennert
#
#    Creation date : 2015-Sep
#
#    Description:
'''Test module: Text display snapshot test'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *
from os import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#
def delete_existing_file( fileName = None ):
    if( None != fileName ):
        if( os.access( fileName, os.W_OK ) ):
            os.remove( fileName )

def check_if_file_exists( fileName = None, maxTimeout = 0 ):
    if( None != fileName ):
        start = user.get_reference_time()
        now   = user.get_reference_time()
        while( ( now - start ) <= maxTimeout ):
            if( os.access( fileName, os.R_OK ) ):
                return True
            user.wait_event()
            now = user.get_reference_time()
    return False

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def snapshot_menus_1():
    '''Text display snapshot test with different menus 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = prepare_test()

    working_directory = user.get_working_directory()
    file1 = working_directory + 'menus_txt_01.png'
    file2 = working_directory + 'menus_txt_02.png'
    # Delete old files, if existent
    delete_existing_file( file1 )
    delete_existing_file( file2 )

    # Choose menu item 1: "Miscellaneous"
    user.enter_keys( "1" )
    # Wait until headline indicates "Miscellaneous Menu"
    my_search = misc_menu_title
    if( ( 13 == myIndex ) or ( 14 == myIndex ) or ( 15 == myIndex ) ):
        # e315, e315M and e335 have small displays, so no title possible
        my_search = '1: DUT to PC cust.ev.'
    user.wait_display_contains( my_search )

    if( ats.check( user.does_display_contain( my_search ), 'Select "Miscellaneous" menu failed!!!' ) ):

        user.take_terminal_display_snapshot( file1 )

        # back to main menu
        back_to_main_menu()

        # Choose menu item 2: "Display"
        user.enter_keys( "2" )
        # Wait until headline indicates "Display Menu"
        user.wait_display_contains( disp_menu_title )

        # Choose menu item 1: "display    test  1"
        user.enter_keys( "1" )
        # Wait until headline indicates test start
        user.wait_display_contains( "display test" )

        if( ats.check( user.does_display_contain( "display test" ), 'Select "display    test  1" menu failed!!!' ) ):

            user.take_terminal_display_snapshot( file2 )

            ats.check( check_if_file_exists( file1, default_print_wait ), "Snapshot 1 failed!" )
            ats.check( check_if_file_exists( file2, default_print_wait ), "Snapshot 2 failed!" )
            ats.log_warning( "Currently there is no mechanism for automated image compare / OCR available! Please compare manually the following files, resp. do OCR manually: " + file1 + " and " + file2 )

    # back to main menu
    back_to_main_menu()


#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.3" )
ats.set_script_summary( "Text display snapshot testing" )
ats.set_recovery_function( back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( snapshot_menus_1, "Text display snapshot test with different menus 1" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
