#----------------------------------------------------------------------------
#
#    File : gui_keyboard_key_injection.py
#
#    VATS demo : Keyboard testing
#
#    Author : Achim Groennert
#
#    Creation date : 2014-Mar
#
#    Description:
'''Test module: GUI keyboard test with key injection'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *
from vats_html import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def gui_keyboard_key_inject_1():
    '''GUI keyboard test 1 key injection with input field'''

    # Wait for idle screen and set terminal specific values
    myIndex = gui_prepare_test()

    # Choose menu item 3: "User input"
    myMenu = vats_json.html_gui_selectMenu( 'User input' )
    # Wait until headline indicates "User Input Menu"
    vats_json.html_waitDisplayContains( back_to_main_menu_item, AppName = 'vats-demo' )
    if( ats.check( vats_json.html_doesDisplayContain( user_menu_title, AppName = 'vats-demo' ), 'Select "User input" menu failed!!!' ) ):

        # Choose menu item "keyboard   test  1" from menu
        myMenu = vats_json.html_gui_selectMenu( 'keyboard   test  1' )

        #---------------------------------------------------------------------------------------------------
        # Wait until GUI display indicates start of test
        if ats.check( vats_json.html_waitDisplayContains( 'Enter amount', AppName = 'vats-demo' ), 'Test start failed!!!' ):

            # Apply "CONFIRM" key press (don't change the input value)
            ats.check( user.enter_keys( '<CONFIRM>' ), 'Input failed!!!' )

            # Wait until GUI display indicates end of test and check content
            if( ( 13 == myIndex ) or ( 14 == myIndex ) or ( 15 == myIndex ) ):
                # e315, e315M and e335 have small displays
                ats.check( vats_json.html_waitDisplayContains( 'Amount: 1', AppName = 'vats-demo' ), 'Input failed!!!' )
            else:
                ats.check( vats_json.html_waitDisplayContains( 'Amount:\n1', AppName = 'vats-demo' ), 'Input failed!!!' )

            #---------------------------------------------------------------------------------------------------
            # Wait until GUI display indicates restart of test
            ats.check( vats_json.html_waitDisplayContains( 'Enter amount', AppName = 'vats-demo' ), 'Test restart failed!!!' )

            # Apply "CORRECT" "CONFIRM" key press sequence (change the input value to 0)
            ats.check( user.enter_keys( '<CORRECT><CONFIRM>' ), 'Input failed!!!' )

            # Wait until GUI display indicates end of test and check content
            if( ( 13 == myIndex ) or ( 14 == myIndex ) or ( 15 == myIndex ) ):
                # e315, e315M and e335 have small displays
                ats.check( vats_json.html_waitDisplayContains( 'Amount: 0', AppName = 'vats-demo' ), 'Input failed!!!' )
            else:
                ats.check( vats_json.html_waitDisplayContains( 'Amount:\n0', AppName = 'vats-demo' ), 'Input failed!!!' )

            #---------------------------------------------------------------------------------------------------
            # Wait until GUI display indicates restart of test
            ats.check( vats_json.html_waitDisplayContains( 'Enter amount', AppName = 'vats-demo' ), 'Test restart failed!!!' )

            # Apply "CORRECT" "9" "0" "0" "CONFIRM" key press sequence (change the input value to 900)
            ats.check( user.enter_keys( '<CORRECT>900<CONFIRM>' ), 'Input failed!!!' )

            # Wait until GUI display indicates end of test and check content
            if( ( 13 == myIndex ) or ( 14 == myIndex ) or ( 15 == myIndex ) ):
                # e315, e315M and e335 have small displays
                ats.check( vats_json.html_waitDisplayContains( 'Amount: 900', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Input failed!!!' )
            else:
                ats.check( vats_json.html_waitDisplayContains( 'Amount:\n900', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Input failed!!!' )

            #---------------------------------------------------------------------------------------------------
            # Wait until GUI display indicates restart of test
            ats.check( vats_json.html_waitDisplayContains( 'Enter amount', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )

    # back to main menu
    gui_back_to_main_menu()

def gui_keyboard_key_inject_2():
    '''GUI keyboard test 2 key injection with buttons'''

    # Wait for idle screen and set terminal specific values
    myIndex = gui_prepare_test()

    # Choose menu item 3: "User input"
    myMenu = vats_json.html_gui_selectMenu( 'User input' )
    # Wait until headline indicates "User Input Menu"
    vats_json.html_waitDisplayContains( back_to_main_menu_item, AppName = 'vats-demo' )
    if( ats.check( vats_json.html_doesDisplayContain( user_menu_title, AppName = 'vats-demo' ), 'Select "User input" menu failed!!!' ) ):

        # Choose menu item "keyboard   test  2" from menu
        myMenu = vats_json.html_gui_selectMenu( 'keyboard   test  2' )

        #---------------------------------------------------------------------------------------------------
        # Wait until GUI display indicates start of test
        if( ats.check( vats_json.html_waitDisplayContains( 'Press any key', AppName = 'vats-demo' ), 'Test start failed!!!' ) ):

            #---------------------------------------------------------------------------------------------------
            # VX520BW and VX805
            if( ( 0 == myIndex ) or ( 11 == myIndex ) ):
                # Apply "F1" key press
                ats.check( user.enter_keys( '<F1>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 129', AppName = 'vats-demo' ), 'html_waitDisplayContains( 129 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                # Apply "F2" key press
                ats.check( user.enter_keys( '<F2>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 130', AppName = 'vats-demo' ), 'html_waitDisplayContains( 130 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                # Apply "F3" key press
                ats.check( user.enter_keys( '<F3>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 131', AppName = 'vats-demo' ), 'html_waitDisplayContains( 131 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                # Apply "F4" key press
                ats.check( user.enter_keys( '<F4>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 132', AppName = 'vats-demo' ), 'html_waitDisplayContains( 132 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                # Press key "a"
                ats.check( user.enter_keys( '<A>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 133', AppName = 'vats-demo' ), 'html_waitDisplayContains( 133 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                # Press key "b"
                ats.check( user.enter_keys( '<B>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 134', AppName = 'vats-demo' ), 'html_waitDisplayContains( 134 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                # Apply "alpha" key press
                ats.check( user.enter_keys( '<ALPHA>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 135', AppName = 'vats-demo' ), 'html_waitDisplayContains( 135 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                # Apply "c" key press
                ats.check( user.enter_keys( '<C>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 136', AppName = 'vats-demo' ), 'html_waitDisplayContains( 136 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                # Apply "d" key press
                ats.check( user.enter_keys( '<D>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 137', AppName = 'vats-demo' ), 'html_waitDisplayContains( 137 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )

            #---------------------------------------------------------------------------------------------------
            # UX100 for UX300 and UX410
            if( ( 3 == myIndex ) or ( 18 == myIndex ) ):
                # Apply "up" key press
                ats.check( user.enter_keys( '<UP>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 134', AppName = 'vats-demo' ), 'html_waitDisplayContains( 134 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                # Apply "down" key press
                ats.check( user.enter_keys( '<DOWN>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 136', AppName = 'vats-demo' ), 'html_waitDisplayContains( 136 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                # Apply "info" key press
                ats.check( user.enter_keys( '<INFO>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 140', AppName = 'vats-demo' ), 'html_waitDisplayContains( 140 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )

            #---------------------------------------------------------------------------------------------------
            # VX520C and VX675
            if( ( 4 == myIndex ) or ( 5 == myIndex ) ):
                # Apply "a" key press
                ats.check( user.enter_keys( '<A>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 133', AppName = 'vats-demo' ), 'html_waitDisplayContains( 133 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                # Apply "b" key press
                ats.check( user.enter_keys( '<B>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 134', AppName = 'vats-demo' ), 'html_waitDisplayContains( 134 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                # Apply "left" key press
                ats.check( user.enter_keys( '<LEFT>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 130', AppName = 'vats-demo' ), 'html_waitDisplayContains( 130 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                # Apply "up" key press
                ats.check( user.enter_keys( '<UP>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 129', AppName = 'vats-demo' ), 'html_waitDisplayContains( 129 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                # Apply "center" key press
                ats.check( user.enter_keys( '<CENTER>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 135', AppName = 'vats-demo' ), 'html_waitDisplayContains( 135 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                # Apply "down" key press
                ats.check( user.enter_keys( '<DOWN>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 132', AppName = 'vats-demo' ), 'html_waitDisplayContains( 132 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                # Apply "right" key press
                ats.check( user.enter_keys( '<RIGHT>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 131', AppName = 'vats-demo' ), 'html_waitDisplayContains( 131 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                # Apply "c" key press
                ats.check( user.enter_keys( '<C>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 136', AppName = 'vats-demo' ), 'html_waitDisplayContains( 136 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                # Apply "d" key press
                ats.check( user.enter_keys( '<D>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 137', AppName = 'vats-demo' ), 'html_waitDisplayContains( 137 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )

            #---------------------------------------------------------------------------------------------------
            # V200 and P200
            if( ( 9 == myIndex ) or ( 17 == myIndex ) ):
                # Apply "a" key press (left func key)
                ats.check( user.enter_keys( '<A>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 133', AppName = 'vats-demo' ), 'html_waitDisplayContains( 133 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                # Apply "left" key press
                ats.check( user.enter_keys( '<LEFT>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 130', AppName = 'vats-demo' ), 'html_waitDisplayContains( 130 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                # Apply "up" key press
                ats.check( user.enter_keys( '<UP>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 129', AppName = 'vats-demo' ), 'html_waitDisplayContains( 129 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                # Apply "down" key press
                ats.check( user.enter_keys( '<DOWN>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 132', AppName = 'vats-demo' ), 'html_waitDisplayContains( 132 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                # Apply "right" key press
                ats.check( user.enter_keys( '<RIGHT>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 131', AppName = 'vats-demo' ), 'html_waitDisplayContains( 131 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                # Apply "d" key press (right func key)
                ats.check( user.enter_keys( '<D>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 137', AppName = 'vats-demo' ), 'html_waitDisplayContains( 137 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )

            #---------------------------------------------------------------------------------------------------
            # VX520BW, VX680, VX820, VX520C, VX675, VX690, VX825, V200, P400, VX805, e355, e315, e315M, e335, e265, P200 and VX685
            if( ( 0 == myIndex ) or ( 1 == myIndex ) or ( 2 == myIndex ) or ( 4 == myIndex ) or ( 5 == myIndex ) or ( 7 == myIndex ) or ( 8 == myIndex ) or ( 9 == myIndex ) or ( 10 == myIndex ) or ( 11 == myIndex ) or ( 12 == myIndex ) or ( 13 == myIndex ) or ( 14 == myIndex ) or ( 15 == myIndex ) or ( 16 == myIndex ) or ( 17 == myIndex ) or ( 19 == myIndex ) ):
                # Apply "*" key press
                ats.check( user.enter_keys( '<STAR>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 138', AppName = 'vats-demo' ), 'html_waitDisplayContains( 138 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                # Apply "#" key press
                ats.check( user.enter_keys( '<HASH>' ), 'Input failed!!!' )
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Entered: 139', AppName = 'vats-demo' ), 'html_waitDisplayContains( 139 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )

            #---------------------------------------------------------------------------------------------------
            # all terminals
            #---------------------------------------------------------------------------------------------------
            # Apply "2" key press
            ats.check( user.enter_keys( '2' ), 'Input failed!!!' )
            # Wait until correct key code is displayed
            ats.check( vats_json.html_waitDisplayContains( 'Entered: 2', AppName = 'vats-demo' ), 'html_waitDisplayContains( 2 ) failed!' )

            #---------------------------------------------------------------------------------------------------
            # Wait until GUI display again indicates start of test
            ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
            # Apply "5" key press
            ats.check( user.enter_keys( '5' ), 'Input failed!!!' )
            # Wait until correct key code is displayed
            ats.check( vats_json.html_waitDisplayContains( 'Entered: 5', AppName = 'vats-demo' ), 'html_waitDisplayContains( 5 ) failed!' )

            #---------------------------------------------------------------------------------------------------
            # Wait until GUI display again indicates start of test
            ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
            # Apply "8" key press
            ats.check( user.enter_keys( '8' ), 'Input failed!!!' )
            # Wait until correct key code is displayed
            ats.check( vats_json.html_waitDisplayContains( 'Entered: 8', AppName = 'vats-demo' ), 'html_waitDisplayContains( 8 ) failed!' )

            #---------------------------------------------------------------------------------------------------
            # Wait until GUI display again indicates start of display test
            ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
            # Apply "0" key press
            ats.check( user.enter_keys( '0' ), 'Input failed!!!' )
            # Wait until correct key code is displayed
            ats.check( vats_json.html_waitDisplayContains( 'Entered: 0', AppName = 'vats-demo' ), 'html_waitDisplayContains( 0 ) failed!' )

            #---------------------------------------------------------------------------------------------------
            # Wait until GUI display again indicates start of test
            ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
            # Apply "CONFIRM" key press
            ats.check( user.enter_keys( '<CONFIRM>' ), 'Input failed!!!' )
            # Wait until correct key code is displayed
            ats.check( vats_json.html_waitDisplayContains( 'Entered: 22', AppName = 'vats-demo' ), 'html_waitDisplayContains( 22 ) failed!' )

            #---------------------------------------------------------------------------------------------------
            # Wait until GUI display again indicates start of test
            ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
            # Apply "CORRECT" key press
            ats.check( user.enter_keys( '<CORRECT>' ), 'Input failed!!!' )
            # Wait until correct key code is displayed
            ats.check( vats_json.html_waitDisplayContains( 'Entered: 21', AppName = 'vats-demo' ), 'html_waitDisplayContains( 21 ) failed!' )

            #---------------------------------------------------------------------------------------------------
            # Wait until GUI display again indicates start of test
            ats.check( vats_json.html_waitDisplayContains( 'Press any key', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
            # Apply "CANCEL" key press
            ats.check( user.enter_keys( '<CANCEL>' ), 'Input failed!!!' )
            # Wait until correct key code is displayed
            ats.check( vats_json.html_waitDisplayContains( 'Entered: 20', AppName = 'vats-demo' ), 'html_waitDisplayContains( 20 ) failed!' )

    #---------------------------------------------------------------------------------------------------
    # back to main menu
    gui_back_to_main_menu()

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.14" )
ats.set_script_summary( "GUI keyboard test with key injection" )
ats.set_recovery_function( gui_back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( gui_keyboard_key_inject_1, "GUI keyboard test 1 key injection with input field" )
ats.add_test( gui_keyboard_key_inject_2, "GUI keyboard test 2 key injection with buttons" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
