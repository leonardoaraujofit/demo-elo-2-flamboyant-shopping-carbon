#----------------------------------------------------------------------------
#
#    File : gui_touch.py
#
#    VATS demo : Touch testing
#
#    Author : Achim Groennert
#
#    Creation date : 2014-Apr
#
#    Description:
'''Test module: GUI touch test'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *
from vats_html import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def gui_touch_1():
    '''GUI touch test 1 with buttons'''

    # Wait for idle screen and set terminal specific values
    myIndex = gui_prepare_test()

    # Choose menu item 3: "User input"
    myMenu = vats_json.html_gui_selectMenu( 'User input' )
    # Wait until headline indicates "User Input Menu"
    vats_json.html_waitDisplayContains( back_to_main_menu_item, AppName = 'vats-demo' )
    if( ats.check( vats_json.html_doesDisplayContain( user_menu_title, AppName = 'vats-demo' ), 'Select "User input" menu failed!!!' ) ):

        # Check if the touch is available
        touch_active = False
        if( vats_json.html_doesDisplayContain( "touch      test  1", AppName = 'vats-demo' ) ):
            touch_active = True
        term_type = user.is_touch_display()

        if( touch_active and term_type ):
            #----------------------------------------------------------------------------
            # release touch to be sure the VATS is synchronized
            user.touchRelease()

            # Choose menu item "touch      test  1" from menu
            myMenu = vats_json.html_gui_selectMenu( 'touch      test  1' )

            #---------------------------------------------------------------------------------------------------
            # Wait until GUI display indicates start of test
            if ats.check( vats_json.html_waitDisplayContains( 'Touch any button', AppName = 'vats-demo' ), 'Test start failed!!!' ):

                #---------------------------------------------------------------------------------------------------
                if( 6 == myIndex ):
                    # Razor
                    # touch at 44,15
                    user.touchCoordinate( 44, 15 )
                else:
                    if( ( 1 == myIndex ) or ( 2 == myIndex ) or ( 7 == myIndex ) or ( 8 == myIndex ) or ( 19 == myIndex ) or ( 22 == myIndex ) ):
                        # VX680 or VX820 or VX825 or VX690 or VX685
                        # touch at 36,14
                        user.touchCoordinate( 36, 14 )
                    else:
                        if( ( 10 == myIndex ) ):
                            # P400
                            # touch at 47,15
                            user.touchCoordinate( 47, 15 )
                        else:
                            if( ( 21 == myIndex ) ):
                                # X10
                                user.touchCoordinate( 38, 24 )
                user.wait( 2000 )
                user.touchRelease()
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Touched: 1', AppName = 'vats-demo' ), 'html_waitDisplayContains( 1 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Touch any button', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                if( 6 == myIndex ):
                    # Razor
                    # touch at 44,75
                    user.touchCoordinate( 44, 75 )
                else:
                    if( ( 1 == myIndex ) or ( 2 == myIndex ) or ( 7 == myIndex ) or ( 8 == myIndex ) or ( 19 == myIndex ) or ( 22 == myIndex ) ):
                        # VX680 or VX820 or VX825 or VX690 or VX685
                        # touch at 36,63
                        user.touchCoordinate( 36, 63 )
                    else:
                        if( ( 10 == myIndex ) ):
                            # P400
                            # touch at 47,75
                            user.touchCoordinate( 47, 75 )
                        else:
                            if( ( 21 == myIndex ) ):
                                # X10
                                user.touchCoordinate( 38, 120 )
                user.wait( 2000 )
                user.touchRelease()
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Touched: 4', AppName = 'vats-demo' ), 'html_waitDisplayContains( 4 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Touch any button', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                if( 6 == myIndex ):
                    # Razor
                    # touch at 44,75
                    user.touchCoordinate( 44, 135 )
                else:
                    if( ( 1 == myIndex ) or ( 2 == myIndex ) or ( 7 == myIndex ) or ( 8 == myIndex ) or ( 19 == myIndex ) or ( 22 == myIndex ) ):
                        # VX680 or VX820 or VX825 or VX690 or VX685
                        # touch at 36,117
                        user.touchCoordinate( 36, 117 )
                    else:
                        if( ( 10 == myIndex ) ):
                            # P400
                            # touch at 47,135
                            user.touchCoordinate( 47, 135 )
                        else:
                            if( ( 21 == myIndex ) ):
                                # X10
                                user.touchCoordinate( 38, 200 )
                user.wait( 2000 )
                user.touchRelease()
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Touched: 7', AppName = 'vats-demo' ), 'html_waitDisplayContains( 7 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Touch any button', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                if( 6 == myIndex ):
                    # Razor
                    # touch at 44,75
                    user.touchCoordinate( 134, 195 )
                else:
                    if( ( 1 == myIndex ) or ( 2 == myIndex ) or ( 7 == myIndex ) or ( 8 == myIndex ) or ( 19 == myIndex ) or ( 22 == myIndex ) ):
                        # VX680 or VX820 or VX825 or VX690 or VX685
                        # touch at 122,167
                        user.touchCoordinate( 122, 167 )
                    else:
                        if( ( 10 == myIndex ) ):
                            # P400
                            # touch at 157,195
                            user.touchCoordinate( 157, 195 )
                        else:
                            if( ( 21 == myIndex ) ):
                                # X10
                                user.touchCoordinate( 38, 280 )
                user.wait( 2000 )
                user.touchRelease()
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Touched: 0', AppName = 'vats-demo' ), 'html_waitDisplayContains( 0 ) failed!' )

                #---------------------------------------------------------------------------------------------------
                # Wait until GUI display again indicates start of test
                ats.check( vats_json.html_waitDisplayContains( 'Touch any button', response_time = ( default_menu_wait * 2 ), AppName = 'vats-demo' ), 'Test restart failed!!!' )
                if( 6 == myIndex ):
                    # Razor
                    # touch at 44,75
                    user.touchCoordinate( 44, 254 )
                else:
                    if( ( 1 == myIndex ) or ( 2 == myIndex ) or ( 7 == myIndex ) or ( 8 == myIndex ) or ( 19 == myIndex ) or ( 22 == myIndex ) ):
                        # VX680 or VX820 or VX825 or VX690 or VX685
                        # touch at 41,221
                        user.touchCoordinate( 41, 221 )
                    else:
                        if( ( 10 == myIndex ) ):
                            # P400
                            # touch at 43,255
                            user.touchCoordinate( 43, 255 )
                        else:
                            if( ( 21 == myIndex ) ):
                                # X10
                                user.touchCoordinate( 38, 376 )
                user.wait( 2000 )
                user.touchRelease()
                # Wait until correct key code is displayed
                ats.check( vats_json.html_waitDisplayContains( 'Touched: 20', AppName = 'vats-demo' ), 'html_waitDisplayContains( 20 ) failed!' )

        else:
            if( ( not touch_active ) and ( not term_type ) ):
                # terminal does not support touch screen, so mark test as passed
                ats.log_info( "Test case succeeded, as no touch device is available!" )
                ats.check( True, "Test case succeeded, as no touch device is available!" )
            else:
                # something is wrong, as both variables should state the same result
                ats.check( False, "Test case failed due to terminal type, resp. application menu output mismatch!" )
                ats.check( not touch_active, "Terminal application states no touch test available!" )
                ats.check( not term_type, "Terminal type states no touch device available!" )

    #---------------------------------------------------------------------------------------------------
    # back to main menu
    gui_back_to_main_menu()

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.10" )
ats.set_script_summary( "GUI touch test" )
ats.set_recovery_function( gui_back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( gui_touch_1, "GUI touch test 1 with buttons" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
