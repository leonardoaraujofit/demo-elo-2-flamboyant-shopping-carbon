#----------------------------------------------------------------------------
#
#    File : restart_terminal.py
#
#    VATS demo : Restart terminal
#
#    Author : Dirk Germann
#
#    Creation date : 2013-Jan
#
#    Description:
'''Test module: Restart terminal test'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def restart_terminal_1():
    '''Restart terminal test 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = prepare_test()

    # Choose menu item 1: "Miscellaneous"
    user.enter_keys( "1" )
    # Wait until headline indicates "Miscellaneous Menu"
    user.wait_display_contains( misc_menu_title )

    # restart terminal
    ats.log_info( "restarting terminal now..." )
    user.restart_terminal()

    # Check that terminal returns to idle screen (reconnect after 7 minutes and 5 seconds default timeout)
    ats.check( user.wait_display_contains( main_menu_title, 425000 ), "wait_display_contains() failed!" )

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.5" )
ats.set_script_summary( "Restart terminal testing" )
ats.set_recovery_function( back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( restart_terminal_1, "Restart terminal test 1" )

#---------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
