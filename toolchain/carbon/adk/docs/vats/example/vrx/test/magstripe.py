#----------------------------------------------------------------------------
#
#    File : magstripe.py
#
#    VATS demo : Magnetic stripe testing
#
#    Author : Dirk Germann
#
#    Creation date : 2012-Oct
#
#    Description:
'''Test module: Magnetic stripe test'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def magstripe_1():
    '''Magnetic stripe test 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = prepare_test()

    # Set timeout values
    responsetime_ms = 5000   # latest after this response time the desired display must appear
    holdtime_ms     = 500    # for minimum this hold time the display must be stable

    # Choose menu item 5: "Card reader"
    user.enter_keys( "5" )
    # Wait until headline indicates "Card Reader Menu"
    user.wait_display_contains( card_menu_title )

    # Choose menu item 2: "MSR read      test"
    user.enter_keys( "2" )
    # Wait until headline indicates test start
    user.wait_display_contains( "MSR test:" )

    # Check that no MSR data was already read
    if( ( 13 == myIndex ) or ( 14 == myIndex ) or ( 15 == myIndex ) ):
        # e315, e315M and e335 have small displays
        ats.check( user.does_display_contain( 'MSR test:\n' ), "does_display_contain() failed!!!" )
    else:
        ats.check( not user.does_display_contain( "Total Bytes:" ), "NOT wait_display_contains() failed!" )

    #----------------------------------------------------------------------------
    # Swipe Card
    track1 = ""
    track2 = "4761739001010010=15122011143857589"
    track3 = ""
    user.swipe_magcard( track1 + "|" + track2 + "|" + track3 )

    # Check displayed MSR data
    pattern = "TK#1:%03d 00-NO ERR\s+TK#2:%03d 00-NO ERR\s+TK#3:%03d 00-NO ERR" % ( len( track1 ), len( track2 ), len( track3 ) )
    # take care: responsetime_ms must be large enough to include holdtime_ms
    ats.check( user.wait_display_contains( pattern, responsetime_ms, holdtime_ms ), "Card swipe failed!!!" )

    # Check that magnetic stripe data is read
    if( ( 13 == myIndex ) or ( 14 == myIndex ) or ( 15 == myIndex ) ):
        # e315, e315M and e335 have small displays
        ats.check( user.does_display_contain( 'MSR test:  40 \(' ), "does_display_contain() failed!!!" )
    else:
        ats.check( user.does_display_contain( 'Total Bytes:   40' ), "does_display_contain() failed!!!" )

    #----------------------------------------------------------------------------
    # Swipe next Card
    track1 = "11003132333D4A4F484E3D534D4954487802010201"
    track2 = "6726672910070115390D07091010274904490F"
    track3 = "015966666666D10070115392D2809540000000007001003012749020000007090232483411DD1D0000449000005020F"
    user.insert_hybrid_card( track1, track2, track3 )
    user.engage_hybrid_card( False, True )

    # Check displayed MSR data
    pattern = "TK#1:%03d 00-NO ERR\s+TK#2:%03d 00-NO ERR\s+TK#3:%03d 00-NO ERR" % ( len( track1 ), len( track2 ), len( track3 ) )
    # take care: responsetime_ms must be large enough to include holdtime_ms
    ats.check( user.wait_display_contains( pattern, responsetime_ms, holdtime_ms ), "Card swipe failed!!!" )

    # Check that magnetic stripe data is read
    if( ( 13 == myIndex ) or ( 14 == myIndex ) or ( 15 == myIndex ) ):
        # e315, e315M and e335 have small displays
        ats.check( user.does_display_contain( 'MSR test: 181 \(' ), "does_display_contain() failed!!!" )
    else:
        ats.check( user.does_display_contain( 'Total Bytes:  181' ), "does_display_contain() failed!!!" )

    # Remove next card
    user.disengage_hybrid_card()
    user.remove_hybrid_card()

    # back to main menu
    back_to_main_menu()

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.7" )
ats.set_script_summary( "Magnetic stripe testing" )
ats.set_recovery_function( back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( magstripe_1, "Magnetic stripe test 1" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
