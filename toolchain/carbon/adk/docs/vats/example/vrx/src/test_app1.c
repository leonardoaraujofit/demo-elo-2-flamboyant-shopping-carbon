/*****************************************************************************/
/**
 * \file test_app1.c
 *
 * \author GSS R&D Germany
 *
 * Company: VeriFone GmbH
 *
 * Product: Verifone Automated Test System (VATS)
 *
 *       (c) Copyright 2012
 */
/*****************************************************************************/


#ifndef _VOS


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <libvoy.h>
#include <svc.h>
#include <eoslog.h>
#include <svc_sec.h>               // PIN entry
#include "atsmisc.h"
#include "atslib.h"
#include <svc_net.h>


#define APPNAME                    "TESTAPP"
#define LOG_APPFILTER              0x00000001L
#define KEYBOARD_BUF_MAX           22

#define RSA_RESULT_SIZE            248
#define EMV_SCRIPT_NO              0
#define MACRO_GET_CLEARTEXT_PIN    0x10


static char printerAvailable       = 'X';
static char keys[KEYBOARD_BUF_MAX] = {0};
static int  keys_len               = sizeof( keys );
static char bSmallDisplay          = 0;
const char  tType_e315[]           = "e315        ";
const char  tType_e335[]           = "e335        ";
static char first_status           = 0xFF;


// =================================================================================================
// read manufacturing block in order to determine if a printer is available
int PrinterAvailable( void )
{
  int  result    = 0;
  char myPrinter = '0';


  if( 'X' == printerAvailable )
  {
    result = SVC_INFO_PRNTR( &myPrinter );
    if( 0 == result )
    {
      printerAvailable = myPrinter;
    }
    if( '0' == printerAvailable )
    {
      char model[20];


      memset( model, 0, sizeof( model ) );
      if( 0 == SVC_INFO_MODELNO( model ) )
      {
        if( ( 0 == strncmp( "VX820", model, strlen( "VX820" ) ) ) && ( get_usb_device_bits() & ( UDB_IBHUB | UDB_COM4 ) ) )
        {
          // set value to '4' to recognize that a printer is present
          printerAvailable = '4';
        }
      }
    }
  }
  // value is '2' on VX675, DOC00301_Verix_eVo_Volume_I_Operating_System_Programmers_Manual.pdf says 0 or 1.
  // value is '4' for VX820 DUET (see above)
  if( '0' != printerAvailable )
  {
    result = 1;
  }
  else
  {
    result = 0;
  }
  return result;
}


// =================================================================================================
// read environment variable in order to determine the routing (DQ driver / pipe) for this event type
int EventViaDQDriver (enum AtsEvent event)
{
  int  n  = 0;
  char is = '0';


  switch (event)
  {
    case ATSEVENT_TERMINAL_DISPLAY: n = get_env ("*VATS_DISP_VIA_DQDRV",      &is, 1); break;
    case ATSEVENT_TERMINAL_PRINTER: n = get_env ("*VATS_PRINTER_VIA_DQDRV",   &is, 1); break;
    case ATSEVENT_USER_KEYBOARD:    n = get_env ("*VATS_KBD_VIA_DQDRV",       &is, 1); break;
    case ATSEVENT_USER_MAGSTRIPE:   n = get_env ("*VATS_MAGSTRIPE_VIA_DQDRV", &is, 1); break;
    case ATSEVENT_USER_ICCCARD_IN:  n = get_env ("*VATS_CARDIN_VIA_DQDRV",    &is, 1); break;
    case ATSEVENT_USER_TOUCH:       n = get_env ("*VATS_TOUCH_VIA_DQDRV",     &is, 1); break;
    default:
         LOG_PRINTF ("test_app1: EventViaDQDriver: invalid parameter value\n");
         return 1;
  }

  if (n==1 && is=='0')
    return 0;
  else
    return 1;
} // EventViaDQDriver


// =================================================================================================
int touchScreenAvailable( void )
{
  int devices = 0;
  int retVal  = 0;


  devices = SVC_INFO_PRESENT();
  vDebugOutVats( __MODULE__, __LINE__, "devices=0x%08X", devices );
  if( ( devices & INFO_PRES_TOUCH ) != 0 )
  {
    retVal = 1;
  }
  return retVal;
}


// =================================================================================================
// * helper function in order to get the keyboard input from OS / DQ driver or
// * the keyboard input via pipe from VATS handler.
int vats_keyboardGet (int console, char *keys, int *len)
{
  long          event         = 0;
  enum AtsEvent ats_event     = ATSEVENT_NONE;
  char          dataBuf [500] = {0};
  int           datalen       = sizeof (dataBuf);


  // handle (physical) pending keyboard input
  if (kbd_pending_count () > 0)
  {
    int ret = 0;


    ret = read (console, keys, *len);
    if (ret > 0)
    {
      vDebugOutVats( __MODULE__, __LINE__, "vats_keyboardGet: get key from console: keys=%d ret=%d.", keys[0], ret );
      *len = ret;
      return ATSERROR_OK;// key available
    }
  }

  while (1)
  {
    event = wait_evt (EVT_KBD | EVT_USER); // waits til event occurs

    if ( AtsIsActive () && (0 == EventViaDQDriver (ATSEVENT_USER_KEYBOARD)) && (event & EVT_USER) )
    {
      // VATS is active but DQ driver is inactive
      ats_event = ATSEVENT_NONE;
      datalen   = sizeof (dataBuf) - 1;

      if ( ATSERROR_OK == AtsReceiveEvent( &ats_event, 500, &datalen, dataBuf ) )
      {
        if (ats_event == ATSEVENT_USER_KEYBOARD)
        {
          if (datalen > sizeof (keys))
            datalen = sizeof (keys);
          memcpy (keys, dataBuf, datalen);
          *len = datalen;
          vDebugOutVats( __MODULE__, __LINE__, "vats_keyboardGet: RECEIVED via PIPE: *keys=%d.", (int)*keys );
          break; // key available
        }
      }
    }
    else if (event & EVT_KBD)
    {
      int ret = 0;


      ret = read (console, keys, *len);
      if (ret > 0)
      {
        vDebugOutVats( __MODULE__, __LINE__, "vats_keyboardGet: get key from console: keys=%d ret=%d.", keys[0], ret );
        *len = ret;
        break; // key available
      }
    }
  } // while

  vDebugOutVats( __MODULE__, __LINE__, "vats_keyboardGet: DONE" );
  return ATSERROR_OK;
} // vats_keyboardGet


// =================================================================================================
// * Helper function in order to get custom events via pipe, or the keyboard input from
// * OS / DQ driver or via pipe from VATS handler.
int vats_customEventOrKeyboardGet( int console, char *keys, int *len, unsigned short *usCustomEvent, char *eventData, int *eventDataLen )
{
  if( AtsIsActive() && ( NULL != keys ) && ( NULL != len ) && ( 0 < *len ) && ( NULL != usCustomEvent ) && ( NULL != eventData ) && ( NULL != eventDataLen ) && ( 0 < eventDataLen ) )
  {
    long          event;
    enum AtsEvent ats_event = ATSEVENT_NONE;
    char         *dataBuf   = NULL;
    int           datalen   = 0;


    // handle (physical) pending keyboard input
    if( kbd_pending_count () > 0 )
    {
      int ret = 0;


      ret = read( console, keys, *len );
      if( 0 < ret )
      {
        vDebugOutVats( __MODULE__, __LINE__, "vats_customEventOrKeyboardGet: key from console: keys=%d ret=%d.", keys[0], ret );
        *len = ret;
        *eventDataLen = 0;
        return ATSERROR_OK;
      }
    }

    while( 1 )
    {
      event = wait_evt( EVT_KBD | EVT_USER );   // waits until any of the specified events occurs

      if( event & EVT_USER )
      {
        ats_event = ATSEVENT_NONE;
        datalen   = 0;
        if( NULL != dataBuf )
        {
          free( dataBuf );
          dataBuf = NULL;
        }

        vDebugOutVats( __MODULE__, __LINE__, "vats_customEventOrKeyboardGet: AtsReceiveEventChained (%d/%d/%d/%d;%d/%d).", &ats_event, &datalen, &dataBuf, dataBuf, (void **)(&dataBuf), *((void **)(&dataBuf)) );
        if( ATSERROR_OK == AtsReceiveEventChained( &ats_event, 0, &datalen, (void **)(&dataBuf) ) )
        {
          vDebugOutVats( __MODULE__, __LINE__, "vats_customEventOrKeyboardGet: AtsReceiveEventChained ats_event=%d.", ats_event );
          if( ( ats_event == ATSEVENT_USER_KEYBOARD ) && ( 0 == EventViaDQDriver( ATSEVENT_USER_KEYBOARD ) ) )
          {
            if( datalen > *len )
            {
              datalen = *len;
            }
            memcpy( keys, dataBuf, datalen );
            *len          = datalen;
            *eventDataLen = 0;
            vDebugOutVats( __MODULE__, __LINE__, "vats_customEventOrKeyboardGet: key via pipe: keys=%d.", keys[0] );
            break;
          }
          else
          {
            if( ( ats_event > ATSEVENT_CUSTOM_START ) && ( ats_event < ATSEVENT_CUSTOM_END ) )
            {
              if( datalen > *eventDataLen )
              {
                vDebugOutVats( __MODULE__, __LINE__, "vats_customEventOrKeyboardGet: custom event truncation: datalen=%d, eventDataLen=%d.", datalen, *eventDataLen );
                datalen = *eventDataLen;
              }
              memcpy( eventData, dataBuf, datalen );
              *len           = 0;
              *eventDataLen  = datalen;
              *usCustomEvent = ats_event;
              vDebugOutVats( __MODULE__, __LINE__, "vats_customEventOrKeyboardGet: custom event: eventId=%d, len=%d.", *usCustomEvent, *eventDataLen );
              break;
            }
          }
          if( NULL != dataBuf )
          {
            free( dataBuf );
            dataBuf = NULL;
          }
        }
      }
      else if( event & EVT_KBD )
      {
        int ret = 0;


        ret = read( console, keys, *len );
        if( 0 < ret )
        {
          vDebugOutVats( __MODULE__, __LINE__, "vats_customEventOrKeyboardGet: key from console: keys=%d ret=%d.", keys[0], ret );
          *len = ret;
          *eventDataLen = 0;
          break;
        }
      }
    }
    if( NULL != dataBuf )
    {
      free( dataBuf );
      dataBuf = NULL;
    }
    return ATSERROR_OK;
  }
  return ATSERROR_INVALID_PARAMETER;
}


// =================================================================================================
int vats_clrscr (void)
{
  int  res = 0;
  char dummybuffer [3] = " ";


  res = clrscr ();
  if ( AtsIsActive () && (0 == EventViaDQDriver (ATSEVENT_TERMINAL_DISPLAY)) && (res == 0) )
  {
    if ( ATSERROR_OK == AtsSendDisplayText (strlen( dummybuffer ), dummybuffer, 0, 0) )
      return 0;
    else
      return -1;
  }
  return res;
} // vats_clrscr


// =================================================================================================
/** VATS write_at
 * \param[in]  buf      pointer to data to write
 * \param[in]  len      number of bytes in buf to write
 * \param[in]  x        x (column) coordinate to start write
 * \param[in]  y        y (row   ) coordinate to start write
 * \return  ATSERROR_OK on success, else error */
int vats_write_at (char *buf, int len, int x, int y)
{
  int res;


  res = write_at (buf, len, x, y);

  if ( AtsIsActive () && (0 == EventViaDQDriver (ATSEVENT_TERMINAL_DISPLAY)) && (res >= 0) )
    ( ATSERROR_OK == AtsSendDisplayText (len, buf, x, y) ) ? (res = len) : (res = -1);

  return res;
} // vats_write_at


// =================================================================================================
void display_test (void)
{
  int row = 1;
  char txt  [23] = {0};


  vDebugOutVats( __MODULE__, __LINE__, "display_test: called" );
  vats_clrscr ();
  sprintf (txt, "display test");
  vats_write_at (txt, strlen(txt), 1, row++);
  sprintf (txt, "VATS test");
  vats_write_at (txt, strlen(txt), 1, row++);
  sprintf (txt, " VATS test");
  vats_write_at (txt, strlen(txt), 1, row++);
  sprintf (txt, "  VATS test");
  vats_write_at (txt, strlen(txt), 1, row++);
  if( 0 == bSmallDisplay )
  {
    sprintf (txt, "   VATS test");
    vats_write_at (txt, strlen(txt), 1, row++);
    sprintf (txt, "    VATS test");
    vats_write_at (txt, strlen(txt), 1, row++);
    sprintf (txt, "     VATS test");
    vats_write_at (txt, strlen(txt), 1, row++);
    sprintf (txt, "      VATS test");
    vats_write_at (txt, strlen(txt), 1, row++);
  }
  SVC_WAIT (3000);
}


// =================================================================================================
void keyboard_test (int console)
{
  int  i, len;
  char keys [KEYBOARD_BUF_MAX];
  char keyhex [] = "0xXX               \n";


  vDebugOutVats( __MODULE__, __LINE__, "keyboard_test: called" );

  vats_clrscr ();
  vats_write_at ("keyboard test:",   sizeof ("keyboard test:")   - 1, 1, 1);
  vats_write_at ("CANCEL to abort:", sizeof ("CANCEL to abort:") - 1, 1, 2);

  for (;;)
  {
    vats_write_at ("Press any key: ",    sizeof ("Press any key: ")    - 1, 1, 3);
    // TODO: speed up ??? :SVC_WAIT (200); // msec

    {
      len = sizeof (keys);
      vats_keyboardGet (console, keys, &len);
    }

    for (i = 0; i < len; i++)
    {
      char key = keys [i];


      sprintf (keyhex, "0x%02x               \n", key);
      vDebugOutVats( __MODULE__, __LINE__, "keyboard_test: vats_keyboardGet: key=%d, keyhex=%s len=%d.", key, keyhex, len );
      vats_write_at (keyhex, sizeof (keyhex)-1, 1, 3);
      SVC_WAIT (1000);    // msec

      if (*keys == 0x9B)  // ABORT key
      {
        vDebugOutVats( __MODULE__, __LINE__, "keyboard_test: ABORTED" );
        return;
      }
    }
  }
} // keyboard_test


// =================================================================================================
char getPrinterStatus( int *printerHandle )
{
  char          status;
  int           res;
  unsigned long deadline;

  while( 1 )
  {
    if( !( 1 == EventViaDQDriver( ATSEVENT_TERMINAL_PRINTER ) ) )
    {
      read_evt( EVT_COM4 );                             // clear the events
      while( 1 == read( *printerHandle, &status, 1 ) )
      {
        ;                                               // read garbage from COM port
      }
    }

    // send <ESC>d to request printer status
    res = write( *printerHandle, "\x1B\x64", 2 );
    if( 2 != res )
    {
      vDebugOutVats( __MODULE__, __LINE__, "getPrinterStatus(), write() returned %d!=2.", res );
      return 0xFF;
    }

    if( !( 1 == EventViaDQDriver( ATSEVENT_TERMINAL_PRINTER ) ) )
    {
      // wait for event from printer
      wait_evt( EVT_COM4 );
    }

    deadline = read_ticks() + 10000;
    while( 1 )
    {
      if( 1 == read( *printerHandle, &status, 1 ) )
      {
        break;
      }
      SVC_WAIT( 50 );     // msecs
      if( read_ticks() >= deadline )
      {
        vDebugOutVats( __MODULE__, __LINE__, "getPrinterStatus(), read() timed out (10 sec)" );
        return 0xFF;
      }
    }

    if( !( 1 == EventViaDQDriver( ATSEVENT_TERMINAL_PRINTER ) ) )
    {
      // this is a workaround for VX820DUET sometimes returning a wrong with first status call (0x42: mechanism failure/paper out, see ADKRA-2341)
      if( ( '4' == printerAvailable ) && ( status & 0x42 ) )    // do one repeat, if 0x40 or 0x02 is set
      {
        if( 0xFF == first_status )
        {
          vDebugOutVats( __MODULE__, __LINE__, "getPrinterStatus()=%d (byte 0x%x), retry to fix VX820DUET issue with first printer status.", status, status );
          first_status = 0;
          continue;
        }
      }
    }
    break;
  }
  return status;
}


// =================================================================================================
signed char wait_for_printer( int *printerHandle, char *status, char waitForAllOkay )
{
  signed char retVal = -1;


  if( NULL != printerHandle )
  {
    if( ( !AtsIsActive() ) || ( 1 == EventViaDQDriver( ATSEVENT_TERMINAL_PRINTER ) ) )
    {
      // wait until printer is ready
      while( get_port_status( *printerHandle, status ) != 0 )
      {
        SVC_WAIT( 200 );      // msecs
      }
      if( NULL != status )
      {
        unsigned char loop_count = 20;
        unsigned char i;


        if( 0 != waitForAllOkay )
        {
          // adjust the loop_count in the case we have to wait some time until "all okay" status
          loop_count = 250;
        }
        for( i = 0; i < loop_count; i++ )
        {
          *status = getPrinterStatus( printerHandle );
          if( ( 0xFF != *status ) && ( ( 0 == waitForAllOkay ) || ( 0x20 == *status ) ) )
          {
            // we got the current, resp. "all okay" status, so exit loop
            retVal = 0;
            break;
          }
          else
          {
            SVC_WAIT( 200 );  // msecs
          }
        }
        if( ( 0 != retVal ) && ( 0xFF != *status ) )
        {
          // signal that we gor a valid result
          retVal = 1;
        }
      }
    }
    else
    {
      vDebugOutVats( __MODULE__, __LINE__, "wait_for_printer: pipes used, so fake the values." );
      retVal = 0;
      if( NULL != status )
      {
        *status = 0x20;
      }
    }
  }
  return retVal;
}


// =================================================================================================
void printer_test( void )
{

  int           ret         = 0;
  int           row         = 1;
  char          txtPrint1[] = "\n012345678901234567890123456789012345678901000000000011111111112222222222333333333344\n";
  char          txtPrint2[] = "\nVATS printer test!\n\n\n\n\n\n\n";   // at least 7*\n in order to scroll over the default_printer_tearbar
  char          txtPrint3[] = "0123456789012345678901234567890123456789012345678901234\n5678901234567890123456789\n01234567890123456789\n0123456789012345678901234567890123456789\n0123456789012345678901234567890123456789\n01234567890123456789\n0123456789012345678901234567890123456789\n0123456789012345678901234567890123456789\n01234567890123456789\n0123456789012345678901234567890123456789\n0123456789012345678901234567890123456789\n01234567890123456789\n0123456789012345678901234567890123456789\n0123456789012345678901234567890123456789\n01234567890123456789\n0123456789012345678901234567890123456789\n0123456789012345678901234567890123456789\n01234567890123456789\n0123456789012345678901234567890123456789\n0123456789012345678901234567890123456789\n01234567890123456789\n0123456789012345678901234567890123456789\n0123456789012345678901234567890123456789\n01234567890123456789\n0123456789012345678901234567890123456789\n0123456789012345678901234567890123456789\n01234567890123456789\n0123456789012345678901234567890123456789\n0123456789012345678901234567890123456789\n01234567890123456789\n0123456789012345678901234567890123456789\n0123456789012345678901234567890123456789\n01234567890123456789\n0123456789012345678901234567890123456789\n0123456789012345678901234567890123456789\n01234567890123456789\n0123456789012345678901234567890123456789\n0123456789012345678901234567890123456789\n01234567890123456789\n0123456789012345678901234567890123456789\n0123456789012345678901234567890123456789\n01234567890123456789\n0123456789012345678901234567890123456789\n0123456789012345678901234567890123456789\n012345678901\nVATS END\n\n\n\n\n";
  char          txtDisp[128];
  char          status;
  int           printerHandle;


  vDebugOutVats( __MODULE__, __LINE__, "printer_test: started" );
  vats_clrscr();
  sprintf( txtDisp, "Printer test" );
  vats_write_at( txtDisp, strlen( txtDisp ), 1, row++ );

  if( AtsIsActive() )
  {
    sprintf( txtDisp, "Vats is active" );
    vats_write_at( txtDisp, strlen( txtDisp ), 1, row++ );
  }
  else
  {
    sprintf( txtDisp, "Vats is NOT active" );
    vats_write_at( txtDisp, strlen( txtDisp ), 1, row++ );
  }

  // open printer
  printerHandle = open( "/dev/com4", 0 );
  if( 0 <= printerHandle )
  {
    signed char prtWait = 5;


    sprintf( txtDisp, "device opened" );
    vats_write_at( txtDisp, strlen( txtDisp ), 1, row++ );

    // wait for simulation of printer state
    SVC_WAIT( 2000 );     // msec
    // reading 1. printer status
    // wait until printer is ready
    prtWait = wait_for_printer( &printerHandle, &status, 0 );
    if( -1 == prtWait )
    {
      sprintf( txtDisp, "printer wait failed!" );
      vats_write_at( txtDisp, strlen( txtDisp ), 1, row++ );
    }
    else
    {
      vDebugOutVats( __MODULE__, __LINE__, "printer_test: 1 status=%d.", status );
      sprintf( txtDisp, "Status 1: %d (0x%02X)", status, status );
      vats_write_at( txtDisp, strlen( txtDisp ), 1, row++ );
      if( 0x20 != status )
      {
        // wait for simulation of printer state
        SVC_WAIT( 2000 );     // msecs
        // reading 2. printer status
        // wait until printer is ready
        prtWait = wait_for_printer( &printerHandle, &status, 1 );
        if( -1 == prtWait )
        {
          sprintf( txtDisp, "printer wait failed!" );
          vats_write_at( txtDisp, strlen( txtDisp ), 1, row++ );
          SVC_WAIT( 4000 );     // msecs
          close( printerHandle );
          return;
        }
        else
        {
          vDebugOutVats( __MODULE__, __LINE__, "printer_test: 2 status=%d.", status );
          sprintf( txtDisp, "Status 2: %d (0x%02X)", status, status );
          vats_write_at( txtDisp, strlen( txtDisp ), 1, row++ );
        }
      }
      else
      {
        row++;
      }
      sprintf( txtDisp, "start printing" );
      vats_write_at( txtDisp, strlen( txtDisp ), 1, row++ );

      if( ( !AtsIsActive() ) || ( 1 == EventViaDQDriver( ATSEVENT_TERMINAL_PRINTER ) ) )
      {
        if( 1 == EventViaDQDriver( ATSEVENT_TERMINAL_PRINTER ) )
        {
          // DQ driver used
          vDebugOutVats( __MODULE__, __LINE__, "printer_test: via DQ driver." );
        }
        else
        {
          // VATS is off, so do real printing
          vDebugOutVats( __MODULE__, __LINE__, "printer_test: VATS is off, so use the real printer." );
        }
        // print txtPrint3 and wait for finish
        ret = write( printerHandle, txtPrint3, strlen( txtPrint3 ) );
        vDebugOutVats( __MODULE__, __LINE__, "printer_test: print text 3 write=%d/%d.", strlen( txtPrint3 ), ret );
        prtWait = wait_for_printer( &printerHandle, &status, 1 );
        if( -1 == prtWait )
        {
          sprintf( txtDisp, "printer wait failed!" );
          vats_write_at( txtDisp, strlen( txtDisp ), 1, row++ );
          SVC_WAIT( 4000 );     // msecs
          close( printerHandle );
          return;
        }
        // print txtPrint1 and wait for finish
        ret = write( printerHandle, txtPrint1, strlen( txtPrint1 ) );
        vDebugOutVats( __MODULE__, __LINE__, "printer_test: print text 1 write=%d/%d.", strlen( txtPrint1 ), ret );
        prtWait = wait_for_printer( &printerHandle, &status, 1 );
        if( -1 == prtWait )
        {
          sprintf( txtDisp, "printer wait failed!" );
          vats_write_at( txtDisp, strlen( txtDisp ), 1, row++ );
          SVC_WAIT( 4000 );     // msecs
          close( printerHandle );
          return;
        }
        // print txtPrint2
        ret = write( printerHandle, txtPrint2, strlen( txtPrint2 ) );
        vDebugOutVats( __MODULE__, __LINE__, "printer_test: print text 2 write=%d/%d.", strlen( txtPrint2 ), ret );
      }
      else
      {
        // pipes used
        vDebugOutVats( __MODULE__, __LINE__, "printer_test: via VATS pipe." );
        // send printer text to VATS virtual printer
        AtsSendEvent( ATSEVENT_TERMINAL_PRINTER,  strlen( txtPrint3 ), txtPrint3 );
        AtsSendEvent( ATSEVENT_TERMINAL_PRINTER,  strlen( txtPrint1 ), txtPrint1 );
        AtsSendEvent( ATSEVENT_TERMINAL_PRINTER,  strlen( txtPrint2 ), txtPrint2 );
        // simulate printing, so wait some time
        SVC_WAIT( 2000 );   // msec
      }

      // wait for finish
      prtWait = wait_for_printer( &printerHandle, &status, 1 );
      if( -1 == prtWait )
      {
        sprintf( txtDisp, "printer wait failed!" );
        vats_write_at( txtDisp, strlen( txtDisp ), 1, row++ );
      }
      else
      {
        sprintf( txtDisp, "text printed (0x%02X)", status );
        vats_write_at( txtDisp, strlen( txtDisp ), 1, row++ );
      }

    }
    close( printerHandle );
  }
  else
  {
    sprintf( txtDisp, "device open failed" );
    vats_write_at( txtDisp, strlen( txtDisp ), 1, row++ );
    vDebugOutVats( __MODULE__, __LINE__, "printer_test: open device failed!" );
  }
  SVC_WAIT( 4000 );     // msecs
}


// =================================================================================================
unsigned long SmartOpen ( void )
{
  DWORD retVal = IFD_Failure;
  unsigned char CardBuffer [64];


  // convert to Big Endian
  CardBuffer[0] = 0;
  CardBuffer[1] = 0;
  CardBuffer[2] = 0;
  CardBuffer[3] = CUSTOMER_CARD;

  // open the icc
  retVal = IFD_Set_Capabilities( Tag_Open_ICC, CardBuffer );
  if( IFD_Success != retVal )
  {
    vDebugOutVats( __MODULE__, __LINE__, "  test_app1: SmartOpen: Tag_Open_ICC: ERROR: res=%ld.", retVal );
  }
  else
  {
    // select the icc 
    retVal = IFD_Set_Capabilities( Tag_Select_ICC, CardBuffer );
    if( IFD_Success != retVal )
    {
      vDebugOutVats( __MODULE__, __LINE__, "  test_app1: SmartOpen: Tag_Select_ICC: ERROR: res=%ld.", retVal );
    }
  }

  return retVal;
} // SmartOpen


// =================================================================================================
void cardin_test( int console )
{
  int rowHead       = 1;
  int rowVatsState  = 2;
  int rowVatsCardIn = 3;
  int rowPhysInit   = 4;
  int rowPhysCardIn = 5;
  int rowInsertCard = 6;
  int rowKeyAbort   = 7;
  char buf[500]     = "";
  int  iRet;
  long event        = 0;


  if( 1 == bSmallDisplay )
  {
    rowVatsCardIn = 2;
    rowPhysCardIn = 3;
    rowKeyAbort   = 4;
  }
  vDebugOutVats( __MODULE__, __LINE__, "cardin_test started" );
  vats_clrscr();
  read_event();     // in order to clear the event mask at first

  memset( buf, 0x00, sizeof( buf ) );
  sprintf( buf, "Card in test" );
  vats_write_at( buf, strlen( buf ), 1, rowHead );
  if( 0 == bSmallDisplay )
  {
    if( AtsIsActive() )
    {
      sprintf( buf, "VATS active" );
    }
    else
    {
      sprintf( buf, "VATS inactive" );
    }
    vats_write_at( buf, strlen( buf ), 1, rowVatsState );
  }
  sprintf( buf, "VATS cardin: ???" );
  vats_write_at( buf, strlen( buf ), 1, rowVatsCardIn );

  iRet = SmartOpen();
  if( 0 == bSmallDisplay )
  {
    if( iRet == 1 )
    {
      sprintf( buf, "Phys. init: ok" );
    }
    else
    {
      sprintf( buf, "Phys. init: %d", iRet );
    }
    vats_write_at( buf, strlen( buf ), 1, rowPhysInit );
  }

  iRet = IFD_Is_ICC_Present();
  if( iRet == IFD_Success )
  {
    sprintf( buf, "Phys. cardin: in " );
  }
  else
  {
    sprintf( buf, "Phys. cardin: out" );
  }
  vats_write_at( buf, strlen( buf ), 1, rowPhysCardIn );
  if( 0 == bSmallDisplay )
  {
    strcpy( buf, "Please insert card" );
    vats_write_at( buf, strlen( buf ), 1, rowInsertCard );
  }
  strcpy( buf, "Press CANCEL to abort" );
  vats_write_at( buf, strlen( buf ), 1, rowKeyAbort );

  while( 1 )
  {
    event = 0;
    // blocking wait for desired event
    event = wait_evt( EVT_ICC1_INS | EVT_ICC1_REM | EVT_USER | EVT_KBD );

    if( event & EVT_ICC1_INS )
    {
      vDebugOutVats( __MODULE__, __LINE__, "cardin_test: event = EVT_ICC1_INS" );
      iRet = IFD_Is_ICC_Present();
      if( iRet == IFD_Success )
      {
        strcpy( buf, "Phys. cardin: in " );
      }
      else
      {
        strcpy( buf, "Phys. cardin: error" );
      }
      vats_write_at( buf, strlen( buf ), 1, rowPhysCardIn );
    }
    else if( event & EVT_ICC1_REM )
    {
      vDebugOutVats( __MODULE__, __LINE__, "cardin_test: event = EVT_ICC1_REM" );
      iRet = IFD_Is_ICC_Present();
      if( iRet != IFD_Success )
      {
        strcpy( buf, "Phys. cardin: out" );
      }
      else
      {
        strcpy( buf, "Phys. cardin: error" );
      }
      vats_write_at( buf, strlen( buf ), 1, rowPhysCardIn );
    }
    if( event & EVT_KBD )
    {
      char key;


      vDebugOutVats( __MODULE__, __LINE__, "cardin_test: event = EVT_KBD" );
      if( 0 >= read( console, &key, 1 ) )
      {
        key = 0;
      }
      if( key == 0x9B ) // ignore other keys
      {
        vDebugOutVats( __MODULE__, __LINE__, "cardin_test: ABORTED" );
        return;
      }
    }
    
    if( AtsIsActive() && ( 0 == EventViaDQDriver( ATSEVENT_USER_ICCCARD_IN ) ) && ( event & EVT_USER ) )
    {
      // handle event ATSEVENT_USER_ICCCARD_IN via pipe
      enum AtsEvent ats_event    = ATSEVENT_NONE;
      char          dataBuf[500] = {0};
      int           datalen      = sizeof( dataBuf );


      vDebugOutVats( __MODULE__, __LINE__, "cardin_test: event = EVT_USER" );
      if( ATSERROR_OK == AtsReceiveEvent( &ats_event, 500, &datalen, dataBuf ) )
      {
        if( ats_event == ATSEVENT_USER_ICCCARD_IN )
        {
          vDebugOutVats( __MODULE__, __LINE__, "cardin_test: ats_event == ATSEVENT_USER_ICCCARD_IN" );
          if( 1 == dataBuf[0] )
          {
            strcpy( buf, "VATS cardin: in " );
          }
          else
          {
            strcpy( buf, "VATS cardin: out" );
          }
          vats_write_at( buf, strlen( buf ), 1, rowVatsCardIn );
        }
        else if( ( ats_event == ATSEVENT_USER_KEYBOARD ) && ( dataBuf[0] == 0x9B ) )    // ABORT key
        {
          vDebugOutVats( __MODULE__, __LINE__, "cardin_test: ABORTED" );
          return;
        }
      }
    }
  }
} // cardin_test


// =================================================================================================
void magstripe_test (void)
{
  // example data: 123=JOHN=SMITH|1234567890123456=1204120600100|
  int taskIDConsole;
  int ret;


  vDebugOutVats( __MODULE__, __LINE__, "$$$test_app1: magstripe_test:" );
  if (get_owner ("/DEV/MAG", &taskIDConsole) < 0)
  {
    vDebugOutVats( __MODULE__, __LINE__, "$$$test_app1: magstripe_test: get_owner /DEV/MAG failed" );
  }
  else
  {
    vDebugOutVats( __MODULE__, __LINE__, "$$$test_app1: magstripe_test: taskIDConsole=%d.", taskIDConsole );

    ret = activate_task (taskIDConsole); // other task gets console access
    vDebugOutVats( __MODULE__, __LINE__, "$$$test_app1: magstripe_test: taskIDConsole=%d ret=%d.", taskIDConsole, ret );

    vDebugOutVats( __MODULE__, __LINE__, "$$$test_app1: magstripe_test: wait til finished" );
    wait_evt (EVT_ACTIVATE);
    vDebugOutVats( __MODULE__, __LINE__, "$$$test_app1: magstripe_test: EVT_ACTIVATE received" );
  }
} // magstripe_test


// =================================================================================================
void trace_event_test (void)
{
  char  buffer[30];
  char  txtTrace[] = "VATS trace testing!";
  int   siz        = ((PIPE_MAX_MSG_LEN - VATS_PIPE_OVERHEAD) / 10) *8;   // don't exceed max. pipe message length, and have a multiple of 8
  char *trc        = NULL;
  int   i          = 0;
  int   maximum    = siz / 8;


  vats_clrscr();
  sprintf( buffer, "Trace test" );
  vats_write_at( buffer, strlen( buffer ), 1, 1 );
  // 1st trace: very short
  AtsTraceEvent( /*source=*/7, /*channel=*/8, strlen( txtTrace ), txtTrace );

  // 2nd trace: very long, will be chained on PC interface
  trc = (char*)malloc( siz + 1 );                                         // as siz is a multiple of 8, we need the trailing '\0' additionally
  if( trc )
  {
    for( i = 0; i < maximum; i++ )
    {
      memcpy( (trc + (i * 8)), "test ...", strlen( "test ..." ) );
    }
    AtsTraceEvent( /*source=*/5, /*channel=*/6, siz, trc );
    free( trc );
    trc = NULL;
  }

  // traces should be successfully transferred to VATS PC now
  sprintf( buffer, "Done." );
  vats_write_at( buffer, strlen( buffer ), 2, 2 );
  SVC_WAIT( 3000 );
} // trace_event_test


// =================================================================================================
void custom_event_test (void)
{
  // send a custom event
  char dat[] = "Hello World! \x00\x01\x02\x03\x04";
  char buffer[22];


  vats_clrscr();
  sprintf( buffer, "Custom event test" );
  vats_write_at( buffer, strlen( buffer ), 1, 1 );

  AtsSendEvent( 1001, sizeof( dat ), (const void *)dat );

  // custom event should be successfully transferred to VATS PC now
  sprintf( buffer, "Done." );
  vats_write_at( buffer, strlen( buffer ), 2, 2 );
  SVC_WAIT( 3000 );
} // custom_event_test


// =================================================================================================
int convertBufferSliceToVisibleStringLine( char *destination, int destinationMaxCharLen, char *source, int sourceMaxLen )
{
  int sourceOffset = 0;


  if( ( NULL != destination ) && ( 0 < destinationMaxCharLen ) && ( NULL != source ) && ( 0 < sourceMaxLen ) )
  {
    int  destinationOffset = 0;
    char nibble            = ' ';


    while( ( destinationMaxCharLen > destinationOffset ) && ( sourceMaxLen > sourceOffset ) )
    {
      if(
          !(
                (
                      ( (signed char)source[sourceOffset] >= 0 )
                  &&  ( source[sourceOffset] <  ' ' )
                )
            ||  ( source[sourceOffset] == 127 )
            ||  ( (signed char)source[sourceOffset] == (-1) )
          )
        )
      {
        destination[destinationOffset++] = source[sourceOffset];
      }
      else if( source[sourceOffset] == '\n' )
      {
        sourceOffset++;
        break;
      }
      else
      {
        if( (destinationMaxCharLen - 2) > destinationOffset )
        {
          destination[destinationOffset++] = '\\';
          nibble = (source[sourceOffset] >> 4) & 0xF;
          destination[destinationOffset++] = (nibble < 10) ? (nibble + '0') : (nibble - 10 + 'A');
          nibble = source[sourceOffset] & 0xF;
          destination[destinationOffset++] = (nibble < 10) ? (nibble + '0') : (nibble - 10 + 'A');
        }
        else
        {
          break;
        }
      }
      sourceOffset++;
    }
    destination[destinationOffset] = '\0';
  }
  return sourceOffset;
}


// =================================================================================================
void custom_event_test2( int console )
{
  char           keys[KEYBOARD_BUF_MAX];
  int            len = sizeof( keys );
  unsigned short usCustomEvent = 0;
  char           eventData[8192];
  int            eventDataLen = sizeof( eventData );
  char           buf[512];


  vats_clrscr();
  sprintf( buf, "AppName: '%s'", APPNAME );
  vats_write_at( buf, strlen( buf ), 1, 1 );
  if( 0 == bSmallDisplay )
  {
    sprintf( buf, "Waiting for custom\nevent from test\nscript.\nMax len = %d.", eventDataLen );
  }
  else
  {
    strcpy( buf, "Waiting for custom\nevent from test\nscript." );
  }
  vats_write_at( buf, strlen( buf ), 1, 2 );
  if( 0 == bSmallDisplay )
  {
    strcpy( buf, "Press CANCEL to abort" );
    vats_write_at( buf, strlen( buf ), 1, 8 );
  }
  memset( eventData, 0, sizeof( eventData ) );
  vats_customEventOrKeyboardGet( console, keys, &len, &usCustomEvent, eventData, &eventDataLen );
  vats_clrscr();
  if( 0 < eventDataLen )
  {
    int offset  = 0;
    int myCount = 0;
    int i       = 0;
    int max     = 8;


    sprintf( buf, "Custom event: %d", usCustomEvent );
    vats_write_at( buf, strlen( buf ), 1, 1 );
    sprintf( buf, "Data length: %d", eventDataLen );
    vats_write_at( buf, strlen( buf ), 1, 2 );
    if( 1 == bSmallDisplay )
    {
      max = 4;
    }
    for( i = 3; i <= max; i++ )
    {
      myCount = 0;
      memset( buf, 0xFF, sizeof( buf ) );
      myCount = convertBufferSliceToVisibleStringLine( buf, 20, &eventData[offset], (eventDataLen - offset) );
      if( 0 < myCount )
      {
        vats_write_at( buf, strlen( buf ), 1, i );
      }
      else
      {
        break;
      }
      offset += myCount;
      if( offset >= eventDataLen )
      {
        break;
      }
    }
  }
  else
  {
    sprintf( buf, "Key entered: %d", keys[0] );
    vats_write_at( buf, strlen( buf ), 1, 1 );
  }
  SVC_WAIT (2000);
}


// =================================================================================================
void screen_test (void)
{
  char buffer[200];


  vats_clrscr();
  if( 1 == bSmallDisplay )
  {
    sprintf( buffer, "1VATS Screen 123456782VATS Screen 123456787VATS Screen 123456788VATS Screen 12345678" );
  }
  else
  {
    sprintf( buffer, "1VATS Screen 123456782VATS Screen 123456783VATS Screen 123456784VATS Screen 123456785VATS Screen 123456786VATS Screen 123456787VATS Screen 123456788VATS Screen 12345678" );
  }
  vDebugOutVats( __MODULE__, __LINE__, "screen_test: strlen=%d\n", strlen( buffer ) );
  vats_write_at( buffer, strlen( buffer ), 1, 1 );
  SVC_WAIT( 3000 );
} // screen_test


// =================================================================================================
void pin_entry (void)
{
  int           rc;
  int           iStatus;
  int           time             = 60;  // timeout for PIN input in seconds
  unsigned long ulStart;
  unsigned char ucINPANData [20] = {0};
  PINRESULT     strOutData;
  PINPARAMETER  strKeypadSetup;
  char          txt[21+1];
  int           hCrypt;


  hCrypt = open ("/dev/crypto", 0);
  if( 0 > hCrypt )
  {
    vDebugOutVats( __MODULE__, __LINE__, "test_app1: pin_entry: *** Error on opening crypto device ***" );
    return;
  }
  if (iPS_InstallScript ("emvadf.vso") != 0)
  {
    vDebugOutVats( __MODULE__, __LINE__, "test_app1: pin_entry: *** Error on install script ***" );
  }

  // Enter PIN
  rc = iPS_SelectPINAlgo (0x0B);
  if (rc != 0)
  {
    vDebugOutVats( __MODULE__, __LINE__, "test_app1: pin_entry: *** Error on select PIN algo: rc=0x%08X ***", rc );
    close( hCrypt );
    return;
  }

  memset (&strKeypadSetup, 0, sizeof(strKeypadSetup));
  strKeypadSetup.ucMin      = 4;    // MINPINLEN
  strKeypadSetup.ucMax      = 4;    // MaxLen
  strKeypadSetup.ucEchoChar = '*';
  strKeypadSetup.ucDefChar  = 0x20;
  strKeypadSetup.ucOption  |= 0x08; // bit 3 - <BS> = 1 char
  vDebugOutVats( __MODULE__, __LINE__, "test_app1: pin_entry: *** set PIN parameters now ***" );

  rc = iPS_SetPINParameter (&strKeypadSetup);
  if (rc != 0)
  {
    vDebugOutVats( __MODULE__, __LINE__, "test_app1: pin_entry: *** Error on set PIN parameter: rc=0x%08X ***", rc );
    close( hCrypt );
    return;
  }

  vDebugOutVats( __MODULE__, __LINE__, "test_app1: pin_entry: *** request PIN entry now ***" );
  vats_clrscr ();
  sprintf (txt, "PIN entry test:");
  vats_write_at (txt, strlen(txt), 1, 1);

  rc = iPS_RequestPINEntry (0, ucINPANData);
  if (rc != 0)
  {
    vDebugOutVats( __MODULE__, __LINE__, "test_app1: pin_entry: *** Error on request PIN entry: rc=0x%08X ***", rc );
    close( hCrypt );
    return;
  }

  ulStart = read_ticks();
  while (1)
  {
    if (ulStart+1000L*time < read_ticks())
    {
      vDebugOutVats( __MODULE__, __LINE__, "test_app1: pin_entry: *** PIN was cancelled ***" );
      iPS_CancelPIN ();
      close( hCrypt );
      return;
    }

    rc = iPS_GetPINResponse(&iStatus, &strOutData);
    if (rc != 0)
    {
      vDebugOutVats( __MODULE__, __LINE__, "test_app1: pin_entry: *** Error on getting PIN response: rc=0x%08X ***", rc );
      close( hCrypt );
      return;
    }

    if (iStatus == 0)
    {
      unsigned char  pinBlock [RSA_RESULT_SIZE];
      unsigned short pinBlockLen;


      vDebugOutVats( __MODULE__, __LINE__, "test_app1: pin_entry: *** Status of PIN reponse okay ***" );

      // Print out entered PIN
      rc = iPS_ExecuteScript (EMV_SCRIPT_NO, MACRO_GET_CLEARTEXT_PIN, 0, NULL, RSA_RESULT_SIZE, &pinBlockLen, pinBlock);
      if (rc != 0)
      {
        vDebugOutVats( __MODULE__, __LINE__, "test_app1: pin_entry: *** Error on executing VSS script: rc=0x%08X ***", rc );
        close( hCrypt );
        return;
      }

      vDebugOutVats( __MODULE__, __LINE__, "test_app1: pin_entry: *** SUCCESS ***" );
      LOG_HEX_PRINTF ("*** Entered PIN read out: ", pinBlock, pinBlockLen);  // 4 digit PIN
      sprintf (txt, "PIN is %02x%02x", pinBlock[1], pinBlock[2]);
      vats_write_at (txt, strlen(txt), 1, 3);
      SVC_WAIT (2000); // msec
      close( hCrypt );
      return;
    }
    else if (0x05 == iStatus || 0x0A == iStatus)
    {
      vDebugOutVats( __MODULE__, __LINE__, "test_app1: pin_entry: *** PIN response failed: rc=0x%08X iStatus=%d ***", rc, iStatus );
      close( hCrypt );
      return;
    }
    SVC_WAIT (10); // msec
  }
} // pin_entry


// =================================================================================================
void datetime_test( int console )
{
  int  i            = 1;
  int  row          = 1;
  char txt[22]      = {0};
  int  iContinue    = 1;
  char emptyLine[]  = "                     ";
  char dateAndTime[16];
  char keys[KEYBOARD_BUF_MAX];


  vDebugOutVats( __MODULE__, __LINE__, "datetime_test: called" );
  vats_clrscr ();
  sprintf( txt, "Date and Time test" );
  vats_write_at( txt, strlen( txt ), 1, row++ );

  if( 0 == bSmallDisplay )
  {
    row = 5;
    sprintf( txt, "Press any key to" );
    vats_write_at( txt, strlen( txt ), 1, row++ );
    sprintf( txt, "update time stamp" );
    vats_write_at( txt, strlen( txt ), 1, row++ );

    row++;
    sprintf( txt, "Press CANCEL to abort" );
    vats_write_at( txt, strlen(txt), 1, row );
  }
  while( 1 == iContinue )
  {
    row = 2;
    vats_write_at( emptyLine, strlen( emptyLine ), 1, row );
    vats_write_at( emptyLine, strlen( emptyLine ), 1, (row + 1) );
    memset( dateAndTime, 0, sizeof( dateAndTime ) );
    read_clock( dateAndTime );
    vDebugOutVats( __MODULE__, __LINE__, "datetime_test(%d): %s.", i, dateAndTime );

    sprintf( txt, "date and time %d:", i );
    vats_write_at( txt, strlen( txt ), 1, row++ );
    vats_write_at( dateAndTime, strlen( dateAndTime ) - 1, 1, row++ );
    keys_len = sizeof( keys );
    memset( keys, 0, keys_len );
    vats_keyboardGet( console, keys, &keys_len );
    if( keys[0] == 0x9B )   // ignore other keys
    {
      iContinue = 0;
    }
    else
    {
      i++;
    }
  }
} // datetime_test


// =================================================================================================
void vats_keyboardOrTouch( int console, char *touchKey, char *key, int *xCoord, int *yCoord )
{
  if( ( NULL != touchKey ) && ( NULL != key ) && ( NULL != xCoord ) && ( NULL != yCoord ) )
  {
    long          event;
    enum AtsEvent ats_event     = ATSEVENT_NONE;
    char          dataBuf [500] = {0};
    int           datalen       = sizeof (dataBuf);
    int           len           = sizeof( *key );
    int           touch         = 0;
    static char   lastTouch     = 0;
    static int    lastX         = -1;
    static int    lastY         = -1;


    // handle physical touch
    touch = get_touchscreen( xCoord, yCoord );
    if( 0 != touch )
    {
      *touchKey = 0x02;
      lastTouch = 2;
    }
    else
    {
      *touchKey = 0x00;
      if( 2 == lastTouch )
      {
        lastTouch = 0;
      }
    }
    // handle physical pending keyboard input
    if( kbd_pending_count() > 0 )
    {
      int ret = 0;
      ret = read( console, key, len );
      if( ret > 0 )
      {
        *touchKey |= 0x01;
      }
    }
    if( 0 == *touchKey )
    {
      // no physical events, so check for VATS IPC events
      event = read_event();

      if( AtsIsActive() && ( event & EVT_USER ) )
      {
        // VATS is active and pipe event
        if( ATSERROR_OK == AtsReceiveEvent( &ats_event, 0, &datalen, (void *)dataBuf ) )
        {
          vDebugOutVats( __MODULE__, __LINE__, "vats_keyboardOrTouch: event=%08X, len=%d.", ats_event, datalen );
          if( ( 0 == EventViaDQDriver( ATSEVENT_USER_KEYBOARD ) ) && ( ats_event == ATSEVENT_USER_KEYBOARD ) && ( datalen > 0 ) )
          {
            *key       = dataBuf[0];
            *touchKey |= 0x01;
          }
          if( ( 0 == EventViaDQDriver( ATSEVENT_USER_TOUCH ) ) && ( ats_event == ATSEVENT_USER_TOUCH ) && ( datalen > 0 ) )
          {
            lastTouch = dataBuf[0];
            if( 1 == lastTouch )
            {
              vDebugOutVats( __MODULE__, __LINE__, "vats_keyboardOrTouch: dataBuf=%02X %02X%02X%02X%02X %02X%02X%02X%02X.", dataBuf[0], dataBuf[1], dataBuf[2], dataBuf[3], dataBuf[4], dataBuf[5], dataBuf[6], dataBuf[7], dataBuf[8] );
              lastX = ((int)(dataBuf[1]) << 24) + ((int)(dataBuf[2]) << 16) + ((int)(dataBuf[3]) << 8) + dataBuf[4];
              lastY = ((int)(dataBuf[5]) << 24) + ((int)(dataBuf[6]) << 16) + ((int)(dataBuf[7]) << 8) + dataBuf[8];
            }
          }
        }
      }
      if( 1 == lastTouch )
      {
        *xCoord    = lastX;
        *yCoord    = lastY;
        *touchKey |= 0x02;
      }
    }
  }
}


// =================================================================================================
void touch_event_test( int console )
{
  char output1[21];
  char keys         = 0;
  int  touchRes     = 0;
  int  lastTouchRes = 0;
  int  xCoord       = 0;
  int  yCoord       = 0;
  int  row          = 1;
  char emptyLine[]  = "                     ";
  char touchKey     = 0;
  int  iContinue    = 1;
  char redraw       = 1;
  int  xCoord_old   = 0;
  int  yCoord_old   = 0;


  vDebugOutVats( __MODULE__, __LINE__, "touch_event_test: called" );
  vats_clrscr ();
  strcpy( output1, "Touch testing" );
  vats_write_at( output1, strlen( output1 ), 1, row++);
  strcpy( output1, "CANCEL to abort:" );
  vats_write_at( output1, strlen( output1 ), 1, row++);
  while( 1 == iContinue )
  {
    vats_keyboardOrTouch( console, &touchKey, &keys, &xCoord, &yCoord );
    if( ( touchKey & 0x01 ) && ( 0x9B == keys ) )
    {
      // CANCEL pressed, so leave loop
      iContinue = 0;
    }
    else
    {
      row = 3;
      if( touchKey & 0x02 )
      {
        touchRes = 1;
      }
      else
      {
        touchRes = 0;
      }
      if( lastTouchRes != touchRes )
      {
        lastTouchRes = touchRes;
        vats_write_at( emptyLine, strlen( emptyLine ), 1, row );
        redraw = 1;
      }
      if( 0 == redraw )
      {
        if( ( xCoord_old != xCoord ) || ( yCoord_old != yCoord ) )
        {
          xCoord_old = xCoord;
          yCoord_old = yCoord;
          redraw = 1;
        }
      }
      if( 1 == redraw )
      {
        if( 0 != touchRes )
        {
          vats_write_at( "pen down", strlen( "pen down" ), 1, row++ );
          sprintf( output1, "x pos: %3d\n", xCoord );
          vats_write_at( output1, strlen( output1 ), 1, row++);
          sprintf( output1, "y pos: %3d\n", yCoord );
          vats_write_at( output1, strlen( output1 ), 1, row++);
        }
        else
        {
          vats_write_at( "pen up", strlen( "pen up" ), 1, row++ );
        }
        redraw = 0;
      }
    }
  }
}


// *************************************************************************************************
// test application menu handling
// *************************************************************************************************

// =================================================================================================
void menu_card_display( void )
{
  int  row     = 1;
  char txt[22] = { 0 };


  vats_clrscr();
  sprintf( txt, "Card Reader Menu" );
  vats_write_at( txt, strlen( txt ), 1, row++ );
  sprintf( txt, "1: ICC card in   test" );
  vats_write_at( txt, strlen( txt ), 1, row++ );
  sprintf( txt, "2: MSR read      test" );
  vats_write_at( txt, strlen( txt ), 1, row++ );

  sprintf( txt, "Please select!" );
  if( 1 == bSmallDisplay )
  {
    vats_write_at( txt, strlen( txt ), 1, 4 );
  }
  else
  {
    vats_write_at( txt, strlen( txt ), 1, 8 );
  }
}


// =================================================================================================
void menu_card_handling( int console )
{
  int iContinue = 1;


  while( 1 == iContinue )
  {
    menu_card_display();

    keys_len = sizeof( keys );
    memset( keys, 0, keys_len );
    vats_keyboardGet( console, keys, &keys_len );

    vDebugOutVats( __MODULE__, __LINE__, "test_app1: card reader menu key=%d.", (int)keys[0] );

    switch( keys[0] )
    {
      case 0xB1:
        cardin_test( console );
        break;
      case 0xB2:
        magstripe_test();
        break;
      default:
        iContinue = 0;
        break;
    }
    if( 0 >= read( console, keys, sizeof( keys ) ) )  // clear keyboard buffer
    {
      keys[0] = 0;
    }
  }
}


// =================================================================================================
void menu_print_display( void )
{
  int  row     = 1;
  char txt[22] = { 0 };


  vats_clrscr ();
  sprintf (txt, "Printer Menu");
  vats_write_at (txt, strlen(txt), 1, row++);
  // here no further check for printer is done, as the main menu should prevent the user from
  // getting here
  sprintf (txt, "1: printer    test  1");
  vats_write_at (txt, strlen(txt), 1, row++);

  sprintf (txt, "Please select!");
  vats_write_at (txt, strlen(txt), 1, 8);
}


// =================================================================================================
void menu_print_handling( int console )
{
  int iContinue = 1;


  while( 1 == iContinue )
  {
    menu_print_display();

    keys_len = sizeof( keys );
    memset( keys, 0, keys_len );
    vats_keyboardGet( console, keys, &keys_len );

    vDebugOutVats( __MODULE__, __LINE__, "test_app1: print menu key=%d.", (int)keys[0] );

    switch( keys[0] )
    {
      case 0xB1:
        // here no further check for printer is done, as the main menu should prevent the user from
        // getting here
        printer_test();
        break;
      default:
        iContinue = 0;
        break;
    }
    if( 0 >= read( console, keys, sizeof( keys ) ) )  // clear keyboard buffer
    {
      keys[0] = 0;
    }
  }
}


// =================================================================================================
void menu_user_display( void )
{
  int  row     = 1;
  char txt[22] = { 0 };


  vats_clrscr();
  sprintf( txt, "User Input Menu" );
  vats_write_at( txt, strlen( txt ), 1, row++ );
  sprintf( txt, "1: keyboard   test  1" );
  vats_write_at( txt, strlen( txt ), 1, row++ );
  if( 0 != EventViaDQDriver( ATSEVENT_USER_KEYBOARD ) )
  {
    sprintf( txt, "2: PIN entry  test  1" );
  }
  else
  {
    sprintf( txt, "No DQ driver active" );
  }
  vats_write_at( txt, strlen( txt ), 1, row++ );
  if( 0 == bSmallDisplay )
  {
    if( touchScreenAvailable() > 0 )
    {
      sprintf( txt, "3: touch      test  1" );
    }
    else
    {
      sprintf( txt, "No touch device" );
    }
    vats_write_at( txt, strlen( txt ), 1, row++ );
  }

  sprintf( txt, "Please select!" );
  if( 1 == bSmallDisplay )
  {
    vats_write_at( txt, strlen( txt ), 1, 4 );
  }
  else
  {
    vats_write_at( txt, strlen( txt ), 1, 8 );
  }
}


// =================================================================================================
void menu_user_handling( int console )
{
  int iContinue = 1;


  while( 1 == iContinue )
  {
    menu_user_display();

    keys_len = sizeof( keys );
    memset( keys, 0, keys_len );
    vats_keyboardGet( console, keys, &keys_len );

    vDebugOutVats( __MODULE__, __LINE__, "test_app1: user input menu key=%d.", (int)keys[0] );

    // default is exit loop
    iContinue = 0;
    switch( keys[0] )
    {
      case 0xB1:
        keyboard_test( console );
        iContinue = 1;
        break;
      case 0xB2:
        if( 0 != EventViaDQDriver( ATSEVENT_USER_KEYBOARD ) )
        {
          pin_entry();
          iContinue = 1;
        }
        break;
      case 0xB3:
        if( ( touchScreenAvailable() > 0 ) && ( 0 == bSmallDisplay ) )
        {
          touch_event_test( console );
          iContinue = 1;
        }
        break;
    }
    if( 0 >= read( console, keys, sizeof( keys ) ) )  // clear keyboard buffer
    {
      keys[0] = 0;
    }
  }
}


// =================================================================================================
void menu_disp_display( void )
{
  int  row     = 1;
  char txt[22] = { 0 };


  vats_clrscr();
  sprintf( txt, "Display Menu" );
  vats_write_at( txt, strlen( txt ), 1, row++ );
  sprintf( txt, "1: display    test  1" );
  vats_write_at( txt, strlen( txt ), 1, row++ );
  sprintf( txt, "2: display    test  2" );
  vats_write_at( txt, strlen( txt ), 1, row++ );

  sprintf( txt, "Please select!" );
  if( 1 == bSmallDisplay )
  {
    vats_write_at( txt, strlen( txt ), 1, 4 );
  }
  else
  {
    vats_write_at( txt, strlen( txt ), 1, 8 );
  }
}


// =================================================================================================
void menu_disp_handling( int console )
{
  int iContinue = 1;


  while( 1 == iContinue )
  {
    menu_disp_display();

    keys_len = sizeof( keys );
    memset( keys, 0, keys_len );
    vats_keyboardGet( console, keys, &keys_len );

    vDebugOutVats( __MODULE__, __LINE__, "test_app1: display menu key=%d.", (int)keys[0] );

    switch( keys[0] )
    {
      case 0xB1:
        display_test();
        break;
      case 0xB2:
        screen_test();
        break;
      default:
        iContinue = 0;
        break;
    }
    if( 0 >= read( console, keys, sizeof( keys ) ) )  // clear keyboard buffer
    {
      keys[0] = 0;
    }
  }
}


// =================================================================================================
void menu_misc_display( void )
{
  int  row     = 1;
  char txt[22] = { 0 };


  vats_clrscr();
  if( 0 == bSmallDisplay )
  {
    sprintf( txt, "Miscellaneous Menu" );
    vats_write_at( txt, strlen( txt ), 1, row++ );
  }
  sprintf( txt, "1: DUT to PC cust.ev." );
  vats_write_at( txt, strlen( txt ), 1, row++ );
  sprintf( txt, "2: PC to DUT cust.ev." );
  vats_write_at( txt, strlen( txt ), 1, row++ );
  sprintf( txt, "3: trace         test" );
  vats_write_at( txt, strlen( txt ), 1, row++ );
  sprintf( txt, "4: date and time test" );
  vats_write_at( txt, strlen( txt ), 1, row++ );

  if( 0 == bSmallDisplay )
  {
    sprintf( txt, "Please select!" );
    vats_write_at( txt, strlen( txt ), 1, 8 );
  }
}


// =================================================================================================
void menu_misc_handling( int console )
{
  int iContinue = 1;


  while( 1 == iContinue )
  {
    menu_misc_display();

    keys_len = sizeof( keys );
    memset( keys, 0, keys_len );
    vats_keyboardGet( console, keys, &keys_len );

    vDebugOutVats( __MODULE__, __LINE__, "test_app1: miscellaneous menu key=%d.", (int)keys[0] );

    switch( keys[0] )
    {
      case 0xB1:
        custom_event_test();
        break;
      case 0xB2:
        custom_event_test2( console );
        break;
      case 0xB3:
        trace_event_test();
        break;
      case 0xB4:
        datetime_test( console );
        break;
      default:
        iContinue = 0;
        break;
    }
    if( 0 >= read( console, keys, sizeof( keys ) ) )  // clear keyboard buffer
    {
      keys[0] = 0;
    }
  }
}


// =================================================================================================
void menu_main_display( void )
{
  int  row     = 1;
  char txt[22] = { 0 };


  vats_clrscr();
  sprintf( txt, "VATS Main Menu" );
  vats_write_at( txt, strlen( txt ), 1, row++ );
  if( 1 == bSmallDisplay )
  {
    sprintf( txt, "1: Misc.   2: Display" );
    vats_write_at( txt, strlen( txt ), 1, row++ );
    sprintf( txt, "3: Input   no printer" );
    vats_write_at( txt, strlen( txt ), 1, row++ );
    sprintf( txt, "5: Card" );
    vats_write_at( txt, strlen( txt ), 1, row++ );
  }
  else
  {
    sprintf( txt, "1: Miscellaneous" );
    vats_write_at( txt, strlen( txt ), 1, row++ );
    sprintf( txt, "2: Display" );
    vats_write_at( txt, strlen( txt ), 1, row++ );
    sprintf( txt, "3: User input" );
    vats_write_at( txt, strlen( txt ), 1, row++ );
    if( 1 == PrinterAvailable() )
    {
      sprintf( txt, "4: Printer" );
    }
    else
    {
      sprintf( txt, "no printer on tap" );
    }
    vats_write_at( txt, strlen( txt ), 1, row++ );
    sprintf( txt, "5: Card reader" );
    vats_write_at( txt, strlen( txt ), 1, row++ );

    sprintf( txt, "Please select!" );
    vats_write_at( txt, strlen( txt ), 1, 8 );
  }
}


// =================================================================================================
void menu_main_handling( int console )
{
  while( 1 )
  {
    menu_main_display();

    keys_len = sizeof( keys );
    memset( keys, 0, keys_len );
    vats_keyboardGet( console, keys, &keys_len );

    vDebugOutVats( __MODULE__, __LINE__, "test_app1: main menu key=%d.", (int)keys[0] );

    switch( keys[0] )
    {
      case 0xB0:
        SVC_RESTART( "" );
        break;
      case 0xB1:
        menu_misc_handling( console );
        break;
      case 0xB2:
        menu_disp_handling( console );
        break;
      case 0xB3:
        menu_user_handling( console );
        break;
      case 0xB4:
        if( ( 1 == PrinterAvailable() ) && ( 0 == bSmallDisplay ) )
        {
          menu_print_handling( console );
        }
        break;
      case 0xB5:
        menu_card_handling( console );
        break;
    }
    if( 0 >= read( console, keys, sizeof( keys ) ) )  // clear keyboard buffer
    {
      keys[0] = 0;
    }
  }
}


// =================================================================================================
void GetModelInfo( char *model )
{
  char  buffer[13];
  char *buffer_used = buffer;


  // determine model number of terminal
  if( 0 == SVC_INFO_MODELNO( buffer ) )
  {
    // terminate the fixed length string
    buffer[12] = '\0';
    while( ' ' == *buffer_used )  // skip preceding blanks
    {
      buffer_used++;
    }
    strcpy( model, buffer_used );
  }
  else
  {
    strcpy( model, "unknown" );
  }
}


// =================================================================================================
int main( void )
{
  int console;


  console = open (DEV_CONSOLE, 0);

  if( 0 <= console )
  {
    char model[20];
    displayInfo_t myDispInfo;


    LOG_INIT (APPNAME, LOGSYS_OS, LOG_APPFILTER);
    vDebugOutVats( __MODULE__, __LINE__, "  Verix: test_app1: START: 02.02.2015" );

    // Initialize VATS lib (incl. VATS pipe)
    AtsInitExt( APPNAME, (int)( ATSREG_KEYBOARD | ATSREG_CUSTOM | ATSREG_ICCCARD_IN ), get_task_id(), 3 );
    vDebugOutVats( __MODULE__, __LINE__, "test_app1: VATS is active: %d.", AtsIsActive() );

    if (0 == EventViaDQDriver (ATSEVENT_TERMINAL_DISPLAY))
      vDebugOutVats( __MODULE__, __LINE__, "test_app1: AtsDisplayViaDQDriver: 0." );

    // display size
    SVC_INFO_DISPLAY_EXT( &myDispInfo );
    if( ( myDispInfo.width <= 128 ) && ( myDispInfo.height < 64 ) )
    {
      bSmallDisplay = 1;
    }
    // keypad type
    GetModelInfo( model );
    if( ( 0 == strcmp( tType_e315, model ) ) || ( 0 == strcmp( tType_e335, model ) ) )
    {
      unsigned long ulCount = 100;


      VatsLogging( VATS_LOG_INFO, __MODULE__, __LINE__, "VATS Handler: wake up the %s keypad", model );
      // wake up the keypad
      cs_set_sleep_state( 0 );
      // wait for activated keypad
      while( ( 1 < ulCount ) && ( 0 == cs_spi_cmd_status() ) )
      {
        --ulCount;
        svcSleep( 10 );     // msec
      }
      VatsLogging( VATS_LOG_INFO, __MODULE__, __LINE__, "VATS Handler: %s keypad status %d.", model, cs_spi_cmd_status() );
    }
    // infinite loop handling the menus and test cases
    menu_main_handling( console );
  }
} // main

#endif  // #ifndef _VOS
