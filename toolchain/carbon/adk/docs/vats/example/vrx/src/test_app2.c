/*****************************************************************************/
/**
 * \file test_app2.c
 *
 * \author GSS R&D Germany
 *
 * Company: VeriFone GmbH
 *
 * Product: Verifone Automated Test System (VATS)
 *
 *       (c) Copyright 2012
 */
/*****************************************************************************/
#ifndef _VOS


#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <svc.h>
#include <errno.h>
#include <eoslog.h>
#include "atsmisc.h"
#include "atslib.h"


#define APPNAME           "TESTAPP2"
#define LOG_APPFILTER     0x00000001L
#define MAX_ERR           10


static const char cardErrTbl[MAX_ERR][22] =
{
    "NO ERR",                        //  0
    "NO DATA",                       //  1
    "NO STX",                        //  2
    "NO ETX",                        //  3
    "BAD LRC",                       //  4
    "B PARITY",                      //  5
    "REV ETX",                       //  6
    "BAD JIIS",                      //  7
    "BAD TRK",                       //  8
    "BUF OVR"                        //  9
};

static char bSmallDisplay = 0;


// =================================================================================================
// read environment variable in order to determine the routing (DQ driver / pipe) for this event type
int EventViaDQDriver (enum AtsEvent event)
{
  int  n  = 0;
  char is = '0';


  switch (event)
  {
    case ATSEVENT_TERMINAL_DISPLAY: n = get_env ("*VATS_DISP_VIA_DQDRV",      &is, 1); break;
    case ATSEVENT_USER_MAGSTRIPE:   n = get_env ("*VATS_MAGSTRIPE_VIA_DQDRV", &is, 1); break;
    default:
         LOG_PRINTF ("test_app.c: EventViaDQDriver: invalid parameter value\n");
         return 1;
  }

  if (n==1 && is=='0')
    return 0;
  else
    return 1;
}


// =================================================================================================
int vats_clrscr (void)
{
  int  res = 0;
  char dummybuffer [3] = " ";


  res = clrscr ();

  if ( AtsIsActive () && (0 == EventViaDQDriver (ATSEVENT_TERMINAL_DISPLAY)) && (res == 0) )
  {
    if ( ATSERROR_OK == AtsSendDisplayText (strlen( dummybuffer ), dummybuffer, 0, 0) )
      return 0;
    else
      return -1;
  }
  return res;
}


// =================================================================================================
int vats_write_at (char *buf, int len, int x, int y)
{
  int res;


  res = write_at (buf, len, x, y);

  if ( AtsIsActive () && (0 == EventViaDQDriver (ATSEVENT_TERMINAL_DISPLAY)) && (res >= 0) )
  {
    if ( ATSERROR_OK != AtsSendDisplayText (len, buf, x, y) )
      return -1;
    return len;
  }
  return res;
}


// =================================================================================================
void DisplayMagData2 (char *dataBuf, int dataLen, int *swipeCount)
{
  int  rowHead      = 1;
  int  rowSwipe     = 7;
  int  rowKeyPress  = 8;
  int  rowCounter   = 6;
  int  rowNoOfBytes = 5;
  char dispBuf[60]  = "";


  (*swipeCount)++;
  vats_clrscr();
  if( 0 == bSmallDisplay )
  {
    sprintf( dispBuf, "MSR test:" );
    vats_write_at( dispBuf, strlen( dispBuf ), 1, rowHead );
    sprintf( dispBuf, "Please swipe card" );
    vats_write_at( dispBuf, strlen( dispBuf ), 1, rowSwipe );
    sprintf( dispBuf, "Press CANCEL to abort" );
    vats_write_at( dispBuf, strlen( dispBuf ), 1, rowKeyPress );
    sprintf( dispBuf, "Number of swipes: %3d", *swipeCount );
    vats_write_at( dispBuf, strlen( dispBuf ), 1, rowCounter );
  }

  errno = 0;
  if( dataLen > 0 )
  {
    int i,j;


    if( 0 == bSmallDisplay )
    {
      sprintf( dispBuf, "Total Bytes: %4d", dataLen );
      vats_write_at( dispBuf, strlen( dispBuf ), 1, rowNoOfBytes );
    }
    else
    {
      sprintf( dispBuf, "MSR test: %3d (%3d)", dataLen, *swipeCount );
      vats_write_at( dispBuf, strlen( dispBuf ), 1, rowHead );
    }

    for (i = 1, j = 0; i <= 3; i++) // i: line, j: bytes
    {
      // size1  state1 (data1) size2  state2 (data2) size3  state3 (data3)
      sprintf( dispBuf, "TK#%d:%03d %02X-%.8s", i,
               ( ( dataBuf[j] > 0 ) ? ( dataBuf[j] - 2 ) : 0 ),
               dataBuf[j + 1],
               ( ( dataBuf[j + 1] >= MAX_ERR ) ? "UNKNOWN" : cardErrTbl[dataBuf[j + 1]] ) );
      vats_write_at( dispBuf, strlen( dispBuf ), 1, rowHead+i );
      j += dataBuf[j];
    }
    LOG_HEX_PRINTF( "magdata from OS: ", dataBuf, dataLen );
  }
  else
  {
    sprintf( dispBuf, "ERR: %-3d / %-3d", dataLen, errno );
    if( 0 == bSmallDisplay )
    {
      vats_write_at( dispBuf, strlen( dispBuf ), 1, rowNoOfBytes );
    }
    else
    {
      vats_write_at( dispBuf, strlen( dispBuf ), 1, rowHead+1 );
      sprintf( dispBuf, "MSR test:     (%3d)", *swipeCount );
      vats_write_at( dispBuf, strlen( dispBuf ), 1, rowHead );
    }
  }
}


// =================================================================================================
int main (void)
{
  int  ProcessingActive   = 0;                                  // magstripe processing is not active
  int  rowHead            = 1;
  int  rowSwipe           = 7;
  int  rowKeyPress        = 8;
  int  magHandle          = -1;
  int  event;
  int  dataLen;
  int  taskIDConsole;
  int  console;
  int  ret;
  int  swipeCount         = 0;
  char dispBuf[60]        = "";
  char dataBuf[500]       = "";
  enum AtsEvent ats_event = ATSEVENT_NONE;
  displayInfo_t myDispInfo;


  LOG_INIT (APPNAME, LOGSYS_OS, LOG_APPFILTER);
  vDebugOutVats( __MODULE__, __LINE__, "  Verix: test_app2: START: 26.09.2013" );

  // Initialize VATS lib (incl. VATS pipe)
  ret = AtsInitExt( APPNAME, (int)( ATSREG_MAGSTRIPE | ATSREG_KEYBOARD ), get_task_id(), 4 );
  vDebugOutVats( __MODULE__, __LINE__, "+++test_app2.c: VATS is active: %d task=%d ret=%d", AtsIsActive(), get_task_id(), ret );

  // owner of /DEV/MAG is used to determine its task id
  magHandle = open ("/DEV/MAG", 0);
  if (magHandle < 0)
  {
    vDebugOutVats( __MODULE__, __LINE__, "+++test_app2.c: Can't open //DEV//MAG" );
    SVC_WAIT( 3000 );                                           // msec
    return 1;
  }
  vDebugOutVats( __MODULE__, __LINE__, "+++test_app2.c: Opened //DEV//MAG" );

  console = get_owner (DEV_CONSOLE, &taskIDConsole);            // get the console handle
  if( 0 > console )
  {
    vDebugOutVats( __MODULE__, __LINE__, "+++test_app2.c: get_owner( DEV_CONSOLE ) failed." );
    SVC_WAIT( 3000 );                                           // msec
    close( magHandle );
    return 1;
  }

  // display size
  SVC_INFO_DISPLAY_EXT( &myDispInfo );
  if( ( myDispInfo.width <= 128 ) && ( myDispInfo.height < 64 ) )
  {
    bSmallDisplay = 1;
  }

  // wait for event EVT_ACTIVATE to start processing - otherwise pipe input is ignored
  while( 1 )
  {
    event = wait_evt( EVT_ACTIVATE | EVT_KBD | EVT_MAG | EVT_USER );

    vDebugOutVats( __MODULE__, __LINE__, "+++test_app2.c: EVENT received: event=%d.", event );
    if( event & EVT_ACTIVATE )
    {
      // dummy read before test start (in the case the ICC card in test with hybrid functions and empty MSR data was already performed)
      dataLen = read( magHandle, dataBuf, sizeof( dataBuf ) );
      // reset the variables
      dataLen = 0;
      memset( dataBuf, 0, sizeof( dataBuf ) );

      vDebugOutVats( __MODULE__, __LINE__, "+++test_app2.c: EVT_ACTIVATE received" );
      vats_clrscr();
      sprintf( dispBuf, "MSR test:" );
      vats_write_at( dispBuf, strlen( dispBuf ), 1, rowHead );
      if( 0 == bSmallDisplay )
      {
        sprintf( dispBuf, "Please swipe card" );
        vats_write_at( dispBuf, strlen( dispBuf ), 1, rowSwipe );
        sprintf( dispBuf, "Press CANCEL to abort" );
        vats_write_at( dispBuf, strlen( dispBuf ), 1, rowKeyPress );
      }
      ProcessingActive = 1;
      swipeCount = 0;                                           // count within one activation only
    }

    if( ProcessingActive == 1 )
    {
      if( event & EVT_KBD )
      {
        char key;


        if( 0 >= read( console, &key, 1 ) )
        {
          key = 0;
        }
        if( key == 0x9B )                                       // ABORT key
        {
          ret = activate_task( taskIDConsole );                 // return console access to original task
          vDebugOutVats( __MODULE__, __LINE__, "+++test_app2.c: taskIDConsole=%d ret= %d.", taskIDConsole, ret );
          ProcessingActive = 0;
        }
      }
      else if( event & EVT_MAG && card_pending() )              // card_pendig == 1: card swipe is available
      {
        vDebugOutVats( __MODULE__, __LINE__, "+++test_app2.c: EVT_MAG via DQ driver" );
        dataLen = read( magHandle, dataBuf, sizeof( dataBuf ) );
        DisplayMagData2( dataBuf, dataLen, &swipeCount );
      }
      else if( AtsIsActive() && ( 0 == EventViaDQDriver( ATSEVENT_USER_MAGSTRIPE ) ) && ( event & EVT_USER ) )
      {
        // handle pipe events
        ats_event = ATSEVENT_NONE;
        dataLen = sizeof( dataBuf ) - 1;

        vDebugOutVats( __MODULE__, __LINE__, "+++test_app2.c: EVT_USER" );
        if( ATSERROR_OK == AtsReceiveEvent( &ats_event, 0, &dataLen, (void *)dataBuf ) )
        {
          vDebugOutVats( __MODULE__, __LINE__, "+++test_app2.c: EVT_USER: ats_event=%d.", ats_event );
          if( ats_event == ATSEVENT_USER_MAGSTRIPE )
          {
            DisplayMagData2( dataBuf, dataLen, &swipeCount );
          }
          else if( ats_event == ATSEVENT_USER_KEYBOARD )        // ABORT by any key
          {
            if( dataBuf [0] == 0x9B )                           // ABORT key
            {
              vDebugOutVats( __MODULE__, __LINE__, "+++test_app2.c: ABORT: console=%d.", console );
              ret = activate_task( taskIDConsole );             // return console access to original task
              vDebugOutVats( __MODULE__, __LINE__, "+++test_app2.c: taskIDConsole=%d ret=%d.", taskIDConsole, ret );
              ProcessingActive = 0;
            }
          }
          else
          {
            vDebugOutVats( __MODULE__, __LINE__, "+++test_app2.c: IGNORED: ats_event=%d event=%d.", ats_event, event );
          }
        }
      }
    }
    else
    {
      if( AtsIsActive() && ( 0 == EventViaDQDriver( ATSEVENT_USER_MAGSTRIPE ) ) && ( event & EVT_USER ) )
      {
        // dummy read before test start (in the case the ICC card in test with hybrid functions and empty MSR data was already performed)
        dataLen = sizeof( dataBuf ) - 1;
        (void)AtsReceiveEvent( &ats_event, 0, &dataLen, (void *)dataBuf );
        // reset the variables
        dataLen = 0;
        memset( dataBuf, 0, sizeof( dataBuf ) );
      }
    }
  } // while( 1 )
}

#endif
