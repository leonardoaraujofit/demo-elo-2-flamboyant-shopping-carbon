/*****************************************************************************/
/**
 * \file test_app_verix_html.cpp
 *
 * \author GSS R&D Germany
 *
 * Company: VeriFone GmbH
 *
 * Product: Verifone Automated Test System (VATS)
 *
 *       (c) Copyright 2012
 */
/*****************************************************************************/


#ifndef _VOS
#define _EMV


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <libvoy.h>
#include <svc.h>
#include <eoslog.h>
#include <svc_sec.h>                    // PIN entry
#include "stdlib2.h"
#include "atsmisc.h"
#include "atslib.h"
#include <svc_net.h>
#include "gui.h"
#include "prt.h"                        // HTML printer

#include "test_app_emv.h"               // ADKVATS-215
#include "all_test_apps.h"


#define APPNAME                    "TESTAPP"
#define LOG_APPFILTER              0x00000001L

#define EMV_PIN                    0x0A


#ifndef MIN
#define MIN(a,b) ((a)<(b)?(a):(b))
#endif
#ifndef POST_PROC_PIN
#define POST_PROC_PIN 0x0B
#endif
#define UI_PIN_TIMEOUT                30000               // Enter PIN dialog timeout in msec
//error code for emv init
#define EMV_INIT_ERR_NONE             0x00
#define EMV_INIT_ERR_GET_MEM          0x010000
#define EMV_INIT_ERR_INIT_KERNEL      0x020000
#define EMV_INIT_ERR_INIT_PP_COMM_BUF 0x030000
#define EMV_INIT_ERR_PP_IDENT         0x040000
#define EMV_INIT_ERR_SET_TERM_DATA    0x050000
#define EMV_INIT_ERR_SET_APPLI_DATA   0x060000
#define EMV_INIT_ERR_SET_CAP_KEY      0x070000
#define EMV_INIT_ERR_APPLY_CFG        0x080000

#define UC_READER_0                   0

#define PRINTER_LINE_LENGTH           24

#define EMV_TRACE_MAX_LINE_LEN        112


enum IccState
{
  ICC_NONE,
  ICC_IN,
  ICC_OUT
};


struct customEventDisplay
{
  char *line1;
  char *line2;
  char *line3;
  char *line4;
  char *line5;
  char *line6;
  char *line7;
  char *line8;
  int   lineLength;
};


static IccState cardInStatus        = ICC_NONE;
static int      physicalCardIn      = 0;
static char     printerAvailable    = 'X';

int             magHandle           = -1;
static char     magDataBuffer [500] = {0};
static int      magDataBufferLen    = 0;

static bool doMagDataTest = false;  // ADKVATS-164
static bool doPINTest     = false;  // ADKVATS-164
static bool doCardinTest  = false;  // ADKVATS-164

static const int mainMenuItems[] = {
  1,
  2,
  3,
  4,
  5,
  0                               // this item has to remain the last one, as this value is used to mark the end of the array
};

static const int miscMenuItems[] = {
  VATS_GUI_TEST_ID_CUSTOM1,
  VATS_GUI_TEST_ID_CUSTOM2,
  VATS_GUI_TEST_ID_TRACE1,
  VATS_GUI_TEST_ID_DATE_TIME1,
  VATS_GUI_TEST_ID_UNIT_TEST,
  VATS_GUI_TEST_ID_NONE           // this item has to remain the last one, as this value is used to mark the end of the array
};

static const int dispMenuItems[] = {
  VATS_GUI_TEST_ID_DISPLAY1,
  VATS_GUI_TEST_ID_DISPLAY2,
  VATS_GUI_TEST_ID_DISPLAY3,
  VATS_GUI_TEST_ID_NONE           // this item has to remain the last one, as this value is used to mark the end of the array
};

static const int userMenuItems[] = {
  VATS_GUI_TEST_ID_KEYBOARD1,
  VATS_GUI_TEST_ID_KEYBOARD2,
  VATS_GUI_TEST_ID_KEYBOARD3,
  VATS_GUI_TEST_ID_TOUCH1,
  VATS_GUI_TEST_ID_MULTI_MENU1,
  VATS_GUI_TEST_ID_NONE           // this item has to remain the last one, as this value is used to mark the end of the array
};

static const int printMenuItems[] = {
  VATS_GUI_TEST_ID_PRINTER1,
  VATS_GUI_TEST_ID_NONE           // this item has to remain the last one, as this value is used to mark the end of the array
};

static const int cardMenuItems[] = {
  VATS_GUI_TEST_ID_ICC_CARDIN1,
  VATS_GUI_TEST_ID_MSR1,
  VATS_GUI_TEST_ID_EMV1,
  VATS_GUI_TEST_ID_NONE           // this item has to remain the last one, as this value is used to mark the end of the array
};

static const int tutMenuItems[] = {
  VATS_GUI_TEST_ID_UNIT_MAGDATA,
  VATS_GUI_TEST_ID_UNIT_PININPUT,
  VATS_GUI_TEST_ID_NONE           // this item has to remain the last one, as this value is used to mark the end of the array
};

static bool bSmallDisplay = false;
const char  tType_e315[]  = "e315        ";
const char  tType_e335[]  = "e335        ";


void menu_terminal_unit_test (void);


using namespace std;
using namespace vfigui;


// =================================================================================================
// read manufacturing block in order to determine if a printer is available
int PrinterAvailable( void )
{
  int  result    = 0;
  char myPrinter = '0';


  if( 'X' == printerAvailable )
  {
    result = SVC_INFO_PRNTR( &myPrinter );
    if( 0 == result )
    {
      printerAvailable = myPrinter;
    }
    if( '0' == printerAvailable )
    {
      char model[20];


      memset( model, 0, sizeof( model ) );
      if( 0 == SVC_INFO_MODELNO( model ) )
      {
        if( ( 0 == strncmp( "VX820", model, strlen( "VX820" ) ) ) && ( get_usb_device_bits() & ( UDB_IBHUB | UDB_COM4 ) ) )
        {
          // set value to '4' to recognize that a printer is present
          printerAvailable = '4';
        }
      }
    }
  }
  // value is '2' on VX675, DOC00301_Verix_eVo_Volume_I_Operating_System_Programmers_Manual.pdf says 0 or 1.
  // value is '4' for VX820 DUET (see above)
  if( '0' != printerAvailable )
  {
    result = 1;
  }
  else
  {
    result = 0;
  }
  return result;
}


// =================================================================================================
// read environment variable in order to determine the routing (DQ driver / pipe) for this event type
int EventViaDQDriver (enum AtsEvent event)
{
  int  n  = 0;
  char is = '0';


  switch (event)
  {
    case ATSEVENT_TERMINAL_DISPLAY: n = get_env ("*VATS_DISP_VIA_DQDRV",      &is, 1); break;
    case ATSEVENT_TERMINAL_PRINTER: n = get_env ("*VATS_PRINTER_VIA_DQDRV",   &is, 1); break;
    case ATSEVENT_USER_KEYBOARD:    n = get_env ("*VATS_KBD_VIA_DQDRV",       &is, 1); break;
    case ATSEVENT_USER_MAGSTRIPE:   n = get_env ("*VATS_MAGSTRIPE_VIA_DQDRV", &is, 1); break;
    case ATSEVENT_USER_ICCCARD_IN:  n = get_env ("*VATS_CARDIN_VIA_DQDRV",    &is, 1); break;
    default:
      LOG_PRINTF ("test_app.c: EventViaDQDriver: invalid parameter value\n");
      return 1;
  }

  if (n==1 && is=='0')
    return 0;
  else
    return 1;
} // EventViaDQDriver


// =================================================================================================
int touchScreenAvailable( void )
{
  int devices = 0;
  int retVal  = 0;


  devices = SVC_INFO_PRESENT();

  vDebugOutVats( __MODULE__, __LINE__, "devices=0x%08X\n", devices );
  if( ( devices & INFO_PRES_TOUCH ) != 0 )
  {
    retVal = 1;
  }

  return retVal;
}


// =================================================================================================
void display_test (void)
{
  vDebugOutVats( __MODULE__, __LINE__, "test_app: display_test called\n" );
  uiConfirm ("confirm-cancel", "VATS test<br>&nbsp;VATS test<br>&nbsp;&nbsp;VATS test<br>&nbsp;&nbsp;&nbsp;VATS test<br>&nbsp;&nbsp;&nbsp;&nbsp;VATS test<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VATS test<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VATS test<input type='timeout' value='3' style='visibility:hidden' action='return 100'>");
}


// =================================================================================================
void keyboard_test (void)
{
  int r = UI_ERR_OK;
  vector<string> value(1);


  vDebugOutVats( __MODULE__, __LINE__, "test_app: keyboard_test called\n" );

  while( UI_ERR_ABORT != r )
  {
    value[0] = "1";
    if( bSmallDisplay )
    {
      r = uiInput ("input", value, "<h4>Input test</h4>Enter amount <input type='number' precision='0' maxlength='3'>");
    }
    else
    {
      r = uiInput ("input", value, "<h4>Input test</h4>Enter amount <input type='number' precision='0' maxlength='3'><br>CANCEL to abort");
    }

    vDebugOutVats( __MODULE__, __LINE__, "test_app: keyboard_test: r=%d\n", r );
    if (r == UI_ERR_OK)
    {
      if( bSmallDisplay )
      {
        uiDisplay (string("<h4>Input test</h4>Amount: <input type='timeout' value='2' style='visibility:hidden' action='return 200'>")+value[0]);
      }
      else
      {
        uiDisplay (string("<h4>Input test</h4>Amount:<br><input type='timeout' value='2' style='visibility:hidden' action='return 200'>")+value[0]);
      }
    }
  }
} // keyboard_test


// =================================================================================================
unsigned long SmartOpen ( void )
{
  DWORD retVal = IFD_Failure;
  unsigned char CardBuffer [64];


  // convert to Big Endian
  CardBuffer[0] = 0;
  CardBuffer[1] = 0;
  CardBuffer[2] = 0;
  CardBuffer[3] = CUSTOMER_CARD;

  // open the icc
  retVal = IFD_Set_Capabilities( Tag_Open_ICC, CardBuffer );
  if( IFD_Success != retVal )
  {
    vDebugOutVats( __MODULE__, __LINE__, "  test_app: SmartOpen: Tag_Open_ICC: ERROR: res=%ld\n", retVal );
  }
  else
  {
    // select the icc
    retVal = IFD_Set_Capabilities( Tag_Select_ICC, CardBuffer );
    if( IFD_Success != retVal )
    {
      vDebugOutVats( __MODULE__, __LINE__, "  test_app: SmartOpen: Tag_Select_ICC: ERROR: res=%ld\n", retVal );
    }
  }

  return retVal;
} // SmartOpen


// =================================================================================================
// callback function for uiConfirm () that handles a cardin event or a physically inserted/removed
// card
bool cb_cardin (void *)
{
  int           iRet;
  enum AtsEvent ats_event = ATSEVENT_NONE;
  char          dataBuf [500] = {0};
  int           datalen   = sizeof (dataBuf);


  if ( ATSERROR_OK == AtsReceiveEvent (&ats_event, 0, &datalen, (void*) dataBuf) )
  {
    vDebugOutVats( __MODULE__, __LINE__, "     test_app: cb_cardin: AtsReceiveEvent: ats_event=%d dataBuf[0]=%d\n", ats_event, dataBuf [0] );
    if (ats_event == ATSEVENT_USER_ICCCARD_IN)
    {
      if (dataBuf [0] == 1)
        cardInStatus = ICC_IN;
      else
        cardInStatus = ICC_OUT;
      return false; // terminate uiConfirm ()
    }
    else
    {
      vDebugOutVats( __MODULE__, __LINE__, "     test_app: cb_cardin: AtsReceiveEvent: E R R O R\n" );
    }
  }

  // examine physical card_in
  iRet = IFD_Is_ICC_Present();

  if (iRet != physicalCardIn)
  {
    vDebugOutVats( __MODULE__, __LINE__, "     test_app: cb_cardin: physical card detected\n" );
    physicalCardIn = iRet;
    return false; // terminate uiConfirm ()
  }
  return true;    // continue uiConfirm ()
} // cb_cardin


// =================================================================================================
int prepareIccDisplay (IccState iccStat, int *smartOpen)
{
  int  iRet;
  char lines [7][21]; // VX520
  int  line = 0;


  vDebugOutVats( __MODULE__, __LINE__, "  test_app: prepareIccDisplay\n" );
  memset( lines, 0x00, sizeof( lines ) );
  sprintf( lines[line++], "Card in test" );
  if( !bSmallDisplay )
  {
    if( AtsIsActive() )
    {
      sprintf( lines[line++], "VATS active" );
    }
    else
    {
      sprintf( lines[line++], "VATS inactive" );
    }
  }

  if( iccStat == ICC_IN )
  {
    sprintf( lines[line++], "VATS cardin: in" );
  }
  else if( iccStat == ICC_OUT )
  {
    sprintf( lines[line++], "VATS cardin: out" );
  }
  else
  {
    sprintf( lines[line++], "VATS cardin: ???" );
  }

  if( *smartOpen == 0 )
  {
    iRet = SmartOpen();
    if( !bSmallDisplay )
    {
      if( iRet == 0 )
      {
        sprintf( lines[line++], "Phys. init:   ok" );
        *smartOpen = iRet;
      }
      else
      {
        sprintf( lines[line++], "Phys. init: %d", iRet );
        *smartOpen = iRet;
      }
    }
  }
  else
  {
    if( !bSmallDisplay )
    {
      sprintf( lines[line++], "Phys. init:   ok" );
    }
  }

  physicalCardIn = IFD_Is_ICC_Present();

  if( physicalCardIn == IFD_Success )
  {
    sprintf( lines[line++], "Phys. cardin: in" );
  }
  else
  {
    sprintf( lines[line++], "Phys. cardin: out" );
  }

  if( !bSmallDisplay )
  {
    sprintf( lines[line++], "Please insert card" );
    sprintf( lines[line++], "CANCEL to abort" );
  }

  if( bSmallDisplay )
  {
    iRet = uiConfirm ("confirm-cancel", uiPrint ("%s<br>%s<br>%s<br>%s<br>%s<br>%s<br>%s", lines[0], lines[1], lines[2]), cb_cardin );
  }
  else
  {
    iRet = uiConfirm ("confirm-cancel", uiPrint ("%s<br>%s<br>%s<br>%s<br>%s<br>%s<br>%s", lines[0], lines[1], lines[2], lines[3], lines[4], lines[5], lines[6]), cb_cardin );
  }

  vDebugOutVats( __MODULE__, __LINE__, "  test_app: prepareIccDisplay: DONE\n" );
  return iRet;
} // prepareIccDisplay


// =================================================================================================
void cardin_test (void)
{
  int  smartOpen = 0;
  int  r         = UI_ERR_OK;


  vDebugOutVats( __MODULE__, __LINE__, "  test_app: cardin_test\n" );

  while( UI_ERR_ABORT != r )
  {
    r = prepareIccDisplay (cardInStatus, &smartOpen);
  } // while
} // cardin_test


// =================================================================================================
bool cb_magdata (void *)
{
  int           res;
  int           ats_len;
  enum AtsEvent ats_event = ATSEVENT_NONE;


  magDataBufferLen = 0;
  // handle physical mag data
  memset (magDataBuffer, 0x00, sizeof (magDataBuffer));
  res = read (magHandle, magDataBuffer, sizeof (magDataBuffer));

  if (res > 0)
  {
    vDebugOutVats( __MODULE__, __LINE__, "  test_app: cb_magdata: read: physical data available: res=%d\n", res );
    magDataBufferLen = res;
    return false;   // terminate uiConfirm ()
  }

  ats_len = sizeof (magDataBuffer);
  if ( ATSERROR_OK == AtsReceiveEvent (&ats_event, 0, &ats_len, (void*) magDataBuffer) ) // timeout = 0
  {
    if (ats_event == ATSEVENT_USER_MAGSTRIPE)
    {
      magDataBufferLen = ats_len;
      vDebugOutVats( __MODULE__, __LINE__, "  test_app: cb_magdata: AtsReceiveEvent: ats_event=%d magDataBufferLen=%d\n", ats_event, magDataBufferLen );
      return false;   // terminate uiConfirm ()
    }
  }

  return true;    // continue uiConfirm ()
} // cb_magdata


// =================================================================================================
int DisplayMagData2 (char *dataBuf, int dataLen, int *swipeCount)
{
  int ret;
  char lines[7][21]; // VX520


  vDebugOutVats( __MODULE__, __LINE__, "  test_app: DisplayMagData2: dataLen=%d\n", dataLen );

  memset( lines, 0x00, sizeof( lines ) );
  sprintf( lines[0], "Magstripe test" );

  if( dataLen > 0 )
  {
    (*swipeCount)++;
  }
  sprintf( lines[5], "No. of swipes: %3d", *swipeCount );
  if( !bSmallDisplay )
  {
    sprintf( lines[6], "CANCEL to abort" );
  }

  errno = 0;
  if( dataLen > 0 )
  {
    int i,j;


    sprintf( lines[4], "Total Bytes: %4d", dataLen );
    for( i = 1, j = 0; i <= 3; i++ )  // i: track, j: bytes
    {
      // size1  state1 (data1) size2  state2 (data2) size3  state3 (data3)
      sprintf ( lines[i], "TK#%d:%03d %02X-%.8s", i,
                ((dataBuf[j] > 0) ? (dataBuf[j] - 2) : 0),
                dataBuf[j + 1],
                ((dataBuf[j + 1] >= MAX_ERR) ? "UNKNOWN" : VATScardErrTbl[int (dataBuf[j + 1])]) );
      j += dataBuf[j]; // increase j by size of track data
    }

    vDebugOutVats( __MODULE__, __LINE__, "  test_app: DisplayMagData2: Magdata: %s %s %s\n", lines[1], lines[2], lines[3] );
    ret = uiConfirm( "confirm-cancel", uiPrint( "%s<br>%s<br>%s<br>%s<br>%s<br>%s<br>%s", lines[0], lines[1], lines[2], lines[3], lines[4], lines[5], lines[6] ), cb_magdata );
  }
  else
  {
    sprintf( lines[4], "ERR: %-3d / %-3d", dataLen, errno );
    ret = uiConfirm( "confirm-cancel", uiPrint( "%s<br><br><br><br>%s<br>%s<br>%s", lines[0], lines[4], lines[5], lines[6] ), cb_magdata );
  }
  return ret;
} // DisplayMagData2


// =================================================================================================
void magstripe_test (void)
{
  int  res;
  int  swipeCount = 0;


  vDebugOutVats( __MODULE__, __LINE__, "  test_app: magstripe_test\n" );

  // owner of /DEV/MAG is used to determine its task id
  magHandle = open ("/DEV/MAG", 0);
  if (magHandle < 0)
  {
    vDebugOutVats( __MODULE__, __LINE__, "test_app: magstripe_test: Can't open //DEV//MAG\n" );
    return;
  }
  vDebugOutVats( __MODULE__, __LINE__, "test_app: magstripe_test: Opened //DEV//MAG\n" );

  // dummy read before test start (in the case the ICC card in test with hybrid functions and empty MSR data was already performed)
  magDataBufferLen = (int)cb_magdata( NULL );
  // reset the variables
  magDataBufferLen = 0;
  memset (magDataBuffer, 0x00, sizeof (magDataBuffer));

  res = UI_ERR_OK;
  while( UI_ERR_ABORT != res )
  {
    vDebugOutVats( __MODULE__, __LINE__, "  test_app: magstripe_test: magDataBufferLen=%d\n", magDataBufferLen );
    res = DisplayMagData2 (magDataBuffer, magDataBufferLen, &swipeCount);
    svcSleep (100);
  } // while
  close (magHandle);
} // magstripe_test


// =================================================================================================
void trace_event_test( void )
{
  char  txtTrace[]   = "VATS trace testing!";
  char *trc;
  char  txtPattern[] = "test ...";
  int   patternSize  = strlen( txtPattern );
  int   siz          = ( (PIPE_MAX_MSG_LEN - VATS_PIPE_OVERHEAD) / ( patternSize + 2 ) ) * patternSize;     // don't exceed max. pipe message length, and have a multiple of patternSize
  int   i            = 0;


  vDebugOutVats( __MODULE__, __LINE__, "  test_app: trace_event_test\n" );

  // 1st trace: very short
  AtsTraceEvent( /*source=*/7, /*channel=*/8, strlen( txtTrace ), txtTrace );

  // 2nd trace: very long, will be chained on PC interface
  trc = (char *)malloc( siz );
  if( NULL != trc )
  {
    for( i = 0; i < ( siz / patternSize ); i++ )
    {
      memcpy( (trc + ( i * patternSize ) ), txtPattern, patternSize );
    }
    AtsTraceEvent( /*source=*/5, /*channel=*/6, siz, trc );
    free( trc );
    trc = NULL;
  }
  if( bSmallDisplay )
  {
    uiConfirm( "confirm-cancel", "<h4>Trace</h4>Sent to PC.<input type='timeout' value='3' style='visibility:hidden' action='return 600'>" );
  }
  else
  {
    uiConfirm( "confirm-cancel", "<h4>Trace</h4>Trace messages have been sent to PC.<input type='timeout' value='3' style='visibility:hidden' action='return 600'>" );
  }
} // trace_event_test


// =================================================================================================
void screen_test (void)
{
  vDebugOutVats( __MODULE__, __LINE__, "  test_app: screen_test: called\n" );
  uiConfirm ("confirm-cancel", "<h4>Display</h4>1VATS Screen 123456782<br>VATS Screen 123456783<br>VATS Screen 123456784<br>VATS Screen 123456785<br>VATS Screen 123456786<br>VATS Screen 123456787<br>VATS Screen 12345678<br>8VATS Screen 12345678<input type='timeout' value='3' style='visibility:hidden' action='return 300'>");
} // screen_test


// =================================================================================================
void custom_event_test (void)
{
  // send a custom event
  char dat[] = "Hello World! \x01\x02\x03\x04";


  vDebugOutVats( __MODULE__, __LINE__, "  test_app: custom_event_test\n" );
  AtsSendEvent( ATSEVENT_CUSTOM_START+1, sizeof (dat), (const void*) dat );
  if( bSmallDisplay )
  {
    uiConfirm( "confirm-cancel", "<h4>DUT to PC Test</h4>Event has been sent.<input type='timeout' value='3' style='visibility:hidden' action='return 500'>" );
  }
  else
  {
    uiConfirm( "confirm-cancel", "<h4>DUT to PC Test</h4>Custom event has been sent to PC.<input type='timeout' value='3' style='visibility:hidden' action='return 500'>" );
  }
} // custom_event_test


// =================================================================================================
int convertBufferSliceToVisibleStringLine( char *destination, int destinationMaxCharLen, char *source, int sourceMaxLen )
{
  int sourceOffset = 0;


  if( ( NULL != destination ) && ( 0 < destinationMaxCharLen ) && ( NULL != source ) && ( 0 < sourceMaxLen ) )
  {
    int  destinationOffset = 0;
    char nibble            = ' ';


    while( ( destinationMaxCharLen > destinationOffset ) && ( sourceMaxLen > sourceOffset ) )
    {
      if(
          !(
                (
                      ( (signed char)source[sourceOffset] >= 0 )
                  &&  ( source[sourceOffset] <  ' ' )
                )
            ||  ( source[sourceOffset] == 127 )
            ||  ( (signed char)source[sourceOffset] == (-1) )
          )
        )
      {
        destination[destinationOffset++] = source[sourceOffset];
      }
      else if( source[sourceOffset] == '\n' )
      {
        sourceOffset++;
        break;
      }
      else
      {
        if( (destinationMaxCharLen - 2) > destinationOffset )
        {
          destination[destinationOffset++] = '\\';
          nibble = (source[sourceOffset] >> 4) & 0xF;
          destination[destinationOffset++] = (nibble < 10) ? (nibble + '0') : (nibble - 10 + 'A');
          nibble = source[sourceOffset] & 0xF;
          destination[destinationOffset++] = (nibble < 10) ? (nibble + '0') : (nibble - 10 + 'A');
        }
        else
        {
          break;
        }
      }
      sourceOffset++;
    }
    destination[destinationOffset] = '\0';
  }
  return sourceOffset;
} // convertBufferSliceToVisibleStringLine


// =================================================================================================
bool customEvent_callBack( void *pDisplay )
{
  enum AtsEvent       ats_event    = ATSEVENT_NONE;
  char               *eventData    = NULL;
  int                 eventDataLen = 0;
  customEventDisplay *myDisplay    = (customEventDisplay *)pDisplay;


  if( ATSERROR_OK == AtsReceiveEventChained( &ats_event, 0, &eventDataLen, (void **)(&eventData) ) )   // timeout = 0
  {
    if( ( ats_event > ATSEVENT_CUSTOM_START ) && ( ats_event < ATSEVENT_CUSTOM_END ) )
    {
      int   offset  = 0;
      int   myCount = 0;
      int   i       = 0;
      char *buf     = NULL;


      vDebugOutVats( __MODULE__, __LINE__, "test_app: customEvent_callBack: ats_event=%d eventDataLen=%d\n", ats_event, eventDataLen );
      sprintf( myDisplay->line1, "Custom event: %d", ats_event );
      sprintf( myDisplay->line2, "Data length: %d", eventDataLen );
      memset( myDisplay->line3, 0, myDisplay->lineLength );
      memset( myDisplay->line4, 0, myDisplay->lineLength );
      memset( myDisplay->line5, 0, myDisplay->lineLength );
      memset( myDisplay->line6, 0, myDisplay->lineLength );
      memset( myDisplay->line7, 0, myDisplay->lineLength );
      if( !bSmallDisplay )
      {
        sprintf( myDisplay->line8, "CANCEL to abort" );
      }
      for( i = 3; i <= 7; i++ )
      {
        myCount = 0;
        switch( i )
        {
          case 3:
            buf = myDisplay->line3;
            break;
          case 4:
            buf = myDisplay->line4;
            break;
          case 5:
            buf = myDisplay->line5;
            break;
          case 6:
            buf = myDisplay->line6;
            break;
          case 7:
            buf = myDisplay->line7;
            break;
        }
        memset( buf, 0, myDisplay->lineLength );
        myCount = convertBufferSliceToVisibleStringLine( buf, ( myDisplay->lineLength - (myDisplay->lineLength % 2) ), &eventData[offset], (eventDataLen - offset) );
        if( 0 == myCount )
        {
          break;
        }
        offset += myCount;
        if( offset >= eventDataLen )
        {
          break;
        }
      }
      if( NULL != eventData )
      {
        free( eventData );
        eventData = NULL;
      }
      return false;     // terminate uiConfirm ()
    }
  }
  if( NULL != eventData )
  {
    free( eventData );
    eventData = NULL;
  }
  return true;          // continue uiConfirm ()
} // customEvent_callBack


// =================================================================================================
void custom_event_test2( void )
{
  int                ret = UI_ERR_OK;
  char               lines[8][CE2_MAX_LINE_LEN];
  customEventDisplay myDisplay;


  memset( lines, 0x00, sizeof( lines ) );
  myDisplay.line1 = &lines[0][0];
  myDisplay.line2 = &lines[1][0];
  myDisplay.line3 = &lines[2][0];
  myDisplay.line4 = &lines[3][0];
  myDisplay.line5 = &lines[4][0];
  myDisplay.line6 = &lines[5][0];
  myDisplay.line7 = &lines[6][0];
  myDisplay.line8 = &lines[7][0];
  myDisplay.lineLength = CE2_MAX_LINE_LEN;

  sprintf( lines[0], "AppName: '%s'", APPNAME );
  if( bSmallDisplay )
  {
    sprintf( lines[1], "Awaiting event." );
  }
  else
  {
    sprintf( lines[1], "Waiting for custom" );
    sprintf( lines[2], "event from test" );
    sprintf( lines[3], "script." );
    sprintf( lines[7], "CANCEL to abort" );
  }
  while( UI_ERR_ABORT != ret )
  {
    ret = uiConfirm( "confirm-cancel", uiPrint( "%s<br>%s<br>%s<br>%s<br>%s<br>%s<br>%s<br>%s", lines[0], lines[1], lines[2], lines[3], lines[4], lines[5], lines[6], lines[7] ), customEvent_callBack, &myDisplay );
  }
} // custom_event_test2


// =================================================================================================
void html_printer_test (void)
{
#if 0 // long duration test for HTML printer available

  bool               landscape = false;
  map<string,string> value;
  int                r         = vfiprt::PRT_OK;


  vDebugOutVats( __MODULE__, __LINE__, "  test_app: html_printer_test: called\n" );

  for( int i = 0; i < 2000; i++ )
  {
    value.clear();
    value["receipt_n"] = "001";
    value["itemlist"]  = "name:value";
    vDebugOutVats( __MODULE__, __LINE__, "  test_app: html_printer_test: i=%d\n", i );
    uiConfirm( "confirm-cancel", uiPrint( "<h4>Printer test</h4>%s<br>%d<input type='timeout' value='1' style='visibility:hidden' action='return 400'>", value["receipt_n"].c_str(), i ) );
    r = vfiprt::prtURL( value, "receipt.html", landscape );
    if( vfiprt::PRT_OK != r )
    {
      vDebugOutVats( __MODULE__, __LINE__, "  test_app: html_printer_test: ERROR: r=%d\n", r );
      return;
    }

    {
      int j;
      struct task_info t_info;


      for (j = 1; j < 70; j++)
      {
        // read accumulated process time for all tasks
        int res;
        if (get_task_info (j, &t_info) > 0)
          vDebugOutVats(__MODULE__, __LINE__, "  test_app: html_printer_test: get_task_info: j=%d id=%d stack=%ld data=%ld sts=%d path=%s\n", j, t_info.id, t_info.stacksize, t_info.datasize, (int) t_info.sts, t_info.path );
        else
          break;
      }
    }
  }

#else   // #if 0

  bool               landscape = false;
  JSObject           jHelp;
  map<string,string> value;
  int                r         = vfiprt::PRT_OK;


  vDebugOutVats( __MODULE__, __LINE__, "  test_app: html_printer_test: called\n" );

  // first receipt
  value.clear();
  value["receipt_n"]="007";
  jHelp[0]("name")="my name";
  jHelp[0]("value")="VATS test";
  jHelp[1]("name")="high score";
  jHelp[1]("value")=7000;
  value["itemlist"]=jHelp.dump();
  uiConfirm ("confirm-cancel", uiPrint( "<h4>Printer test</h4>%s<input type='timeout' value='5' style='visibility:hidden' action='return 400'>", value["receipt_n"].c_str() ) );
  r = vfiprt::prtURL( value, "receipt.html", landscape );
  vDebugOutVats( __MODULE__, __LINE__, "  test_app: html_printer_test: receipt.html=%d\n", r );
  uiConfirm ("confirm-cancel", uiPrint( "<h4>Printer test</h4>%s<br>Result: %d<input type='timeout' value='5' style='visibility:hidden' action='return 401'>", value["receipt_n"].c_str(), r ) );
  if( vfiprt::PRT_OK != r )
  {
    uiConfirm ("confirm-cancel", uiPrint( "<h4>Printer test</h4>%s<br>Reprinting...<input type='timeout' value='5' style='visibility:hidden' action='return 402'>", value["receipt_n"].c_str() ) );
    r = vfiprt::prtURL( value, "receipt.html", landscape );
    uiConfirm ("confirm-cancel", uiPrint( "<h4>Printer test</h4>%s<br>Result: %d<input type='timeout' value='5' style='visibility:hidden' action='return 403'>", value["receipt_n"].c_str(), r ) );
  }
  // second receipt
  value.clear();
  value["receipt_n"]="0815";
  value["itemlist"]="";
  uiConfirm ("confirm-cancel", uiPrint( "<h4>Printer test</h4>%s<input type='timeout' value='5' style='visibility:hidden' action='return 404'>", value["receipt_n"].c_str() ) );
  r = vfiprt::prtURL( value, "receipt.html", landscape );
  uiConfirm ("confirm-cancel", uiPrint( "<h4>Printer test</h4>%s<br>Result: %d<input type='timeout' value='5' style='visibility:hidden' action='return 405'>", value["receipt_n"].c_str(), r ) );
  if( vfiprt::PRT_OK != r )
  {
    uiConfirm ("confirm-cancel", uiPrint( "<h4>Printer test</h4>%s<br>Reprinting...<input type='timeout' value='5' style='visibility:hidden' action='return 406'>", value["receipt_n"].c_str() ) );
    r = vfiprt::prtURL( value, "receipt.html", landscape );
    uiConfirm ("confirm-cancel", uiPrint( "<h4>Printer test</h4>%s<br>Result: %d<input type='timeout' value='5' style='visibility:hidden' action='return 407'>", value["receipt_n"].c_str(), r ) );
  }

#endif
} // html_printer_test


// =================================================================================================
void disp_region_test( void )
{
  const struct UIRegion reg_no_statusbar[]=
  {
    {UI_REGION_DEFAULT, 0, 0,-1,-1}   // main screen
  };
  const struct UIRegion reg_statusbarBig[]=
  {
    {1,                 0, 0,-1,31},  // status bar
    {UI_REGION_DEFAULT, 0,32,-1,-1}   // main screen
  };
  const struct UIRegion reg_statusbarSmall[]=
  {
    {1,                 0, 0,-1,11},  // status bar
    {UI_REGION_DEFAULT, 0,12,-1,-1}   // main screen
  };


  // show main screen
  if( bSmallDisplay )
  {
    uiDisplay( "Region test<br>Main screen." );
  }
  else
  {
    uiDisplay( "<h4>Region test</h4>Main screen for display regions test." );
  }
  // wait some time before changing the display regions
  svcSleep( 5000 );                   // msec
  // add status bar
  if( bSmallDisplay )
  {
    uiLayout( reg_statusbarSmall, sizeof( reg_statusbarSmall ) / sizeof( reg_statusbarSmall[0] ) );
  }
  else
  {
    uiLayout( reg_statusbarBig, sizeof( reg_statusbarBig ) / sizeof( reg_statusbarBig[0] ) );
  }
  uiDisplay( 1, "statusbar", "01.11.2013 12:45" );
  // wait some time before updating the main screen
  svcSleep( 3000 );                   // msec
  if( bSmallDisplay )
  {
    uiDisplay( "Region test<br>Update 1." );
  }
  else
  {
    uiDisplay( "<h4>Region test</h4>Main screen update 1." );
  }
  // wait some time before updating the display regions
  svcSleep( 3000 );                   // msec
  uiDisplay( 1, "statusbar", "01.11.2013 12:55" );
  if( bSmallDisplay )
  {
    uiDisplay( "Region test<br>Update 2." );
  }
  else
  {
    uiDisplay( "<h4>Region test</h4>Main screen update 2." );
  }
  // wait some time before updating the display regions
  svcSleep( 3000 );                   // msec
  if( bSmallDisplay )
  {
    uiDisplay( "Region test<br>Update 3." );
  }
  else
  {
    uiDisplay( "<h4>Region test</h4>Main screen update 3." );
  }
  uiDisplay( 1, "statusbar", "01.11.2013 13:05" );
  // wait some time before removing the status bar
  svcSleep( 3000 );                   // msec
  // remove the status bar
  uiLayout( reg_no_statusbar, sizeof( reg_no_statusbar ) / sizeof( reg_no_statusbar[0] ) );
  uiConfirm( "confirm-cancel", "<h4>Region test</h4>Main screen update after status bar removal.<input type='timeout' value='3' style='visibility:hidden' action='return 998'>" );
} // disp_region_test


// =================================================================================================
void pin_test( void )
{
  int                  r = UI_ERR_OK;
  map <string, string> value;


  vDebugOutVats( __MODULE__, __LINE__, "test_app: pin_test: called\n" );
  // modify these values to change default behaviour of PIN entry
  (void)uiSetPropertyInt( UI_PROP_PASSWORD_CHAR,  '*' );
  (void)uiSetPropertyInt( UI_PROP_PIN_AUTO_ENTER, 0 );        // 1 -> enable
  (void)uiSetPropertyInt( UI_PROP_PIN_CLEAR_ALL,  0 );        // 1 -> enable
  (void)uiSetPropertyInt( UI_PROP_PIN_BYPASS_KEY, 8 );        // 13 -> enter key, 8 -> clear key
  (void)uiSetPropertyInt( UI_PROP_PIN_ALGORITHM,  EMV_PIN );

  if( bSmallDisplay )
  {
    r = uiInvoke( value, "<h4>PIN test</h4>PIN <input type='pin' name='input' minlength='4' maxlength='6' action='return 0'> (4-6 dig)" );
  }
  else
  {
    r = uiInvoke( value, "<h4>PIN test</h4>PIN <input type='pin' name='input' minlength='4' maxlength='6' action='return 0'><br>(4 to 6 digits)" );
  }

  vDebugOutVats( __MODULE__, __LINE__, "test_app: pin_test: r=%d\n", r );
  string v = uiPrint( "Result: %d<br>", r );
  map <string, string>::iterator i;
  for( i = value.begin(); i != value.end(); ++i )
  {
    v += uiPrint( "%S=%S<br>", i->first.c_str(), i->second.c_str() );
  }
  v.append( "<input type='timeout' value='2' style='visibility:hidden' action='return -999'>" );
  uiConfirm( "confirm-cancel", v );
  vDebugOutVats( __MODULE__, __LINE__, "test_app: pin_test: done\n" );
} // pin_test


// =================================================================================================
void vats_key_inject_test (void)
{
  int r = UI_ERR_OK;


  vDebugOutVats( __MODULE__, __LINE__, "test_app: vats_key_inject_test called\n" );
  while( 20 != r )
  {
    r = uiConfirm( "key-inject", "Press any key<br>CANCEL to abort" );
    uiConfirm( "confirm-cancel", uiPrint( "Entered: %d<input type='timeout' value='2' style='visibility:hidden' action='return -999'>", r ) );
  }

  vDebugOutVats( __MODULE__, __LINE__, "test_app: vats_key_inject_test done\n" );
} // vats_key_inject_test


// =================================================================================================
void datetime_test( void )
{
  int    i = 1;
  int    r = UI_ERR_OK;
  char   dateAndTime[16];
  string myDisplay;


  vDebugOutVats( __MODULE__, __LINE__, "test_app: datetime_test called\n" );

  while( UI_ERR_ABORT != r )
  {
    memset( dateAndTime, 0x00, sizeof( dateAndTime ) );
    read_clock( dateAndTime );
    dateAndTime[( sizeof( dateAndTime ) - 2 )] = '\0';
    vDebugOutVats( __MODULE__, __LINE__, "test_app: datetime_test i=%d %s\n", i, dateAndTime );

    if( bSmallDisplay )
    {
      myDisplay.assign( uiPrint( "<h4>Date and Time</h4>#%3d, %s<br><input type='timeout' value='2' style='visibility:hidden' action='return 1100'>", i, dateAndTime ) );
    }
    else
    {
      myDisplay.assign( uiPrint( "<h4>Date and Time</h4>date and time %d:<br>%s<br><input type='timeout' value='2' style='visibility:hidden' action='return 1100'>", i, dateAndTime ) );
    }
    r = uiConfirm( "confirm-cancel", myDisplay );
    i++;
  }

  vDebugOutVats( __MODULE__, __LINE__, "test_app: datetime_test done\n" );
} // datetime_test


// =================================================================================================
void vats_touch_test (void)
{
  int r = UI_ERR_OK;


  vDebugOutVats( __MODULE__, __LINE__, "test_app: vats_touch_test called\n" );
  while( 20 != r )
  {
    r = uiConfirm( "touch-inject", "Touch any button<br>CANCEL to abort" );
    uiConfirm( "confirm-cancel", uiPrint( "Touched: %d<input type='timeout' value='2' style='visibility:hidden' action='return -999'>", r ) );
  }

  vDebugOutVats( __MODULE__, __LINE__, "test_app: vats_touch_test done\n" );
}


// =================================================================================================
void multi_menu_test()
{
  int ret = UI_ERR_OK;
  string myFirst1    = "First ";
  string mySecond1   = "Second ";
  string myThird     = "Third ";
  string myFourth    = "Fourth ";
  string myTestItem1 = "'Test item'";
  string myTestItem2 = "'Other item'";
  string myTestItem3 = "'Who am I'";
  string myIn        = " in ";
  string myFirst2    = "first";
  string mySecond2   = "second";
  string myEnd       = " menu was selected.<input type='timeout' value='2' style='visibility:hidden' action='return -999'>";
  string myOutput;


  ret = uiConfirm( "confirm-cancel", "<table border='0' cellspacing='0' cellpadding='0' style='height:100%;width:100%'><tr><td style='height:1px;vertical-align:top'><h4>Multiple menu</h4></td></tr><tr><td style='height:100%'>Menu 1<select name='testmenu1' size='4' style='width:100%'><option action='return 4' selected>Test item</option><option action='return 2'>Test item</option><option action='return 3'>Other item</option><option action='return 1'>Who am I</option></select>Menu 2<select name='testmenu2' size='4' style='width:100%'><option action='return 5'>Other item</option><option action='return 7'>Test item</option><option action='return 8' selected>Test item</option><option action='return 6'>Who am I</option></select></td></tr></table><button accesskey='1' action='call testmenu1.action()' style='visibility:hidden'></button><button accesskey='2' action='call testmenu1.up()' style='visibility:hidden'></button><button accesskey='3' action='call testmenu1.down()' style='visibility:hidden'></button><button accesskey='4' action='call testmenu2.action()' style='visibility:hidden'></button><button accesskey='5' action='call testmenu2.up()' style='visibility:hidden'></button><button accesskey='6' action='call testmenu2.down()' style='visibility:hidden'></button><input type='timeout' value='15' style='visibility:hidden' action='return 100'>" );
  myOutput.clear();
  switch( ret )
  {
    case 4:
      myOutput.append( myFirst1 );
      myOutput.append( myTestItem1 );
      myOutput.append( myIn );
      myOutput.append( myFirst2 );
      myOutput.append( myEnd );
      break;
    case 2:
      myOutput.append( mySecond1 );
      myOutput.append( myTestItem1 );
      myOutput.append( myIn );
      myOutput.append( myFirst2 );
      myOutput.append( myEnd );
      break;
    case 3:
      myOutput.append( myFirst1 );
      myOutput.append( myTestItem2 );
      myOutput.append( myIn );
      myOutput.append( myFirst2 );
      myOutput.append( myEnd );
      break;
    case 1:
      myOutput.append( myFirst1 );
      myOutput.append( myTestItem3 );
      myOutput.append( myIn );
      myOutput.append( myFirst2 );
      myOutput.append( myEnd );
      break;
    case 5:
      myOutput.append( mySecond1 );
      myOutput.append( myTestItem2 );
      myOutput.append( myIn );
      myOutput.append( mySecond2 );
      myOutput.append( myEnd );
      break;
    case 7:
      myOutput.append( myThird );
      myOutput.append( myTestItem1 );
      myOutput.append( myIn );
      myOutput.append( mySecond2 );
      myOutput.append( myEnd );
      break;
    case 8:
      myOutput.append( myFourth );
      myOutput.append( myTestItem1 );
      myOutput.append( myIn );
      myOutput.append( mySecond2 );
      myOutput.append( myEnd );
      break;
    case 6:
      myOutput.append( mySecond1 );
      myOutput.append( myTestItem3 );
      myOutput.append( myIn );
      myOutput.append( mySecond2 );
      myOutput.append( myEnd );
      break;
    default:
      myOutput.append( uiPrint( "Result: %d<input type='timeout' value='2' style='visibility:hidden' action='return -999'>", ret ) );
      break;
  }
  uiConfirm( "confirm-cancel", myOutput );
}


// =================================================================================================
int checkMenuItems( const int *menuItemValues, int menuItem )
{
  int        retVal = 0;
  const int *myMenu = menuItemValues;
  bool       bFound = false;


  if( NULL != menuItemValues )
  {
    retVal = *menuItemValues;
    while( ( VATS_GUI_TEST_ID_NONE != *myMenu ) && ( !bFound ) )
    {
      if( *myMenu == menuItem )
      {
        retVal = menuItem;
        bFound = true;
      }
      else
      {
        myMenu++;
      }
    }
  }
  else
  {
    retVal = menuItem;
  }
  return retVal;
}


// =================================================================================================
void menu_card_handling( void )
{
  int    ret                = cardMenuItems[ 0];
  int    iContinue          = 1;
  struct UIMenuEntry menu[] = {
    { "ICC card in   test", cardMenuItems[ 0], 0 },
    { "MSR read      test", cardMenuItems[ 1], 0 },
    { "EMV           test", cardMenuItems[ 2], 0 },
    { "back to main menu", VATS_GUI_MENU_BACK, 0 }
  };


  while( 1 == iContinue )
  {
    ret = checkMenuItems( cardMenuItems, ret );
    ret = uiMenu( "mainmenu", "<h4>Card Reader Menu</h4>", menu, (sizeof( menu ) / sizeof( menu[0] )), ret > VATS_GUI_TEST_ID_NONE ? ret : cardMenuItems[ 0] );

    if( ( UI_ERR_ABORT == ret ) || ( UI_ERR_BACK == ret ) || ( UI_ERR_TIMEOUT == ret ) )
    {
      // back to main menu
      iContinue = 0;
    }
    else if( cardMenuItems[ 0] == ret ) cardin_test();
    else if( cardMenuItems[ 1] == ret ) magstripe_test();
#ifdef _EMV
    else if( cardMenuItems[ 2] == ret ) EMV_Test_VATS();
#endif
  }
}


// =================================================================================================
void menu_print_handling( void )
{
  int    ret                = printMenuItems[ 0];
  int    iContinue          = 1;
  struct UIMenuEntry menu[] = {
    { "printer    test  1", printMenuItems[ 0], 0 },
    { "back to main menu", VATS_GUI_MENU_BACK, 0 }
  };


  while( 1 == iContinue )
  {
    ret = checkMenuItems( printMenuItems, ret );
    ret = uiMenu( "mainmenu", "<h4>Printer Menu</h4>", menu, (sizeof( menu ) / sizeof( menu[0] )), ret > VATS_GUI_TEST_ID_NONE ? ret : printMenuItems[ 0] );

    if( ( UI_ERR_ABORT == ret ) || ( UI_ERR_BACK == ret ) || ( UI_ERR_TIMEOUT == ret ) )
    {
      // back to main menu
      iContinue = 0;
    }
    else if( printMenuItems[ 0] == ret ) html_printer_test();
  }
}


// =================================================================================================
void menu_user_handling( void )
{
  int    ret                = userMenuItems[ 0];
  int    iContinue          = 1;
  struct UIMenuEntry menu[] = {
    { "keyboard   test  1", userMenuItems[ 0], 0 },
    { "keyboard   test  2", userMenuItems[ 1], 0 },
    { "PIN entry  test  1", userMenuItems[ 2], 0 },
    { "touch      test  1", userMenuItems[ 3], 0 },
    { "Multiple menus", userMenuItems[ 4], 0 },
    { "back to main menu", VATS_GUI_MENU_BACK, 0 }
  };


  if( 0 == EventViaDQDriver( ATSEVENT_USER_KEYBOARD ) )
  {
    menu[1].text.assign( "no DQ driver active" );
    menu[1].value   = VATS_GUI_TEST_ID_NONE;
    menu[1].options = 0;
    menu[2].text.assign( "no DQ driver active" );
    menu[2].value   = VATS_GUI_TEST_ID_NONE;
    menu[2].options = 0;
  }
  if( 1 != touchScreenAvailable() )
  {
    menu[3].text.assign( "no touch available" );
    menu[3].value   = VATS_GUI_TEST_ID_NONE;
    menu[3].options = 0;
  }
  while( 1 == iContinue )
  {
    ret = checkMenuItems( userMenuItems, ret );
    ret = uiMenu( "mainmenu", "<h4>User Input Menu</h4>", menu, (sizeof( menu ) / sizeof( menu[0] )), ret > VATS_GUI_TEST_ID_NONE ? ret : userMenuItems[ 0] );

    if( ( UI_ERR_ABORT == ret ) || ( UI_ERR_BACK == ret ) || ( UI_ERR_TIMEOUT == ret ) )
    {
      // back to main menu
      iContinue = 0;
    }
    else if(   userMenuItems[ 0] == ret )                                                          keyboard_test();
    else if( ( userMenuItems[ 1] == ret ) && ( 0 != EventViaDQDriver( ATSEVENT_USER_KEYBOARD ) ) ) vats_key_inject_test();
    else if( ( userMenuItems[ 2] == ret ) && ( 0 != EventViaDQDriver( ATSEVENT_USER_KEYBOARD ) ) ) pin_test();
    else if( ( userMenuItems[ 3] == ret ) && ( 1 == touchScreenAvailable() ) )                     vats_touch_test();
    else if(   userMenuItems[ 4] == ret )                                                          multi_menu_test();
  }
}


// =================================================================================================
void menu_disp_handling( void )
{
  int    ret                = dispMenuItems[ 0];
  int    iContinue          = 1;
  struct UIMenuEntry menu[] = {
    { "display    test  1", dispMenuItems[ 0], 0 },
    { "display    test  2", dispMenuItems[ 1], 0 },
    { "disp.regs. test  1", dispMenuItems[ 2], 0 },
    { "back to main menu", VATS_GUI_MENU_BACK, 0 }
  };


  while( 1 == iContinue )
  {
    ret = checkMenuItems( dispMenuItems, ret );
    ret = uiMenu( "mainmenu", "<h4>Display Menu</h4>", menu, (sizeof( menu ) / sizeof( menu[0] )), ret > VATS_GUI_TEST_ID_NONE ? ret : dispMenuItems[ 0] );

    if( ( UI_ERR_ABORT == ret ) || ( UI_ERR_BACK == ret ) || ( UI_ERR_TIMEOUT == ret ) )
    {
      // back to main menu
      iContinue = 0;
    }
    else if( dispMenuItems[ 0] == ret ) display_test();
    else if( dispMenuItems[ 1] == ret ) screen_test();
    else if( dispMenuItems[ 2] == ret ) disp_region_test();
  }
}


// =================================================================================================
void menu_misc_handling( void )
{
  int    ret                = miscMenuItems[ 0];
  int    iContinue          = 1;
  struct UIMenuEntry menu[] = {
    { "DUT to PC cust.ev.", miscMenuItems[ 0], 0 },
    { "PC to DUT cust.ev.", miscMenuItems[ 1], 0 },
    { "trace         test", miscMenuItems[ 2], 0 },
    { "date and time test", miscMenuItems[ 3], 0 },
    { "terminal unit test", miscMenuItems[ 4], 0 },
    { "back to main menu", VATS_GUI_MENU_BACK, 0 }
  };


  while( 1 == iContinue )
  {
    ret = checkMenuItems( miscMenuItems, ret );
    ret = uiMenu( "mainmenu", "<h4>Miscellaneous</h4>", menu, (sizeof( menu ) / sizeof( menu[0] )), ret > VATS_GUI_TEST_ID_NONE ? ret : miscMenuItems[ 0] );

    if( ( UI_ERR_ABORT == ret ) || ( UI_ERR_BACK == ret ) || ( UI_ERR_TIMEOUT == ret ) )
    {
      // back to main menu
      iContinue = 0;
    }
    else if( miscMenuItems[ 0] == ret ) custom_event_test();
    else if( miscMenuItems[ 1] == ret ) custom_event_test2();
    else if( miscMenuItems[ 2] == ret ) trace_event_test();
    else if( miscMenuItems[ 3] == ret ) datetime_test();
    else if( miscMenuItems[ 4] == ret ) menu_terminal_unit_test();
  }
}


// =================================================================================================
void menu_main_handling( void )
{
  int                ret    = mainMenuItems[ 0];
  int    iContinue          = 1;
  struct UIMenuEntry menu[] = {
    { "Miscellaneous", mainMenuItems[ 0], 0 },
    { "Display",       mainMenuItems[ 1], 0 },
    { "User input",    mainMenuItems[ 2], 0 },
    { "Printer",       mainMenuItems[ 3], 0 },
    { "Card reader",   mainMenuItems[ 4], 0 }
  };


  if( 1 != PrinterAvailable() )
  {
    menu[3].text.assign( "no printer on tap" );
    menu[3].value   = VATS_GUI_TEST_ID_NONE;
    menu[3].options = 0;
  }
  while( 1 == iContinue )
  {
    ret = checkMenuItems( mainMenuItems, ret );
    ret = uiMenu( "mainmenu", "<h4>VATS Main Menu</h4>", menu, (sizeof( menu ) / sizeof( menu[0] )), ret > VATS_GUI_TEST_ID_NONE ? ret : mainMenuItems[ 0] );

    vDebugOutVats( __MODULE__, __LINE__, "  test_app: menu_main_handling: uiMenu: ret=%d\n", ret );

    if( UI_ERR_CONNECTION_LOST == ret )
    {
      vDebugOutVats( __MODULE__, __LINE__, "  test_app: menu_main_handling: ERROR: UI_ERR_CONNECTION_LOST\n" );
      iContinue = 0;
    }
    else if(   mainMenuItems[ 0] == ret )                                   menu_misc_handling();
    else if(   mainMenuItems[ 1] == ret )                                   menu_disp_handling();
    else if(   mainMenuItems[ 2] == ret )                                   menu_user_handling();
    else if( ( mainMenuItems[ 3] == ret ) && ( 1 == PrinterAvailable() ) )  menu_print_handling();
    else if(   mainMenuItems[ 4] == ret )                                   menu_card_handling();
    else if(                 700 == ret )                                   SVC_RESTART( "" );
  }
}


// =================================================================================================
// ADKVATS-164: issue events for unit test
void UnitTestThread (int dummy)
{
  int  i;
  int  ret;
  char data [2];
  int  pos;
  char magdata [512];


  ret = AtsInit ("UnitTestThread", get_task_id(), 2);
  vDebugOutVats (__MODULE__, __LINE__, "  test_app: UnitTestThread: AtsInit: ret=%d\n", ret);

  //          VX520   UX300
  // CONFIRM: 13      13
  // CANCEL : 27      27
  // DOWN   : 125     99
  // UP:              98
  while (1)
  {
    if (doPINTest)
    {
      doPINTest = false;

      i = 1;
      data [0] = 13; // CONFIRM
      ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: UnitTestThread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
      SVC_WAIT (1000);
      i = 2;
      data [0] = '1'; // enter PIN digit
      ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: UnitTestThread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
      SVC_WAIT (1000);
      i = 3;
      data [0] = '2'; // enter PIN digit
      ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: UnitTestThread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
      SVC_WAIT (1000);
      i = 4;
      data [0] = '3'; // enter PIN digit
      ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: UnitTestThread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
      SVC_WAIT (1000);
      i = 5;
      data [0] = '4'; // enter PIN digit
      ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: UnitTestThread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
      SVC_WAIT (1000);
      i = 6;
      data [0] = 13; // CONFIRM
      ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: UnitTestThread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
      SVC_WAIT (1000);
      i = 7;
      data [0] = 27; // CANCEL
      ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: UnitTestThread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
    }

    if (doMagDataTest)
    {
      doMagDataTest = false;

      i = 1;
      data [0] = 13; // CONFIRM
      ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: MagDatathread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
      svcSleep (1000);

      i = 2;
      // here without sentinels and LRC:
      // Magdata stripe 1, len=42, dat=11003132333D4A4F484E3D534D4954487802010201
      // Magdata stripe 2, len=38, dat=6726672910070115390D07091010274904490F
      // Magdata stripe 3, len=95, dat=015966666666D10070115392D2809540000000007001003012749020000007090232483411DD1D0000449000005020F
      pos = 0;
      magdata [pos++] = 44;
      magdata [pos++] = 0;
      memcpy (&magdata [pos], "11003132333D4A4F484E3D534D4954487802010201", 42);
      pos += 42;
      magdata [pos++] = 40;
      magdata [pos++] = 0;
      memcpy (&magdata [pos], "6726672910070115390D07091010274904490F", 38);
      pos += 38;
      magdata [pos++] = 97;
      magdata [pos++] = 0;
      memcpy (&magdata [pos], "015966666666D10070115392D2809540000000007001003012749020000007090232483411DD1D0000449000005020F", 95);
      pos += 95;

      ret = AtsSendEvent (ATSEVENT_USER_MAGSTRIPE, pos, (const void*) magdata);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: unitTestThread: AtsSendEvent: ATSEVENT_USER_MAGSTRIPE: i=%d ret=%d pos=%d\n", i, ret, pos);
      svcSleep (1000);
      i = 3;
      data [0] = 27; // CANCEL
      ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: unitTestThread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
      svcSleep (2000);
    }

    if (doCardinTest)
    {
      doCardinTest = false;

      i = 1;
      data [0] = 13; // CONFIRM
      ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: unitTestThread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
      svcSleep (1000);
      i = 2;
      data [0] = 1;  // Card_in set
      ret = AtsSendEvent (ATSEVENT_USER_ICCCARD_IN, 1, (const void*) data);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: unitTestThread: AtsSendEvent: ATSEVENT_USER_ICCCARD_IN: i=%d ret=%d\n", i, ret);
      svcSleep (2000);
      i = 3;
      data [0] = 0;  // Card_in set
      ret = AtsSendEvent (ATSEVENT_USER_ICCCARD_IN, 1, (const void*) data);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: unitTestThread: AtsSendEvent: ATSEVENT_USER_ICCCARD_IN: i=%d ret=%d\n", i, ret);
      svcSleep (2000);
      i = 4;
      data [0] = 27; // CANCEL
      ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
      vDebugOutVats (__MODULE__, __LINE__, "  test_app: unitTestThread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
      svcSleep (1000);
    }

    svcSleep (1000);
  }
} // UnitTestThread


// =================================================================================================
// ADKVATS-164
void menu_terminal_unit_test ( void )
{
  int    ret                = tutMenuItems[ 0];
  int    iContinue          = 1;
  struct UIMenuEntry menu[] =
  {
    { "magdata unit test",   tutMenuItems[ 0], 0 },
    { "PIN input unit test", tutMenuItems[ 1], 0 },
    { "Card_in unit test",   tutMenuItems[ 2], 0 },
    { "back to main menu",   VATS_GUI_MENU_BACK, 0 }
  };


  while( 1 == iContinue )
  {
    ret = checkMenuItems( tutMenuItems, ret );
    ret = uiMenu( "mainmenu", "<h4>Terminal Unit Tests</h4>", menu, (sizeof( menu ) / sizeof( menu[0] )), ret > VATS_GUI_TEST_ID_NONE ? ret : tutMenuItems[ 0] );

    vDebugOutVats (__MODULE__, __LINE__, "test_app: menu_terminal_unit_test: ret=%d\n", ret); 
    if( ( UI_ERR_ABORT == ret ) || ( UI_ERR_BACK == ret ) || ( UI_ERR_TIMEOUT == ret ) )
    {
      iContinue = 0; // back to upper menu
    }
    else if( tutMenuItems[ 0] == ret )
    {
      doMagDataTest = true; // activate one pass in the unit test thread in order to provide data input
      magstripe_test ();
    }
    else if( tutMenuItems[ 1] == ret )
    {
      doPINTest = true; // activate one pass in the unit test thread in order to provide data input
      pin_test ();
    }
    else if( tutMenuItems[ 2] == ret )
    {
      doCardinTest = true; // activate one pass in the unit test thread in order to provide data input
      cardin_test ();
    }
  }
} // menu_terminal_unit_test


// =================================================================================================
#if 0 // ADKVATS-164: issue MTI events
void mythread (int dummy)
{
  int i;
  int pos;
  int ret;
  char data [2];  
  char magdata [512]; 

  // data [1] = 0x00;

  AtsInit ("mythread", get_task_id(), 2);
  SVC_WAIT (5000); // msec: wait for the first menu

  // CONFIRM: 13
  // CANCEL : 27
  // DOWN   : 125 

  for (i = 1; i <= 4; i++) // select "Card reader"
  {
    data [0] = 125; // DOWN: ko: 137,132
    ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
    vDebugOutVats (__MODULE__, __LINE__, "  test_app: mythread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
    SVC_WAIT (1000);
  }

  i = 5;
  data [0] = 13; // CONFIRM
  ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
  vDebugOutVats (__MODULE__, __LINE__, "  test_app: mythread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
  SVC_WAIT (1000);

  i = 6;
  data [0] = 125; // select MSR read
  ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
  vDebugOutVats (__MODULE__, __LINE__, "  test_app: mythread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
  SVC_WAIT (1000);

  i = 7;
  data [0] = 13; // CONFIRM
  ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
  vDebugOutVats (__MODULE__, __LINE__, "  test_app: mythread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
  SVC_WAIT (1000);

  i = 8;
  // here without sentinels and LRC:
  // Magdata stripe 1, len=42, dat=11003132333D4A4F484E3D534D4954487802010201
  // Magdata stripe 2, len=38, dat=6726672910070115390D07091010274904490F
  // Magdata stripe 3, len=95, dat=015966666666D10070115392D2809540000000007001003012749020000007090232483411DD1D0000449000005020F
  pos = 0;
  magdata [pos++] = 44;
  magdata [pos++] = 0;
  memcpy (&magdata [pos], "11003132333D4A4F484E3D534D4954487802010201", 42);
  pos += 42;
  magdata [pos++] = 40;
  magdata [pos++] = 0;
  memcpy (&magdata [pos], "6726672910070115390D07091010274904490F", 38);
  pos += 38;
  magdata [pos++] = 97;
  magdata [pos++] = 0;
  memcpy (&magdata [pos], "015966666666D10070115392D2809540000000007001003012749020000007090232483411DD1D0000449000005020F", 95);
  pos += 95;

  ret = AtsSendEvent (ATSEVENT_USER_MAGSTRIPE, pos, (const void*) magdata);
  vDebugOutVats (__MODULE__, __LINE__, "  test_app: mythread: AtsSendEvent: ATSEVENT_USER_MAGSTRIPE: i=%d ret=%d pos=%d\n", i, ret, pos);
  SVC_WAIT (1000);

  for (i = 9; i <= 10; i++) // cancel
  {
    data [0] = 27; // CANCEL
    ret = AtsSendEvent (ATSEVENT_USER_KEYBOARD, 1, (const void*) data);
    vDebugOutVats (__MODULE__, __LINE__, "  test_app: mythread: AtsSendEvent: ATSEVENT_USER_KEYBOARD: i=%d ret=%d data=%d\n", i, ret, (int) data[0]);
    SVC_WAIT (1000);
  }

  while (1) // just to continue the thread
  {
    vDebugOutVats (__MODULE__, __LINE__, "mythread: Loop\n");
    SVC_WAIT (10000); // msec: wait til new owner is set
  }
}
#endif


// =================================================================================================
void GetModelInfo( char *model )
{
  char  buffer[13];
  char *buffer_used = buffer;


  // determine model number of terminal
  if( 0 == SVC_INFO_MODELNO( buffer ) )
  {
    // terminate the fixed length string
    buffer[12] = '\0';
    while( ' ' == *buffer_used )  // skip preceding blanks
    {
      buffer_used++;
    }
    strcpy( model, buffer_used );
  }
  else
  {
    strcpy( model, "unknown" );
  }
}


// =================================================================================================
int main (void)
{
  int           propertyValue;
  unsigned long r = EMV_INIT_ERR_NONE;
  char          model[20];
  displayInfo_t myDispInfo;


  // set resource subfolder for this application (if not already done by MAC)
  setenv( "GUIPRT_APPNAME", "vats-demo", 1 );

  LOG_INIT (APPNAME, LOGSYS_OS, LOG_APPFILTER);
  vDebugOutVats( __MODULE__, __LINE__, "test_app: main: START: 18.02.2014\n" );

  // Initialize VATS lib (incl. VATS pipe)
  AtsInitExt( APPNAME, (int)( ATSREG_ICCCARD_IN | ATSREG_MAGSTRIPE | ATSREG_CUSTOM ), get_task_id(), 5 );

  // GUI init
  uiReadConfig();
  propertyValue = 60000;
  (void)uiSetPropertyInt( UI_PROP_TIMEOUT, propertyValue );
  propertyValue = 0;
  (void)uiGetPropertyInt( UI_PROP_TIMEOUT, &propertyValue );
  vDebugOutVats( __MODULE__, __LINE__, "  test_app: UI_PROP_TIMEOUT=%d\n", propertyValue );

  // display size
  SVC_INFO_DISPLAY_EXT( &myDispInfo );
  if( ( myDispInfo.width <= 128 ) && ( myDispInfo.height < 64 ) )
  {
    bSmallDisplay = true;
  }
  // keypad type
  GetModelInfo( model );
  if( ( 0 == strcmp( tType_e315, model ) ) || ( 0 == strcmp( tType_e335, model ) ) )
  {
    unsigned long ulCount = 100;


    VatsLogging( VATS_LOG_INFO, __MODULE__, __LINE__, "VATS Handler: wake up the %s keypad", model );
    // wake up the keypad
    cs_set_sleep_state( 0 );
    // wait for activated keypad
    while( ( 1 < ulCount ) && ( 0 == cs_spi_cmd_status() ) )
    {
      --ulCount;
      svcSleep( 10 );     // msec
    }
    VatsLogging( VATS_LOG_INFO, __MODULE__, __LINE__, "VATS Handler: %s keypad status %d.", model, cs_spi_cmd_status() );
  }

  // ADKVATS-164: start threads once only
  {
    int pid;
    pid = run_thread ((int) UnitTestThread, 0, 20000);  // stack size: 20000 bytes
    vDebugOutVats (__MODULE__, __LINE__, " test_app: run_thread UnitTestThread: ret=%d\n", pid);
  }

#ifdef _EMV
  // EMV init
  uiDisplay( "<h4>VATS</h4>Initializing EMV..." );
  r = EMV_InitEMV_VATS (0);
  if( EMV_INIT_ERR_NONE != r )
  {
    vDebugOutVats( __MODULE__, __LINE__, "test_app: main: EMV_InitEMV: %d.\n", r );
  }
  EMV_CTLS_ApplyConf_VATS ();
#endif

  // some debug information before starting the loop
  vDebugOutVats( __MODULE__, __LINE__, "test_app: main: VATS is active: %d.\n", AtsIsActive() );
  vDebugOutVats( __MODULE__, __LINE__, "test_app: main: AtsDisplayViaDQDriver: %d.\n", EventViaDQDriver( ATSEVENT_TERMINAL_DISPLAY ) );

  // infinite loop handling the menus and test cases
  menu_main_handling();
  vDebugOutVats( __MODULE__, __LINE__, "test_app: main: Loop end\n" );
}


#endif  // #ifndef _VOS

