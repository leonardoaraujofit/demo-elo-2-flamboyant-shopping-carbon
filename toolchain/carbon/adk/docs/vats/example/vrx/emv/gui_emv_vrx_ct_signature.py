#----------------------------------------------------------------------------
#
#    File : gui_emv_vrx_ct_signature.py
#
#    VATS demo : EMV testing
#
#    Author : Reinhard Lieberum
#
#    Creation date : 2014-May
#                    2014-June: - add responses of 39 14 (fetch)
#                               - does not depend on insert/remove_smartcard any longer
#                    2014-Oct:  - apply vats recording
#
#    Description:
'''Test module: GUI EMV test VRX CT signature'''

#    CLA_EMV, INS_TERM_CFG :  39 01: don't care, are really executed by EMV
#    CLA_EMV, INS_APPLI_CFG:  39 02: don't care, are really executed by EMV
#    card: MasterCard Test Card, 20.00, signature
#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *
from vats_html import *


#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def gui_emv_test1():
    '''GUI EMV test VRX CT signature'''

    # set keyboard mapping
    terminalIndex = user.setTerminalSpecificValues()
    holdtime_ms     =   50  # for minimum this holdtime the display must be stable
    responsetime_ms = 3000  # latest after this response time the desired display must appear

    # Remove card to have a defined starting point
    user.remove_smartcard()

    # tear off last receipt in the case there is any
    user.tearoff_printout()

    # Wait until headline indicates "Welcome"
    ats.check( vats_json.html_waitDisplayContains( "Welcome", 0, default_menu_wait ), 'Welcome failed!!!' )

    vats_json.html_gui_setInput( 'number', '2000', 'in0' )
    myButton = vats_json.html_gui_pressButton( '', vats_json.VATS_KEYB_ENTER )

    #---------------------------------------------------------------------------------------------------
    # Wait until GUI display prompts for card insertion or tapping
    if ats.check( vats_json.html_waitDisplayContains( 'Insert or', 0, 10000 ), 'Insert card failed!!!' ):

        ats.log_info( 'EMV CT simulation messages transfer started.' )
        # VATS cleanup
        # recording from 2016_07_06
        user.send_emv_ct_data ("FFFFFF")
        user.send_emv_ct_data ("90010000")
        user.send_emv_ct_data ("90010000")
        user.send_emv_ct_data ("90010000")
        user.send_emv_ct_data ("90000000")
        user.send_emv_ct_data ("90000000F014DF68113BE800008131FE454A434F505632343194")
        user.send_emv_ct_data ("91010000F057BF0454DF740101500C436F6D626F30312076312030500C436F6D626F30312076312030DF0407A0000000041010DF0407A0000000043060DF600101DF6001025F2D090000000000000000005F2D09000000000000000000")
        user.send_emv_ct_data ("90000000F00CF1049F060107F1049F060107")
        user.send_emv_ct_data ("90000000F06A8407A0000000041010500C436F6D626F30312076312030DF330C436F6D626F30312076312030DF4E0100FF0318DF0407A0000000041010500C436F6D626F30312076312030FF0318DF0407A0000000043060500C436F6D626F30312076312030DF0407A0000000041010")
        user.send_emv_ct_data ("90000000F04B5F5600DF620F30009000AC20200000000000000001FF0318DF0407A0000000041010500C436F6D626F30312076312030FF0318DF0407A0000000043060500C436F6D626F30312076312030")
        user.send_emv_ct_data ("91010000F0818FBF06818B9F420209785A0854133300896040129F02060000000000009F1B04000007D05F280200565F2403251231DF5B0100DF5C03000000DF21050400000000DF2205F850ACF800DF2305FC50ACA80057115413330089604012D25122010123409172500C436F6D626F303120763120308E1C000000000000000042035E031F030000000000000000000000000000")
        user.send_emv_ct_data ("91010000F05ABF0757DF7D01009F420209785A0854133300896040129F02060000000000005F2A0208405F3601029F1B04000007D0DF2404000001F48E1C000000000000000042035E031F030000000000000000000000000000DF3903000000")
        user.send_emv_ct_data ("91010000F022BF081FDF790100DF4101019F02060000000000009F03060000000000005F2A020840")
        user.send_emv_ct_data ("90000000F0099F0206000000000000")
        user.send_emv_ct_data ("90000000F0039F1200")
        user.send_emv_ct_data ("90000000F0098407A0000000041010")
        user.send_emv_ct_data ("90000000")
        user.send_emv_ct_data ("90000000")
        user.send_emv_ct_data ("90000000")
        # indicates end of EMV input via VATS
        user.send_emv_ct_data ("FFFF")
        ats.log_info( 'EMV CT simulation messages transfer ended.' )

        #ats.check( vats_json.html_waitDisplayContains( 'Please wait', 0, 30000), 'Please wait failed!!!' )

        ats.check( vats_json.html_waitDisplayContains( 'Select Application', 0, 120000), 'Select Application failed!!!' )
        myMenu = vats_json.html_gui_selectMenu( 'Combo01 v1 0' )
        ats.check( vats_json.html_waitDisplayContains( 'Combo01 v1 0', 0, 30000), 'Combo01 v1 0 failed!!!' )

        ats.check( vats_json.html_waitDisplayContains( 'Please Enter PIN', 0, 120000), 'Please Enter PIN failed!!!' )
        ats.check( user.enter_keys( "4" ), 'Input failed!!!' )
        ats.check( user.enter_keys( "3" ), 'Input failed!!!' )
        ats.check( user.enter_keys( "1" ), 'Input failed!!!' )
        ats.check( user.enter_keys( "5" ), 'Input failed!!!' )
        ats.check( user.enter_keys( "<CONFIRM>" ), 'Input failed!!!' )
        #ats.check( vats_json.html_waitDisplayContains( 'Please wait', 0, 30000), 'Please wait failed 2!!!' )
        ats.check( vats_json.html_waitDisplayContains( 'Please Wait, authorizing', 0, 20000), 'Please wait failed!!!' )

        ats.check( vats_json.html_waitDisplayContains( 'Transaction approved', 0, 60000), 'Transaction approved failed!!!' )
        ats.check( vats_json.html_waitDisplayContains( 'Printout', 0, 10000), 'Printout failed!!!' )
        myResult = vats_json.html_gui_pressButton( 'CAN1' )
        vats_json.html_waitDisplayContains( "Welcome", 0, 10000 )
        # VATS cleanup
        user.send_emv_ct_data ("FFFFFF")
 

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "2.3" )
ats.set_script_summary( "GUI EMV test VRX CT signature" )
ats.set_recovery_function( gui_back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( gui_emv_test1, "GUI EMV test VRX CT signature" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
