#----------------------------------------------------------------------------
#
#    File : gui_emv_vrx_ctls_online.py
#
#    VATS demo : EMV CTLS testing 
#
#    Author : Reinhard Lieberum
#
#    Creation date : 2014-June
#                    2014-Oct:  - apply vats recording
#
#    Description:
'''Test module: GUI EMV test VRX CTLS online'''

#    With VFI reader !!!
#    CLA_EMV, INS_TERM_CFG :  39 10: don't care, are really executed by EMV
#    CLA_EMV, INS_APPLI_CFG:  39 11: don't care, are really executed by EMV
#    card: MasterCard Test Card, 100.00, no PIN, online, with host resp
#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *
from vats_html import *


#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def gui_emv_test_ctls1():
    '''GUI EMV test VRX CTLS online'''

    # set keyboard mapping
    terminalIndex   = user.setTerminalSpecificValues()
    holdtime_ms     =   50  # for minimum this holdtime the display must be stable
    responsetime_ms = 3000  # latest after this response time the desired display must appear

    # Remove card to have a defined starting point
    user.remove_smartcard()

    # tear off last receipt in the case there is any
    user.tearoff_printout()

    # Wait until headline indicates "SALE Enter amount:"
    ats.check( vats_json.html_waitDisplayContains( "Welcome1\nEnter amount", 0, default_menu_wait ), 'start failed!!!' )

    # take a higher amount in order to get online
    vats_json.html_gui_setInput( 'number', '8567', 'in0' )


    #---------------------------------------------------------------------------------------------------
    # Wait until GUI display prompts for card insertion
    # HTML: a new line is mapped to ' \n'
    # ats.check( vats_json.html_waitDisplayContains( 'Insert or Tap \nCard', 0, 10000 ), 'Insert card failed!!!' ):
    if ats.check ( vats_json.html_gui_pressButton( '', vats_json.VATS_KEYB_ENTER ), 'amount input failed!!!'):

        ats.log_info( 'EMV CTLS simulation messages transfer started.' )
        # VATS cleanup
        user.send_emv_ctls_data ("FFFFFF")
        user.send_emv_ctls_data ("90000000F000")
        user.send_emv_ctls_data ("90DF0000F007DF420400008000")
        user.send_emv_ctls_data ("90DF0000F007DF420400008000")
        user.send_emv_ctls_data ("90E20000F0820168DF4204000088029F2701809F360202609F2608A56D9434CF21A9CC5A0A5413330089601018FFFF9F3901075F24031412319F4104000000005F34010057115413330089601018D141222001234091729F10120110A04009248000000000000000000001FF9F37045DC751A0950500000080009A031408019F21031200009C01005F2A020036820259809F1A0200369F34035E03009F3303E060089F3501229F1E082D3234352D3839338407A00000000410109F090200029B02E8009F0607A00000000410109F5301529F02060000000100009F03060000000000009F120F505043204D41502030312020763230DFD0020F505043204D415020303120207632305F25030401019F4C08F224240DB6117B44DF21050000000000DF2205FC509C8800DF2305FC509C88009F0E0500000000009F0F0500000000009F0D0500000000009F40056000B0A0019F160F202020202020202020202020202020C204000000039F660436204000")
        user.send_emv_ctls_data ("90000000F00CDF81290830F0F01038F0FF00")
        user.send_emv_ctls_data ("90E30000F0820168DF4204000008029F2701809F360202609F2608A56D9434CF21A9CC5A0A5413330089601018FFFF9F3901075F24031412319F4104000000005F34010057115413330089601018D141222001234091729F10120110A04009248000000000000000000001FF9F37045DC751A0950500000080009A031408019F21031200009C01005F2A020036820259809F1A0200369F34035E03009F3303E060089F3501229F1E082D3234352D3839338407A00000000410109F090200029B02E8009F0607A00000000410109F5301529F02060000000100009F03060000000000009F120F505043204D41502030312020763230DFD0020F505043204D415020303120207632305F25030401019F4C08F224240DB6117B44DF21050000000000DF2205FC509C8800DF2305FC509C88009F0E0500000000009F0F0500000000009F0D0500000000009F40056000B0A0019F160F202020202020202020202020202020C204000000039F660436204000")
        user.send_emv_ctls_data ("90000000F0098407A0000000041010")
        # indicates end of EMV input via VATS
        user.send_emv_ctls_data ("FFFF")
        ats.log_info( 'EMV CTLS simulation messages transfer ended.' )

        # Insert or Tap<br>Card
        # ats.check( vats_json.html_waitDisplayContains( 'Insert or Tap', 0, 10000 ), 'Insert or Tap failed!!!' )

        ats.check( vats_json.html_waitDisplayContains( 'Host Connection', 0, 10000), 'Host Connection failed!!!' )
        # enter host result
        myMenu = vats_json.html_gui_selectMenu( 'AC=00 (OK)' )

        ats.check( vats_json.html_waitDisplayContains( "Online Response\nreceived", 0, 10000 ), 'Online Response received missing!!!' )
        ats.check( vats_json.html_waitDisplayContains( "Signature needed", 0, 10000 ), 'Signature needed missing!!!' )
        
        # HTML: a new line is mapped to ' \n'
        ats.check( vats_json.html_waitDisplayContains( "Transaction\napproved", 0, 10000 ), 'Transaction approved missing!!!' )
        myResult = vats_json.html_gui_pressButton( 'CAN1' )
        # myResult = vats_json.html_gui_pressButton( '', vats_json.VATS_KEYB_CANCEL )

        ats.check( vats_json.html_waitDisplayContains( "Welcome", 0, 20000 ), 'return to start failed!!!' )
        # VATS cleanup
        user.send_emv_ctls_data ("FFFFFF")


#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "2.2" )
ats.set_script_summary( "GUI EMV test VRX CTLS online" )
ats.set_recovery_function( gui_back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( gui_emv_test_ctls1, "GUI EMV test VRX CTLS online" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
