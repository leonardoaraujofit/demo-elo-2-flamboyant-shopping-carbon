#----------------------------------------------------------------------------
#
#    File : gui_emv_vrx_ctls_offline.py
#
#    VATS demo : EMV CTLS testing 
#
#    Author : Reinhard Lieberum
#
#    Creation date : 2014-June
#                    2014-Oct:  - apply vats recording
#
#    Description:
'''Test module: GUI EMV test CTLS offline'''

#    With VFI reader !!!
#    CLA_EMV, INS_TERM_CFG :  39 10: don't care, are really executed by EMV
#    CLA_EMV, INS_APPLI_CFG:  39 11: don't care, are really executed by EMV
#    card: MasterCard Test Card, no pin, 20.00
#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *
from vats_html import *


#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def gui_emv_test_ctls1():
    '''GUI EMV test VRX CTLS offline'''

    # set keyboard mapping
    terminalIndex   = user.setTerminalSpecificValues()
    holdtime_ms     =   50  # for minimum this holdtime the display must be stable
    responsetime_ms = 3000  # latest after this response time the desired display must appear

    # Remove card to have a defined starting point
    user.remove_smartcard()

    # tear off last receipt in the case there is any
    user.tearoff_printout()

    # Wait until headline indicates "SALE Enter amount:"
    ats.check( vats_json.html_waitDisplayContains( "Welcome1\nEnter amount", 0, default_menu_wait ), 'start failed!!!' )
    vats_json.html_gui_setInput( 'number', '2000', 'in0' )

    #---------------------------------------------------------------------------------------------------
    # Wait until GUI display prompts for card insertion
    if ats.check ( vats_json.html_gui_pressButton( '', vats_json.VATS_KEYB_ENTER ), 'amount input failed!!!'):

        ats.log_info( 'EMV CTLS simulation messages transfer started.' )
        # VATS cleanup
        user.send_emv_ctls_data ("FFFFFF")
        user.send_emv_ctls_data ("90000000F000")
        user.send_emv_ctls_data ("90DF0000F007DF420400008000")
        user.send_emv_ctls_data ("90DF0000F007DF420400008000")
        user.send_emv_ctls_data ("90DF0000F007DF420400008000")
        user.send_emv_ctls_data ("90DF0000F007DF420400008000")
        user.send_emv_ctls_data ("90E30000F0820168DF4204000088009F2701409F360202659F2608744003EF7D0ABCD35A0A5413330089601018FFFF9F3901075F24031412319F4104000000005F34010057115413330089601018D141222001234091729F10120110904009248000000000000000000004FF9F37046B701976950500000000009A031408019F21031200009C01005F2A020036820259809F1A0200369F34031F03029F3303E008089F3501229F1E082D3234352D3839338407A00000000410109F090200029B02E8009F0607A00000000410109F5301529F02060000000020009F03060000000000009F120F505043204D41502030312020763230DFD0020F505043204D415020303120207632305F25030401019F4C089A622D9185FC4CD1DF21050000000000DF2205FC509C8800DF2305FC509C88009F0E0500000000009F0F0500000000009F0D0500000000009F40056000B0A0019F160F202020202020202020202020202020C204000000039F660436204000")
        user.send_emv_ctls_data ("90000000F00CDF81290810F0F00030F0FF00")
        user.send_emv_ctls_data ("90000000F0098407A0000000041010")
        # indicates end of EMV input via VATS
        user.send_emv_ctls_data ("FFFF")
        ats.log_info( 'EMV CTLS simulation messages transfer ended.' )

        ats.check( vats_json.html_waitDisplayContains( "Transaction\napproved", 0, 10000 ), 'Transaction approved missing!!!' )
        myResult = vats_json.html_gui_pressButton( 'CAN1' )
        # myResult = vats_json.html_gui_pressButton( '', vats_json.VATS_KEYB_CANCEL )

        ats.check( vats_json.html_waitDisplayContains( "Welcome", 0, 20000 ), 'return to start failed!!!' )
        # VATS cleanup
        user.send_emv_ctls_data ("FFFFFF")


#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "2.2" )
ats.set_script_summary( "GUI EMV test VRX CTLS offline" )
ats.set_recovery_function( gui_back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( gui_emv_test_ctls1, "GUI EMV test VRX CTLS offline" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
