#----------------------------------------------------------------------------
#
#    File : gui_emv_vrx_ct_nohostresp.py
#
#    VATS demo : EMV testing
#
#    Author : Reinhard Lieberum
#
#    Creation date : 2014-May
#                    2014-June: - add responses of 39 14 (fetch)
#                               - does not depend on insert/remove_smartcard any longer
#                    2014-Oct:  - apply vats recording
#
#    Description:
'''Test module: GUI EMV test VRX CT no host resp'''

#    CLA_EMV, INS_TERM_CFG :  39 01: don't care, are really executed by EMV
#    CLA_EMV, INS_APPLI_CFG:  39 02: don't care, are really executed by EMV
#    card: bank-verlag koeln, with pin=9536, 20.00, girocard0, no host resp.
#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *
from vats_html import *


#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def gui_emv_test():
    '''GUI EMV test CT VRX CT no host resp'''

    # set keyboard mapping
    terminalIndex = user.setTerminalSpecificValues()
    holdtime_ms     =   50  # for minimum this holdtime the display must be stable
    responsetime_ms = 3000  # latest after this response time the desired display must appear

    # Remove card to have a defined starting point
    user.remove_smartcard()

    # tear off last receipt in the case there is any
    user.tearoff_printout()

    # Wait until headline indicates "Welcome"
    ats.check( vats_json.html_waitDisplayContains( "Welcome", 0, default_menu_wait ), 'Welcome failed!!!' )

    vats_json.html_gui_setInput( 'number', '2000', 'in0' )
    myButton = vats_json.html_gui_pressButton( '', vats_json.VATS_KEYB_ENTER )

    #---------------------------------------------------------------------------------------------------
    # Wait until GUI display prompts for card insertion or tapping
    if ats.check( vats_json.html_waitDisplayContains( 'Insert or Tap', 0, 10000 ), 'Insert card failed!!!' ):

        ats.log_info( 'EMV CT simulation messages transfer started.' )
        # VATS cleanup
        user.send_emv_ct_data ("FFFFFF")
        user.send_emv_ct_data ("90010000")
        user.send_emv_ct_data ("90010000")
        user.send_emv_ct_data ("90010000")
        user.send_emv_ct_data ("90010000")
        user.send_emv_ct_data ("90000000")
        user.send_emv_ct_data ("90000000F01CDF68193BFF9600FF8131FE4565630D09710764000D000753050701D7")
        user.send_emv_ct_data ("91010000F048BF0445DF74010150086769726F6361726450086769726F6361726450074D61657374726FDF040AA0000003591010028001DF040AA0000003591010028001DF0407A0000000043060")
        user.send_emv_ct_data ("91010000F007BF0104DF700107")
        user.send_emv_ct_data ("90000000F08183840AA000000359101002800150086769726F63617264DF33086769726F636172645F2D046465656EDF4E0100FF0317DF040AA000000359101002800150086769726F63617264FF0317DF040AA000000359101002800150086769726F63617264FF0313DF0407A000000004306050074D61657374726FDF040AA0000003591010028001")
        user.send_emv_ct_data ("91010000F007BF0104DF700182")
        user.send_emv_ct_data ("91010000F007BF0104DF700103")
        user.send_emv_ct_data ("91010000F019BF03165A0A6722395020090821087F5F3401005F2403131231")
        user.send_emv_ct_data ("91010000F08193BF06818F5A0A6722395020090821087F9F02060000000020009F1B04000013885F280202805F2403131231DF5B0100DF5C03000000DF21050400000000DF2205F850ACF800DF2305FC50ACA000DF770600000000000057136722395020090821087D13122010338304420F50086769726F636172648E1C00000000000000004403020300000000000000000000000000000000")
        user.send_emv_ct_data ("91010000F057BF0754DF7D01005A0A6722395020090821087F9F02060000000020005F2A0200365F3601029F1B0400001388DF2404000001F48E1C00000000000000004403020300000000000000000000000000000000DF3903000000")
        user.send_emv_ct_data ("91010000F007BF0104DF700104")
        user.send_emv_ct_data ("91010000F007BF0104DF700102")
        user.send_emv_ct_data ("91010000F007BF0104DF70010A")
        user.send_emv_ct_data ("91010000F01DBF081ADF790100DF4101019F02060000000020009F0306000000000000")
        user.send_emv_ct_data ("90000000F0099F0206000000002000")
        user.send_emv_ct_data ("90000000F00C840AA0000003591010028001")
        user.send_emv_ct_data ("91010000F007BF0104DF700105")
        user.send_emv_ct_data ("90E20000F0820199DF4204000000019F2701809F3602022B9F26086CA2510529DD6FA85A0A6722395020090821087F9F3901055F24031312319F4104000000015F34010057136722395020090821087D13122010338304420F9F10200FA507A231C0060000000000000000000F1206010100000000000000000000009F3704BAB2146A950508400400009A031401019F21031200009C01005F2A020036820278009F1A0200369F34030203009F3303E0D8C89F3501229F1E083132333435363738840AA00000035910100280019F090200029B02E8009F060AA0000003591010028001DF29099F3303E0D8C88F01FF9F5301529F02060000000020009F03060000000000009F12086769726F63617264DFD002086769726F636172645F28020280DF21050400000000DF2205F850ACF800DF2305FC50ACA0009F0E0500101800009F0F05FC40A498009F0D05FC40A480009F4005F000F0A0019F160F202020202020202020202020202020DF6301005F2D046465656E9F080200025F360102DF5901008E1C00000000000000004403020300000000000000000000000000000000")
        user.send_emv_ct_data ("91010000F007BF0104DF700106")
        user.send_emv_ct_data ("90E40000F08201ABDF4204000000019F2701009F3602022B9F2608E9280D7A1603E8645A0A6722395020090821087F9F3901055F24031312319F4104000000015F34010057136722395020090821087D13122010338304420F9F10200FA507223100070000000000000000000F1206010100000000000000000000009F3704F826F9C3950508400400009A031401019F21031200009C01005F2A020036820278009F1A0200369F34030203009F3303E0D8C89F3501229F1E083132333435363738840AA00000035910100280019F090200029B02E8009F060AA0000003591010028001DF29099F3303E0D8C88F01FF9F5301529F02060000000020009F03060000000000009F12086769726F63617264DFD002086769726F636172645F28020280DF21050400000000DF2205F850ACF800DF2305FC50ACA0009F0E0500101800009F0F05FC40A498009F0D05FC40A480009F4005F000F0A001DF620F90029000035A330840040000E800019F160F202020202020202020202020202020DF6301045F2D046465656E9F080200025F360102DF5901008E1C00000000000000004403020300000000000000000000000000000000")
        user.send_emv_ct_data ("90000000F00C840AA0000003591010028001")
        user.send_emv_ct_data ("90000000F01F9F5301528E0C0000000000000000440302039F3303E0D8C89F3501229F6600")
        user.send_emv_ct_data ("90030000")
        user.send_emv_ct_data ("90000000")
        user.send_emv_ct_data ("90000000")
        # indicates end of EMV input via VATS
        user.send_emv_ct_data ("FFFF")
        ats.log_info( 'EMV CT simulation messages transfer ended.' )

        ats.check( vats_json.html_waitDisplayContains( 'Please wait', 0, 20000,10), 'Please wait failed!!!' )
        ats.check( vats_json.html_waitDisplayContains( 'Select Application', 0, 60000), 'Select Application failed!!!' )
        # Apply "F4" key press to select 2. item
        # ats.check( user.enter_keys( '<F4><CONFIRM>' ), 'Input failed!!!' )
        myMenu = vats_json.html_gui_selectMenu( 'girocard0' )
        ats.check( vats_json.html_waitDisplayContains( 'girocard0', 0, 20000), 'girocard0 failed!!!' )

        ats.check( vats_json.html_waitDisplayContains( 'Please Enter PIN', 0, 60000), 'Please Enter PIN failed!!!' )
        # vats_json.html_gui_setInput( 'pin', '9536', 'pin' )
        # myResult = vats_json.html_gui_pressButton( '', vats_json.VATS_KEYB_ENTER )
        ats.check( user.enter_keys( "9" ), 'Input failed!!!' )
        ats.check( user.enter_keys( "5" ), 'Input failed!!!' )
        ats.check( user.enter_keys( "3" ), 'Input failed!!!' )
        ats.check( user.enter_keys( "6" ), 'Input failed!!!' )
        ats.check( user.enter_keys( "<CONFIRM>" ), 'Input failed!!!' )

        ats.check( vats_json.html_waitDisplayContains( 'Please wait', 0, 10000), 'Please wait 2 failed!!!' )
        ats.check( vats_json.html_waitDisplayContains( 'Host Connection', 0, 30000), 'Host Connection failed!!!' )
        # enter host result
        myMenu = vats_json.html_gui_selectMenu( 'No Host Resp' )
        ats.check( vats_json.html_waitDisplayContains( 'No Online\nresponse\nreceived', 0, 20000), 'No Online response received failed!!!' )

        ats.check( vats_json.html_waitDisplayContains( 'Transaction declined', 0, 20000), 'Transaction declined failed!!!' )
        myResult = vats_json.html_gui_pressButton( 'CAN1' )
        # myResult = vats_json.html_gui_pressButton( '', vats_json.VATS_KEYB_CANCEL )

        # Wait until headline indicates "Welcome"
        vats_json.html_waitDisplayContains( "Welcome", 0, 10000 )
        # VATS cleanup
        user.send_emv_ct_data ("FFFFFF")

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "2.2" )
ats.set_script_summary( "GUI EMV test VRX CT no host resp" )
ats.set_recovery_function( gui_back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( gui_emv_test, "GUI EMV test VRX CT no host resp" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
