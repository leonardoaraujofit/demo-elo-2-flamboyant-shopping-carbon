#----------------------------------------------------------------------------
#
#    File : gui_custom_event.py
#
#    VATS demo : Custom event testing
#
#    Author : Achim Groennert
#
#    Creation date : 2013-May
#
#    Description:
'''Test module: GUI custom event test'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *
from vats_html import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def gui_custom_event_1():
    '''Custom event DUT to PC test 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = gui_prepare_test()

    # Choose menu item 1: "Miscellaneous"
    myMenu = vats_json.html_gui_selectMenu( 'Miscellaneous' )
    # Wait until headline indicates "Miscellaneous Menu"
    vats_json.html_waitDisplayContains( back_to_main_menu_item, AppName = 'vats-demo' )
    if( ats.check( vats_json.html_doesDisplayContain( misc_menu_title_gui, AppName = 'vats-demo' ), 'Select "Miscellaneous" menu failed!!!' ) ):

        #----------------------------------------------------------------------------
        # Clear Custom Event list and check it
        user.clear_custom_event()
        cust_eve = user.get_custom_event( 1000 )
        ats.check( len( cust_eve ) == 0, 'Clear Custom Event list failed !!!' )

        # Choose menu item "DUT to PC cust.ev." from menu to generate a result automatically
        myMenu = vats_json.html_gui_selectMenu( 'DUT to PC cust.ev.' )
        # Wait until headline indicates test start
        if( ats.check( vats_json.html_waitDisplayContains( 'DUT to PC Test', AppName = 'vats-demo' ), 'Test start failed!!!' ) ):

            #----------------------------------------------------------------------------
            # Wait display indicates test end
            if( ats.check( vats_json.html_waitDisplayContains( 'has been sent', AppName = 'vats-demo' ), 'Test end failed!!!' ) ):
                # Check contents of receipt
                cust_eve = user.get_custom_event( 1000 )
                if( ats.check( len( cust_eve ) != 0, 'Get Custom Event failed !!!' ) ):
                    eve_id = cust_eve[0]*256 + cust_eve[1]
                    ats.check( eve_id == 1001, 'Wrong Custom Event ID !!!' )

    # back to main menu
    gui_back_to_main_menu()

def gui_custom_event_2():
    '''Custom event PC to DUT test 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = gui_prepare_test()

    # Choose menu item 1: "Miscellaneous"
    myMenu = vats_json.html_gui_selectMenu( 'Miscellaneous' )
    # Wait until headline indicates "Miscellaneous Menu"
    vats_json.html_waitDisplayContains( back_to_main_menu_item, AppName = 'vats-demo' )
    if( ats.check( vats_json.html_doesDisplayContain( misc_menu_title_gui, AppName = 'vats-demo' ), 'Select "Miscellaneous" menu failed!!!' ) ):

        # Choose menu item "PC to DUT cust.ev." from menu
        myMenu = vats_json.html_gui_selectMenu( 'PC to DUT cust.ev.' )
        # Wait until GUI display indicates start of test
        if( ( 13 == myIndex ) or ( 14 == myIndex ) or ( 15 == myIndex ) ):
            # e315, e315M and e335 have small displays
            result = ats.check( vats_json.html_waitDisplayContains( 'Awaiting event', AppName = 'vats-demo' ), 'Test start failed !!!' )
        else:
            result = ats.check( vats_json.html_waitDisplayContains( 'Waiting for custom', AppName = 'vats-demo' ), 'Test start failed !!!' )
        if( result ):

            myAppName = 'TESTAPP'

            #----------------------------------------------------------------------------
            # binary data has to be converted to string
            # in C syntax it would be 'char myData[] = "Hello Terminal!\x00\x01\x02\x04\x03";'
            myData = 'Hello Terminal! ' + chr(0) + chr(1) + chr(2) + chr(4) + chr(3)
            cust_eve = user.set_custom_event( myAppName, 2002, len( myData ), myData )

            # wait for test result screen
            ats.check( vats_json.html_waitDisplayContains( 'Custom event: 2002', AppName = 'vats-demo' ), 'No custom event !!!' )

            # check display content
            ats.check( vats_json.html_doesDisplayContain( 'Hello Terminal!', AppName = 'vats-demo' ), 'Wrong data !!!' )
            ats.check( vats_json.html_doesDisplayContain( r'\\01\\02\\04\\03', AppName = 'vats-demo' ), 'Wrong data !!!' )
            ats.check( vats_json.html_doesDisplayContain( 'Data length: ' + str( len( myData ) ), AppName = 'vats-demo' ), "Wrong data !!!" )

            # do it again with other data
            user.wait( 2000 )

            #----------------------------------------------------------------------------
            # binary data has to be converted to string
            myData = '12cond try:\nblabla1\nwokgnwokgnowkgnognqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaiqweifnpwagunpwiounga49nwgriogt9048rg9pwuigrnp98r4paw3n8rg9ihnaw489pa<h9p4h9p8thwokgnwokgnowkgnognqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaiqweifnpwagunpwiounga49nwgriogt9048rg9pwuigrnp98r4paw3n8rg9ihnaw489pa<h9p4h9p8thwokgnwokgnowkgnognqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaiqweifnpwagunpwiounga49nwgriogt9048rg9pwuigrnp98r4paw3n8rg9ihnaw489pa<h9p4h9p8thwokgnwokgnowkgnognqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaiqweifnpwagunpwiounga49nwgriogt9048rg9pwuigrnp98r4paw3n8rg9ihnaw489pa<h9p4h9p8thwokgnwokgnowkgnognqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaiqweifnpwagunpwiounga49nwgriogt9048rg9pwuigrnp98r4paw3n8rg9ihnaw489pa<h9p4h9p8thwokgnwokgnowkgnognqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaiqweifnpwagunpwiounga49nwgriogt9048rg9pwuigrnp98r4paw3n8rg9ihnaw489pa<h9p4h9p8thwokgnwokgnowkgnognqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaiqweifnpwagunpwiounga49nwgriogt9048rg9pwuigrnp98r4paw3n8rg9ihnaw489pa<h9p4h9p8thwokgnwokgnowkgnognqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaiqweifnpwagunpwiounga49nwgriogt9048rg9pwuigrnp98r4paw3n8rg9ihnaw489pa<h9p4h9p8thwokgnwokgnowkgnognqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaiqweifnpwagunpwiounga49nwgriogt9048rg9pwuigrnp98r4paw3n8rg9ihnaw489pa<h9p4h9p8thwokgnwokgnowkgnognqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaiqweifnpwagunpwiounga49nwgriogt9048rg9pwuigrnp98r4paw3n8rg9ihnaw489pa<h9p4h9p8thwokgnwokgnowkgnognqggpngaunupjenPNuPnPRNPrgpNPNPANpPaueorihnauohirnuaeitnhuainrhoinoaeirnghpanrguanrgaijrngionaiqweifnpwagunpwiounga49nwgriogt9048rg9pwuigrnp98r4paw3n8rg9ihnaw489pa<h9p4h9p8th4321'
            cust_eve = user.set_custom_event( myAppName, 25004, len( myData ), myData )

            # wait for test result screen
            ats.check( vats_json.html_waitDisplayContains( 'Custom event: 25004', AppName = 'vats-demo' ), 'No custom event !!!' )

            # check display content
            ats.check( vats_json.html_doesDisplayContain( '12cond try:', AppName = 'vats-demo' ), 'Wrong data !!!' )
            ats.check( vats_json.html_doesDisplayContain( 'wokgnwokgnowk', AppName = 'vats-demo' ), 'Wrong data !!!' )
            ats.check( vats_json.html_doesDisplayContain( 'Data length: ' + str( len( myData ) ), AppName = 'vats-demo' ), "Wrong data !!!" )

            # do it again with other data
            user.wait( 2000 )

            #----------------------------------------------------------------------------
            # binary data has to be converted to string
            # in C syntax it would be 'char myData[] = "Second try:\nblabla";'
            myData = 'Second try:\nblabla'
            cust_eve = user.set_custom_event( myAppName, 2003, len( myData ), myData )

            # wait for test result screen
            ats.check( vats_json.html_waitDisplayContains( 'Custom event: 2003', AppName = 'vats-demo' ), 'No custom event !!!' )

            # check display content
            ats.check( vats_json.html_doesDisplayContain( 'Second try:', AppName = 'vats-demo' ), 'Wrong data !!!' )
            ats.check( vats_json.html_doesDisplayContain( 'blabla', AppName = 'vats-demo' ), 'Wrong data !!!' )
            ats.check( vats_json.html_doesDisplayContain( 'Data length: ' + str( len( myData ) ), AppName = 'vats-demo' ), "Wrong data !!!" )

    # back to main menu
    gui_back_to_main_menu()

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.11" )
ats.set_script_summary( "GUI custom event testing" )
ats.set_recovery_function( gui_back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( gui_custom_event_1, "Custom event DUT to PC test 1" )
ats.add_test( gui_custom_event_2, "Custom event PC to DUT test 1" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
