#-----------------------------------------------------------------------------
#
#    File : gui_cardin.py
#
#    VATS demo : GUI ICC card in testing
#
#    Author : Achim Groennert
#
#    Creation date : 2013-May
#
#    Description:
'''Test module: GUI ICC card in test'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *
from vats_html import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def gui_cardin_1():
    '''GUI ICC card in test'''

    # Wait for idle screen and set terminal specific values
    myIndex = gui_prepare_test()

    if( 18 != myIndex ):
        # Choose menu item "Card reader"
        myMenu = vats_json.html_gui_selectMenu( 'Card reader' )
        # Wait until headline indicates "Card Reader Menu"
        vats_json.html_waitDisplayContains( back_to_main_menu_item, AppName = 'vats-demo' )
        if( ats.check( vats_json.html_doesDisplayContain( card_menu_title, AppName = 'vats-demo' ), 'Select "Card reader" menu failed!!!' ) ):

            # Choose menu item "ICC card in   test" from menu
            myMenu = vats_json.html_gui_selectMenu( 'ICC card in   test' )

            #---------------------------------------------------------------------------------------------------
            # Wait until GUI display indicates start of test
            if( ats.check( vats_json.html_waitDisplayContains( 'Card in test', AppName = 'vats-demo' ), 'Test start failed!!!' ) ):

                # Insert Card
                user.insert_smartcard()

                # Check displayed ICC card in state
                ats.check( vats_json.html_waitDisplayContains( 'VATS cardin: in', AppName = 'vats-demo' ), 'Insert card failed!!!' )

                # Remove card
                user.remove_smartcard()

                # Check displayed ICC card in state
                ats.check( vats_json.html_waitDisplayContains( 'VATS cardin: out', AppName = 'vats-demo' ), 'Remove card failed!!!' )

                # Insert Card
                user.insert_hybrid_card( "", "", "" )
                user.engage_hybrid_card( True, False )

                # Check displayed ICC card in state
                ats.check( vats_json.html_waitDisplayContains( 'VATS cardin: in', AppName = 'vats-demo' ), 'Insert card failed!!!' )

                # Remove card
                user.disengage_hybrid_card()
                user.remove_hybrid_card()

                # Check displayed ICC card in state
                ats.check( vats_json.html_waitDisplayContains( 'VATS cardin: out', AppName = 'vats-demo' ), 'Remove card failed!!!' )
    else:
        # terminal does not have any ICC reader, so mark test as passed
        ats.log_info( "Test case succeeded, as no ICC reader is available!" )
        ats.check( True, "Test case succeeded, as no ICC reader is available!" )

    # back to main menu
    gui_back_to_main_menu()

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.10" )
ats.set_script_summary( "GUI ICC card in testing" )
ats.set_recovery_function( gui_back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( gui_cardin_1, "GUI ICC card in test" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
