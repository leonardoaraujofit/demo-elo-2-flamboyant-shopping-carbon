#----------------------------------------------------------------------------
#
#    File : gui_buzzer.py
#
#    VATS demo : Buzzer testing
#
#    Author : Reinhard Lieberum
#
#    Creation date : 2014-Mar
#
#    Description:
'''Test module: GUI buzzer test'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *
from vats_html import *
import time

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#


#----------------------------------------------------------------------------
#
#    Test Definitions
#
def gui_buzzer_1 () :
    '''GUI buzzer test, function 1'''

    # for this test a longer time-out is needed
    buzzer_menu_wait = 10000

    # Wait for idle screen and set terminal specific values
    myIndex = gui_prepare_test()

    # initialize buzzer recording
    user.init_VatsBuzzerRecording ()

    # check buzzer record count: expected 0
    NumRecs = user.get_VatsNumberOfBuzzerRecords ()
    ats.check( ( 0 == NumRecs ), '0 buzzer records expected!' )

    # Choose menu item 1: "Miscellaneous"
    myMenu = vats_json.html_gui_selectMenu( 'Miscellaneous' )
    # Wait until headline indicates "Miscellaneous Menu"
    vats_json.html_waitDisplayContains( back_to_main_menu_item, response_time = buzzer_menu_wait, AppName = 'vats-demo' )
    if( ats.check( vats_json.html_doesDisplayContain( misc_menu_title_gui, AppName = 'vats-demo' ), 'Select "Miscellaneous" menu failed!!!' ) ):

        # get current timestamp from terminal 
        DateTime = user.get_EventDateTime ()
        ats.log_info( time.strftime( "time buzzer test: %Y-%m-%d %H:%M:%S", time.gmtime( DateTime[0] ) ) )

        # Choose menu item "buzzer test" from menu to generate a result automatically
        myMenu = vats_json.html_gui_selectMenu( 'buzzer        test' )

        # Wait until GUI buzzer indicates start of display test
        ats.check( vats_json.html_waitDisplayContains( 'Buzzer test start', response_time = buzzer_menu_wait, AppName = 'vats-demo' ), "Test start failed!!!" )
     
        # Wait until GUI buzzer indicates start of display test
        ats.check( vats_json.html_waitDisplayContains( 'Buzzer test stop', response_time = buzzer_menu_wait, AppName = 'vats-demo' ), "Test stop failed!!!" )
        user.stop_VatsBuzzerRecording ()

    # back to main menu
    gui_back_to_main_menu()

    # get idx of buzzer record 3 seconds after init
    idx = user.get_VatsBuzzerRecordIdxNext (3)
    ats.log_info( "buzzer record index 3 secondss after buzzer record start: " + str( idx ) )

    # get idx of buzzer record 5 seconds after init
    idx = user.get_VatsBuzzerRecordIdxNext (5)
    ats.log_info( "buzzer record index 5 secondss after buzzer record start: " + str( idx ) )

    # check buzzer record count: expected 5
    NumRecs = user.get_VatsNumberOfBuzzerRecords ()
    if( ats.check( ( 5 == NumRecs ), '5 buzzer records expected!' ) ):
        delta_count = 0
        prev_time   = DateTime
        for i in range( 0, NumRecs ):
            record = user.get_VatsBuzzerRecord (i)
            ats.check( (2000 + i == record[2]), 'Wrong frequency!' )
            ats.check( (500  + i == record[3]), 'Wrong duration!' )
            ats.check( (50   + i == record[4]), 'Wrong volume!' )
            usec_diff = ( ( ( record[0] - prev_time[0] ) * 1000000 ) + record[1] ) - prev_time[1]
            prev_time = ( record[0], record[1] )
            if( 0 == i ):
                ats.log_info( "difference to test start:            " + str( usec_diff ) + "us" )
            else:
                ats.log_info( "difference to previous buzzer event: " + str( usec_diff ) + "us" )
                if( 1 == i ):
                    ats.check( ( 3000000 < usec_diff ), "Expected difference to previous record is more than 3s" )
                else:
                    if( 1 < i ):
                        ats.check( ( 600000 < usec_diff ), "Expected difference to previous record is more than 600ms" )

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version ("1.4")
ats.set_script_summary ("GUI buzzer testing")
ats.set_recovery_function (gui_back_to_main_menu)

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test (gui_buzzer_1, "GUI buzzer test 1")

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run ()
