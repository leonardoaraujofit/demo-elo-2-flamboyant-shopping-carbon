#----------------------------------------------------------------------------
#
#    File : gui_keyboard_result_simulation.py
#
#    VATS demo : Keyboard testing
#
#    Author : Achim Groennert
#
#    Creation date : 2013-May
#
#    Description:
'''Test module: GUI keyboard test with ADK answer simulation'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *
from vats_html import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#

#----------------------------------------------------------------------------
#
#    Test Definitions
#
def gui_keyboard_result_sim_1():
    '''GUI keyboard test 1 GUI result simulation'''

    # Wait for idle screen and set terminal specific values
    myIndex = gui_prepare_test()

    # Choose menu item 3: "User input"
    myMenu = vats_json.html_gui_selectMenu( 'User input' )
    # Wait until headline indicates "User Input Menu"
    vats_json.html_waitDisplayContains( back_to_main_menu_item, AppName = 'vats-demo' )
    if( ats.check( vats_json.html_doesDisplayContain( user_menu_title, AppName = 'vats-demo' ), 'Select "User input" menu failed!!!' ) ):

        # Choose menu item "keyboard   test  1" from menu
        myMenu = vats_json.html_gui_selectMenu( 'keyboard   test  1' )

        #---------------------------------------------------------------------------------------------------
        # Wait until GUI display indicates start of test
        if( ats.check( vats_json.html_waitDisplayContains( 'Enter amount', AppName = 'vats-demo' ), 'Test start failed!!!' ) ):

            # Simulate "CONFIRM" key press (don't change the input value)
            if( user.is_touch_display() ):
                myButton = vats_json.html_gui_pressButton( 'OK' )
            else:
                myButton = vats_json.html_gui_pressButton( '', vats_json.VATS_KEYB_ENTER )

            # Check key press result
            ats.check( ( myButton != None ), 'Key press failed!!!' )

            # Wait until GUI display indicates end of test and check contents of display
            if( ( 13 == myIndex ) or ( 14 == myIndex ) or ( 15 == myIndex ) ):
                # e315, e315M and e335 have small displays
                ats.check( vats_json.html_waitDisplayContains( 'Amount: 1', AppName = 'vats-demo' ), 'Input failed!!!' )
            else:
                ats.check( vats_json.html_waitDisplayContains( 'Amount:\n1', AppName = 'vats-demo' ), 'Input failed!!!' )

            #---------------------------------------------------------------------------------------------------
            # Wait until GUI display indicates restart of test
            ats.check( vats_json.html_waitDisplayContains( 'Enter amount', AppName = 'vats-demo' ), 'Test restart failed!!!' )

            # Simulate "CORRECT" "CONFIRM" key press sequence (change the input value to 0)
            vats_json.html_gui_setInput( 'number', '0', 'in0' )
            if( user.is_touch_display() ):
                myButton = vats_json.html_gui_pressButton( 'OK' )
            else:
                myButton = vats_json.html_gui_pressButton( '', vats_json.VATS_KEYB_ENTER )

            # Wait until GUI display indicates end of test and check contents of display
            if( ( 13 == myIndex ) or ( 14 == myIndex ) or ( 15 == myIndex ) ):
                # e315, e315M and e335 have small displays
                ats.check( vats_json.html_waitDisplayContains( 'Amount: 0', AppName = 'vats-demo' ), 'Input failed!!!' )
            else:
                ats.check( vats_json.html_waitDisplayContains( 'Amount:\n0', AppName = 'vats-demo' ), 'Input failed!!!' )

            #---------------------------------------------------------------------------------------------------
            # Wait until GUI display indicates restart of test
            ats.check( vats_json.html_waitDisplayContains( 'Enter amount', AppName = 'vats-demo' ), 'Test restart failed!!!' )

            # Simulate "CORRECT" key and "9" "0" "0" "CONFIRM" key press sequence (change the input value to 900)
            vats_json.html_gui_setInput( 'number', '900', 'in0' )
            if( user.is_touch_display() ):
                myButton = vats_json.html_gui_pressButton( 'OK' )
            else:
                myButton = vats_json.html_gui_pressButton( '', vats_json.VATS_KEYB_ENTER )

            # Wait until GUI display indicates end of test and check contents of display
            if( ( 13 == myIndex ) or ( 14 == myIndex ) or ( 15 == myIndex ) ):
                # e315, e315M and e335 have small displays
                ats.check( vats_json.html_waitDisplayContains( 'Amount: 900', AppName = 'vats-demo' ), 'Input failed!!!' )
            else:
                ats.check( vats_json.html_waitDisplayContains( 'Amount:\n900', AppName = 'vats-demo' ), 'Input failed!!!' )

            #---------------------------------------------------------------------------------------------------
            # Wait until GUI display indicates restart of test
            ats.check( vats_json.html_waitDisplayContains( 'Enter amount', AppName = 'vats-demo' ), 'Test restart failed!!!' )

    # back to main menu
    gui_back_to_main_menu()

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.14" )
ats.set_script_summary( "GUI keyboard test with ADK answer simulation" )
ats.set_recovery_function( gui_back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( gui_keyboard_result_sim_1, "GUI keyboard test 1 GUI result simulation" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
