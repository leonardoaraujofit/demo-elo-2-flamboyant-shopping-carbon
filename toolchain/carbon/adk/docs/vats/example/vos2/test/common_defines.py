#----------------------------------------------------------------------------
#
#    File : common_defines.py
#
#    Author : Achim Groennert
#
#    Version: 1.4
#

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from vats_html import *

#----------------------------------------------------------------------------
#
#   Common defines/variables
#
main_menu_title           = "VATS Main Menu"
misc_menu_title           = "Miscellaneous Menu"
disp_menu_title           = "Display Menu"
user_menu_title           = "User Input Menu"
print_menu_title          = "Printer Menu"
card_menu_title           = "Card Reader Menu"
misc_menu_title_gui       = "Miscellaneous"
back_to_main_menu_item    = "back to main menu"

cancel_key                = "<CANCEL>"
default_wait              = 2000
default_menu_wait         = 7000
default_print_wait        = 14000
default_hold_time_gui     = 100
default_response_time_gui = 7000
default_hold_time_txt     = 100
default_response_time_txt = 5000

#----------------------------------------------------------------------------
#
#   Common functions
#
def back_to_main_menu( response_time_txt = None, hold_time_txt = None, attempts = 15 ):
    '''Returns to the terminal main menu.'''

    myCounter = attempts    # limit the wait as an endless loop is not wanted

    # Wait until back in idle screen
    # check display
    status = user.wait_display_contains( main_menu_title, response_time = response_time_txt, hold_time = hold_time_txt )
    while( ( myCounter > 0 ) and ( not status ) ):
        # Press "cancel" to leave the test, resp. submenu
        user.enter_keys( cancel_key )

        # decrease counter
        myCounter -= 1
        # check display again
        status = user.wait_display_contains( main_menu_title, response_time = response_time_txt, hold_time = hold_time_txt )

    # use ats.check() to show an error in the test case which was not able to go back to main menu
    ats.check( status, "Not yet back in main menu!" )

def prepare_test():
    '''Waits for idle screen and sets terminal specific values'''

    user.set_response_time( default_response_time_txt )
    user.set_hold_time( default_hold_time_txt )

    # wait for idle screen
    user.wait_display_contains( main_menu_title )

    # set terminal-specific values
    terminalIndex = user.setTerminalSpecificValues()

    # Remove card to have a defined starting point
    user.remove_smartcard()

    # tear off last receipt in the case there is any
    user.tearoff_printout()

    return terminalIndex

def gui_back_to_main_menu( display_region_id = "/0", response_time_gui = None, hold_time_gui = None, attempts = 6 ):
    '''Returns to the terminal main GUI menu.'''

    myCounter = attempts    # limit the wait as an endless loop is not wanted

    # Wait until back in idle screen
    # check display
    status = vats_json.html_waitDisplayContains( main_menu_title, displayRegionId = display_region_id, response_time = response_time_gui, hold_time = hold_time_gui, AppName = 'vats-demo' )
    while( ( myCounter > 0 ) and ( not status ) ):
        # Simulate "CANCEL" key press (or whatever is necessary) to return to main menu (it is unknown if any button has a text, or if the button is invisible, etc.)
        # Try invisible "CANCEL" button
        myResult = vats_json.html_gui_pressButton( '', vats_json.VATS_KEYB_CANCEL )
        if( None == myResult ):
            # Try visible "CANCEL" button (case insensitive)
            myResult = vats_json.html_gui_pressButton( '(?i)CANCEL' )
            if( None == myResult ):
                # Try visible "EXIT" button (case insensitive)
                myResult = vats_json.html_gui_pressButton( '(?i)EXIT' )
                if( None == myResult ):
                    # Try visible "QUIT" button (case insensitive)
                    myResult = vats_json.html_gui_pressButton( '(?i)QUIT' )
                    if( None == myResult ):
                        # Try visible "NO" button (case insensitive)
                        myResult = vats_json.html_gui_pressButton( '(?i)NO' )
                        if( None == myResult ):
                            # Try visible "OK" button (case insensitive)
                            myResult = vats_json.html_gui_pressButton( '(?i)OK' )
                            if( None == myResult ):
                                # Try visible "YES" button (case insensitive)
                                myResult = vats_json.html_gui_pressButton( '(?i)YES' )
                                if( None == myResult ):
                                    # Try "back to main menu" menu item
                                    myResult = vats_json.html_gui_selectMenu( back_to_main_menu_item )
                                    if( None == myResult ):
                                        ats.log_warning( 'gui_back_to_main_menu(): Not yet back in main menu and also no button found!!!' )

        # decrease counter
        myCounter -= 1
        # check display again
        status = vats_json.html_waitDisplayContains( main_menu_title, displayRegionId = display_region_id, response_time = response_time_gui, hold_time = hold_time_gui, AppName = 'vats-demo' )

    # use ats.check() to show an error in the test case which was not able to go back to main menu
    ats.check( status, "Not yet back in main menu!" )

def gui_prepare_test():
    '''Waits for GUI idle screen and sets terminal specific values'''

    vats_json.reset_gui_known_Apps()
    # wait for GUI idle screen
    myCounter = 10 # limit the wait as an endless loop is not wanted
    user.set_response_time( default_response_time_gui )
    user.set_hold_time( default_hold_time_gui )

    # check display with default_response_time_gui timeout
    status = vats_json.html_waitDisplayContains( main_menu_title, AppName = 'vats-demo' )
    while( ( myCounter > 0 ) and ( not status ) ):
        # decrease counter
        myCounter -= 1
        # check display again with default_response_time_gui timeout
        status = vats_json.html_waitDisplayContains( main_menu_title, AppName = 'vats-demo' )

    # check result
    if( not status ):
        ats.log_warning( 'gui_prepare_test(): No main menu available!!!' )

    # set terminal-specific values
    terminalIndex = user.setTerminalSpecificValues()

    # Remove card to have a defined starting point
    user.remove_smartcard()

    # tear off last receipt in the case there is any
    user.tearoff_printout()

    return terminalIndex


