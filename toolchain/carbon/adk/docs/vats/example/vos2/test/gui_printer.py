#----------------------------------------------------------------------------
#
#    File : gui_printer.py
#
#    VATS demo : Printer testing
#
#    Author : Achim Groennert
#
#    Creation date : 2013-May
#
#    Description:
'''Test module: GUI printer test'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
from atsunit import *
from user import *
from common_defines import *
from vats_html import *

#----------------------------------------------------------------------------
#
#    Script Specific Helper Routines
#


#----------------------------------------------------------------------------
#
#    Test Definitions
#
def gui_printer_1 () :
    '''GUI printer test, function 1'''

    # Wait for idle screen and set terminal specific values
    myIndex = gui_prepare_test()

    # Check if printer is available
    if( ats.check( vats_json.html_doesDisplayContain( 'Printer', AppName = 'vats-demo' ), 'No printer available!!!' ) ):
        # Choose menu item 4: "Printer"
        myMenu = vats_json.html_gui_selectMenu( 'Printer' )
        # Wait until headline indicates "Printer Menu"
        vats_json.html_waitDisplayContains( back_to_main_menu_item, AppName = 'vats-demo' )
        if( ats.check( vats_json.html_doesDisplayContain( print_menu_title, AppName = 'vats-demo' ), 'Select "Printer" menu failed!!!' ) ):

            # Choose menu item "printer    test  1" from menu
            myMenu = vats_json.html_gui_selectMenu( 'printer    test  1' )

            #---------------------------------------------------------------------------------------------------
            # Wait until GUI display indicates start of test
            ats.check( vats_json.html_waitDisplayContains( 'Printer test', AppName = 'vats-demo' ), 'Test start failed!!!' )

            # check display
            ats.check( vats_json.html_waitDisplayContains( '007', AppName = 'vats-demo' ), 'Test 007 start failed!!!' )

            # check display
            ats.check( vats_json.html_waitDisplayContains( 'Result: 0', response_time = default_print_wait, AppName = 'vats-demo' ), 'Test run failed!!!' )

            # wait for receipt
            ats.check( vats_json.html_waitPrintoutContains( 'RECEIPT N: 007', default_print_wait ), 'Print 007 failed!!!' )

            # set error code (-4 = Paper end)
            vats_json.set_HtmlPrintResult( -4 )

            #---------------------------------------------------------------------------------------------------
            # Wait until GUI display indicates restart of test
            ats.check( vats_json.html_waitDisplayContains( '0815', response_time = default_print_wait, AppName = 'vats-demo' ), 'Test 0815 start failed!!!' )

            # check display
            ats.check( vats_json.html_waitDisplayContains( 'Result: -4', response_time = default_print_wait, AppName = 'vats-demo' ), 'Print did not fail as expected!!!' )

            # reset error code (0 = no error)
            vats_json.set_HtmlPrintResult( 0 )

            #---------------------------------------------------------------------------------------------------
            # Wait until GUI display indicates restart of test
            ats.check( vats_json.html_waitDisplayContains( 'Reprinting...', response_time = default_print_wait, AppName = 'vats-demo' ), 'Test restart failed!!!' )

            # check display
            ats.check( vats_json.html_waitDisplayContains( 'Result: 0', response_time = default_print_wait, AppName = 'vats-demo' ), 'Print failed!!!' )

            # wait for receipt
            ats.check( vats_json.html_waitPrintoutContains( 'RECEIPT N: 0815', default_print_wait ), 'Print 0815 failed!!!' )

        # back to main menu
        gui_back_to_main_menu()

        # tear off test receipt
        user.tearoff_printout()

#----------------------------------------------------------------------------
#
#    Script Summary and Version
#
ats.set_version( "1.7" )
ats.set_script_summary( "GUI printer testing" )
ats.set_recovery_function( gui_back_to_main_menu )

#----------------------------------------------------------------------------
#
#    Test Registration
#
ats.add_test( gui_printer_1, "GUI printer test 1" )

#----------------------------------------------------------------------------
#
#   Run Tests
#
ats.run()
