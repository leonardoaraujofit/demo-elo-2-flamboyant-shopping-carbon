ECHO OFF
rem ===========================================================================
rem VATS DEMO PROGRAM (VERIX)
rem 2014/04/28
rem Automatically loads VATS test application into VX680 and executes several
rem python test scripts. The terminal returns into download mode after test
rem execution.
rem
rem Prerequisites:
rem - DQ driver has to be installed correctly
rem - terminal has to be in download mode (partial download, "USB Dev")
rem - Jenkins Slave is enabled for signing (FST + card_reader + signing card)
rem - this batch file should be located in your projects root folder
rem - execute the batch file from command window
rem
rem Test case evaluation:
rem - check the test results on "outcome: pass"
rem
rem Modifications:
rem - 2014/04/28: add unzip, signing, and error exit
rem
rem ===========================================================================


rem ===========================================================================
rem TODO 1: SET CONFIGURATION
rem ===========================================================================
set termtype=VX680
set zipdir=autoload
set loaddir=%zipdir%\bin
set pythondir=%zipdir%\test
set curdir=%CD%
set PYTHONPATH=C:\Program Files (x86)\VERIFONE\VATS\bin

rm -rf %zipdir%
mkdir %zipdir% 
cd %zipdir%
rm -rf %loaddir%
rm -rf %pythondir%

echo ===========================================================================
echo COPY AND UNPACK ZIP FILE
echo ===========================================================================
copy D:\ATS\GIT\dev-vats-Kopie\dist\vats-vrx-1.3.0-bin.zip *
unzip -o vats-vrx-1.3.0-bin.zip
if errorlevel 1 goto error

echo ===========================================================================
echo SIGN BINARIES
echo ===========================================================================
cd %curdir%
rem DOES NOT WORK YET
rem copy D:\ATS\ADK-1.5\VATS_Release_1.3.0_LOAD_VRX_680\Load\bin\vats_hdlr.vsa.P7s %loaddir%
rem "%VSFSTOOL%\FileSignature.exe" --file %loaddir%\vats_hdlr.vsa I
rem "%VSFSTOOL%\FileSignature.exe" --file %loaddir%\vats_test_html.out I
rem "%VSFSTOOL%\FileSignature.exe" --file %loaddir%\guiprtservervats.out I

copy D:\ATS\GIT\dev-vats\Signed\vats_hdlr.vsa.P7S %loaddir%
copy D:\ATS\GIT\dev-vats\Signed\vats_test_html.out.P7S %loaddir%
copy D:\ATS\GIT\dev-vats\Signed\guiprtservervats.out.P7S %loaddir%

rem ats_hdlr.ini has to be prepared correctly
copy vats_handler\export\vrxevo\ats_hdlr_dq_vx680_autoload.ini %loaddir%\ats_hdlr.ini
if errorlevel 1 goto error

rem ===========================================================================
rem TODO 2: SETUP THE DOWNLOAD OF YOUR APPLICATION HERE INSTEAD
rem ===========================================================================

rem ===========================================================================
rem CONFIGURE DOWNLOAD FOR VATS TEST APP WITH GUI, HTML PRINTER AND USE DQ DRV
rem ===========================================================================
SET ENVIRO=-r*:*/ -rconfig.sys ^
setdrive.i setgroup.1 ^
*GO=vats_hdlr.vsa ^
*VATS_STARTAPPS=guiprtservervats.out,vats_test_html.out ^
*VATS_ENABLED=1 ^
*VATS_DISP_VIA_DQDRV=1 ^
*VATS_KBD_VIA_DQDRV=1 ^
*VATS_MAGSTRIPE_VIA_DQDRV=1 ^
*VATS_CARDIN_VIA_DQDRV=0 ^
*VATS_PRINTER_VIA_DQDRV=0 ^
*DEBUG=1 ^
*DQAGENT=1

rem ===========================================================================
rem DON'T FORGET TO SET in ats_hdlr.ini:
rem       ENABLE DQ DRIVER SETTINGS
rem       VATS_CONNECTION_TYPE=04
rem       VATS_DOWNLOADMODE=1
rem ===========================================================================
SET FILES=%loaddir%\ats_hdlr.ini ^
%loaddir%\vats_hdlr.vsa ^
%loaddir%\vats_hdlr.vsa.P7s ^
%loaddir%\vats_test_html.out ^
%loaddir%\vats_test_html.out.P7S ^
%loaddir%\guiprtservervats.out ^
%loaddir%\guiprtservervats.out.P7S ^
setgroup.15 ^
setdrive.i ^
%loaddir%\www\%termtype%\confirm-cancel.tmpl@www/vats-demo/%termtype%/confirm-cancel.tmpl ^
%loaddir%\www\%termtype%\gui.ini@www/vats-demo/%termtype%/gui.ini ^
%loaddir%\www\%termtype%\input.tmpl@www/vats-demo/%termtype%/input.tmpl ^
%loaddir%\www\%termtype%\mainmenu.tmpl@www/vats-demo/%termtype%/mainmenu.tmpl ^
%loaddir%\www\%termtype%\statusbar.tmpl@www/vats-demo/%termtype%/statusbar.tmpl ^
%loaddir%\www\img_bw\config.png@www/vats-demo/img_bw/config.png ^
%loaddir%\www\img_bw\options.png@www/vats-demo/img_bw/options.png ^
%loaddir%\www\print\receipt.html@www/vats-demo/print/receipt.html ^
setgroup.1 ^
setdrive.i ^
%loaddir%\www\print\prt.ini@www/vats-demo/print/prt.ini ^
setgroup.15 ^
setdrive.f ^
%loaddir%\load\vrx\fonts\fonts.dir@fonts/fonts.dir ^
%loaddir%\load\vrx\fonts\DejaVuSansMono.ttf@fonts/DejaVuSansMono.ttf ^
%loaddir%\load\vrx\fonts\DejaVuSansMono-Bold.ttf@fonts/DejaVuSansMono-Bold.ttf


echo ===========================================================================
echo DOWNLOAD VIA COM9 (USB cable) INTO TERMINAL
echo ===========================================================================
rem use -p1 for serial loading;
ddl -p9 %ENVIRO% %FILES%
if errorlevel 1 goto error2

echo ===========================================================================
echo WAITING FOR TERMINAL RESTART (50 seconds)
echo ===========================================================================
@ping -n 50 localhost> nul

echo ===========================================================================
echo SETUP CONNECTION TO TERMINAL
echo START KM4: connect terminal via USB cable
echo ===========================================================================
START /B CMD /K CALL "load_start_km4_USB.bat"
rem START /B CMD /K CALL "load_start_km4_TCP_.bat"
if errorlevel 1 goto error3

echo ===========================================================================
echo WAITING FOR APPLICATION STARTUP (10 seconds)
echo ===========================================================================
@ping -n 10 localhost> nul

rem ===========================================================================
rem change to the folder where your python test scripts are located
rem ===========================================================================
cd %pythondir%

echo ===========================================================================
echo START PYTHON TEST SCRIPTS
echo ===========================================================================

rem ===========================================================================
rem TODO 3: CALL YOUR! PYTHON TEST SCRIPTS HERE INSTEAD
rem ===========================================================================

gui_model_info.py   --execute=gui_model_info_1
if errorlevel 1 goto error4
gui_display.py      --execute=gui_display_1
if errorlevel 1 goto error4
gui_screen.py       --execute=gui_screen_1
if errorlevel 1 goto error4
gui_keyboard.py     --execute=gui_keyboard_1
if errorlevel 1 goto error4
gui_cardin.py       --execute=gui_cardin_1
if errorlevel 1 goto error4
gui_magstripe.py    --execute=gui_magstripe_1
if errorlevel 1 goto error4

echo ===========================================================================
echo "TEST DONE - please check for outcome: pass"
echo ===========================================================================
goto end

:error
echo ===========================================================================
echo ABORTED due to ERROR !!!
echo ===========================================================================
taskkill /im km4.exe /f
exit /b 1

:error1
echo ===========================================================================
echo ERROR DURING SIGNING !!!
echo ===========================================================================
exit /b 1

:error2
echo ===========================================================================
echo ERROR DURING DOWNLOAD !!!
echo ===========================================================================
exit /b 1

:error3
echo ===========================================================================
echo ERROR DURING km4 START !!!
echo ===========================================================================
exit /b 1

:error4
echo ===========================================================================
echo ERROR DURING TEST EXECUTION !!!
echo ===========================================================================
taskkill /im km4.exe /f
exit /b 1

:end
taskkill /im km4.exe /f
echo ===========================================================================
echo ENDED
echo ===========================================================================
exit
