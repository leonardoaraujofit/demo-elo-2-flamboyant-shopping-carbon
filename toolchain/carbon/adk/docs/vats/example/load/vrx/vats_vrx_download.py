#-----------------------------------------------------------------------------
#
#    File : vats_vrx_download.py
#
#    VATS demo : Collects all necessary files, modifies them as specified,
#                creates a vats.zip file and if necessary downloads all
#                files and configurations via DDL tool.
#
#    Author : Achim Groennert
#
#    Creation date : 2015-Jun
#
#    Last modified date : 2016-Jul-19
#
#    Description:
'''Collects all necessary files, modifies them as specified, creates a vats.zip
   file and if necessary downloads all files and configurations via DDL tool.'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
import os
import shutil
import time
import winreg
import subprocess
import re


#----------------------------------------------------------------------------
#
#   delete existing folder
#
def delete_existing_dir( dirName = None ):
    if( None != dirName ):
        if( os.access( dirName, os.W_OK ) ):
            shutil.rmtree( dirName )
            time.sleep( 0.5 )


#----------------------------------------------------------------------------
#
#   delete existing file
#
def delete_existing_file( fileName = None ):
    if( None != fileName ):
        if( os.access( fileName, os.W_OK ) ):
            os.remove( fileName )


#----------------------------------------------------------------------------
#
#   delete old files and folders from previous downloads
#
def clean_dir():
    myCwd = os.getcwd()
    if( 0 < len( myCwd ) ):
        if( myCwd[-1] != '\\' ):
            myCwd = myCwd + '\\'
    delete_existing_dir( myCwd + 'staging' )
    delete_existing_file( myCwd + 'ats_hdlr.ini' )
    delete_existing_file( myCwd + 'vats.zip' )
    delete_existing_file( myCwd + 'CTLSL1_A4.zip' )
    delete_existing_file( myCwd + 'CTLSL2_A4.zip' )


#----------------------------------------------------------------------------
#
#   copy specific file types from source dir to dest dir, '*' copies all files
#
def copy_filetype( src_dir, dest_dir, file_ext ):
    if( ( None != src_dir ) and ( 0 < len( src_dir ) ) and ( None != dest_dir ) and ( 0 < len( dest_dir ) ) and ( None != file_ext ) and ( 0 < len( file_ext ) ) ):
        # be sure the paths end with backslash
        if( src_dir[-1] != '\\' ):
            src_dir = src_dir + '\\'
        if( dest_dir[-1] != '\\' ):
            dest_dir = dest_dir + '\\'
        # get all files from directory
        file_list = os.listdir( src_dir )
        file_list.sort()
        if( ( '*' == file_ext ) and ( 0 < len( file_list ) ) ):
            # copy all files
            if( not os.access( dest_dir, os.R_OK ) ):
                os.makedirs( dest_dir )
            for file in file_list:
                shutil.copy( src_dir + file, dest_dir )
        else:
            # check all files for extension match
            for file in file_list:
                file_parts = file.split( '.' )
                if( 1 < len( file_parts ) ):
                    if( file_parts[-1].lower() == file_ext.lower() ):
                        if( not os.access( dest_dir, os.R_OK ) ):
                            os.makedirs( dest_dir )
                        shutil.copy( src_dir + file, dest_dir )


#----------------------------------------------------------------------------
#
#   create config and serve files
#
def create_vrx_config( myenv, myfiles ):
    if( ( None != myenv ) and ( 0 < len( myenv ) ) and ( None != myfiles ) and ( 0 < len( myfiles ) ) ):
        # Open config.txt file
        with open( 'config.txt', 'w' ) as cfgfile:
            for cfg_elem in myenv.split( ' ' ):
                myList = cfg_elem.split( '=' )
                for cfg_line in myList:
                    if( '*GO' == cfg_line ):
                        # set *GO to zero length value to be able to overwrite it in the next step
                        cfgfile.write( '*GO\n' )
                        cfgfile.write( '\n' )
                    cfgfile.write( cfg_line + '\n' )

        saved_txt = ''
        # Open serve.txt file
        with open( 'serve.txt', 'w' ) as srvfile:
            for file_elem in myfiles.split( ' ' ):
                if( 'vats.zip' == file_elem ):
                    saved_txt = file_elem
                else:
                    srvfile.write( os.getcwd() + '\\' + file_elem + '\n' )
            if( 0 < len( saved_txt ) ):
                srvfile.write( os.getcwd() + '\\' + saved_txt + '\n' )


#----------------------------------------------------------------------------
#
#   fetch DQ driver settings from ini file and return the corresponding environment settings
#
def fetch_dq_config( iniFile ):
    # Open ini file
    with open( iniFile, 'r+b' ) as tmpfile:
        data = tmpfile.read()
        input_string = data.decode( 'iso-8859-1', 'replace' )

    display_via_dqdrv   = re.compile( r'''VATS_DISPLAY_VIA_DQDRV=([\d]*?)[\D\s\r\n]''' )
    printer_via_dqdrv   = re.compile( r'''VATS_PRINTER_VIA_DQDRV=([\d]*?)[\D\s\r\n]''' )
    kbd_via_dqdrv       = re.compile( r'''VATS_KBD_VIA_DQDRV=([\d]*?)[\D\s\r\n]''' )
    magstripe_via_dqdrv = re.compile( r'''VATS_MAGSTRIPE_VIA_DQDRV=([\d]*?)[\D\s\r\n]''' )
    cardin_via_dqdrv    = re.compile( r'''VATS_CARDIN_VIA_DQDRV=([\d]*?)[\D\s\r\n]''' )
    touch_via_dqdrv     = re.compile( r'''VATS_TOUCH_VIA_DQDRV=([\d]*?)[\D\s\r\n]''' )

    text = ' *VATS_DISP_VIA_DQDRV='
    helper = display_via_dqdrv.findall( input_string )
    if( ( 0 < len( helper ) ) and ( 1 == len( helper[0] ) ) ):
        text += helper[0]
    else:
        text += '0'
        print( 'error 1' )
    text += ' *VATS_PRINTER_VIA_DQDRV='
    helper = printer_via_dqdrv.findall( input_string )
    if( ( 0 < len( helper ) ) and ( 1 == len( helper[0] ) ) ):
        text += helper[0]
    else:
        text += '0'
        print( 'error 2' )
    text += ' *VATS_KBD_VIA_DQDRV='
    helper = kbd_via_dqdrv.findall( input_string )
    if( ( 0 < len( helper ) ) and ( 1 == len( helper[0] ) ) ):
        text += helper[0]
    else:
        text += '0'
        print( 'error 3' )
    text += ' *VATS_MAGSTRIPE_VIA_DQDRV='
    helper = magstripe_via_dqdrv.findall( input_string )
    if( ( 0 < len( helper ) ) and ( 1 == len( helper[0] ) ) ):
        text += helper[0]
    else:
        text += '0'
        print( 'error 4' )
    text += ' *VATS_CARDIN_VIA_DQDRV='
    helper = cardin_via_dqdrv.findall( input_string )
    if( ( 0 < len( helper ) ) and ( 1 == len( helper[0] ) ) ):
        text += helper[0]
    else:
        text += '0'
        print( 'error 5' )
    text += ' *VATS_TOUCH_VIA_DQDRV='
    helper = touch_via_dqdrv.findall( input_string )
    if( ( 0 < len( helper ) ) and ( 1 == len( helper[0] ) ) ):
        text += helper[0]
    else:
        text += '0'
        print( 'error 6' )
    return text


#----------------------------------------------------------------------------
#
#   Main
#
if __name__ == '__main__':
    # get environment variables
    myDWL_IF    = os.getenv( 'DWL_PARAM_DOWNLOAD_IF'       , default = '' )
    myDQ_DRIVER = os.getenv( 'DWL_PARAM_DQ_DRIVER'         , default = '' )
    myTERM_TYPE = os.getenv( 'DWL_PARAM_TERM_TYPE'         , default = '' )
    myAPP_TYPE  = os.getenv( 'DWL_PARAM_APP_TYPE'          , default = '' )
    mySTAR_DBG  = os.getenv( 'DWL_PARAM_STAR_DEBUG'        , default = '' )
    myLOAD_CTLS = os.getenv( 'DWL_PARAM_LOAD_CTLS'         , default = '' )
    myCONN_TYPE = os.getenv( 'DWL_PARAM_VATS_CONN_TYPE'    , default = '' )
    myDWL_MODE  = os.getenv( 'DWL_PARAM_VATS_DOWNLOAD_MODE', default = '' )
    myDWL_TYPE  = os.getenv( 'DWL_TYPE'                    , default = '' )

    # print the environment variables
    print( "Download comm '" + myDWL_IF    + "'" )
    print( "dq            '" + myDQ_DRIVER + "'" )
    print( "term type     '" + myTERM_TYPE + "'" )
    print( "app type      '" + myAPP_TYPE  + "'" )
    print( "*debug        '" + mySTAR_DBG  + "'" )
    print( "ctls          '" + myLOAD_CTLS + "'" )
    print( "VATS comm     '" + myCONN_TYPE + "'" )
    print( "VATS download '" + myDWL_MODE  + "'" )
    print( "Download env. '" + myDWL_TYPE  + "'" )

    # determine current working directory
    myCwd = os.getcwd()
    if( 0 < len( myCwd ) ):
        if( myCwd[-1] != '\\' ):
            myCwd = myCwd + '\\'

    # global defines for error checking and configuration
    glob_dq_drv    = [ "on", "off" ]
    glob_app_type  = [ "text", "adkgui" ]
    glob_debug_if  = [ "0", "1", "2", "4" ]
    glob_ctls_load = [ "0", "1" ]
    glob_vats_conn = [ "1", "4", "8", "2", "16", "32" ]
    glob_dwl_mode  = [ "0", "1" ]
    # this dict contains all terminal-specific defines, e.g. ini file for pipe mode ('P_INI'),
    # ini file for DQ mode with ADK ('DA_INI'), ini file for DQ mode without ADK ('DT_INI'), etc.
    glob_term_defs = {
                        "e265":      { 'DQ': '1', 'P_INI': myCwd + 'ats_hdlr_pipe_e265.ini',   'DA_INI': myCwd + 'ats_hdlr_dq_adk_e265.ini',   'DT_INI': myCwd + 'ats_hdlr_dq_e265.ini',   'CTLS': myCwd + 'VX-CTLS-2-01.15.00A4-600PCD4\\'      },
                        "e315":      { 'DQ': '1', 'P_INI': myCwd + 'ats_hdlr_pipe_e315.ini',   'DA_INI': myCwd + 'ats_hdlr_dq_adk_e315.ini',   'DT_INI': myCwd + 'ats_hdlr_dq_e315.ini',   'CTLS': myCwd + 'VX-CTLS-2-01.15.00A4-600PCD_Ver.B\\' },
                        "e315M":     { 'DQ': '1', 'P_INI': myCwd + 'ats_hdlr_pipe_e315m.ini',  'DA_INI': myCwd + 'ats_hdlr_dq_adk_e315m.ini',  'DT_INI': myCwd + 'ats_hdlr_dq_e315m.ini',  'CTLS': myCwd + 'VX-CTLS-2-01.15.00A4-600PCD4\\'      },
                        "e335":      { 'DQ': '1', 'P_INI': myCwd + 'ats_hdlr_pipe_e335.ini',   'DA_INI': myCwd + 'ats_hdlr_dq_adk_e335.ini',   'DT_INI': myCwd + 'ats_hdlr_dq_e335.ini',   'CTLS': myCwd + 'VX-CTLS-2-01.15.00A4-600PCD_Ver.B\\' },
                        "e355":      { 'DQ': '3', 'P_INI': myCwd + 'ats_hdlr_pipe_e355.ini',   'DA_INI': myCwd + 'ats_hdlr_dq_adk_e355.ini',   'DT_INI': myCwd + 'ats_hdlr_dq_e355.ini',   'CTLS': myCwd + 'VX-CTLS-2-01.15.00A4-600PCD4\\'      },
                        "VX520":     { 'DQ': '1', 'P_INI': myCwd + 'ats_hdlr_pipe_vx520.ini',  'DA_INI': myCwd + 'ats_hdlr_dq_adk_vx520.ini',  'DT_INI': myCwd + 'ats_hdlr_dq_vx520.ini',  'CTLS': myCwd + 'VX-CTLS-2-01.15.00A4-520PCD3\\'      },
                        "VX520C":    { 'DQ': '3', 'P_INI': myCwd + 'ats_hdlr_pipe_vx520c.ini', 'DA_INI': myCwd + 'ats_hdlr_dq_adk_vx520c.ini', 'DT_INI': myCwd + 'ats_hdlr_dq_vx520c.ini', 'CTLS': myCwd + 'VX-CTLS-2-01.15.00A4-520CPCD2\\'     },
                        "VX675GPRS": { 'DQ': '3', 'P_INI': myCwd + 'ats_hdlr_pipe_vx675.ini',  'DA_INI': myCwd + 'ats_hdlr_dq_adk_vx675.ini',  'DT_INI': myCwd + 'ats_hdlr_dq_vx675.ini',  'CTLS': myCwd + 'VX-CTLS-2-01.15.00A4-675PCD2\\'      },
                        "VX6753G":   { 'DQ': '3', 'P_INI': myCwd + 'ats_hdlr_pipe_vx675.ini',  'DA_INI': myCwd + 'ats_hdlr_dq_adk_vx675.ini',  'DT_INI': myCwd + 'ats_hdlr_dq_vx675.ini',  'CTLS': myCwd + 'VX-CTLS-2-01.15.00A4-675PCD4\\'      },
                        "VX680":     { 'DQ': '3', 'P_INI': myCwd + 'ats_hdlr_pipe_vx680.ini',  'DA_INI': myCwd + 'ats_hdlr_dq_adk_vx680.ini',  'DT_INI': myCwd + 'ats_hdlr_dq_vx680.ini',  'CTLS': myCwd + 'VX-CTLS-2-01.15.00A4-680PCD5\\'      },
                        "VX685":     { 'DQ': '3', 'P_INI': myCwd + 'ats_hdlr_pipe_vx685.ini',  'DA_INI': myCwd + 'ats_hdlr_dq_adk_vx685.ini',  'DT_INI': myCwd + 'ats_hdlr_dq_vx685.ini',  'CTLS': myCwd + 'VX-CTLS-2-01.15.00A4-690PCD2\\'      },
                        "VX690":     { 'DQ': '3', 'P_INI': myCwd + 'ats_hdlr_pipe_vx690.ini',  'DA_INI': myCwd + 'ats_hdlr_dq_adk_vx690.ini',  'DT_INI': myCwd + 'ats_hdlr_dq_vx690.ini',  'CTLS': myCwd + 'VX-CTLS-2-01.15.00A4-690PCD2\\'      },
                        "VX805":     { 'DQ': '1', 'P_INI': myCwd + 'ats_hdlr_pipe_vx805.ini',  'DA_INI': myCwd + 'ats_hdlr_dq_adk_vx805.ini',  'DT_INI': myCwd + 'ats_hdlr_dq_vx805.ini',  'CTLS': myCwd + 'VX-CTLS-2-01.15.00A4-805PCD_Ver.B\\' },
                        "VX820":     { 'DQ': '3', 'P_INI': myCwd + 'ats_hdlr_pipe_vx820.ini',  'DA_INI': myCwd + 'ats_hdlr_dq_adk_vx820.ini',  'DT_INI': myCwd + 'ats_hdlr_dq_vx820.ini',  'CTLS': myCwd + 'VX-CTLS-2-01.15.00A4-820PCD4\\'      },
                        "VX825":     { 'DQ': '3', 'P_INI': myCwd + 'ats_hdlr_pipe_vx825.ini',  'DA_INI': myCwd + 'ats_hdlr_dq_adk_vx825.ini',  'DT_INI': myCwd + 'ats_hdlr_dq_vx825.ini',  'CTLS': myCwd + 'VX-CTLS-2-01.15.00A4-825PCD2\\'      }
                     }

    # check environment variables
    if( myDQ_DRIVER in glob_dq_drv ):
        if( myTERM_TYPE in glob_term_defs ):
            if( myAPP_TYPE in glob_app_type ):
                if( mySTAR_DBG in glob_debug_if ):
                    if( myLOAD_CTLS in glob_ctls_load ):
                        if( myCONN_TYPE in glob_vats_conn ):
                            if( myDWL_MODE in glob_dwl_mode ):
                                # Reset the path to the python.exe
                                python_executable = ''
                                with winreg.OpenKey( winreg.HKEY_LOCAL_MACHINE, 'SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\App Paths\Python.exe' ) as key:
                                    # Fetch the path to the python.exe path from registry
                                    python_executable = winreg.QueryValue( key, '' )
                                if( 10 > len( python_executable ) ):
                                    # Fetching from registry seems to be failed: Use default path to the python.exe (currently python 3.4.x)
                                    python_executable = 'C:\\Python34\\python.exe'

                                # clean up old files
                                clean_dir()

                                # set default values
                                dwlEnv      = '-r*:*/ -rconfig.sys *unzip2=vats.zip'
                                dwlFiles    = 'vats.zip'
                                vatsCfg     = '*DQAGENT=' + glob_term_defs[myTERM_TYPE]['DQ']
                                ctls_files  = ''
                                ini_key     = ''

                                # identical settings, no matter if DQ activated or not
                                vatsCfg += ' *DEBUG=' + mySTAR_DBG
                                if( "text" == myAPP_TYPE ):
                                    # text mode
                                    vatsCfg += ' *GO=VATS_HDLR.VSA *VATS_STARTAPPS=VATS_TEST.OUT,VATS_TEST2.OUT *VATS_ENABLED=1'
                                else:
                                    # ADK mode
                                    vatsCfg += ' *GO=VATS_HDLR.VSA *VATS_STARTAPPS=GUIPRTSERVERVATS.OUT,VATS_TEST_HTML.VSA *VATS_ENABLED=1'
                                    vatsCfg += ' GUI_DISPLAY=VFIGUIPIPE'

                                if( "off" == myDQ_DRIVER ):
                                    # pipes section

                                    # set ini_key value
                                    ini_key = 'P_INI'
                                    print( 'VATS handler using pipes for ' + myTERM_TYPE )
                                    print( '===========================================================================' )

                                else:
                                    # DQ driver section

                                    # set ini_key value
                                    if( "adkgui" == myAPP_TYPE ):
                                        ini_key = 'DA_INI'
                                    else:
                                        ini_key = 'DT_INI'
                                    print( 'VATS handler using DQ driver for ' + myTERM_TYPE )
                                    print( '===========================================================================' )

                                    # common dq section
                                    if( "text" == myAPP_TYPE ):
                                        # text mode
                                        vatsCfg += ' *DQPRT=64 *DQPRTOFF=1'

                                # terminal-specific values from global defines: myTERM_TYPE is the key of the dict
                                subprocess.call( [python_executable, myCwd + "modify_vats_hdlr_ini_file.py", "-i", glob_term_defs[myTERM_TYPE][ini_key], "-d", myDWL_MODE, "-c", myCONN_TYPE] )
                                vatsCfg += fetch_dq_config( myCwd + 'ats_hdlr.ini' )

                                # prepare for CTLS download
                                if( ( "adkgui" == myAPP_TYPE ) and ( "1" == myLOAD_CTLS ) ):
                                    dwlEnv     += ',CTLSL1_A4.zip,CTLSL2_A4.zip'
                                    dwlFiles   += ' CTLSL1_A4.zip CTLSL2_A4.zip'
                                    ctls_files  = glob_term_defs[myTERM_TYPE]['CTLS']

                                # copy files section
                                if( 0 < len( ctls_files ) ):
                                    copy_filetype( ctls_files, myCwd, '*' )
                                base_path = myCwd + 'staging\\'
                                # common files
                                os.makedirs( base_path + '1\\' )
                                shutil.copy( myCwd + 'ats_hdlr.ini',                      base_path + '1\\' )
                                shutil.copy( myCwd + 'vats_hdlr.vsa',                     base_path + '1\\' )
                                shutil.copy( myCwd + 'vats_hdlr.vsa.P7S',                 base_path + '1\\' )
                                shutil.copy( myCwd + 'libvfiipc.vsl',                     base_path + '1\\' )
                                shutil.copy( myCwd + 'libvfiipc.vsl.P7S',                 base_path + '1\\' )
                                shutil.copy( myCwd + 'libposix.vsl',                      base_path + '1\\' )
                                shutil.copy( myCwd + 'libposix.vsl.P7S',                  base_path + '1\\' )
                                if( "adkgui" == myAPP_TYPE ):
                                    # common for all "adkgui" variants
                                    shutil.copy( myCwd + 'EMV_APPLICATIONS.XML',          base_path + '1\\' )
                                    shutil.copy( myCwd + 'EMV_KEYS.XML',                  base_path + '1\\' )
                                    shutil.copy( myCwd + 'EMV_TERMINAL.XML',              base_path + '1\\' )
                                    shutil.copy( myCwd + 'EMV_CTLS_APPLICATIONS.XML',     base_path + '1\\' )
                                    shutil.copy( myCwd + 'EMV_CTLS_KEYS.XML',             base_path + '1\\' )
                                    shutil.copy( myCwd + 'EMV_CTLS_TERMINAL.XML',         base_path + '1\\' )
                                    shutil.copy( myCwd + 'guiprtservervats.out',          base_path + '1\\' )
                                    shutil.copy( myCwd + 'guiprtservervats.out.P7S',      base_path + '1\\' )
                                    shutil.copy( myCwd + 'libEMV_CT_Client.vsl',          base_path + '1\\' )
                                    shutil.copy( myCwd + 'libEMV_CT_Client.vsl.P7S',      base_path + '1\\' )
                                    shutil.copy( myCwd + 'libEMV_CT_Framework.vsl',       base_path + '1\\' )
                                    shutil.copy( myCwd + 'libEMV_CT_Framework.vsl.P7S',   base_path + '1\\' )
                                    shutil.copy( myCwd + 'libEMV_CT_VelocityK.vsl',       base_path + '1\\' )
                                    shutil.copy( myCwd + 'libEMV_CT_VelocityK.vsl.P7S',   base_path + '1\\' )
                                    shutil.copy( myCwd + 'libEMV_CTLS_Client.vsl',        base_path + '1\\' )
                                    shutil.copy( myCwd + 'libEMV_CTLS_Client.vsl.P7S',    base_path + '1\\' )
                                    shutil.copy( myCwd + 'libEMV_CTLS_Framework.vsl',     base_path + '1\\' )
                                    shutil.copy( myCwd + 'libEMV_CTLS_Framework.vsl.P7S', base_path + '1\\' )
                                    shutil.copy( myCwd + 'libvfiguiprt.vsl',              base_path + '1\\' )
                                    shutil.copy( myCwd + 'libvfiguiprt.vsl.P7S',          base_path + '1\\' )
                                    shutil.copy( myCwd + 'vats_test_html.vsa',            base_path + '1\\' )
                                    shutil.copy( myCwd + 'vats_test_html.vsa.P7S',        base_path + '1\\' )
                                    os.makedirs( base_path + '15\\' )
                                    shutil.copy( myCwd + 'SchemeInfo.xml',                base_path + '15\\' )
                                    os.makedirs( base_path + 'f1\\' )
                                    shutil.copy( myCwd + 'EMV_CTLS_DATA.XML',             base_path + 'f1\\' )
                                    copy_filetype( myCwd + 'load\\vrx\\fonts\\',          base_path + 'f15\\fonts\\',     '*' )

                                    # e265, e315, e315M, e335, e355, VX805 and VX825 have no printer
                                    if( ( "e265" != myTERM_TYPE ) and ( "e315" != myTERM_TYPE ) and ( "e315M" != myTERM_TYPE ) and ( "e335" != myTERM_TYPE ) and ( "e355" != myTERM_TYPE ) and ( "VX805" != myTERM_TYPE ) and ( "VX825" != myTERM_TYPE ) ):
                                        copy_filetype( myCwd + 'www\\print\\',            base_path + '1\\www\\vats-demo\\print\\',  'ini' )
                                        copy_filetype( myCwd + 'www\\print\\',            base_path + '1\\www\\vats-demo\\print\\', 'html' )

                                    # copy images
                                    # the displays of e315, e315M and e335 are very very small, so don't use images at all
                                    if( ( "e315" != myTERM_TYPE ) and ( "e315M" != myTERM_TYPE ) and ( "e335" != myTERM_TYPE ) ):
                                        if( ( "e265" == myTERM_TYPE ) or ( "VX520" == myTERM_TYPE ) or ( "VX805" == myTERM_TYPE ) ):
                                            # VX520 and VX805 have a monochrome display
                                            copy_filetype( myCwd + 'www\\img_bw\\',       base_path + '1\\www\\vats-demo\\img_bw\\', '*' )
                                        else:
                                            # all other terminal types have color display
                                            copy_filetype( myCwd + 'www\\img\\',          base_path + '1\\www\\vats-demo\\img\\',    '*' )

                                    # copy resource files
                                    # 128x32M15N for e315, e315M, e335
                                    if( ( "e315" == myTERM_TYPE ) or ( "e315M" == myTERM_TYPE ) or ( "e335" == myTERM_TYPE ) ):
                                        copy_filetype( myCwd + 'www\\128x32M15N\\',       base_path + '1\\www\\vats-demo\\128x32M15N\\',  '*' )
                                    # 128x64M24N for VX520, VX805
                                    if( ( "VX520" == myTERM_TYPE ) or ( "VX805" == myTERM_TYPE ) ):
                                        copy_filetype( myCwd + 'www\\128x64M24N\\',       base_path + '1\\www\\vats-demo\\128x64M24N\\',  '*' )
                                    # 160x120M15N for e265
                                    if( ( "e265" == myTERM_TYPE ) ):
                                        copy_filetype( myCwd + 'www\\160x120M15N\\',      base_path + '1\\www\\vats-demo\\160x120M15N\\', '*' )
                                    # 240x320C15T for VX680, VX685, VX690, VX820, VX825
                                    if( ( "VX680" == myTERM_TYPE ) or ( "VX685" == myTERM_TYPE ) or ( "VX690" == myTERM_TYPE ) or ( "VX820" == myTERM_TYPE ) or ( "VX825" == myTERM_TYPE ) ):
                                        copy_filetype( myCwd + 'www\\240x320C15T\\',      base_path + '1\\www\\vats-demo\\240x320C15T\\', '*' )
                                    # 320x240C15N for e355
                                    if( "e355" == myTERM_TYPE ):
                                        copy_filetype( myCwd + 'www\\320x240C15N\\',      base_path + '1\\www\\vats-demo\\320x240C15N\\', '*' )
                                    # 320x240C24N for VX520C, VX675GPRS, VX6753G
                                    if( ( "VX520C" == myTERM_TYPE ) or ( "VX675GPRS" == myTERM_TYPE ) or ( "VX6753G" == myTERM_TYPE ) ):
                                        copy_filetype( myCwd + 'www\\320x240C24N\\',      base_path + '1\\www\\vats-demo\\320x240C24N\\', '*' )

                                else:
                                    # common for all "text" variants
                                    shutil.copy( myCwd + 'vats_test.out',                 base_path + '1\\' )
                                    shutil.copy( myCwd + 'vats_test.out.P7S',             base_path + '1\\' )
                                    shutil.copy( myCwd + 'vats_test2.out',                base_path + '1\\' )
                                    shutil.copy( myCwd + 'vats_test2.out.P7S',            base_path + '1\\' )

                                    if( "on" == myDQ_DRIVER ):
                                        # only necessary for PIN entry
                                        shutil.copy( myCwd + 'emvadf.vso',                base_path + '1\\' )
                                        shutil.copy( myCwd + 'emvadf.vso.P7S',            base_path + '1\\' )

                                # finish staging directory creation
                                print( '\n***************************************************************************' )
                                print( 'created environment variables:' )
                                print( '***************************************************************************' )
                                print( 'VATSCFG  : ' + vatsCfg )
                                print( '\nENVIRO   : ' + dwlEnv )
                                print( '\ncurdir   : ' + myCwd )
                                print( '\nFILES    : ' + dwlFiles )
                                print( '\nDWL_TYPE : ' + myDWL_TYPE )
                                print( '***************************************************************************' )
                                create_vrx_config( vatsCfg, dwlFiles )
                                print( '\n***************************************************************************' )
                                print( 'Convert config.txt to CONFIG.$$$' )
                                subprocess.call( ["vlr.exe", "-c", "config.txt", "staging\\1\\CONFIG.$$$"] )
                                print( '\n***************************************************************************' )
                                print( 'Create vats.zip' )
                                shutil.make_archive( myCwd + 'vats', 'zip', myCwd + 'staging' )
                                # remove staging dir
                                delete_existing_dir( base_path )

                                # in the case it is a non-Jenkins environment start the DDL tool and download the files
                                if( "jenkins" != myDWL_TYPE ):
                                    print( '\n***************************************************************************' )
                                    print( 'ddl -p' + myDWL_IF + ' ' + dwlEnv + ' ' + dwlFiles )
                                    ddl_list = ["ddl.exe", "-p" + myDWL_IF ]
                                    for elem in dwlEnv.split( ' ' ):
                                        ddl_list.append( elem )
                                    for elem in dwlFiles.split( ' ' ):
                                        ddl_list.append( elem )
                                    subprocess.call( ddl_list )

                            else:
                                print( "===========================================================================" )
                                print( "ERROR!!!" )
                                print( "unknown user choice VATS download '" + myDWL_MODE + "'" )
                                print( "===========================================================================" )
                        else:
                            print( "===========================================================================" )
                            print( "ERROR!!!" )
                            print( "unknown user choice VATS comm '" + myCONN_TYPE + "'" )
                            print( "===========================================================================" )
                    else:
                        print( "===========================================================================" )
                        print( "ERROR!!!" )
                        print( "unknown user choice ctls '" + myLOAD_CTLS + "'" )
                        print( "===========================================================================" )
                else:
                    print( "===========================================================================" )
                    print( "ERROR!!!" )
                    print( "unknown user choice *debug '" + mySTAR_DBG + "'" )
                    print( "===========================================================================" )
            else:
                print( "===========================================================================" )
                print( "ERROR!!!" )
                print( "unknown user choice app type '" + myAPP_TYPE + "'" )
                print( "===========================================================================" )
        else:
            print( "===========================================================================" )
            print( "ERROR!!!" )
            print( "unknown user choice term type '" + myTERM_TYPE + "'" )
            print( "===========================================================================" )
    else:
        print( "===========================================================================" )
        print( "ERROR!!!" )
        print( "unknown user choice dq '" + DWL_PARAM_DQ_DRIVER + "'" )
        print( "===========================================================================" )
