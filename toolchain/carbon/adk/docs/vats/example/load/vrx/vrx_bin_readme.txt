Note:
	Some Verix versions do not support dynamic heap management, so in the case
	a terminal has a Verix version which does not support the dynamic heap
	management all binaries have to be patched before signing:
		vrx_bin_patch_heap.bat
	Note:
		vrx_bin_patch_heap.bat will patch all VATS demo binaries with a default
		heap size of 5 MB. VATS team did not check if a lower value would also
		fit, as it is only used for the VATS demo application.
		VATS team recommends to use a Verix version which has dynamic heap
		management support!

Note:
	Before downloading the binary files using vrx_bin_download.bat the
	following files have to be signed using the VeriFone FST:
		vats_hdlr.vsa
		vats_test.out
		vats_test2.out
		emvadf.vso
		vats_test_html.vsa
		libEMV_CT_Client.vsl
		libEMV_CT_Framework.vsl
		libEMV_CT_VelocityK.vsl
		libEMV_CTLS_Client.vsl
		libEMV_CTLS_Framework.vsl
		guiprtservervats.out
		libvfiguiprt.vsl
		libvfiipc.vsl
	There is a file "vrx_signing.fst" which can be used with the FST GUI for
	signing all files. Example CLI command after putting the FST into remote
	signing mode:
		"C:\Program Files (x86)\VERIFONE\FST\FileSignature.exe" vrx_signing.fst -nogui

Note:
	guiprtservervats.out, libEMV_CT_Client.vsl, kibEMV_CT_Framework.vsl, 
	libEMV_CT_VelocityK.vsl, libEMV_CTLS_Client.vsl, libEMV_CTLS_Framework.vsl,
	libvfiguiprt.vsl and libvfiipc.vsl files are provided "as is". They were
	copied from the corresponding distribution packages.

Note:
	The vats_test_html.vsa is linked with ADK EMV CTLS shared objects, so the
	CTLS shared objects have to be loaded into the terminal even if it is not
	CTLS capable. The VFI reader has to be loaded also, as that installation
	includes a needed library (ctls.lib) which is needed for ADK EMV CTLS. The
	VFI reader has to be loaded only once.

Note:
	Before downloading the binary files using vrx_bin_download.bat please
	specify the PYTHONPATH variable. It is also assumed that the DDL and VLR tools
	are accessible via PATH variable. Both tools are part of the Verix SDK ("bin"
	folder of the SDK).
	Example CLI commands for default VATS installation path:
		set PYTHONPATH=C:\Program Files (x86)\VERIFONE\VATS\bin
		vrx_bin_download.bat

Note:
	The example CLI commands for signing and downloading will only work in the
	case the current working directory is the one where the Verix binaries, the
	vrx_bin_download.bat and the vrx_signing.fst are located. It is also
	assumed that both the DDL and the VLR tool are accessible via PATH variable.
