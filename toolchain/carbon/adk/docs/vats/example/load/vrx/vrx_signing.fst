; File syntax :
; - lines beginning with ';' and empty lines are ignored
; - filenames are in "quotation marks"
; - do not remove the '/1', ..., '/4' section delimiters

[Files to sign]/1

;(1=in flash, 0=not in flash)

0 ".\emvadf.vso"

0 ".\guiprtservervats.out"

0 ".\libEMV_CT_Client.vsl"

0 ".\libEMV_CT_Framework.vsl"

0 ".\libEMV_CT_VelocityK.vsl"

0 ".\libEMV_CTLS_Client.vsl"

0 ".\libposix.vsl"

0 ".\libvfiguiprt.vsl"

0 ".\libvfiipc.vsl"

0 ".\libEMV_CTLS_Framework.vsl"

0 ".\vats_hdlr.vsa"

0 ".\vats_test.out"

0 ".\vats_test_html.vsa"

0 ".\vats_test2.out"


[Security Level]/2

;(0=SECURED, 1=CONTROLLED, 2=DEFAULT)

0


[Certificate name & location]/3

".\Certif.crt"


[Notes]/4

