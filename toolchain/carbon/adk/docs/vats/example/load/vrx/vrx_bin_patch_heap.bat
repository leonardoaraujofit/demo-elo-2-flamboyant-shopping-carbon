@echo off
echo.
echo.
echo ***************************************************************************
echo vrx_patch_heap.bat: 14.09.2015
echo ***************************************************************************
echo.

SETLOCAL ENABLEDELAYEDEXPANSION

echo.
echo ===========================================================================
echo SIGN BINARIES
echo ===========================================================================
echo ---------------------------------------------------------------------------
echo %CD%/vats_hdlr.vsa
vrxhdr.exe -h 5242880 vats_hdlr.vsa
if errorlevel 1 goto error
echo ---------------------------------------------------------------------------
echo %CD%/vats_test.out
vrxhdr.exe -h 5242880 vats_test.out
if errorlevel 1 goto error
echo ---------------------------------------------------------------------------
echo %CD%/vats_test2.out
vrxhdr.exe -h 5242880 vats_test2.out
if errorlevel 1 goto error
echo ---------------------------------------------------------------------------
echo %CD%/vats_test_html.vsa
vrxhdr.exe -h 5242880 vats_test_html.vsa
if errorlevel 1 goto error
echo ---------------------------------------------------------------------------
echo %CD%/guiprtservervats.out
vrxhdr.exe -h 5242880 guiprtservervats.out
if errorlevel 1 goto error
echo ---------------------------------------------------------------------------

goto end

:error
echo ===========================================================================
echo vrx_patch_heap.bat ERROR!!!
echo ===========================================================================

:end
echo ***************************************************************************
echo end of vrx_patch_heap.bat
echo ***************************************************************************
pause
