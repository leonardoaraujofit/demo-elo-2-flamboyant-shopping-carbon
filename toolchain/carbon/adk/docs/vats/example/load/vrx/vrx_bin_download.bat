@echo off
echo.
echo.
echo ***************************************************************************
echo vrx_bin_download.bat: 11.11.2015
echo ***************************************************************************
echo.

SETLOCAL ENABLEDELAYEDEXPANSION


set DWL_PARAM_DOWNLOAD_IF=0
echo Choose the desired download com port, e.g. COM1 (1), COM9 (9), COM12 (12), etc.
set /p DWL_PARAM_DOWNLOAD_IF=
echo.

set DWL_PARAM_DQ_DRIVER=0
echo Choose the desired method: DQ driver (on) or pipes (off)
set /p DWL_PARAM_DQ_DRIVER=
echo.

set DWL_PARAM_TERM_TYPE=0
echo Choose the desired Terminal: e265 (e265), e315 (e315), e315M (e315M), e335 (e335), e355 (e355), VX520 (VX520), VX520C (VX520C), VX675 (VX675GPRS, VX6753G), VX680 (VX680), VX685 (VX685), VX690 (VX690), VX805 (VX805), VX820 (VX820) or VX825 (VX825)
set /p DWL_PARAM_TERM_TYPE=
echo.

set DWL_PARAM_APP_TYPE=0
echo Choose the desired Application: Text mode (text) or HTML mode (adkgui)
set /p DWL_PARAM_APP_TYPE=
echo.

set DWL_PARAM_STAR_DEBUG=0
echo Choose the desired debug mode: on (1: COM1, 2: COM2, 4: COM4) or off (0)
set /p DWL_PARAM_STAR_DEBUG=
echo.

set DWL_PARAM_LOAD_CTLS=0
echo Note:
echo      Load of VFI CTLS reader into terminal is necessary for HTML mode at least once.
echo      Even in the case the terminal is not CTLS capable.
echo.
echo Load VFI CTLS reader into terminal (only for HTML mode): load (1) or don't load (0)
set /p DWL_PARAM_LOAD_CTLS=
echo.

set DWL_PARAM_VATS_CONN_TYPE=0
echo Choose the desired VATS connection type: COM1 (1), COM2 (2), USB (4), TCP/IP (8), COM6 (16) or WiFi (32)
echo Note: COM2 and COM6 are only possible for VX820 DUET!
set /p DWL_PARAM_VATS_CONN_TYPE=
echo.

set DWL_PARAM_VATS_DOWNLOAD_MODE=0
echo Choose the desired VATS auto download mode: activated (1) or deactivated (0)
set /p DWL_PARAM_VATS_DOWNLOAD_MODE=
echo.

call vats_vrx_download.py

pause
