#-----------------------------------------------------------------------------
#
#    File : modify_vats_hdlr_ini_file.py
#
#    VATS demo : Fetch VATS handler ini file (input), change the desired
#                settings and save it to ats_hdlr.ini in the same directory.
#
#    Author : Achim Groennert
#
#    Creation date : 2014-Oct
#
#    Last modified date : 2015-Oct-13
#
#    Description:
'''Fetch VATS handler ini file (input), change the desired settings and save
   it to ats_hdlr.ini in the same directory.'''

#----------------------------------------------------------------------------
#
#   Library Import Section
#
import argparse
import re
import os
import subprocess

#----------------------------------------------------------------------------
#
#   change ini file
#
def change_ini( file, dwl_mode, connect = None ):
    # Open log file
    with open( file, 'r+b' ) as tmpfile:
        data = tmpfile.read()
        input_string = data.decode( 'iso-8859-1', 'replace' )

    myWd = os.path.dirname( file )
    if( 0 < len( myWd ) ):
        myWd = myWd + '\\'
    else:
        myWd = os.getcwd() + '\\'
    fileName = myWd + 'ats_hdlr.ini'

    dwl_str = 'VATS_DOWNLOADMODE=%(dwl)s' % { "dwl": dwl_mode }
    # Parse the file
    if( None == connect ):
        for part1, part3 in redown1.findall( input_string ):
            if( 0 < len( part1 ) ):
                # Change the line endings to UNIX
                part1 = part1.replace( '\r\n', '\n' )
                if( 0 < len( part1 ) ):
                    if( part1[-1] != '\n' ):
                        part1 = part1 + '\n'
                else:
                    part1 = '\n'
            else:
                part1 = '\n'
            if( 0 < len( part3 ) ):
                # Change the line endings to UNIX
                part3 = part3.replace( '\r\n', '\n' )
                if( 0 < len( part3 ) ):
                    if( part3[-1] != '\n' ):
                        part3 = part3 + '\n'
                else:
                    part3 = '\n'
            else:
                part3 = '\n'
            if( ( 0 < len( part1 ) ) and ( 0 < len( part3 ) ) ):
                # write file
                myFile = open( fileName, 'w' )
                myFile.write( part1 )
                myFile.write( dwl_str )
                myFile.write( part3 )
                myFile.close()
    else:
        for part1, part2, part3 in redown2.findall( input_string ):
            connection = 'VATS_CONNECTION_TYPE=' + connect
            if( 0 < len( part1 ) ):
                # Change the line endings to UNIX
                part1 = part1.replace( '\r\n', '\n' )
                if( 0 < len( part1 ) ):
                    if( part1[-1] != '\n' ):
                        part1 = part1 + '\n'
                else:
                    part1 = '\n'
            else:
                part1 = '\n'
            if( 0 < len( part2 ) ):
                # Change the line endings to UNIX
                part2 = part2.replace( '\r\n', '\n' )
                if( 0 < len( part2 ) ):
                    if( part2[-1] != '\n' ):
                        part2 = part2 + '\n'
                else:
                    part2 = '\n'
            else:
                part2 = '\n'
            if( 0 < len( part3 ) ):
                # Change the line endings to UNIX
                part3 = part3.replace( '\r\n', '\n' )
                if( 0 < len( part3 ) ):
                    if( part3[-1] != '\n' ):
                        part3 = part3 + '\n'
                else:
                    part3 = '\n'
            else:
                part3 = '\n'
            if( ( 0 < len( part1 ) ) and ( 0 < len( part2 ) ) and ( 0 < len( part3 ) ) ):
                # write file
                myFile = open( fileName, 'w' )
                myFile.write( part1 )
                myFile.write( connection )
                myFile.write( part2 )
                myFile.write( dwl_str )
                myFile.write( part3 )
                myFile.close()

#----------------------------------------------------------------------------
#
#   Main
#
if __name__ == '__main__':
    # Set-up command-line option parser
    parser = argparse.ArgumentParser( description = 'Fetch VATS handler ini file (input), change the desired settings and save it to ats_hdlr.ini in the same directory.' )
    parser.add_argument( "-i", "--input",  dest = "input",  help = "input file to be parsed; default is 'C:\\temp\\ats_hdlr_dq_adk_vx520.ini'",  default = "C:\\temp\\ats_hdlr_dq_adk_vx520.ini" )
    parser.add_argument( "-d", "--downloadmode",  dest = "downloadmode",  help = "VATS download mode; default is enable download mode",  default = "1" )
    parser.add_argument( "-c", "--connect",  dest = "connect",  help = "VATS connection type; default is no change",  default = "" )
    # Parse known arguments, ignore unknown arguments
    ( options, args ) = parser.parse_known_args()

    # Prepare regular expressions search patterns
    redown1 = re.compile( r'''
                        ([\s\S\r\n]*?VATS_DOWNLOADMODE=[\s\S\r\n]*?)
                        [\r\n]+VATS_DOWNLOADMODE=\S
                        ([\s\S\r\n]*)
                        ''', re.X )
    redown2 = re.compile( r'''
                        ([\s\S\r\n]*?VATS_CONNECTION_TYPE=[\s\S\r\n]*?)
                        [\r\n]+VATS_CONNECTION_TYPE=\S
                        ([\s\S\r\n]*?VATS_DOWNLOADMODE=[\s\S\r\n]*?)
                        [\r\n]+VATS_DOWNLOADMODE=\S
                        ([\s\S\r\n]*)
                        ''', re.X )

    if( 0 == len( options.connect ) ):
        connection = None
    else:
        connection = options.connect
    # Parse the given VATS log file and split into several documents
    change_ini( options.input, options.downloadmode, connection )
