var searchData=
[
  ['appname_5fmax',['APPNAME_MAX',['../atslib_8h.html#ab25e978178075de5ff440461350cc653',1,'atslib.h']]],
  ['ats_5fchained_5fevent_5fchunk_5ftimout',['ATS_CHAINED_EVENT_CHUNK_TIMOUT',['../atslib_8h.html#a7311d4f2d6c2c4c1d11019f18ac8ddc4',1,'atslib.h']]],
  ['atsreg_5fcom_5ferror',['ATSREG_COM_ERROR',['../atslib_8h.html#ad261c41d838e7f6a05c80b5644cecf87',1,'atslib.h']]],
  ['atsreg_5fcustom',['ATSREG_CUSTOM',['../atslib_8h.html#a246afcc8f48deed7e0ce3ee2a5debe2c',1,'atslib.h']]],
  ['atsreg_5femv_5fct',['ATSREG_EMV_CT',['../atslib_8h.html#a52c96998f6ac03b39473ec9be7e56294',1,'atslib.h']]],
  ['atsreg_5femv_5fctls',['ATSREG_EMV_CTLS',['../atslib_8h.html#a5a64ac8c56e791497f56aa99e64d73c7',1,'atslib.h']]],
  ['atsreg_5fgui',['ATSREG_GUI',['../atslib_8h.html#a42138cc97700234cc45d41e778afbe44',1,'atslib.h']]],
  ['atsreg_5ficccard_5fin',['ATSREG_ICCCARD_IN',['../atslib_8h.html#ae1fede453c1005134516eecac2ce1b0c',1,'atslib.h']]],
  ['atsreg_5fkeyboard',['ATSREG_KEYBOARD',['../atslib_8h.html#a058e3091c86d5a6790471ac434680705',1,'atslib.h']]],
  ['atsreg_5flast',['ATSREG_LAST',['../atslib_8h.html#a1993d4f4a070c05bffdedb4dd78750b7',1,'atslib.h']]],
  ['atsreg_5fmagstripe',['ATSREG_MAGSTRIPE',['../atslib_8h.html#a921ab32ba56ee361092f2d8077c99fb3',1,'atslib.h']]],
  ['atsreg_5fnone',['ATSREG_NONE',['../atslib_8h.html#a999b5d8982d592355f31906b71fc9591',1,'atslib.h']]],
  ['atsreg_5fprinter',['ATSREG_PRINTER',['../atslib_8h.html#ab915cacc847c137e0deed5833b003fcf',1,'atslib.h']]],
  ['atsreg_5fuser_5ftouch',['ATSREG_USER_TOUCH',['../atslib_8h.html#ab3b35d21af549cc029349880153c2cac',1,'atslib.h']]]
];
