var NAVTREE =
[
  [ "ADK-VATS", "index.html", [
    [ "Programmers Guide - Overview", "index.html", [
      [ "Preface", "index.html#sec_Preface_Over", [
        [ "Audience", "index.html#subsec_Preface_Audience_Over", null ],
        [ "Organization", "index.html#subsec_Preface_Organization_Over", null ],
        [ "Error reports", "index.html#subsec_Preface_ErrorReports_Over", null ],
        [ "Feature requests", "index.html#subsec_Preface_FeatureRequests_Over", null ],
        [ "Related Documentation", "index.html#subsec_Preface_RelatedDocumentation_Over", [
          [ "ADK framework", "index.html#subsubsec_Preface_RelatedDocumentation_AdkFramework_Over", null ],
          [ "JIRA references", "index.html#subsubsec_Preface_RelatedDocumentation_JiraReferences_Over", null ],
          [ "Web references", "index.html#subsubsec_Preface_RelatedDocumentation_WebReferences_Over", null ],
          [ "Glossary", "index.html#subsubsec_Preface_RelatedDocumentation_Glossary_Over", null ]
        ] ]
      ] ],
      [ "Concept", "index.html#sec_Concept_Over", [
        [ "History", "index.html#subsec_Concept_History_Over", null ],
        [ "Basis", "index.html#subsec_Concept_Basis_Over", null ],
        [ "Limitations", "index.html#subsec_Concept_Limitations_Over", null ]
      ] ],
      [ "Components", "index.html#sec_Components_Over", [
        [ "VATS integration options", "index.html#subsec_Components_VatsIntegrationOptions_Over", null ],
        [ "VATS in general", "index.html#subsec_Components_VatsInGeneral_Over", null ],
        [ "ADK-GUIPRT usage", "index.html#subsec_Components_GuiprtUsage_Over", null ],
        [ "No ADK-GUIPRT usage", "index.html#subsec_Components_NoGuiprtUsage_Over", null ]
      ] ],
      [ "Features", "index.html#sec_Features_Over", [
        [ "Verix", "index.html#subsec_Features_Verix_Over", null ],
        [ "V/OS", "index.html#subsec_Features_VOs_Over", null ],
        [ "V/OS2", "index.html#subsec_Features_VOs2_Over", null ],
        [ "Common features", "index.html#subsec_Features_CommonFeatures_Over", null ]
      ] ],
      [ "Flows", "index.html#sec_Flows_Over", [
        [ "Message flow from application to VATS test scripts", "index.html#subsec_Flows_MessageFlowFromApplicationToVatsTestScripts_Over", null ],
        [ "Message flow from VATS test script to application", "index.html#subsec_Flows_MessageFlowFromVatsTestScriptToApplication_Over", null ],
        [ "Printing message flow (both directions)", "index.html#subsec_Flows_PrintingMessageFlowBothDirections_Over", null ]
      ] ],
      [ "Security", "index.html#sec_Security_Over", [
        [ "Problem statement", "index.html#subsec_Security_ProblemStatement_Over", null ],
        [ "Requirements", "index.html#subsec_Security_Requirements_Over", null ],
        [ "Current situation", "index.html#subsec_Security_CurrentSituation_Over", [
          [ "Verix", "index.html#subsubsec_Security_CurrentSituation_Verix_Over", null ],
          [ "V/OS", "index.html#subsubsec_Security_CurrentSituation_VOs_Over", null ],
          [ "V/OS2", "index.html#subsubsec_Security_CurrentSituation_VOs2_Over", null ]
        ] ]
      ] ],
      [ "Packaging overview", "index.html#sec_PackagingOverview_Over", [
        [ "VATS-DOC package", "index.html#subsec_PackagingOverview_VatsDocPackage_Over", null ],
        [ "VATS-VOS2-DEV package", "index.html#subsec_PackagingOverview_VatsVos2DevPackage_Over", null ],
        [ "VATS-VOS2-LOAD package", "index.html#subsec_PackagingOverview_VatsVos2LoadPackage_Over", null ],
        [ "VATS-VOS-DEV package", "index.html#subsec_PackagingOverview_VatsVosDevPackage_Over", null ],
        [ "VATS-VOS-LOAD package", "index.html#subsec_PackagingOverview_VatsVosLoadPackage_Over", null ],
        [ "VATS-VRX-DEV package", "index.html#subsec_PackagingOverview_VatsVrxDevPackage_Over", null ],
        [ "VATS-VRX-LOAD package", "index.html#subsec_PackagingOverview_VatsVrxLoadPackage_Over", null ],
        [ "VATS-WIN32-DEV package", "index.html#subsec_PackagingOverview_VatsWin32DevPackage_Over", null ],
        [ "VATS-FULL package", "index.html#subsec_PackagingOverview_VatsFullPackage_Over", null ],
        [ "Where to get the VATS packages", "index.html#subsec_PackagingOverview_WhereToGetTheVatsPackages_Over", null ]
      ] ],
      [ "Demo application", "index.html#sec_DemoApplication_Over", [
        [ "Preparations", "index.html#subsec_DemoApplication_Preparations_Over", [
          [ "Getting a DQ driver for your Verix terminal", "index.html#subsubsec_DemoApplication_Preparations_GettingADqDriverForYourVerixTerminal_Over", null ],
          [ "Getting a VATSOS package for your V/OS terminal", "index.html#subsubsec_DemoApplication_Preparations_GettingAVatsosPackageForYourVOsterminal_Over", null ]
        ] ],
        [ "Running the VATS demo application", "index.html#subsec_DemoApplication_RunningTheVatsDemoApplication_Over", [
          [ "Verix without ADK-GUIPRT", "index.html#subsubsec_DemoApplication_RunningTheVatsDemoApplication_VerixWithoutGuiprt_Over", null ],
          [ "V/OS with ADK-GUIPRT", "index.html#subsubsec_DemoApplication_RunningTheVatsDemoApplication_VOsWithGuiprt_Over", null ],
          [ "Verix with ADK-GUIPRT", "index.html#subsubsec_DemoApplication_RunningTheVatsDemoApplication_VerixWithGuiprt_Over", null ]
        ] ]
      ] ]
    ] ],
    [ "Legal Notes", "legal_notes_page.html", null ],
    [ "Programmers Guide - PC", "page__programmers__guide__p_c.html", [
      [ "Preface", "page__programmers__guide__p_c.html#sec_Preface_Pc", [
        [ "Audience", "page__programmers__guide__p_c.html#subsec_audience_Pc", null ],
        [ "Organization", "page__programmers__guide__p_c.html#subsec_organization_Pc", [
          [ "JIRA references", "page__programmers__guide__p_c.html#subsubsec_Jira_References_Pc", null ],
          [ "Web references", "page__programmers__guide__p_c.html#subsubsec_Web_References_Pc", null ],
          [ "Glossary", "page__programmers__guide__p_c.html#subsubsec_Glossary_Pc", null ]
        ] ]
      ] ],
      [ "Installation", "page__programmers__guide__p_c.html#sec_Installation_Pc", [
        [ "System specifications", "page__programmers__guide__p_c.html#subsec_Installation_System_specifications_Pc", null ],
        [ "Cleanup / remove old version", "page__programmers__guide__p_c.html#subsec_Cleanup___remove_old_version_Pc", null ],
        [ "Installation Procedure", "page__programmers__guide__p_c.html#subsec_Installation_Installation_Procedure_Pc", null ]
      ] ],
      [ "VATS PC components", "page__programmers__guide__p_c.html#sec_VATS_PC_components_Pc", [
        [ "Terminal viewer variants", "page__programmers__guide__p_c.html#subsec_Terminal_viewer_variants_Pc", [
          [ "ADK component usage", "page__programmers__guide__p_c.html#subsubsec_ADK_component_usage_Pc", null ],
          [ "Application without DirectGUI and without HTMLPrinter", "page__programmers__guide__p_c.html#subsubsec_Application_w_o_DirectGUI_and_w_o_HTMLPrinter_Pc", null ],
          [ "Application with DirectGUI and without HTMLPrinter", "page__programmers__guide__p_c.html#subsubsec_Application_with_DirectGUI_and_w_o_HTMLPrinter_Pc", null ],
          [ "Application without DirectGUI and with HTMLPrinter", "page__programmers__guide__p_c.html#subsubsec_Application_w_o_DirectGUI_and_with_HTMLPrinter_Pc", null ],
          [ "Application with DirectGUI and with HTMLPrinter", "page__programmers__guide__p_c.html#subsubsec_Application_with_DirectGUI_and_with_HTMLPrinter_Pc", null ],
          [ "Terminal viewer \"*.ini\" files", "page__programmers__guide__p_c.html#subsubsec_Terminal_viewer_ini_files_Pc", null ]
        ] ]
      ] ],
      [ "Configuration", "page__programmers__guide__p_c.html#sec_Configuration_Pc", [
        [ "Example configuration 1:", "page__programmers__guide__p_c.html#subsec_Example_configuration_1_Pc", null ],
        [ "Example configuration 2:", "page__programmers__guide__p_c.html#subsec_Example_configuration_2_Pc", null ],
        [ "Example configuration 3:", "page__programmers__guide__p_c.html#subsec_Example_configuration_3_Pc", null ]
      ] ],
      [ "TestManager", "page__programmers__guide__p_c.html#sec_TestManager_Pc", [
        [ "Running the TestManager", "page__programmers__guide__p_c.html#subsec_Running_the_TestManager_Pc", null ],
        [ "Adding tests and groups", "page__programmers__guide__p_c.html#subsec_Adding_tests_and_groups_Pc", null ],
        [ "Saving and restoring test configurations", "page__programmers__guide__p_c.html#subsec_Saving_and_restoring_test_configurations_Pc", null ],
        [ "Organizing tests within groups", "page__programmers__guide__p_c.html#subsec_Organizing_tests_within_groups_Pc", null ],
        [ "Running tests", "page__programmers__guide__p_c.html#subsec_Running_tests_Pc", null ],
        [ "Log output", "page__programmers__guide__p_c.html#subsec_Log_output_Pc", null ],
        [ "Log file rotation", "page__programmers__guide__p_c.html#subsec_Logfile_rotation", null ],
        [ "Search facility", "page__programmers__guide__p_c.html#subsec_Search_facility_Pc", null ],
        [ "Agent control", "page__programmers__guide__p_c.html#subsec_Agent_control_Pc", null ],
        [ "Options", "page__programmers__guide__p_c.html#subsec_sec_TestManager_Options_Pc", [
          [ "Logging", "page__programmers__guide__p_c.html#subsubsec_TestManager_Options_Logging_Pc", null ],
          [ "Report", "page__programmers__guide__p_c.html#subsubsec_TestManager_Options_Report_Pc", null ],
          [ "Terminal", "page__programmers__guide__p_c.html#subsubsec_TestManager_Options_Terminal_Pc", null ],
          [ "Manual", "page__programmers__guide__p_c.html#subsubsec_TestManager_Options_Manual_Pc", null ],
          [ "Viewer", "page__programmers__guide__p_c.html#subsubsec_TestManager_Options_Viewer_Pc", null ],
          [ "Learn", "page__programmers__guide__p_c.html#subsubsec_TestManager_Options_Learn_Pc", null ],
          [ "Editor", "page__programmers__guide__p_c.html#subsubsec_TestManager_Options_Editor_Pc", null ],
          [ "Python CLI params / working dir", "page__programmers__guide__p_c.html#subsubsec_TestManager_Options_Python_CLI_params_working_dir_Pc", null ]
        ] ],
        [ "Communication settings", "page__programmers__guide__p_c.html#subsec_TestManager_Communication_settings_Pc", null ],
        [ "Editor feature", "page__programmers__guide__p_c.html#subsec_TestManager_Editor_feature_Pc", null ],
        [ "Terminal viewer", "page__programmers__guide__p_c.html#subsec_TestManager_Terminal_viewer_Pc", [
          [ "Terminal viewer without DirectGUI and without HTMLPrinter", "page__programmers__guide__p_c.html#subsubsec_TestManager_Terminal_viewer_Terminal_viewer_w_o_DirectGUI_and_w_o_HTMLPrinter_Pc", null ],
          [ "Terminal viewer with DirectGUI and with HTMLPrinter", "page__programmers__guide__p_c.html#subsubsec_TestManager_Terminal_viewer_Terminal_viewer_with_DirectGUI_and_with_HTMLPrinter_Pc", null ]
        ] ],
        [ "Learn tool", "page__programmers__guide__p_c.html#subsec_TestManager_Learn_tool_Pc", [
          [ "Basic principles", "page__programmers__guide__p_c.html#subsubsec_TestManager_Learn_tool_Basic_principles_Pc", null ],
          [ "Buttons and functions", "page__programmers__guide__p_c.html#subsubsec_TestManager_Learn_tool_Buttons_and_functions_Pc", null ],
          [ "Mouse and keyboard controls", "page__programmers__guide__p_c.html#subsubsec_TestManager_Learn_tool_Mouse_and_keyboard_controls_Pc", null ]
        ] ],
        [ "Test flow", "page__programmers__guide__p_c.html#subsec_TestManager_Test_flow_Pc", null ]
      ] ],
      [ "Test scripts", "page__programmers__guide__p_c.html#sec_Test_scripts_Pc", [
        [ "Script and test titles", "page__programmers__guide__p_c.html#subsec_Test_scripts_Script_and_test_titles_Pc", null ],
        [ "Script sections", "page__programmers__guide__p_c.html#subsec_Test_scripts_Script_sections_Pc", [
          [ "Script description", "page__programmers__guide__p_c.html#subsubsec_Test_scripts_Script_sections_Script_description_Pc", null ],
          [ "Links to libraries", "page__programmers__guide__p_c.html#subsubsec_Test_scripts_Script_sections_Links_to_libraries_Pc", null ],
          [ "Helper subroutines", "page__programmers__guide__p_c.html#subsubsec_Test_scripts_Script_sections_Helper_subroutines_Pc", null ],
          [ "Test subroutines", "page__programmers__guide__p_c.html#subsubsec_Test_scripts_Script_sections_Test_subroutines_Pc", null ],
          [ "Script summary and script version", "page__programmers__guide__p_c.html#subsubsec_Test_scripts_Script_sections_Script_summary_and_script_version_Pc", null ],
          [ "Recovery function", "page__programmers__guide__p_c.html#subsubsec_Test_scripts_Script_sections_Recovery_function_Pc", null ],
          [ "Test registration", "page__programmers__guide__p_c.html#subsubsec_Test_scripts_Script_sections_Test_registration_Pc", null ],
          [ "Run tests", "page__programmers__guide__p_c.html#subsubsec_Test_scripts_Script_sections_Run_tests_Pc", null ]
        ] ],
        [ "Test subroutine structure", "page__programmers__guide__p_c.html#subsec_Test_scripts_Test_subroutine_structure_Pc", null ],
        [ "Test script management", "page__programmers__guide__p_c.html#subsec_Test_scripts_Test_script_management_Pc", [
          [ "Script and test descriptions", "page__programmers__guide__p_c.html#subsubsec_Test_scripts_Test_script_management_Script_and_test_descriptions_Pc", null ]
        ] ],
        [ "Planning your testing", "page__programmers__guide__p_c.html#subsec_Test_scripts_Planning_your_testing_Pc", [
          [ "Separating tests into modules", "page__programmers__guide__p_c.html#subsubsec_Test_scripts_Planning_your_testing_Separating_tests_into_modules_Pc", null ],
          [ "Consistent start point for tests", "page__programmers__guide__p_c.html#subsubsec_Test_scripts_Planning_your_testing_Consistent_start_point_for_tests_Pc", null ],
          [ "Libraries for common code", "page__programmers__guide__p_c.html#subsubsec_Test_scripts_Planning_your_testing_Libraries_for_common_code_Pc", null ],
          [ "Country/application independence", "page__programmers__guide__p_c.html#subsubsec_Test_scripts_Planning_your_testing_Country_application_independence_Pc", null ],
          [ "Recovering from failures", "page__programmers__guide__p_c.html#subsubsec_Test_scripts_Planning_your_testing_Recovering_from_failures_Pc", null ]
        ] ],
        [ "Test infrastructure", "page__programmers__guide__p_c.html#subsec_Test_scripts_Test_infrastructure_Pc", null ],
        [ "Writing your first script", "page__programmers__guide__p_c.html#subsec_Test_scripts_Writing_your_first_script_Pc", [
          [ "Standalone test script", "page__programmers__guide__p_c.html#subsubsec_Test_scripts_Writing_your_first_script_Standalone_test_script_Pc", null ],
          [ "Terminal test script", "page__programmers__guide__p_c.html#subsubsec_Test_scripts_Writing_your_first_script_Terminal_test_script_Pc", null ],
          [ "Example VATS demo applications test scripts", "page__programmers__guide__p_c.html#subsubsec_Test_scripts_Writing_your_first_script_Example_VATS_demo_applications_test_scripts_Pc", null ]
        ] ]
      ] ],
      [ "Python functions overview", "page__programmers__guide__p_c.html#sec_Python_functions_overview_Pc", [
        [ "atsunit.py", "page__programmers__guide__p_c.html#subsec_Python_functions_overview_atsunit_py_Pc", [
          [ "add_test", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_atsunit_py_add_test_Pc", null ],
          [ "check", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_atsunit_py_check_Pc", null ],
          [ "default_recovery_function", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_atsunit_py_default_recovery_function_Pc", null ],
          [ "log_error", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_atsunit_py_log_error_Pc", null ],
          [ "log_info", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_atsunit_py_log_info_Pc", null ],
          [ "log_warning", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_atsunit_py_log_warning_Pc", null ],
          [ "recover", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_atsunit_py_recover_Pc", null ],
          [ "recover_unless", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_atsunit_py_recover_unless_Pc", null ],
          [ "run", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_atsunit_py_run_Pc", null ],
          [ "set_recovery_function", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_atsunit_py_set_recovery_function_Pc", null ],
          [ "set_script_summary", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_atsunit_py_set_script_summary_Pc", null ],
          [ "set_version", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_atsunit_py_set_version_Pc", null ]
        ] ],
        [ "user.py", "page__programmers__guide__p_c.html#subsec_Python_functions_overview_user_py_Pc", [
          [ "adjust_info_date_time", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_adjust_info_date_time_Pc", null ],
          [ "clear_custom_event", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_clear_custom_event_Pc", null ],
          [ "configure_text_printer", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_configure_text_printer_Pc", null ],
          [ "debug", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_debug_Pc", null ],
          [ "disengage_hybrid_card", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_disengage_hybrid_card_Pc", null ],
          [ "does_display_contain", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_does_display_contain_Pc", null ],
          [ "does_printout_contain", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_does_printout_contain_Pc", null ],
          [ "does_string_contain", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_does_string_contain_Pc", null ],
          [ "engage_hybrid_card", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_engage_hybrid_card_Pc", null ],
          [ "enter_keys", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_enter_keys_Pc", null ],
          [ "get_card_data", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_card_data_Pc", null ],
          [ "get_custom_event", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_custom_event_Pc", null ],
          [ "get_display", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_display_Pc", null ],
          [ "get_emv_ct_tag", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_emv_ct_tag_Pc", null ],
          [ "get_emv_ctls_tag", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_get_emv_ctls_tag_Pc", null ],
          [ "get_EventDateTime", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_EventDateTime_Pc", null ],
          [ "get_hold_time", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_hold_time_Pc", null ],
          [ "get_hybrid_insertion_state", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_hybrid_insertion_state_Pc", null ],
          [ "get_hybrid_mag_flag", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_hybrid_mag_flag_Pc", null ],
          [ "get_hybrid_smart_flag", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_hybrid_smart_flag_Pc", null ],
          [ "get_hybrid_track_buffer", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_hybrid_track_buffer_Pc", null ],
          [ "get_keyboard_buffer", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_keyboard_buffer_Pc", null ],
          [ "get_printout", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_printout_Pc", null ],
          [ "get_reference_time", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_reference_time_Pc", null ],
          [ "get_response_time", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_response_time_Pc", null ],
          [ "get_smartcard_insertion_state", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_smartcard_insertion_state_Pc", null ],
          [ "get_tearbar_lines", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_tearbar_lines_Pc", null ],
          [ "get_terminal_model_info", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_terminal_model_info_Pc", null ],
          [ "get_terminal_version_info", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_terminal_version_info_Pc", null ],
          [ "get_trace_channel", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_trace_channel_Pc", null ],
          [ "get_trace_data", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_trace_data_Pc", null ],
          [ "get_trace_entries", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_trace_entries_Pc", null ],
          [ "get_trace_hex", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_trace_hex_Pc", null ],
          [ "get_trace_reference_time", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_trace_reference_time_Pc", null ],
          [ "get_trace_size", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_trace_size_Pc", null ],
          [ "get_trace_source", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_trace_source_Pc", null ],
          [ "get_trace_string", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_trace_string_Pc", null ],
          [ "get_track_data", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_track_data_Pc", null ],
          [ "get_VatsBuzzerRecord", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_VatsBuzzerRecord_Pc", null ],
          [ "get_VatsBuzzerRecordIdxNext", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_VatsBuzzerRecordIdxNext_Pc", null ],
          [ "get_VatsNumberOfBuzzerRecords", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_VatsNumberOfBuzzerRecords_Pc", null ],
          [ "get_working_directory", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_get_working_directory_Pc", null ],
          [ "init_VatsBuzzerRecording", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_init_VatsBuzzerRecording_Pc", null ],
          [ "insert_hybrid_card", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_insert_hybrid_card_Pc", null ],
          [ "insert_smartcard", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_insert_smartcard_Pc", null ],
          [ "is_touch_display", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_is_touch_display_Pc", null ],
          [ "remove_hybrid_card", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_remove_hybrid_card_Pc", null ],
          [ "remove_smartcard", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_remove_smartcard_Pc", null ],
          [ "reset_com", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_reset_com_Pc", null ],
          [ "reset_peripherals", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_reset_peripherals_Pc", null ],
          [ "restart_terminal", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_restart_terminal_Pc", null ],
          [ "send_emv_ct_data", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_send_emv_ct_data_Pc", null ],
          [ "send_emv_ctls_data", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_send_emv_ctls_data_Pc", null ],
          [ "send_emv_tag", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_send_emv_tag_Pc", null ],
          [ "setDisplayDimensions", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_setDisplayDimensions_Pc", null ],
          [ "setTerminalSpecificValues", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_setTerminalSpecificValues_Pc", null ],
          [ "set_com_con_result", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_set_com_con_result_Pc", null ],
          [ "set_com_event", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_set_com_event_Pc", null ],
          [ "set_com_net_result", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_set_com_net_result_Pc", null ],
          [ "set_custom_event", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_set_custom_event_Pc", null ],
          [ "set_DateAndTime", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_set_DateAndTime_Pc", null ],
          [ "set_DisplayCodePage", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_set_DisplayCodePage_Pc", null ],
          [ "set_display_region", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_set_display_region_Pc", null ],
          [ "set_emv_ct_tag", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_set_emv_ct_tag_Pc", null ],
          [ "set_emv_ctls_tag", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_set_emv_ctls_tag_Pc", null ],
          [ "set_event_debug", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_set_event_debug_Pc", null ],
          [ "set_event_debug_level", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_set_event_debug_level_Pc", null ],
          [ "set_hold_time", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_set_hold_time_Pc", null ],
          [ "set_inactivity_timeout", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_set_inactivity_timeout_Pc", null ],
          [ "set_keyboard_mapping", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_set_keyboard_mapping_Pc", null ],
          [ "set_response_time", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_set_response_time_Pc", null ],
          [ "set_script_debug", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_set_script_debug_Pc", null ],
          [ "set_sleep_time", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_set_sleep_time_Pc", null ],
          [ "set_tearbar_lines", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_set_tearbar_lines_Pc", null ],
          [ "set_terminal_debug", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_set_terminal_debug_Pc", null ],
          [ "set_text_printer_state", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_set_text_printer_state_Pc", null ],
          [ "set_user_debug", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_set_user_debug_Pc", null ],
          [ "stop_VatsBuzzerRecording", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_stop_VatsBuzzerRecording_Pc", null ],
          [ "swipe_magcard", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_swipe_magcard_Pc", null ],
          [ "SysMode_button_for_application", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_SysMode_button_for_application_Pc", null ],
          [ "take_terminal_display_snapshot", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_take_terminal_display_snapshot_Pc", null ],
          [ "tearoff_printout", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_tearoff_printout_Pc", null ],
          [ "touchCoordinate", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_touchCoordinate_Pc", null ],
          [ "touchRelease", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_touchRelease_Pc", null ],
          [ "trace_clear", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_trace_clear_Pc", null ],
          [ "trace_control", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_trace_control_Pc", null ],
          [ "trace_record", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_trace_record_Pc", null ],
          [ "trace_reset", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_trace_reset_Pc", null ],
          [ "trace_resume", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_trace_resume_Pc", null ],
          [ "trace_stop", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_trace_stop_Pc", null ],
          [ "wait", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_wait_Pc", null ],
          [ "wait_display_contains", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_wait_display_contains_Pc", null ],
          [ "wait_display_contains_info", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_wait_display_contains_info_Pc", null ],
          [ "wait_display_update", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_wait_display_update_Pc", null ],
          [ "wait_event", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_wait_event_Pc", null ],
          [ "wait_printout_contains", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_wait_printout_contains_Pc", null ],
          [ "wait_printout_update", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_user_py_wait_printout_update_Pc", null ]
        ] ],
        [ "vats_html.py", "page__programmers__guide__p_c.html#subsec_Python_functions_overview_vats_html_py_Pc", [
          [ "get_gui_menu_item_action", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_html_py_get_gui_menu_item_action_Pc", null ],
          [ "get_gui_menu_name", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_html_py_get_gui_menu_name_Pc", null ],
          [ "get_HtmlDisplay", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_html_py_get_HtmlDisplay_Pc", null ],
          [ "get_HtmlDisplayRegions", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_html_py_get_HtmlDisplayRegions_Pc", null ],
          [ "get_HtmlPrint", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_html_py_get_HtmlPrint_Pc", null ],
          [ "html_doesDisplayContain", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_html_py_html_doesDisplayContain_Pc", null ],
          [ "html_doesPrintoutContain", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_html_py_html_doesPrintoutContain_Pc", null ],
          [ "html_gui_getButtonList", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_html_py_html_gui_getButtonList_Pc", null ],
          [ "html_gui_getImageList", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_html_py_html_gui_getImageList_Pc", null ],
          [ "html_gui_getInputList", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_html_py_html_gui_getInputList_Pc", null ],
          [ "html_gui_getMenuList", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_html_py_html_gui_getMenuList_Pc", null ],
          [ "html_gui_getPlainText", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_html_py_html_gui_getPlainText_Pc", null ],
          [ "html_gui_pressButton", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_html_py_html_gui_pressButton_Pc", null ],
          [ "html_gui_selectMenu", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_html_py_html_gui_selectMenu_Pc", null ],
          [ "html_gui_setInput", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_html_py_html_gui_setInput_Pc", null ],
          [ "html_prt_getImageList", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_html_py_html_prt_getImageList_Pc", null ],
          [ "html_prt_getPlainText", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_html_py_html_prt_getPlainText_Pc", null ],
          [ "html_waitDisplayContains", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_html_py_html_waitDisplayContains_Pc", null ],
          [ "html_waitDisplayUpdate", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_html_py_html_waitDisplayUpdate_Pc", null ],
          [ "html_waitPrintoutContains", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_html_py_html_waitPrintoutContains_Pc", null ],
          [ "html_waitPrintoutUpdate", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_html_py_html_waitPrintoutUpdate_Pc", null ],
          [ "json_gui_getString", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_html_py_json_gui_getString_Pc", null ],
          [ "json_prt_getString", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_html_py_json_prt_getString_Pc", null ],
          [ "reset_gui_known_Apps", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_html_py_reset_gui_known_Apps_Pc", null ],
          [ "set_HtmlDisplayResult", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_html_py_set_HtmlDisplayResult_Pc", null ],
          [ "set_HtmlPrintResult", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_html_py_set_HtmlPrintResult_Pc", null ],
          [ "take_terminal_display_snapshot", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_html_py_take_terminal_display_snapshot_Pc", null ]
        ] ],
        [ "vats_collis.py", "page__programmers__guide__p_c.html#subsec_Python_functions_overview_vats_collis_py_Pc", [
          [ "constructor", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_collis_py_constructor_Pc", null ],
          [ "collis_connect", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_collis_py_collis_connect_Pc", null ],
          [ "collis_disconnect", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_collis_py_collis_disconnect_Pc", null ],
          [ "collis_select_image", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_collis_py_collis_select_image_Pc", null ],
          [ "collis_start_simulation", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_collis_py_collis_start_simulation_Pc", null ],
          [ "collis_stop_simulation", "page__programmers__guide__p_c.html#subsubsec_Python_functions_overview_vats_collis_py_collis_stop_simulation_Pc", null ]
        ] ]
      ] ],
      [ "Test focus", "page__programmers__guide__p_c.html#sec_Test_focus_Pc", [
        [ "ADK-EMV component", "page__programmers__guide__p_c.html#subsec_Test_focus_ADK_EMV_component_Pc", [
          [ "ADK-EMV CT transactions", "page__programmers__guide__p_c.html#subsubsec_Test_focus_ADK_EMV_component_EMV_CT_transactions_Pc", null ],
          [ "ADK-EMV CTLS transactions", "page__programmers__guide__p_c.html#subsubsec_Test_focus_ADK_EMV_component_EMV_CTLS_transactions_Pc", null ],
          [ "ADK-EMV VATS Recording Mode", "page__programmers__guide__p_c.html#subsubsec_Test_focus_ADK_EMV_component_EMV_VATS_Recording_Mode_Pc", null ],
          [ "EMV VATS fetch tags", "page__programmers__guide__p_c.html#subsubsec_Test_focus_ADK_EMV_component_EMV_VATS_fetch_tags_Pc", null ]
        ] ],
        [ "ADK-GUI component", "page__programmers__guide__p_c.html#subsec_Test_focus_ADK_GUI_component_Pc", [
          [ "Using touch events to trigger buttons or menu items", "page__programmers__guide__p_c.html#subsubsec_Test_focus_ADK_GUI_component_Using_touch_events_to_trigger_buttons_or_menu_items_Pc", null ],
          [ "Using access keys to trigger buttons", "page__programmers__guide__p_c.html#subsubsec_Test_focus_ADK_GUI_component_Using_access_keys_to_trigger_buttons_Pc", null ],
          [ "Taking terminal display snapshots", "page__programmers__guide__p_c.html#subsubsec_Test_focus_ADK_GUI_component_Taking_terminal_display_snapshots_Pc", null ],
          [ "Example for comparing terminal display snapshots", "page__programmers__guide__p_c.html#subsubsec_Test_focus_ADK_GUI_component_Example_for_comparing_terminal_display_snapshots_Pc", null ],
          [ "region_path", "page__programmers__guide__p_c.html#subsubsec_Test_focus_ADK_GUI_component_region_path_Pc", null ]
        ] ],
        [ "CP Trigger Points", "page__programmers__guide__p_c.html#subsec_Test_focus_CP_Trigger_Points_Pc", null ]
      ] ],
      [ "Unattended tests", "page__programmers__guide__p_c.html#sec_Unattended_tests_Pc", [
        [ "Initial situation", "page__programmers__guide__p_c.html#subsec_Unattended_tests_Initial_situation_Pc", null ],
        [ "CLI call of TestManager in silent mode", "page__programmers__guide__p_c.html#subsec_Unattended_tests_CLI_call_of_TestManager_in_silent_mode_Pc", null ],
        [ "Modifying the example ini file", "page__programmers__guide__p_c.html#subsec_Unattended_tests_Modifying_the_example_ini_file_Pc", null ],
        [ "Performing tests on several terminals simultaneously", "page__programmers__guide__p_c.html#subsec_Unattended_tests_Performing_tests_on_several_terminals_simultaneously_Pc", [
          [ "How it works", "page__programmers__guide__p_c.html#subsubsec_Unattended_tests_Performing_tests_on_several_terminals_simultaneously_How_it_works_Pc", null ],
          [ "Known limitations", "page__programmers__guide__p_c.html#subsubsec_Unattended_tests_Performing_tests_on_several_terminals_simultaneously_Known_limitations_Pc", null ],
          [ "What to do", "page__programmers__guide__p_c.html#subsubsec_Unattended_tests_Performing_tests_on_several_terminals_simultaneously_What_to_do_Pc", null ]
        ] ],
        [ "Jenkins helper scripts", "page__programmers__guide__p_c.html#subsec_Unattended_tests_Jenkins_helper_scripts_Pc", [
          [ "vatslog2junitxml.py", "page__programmers__guide__p_c.html#subsec_Unattended_tests_Jenkins_helper_scripts_vatslog2junitxml_py_Pc", null ],
          [ "km4log2xml.py", "page__programmers__guide__p_c.html#subsec_Unattended_tests_Jenkins_helper_scripts_km4log2xml_py_Pc", null ],
          [ "wait.py", "page__programmers__guide__p_c.html#subsec_Unattended_tests_Jenkins_helper_scripts_wait_py_Pc", null ],
          [ "modify_test_manager_ini_file.py", "page__programmers__guide__p_c.html#subsec_Unattended_tests_Jenkins_helper_scripts_modify_test_manager_ini_file_py_Pc", null ]
        ] ],
        [ "TestManager CLI call variants", "page__programmers__guide__p_c.html#subsec_Unattended_tests_TestManager_CLI_call_variants_Pc", null ],
        [ "No ini file", "page__programmers__guide__p_c.html#subsec_Unattended_tests_TestManager_CLI_call_variants_No_ini_file_Pc", null ],
        [ "Change the communication type", "page__programmers__guide__p_c.html#subsec_Unattended_tests_TestManager_CLI_call_variants_No_ini_file_Change_the_communication_type_Pc", null ]
      ] ],
      [ "Debugging scripts and troubleshooting", "page__programmers__guide__p_c.html#sec_Debugging_scripts_and_troubleshooting_Pc", [
        [ "Debugging test scripts", "page__programmers__guide__p_c.html#subsec_Debugging_scripts_and_troubleshooting_Debugging_test_scripts_Pc", [
          [ "Eclipse with PyDev", "page__programmers__guide__p_c.html#subsubsec_Debugging_scripts_and_troubleshooting_Debugging_test_scripts_Eclipse_with_PyDev_Pc", null ],
          [ "Visual Studio with Python Tools for Visual Studio", "page__programmers__guide__p_c.html#subsubsec_Debugging_scripts_and_troubleshooting_Debugging_test_scripts_Visual_Studio_with_Python_Tools_for_Visual_Studio_Pc", null ]
        ] ],
        [ "Troubleshooting", "page__programmers__guide__p_c.html#subsec_Debugging_scripts_and_troubleshooting_Troubleshooting_Pc", [
          [ "Problem: The TestManager is not able to connect to the terminal.", "page__programmers__guide__p_c.html#subsubsec_Debugging_scripts_and_troubleshooting_Troubleshooting_Problem_0_Pc", null ],
          [ "Problem: The Verix terminal seems not to be controllable by the test script, as none of the key press simulations forces the terminal application to react.", "page__programmers__guide__p_c.html#subsubsec_Debugging_scripts_and_troubleshooting_Troubleshooting_Problem_1_Pc", null ],
          [ "Problem: The DirectGUI is used by the terminal application, but both the VATS terminal viewer and the test script are not able to get the display output.", "page__programmers__guide__p_c.html#subsubsec_Debugging_scripts_and_troubleshooting_Troubleshooting_Problem_2_Pc", null ],
          [ "Problem: The HTMLPrinter is used by the terminal application, but both the VATS terminal viewer and the test script are not able to get the printer output.", "page__programmers__guide__p_c.html#subsubsec_Debugging_scripts_and_troubleshooting_Troubleshooting_Problem_3_Pc", null ],
          [ "Problem: The test script states sometimes an error, but the DUT seems to be working correctly.", "page__programmers__guide__p_c.html#subsubsec_Debugging_scripts_and_troubleshooting_Troubleshooting_Problem_4_Pc", null ],
          [ "Problem: The DirectGUI is able to show unicode characters e.g. in the menu item title. How do I select such a menu item?", "page__programmers__guide__p_c.html#subsubsec_Debugging_scripts_and_troubleshooting_Troubleshooting_Problem_5_Pc", null ],
          [ "Problem: VATS on a Verix terminal seems to be sending unexpected text display outputs in the case the applications (resp. ADK-GUI lib for graphical display output) sends the text display directly without using the DQ driver for display output.", "page__programmers__guide__p_c.html#subsubsec_Debugging_scripts_and_troubleshooting_Troubleshooting_Problem_6_Pc", null ],
          [ "Problem: The test script crashes while waiting for a text display output with special characters.", "page__programmers__guide__p_c.html#subsubsec_Debugging_scripts_and_troubleshooting_Troubleshooting_Problem_7_Pc", null ],
          [ "Problem: VATS seems not to support checkbox and radio button validation.", "page__programmers__guide__p_c.html#subsubsec_Debugging_scripts_and_troubleshooting_Troubleshooting_Problem_8_Pc", null ],
          [ "Problem: ADK-EMV CT tests seem not to be working on Verix terminals, especially on e315, e315M and e335 terminals.", "page__programmers__guide__p_c.html#subsubsec_Debugging_scripts_and_troubleshooting_Troubleshooting_Problem_9_Pc", null ],
          [ "Problem: Something in my new/changed Python test script does not work. I tried to locate the problem using print(), but the logs seem to lead to a wrong code line.", "page__programmers__guide__p_c.html#subsubsec_Debugging_scripts_and_troubleshooting_Troubleshooting_Problem_10_Pc", null ]
        ] ]
      ] ]
    ] ],
    [ "Programmers Guide - Terminal", "page__programmers__guide__terminal.html", [
      [ "Introduction", "page__programmers__guide__terminal.html#sec_Terminal_Introduction_Term", [
        [ "Purpose", "page__programmers__guide__terminal.html#subsec_Terminal_Purpose_Term", null ],
        [ "Target Audience", "page__programmers__guide__terminal.html#subsec_Terminal_Target_Audience_Term", null ],
        [ "System Specifications", "page__programmers__guide__terminal.html#subsec_Terminal_System_Specifications_Term", [
          [ "Hardware Support", "page__programmers__guide__terminal.html#subsubsec_Terminal_Hardware_Support_Term", null ],
          [ "Software Requirements", "page__programmers__guide__terminal.html#subsubsec_Terminal_Software_Requirements_Term", null ]
        ] ],
        [ "Restrictions / Open items", "page__programmers__guide__terminal.html#subsec_Terminal_Restrictions_Open_items_Term", [
          [ "Verix", "page__programmers__guide__terminal.html#subsec_Terminal_Restrictions_Open_items_Verix_Term", null ],
          [ "V/OS", "page__programmers__guide__terminal.html#subsec_Terminal_Restrictions_Open_items_VOS_Term", null ],
          [ "JIRA references", "page__programmers__guide__terminal.html#subsubsec_Jira_References_Term", null ],
          [ "Web references", "page__programmers__guide__terminal.html#subsubsec_Web_References_Term", null ],
          [ "Glossary", "page__programmers__guide__terminal.html#subsubsec_Glossary_Term", null ]
        ] ]
      ] ],
      [ "Overview", "page__programmers__guide__terminal.html#sec_Terminal_Overview_Term", [
        [ "Target Platforms", "page__programmers__guide__terminal.html#subsec_Terminal_Target_Platforms_Term", [
          [ "Running under V/OS", "page__programmers__guide__terminal.html#subsubsec_Terminal_Running_under_V_OS_Term", null ],
          [ "Running under Verix", "page__programmers__guide__terminal.html#subsubsec_Terminal_Running_under_Verix_Term", null ]
        ] ],
        [ "Deliveries", "page__programmers__guide__terminal.html#subsec_Terminal_Deliveries_Term", [
          [ "Headers and binary deliveries", "page__programmers__guide__terminal.html#subsubsec_Terminal_Headers_and_binary_deliveries_Term", null ],
          [ "VATS related Packages", "page__programmers__guide__terminal.html#subsubsec_Terminal_VATS_related_Packages_Term", null ],
          [ "Running with ADK-GUIPRT server for VATS", "page__programmers__guide__terminal.html#subsubsec_Terminal_Running_with_GUIPRT_server_for_VATS_Term", null ],
          [ "Running with ADK-GUIPRT library for VATS", "page__programmers__guide__terminal.html#subsubsec_Terminal_Running_with_GUIPRT_library_for_VATS_Term", null ],
          [ "File Location", "page__programmers__guide__terminal.html#subsubsec_Terminal_File_Location_Term", null ]
        ] ]
      ] ],
      [ "VATS Application Library", "page__programmers__guide__terminal.html#sec_Terminal_VATS_Application_Library_Term", null ],
      [ "VATS Handler", "page__programmers__guide__terminal.html#sec_Terminal_VATS_Handler_Term", [
        [ "VATS Handler Configuration File", "page__programmers__guide__terminal.html#subsec_Terminal_VATS_Handler_Configuration_File_Term", null ],
        [ "Verix specific settings of VATS Handler", "page__programmers__guide__terminal.html#subsec_Terminal_Verix_specific_settings_of_VATS_Handler_Term", null ],
        [ "V/OS specific settings of VATS Handler", "page__programmers__guide__terminal.html#subsec_Terminal_V_OS_specific_settings_of_VATS_Handler_Term", null ],
        [ "VATS Handler Start of application(s)", "page__programmers__guide__terminal.html#subsec_Terminal_VATS_Handler_Start_of_applications_Term", [
          [ "Verix", "page__programmers__guide__terminal.html#subsubsec_Terminal_VATS_Handler_Start_of_applications_Verix_Term", null ],
          [ "V/OS", "page__programmers__guide__terminal.html#subsubsec_Terminal_VATS_Handler_Start_of_applications_VOS_Term", null ]
        ] ],
        [ "VATS Handler event distribution", "page__programmers__guide__terminal.html#subsec_Terminal_VATS_Handler_event_distribution_Term", [
          [ "Verix", "page__programmers__guide__terminal.html#subsubsec_Terminal_VATS_Handler_event_distribution_Verix_Term", null ],
          [ "V/OS", "page__programmers__guide__terminal.html#subsubsec_Terminal_VATS_Handler_event_distribution_VOS_Term", null ]
        ] ],
        [ "ADKVATS-161/ADKVATS-222/ADKVATS-253: VERIX: integrate and test DQ driver functionality for \"printer support\"", "page__programmers__guide__terminal.html#subsec_Terminal_ADKVATS_161_ADKVATS_222_ADKVATS_253_VERIX_integrate_and_test_DQ_driver_functionality_for_printer_support_Term", null ],
        [ "ADKVATS-164: Add ability for applications to trigger VATS functions", "page__programmers__guide__terminal.html#subsec_Terminal_ADKVATS_164_Add_ability_for_applications_to_trigger_VATS_functions_Term", null ]
      ] ],
      [ "VATS Test Application", "page__programmers__guide__terminal.html#sec_Terminal_VATS_Test_Application_Term", [
        [ "Introduction", "page__programmers__guide__terminal.html#subsec_Terminal_Introduction_Term", null ],
        [ "Verix specific settings for Test Application", "page__programmers__guide__terminal.html#subsec_Terminal_Verix_specific_settings_for_Test_Application_Term", null ],
        [ "Functions of Test Applications", "page__programmers__guide__terminal.html#subsec_Terminal_Functions_of_Test_Applications_Term", null ]
      ] ],
      [ "Installation", "page__programmers__guide__terminal.html#sec_Terminal_Installation_Term", [
        [ "Installation of VATS on Verix", "page__programmers__guide__terminal.html#subsec_Terminal_Installation_of_VATS_on_Verix_Term", null ],
        [ "Installation of VATS on V/OS without VATSOS enabling package", "page__programmers__guide__terminal.html#subsec_Terminal_Installation_of_VATS_on_V_OS_without_VATSOS_enabling_package_Term", null ],
        [ "Installation of VATS on V/OS with VATSOS enabling package", "page__programmers__guide__terminal.html#subsec_Terminal_Installation_of_VATS_on_V_OS_with_VATSOS_enabling_package_Term", null ],
        [ "Installation of VATS on V/OS2 with VATSOS enabling package", "page__programmers__guide__terminal.html#subsec_Terminal_Installation_of_VATS_on_V_OS2_with_VATSOS_enabling_package_Term", null ]
      ] ],
      [ "VATS integration into ADK-COM", "page__programmers__guide__terminal.html#sec_Terminal_VATS_integration_into_ADK_COM_Term", [
        [ "ADK-COM VATS library variants", "page__programmers__guide__terminal.html#subsec_Terminal_COM_VATS_library_variants_Term", null ],
        [ "ADK-COM VATS daemon", "page__programmers__guide__terminal.html#subsec_Terminal_COM_VATS_daemon_Term", null ],
        [ "ADK-COM VATS packages", "page__programmers__guide__terminal.html#subsec_Terminal_COM_VATS_packages_Term", null ],
        [ "Building an application with VATS variants", "page__programmers__guide__terminal.html#subsec_Terminal_Building_an_application_with_VATS_variants_Term", null ],
        [ "Multiple applications", "page__programmers__guide__terminal.html#subsec_Terminal_Multiple_applications_Term", null ],
        [ "Restrictions and remarks", "page__programmers__guide__terminal.html#subsec_Terminal_Restrictions_and_remarks_Term", null ]
      ] ],
      [ "Automatic downloading and testing", "page__programmers__guide__terminal.html#sec_Terminal_Automatic_downloading_and_testing_Term", null ],
      [ "Notes on older VATS Releases", "page__programmers__guide__terminal.html#sec_Terminal_Notes_on_older_VATS_Releases_Term", null ],
      [ "Programmers API", "page__programmers__guide__terminal.html#sec_Terminal_Programmers_API_Term", [
        [ "Enumerations", "page__programmers__guide__terminal.html#subsec_Terminal_Enumerations_Term", null ],
        [ "Functions of VATS Library (relevant for applications !)", "page__programmers__guide__terminal.html#subsec_Terminal_Functions_of_VATS_Library_relevant_for_applications_Term", [
          [ "int AtsIsActive( void );", "page__programmers__guide__terminal.html#subsubsec_Terminal_int_AtsIsActive_void_Term", null ],
          [ "int AtsInit( const char *app_name, int taskId, int eventMask );", "page__programmers__guide__terminal.html#subsubsec_Terminal_int_AtsInit_const_char_app_name_int_taskId_int_eventMask_Term", null ],
          [ "int AtsInitExt( const char *app_name, int regEvents, int taskId, int eventMask );", "page__programmers__guide__terminal.html#subsubsec_Terminal_int_AtsInitExt_const_char_app_name_int_regEvents_int_taskId_int_eventMask_Term", null ],
          [ "int AtsInitVOS( const char *app_name, int regEvents );", "page__programmers__guide__terminal.html#subsubsec_Terminal_int_AtsInitVOS_const_char_app_name_int_regEvents_Term", null ],
          [ "int AtsSendEvent( unsigned short event, int datalen, const void *data );", "page__programmers__guide__terminal.html#subsubsec_Terminal_int_AtsSendEvent_unsigned_short_event_int_datalen_const_void_data_Term", null ],
          [ "int AtsSendDisplayText( int datalen, const void *data, unsigned char xpos, unsigned char ypos );", "page__programmers__guide__terminal.html#subsubsec_Terminal_int_AtsSendDisplayText_int_datalen_const_void_data_unsigned_char_xpos_unsigned_char_ypos_Term", null ],
          [ "int AtsTraceEvent( unsigned char source, unsigned char channel, int datalen, const void *data );", "page__programmers__guide__terminal.html#subsubsec_Terminal_int_AtsTraceEvent_unsigned_char_source_unsigned_char_channel_int_datalen_const_void_data_Term", null ],
          [ "int AtsReceiveEvent( enum AtsEvent *event, int timeout_msec, int *datalen, void *data );", "page__programmers__guide__terminal.html#subsubsec_Terminal_int_AtsReceiveEvent_enum_AtsEvent_event_int_timeout_msec_int_datalen_void_data_Term", null ],
          [ "int AtsRegisterForEvent( int eventMask )", "page__programmers__guide__terminal.html#subsubsec_Terminal_int_AtsRegisterForEvent_int_eventMask_Term", null ]
        ] ],
        [ "More Functions of VATS Library", "page__programmers__guide__terminal.html#subsec_Terminal_More_Functions_of_VATS_Library_Term", null ]
      ] ]
    ] ],
    [ "Data Structures", null, [
      [ "Data Structures", "annotated.html", "annotated" ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Functions", "globals_func.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"page__programmers__guide__p_c.html#subsubsec_Debugging_scripts_and_troubleshooting_Troubleshooting_Problem_5_Pc",
"page__programmers__guide__terminal.html#subsubsec_Terminal_int_AtsInitExt_const_char_app_name_int_regEvents_int_taskId_int_eventMask_Term"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';
var navTreeSubIndices = new Array();

function getData(varName)
{
  var i = varName.lastIndexOf('/');
  var n = i>=0 ? varName.substring(i+1) : varName;
  return eval(n.replace(/\-/g,'_'));
}

function stripPath(uri)
{
  return uri.substring(uri.lastIndexOf('/')+1);
}

function stripPath2(uri)
{
  var i = uri.lastIndexOf('/');
  var s = uri.substring(i+1);
  var m = uri.substring(0,i+1).match(/\/d\w\/d\w\w\/$/);
  return m ? uri.substring(i-6) : s;
}

function hashValue()
{
  return $(location).attr('hash').substring(1).replace(/[^\w\-]/g,'');
}

function hashUrl()
{
  return '#'+hashValue();
}

function pathName()
{
  return $(location).attr('pathname').replace(/[^-A-Za-z0-9+&@#/%?=~_|!:,.;\(\)]/g, '');
}

function localStorageSupported()
{
  try {
    return 'localStorage' in window && window['localStorage'] !== null && window.localStorage.getItem;
  }
  catch(e) {
    return false;
  }
}


function storeLink(link)
{
  if (!$("#nav-sync").hasClass('sync') && localStorageSupported()) {
      window.localStorage.setItem('navpath',link);
  }
}

function deleteLink()
{
  if (localStorageSupported()) {
    window.localStorage.setItem('navpath','');
  }
}

function cachedLink()
{
  if (localStorageSupported()) {
    return window.localStorage.getItem('navpath');
  } else {
    return '';
  }
}

function getScript(scriptName,func,show)
{
  var head = document.getElementsByTagName("head")[0]; 
  var script = document.createElement('script');
  script.id = scriptName;
  script.type = 'text/javascript';
  script.onload = func; 
  script.src = scriptName+'.js'; 
  if ($.browser.msie && $.browser.version<=8) { 
    // script.onload does not work with older versions of IE
    script.onreadystatechange = function() {
      if (script.readyState=='complete' || script.readyState=='loaded') { 
        func(); if (show) showRoot(); 
      }
    }
  }
  head.appendChild(script); 
}

function createIndent(o,domNode,node,level)
{
  var level=-1;
  var n = node;
  while (n.parentNode) { level++; n=n.parentNode; }
  if (node.childrenData) {
    var imgNode = document.createElement("img");
    imgNode.style.paddingLeft=(16*level).toString()+'px';
    imgNode.width  = 16;
    imgNode.height = 22;
    imgNode.border = 0;
    node.plus_img = imgNode;
    node.expandToggle = document.createElement("a");
    node.expandToggle.href = "javascript:void(0)";
    node.expandToggle.onclick = function() {
      if (node.expanded) {
        $(node.getChildrenUL()).slideUp("fast");
        node.plus_img.src = node.relpath+"ftv2pnode.png";
        node.expanded = false;
      } else {
        expandNode(o, node, false, false);
      }
    }
    node.expandToggle.appendChild(imgNode);
    domNode.appendChild(node.expandToggle);
    imgNode.src = node.relpath+"ftv2pnode.png";
  } else {
    var span = document.createElement("span");
    span.style.display = 'inline-block';
    span.style.width   = 16*(level+1)+'px';
    span.style.height  = '22px';
    span.innerHTML = '&#160;';
    domNode.appendChild(span);
  } 
}

var animationInProgress = false;

function gotoAnchor(anchor,aname,updateLocation)
{
  var pos, docContent = $('#doc-content');
  var ancParent = $(anchor.parent());
  if (ancParent.hasClass('memItemLeft') ||
      ancParent.hasClass('fieldname') ||
      ancParent.hasClass('fieldtype') ||
      ancParent.is(':header'))
  {
    pos = ancParent.position().top;
  } else if (anchor.position()) {
    pos = anchor.position().top;
  }
  if (pos) {
    var dist = Math.abs(Math.min(
               pos-docContent.offset().top,
               docContent[0].scrollHeight-
               docContent.height()-docContent.scrollTop()));
    animationInProgress=true;
    docContent.animate({
      scrollTop: pos + docContent.scrollTop() - docContent.offset().top
    },Math.max(50,Math.min(500,dist)),function(){
      if (updateLocation) window.location.href=aname;
      animationInProgress=false;
    });
  }
}

function newNode(o, po, text, link, childrenData, lastNode)
{
  var node = new Object();
  node.children = Array();
  node.childrenData = childrenData;
  node.depth = po.depth + 1;
  node.relpath = po.relpath;
  node.isLast = lastNode;

  node.li = document.createElement("li");
  po.getChildrenUL().appendChild(node.li);
  node.parentNode = po;

  node.itemDiv = document.createElement("div");
  node.itemDiv.className = "item";

  node.labelSpan = document.createElement("span");
  node.labelSpan.className = "label";

  createIndent(o,node.itemDiv,node,0);
  node.itemDiv.appendChild(node.labelSpan);
  node.li.appendChild(node.itemDiv);

  var a = document.createElement("a");
  node.labelSpan.appendChild(a);
  node.label = document.createTextNode(text);
  node.expanded = false;
  a.appendChild(node.label);
  if (link) {
    var url;
    if (link.substring(0,1)=='^') {
      url = link.substring(1);
      link = url;
    } else {
      url = node.relpath+link;
    }
    a.className = stripPath(link.replace('#',':'));
    if (link.indexOf('#')!=-1) {
      var aname = '#'+link.split('#')[1];
      var srcPage = stripPath(pathName());
      var targetPage = stripPath(link.split('#')[0]);
      a.href = srcPage!=targetPage ? url : "javascript:void(0)"; 
      a.onclick = function(){
        storeLink(link);
        if (!$(a).parent().parent().hasClass('selected'))
        {
          $('.item').removeClass('selected');
          $('.item').removeAttr('id');
          $(a).parent().parent().addClass('selected');
          $(a).parent().parent().attr('id','selected');
        }
        var anchor = $(aname);
        gotoAnchor(anchor,aname,true);
      };
    } else {
      a.href = url;
      a.onclick = function() { storeLink(link); }
    }
  } else {
    if (childrenData != null) 
    {
      a.className = "nolink";
      a.href = "javascript:void(0)";
      a.onclick = node.expandToggle.onclick;
    }
  }

  node.childrenUL = null;
  node.getChildrenUL = function() {
    if (!node.childrenUL) {
      node.childrenUL = document.createElement("ul");
      node.childrenUL.className = "children_ul";
      node.childrenUL.style.display = "none";
      node.li.appendChild(node.childrenUL);
    }
    return node.childrenUL;
  };

  return node;
}

function showRoot()
{
  var headerHeight = $("#top").height();
  var footerHeight = $("#nav-path").height();
  var windowHeight = $(window).height() - headerHeight - footerHeight;
  (function (){ // retry until we can scroll to the selected item
    try {
      var navtree=$('#nav-tree');
      navtree.scrollTo('#selected',0,{offset:-windowHeight/2});
    } catch (err) {
      setTimeout(arguments.callee, 0);
    }
  })();
}

function expandNode(o, node, imm, showRoot)
{
  if (node.childrenData && !node.expanded) {
    if (typeof(node.childrenData)==='string') {
      var varName    = node.childrenData;
      getScript(node.relpath+varName,function(){
        node.childrenData = getData(varName);
        expandNode(o, node, imm, showRoot);
      }, showRoot);
    } else {
      if (!node.childrenVisited) {
        getNode(o, node);
      } if (imm || ($.browser.msie && $.browser.version>8)) { 
        // somehow slideDown jumps to the start of tree for IE9 :-(
        $(node.getChildrenUL()).show();
      } else {
        $(node.getChildrenUL()).slideDown("fast");
      }
      if (node.isLast) {
        node.plus_img.src = node.relpath+"ftv2mlastnode.png";
      } else {
        node.plus_img.src = node.relpath+"ftv2mnode.png";
      }
      node.expanded = true;
    }
  }
}

function glowEffect(n,duration)
{
  n.addClass('glow').delay(duration).queue(function(next){
    $(this).removeClass('glow');next();
  });
}

function highlightAnchor()
{
  var aname = hashUrl();
  var anchor = $(aname);
  if (anchor.parent().attr('class')=='memItemLeft'){
    var rows = $('.memberdecls tr[class$="'+hashValue()+'"]');
    glowEffect(rows.children(),300); // member without details
  } else if (anchor.parent().attr('class')=='fieldname'){
    glowEffect(anchor.parent().parent(),1000); // enum value
  } else if (anchor.parent().attr('class')=='fieldtype'){
    glowEffect(anchor.parent().parent(),1000); // struct field
  } else if (anchor.parent().is(":header")) {
    glowEffect(anchor.parent(),1000); // section header
  } else {
    glowEffect(anchor.next(),1000); // normal member
  }
  gotoAnchor(anchor,aname,false);
}

function selectAndHighlight(hash,n)
{
  var a;
  if (hash) {
    var link=stripPath(pathName())+':'+hash.substring(1);
    a=$('.item a[class$="'+link+'"]');
  }
  if (a && a.length) {
    a.parent().parent().addClass('selected');
    a.parent().parent().attr('id','selected');
    highlightAnchor();
  } else if (n) {
    $(n.itemDiv).addClass('selected');
    $(n.itemDiv).attr('id','selected');
  }
  if ($('#nav-tree-contents .item:first').hasClass('selected')) {
    $('#nav-sync').css('top','30px');
  } else {
    $('#nav-sync').css('top','5px');
  }
  showRoot();
}

function showNode(o, node, index, hash)
{
  if (node && node.childrenData) {
    if (typeof(node.childrenData)==='string') {
      var varName    = node.childrenData;
      getScript(node.relpath+varName,function(){
        node.childrenData = getData(varName);
        showNode(o,node,index,hash);
      },true);
    } else {
      if (!node.childrenVisited) {
        getNode(o, node);
      }
      $(node.getChildrenUL()).css({'display':'block'});
      if (node.isLast) {
        node.plus_img.src = node.relpath+"ftv2mlastnode.png";
      } else {
        node.plus_img.src = node.relpath+"ftv2mnode.png";
      }
      node.expanded = true;
      var n = node.children[o.breadcrumbs[index]];
      if (index+1<o.breadcrumbs.length) {
        showNode(o,n,index+1,hash);
      } else {
        if (typeof(n.childrenData)==='string') {
          var varName = n.childrenData;
          getScript(n.relpath+varName,function(){
            n.childrenData = getData(varName);
            node.expanded=false;
            showNode(o,node,index,hash); // retry with child node expanded
          },true);
        } else {
          var rootBase = stripPath(o.toroot.replace(/\..+$/, ''));
          if (rootBase=="index" || rootBase=="pages" || rootBase=="search") {
            expandNode(o, n, true, true);
          }
          selectAndHighlight(hash,n);
        }
      }
    }
  } else {
    selectAndHighlight(hash);
  }
}

function removeToInsertLater(element) {
  var parentNode = element.parentNode;
  var nextSibling = element.nextSibling;
  parentNode.removeChild(element);
  return function() {
    if (nextSibling) {
      parentNode.insertBefore(element, nextSibling);
    } else {
      parentNode.appendChild(element);
    }
  };
}

function getNode(o, po)
{
  var insertFunction = removeToInsertLater(po.li);
  po.childrenVisited = true;
  var l = po.childrenData.length-1;
  for (var i in po.childrenData) {
    var nodeData = po.childrenData[i];
    po.children[i] = newNode(o, po, nodeData[0], nodeData[1], nodeData[2],
      i==l);
  }
  insertFunction();
}

function gotoNode(o,subIndex,root,hash,relpath)
{
  var nti = navTreeSubIndices[subIndex][root+hash];
  o.breadcrumbs = $.extend(true, [], nti ? nti : navTreeSubIndices[subIndex][root]);
  if (!o.breadcrumbs && root!=NAVTREE[0][1]) { // fallback: show index
    navTo(o,NAVTREE[0][1],"",relpath);
    $('.item').removeClass('selected');
    $('.item').removeAttr('id');
  }
  if (o.breadcrumbs) {
    o.breadcrumbs.unshift(0); // add 0 for root node
    showNode(o, o.node, 0, hash);
  }
}

function navTo(o,root,hash,relpath)
{
  var link = cachedLink();
  if (link) {
    var parts = link.split('#');
    root = parts[0];
    if (parts.length>1) hash = '#'+parts[1].replace(/[^\w\-]/g,'');
    else hash='';
  }
  if (hash.match(/^#l\d+$/)) {
    var anchor=$('a[name='+hash.substring(1)+']');
    glowEffect(anchor.parent(),1000); // line number
    hash=''; // strip line number anchors
  }
  var url=root+hash;
  var i=-1;
  while (NAVTREEINDEX[i+1]<=url) i++;
  if (i==-1) { i=0; root=NAVTREE[0][1]; } // fallback: show index
  if (navTreeSubIndices[i]) {
    gotoNode(o,i,root,hash,relpath)
  } else {
    getScript(relpath+'navtreeindex'+i,function(){
      navTreeSubIndices[i] = eval('NAVTREEINDEX'+i);
      if (navTreeSubIndices[i]) {
        gotoNode(o,i,root,hash,relpath);
      }
    },true);
  }
}

function showSyncOff(n,relpath)
{
    n.html('<img src="'+relpath+'sync_off.png" title="'+SYNCOFFMSG+'"/>');
}

function showSyncOn(n,relpath)
{
    n.html('<img src="'+relpath+'sync_on.png" title="'+SYNCONMSG+'"/>');
}

function toggleSyncButton(relpath)
{
  var navSync = $('#nav-sync');
  if (navSync.hasClass('sync')) {
    navSync.removeClass('sync');
    showSyncOff(navSync,relpath);
    storeLink(stripPath2(pathName())+hashUrl());
  } else {
    navSync.addClass('sync');
    showSyncOn(navSync,relpath);
    deleteLink();
  }
}

function initNavTree(toroot,relpath)
{
  var o = new Object();
  o.toroot = toroot;
  o.node = new Object();
  o.node.li = document.getElementById("nav-tree-contents");
  o.node.childrenData = NAVTREE;
  o.node.children = new Array();
  o.node.childrenUL = document.createElement("ul");
  o.node.getChildrenUL = function() { return o.node.childrenUL; };
  o.node.li.appendChild(o.node.childrenUL);
  o.node.depth = 0;
  o.node.relpath = relpath;
  o.node.expanded = false;
  o.node.isLast = true;
  o.node.plus_img = document.createElement("img");
  o.node.plus_img.src = relpath+"ftv2pnode.png";
  o.node.plus_img.width = 16;
  o.node.plus_img.height = 22;

  if (localStorageSupported()) {
    var navSync = $('#nav-sync');
    if (cachedLink()) {
      showSyncOff(navSync,relpath);
      navSync.removeClass('sync');
    } else {
      showSyncOn(navSync,relpath);
    }
    navSync.click(function(){ toggleSyncButton(relpath); });
  }

  $(window).load(function(){
    navTo(o,toroot,hashUrl(),relpath);
    showRoot();
  });

  $(window).bind('hashchange', function(){
     if (window.location.hash && window.location.hash.length>1){
       var a;
       if ($(location).attr('hash')){
         var clslink=stripPath(pathName())+':'+hashValue();
         a=$('.item a[class$="'+clslink.replace(/</g,'\\3c ')+'"]');
       }
       if (a==null || !$(a).parent().parent().hasClass('selected')){
         $('.item').removeClass('selected');
         $('.item').removeAttr('id');
       }
       var link=stripPath2(pathName());
       navTo(o,link,hashUrl(),relpath);
     } else if (!animationInProgress) {
       $('#doc-content').scrollTop(0);
       $('.item').removeClass('selected');
       $('.item').removeAttr('id');
       navTo(o,toroot,hashUrl(),relpath);
     }
  })
}

