var NAVTREE =
[
  [ "VWI-VAS-Kernel", "index.html", [
    [ "VWI VAS Kernel", "index.html", [
      [ "PREFACE", "index.html#sec_Preface", [
        [ "Audience", "index.html#subsec_Audience", null ],
        [ "Organization", "index.html#subsec_Organization", null ]
      ] ],
      [ "INTRODUCTION", "index.html#sec_Introduction", null ],
      [ "ARCHITECTURE", "index.html#sec_vwi_vas_kern_Architecture", null ],
      [ "VWI VAS KERNEL INTERFACE", "index.html#sec_vwi_vas_kern_interface", null ],
      [ "SW1 SW2", "index.html#sec_vwi_vas_sw1sw2", null ],
      [ "TYPES", "index.html#sec_vwi_vas_kern_types", [
        [ "List Of Known JSON Objects", "index.html#subsec_vwi_vas_json_objects", null ],
        [ "Service Types", "index.html#subsec_vwi_vas_kern_service_types", null ],
        [ "Service Issuer", "index.html#subsec_vwi_vas_kern_service_issuer", null ],
        [ "Expected Format", "index.html#subsec_vwi_vas_kern_expected_format", null ],
        [ "New Service Type", "index.html#subsec_vwi_vas_kern_new_service_type", null ],
        [ "Usage Status", "index.html#subsec_vwi_vas_kern_usage_status", null ],
        [ "Update Operation", "index.html#subsec_vwi_vas_kern_update_operation", null ]
      ] ],
      [ "VWI VAS APDU Message Format", "index.html#sec_vwi_vas_apdu_format", [
        [ "VWI VAS APDU - P2 extension", "index.html#subsec_vwi_vas_apdu_p2", null ]
      ] ],
      [ "VAS Wallet Configuration Parameters", "index.html#sec_vwi_vas_config_types", null ],
      [ "VAS Wallet PreLoad/Dynamic Configuration Parameters", "index.html#sec_vwi_vas_prl_dyn_conf_types", [
        [ "PreLoad/Dynamic jSON objects", "index.html#subsec_adk_nfc_vas_prl_dyn", null ]
      ] ],
      [ "VAS Wallet Read Configuration Parameters", "index.html#sec_vwi_vas_get_wallet_config_types", null ],
      [ "VAS Wallet Response Format", "index.html#sec_vwi_vas_response", null ],
      [ "Customer Data Functions", "index.html#sec_vwi_vas_customer_data_func", null ],
      [ "VAS Wallet JSON Examples", "index.html#sec_vwi_vas_json_examples", [
        [ "VAS Wallets Configuration", "index.html#subsec_adk_nfc_VAS_Wallets_Json_VASConfig", null ],
        [ "Read VAS Wallets Configuration", "index.html#subsec_adk_nfc_VAS_Wallets_Json_VASConfig_Read", null ],
        [ "Dynamic Configuration", "index.html#subsec_adk_nfc_VAS_Wallets_Json_DynamicConfig", null ],
        [ "PreLoad Configuration", "index.html#subsec_adk_nfc_VAS_Wallets_Json_PreLoadConfig", null ],
        [ "VAS Wallet Response", "index.html#subsec_adk_nfc_VAS_Wallets_Json_Response", null ]
      ] ],
      [ "VAS Wallet Flowchart", "index.html#sec_adk_nfc_VAS_Wallets_Flows", null ],
      [ "Main Flow", "index.html#subsec_adk_nfc_VAS_Wallets_Main_Flow", null ],
      [ "End/Completion Flow", "index.html#subsec_adk_nfc_VAS_Wallets_End_Flow", null ],
      [ "EXAMPLES", "index.html#sec_vwi_vas_kern_examples", null ],
      [ "KNOWN ISSUES", "index.html#sec_vwi_vas_kern_KnownIssues", null ],
      [ "HISTORY", "index.html#sec_vwi_vas_history", null ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"index.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';