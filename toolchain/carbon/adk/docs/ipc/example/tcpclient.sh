#!/bin/bash

argv0=$0

function usage {
   echo "Usage: $argv0 <ip-address> <port> [--loop <number>] [--delay <seconds] [--msg <string>] [--timeprint]"
   echo "Mandatory parameters:"
   echo "ip-address: ip address or hostname of the echo server"
   echo "port: port number of the echo server"
   echo "Optional parameters:"
   echo "--loop: number of echo requrests, default is 1, number<=0 means endless loop"
   echo "--delay: delay time between echo requests in seconds, default is 0"
   echo "         Examples: \"--delay 1\" means 1 second, \"--delay 1.5\" means 1500 ms"
   echo "--msg: string used for the echo message, default is \"Hello World\""
   echo "--timeprint: print out time that was required for echo request and response"
   exit 1
}

function clean_up {
	# housekeeping
	if [ -f $timefile ]; then rm $timefile; fi
  trap 0  # reset to default action
	exit
}

if [ $# -lt 2 ]; then usage; fi

ip=$1
port=$2
shift 2

loop=1
delay=0
msg="Hello World"
timeprint=0

while [ $# -gt 0 -a "${1:0:2}" = "--" ] ; do
    case "$1" in
        --loop)
            if [ $# -lt 2 ]; then usage; fi
            loop="$2"
            # this makes the endless loop
            if [ $loop -eq 0 ]; then loop=-1; fi
            shift 2
            ;;
        --delay)
            if [ $# -lt 2 ]; then usage; fi
            delay="$2"
            shift 2
            ;;
        --msg)
            if [ $# -lt 2 ]; then usage; fi
            msg="$2"
            shift 2
            ;;
        --timeprint)
            timefile=$(mktemp)
            trap clean_up 0 1 2 3 15
            shift 1
            ;;
        *)
            usage
            ;;
    esac
done

while true; do
  echo "echo $msg | nc $ip $port" 
  if [ -z $timefile ]; then
    echo $msg | nc $ip $port    
  else
    (time echo $msg | nc $ip $port) 2>$timefile
    cat $timefile | grep real | awk '{print $2}'
  fi
  
  # break if loop counter is down
  if [ $loop -gt 0 ]; then
    loop=$(($loop-1))
    if [ $loop -eq 0 ]; then break; fi
    sleep $delay
  fi
done
