#!/bin/bash

# exit in case of errors:
set -e 

if [ $# != 1 ] ; then
   echo "Usage: $0 <app-dir>"
   exit 1
fi

echo "CP Signer"
echo "--------------------------"


script_path=$(cd $(dirname $0); pwd)

bin_signer=$script_path/vfisigner
ziptool=zip

appdir="$1"
tempdir="$appdir/pkg"
appname=$(cd "$appdir/mp"; ls)

file_mpmanifest="$appdir/mp/$appname/mpmanifest.mft"
file_confowner="$appdir/mp/$appname/confOwner.json"
file_appuninstall="$appdir/mp/$appname/appuninstall.mft"

# echo input dir: %appdir%
# echo app name : %appname%
# echo script_path: %script_path%
# echo --------------------------
cat "$file_mpmanifest"
cat "$file_appuninstall"
echo "--------------------------"

# **** a few checks and warnings
if [ ! -e "$appdir/mp/$appname" ] ; then
    echo "ERROR: marketplace (mp) files folder $appdir/mp/$appname does not exist."
    exit 1
fi

if [ ! -e "$appdir/www/$appname" ] ; then
    echo "ERROR: application files (www) folder $appdir/www/$appname does not exist."
    exit 1
fi

if [ ! -e "$file_mpmanifest" ] ; then
    echo "ERROR: marketplace manifest $file_mpmanifest does not exist."
    exit 1
fi

if [ ! -e "file_confowner" ] ; then 
    echo "WARNING: no confOwner.json found"
fi

if [ ! -e "$file_appuninstall" ] ; then  
    echo "WARNING: no appuninstall.mft found"
fi

packaging() {
    # **** delete old tempdir recursively and create freshly
    rm -rf "$appdir/pkg/$arch" 2>/dev/null

    # **** create the application package
    echo   "create application package..."
    mkdir -p "$appdir/pkg/$arch/zip"
    (cd "$appdir/www"; "$ziptool" -r "../pkg/$arch/zip/$appname.zip" * ) >/dev/null 
    cp "$file_cert_approval" "$appdir/pkg/$arch/zip" 
    "$bin_signer" --cert "$file_cert_approval" --key "$file_key_approval" $appdir/pkg/$arch/zip/$appname.zip >/dev/null 2>&1
    mv "$appdir/pkg/$arch/zip/$appname.zip.p7s" "$appdir/pkg/$arch/zip/$appname.p7s" 
    (cd "$appdir/pkg/$arch/zip"; "$ziptool" -r "../pkg-$appname.zip" * ) >/dev/null 2>&1
    mkdir -p "$appdir/pkg/$arch/all"
    (cd "$appdir/pkg/$arch/zip"; mv * ../all)
    rm -rf "$appdir/pkg/$arch/zip" 

    # **** create the MP manifest package
    echo   "create marketplace package..."
    mkdir -p "$appdir/pkg/$arch/zip"
    mkdir -p "$appdir/pkg/$arch/files/$appname"
    if [ -e "$file_confowner" ]; then
        cp "$file_confowner"  "$appdir/pkg/$arch/files/$appname"
    fi
    cp "$file_mpmanifest" "$appdir/pkg/$arch/files/$appname"
    (cd "$appdir/pkg/$arch/files"; "$ziptool" -r "../zip/$appname-o.zip" * ) >/dev/null 2>&1
    cp "$file_cert_manifest" "$appdir/pkg/$arch/zip"
    "$bin_signer" --cert "$file_cert_manifest" --key "$file_key_manifest" $appdir/pkg/$arch/zip/$appname-o.zip >/dev/null 2>&1
    mv "$appdir/pkg/$arch/zip/$appname-o.zip.p7s" "$appdir/pkg/$arch/zip/$appname-o.p7s" 
    (cd "$appdir/pkg/$arch/zip"; "$ziptool" -r "../pkg-$appname-o.zip" *)  >/dev/null 2>&1
    (cd "$appdir/pkg/$arch/zip"; mv * ../all)
    rm -rf "$appdir/pkg/$arch/files"
    rm -rf "$appdir/pkg/$arch/zip"

    # **** create the combination installation package
    echo   "create combined installation package..."
    (cd "$appdir/pkg/$arch/all"; "$ziptool" -r "../pkg-$appname-inst.zip" * ) >/dev/null 2>&1
    rm -rf "$appdir/pkg/$arch/all"

    # **** create the uninstall packages
    echo   "create uninstall package..."
    if [ -e "$file_appuninstall" ] ; then
        mkdir -p "$appdir/pkg/$arch/zip"
        mkdir -p "$appdir/pkg/$arch/files/$appname"
        cp "$file_appuninstall" "$appdir/pkg/$arch/files/$appname" 
        (cd "$appdir/pkg/$arch/files"; "$ziptool" -r "../zip/$appname-u.zip" * ) >/dev/null 2>&1
        rm -rf "$appdir/pkg/$arch/files"
        cp "$file_cert_manifest" "$appdir/pkg/$arch/zip"
        "$bin_signer" --cert "$file_cert_manifest" --key "$file_key_manifest" $appdir/pkg/$arch/zip/$appname-u.zip >/dev/null 2>&1
        mv "$appdir/pkg/$arch/zip/$appname-u.zip.p7s" "$appdir/pkg/$arch/zip/$appname-u.p7s"
        (cd "$appdir/pkg/$arch/zip"; "$ziptool" -r "../pkg-$appname-u.zip" *) >/dev/null 2>&1
        rm -rf "$appdir/pkg/$arch/zip"
    else
        echo "WARNING: Skipping uninstall package creation."
    fi
}


echo "VERIX-CPDEV"
file_cert_approval="$script_path/vs208928-cpapproval.crt"
file_cert_manifest="$script_path/vs208937-cpmanifest.crt"
file_key_approval="$script_path/vs208928-cpapproval.key"
file_key_manifest="$script_path/vs208937-cpmanifest.key"
arch="vrx-cpdev"
echo "output: $appdir/pkg/$arch"
packaging

echo "VOS-OSDEV"
file_cert_approval="$script_path/vs200047-sys14signer.crt"
file_cert_manifest="$script_path/vs200046-sys11signer.crt"
file_key_approval="$script_path/vs200047-sys14signer.key"
file_key_manifest="$script_path/vs200046-sys11signer.key"
arch="vos-osdev"
echo "output: $appdir/pkg/$arch"
packaging

echo VOS-CPDEV
file_cert_approval="$script_path/vs208730-sys14signer.crt"
file_cert_manifest="$script_path/vs208729-sys11signer.crt"
file_key_approval="$script_path/vs208730-sys14signer.key"
file_key_manifest="$script_path/vs208729-sys11signer.key"
arch="vos-cpdev"
echo "output: $appdir/pkg/$arch"
packaging

