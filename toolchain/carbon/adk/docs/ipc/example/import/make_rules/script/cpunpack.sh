#!/bin/bash

# exit in case of errors:
set -e 

if [ $# != 2 ] ; then
   echo "Usage: $0 <app-inst-package> <app-uninst-package>"
   exit 1
fi

tempdir=`mktemp -d unpack.XXXXXXXX`

unzip -d $tempdir $2
unzip -o -d $tempdir $1

for i in $tempdir/*.zip; do

filename=${i##*/}
appname=${filename%.zip}
appname=${appname%-o}
appname=${appname%-u}

if [ "$appname.zip" = "$filename" ] ; then
    mkdir -p $appname/www
    unzip -o -d $appname/www $i
else 
    mkdir -p $appname/mp
    unzip -o -d $appname/mp $i
fi
done

rm -rf $tempdir

