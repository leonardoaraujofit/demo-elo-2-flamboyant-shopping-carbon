#include "ipc/ipc.h"
#include "ipc/jsobject.h"
#include <vector>
#include <pthread.h>
#include <unistd.h>
#include <string.h>

#ifdef _VRXEVO
#include "vrx_printf.h"
// initialization of printf() on Verix
#define STRINGIZE(x) #x
#define STRINGIZE_VALUE_OF(x) STRINGIZE(x)
class VrxLogInit {
public:
  // constructor
  VrxLogInit() { vfiipc::vrx_set_component_name(STRINGIZE_VALUE_OF(COMPONENT_NAME)); }
};
static VrxLogInit vrx_log_initializer;
#else
#include <stdio.h>
#endif

using namespace std;
using namespace vfiipc;

#define PREFIX 0x164D5347
#define SIZELIMIT (1024*1024)
#define USE_IPC_MESSAGES 0
#ifdef _VRXEVO
#  define PIPE_NAME "foobar"
#else
#  define PIPE_NAME "/tmp/foobar"
#endif
#define TCP_PORT 12345

///////////////////////////////////////////////////////////
//////////////////////// DEMO SERVER //////////////////////
///////////////////////////////////////////////////////////
#ifdef DEMO_SERVER

/* Set to 1 to build a single threaded server. 
 * Set to 0 to create a seperate thread for each new connection. */
#define SINGLE_THEADED_SERVER 0
/* Set to 1 to build a server reading one request and sending one response before the connection is closed.
 * Set to 0 to let the connection open and do a request/responce loop. */
#define SINGLE_SHOT_SERVER    0
/* Set to 1 to build a server using IPC callbacks signaling available data
 * Set to 0 to build a server that just calls of blocking read()/read_msg() with a timeout */
#define USE_CALLBACK_SERVER   0

//! [server_source]
static int response(IPC *r, int timeout_msec)
{
#if USE_IPC_MESSAGES==1 // use read_msg() and write_msg()
   vector<char> buf;
   buf.push_back(0);
   int id;
   bool res;
   res=r->read_msg(PREFIX,id,buf,SIZELIMIT,timeout_msec);
   if(!res) {
      if(r->error() || r->eof()) {
         printf("!!!!!!!!!!!!!!!! read_msg():error=%d,eof=%d !!!!!!!!!!!!!!!!\n",r->error(),r->eof());
         return -1;
      }
      printf("!!!!!!!!!!!!!!!!read_msg():TIMEOUT !!!!!!!!!!!!!!!!\n");
      return 0;
   }
   printf("read_msg() OK: %s\n",&buf[0]);
   if(!r->write_msg(PREFIX,id,buf)) {
      printf("!!!!!!!!!!!!!!!! write_msg():error=%d !!!!!!!!!!!!!!!!\n",r->error());
      return -1;
   }
#else // use low level read()/write()  -> this runs with test/client.sh
   char buf[256];
   int n=r->read(buf,sizeof(buf)-1,timeout_msec,0);
   if(n<=0) {
       if(r->error() || r->eof()) {
          printf("!!!!!!!!!!!!!!!! read():error=%d,eof=%d !!!!!!!!!!!!!!!!\n",r->error(),r->eof());
          return -1;
       }
       printf("!!!!!!!!!!!!!!!!read():TIMEOUT !!!!!!!!!!!!!!!!\n");
       return 0;
    }
    buf[n]=0;
    printf("read() OK: %s\n",buf);
    if(!r->write(buf,n)) {
       printf("!!!!!!!!!!!!!!!! write():error=%d !!!!!!!!!!!!!!!!\n",r->error());
       return -1;
    }
    return 1;
#endif
}

#if USE_CALLBACK_SERVER==1
static void ipc_callback(void *data, IPC *ipc)
{
  printf("ipc_callback() invoked: data available on IPC object %p\n",ipc);
  int *callback_done=(int *)data;
  int ret=response(ipc,0); // use a timeout of 0, since available data is notified by this callback
  if(ret<0) *callback_done=1; // leave loop on error
  if(SINGLE_SHOT_SERVER) *callback_done=1;
}
#endif

void *server(void *p)
{
   IPC *r=(IPC *)p;

#if USE_CALLBACK_SERVER==1 
   int callback_done=0;
   if(!r->set_callback(ipc_callback,(void *)&callback_done)) callback_done=1; // error
   while(!callback_done) sleep(1);
#else
   do
   {
     int ret=response(r,2000);
     if(ret<0) break; // leave loop on error
   }
   while(!SINGLE_SHOT_SERVER);
#endif
   delete r; // closes the session and does not require an explicit call of r->close()
   return 0;
}

int main()
{
   printf("******************* Demo-Server started *********************\n");
#ifdef USE_TCP
   TCP s;
   s.listen(TCP_PORT);
#else // pipe test
   Pipe s(Pipe::PC_EnableCredentials);
   s.listen(PIPE_NAME);
#endif
   while(1)
   {
      IPC *r=s.accept(); 
      if(!r) continue;
      printf("remote: %s\n",r->remote_addr());

      if(SINGLE_THEADED_SERVER) {
         printf("Single-threaded server\n");
         server(r);
      }
      else {
         printf("Multi-threaded server\n");
         pthread_t server_thread;
         pthread_attr_t attr;
         pthread_attr_init(&attr);
         pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
         pthread_attr_setstacksize(&attr,8*1024); // use 8 KB as default stack size for session threads
         if(!pthread_create(&server_thread,&attr,server,(void *)r)) {
            printf("New session thread with ipc=%lx successfully started\n",(long)r);
         } else {
            printf("!!!!!! Could NOT start session thread with ipc=%lx !!!!!!\n",(long)r);
         }
         pthread_attr_destroy(&attr);
      }
      // printf("main() server T%d: heap_current=%d,heap_max=%d,stack_max=%d\n",pthread_self(),_heap_current(),_heap_max(),_stack_max());
   }
   return 0;
}
//! [server_source]
#endif  // ifdef DEMO_SERVER

///////////////////////////////////////////////////////////
//////////////////////// DEMO CLIENT //////////////////////
///////////////////////////////////////////////////////////
#ifdef DEMO_CLIENT

/* Set to 1 to build a client sending one request and reading one response before the connection is closed. 
 * Set to 0 to let the connection open and do a request/responce loop. */
#define SINGLE_SHOT_CLIENT    1
/* Set to 1 to reconnect to the server to do a new request/responce, after connection was closed before. 
 * Set to 0 to terminate the client after first connection. */
#define SINGLE_CONNECT_CLIENT 0
/* Set to 1 to build a client using IPC callbacks signaling available data
 * Set to 0 to build a client that just calls of blocking read()/read_msg() with a timeout */
#define USE_CALLBACK_CLIENT   0

//! [client_source]
static int receive(IPC *r, int timeout_msec)
{
#if USE_IPC_MESSAGES==1 // use read_msg() and write_msg()
   vector<char> buf;
   buf.push_back(0);
   int id;
   bool res;
   res=r->read_msg(PREFIX,id,buf,SIZELIMIT,timeout_msec);
   if(!res) {
      if(r->error() || r->eof()) {
         printf("!!!!!!!!!!!!!!!! read_msg():error=%d,eof=%d !!!!!!!!!!!!!!!!\n",r->error(),r->eof());
         return -1;
      }
      printf("!!!!!!!!!!!!!!!! read_msg():TIMEOUT !!!!!!!!!!!!!!!!\n");
      return 0;
   }
   printf("read_msg() OK: %s\n",&buf[0]);
#else // use low level read()/write()
   char buf[256];
   int n=r->read(buf,sizeof(buf)-1,timeout_msec,0);
   if(n<=0) {
      if(r->error() || r->eof()) {
         printf("!!!!!!!!!!!!!!!! read():error=%d,eof=%d !!!!!!!!!!!!!!!!\n",r->error(),r->eof());
         return -1;
      }
      printf("!!!!!!!!!!!!!!!! read():TIMEOUT !!!!!!!!!!!!!!!!\n");
      return 0;
   }
   buf[n]=0;
   printf("read() OK: %s\n",buf);
   return 1;
#endif
}

#if USE_CALLBACK_CLIENT==1

static void ipc_callback(void *data, IPC *ipc)
{
  int *callback_invoked=(int *)data;
  printf("ipc_callback() invoked: data available on IPC object %p with data=%p\n",ipc,data);
  *callback_invoked=1;
  return;
}
#endif

void *client(void *p)
{
   IPC *r=(IPC *)p;
#if USE_CALLBACK_CLIENT==1  
   int callback_invoked=0;
   if(!r->set_callback(ipc_callback,(void *)&callback_invoked)) {
     printf("!!!!!!!!!!!!!!!! set_callback() error !!!!!!!!!!!!!!!!\n");
     return 0;
   }
#endif
   
   do {
      const char hello[]="Hello world!";
#if USE_IPC_MESSAGES==1 // use read_msg() and write_msg()
      if(!r->write_msg(PREFIX,0,hello,strlen(hello))) {
         printf("!!!!!!!!!!!!!!!! write_msg():error=%d !!!!!!!!!!!!!!!!\n",r->error());
         break;
      }
#else 
      if(!r->write(hello,strlen(hello))) {
         printf("!!!!!!!!!!!!!!!! write():error=%d !!!!!!!!!!!!!!!!\n",r->error());
         break;
      }
#endif

     int ret;
#if USE_CALLBACK_CLIENT==1
     while(!callback_invoked) {
       printf("waiting for callback %p...\n",ipc_callback);
       usleep(50000); // 50ms
     }
     ret=receive(r,0); // use a timeout of 0, since available data is notified by this callback
#else
     ret=receive(r,2000);
#endif
     if(ret<=0) break; // leave loop on error
   } while(!SINGLE_SHOT_CLIENT);
   return 0;
}

int main(int argc, char *argv[])
{
   printf("******************* Demo-Client started *********************\n");
   do {
#ifdef USE_TCP
      TCP s;
      const char *ip_addr="127.0.0.1";
      if(argc==2) ip_addr=argv[1]; // allow to set IP address via argument list
      while(!s.connect(ip_addr,TCP_PORT)) {
         fprintf(stderr,"!!!!!!!!!!!!!!!! Connect failed !!!!!!!!!!!!!!!!\n");
         sleep(1);
      }
#else // pipe test
      Pipe s(Pipe::PC_EnableCredentials);
      while(!s.connect(PIPE_NAME)) {
         fprintf(stderr,"!!!!!!!!!!!!!!!! Connect failed !!!!!!!!!!!!!!!!\n");
         sleep(1);
      }
#endif
      printf("remote: %s\n",s.remote_addr());
      client(&s); // fire
      
      // printf("main() client T%d: heap_current=%d,heap_max=%d,stack_max=%d\n",pthread_self(),_heap_current(),_heap_max(),_stack_max());
   } while(!SINGLE_CONNECT_CLIENT);
   return 0;
}
//! [client_source]
#endif // ifdef DEMO_CLIENT

///////////////////////////////////////// JSON demo /////////////////////////////////////////

#ifdef JSON_DEMO

//! [json_source]

using namespace vfiipc;

int main()
{
   JSObject obj;                                // JavaScript equivalent:
   obj("float")=42.5;                           // obj.number=42
   obj("intnum")=42;
   obj("string")="Hello world!";                // obj.string="Hello world!" 
   obj("object")("bool")=true;                  // obj.object.bool=true
   obj("array")[3]("test")=3;                   // obj.array[3].test=3

   printf("%d\n",(int)obj("float"));
   printf("%.16g\n",(float)obj("float"));
   printf("%d\n",(int)obj("intnum"));
   printf("%.16g\n",(float)obj("intnum"));
   printf("%d\n",(int)obj("array")[3]("test"));

   // iterate over the object
   JSObject::const_iterator i;
   for(i=obj.begin();i!=obj.end();++i) {
      printf("obj[%s]=%s\n",i->first.c_str(),i->second.getString().c_str());
   }

   string s2=obj("string");
   printf("%s\n",s2.c_str());

   string s=obj.dump();
   printf("%s\n",s.c_str());
   printf("%d\n",(int)obj.load(s));

   s=obj.dump();
   printf("%s\n",s.c_str());


   string a="1\n2";
   printf("\n%s\n",a.c_str());
   obj("twoline")=a;
   s=obj.dump();
   printf("%s\n",s.c_str());
   obj.load(s);
   a=(string)obj("twoline");
   printf("%s\n",a.c_str());

   return 0;
}

//! [json_source]

#endif // ifdef JSON_DEMO
