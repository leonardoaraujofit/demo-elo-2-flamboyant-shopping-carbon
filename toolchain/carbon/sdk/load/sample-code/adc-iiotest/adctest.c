/* Hwmon client for industrial I/O devices
 *
 * Copyright (c) 2011 Jonathan Cameron
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/err.h>
#include <linux/platform_device.h>
#include <linux/hwmon.h>
#include <linux/of.h>
#include <linux/hwmon-sysfs.h>
#include <linux/iio/consumer.h>
#include <linux/iio/types.h>
#include <linux/kthread.h>
#include <linux/sched.h>
#include <linux/time.h>   // for using jiffies
#include <linux/timer.h>
#include <linux/delay.h>

#define NTHREADS 5

static struct task_struct *pstKthread;

/* prototype for the example thread */
static int example_thread(void *data);

/**
 * struct adctest_state - device instance state
 * @channels:		filled with array of channels from iio
 * @num_channels:	number of channels in channels (saves counting twice)
 * @adctest_dev:	associated hwmon device
 */
struct adctest_state {
	struct iio_channel *channels;
	int num_channels;
	struct device *adctest_dev;
};
struct adctest_state *st;

static char *szAdcNames [8] = {"Ambient light", "Temp sensor",
							 "Touch Stylus", "Printer head Voltage",
							 "Printer head temp", "Printer paper rpesent",
							 "Battery Voltage", "Battery thermistor"};
/* this is the thread function that we are executing */
static int example_thread (void *data)
{
	int nRC = 0;
	int nResult;
	int i, nCount;

	for (nCount = 0; nCount < 2; nCount++)
	{
		set_current_state(TASK_INTERRUPTIBLE);
		schedule_timeout (2*HZ);
		printk("\n");
		for (i = 0; i < st->num_channels; i++) {
			/* take a nap and wake up in "s" seconds */
			nRC = iio_read_channel_raw(&st->channels[i], &nResult);
			if (nRC >= 0)
			{
				printk ("%s: %d\n", szAdcNames[i], nResult);
			}
			else
			{
				printk ("Failed to read %s value", szAdcNames[i]);
			}
		}
	}
	return (0);
}

static int adctest_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	int nRC = 0;
	struct iio_channel *channels;

	channels = iio_channel_get_all(dev);
	if (IS_ERR(channels))
	{
		nRC = PTR_ERR(channels);
	}
	else
	{
		st = devm_kzalloc(dev, sizeof(*st), GFP_KERNEL);
		if (st == NULL)
		{
			nRC = -ENOMEM;
			iio_channel_release_all(channels);
		}
		else
		{
			st->channels = channels;

			/* count how many attributes we have */
			while (st->channels[st->num_channels].indio_dev)
				st->num_channels++;

			platform_set_drvdata(pdev, st);

			/* create new kernel threads */
			pstKthread = kthread_create(example_thread, NULL, "kadctestd");
			if((pstKthread))
			{
				printk(KERN_INFO "start thread");
				wake_up_process(pstKthread);
			}
		}
	}

	return (nRC);
}

static int adctest_remove(struct platform_device *pdev)
{
	struct adctest_state *st = platform_get_drvdata(pdev);
	iio_channel_release_all(st->channels);
	return 0;
}

static struct of_device_id adctest_of_match[] = {
	{ .compatible = "adctest", },
	{ }
};
MODULE_DEVICE_TABLE(of, adctest_of_match);

static struct platform_driver __refdata adctest_driver = {
	.driver = {
		.name = "adctest",
		.owner = THIS_MODULE,
		.of_match_table = adctest_of_match,
	},
	.probe = adctest_probe,
	.remove = adctest_remove,
};

module_platform_driver(adctest_driver);

MODULE_AUTHOR("Aidan Totterdell <blahblah@blah>");
MODULE_DESCRIPTION("IIO to adc driver");
MODULE_LICENSE("GPL v2");
