#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>
#include "extra.h"
static dev_t first; // Global variable for the first device number
static struct cdev c_dev; // Global variable for the character device structure
static struct class *cl; // Global variable for the device class

static int my_open(struct inode *i, struct file *f)
{
	printk(KERN_INFO "Driver: open()\n");
	testfunc();
	testfuncmore();
	return 0;
}


static int my_close(struct inode *i, struct file *f)
{
	printk(KERN_INFO "Driver: close()\n");
	return 0;
}

static char szRxBuf[80];

static ssize_t my_read(struct file *f, char __user *buf, size_t
  len, loff_t *off)
{
	char szTxBuf[80];
	strcpy (szTxBuf, "Right back at you!");

	if (copy_to_user (buf, szTxBuf, sizeof (szTxBuf)))
		return EFAULT;

	printk(KERN_INFO "Driver: read()\n");
	return 0;
}


static ssize_t my_write(struct file *f, const char __user *buf,
  size_t len, loff_t *off)
{

	if (copy_from_user ((void *)szRxBuf , buf, len))
		return EFAULT;
	printk(KERN_INFO "Driver: write() --- string %s\n", szRxBuf);
	return len;
}


static struct file_operations pugs_fops =
{
	.owner = THIS_MODULE,
	.open = my_open,
	.release = my_close,
	.read = my_read,
	.write = my_write
};


static int __init modtest_init(void) /* Constructor */
{
	printk(KERN_INFO "Aidan: modtest registered\n");
	if (alloc_chrdev_region(&first, 0, 1, "Aidan") < 0)
	{
		return -1;
	}
	if ((cl = class_create(THIS_MODULE, "chardrvdemo")) == NULL)

	{
		unregister_chrdev_region(first, 1);
		return -1;
	}

	if (device_create(cl, NULL, first, NULL, "mymod") == NULL)
	{
		class_destroy(cl);
		unregister_chrdev_region(first, 1);
		return -1;
	}
	cdev_init(&c_dev, &pugs_fops);
	if (cdev_add(&c_dev, first, 1) == -1)
	{
		device_destroy(cl, first);
		class_destroy(cl);
		unregister_chrdev_region(first, 1);
		return -1;
	}

	return 0;
}

static void __exit modtest_exit(void) /* Destructor */
{
	cdev_del(&c_dev);
	device_destroy(cl, first);
	class_destroy(cl);
	unregister_chrdev_region(first, 1);
	printk(KERN_INFO "Aidan: modtest unregistered\n");
}

module_init(modtest_init);
module_exit(modtest_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Aidan <aidan_t1@verifone.com>");
MODULE_DESCRIPTION("Test module for linux 3.12 kernel");
