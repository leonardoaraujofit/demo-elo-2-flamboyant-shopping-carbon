#include <linux/ioctl.h>
#include <linux/kdev_t.h> /* for MKDEV */

#ifdef __cplusplus
extern "C" {
#endif

#define DEVICE_NAME "mymod"
#define DEVICE_PATH "/dev/mymod"

#ifdef __cplusplus
}
#endif
