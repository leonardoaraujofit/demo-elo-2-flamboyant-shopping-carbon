#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include <mymod.h>
#include "mylib.h"

int readDevice (char *szBuff, int nBufLen)
{
	int nFd = 0;
	int nRc = 0;
	int nReadCount = 0;

	if ((nFd = open (DEVICE_PATH, O_RDWR)) >= 0)
	{
		nReadCount = read (nFd, szBuff, nBufLen);
		if (nReadCount < 0)
		{
			perror ("TESTLIB: device read failed");
			nRc = 1;
		}
		else
		{
			fprintf (stderr, "TESTLIB: read bytes ok!\n");
		}
	    close(nFd);
	}
	else
	{
		perror ("TESTLIB: Failed to open device");
		nRc = 1;
	}

	return (nRc);
}


int writeDevice (char *szBuff, int nBufLen)
{
	int nFd = 0;
	int nRc = 0;
	int nWriteCount = 0;

	if ((nFd = open (DEVICE_PATH, O_RDWR)) >= 0)
	{
		nWriteCount = write (nFd, szBuff, nBufLen);
		if (nWriteCount > 0)
		{
			fprintf (stderr, "TESTLIB: wrote %d bytes ok!\n", nWriteCount);
		}
		else
		{
			perror ("TESTLIB: device write failed");
			nRc = 1;
		}
	    close(nFd);
	}
	else
	{
		perror ("TESTLIB: Failed to open device");
		nRc = 1;
	}

	return (nRc);
}
