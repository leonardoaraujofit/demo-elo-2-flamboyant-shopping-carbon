#ifndef _MYLIB_H__
#define _MYLIB_H__

#ifdef __cplusplus
extern "C" {
#endif

int readDevice (char *szBuff, int nBufLen);
int writeDevice (char *szBuff, int nBufLen);

#ifdef __cplusplus
}
#endif

#endif
