#include <stdio.h>
#include <string.h>

#include <mylib.h>

int main (void)
{
	int nRC = 0;
	char *szMsg = "Yahooooo!";
	char szReadBuf [80];

	fprintf (stderr, "About to write string to device\n");
	nRC =  writeDevice (szMsg, strlen (szMsg));
	if (!nRC)
	{
		fprintf (stderr, "Device write ok\n");
		nRC =  readDevice (szReadBuf, sizeof (szReadBuf));
		if (!nRC)
		{
			fprintf (stderr, "Device read ok\n");
			fprintf (stderr, "Device read successful read string=%s\n", szReadBuf);
		}
		else
		{
			fprintf (stderr, "Device write failed: err=%d\n", nRC);
		}
	}
	else
	{
		fprintf (stderr, "Device write failed: err=%d\n", nRC);
	}

	return (0);
}
