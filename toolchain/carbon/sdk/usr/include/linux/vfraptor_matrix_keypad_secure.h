/*
 *  Verifone Raptor secure pinpad interface
 *
 *  Copyright (c) 2014 Guofeng Li <guofengl2@verfione.com>
 */

#ifndef _UAPI_VFR_MK_SECURE_H_
#define _UAPI_VFR_MK_SECURE_H_

struct touch_hotspot_info
{
   unsigned short  x1;         /* starting x of hotspot */
   unsigned short  y1;         /* starting y of hotspot */
   unsigned short  x2;         /* ending x of hotspot */
   unsigned short  y2;         /* ending y of hotspot */
   char            result;     /* ASCII value to return */
   char            options;    /* RESERVED */
};

/* Touchscreen */
#define MAX_NUM_HOTSPOTS    20

struct touch_hs_s
{
   short   num_hotspots;       /* the number of active hotspots */
   struct  touch_hotspot_info  touch_spot[MAX_NUM_HOTSPOTS];
};

#endif /* _UAPI_VFR_MK_SECURE_H_ */
