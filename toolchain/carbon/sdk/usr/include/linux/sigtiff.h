
/**************************************************************************
 * FILE NAME:   sigtiff.h                                                 *
 * MODULE NAME: sigtiff.h                                                 *
 * PROGRAMMER:  Don Ward                                                  *
 * DESCRIPTION: Verifone Touch and Signature Capture header               *
 **************************************************************************/

// Trident keeps this file in directory buildroot/vsc/kmod-cirque_k2 and to be accessed always as #include "sigtiff.h"

// Raptor, on the other hand, has to keep the same file in directory linux/include/uapi/linux for access by the Atmel driver which in linux
// and is accessed as #include <linux/input/sigtiff.h> from driver/kernel space and as #include <linux/sigtiff.h> from application space.

#ifndef _UAPI_SIGTIFF_H
#define _UAPI_SIGTIFF_H

#ifndef __KERNEL__
#include <sys/types.h>
#include <tiffio.h>
#endif

// Note: x,y need to be at least 12 bits in xy_t and xyz_t
// to allow negative values to mean no touch / pen up
typedef struct
{
    short x;
    short y;
} __attribute__((packed)) xy_t;
#define SET_PENUP_XY(p) ((p).x = (p).y = -1)
#define IS_EQUAL_XY(p1,p2) ((p1).x == (p2).x && (p1).y == (p2).y)

typedef struct
{
    long x;
    long y;
} __attribute__((packed)) xy_long_t;

#ifdef RAPTOR
// This is used for signature / raw data capture
typedef struct
{
    unsigned long x : 12; 
    unsigned long y : 12;
    long z : 8;
} __attribute__((packed)) xyz_t;

typedef struct
{
   	long x : 12; 
	long y : 12;
	long z : 8;
} __attribute__((packed)) xyz_signed_t;

#else

// This is used for signature / raw data capture
typedef struct
{
   	long x : 12; 
	long y : 12;
	long z : 8;
} __attribute__((packed)) xyz_t;

#endif

typedef struct
{
	char identifier[16];
	short int x;
	short int y;
	short int z;
} hotspot_event_t;

typedef struct
{
	char identifier[16];
    short int left;
    short int upper;
    short int right;
    short int lower;
    short int centerX;
    short int centerY;
} hotspot_t;
#define MAX_HOTSPOTS 16
typedef struct
{
    int count;
    hotspot_t hotspots[MAX_HOTSPOTS];
} hotspot_table_t;

typedef struct
{
    int count;
    hotspot_event_t *hsEvent;
} hotspot_data_t;

#define HOTSPOT_FIFO_DEPTH	16

// Hotspot mode bits, to be or'ed together to specify a mode
// DO NOT rearrange or renumber as hotspot service defines some modes as ors of these bits
#define HOTSPOT_REPORT_HS_DOWN_AT_DOWN		(1<<0)      // used in service
#define HOTSPOT_REPORT_UP			(1<<1)              // used in service
#define HOTSPOT_REPORT_HS_SLIDES 		(1<<2)          // used in service
#define HOTSPOT_REPORT_UP_BETWEEN_HS_SLIDES 	(1<<3)
#define HOTSPOT_REPORT_HS_DOWN_AT_UP		(1<<4)      // used in service
#define HOTSPOT_REPORT_HS_UP_AT_UP		(1<<5)
#define HOTSPOT_REPORT_NHS_SLIDE_AS_NULL_DOWN	(1<<6)
#define HOTSPOT_REPORT_NHS_SLIDE_AS_UP		(1<<7)
#define HOTSPOT_REPORT_DUPLICATES 		(1<<8)

typedef long irq_no_print_table_t[96];

// These macros are for use by driver, library, cttest program and applications.
#define Z(xyz) ((xyz).z & 0x3f)
#define PALM_REJECT(xyz) ((xyz).z & 0x40)
#define FINGER(xyz) ((xyz).z & 0x80)

// #define SET_PENUP_XYZ(p) ((p).x = (p).y = (p).z = (p).finger = -1)
#define SET_PENUP_XYZ(p) ((p).x = (p).y = (p).z = -1)
#define SET_MARKER_XYZ(p) ((p).x = -2, (p).y = (p).z = -1)

#define IS_PENUP(p) ((p).x < 0 && (p).y < 0)

enum model_info
{
    BigBoard = 0,
    EVT1 = 1,
    EVT2 = 2,
    DVT1 = 3,
    DVT2 = 4,
    DVT3 = 5,
    PVT1 = 6,
    VX680 = 7,
    UX200_EVT1 = 8,
    UX200_EVT2 = 9,
    UX200_DVT = 10,
    C_SERIES = 11,
    RAPTOR_BB = 12,
    SWORDFISH = 13,
};

enum touchpanel_type
{ 
    NotSpecified = 0,
    Gapped3p5in12x16 = 1,
    Gapped4p3in12x16 = 2,
    Gapped5p7in12x16 = 3,
    Gapped7p0in12x16 = 4,
    NonGapped3p5in12x16 = 5,
    NonGapped4p3in12x16 = 6,
    NonGapped5p7in12x16 = 7,
    NonGapped7p0in12x16 = 8,
    Resistive3p5in = 9,
    Capacitive10p0in = 10,
    Capacitive5p0in = 11,
};

// Even though enum type allows for 32 bits, please don't use bits 16 ... 31 as variable may be stored as 16 bits in places.
enum touchpanel_mfr
{
    GiantPlus = (1<<0),
    TouchInternational = (1<<1),
    Ocular = (1<<8),
    AtmelChip = (1<<11),
    Ux200Capacitive = (1<<12),
    Vx680Resistive = (1<<13),
    GenericMfr = (1<<14),
    PanelAbsent = (1<<15),
    DontUse = (0xffff<<16), // Please don't use bits 16 ... 31
};

enum stylus_id
{
    Stylus_Absent = (1<<0),
    Lorom_Rev_B = (1<<10),
    Lorom_Rev_A = (1<<15),
    Stylus_Unknown = (1<<30),
    /* Bit 31 should not be used */
};

// These are for use in tables by opsys only
enum tweaks
{
    No_Tweaks = 0,
    Finger_Correction_X = (1<<1),
    Finger_Correction_Y = (1<<2),
    Dont_Sync_Cmd_Complete_With_Hw_Dr = (1<<3),
    Invert_Axis_X = (1<<4), // Currently used only with Egalax chip
    Invert_Axis_Y = (1<<5), // Currently used only with Egalax chip 
};

// These provided are for apps to be able to modify touch / sigcap behavior
enum app_options
{
    No_Application_Options = 0,
    Init_Sig_Cap_Buff = (1<<0),
    Mark_Sig_Cap_Buff = (1<<1),
};

struct devmem_access
{
    int write; // 0 read, 1 write
    unsigned long physical; // physical address
    unsigned long value; // value read or to write
};

// Special 4-byte type that is float in user space but long in kernel
// Created to allow same struct definition to be used in user space AND kernel
typedef 
#ifdef __KERNEL__
    long // Kernel doesn't support float type
#else
    float // Used for calculations only in user space
#endif	
lorf;

// Note that all values default to 0 unless specifically set in initialiser or in some cases by an environment variable
struct touchpanel_info
{
    enum touchpanel_type type; // Touchpanel type
    enum touchpanel_mfr mfr; // Touch panel manufacturer code
    unsigned int firmware_info; // Resistive : 0
				 // Cirque: 0x00IIVVSS where II is Firmware ID (chip family ID code), VV is Firmware Version and SS is Firmware SubVersion
				 // Egalax: 0xMmmm000t where Mmmm are EETI version numbers M.mmm and t is test subversion number
    u_char rext_slot; // Analog multiplexor slot for external K2 resistor Rext
    u_char filter_length; // Smoothing filter length override
    u_char aperture_width; // Stylus aperture width override
    u_char sample_length; // Stylus sample length override
    u_char setup_mode; // Pen-finger setup mode
    u_char pen_gain; // Pen gain override
    u_short max_comp_diff; // Maximum allowable diff between comp and factory comp
    enum stylus_id stylus_id; // ID returned by stylus
    enum tweaks tweaks; // Tweaks bit mask
    xy_t pix;  // Panel dimensions in pixels
    xy_t thou; // Panel dimensions in thousandths of an inch
    xy_t dpi;  // Dots per inch
    struct z_info // These can be overridden by envirnoment variables with same spelling, e.g. *SON
    {
	u_char son,soff; // Z on and Z off thresholds for stylus
	u_char fon,foff; // Z on and Z off thresholds for finger
	u_char con,coff; // Z on and Z off (idle) counts
    } z_info;
    enum app_options app_options; // Can be set by *TOUCHOPTIONS environment variable
    struct { short x,y,z; } raw_min, raw_max, raw_range;
    long spike_threshold; // Derived value
    struct scaling_const_float
    {
	lorf xk,xx,xy,yk,yx,yy;
    } scaling_const_float;
    struct scaling_const  // Derived scaling constants
    {
	long xk,xx,xy,yk,yx,yy;
    } scaling_const, scaling_const_inverse;
};

#define SCALING_FACTOR 	(1<<16) // Used in a variety of calculations

// Default RA filter lengths for capacitive and resistive panels
#define DEFAULT_CAP_FILTER_LENGTH	8
#define DEFAULT_RES_FILTER_LENGTH	8

#define FW_INFO_STR_SIZE		64

enum egx_cmdTweaks
{ 
    TRUNCATE_PACKET = 1,
    STRETCH_CLOCK = 2
};

struct sigcap_params
{
	xyz_t *data;
	long size;
	long offset;
};

#ifndef __KERNEL__

// Range allowed for user-defined tags
#define MIN_TIFFTAG_USER 32768
#define MAX_TIFFTAG_USER 65535

typedef struct
{
	short left,upper,right,lower;
} SigCapBox_t;

typedef struct
{
	long joinPoints : 1;
	long trimWidth : 1;
	long trimHeight : 1;

	// 3x3 thickening matrix (9 bits)
	long xminus1yminus1 : 1;
	long xyminus1 : 1;
	long xplus1yminus1 : 1;
	long xminus1y : 1;
	long xy : 1;
	long xplus1y : 1;
	long xminus1yplus1 : 1;
	long xyplus1 : 1;
	long xplus1yplus1 : 1;
	
	// extension to 5x5 thickening matrix (16 more bits)
	long xminus2yminus2 : 1;
	long xminus1yminus2 : 1;
	long xyminus2 : 1;
	long xplus1yminus2 : 1;
	long xplus2yminus2 : 1;
	
	long xminus2yminus1 : 1;
	long xplus2yminus1 : 1;
	
	long xminus2y : 1;
	long xplus2y : 1;
	
	long xminus2yplus1 : 1;
	long xplus2yplus1 : 1;
	
	long xminus2yplus2 : 1;
	long xminus1yplus2 : 1;
	long xyplus2 : 1;
	long xplus1yplus2 : 1;
	long xplus2yplus2 : 1;
	
	long png : 1; // Generate png file
	long : 2; // Spare
	long alt : 1; // Set to indicate use of alternate structure below
} __attribute__((packed)) SigCapOptions_t;
#define THICKENING_MASK 		0x0FFFFFF8 // All thickening bits including xy bit

typedef struct
{
	long joinPoints : 1;
	long trimWidth : 1;
	long trimHeight : 1;
	long : 1;
	
	unsigned long thickWidth: 4; // X thickening count
	unsigned long thickHeight : 4; // Y thickening count

	long : 16;
	
	long png : 1; // Generate png file
	long : 2; // Spare
	long alt : 1; // Set to indicate use of this alternate structure
} __attribute__((packed)) SigCapOptions_alt;
#define THICKENING_MASK_ALT 		0x80000FF0 // Nibbles that specify thickening box widths with alt bit set

#define THICKENING_MAX			16	   // Max range of thickening box in x or y dimension (-8 to +7)

extern int SigCapBoxApply(xyz_t *sig,int count,SigCapBox_t *box);

extern int SigCapBoxApplyPart(xyz_t *sig,int count,SigCapBox_t *box);

extern int SigCap2Tiff(char *fname,xyz_t *sig,int count,short compression_scheme,xy_t *dpi,SigCapBox_t *box,SigCapOptions_t *options,void (*setTiffUserTags)(TIFF *));

extern int tiff2png_main (int argc, char *argv[]);

extern int SigCapInsert(long value);

extern int SigCapGetPart(void *data,long size,long offset);

#endif

// This is designed to be compatible with Mx legacy API
// Same absolute values are used in svc.h to define SigCap...() functions
// except as notes below
enum sigcap_command
{
    SIGCAP_FIRSTMOUSEDOWN = -4,	// NOT in svc.h as this value is for opsys use only
    SIGCAP_SUSPEND = -3,
    SIGCAP_RESUME  = -2,
    SIGCAP_COUNT   = -1,
    SIGCAP_END     =  0,
};

#endif /* #ifndef _UAPI_SIGTIFF_H */
