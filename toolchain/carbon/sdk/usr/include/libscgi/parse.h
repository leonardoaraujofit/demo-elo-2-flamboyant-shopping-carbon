/*
 * This file is part of libscgi
 * Copyright (C) 2008-2009 by ASN <http://www.asn.pl/>
 * Author: Łukasz Zemczak <sil2100@asn.pl>
 *
 * libscgi is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * libscgi is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PARSE_H__
#define __PARSE_H__

#include <libasn/thash.h>

/**
 * Parse the given netstring interpretation.
 *
 * The function parses a netstring interpretation extracting key:value pairs
 * and adding them to the given hash table.
 *
 * @param hash   variable hash table
 * @param string string of bytes with the netstring interpretation
 * @param length size of the netstring interpretation
 * @param max    maximum size of a single element value
 * @return status of parsing process
 * @ret val SCGI_OK on success and SCGI_ERROR on error
 */
int netstring_parse(thash *hash, char *string, long int length, long int max);

/**
 * Parse the given urlencoded string
 *
 * The function parses a urlencoded string, extracting encoded key:value pairs
 * adding them to the given hash table.
 *
 * @param hash   variable hash table
 * @param string urlencoded string
 * @param length size of the urlencoded string
 * @param max    maximum size of a single element value
 * @return status of parsing process
 * @ret val SCGI_OK on success and SCGI_ERROR on error
 */
int urlencoded_parse(thash *hash, char *string, long int length, long int max);

#endif
