/*
 * This file is part of libscgi
 * Copyright (C) 2008-2009 by ASN <http://www.asn.pl/>
 * Author: ukasz Zemczak <sil2100@asn.pl>
 *
 * libscgi is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * libscgi is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SCGI_H__
#define __SCGI_H__

#include <libasn/thash.h>

/* uncomment following define if you want to redirect stdin/stdout to scgi->fd_conn */
//#define STDIO_REDIRECT

/** Status codes */
enum {
	SCGI_ERROR = -1,
	SCGI_OK,
	SCGI_NOTAVAIL,
	SCGI_INVTYPE,
	SCGI_MAXSIZE,
	SCGI_CONTINUE
};


/** Supported MIME content types */
enum {
	MIME_TEXT = 0,
	MIME_IMAGE,
	MIME_AUDIO,
	MIME_VIDEO,
	MIME_APPLICATION,
	MIME_MULTIPART,
	MIME_MESSAGE,
	MIME_UNKNOWN
};


/** Supported MIME content encodings */
enum {
	ENCODING_7BIT = 0,
	ENCODING_8BIT,
	ENCODING_BASE64,
	ENCODING_QUOTED,
	ENCODING_BINARY,
	ENCODING_UNKNOWN
};


/** Default maximum content/part sizes */
#define DEFAULT_MAX_CONTENT 128000000
#define DEFAULT_MAX_PART    1024
#define DEFAULT_MAX_NETSTR  2000000



struct mime_data_t;
struct scgi_connection_t;

/** Information callback definition */
typedef int (callback_info)(struct scgi_connection_t *scgi, struct mime_data_t *mime,
    char *name);

/** Data callback definition */
typedef int (callback_data)(struct scgi_connection_t *scgi, struct mime_data_t *mime, 
    char *name, char *data, int size);

/** SCGI instance structure */
typedef struct scgi_connection_t {
	/** Table holding values (variables) aquired during read */
	thash *vars;

	/** Descriptor for control communication */
	int fd_control;

	/** Descriptor for SCGI I/O */
	int fd_conn;

#ifdef STDIO_REDIRECT
	/** Backup copies of standard I/O descriptors */
	int fd_std[2];
#endif

	/** Maximum allowed size of the request content */
	long int max_content_size;

	/** Maximum allowed size of multipart data to hold directly in memory */
	long int max_part_size;

	long int content_pos; /* TODO: use this in all reading functions */

	long int content_len;

	char *part_data;

	/** Callback for MIME multipart data, called for every data chunk */
	callback_data *data_callback;

	/** Callback function called at the beginning of parsing a multipart object */
	callback_info *start_callback;

	/** Callback function called at the end of parsing a multipart object */
	callback_info *end_callback;

#ifndef SINGLE_THREAD
	#ifdef USE_PTHREAD
	/** Mutex used for structure protection */
	pthread_mutex_t mutex;
	#endif
#endif

} scgi_connection;



/**
 * Initilizes a new SCGI connection.
 *
 * The file descriptor of the server will be used for control communication.
 *
 * @param scgi          the scgi connection structure to be initialized
 * @param fd_control    file descriptor of connection to the server
 * @param max_content   maximum allowed size of the message content
 * @param max_part      maximum allowed size of a multipart variable in memory
 * @param data_callback callback function to call when multipart data present
 * @param start         callback function to call when multipart object found
 * @param end           callback function to call when multipart object finished
 * @return structure defining the connection
 */
void scgi_init(scgi_connection *scgi, int fd_control, long int max_content, 
    long int max_part, callback_data *data_callback, callback_info *start, 
    callback_info *end);

/**
 * Creates a new SCGI connection.
 *
 * The file descriptor of the server will be used for control communication.
 * This function allocates a new scgi_connection structure and initializes it
 * using scgi_init().
 *
 * @param fd_control    file descriptor of connection to the server
 * @param max_content   maximum allowed size of the message content
 * @param max_part      maximum allowed size of a multipart variable in memory
 * @param data_callback callback function to call when multipart data present
 * @param start         callback function to call when multipart object found
 * @param end           callback function to call when multipart object finished
 * @return structure defining the connection
 */
scgi_connection *scgi_create(int fd_control, long int max_content, long int max_part,
    callback_data *data_callback, callback_info *start, callback_info *end);

/**
 * Initialize a SCGI connection with default parameters.
 *
 * A wrapper for scgi_create with size limits set to default values.
 *
 * @return structure defining the connection
 */
scgi_connection *scgi_create_default(callback_data *data_callback, 
    callback_info *start, callback_info *end);

/**
 * Deinitializes SCGI connection, freeing all internal fields of the structure.
 *
 * @param scgi structure defining the connection
 */
void scgi_deinit(scgi_connection *scgi);

/**
 * Deinitializes SCGI connection, freeing all fields of the structure as well
 * as the scgi connection structure itself using free().
 *
 * @param scgi structure defining the connection to be freed
 */
void scgi_free(scgi_connection *scgi);

/**
 * Aquire input from a SCGI connection.
 *
 * This function tries to read SCGI input from a given working connection.
 * If there is no input currently present, the function returns SCGI_NOTAVAIL.
 * If the content of the incoming message is too large for the given size
 * limits, the function returns SCGI_MAXSIZE.
 * Otherwise, the function will read the input, parse it and insert the
 * resulting values into the variable hashtable.
 *
 * @param scgi structure defining the connection
 * @return status of the acquisition trial
 * @ret val SCGI_OK on valid input, SCGI_NOTAVAIL when no input, SCGI_MAXSIZE if content/part too large and SCGI_ERROR on failure
 */
int scgi_input(scgi_connection *scgi);

/**
 * Flush the given SCGI connection.
 *
 * What this function literaly does is simply closing the SCGI client
 * connection, therefore informing the client that we're done with our response.
 *
 * @param scgi structure defining the connection
 */
void scgi_flush(scgi_connection *scgi);

/**
 * Fetch a single string from acquired SCGI input.
 *
 * @param scgi structure defining the connection
 * @param name the variable name
 * @param buf  target buffer for holding the fetched string
 * @param max  maximum number of bytes to copy from the string
 * @return status of the fetching operation
 * @ret val SCGI_OK on success, SCGI_NOTAVAIL when variable nonexistent, SCGI_INVTYPE when variable is not a string and SCGI_ERROR on error
 */
int scgi_fetchstring(scgi_connection *scgi, const char *name, char *buf, long int max);

/**
 * Fetch an integer variable from the acquired SCGI input.
 *
 * @param scgi structure defining the connection
 * @param name the variable name
 * @param buf  target buffer for holding the fetched integer
 * @return status of the fetching operation
 * @ret val SCGI_OK on success, SCGI_NOTAVAIL when variable nonexistent, SCGI_INVTYPE when variable is not a string and SCGI_ERROR on error
 */
int scgi_fetchinteger(scgi_connection *scgi, const char *name, int *buf);

/**
 * Get the MIME type of a given variable.
 *
 * @param scgi structure defining the connection
 * @param name the variable name
 * @param type integer variable for holding the MIME type
 * @return status of the fetching operation
 * @ret val SCGI_OK on success, SCGI_NOTAVAIL when variable nonexistent and SCGI_ERROR on error
 */
int scgi_getmimetype(scgi_connection *scgi, const char *name, int *type);

/**
 * Get the size of a given variable.
 *
 * When the given variable is of type MIME_MULTIPART, the returned size is the
 * number of elements (MIME parts) in the container (tlist). Otherwise, the
 * size of the variable in bytes is returned. In case of a MIME_TEXT element,
 * the null terminating character is not counted.
 *
 * @param scgi structure defining the connection
 * @param name the variable name
 * @param type long integer variable for holding the size value
 * @return status of the fetching operation
 * @ret val SCGI_OK on success, SCGI_NOTAVAIL when variable nonexistent and SCGI_ERROR on error
 */
int scgi_getsize(scgi_connection *scgi, const char *name, long int *size);

/**
 * write string contents using the connection defined in the scgi structure
 *
 *  @param scgi structure defining the connection
 *  @string output string
 *  @return status of the writing of data out the connection
 *  $ret val number of bytes sent out connection on success, negative value on error
 */
int scgi_writestring(scgi_connection *scgi, const char *string);

#endif
