/*
 * This file is part of libscgi
 * Copyright (C) 2008-2009 by ASN <http://www.asn.pl/>
 * Author: Łukasz Zemczak <sil2100@asn.pl>
 *
 * libscgi is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * libscgi is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SCOMMON_H__
#define __SCOMMON_H__

/* Common defines for both libscgi and the scgiserver */

/** Byte used as a ready to read indication character */
#define DEFAULT_READY_BYTE 'R'

/** The default file descriptor used for the control socket between libscgi and scgiserver */
#define DEFAULT_FD_CONTROL  3

/** By default, use pthread mutexes to lock the scgi structures */
#define USE_PTHREAD

#ifdef SINGLE_THREAD
	#define INIT_LOCK()    while(0) {}
	#define DESTROY_LOCK() while(0) {}
	#define LOCK()         while(0) {}
	#define UNLOCK()       while(0) {}
#else
	#ifdef USE_PTHREAD
		#include <pthread.h>
		#define INIT_LOCK() pthread_mutex_init(&(scgi->mutex), NULL)
		#define DESTROY_LOCK() pthread_mutex_destroy(&(scgi->mutex))
		#define LOCK() pthread_mutex_lock(&(scgi->mutex))
		#define UNLOCK() pthread_mutex_unlock(&(scgi->mutex))
	#endif
#endif

#endif
