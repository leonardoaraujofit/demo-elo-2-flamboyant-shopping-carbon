/*
 * This file is part of libscgi
 * Copyright (C) 2008-2009 by ASN <http://www.asn.pl/>
 * Author: Łukasz Zemczak <sil2100@asn.pl>
 *
 * libscgi is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * libscgi is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __LIB_H__
#define __LIB_H__

/**
 * Read count bytes from the given file descriptor to the given buffer.
 *
 * The buffer must be at least count + 1 bytes long, since the resulting
 * byte stream in buf will be null-terminated. The function will read until
 * the given number of bytes are read or an I/O error occurs.
 *
 * @param fd    file descriptor
 * @param buf   buffer for holding the data
 * @param count number of bytes to read
 * @return status of the read process
 * @ret val SCGI_OK on success and SCGI_ERROR on error
 */
int readlen(int fd, char *buf, long int count);

/**
 * Read bytes from file descriptor until a CRLF.
 *
 * The resulting byte stream includes the CRLF pair (0x0D 0x0A) and is
 * null-terminated. The length of the data returned includes the null character.
 *
 * @param fd       file descriptor
 * @param buf      buffer for holding the data
 * @param size     size of the buffer
 * @param lastchar pointer to the previously last extracted character
 * @return number of bytes read
 * @ret val number of bytes or -1 on error
 */
long int readcrlf(int fd, char *buf, long int size, char *lastchar);

/**
 * Read a netstring from file descriptor.
 *
 * Reads from file descriptor a netstring of the for [len]":"[string]"," and
 * save the resulting byte stream to newly allocated byte string of 'length'
 * length.
 *
 * @param fd     file descriptor
 * @param string pointer to the pointer of a newly allocated resulting string
 * @param length pointer to the length of the resulting netstring
 * @return status of the read process
 * @ret val SCGI_OK on success, SCGI_NOTAVAIL on no input and SCGI_ERROR on error
 */
int netstring_read(int fd, char **string, int *length);

/** Extract header key-value pair (<key>:<value>)
 *
 * @param string pointer pointing to the current position in the string
 * @param key    pointer that will point at the extracted key
 * @param klen   length of the key
 * @param val    pointer that will point at the extracted value
 * @param vlen   length of the value
 * @retval SCGI_OK on success, SCGI_NOTAVAIL when no parameter/key is present, 
 *         SCGI_ERROR on error
 */
int extract_header(char **string, char **key, long int *klen, char **val, 
    long int *vlen);

/** Extract parameter key-value pair (<key>=<value>;)
 *
 * @param string pointer pointing to the current position in the string
 * @param key    pointer that will point at the extracted key
 * @param klen   length of the key
 * @param val    pointer that will point at the extracted value
 * @param vlen   length of the value
 * @retval SCGI_OK on success, SCGI_NOTAVAIL when no parameter/key is present, 
 *         SCGI_ERROR on error
 */
int extract_parameter(char **string, char **key, long int *klen, char **val, 
    long int *vlen);

/** Return the associated integer value for the given MIME type 
 *
 * @param mime_type the mime type string
 * @retval integer representing the given mime type
 */
int get_type_from_mime(const char *mime_type);

/** Return the associated integer value for the given MIME encoding 
 *
 * @param mime_encoding the mime encoding string
 * @retval integet representing the given mime encoding type
 */
int get_encoding_from_mime(const char *mime_encoding);

#endif
