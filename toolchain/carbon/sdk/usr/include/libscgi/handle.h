/*
 * This file is part of libscgi
 * Copyright (C) 2008-2009 by ASN <http://www.asn.pl/>
 * Author: Łukasz Zemczak <sil2100@asn.pl>
 *
 * libscgi is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * libscgi is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __HANDLE_H__
#define __HANDLE_H__

#include <libscgi/scgi.h>

/** Handle a SCGI content stream */
int content_handle(scgi_connection *scgi);

/** Handle a urlencoded SCGI content stream */
int urlencoded_handle(scgi_connection *scgi);

/** Handle a multipart SCGI content stream */
int multipart_handle(scgi_connection *scgi);

/** Handle a SCGI GET request */
int handle_request_get(scgi_connection *scgi);

/** Handle a SCGI POST request */
int handle_request_post(scgi_connection *scgi);

#endif
