/*
 * This file is part of libscgi
 * Copyright (C) 2008-2009 by ASN <http://www.asn.pl/>
 * Author: Łukasz Zemczak <sil2100@asn.pl>
 *
 * libscgi is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * libscgi is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __MIME_H__
#define __MIME_H__

#include <libscgi/scgi.h>


#define BUFFER_FLUSH_OFF 128

/** Structure defining a single data item */
typedef struct mime_data_t {
	/** The MIME type of the item */
	int type;

	/** The data encoding method of the item contents */
	int encoding;

	/** The size of the item contents (in case of multipart, size of list) */
	long int size;

	/** Contents of the item */
	void *data;

	/** Additional filename parameter, if given */
	char *filename;

	/** Flag defining whether data was truncated during read or not */
	int callback_data;

} mime_data;


/**
 * Free a MIME data (mime_data) item.
 *
 * @param mime MIME data item to free
 */
void mime_free(void *mime);

/**
 * Create a MIME text/plain data item from a string.
 *
 * @param data null-terminated string
 * @param max  maximum allowed size of MIME item data
 * @return mime_data item of type text/plain with the given string
 */
mime_data *mime_textcreate(char *data, long int max);

/** Parse a incoming MIME message part from a multipart input stream */
int mime_parse(scgi_connection *scgi, const char *boundary, int boundary_len);

/**
 * Handle an incoming MIME multipart message stream.
 *
 * This function, for each MIME part arriving at the input of the scgi
 * connection calls the parsing function mime_parse() and then adds the
 * extracted data to the variable table of the given connection.
 *
 * @param scgi         structure defining the connection
 * @param boundary     maximum allowed size of MIME item data
 * @param boundary_len maximum allowed size of MIME item data
 * @return status of the MIME message handling
 * @ret val SCGI_OK on valid input, SCGI_MAXSIZE if a part had to be truncated and SCGI_ERROR on failure
 */
int mime_handle(scgi_connection *scgi, const char *boundary, int boundary_len);


#endif
