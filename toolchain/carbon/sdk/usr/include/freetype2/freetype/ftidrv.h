#ifndef __FTITYPEDRV_H__
#define __FTITYPEDRV_H__

#include <ft2build.h>
#include FT_FREETYPE_H


FT_BEGIN_HEADER


  typedef enum  STF_PropertyType_
  {
    BDF_PROPERTY_TYPE_NONE     = 0,
    BDF_PROPERTY_TYPE_ATOM     = 1,
    BDF_PROPERTY_TYPE_INTEGER  = 2,
    BDF_PROPERTY_TYPE_CARDINAL = 3

  } STF_PropertyType;


  typedef struct STF_PropertyRec_*  STF_Property;


  typedef struct  STF_PropertyRec_
  {
    STF_PropertyType  type;
    union {
      const char*     atom;
      FT_Int32        integer;
      FT_UInt32       cardinal;

    } u;

  } STF_PropertyRec;


  FT_EXPORT( FT_Error )
  FT_Get_STF_Charset_ID( FT_Face       face,
                         const char*  *acharset_encoding,
                         const char*  *acharset_registry );


  FT_EXPORT( FT_Error )
  FT_Get_STF_Property( FT_Face           face,
                       const char*       prop_name,
                       STF_PropertyRec  *aproperty );


FT_END_HEADER

#endif 
