/*******************************************************************
 *
 *  Copyright 2008
 *  Broadcom Corporation
 *  5300 California Avenue
 *  Irvine, CA 92617
 *
 *  Broadcom provides the current code only to licensees who
 *  may have the non-exclusive right to use or modify this
 *  code according to the license.
 *  This program may be not sold, resold or disseminated in
 *  any form without the explicit consent from Broadcom Corp
 *
 *******************************************************************
 *
 *  Broadcom Claudius Project
 *  File: sbi.h
 *  Description: SBI header structure declarations
 *
 *  $Copyright (C) 2008 Broadcom Corporation$
 *
 *  Version: $Id:   /h03_u01/pvcs/Combined/PVCS8.0/Engg_Projects/archives/Trident/SBI/src/usb/include/sbi.h-arc   1.0   09 Dec 2009 03:51:28   moosa_b1  $
 *
 *****************************************************************************/

#ifndef __SBI_H
#define __SBI_H


#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <inttypes.h>
#endif


/*****************************************************
 *****       Defintion of SBI Header            *****
 *****************************************************/
typedef struct UnAuthenticatedHeader_t {
	uint32_t UnauthFlags;       /* see below  */
	uint32_t UMCCycles;         /* If either umc field not 0: Used to set the UMC cycles register */
	uint32_t UMCOpMode;         /* If either umc field not 0: Used to set the UMC OpMode register */
	uint32_t ClkConfig;         /* One of the three clock configurations supported by Bootstrap, see Clock Config
                                                               table in BCM5892 Bootstrap design document */
	uint32_t BSCTIM;         /* Used to set the BSCTIM reigster */
	uint32_t BSCCLKEN;         /* If bit#0 is not zero: used to set BSCCLKEN register */
	uint32_t Reserved;
	uint32_t SPIDividers;       /* If not 0: SCR (high 16 bit) & SSPCPSR (low 16 bit) clock dividers */
	uint32_t SPIVendor;         /* If not 0: highest bit means if using fast mode */
	uint32_t SPISize;           /* the size of SPI flash */
	uint32_t RecAddress;        /* Asymmetric Recovery Address to jump to if BBL is cleared and
                                                               DCFG[use_bbl_kek] is set */
    /* Any other fields should be placed here */

	/* CRC and field should be the last field, CRC should be computed on all
	   other fields in this structure excluding crc field */
	uint32_t crc;
} UNAUTHENT_SBI_HEADER;

typedef struct AuthenticatedHeader_t {
	uint32_t HeaderFlags;          /* Indicates SBI options */
	uint32_t ReleaseInfoOffset;    /* Offset to release info paylaod */
	uint32_t ReleaseInfoLength;    /* length of release info payload */
	uint32_t PublicKeyOffset;      /* Points to the public key */
	uint32_t ChainTrustDepth;      /* 0 means no chain of trust */
	uint32_t BootLoaderCodeOffset; /* Offset to bootloader payload */
	uint32_t BootLoaderCodeLength; /* length of bootloader payload */
	uint32_t AuthenticationPayloadOffset;   /* End of image, start of Auth data */
	uint32_t AuthLen; 						/*Length of Final image signature */
    uint32_t Reserved[2];
	uint32_t MagicNumber;          /* Sentinel for header */
} AUTHENTICATED_SBI_HEADER;


typedef struct AuthenticatedBootImageHeader_t {
	UNAUTHENT_SBI_HEADER		unh;  /* Not covered by DSA signature */
	AUTHENTICATED_SBI_HEADER	sbi;  /* Start of signed data */
} SBI_HEADER;


#define SBITotalLength		AuthenticationPayloadOffset

/*****************************************************
 *****        SBI Header Flags                   *****
 *****************************************************/
#define SBI_HAS_BOOT_IMAGE				0x00000001
#define SBI_HAS_RELEASE_INFO			0x00000004
#define SBI_AUTHFLAG_ENCR_AES			0x00000100 /* if the SBI Data payload is encrypted */

#define SBI_MAGIC_NUMBER				0x45F2D99A
#define SBI_VER_SIZE					5

/* UnauthFlags, the first 16 bits are the magic number */
#define SBI_UNAUTHFLAG_SPI_FAST_MODE    0x00000001 /* use SPI fast mode to read */
#define SBI_UNAUTHFLAG_RSA_SIGNATURE	0x00000010
#define SBI_UNAUTHFLAG_K_BASE_PUB_DAUTH 0x00000020



#define SBI_RESERVED_MAGIC_MASK			0xFFFF0000

#define SBI_BBL_STATUS_DONT_CARE     (0) /* Run SBI regardless of BBL state */
#define SBI_BBL_STATUS_KEY_VALID	 (1) /* Run SBI if BBL KEK is valid */
#define SBI_BBL_STATUS_KEY_CLEARED   (2) /* Run SBI if BBL KEK is cleared */
#define SBI_BBL_STATUS_UNLOCKED      (3) /* Run SBI if BBL lock bits are not set */
#define SBI_BBL_STATUS_KEY_INVALID   (4) /* Run SBI if BBL KEK is invalid */
#define SBI_BBL_STATUS_BATTERY_OFF   (5) /* Secure Bootloader internal use only,
                                            Should NOT be used by SBI */
#define SBI_BBL_STATUS_ACC_TIMEOUT   (6) /* Timeout on BBL indirect access */

/* clock control values */
#define SBI_GET_CLK_P1(v1, v2)          ((v1 & 0x000000F) >> 0)  /* bits  3: 0 */
#define SBI_GET_CLK_P2(v1, v2)          ((v1 & 0x00000F0) >> 4)  /* bits  7: 4 */
#define SBI_GET_CLK_N(v1, v2)           ((v1 & 0x001FF00) >> 8)  /* bits 16: 8 */
#define SBI_GET_CLK_M1(v1, v2)          ((v2 & 0x00000FF) >> 0)  /* bits  7: 0 */
#define SBI_GET_CLK_M2(v1, v2)          ((v2 & 0x000FF00) >> 8)  /* bits 15: 8 */

#define SBI_MAX_CLK_VAL1                0x001FFFF
#define SBI_MAX_CLK_VAL2                0x000FFFF

/* SMC related info */
#define SBI_GET_SMC_MEMTYPE(v)          ((v & 0xFFFF0000) >> 16)
#define SBI_GET_SMC_MEMID(v)            ((v & 0x0000FFFF))

/* SPI related info */
#define SBI_GET_SPI_FASTMODE(v)         ((v & 0x80000000))
#define SBI_GET_SPI_MEMTYPE(v)          ((v & 0x0F000000)) /* MEMORY TYPE, see defines below */
#define SBI_GET_SPI_MEMWIDTH(v)         ((v & 0x00F00000)) /* MEMORY WIDTH, see defines below */
#define SBI_GET_SPI_VENDOR(v)           ((v & 0x000FFFFF))

#define SBI_SPI_TYPE_FLASH              0x00000000
#define SBI_SPI_TYPE_SRAM               0x01000000
#define SBI_SPI_TYPE_PSRAM              0x02000000

#define SBI_SPI_WIDTH_8                 0x00000000
#define SBI_SPI_WIDTH_16                0x00100000

/* Hibernation flag present in DDRAM */
#define HIBERNATE_PWR_FLAG_RAM_ADDR		0x40DFFF00


typedef struct SBIScpecificHeader_t {
	uint32_t Signature;        /* see below == SBI_HEADER_NUMBER */
    uint32_t HeaderLen;        /* Header Length  */
    uint8_t  Svid;             /* SBI svid number  */
    uint8_t  SBIVersion[SBI_VER_SIZE];    /* SBI Version */
    uint8_t  BuildDate[12];    /* SBI Date of creation */
    uint8_t  Spare0[2];        /* spare  */
    uint32_t Spare[9];         /* spare  */
} SBISpecificHeader;  /* size == 64 bytes */


/*****************************************************
 *****        SBI Header Flags                   *****
 *****************************************************/

#define SBI_HEADER_NUMBER   0xA19BC38F

/*****************************************************
 *****        SBI Functions Prototypes           *****
 *****************************************************/
int InitializeSBI(SBI_HEADER *header);
int PrintSBI(SBI_HEADER *header);
int VerifySBI(char *filename);
int SBIAddReleasePayload(SBI_HEADER *header, char *filename);
int SBIAddBootLoaderPayload(SBI_HEADER *header, char *filename, char *ivfilename, uint8_t big_endian);
int SBIAddSBIcodePayload(SBI_HEADER *header, char *filename);
int CreateLocalSecureBootImage(SBI_HEADER *h, char *file, char *bblkey);
int SBIAddPubkeyChain(SBI_HEADER *h, char *filename);
int WriteSBI(SBI_HEADER *header, char *filename);
int  SBIAddDauthKey( SBI_HEADER *h, char *DauthKey, int dauth_pubin);
void FillUnauthFromConfigFile(SBI_HEADER *sbi, char* ConfigFileName);
int SBIAddRecoveryImage(SBI_HEADER *h, char *filename);


#define SBI_BOOT_MAX			3
#define SBI_UNSTABLE_FLAG_ON	0xB2D58B32


typedef struct
{
	uint8_t SMMSha[32];
	uint32_t SMMShaValidation;
	uint8_t VxSha[32];
	uint32_t VxShaValidation;
	uint32_t SystemUnstableFlag;
	uint8_t BblBootCount;

} SBIGlobalInfo;


#define SBI_OS_GLOBAL    0xA3F6B17E
#define SBI_OS_VER       0x0

typedef struct
{
	uint32_t Signature;			/* Struct signature == SBI_OS_GLOBAL */
	uint32_t Version;			/* Struct version  == SBI_OS_VER */
	uint32_t CIBStatus;			/* see cib.h for enum */
	uint32_t CIBAddress;		/* CIB address in NAND only valid if CIBStatus == CIB_AUTH_OK */
	uint32_t SBIStatus;			/* */
	uint32_t LastResetStatus;	/* */
	uint32_t PmicStatus;		/* pmic status register before it was reset */

} OSGlobalInfo;


#endif /* __SBI_H. */
