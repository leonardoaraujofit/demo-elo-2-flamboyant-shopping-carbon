#ifndef __BBSRAM_PUB_H__
#define __BBSRAM_PUB_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @file bbsram_pub.h
 *
 * This file exists to allow the public API file for diag_counters use the
 * following #define for LOG size.
 */
#define BBSRAM_SYSTEM_LOG_SIZE                     (1024)

#ifdef __cplusplus
}
#endif

#endif
