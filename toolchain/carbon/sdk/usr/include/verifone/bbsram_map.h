/****************************************************************************
 *                                                                          *
 * FILE NAME:   bbsram_map.h                                                *
 *                                                                          *
 * MODULE NAME: Battery Backed SRAM memory map                              *
 *                                                                          *
 * PROGRAMMER:  Oren Sokolowsky                                             *
 *                                                                          *
 * DESCRIPTION: Description of the 14KB Battery Backed SRAM memory map	    *
 *             -The 14kb is devided to 2 regions 6kb and 8kb                *
 *             -The 6KB region is always battery backed up                  *
 *             -The 8KB region can be programmed on/off battery backed up   *
 *                                                                          *
 * REVISION:    01.00                                                       *
 *                                                                          *
 ****************************************************************************/

#ifndef __BBSRAM_MAP_H__
#define __BBSRAM_MAP_H__

#include "bbsram_pub.h"

#ifdef __cplusplus
extern "C" {
#endif

#define BBSRAM_START_ADDRESS 0x01008000
#define BBSRAM_END_ADDRESS   0x0100B7FF

#define BBSRAM_VAULT_PERMANENT_DATA_START_ADDRESS   (BBSRAM_START_ADDRESS)
#define BBSRAM_VAULT_PERMANENT_DATA_SIZE            (1024)

#define BBSRAM_RESERVED_AREA_START                 (BBSRAM_VAULT_PERMANENT_DATA_START_ADDRESS +BBSRAM_VAULT_PERMANENT_DATA_SIZE)
#define BBSRAM_RESERVED_AREA_SIZE                  (4*1024)


#define BBSRAM_SYSTEM_LOG_START_ADDRESS            (BBSRAM_RESERVED_AREA_START + BBSRAM_RESERVED_AREA_SIZE)

// Offset in daigs area of BBSRAM for SYSTEM_FLAGS
// SYSTEM_FLAGS is 32-bit flags
#define BBSRAM_SYSTEM_FLAGS_OFFSET					((BBSRAM_SYSTEM_LOG_SIZE/4)-1)
#define BBSRAM_SYSTEM_FLAG_KERNEL_CONSOLE			(0x00000001)
#define BBSRAM_SYSTEM_FLAG_UBOOT_CONSOLE			(0x00000002)
#define BBSRAM_SYSTEM_FLAG_UBOOT_FIRST_RUN			(0x00000004)

/* Hibernation flags */
/* last 4 bytes are reservered for hibernation flag at the end of the 14KB BBRAM*/
#define HIBERNATE_PWR_FLAG_ADDR						(BBSRAM_START_ADDRESS + (14*1024) - 4)
#define HIBERNATE_PWR_FLAG_ADDR_STR					"0x0100B7FC"
#define HIBERNATE_PWR_FLAG_VALUE_ON					0xDABBAD00
#define HIBERNATE_PWR_FLAG_VALUE_OFF					0xA5A5A5A5
#define HIBERNATE_PWR_FLAG_RAM_ADDR					0x40DFFF00
#define HIBERNATE_RETURN_ADDR						(HIBERNATE_PWR_FLAG_RAM_ADDR + 4)

#ifdef __cplusplus
}
#endif

#endif /*__BBSRAM_MAP_H__*/
