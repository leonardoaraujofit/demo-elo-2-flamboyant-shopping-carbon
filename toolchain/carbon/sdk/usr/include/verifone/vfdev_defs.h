/* CiR - 16Oct2014. */

#ifndef __VFDEV_DEFS_H
#define __VFDEV_DEFS_H

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Description of typical use of each macro type and treatment of in-line code
 * additions for automated code clean-up operations.  For all cases remove macro
 * invocation itself including containing brackets, plus any further content
 * listed below.
 *
 * VFDEV_VATS
 *   Use when a single-line statement should be present only if VATS is
 *   enabled.  An empty statement will remain for non-VATS cases.  Bear in mind
 *   that this will have effect in conditional blocks.
 *   Cleanup: remove all macro content.
 *
 * VFDEV_VATS_OPTION
 *   Use when an expression should be present regardless of whether VATS is
 *   enabled or not.  The first argument is the non-VATS expression; the second
 *   argument is the VATS expression.
 *   Clean-up: remove comma and all macro content thereafter.
 *
 * VFDEV_VATS_AND
 *   Use when VATS being enabled requires that an additional condition must be
 *   added to existing code with the && operator.
 *   Clean-up: remove all except the first macro argument.
 *
 * VFDEV_VATS_OR
 *   Use when VATS being enabled requires that an additional condition must be
 *   added to existing code with the || operator.
 *   Clean-up: remove all except the first macro argument.
 *
 * vats_if
 *   Use when VATS being enabled requires that a conditional branch must be
 *   chosen.
 *   Clean-up: remove conditional expression and success branch content
 *   including any enclosing braces.
 *
 * vats_else
 *   Use to identify the non-success branch of a conditional started with
 *   vats_if().
 *   Clean-up: remove branch content including any enclosing braces.
 */


/* Bit positions representing development modes. */
#define __VFDEV_MODE_vats	(1UL << 0)
#define __VFDEV_MODE_debug	(1UL << 1)
#define __VFDEV_MODE_assert	(1UL << 2)

#define __VFDEV_MODE__endmarker	(1UL << 31)

/* Test if bit position set using mode name as key. */
#define __VFDEV_MODE(mode)	((VFDEV_MODES_MAP) & __VFDEV_MODE_ ## mode)

/*
 * Inject a fingerprint into the final linked object so that it can be detected
 * after images are created.  This is bold; allocating object memory in a header
 * file is bad practice, but since this will not make it into production builds
 * we can tolerate it.
 */
#define VFDEV_FINGERPRINT(mode) \
	static const char __VFDEV_MARKER ## mode[] = "\a\a!VFDEV_MARKER:" #mode "!"

#if __VFDEV_MODE(vats)

 #define VFDEV_VATS(x...)	x
 #define VFDEV_VATS_OPTION(disabled, enabled)	enabled
 #define VFDEV_VATS_AND(x, y)	((x) && (y))
 #define VFDEV_VATS_OR(x, y)	((x) || (y))
 #define vats_if	if
 #define vats_else	else

#else /* __VFDEV_MODE(vats). */

 #define VFDEV_VATS(x...)
 #define VFDEV_VATS_OPTION(disabled, enabled)	disabled
 #define VFDEV_VATS_AND(x, y)	(x)
 #define VFDEV_VATS_OR(x, y)	(x)
 #define vats_if(x...)		if (0)
 #define vats_else(x...)	else if (0)

#endif /* __VFDEV_MODE(vats). */

#ifdef __cplusplus
}
#endif

#endif /* __VFDEV_DEFS_H. */
