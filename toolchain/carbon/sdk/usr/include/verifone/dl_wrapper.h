/****************************************************************************
 *                                                                          *
 * FILE NAME:    dl_wrapper.h                                               *
 *                                                                          *
 * COMPANY:      Verifone                                                   *
 *                                                                          *
 * DESCRIPTION:  dynamic link library function wrapper header               *
 *               this header file is used to assist usage of functions      *
 *               implemented in external library (shared object)            *
 *               Example can be found at end of file                        *
 *                                                                          *
 ****************************************************************************/

#ifndef _DL_WRAPPER_H_
#define _DL_WRAPPER_H_

#include <dlfcn.h>

#ifdef __cplusplus
extern "C" {
#endif

#define GEN_LIB_API_TEMPLATE(lib) \
static void* lib##_hdl=0;\
static void* lib##_open(void)\
{\
	if(lib##_hdl == 0)\
	{\
		if ((lib##_hdl = (void*)dlopen("lib"#lib".so", RTLD_LOCAL | RTLD_LAZY)) == 0) \
		{ \
			printf("unable to find %s library on the device. reason %s","lib"#lib".so",(dlerror()));\
		}\
	}\
	return lib##_hdl;\
}\
static void lib##_close(void)\
{\
	if( (lib##_hdl != 0) && (dlclose(lib##_hdl)!= 0) )\
	{\
		printf("unable to close %s library on the device. reason %s",#lib,(dlerror()));\
		return;\
	}\
	lib##_hdl=0;\
}\
static void* lib##_loadFunc(char* _func)\
{\
	void* p=0;\
	if(lib##_open()==0) return NULL;\
	if( (p = (void*)dlsym( lib##_hdl ,_func)) == 0)\
	{\
		printf("unable to find %s symbol. reason: %s\n", _func,(dlerror())); \
	}\
	return p;\
}


#define LIB_LOAD(lib,err) \
if ( lib##_open() == 0) return (err); \
while(0)


#define LIB_LOAD_VR(lib) \
if ( lib##_open() == 0) return; \
while(0)

#define LIB_UNLOAD(lib) lib##_close()

#define LOAD_FUNC_TYPE(x,lib,err) \
__##x _##x = (__##x)lib##_loadFunc(#x);\
if (_##x == 0) return (err);\
while(0)

#define LOAD_FUNC_TYPE_VR(x,lib) \
__##x _##x = (__##x)lib##_loadFunc(#x);\
if (_##x == 0) return;\
while(0)

#define FUNC_RUN(x,...) _##x( __VA_ARGS__ )



/////////////////////////////USAGE example//////////////////////////////////
/////    #include "verifone/dl_wrapper.h"
/////    typedef int (*__vfiSec_GetVaultVersion)(unsigned char * pVersion);   /* note: such typedef to the desirable api with "__"  prefix  must exist ! */
/////
/////    GEN_LIB_API_TEMPLATE(vfiVaultapi); // gen the api's
/////    int main()
/////    {
/////        int version_len;
/////        char line[20];
/////
/////        LOAD_FUNC_TYPE(vfiSec_GetVaultVersion,vfiVaultapi,-1); //load api, in case of error return -1.
/////        version_len = FUNC_RUN(vfiSec_GetVaultVersion,line);   //run api with comma seperated params
/////        LIB_UNLOAD(vfiVaultapi); // don't forget to unload the library at the end of usage
/////
/////        printf("version_len=%d\n");
/////        return 0;
/////    }
///// 
/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif /* _DL_WRAPPER_H_ */
