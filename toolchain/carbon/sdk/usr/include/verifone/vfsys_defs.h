
#ifndef _VFSYS_DEFS_H__
#define _VFSYS_DEFS_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* Define relating to uids and gids used on the system */
#define MAX_USERS_PER_SIGNER		16
#define MIN_SIGNER_UID				500
#define SIGNER_GID					(MIN_SIGNER_UID + MAX_USERS_PER_SIGNER)
#define MIN_SYSTEM_UID				(MIN_SIGNER_UID + 100)
#define SYSTEM_GID					(MIN_SYSTEM_UID + MAX_USERS_PER_SIGNER)
#define COMMON_GID					700
#ifndef RAPTOR
#define SYSMODE_SHARE_GID_START		710		// 710 to 715 - usr1sys to usr6sys
#define APP_SEC_SHARE_GID_START		716		// 716 to 731
#define SYS_SEC_SHARE_GID_START		732		// 732 to 747
#define SYSMODE_SHARE_GID_USR7		748		// 748 to 757 - usr7sys to usr16sys
#else
#define SYSMODE_SHARE_GID_START		710		// 710 to 725 - usr1sys to usr16sys
#define APP_SEC_SHARE_GID_START		726		// 726 to 741
#define SYS_SEC_SHARE_GID_START		742		// 742 to 757
#endif

/* Define Secure Installers Pkg and Bdl Info structure */
#define MAX_PKG_NAME_LEN			24
#define MAX_PKG_VERSION_LEN			16
#define MAX_PKG_USER_LEN			8
#define MAX_PKG_TYPE_LEN			16
#define MAX_SIGNER_LEN				4 // os, sys, user
#define MAX_PKG_DATE_LEN			10 // e.g YYYY-MM-DD
#define MAX_PKG_CATEGORY_LEN		16
#define MAX_PKG_OPTION_LEN			64

typedef struct
{
	char name[MAX_PKG_NAME_LEN + 1];
	char version[MAX_PKG_VERSION_LEN + 1];
	char user[MAX_PKG_USER_LEN + 1];
	char signer[MAX_SIGNER_LEN + 1];
	char type[MAX_PKG_TYPE_LEN + 1];
	char bundlename[MAX_PKG_NAME_LEN + 1];
	char bundleversion[MAX_PKG_VERSION_LEN + 1];
	char bundledate[MAX_PKG_DATE_LEN + 1];

} SecinsPkgInfo;


typedef struct
{
	char name[MAX_PKG_NAME_LEN + 1];
	char version[MAX_PKG_VERSION_LEN + 1];
	char user[MAX_PKG_USER_LEN + 1];
	char category[MAX_PKG_CATEGORY_LEN + 1];
	char date[MAX_PKG_DATE_LEN + 1];
	char option[MAX_PKG_OPTION_LEN + 1];

} SecinsBdlInfo;


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _VFSYS_DEFS_H__ */
