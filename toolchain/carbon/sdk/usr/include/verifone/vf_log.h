/****************************************************************************
 *                                                                          *
 * FILE NAME:    vf_log.h                                                   *
 *                                                                          *
 * COMPANY:      Verifone                                                   *
 *                                                                          *
 * DESCRIPTION:  Defines for common style of the output log.                *
 *                                                                          *
 ****************************************************************************/

/*
 * Still in development!
 *
 * TODO:
 *     - Log macros for userspace;
 *     - The separate switchable logs for debugging (within the module);
 */


/****************************************************************************
 * Defines:
 *     VFLOG(format, ...);              // VFLOG_WARN level short version
 *     VFLOG_CRIT(format, ...);
 *     VFLOG_ERR(format, ...);
 *     VFLOG_WARN(format, ...);
 *     VFLOG_INFO(format, ...);
 *     VFLOG_NOTICE(format, ...);
 *     VFLOG_DEBUG(format, ...);
 *
 *     VFLOG_DEBUG_HIB(format, ...);    // HIB_DEBUG() replacement
 *
 *     VFLOG_ERR_FAILED(name);
 *     VFLOG_ERR_FAILED_EXT(name, pname, pval);
 *     VFLOG_ERR_FAILED_EXT2(name, pname, pval);
 *
 *
 ****************************************************************************
 * Global configuration variables (must be define here or in make command):
 *
 *    VFLOG_GLB_FILE_NAME_AS_PREFIX
 *        Ignore VFLOG_MODULE_PREFIX and always use file name as module
 *        identifier.
 *
 *    VFLOG_GLB_HIDE_FUNCTION_NAME
 *        Function names will not be displayed in output log.
 *
 *
 ****************************************************************************
 * Before including of the <verifone/vf_log.h> file the following local
 * configuration variables can be defined:
 *
 *    VFLOG_MODULE_PREFIX <string>
 *        Module identifier. If not defined then __FILE__ macros is used.
 *
 *    VFLOG_DEBUG_MODE <0 | 1_or_not_defined | 2>
 *        VFLOG_DEBUG() output mode. One of the following:
 *            0 - do not print out.
 *            1 - print out only if DEBUG is defined (default).
 *            2 - always print out.
 *
 *    VFLOG_HIDE_FUNCTION_NAME
 *        Function names will not be displayed in output log.
 *
 *
 ****************************************************************************
 * After <verifone/vf_log.h> file was included, the following status values
 * can be used:
 *
 *    VFLOG_STATUS_DEBUG_ACTIVE     0 - debug logs will not be printed;
 *                                  1 - debug logs will be printed.
 *
 ****************************************************************************
 * Kernel output sample:
 *
 *                +----------------  VFLOG_MODULE_PREFIX
 *                |       +---------  Message log level
 *                |       |     +---  Function
 *                |       |     |
 * [   12.440000] PM.arch:ERROR:pm_set_alarm: Invalid time specified!
 *                                            |
 *                                Message  ---+
 *
 * 'grep' can be easy used to find what module/file is responsible for log
 *  output in case if module identfier is not a file name.
 *  For example, for log with module prefix "PM.arch":
 *  # grep -r VFLOG_MODULE_PREFIX * | grep "PM.arch"
 *
 */


#ifndef _VF_LOG_H_
#define _VF_LOG_H_


/*
 * TODO: Is there a possibility to detect production build?
 * Then would be useful to do something like that:
 */
#if defined(THIS_IS_PRODUCTION_BUILD) && !defined(DEBUG)
#	ifdef VFLOG_DEBUG_MODE
#		undef VFLOG_DEBUG_MODE
#	endif
#	ifdef HIBERNATION_DEBUG
#		undef HIBERNATION_DEBUG
#	endif
/*
 * TODO: Perhaps names of functions shall be concealed in production build...
 * #	ifndef VFLOG_GLB_HIDE_FUNCTION_NAME
 * #		define VFLOG_GLB_HIDE_FUNCTION_NAME
 * #	endif
 */
#endif


#if defined(VFLOG_GLB_FILE_NAME_AS_PREFIX) && defined(VFLOG_MODULE_PREFIX)
#	undef VFLOG_MODULE_PREFIX
#endif

#if defined(VFLOG_GLB_HIDE_FUNCTION_NAME) && !defined(VFLOG_HIDE_FUNCTION_NAME)
#	define VFLOG_HIDE_FUNCTION_NAME
#endif


#ifndef VFLOG_MODULE_PREFIX
#	define VFLOG_MODULE_PREFIX		__FILE__
#endif

#ifndef VFLOG_DEBUG_MODE
#	define VFLOG_DEBUG_MODE			1
#endif

#define VFLOG_STATUS_DEBUG_ACTIVE		0
#if VFLOG_DEBUG_MODE > 1 || (VFLOG_DEBUG_MODE == 1 && defined(DEBUG))
#	define VFLOG_STATUS_DEBUG_ACTIVE	1
#endif

#ifdef __KERNEL__ /* KERNEL ***************************************************/

#ifndef pr_warn
#	define	pr_warn		pr_warning
#endif

#ifdef VFLOG_HIDE_FUNCTION_NAME
#	define VFLOG_BASE(proc, type, format, ...) \
		proc("%s:%s: " format "\n", \
		VFLOG_MODULE_PREFIX, type,  ##__VA_ARGS__)
#	define VFLOG_BASE_SHORT(proc, format, ...) \
		proc("%s: " format "\n", \
		VFLOG_MODULE_PREFIX, ##__VA_ARGS__)
#else
#	define VFLOG_BASE(proc, type, format, ...) \
		proc("%s:%s:%s: " format "\n", \
		VFLOG_MODULE_PREFIX, type, __func__, ##__VA_ARGS__)
#	define VFLOG_BASE_SHORT(proc, format, ...) \
		proc("%s:%s: " format "\n", \
		VFLOG_MODULE_PREFIX, __func__, ##__VA_ARGS__)
#endif

#define VFLOG(format, ...) \
		VFLOG_BASE_SHORT(pr_warn, format, ##__VA_ARGS__)

#define VFLOG_CRIT(format, ...) \
		VFLOG_BASE(pr_crit, "CRITICAL", format, ##__VA_ARGS__)
#define VFLOG_ERR(format, ...) \
		VFLOG_BASE(pr_err, "ERROR", format, ##__VA_ARGS__)
#define VFLOG_WARN(format, ...) \
		VFLOG_BASE(pr_warn, "Warning", format, ##__VA_ARGS__)
#define VFLOG_INFO(format, ...) \
		VFLOG_BASE(pr_info, "info", format, ##__VA_ARGS__)
#define VFLOG_NOTICE(format, ...) \
		VFLOG_BASE(pr_notice, "notice", format, ##__VA_ARGS__)

#if VFLOG_DEBUG_MODE == 1
#	define VFLOG_DEBUG(format, ...) \
		VFLOG_BASE(pr_debug, "DEBUG", format, ##__VA_ARGS__)
#elif VFLOG_DEBUG_MODE >= 2
#	define VFLOG_DEBUG(format, ...) \
		VFLOG_BASE(pr_crit, "DEBUG", format, ##__VA_ARGS__)
#else
#	define VFLOG_DEBUG(format, ...)
#endif

#ifdef HIBERNATION_DEBUG
#	define VFLOG_DEBUG_HIB(format, ...) \
		VFLOG_BASE(pr_crit, "DEBUG-HIBERNATE", format, ##__VA_ARGS__)
#else
#	define VFLOG_DEBUG_HIB(format, ...)
#endif


#else /* not KERNEL ***********************************************************/
/*
 * TODO: to write the defines for applications output log
 */
#	define VFLOG_DEBUG_HIB(format, ...) \
		HIB_DEBUG(format, ##__VA_ARGS__)
#endif


/*
 * Some common defines to output "<something> failed!" type messages.
 */
#define VFLOG_ERR_FAILED(name) \
		VFLOG_ERR("%s failed!", name)
#define VFLOG_ERR_FAILED_EXT(name, pname, pval) \
		VFLOG_ERR("%s failed! %s = %d", name, pname, (int) pval)
#define VFLOG_ERR_FAILED_EXT2(name, pname, pval) \
		VFLOG_ERR("%s failed! %s = %d (0x%08x)", \
		name, pname, (int) pval, (unsigned int) pval)


#endif /* _VF_LOG_H_ */
