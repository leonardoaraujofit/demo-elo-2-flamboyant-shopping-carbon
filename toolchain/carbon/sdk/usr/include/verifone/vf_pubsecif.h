
#ifndef _VF_PUBSECIF_H__
#define _VF_PUBSECIF_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Secure Applet App IDs */
#define PG_SECURE_APP_ID				0x8100
#define IPP_APP_ID						0x8102
#define KEYSTORE_APP_ID					0x8103
#define PINENTRY_APP_ID					0x8104
#define RMCERT_APP_ID					0x8105
#define EC_APP_ID						0x8106
#define AESDUKPT_APP_ID					0x8107
#define KEYPAD_APP_ID					0x8121
#define SECMODE_SECURE_APP_ID			0x8200 /* Cryptoapp */
#define AESDESAPP_APP_ID				0x8201 /* AESDESapp */
#define TESTAPP_APP_ID					0x8202 /* TESTapp */
#define TI_AESAPP_APP_ID				0x8203
#define SREDAPP_APP_ID					0x8204
#define TPMSS_APP_ID					0x8400
#define NVUSR_APP_ID					0x8401
#define LIFECYCLE_APP_ID				0x8600
#define VSS_SECURE_APP_ID				0x8700
#define POSEIDON_APP_ID					0x8710
#define SECTEST_SECURE_APP_ID			0x8800


/* Some Generic Service Ids (used by all secure applets) */
#define GENERIC_SERVICE_ID				0x80000000
#define APP_VERSION_SERVICE_ID			(GENERIC_SERVICE_ID + 0)


/* Generic Tag Numbers */
#define APP_NAME_TAGNUM					126	/* DF7E */
#define APP_VER_TAGNUM					127 /* DF7F */


/* Nvusr services ids */
#define NVUSR_CLEAR_SERVICE_ID			1
#define NVUSR_SET_SERVICE_ID			2
#define NVUSR_GET_SERVICE_ID			3
#define NVUSR_ADD_SERVICE_ID			4
#define NVUSR_CLEAR_BITS_SERVICE_ID		5
#define NVUSR_SET_BITS_SERVICE_ID		6
#define NVUSR_CLEAR_ALL_SERVICE_ID		7
#define NVUSR_SET_ALL_SERVICE_ID		8
#define NVUSR_GET_ALL_SERVICE_ID		9
#define NVUSR_KERNEL_SERVICE_ID			0x8402

/*
 * The following are used by Nvusr as secondary service ids for the
 * NVUSR_KERNEL_SERVICE_ID service.
 */
#define KERNEL_NVUSR_CLEAR				1
#define KERNEL_NVUSR_SET				2
#define KERNEL_NVUSR_GET				3
#define KERNEL_NVUSR_ADD				4
#define KERNEL_NVUSR_CLEARBITS			5
#define KERNEL_NVUSR_SETBITS			6

/* Nvusr Tag ids */
#define TAG_NVUSR_INT_INPUT				0x1
#define TAG_NVUSR_INT_OUTPUT			0x2
#define TAG_NVUSR_INDEX					0x3
#define TAG_NVUSR_MASK					0x4
#define TAG_NVUSR_WHOLE					0x5

/* Nvusr error codes */
#define ERR_NVUSR_READ					1
#define ERR_NVUSR_WRITE					2
#define ERR_NVUSR_INVARG				3

/* Nvusr misc defines */
#define NVUSR_SIZE						1024


/* Cryptoapp service ids */
#define SECTEST_SERVICE_ID				0x8200
#define SECLOG_SERVICE_ID				0x8300
#define VFICRYPT_SERVICE_ID				0x8500
#define VF_HMAC_DMA_SERVICE_ID			0x8900

#define VFICRYPT_LIBCRY_RSA_PUB_OP_SERVICE_ID			(VFICRYPT_SERVICE_ID + 0)
#define VFICRYPT_LIBCRY_RNG_EX_SERVICE_ID				(VFICRYPT_SERVICE_ID + 1)
#define VFICRYPT_VAULT_OPAQUE_SERVICE_ID				(VFICRYPT_SERVICE_ID + 2)
#define VFICRYPT_LIBCRY_RSA_OP_SERVICE_ID				(VFICRYPT_SERVICE_ID + 3)
#define VFICRYPT_LIBCRY_LOAD_KEY_SERVICE_ID				(VFICRYPT_SERVICE_ID + 4)
#define VFICRYPT_LIBCRY_SHA_SERVICE_ID					(VFICRYPT_SERVICE_ID + 5)
#define VFICRYPT_LIBCRY_RNG_SERVICE_ID					(VFICRYPT_SERVICE_ID + 6)
#define VFICRYPT_CRYPTO_READWRITE_SERVICE_ID			(VFICRYPT_SERVICE_ID + 7)
#define VFICRYPT_LIBCRY_CREATE_SKEY_SERVICE_ID			(VFICRYPT_SERVICE_ID + 8)
#define VFICRYPT_CRYVER_SERVICE_ID						(VFICRYPT_SERVICE_ID + 9)
#define VFICRYPT_LIBCRY_SHA_INIT_SERVICE_ID				(VFICRYPT_SERVICE_ID + 10)
#define VFICRYPT_LIBCRY_SHA_BUFF_SERVICE_ID				(VFICRYPT_SERVICE_ID + 11)
#define VFICRYPT_LIBCRY_SHA_TERM_SERVICE_ID				(VFICRYPT_SERVICE_ID + 12)
#define VFICRYPT_VAULT_FILE_SIGN_OTP_ID					(VFICRYPT_SERVICE_ID + 13)
#define VFICRYPT_LIBCRY_TOKEN_CHALLENGE_SERVICE_ID		(VFICRYPT_SERVICE_ID + 14)
#define VFICRYPT_LIBCRY_TOKEN_EXECUTE_SERVICE_ID		(VFICRYPT_SERVICE_ID + 15)
#define VFICRYPT_LIBCRY_RSA_PRIVATE_OP_SERVICE_ID		(VFICRYPT_SERVICE_ID + 16)
#define VFICRYPT_LIBCRY_ECDH_COMPUTE_KEY_SERVICE_ID		(VFICRYPT_SERVICE_ID + 17)
#define VFICRYPT_LIBCRY_CHECK_SYSTEM_KEY_SERVICE_ID		(VFICRYPT_SERVICE_ID + 18)
#define VFICRYPT_VAULT_OPAQUE_KEYTYPE_SERVICE_ID		(VFICRYPT_SERVICE_ID + 19)
#define VFICRYPT_VAULT_TR34_SERVICE_ID					(VFICRYPT_SERVICE_ID + 20)
#define VFICRYPT_LIBCRY_SHAHMAC_KEY_SERVICE_ID			(VFICRYPT_SERVICE_ID + 21)
#define VFICRYPT_VAULT_TR31_SERVICE_ID					(VFICRYPT_SERVICE_ID + 22)
#define VFICRYPT_LIBCRY_EC_KEY_SERVICE_ID				(VFICRYPT_SERVICE_ID + 23)

/* VFICRYPT_DIAGNOSTICS services are not available in the production cryptoapp */
#define VFICRYPT_DIAGNOSTICS_RSA_CREATESESSKEYCONTEX_SERVICE_ID		(VFICRYPT_SERVICE_ID + 40)
#define VFICRYPT_DIAGNOSTICS_RSA_GETSYMMKEYCONTEXTS_SERVICE_ID		(VFICRYPT_SERVICE_ID + 41)
#define VFICRYPT_DIAGNOSTICS_RSA_PUTSESSKEYCONTEXT_SERVICE_ID		(VFICRYPT_SERVICE_ID + 42)
#define VFICRYPT_DIAGNOSTICS_RSA_RELEASESESSKEYCONTEXT_SERVICE_ID	(VFICRYPT_SERVICE_ID + 43)
#define VFICRYPT_DIAGNOSTICS_INITKEYSLIST_SERVICE_ID				(VFICRYPT_SERVICE_ID + 44)
#define VFICRYPT_DIAGNOSTICS_REMOVEKEYSLIST_SERVICE_ID				(VFICRYPT_SERVICE_ID + 45)
#define	VFICRYPT_DIAGNOSTICS_STATUS_PASS_SERVICE_ID					(VFICRYPT_SERVICE_ID + 46)
#define	VFICRYPT_DIAGNOSTICS_STATUS_FAIL_SERVICE_ID					(VFICRYPT_SERVICE_ID + 47)
#define	VFICRYPT_DIAGNOSTICS_GENERIC_SERVICE_ID						(VFICRYPT_SERVICE_ID + 48)
#define	VFICRYPT_DIAGNOSTICS_GENERATEKEKCMACGEKCWK_SERVICE_ID		(VFICRYPT_SERVICE_ID + 49)
#define VFICRYPT_CRYPTO_READWRITE_BUFFER_SERVICE_ID					(VFICRYPT_SERVICE_ID + 50)
#define VFICRYPT_DIAGNOSTICS_RSA_REPLACERKLPRIVATEKEYWITHKEYCONTEXT_SERVICE_ID	(VFICRYPT_SERVICE_ID + 51)
#define VFICRYPT_DIAGNOSTICS_CWKCALC_SERVICE_ID						(VFICRYPT_SERVICE_ID + 52)
#define VFICRYPT_DIAGNOSTICS_GEKCALC_SERVICE_ID						(VFICRYPT_SERVICE_ID + 53)
#define VFICRYPT_DIAGNOSTICS_TEST_SERVICE_ID						(VFICRYPT_SERVICE_ID + 54)
#define VFICRYPT_DIAGNOSTICS_SETGEKCWK_SERVICE_ID					(VFICRYPT_SERVICE_ID + 55)
#define VFICRYPT_DIAGNOSTICS_AESCMAC_SERVICE_ID						(VFICRYPT_SERVICE_ID + 56)
#define VFICRYPT_DIAGNOSTICS_SHA_SERVICE_ID							(VFICRYPT_SERVICE_ID + 57)
#define VFICRYPT_DIAGNOSTICS_SETHMAC_SERVICE_ID						(VFICRYPT_SERVICE_ID + 58)
#define VFICRYPT_DIAGNOSTICS_SHATEST_SERVICE_ID						(VFICRYPT_SERVICE_ID + 59)
#define VFICRYPT_DIAGNOSTICS_RSACALC_SERVICE_ID						(VFICRYPT_SERVICE_ID + 60)
#define VFICRYPT_DIAGNOSTICS_AESGENERIC_SERVICE_ID					(VFICRYPT_SERVICE_ID + 61)
#define VFICRYPT_DIAGNOSTICS_TESTSECUREAPI_SERVICE_ID				(VFICRYPT_SERVICE_ID + 62)
#define VFICRYPT_DIAGNOSTICS_GETSESSKEYS_SERVICE_ID					(VFICRYPT_SERVICE_ID + 63)
#define VFICRYPT_DIAGNOSTICS_CHECKSBF_SERVICE_ID					(VFICRYPT_SERVICE_ID + 64)
#define VFICRYPT_DIAGNOSTICS_ICWKCALC_SERVICE_ID					(VFICRYPT_SERVICE_ID + 66)
#define VFICRYPT_DIAGNOSTICS_RSA_DECRYPT_SERVICE_ID					(VFICRYPT_SERVICE_ID + 67)
#define VFICRYPT_DIAGNOSTICS_RSA_ENCRYPT_SERVICE_ID					(VFICRYPT_SERVICE_ID + 68)
#define VFICRYPT_DIAGNOSTICS_PINBLOCKF4_ENCR						(VFICRYPT_SERVICE_ID + 69)
#define VFICRYPT_DIAGNOSTICS_PINBLOCKF4_DECR						(VFICRYPT_SERVICE_ID + 70)


/* TI TESTAPP */
#define TI_TESTAPP_TEST												(TI_AESAPP_APP_ID + 1)

/* VCL */
#define VFICRYPT_VCL_VAES_SERVICE_ID					(VFICRYPT_SERVICE_ID + 65)

/* SYM */
#define SYM_CRYPT_SERVICE_ID							(VFICRYPT_SERVICE_ID + 90)
#define SYM_WRAP_SERVICE_ID								(VFICRYPT_SERVICE_ID + 91)


/* TAGS for VFICRYPT */
#define KEY_TAGNUM						32
#define RSAKEY_N_TAGNUM					33
#define RSAKEY_E_TAGNUM					34
#define RETVAL_TAGNUM					35
#define RESULT_TAGNUM					36
#define RND_LENGTH_TAGNUM				37
#define RANDOM_OPTIONS_TAGNUM			38
#define RND_RETVAL_TAGNUM				39
#define PADDING_TYPE_TAGNUM				40
#define UC_OPAQUE_MODE_TAGNUM			41
#define ENCRYTED_BUFFER_TAGNUM			42
#define RESULT_KEY_TAGNUM				43
#define RND_PADDING_TAGNUM				44
#define KEYTAG_TYPE_TAGNUM				45
#define KEYTAG_TAGNUM					46
#define UNENCRYTED_BUFFER_TAGNUM		47
#define INPUT_MSG_TAGNUM				48
#define RESULT_MSG_TAGNUM				49
#define SEARCH_KEY_TAGNUM				50
#define CONTEXT_TAGNUM					51
#define RSAKEY_D_TAGNUM					52
#define RSAKEY_EXP_TAGNUM				53
#define RSAKEY_Y_TAGNUM					54
#define RSAKEY_X_TAGNUM					55
#define SHAMODE_TAGNUM					56
#define HASH_TAGNUM						57
#define RW_OPTION_TAGNUM				58
#define POS_TAGNUM						59
#define RKL_KEY_TAGNUM					60
#define RWMODE_TAGNUM					61
#define DIGEST_TAGNUM					62
#define FSEEK_CUR_TAGNUM				63
#define KEY_LENGTH_TAGNUM				64
#define KAES_KEY_TAGNUM					65
#define KEK_KEY_TAGNUM					66
#define CMAC_KEY_TAGNUM					67
#define GEK_KEY_TAGNUM					68
#define CWK_KEY_TAGNUM					69
#define OPTION_TAGNUM					70
#define SHAALGO_TAGNUM					71
#define REALLYSECRETKEY_TAGNUM			72
#define VERSION_TAGNUM					73
#define SHAACTION_TAGNUM				74
#define HMAC_KEY_TAGNUM					75
#define EXPONENT_SIZE_TAGNUM			76
#define RESULT_BUF_LEN_TAGNUM			77
#define CHALLENGE_MSG_TAGNUM			78
#define CAFETR31KEY_TR31_KEYFILE		79
#define TOKEN_FILE_TAGNUM				79
#define TOKEN_SERNUM_TAGNUM				80
#define TOKEN_TYPE_TAGNUM				81
#define TOKEN_OPTION_TAGNUM				82
#define TOKEN_SIG_TAGNUM				83
/* VCL */
#define OP_CODE_TAGNUM					84
#define ARG_CMD_RESP_TAGNUM				85
#define FIRST_ARG_TAGNUM				86
#define SECOND_ARG_TAGNUM				87
#define VCL_KEY_TAGNUM					88
#define DATA_BUFFER_TAGNUM				89
#define SECOND_DATA_BUFFER_TAGNUM		90
#define WRAPPED_KEY_BUFFER_TAGNUM		91
#define VCL_OPERATION_RET_TAGNUM		92

/* AES */
#define CRYPT_HANDLE_TAGNUM				36
#define CRYPT_TYPE_TAGNUM				37
#define ENCRYPT_TAGNUM					38
#define MODE_TAGNUM						39
#define KEY_DATA_TAGNUM					40
#define AUTH_KEY_TAGNUM					41
#define IV_TAGNUM						42
#define SRC_DATA_TAGNUM					43
#define DST_DATA_TAGNUM					44
#define DST_LEN_TAGNUM					45
#define CRYPT_FILE_TAGNUM				46
#define CRYPT_FILENAME_TAGNUM			47
#define CRYPT_FILEDATA_TAGNUM			48
#define KEY_SOURCE_TAGNUM				49
#define TAG_TAGNUM						50
#define TAGSIZE_TAGNUM					51

/* ECC */
#define EC_SERVICE_ID					EC_APP_ID
#define EC_VERIFY_SERVICE_ID			(EC_APP_ID + 1)

#define EC_KEY_TAGNUM					49
#define EC_POINT_TAGNUM					50
#define EC_OUTLEN_TAGNUM				51
#define EC_KDF_UKM_TAGNUM				52
#define EC_SIG_TAGNUM					53
#define EC_DIGEST_TAGNUM				54

/* Opaque mode */
#define OPAQUE_KEYTYPE_TAGNUM			53

/* Keystore App Service ids */
#define KEYSTORE_SERVICE_ID				0x0200
#define KEYSTORE_INIT_KEK_SVC			(KEYSTORE_SERVICE_ID + 0)

/* Pedguard App Service ids */
#define PG_SERVICE_ID					0x8100
#define PG_TEST_SERVICE_ID				(PG_SERVICE_ID + 0)
#define PG_INIT_SERVICE_ID				(PG_SERVICE_ID + 1)
#define PG_ADD_CERT_SERVICE_ID			(PG_SERVICE_ID + 2)
#define PG_DIAG_SERVICE_ID				(PG_SERVICE_ID + 3)

#define PG_PKCS7_INFO_SERVICE_ID		(PG_SERVICE_ID + 5)
#define PG_AUTH_SERVICE_ID				(PG_SERVICE_ID + 6)
#define PG_AUTH_LOAD_BIND_ID			(PG_SERVICE_ID + 7)

/* Pedguard App Tags */
#define PG_SERVICE_TAGNUM				32 /* DF20 */
#define PG_RESULT_TAGNUM				33 /* DF21 */
#define PG_CERT_TAGNUM					34 /* DF22 */
#define PG_DIAG_OPTION_TAGNUM			35 /* DF23 */
#define PG_MAX_DATA_LEN_TAGNUM			36 /* DF24 */
#define PG_DIAG_RET_LEN_TAGNUM			37 /* DF25 */
#define PG_DATA_TAGNUM					38 /* DF26 */
#define PG_AUTH_ATTR_TAGNUM				39 /* DF27 */
#define PG_SIG_DIGEST_TAGNUM			40 /* DF28 */
#define PG_SUBJ_ID_TAGNUM				41 /* DF29 */
#define PG_SIG_TAGNUM					42 /* DF2A */
#define PG_FILENAME_TAGNUM				43 /* DF2B */
#define PG_ALGOR_TAGNUM					44 /* DF2C */
#define PG_DIGEST_TAGNUM				45 /* DF2D */
#define PG_FILETYPE_TAGNUM				46 /* DF2E */
#define PG_PKCS7_SIG_TAGNUM				47 /* DF2F */
#define PG_APPID_TAGNUM					48 /* DF30 */
#define PG_CERT_SERIALID_TAGNUM			49 /* DF31 */
#define PG_DIAG_INDEX_TAGNUM			53 /* DF35 */


/* IPP App Service ids */
#define IPPAPP_SERVICE_ID						0x100
#define IPPAPP_ECHO_SERVICE_ID					(IPPAPP_SERVICE_ID + 0)
#define IPPAPP_LOAD_KEYAREA						(IPPAPP_SERVICE_ID + 1)
#define IPPAPP_UNLOAD_KEYS						(IPPAPP_SERVICE_ID + 2)
#define IPPAPP_GET_KEYAREA_INDEX				(IPPAPP_SERVICE_ID + 3)
#define IPPAPP_CHECK_KEYAREA					(IPPAPP_SERVICE_ID + 4)
#define IPPAPP_IPP_PKT							(IPPAPP_SERVICE_ID + 5)
#define IPPAPP_EMV_PIN							(IPPAPP_SERVICE_ID + 6)
#define IPPAPP_DELETE_KEYAREA					(IPPAPP_SERVICE_ID + 7)
#define IPPAPP_DELETE_ALL_KEYS					(IPPAPP_SERVICE_ID + 8)
#define IPPAPP_SC_SEND_DATA						(IPPAPP_SERVICE_ID + 9)
#define IPPAPP_RKL_DUKPT_KEY					(IPPAPP_SERVICE_ID + 10)
#define IPPAPP_RKL_MS_KEY						(IPPAPP_SERVICE_ID + 11)
#define IPPAPP_RKL_ADE_KEY						(IPPAPP_SERVICE_ID + 12)
#define IPPAPP_ADE_STATUS						(IPPAPP_SERVICE_ID + 13)
#define IPPAPP_ADE_ENCRYPT						(IPPAPP_SERVICE_ID + 14)
#define IPPAPP_STORE_KEY_PWD					(IPPAPP_SERVICE_ID + 15)
#define IPPAPP_SC_PFAIL_INSTALL					(IPPAPP_SERVICE_ID + 16)
#define IPPAPP_VF_CRYPTO						(IPPAPP_SERVICE_ID + 17)
#define IPPAPP_DELETE_KEY						(IPPAPP_SERVICE_ID + 20)
/*  Secure UART (IPP app)*/
#define IPP_MKI_SERV_ID							(IPPAPP_SERVICE_ID + 22)
/* EPP service ID (IPP app)*/
#define LIBPP_GEN_RSA_KEY_SERVICE_ID			(IPPAPP_SERVICE_ID + 40)
#define LIBPP_GET_PUB_KEY_SERVICE_ID			(IPPAPP_SERVICE_ID + 41)
#define LIBPP_RSLV_NS_SERVICE_ID				(IPPAPP_SERVICE_ID + 42)
#define LIBPP_FINAL_NS_SERVICE_ID				(IPPAPP_SERVICE_ID + 43)
#define LIBPP_INIT_NS_SERVICE_ID				(IPPAPP_SERVICE_ID + 44)
#define LIBPP_VERIFY_NS_SERVICE_ID				(IPPAPP_SERVICE_ID + 45)
#define LIBPP_GEN_CHALLENGE_SERVICE_ID			(IPPAPP_SERVICE_ID + 46)
#define LIBPP_RSLV_CHALLENGE_SERVICE_ID			(IPPAPP_SERVICE_ID + 47)
#define LIBPP_VERIFY_CHALLENGE_SERVICE_ID		(IPPAPP_SERVICE_ID + 48)
#define LIBPP_ENC_DATA_SERVICE_ID				(IPPAPP_SERVICE_ID + 49)
#define LIBPP_DEC_DATA_SERVICE_ID				(IPPAPP_SERVICE_ID + 50)
#define LIBPP_INIT_LINK_SERVICE_ID				(IPPAPP_SERVICE_ID + 51)
#define LIBPP_CIPHER_DATA_SERVICE_ID			(IPPAPP_SERVICE_ID + 52)
#define LIBPP_MAC_DATA_SERVICE_ID				(IPPAPP_SERVICE_ID + 53)
#define LIBPP_GET_PIN_KEY_SERVICE_ID			(IPPAPP_SERVICE_ID + 54)
#define LIBPP_ENCRYPT_PIN_KEY_SERVICE_ID		(IPPAPP_SERVICE_ID + 55)
#define LIBPP_LOAD_PIN_SERVICE_ID				(IPPAPP_SERVICE_ID + 56)
#define LIBPP_GEN_ARSK_CHALLENGE_SERVICE_ID		(IPPAPP_SERVICE_ID + 57)
#define LIBPP_RET_ARSK_CHALLENGE_SERVICE_ID		(IPPAPP_SERVICE_ID + 58)
#define LIBPP_VERIFY_ARSK_CHALLENGE_SERVICE_ID	(IPPAPP_SERVICE_ID + 59)
#define LIBPP_IS_PIN_INJECTED_SERVICE_ID				(IPPAPP_SERVICE_ID + 60)
#define IPP_KEY_STAT							(IPPAPP_SERVICE_ID + 89)
#define IPP_INSTALL_MIB_SVC						(IPPAPP_SERVICE_ID + 90)
#define IPPAPP_TEST_SERVICE						(IPPAPP_SERVICE_ID + 99)

/*AES n DES Generic*/
#define VFICRYPT_AES_GENERIC_CRYPT_SERVICE_ID			(VFICRYPT_SERVICE_ID + 69)
#define VFICRYPT_DES_GENERIC_CRYPT_SERVICE_ID			(VFICRYPT_SERVICE_ID + 70)
#define VFICRYPT_AESDECRYPT_INIT_SERVICE_ID				(VFICRYPT_SERVICE_ID + 71)
#define VFICRYPT_AESDECRYPT_UPDATE_SERVICE_ID			(VFICRYPT_SERVICE_ID + 72)
#define VFICRYPT_AESDECRYPT_FINAL_SERVICE_ID			(VFICRYPT_SERVICE_ID + 73)
#define VFICRYPT_AESENCRYPT_INIT_SERVICE_ID				(VFICRYPT_SERVICE_ID + 74)
#define VFICRYPT_AESENCRYPT_UPDATE_SERVICE_ID			(VFICRYPT_SERVICE_ID + 75)
#define VFICRYPT_AESENCRYPT_FINAL_SERVICE_ID			(VFICRYPT_SERVICE_ID + 76)
#define VFICRYPT_AESCTX_CTRL_SERVICE_ID					(VFICRYPT_SERVICE_ID + 77)
#define VFICRYPT_AESENCRYPTFULL_SERVICE_ID				(VFICRYPT_SERVICE_ID + 78)
#define VFICRYPT_AESDECRYPTFULL_SERVICE_ID				(VFICRYPT_SERVICE_ID + 79)
#define VFICRYPT_AESCTX_NEW_SERVICE_ID					(VFICRYPT_SERVICE_ID + 80)
#define VFICRYPT_AESCTX_FREE_SERVICE_ID					(VFICRYPT_SERVICE_ID + 81)
#define VFICRYPT_AESCTX_SETPADDING_SERVICE_ID			(VFICRYPT_SERVICE_ID + 82)

/* IPP App Service tags */
#define IPP_DATA_TAGNUM					32 /* DF20 */
#define IPP_KEYAREA_TAGNUM				33 /* DF21 */
#define IPP_AREA_INDEX_TAGNUM			34 /* DF22 */
#define IPP_KEY_TYPE_TAGNUM				35 /* DF23 */
#define IPP_KEY_ID_TAGNUM				36 /* DF24 */
#define IPP_AREA_LOADED_TAGNUM			37 /* DF25 */
#define IPP_AREA_DATA_TAGNUM			38 /* DF26 */
#define IPP_ALLOC_KEYAREA_TAGNUM		39 /* DF27 */
#define IPP_NUM_KEYS_TAGNUM				40 /* DF28 */
#define IPP_LOADABLE_KEYS_TAGNUM		41 /* DF29 */
#define IPP_CELL_SIZE_TAGNUM			42 /* DF2A */
#define IPP_EMV_PIN_MODE_TAGNUM			43 /* DF2B */
#define IPP_CHALLENGE_IPP_TAGNUM		44 /* DF2C */
#define IPP_PUBLIC_KEY_TAGNUM			45 /* DF2D */
#define IPP_EXPONENT_TAGNUM				46 /* DF2E */
#define IPP_PIN_BLOCK_TAGNUM			47 /* DF2F */
#define IPP_REQ_TAGNUM					48 /* DF30 */
#define IPP_DELKEYS_CONDITION_TAGNUM	49 /* DF31 */
#define KEK_CMAC_TAGNUM					50 /* DF32 Keystore app. */
#define TR31_SLOT_TAGNUM				51 /* DF33 */
#define TR31_KMM_TAGNUM					52 /* DF34 */
#define TR31_KMM_MASK_TAGNUM			53 /* DF35 */
#define TR31_KSN_TAGNUM					54 /* DF36 */
#define TR31_ENC_KEY_TAGNUM				55 /* DF37 */
#define TR31_USAGE_TAGNUM				56 /* DF38 */
#define TR31_ALGORITHM_TAGNUM			57 /* DF39 */
#define TR31_MODE_TAGNUM				58 /* DF3A */
#define TR31_KEY_VER_TAGNUM				59 /* DF3B */
#define TR31_DELETE_KEYS_TAGNUM			60 /* DF3C */

#define IPP_ADE_KEYINDEX_TAGNUM			60
#define IPP_ADE_ENCALGO_TAGNUM			61
#define IPP_ADE_ENCMODE_TAGNUM			62
#define IPP_ADE_IV_TAGNUM				63
#define IPP_ADE_PAD_TAGNUM				64
#define IPP_ADE_PLAINTEXT_TAGNUM		65
#define IPP_ADE_CIPHERTEXT_TAGNUM		66
#define IPP_ADE_SUPPDATA_TAGNUM			67
#define IPP_ADE_IVDATA_TAGNUM			68
#define IPP_KEYLDPWD1_TAGNUM			69 /* DF45 */
#define IPP_KEYLDPWD2_TAGNUM			70 /* DF46 */

/*  Secure UART */
#define IPP_ACTION_TAGNUM				70
#define IPP_COM2IPP_TAGNUM				71
#define IPP_IPP2COM_TAGNUM				72
#define IPP_PASSTHRU_STATE_TAGNUM		73

#define IPP_DELETE_AREA_TAGNUM			74
#define IPP_ADE_KEY_VARIANT_TAGNUM		75
#define IPP_ADE_ENCFLAG_TAGNUM			76
#define IPP_ADE_KSNSTATE_TAGNUM			77
#define IPP_ADE_MAC_DATA_TAGNUM			78
#define IPP_ADE_RESULT_TAGNUM			79
#define IPP_ADE_MAC_RESULT_TAGNUM		80

#define MIB_MSG_TAGNUM					93
#define MIBSIG_MSG_TAGNUM				94

/* EPP tagnum (IPP app) */
#define IPP_INPUT_MSG_TAGNUM			148
#define IPP_RESULT_MSG_TAGNUM			136
#define IPP_SIZE_TAGNUM					179
#define IPP_EXPVALUE_TAGNUM				180
#define IPP_FORCERENEW_TAGNUM			181
#define IPP_RETVAL_TAGNUM				182
#define IPP_MAXMODLENGTH_TAGNUM			183
#define IPP_MODE_TAGNUM					184
#define IPP_MODVALUE_TAGNUM				185
#define IPP_CHALLENGE_TAGNUM			144
#define IPP_SYMMKEY_TAGNUM				187
#define IPP_COUNTER_TAGNUM				188
#define IPP_PIN_TAGNUM					189
#define IPP_CAFETR31KEY_TAGNUM			190
#define IPP_IS_INJECTED_TAGNUM			191

/* PinBlock Formt 4 */
#define PB4_KEY_TAGNUM					10
#define PB4_ENCR_TAGNUM					11
#define PB4_PAN_TAGNUM					12
#define PB4_PIN_TAGNUM					13

/* Keypad applet Service Ids */
#define SA_KEYPAD_INIT_COMMON			0x80
#define SA_KEYPAD_INSTALL_KEYPAD		0
#define SA_KEYPAD_KEY_SIZE				1
#define SA_KEYPAD_SWITCH_KEYPAD			2
#define SA_KEYPAD_GET_EVENT_ADDR		3
#define SA_GET_SHA_PASSWORD				4
#define SA_GET_SHA_PASSWORD_EXT			5
#define SA_RESERVED_1					6
#define SA_RESERVED_2					7
#define SA_TOUCH_SET_RESOLUTION			10
#define SA_TOUCH_SET_SCALING			11
#define SA_TOUCH_SET_HOTSPOTS			12
#define SA_KEYPAD_CLEAR_PIN			13
#define SA_KEYPAD_IS_SECURE			14
#define SA_TOUCH_SET_DOUBLE_TAP			15
#define SA_TOUCH_SET_ATMEL			16

/* Pin option bits (pin or password, yellow key set-up, touch screen mode) */
#define PIN_OPTION_PIN			1 /* PIN entry */
#define PIN_OPTION_PWD			2 /* Password entry */
#define PIN_OPTION_BS_BS		4 /* BS - Backspace: 1 - BS, 0 - Clear */
#define PIN_OPTION_TS_ACCESS		8 /* TS Mode - Accessibility: 1 - Regular: 0 */

/* lifecycle app Service Ids */
/*
 * This service upgrades SVID.
 * It MUST be called after PedGuard installed.
 * It needs parameter and its signature.
 */
#define SA_LIFECYCLE_UPGRADE_SVID		1

/*
 * This service programs efuse.
 * It MUST be called after PedGuard installed.
 * It needs a efuse bin and its signature.
 */
#define SA_LIFECYCLE_PROGRAM_EFUSE		2

/*
 * This service returns NVM fields which is defined by
 * TAGs.
 * It needs parameter and its signature.
 */
#define SA_LIFECYCLE_READ_NVM			3

/*
 * This service programs SVID from 0x0 to 0x1
 * or from Manufacturing mode to Production mode.
 */
#define SA_SVID_MAN_TO_PROD				4

/*
 * This service converts unit to non-payment mode
 * and only works in Manafacturing mode.
 */
#define SA_TO_NONPAYMENT				5

/* lifecycle app Tag Ids */
/* BER_TLV Tags of SA_EFUSE_UPGRADE_SVID */
#define TAG_UPGRADE_SVID_SIG			3
#define TAG_UPGRADE_SVID_PARA			5

/* BER_TLV Tags of SA_EFUSE_PROGRAM_EFUSE */
#define TAG_PROGRAM_EFUSE_SIG			7
#define TAG_PROGRAM_EFUSE_BIN			9

/* BER_TLV Tags of SA_LIFECYCLE_READ_NVM */
#define TAG_READ_NVM_SVID				21
#define TAG_READ_NVM_DEVID				33
#define TAG_READ_NVM_APPDEV				35
#define TAG_READ_NVM_VATS				38
#define TAG_READ_NVM_NOPAY				40
#define TAG_READ_NVM_PCI				44


/* lifecycle app Error Codes */
#define ERR_LC_EFUSE_SVID_BAD			1
#define ERR_LC_AUTH_FAIL				2
#define ERR_LC_PARA_INVALID				3
#define ERR_LC_MODE_INVALID				4

/* TPMSS service Ids (sub-services) provided by this application */
#define SA_TPMSS_GET_STATUS				0
#define SA_TPMSS_GET_TAMPER_LOG			1
#define SA_TPMSS_DETAMPER				2
#define SA_TPMSS_PMICOFF				3
#define SA_TPMSS_RTC_GET_TIME			4
#define SA_TPMSS_RTC_SET_TIME			5
#define SA_TPMSS_RTC_ENABLE_ALARM		6
#define SA_TPMSS_RTC_SET_ALARM_TIME		7
#define SA_TPMSS_RTC_GET_ALARM_TIME		8
#define SA_TPMSS_RTC_CLEAR_EXT_WAKEUP	9

/* Tag of returned value */
#define TAG_RETURN						3
/* BER_TLV Tags of SA_TPMSS_Get_STATUS */
#define TAG_GET_STATUS_SBF				8
#define TAG_GET_STATUS_STS				5
#define TAG_GET_STATUS_RESET_LOG		7
/* BER_TLV Tags of SA_TPMSS_Get_TAMPER_LOG */
#define TAG_GET_TAMPER_LOG				6


/* VSS app Service Ids */
#define VSS_SERVICE_ID							0x8700
#define VSS_VERIFY_PASSWORD_SERVICE_ID			(VSS_SERVICE_ID + 10)
#define VSS_STORE_KEY_SERVICE_ID				(VSS_SERVICE_ID + 11)
#define VSS_SET_PMC_GLOBAL_VAR_KEY_SERVICE_ID	(VSS_SERVICE_ID + 12)
#define VSS_EXECUTE_OPCODE_SERVICE_ID			(VSS_SERVICE_ID + 13)
#define EXECUTE_VSS_MACRO_SERVICE_ID			(VSS_SERVICE_ID + 14)
#define VSS_CHECH_PASSWORD_ID					(VSS_SERVICE_ID + 15)
#define VSS_LOAD_SYSTEM_KEY						(VSS_SERVICE_ID + 16)
#define VSS_LOAD_MASTER_KEY						(VSS_SERVICE_ID + 17)
#define VSS_CHECK_MASTER_KEY					(VSS_SERVICE_ID + 18)
#define VSS_RKL_STORE_KLK						(VSS_SERVICE_ID + 19)
#define VSS_RKL_STORE_KEY						(VSS_SERVICE_ID + 20)


/* VSS Tag Numbers */
#define KLK_KEY_TAGNUM							64
#define UC_KEY_INDEX_TAGNUM						65
#define SCR_HDR_NAME_TAGNUM						66
#define BLOCK_RETENTION_CODE_TAGNUM				67
#define VSS_VERIFY_PASS_RET_VAL_TAGNUM			68
#define BLOCK_INDEX_TAGNUM						69
#define BLOCK_VALUE_TAGNUM						70
#define VSS_KEY_AREA_TAGNUM						71
#define SEQ_ID_TAGNUM							72
#define NEXT_SEQ_ID_TAGNUM						73
#define SEC_RET_VAL_TAGNUM						74
#define OPCODE_TAGNUM							75
#define UPDATE_REG_TAGNUM						76
#define US_TEMP_ONE_TAGNUM						77
#define US_TEMP_SECOND_TAGNUM					78
#define UC_TEMP_ONE_TAGNUM						79
#define UC_TEMP_SECOND_TAGNUM					80
#define VSS_SCRIPT_FILE_TAGNUM					81
#define COMM_BUFFER_TAGNUM						82
#define COMM_BUFFER_RET_VAL_TAGNUM				83
#define EXEC_OPCODE_RET_VAL_TAGNUM				84
#define SCRIPT_ID_VAL_TAGNUM					85
#define CODE_START_TAGNUM						86
#define SYS_KEY_TYPE_TAGNUM						87
#define SYS_CLEAR_KEY_TAGNUM					88
#define SYS_KEY_DATA_TAGNUM						89
#define SYS_LOAD_TYPE_TAGNUM					90
#define STATE_IDENTIFIER						91
#define MACRO_START_TAGNUM						92
#define SCRIPT_OPERATION_TAGNUM                 93


/* POSEIDON Service Id */
#define POSEIDON_SERVICE_ID						(POSEIDON_APP_ID + 22)


/* Secure Log Services */
#define SECLOG_LOGVIEW_SERVICE_ID				(SECLOG_SERVICE_ID + 0)
#define SECLOG_RESET_SERVICE_ID					(SECLOG_SERVICE_ID + 1)
#define SECTEST_TEST_SERVICE_ID					(SECTEST_SECURE_APP_ID + 0)

/* Secure Log Tags */
#define DATA_TAGNUM								34
#define IDX_TAGNUM								35
#define BUMF_TAGNUM								666

/* Secure Log Defines */
#define ARG_SPRINTF_SIZE						16000
#define ARG_BLOCK_SIZE							4000

/* AES DUKPT services */
#define AESDUKPT_INJECT_INITIAL_KEY				1
#define AESDUKPT_UPDATE_STATE_NEXT_TRANS		2
#define AESDUKPT_ENCRYPT_INIT					3
#define AESDUKPT_HMAC_INIT						4
#define AESDUKPT_GET_KSN						5
#define AESDUKPT_SRED_INIT						6
#define AESDUKPT_ENCRYPT_PIN					7

/* AES DUKPT Tags */
#define AESDUKPT_BDK_TAGNUM						32
#define AESDUKPT_KEYID_TAGNUM					33
#define AESDUKPT_KEY_LEN_TAGNUM					34
#define AESDUKPT_CTX_TAGNUM						35
#define AESDUKPT_DATA_TAGNUM					36
#define AESDUKPT_ENCRYPT_TAGNUM					37
#define AESDUKPT_KEY_USAGE_TAGNUM				38
#define AESDUKPT_IV_TAGNUM						39
#define AESDUKPT_KSN_TAGNUM						40
#define AESDUKPT_ENC_MODE_TAGNUM				41
#define AESDUKPT_AES_CRYPTO_CTX_TAGNUM			42
#define AESDUKPT_INTERN_CTX_TAGNUM				43
#define AESDUKPT_SHA_CTX_TAGNUM					44
#define AESDUKPT_CTX_FILENAME_TAGNUM			45
#define AESDUKPT_PAN_TAGNUM						46
#define AESDUKPT_ENC_PINBLOCK_TAGNUM			47


/* AES DUKPT Encryption Key Usage	*/
enum aesdukpt_enckey
{
	AESDUKPT_DATA_ENCRYPT = 1,
	AESDUKPT_DATA_DECRYPT,
	AESDUKPT_DATA_BOTHWAYS
};

/* AES DUKPT Message Authentication Key Usage	*/
typedef enum aesdukpt_mackey
{
	AESDUKPT_MAC_GENERATION = 1,
	AESDUKPT_MAC_VERIFICATION,
	AESDUKPT_MAC_BOTHWAYS
} keyvariant;


#define AESDUKPT_INITIAL_KEY_ID_SIZE_BYTES		8
#define AESDUKPT_ENC_COUNTER_SIZE_BYTES			4
#define AESDUKPT_KSN_SIZE_BYTES					(AESDUKPT_INITIAL_KEY_ID_SIZE_BYTES + \
												AESDUKPT_ENC_COUNTER_SIZE_BYTES)
#define AESDUKPT_ASCII_KSN_LENGTH				(AESDUKPT_KSN_SIZE_BYTES * 2)
#define AESDUKPT_ENC_PINBLOCK_SIZE				16
#define AESDUKPT_MAX_PIN_DIGITS					12
#define AESDUKPT_MAX_PAN_DIGITS					19


/* ADE vf_crypto defines */
#define VF_MAX_DATA_LEN		2048  /* Max data bytes to send to crypto function */

/* Key Types */
#define VF_TDES_DUKPT_SRED      0  /* keys 0 - 9 are ADE keys  0 -  9 */
#define VF_TDES_DUKPT_NON_SRED  1  /* keys 0 - 9 are ADE keys 10 - 19 */
#define VF_IPP_DUKPT            3

/* Key Variant */
#define VF_X924_MAC_REQ_BW		1
#define VF_X924_MAC_RESP		2
#define VF_X924_DE_REQ_BW		3

/* Algorithms */
#define VF_ALG_DES112			1
/*
 * NOTE:
 * For MAC algorithms that have last block processing and
 * the data to MAC is <= VF_MAX_DATA_LEN then the complete
 * MAC can be done with one call using the LAST algorithm
 */
#define VF_ALG_9797_MAC_1			6 /* K is a single length key (K1) */
#define VF_ALG_9797_MAC_1A			7 /* K is a double length key (K1 and K2) */
#define VF_ALG_9797_MAC_2			8 /* K1 is a single length key (K1) */
									  /* K2 is a single length key (K2) */
#define VF_ALG_9797_MAC_2_LAST		9 /* When the last block is MACed, do last block processing */
									  /* Can MAC, 1 or more blocks */
#define VF_ALG_9797_MAC_3			10/* K1 is a single length key (K1) */
									  /* K2 is a single length key (K2) */
#define VF_ALG_9797_MAC_3_LAST		11/* When the last block is MACed, do last block processing */
									  /* Can MAC, 1 or more blocks */
#define VF_ALG_9797_MAC_4_FIRST		12/* When the first block is MACed, do first block processing */
									  /* Can MAC, 1 or more blocks */
									  /* K is a single length key (K1) */
									  /* K1 is a single length key (K1 XOR 0xF0F0F0F0F0F0F0F0) */
									  /* K2 is a single length key (K2) */
#define VF_ALG_9797_MAC_4			13/* Middle block processing */
#define VF_ALG_9797_MAC_4_LAST		14/* When the last block is MACed, do last block processing */
									  /* Can MAC, 1 or more blocks */
#define VF_ALG_9797_MAC_5			15/* K1 is a single length key (K1) */
									  /* K2 is a single length key (K1 XOR 0xF0F0F0F0F0F0F0F0) */
#define VF_ALG_9797_MAC_5_LAST		16/* When the last block is MACed, do last block processing */
									  /* Can MAC, 1 or more blocks */
#define VF_ALG_9797_MAC_5A			17/* K1 is a double length key (K1 and K2) */
									  /* K2 is a double length key (K1 XOR 0xF0F0F0F0F0F0F0F0) */
									  /*                           (K2 XOR 0xF0F0F0F0F0F0F0F0) */
#define VF_ALG_9797_MAC_5A_LAST		18/* When the last block is MACed, do last block processing */
									  /* Can MAC, 1 or more blocks */
#define VF_ALG_CMAC_TDEA			22
#define VF_ALG_HMAC_SHA256			24
#define VF_ALG_VISA_FPE_PAN			26
#define VF_ALG_VISA_FPE_CHNAME		27
#define VF_ALG_VISA_FPE_TRACK1		28
#define VF_ALG_VISA_FPE_TRACK2		29

/* Encrypt Flag */
#define VF_ENCRYPT					1
#define VF_MAC_VERIFY				2

/* Modes of Operation */
#define VF_MODE_ECB					0
#define VF_MODE_CBC					1

/* IV Settings */
#define VF_IV_NONE					0
#define VF_IV_ZERO					1
#define VF_IV_RAND					2
#define VF_IV_USE_INPUT				3

/* Padding Schemes */
#define VF_PAD_NONE					0
#define VF_PAD_PKCS7				1 /* Each padding byte equals the padding length. */
									  /* Example: XX XX XX XX 04 04 04 04 */
#define VF_PAD_X923					2 /* Final padding byte equals the padding length and all other */
									  /* padding bytes equal 0x00. */
									  /* Example: XX XX XX XX 00 00 00 04 */
#define VF_PAD_ISO7816				3 /* First padding byte equals 0x80 and all other padding bytes */
									  /* equal 0x00. */
									  /* Example: XX XX XX XX 80 00 00 00 */
#define VF_PAD_ISO9797_1			4 /* Padding bytes are 0x00 if needed to fill block. */
									  /* Example: XX XX XX XX 00 00 00 00 */
#define VF_PAD_ISO9797_2			5 /* First padding byte equals 0x80 and all other padding bytes */
									  /* equal 0x00.  There must be at least one padding byte. */
									  /* Example: XX XX XX XX 80 00 00 00 */

/* Set DUKPT KSN Key State */
#define VF_GET_NEXT_KSN_KEY			1 /* Store Next KSN and Key */
#define VF_USE_SAME_KSN_KEY			2 /* Use Stored KSN and Key */
#define VF_CLEAR_KSN_KEY			3 /* Clear Stored KSN and Key */

/* MAC Value Verified */
#define VF_MAC_VALUE_DID_NOT_MATCH	0
#define VF_MAC_VALUE_MATCHED		1

/* vf_crypto() Return Codes */
#define VF_SUCCESS						0
#define VF_ERR_DATA						-1
#define VF_ERR_LEN						-2
#define VF_ERR_KEY_TYPE					-3
#define VF_ERR_KEY_INDEX				-4
#define VF_ERR_KEY_VARIANT				-5
#define VF_ERR_ALG						-6
#define VF_ERR_ENC_FLAG					-7
#define VF_ERR_MODE						-8
#define VF_ERR_IV						-9
#define VF_ERR_PAD						-10
#define VF_ERR_DUKPT_KSN_KEY_STATE		-11
#define VF_ERR_USE_SAME_KSN_KEY			-12 /* Error if no key stored */
#define VF_ERR_MAC_VER_LEN				-14
#define VF_ERR_NO_KEY					-15
#define VF_ERR_OFF						-16
#define VF_ERR_KEY_VARIANT_WITH_ALGO	-17
/* TBD align VF_ERR_VISA_FPE_MALLOC with Verix */
//-----------------------------------------------
#define VF_ERR_VISA_FPE_MALLOC			-18
#define VF_ERR_GENERIC					-99

#define TOKEN_CHALLENGE_SIZE        20
#define TOKEN_CHALLENGE_ASC_SIZE    ( 2 * TOKEN_CHALLENGE_SIZE + 1 )

#define TOKEN_OK                 0x00   /* Execution was successful             */
#define TOKEN_E_SIGNATURE       -0x11   /* Invalid token signature              */
#define TOKEN_E_TYPE            -0x12   /* Unknown token type                   */
#define TOKEN_E_READ            -0x13   /* Error reading token file             */
#define TOKEN_E_MEM             -0x14   /* General memory error                 */
#define TOKEN_E_DETAMPER        -0x15   /* Detampering error                    */
#define TOKEN_E_TID             -0x16   /* Invalid terminal id                  */
#define TOKEN_E_CHALLENGE       -0x17   /* Invalid challenge                    */
#define TOKEN_E_MIB_DETAMPER_METHOD       -0x18   /* Invalid mib_detamper_method */

#define AESBLOCKSIZE (16)
#define AESIVSIZE (16)
#define MAX_AES_CONTEXT (512)

typedef struct
{
	unsigned char	ctx[MAX_AES_CONTEXT];
    unsigned char	packetBuffer[AESBLOCKSIZE];
    unsigned int	packetLen;
    unsigned int	paddingType;
}AESContext;

/* AES -- DES Modes */
#define     VFI_ENCRYPT         0   /*  Are we encrpyting?  */
#define     VFI_DECRYPT         1   /*  Are we decrpyting?  */

#define VFI_CRYPTO_READ     0
#define VFI_CRYPTO_WRITE    1

#define     VFI_MODE_NONE       0
#define     VFI_MODE_CBC        1
#define     VFI_MODE_ECB        2
#define     VFI_MODE_CTR        3
#define     VFI_MODE_CCM        5
#define     VFI_MODE_CMAC       6
#define     VFI_MODE_CFB	    7
#define     VFI_MODE_CBCMAC     13
#define     VFI_MODE_GCM	    16
#define     VFI_MODE_OFB	    17

/* AES GCM tag sizes supported  */
#define VFI_AES_TAG_128			16
#define VFI_AES_TAG_120			15
#define VFI_AES_TAG_112			14
#define VFI_AES_TAG_104			13
#define VFI_AES_TAG_96			12
#define VFI_AES_TAG_64			8
#define VFI_AES_TAG_32			4

/* AES Key sizes    */
#define     VFI_AES_128         4   /* Key size is 128 bits AES_256*/
#define     VFI_AES_192         6   /* Key size is 192 bits AES_192*/
#define     VFI_AES_256         8   /* Key size is 256 bits AES_128*/

#define     VFI_DES_56              8   /* Key size is 56 bits  */
#define     VFI_DES_112             16  /* Key size is 112 bits */
#define     VFI_DES_168             24  /* Key size is 168 bits */

#define     VFI_SYM_DES_OLD         0
#define     VFI_SYM_DES             1
#define     VFI_SYM_AES             2

/* PADDING defines*/
#define PADDING_MODE_ZERO			1
#define PADDING_MODE_9797_1			2
#define PADDING_MODE_7816_4			3
#define PADDING_MODE_9797_2			4
#define PADDING_MODE_9797_3			5
#define PADDING_MODE_RANDOM			6
#define PADDING_MODE_10126			7
#define PADDING_MODE_PKCS7			8
#define PADDING_MODE_X923			9

#define MAX_KEY_LENGTH_BIT	256
#define MAX_KEY_LENGTH_BYTE	(MAX_KEY_LENGTH_BIT / 8)

#define MAX_SHA_CONTEXT 256
#define MAX_HMAC_KEY_SIZE (128)
#define MAX_SHA_BLOCK_SIZE (128)
#ifdef SHACONTEXT_DEFINED
	#undef SHACONTEXT_DEFINED
#else
typedef struct
{
	unsigned char Context[MAX_SHA_CONTEXT];
	unsigned char packetBuffer[MAX_SHA_BLOCK_SIZE];
	unsigned int packetLen;
	unsigned int init;
	unsigned int packetSize;
	unsigned char type;
	unsigned char hmacKey[MAX_HMAC_KEY_SIZE];
	unsigned int hmacKeyLen;
} SHAcontext;
#define SHACONTEXT_DEFINED
#endif


/* Visa DSP operationTypes TBD allign with Verix*/
//------------------------------------------------
#define		EN0						0
#define		DE1						1


/* SRED */
#define SREDAPP_TEST			((SREDAPP_APP_ID)+10)
#define SREDAPP_SERVICES		((SREDAPP_APP_ID)+11)

/* SRED Tagnums */
#define SRED_CONTXT_TAGNUM				8
#define SRED_INCTX_TAGNUM				9
#define SRED_INDATA_TAGNUM				10
#define SRED_OUTMAC_TAGNUM				22
#define SRED_OUTKSN_TAGNUM				23
#define SRED_OP_TAGNUM					24
#define SRED_HMAC_TAGNUM				25
#define SRED_KLEN_TAGNUM				26
#define SRED_KVAR_TAGNUM				27
#define SRED_ALG_TAGNUM					28
#define SRED_DATAPAD_TAGNUM				29
#define SRED_IVPAD_TAGNUM				30
#define SRED_IV_TAGNUM					31
#define	SRED_MACSIZE_TAGNUM				32

/* SRED defines*/
#define SRED_KEY_128					16
#define SRED_KEY_192					24
#define SRED_KEY_256					32


typedef enum{
	SRED_DUKPT_AES_128		= SRED_KEY_128,
	SRED_DUKPT_AES_192		= SRED_KEY_192,
	SRED_DUKPT_AES_256		= SRED_KEY_256,
}sred_key_len;


typedef enum{
	SRED_MAC_ISO_9797_MODE_1,
	SRED_MAC_ISO_9797_MODE_2,
	SRED_MAC_ISO_9797_MODE_3,
	SRED_MAC_ISO_9797_MODE_4,
	SRED_MAC_ISO_9797_MODE_5,
	SRED_MAC_ISO_9797_MODE_6,
	SRED_MAC_CMAC_AES
}mac_algorithm;


typedef enum{
	SRED_INIT,
	SRED_UPDATING,
	SRED_FINALIZE
}sred_op;

typedef enum {
	ZERO_PADDING,
	ISO_9797_MODE_1,
	ISO_IEC_7816_4,
	ISO_9797_MODE_2,
	ISO_9797_MODE_3,
	RANDOM_PADDING,
	ISO_10126,
	PKCS7_PADDING,
	X923
} padding_type;

typedef struct{
	int 			contxt;
	unsigned char	ksn[AESDUKPT_KSN_SIZE_BYTES];
}sred;


#define KLK_PRIVATE_KEY_FILE "/mnt/flash/system/rkl_keys/klk_key.der"

#ifdef __cplusplus
}
#endif

#endif
