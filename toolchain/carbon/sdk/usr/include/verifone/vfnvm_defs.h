
#ifndef _VFNVM_DEFS_H__
#define _VFNVM_DEFS_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifndef RAPTOR
typedef struct
{
	unsigned char   Scfg;      /* 8 bits of SCFG           */
	unsigned short  BrcmRev;
	unsigned int    Dcfg;      /* 32 bits of DCFG bits     */
	unsigned char   DevId[32]; /* Device ID                */
	unsigned char   SVID;      /* SVID                     */
}S_nvm;

#else
typedef struct
{
	unsigned char   Scfg;      /* 8 bits of SCFG           */
	unsigned short  BrcmRev;
	unsigned int    Dcfg;      /* 32 bits of DCFG bits     */
	unsigned char   DevId[32]; /* Device ID                */
	unsigned char   SVID;	   /* SVID                     */
	unsigned char   appdev_mode; /* Set if device is in appdev mode */
	unsigned char   vats_mode;   /* Set if device is in vats mode */
	unsigned char   nopay_mode;  /* Set if device is in non payment mode */
	unsigned char   PCI;		 /* Set if device is PCI4 */
}S_nvm;
#endif

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _VFNVM_DEFS_H__ */
