/****************************************************************************
 *                                                                          *
 * FILE NAME:    sysfiles.h                                                 *
 *                                                                          *
 * COMPANY:      Verifone                                                   *
 *                                                                          *
 * DESCRIPTION:  Version common definitions of system files                 *
 *                                                                          *
 ****************************************************************************/

#ifndef _SYSFILES_H_
#define _SYSFILES_H_

#ifdef __cplusplus
extern "C" {
#endif

// Files defines
#define SYSFILES_MIB_PATH                         "/etc/mib"                    // Exported binary MIB file
#define SYSFILES_KERNEL_VER_FILE                  "/etc/kernel-version"         // Linux kernel version file
#define SYSFILES_RFS_VER_FILE                     "/etc/rfs-version"            // RFS version file
#define SYSFILES_SECURITY_RFS_VER_FILE            "/etc/rfs-security-version"   // RFS Security version file
#define SYSFILES_SBI_VER_FILE                     "/etc/sbi-version"            // SBI version file
#define SYSFILES_UBOOT_VER_FILE                   "/etc/uboot-version"          // UBOOT version file
#define SYSFILES_VAULT_VER_FILE                   "/etc/vault-version"          // Vault version file
#define SYSFILES_VAULT_PEDGUARD_VER_FILE          "/etc/vault-pedguard-version" // Vault pedguard version file
#define SYSFILES_RELEASE_VER_FILE                 "/etc/issue"                  // Release version file
#define SYSFILES_SRED_VER_FILE                    "/etc/sred-version"           // SRED version file
#define SYSFILES_OPENPROTOCOL_VER_FILE            "/etc/openprotocol-version"   // Open Protocol version file
#define SYSFILES_VHQ_VER_FILE                     "/etc/vhq-version"            // VHQ version file
#define SYSFILES_CTLS_VER_FILE                    "/tmp/ctls-version"           // Contactless version file
#define SYSFILES_ETH_MAC_ADDRESS_FILE             "/mnt/flash/etc/eth_mac"      // Ethernet MAC Address file
#define SYSFILES_WIFI_MAC_ADDRESS_FILE            "/mnt/flash/etc/wifi_mac"     // WiFi MAC Address file
#define SYSFILES_BT_ADDRESS_FILE                  "/mnt/flash/etc/bt_address"   // Bluetooth address file
#define SYSFILES_ETH2_MAC_ADDRESS_FILE            "/mnt/flash/etc/eth2_mac"     // Ethernet 2 MAC address file

#ifdef __cplusplus
}
#endif

#endif /* _SYSFILES_H_ */
