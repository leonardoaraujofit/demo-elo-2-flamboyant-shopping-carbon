/**
 *@file diag_counters_API.h
 */

#ifndef _DIAG_COUNTERS_API_H_
#define _DIAG_COUNTERS_API_H_

#include "bbsram_pub.h"
#ifndef __VAULT__
#include <linux/types.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define DIAG_COUNTERS_ALLOCATED				(BBSRAM_SYSTEM_LOG_SIZE/4)
#define DIAG_COUNTERS_SIZE 					(BBSRAM_SYSTEM_LOG_SIZE)

#ifdef RAPTOR
#define DIAG_COUNTERS_BURN_FLAG				0x1
#define DIAG_COUNTERS_LVL1_FLAG				0x2
#define DIAG_COUNTERS_HMAC_PENDING_FLAG			0x4
#define DIAG_COUNTERS_CONSOLE_FLAG			0x700
#endif

/**
 * Area between 1 and 127 reserved for application counters
 */
enum diag_counter_index_number
{
    DIAG_COUNT_ID					= 0,	/**< Opsys use only: 0xVVVVMMmm -- Diag Counters Validity Marker 2 bytes (VVVV), Major Version 1 byte (MM), Minor Version 1 byte (mm)*/

    DIAG_COUNT_APPLICATION_FIRST			= 1,	/**< First of 127 non-volatile 32-bit locations that may be allocated by application for counting, timestamping, etc. */

    // This area reserved for application counters

    DIAG_COUNT_APPLICATION_LAST				= 127,	/**< Last of 127 non-volatile 32-bit locations that may be allocated by application for counting, timestamping, etc. */

    DIAG_COUNT_NAND_FLASH_ECC_ERRORS 			= 128,	/**< Number of flash memory ECC errors detected */
    DIAG_COUNT_NAND_FLASH_ECC_CORRECTED		= 129,	/**< Number of flash memory ECC errors corrected */
    DIAG_COUNT_NAND_FLASH_BAD_BLOCKS 			= 130,	/**< Number of flash memory bad blocks detected */
    DIAG_COUNT_HARD_KEYPAD_PRESSES   			= 131,	/**< Number of key presses on hard keypad in non-secure mode */
    DIAG_COUNT_LAST_PIN_ENTRY_START_TIME_STAMP		= 132,  /**< Time (number of seconds since 1970/01/01 epoch) that last PIN entry was started */
    DIAG_COUNT_LAST_PIN_ENTRY_END_TIME_STAMP		= 133,  /**< Time (number of seconds since 1970/01/01 epoch) that last PIN entry was completed */
    DIAG_COUNT_PIN_ENTRY_ERRORS				= 134,  /**< Number of PIN entry errors */
    DIAG_COUNT_POWER_CYCLES_OR_REBOOTS 		= 135,  /**< Number of power cycles plus software-initiated reboots */
    DIAG_COUNT_SYSTEM_MODE_ENTERED	 		= 136,	/**< Number of times that system mode has been entered */
    DIAG_COUNT_SYSTEM_MODE_ENTRY_FAILED 		= 137,	/**< Number of failed attempts to enter system mode */
    DIAG_COUNT_SYSTEM_MODE_PASSWORD_CHANGE_TIME	= 138,	/**< Time (number of seconds since 1970/01/01 epoch) that any system mode password was last changed */
    DIAG_COUNT_PROCESSED_DOWNLOAD_FILES_OR_PACKAGES	= 139,  /**< Number of downloaded files and packages processed */
    DIAG_COUNT_MSR_READS_ATTEMPTED	 		= 140,	/**< Number of magnetic card reads attempted */
    DIAG_COUNT_MSR_READS_WITH_ERRORS_TRACK_1		= 141,	/**< Number of magnetic card read errors on track one */
    DIAG_COUNT_MSR_READS_WITH_ERRORS_TRACK_2		= 142,	/**< Number of magnetic card read errors on track two */
    DIAG_COUNT_MSR_READS_WITH_ERRORS_TRACK_3		= 143,	/**< Number of magnetic card read errors on track three */
    DIAG_COUNT_EMF_ENCOUNTERED				= 144,	/**< Number of times electromagnetic intereference detected by  magnetic card reader (hw+drivers don't support so actually counts aggregate track read counters instead) */
    DIAG_COUNT_SMARTCARD_INSERTIONS     		= 145,  /**< Number of smart card insertions with no errors */
    DIAG_COUNT_SMARTCARD_ERRORS        		= 146,  /**< Number of smart card insertions with errors */
    DIAG_COUNT_CONTACTLESS_READS      			= 147,	/**< Number of contactless card reads */
    DIAG_COUNT_CONTACTLESS_ERRORS     			= 148,	/**< Number of contactless card errors */
    DIAG_COUNT_TOUCHPANEL_CALIBRATIONS	 		= 149,	/**< Number of times capacitive touch panel has been calibrated */
    DIAG_COUNT_TOUCHPANEL_CALIBRATION_TIME_STAMP 	= 150,	/**< Time (number of seconds since 1970/01/01 epoch) that capacitive touch panel was last calibrated */
    DIAG_COUNT_SIGNATURE_CAPTURES_PERFORMED      	= 151,	/**< Number of signature captures performed */
    DIAG_COUNT_USB_SERIAL_DISCONNECTS	 		= 152,	/**< Number of times a USB cable disconnection has been detected on USB gadget */
    DIAG_COUNT_TAILGATE_NAKS_SENT		 	= 153,	/**< Number of NAKs sent by terminal during ECR tailgate protocol */
    DIAG_COUNT_TAILGATE_NAKS_RECEIVED	 		= 154,	/**< Number of NAKs received by terminal during ECR tailgate protocol */
    DIAG_COUNT_TOUCHPANEL_DATA_SAMPLES			= 155,  /**< Number of individual capacitive or resistive touch panel data samples received */
    DIAG_COUNT_UBI_MAX_ERASE_COUNT			= 156,  /**< Number of times the flash memory has been completely erased (same as # that shows up near bottom of screen during reboots) */
    DIAG_COUNT_TOUCHPANEL_SCALING_CALC			= 157,	/**< Number of times touch panel scaling constants have been recalculated */
    DIAG_COUNT_TOUCHPANEL_SCALING_CALC_TIME_STAMP 	= 158,	/**< Time (number of seconds since 1970/01/01 epoch) of last touch panel scaling constants recalculation */
    DIAG_COUNT_TAILGATE_POLL_ADDRESS		 	= 159,	/**< Current poll address for ECR tailgate protocol */
    DIAG_COUNT_CUMULATIVE_UPTIME		 	= 160,	/**< Cumulative uptime of the unit - 30second granuality */
    DIAG_COUNT_HIGH_TEMP_SHUT_STATE_NORMAL	 	= 161,	/**< Number of times temperature returns to normal */
    DIAG_COUNT_HIGH_TEMP_SHUT_STATE_WARN	 	= 162,	/**< Number of times temperature is high */
    DIAG_COUNT_HIGH_TEMP_SHUT_STATE_DANGER	 	= 163,	/**< Number of times temperature reaches critical level */
    DIAG_COUNT_MCR_MTR_RUNTIME				= 164,  /**< Total motor run time */
    DIAG_COUNT_MCR_MTR_ERROR				= 165,  /**< Number of error for motor */
    DIAG_COUNT_MCR_SHTR_ERROR				= 166,  /**< Number of error for shutter solenoid */
    DIAG_COUNT_MCR_SW_ERROR				= 167,  /**< Number of sensors error */
    DIAG_COUNT_MCR_EJECT_CNT				= 168,  /**< Number of eject due to error */
    DIAG_COUNT_MCR_MAX_CURRENT				= 169,  /**< maximum current in mA */
    DIAG_COUNT_PRINTER_VP_INIT_ERROR        = 170,  /**< Printer Vp Initilization error */
    DIAG_COUNT_PRINTER_CUT_ERROR_1          = 171,  /**< Printer cutter cannot exit in specified steps, but blade moves to home position after retry. */
    DIAG_COUNT_PRINTER_CUT_ERROR_2          = 172,  /**< Printer cutter cannot exit in specified steps, and blade does not return to home position after retry. */
    DIAG_COUNT_PRINTER_CUT_ERROR_3          = 173,  /**< Printer cutter initialization error. */

    // Enter new opsys counters here

    /** 
     * last diagnostic counter
     */
    DIAG_COUNT_SYSTEM_FLAGS				= (DIAG_COUNTERS_ALLOCATED-1),  /**< Opsys use only: Bit flags used to control various debugging features such as console during boot-up */
 };


struct diag_counter_info
{
    char text[17]; /**< Max of 16 chars + 1 null */
    __u32 groups;
    __u32 flags;
    __s32 value;
    __s16 index;
};

struct diag_counter_info_group
{
    char text[13]; /**< Max of 12 chars + null */
    __u32 group;
};

#define DIAG_COUNTERS_METHOD __attribute__((unused)) static __inline int

DIAG_COUNTERS_METHOD diag_counter_clr(__s16 index);
DIAG_COUNTERS_METHOD diag_counter_set(__s16 index,__s32 value);
DIAG_COUNTERS_METHOD diag_counter_get(__s16 index,__s32 *value);
DIAG_COUNTERS_METHOD diag_counter_add(__s16 index,__s32 value);
DIAG_COUNTERS_METHOD diag_counter_sub(__s16 index,__s32 value);
DIAG_COUNTERS_METHOD diag_counter_inc(__s16 index);
DIAG_COUNTERS_METHOD diag_counter_dec(__s16 index);
DIAG_COUNTERS_METHOD diag_counter_set_bits(__s16 index,__s32 bitMask);
DIAG_COUNTERS_METHOD diag_counter_clr_bits(__s16 index,__s32 bitMask);
DIAG_COUNTERS_METHOD diag_counter_set_time(__s16 index);
DIAG_COUNTERS_METHOD diag_counters_get_all(__s32 *buff);


#ifndef __VAULT__
#ifndef __KERNEL__
DIAG_COUNTERS_METHOD diag_counter_set_info(__s16 index,struct diag_counter_info *info); /**< Method needed only in user space */
DIAG_COUNTERS_METHOD diag_counter_get_info(__s16 index,struct diag_counter_info *info); /**< Method needed only in user space */
DIAG_COUNTERS_METHOD diag_counter_get_info_group(struct diag_counter_info_group *ginfo); /**< Method needed only in user space */
DIAG_COUNTERS_METHOD diag_counters_get_info_all(struct diag_counter_info *info); /**< Method needed only in user space */
#endif
#endif

enum diag_counter_group_mask
{
    OS 		= (1<<0),
    APP 	= (1<<1),
    FLASH	= (1<<2),
    KPAD 	= (1<<3),
    SYS 	= (1<<4),
    MSR 	= (1<<5),
    SCR		= (1<<6),
    CTLS 	= (1<<7),
    TOUCH 	= (1<<8),
    ECR		= (1<<9),
    PIN		= (1<<10),
    USB		= (1<<11),
    NFC		= (1<<12),
    MCR		= (1<<13),
    PRINT   = (1<<14),
};

enum diag_counter_flags_mask
{
    RESERVED	= (1<<0),
    TIME	= (1<<1),
    HEX		= (1<<2),
    SECRET	= (1<<3),
    ASCII	= (1<<4),
};

#ifndef DIAG_COUNTERS_MAGIC
#define DIAG_COUNTERS_MAGIC 		0xAA
#endif

#ifndef __VAULT__

struct diag_counter_ioctl_args
{
    __s16 index;
    union
    {
	__s32 value;
	__s32 *ps32;
	void *pvoid;
    };
};

#define DIAG_COUNTER_CLR		_IOW(DIAG_COUNTERS_MAGIC,1,struct diag_counter_ioctl_args)
#define DIAG_COUNTER_SET		_IOW(DIAG_COUNTERS_MAGIC,2,struct diag_counter_ioctl_args)
#define DIAG_COUNTER_SET_INFO		_IOW(DIAG_COUNTERS_MAGIC,3,struct diag_counter_ioctl_args)
#define DIAG_COUNTER_GET_INFO		_IOR(DIAG_COUNTERS_MAGIC,4,struct diag_counter_ioctl_args)
#define DIAG_COUNTERS_GET_INFO_ALL	_IOR(DIAG_COUNTERS_MAGIC,5,struct diag_counter_ioctl_args)
#define DIAG_COUNTER_GET_INFO_GROUP	_IOR(DIAG_COUNTERS_MAGIC,6,struct diag_counter_ioctl_args)
#define DIAG_COUNTER_GET		_IOR(DIAG_COUNTERS_MAGIC,7,struct diag_counter_ioctl_args)
#define DIAG_COUNTER_ADD		_IOW(DIAG_COUNTERS_MAGIC,8,struct diag_counter_ioctl_args)
#define DIAG_COUNTER_SUB		_IOW(DIAG_COUNTERS_MAGIC,9,struct diag_counter_ioctl_args)
#define DIAG_COUNTER_INC		_IOW(DIAG_COUNTERS_MAGIC,10,struct diag_counter_ioctl_args)
#define DIAG_COUNTER_DEC		_IOW(DIAG_COUNTERS_MAGIC,11,struct diag_counter_ioctl_args)
#define DIAG_COUNTER_SET_BITS		_IOW(DIAG_COUNTERS_MAGIC,12,struct diag_counter_ioctl_args)
#define DIAG_COUNTER_CLR_BITS		_IOW(DIAG_COUNTERS_MAGIC,13,struct diag_counter_ioctl_args)
#define DIAG_COUNTER_SET_TIME		_IOW(DIAG_COUNTERS_MAGIC,14,struct diag_counter_ioctl_args)
#define DIAG_COUNTERS_CLR_ALL		_IOW(DIAG_COUNTERS_MAGIC,15,struct diag_counter_ioctl_args)
#define DIAG_COUNTERS_SET_ALL		_IOW(DIAG_COUNTERS_MAGIC,16,struct diag_counter_ioctl_args)
#define DIAG_COUNTERS_GET_ALL		_IOR(DIAG_COUNTERS_MAGIC,17,struct diag_counter_ioctl_args)

#define DIAG_COUNTERS_NAME	"diag_counters"

#ifndef __KERNEL__ /* User Space */

#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>

/**
 * Slower with 0 but easier to unload driver as not locked by handles
 */
#ifndef DIAG_COUNTERS_HANDLE_PREALLOCATE
#define DIAG_COUNTERS_HANDLE_PREALLOCATE 0
#endif


#if DIAG_COUNTERS_HANDLE_PREALLOCATE
static int diag_counters_handle = -1; /**< Handle allocated by constructor function */

/**
 * If handle not yet established, try to establish then return 1 if now a good handle, 0 otherwise
 */
__attribute__((constructor))
DIAG_COUNTERS_METHOD diag_counters_handle_ok(void)
{
    if (diag_counters_handle < 0) diag_counters_handle = open("/dev/" DIAG_COUNTERS_NAME,O_RDWR);
    return diag_counters_handle < 0 ? 0 : 1;
}

__attribute__((destructor))
__attribute__((unused)) static __inline void diag_counters_close(void)
{
    if (diag_counters_handle >= 0) close(diag_counters_handle);
}
#endif

#ifdef RAPTOR
#define diag_counters_call(cmd, index, arg)	vfiSec_DiagCall(cmd, index, (void*)arg)
extern int vfiSec_DiagCall(unsigned int command, __s16 index, void *vp);
#else
#define diag_counters_call(cmd, index, arg)	diag_counters_ioctl(cmd, index, arg)
DIAG_COUNTERS_METHOD diag_counters_ioctl(unsigned int command, __s16 index, union { __s32 v; void *vp; } __attribute__((transparent_union)) arg)
{
    int ret;
#if DIAG_COUNTERS_HANDLE_PREALLOCATE == 0
    int diag_counters_handle = open("/dev/" DIAG_COUNTERS_NAME,O_RDWR);
    if (diag_counters_handle < 0) return -ENXIO;
#else
    if (!diag_counters_handle_ok()) return -ENXIO;
#endif
    ret = ioctl(diag_counters_handle,command,&(struct diag_counter_ioctl_args){ index,{arg.v} });
#if DIAG_COUNTERS_HANDLE_PREALLOCATE == 0
    close(diag_counters_handle);
#endif
    return ret;
}
#endif

DIAG_COUNTERS_METHOD diag_counter_clr(__s16 index)
{
    return diag_counters_call(DIAG_COUNTER_CLR,index,0);
}

DIAG_COUNTERS_METHOD diag_counter_set(__s16 index,__s32 value)
{
    return diag_counters_call(DIAG_COUNTER_SET,index,value);
}

DIAG_COUNTERS_METHOD diag_counter_set_info(__s16 index,struct diag_counter_info *info)
{
    return diag_counters_call(DIAG_COUNTER_SET_INFO,index,info);
}

DIAG_COUNTERS_METHOD diag_counter_get_info(__s16 index,struct diag_counter_info *info)
{
    return diag_counters_call(DIAG_COUNTER_GET_INFO,index,info);
}

DIAG_COUNTERS_METHOD diag_counters_get_info_all(struct diag_counter_info *info)
{
    return diag_counters_call(DIAG_COUNTERS_GET_INFO_ALL,0,info);
}

DIAG_COUNTERS_METHOD diag_counter_get_info_group(struct diag_counter_info_group *infog)
{
    return diag_counters_call(DIAG_COUNTER_GET_INFO_GROUP,0,infog);
}

DIAG_COUNTERS_METHOD diag_counter_get(__s16 index,__s32 *value)
{
    return diag_counters_call(DIAG_COUNTER_GET,index,value);
}

DIAG_COUNTERS_METHOD diag_counter_add(__s16 index,__s32 value)
{
    return diag_counters_call(DIAG_COUNTER_ADD,index,value);
}

DIAG_COUNTERS_METHOD diag_counter_sub(__s16 index,__s32 value)
{
    return diag_counters_call(DIAG_COUNTER_SUB,index,value);
}

DIAG_COUNTERS_METHOD diag_counter_inc(__s16 index)
{
    return diag_counters_call(DIAG_COUNTER_INC,index,0);
}

DIAG_COUNTERS_METHOD diag_counter_dec(__s16 index)
{
    return diag_counters_call(DIAG_COUNTER_DEC,index,0);
}

DIAG_COUNTERS_METHOD diag_counter_set_bits(__s16 index,__s32 bitMask)
{
    return diag_counters_call(DIAG_COUNTER_SET_BITS,index,bitMask);
}

DIAG_COUNTERS_METHOD diag_counter_clr_bits(__s16 index,__s32 bitMask)
{
    return diag_counters_call(DIAG_COUNTER_CLR_BITS,index,bitMask);
}

DIAG_COUNTERS_METHOD diag_counter_set_time(__s16 index)
{
    return diag_counters_call(DIAG_COUNTER_SET_TIME,index,0);
}

DIAG_COUNTERS_METHOD diag_counters_clr_all(void)
{
    return diag_counters_call(DIAG_COUNTERS_CLR_ALL,0,0);
}

DIAG_COUNTERS_METHOD diag_counters_set_all(__s32 *buff)
{
    return diag_counters_call(DIAG_COUNTERS_SET_ALL,0,buff);
}

DIAG_COUNTERS_METHOD diag_counters_get_all(__s32 *buff)
{
    return diag_counters_call(DIAG_COUNTERS_GET_ALL,0,buff);
}

#endif // __KERNEL__
#endif // __VAULT__

#ifdef __cplusplus
}
#endif

#endif // _DIAG_COUNTERS_API_H_
