/* (C) Verifone 2009. */

#ifndef __CIB_H
#define __CIB_H

#include <linux/types.h>

#ifdef __cplusplus
extern "C" {
#endif

#define CIB_MAGIC_NUM 0xA99256D9


typedef struct __DdrConfig
{
	__u32 Width;		//the board width 0=32 1=16
	__u32 Rows;			//Rows
	__u32 Cols;			//Cols
	__u32 Size;			//Size
	__u32 BoardDelay;	//0 //delay between the board and the memory
	__u32 DS;			//1 //this is the drive strength = ampers into the memory
	__u32 TCSR;			//0
	__u32 PASR;			//0
	__u32 CasLatency;	//3 //cl
	__u32 WR2RD_NOP;	//the amount of nops between write and read depends on CL = 3
	__u32 RD2WR_NOP;	//the amount of nops read and write depends on CL = 4
	__u32 TRRD_NOP;		//in nS (not in nops)
	__u32 TRP_NOP;		//in nS (not in nops)
	__u32 TXSR_NOP;		//in nS (not in nops)
	__u32 TMRD_NOP;		//in nS (not in nops)
	__u32 TRCD_NOP;		//in nS (not in nops)
	__u32 TWR_NOP;		//in nS (not in nops)
	__u32 TRAS_NOP;		//in nS (not in nops)
	__u32 TRFC_NOP;		//in nS (not in nops)
	__u32 REFRESH;		// in ns

} DDRConfig;


typedef struct
{
	__u8 PowerICType;
	__u8 VF1101_IIC_Addr[2];
	__u8 PMIC_GPIO_A_CONFIG;
	__u8 PMIC_GPIO_B_CONFIG;
	__u8 PMIC_GPIO_C_CONFIG;
	__u8 PMIC_GPIO_D_CONFIG;
	__u8 PMIC_GPIO_DATA;
	__u8 PMIC_PWM_BANK0;
	__u8 PMIC_PWM_BANK1;
	__u8 PMIC_ENABLE;
	__u8 PMIC_SHUTDOWN;
	__u8 PMIC_MODE;
	__u8 PMIC_ADJSP;
	__u8 PMIC_BSTCSP;
	__u8 PMIC_BSTVSP;
	__u8 HW_REV;
	__u8 Spare[15];

} SystemInfo;


typedef struct
{
	__u32 BBL_REG_0x08;
	__u32 BBL_REG_0x0B;
	__u32 BBL_REG_0x0D;
	__u32 BBL_REG_0x0E;
	__u32 BBL_REG_0x11;
	__u32 BBL_REG_0x12;
	__u8 Spare[16];

} BBL_TamperSetup;


typedef struct
{
	__u8 NAND_ID[2];
	__u8 NANDCycle[4];
	__u8 Spare[6];

} NANDInfo;


typedef struct
{
	__u8 mDDRId[4];
	DDRConfig DDR;
	__u8 Spare[44];

} mDDRInfo;


typedef struct
{
	__u8 Type;
	__u8 PortNum;
	__u8 EEDevAddr[2];
	__u8 Size[4];
	__u8 Spare[8];

} SERIALInfo;


typedef struct
{
	NANDInfo NInfo;
	mDDRInfo DInfo;
	SERIALInfo SInfo[3];

} MemoryInfo;


typedef struct
{
	__u8 SKType[2];
	__u8 KPMap[48];
	__u8 ATMKeyStyle;
	__u8 Type;
	__u8 PortNum;
	__u8 Spare[11];

} KeyPadInfo;


typedef struct
{
	__u8 Type;
	__u8 PortNum;
	__u8 ControllerID;
	__u8 PortAddr[2];
	char CompatibilityMode[1];
	__u8 Spare[10];

} TouchSCRInfo;


typedef struct
{
	__u8 LCDTiming[4][4];	/* Added one forgotten slot according to Mehran's excel 5-Jan-2010 */
	__u8 LCDControl[4];

} LCDRegs;


typedef struct
{
	__u8 Type;
	__u8 PortNum;
	__u8 ControllerID[2];
	__u8 Color;
	__u8 Depth[3];
	__u8 HorizalResolution[4];
	__u8 VerticalResolution[4];
	__u8 RGBFormat[8];
	__u16 VideoDecoder;
	__u8 ContrastLevel[2];
	__u8 ContractMinLevel[2];
	__u8 ContractMaxLevel[2];
	LCDRegs Regs;
	__u8 isGpioId;
	__u8 LCDTableIndex[16];
	__u8 Spare[3];

} DisplayInfo;

typedef struct
{
	__u8 Type;
	__u8 PortNum;
	__u8 ControllerID;
	__u8 HorizontalResolution;
	__u8 VerticalResolution;
	__u8 Backlight;
	__u8 Spare[2];

} CustomerDisplayInfo;

typedef struct
{
    __u8 Portable;
    __u8 PrimarySuppVolt[3];
    __u8 PrimarySuppCurr[3];
    __u8 SecSuppThreshold[3];
    __u8 SecSuppCurr[3];
    __u8 PFThresholdType;
    __u8 Spare[2];

} PowerManagementInfo;

typedef struct
{
	__u8 Type;
	__u8 ControllerID;
	__u8 PortNum;
	__u8 PortAddr[2];
    __u8 AdcSensitivity;
    __u8 T0ScanRate;
    __u8 T1ScanRate;
    __u8 T2ScanRate;
    __u8 Spare[7];

} MSRInfo;


typedef struct
{
	__u8 Type;
	__u8 PortNum;
	__u8 ControllerID;
	__u8 IsPrimarySocket;
	__u8 NumOfSecSockets;
	__u8 IsMsamSwitch;
	__u8 Motor;
	__u8 Spare[9];

} IccInfo;


typedef struct
{
	__u8 Type;
	__u8 PortNum;
	__u8 ControllerID;
	__u8 ConfigurationID;
	__u8 Spare[4];

} ContactlessInfo;


typedef struct
{
	__u8 Type[2];
	__u8 MotorController;
	__u8 MotorStepControl;
	__u8 MaxOnDotPerStrobe[2];
	__u8 DefaultDarkness[2];
	__u8 MaxDarkness[2];
	__u8 MaxSpeed[2];
	__u8 Spare[12];

} PrinterInfo;


typedef struct
{
	__u8 Uart0;
	__u8 Uart1;
	__u8 Uart2;
	__u8 Uart3;
	__u8 Spare[12];

} UartInfo;


typedef struct
{
	__u8 PowerBudget[3];
	__u8 Usb0[4];
	__u8 Usb1[4];
	__u8 Usb2[4];
	__u8 Spare[17];

} UsbInfo;


typedef struct
{
	__u8 Ethernet;
	__u8 LinkLED;
	__u8 ActivityLED;
	__u8 Spare[9];

} EthernetInfo;


typedef struct
{
	__u8 Type;
	__u8 PortNum;
	__u8 ControllerID;
	__u8 MDM_CTSPort;
	__u8 MDM_CTSPin[2];
	__u8 MDM_RTSPort;
	__u8 MDM_RTSPin[2];
	__u8 MDM_DTRPort;
	__u8 MDM_DTRPin[2];
	__u8 MDM_DCDPort;
	__u8 MDM_DCDPin[2];
	__u8 MDM_DSRPort;
	__u8 MDM_DSRPin[2];
	__u8 MDM_RIPort;
	__u8 MDM_RIPin[2];
	__u8 Spare[3];

} ModemInfo;


typedef struct
{
	__u8 Type;
	__u8 PortNum;
	__u8 ControllerID;
	__u8 HandshakingConfig;
	__u8 Spare[13];
	__u8 Dual_SIM;
	__u8 SIM_Toggle;	
	__u8 Spare2[5];

} RadioInfo;


typedef struct
{
	__u8 Type;
	__u8 PortNum;
	__u8 ControllerID;

} GPSInfo;


typedef struct
{
	__u8 Type;
	__u8 GPIOPort;
	__u8 PortPin[2];
	__u8 Spare[4];

} BuzzerInfo;


typedef struct
{
	__u8 Type;
	__u8 PortNum;
	__u8 ControllerID;
	__u8 SecIntType;
	__u8 SecPortNum;
	__u8 Address[2];
	__u8 DefSetting[2];
	__u8 MinVolume[2];
	__u8 MaxVolume[2];
	__u8 Spare[11];

} SpeakerInfo;


typedef struct
{
	__u8 Type;
	__u8 PortNum;
	__u8 ControllerID;
	__u8 BatteryRequired;
	__u8 BatteryCapacity;
	__u8 Spare[3];

} BatteryInfo;


typedef struct
{
	__u8 IsCircuit;
	__u8 CHRG_ONPort;
	__u8 CHRG_ONPin[2];
	__u8 STAT1port;
	__u8 STAT1pin[2];
	__u8 STAT2port;
	__u8 STAT2pin[2];
	__u8 ControllerID;
	__u8 Spare[8];

} ChargerInfo;

typedef struct
{
	__u8 Type;
	__u8 PortNum;
	__u8 SensorType;
	__u8 RemoteSensingPoints;
	__u8 SensingFrequency;
	__u8 SensingRange;
	__u8 Spare[2];

} TemperatureSensorInfo;

typedef struct
{
	__u8 Type;
	__u8 PortNum;
	__u8 Input0VideoStandard[2];
	__u8 Input1VideoStandard[2];
	__u8 OutputVideoStandard[2];
	__u8 DefaultDisplayMode;
	__u8 ControllerID;
	__u8 Spare[3];

} VideoSwitchInfo;

typedef struct
{
	__u8 InternalDecoder;
	__u8 Spare[7];

} VideoInfo;


typedef struct
{
	__u8 InternalPinPad;
	__u8 Spare[7];

} PinPadInfo;


typedef struct
{
	__u8 IsSocket;
	__u8 Spare[3];

} SDIOInfo;


typedef struct
{
	__u8 Type;
	__u8 PortNum;
	__u8 PortPin[2];
	__u8 ControllerID;
	__u8 DefBrightness[2];
	__u8 MinLevel[2];
	__u8 MaxLevel[2];
	__u8 Spare[9];

} St_Backlight;


typedef struct
{
	__u8 MsrLed1Port;
	__u8 MsrLed1Pin[2];
	__u8 MsrLed2Port;
	__u8 MsrLed2Pin[2];
	__u8 MsrLed3Port;
	__u8 MsrLed3Pin[2];
	__u8 ControllerID;
	__u8 Spare[6];

} MSRLedInfo;


typedef struct
{
	__u8 Led1Port;
	__u8 Led1Pin[2];
	__u8 Spare[5];

} ICCLedInfo;


typedef struct
{
	char Port;
	char Pin[2];
	char Spare[5];

} DiscreteLedInfo;


typedef struct
{
	St_Backlight BackL[2];
	MSRLedInfo MSRLed;
	ICCLedInfo ICCLed;
	DiscreteLedInfo DiscreteLed[8];

} LedAndBcklInfo;


typedef struct
{
	__u8 Type;
	__u8 PortNum;

} BarcodeReadInfo;


typedef struct
{
	__u8 Type;
	__u8 PortNum;

} WrenchmanInfo;

typedef struct
{
	__u8 Type;
	__u8 PortNum;
	__u8 PortAddr[2];
	__u8 ControllerID;

} ProximitySensorInfo;

typedef struct
{
	__u8 Type;
	__u8 PortNum;
	__u8 Spare[2];
} StandbyMcuInfo;

typedef struct
{
	__u8 Type;
	__u8 PortNum;
	__u8 ControllerID;
	__u8 Spare[3];

} FiscalInfo;

#define CIB_MODULEID_LEN 12
#define CIB_PRODUCTNUM_LEN 11

typedef struct
{
	__u32 signature;
	__u32 fileSize;
	__u8 dateTime[14]; /* DD/MM/YYYY hh:mm:ss */
	__u8 version[4];
	__u8 ModelID[CIB_MODULEID_LEN];
	__u8 ProductNumber[CIB_PRODUCTNUM_LEN];
	__u8 spare[15];

} CIBHeaderInfo;


typedef struct
{
	CIBHeaderInfo HeaderInfo;
	SystemInfo SysInfo;
	BBL_TamperSetup BblTampSetup;
	MemoryInfo Memory;
	KeyPadInfo KeyPad;
	TouchSCRInfo TouchScr;
	DisplayInfo Display;
	CustomerDisplayInfo CustomerDisplay;
	PowerManagementInfo Power;
	MSRInfo MSR;
	IccInfo Icc;
	ContactlessInfo Contactless;
	PrinterInfo Printer;
	UartInfo Uart;
	UsbInfo Usb;
	EthernetInfo Ethernet;
	ModemInfo Modem;
	RadioInfo Radio[4];
	__u8 SpareRadio[12];
	GPSInfo GPS;
	StandbyMcuInfo StandbyMcu;
	ProximitySensorInfo ProximitySensor;
	BuzzerInfo Buzzer;
	SpeakerInfo Speaker;
	BatteryInfo Battery;
	ChargerInfo Charger;
	TemperatureSensorInfo TemperatureSensor;
	VideoSwitchInfo VideoSwitch;
	VideoInfo Video;
	PinPadInfo PinPad;
	SDIOInfo SDIO[2];
	LedAndBcklInfo LedAndBckl;
	BarcodeReadInfo BarcodeReader;
	WrenchmanInfo Wrenchman;
	FiscalInfo Fiscal;
	__u8 Spare[10];
	__u32 CRC;

} CIB_Struct;


typedef enum
{
	CIB_NOT_EXIST = 0,
	CIB_AUTH_OK,
	CIB_AUTH_FAIL,
	CIB_CRC_FAIL,

} CIB_Status;

#ifdef __cplusplus
}
#endif

#endif /* __CIB_H. */
