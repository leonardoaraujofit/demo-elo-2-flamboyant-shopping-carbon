#ifndef _DIAG_COUNTERS_H_
#define _DIAG_COUNTERS_H_

#ifndef __VAULT__

#include <verifone/bbsram_map.h>

#ifdef __KERNEL__

#include <linux/spinlock.h>
#include <linux/time.h>

#else

#include <errno.h>
#include <syslog.h>

#endif

#else // __VAULT__

#include <bbsram_map.h>
#include <bbl/bbl.h>
#include "errno.h"
typedef s16 __s16;
typedef u32 __u32;
typedef s32 __s32;

#endif

#define DIAG_COUNTERS_MAGIC_OS 0xDC // Opsys

#define DIAG_COUNTERS_MAGIC DIAG_COUNTERS_MAGIC_OS // Apps do NOT set so it adopts a (different) default value

#include "diag_counters_API.h"

#ifdef __cplusplus
extern "C" {
#endif

// Method Protoypes
#define diag_counters_map() _diag_counters_map((char *)__func__)
#define diag_counters_unmap() _diag_counters_unmap((char *)__func__)

// Never to be used by apps
DIAG_COUNTERS_METHOD diag_counters_clr_all(void);
DIAG_COUNTERS_METHOD diag_counters_set_all(__s32 *buff);

#define DIAG_COUNTERS_VERSION_MINOR	2 // Minor Version
#define DIAG_COUNTERS_VERSION_MAJOR	1 // Major Version
#define DIAG_COUNTERS_VALID_MARKER	0xD1AC0000 // Validity Marker shifted to avoid silly vault ARM compiler warnings
#define DIAG_COUNTERS_ID 		(DIAG_COUNTERS_VALID_MARKER | (DIAG_COUNTERS_VERSION_MAJOR << 8) | DIAG_COUNTERS_VERSION_MINOR)
#define DIAG_COUNTERS_ID_MASK		0xFFFFFF00 // All of the above except version minor

#ifndef __VAULT__
#ifdef __KERNEL__ // Kernel Space

__attribute__((weak))
atomic_t *diag_counters = 0; // Pointer to mapped counter block

__attribute__((weak))
spinlock_t diag_counters_spinlock;

DIAG_COUNTERS_METHOD diag_counter_clr(__s16 index)	{ return diag_counters ? atomic_set(&diag_counters[index],0),0 : -ENXIO; }

DIAG_COUNTERS_METHOD diag_counter_set(__s16 index,__s32 value)	{ return diag_counters ? atomic_set(&diag_counters[index],value),0 : -ENXIO; }

DIAG_COUNTERS_METHOD diag_counter_get(__s16 index,__s32 *value) { return diag_counters ? *value = atomic_read(&diag_counters[index]),0 : -ENXIO; }

DIAG_COUNTERS_METHOD diag_counter_add(__s16 index,__s32 value)	{ return diag_counters ? atomic_add(value,&diag_counters[index]),0 : -ENXIO; }

DIAG_COUNTERS_METHOD diag_counter_sub(__s16 index,__s32 value)	{ return diag_counters ? atomic_sub(value,&diag_counters[index]),0 : -ENXIO; }

DIAG_COUNTERS_METHOD diag_counter_inc(__s16 index)	{ return diag_counters ? atomic_inc(&diag_counters[index]),0 : -ENXIO; }

DIAG_COUNTERS_METHOD diag_counter_dec(__s16 index)	{ return diag_counters ? atomic_dec(&diag_counters[index]),0 : -ENXIO; }

DIAG_COUNTERS_METHOD diag_counter_set_bits(__s16 index,__s32 bitMask)
{
    if (!diag_counters) return -ENXIO;
    spin_lock(&diag_counters_spinlock);
    atomic_set(&diag_counters[index],atomic_read(&diag_counters[index]) | bitMask);
    spin_unlock(&diag_counters_spinlock);
    return 0;
}    

DIAG_COUNTERS_METHOD diag_counter_clr_bits(__s16 index,__s32 bitMask)
{
    if (!diag_counters) return -ENXIO;
    spin_lock(&diag_counters_spinlock);
    atomic_set(&diag_counters[index],atomic_read(&diag_counters[index]) & ~bitMask);
    spin_unlock(&diag_counters_spinlock);
    return 0;
}    

DIAG_COUNTERS_METHOD diag_counter_set_time(__s16 index)
{
    struct timeval tv;
    if (!diag_counters) return -ENXIO;
    do_gettimeofday(&tv);
    atomic_set(&diag_counters[index],tv.tv_sec);
    return 0;
}    

DIAG_COUNTERS_METHOD diag_counters_clr_all(void)
{
    __s16 index;
    if (!diag_counters) return -ENXIO;
    for (index = 0; index <  DIAG_COUNTERS_ALLOCATED; index++) diag_counter_clr(index);
    diag_counter_set(DIAG_COUNT_ID, DIAG_COUNTERS_ID);
    return 0;
}

DIAG_COUNTERS_METHOD diag_counters_set_all(__s32 *buff)	
{
    __s16 index;
    if (!diag_counters) return -ENXIO;
    for (index = 0; index <  DIAG_COUNTERS_ALLOCATED; index++) diag_counter_set(index,buff[index]);
    diag_counter_set(DIAG_COUNT_ID, DIAG_COUNTERS_ID);
    return 0;
}

DIAG_COUNTERS_METHOD diag_counters_get_all(__s32 *buff)
{
    __s16 index;
    if (!diag_counters) return -ENXIO;
    for (index = 0; index <  DIAG_COUNTERS_ALLOCATED; index++) diag_counter_get(index,&buff[index]);
    return 0;
}

#if  (DIAG_COUNTERS_SIZE != BBSRAM_SYSTEM_LOG_SIZE)
#error Mismatch between diagnostic counters size defined and size allocated
#endif

DIAG_COUNTERS_METHOD _diag_counters_map(char *message)
{
      printk("%s(%s)\n",__func__,message);
      diag_counters = ioremap(BBSRAM_SYSTEM_LOG_START_ADDRESS,BBSRAM_SYSTEM_LOG_SIZE);
      if (IS_ERR(diag_counters)) 
      {
	    diag_counters = 0;
 	    printk("Unable to map diagnostic counters\n");
	    return -ENOMEM;
      }
      spin_lock_init(&diag_counters_spinlock);
      return 0;
}

__attribute__((unused)) 
static __inline void _diag_counters_unmap(char *message)
{
      printk("%s(%s)\n",__func__,message);
      if (diag_counters) iounmap(diag_counters);
}

#endif
#endif

#ifdef __VAULT__

extern __s32 *diag_counters;

DIAG_COUNTERS_METHOD diag_counter_clr(__s16 index)	{ return diag_counters ? diag_counters[index] = 0, 0 : -ENXIO; }

DIAG_COUNTERS_METHOD diag_counter_set(__s16 index,__s32 value) { return diag_counters ? diag_counters[index] = value, 0 : -ENXIO; }

DIAG_COUNTERS_METHOD diag_counter_get(__s16 index,__s32 *value) { return diag_counters ? *value = diag_counters[index], 0 : -ENXIO; }

DIAG_COUNTERS_METHOD diag_counter_add(__s16 index,__s32 value) { return diag_counters ? diag_counters[index] += value, 0 : -ENXIO; }

DIAG_COUNTERS_METHOD diag_counter_sub(__s16 index,__s32 value) { return diag_counters ? diag_counters[index] -= value, 0 : -ENXIO; }

DIAG_COUNTERS_METHOD diag_counter_inc(__s16 index)	{ return diag_counters ? diag_counters[index]++, 0 : -ENXIO; }

DIAG_COUNTERS_METHOD diag_counter_dec(__s16 index)	{ return diag_counters ? diag_counters[index]--, 0 : -ENXIO; }

DIAG_COUNTERS_METHOD diag_counter_set_bits(__s16 index,__s32 bitMask) { return diag_counters ? diag_counters[index] |= bitMask, 0 : -ENXIO; }

DIAG_COUNTERS_METHOD diag_counter_clr_bits(__s16 index,__s32 bitMask) { return diag_counters ? diag_counters[index] &= ~bitMask, 0 : -ENXIO; }

DIAG_COUNTERS_METHOD diag_counter_set_time(__s16 index) { return diag_counters ? BBL_ReadRtc((ulint *)&diag_counters[index]), 0 : -ENXIO; }

DIAG_COUNTERS_METHOD diag_counters_clr_all(void)
{
    __s16 index;
    if (!diag_counters) return -ENXIO;
    for (index = 0; index <  DIAG_COUNTERS_ALLOCATED; index++) diag_counter_clr(index);
    diag_counter_set(DIAG_COUNT_ID, DIAG_COUNTERS_ID);
    return 0;
}

DIAG_COUNTERS_METHOD diag_counters_set_all(__s32 *buff)	
{
    __s16 index;
    if (!diag_counters) return -ENXIO;
    for (index = 0; index <  DIAG_COUNTERS_ALLOCATED; index++) diag_counter_set(index,buff[index]);
    diag_counter_set(DIAG_COUNT_ID, DIAG_COUNTERS_ID);
    return 0;
}

DIAG_COUNTERS_METHOD diag_counters_get_all(__s32 *buff)
{
    __s16 index;
    if (!diag_counters) return -ENXIO;
    for (index = 0; index <  DIAG_COUNTERS_ALLOCATED; index++) diag_counter_get(index,&buff[index]);
    return 0;
}

DIAG_COUNTERS_METHOD _diag_counters_map(char *message) { diag_counters = (__s32 *)BBSRAM_SYSTEM_LOG_START_ADDRESS; return 0; }

__attribute__((unused)) static __inline void _diag_counters_unmap(char *message) { diag_counters = 0; }

#endif // __VAULT__

#ifdef __cplusplus
}
#endif

#endif // _DIAG_COUNTERS_H_
