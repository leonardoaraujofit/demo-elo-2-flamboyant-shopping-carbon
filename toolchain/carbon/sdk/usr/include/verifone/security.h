/****************************************************************************
 *                                                                          *
 * FILE NAME:   Security.DEF                                                *
 *                                                                          *
 * MODULE NAME: Security                                                    *
 *                                                                          *
 * PROGRAMMER:  Moosa Baransi                                               *
 *                                                                          *
 * DESCRIPTION: Definition file of the security components                  *
 *                                                                          *
 * REVISION:    01.00 13/9/2008                                             *
 *                                                                          *
 * HISTORY:     None                                                        *
 *                                                                          *
 ****************************************************************************/

#ifndef _SECURITY_DEF_
#define _SECURITY_DEF_


#include <linux/types.h>


#define SECURITY_VERSION    "02.02.06"

/********************************/
/***** Error/Return Section *****/
/********************************/
/* General */
#define SECURITY_OK                  0x00   /* Execution was successful             */
#define SECURITY_E_NULL_ASSIGN      -0x01   /* Passing NULL pointer to a function   */
#define SECURITY_E_READ             -0x02   /* General read error                   */
#define SECURITY_E_WRITE            -0x03   /* General write error                  */
#define SECURITY_E_MISMATCH         -0x04   /* Security mismatch                    */
#define SECURITY_E_LEN              -0x05   /* length value error                   */
#define SECURITY_E_ATTRIBUTE        -0x06   /* Invalid attribute                    */
#define SECURITY_E_BUF_TOO_SHORT    -0x07   /* Passing short buffer to function     */
#define SECURITY_E_MEM              -0x08   /* General memory error                 */
#define SECURITY_E_MAC              -0x09   /* MAC Verification error               */
#define SECURITY_E_VERSION          -0x0A   /* Version error                        */
#define SECURITY_E_TK               -0x0B   /* Invalid Transport key                */
#define SECURITY_E_STANDARD         -0x0C   /* Invalid/Unknown Standard             */
#define SECURITY_E_DUKPT_END        -0x0D   /* Next Transaction will exceed limit   */
#define SECURITY_E_DUKPT_CNT        -0x0E   /* Counter is empty                     */
#define SECIRITY_E_KCV              -0x0F   /* Wrong Key Check Value                */
#define SECURITY_E_FATAL            -0x10   /* General fatal error                  */
#define SECURITY_E_EMPTY            -0x11   /* Empty buffer / no item               */
#define SECURITY_E_PARITY           -0x12   /* Parity Check Error                   */
#define SECURITY_E_NOT_SUPPORTED    -0x13   /* Operation not supported              */
#define SECURITY_E_MAC_TYPE         -0x14   /* MAC type error                       */
/* DB Specific */
#define SECURITY_E_DB_CORRUPTED     -0x20   /* Database corrupted                   */
#define SECURITY_E_DB_FULL          -0x21   /* Database is full                     */
#define SECURITY_E_DB_OPENED        -0x22   /* Database already opened              */
#define SECURITY_E_DB_CLOSED        -0x23   /* Database is closed                   */
#define SECURITY_E_DB_TYPE          -0x24   /* Invalid database type                */
#define SECURITY_E_DB_FOUND         -0x25   /* Item was found in database           */
#define SECURITY_E_DB_NOT_FOUND     -0x26   /* Item was not found in database       */
#define SECURITY_E_DB_RULE          -0x27   /* DB rule was violated                 */
/* Cryptographic Engine Specific */
#define SECURITY_E_CRYPTO_MODE      -0x40   /* Invalid cryptographic mode           */
#define SECURITY_E_CRYPTO_HASH      -0x41   /* Invalid hash type                    */
#define SECURITY_E_CRYPTO_ALGO      -0x42   /* Invalid cryptographic algorithm      */
#define SECURITY_E_CRYPTO_MAC       -0x43   /* Invalid MAC type                     */
/* Session specific */
#define SECURITY_E_SESSION_MODE     -0x60   /* Invalid session mode                 */
/* PIN & Password specific */
#define SECURITY_E_PB_PINLEN        -0x80   /* Invalid PIN length                   */
#define SECURITY_E_PB_FORMAT        -0x81   /* Invalid PIN format                   */
#define SECURITY_E_PB_NO_PIN        -0x82   /* No PIN exists in the system          */
#define SECURITY_E_PASSWD           -0x83   /* No password is inside the system     */
/* Message Box Errors */
#define SECURITY_E_FUNC_ID          -0xA0   /* Invalid function ID                  */
#define SECURITY_E_CALLER_ID        -0xA1   /* Invalid caller ID                    */
#define SECURITY_E_QUEUE_FULL       -0xA2   /* Message queue is full                */
#define SECURITY_E_QUEUE_NOT_FOUND  -0xA3   /* Message not found in queue           */
#define SECURITY_E_QUEUE_INVALID    -0xA4   /* Message queue not created/invalid    */
#define SECURITY_E_QUEUE_EMPTY      -0xA5   /* Message queue is empty               */
#define SECURITY_E_QUEUE_WAIT       -0xA6   /* Message is not fetched yet           */
#define SECURITY_E_QUEUE_MSG_CR     -0xA7   /* Message corrupted in queue           */
/* Internal state machine error */
#define SECURITY_E_PIN_ST           -0xB0   /* Error in PIN state machine           */
#define SECURITY_E_DISTRBTR_INVALID -0xC0   /* Invalid Distributor                  */

/**************************/
/* Secure session section */
/**************************/
#define SECURITY_SESSION_INIT       0x01    /* Initialize the session to idle       */
#define SECURITY_SESSION_UNSECURE   0x02    /* Normal session                       */
#define SECURITY_SESSION_PIN        0x03    /* PIN entry session                    */
#define SECURITY_SESSION_PASSWORD   0x04    /* Password entry session               */

/*************************/
/***** Database Type *****/
/*************************/
/* !! Do not change the values !! */
#define SECURITY_DB_TYPE_PAYMENT    0x00    /* Payment (PCI) database for PIN/MAC   */
#define SECURITY_DB_TYPE_OS         0x01    /* OS proprietary database              */
#define SECURITY_DB_TYPE_APPL       0x02    /* Application proprietary datanbase    */
/* Add other DB types here... */
#define SECURITY_DB_NUM             3       /* Number of databases in system        */

/******************************/
/***** Key Length Section *****/
/******************************/
/* For DES / AES */
#define SECURITY_KEY_LEN08          8
#define SECURITY_KEY_LEN16          16
#define SECURITY_KEY_LEN24          24
#define SECURITY_KEY_LEN32          32
#define SECURITY_KEY_LEN64          64
#define SECURITY_KEY_LEN128         128
#define SECURITY_KEY_LEN256         256
#define SECURITY_KEY_LEN512         512
#define SECURITY_KEY_LEN1024        1024

/* For RSA */
typedef struct
{
    uint8_t    Modulus[SECURITY_KEY_LEN256];   /* Key Modulus                      */
    uint8_t    PubExp[SECURITY_KEY_LEN256];    /* Public Exponet                   */
    uint8_t    PrvExp[SECURITY_KEY_LEN256];    /* Private Exponent                 */
    uint8_t    Prime1[SECURITY_KEY_LEN128];    /* First Prime                      */
    uint8_t    Prime2[SECURITY_KEY_LEN128];    /* Second Prime                     */
    uint8_t    InvMod[SECURITY_KEY_LEN128];    /* Prime1^(-1) mod Prime2           */
    uint8_t    Exp1[SECURITY_KEY_LEN128];      /* PrvExp mod (Prime1 - 1 )         */
    uint8_t    Exp2[SECURITY_KEY_LEN128];      /* PrvExp mod (Prime2 - 1 )         */
    uint32_t     IsPreCalcFieldsUsed;            /* Is pre-calculated fields used    */
} S_SecurityRsaPrv2k;
/* RSA Private keys up to 2048 bits, should use this structure */
#define SECURITY_KEY_LEN_RSA2K  (sizeof(S_SecurityRsaPrv2k))

/**************************************/
/***** Key Insertion Mode Section *****/
/**************************************/
#define SECURITY_KIM_APP_CLEAR      0x01
#define SECURITY_KIM_APP_CIPHERED   0x02
#define SECURITY_KIM_RANDOM         0x03
/* Do not apply DB rules on this mode */
#define SECURITY_KIM_SUPERVISOR     0x04

/**********************************/
/***** Key Management Section *****/
/**********************************/
#define SECURITY_KEY_MGMT_DUKPT     0x44
#define SECURITY_KEY_MGMT_MS        0x4D
#define SECURITY_KEY_MGMT_FIXED     0x46

/*********************************/
/***** Key Algorithm Section *****/
/*********************************/
#define SECURITY_ALGO_AES           0x41
#define SECURITY_ALGO_DES           0x44
#define SECURITY_ALGO_EC            0x45
#define SECURITY_ALGO_RSA           0x52
#define SECURITY_ALGO_DSA           0x53
#define SECURITY_ALGO_TDEA          0x54
#define SECURITY_ALGO_HMACSHA1      0x56

/****************************/
/***** DB Rules Section *****/
/****************************/
/* These values are flags (bit mask) */
#define SECURITY_DBR_KEY_VALUE_DUP      0x01    /* Check that a key value is not duplicated for more than one key   */
#define SECURITY_DBR_CLEAR_KEY_ERASE    0x02    /* Erase symetric keys when having a clear text key loading         */
#define SECURITY_DBR_KEY_STRENGTH       0x04    /* A key cannot be decypted by a less powerful key                  */

/**********************************/
/***** DB Clear Flags Section *****/
/**********************************/
/* These flags are used when deleting the entire database */
#define SECURITY_DBF_CLR_SYMMETRIC      0x01    /* Delete symmetric keys */
#define SECURITY_DBF_CLR_ASYMMETRIC     0x02    /* Delete asymmetric keys */
#define SECURITY_DBF_CLR_SYSTEM         0x04    /* Delete system keys (Unique Key Types - KLK's) */
#define SECURITY_DBF_CLR_DATA           0x08    /* Delete application data */
#define SECURITY_DBF_CLR_ALL            0xFF    /* Delete the entire database */

/************************************/
/***** Unique Key Type Section *****/
/************************************/
#define SECURITY_UKT_KTK            0x4B
#define SECURITY_UKT_VSSKLK         0x56
#define SECURITY_UKT_GISKEKLK       0x47
#define SECURITY_UKT_TR31KLK        0x54

/************************************/
/***** DB Access Rights Section *****/
/************************************/
#define SECURITY_DB_ACCESS_READ     0x01
#define SECURITY_DB_ACCESS_WRITE    0x02
#define SECURITY_DB_ACCESS_UPDATE   0x04

/**********************/
/***** PIN Status *****/
/**********************/
#define SECURITY_PIN_ST_IDLE        0x01
#define SECURITY_PIN_ST_GET         0x02
#define SECURITY_PIN_ST_EXISTS      0x03
#define SECURITY_PIN_ST_PROCESSED   0x04
#define SECURITY_PIN_ST_ABORT       0x05

/********************************/
/***** Result Types Section *****/
/********************************/
/* PIN Block is returned */
#define SECURITY_RES_PIN            0x01
/* Encrypted/Decrypted data is returned */
#define SECURITY_RES_DATA           0x02
/* MAC'ed data is returned              */
#define SECURITY_RES_MAC            0x03


/*****************************/
/***** Key Usage Section *****/
/*****************************/
/* Any Usage */
#define SECURITY_KU_ANY             "AN"
/* Base Derivation Key */
#define SECURITY_KU_BDK             "B0"
/* Card Verification Key */
#define SECURITY_KU_CVK             "C0"
/* Data Encryption */
#define SECURITY_KU_DE              "D0"
/* Application Cryptograms */
#define SECURITY_KU_EMV_AC          "E0"
/* Secure Messaging for Confidentiality */
#define SECURITY_KU_EMV_SMC         "E1"
/* Secure Messaging for Integrity */
#define SECURITY_KU_EMV_SMI         "E2"
/* Data Authentication Code */
#define SECURITY_KU_EMV_DAC         "E3"
/* Dynamic Numbers */
#define SECURITY_KU_EMV_DN          "E4"
/* Card Personalization */
#define SECURITY_KU_EMV_CP          "E5"
/* Other */
#define SECURITY_KU_EMV_O           "E6"
/* Initialization Vector */
#define SECURITY_KU_IV              "I0"
/* Key Encryption or Wrapping */
#define SECURITY_KU_KEW             "K0"
/* ISO 16609 MAC algorithm 1 (using TDEA) */
#define SECURITY_KU_M0              "M0"
/* ISO 9797-1 MAC Algorithm 1 */
#define SECURITY_KU_M1              "M1"
/* ISO 9797-1 MAC Algorithm 2 */
#define SECURITY_KU_M2              "M2"
/* ISO 9797-1 MAC Algorithm 3 */
#define SECURITY_KU_M3              "M3"
/* ISO 9797-1 MAC Algorithm 4 */
#define SECURITY_KU_M4              "M4"
/* ISO 9797-1 MAC Algorithm 5 */
#define SECURITY_KU_M5              "M5"
/* General MAC ANSI           */
#define SECURITY_KU_MA              "MA"
/* MAC HYPERCOM               */
#define SECURITY_KU_MH              "MH"
/* PIN Encryption */
#define SECURITY_KU_PE              "P0"
/* PIN verification, KPV, other algorithm */
#define SECURITY_KU_KPV             "V0"
/* PIN verification, IBM 3624 */
#define SECURITY_KU_IBMV            "V1"
/* PIN Verification, VISA PVV */
#define SECURITY_KU_PVV             "V2"

/**************************************/
/***** Special Usage Mode Section *****/
/**************************************/
/* Both Encrypt and Decrypt */
#define SECURITY_UM_BOTH            0x42
/* MAC Calculate (Generate or Verify) */
#define SECURITY_UM_MGV             0x43
/* Decrypt only */
#define SECURITY_UM_DEC             0x44
/* Encrypt only */
#define SECURITY_UM_ENC             0x45
/* MAC Generate only */
#define SECURITY_UM_MGO             0x47
/* No special restrictions or not applicable */
#define SECURITY_UM_NSR             0x4E
/* Signature only */
#define SECURITY_UM_SO              0x53
/* MAC Verify only */
#define SECURITY_UM_MVO             0x56

/*********************************/
/***** Exportability Section *****/
/*********************************/
/* Exportable under trusted key. A trusted key is either the MFK or a KEK in Key Block format */
#define SECURITY_EXP_E              0x45
/* Non Exportable */
#define SECURITY_EXP_NE             0x4E
/* Sensitive, Exportable under untrusted key. A trusted key is either the MFK or a KEK in Key Block format */
#define SECURITY_EXP_SE             0x53

/**************************/
/***** PIN/PAN Length *****/
/**************************/
#define SECURITY_PIN_LEN_MIN        4
#define SECURITY_PIN_LEN_MAX        12

#define SECURITY_PAN_LEN_MIN        12
#define SECURITY_PAN_LEN_MAX        19

#define SECURITY_PASSWD_LEN_MAX     20

/*****************************/
/***** MAC Types Section *****/
/*****************************/
#define SECURITY_MAC_ANSI               0x01
#define SECURITY_MAC_HYPERCOM           0x02
/* This is the last packet for MAC              */
#define SECURITY_MAC_P_LAST             0x00
/* This is an intermediate packet for MAC       */
#define SECURITY_MAC_P_INTER            0x01
/* This is MAC chained packet meaning: Return   */
/* the mac before the output transformation,    */
/* then return the result with transformation   */
#define SECURITY_MAC_P_CHAIN            0x02

/*************************************/
/***** PIN Block Formats Section *****/
/*************************************/
/* ISO 9564-1 Format 0 PIN Block    */
#define SECURITY_PIN_BLK_FRMT_0         0x01
/* ISO 9564-1 Format 1 PIN Block    */
#define SECURITY_PIN_BLK_FRMT_1         0x02
/* ISO 9564-1 Format 2 PIN Block    */
#define SECURITY_PIN_BLK_FRMT_2         0x03
/* ISO 9564-1 Format 3 PIN Block    */
#define SECURITY_PIN_BLK_FRMT_3         0x04
/* Italy, Banesto - 4B              */
#define SECURITY_PIN_BLK_FRMT_4B        0x05
/* China, EPS                       */
#define SECURITY_PIN_BLK_FRMT_EPS       0x06
/* Similar to ISO 9564-1 Format 0   */
#define SECURITY_PIN_BLK_FRMT_BINARY    0x07
/* Italy, AP                        */
#define SECURITY_PIN_BLK_FRMT_AP        0x08

/*************************************/
/***** Key Diversification Types *****/
/*************************************/

#define SECURITY_DIVERSIFY_ENCRYPT              1 /* DES / 3-DES encryption                                       */
#define SECURITY_DIVERSIFY_SWAP_ENCRYPT_3DES    2 /* Swap Key, then 3-DES encrypt (valid only for 3-DES keys)     */
#define SECURITY_DIVERSIFY_SPLIT_3DES_KEY       3 /* Split a 3-DES key (16-byte) in 2 different DES keys (8-byte) */
#define SECURITY_DIVERSIFY_OWF                  4 /* One-Way-Function diversification                             */
#define SECURITY_DIVERSIFY_APACS40              5 /* "Key Register" Derivation according to APACS 40 Standard     */
#define SECURITY_DIVERSIFY_RACAL_MODE_X_3DES    6 /* Key Derivation according to RACAL "Mode X" Standard          */
#define SECURITY_DIVERSIFY_SWAP_DECRYPT_3DES    7 /* Swap Key, then 3-DES decrypt (valid only for 3-DES keys)     */
#define SECURITY_DIVERSIFY_DUPLICATE_KEY        8 /* Duplicate Key with a different name                          */
#define SECURITY_DIVERSIFY_OWF_AS2805           9 /* One-Way-Function   - Australian Standard AS2805 (6.4)        */
#define SECURITY_DIVERSIFY_OWF2_AS2805         10 /* One-Way-Function 2 - Australian Standard AS2805 (6.4)        */
#define SECURITY_DIVERSIFY_OWF3_AS2805         11 /* One-Way-Function 3 - Australian Standard AS2805 (6.4)        */
#define SECURITY_DIVERSIFY_GOWF_AS2805         12 /* G One-Way-Function - Australian Standard AS2805 (6.4)        */

/*************************/
/***** Extra Section *****/
/*************************/
/* Length of the name/owner of block    */
#define SECURITY_NAME_LEN               0x08
/* Length of the PIN block              */
#define SECURITY_PIN_BLOCK_LEN          0x08
/* Length of the DUKPT serial number    */
#define SECURITY_DUKPT_SERIAL_LEN       0x0A
/* Key component character              */
#define SECURITY_COMPONENT_CHAR         0x63
/* Maximum result from the encryptor    */
#define SECURITY_MAX_RESULT_LEN         SECURITY_KEY_LEN1024
/* DUKPT Serial Number */
#define SECURITY_DUKPT_KSN_LEN          10
/* Number of DUKPT set */
#define SECURITY_DUKPT_KEYS_SET         21
/* Length of DES DUKPT */
#define SECURITY_DUKPT_KEY_LEN          (SECURITY_KEY_LEN08 + 1)
/* Length of TDES 24 DUKPT */
#define SECURITY_DUKPT_3DES_KEY_LEN     (SECURITY_KEY_LEN16 + 1)
/* Parity Definitions for key diversification */
#define SECURITY_PARITY_NONE            0
#define SECURITY_PARITY_ODD             1
#define SECURITY_PARITY_EVEN            2
/* Key Control Value Length */
#define SECURITY_KCV_LEN                3


/******************************/
/***** Block ID Structure *****/
/******************************/
typedef struct
{
    char    Name[SECURITY_NAME_LEN];
    char    Owner[SECURITY_NAME_LEN];
    uint32_t     GID;
} S_SecurityBlockId;

/********************************/
/***** Key Header Structure *****/
/********************************/
typedef struct
{
    uint8_t                Version[2];      /* Key Version                                                             */
    S_SecurityBlockId   BlkId;           /* Unique ID of the block                                                  */
    S_SecurityBlockId   ParentId;        /* Unique ID of the block which transferred this block                     */
    uint32_t                 KeyLen;          /* See Key Length Section, or application data size                        */
    uint32_t                 KeyMgmt;         /* How is the key managed.      See Key Management Section                 */
    uint32_t                 KeyAlgo;         /* Key Algorithm.               See Key Algorithm Section                  */
    uint8_t                KeyUsage[2];     /* What is the key used for.    See Key Usage Section                      */
    uint32_t                 UsageMode;       /* Key mode of use.             See Special Usage Mode Section             */
    uint32_t                 Exportability;   /* Key exportability            See Exportability Section                  */
    uint32_t                 UniqueType;      /* Unique key type, DB shall not have two keys with the same unique type   */
    uint32_t                 Reserved;        /* For future use                                                          */
} S_SecurityKeyHeader;

/*******************************/
/***** PIN Block Structure *****/
/*******************************/
typedef struct
{
    uint32_t     AppPinLen;      /* The maximum PIN len determined by the application                       */
    uint8_t   *Pan;            /* Pointer to the Primary Account Number  *** Not used for Format 1 ***    */
    uint32_t     PanLen;         /* Primary Account Number length          *** Not used for Format 1 ***    */
    uint8_t   *Fill;           /* Pointer to the Filling digits          *** Not used for Format 0 ***    */
    uint32_t     FillLen;        /* Filling digits length in bytes         *** Not used for Format 0 ***    */
} S_SecurityPinBlkISOFrmt;  /* Structure for Format 0, 1 & 3 PIN Block                                 */

typedef struct
{
    uint32_t     AppPinLen;      /* The maximum PIN len determined by the application                       */
    uint8_t   *RandomNo;       /* Pointer to random number array                                          */
    uint8_t    RandomNoLen;    /* Random number array length in bytes                                     */
}  S_SecurityPinBlk4BFrmt;  /* Banesto-4B Pin Block Format Structure                                   */

typedef union
{
    /* Putting all PIN Block structures together */
    S_SecurityPinBlkISOFrmt    PinBlkISOFrmt;
    S_SecurityPinBlk4BFrmt     PinBlk4BFrmt;
    /* Future formats... */
} U_SecurityPinBlkFrmt;

typedef struct
{
    u8                      FormatType; /* See PIN Block Formats Section */
    U_SecurityPinBlkFrmt    PinBlk;
} S_SecurityPinBlkFrmt;

/* MAC parameters structure */
typedef struct
{
    uint8_t               *pPrevMac;   /* Pointer to the previous MAC. Optional        */
    uint8_t               *pData;      /* Pointer to the data to be MAC'ed             */
    uint32_t                 DataLen;    /* Length of the data to be MAC'ed              */
    uint8_t               *pWK;        /* Optional working key for MS key management   */
    uint32_t                 WKLen;      /* Length of the working key                    */
    S_SecurityBlockId  *pSecondKey; /* Optional process using second key            */
    uint8_t                MacType[2]; /* Taken from Key Usage Section, MAC algorithms */
    uint32_t                 PacketType; /* See MAC Types Section                        */
} S_SecurityMac;

/**********************************************/
/***** PIN Block Key Management Structure *****/
/**********************************************/
typedef struct
{
    S_SecurityBlockId  BlockId;         /* Key block identifier                                                    */
} S_SecurityPinBlkKeyFD;                /* Structure for building PIN block using Fixed/DUKPT key management       */

typedef struct
{
    S_SecurityBlockId   BlockId;         /* Key block identifier of the Master key                                  */
    uint8_t               *WorkingKey;      /* Working key                                                             */
    uint32_t                 WorkingKeyLen;
} S_SecurityPinBlkKeyMS;                 /* Structure for building PIN block using Master/Session key management    */

/* Add other PIN Block Key Management structures here */

typedef union
{
    /* Putting all PIN Block Key Management structures together */
    S_SecurityPinBlkKeyFD PinBlkKeyFD;
    S_SecurityPinBlkKeyMS PinBlkKeyMS;

    /* Add other PIN Block Key Management attributes here */

} U_SecurityPinBlkKey;

typedef struct
{
    u8                      KeyMgmt;     /* See Key Management Section                                              */
    U_SecurityPinBlkKey     KeyBlk;
} S_SecurityPinBlkKey;

/**************************************/
/***** PIN Block Result Structure *****/
/**************************************/
typedef struct
{
    uint8_t   *EncryptedBlk;       /* Pointer to the encrypted PIN Block                               */
    uint32_t     EncryptedBlkLen;    /* Length of the encrypted PIN Block in bytes                       */
    uint8_t   *KSN;                /* Pointer to the Key Serial Number (In case of DUKPT)              */
} S_SecurityPinBlkRes;

/*********************************/
/***** Data Result Structure *****/
/*********************************/
typedef struct
{
    uint8_t   *Data;               /* Pointer to the encrypted/decrypted data                          */
    uint32_t     DataLen;            /* Output data length                                               */
    uint8_t   *KSN;                /* Pointer to the Key Serial Number (In case of DUKPT)              */
} S_SecurityDataRes;

/********************************/
/***** MAC Result Structure *****/
/********************************/
typedef struct
{
    uint8_t    MacBlock[SECURITY_KEY_LEN08];   /* Holds the MAC result                                 */
    uint8_t   *pMacChained;                    /* Pointer to chained MAC (Optional)                    */
} S_SecurityMacRes;

typedef union
{
    S_SecurityPinBlkRes     PinBlkRes;
    S_SecurityDataRes       DataRes;
    S_SecurityMacRes        MacRes;
    /************************************/
    /* Add other Result structures here */
    /************************************/
} U_SecurityRes;

typedef struct
{
    uint32_t                     KeyMgmt;    /* Output: See Key Management Section   */
    U_SecurityRes           ResBlk;
} S_SecurityRes;

typedef struct
{
    uint8_t                   *Data;
    uint32_t                     DataLen;    /* Must be multiple of 8 bytes                      */
    uint32_t                     KeyMgmt;    /* See Key Management Section                       */
    uint32_t                     IsEncrypt;  /* TRUE - Encrypt, FALSE - Decrypt                  */
    S_SecurityBlockId       CryptKey;   /* The key ID used for the cryptographic operation  */
    uint8_t                   *WK;         /* Working Key used for MS keys only                */
    uint32_t                     WKLen;      /* Working Key Lengrh                               */
} S_SecurityDataInput;

/********************************/
/***** MAC Result Structure *****/
/********************************/
typedef struct
{
    uint8_t    EncryptedBlock[SECURITY_KEY_LEN08];
    uint8_t    MacChained[SECURITY_KEY_LEN08];
} S_SecurityMacResult;


/* OWF DES/3DES Encryption Input Format Structure. */
typedef struct
{
    uint8_t   *Data;           /* 8-byte ONLY          */
    uint8_t   *X;              /* 8-byte ONLY          */
    uint8_t   *Y;              /* 8 or 16 byte exactly */
    uint32_t     AdjustParity;   /* See Extra Section    */
} S_SecurityOWF;

/*************************************/
/***** Key Calculation Algorithm *****/
/*************************************/
#define SECURITY_KEY_CALC_NONE                  1
#define SECURITY_KEY_CALC_XOR                   2
#define SECURITY_KEY_CALC_DES                   3
#define SECURITY_KEY_CALC_3DES                  4
#define SECURITY_KEY_CALC_UNDES                 5
#define SECURITY_KEY_CALC_3UNDES                6
#define SECURITY_KEY_CALC_DES_CBC               7
#define SECURITY_KEY_CALC_3DES_CBC              8
#define SECURITY_KEY_CALC_UNDES_CBC             9
#define SECURITY_KEY_CALC_3UNDES_CBC            10
#define SECURITY_KEY_CALC_ALTERNATE             11 /* Alternate the key data with another key data    */
#define SECURITY_KEY_CALC_OWF_AS2805            12 /* One-Way-Function - Australian Standard 2805 6.4 */

/********************************/
/***** Standard Proprietary *****/
/********************************/
#define SECURITY_STD_NONE                       1
#define SECURITY_STD_SERMEPA                    2
#define SECURITY_STD_BANESTO                    3
#define SECURITY_STD_SPAIN_4B                   4
#define SECURITY_STD_EPS                        5
#define SECURITY_STD_AS2805                     6
#define SECURITY_STD_GISKE                      7
#define SECURITY_STD_PRC                        8
#define SECURITY_STD_VSS                        9
#define SECURITY_STD_TR31                       10

/**********************************/
/***** Special Key Operations *****/
/**********************************/
#define SECURITY_OP_NOP                      0   /* No specific operation to perform. */
#define SECURITY_OP_KCV_CHECK                1   /* Check the Key Control Value, before write it in DB. */
#define SECURITY_OP_ZERO_CHECK               2   /* Check if key data is all '0'. */
#define SECURITY_OP_TK_CALCULATION           3   /* Perform TK calculation before decrypt the key to be loaded. */
#define SECURITY_OP_KEY_CALCULATION          4   /* Perform key calculation (after been decrypted) before write it in DB. */
#define SECURITY_OP_CALC_GLOBAL_KCV          5   /* Calculate the Global Key(s) Control Value. */
#define SECURITY_OP_REPLACE_KEY              6   /* Replace the existing data key in DB with new data. */
#define SECURITY_OP_DOUBLE_DECRYPT           7   /* Decrypt key: 1st time with TK 1 and 2nd time with TK 2. */
#define SECURITY_OP_CALC_WK_KCV              8   /* Calculates the Control Value of an encrypted Working Key (Decrypt the Working Key by the Master Key, then, calculates KCV).*/
#define SECURITY_OP_CALC_KCV_WITH_DATA       9   /* Calculates the Control Value with a specific data. */
#define SECURITY_OP_PARITY_CHECK             10  /* Check  parity bits, according to the Standard. */
#define SECURITY_OP_PARITY_ADJUST            11  /* Adjust parity bits, according to the Standard. */
#define SECURITY_OP_CHECK_KEY_VERSION        12    /* Check the version of the Key in DB. Allow to replace only with version greater than the one in DB. */
#define SECURITY_OP_MAC_VERIFY               13  /* Calculate MAC across the Key and compare it with the expected MAC. */

/*******************************************/
/***** Standard Proprietary Parameters *****/
/*******************************************/
typedef struct
{
    /* Standard == SECURITY_STD_SERMEPA */
    uint8_t           *pKeyControlValueIn; /* Expected Key Control Value */
} S_SecuritySermepaOp;

/* BANESTO specific operations parameters structure. */
typedef struct
{
    /* Standard == SECURITY_STD_BANESTO */
    uint8_t                *pKeyControlValueIn; /* Expected Key Control Value */
    S_SecurityBlockId   *pTKName1;           /* Transport Key component */
    uint8_t                *pTKCalcAlgo;        /* Pointer to array of Operations to perform for TK calculation */
    uint8_t                *pTKCalcData;        /* Transport Key calculation data */
    S_SecurityBlockId   *pKeyName1;          /* Key component */
    uint8_t                *pKeyCalcAlgo;       /* Pointer to array of Operations to perform for Key calculation */
    uint8_t                *pKeyCalcData;       /* Key calculation data */
} S_SecurityBanestoOp;

/* 4B specific parameters structure */
typedef struct
{
    /* Standard == SECURITY_STD_SPAIN_4B */
    uint8_t                *pKeyControlValueIn; /* Expected Key Control Value */
    S_SecurityBlockId   *pTKName1;           /* First Transport Key component */
    uint8_t                *pTKCalcAlgo1;       /* Pointer to array of Operations to perform for 1st TK calculation */
    uint8_t                *pTKCalcData1;       /* First Transport Key calculation data */
    S_SecurityBlockId   *pTKName2;           /* Second Transport Key component */
    S_SecurityBlockId   *pKeyName1;          /* Key component */
    uint8_t                *pKeyCalcAlgo;       /* Pointer to array of Operations to perform for Key calculation */
    uint8_t                *pKeyCalcData;       /* Key calculation data */
} S_SecuritySpain4BOp;

/* Australian Standard 2805 6.4 specific operations parameters structure. */
typedef struct
{
    /* Standard == SECURITY_STD_AS2805 */
    uint8_t                *pKeyControlValueIn; /* Expected Key Control Value */
    S_SecurityBlockId   *pTKName1;           /* Auxiliary Transport Key used */
    uint8_t                *pTKCalcAlgo;        /* Pointer to array of Operations to perform for TK calculation */
    uint8_t                *pTKCalcData;        /* Transport Key calculation data */
    S_SecurityBlockId   *pKeyName1;          /* Auxiliary Key used for Key calculation */
    uint8_t                *pKeyCalcAlgo;       /* Pointer to array of Operations to perform for Key calculation */
    uint8_t                *pKeyCalcData;       /* Key calculation data */
    S_SecurityBlockId   *pKeyAlternateName;  /* Alternative key to be used */
    uint8_t                *pInitialVector;     /* Initial Vector (ALWAYS 8-bytes length), for DES CBC calculation */
    uint8_t              UseTKForCalc;       /* Transport Key is not used to decipher key, but to calculate key */
} S_SecurityAS2805Op;

/* GISKE specific operations parameters structure. */
typedef struct
{
    /* Standard ==  SECURITY_STD_PRC     */
    uint8_t           *pKeyControlValueIn; /* Expected Key Control Value       */
    uint8_t           *pMACData;           /* MAC will be calculated across this data. */
    uint32_t             DataLen;
    uint8_t           *pMACVerification;   /* Expected MAC Result              */
} S_SecurityPRCOp;


/* Union of all Standards specific operations parameters. */
typedef union
{
    S_SecuritySermepaOp   SermepaOp;
    S_SecurityBanestoOp   BanestoOp;
    S_SecuritySpain4BOp   Spain4BOp;
    S_SecurityAS2805Op    AS2805Op;
    S_SecurityPRCOp       PRCOp;
} U_SecurityStdOp;

typedef struct
{
    uint32_t                     Std;
    U_SecurityStdOp         StdOp;
    uint8_t                   *KeyOp;
} S_SecurityStdOp;



#define SEC_CSPURP_BLOCK    0x01
#define SEC_CSPURP_MSG      0x02
#define SEC_CSPURP_INT      0x03
#define SEC_CSPURP_RET      0x04

#define CLEAN_EXIT(RESULT)      {                           \
                                    Result = (RESULT);      \
                                    goto CLEAN_EXIT_LABEL;  \
                                }

#define CLEAN_WHEN_NOT(RESULT)  {                               \
                                    if ( Result != (RESULT) )   \
                                        goto CLEAN_EXIT_LABEL;  \
                                }

#define VER_TO_U32(A,B,C,D)     (A)*16777216+(B)*65536+(C)*256+(D)

#define SECURITY_MAX_KEY_LEN    (3 * SECURITY_KEY_LEN1024)
/* This structure defines the expected key attributes when the key is no type */
typedef struct
{
    uint32_t     KeyMgmt;        /* How is the key managed.      See Key Management Section     */
    uint8_t    KeyUsage[2];    /* What is the key used for.    See Key Usage Section          */
    uint32_t     UsageMode;      /* Key mode of use.             See Special Usage Mode Section */
} S_SecurityExpKeyAttr;

typedef struct
{
    uint32_t     Signature;              /* Signature that header is valid                   */
    void   *pBlkOM2SMQ;             /* Pointer to the Blocking API of OM --> SM         */
    void   *pBlkSM2OMQ;             /* Pointer to the Blocking API of SM --> OM         */
    void   *pOM2SMQ;                /* Pointer to the Queue of OM --> SM                */
    void   *pSM2OMQ;                /* Pointer to the Queue of SM --> OM                */
    uint32_t     Purpose;                /* CS occured because of Blocking API/Msg/Interrupt */
    uint32_t     TotalSize;
} S_SecurityCS;

/* Structure for SHA-1 parameters */
typedef struct
{
    uint8_t    data[64]; 
    uint32_t     datalen; 
    uint32_t     bitlen[2]; 
    uint32_t     state[5]; 
    uint32_t     k[4]; 
} S_SecurityShaContext;


#endif /* _SECURITY_DEF_ */
