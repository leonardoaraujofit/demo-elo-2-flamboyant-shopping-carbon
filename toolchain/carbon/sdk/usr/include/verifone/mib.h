/****************************************************************************
 *                                                                          *
 * FILE NAME:    mib.h                                                      *
 *                                                                          *
 * COMPANY:      Verifone                                                   *
 *                                                                          *
 * DESCRIPTION:  Header file for Manufacture Information Block              *
 *                                                                          *
 ****************************************************************************/

#ifndef _MIB_H_
#define _MIB_H_


#define MIB_MAC_ADDR_SIZE     12
#define MIB_MAX_SERIAL_SIZE   16
#define MIB_MAX_PTID_SIZE     8

typedef struct {
    unsigned long hdr_signature;             // Little-endian!
    unsigned long hdr_fileSize;              // Little-endian!
    char hdr_dateTime[14];                   // DD/MM/YYYY hh:mm:ss  be careful!
    char hdr_version[4];
    char hdr_spare[38];
    char mfg_block[30];                      // Reserved for Manufacturing
    char model_num[12];
    char country[12];
    char part_number[32];                    // M-level number with hyphens removed
    char hdwr_version[4];                    // HW Rev match to the BOM (M=level)
    char lot_number[8];
    char serial_number[MIB_MAX_SERIAL_SIZE]; // Unique for every terminal
    char PTID[MIB_MAX_PTID_SIZE];            // If not needed, put 14000000
    char ETH_MAC_addr[MIB_MAC_ADDR_SIZE];    // VFI range 00-0B-4F-xx-yy-zz
    char WiFi_MAC_addr[MIB_MAC_ADDR_SIZE];   // VFI range 00-0B-4F-xx-yy-zz
    char security_option[1];                 // Pointer to 1 char
    char BT_MAC_addr[MIB_MAC_ADDR_SIZE];     // Bluetooth address if needed, VFI range 00-0B-4F-xx-yy-zz
    char detamper_option;                    // Detamper option (0=Default, 1=KLD)
    unsigned char expected_keys[32];         // List of expected security keys (packed)
    char undefined[764];
    unsigned char crc[4];

} Manufacturing_Information_Block_t;    // In NAND block : written by OS


#endif /* _MIB_H_ */
