/**
 * @file msrDevice.h
 * @brief Header file containing interface to MSR device
 *
 * Contains functions and data to interface with the MSR device.
 */

#ifndef _MSR_DEVICE_H
#define _MSR_DEVICE_H
/*
 *    COPYRIGHT (C) 2007 by VeriFone, Inc.  All rights reserved.
 *
 *    Filename:                msrDevice.h
 *    Module Description:      Spectrum Mag Stripe Reader Library
 *    Component of:            Spectrum
 *    Authors(s):              Winston_L1
 *    Function:
 *
 */
 
#ifdef __cplusplus
extern "C" {
#endif
#include <errno.h>
#include "SemtekHTdes.h"
#include "SemtekHTDesStatus.h"

//#include "htdes.h"
//


    /* msrIoctl types */
#define MSR_MAG_CARD_PRESENT           1
#define MSR_GET_RAW_DATA               2
#define MSR_GET_STRUCT_DATA				3
#define MSR_GET_GLOB_STRUCT_DATA		4
#define MSR_CADL_ENABLE                10
#define MSR_CADL_DISABLE               11
#define MSR_REGISTER_CALLBACK          12
#define MSR_SET_MIN_BYTES              13
#define MSR_SET_MAX_NODATA             14
#define MSR_REPORT_EXTD_ERRORS         15
#define MSR_UIC_CMD                    30
#define MSR_UIC_STATUS                 31
#define MSR_SET_HTDES_STATE            40
#define MSR_GET_HTDES_STATE            41
#define MSR_GET_HTDES_CONTROLLER_STATE 42
#define MSR_GET_HTDES_STATUS           43
#define MSR_SET_HTDES_RESET            44
#define MSR_GET_HTDES_CLEAR_PAN			45
#define MSR_SET_DECODE_FLAG				46
#define MSR_GET_DECODE_DIRECTION        50
#define MSR_GET_LOOPPAY					55
#define MSR_SET_HYBRID_MODE				60
#define MSR_GET_SWITCH_STATE			65
#define SEMTEKD_NOT_OPERATIONAL         99 // not actually returned by semtekd

// These bit flags are specific to the Ux300 (which has some special needs due to being a hybrid dip reader).
// Use these with msrSetHybridMode() [see below]
#define MSR_IGNORE_EVENTS		0x01
#define MSR_WAIT4SWITCH			0x02

    typedef enum {
		ST_DISABLE = 0,
		ST_ENABLE  = 1
	} ENUM_STATE;

    /* error returns */
#define MSR_ERR_DEVICE_BUSY  -EBUSY   // msr device busy (open failed)
#define MSR_ERR_NO_MEMORY    -ENOMEM  // no memory available (internal malloc() failed)
#define MSR_ERR_BADF         -EBADF   // bad file descriptor (did you open msr? successfully?)
#define MSR_ERR_EMF          -EINTR     // EMF detected
#define MSR_ERR_ENCRYPT      -ENOMEDIUM // encryption failed (no keys loaded or bad,...)
#define MSR_ERR_ENOBUFS      -ENOBUFS   // enable to open queues' to encryption daemon
#define MSR_ERR_EINVAL       -EINVAL  // arguments to API are invalid
#define MSR_ERR_ENOSYS       -ENOSYS  // Function not supported


/**
 * MSR TRACK DATA
 */
    typedef struct {
        unsigned char    ucStatus;    // status of track
        unsigned char    ucCount;    // size in bytes of track data
        char            *cpData;    // pointer to track data
    } MSR_TRACK_DATA;

    typedef struct {
        MSR_TRACK_DATA    stTrack1;
        MSR_TRACK_DATA    stTrack2;
        MSR_TRACK_DATA    stTrack3;
    } MSR_DATA;

	/** @brief Structure used to hold MSR data that is returned in msrGlobStructured.
	 *
	 * It holds track data as well as the type of card that was read.
	 * msrCardType indicates which card was read. This can be MSR_ISO or MSR_JIS_II.
	 */
    typedef struct {
        int               msrCardType;
        MSR_DATA    msrData;
    } MSR_GLOB_DATA;

    typedef enum {
        TE_NO_ERRORS,
        TE_NO_DATA_ON_TRACK,
        TE_NO_START_SENTINEL,
        TE_NO_END_SENTINEL,
        TE_CHECKSUM_ERROR,
        TE_PARITY_ERROR,
        TE_SPECIAL_ERROR,

        NUM_TRACK_ERRORS

    } ENUM_TRACK_ERRORS;


    typedef enum {
        TRK_ST_OK,
        TRK_ST_ERR_PAN_MISMATCH, // pan mismatch on pin/debit attempt
        TRK_ST_ERR_BAD_CMDCARD,  // bad command card (e.g.crc fail after decrypt)
        TRK_ST_ERR_CMDFN,        // unrecognised  command card function
        TRK_ST_ERR_GENERAL,
        TRK_ST_ERR_PARSE,        // unrecognised  command card function
        TRK_ST_ERR_KEY_FMT,      // dec value > 16 ^ 16
        TRK_ST_ERR_TRK_BINREC,
        TRK_ST_ERR_TRK_BAD_PAN,
        TRK_ST_ERR_UNKNOWN
    } MSR_UL_STATUS;

    typedef enum {
        TRK_CRYPTO_NOT_ENCRYPTED,
        TRK_CRYPTO_ERR,
        TRK_CRYPTO_ALG1,
        TRK_CRYPTO_ALG2,
        TRK_CRYPTO_UNKNOWN
    } MSR_UL_ENCRYPTION_STATUS;

    typedef struct {
        MSG_HTDES_STATUS htdesControllerStatus;
        MSR_UL_STATUS lastMsrData_ulStatus;
        MSR_UL_ENCRYPTION_STATUS lastMsrData_ulEncryptionStatus;
    }MSR_HTDES_STATUS;

/** @cond
 *
 */
#ifndef _MSR_LIB_
    extern int _msrOpen( int iFlags, void *fnCallback );
    extern int _msrGlobOpen( int iFlags, void *pfnCallback, int iCards );
    extern int _msrClose( void );
    extern int _msrRead( char *pachDest, int iCount );
    extern int _msrWrite( char *pachSource, int iCount );
    extern int _msrIoctl( int iType, void *pData );
    extern int _msrVersion( char *pchVersion );
#endif
/** @endcond
 *
 */

// Valid card types for decoder selection
#define MSR_ISO 1
#define MSR_JIS_II 2
/* The following are unsupported
 * these values should be used when supported
 * and MSR_UNUSED moved to the next bit
 * 
 *#define MSR_AAMVA 4
 *#define MSR_CADL 8
 */
#define MSR_UNUSED 4
	
#define msrOpen(iFlags, fnCallback)     _msrOpen(iFlags, fnCallback)
#define msrClose()                      _msrClose()
#define msrRead(pachDest, iCount)       _msrRead(pachDest, iCount)
#define msrWrite(pachSource, iCount)    _msrWrite( pachSource, iCount)
#define msrVersion(pchVersion)          _msrVersion(pchVersion)
#define msrRaw(pData)                   _msrIoctl(MSR_GET_RAW_DATA, (void *)pData)
#define msrStructured(pData)            _msrIoctl(MSR_GET_STRUCT_DATA, (void *)pData)
#define msrMagneticCardPresent()        _msrIoctl(MSR_MAG_CARD_PRESENT, NULL)
#define msrEnableHTdes()                _msrIoctl(MSR_SET_HTDES_STATE, (void *)ST_ENABLE)
#define msrDisableHTdes()               _msrIoctl(MSR_SET_HTDES_STATE, (void *)ST_DISABLE)
#define msrDecodeDirection()            _msrIoctl(MSR_GET_DECODE_DIRECTION, NULL)
#define msrLoopPay()					_msrIoctl(MSR_GET_LOOPPAY, NULL)
#define msrSetHybridMode(mode)			_msrIoctl(MSR_SET_HYBRID_MODE, (void *)mode)
#define msrHybridSwitchState()     		_msrIoctl(MSR_GET_SWITCH_STATE, NULL)

/** @brief Opens the MSR device, while specifying which cards to decode.
 *
 * msrGlobOpen is a macro which maps to a function which does the same as msrOpen
 * but it accepts a bitmask (iCards) which tells the decoding code
 * which decoding types to implement.
 * @param iFlags Type int. This is passed into the open call to the msr device.
 * @param fnCallback Type void*. This is a pointer to a function, implemented by the user, that the lib will automatically call when data is detected.
 * @param iCards Type int. A bitmask which indicates which decoding types to implement. Possible types are MSR_ISO
 * and MSR_JIS_II. These can be ORd together to in order to decode all types. If 0 is used, all card types will be decoded.
 *
 * @return 0 on success. Returns negative Error code on failure (see #define list). Returns MSR_ERR_EINVAL if iCards is invalid.
 * @see msrGlobStructured - used to read the data and card type.
 * @warning Not supported on Trident devices, MSR_ERR_ENOSYS will be returned.
 */
#define msrGlobOpen(iFlags, fnCallback, iCards)		_msrGlobOpen(iFlags, fnCallback, iCards)

/** @brief Populates a MSR_GLOB_DATA structure with data read from the MSR
 *
 * msrGlobStructured is a macro which maps to a function which does the
 * same as msrStructured except a pointer to MSR_GLOB_DATA structure is passed as pData.
 * This has a msrCardType member which will be populated if the following are satisfied
 * @li Success (0) was returned.
 * @li At least one of the tracks, returned in the structure contain valid data, i.e. ucStatus for that track = 0.
 *
 * If either of the above aren't true, msrCardType should be ignored.
 *
 * @param pData - This should be of type MSR_GLOB_DATA
 * @return Returns 0 on success.
 * On error, a negative error code is returned. (See #define list)
 * @see msrGlobOpen - this should be called before msrGlobStructured
 * @warning This function is not supported on Trident devices, MSR_ERR_ENOSYS will be returned in this case
 */
#define msrGlobStructured(pData)			_msrIoctl(MSR_GET_GLOB_STRUCT_DATA, (void *)pData)


/*Not Implemented*/
#define msrEnableLicenseDecode()        _msrIoctl(MSR_CADL_ENABLE, NULL)
#define msrDisableLicenseDecode()       _msrIoctl(MSR_CADL_DISABLE, NULL)
#define msrSetMinBytes(iCount)          _msrIoctl(MSR_SET_MIN_BYTES, (void *)iCount)
#define msrSetMaxNoDataCount(iCount)    _msrIoctl(MSR_SET_MAX_NODATA, (void *)iCount)
#define msrReportExtendedErrors()       _msrIoctl(MSR_REPORT_EXTD_ERRORS, NULL)
#define msrHTdesStatus(pData)           _msrIoctl(MSR_GET_HTDES_STATUS, (void *)pData)
#define msrHTdesReset()                 _msrIoctl(MSR_SET_HTDES_RESET)
#define msrHTdesGetClearPan(pData)		_msrIoctl(MSR_GET_HTDES_CLEAR_PAN, (void *)pData)
#define msrSetDecodeFlag(pData)			_msrIoctl( MSR_SET_DECODE_FLAG, (void*)pData )
/*End of Not Implemented*/


int msrReadStructured(MSR_DATA* pData);


extern int msrGetTrackData(MSR_DATA* track_data);
extern int msrGetFormattedData(char *pachDest, int iCount, MSR_DATA* encrypted_data);
extern void SetProxyEnabled(int value);

extern MSR_DATA	mastTrackData;


// For the correct integer value see definitions above
#define MSR_SET_VATS_DATA				47

int _msrWriteVATS( const char *pachSource, int iCount );
int _msrWriteVATSioctl( char *pachSource, int iCount );
int _msrGetVersionVATS(void);


#ifdef __cplusplus
}
#endif
#endif /* _MSR_DEVICE_H */
