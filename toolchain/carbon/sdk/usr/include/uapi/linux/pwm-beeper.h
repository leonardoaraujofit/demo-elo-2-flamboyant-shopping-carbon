#ifndef _INPUT_PWM_BEEPER_H
#define _INPUT_PWM_BEEPER_H
#include <linux/buzzer_param.h>

struct input_dev* get_pwm_beeper_input_dev(void* addr);
#endif
