/*
 * hsp-defines.h
 *
 * HyperSEAM: Exported definitions for public interface.
 *
 * Copyright(C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed 'as is' WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef HSP_DEFINES_H
#define HSP_DEFINES_H


/*
 * Test service for each module/submodule starts at this offset from the
 * respective base
 */
#define HSP_TEST_OFF            0x80

/*
 * Secure module - Core services
 */
#define HSP_MOD_CORE_NAME        "CORE"
#define HSP_MOD_CORE_ID          0x0100
#define HSP_MOD_CORE_BASE        (HSP_MOD_CORE_ID + 1)
#define HSP_MOD_CORE_LAST        0x01FF

/*
 * Secure module - Loader
 */
#define HSP_MOD_LOAD_NAME        "LOAD"
#define HSP_MOD_LOAD_ID          0x0900
#define HSP_MOD_LOAD_BASE        (HSP_MOD_LOAD_ID + 1)
#define HSP_MOD_LOAD_LAST        0x09FF

/*
** Secure module - ROM Code Services (Public)
*/
#define HSP_MOD_ROMPUB_ID         0x0A00
#define HSP_MOD_ROMPUB_NAME       "ROMPUB"
#define HSP_MOD_ROMPUB_BASE       (HSP_MOD_ROMPUB_ID + 1)
#define HSP_MOD_ROMPUB_LAST       0x0AFF

/*
 * Secure module - ROM Code Services (Secure)
 */
#define HSP_MOD_ROMSEC_ID         0x0B00
#define HSP_MOD_ROMSEC_NAME       "ROMSEC"
#define HSP_MOD_ROMSEC_BASE       (HSP_MOD_ROMSEC_ID + 1)
#define HSP_MOD_ROMSEC_LAST       0x0BFF

/*
 * Secure module - Public-to-secure Communication
 */
#define HSP_MOD_PUB2SEC_ID        0x0C00
#define HSP_MOD_PUB2SEC_NAME      "PUB2SEC"
#define HSP_MOD_PUB2SEC_BASE      (HSP_MOD_PUB2SEC_ID + 1)
#define HSP_MOD_PUB2SEC_LAST      0x0CFF

/*
 * Secure module - User-to-Privilege mode communication
 */
#define HSP_MOD_PRIV2USR_ID        0x0D00
#define HSP_MOD_PRIV2USR_NAME      "PRIV2USR"
#define HSP_MOD_PRIV2USR_BASE      (HSP_MOD_PRIV2USR_ID + 1)
#define HSP_MOD_PRIV2USR_LAST      0x0DFF

/*
 * Secure module - Log errors and messages
 */
#define HSP_MOD_LOG_ID             0x0E00
#define HSP_MOD_LOG_NAME           "LOG"
#define HSP_MOD_LOG_BASE           (HSP_MOD_LOG_ID + 1)
#define HSP_MOD_LOG_LAST           0x0EFF

/*
 * Secure module - Crypto services
 */
#define HSP_MOD_CRYPTO_ID          0x1000
#define HSP_MOD_CRYPTO_NAME        "CRYPTO"
#define HSP_MOD_CRYPTO_BASE        (HSP_MOD_CRYPTO_ID + 1)
#define HSP_MOD_CRYPTO_LAST        0x1FFF

/*
 * Secure module - Drivers
 */
#define HSP_MOD_DRIVER_ID          0x2000
#define HSP_MOD_DRIVER_NAME        "DRIVER"
#define HSP_MOD_DRIVER_BASE        (HSP_MOD_DRIVER_ID + 1)
#define HSP_MOD_DRIVER_LAST        0x2FFF

/*
 * Secure module - User mode services
 */
#define HSP_MOD_USERSVC_ID          0x8000
#define HSP_MOD_USERSVC_NAME        "USERSVC"
#define HSP_MOD_USERSVC_BASE        (HSP_MOD_USERSVC_ID + 1)
#define HSP_MOD_USERSVC_LAST        0x8FFF

/*
 * Secure module - Configuration services
 */
#define HSP_MOD_CONFIG_ID           0x9000
#define HSP_MOD_CONFIG_NAME         "CONFIG"
#define HSP_MOD_CONFIG_BASE         (HSP_MOD_CONFIG_ID + 1)
#define HSP_MOD_CONFIG_LAST         0x9FFF

/*
 * Secure interrupt (module)
 *
 * Though not a module in strict sense, assigning a module ID allows consistent
 * error reporting from secure world.
 */
#define HSP_MOD_INT_ID              0xF000
#define HSP_MOD_INT_NAME            "INT"

/*
 * Qualifier indicating the severity of the log messages.
 *
 * This qualifier is (optionally) added to the Most Significant Byte of the
 * error code.
 */
#define HSP_LOG_NORMAL       0x00000000
#define HSP_LOG_DEBUG        0x01000000
#define HSP_LOG_WARN         0x02000000
#define HSP_LOG_ERROR        0x04000000

/*
 * Services exported by LOG module
 */
#define HSP_LOG_PUB_ADDR   (HSP_MOD_LOG_BASE)
#define HSP_LOG_TEST_BASE  (HSP_MOD_LOG_BASE + HSP_TEST_OFF)
#define HSP_LOG_TEST       (HSP_LOG_TEST_BASE)
#define HSP_LOG_TEST0        0
#define HSP_LOG_TEST1        1
#define HSP_LOG_TEST2        2
#define HSP_LOG_TEST3        3
#define HSP_LOG_TEST4        4
#define HSP_LOG_TEST5        5

/*
 * Services exported by LOAD module
 */
#define HSP_LOAD_LOAD           (HSP_MOD_LOAD_BASE)
#define HSP_LOAD_UNLOAD         (HSP_MOD_LOAD_BASE + 1)
#define HSP_LOAD_GETMAX         (HSP_MOD_LOAD_BASE + 2)
#define HSP_LOAD_GET_DROPBOX    (HSP_MOD_LOAD_BASE + 3)
#define HSP_LOAD_GET_EXECSIZE   (HSP_MOD_LOAD_BASE + 4)
#define HSP_LOAD_AUTH			(HSP_MOD_LOAD_BASE + 5)
#define HSP_LOAD_TEST_BASE      (HSP_MOD_LOAD_BASE + HSP_TEST_OFF)
#define HSP_LOAD_TEST_LOAD      (HSP_LOAD_TEST_BASE)
#define HSP_LOAD_TEST_EXEC      (HSP_LOAD_TEST_BASE + 1)

/*
 * Get HyperSEAM version
 */
#define HSP_CORE_GETVERSION         (HSP_MOD_CORE_BASE)

/*
 * Services exported by CONFIG module
 * These services are further divided amongst sub-modules.
 */
#define HSP_CONFIG_INIT            (HSP_MOD_CONFIG_BASE)
#define HSP_CONFIG_INIT2           (HSP_MOD_CONFIG_BASE + 1)
#define HSP_CONFIG_TEST_BASE       (HSP_MOD_CONFIG_BASE + HSP_TEST_OFF)
#define HSP_CONFIG_TEST            (HSP_CONFIG_TEST_BASE)
#define HSP_CONFIG_SGI_TEST        (HSP_CONFIG_TEST_BASE + 1)
#define HSP_CONFIG_SVC_MEM_BASE    (HSP_MOD_CONFIG_BASE + 0x100)
#define HSP_CONFIG_SVC_MEM_LAST    (HSP_MOD_CONFIG_BASE + 0x1FF)

/*
 * Sub-module - CONFIG/MEM
 */
#define HSP_CONFIG_MEM_INIT        (HSP_CONFIG_SVC_MEM_BASE)
#define HSP_CONFIG_MEM_PUB_RO      (HSP_CONFIG_SVC_MEM_BASE + 1)
#define HSP_CONFIG_MEM_PUB_RW      (HSP_CONFIG_SVC_MEM_BASE + 2)
#define HSS_GET_HYPERSEAM_CFG      (HSP_CONFIG_SVC_MEM_BASE + 3)
#define HSP_CONFIG_MEM_TEST        (HSP_CONFIG_SVC_MEM_BASE + HSP_TEST_OFF)
#define HSP_CONFIG_TEST_PAYLOAD     0

/*
 * Services exported by USERSVC module
 */
#define HSP_USERSVC_INIT        (HSP_MOD_USERSVC_BASE)
#define HSP_USERSVC_BIND        (HSP_MOD_USERSVC_BASE + 1)
#define HSP_USERSVC_CLEAR       (HSP_MOD_USERSVC_BASE + 2)

/*
 * Test service for USERSVC module
 */
#define HSP_USERSVC_TEST        (HSP_MOD_USERSVC_BASE + 0x10)

/*
 * Available IDs for the user services.
 */
#define HSP_USERSVC_FIRST       (HSP_MOD_USERSVC_BASE + 0x20)
#define HSP_USERSVC_LAST        (HSP_MOD_USERSVC_LAST)

/**
 * \brief   OTFA service base ID
 */
#define HSP_DRIVER_SVC_OTFA_BASE   (HSP_MOD_DRIVER_BASE + 0x100)
#define HSP_DRIVER_SVC_OTFA_LAST   (HSP_MOD_DRIVER_BASE + 0x1FF)

/**
 * \brief   TAMPER service base ID
 */
#define HSP_DRIVER_SVC_TAMPER_BASE   (HSP_MOD_DRIVER_BASE + 0x200)
#define HSP_DRIVER_SVC_TAMPER_LAST   (HSP_MOD_DRIVER_BASE + 0x2FF)

/**
 * \brief   EFUSE service base ID
 */
#define HSP_DRIVER_SVC_EFUSE_BASE   (HSP_MOD_DRIVER_BASE + 0x300)
#define HSP_DRIVER_SVC_EFUSE_LAST   (HSP_MOD_DRIVER_BASE + 0x3FF)

/**
 * \brief    Sub-module - DRIVER/OTFA
 */
#define HSP_DRIVER_OTFA_DISABLE     (HSP_DRIVER_SVC_OTFA_BASE + 0x1)
#define HSP_DRIVER_OTFA_DBG_LOG     (HSP_DRIVER_SVC_OTFA_BASE + 0x2)
#define HSP_DRIVER_OTFA_WRT_PROT    (HSP_DRIVER_SVC_OTFA_BASE + 0x3)

/* OTFA Test Services */
#define HSP_DRIVER_OTFA_TEST_BASE       (HSP_DRIVER_SVC_OTFA_BASE + HSP_TEST_OFF)
#define HSP_DRIVER_OTFA_ENC_TEST        (HSP_DRIVER_OTFA_TEST_BASE)

/**
 * \brief    Sub-module - DRIVER/TAMPER
 */
#define HSP_DRIVER_TAMPER_ENA_CORE_MON  (HSP_DRIVER_SVC_TAMPER_BASE + 0x1)
#define HSP_DRIVER_TAMPER_SMART_IDLE    (HSP_DRIVER_SVC_TAMPER_BASE + 0x2)
#define HSP_DRIVER_TAMPER_PMIC_OFF      (HSP_DRIVER_SVC_TAMPER_BASE + 0x3)
#define HSP_DRIVER_TAMPER_CPU_TPM_TRIG  (HSP_DRIVER_SVC_TAMPER_BASE + 0x4)
#define HSP_DRIVER_TAMPER_SET_RTC_ALARM (HSP_DRIVER_SVC_TAMPER_BASE + 0x5)
#define HSP_DRIVER_TAMPER_MON_CONTROL	(HSP_DRIVER_SVC_TAMPER_BASE + 0x6)
#define HSP_DRIVER_TAMPER_RTC_ALARM_ST  (HSP_DRIVER_SVC_TAMPER_BASE + 0x7)
#define HSP_DRIVER_TAMPER_GET_STS_AND_IRQ_STAT  (HSP_DRIVER_SVC_TAMPER_BASE + 0x10)
#define HSP_DRIVER_TAMPER_GET_TAMPER_LOGS		(HSP_DRIVER_SVC_TAMPER_BASE + 0x11)
#define HSP_DRIVER_TAMPER_GET_CORE_RESET_LOG    (HSP_DRIVER_SVC_TAMPER_BASE + 0x12)
#define HSP_DRIVER_TAMPER_RTC_GET_TIME			(HSP_DRIVER_SVC_TAMPER_BASE + 0x14)
#define HSP_DRIVER_TAMPER_RTC_SET_TIME			(HSP_DRIVER_SVC_TAMPER_BASE + 0x15)
#define HSP_DRIVER_TAMPER_RTC_ENABLE_ALARM		(HSP_DRIVER_SVC_TAMPER_BASE + 0x16)
#define HSP_DRIVER_TAMPER_RTC_SET_ALARM_TIME	(HSP_DRIVER_SVC_TAMPER_BASE + 0x17)
#define HSP_DRIVER_TAMPER_RTC_GET_ALARM_TIME	(HSP_DRIVER_SVC_TAMPER_BASE + 0x18)
#define HSS_DRIVER_TAMPER_RTC_GEN_ALARM_SGI		(HSP_DRIVER_SVC_TAMPER_BASE + 0x1A)
#define HSP_DRIVER_TAMPER_TEST_BASE         (HSP_DRIVER_SVC_TAMPER_BASE + HSP_TEST_OFF)
#define HSP_DRIVER_TAMPER_EVENT_LOG_TEST    (HSP_DRIVER_TAMPER_TEST_BASE + 0x4)
#define HSP_DRIVER_TAMPER_BBD_RESET_TEST    (HSP_DRIVER_TAMPER_TEST_BASE + 0x5)
#define HSP_DRIVER_TAMPER_LOCK_CTRL_TEST    (HSP_DRIVER_TAMPER_TEST_BASE + 0x6)
#define HSP_DRIVER_TAMPER_FAC_INIT_TEST     (HSP_DRIVER_TAMPER_TEST_BASE + 0x7)

/**
 * \brief    Sub-module - DRIVER/EFUSE
 */
#define HSP_DRIVER_EFUSE_READ_SVID      (HSP_DRIVER_SVC_EFUSE_BASE + 0x1)
#define HSP_DRIVER_EFUSE_VERIFY_SVID    (HSP_DRIVER_SVC_EFUSE_BASE + 0x2)

#endif	/* HSP_DEFINES_H */
