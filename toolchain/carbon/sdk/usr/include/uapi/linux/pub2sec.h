/*
 * Character driver to communicate with secure world.
 *
 *
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */


#ifndef __PUB2SEC_H
#define __PUB2SEC_H

/*
 * Define invariant to spot header mismatch between kernel and
 * user space applications.
 */
#define PUB2SEC_VERS		"v0.83"

#ifdef __KERNEL__
#define PUB2SEC_NAME		"pub2sec"	/* Platform Device */
#else
#define PUB2SEC_NODE		"/dev/pub2sec"	/* Device node */
#endif	/* __KERNEL__ */


/*
 * Maximum number of arguments that can be passed to secure world.
 */
#define PUB2SEC_MAXARGS		4

/**
 * Structure defines optional buffer payload with each IOCTL.
 */
struct pub2sec_buf {
	unsigned int src;
	unsigned int dst;
	unsigned int size;
};

/**
 * Structure of arguments passed to the character driver via IOCTL.
 */
struct pub2sec_cmd {
	unsigned int req;
	unsigned int flags;
	unsigned int status;
	unsigned int nargs;
	unsigned int args[PUB2SEC_MAXARGS];
	struct pub2sec_buf buf;
};

/*
 * Magic number for IOCTL
 */
#define PUB2SEC_MAGIC		'S'

/*
 * Sequence number for IOCTL
 */
#define PUB2SEC_SEQ		0x100

/*
 * Flags associated with calls to secure world
 */
#define PUB2SEC_FLAG_CRITICAL		0x4
#define PUB2SEC_FLAG_IRQFIQ		0x3
#define PUB2SEC_FLAG_IRQ		0x2
#define PUB2SEC_FLAG_FIQ		0x1
#define PUB2SEC_FLAG_NONE		0x0

/*
 * Bi-directional command for the driver
 */
#define PUB2SEC_CMD	_IOWR(PUB2SEC_MAGIC, PUB2SEC_SEQ, struct pub2sec_cmd)

/*
 * Request identifier for services provided by the kernel locally
 * i.e. the request doesn't cause switch to secure world.
 */
#define PUB2SEC_LOCAL_REQ		0x0

#endif	/* __PUB2SEC_H */
