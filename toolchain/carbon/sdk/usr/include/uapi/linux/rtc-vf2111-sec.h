#ifndef _RTC_RAPTOR_H_
#define _RTC_RAPTOR_H_

#define RTC_USER_TASK	_IOW('p', 0x90, struct rtc_time) /* get/set HW RTC */
#define RTC_SET_CURTIME	_IOW('p', 0x91, unsigned int) /* updaye current rtc offset  */

#endif
