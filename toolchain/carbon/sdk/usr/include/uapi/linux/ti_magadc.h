/*
 * ti_magadc.h - Header file for magadc driver
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __UAPI_TI_MAGADC_H__
#define __UAPI_TI_MAGADC_H__

#include <linux/types.h>
#include <linux/ioctl.h>
#include <stdbool.h>

/* User-space definitions */
#define MAGADC_RINGSEGMENTSIZE		(16 * 1024)
#define MAGADC_RINGSEGMENTS		(4)
#define MAGADC_RINGSIZE 		(MAGADC_RINGSEGMENTS * MAGADC_RINGSEGMENTSIZE)

/* This denote raw data for each track from ADC */
#define MAX_TRACK_DATA			(MAGADC_RINGSEGMENTSIZE * 11) /* ~1.5 secs the slower
								       * the swipe the more
								       * data we collect
								       */

/* Size of total Raw data collected from ADC */
#define MAX_NO_OF_TRACKS		(3)
#define MAX_ADC_DATA_RAW		(MAX_TRACK_DATA * MAX_NO_OF_TRACKS)

#define MSR_RANGE_OFFSET		(15)
#define STEPCONFIG_RANGE_CHK_MASK	(1<<27)

struct timag_track {
	bool			is_swiped;
	unsigned int		total_samples;
	unsigned int		num_tracks;
	unsigned int		bias[MAX_NO_OF_TRACKS];
	unsigned int		sampling_freq;
};

#define MAGADC_IOCTL			0xAE

#define MAGADC_READ_CONFIG              _IOR(MAGADC_IOCTL, 0, struct timag_track)


#endif /* __UAPI_TI_MAGADC_H__ */
