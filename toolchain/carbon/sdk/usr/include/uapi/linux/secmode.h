
#ifndef __SECMODE_H__
#define __SECMODE_H__

#include <linux/pub2sec.h>

#ifdef __KERNEL__
#define SECMODE_NAME		"secmode"	/* Platform Device */
#else
#define SECMODE_NODE		"/dev/secmode"	/* Device node */
#endif	/* __KERNEL__ */


#define SECMODE_NONE			0
#define SECMODE_VALUE			1
#define SECMODE_MEMREF_IN		2 // In to secure mode
#define SECMODE_MEMREF_OUT		4 // Out of secure mode


#define SECMODE_MAXARGS		4

struct secmode_memref {
	unsigned char *buffer;
	unsigned int size;
};


struct secmode_cmd {
	unsigned int req;
	unsigned int flags;
	unsigned int status;
	unsigned int args[SECMODE_MAXARGS];
	unsigned int types[SECMODE_MAXARGS];
};

struct secmode_basic_cmd {
	unsigned int req;
	unsigned int flags;
	unsigned int status;
	unsigned int nargs;
	unsigned int args[SECMODE_MAXARGS];
};

struct secmode_data_cmd {
	unsigned int req;
	unsigned int flags;
	unsigned int status;
	unsigned char *data;
	int datalen;
};



/*
 * Magic number for IOCTL
 */
#define SECMODE_MAGIC		'S'

/*
 * Sequence number for IOCTL
 */
#define SECMODE_SEQ		0x200

#define SECMODE_GEN_CMD	_IOWR(SECMODE_MAGIC, SECMODE_SEQ, struct secmode_cmd)
#define SECMODE_BASIC_CMD _IOWR(SECMODE_MAGIC, SECMODE_SEQ + 1, struct secmode_basic_cmd)
#define SECMODE_DATA_CMD _IOWR(SECMODE_MAGIC, SECMODE_SEQ + 2, struct secmode_data_cmd)

#ifdef __KERNEL__
int secure_mode_service(int req,
						int flags,
						int nargs,
						int arg1,
						int arg2,
						int arg3,
						int arg4);

int secure_mode_data_service(char *respdata,
							 int *respdatalen,
							 int maxrespdatalen,
							 char *cmddata,
							 int cmddatalen,
							 int appid,
							 int servid,
							 int flags);
#endif	/* __KERNEL__ */

#endif	/* __PUB2SEC_H */
