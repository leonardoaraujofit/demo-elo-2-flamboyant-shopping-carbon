#ifndef _INPUT_TLV320AIC31XX_H
#define _INPUT_TLV320AIC31XX_H
#include <linux/buzzer_param.h>

struct input_dev* get_tlv320aic31xx_input_dev(void* addr);
#endif
