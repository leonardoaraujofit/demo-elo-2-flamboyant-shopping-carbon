/*
 * Copyright (c) 2013 VeriFone, Inc.
 */

#include "X11/keysym.h"
#include "X11/Xutil.h"


/*
 * Original API renamed with NX_ prefix
 */
extern KeySym NX_XKeycodeToKeysym( Display *dpy, unsigned int kc, int index );
extern KeySym NX_XLookupKeysym(XKeyEvent *event, int index);
extern int NX_XLookupString(XKeyEvent *event, char *buffer, int nbytes, KeySym *keysym, XComposeStatus *status);
extern KeySym NX_XStringToKeysym(_Xconst char *string);
extern KeyCode NX_XKeysymToKeycode(Display *dpy, KeySym ks);
extern void NX_XConvertCase(KeySym in, KeySym *upper, KeySym *lower);

/**
 * \brief XKey API actual implementation callback table
 */
typedef struct {
    KeySym ( *cb_XKeycodeToKeysym )(Display *dpy, unsigned int kc, int index);
    KeySym ( *cb_XLookupKeysym )(XKeyEvent *event, int index);
    int ( *cb_XLookupString )(XKeyEvent *event, char *buffer, int nbytes, KeySym *keysym, XComposeStatus *status);
    KeySym ( *cb_XStringToKeysym )(_Xconst char *string);
    KeyCode ( *cb_XKeysymToKeycode )(Display *dpy, KeySym ks);
    void ( *cb_XConvertCase )(KeySym in, KeySym *upper, KeySym *lower);
} NX_KeyAPI;


/**
 * \brief Get the callback table, which can be modified by application at startup
 * \note Custom callbacks can be used as a proxy to NX_X*() standard API
 */
extern NX_KeyAPI* NX_GetXKeyAPI();