/***********************************************************************************
                            \\\|///
                          \\  ~ ~  //
                           (  @ @  )
/------------------------oOOo-(_)-oOOo----------------------------------------------\
|
|PACKAGE:       Linux Printer High Level declarations.
|DESCRIPTION:   High-level interface code for the termal printer driver
|4B 6F 73 74 61  41 72 74
/---------------------------------Oooo.---------------------------------------------/
                        .oooO     (   )
                        (   )      ) /
                         \ (      (_/
                          \_)
*************************************************************************************/
/**
 * @copyright
 * COPYRIGHT (C) 2012 by VeriFone, Inc.  All rights reserved.
 * @file printer.h
 * @brief High-level interface code for the termal printer driver
 */


#ifndef __PRNTER_H__
#define __PRNTER_H__

	/* ======================================= *
	 *         D E F I N I T I O N S           *
	 * ======================================= */

/** \{ */
#define PRINTER_OK                   0 /**< Successful result */
#define PRINTER_OUT_OF_PAPER	    -1 /**< No Paper */
#define PRINTER_FIFO_FULL	        -2 /**< FIFO is full */
#define PRINTER_TIMEOUT		        -3 /**< timeout */
#define PRINTER_ERROR	            -4 /**< Unrecoverable Error */
#define PRINTER_FONT_NOT_AVAILABLE  -5 /**< No such font */
#define PRINTER_FIFO_OVERFLOW       -6 /**< Data to print exceed FIFO MAX size 61440 bytes Linux fixed */
#define PRINTER_LOW_BATTERY         -7 /**< Battery Voltage < 3.5 volt Can't print */
#define PRINTER_OVERHEAT            -8 /**< VP temp > 70 degC */
#define PRINTER_VP_ERROR            -9 /**< VP Voltage is less than 5.5 V or greater than 13V*/
#define PRINTER_CUT_ERROR           -10 /**< the malfunction of autocutter prformance*/
/** \} */


// Note: Do not use these defines directly, use the variable equivalents so that multiple printer sizes can be supported.
#define FIFO_MAX_SIZE 0x10000-4096 /*65536 bytes Linux fixed*/
#define FIFO_MAX_SIZE_3INCH	0x20000-4096 // Swordfish printer is fast and will be printing large bitmap images



enum printer_e_inverse_color
{
	PRINT_WHITE = 0,
	PRINT_BLACK = 0xFF,
};

typedef struct 
{
	int OutOfPaper;
	int JobsInFifo;
	int CurrentJob;
} PrintStatus;

typedef struct 
{
	int OutOfPaper;
	int JobsInFifo;
	int CurrentJob;
	int TempOverheat;
	int VpError;
	int LowBattery;
	int CutError;
} PrintStatusExt;

// Printer Cut Modes
#define	PRINTER_NO_CUT			0
#define PRINTER_FULL_CUT		1
#define PRINTER_PARTIAL_CUT		2

// Printer Feed Options
#define PRINTER_FEED_NO		0	// no feed after print
#define PRINTER_FEED_ONE	1	// feed type one (paper feed after print - so if cut enabled, cuts below image)


/* Printer API's */

/**
 * Create monochrome bitmap of text  
 *
 * Send Data to FIFO
 *
 * @param[in] buf	 : NULL terminated text string *
 * @param[in] length : string length, <= strlen(string)
 * @param[in] withStatus : none-zero to wait for print to finish and return printer status
 *
 * @return
 * @li Job ID                   - if successfully write all data to printer FIFO
 * @li PRINTER_OUT_OF_PAPER		- No or out of paper
 * @li PRINTER_FIFO_FULL		- FIFO Full
 * @li PRINTER_FIFO_OVERFLOW    - Data to print exceed FIFO MAX size   
 * 
 * @note Non blocking
 */
int Printer_PrintText( const char *buf, int length );
int Printer_PrintText_status( const char *buf, int length, int withStatus );

/**
 * Create monochrome bitmap of pixel lines and write  to FIFO
 *
 * @param[in] line_num : Number of pixel lines
 * @param[in] inverse  : BLACK/WHITE
 * @param[in] withStatus : none-zero to wait for print to finish and return printer status
 *
 * @return:  
 * @li Job ID                   - if successfully write all data to printer FIFO
 * @li PRINTER_OUT_OF_PAPER	    - No paper
 * @li PRINTER_FIFO_FULL        - FIFO Full
 * @li PRINTER_FIFO_OVERFLOW    - Data to print exceed FIFO MAX size
 *
 * @note Non blocking
 */
int Printer_PrintLine( int line_num , int inverse );
int Printer_PrintLine_status( int line_num , int inverse, int withStatus );

/**
 * Wait for finish printing process  
 *
 * @param[in] timeout         - timeout (seconds) positive number
 * @param[in] job_id          - job_id to wait positive number
 *
 * @return
 * @li PRINTER_OK                - printing job finished 
 * @li PRINTER_TIMEOUT           - still printing
 * @li PRINTER_OUT_OF_PAPER      - no paper
 * @li PRINTER_ERROR             - invalid parameter ore unrecoverable error happened
 *
 * @note Blocking API
 */
int Printer_WaitReady( unsigned int job_id, unsigned int timeout );

/**
 * Cancel all printing jobs  
 *
 * @param job_id;
 */
void Printer_CancelPrint( void );

/**
 * Send monochrome bitmap to FIFO
 * @param[in] buf    : bitmap Data
 * @param[in] length : bitmap length
 * @param[in] withStatus : none-zero to wait for print to finish and return printer status
 * 
 * @return
 * @li Job ID                - if successfully write all data to printer FIFO
 * @li PRINTER_OUT_OF_PAPER  - No paper
 * @li PRINTER_FIFO_FULL     - FIFO Full
 * @li PRINTER_FIFO_OVERFLOW - Data to print exceed FIFO MAX size   
 * @li PRINTER_ERROR         - Cannot allocate memory
 */
int Printer_PrintBitMap( const char *buf, int length );
int Printer_PrintBitMap_status( const char *buf, int length, int withStatus );

/**
 * Choose available ttf font. Default printer font Vera.ttf.
 *
 * @param[in] font_name : font name
 *
 * @return
 * @li PRINTER_OK    - Success
 * @li PRINTER_ERROR - Font not found
 */
int Printer_SetFont( char *font_name );

/**
 * Set available ttf font size. Default printer font size 4..
 *
 * @param[in] font_name : font name
 *
 * @return
 * @li PRINTER_OK - Success
 * @li PRINTER_ERROR - size not available
 */
int Printer_SetFontSize( char font_size );

int Printer_SetFireWeight(int FireWeight/*-4 - +8*/); 

/**
 * fill status structure
 *
 * @param[in] buf : pointer to status struct
 *
 * @return: 
 * @li PRINTER_OK - if successfully fill structure
 * @li PRINTER_ERROR - error
 *
 * @note Non blocking
 */
int Printer_GetStatus( PrintStatus *Status );

/**
 * fill status structure
 *
 * @param[in] buf : pointer to status struct
 * @param[in] size : size of status struct
 *
 * @return: 
 * @li PRINTER_OK - if successfully fill structure
 * @li PRINTER_ERROR - error
 *
 * @note Non blocking
 */
int Printer_GetStatusExt( PrintStatusExt *Status, unsigned int size );

/**
 * Blocking API wait paper
 *
 * @param[in] timeout : timeout
 * 
 * @return
 * @li PRINTER_OK      - Paper inside
 * @li PRINTER_TIMEOUT - No paper
 * @li PRINTER_ERROR   - Unrecoverable Error happened
 */
int Printer_WaitPaper( unsigned int timeout );

/**
 * Call function callback when out of paper occured  
 *
 * @param[in] pointer: - callback
 *
 * @return 
 * @li PRINTER_OK    - success
 * @li PRINTER_ERROR - Unrecoverable Error happened
 */
int Printer_OutOfPaperEvent( void(*callback)(int) );

/**
 * Set the printer paper cutter mode - Only works on printers that support paper cutting!
 * As setting the cut mode is outside of performing a print job, this API will not be thread safe.
 * It is recommended that the printer service be used to ensure that print jobs do not interfear with each other.
 *  
 * @param[in] mode: - Cutter Mode, 0 = No Cut, 1 = Full Cut, 2 = Partial Cut
 *
 * @return 
 * @li PRINTER_OK    - Success
 * @li PRINTER_ERROR - Error
 */
int Printer_SetCutterMode( int mode );

/**
 * Set the printer feed mode
 *
 * @param[in] mode: - Feed after print, PRINTER_FEED_NO = No Feed, PRINTER_FEED_ONE = feed type one
 *
 * @return
 * @li PRINTER_OK    - Success
 * @li PRINTER_ERROR - Error
 */
int Printer_SetFeedMode(int mode);

/** Notify printer of high current pulse
 * Something in system is about to cause a high current operation to occur and want to notify printer.
 * This gives printer ability to reduce current draw (slow printing?) during the pulse.
 * All this is to avoid over current draw of the power supply.
 *
 * @param[in] pulseWidth - width of current pulse in ms.
 *
 * @return
 * @li PRINTER_OK    - Success
 * @li PRINTER_ERROR - Error
 */
int Printer_CurrentPulse(int pulseWidth);

#endif /*#ifndef   __PRNTER_H__*/
