/****************************************************************************
* This source code is confidential proprietary information of VeriFone Inc.	*
* and is an unpublished work or authorship protected by the copyright laws  *
* of the United States. Unauthorized copying or use of this document or the *
* program contained herein, in original or modified form, is a violation of *
* Federal and State law.                                                    *
*                                                                           *
*                           Verifone                                        *
*                      Rocklin, CA 95765                                    *
*                        Copyright 2003                                     *
*****************************************************************************
* 
;|==========================================================================+
;| FILE NAME:  | sc_kernel.h
;|-------------+------------------------------------------------------------+
;| DESCRIPTION:| kernel part of the smart card driver
;|-------------+------------------------------------------------------------+
;| TYPE:       | 
;|-------------+------------------------------------------------------------+
;| COMMENTS:   | 
;|==========================================================================+
* 
$Archive:   $
$Author:    Thierry Payet
$Date:      04/08/14
$Modtime:   $
$Revision:  0.1
$Log:       $
*/

#ifndef __SC_KERNEL_H__
#define  __SC_KERNEL_H__

          /*==========================================*
           *           D E F I N I T I O N S          *
           *==========================================*/
/* defines used in notify action
 * action will be composed of two part
 * | 0x0000 | <slot> - 2 bytes| <action>- 2 bytes |
 */
#define SC_PHY_NOTIFICATION_ACTION_MASK (0x000000ffU)
#define SC_PHY_NOTIFICATION_ACTION_SHIFT (0)

#define SC_PHY_NOTIFICATION_SLOT_MASK (0x0000ff00U)
#define SC_PHY_NOTIFICATION_SLOT_SHIFT (8)

#define USIM_SIGID	51

/*
 * smartcard events to be signaled 
 */
enum usim_event {
	SCR_EVENT_CARD_INSERT	= 0x1,
	SCR_EVENT_CARD_REMOVE	= 0x2,
	SCR_EVENT_TIMEOUT		= 0x4,
	SCR_EVENT_ERR_TXRETRY	= 0x8,
	SCREVENT_ERR_LRC		= 0x10,
	SCR_EVENT_ERR_PARITY	= 0x20,
	SCR_EVENT_ERR_FRAME		= 0x40,
	SCR_EVENT_PHYERR		= 0x80,
	SCR_EVENT_CARD_OVERHEAT = 0x100,
};

#define SYNC_ICC_MAX_SCRIPT_LEN     2048
#define SYNC_ICC_MAX_DATA_BITS      8192
#define SYNC_ICC_MAX_DATA_BYTES     ((SYNC_ICC_MAX_DATA_BITS+7)/8)		/*1024*/

// Returned Status for IccSyn_Status() function
#define SYNC_ICC_SYNC_NO_ERR                  1
#define SYNC_ICC_SYNC_ACTIVE                  0
#define SYNC_ICC_NOT_PRESENT                 -1
#define SYNC_ICC_NOT_ACTIVE                  -2
#define SYNC_ICC_ASYNC_ACTIVE                -3
#define SYNC_ICC_BAD_SLOT_NUM                -4

/********************* IccSyn_RunScript() Errors definition ***********************/
#define SYNC_ICC_SCRIPT_LEN_ERR		-10	 /* ScriptLen > SYNC_ICC_MAX_SCRIPT_LEN (2048 bytes) */
#define SYNC_ICC_WRITE_LEN_ERR		-11	 /* DataToLen > SYNC_ICC_MAX_DATA_BITS (8192 bits) */
#define SYNC_ICC_ENDLESS_LOOP		-12	 /* not scEND_LOOP between scSTART_LOOP	and scSTOP_SCRIPT */
#define SYNC_ICC_WRITE_PARAM_ERR	-13	 /* during scDATA_WRITE_LO/HI: DataToCard pointer is NULL */
#define SYNC_ICC_READ_PARAM_ERR		-14	 /* during scDATA_READ_LO/HI: DataFromCard pointer is NULL  */
#define SYNC_ICC_SLOT_NUM_ERR		-15	 /* Slot num > MAX_CARD_NB */
#define SYNC_ICC_DEACTIVATED		-16	 /* card was deactivate during script running */
#define SYNC_ICC_REMOVED			-17	 /* card was removed during script running */
#define SYNC_ICC_LOOP_NO_START		-18	 /* not scSTART_LOOP before scEND_LOOP */
#define SYNC_ICC_DOUBLE_LOOP_START	-19    /* duble scSTART_LOOP before scEND_LOOP */
#define SYNC_ICC_ENDLESS_SCRIPT		-20	 /* script without scSTOP_SCRIPT */
#define SYNC_ICC_I2C_ERR			-21	 /* some I2C communication error */
#define SYNC_ICC_READ_OVERFLOW		-22	 /* during scDATA_READ_LO/HI: try to read more as SYNC_ICC_MAX_DATA_BITS (8192 bits)*/
#define SYNC_ICC_WRITE_OVERFLOW		-23	 /* during scDATA_WRITE_LO/HI: not enough bits in DataToCard array */
#define SYNC_ICC_UNKNOWN_CMND		-24	 /* unknown command in the script */
#define SYNC_ICC_OVERLOAD			-25	 /* card power supply is an overload or an overheating */

          /*==========================================*
           *      T Y P E   D E C L A R A T I O N     *
           *==========================================*/

       /*=================================================*
        *   H W    S U P P O R T    F U N C T I O N S     *
        *================================================*/
void sc_SecureTxIsoUart(unsigned char *TxData,int Len);


#endif /*of __SC_KERNEL_H__ */
