
#ifndef __CTLS_INTERFACE_H__
#define __CTLS_INTERFACE_H__


#ifdef __cplusplus
extern "C"{
#endif

            /*==========================================*
            *           D E F I N I T I O N        *
            *==========================================*/

#define CTLSINTERFACE_VERSION       "CTLSINTF01.00.23.01"
#define CTLSINTERFACE_VERSION_LEN   19


#ifndef BOOLEAN
#define BOOLEAN int
#endif

/* r e t u r n    v a l u e s */
#define CTLSINTERFACE_ERROR -1
#define CTLSINTERFACE_OK     0
#define CTLS_MAX_LED         8
#define E_IN_USE        -104 //Another application has ownership of the device
#define E_NOT_AVAIL     -105 //Terminal HW does not include a CTLS device
#define E_CTLS_APP_ERROR    -101 //CTLS application is not present/running.

/* U I   B u z z e r   s t a t u s */
#define UI_BUZZER_FREQUENCY_OFF         -1  // frequency code -1 means turn the buzzer off
#define UI_BUZZER_FREQUENCY_ON           0

/* f o r m a t / a l i n g m e n t  c o n s t a n t s   u s e d   b y   t h e  L C D  T e x t H a n d l e r */
#define CTLS_SET_LEFT            0
#define CTLS_SET_RIGHT           0x10
#define CTLS_SET_CENTER          0x20
#define CTLS_SET_HPIXEL          0x40    /* Use specific column */
#define CTLS_SET_TOP             0
#define CTLS_SET_BOTTOM          0x01
#define CTLS_SET_VCENTER         0x02
#define CTLS_SET_SPREAD          0x03
#define CTLS_SET_VPIXEL          0x08    /* Use specific row */

/* U I  L e d s   s t a t u s */
#define CTLS_LED_OFF            0
#define CTLS_LED_ON         1
#define CTLS_LED_FLASH          2

/* U I   t y p e s   o f   r e q u e s t */
#define UI_START_REQUEST    1
#define UI_STOP_REQUEST     2
#define UI_BUZZER_REQUEST   3
#define UI_LED_REQUEST      4
#define UI_LCD_REQUEST_TEXT 5
#define UI_REQUEST_SET_PARAM    6
#define UI_LCD_REQUEST_CLEAR    7

#define CTLS_REQUEST        1

/* U I   l e d   s t y l e  */
#define CTLS_RAINBOW_STYLE_LED  1       // used with the UI Framework set param function for red/yellow/green/blue LED
#define CTLS_GREEN_STYLE_LED    0       // used with the UI Framework set param function for green led config

/* f l a s h  t i m e  */
#define CTLS_LED_FLASH_TIME 500   // 500 msec

/* p l a c e  o f  i m a g e s */
#define CTLS_RES_PATH           "/mnt/flash/system/images/ctlsl2"

/*  U I   L E D   i m a g e s  */
#define CTLS_OFF_LED_BMP        "/offled.png"
#define CTLS_GREEN_LED_BMP      "/greenled.png"
#define CTLS_RED_LED_BMP        "/redled.png"
#define CTLS_BLUE_LED_BMP       "/blueled.png"
#define CTLS_YELLOW_LED_BMP     "/yellowled.png"


/* U I  P a r a m e t e r  i d ' s */
#define UI_LANGUAGE_PARAM           1
#define UI_FONT_PARAM               2
#define UI_LED_STYLE_PARAM          3
#define UI_CARD_ISSUER_LOGO_PARAM       4

/* U I  S t y l e s  - F o r E M V S t y l e, C a r d I s s u e r B M P s s h o u l d  b e d i s p l a y e d . */
#define UI_EMV_STYLE        1
#define UI_PASSTHRU_STYLE   2

/* U I C o n s t a n t  i d ' s ( f o r   u s e  w i t h   C T L S C l i e n t G e t U I C o n s t a n t s  ) */
#define CTLS_CONST_TEXT_LINES           1
#define CTLS_CONST_SOFT_LED_POSITIONS       2
#define CTLS_CONST_LOGO_POSITION        3
#define CTLS_CONST_ANTENNA_CENTER       4
#define CTLS_CONST_TAP_BMP_NAME         5
#define CTLS_CONST_TAP_BMP_DIMENSIONS       6

/* U I  E n a b l e d   b i t   s e t t i n g s */
#define CTLS_APP_BIT    (1<<2)
#define CTLS_OWNER_BIT  (1<<1)

            /*==========================================*
            *           S T R U C T U R E S         *
            *==========================================*/



// CTLSUIParms defines parameters related starting the UI
typedef struct {
    int uiMode;         // EMV or pass through.
} CTLSUIParms;

typedef struct {
    int nLED;
    unsigned char led[CTLS_MAX_LED];
} CTLSLEDStatus;

typedef struct {
   int frequency;
   int pause;
   int duration;
   int nTimes;
} CTLSBuzzerRequest;

typedef struct {
   char stringToDisplay[64];
   int backgroundColor;
   int foregroundColor;
   int fontId;
   int format;      // alignment
   int column;
   int row;
} CTLSLCDText;

/*=====================================================
*       U s e r  I n t e r f a c e  H a n d l e r s
*
*  These definitions are used for
*  applications that want to override
*  the default user inteface handlers.
*======================================================*/

typedef void (*CTLSLEDFunc) (CTLSLEDStatus* leds);
typedef void (*CTLSStartUIFunc) (CTLSUIParms* pParms);
typedef void (*CTLSLCDTextFunc) (CTLSLCDText* pText);
typedef void (*CTLSLCDClearFunc) (void* tbd);
typedef void (*CTLSBuzzerFunc) (CTLSBuzzerRequest* pParms);
typedef void (*CTLSStopUIFunc) (void* tbd);
typedef void (*CTLSUIParamFunc) (int paramId, void* paramVal, int paramLength);

typedef struct {
    CTLSStartUIFunc startUIHandler;
    CTLSStopUIFunc stopUIHandler;
    CTLSUIParamFunc uiParamHandler;
    CTLSLEDFunc ledHandler;
    CTLSLCDTextFunc textHandler;
    CTLSLCDClearFunc clearDisplayHandler;
    CTLSBuzzerFunc buzzerHandler;
    void* reserved1;
} CTLSUIFuncs;

/*===================================================
*         U I   R e q u e s t  P a r a m e t e r s
*====================================================*/

typedef struct {
    int  paramId;
    char paramValue[32];
    int  paramLen;
} CTLSUISetParam;


typedef union {
    CTLSBuzzerRequest       buzzRequest;
    CTLSLEDStatus           ledRequest;
    CTLSLCDText         lcdTextRequest;
} UIRequestParms;

            /*==========================================*
            *           F U N C T I O N S           *
            *==========================================*/

/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSClientGetVersion
 * DESCRIPTION:   Return a curent version of ctlsinterface
 * RETURN:        return size of version
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
int CTLSClientGetVersion(char* cVersion);
/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSInitInterface
 * DESCRIPTION:   Initialize Library Interface - Initializing the interface requires establishing communication with the CTLS app.
 * RETURN:        return < 0 then error
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
int CTLSInitInterface(int ticks);

/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSClientUIInit
 * DESCRIPTION:   CTLS UI initialization
 * RETURN:        none
 * NOTES:        ClientUIInit() is called by the UI Thread at start up time. This
 *               just gives the UI handlers an opportunity to initialize any
 *       control variables.
 * ------------------------------------------------------------------------ */
void CTLSClientUIInit();

/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSClientIsUIInit
 * DESCRIPTION:   check if user interface framework is initialized
 * RETURN:        Return a value of uiInitComplete flag
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
int CTLSClientIsUIInit();

/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSInitUIInterface
 * DESCRIPTION:   Initialize the UI framework
 * RETURN:        return < 0 then error
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
int CTLSInitUIInterface(char* logicalName, int tickTimeout);

/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSClientGetUIConstants
 * DESCRIPTION:   retrieve constants used for display positioning
 * RETURN:        none
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
int CTLSClientGetUIConstants(int paramId, void* buff, int max);



            /*======================================================================*
            *           D e v i c e   o w n e r / m a n a g e m e n t   A P I s *
            *=======================================================================*/
/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSOpen
 * DESCRIPTION:   establish ownership of the reader device
 * RETURN:        return < 0 then error
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
int CTLSOpen();

/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSClose
 * DESCRIPTION:   close the device and free it for other applications
 * RETURN:        return < 0 then error
 * NOTES:
 * ------------------------------------------------------------------------ */
int CTLSClose();

/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSGetOwner
 * DESCRIPTION:   get the CTLS owner task id(function are not relevant for Mx)
 * RETURN:        always return error
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
int CTLSGetOwner();

/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSSetOwner
 * DESCRIPTION:   transfer ownership of the reader(function are not relevant for Mx)
 * RETURN:        always return error
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
int CTLSSetOwner (int newOwnerId);


            /*======================================================================*
            *           R e a d e r   C o m m u n i c a t i o n s       *
            *   buff data should conform to reader protocol such as Vivopay *
            *=======================================================================*/
/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSReceive
 * DESCRIPTION:   read a message from the reader
 * RETURN:        return < 0 then error
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
int CTLSReceive (char* buff, int maxLength);

/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSSend
 * DESCRIPTION:   send a message to the reader
 * RETURN:        return < 0 then error
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
int CTLSSend (char* buff, int length);

/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSPending
 * DESCRIPTION:   check if if CTLS data is available.
 * RETURN:        return > 0 if CTLS data is available in socket(return number bytes on socket).
          return = 0 if is no data on socket.
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
int CTLSInputPending();


            /*======================================================================*
            *           U s e r   I n t e r f a c e   H a n d l i n g       *
            *=======================================================================*/
/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSGetUI
 * DESCRIPTION:   retrieve the current callback function pointers
 * RETURN:        return < 0 then error
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
int CTLSGetUI(CTLSUIFuncs* uiFuncs);


/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSSetUI
 * DESCRIPTION:   update the callback function pointers
 * RETURN:        return < 0 then error
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
int CTLSSetUI(CTLSUIFuncs* uiFuncs);

            /*======================================================================*
            *           U I   C a r d   I s s u e r   L o g o   B M P       *
            *=======================================================================*/
/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSClientUIGetCardLogoBMP
 * DESCRIPTION:   UI Card Issuer Logo BMP
 * RETURN:        return < 0 then error
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
int CTLSClientUIGetCardLogoBMP(char* buff, int len);

/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSClientUISetCardLogoBMP
 * DESCRIPTION:   UI Card Issuer Logo BMP
 * RETURN:        return < 0 then error
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
int CTLSClientUISetCardLogoBMP(char* fileName);

            /*=================================================================================================================*
            *          A P I s   f o r   m a n u a l l y  e n a b l i n g  / d i s a b l i n g  u s e r  i n t e r f a c e     *
            *==================================================================================================================*/

/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSClientUIEnable
 * DESCRIPTION:   enable/disable user interface
 * RETURN:        none
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
void CTLSClientUIEnable(int onOff);

/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSClientIsUIEnabled
 * DESCRIPTION:   Return a value of uiEnable flag
 * RETURN:        none
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
int CTLSClientIsUIEnabled(void);



        /*==============================================================================================*
        *           D e f a u l t   U s e r   I n t e r f a c e   H a n d l e r             *
        *                                               *
        *   Default User Interface Handlers - these should not normally be directly called      *
        *   by the application.                                     *
        *   These APIs are made public for applications that need to override default UI Handling   *
        *   (ie. call CTLSSetUI())                                  *
        *===============================================================================================*/

/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSClientStartUI
 * DESCRIPTION:   Default for startUIHandler
 * RETURN:        none
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
void  CTLSClientStartUI(CTLSUIParms* pParms);

/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSClientStopUI
 * DESCRIPTION:   Default for stopUIHandler
 * RETURN:        none
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
void CTLSClientStopUI (void* tbd);

/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSClientUIParamHandler
 * DESCRIPTION:   Default parameter handling
 * RETURN:        none
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
void CTLSClientUIParamHandler(int paramId, void* paramVal, int paramLength);

/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSClientLEDHandler
 * DESCRIPTION:   Default for managing LEDs
 * RETURN:        return < 0 then error
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
void CTLSClientLEDHandler (CTLSLEDStatus* leds);


/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSClientLCDTextHandler
 * DESCRIPTION:   Default for managing text display
 * RETURN:        return < 0 then error
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
void CTLSClientLCDTextHandler (CTLSLCDText* pText);

/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSClientLCDClearHandler
 * DESCRIPTION:   Default for clearing display (CTLS message area)
 * RETURN:        return < 0 then error
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
void CTLSClientLCDClearHandler (void* tbd);

/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSClientBuzzerHandler
 * DESCRIPTION:   Default for managing buzzer requests
 * RETURN:        return < 0 then error
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
void CTLSClientBuzzerHandler (CTLSBuzzerRequest* pParms);

            /*======================================================================*
            *           H W   a n d   S O F T   L E D S      A P I          *
            *=======================================================================*/
/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSClientSoftLEDConfig
 * DESCRIPTION:   configure bmps to use for soft LEDs
 * RETURN:        return < 0 then error
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
int CTLSClientSoftLEDConfig(int led, char* bmp);

/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSClientSoftLEDEnable
 * DESCRIPTION:   enable/disable the Soft LEDs
 * RETURN:        return < 0 then error
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
void CTLSClientSoftLEDEnable(int onOff);

/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSClientLEDEnable
 * DESCRIPTION:   enable/disable the LEDs
 * RETURN:        return < 0 then error
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
void CTLSClientLEDEnable(int on);

/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSClientHWLEDEnable
 * DESCRIPTION:   enable/disable the HW LEDs
 * RETURN:        return < 0 then error
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
void CTLSClientHWLEDEnable(int on);

/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSClientSoftLEDSet
 * DESCRIPTION:   set (turn on/off/flash) the soft LEDs
 * RETURN:        return < 0 then error
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
void CTLSClientSoftLEDSet(int l1, int l2, int l3, int l4);

/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSClientSoftLEDSet
 * DESCRIPTION:   set (turn on/off/flash) the LEDs
 * RETURN:        return < 0 then error
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
void CTLSClientLEDSet(int l1, int l2, int l3, int l4);

/* --------------------------------------------------------------------------
 * FUNCTION NAME: CTLSClientSoftLEDSet
 * DESCRIPTION:   set (turn on/off/flash) the HW LEDs
 * RETURN:        return < 0 then error
 * NOTES:         none.
 * ------------------------------------------------------------------------ */
void CTLSClientHWLEDSet(int l1, int l2, int l3, int l4);

#ifdef __cplusplus
}       /* extern "C" */
#endif

#endif /*__CTLS_INTERFACE_H__*/

