#ifndef __SYSM_VSP__
#define __SYSM_VSP__

#include <sysmode_ui/sysm_ui.h>
#include <sysmode_ui/sysm_lang.h>

#if 1
// Queue Defines
#define QUE_SEMTEKD					500			// To semtekd
#define QUE_SEMTEKRSP  				501			// From semtekd

// Message IDs - (aka function codes), using clientPid as IPC "message type"
#define MSG_CLEAR_MSR				1			// Request encryption
#define MSG_ENCRYPTED_MSR			2
#define MSG_ENCRYPTED_PAN			3			// Request clear pan for PinBlock
#define MSG_CLEAR_PAN				4
#define MSG_STATUS_REQUEST			5
#define MSG_STATUS_ANSWER			6
#define MSG_SEMTEKD_RESET			7			// This message has no associated structure
#define MSG_SEMTEKD_RESET_RESULT	8			// This message sends back a
#define MSG_START					9
#define MSG_START_RESULT			10
#define MSG_STOP					11
#define MSG_STOP_RESULT				12
#define MSG_RESET_PASSWORD			13
#define MSG_RESET_PASSWORD_RESULT	14
#define MSG_MANUAL_ENTRY			15
#define MSG_MANUAL_ENTRY_RESULT 	16
#define MSG_EPARM_REQUEST			17
#define MSG_EPARM_RESULT 			18
#define MSG_CLEAR_DATA_REQUEST		19
#define MSG_CLEAR_DATA_RESPONSE		20
#define MSG_CLEAR_BUFFERS_REQUEST	21
#define MSG_CLEAR_BUFFERS_RESPONSE	22
#define MSG_OVERRIDE_MSG_QUERY		23
#define MSG_OVERRIDE_MSG_RESPONSE	24
#define MSG_DIAG_QUERY				25
#define MSG_DIAG_RESPONSE			26
#define MSG_ADVANCE_DDK				27
#define MSG_ADVANCE_DDK_RSP			28
#define MSG_SWITCH_UKPD				29
#define MSG_SWITCH_UKPD_RSP			30
#define MSG_REGISTART_REQ			31
#define MSG_REGISTART_RSP			32
#define MSG_REGISTOP_REQ			33
#define MSG_REGISTOP_RSP			34
#define MSG_REGISTART_SRED_REQ		35
#define MSG_REGISTART_SRED_RSP		36
#endif

#define MAX_BYTES_TRACK		150
#define MAX_INFO_LEN		255

enum vspInfoType
{
	VSP_INFO_VCL = 0,
	VSP_INFO_STATE,
	VSP_INFO_ENCRYPT,
	VSP_INFO_MDK,
	VSP_INFO_TERR,
	VSP_INFO_SWIPES,
	VSP_INFO_STATUS,
};

void GetSemtekStatus(char *pszStr1, char *pszStr2, char *pszStr3, char *pszStr4,  char *pszStr5);

#endif //__SYSM_VSP__
