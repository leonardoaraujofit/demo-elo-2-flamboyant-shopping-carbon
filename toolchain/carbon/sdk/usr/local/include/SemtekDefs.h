/**
 *  Copyright 2008 Semtek Innovative Solutions Inc.  All Rights Reserved.
 * 
 *  This program is confidential, proprietary, and unpublished property of
 *  Semtek Innovative Solutions Inc.  It may NOT be copied or distributed
 *  in part or in whole on any medium, either electronic or printed, without
 *  the express written consent of Semtek Innovative Solutions Inc.
 * 
 *  Unpublished -- rights reserved under the copyright laws of the 
 *  United States and other countries.
 * 
 *  \file SemtekDefs.h
 *  Contains definitions for semtek local types
 */
#ifndef SEMTEK_DEFS_H
#define SEMTEK_DEFS_H

#ifdef VCL_INTERNALS
typedef unsigned char uint8;
typedef unsigned short uint16;
typedef unsigned long uint32;
typedef unsigned long long uint64;


typedef char int8;
typedef short int16;
typedef long int32;
typedef long long int64;

#endif
typedef unsigned long SEMTEK_RESULT;
#ifdef WIN32
#include <windows.h>
typedef unsigned int uint;
#else
#ifdef VCL_INTERNALS
	typedef unsigned char BYTE;
	typedef uint32 DWORD;
	typedef uint8 BOOL;

	#define TRUE  1
	#define FALSE 0
#endif
#endif

/// These are for the key table

/// Key length for all keys is 128 bits.  For single des, we'll just use half.
#define SEMTEK_KEY_LENGTH 16
#define SEMTEK_VDESKEY_LENGTH 24

/// Our key table will only support 16 entries for Verifone RAI V2.
#define SEMTEK_KEY_TABLE_SIZE 16

/// Length for BIN/IIN range
#define BIN_RNG_LENGTH	6

/// Define our own type for Keys.  All keys for Verifone RAI V2 are 128 bits long.
/// Keys stored in this structure are BINARY...
typedef unsigned char Key[SEMTEK_VDESKEY_LENGTH];

// The platform is really going to define what the key types are but we can
// define a KeyH as a void* and let the platform convert to what it wants
typedef void* KeyH;

/// Here we define the error return value for success (no error).
#define SEMTEK_SUCCESS 0

/// This is the defined length for the serial number, per the warble data format spec.
#define SEMTEK_SERIAL_LENGTH 6

/// This is the defined length of Verifone's serial numbers (Format = XXX-XXX-XXX where X = 0-9)
#define VF_SER_NUM_LENGTH 11

/// This is the definition from Clay's first substitution table.
#define SEMTEK_SUB_TABLE_SIZE 160

// for securing start/stop operations via the API
#define MSG_PASSWD_SIZE 16

// This is the defined length of the Bin Table ID, inicluding the NULL terminator.
#define BIN_TABLE_ID_SIZE 21

// Error bases all get defined here.
#define SEMTEK_ENCRYPTION_ERROR_BASE 1000
#define	SEMTEK_FS_ERROR_BASE 1100
#define SEMTEK_MD_ERROR_BASE 1200
#define SEMTEK_STRING_ERROR_BASE 1300
#define SEMTEK_CMD_CARD_ERROR_BASE 1400
#define SEMTEK_TRACK_ENCRYPTION_ERROR_BASE	5000

/// use this macro to determine number of Array elements
#define ARRAY_SIZEOF(x) (sizeof(x)/sizeof(x[0]))

// These are the different versions that can be specified in the Makefile
#define VERIFONE_SW_MX        1
#define CIPHER_2_HW           2
#define VERIFONE_VERIX_V      3
#define VDES_VERSION_CUSTOM   4

#ifdef WIN32
#define VDES_VERSION VDES_VERSION_CUSTOM
#else
#if (VDES_VERSION!=CIPHER_2_HW && VDES_VERSION!=VERIFONE_VERIX_V && VDES_VERSION!=VDES_VERSION_CUSTOM)
#define VDES_VERSION VERIFONE_SW_MX
#endif
#endif

#if (VDES_VERSION==VERIFONE_VERIX_V)
//#define PACKED_SPEC __packed
#define PACKED_SPEC
#define strcasecmp strcmp
#else
#define PACKED_SPEC
#endif

#ifdef VCL_INTERNALS
#include "SemtekAppConfig.h"
#include "SemtekDbg.h"

void
SemtekWait(DWORD dwMillisecs);
#endif

#endif

