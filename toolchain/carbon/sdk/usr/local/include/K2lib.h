 
/**************************************************************************
 * FILE NAME:   K2lib.h                                                   *
 * MODULE NAME: K2LIB.h                                                   *
 * PROGRAMMER:  Don Ward                                                  *
 * DESCRIPTION: Capacitive Touchpanel Driver Library header               *
 **************************************************************************/
#ifndef __K2LIB_H__
#define __K2LIB_H__

#include <stdint.h>
#include <linux/sigtiff.h>
#include <linux/input.h>
#include "cap_touch.h"

	/*==========================================*
	*         I N T R O D U C T I O N          *
	*==========================================*/

	/*==========================================*
	*           D E F I N I T I O N S          *
	*==========================================*/
	
// Useful attribute abbreviations
#define PACKED	__attribute__((packed))

// STM and Bootloader Addresses and Sizes
#define	STM_RADDR	0x00A0	// RAM
#define STM_RSIZE	0x0130
#define STM_EADDR	0x4000	// EEPROM
#define STM_ESIZE	0x0400
#define STM_OADDR	0x4800	// Options Area
#define STM_OSIZE	0x0080
#define	STM_PADDR	0x8000	// Program Flash
#define STM_PSIZE	0x8000

#define msleep(ms) usleep((ms) * 1000)

// Do sprintf after creating a correctly sized string variable
#define sprintf_create(name,fmt,args...) \
    char name[snprintf(NULL,0,fmt,args) + 1]; \
    sprintf(name,fmt,args)

#ifndef __KERNEL__

// Ported from kernel as #defines only available in kernel
/*
 * min()/max() macros that also do
 * strict type-checking.. See the
 * "unnecessary" pointer comparison.
 */
#define min(x,y) ({ \
	typeof(x) _x = (x);	\
	typeof(y) _y = (y);	\
	(void) (&_x == &_y);		\
	_x < _y ? _x : _y; })

#define max(x,y) ({ \
	typeof(x) _x = (x);	\
	typeof(y) _y = (y);	\
	(void) (&_x == &_y);		\
	_x > _y ? _x : _y; })
	
extern unsigned int Firmware_Info;
extern struct touchpanel_info TouchpanelInfo;

#endif	

	  /*==========================================*
	  *         P U B L I C    T Y P E S         *
	  *==========================================*/

	// Structure used by the STM_GET command
struct stm_get_struct
{
    u_char number_of_commands;
    u_char bootloader_version;
    u_char get_command_id;
    u_char read_command_id;
    u_char go_command_id;
    u_char write_command_id;
    u_char erase_command_id;
};

struct XYZCoefficients
{
    // This struct reflects correction table layout layout in K2 EEPROM
    unsigned char XInterpolation[K2_CORRECTION_DATA_SIZE];
    unsigned char YInterpolation[K2_CORRECTION_DATA_SIZE];
    unsigned char ZCorrection[K2_CORRECTION_DATA_SIZE];
    #define CRC_OFFSET (K2_CORRECTION_DATA_SIZE-2)
    
    // Entries below here are not sent to K2 chip
    short XPixels; // X pixel count
    short YPixels; // Y pixel count
    unsigned long Mfr; // Manufacturer code bits
    unsigned long Type; // Panel type code
    long Date; // Date table plotted / received from Cirque, YYYYMMDD eg. 20120202 (00 for unknown fields, eg. 20101000)
};

typedef short cal_data_t[K2_CAL_DATA_SIZE/2];

	/*==========================================*
	*           P U B L I C   D A T A          *
	*==========================================*/

	/*==========================================*
	*       P U B L I C  F U N C T I O N S     *
	*==========================================*/

char * K2error(int err);

int K2_ReadRegister(unsigned char Register);
int K2_WriteRegister(unsigned char Register,unsigned char Value);
int K2_WriteVerifyRegister(unsigned char Register,unsigned char Value);
int K2_ModifyRegister(unsigned char Register,unsigned char ClrBits,unsigned char SetBits);
int K2_ModifyVerifyRegister(unsigned char Register,unsigned char ClrBits,unsigned char SetBits);
int K2_WaitRegisterBits(unsigned char Register, unsigned char BitMask, unsigned char BitsDesired, int PollingMillisec, int TimeoutMillisec);
int K2_Read_All_Regs(u_char *values);
int K2_WriteRead(const u_char *tx_buff,u_char *rx_buff,int count);
int K2_WriteReadByte(unsigned char txVal);
int K2_Write_Regs(u_char start,u_char *values,u_char count);
int K2_Check_Calibration_Regs(void);
int K2_Measure_Noise(void);
int K2_Factory_Calibration(int display_on_before,int display_off_after,int clear_enforce);
int K2_Copy_Factory_Calibration(void);
int K2_BIST(void);
int K2_Write_Still_Comp_Timer(u_char *value);
int K2_Read_Still_Comp_Timer(u_char *value);
int K2_Write_Still_Comp_Max_Diff(u_char *value);
int K2_Read_Still_Comp_Max_Diff(u_char *value);
int K2_Calibration(int display_on_before,int display_off_after);
int K2_Check_For_Objects_On_Panel();
int K2_EEPROM_Data_RW(u_char command,void *buff,int start,int size);
int K2_Read_Correction_Table_Crcs(u_short *XCrc,u_short *YCrc,u_short *ZCrc);
int K2_Write_Correction_Table_Crcs(u_short *XCrc,u_short *YCrc,u_short *ZCrc);
int K2_Force_Correction_Tables(int table_index);
int K2_Establish_Correction_Tables(int force);
int K2_Hard_Reset(int reset);
int K2_Init(int finger_pen_setup);
int K2_Diag_Lib(long arg);
int K2_Diagnostics(struct diagnostics* diags);
int K2_Get_XYZ(xyz_t *xyz);
int K2_Get_Panel_Info(struct touchpanel_info *TouchpanelInfo);
int K2_Set_Panel_Info(struct touchpanel_info *TouchpanelInfo);
int K2_Cib_Panel_Info(struct touchpanel_info *TouchpanelInfo);
int K2_Smoothing_Filter(unsigned char length);
int K2_RW_Touch_Param(enum touch_param_mode mode, enum touch_param_action action, enum touch_param_item item, int *value);
int K2_XY_Correction(int enable);
int K2_Z_Correction(int enable);
int K2_Pressure_Sensor(int enable);
int K2_Get_Firmware_Info(void);
int K2_Write_Setup_Mode(u_char *value);
int K2_Read_Setup_Mode(u_char *value);
int K2_Write_Rext_Slot(u_char *rext_slot);
int K2_Read_Rext_Slot(u_char *rext_slot);
int K2_Write_Aperture_Width(u_char *aperture_width);
int K2_Read_Aperture_Width(u_char *aperture_width);
int K2_Write_Finger_Aperture_Width(u_char *value);
int K2_Read_Finger_Aperture_Width(u_char *value);
int K2_Write_Sample_Length(u_char *sample_length);
int K2_Read_Sample_Length(u_char *sample_length);
int K2_Write_Pen_Gain(u_char *pen_gain);
int K2_Read_Pen_Gain(u_char *pen_gain);
int K2_Write_Finger_Gain(u_char *value);
int K2_Read_Finger_Gain(u_char *value);
int K2_Write_Max_Comp_Diff(u_short *max_comp_diff);
int K2_Read_Max_Comp_Diff(u_short *max_comp_diff);
int K2_Write_Sync_Cmd_Complete_With_Hw_Dr(u_char *enable);
int K2_Read_Sync_Cmd_Complete_With_Hw_Dr(u_char *enable);

int K2_Get_Z_Info(struct z_info *pz_info);
int K2_Set_Z_Info(struct z_info *pz_info);

int K2_Checksum(int *csum);
int K2_Virgin(void);
int K2_Is_Panel_Missing(void);
void K2_Restart(int display_on_before,int display_off_after);
void K2_Print_Panel_Info(void);
void K2_Print_Calibration_Regs(void);
int K2_Shutdown(void);
int K2_Wakeup(void);
int K2_Suspended(int suspended);

int K2_Z_Value(void);
int K2_Update_Z_And_Scaling_Info(void);
int K2_ClearStatusRegister(void);
int K2_ClearCommandComplete(void);
int K2_WaitCommandComplete(int TimeoutMillisec);
int K2_Printk(char *msg);
int K2_GetDataReady(void);
int K2_WaitDataReady(int millisec);

int touchCmd(int cmd, int value);
int touchCompNSave(void);
int touchSigCap(void *data,long size);
int touchGetStylusId(void);
int touchStylusOnly(int value);
int touchPalmReject(int value);
int touchGrounded(int value);
int touchControlHotspots(int mode);
int touchSetHotspots(hotspot_table_t *hotspot_table);
int touchGetHotspots(hotspot_table_t *hotspot_table);
int touchReadHotspotData(hotspot_event_t *buffer, int count, int nonblocking);

int touchGetInputAbsInfo(int axis, struct input_absinfo *pinput_absinfo);
int touchSetInputAbsInfo(int axis, struct input_absinfo *pinput_absinfo);

int touchSetUserInput(int typ, int code, int value);
int touchSendUserXYZ(int x, int y, int z);

#ifdef RAPTOR
int touchSetDoubleTapTimeout(int TimeoutMillisec);
#endif

int K2_Write_OEM_Data(void *data, int offset, int size);
int K2_Read_OEM_Data(void *data, int offset, int size);

int K2_Read_Image_Tracking_Minima(unsigned long long *tracking_minima);
int K2_Write_Image_Tracking_Minima(unsigned long long *tracking_minima);

int K2_Read_Cal_Data(u_char command, cal_data_t *data, int size);

int K2_Log_Data(const char *fname, const char *mode, const char *caller, int select_mask);
// Bit mask values for "select_mask" parameter above
#define LOG_FACTORY_FINGER 0x01
#define LOG_CURRENT_FINGER 0x02
#define LOG_FACTORY_PEN    0x04
#define LOG_CURRENT_PEN    0x08
#define LOG_REGISTERS	   0x10

/* These are offsets into the K2 OEM Data area for storing tweaks to tracking parameters
 * Order seems a bit strange (X,Y,Y,X,Y,X,Y,X) but this is how Cirque did it
 */
enum tracking_minimum_offsets 
{
    PEN_NARROW_X = 0,
    PEN_NARROW_Y = 1,
    PEN_WIDE_Y = 2,
    PEN_WIDE_X = 3,
    FINGER_NARROW_Y = 4,
    FINGER_NARROW_X = 5,
    FINGER_WIDE_Y = 6,
    FINGER_WIDE_X = 7,
};
int K2_Write_Image_Tracking_Minimum_Byte(u_char tracking_minimum, enum tracking_minimum_offsets offset);

int K2_Read_Electrode_Status(unsigned long long *electrode_status);
#define ELECTRODE_STATUS_MASK		0x00C0000000000000

#ifdef RAPTOR
int touchSetKpHotspots(struct touch_hs_s *hotspot_struct);
#endif

int touchPreambleState(int value);
#ifndef RAPTOR
int touchPreambleMode(int value);
#endif
int touchK2ParamsChanged(int arg);
int touchManualCalNeeded(int arg);
int touchSyncCCWithHwDr(int arg);
int touchLCD(int on);

int touchReadDevmem(unsigned long physical, unsigned long *value);
int touchWriteDevmem(unsigned long physical, unsigned long value);

// Old non-thread-safe functions
int BresenhamInit(void (*draw)(int x, int y),SigCapOptions_t *options);
void Bresenham(int X, int Y);
void BresenhamFree(void);

// Structure used in Bresenham joining and thickening algorithms
#define NUM_THICKENING_SLOTS (THICKENING_MAX * THICKENING_MAX)

#define NUM_CACHE_SLOTS 4096

// New thread-safe functions

struct Bres
{
    void (*draw)(int x, int y);
    int join;
    struct dxy_nibble
    {
	signed char dx : 4;
	signed char dy : 4;
    } PACKED thickening_list[NUM_THICKENING_SLOTS]; // List of relative pixels for use in thickening
    int thickening_count;
    int thickening_calls;
    xy_t cache[NUM_CACHE_SLOTS]; // Cache for duplicate point reduction
    xy_long_t curr; // Current point
    int total_points, drawn_points, collision_points; // Statistics
};

int BresenhamInit_r(struct Bres **ppBres, void (*draw)(int x, int y),SigCapOptions_t *options);
void Bresenham_r(struct Bres *pBres, int X, int Y);
void BresenhamFree_r(struct Bres **ppBres);

int touchDoScalingCalculation(struct cal_pair *cal_pairs, int num_pairs, struct scaling_const_float *pkonst);
int touchDoScalingCalibration(struct cal_pair *cal_pairs, int num_pairs, struct scaling_const_float *pkonst,
			      void (*proximity_callback)(int index), int proximity_divisor, int (*completion_callback)(void));
int touchGetScalingCalibrationCount(void);
int touchGetFirmwareInfoStr(char *str);

// #ifndef RAPTOR
int touchResync(int key_or_button);
// #endif

int STM_BootLoader(u_char *version);
int STM_GetCmd(struct stm_get_struct *get_struct);
int STM_ReadCmd(int address,u_char *buff,int count,int *num_read);
int STM_WriteCmd(int address,u_char *buff,int count,int *num_written);
int STM_GoCmd(u_long address);
int STM_LoadEraseWriteRoutines(u_char bl_version);
int STM_Program(char *pfname);

#ifndef RAPTOR

int I2C_Xfer(struct i2c_params *params);

int EGX_GetDataReady(void);
int EGX_WaitDataReady(int millisec);
int EGX_I2C_Wakeup(void *respData);
int EGX_I2C_Clk_Stretch(long timeout_ms);
int EGX_I2C_Command(void *cmdData, enum egx_cmdTweaks cmdTweaks, int respTimeout, void *respData);

#define EGX_Command_FU(tweaks,timeout,data0,data1etc...) ({ u_char cmd[] = { 0x03, sizeof((u_char[]){data0,##data1etc}), data0, ##data1etc }; EGX_I2C_Command(cmd,tweaks,timeout,EGX_Resp_Data); })
#define EGX_Command_3(data0,data1etc...) ({ u_char cmd[] = { 0x03, sizeof((u_char[]){data0,##data1etc}), data0, ##data1etc }; EGX_I2C_Command(cmd,0,1000,EGX_Resp_Data); })
#define EGX_Command_A(data0,data1etc...) ({ u_char cmd[] = { 0x0A, sizeof((u_char[]){data0,##data1etc}), data0, ##data1etc }; EGX_I2C_Command(cmd,0,1000,EGX_Resp_Data); })

int Special_I2C_Xfer(int reg, int flags, void *data, int len);
int Special_I2C_Write_Regs(int reg, void *data, int len);
int Special_I2C_Read_Regs(int reg, void *data, int len);
int Special_I2C_Write_Reg(int reg, u_char data);
int Special_I2C_Read_Reg(int reg);

#endif

int touchSendDebugData(long debug_data);
int touchMiscDebug(u_long arg);
int enableSerial(int port);

/* Next sections are for accessing mxt_objects in Atmel touch chips */

/* ID Information fields in the Information Block*/
struct mxt_id_info {
  uint8_t family;           /* Device family */
  uint8_t variant;          /* Device variant */

  uint8_t version_minor: 4; /* Firmware version Minor nibble */
  uint8_t version_major: 4; /* Firmware version Minor nibble */
  
  uint8_t build;            /* Firmware build number */

  uint8_t matrix_x_size;    /* Matrix X Size */
  uint8_t matrix_y_size;    /* Matrix Y Size */

  /* Number of elements in the object table. The actual number of objects
   * can be different if any object has more than one instance. */
  uint8_t num_objects;
} PACKED;

/* Checksum element struct at end of information clock */
struct mxt_raw_crc {
  uint32_t CRC : 24; 	    /* CRC field */
} PACKED;

/* Object table element struct (expanded Atmel format) */
struct mxt_object {
  uint16_t type;		 /* Object type ID */
  uint16_t start_pos;		 /* Start address of the obj config structure */
  uint8_t size_minus_one;        /* Byte length of the obj config structure - 1 */
  uint8_t instances_minus_one;   /* Number of objects of this obj type - 1 */
  uint8_t num_report_ids;        /* The max number of touches in a screen,
                                  *  max number of sliders in a slider array, etc.*/
} PACKED;
#define SIZE_OF_MXT_OBJECT_8BIT (sizeof(struct mxt_object) - 1)

// For Atmel chip T-object access
enum mxt_object_action
{
    MXT_OBJECT_GET_SIZE,
    MXT_OBJECT_GET_INSTANCES,
    MXT_OBJECT_DATA_GET_OFFSET,
    MXT_OBJECT_DATA_READ,
    MXT_OBJECT_DATA_WRITE,
};

int mxt_object_operation(enum mxt_object_action action, u_short type, u_char instance, int start, int count, void *data);

// Some Needed Atmel Touch Objects and Offsets
#define CONFIG_OBJECT_T0			0
#define MXT_T0_OBJCNT_OFFSET		6

#define GEN_MESSAGEPROCESSOR_T5		5

#define GEN_COMMANDPROCESSOR_T6		6

#define MXT_COMMAND_RESET		0
#define MXT_BOOT_VALUE		0xa5
#define MXT_RESET_VALUE		0x01

#define MXT_COMMAND_BACKUPNV		1
#define MXT_BACKUP_VALUE	0x55

#define MXT_COMMAND_CALIBRATE		2
#define MXT_COMMAND_REPORTALL		3
#define MXT_COMMAND_DIAGNOSTIC		5

#define GEN_POWERCONFIG_T7			7
#define MXT_T7_IDLEACQINT_OFFSET	0
#define MXT_T7_ACTVACQINT_OFFSET	1

#define GEN_ACQUISITIONCONFIG_T8		8

#define SPT_USERDATA_T38	       		38
#define MXT_T38_CONFIGVER_OFFSET 	0
#define MXT_T38_REPORT_T72_OFFSET	4
#define MXT_T38_REPORT_T6_OFFSET	11

#define PROCI_TOUCHSUPPRESSION_T42		42

#define SPT_DYNAMICCONFIGURATIONCONTROLLER_T70	70

#define PROCG_NOISESUPPRESSION_T72		72
#define MXT_T72_CTRL_OFFSET		0
#define	MXT_T72_CTRL_RPTEN	0x02

#define TOUCH_MULTITOUCHSCREEN_T100		100

#define GEN_INFOBLOCK16BIT_T254			254

struct mxt_config
{
    int type;
    int instance;
    int start;
    int count;
    u_char *data;
};

#define MXT_CONFIG(type, instance, start, data0, data1etc... ) \
{ type, instance, start, sizeof((u_char[]) { data0, ##data1etc }), (u_char[]){ data0, ##data1etc } }

struct mxt_config_table
{
    int count;
    struct mxt_config *config;
};

#define MXT_CONFIG_TABLE(config) { ARRAY_SIZE(config), config }

int mxt_config_update(struct mxt_config_table *mxt_config_table);

#endif /*#ifndef __K2LIB_H__*/
