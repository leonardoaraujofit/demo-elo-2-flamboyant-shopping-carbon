#ifndef __SYSMODE_MENU_ACL__
#define __SYSMODE_MENU_ACL__

#include <sysmode_ui/sysm_ui.h>
#include <sysmode_ui/sysm_lang.h>

void init_main_menu ( ) ;
char* update_menu_title ( const char* title ) ;
void prompt_main_menu();
void prompt_manufacturing_menu();
SYSM_UI::t_ui_menu_list *get_main_menu();

#endif // __SYSMODE_MENU_ACL__
