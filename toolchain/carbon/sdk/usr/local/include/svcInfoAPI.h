/*
 *	COPYRIGHT (C) 2007 by VeriFone, Inc.  All rights reserved.
 *
 *	Filename:			svcInfoAPI.h
 *	Module Description:	Headers for Info and ADC related SVC functions are here
 *	Component of:		Spectrum
 *	Authors(s):			Winston_L1
 *	Function:
 *
 *	Modification History: (use if not filled in by source control)
 *
 */

/** @addtogroup vfisvc Services */
/** @{ */
/** @addtogroup svcinfo Services : System information */
/** @{ */
/** 
 *  @file svcInfoAPI.h 
 *
 *	@brief Services for retrieving system information
 *
 * 
 */

#ifndef _SVCINFOAPI_
#define _SVCINFOAPI_

#include <sys/time.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @name ADC Channels
*/
/** \{ */ 
#define ADC_CHAN_BAT_VOLT		0
#define ADC_CHAN_MOD_ID		    3
#define ADC_CHAN_PCB_ID			4
#define ADC_CHAN_MPC_ID         5
#define ADC_CHAN_RNG			6
#define ADC_CHAN_CABLE			7
/** \} */

/**
 * @name Good battery threshhold
*/
/** \{ */ 
#define GOOD_BATTERY_THRESHOLD	25		/**< 2.5 V */
/** \} */

/**
 * @name Terminal feature definitions used by svcInfoPlatform
*/
/** \{ */ 
#define FEATURE_UNIT_CONFIG			0 /**< Unit configuration */
#define FEATURE_BATTERY_STATUS		1 /**< Lithium battery status */
#define FEATURE_BATTERY_VOLTAGE		2 /**< Lithium battery voltage */
#define FEATURE_SDRAM_SIZE			3 /**< SDRAM size in KB */
#define FEATURE_MPCABLE_STATUS		4 /**< Multi-port cable status */
#define FEATURE_MODULE_CONFIG		5 /**< I/O module configuration */
#define FEATURE_SMARTCARD_VER		6 /**< Smartcard version */
#define FEATURE_MODEL				7 /**< Terminal model */
#define FEATURE_PROCESSOR_MHZ		8 /**< CPU speed */
#define FEATURE_TOUCHPANEL_PINNACLE	9 /**< N/A */
/** \} */

/**
 * @name I/O module types
*/
/** \{ */ 
#define MODULE_NONE		    0x00 // no bits set
#define MODULE_RFID		    0x01 // bit 0 set
#define MODULE_WIFI		    0x02 // bit 1 set
#define MODULE_RFID_WIFI	0x03 // bit 1 & 0 set

#define MODULE_UNKNOWN		0x40
#define MODULE_O7XXX		0x80
/** \} */

/**
 * @name Bitmasks used by getUnitConfig 
 *
 * Bits 0-15 are for onboard hardware
 * configurations.  bits 16-30 are for
 * plug-in configurations.  bit 31 is
 * reserved and must always be 0. These
 * values are passed via an unsigned int,
 * therefore requiring an unsigned int
 * definition to be at least 4 bytes.
/** \{ */ 
#define UNIT_CONFIG_ETHERNET		0x00000001
#define UNIT_CONFIG_SMARTCARD		0x00000002
#define UNIT_CONFIG_AUDIO			0x00000004
#define UNIT_CONFIG_BEEPER			0x00000008
#define UNIT_CONFIG_TOUCHPANEL		0x00000010
#define UNIT_CONFIG_TURBO			0x00000020
#define UNIT_CONFIG_PINNACLE		0x00000040
#define UNIT_CONFIG_MERCURY			0x00000080
#define UNIT_CONFIG_PINNACLESSNA	0x00000100
#define UNIT_CONFIG_SPECTRUMPLUS	0x00000200

#define UNIT_CONFIG_USBETHER_IN		0x00010000
#define UNIT_CONFIG_USBWIFI_IN		0x00020000
/** \} */

/**
 * @name Unit config option masks
*/
/** \{ */ 
#define UNIT_CONFIG_POP_OPTIONS		0x0000FFFF
#define UNIT_CONFIG_PLUGIN_OPTIONS	0x7FFF0000
#define UNIT_CONFIG_BIT31_MASK		0x7FFFFFFF
/** \} */

/**
 * @name getSmartCardVersion version definitions
*/
/** \{ */ 
#define SMARTCARD_VERSION1		0x0001
#define SMARTCARD_VERSION2		0x0002
/** \} */

/**
 * @name getMPCType defines
*/
/** \{ */ 
#define MPC_TYPE_SPECTRUM		0x01
#define MPC_USB_DEVICE			0x02
#define MPC_COM1_RS485			0x04
#define MPC_COM3_TAILGATE		0x08
#define MPC_COM1_ATTACHED		0x10
#define MPC_COM2_ATTACHED		0x20

#define MPC_NO_ETHERNET			0x100
#define MPC_NO_USB				0x200
#define MPC_NO_COM1				0x400
#define MPC_NO_COM2				0x800
#define MPC_NO_COM3				0x1000
#define MPC_CUSTOM				0x8000
/** \} */

/**
 * @name Terminal model values
*/
/** \{ */ 
#define Model_Mx925			925   /**< Razor Mx2 */
#define Model_Mx915			915   /**<  miniRazor Mx2 */
#define Model_Mx880			880
#define Model_Mx870			870
#define Model_Mx860			860
#define Model_Mx850			850
#define Model_Mx830			830
#define Model_Mx760			760
/** \} */

/**
 * @name Mx860 / Mx850 / Mx830 Keypad Modes
*/
/** \{ */ 
#define NUMERIC				0	// Default
#define ALPHA_NUMERIC			1
/** \} */

/**
 * @name Mx9XX (915, 925) Specific
*/
/** \{ */ 
#define Mx9XX_SPEED				400
#define Mx9XX_SMARTCARD_VERSION		0x04
/** \} */


/**
 * @name offset for RFS version number eg. RFS00015 => 015
*/
/** \{ */ 
#define RFS_OFFSET_VERSION  5 /**< used with svcInfoRFS() */
/** \} */



/**
 Flash information
*/
struct flashInformation
{
	long total;
	long used;
	long free;
	long percentUsed;
};
	
// Methods to obtain version information about the OS components
#define MAX_SVCINFO_STATSNAME 80

/**
 Structure for storing statistics
*/
struct svcInfo_versionStats {
	char name[MAX_SVCINFO_STATSNAME];
	time_t timestamp;
};

/**
 Typedef used in functions returning statistics
*/
typedef struct svcInfo_versionStats versionStats_t;

/* Function prototypes */

/**
 * Function not available anymore
*/
int svcInfoKernel_stats( versionStats_t *stats );
/**
 * Function not available anymore
*/
int svcInfoAltKernel_stats( versionStats_t *stats );
/**
 * Function not available anymore
*/
int svcInfoRFS_stats( versionStats_t *stats );
/**
 * Function not available anymore
*/
int svcInfoToolchain_stats( versionStats_t *stats );
/**
 * Function not available anymore
*/
int svcInfoSDK_stats( versionStats_t *stats );

/**
 * Read the terminal serial # xxx-xxx-xxx
*/
void svcInfoSerialNum(char *buffer);
/**
 * Read EPROM version
*/
void svcInfoEprom(char *buffer);
/**
 * Read Root File System version
*/
void svcInfoRFS(char *buffer);
/**
 * Read unique terminal id
 * Default = 12000000
*/
void svcInfoPtid(char *buffer);
/**
 * Read ethernet MAC address
*/
void svcInfoMAC(char *buffer);
/**
 * Read type of card reader supported
 * feature must be set to 0
*/
int svcInfoCard(int feature);
/**
 * Read type of supported supported, based on feature
*/
int svcInfoDsp(int feature);
/**
 * Read type of supported keyboard, based on feature
*/
int svcInfoKey(int feature);
/**
 * Read platform info, specified by feature parameter
*/
int svcInfoPlatform(int feature);
/**
 * Read platform type, based on feature
*/
int svcInfoType(int feature);
/**
 * Read kernel details for both primary and secondary kernels
*/
void svcInfoKernels(char *buf,int length);
/**
 * Read flash file system status
*/
int svcInfoFlash(struct flashInformation *);
/**
 * Read hotplug status, 1 - Active, 0 - Not active
*/
int svcIsHotplugActive( void );

// Derived functions
/**
 * XXXX
*/
#define svcInfoKernel(buffer) svcInfoEprom(buffer)


#ifdef __cplusplus
}
#endif

#endif
