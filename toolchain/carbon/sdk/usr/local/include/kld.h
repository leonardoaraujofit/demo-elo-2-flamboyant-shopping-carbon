/** @addtogroup */
/** @{ */
/** @addtogroup */
/** @{ */
/** 
 *  @file kld.h 
 *
 *	@brief Keyload
 *
 * 
 */
 
#ifndef _KLD_H__
#define _KLD_H__

#ifdef __cplusplus
extern "C" {
#endif


/** 
 * Load KLD keys
 *
 * @param[in] comms_dev Comms file name
 * @param[in] *fp_msg Pointer to a file with KLD data
 *
 * @return 
 * @li 0 = Success
 * @li 148 = KLD in manufacturing mode
 * @li 117 = Failed to open comms
 * @li 118 = Failed to set comms
 * @li 115 = Session fail
 *
 */
/*KLD_PROTOTYPE*/ int KLD_load_keys(const char *comms_dev, FILE *fp_msg);



#ifdef __cplusplus
}
#endif

#endif
/// @} */
/// @} */
