/*
 * LIBMIB library
 * Filename: libmib.h
 * This library give access to device MIB (Manufacture information block)
 *
 */
#ifndef __LIBMIB_H__
#define __LIBMIB_H__

#include <stdio.h>
#include <verifone/mib.h>


// The defines and struct below are also used in the utility service for interfacing to here; Don't change...
#define FIFO_FILE 		"/tmp/mibmgrFIFO"
// MIBmanager status Types
#define MIB_STATUS_TYPE_INFO		1	/**< Information message in msg[], used to give feedback on progress */
#define MIB_STATUS_TYPE_ERROR		7	/**< Error message in msg[], indicates cause of error encountered */
#define MIB_STATUS_TYPE_FINISHED 	9	/**< Finished message, indicated MIBmanager process has finished/done */

#define MIBMSGBUFSIZE	140
// Returned status results via fifo
struct utilityRunMIBstatus {
	int status;		/**< Contains type of status message (see MIB_STATUS_TYPE_xxxx) */
	char msg[MIBMSGBUFSIZE];	/**< Contains message string (null terminated) */
};

int MIBManager_Store(char * MIBFilePath);
int MIBManager_ReadMIBBlock(unsigned char *MIB_block, int *calc_MIB_size,int MIBSize);
int MIBManager_ValidMIBExist();

int MIBManager_VerifyBurn(char* MIBFilePath);
int MIBManager_SetKLDDetamper(char* detamper_option);


void MIBManager_setMsgFifo(FILE *fifo);
void MIBManager_StatusMsg(char *format, ...);
void MIBManager_ErrorMsg(char *format, ...);
void MIBManager_DoneMsg(char *format, ...);

#endif //__LIBMIB_H__
