/*
 *	Copyright, 2016 VeriFone Inc.
 *	88 West Plumeria Dr.
 *	San Jose, CA.  95134
 *
 *  All Rights Reserved. No part of this software may be reproduced,
 *  transmitted, transcribed, stored in a retrieval system, or
 *  translated into any language or computer language, in any form
 *  or by any means electronic, mechanical, magnetic, optical,
 *  chemical, manual or otherwise, without the prior permission of
 *  VeriFone Inc.
 */

#ifndef __SYSM_CPTEST_CONV__
#define __SYSM_CPTEST_CONV__

#include <sysmode_ui/sysm_ui.h>
#include <sysmode_ui/sysm_lang.h>

void cptest_convert_cb(const SYSM_UI::t_ui_menu_entry *);

#endif //__SYSM_CPTEST_CONV__
