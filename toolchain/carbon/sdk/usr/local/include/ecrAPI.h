/***********************************************************************
*
*		Copyright, 2004 VeriFone Inc.
*		2099 Gateway Place, Suite 600
*		San Jose, CA.  95110
*
*  All Rights Reserved. No part of this software may be reproduced,
*  transmitted, transcribed, stored in a retrieval system, or
*  translated into any language or computer language, in any form
*  or by any means electronic, mechanical, magnetic, optical,
*  chemical, manual or otherwise, without the prior permission of
*  VeriFone Inc.
*
***********************************************************************/

/**
* @file ecrAPI.h
*
* @brief ecr API
*
*/


#ifndef ECRAPI
#define ECRAPI

#ifdef __cplusplus
extern "C" {
#endif
 
/**
 * @name Possible values for slots
 */
/** \{ */
#define	SLOT_64			"2A25"
#define	SLOT_65			"2B25"
#define	SLOT_68			"2A23"
#define	SLOT_69			"2B23"
#define	SLOT_AUTO		"AUTO"
#define	SLOT_R232			"R232"
/** \} */


/**
 * @name Possible values for polling
*/
/** \{ */
#define ECR_POLL_64   (unsigned char)0x64
#define ECR_POLL_65   (unsigned char)0x65
#define ECR_POLL_68   (unsigned char)0x68
#define ECR_POLL_69   (unsigned char)0x69
/** \} */


/* Uncomment the #define below when building this    *
 * library for OS versions < RFS16SP1 (SDK's < 2.02) */
/*#define PRE_RFS16SP1*/

/* Prototypes */

/**
 * Downloads the app or OS (in uncompressed or compressed ECR format) from the ECR.
 *
 * @brief  Downloads the app or OS from the ECR.
 * 
 * Downloads the app or OS (in uncompressed or compressed ECR format) from the
 * ECR. The ECR download functionality operates as a separate thread as its own
 * process after calling ecrDownload(). ecrDownload() immediately returns
 * after the process starts so that the application can perform other tasks and
 * System Mode is still functional.
 * This function can expand any compressed Linux tar file using the PCLANCNV
 * utility. The file is placed in the users home directory. Permissions and ownership
 * are changed for every file and directory contained within the archive not owned by
 * root – userid 0.
 * 
 * @param[in] version                Contains the application and parameter versions in
 *                                   the 8-character format (4 for the application 
 *                                   and 4 for the parameter) AAAAPPPP.
 *                                   The OS uses version information to set the O4683
 *                                   configuration variable on a successful download. A
 *                                   TO4684 variable is created at the beginning of the
 *                                   download. The TO4683 variable is deleted on success.
 *                                   If the application passes a null or null-terminated string
 *                                   for version, the OS waits for an ONLINE message from
 *                                   the ECR before starting the download.
 * @param[in] (*dspCallback)(char *) Pointer to a callback function used to display data
 *                                   on the terminal screen.The application passes a callback
 *                                   function pointer to handle data display.
 * @param[in] (*ecrDnldEnd)(int)     Application’s callback pointer. The application must pass
 *                                   a valid callback function pointer to call when the
 *                                   download ends. This is the only way the application is
 *                                   notified of a successful or unsuccessful download.
 *
 * @return
 * @li   =0    - Download started successfully.
 * @li   -1    - The download process failed to start.
 * @li -EBUSY  - An ECR download process is already running.
 * @li -EINVAL - Invalid ECR Download End callback specified.
 * 
 * <b>Callback Return Values</b>:
 * @li =1 Download completed successfully.
 * @li =0 Never received an Online packet from the ECR or abort download.
 * @li -1 Download type not recognized.
 * @li -2 Expansion of archived/expressed file, failed.
 */
short ecrDownload(char *version, void (*dspCallback) (char *), void (*ecrDnldEnd) (int));

/**
 * Cancels the current download.
 * @return
 * @li =0     - Success.
 * @li –ESRCH - Download not currently running (no such process).
 */
int ecrDnldCancel(void);

/**
 * Opens the ECR port and initializes it as set in the I4683 variable.
 * 
 * @param[in] visa_sel Non-zero = the port is configured for VISA 2 processing.
 * @param[in] port_name If specified, it overrides the L4683 environment variable. 
 *                      If not specified, pass a null-terminated string ("") 
 *                      and the value in L4683 is used.
 * 
 * @return
 * @li -0 I4683 environment variable not found.            erno  = -EINVAL
 * @li -1 Device driver not installed                      errno = -ENXIO
 * @li -2 L4683 environment variable not found.            erno  = -EINVAL
 * @li -3 Invalid port number in L4683.                    erno  = -EINVAL
 * @li -4 Invalid port name argument.                      erno  = -EINVAL
 * @li -5 Invalid L4683 environment variable length.       erno  = -EINVAL
 * @li -6 Invalid I4683 slot specified or not initialized. erno  = -EBADSLT
 * @li -7 Visa protocol not started.                       erno  = -EPROT
 * @li -8 Initialization failed of Polling semaphore.      erno  = -EPERM
 */
short ecrOpen(char *, short);

/**
 * Transfers data from the port to the buffer, and returns the number of bytes actually 
 * read or a negative value on error.
 *
 * @param[in] size   The maximum number of bytes to read.
 * @param[in] buffer Pointer to the data area.
 *
 */
short ecrRead(char *, short);

/**
 * Transfers rejected data from ecrWrite() to the buffer, and return the number of
 * bytes actually read or zero if no data is available.
 *
 * @param[in] buffer Pointer to the data area.
 * @param[in] size   The maximum number of bytes to read.
 *
 */
short ecrReadReject(char *, short);

/**
 * @brief Transfers data from an application buffer into the driver’s buffer, 
 *         only if there is adequate buffer space.
 * 
 * Transfers data from an application buffer into the driver’s buffer, 
 * only if there is adequate buffer space. Control immediately returns to the application. 
 * The actual transmission is delayed. Status is monitored using ecrStatus() 
 * (in VISA2 mode). 
 * 
 * Returns FAILURE if the current message is pending or rejected.
 * 
 * @param[in] buffer Pointer to the data area.
 * @param[in] size   Number of bytes to write.
 *
 * @return
 * @li -1 With  errno set to specific error.
 * @li -ENOBUF  Message too big or driver busy sending last message.
 * @li -EBUSY   A rejected message must be read before sending another 
 *              message. Use: ecrReadReject().
 *
 */
short ecrWrite(char *, short);

/**
 *
 */
short ecrWriteNoACKExpected(char *, short);

/**
 * Cancels the write in progress.
 *
 * @return
 * On success all ECR transmit and retransmit flags are reset.
 * @li >= 0  Number of bytes left in transmit buffer.
 * @li  < 0  Error
 *
 */
short ecrWriteCancel(void);

/**
 * Copies current status information to caller’s 4-integer buffer. The information
 * returned varies if the port is in Tailgate or Feature C mode and if VISA is enabled.
 *
 * @li 1st Input messages pending
 *     * 0   = no message pending.
 *     * > 0 = message pending.
 * @li 2nd Number of bytes available (free) in the output buffer.
 * @li 3rd ECR online.
 *     Non-zero = ECR online (default is 0x37).
       * 0 = ECR is offline.
 * @li 4th Status of ecrWrite() message:
 *     * 0 = message ACKed
 *     * 1 = message pending
 *     * 2 = message rejected
 *
 * The result return code is a pointer to vficomErrCounts structure defined as:
 * 
 * @code
 * struct vficomErrCounts {
 * int frame_err;
 * int over_err;
 * int parity_err;
 * };
 * @endcode
 * 
 * The pointer to this structure contains the error counts for the port described by the
 * file descriptor passed into the function. The error counts are for framing, overrun,
 * and parity errors detected for that port. The counts are always the total errors
 * counted for that port since opened, which allows the application to track if any n
 *
 * <b>Prototype for Tailgate mode and VISA not enabled</b>
 *
 * struct vficomErrCounts *result = ecrStatus(int*buf);
 *
 * Copies current status information to caller’s 4-byte buffer.
 * @li 1st Number of bytes pending in input buffer.
 * @li 2nd Number of bytes available (free) in the output buffer.
 * @li 3rd ECR is online.
 *         Non-zero value = ECR online (default is 0x37).
 *     * 0 = ECR offline.
 * 
 * @return
 * The return code is a pointer to vficomErrCounts structure defined as:
 * @code
 * struct vficomErrCounts {
 * int frame_err;
 * int over_err;
 * int parity_err;
 * };
 * @endcode
 * The pointer to this structure contains the error counts for the port described by the
 * file descriptor passed to the function. The error counts are for framing, overrun,
 * and parity errors detected for that port. The counts are always the total errors
 * counted for that port since opened, which allows the application to track new
 * detected errors for that port since the last time this function was called. * 
 * 
 * <b>Prototype for Feature C mode and VISA enabled</b>
 *
 * struct vficomErrCounts *result=ecrStatus(int*buf);
 *
 * Copy current status information to caller’s 4-integer buffer.
 * @li 1st Input messages pending.
 *     * 0 = No pending message.
 *     * > 0 means = Message pending.
 * @li 2nd Number of bytes available (free) in the output buffer.
 * @li 3rd Current signal information for the port. All bits for the
 * corresponding signals are set if the hardware supports signalling
 * for that port and the signal is detected to be asserted. Otherwise
 * this bit is not set.
 *     * 0x80 DTR detected
 *     * 0x40 RTS detected
 *     * 0x20 CTS detected
 *     * 0x10 Ring indicator present
 *     * 0x08 Carrier Detect (CD)
 *     * 0x04 DSR detected
 *     * 0x02 Reserved
 *     * 0x01 Reserved
 * @li 4th Status of ecrWrite() message:
 *     * 0 = message ACKed
 *     * 1 = message pending
 *     * 2 = message rejected
 * @return
 * The return code is a pointer to the vficomErrCounts structure defined as:
 * @code
 * struct vficomErrCounts {
 * int frame_err;
 * int over_err;
 * int parity_err;
 * };
 * @encode
 * The pointer contains the error counts for the port described by the file descriptor
 * passed to the function. The error counts are for framing, overrun, and parity errors
 * detected for that port. The counts are always the total errors counted for that port
 * since opened, which allows the application to track new detected errors for that
 * port since the last time this function was called.
 *
 * <b>Prototype for Feature C mode and VISA enabled</b>
 * 
 * struct vficomErrCounts *result = ecrStatus(int*buf);
 * 
 * Copy current status information to caller’s 4-byte buffer.
 * @li 1st Number of bytes pending in the input buffer.
 * @li 2nd Number of free bytes available in the output buffer.
 * @li 3rd Current signal information for port. All bits for corresponding
 * signals are set if hardware supports signalling for that port and
 * the signal is detected to be asserted. Otherwise this bit is not set.
 *     * 0x80 DTR detected
 *     * 0x40 RTS detected
 *     * 0x20 CTS detected
 *     * 0x10 Ring indicator present
 *     * 0x08 Carrier Detect (CD) present
 *     * 0x04 DSR detected
 *     * 0x02 Reserved
 *     * 0x01 Reserved
 * @return
 * The return code is a pointer to vficomErrCounts structure defined as:
 * struct vficomErrCounts {
 * int frame_err;
 * int over_err;
 * int parity_err;
 * };
 * The pointer contains the error counts for the port described by the file descriptor
 * passed to the function. The error counts are for framing, overrun, and parity errors
 * detected for that port. The counts are always the total errors counted for that port
 * since opened, which allows the application to track new detected errors for that
 * port since the last time this function was called.
 */
struct vficomErrCounts *ecrStatus(unsigned int *);

/**
 * Closes the ECR port.
 *
 * @note
 * Closes the port to return to a known good state. Processes that open the port
 * must trap the necessary signals to clean up before calling ecrClose().
 *
 * @return
 * @li 0= Success
 *
 * Negative errno code on error.
 */
short ecrClose(void);

/**
 *
 */
short ecrWriteRetryTimeout(char *, short, short, short);

/**
 * @brief  “Bubbles up” specific tailgate commands, and passes a function pointer to a
 * tailgate command handler routine as the parameter.
 * 
 * “Bubbles up” specific tailgate commands, and passes a function pointer to a
 * tailgate command handler routine as the parameter. After this call is made, the
 * data packets received for the following three commands are passed to the
 * callback function (bytes in parentheses).
 * @li <b>SETUP</b>: Device Command, 7 bytes in the format: 0x01(1), Baud rate(1),
 * Channel Configuration (1), Channel Status(1), Timeout(3)
 * @li <b>CHANNEL CONTROL</b>: Device Command, 2 bytes in the format: 0x02(1),
 * Channel Status(1)
 * @li <b>RESET</b>: System Command, 2 bytes in the format: 0x00(1), 0x40(1)
 * 
 * Refer to the Tailgate specification for details.
 *
 * Command data passes to the first parameter of the callback as an array type
 * char. The length of the array is passed as the second parameter.
 * 
 * The ECR driver has a FIFO command queue capable of a maximum of 16
 * commands. If commands are not processed, the queue fills with new commands
 * added to the end of the queue, shifting all queue data down and losing the oldest
 * command entry.
 * Call ecrTailgateCommands() any time. Pass it as either
 * @li  A pointer to a function of the correct prototype, or
 * @li  Null
 * 
 * When a function pointer is passed, the command queue clears and new
 * commands are delivered to the callback. You can call
 * ecrTailgateCommands() and pass the same currently active functio
 *
 * @return
 * @li =0 No errors.
 * 
 * A negative errno value indicates a specific error.

 * @note
 * @li 1 Because commands can be lost between when opening the ECR port and
 * calling ecrTailgateCommands() (remember it clears the queue), call
 * ecrTailgateCommands() before opening the ECR port.
 * @li 2 Do not spend too much time in the callback since the queue holds a
 * maximum of 16 commands. 
 */
short ecrTailgateCommands(void(*cmdsCallback)(char *, int));

/* General COM Port/Device Service Functions */

/**
 * @brief Specify a function that an application wants called when data is received on a
 * particular device designated by the file descriptor parameter.
 * 
 * Specify a function that an application wants called when data is received on a
 * particular device designated by the file descriptor parameter. When called it
 * checks to determine if the ECR is open.
 * If Visa mode is enabled, the callback function is called when a complete Visa
 * packet is available. If Visa mode is not enabled, the application's callback is called
 * when data I/O occurs on that device, such as a character is received on the open
 * port. This is also true if this function sets a callback function for any device (not
 * only when using ecrOpen).
 * 
 * @note  
 * Currently only one callback for one device is supported. Only the first device set
 * with this function triggers the callback.
 *
 * <b>DO NOT</b> use this function on multiple devices.
 *
 * If a non-polling implementation is required on multiple devices to indicate data is
 * received, use a blocking read() in a separate thread.
 * 
 * @param[in] fd              File descriptor returned when opening a device, with either
 *                            open() or ecrOpen().
 * @param[in] (*rxfp) (void)  Function pointer to the function to call when data is available.
 * 
 * @return
 * @li =0 Success
 * 
 * Negative errno value if unsuccessful.
 */
int svcSetRxCallback(int fd, void (*rxfp)(void));

/**
 * Must be called when the application no longer needs to know if data I/O occurred
 * on a particular device associated with the specified file descriptor (handle). The
 * file descriptor (fd) passed to this function must be the same as used to register a
 * callback function for the device with svcSetRxCallback().
 *
 * @param[in] fd File descriptor returned when opening a device, with either open()
 * or ecrOpen().
 *
 * @return
 * @li =0 Success
 *
 * Negative errno value if unsuccessful.
 */
short svcReleaseRxCallback(int fd);

/**
 * @brief Allows the application to check if the ECR address V/OS terminals is being polled
 *by the ECR.
 *
 * Allows the application to check if the ECR address V/OS terminals is being polled
 * by the ECR. Only valid in Tailgate mode with the terminal set to a particular
 * address. This function returns the number of polls that occurred since the last
 * request.
 *
 * The internal poll count only updates at most once per second. So, this function
 * returns the last value reported if called more than once in that interval.
 *
 * Use this function during a System Mode ECR download in Tailgate mode to check
 * if the terminal is being polled by the ECR. If so, POLLED displays on the terminal
 * screen.
 * 
 * @param[in] fd File descriptor returned when opening a serial port with open() or
 *               ecrOpen()
 */
int svcCom3Polled(int fd);

/**
 * @brief Allows the application to set the ECR device address the terminal responds to.
 *
 * Allows the application to set the ECR device address the terminal responds to.
 * Only valid in COM3 ECR tailgate mode MODE_SIO_INT. The ECR device address
 * must be set before the V/OS terminal can respond to a poll from the ECR. If the
 * terminal is not in MODE_SIO_INT, the device address is still set successfully, but
 * is ignored.
 *
 * @param[in] fd File descriptor returned when opening a serial port with ecrOpen().
 * @param[in] da A 1-byte value to set the ECR device address to. Valid values are:
 *
 *     @li SIO_DEVADDR_64
 *     @li SIO_DEVADDR_65
 *     @li SIO_DEVADDR_65
 *     @li SIO_DEVADDR_69
 *
 * @return
 * @li =0 Success
 *
 * Negative value if unsuccessful in setting address.
 */
short svcCom3SetDeviceAddr(int fd, char da);

/**
 * Sets the ECR engineering change level reported to the ECR in response to the
 * Request EC command. This value is logged by the ECR and can be used to
 * invoke version specific drivers. Valid values are 00–FF; default is FF
 * corresponding to an OEM device.
 * 
 * @param[in] fd File descriptor returned when opening a serial port with ecrOpen().
 * @param[in] ec A 1-byte value to set the ECR engineering change level.
 * 
 * @return
 * @li =0 Success
 * 
 * Negative errno value if unsuccessful.
 */
short svcCom3SetECLevel(int fd, char ec);

#ifdef __cplusplus
}
#endif
#endif
