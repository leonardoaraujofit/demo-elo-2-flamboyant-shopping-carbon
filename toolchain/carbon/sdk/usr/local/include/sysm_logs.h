#ifndef __SYSM_LOGS__
#define __SYSM_LOGS__

#include <sysmode_ui/sysm_ui.h>
#include <sysmode_ui/sysm_lang.h>

#define  LOG_MSG_SIZE   100

void logs_tamper_cb(const SYSM_UI::t_ui_menu_entry *);
void logs_install_cb(const SYSM_UI::t_ui_menu_entry *);
void logs_transfer_cb(const SYSM_UI::t_ui_menu_entry *);

#endif  // __SYSM_LOGS__
