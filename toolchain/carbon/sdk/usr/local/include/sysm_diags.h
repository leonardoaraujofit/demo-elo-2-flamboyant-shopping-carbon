#ifndef __SYSM_DIAGS__
#define __SYSM_DIAGS__

#include <sysmode_ui/sysm_ui.h>
#include <sysmode_ui/property.h>
#include <sysmode_ui/sysm_lang.h>
#include <sysmode_ui/settings_page.h>
#include<FL/Fl_Window.H>
#include<FL/fl_draw.H>
#include<FL/Fl_PNG_Image.H>
#include<FL/Fl_Table.H>
#include<FL/Fl_Box.H>
#include<FL/Fl_Device.H>
#include<FL/Fl_Text_Display.H>
#include<FL/Fl_Double_Window.H>
#include "sysm_admin.h"
#include "svc.h"
#include<string>
#include<vector>
#include <memory>
#include <stdarg.h>		// used by TextDisplay::print

extern "C" {
    #include<stdio.h>
    #include <sys/types.h>
    #include <libled.h>
    #include "svcmgr/svc_led.h"
    #include "libvoy.h"
    #include "vficom.h"
    #include "svcmgr/svc_sysinfo.h"
    #include "svcmgr/svc_net.h"
    #include "svcmgr/svc_sigcap.h"
    #include "svcmgr/svc_rfcr.h"
    #include "svcmgr/svc_msr.h"
    #include "svcmgr/svc_serialcom.h"
    #include <stdint.h>
    #include <time.h>
    #include <linux/types.h>
    #include <fcntl.h>
    #include <platforminfo_api.h>
    #include <verifone/cib.h>
}

using std::string;
using std::vector;
using std::auto_ptr;



extern const char * DIAGS_DISPLAY_STR;
extern const char * CONT_DIAG_STR;
extern const char * CONT_UNKNOWN_STR;
extern const char * CONT_WAITING_STR;
extern const char * CONT_TIMEOUT_STR;

extern const char * SMART_INSERT_STR;
extern const char * SMART_POWER_ERROR_STR;
extern const char * SMART_CARD_ERROR_STR;
extern const char * SMART_SELECT_ERROR_STR;
extern const char * SMART_OPEN_ERROR_STR;

extern const char * ETH_DOWN_STR;
extern const char * ETH_UP_STR;
extern const char * ETH_DHCP_STR;
extern const char * ETH_NOT_AVAIL_STR;
extern const char * ETH_PING_PASS_STR;
extern const char * ETH_PING_FAIL_STR;

extern const char * SERIAL_RS232_STR;
extern const char * SERIAL_RS485_STR;

extern const char * AUDIO_DIAG_STR;

extern const char * BATTERY_GOOD_STR;
extern const char * BATTERY_BAD_STR;

extern const char * RFCR_FAILED_STR;


class DiagWin : public SYSM_UI::Sysm_Window
{
    int handle( int );
public :
    DiagWin( int X,int Y,int W,int H,char* =0 ):SYSM_UI::Sysm_Window(X,Y,W,H){}
};
/* helper class for Display diagnostics */
class DisplayDiagBox : public SYSM_UI::Sysm_Window
{
    Fl_Box*          background;
    Fl_PNG_Image*    png_image;
    int dspDiagCnt;

    void dspDiag_cb( int& );
    int  handle( int event );
public:
    DisplayDiagBox(int X,int Y,int W,int H,char* =0 ):
                           SYSM_UI::Sysm_Window(X,Y,W,H)
    {
        dspDiagCnt = 0;
        background = new Fl_Box(X,Y,W,H);
        png_image  = NULL;
    }
    ~DisplayDiagBox()
    {
        delete(background);
        delete(png_image);
        background  = NULL;
        png_image   = NULL;
        dspDiagCnt  = NULL;
    }
    void show_background(const char* image)
    {
        png_image  = new Fl_PNG_Image(image);
        background->image(png_image);
        this->add(background);
        this->redraw();
    }
    void set_color( Fl_Color color )
    {
        this->remove(background);
        this->color(color);
        this->redraw();
    }
};

/* helper class for Keypad diagnostics */
class KeypadDiagTab : public Fl_Group
{
    int  * keypressCounter;

    const char ** key_lbls;
    int         * key_names;
    size_t    key_lbls_size;

    int handle( int event );
    vector<char *> labels;
public:
    KeypadDiagTab(int, int, int, int, const char *);
    ~KeypadDiagTab();
  //}
};

namespace SYSM_UI
{
	
	class KeypadDiag_Key : public Sysm_Button
	{
	private:
		Fl_Callback *cb;
		void *data;
	protected:
		int handle(int e);
	public:
		void callback(Fl_Callback *cb, void *d = 0);
		KeypadDiag_Key(int x, int y, int w, int h, const char * t,
				 const char * fi = NULL,
		   const char * ui = NULL):Sysm_Button(x,y,w,h,t,fi,ui){
			   this->cb=0;
			   this->data=0;
		   };
	};
	
	class KeypadDiag_keypad_num : public Fl_Group
	{
	public:
		
		typedef enum {
			TK_ONE = 0,
			TK_TWO,
			TK_THREE,
			TK_FOUR,
			TK_FIVE,
			TK_SIX,
			TK_SEVEN,
			TK_EIGHT,
			TK_NINE,
			TK_STAR,
			TK_ZERO,
			TK_HASH,
			TK_ESCAPE,
			TK_BACKSPACE,
			TK_ENTER,
			TK_MAX_KEYS
		} t_key_id;
		
		typedef struct callback_data {
			Fl_Widget *widget;
			t_key_id key_id;
			int count;
		} t_callback_data;
		
		typedef struct key_param {
			int x;
			int y;
			int w;
			int h;
			char label[16];
			t_callback_data cb_data;
		} t_key_param;
		
		/*
		 * x, y, w, h defines location and total size of keypad, all keys will be fit within w,h
		 */
		KeypadDiag_keypad_num(int x, int y, int w, int h, Fl_Widget *input); // keypad title
		~KeypadDiag_keypad_num() { }
		
	protected:
		
	private:
		t_key_param keys[TK_MAX_KEYS];
	};
};

typedef enum {
	TK_CLEAR,
	TK_X_PRESSED,
	TK_O_PRESSED,
	TK_EXIT,
	TK_MAX_STATES
} t_KeypadDiag_state_id;


/* helper class for mag. reader diagnostics */
typedef enum {
    DIAG_MSR              = 0,
    DIAG_SMART,
    DIAG_CONTACT,
} CARD_TYPE;



class CardDiagTab : public Fl_Table
{
    CARD_TYPE type;
    void draw_data(const char *s, int X, int Y, int W, int H);
    void draw_cell(TableContext context, int ROW=0, int COL=0, int X=0,
                   int Y=0, int W=0, int H=0);
    int SAMs;
    // flag for smartcard reader
    bool Flag_ICC_Reader_Open;
public:
    char smart_data[5][128];
    CardDiagTab(int X, int Y, int W, int H,CARD_TYPE type,const char *L=0) : Fl_Table(X,Y,W,H,L),type(type), SAMs(0), Flag_ICC_Reader_Open(false)
    {
        if( type == DIAG_MSR)
        {
            rows(4);
            row_header(0);
            row_height_all(23);
            row_resize(0);
            cols(3);
            col_header(0);
            col_width_all(81);
            col_resize(0);
        }
        else if( type == DIAG_SMART )
        {
            //CIB_Struct cib;
            PI_sam_info_st sam_info;
            unsigned long r;
            if((platforminfo_get(PI_SAM_INFO, &sam_info, sizeof(sam_info), &r) == PI_OK) && (sam_info.exist))
            {
                SAMs = sam_info.numOfSecSockets;
            }
            rows(2 + SAMs); // number of SAMs
            row_header(0);
            row_height_all(23);
            row_resize(0);
            cols(2);
            col_header(0);
            col_width_all(150);
            col_resize(0);
        }
    end();
    }
    int handle( int event );
    void do_callback(){  Fl_Widget::do_callback(); }
    void update_table();
    int sams_count(void) {return SAMs;}
    bool flag_ICC_reader(void) {return Flag_ICC_Reader_Open;}
    bool set_flag_ICC_reader(bool open) {Flag_ICC_Reader_Open = type == DIAG_SMART ? open : false;}

};


/* struct for Audio diagnostics */
typedef struct  wavHeader
{
    char                RIFF[4];        /* RIFF Header      */ //Magic header
    uint32_t       		ChunkSize;      /* RIFF Chunk Size  */
    char                WAVE[4];        /* WAVE Header      */
    char                fmt[4];         /* FMT header       */
    uint32_t       		Subchunk1Size;  /* Size of the fmt chunk*/
    int16_t     		AudioFormat;    /* Audio format 1=PCM,6=mulaw,7=alaw, 257=IBM Mu-Law, 258=IBM A-Law, 259=ADPCM */
    int16_t     		NumOfChannels;  /* Number of channels 1=Mono 2=Sterio                   */
    uint32_t	        SamplesPerSec;  /* Sampling Frequency in Hz                             */
    uint32_t            bytesPerSec;    /* bytes per second */
    int16_t     		blockAlign;     /* 2=16-bit mono, 4=16-bit stereo */
    int16_t     		bitsPerSample;  /* Number of bits per sample      */
    char                Subchunk2ID[4]; /* "data"  string   */
    uint32_t       		Subchunk2Size;  /* Sampled data length    */
    char data[1];
} wavHeader;


/* Touch panel diagnostics */

class TouchDiagDisplay : public Fl_Text_Display
{
	Fl_Text_Buffer* txt_buffer;
	int x,y;
public:
	TouchDiagDisplay(int X, int Y, int W, int H, const char *l = 0):Fl_Text_Display(X,Y,W,H,l)
	{
		//property class
		SYSM_UI::property *prp = SYSM_UI::property::getInstance();
		SYSM_UI::property::keypadAttribute *keypadAttr = prp->keypad;
		SYSM_UI::property::objectAttribute *menu_unfoc_prp = prp->obj(SYSM_UI::property::MENU_UNFOCUS);
		SYSM_UI::property::objectAttribute *app_prp = prp->obj(SYSM_UI::property::APP);
		txt_buffer = new Fl_Text_Buffer();
		if (keypadAttr->haveKeypad())
			txt_buffer->text("Press 'BackSpace' to stop test\nX,Y Touch");
		else
			txt_buffer->text("\n Press sysmode button to restart sysmode and stop test\n  X,Y Touch");
		this->labeltype(FL_NO_LABEL);
		this->box(FL_NO_BOX);
		this->color(menu_unfoc_prp->color());
		this->textcolor(menu_unfoc_prp->fontcolor());
		this->textfont(app_prp->font());
		this->textsize(app_prp->fontsize());
		this->buffer(txt_buffer);
		Fl::get_mouse(x,y);
	}
	
	int handle( int event);
	
};

/* Serial com diagnostics */

class TextDisplay : public Fl_Text_Display
{
    Fl_Text_Buffer *m_text_buffer;

public:
    TextDisplay(int X, int Y, int W, int H, const char *l = 0) :
    	Fl_Text_Display(X,Y,W,H,l)
    {
		//property class
		SYSM_UI::property *prp = SYSM_UI::property::getInstance();
		SYSM_UI::property::objectAttribute *menu_unfoc_prp = prp->obj(SYSM_UI::property::MENU_UNFOCUS);
		SYSM_UI::property::objectAttribute *app_prp = prp->obj(SYSM_UI::property::APP);
		this->labeltype(FL_NO_LABEL);
		this->box(FL_NO_BOX);
		this->color(menu_unfoc_prp->color());
		this->textcolor(menu_unfoc_prp->fontcolor());
		this->textfont(app_prp->font());
		this->textsize(app_prp->fontsize());
		m_text_buffer = new Fl_Text_Buffer;
		if (m_text_buffer)
			this->buffer(m_text_buffer);
    }

    void append_data(
    		const char* data)
    {
    	if (m_text_buffer) {
			m_text_buffer->append(data);
			this->redraw();
			Fl::check();
    	}
    }

    void print(
    		const char *text,
    		...)
    {
    	if (m_text_buffer) {
			va_list list;
			// calculate size of buffer
			va_start(list, text);
			int text_buffer_size = vsnprintf(NULL, 0, text, list);
			va_end(list);
			// allocate buffer
			if (text_buffer_size > 0) {
				char *text_buffer = new char[text_buffer_size+1];
				if (text_buffer) {
					va_start(list, text);
					vsnprintf(text_buffer, text_buffer_size+1, text, list);
					va_end(list);
					text_buffer[text_buffer_size] = '\0';
					// append the formatted text to the widget
					m_text_buffer->append(text_buffer);
					// scroll to tail
					this->scroll(this->mNBufferLines, 0);
					// release the buffer
					delete[] text_buffer;
				}
			}
    	}
    }

    void clear()
    {
    	if (m_text_buffer)
    		m_text_buffer->text("");
    }
};

class Canvas : public Fl_Box
{
    int old_x,old_y;
    bool init;
    int  handle(int event);
    void draw();
public:
    Canvas(int x,int y,int w,int h):Fl_Box(x,y,w,h),init(false){};
};


enum {
	BUZ_EVENT_LISTENER_LIST = 1, 
	BUZ_EVENT_LISTENER_FREQ,
	BUZ_EVENT_LISTENER_DUR,
	BUZ_EVENT_LISTENER_VOL,
	BUZ_EVENT_LISTENER_BEEP,
    BUZ_EVENT_LISTENER_PLAY
};

class buzzer_conf_c: public SYSM_UI::settings_page_c
{
	unsigned int m_frequency;
	unsigned int m_duration;
	unsigned int m_volume;
public:
	buzzer_conf_c();
protected:
	void on_event(const event_c *);
};

enum {
	LED_EVENT_LISTENER_LIST = 1, 
	LED_EVENT_LISTENER_LOGO, 
	LED_EVENT_LISTENER_KBD
};

class led_conf_c: public SYSM_UI::settings_page_c
{
public:
	led_conf_c();
protected:
	void on_event(const event_c *);
};

enum {
	GEOLOC_EVENT_LISTENER_LIST = 1, 
	GEOLOC_EVENT_LISTENER_SERVER,
	GEOLOC_EVENT_LISTENER_POS,
};

class geoloc_conf_c: public SYSM_UI::settings_page_c
{
	unsigned int m_server;
public:
	geoloc_conf_c();
protected:
	void on_event(const event_c *);
};

enum {
	POWERMNGT_EVENT_LISTENER_LIST = 1, 
	POWERMNGT_EVENT_LISTENER_SUSPEND
};

class powermngt_conf_c: public SYSM_UI::settings_page_c
{
public:
	powermngt_conf_c();
protected:
	void on_event(const event_c *);
};

enum {
	TCH_GRND_EVENT_LISTENER_LIST = 1, 
	TCH_GRND_EVENT_LISTENER_STATUS
};

class tch_grnd_conf_c: public SYSM_UI::settings_page_c
{
public:
	tch_grnd_conf_c();
protected:
	void on_event(const event_c *);
};

void diags_display_cb(const SYSM_UI::t_ui_menu_entry *e );
void diags_keyboard_cb(const SYSM_UI::t_ui_menu_entry *e );
void diags_keyboard_no_keypad_cb(const SYSM_UI::t_ui_menu_entry *e );
void diags_crd_mg_cb(const SYSM_UI::t_ui_menu_entry *e );
void diags_crd_sc_cb(const SYSM_UI::t_ui_menu_entry *e );
void diags_crd_cl_cb(const SYSM_UI::t_ui_menu_entry *e );

void diags_eth_cb(const SYSM_UI::t_ui_menu_entry *e);
void diags_eth0_ipv6_cb(const SYSM_UI::t_ui_menu_entry *e);
void diags_bnep0_cb(const SYSM_UI::t_ui_menu_entry *e);
void diags_ppp1_cb(const SYSM_UI::t_ui_menu_entry *e);
void diags_com_cb(const SYSM_UI::t_ui_menu_entry *e);
void diags_wifi_cb(const SYSM_UI::t_ui_menu_entry *e);
void diags_wifi_ipv6_cb(const SYSM_UI::t_ui_menu_entry *e);
void diags_ibeacon_cb(const SYSM_UI::t_ui_menu_entry *e);
void diags_modem_cb(const SYSM_UI::t_ui_menu_entry *e);
void diags_usb_cb(const SYSM_UI::t_ui_menu_entry *e);
void diags_gps_cb(const SYSM_UI::t_ui_menu_entry *e);
void diags_geoloc_cb(const SYSM_UI::t_ui_menu_entry *e);
void diags_usb0_eth_cb(const SYSM_UI::t_ui_menu_entry *e);
void diags_usb0_eth_ipv6_cb(const SYSM_UI::t_ui_menu_entry *e);
void diags_syslog_cb(const SYSM_UI::t_ui_menu_entry *e);

void diags_battery_cb(const SYSM_UI::t_ui_menu_entry *e );
void diags_audio_cb(const SYSM_UI::t_ui_menu_entry *e );

void diags_tch_coord_cb(const SYSM_UI::t_ui_menu_entry *e );
void diags_tch_sign_cb(const SYSM_UI::t_ui_menu_entry *e );
void diags_tch_grnd_cb(const SYSM_UI::t_ui_menu_entry *e );

void diags_led_cb(const SYSM_UI::t_ui_menu_entry *e);
void diags_powermngt_cb(const SYSM_UI::t_ui_menu_entry *e);
void diags_prnt_cb(const SYSM_UI::t_ui_menu_entry *e);
void diags_buz_cb(const SYSM_UI::t_ui_menu_entry *e);


#endif
