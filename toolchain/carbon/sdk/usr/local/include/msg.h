/**********************************************************************************
*		Copyright, 2004 VeriFone Inc.
*		2099 Gateway Place, Suite 600
*		San Jose, CA.  95110
*
*  All Rights Reserved. No part of this software may be reproduced,
*  transmitted, transcribed, stored in a retrieval system, or
*  translated into any language or computer language, in any form
*  or by any means electronic, mechanical, magnetic, optical,
*  chemical, manual or otherwise, without the prior permission of
*  VeriFone Inc.
*
**********************************************************************************/

/**
 * @file msg.h
 */

#ifndef _MSG_H
#define _MSG_H

// Includes
#ifdef SHIT

#include "env.h"
#include "led.h"
#include "speaker.h"
#include "eeprom.h"
#include "info.h"
#include "touch.h"
#include "rtc.h"
#include "iomod.h"
#include "chgpasswd.h"
#include "setdate.h"
#include "setperms.h"
#include "sysctl.h"
#include "bootargHdr.h"


// Defines
/**
 * Define
 */
/** \{ */
#define  GENQUE_IN_KEY		100
#define	GENQUE_OUT_KEY	      101 
/** \} */

// Message IDs
/**
 * @name Mesage IDs
 */
/** \{ */
#define MSG_PUTENV			1
#define MSG_GETENV			2
#define MSG_CLRENV			3
#define MSG_DMPENV			4
#define MSG_LEDON			5
#define MSG_LEDOFF			6
#define MSG_SPEAKER			7
#define MSG_SETRTC			8
#define MSG_PUTEEPROM		9
#define MSG_GETEEPROM		10
#define MSG_GETBATVOLT		11
#define MSG_MODTYPE			12
#define MSG_GETCABLESTAT	13
#define MSG_TOUCH			14
#define MSG_RTCWRAM			15
#define MSG_RTCRRAM			16
#define MSG_DELENV			17
#define MSG_RTC2LINUX		18
#define MSG_IOMODCTRL		19
#define MSG_CHGPASSWD		20
#define MSG_GETFIRSTEEPROM	21
#define MSG_RESTART			22
#define MSG_SETDATE			23
#define MSG_LOCKROOT		24
#define MSG_SETPERMS		25
#define MSG_PUTINROOT		26
#define MSG_GETINROOT		27
#define MSG_LEDRUNWAY		28
#define MSG_VIRGIN_AGAIN	29
#define MSG_SIGCAP          30
#define MSG_SYSCTL          31
#define MSG_RTCW3610        32
#define MSG_RTCR3610        33
#define MSG_USBSTOR         34
#define MSG_BLOWCRT         35
#define MSG_GETMODEL        36
#define MSG_GETDIAGS	    37
#define MSG_INFOS			38
#define MSG_COLORIZESYSMODE 39
// 40  available
#define MSG_BOOTARGHDR      41
#define MSG_PACKAGE         42

#define  MSG_RUNUSR		61
#define	 MSG_FILEAUTH	62
#define	 MSG_PWDCMP		63
/** \} */

// Structures
/*STRUCT*/
/**
 *
 */
struct genericStrInt {
	int   opRslt;
	char  parm1[128];
	char  parm2[128];
	int	  intP1;
	int	  intP2;
};

/*STRUCT*/
/**
 *
 */
struct genericStr {
	int   opRslt;
	char  parm1[128];
	char  parm2[128];
};

/*STRUCT*/
/**
 *
 */
struct packageStruct {
	int   opRslt;
	int   uid;
	char  command[128];
	char  fileName[128];
	char  sec[8];
};

// Generic Structure
/*STRUCT*/
/**
 *
 */
struct genericCtrl {
		int   opRslt;
		int   parm1;
	};

/*STRUCT*/
/**
 *
 */
struct bootargHdrReq {
  int opRslt;
  bootargHdr_t bootArg;
};

	
// Structures
/*STRUCT*/
/**
 *
 */
struct inMsg {
  long clientPid;
  long message_type;
  union {
    struct PutGetEnv putGetEnv;
    struct LedCtrl ledCtrl;
    struct SpeakerCtrl speaker;
    struct EEPromCtrl EEProm;
    struct InfoCtrl info;
    struct touchCtrl touch;
    struct sigcapCtrl sigcap;
    struct rtcCtrl rtc;
    struct IOModCtrl ioMod;
    struct PasswdCtrl chgPasswd;
    struct dateCtrl setDate;
    struct permsCtrl setPerms;
    struct SysCtl sysCtl;
    struct rtcCtrl3610 rtc3610;
    struct genericCtrl generic;
    struct genericStr generic2Str;
	struct packageStruct package;
    struct bootargHdrReq bootArgReq;
	struct genericStrInt generic2Str2Int;
    char   null[0];
  }  data;
};

#endif

#define msgsend(msqid,msgp,msgsz,msgflg) ({ \
	int rslt; \
	do { rslt = msgsnd(msqid,msgp,msgsz,msgflg); } while (rslt == -1 && errno == EINTR); \
	rslt; \
})

#define msgsndin(type,union) ({ \
	int rslt; \
	amsg.message_type=type; \
	amsg.clientPid=getpid(); \
	do { rslt = msgsnd(inQid,&amsg,offsetof(struct inMsg,data)+sizeof((struct inMsg){}.data.union)-sizeof(long),0); } \
		while (rslt == -1 && errno == EINTR); \
	rslt; \
})

#define msgsndout(result,union) ({ \
	int rslt; \
	amsg.data.union.opRslt = result; \
	do { rslt = msgsnd(outQid,&amsg,offsetof(struct inMsg,data)+sizeof((struct inMsg){}.data.union)-sizeof(long),0); } \
		while (rslt == -1 && errno == EINTR); \
	rslt; \
})

#define msgrcvoutp(msqid,msgp,msgsz,msgtyp,msgflg) ({ \
	int rslt; \
	do { rslt = msgrcv(msqid,msgp,msgsz,msgtyp,msgflg); } \
		while (rslt == -1 && errno == EINTR); \
	rslt; \
})
#define msgrcvout() msgrcvoutp(outQid,&amsg,sizeof(struct inMsg)-sizeof(long),amsg.clientPid,0)

#define  ROOT_SECRET_FILE  "/mnt/sram/.sec"

#endif //msg.h
