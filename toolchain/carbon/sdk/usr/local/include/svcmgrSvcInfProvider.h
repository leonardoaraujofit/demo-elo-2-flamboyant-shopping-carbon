/** @addtogroup utils Utils */
/** @{ */
/**
 *  @file svcmgrSvcInfProvider.h
 *
 *  @brief Service interface private property API
 *
 *  Property API (private), public API's are in svcmgrSvcInf.h
 *
 */

/*
*  Copyright, 2015 VeriFone Inc.
*  2099 Gateway Place, Suite 600
*  San Jose, CA.  95110
*
* All Rights Reserved. No part of this software may be reproduced,
* transmitted, transcribed, stored in a retrieval system, or
* translated into any language or computer language, in any form
* or by any means electronic, mechanical, magnetic, optical,
* chemical, manual or otherwise, without the prior permission of
* VeriFone Inc.
*
*/

#ifndef _SVCMGR_SVCINF_PROVIDER_H_
#define _SVCMGR_SVCINF_PROVIDER_H_

#ifdef __cplusplus
extern "C" {
#endif



/**
* Clears out existing properties and prepares for a new set of property add's.
*
* @return 0 on success, -1 on error.
*
* @note
* Properties are 'static' in that each call to smi_SvcXmlCmd() will receive
* currently set properties which have not been cleared.
* This should be called, then any properties added, prior to every WDE cmd
* call (smi_SvcXmlCmd()).
*/
int smi_NewProperties();


/**
* Clears out existing properties.
*
* @note
* It is really never necessary to call this, smi_NewProperties() calls this first.
* The only reason would be a clean cgi process exit.
*/
void smi_DeleteProperties();


/**
* Adds property.
*
* @param[in] name The string, which specifies the name of the property.
* @param[in] value The string, which specifies the value of the property.
*
* @return 0 on success, -1 if error while adding property
*/
int smi_AddProperty(const char* name, const char* value);


/**
* Public helper method used to allow a service to call methods in another service.
*
* @param[in] service The string, which specifies the name of the service.
*
* @return library descriptor (via dlopen()) for specified service name,
*		or NULL on error.
*/
void *smi_SvcLibOpen(char *service);

/**
* Service WDE XML API
*
* @param[in] command The string, XML command.
*
* @return Response from service XML command entry calll.

* @note
* Call this with a <svc_cmd> string, a <svc_rtn> string is returned.
* any/all properties must be set (added) prior to making this call.
* The returned string must be free'd by the caller.
*/
char* smi_SvcXmlCmd(char* cmd);


#ifdef __cplusplus
}
#endif

#endif // _SVCMGR_SVCINF_PROVIDER_H_
///@}
