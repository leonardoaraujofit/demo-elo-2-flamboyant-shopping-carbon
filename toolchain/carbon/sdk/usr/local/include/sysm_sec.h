#ifndef __SYSM_SEC__
#define __SYSM_SEC__

#include <sysmode_ui/sysm_ui.h>
#include <sysmode_ui/sysm_lang.h>

void sec_dumpkeys_cb(const SYSM_UI::t_ui_menu_entry *e);
void sec_tamper_stat(const SYSM_UI::t_ui_menu_entry * e);
void sec_expected_cb(const SYSM_UI::t_ui_menu_entry * e);
void sec_master_session(const SYSM_UI::t_ui_menu_entry * e);
void sec_dukpt_cb(const SYSM_UI::t_ui_menu_entry * e);
void sec_ade_cb(const SYSM_UI::t_ui_menu_entry * e);
void sec_vrk_cb(const SYSM_UI::t_ui_menu_entry * e);
void sec_vss_cb(const SYSM_UI::t_ui_menu_entry * e);
void sec_features_cb(const SYSM_UI::t_ui_menu_entry *e);
void sec_expire_usr(const SYSM_UI::t_ui_menu_entry *e);
void sec_expire_kld(const SYSM_UI::t_ui_menu_entry *e);
void sec_ldbank_cb(const SYSM_UI::t_ui_menu_entry *e);
void sec_ldvrk_cb(const SYSM_UI::t_ui_menu_entry *e);
void sec_ldperso_cb(const SYSM_UI::t_ui_menu_entry *e);
void sec_chng_pwd(const SYSM_UI::t_ui_menu_entry *e);
void sec_chng_kld(const SYSM_UI::t_ui_menu_entry *e);
void sec_vstree_cb(const SYSM_UI::t_ui_menu_entry *e);
void sec_vsp_status_cb(const SYSM_UI::t_ui_menu_entry *e);
void sec_rsa_status_cb(const SYSM_UI::t_ui_menu_entry *e);

void sec_check_tamper();
int login_keyloads();

#endif //__SYSM_SEC__
