/**********************************************************************************
*		Copyright, 2011 VeriFone Inc.
*		2099 Gateway Place, Suite 600
*		San Jose, CA.  95110
*
*  All Rights Reserved. No part of this software may be reproduced,
*  transmitted, transcribed, stored in a retrieval system, or
*  translated into any language or computer language, in any form
*  or by any means electronic, mechanical, magnetic, optical,
*  chemical, manual or otherwise, without the prior permission of
*  VeriFone Inc.
*
**********************************************************************************/
#ifndef _VFINETCTRL_H
#define _VFINETCTRL_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <memory.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/io.h>
#include <sys/msg.h>
#include <time.h>
#include <sys/mount.h>
#include <errno.h>
#include <syslog.h>


#ifndef BOOL
#define BOOL int
#define TRUE  1
#define FALSE 0
#endif


#define ETHERNET_DEVICE_NAME "eth0"

// Defines
#define  NETCNTRLQUEUE_IN_KEY		210
#define	 NETCNTRLQUEUE_OUT_KEY	    211


#define  MSG_NTP                  1
#define  MSG_NETGETCONF           2
#define  MSG_NETLINK              3
#define  MSG_NETDOWN              4
#define  MSG_NETUP                5
#define  MSG_NETUPDHCP            6
#define  MSG_NETUPETHERDHCP       7
#define  MSG_NETPING              8
#define  MSG_NETGETINTERFACE      9
#define  MSG_NETSETCONF          10
#define  MSG_ETHERCABLEIN       200
#define  MSG_ETHERCABLEOUT      201

#define  MAX_COMMAND_LEN           128


#ifndef STRUCT_NETCONF
#define STRUCT_NETCONF
struct netConf
{
	short int	dhcp;		/**< 1 = DHCP enabled, 0 = DHCP disabled */
	char		MAC[18];	/**< Ethernet MAC address */
	char		ipaddr[16];	/**< Current IP Address */
	char		netmask[16];	/**< Current Network Mask */
	char		gateway[16];	/**< Current Gateway */
	char		dns1[16];	/**< DNS Server 1 Address */
	char		dns2[16];	/**< DNS Server 2 Address */
};
#endif


typedef union {
	struct netConf etherConf;
} network_config_u;


// network related structure
typedef struct {
	int     flag;
	char	hostname[64];
	network_config_u netConfig;
} network_control_data_t;



// The format of this struct is dictated by msg.h and msg.c
typedef struct {
	int   opRslt;  // filled in by msg.c msgsndout()
	int   opRsltErrno;
	network_control_data_t controlData;
}network_control_t;


// Structures
struct networkCtrlMsg {
	long clientPid;
	long message_type;
	union {
		network_control_t network;
		char   null[0];
	} data;
};

#define msgsend(msqid,msgp,msgsz,msgflg) ({ \
	int rslt; \
	do { rslt = msgsnd(msqid,msgp,msgsz,msgflg); } while (rslt == -1 && errno == EINTR); \
	rslt; \
})

#define netCntrlSndIn(type,union) ( \
	netCntrlMsg.message_type = type, \
	netCntrlMsg.clientPid = getpid(), \
	msgsend(inQid, &netCntrlMsg, offsetof(struct networkCtrlMsg, data) + sizeof((struct networkCtrlMsg){}.data.union) - sizeof(long), 0) \
)

#define netCntrlRcvOut() ({ \
	int rslt; \
	do { rslt = msgrcv(outQid, &netCntrlMsg, sizeof(struct networkCtrlMsg) - sizeof(long), netCntrlMsg.clientPid,0); } while (rslt == -1 && errno == EINTR); \
	rslt; \
})

#define netCntrlSndOut(result, union) ( \
	netCntrlMsg.data.union.opRslt = result, \
	msgsend(outQid, &netCntrlMsg, offsetof(struct networkCtrlMsg, data) + sizeof((struct networkCtrlMsg){}.data.union) - sizeof(long), 0) \
)



#ifdef __cplusplus
}
#endif
#endif // _VFINETCTRL_H
