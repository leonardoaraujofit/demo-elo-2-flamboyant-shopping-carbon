#ifndef __LCD8859_IO_H
#define __LCD8859_IO_H

#ifdef __cplusplus
extern "C" {
#endif


typedef struct
{
    int x;
    int y;

} lcd_pos;

#define LCD_WIN_MODE	 	(0)
#define LCD_PROG_ISO8859	(1)
#define LCD_FONT_DETAILS 	(2)
#define LCD_GET_POS	 		(3)
#define LCD_PROG	        (4)
#define RAZOR 				1
#define MINIRAZOR			2
#define MINIRAZOR_BOOTUP	3

#define TRANSPARENT_OFF	0
#define TRANSPARENT_ON	1
/* 
 *  vLcd8859Putc
 *
 *  in params: character
 *  out params: none
 *  
 *  putc (LCD, ch); equivalent
 *  Can be used to display individual characters to screen or
 *  to send escape characters
 */
void vLcd8859Putc (char ch);

/* 
 *  nLcd8859Write
 *
 *  in params: pointer to string
 *             string length   
 *  
 *  out params: Number of characters written
 *  
 *  write (LCD, string, len); equivalent
 *  Can be used to display character string (pre-formatted or not)
 *
 */
int nLcd8859Write (unsigned char *buffer, int length);

/* 
 *  nLcd8859Printf
 *
 *  in params: String to  format
 *  out params: Number of characters written
 *  
 *  printf ("string"); equivalent
 *  Can be used to format and display character string.
 *
 */
int nLcd8859Printf (char *fmt, ...);

/* 
 *  nLcd8859Cmd
 *
 *  in params: command
 *             user argument   
 *  
 *  out params: 0 - success
 *             -1 - failure
 *             
 *  ioctl (LCD_IOC_XXXX); equivalent
 *  Can be used to update system font based opn user input
 *  Can be used to retrieve window mode status
 *  Can be used to retrieve current font details
 *  Can be used to retrieve current xpos, ypos details
 */
int nLcd8859Cmd (unsigned int cmd, void *arg);

/* nLcd8859SetBgColour
 * 
 * in params: colour of background
 * 
 * This function will switch off transparent mode
 * and write a new value to bgcolour in 1567.c
 */
void vLcd8859SetBgColour(unsigned int colour);

/* 
 *  nLcd8859Init
 *
 *  in params: nScale whether to scale from 132x64
 * 				mode - MINIRAZOR or RAZOR
 * 				transparent - 	1 means text written will be written transparently.
 * 								0 means text will be written over white background.
 *  out params: none
 *  
 *  Initialise lower lcd pixel layer
 *  Get display size and set size for upper layer
 *  display logo
 */
int nLcd8859Init (int nScale, int mode, int transparent);


#ifdef __cplusplus
}
#endif

#endif //LCD8859_IO.H
