/**********************************************************************
 * Module       : semtekHTdes.h
 * Version		:
 *
 * Description  : Linux Messaging interface for Semtek Innovative Solutions htDES module
 *
 * Notes        : Defines the messaging structures to communicating with the htDES module
 *				  running in root space in the linux kernel environment
 *
 * Functions    : 
 *
 * Name         : William  Athing
 *
 * Engineering
 * Status       :                       RELEASED         
 *				: UNIT TEST|SYSTEM TEST|        |PROPOSED|UNSUPPORTED
 *
 * Copyright (c) 2007 Semtek Innovative Solutions. 
 * Copyright (c) 2011 Verifone 
 *
 **********************************************************************/


/** @addtogroup semtekhtdes SemtekHTDes */
/** @{ */
/** @addtogroup semtekhtdesapi SemtekHTDes : API */
/** @{ */
/** 
 *  @file SemtekHTdes.h 
 *
 *	@brief Linux Messaging interface for Semtek Innovative Solutions htDES module
 *
 * 
 */


 
#ifndef SEMTEKHTDES_H
#define SEMTEKHTDES_H

#ifdef WIN32
#pragma pack(1)
#endif

#include "SemtekHTDesStatus.h"				// for module status message MSG_HTDES_STATUS	

/** 
 * Linux Messaging interface for Verifone MX800 series terminals
*/

/**
 Queue defines
*/
 /** \{ */
#define QUE_SEMTEKD				500	/**< to semtekd */
#define QUE_SEMTEKRSP  			501 /**< from semtekd */
/** \} */

/**
* Message IDs - (aka function codes), using clientPid as IPC "message type"
*/
/** \{ */
#define MSG_CLEAR_MSR			1 /**< Request encryption */
#define MSG_ENCRYPTED_MSR		2
#define MSG_ENCRYPTED_PAN		3 /**< Request clear pan for PinBlock */
#define MSG_CLEAR_PAN			4
#define MSG_STATUS_REQUEST		5
#define MSG_STATUS_ANSWER		6
#define MSG_SEMTEKD_RESET		7 /**< This message has no associated structure */
#define MSG_SEMTEKD_RESET_RESULT 8
#define MSG_START				9
#define MSG_START_RESULT		10
#define MSG_STOP				11
#define MSG_STOP_RESULT			12
#define MSG_RESET_PASSWORD		13
#define MSG_RESET_PASSWORD_RESULT 14
#define MSG_MANUAL_ENTRY		15
#define MSG_MANUAL_ENTRY_RESULT 16
#define MSG_EPARM_REQUEST		17
#define MSG_EPARM_RESULT 		18
#define MSG_CLEAR_DATA_REQUEST	19
#define MSG_CLEAR_DATA_RESPONSE	20
#define MSG_CLEAR_BUFFERS_REQUEST	21
#define MSG_CLEAR_BUFFERS_RESPONSE		22
#define MSG_OVERRIDE_MSG_QUERY	23
#define MSG_OVERRIDE_MSG_RESPONSE	24
#define MSG_DIAG_QUERY			25
#define MSG_DIAG_RESPONSE		26
#define MSG_ADVANCE_DDK			27
#define MSG_ADVANCE_DDK_RSP		28
#define MSG_SWITCH_UKPD			29
#define MSG_SWITCH_UKPD_RSP		30
#define MSG_REGISTART_REQ		31
#define MSG_REGISTART_RSP		32
#define MSG_REGISTOP_REQ		33
#define MSG_REGISTOP_RSP		34
#define MSG_REGISTART_SRED_REQ	35
#define MSG_REGISTART_SRED_RSP	36
#define MSG_BIN_TABLE_ID_REQ	37
#define MSG_BIN_TABLE_ID_RSP	38
/** \} */

#ifdef VCL_INTERNALS
#include "vcl_private_msgs.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

// track 3 is longest track at 107.  
#define MSG_MSR_MAX_TRACK_DATA 107
// max pan length.  Allow for up to 5 spaces in pan, plus null terminator
#define MSG_MAX_PAN_DATA 19+5+1

#define EPARMS_BLOCK_LEN (3*MSG_MSR_MAX_TRACK_DATA - (MSG_MAX_PAN_DATA+1))


// (Do not overlap return codes of next two groups !)
// this field may be bitmapped in the future, so look at high and low words independently
//
/**
 * ulStatus values for MSG_MSR_DATA
 */
/** \{ */
#define MSR_STATUS_OK			 0  
#define MSR_ERR_PAN_MISMATCH     8      // pan mismatch on pin/debit attempt
#define MSR_ERR_BAD_CMDCARD		 9      // bad cmd card (e.g.crc fail after decrypt) 
#define MSR_ERR_GENERAL			11		// inactive
#define MSR_ERR_PARSE			12      // e.g. missing '=' (t2) or '^' (t1) 	
#define MSG_ERR_BINREC_ERR		14      // cannot find satisfactory BIN match (even default)		
#define MSG_ERR_BAD_PAN			15      // mod 10 fail; len < 12 or > 19; 
#define MSG_ERR_UNINITIALIZED	16
/** \} */

/**
 * Values for ulEncryptionStatus
 */
/** \{ */
#define MSG_TRK_NOT_ENCRYPTED	0x0000
#define MSG_TRK_ENCRYPTION_ERR	0x0001
#define MSG_TRK1_ENCRYPTED		0x0002
#define MSG_TRK2_ENCRYPTED		0x0004
/** \} */

// i don't see a #define for this, it's referred to in the 
// MX800 series programmers manual definition for status byte, part of msrRead function
#define MSR_TRACK_TOO_LONG_OR_NO_ES 3

/**
 Magnetic stripe data
*/
typedef PACKED_SPEC struct {
	unsigned char ucStatus;			// status of track
	unsigned char ucCount;			// size in bytes of track dData payload (excl. status,count). 
	char cData[MSG_MSR_MAX_TRACK_DATA+1];	// pointer to track data
#ifdef WIN32
	unsigned short dummy1;			// dummy variable so windows sizes match linux sizes 
#endif
#ifdef VDRIV
	unsigned short dummy1;			// dummy variable so windows sizes match linux sizes 
#endif
} MSG_MSR_TRACK_DATA;

//  Following message is associated (for card swipes) with 
//  MSG_CLEAR_MSR (to semtekd) and MSG_ENCRYPTED_MSR (from semtekd).  Docuemented above in section MSG_HTDES_STATUS
/**
 Message used in MSG_CLEAR_MSR (to semtekd) and MSG_ENCRYPTED_MSR (from semtekd).
*/
typedef PACKED_SPEC struct {
	unsigned long ulStatus; /**< status of swipe operation (from semtekd)  */
	unsigned long ulEncryptionStatus; 
	MSG_MSR_TRACK_DATA stTrack1;
	MSG_MSR_TRACK_DATA stTrack2;
	MSG_MSR_TRACK_DATA stTrack3;
} MSG_MSR_DATA;

// Following message is associated (for Pin Debit)with 
// MSG_ENCRYPTED_PAN (to semtekd) and MSG_CLEAR_PAN (from semtekd)
/**
 Message used in MSG_ENCRYPTED_PAN (to semtekd) and MSG_CLEAR_PAN (from semtekd).
*/
typedef PACKED_SPEC struct {
	unsigned char ucStatus; /**< status of track */
	unsigned char ucCount;/**< size in bytes of track data */
	char cPanData[MSG_MAX_PAN_DATA+1]; /**< pointer to track data */
	char szExpDate[5]; /**< 4 digit YYMM + null */
#ifdef WIN32
	char dummy1[3]; /**< dummy variable so windows sizes match linux sizes */
#endif
} MSG_PAN_DATA;

/**
 * This structure is part of HTDesMsg structure
 */
typedef PACKED_SPEC struct {
	unsigned long ulStatus; /**<  status of operation  (always 0, success, to correspone to API) */
	unsigned char ucPassword[MSG_PASSWD_SIZE]; /**< 12 byte binary password, no null termination */
} MSG_PASSWORD_DATA;

/**
 * This structure is part of HTDesMsg structure
 */
typedef PACKED_SPEC struct {
	unsigned char ucStatus; /**< (out) status of request */
	char cPanData[MSG_MAX_PAN_DATA+1];  /**< (in) encrypted PAN from the track we (null terminated)
										* want the EParms data for.  (Can 
										* only be most recently swiped card record)
										* this is merely used to authenticate that 
										* the requestor is in the path to receive
										* card data
										*/
	char szEParms[EPARMS_BLOCK_LEN];	
#ifdef WIN32
	char dummy1[2]; /**< dummy variable so windows sizes match linux sizes */
#endif
#if (VDES_VERSION == VERIFONE_VERIX_V)
			char   dummy[4];
#endif
} MSG_EPARMS_DATA;


/**
 * This structure is part of HTDesMsg structure
 */
 typedef PACKED_SPEC struct {
	unsigned char ucStatus; /**< (out) status of request */
	unsigned char ucFmt; /**< 0 = normal verbose mode, 1= consise mode, 2=XML */
	unsigned char ucPageNo; /**< 0-127 */
	unsigned char ucNumPages; /**<  0-127 */
	char szData[EPARMS_BLOCK_LEN];	
} MSG_DIAG_DATA;

/**
 * This structure is part of HTDesMsg structure
 */
typedef PACKED_SPEC struct {
	unsigned char ui8DDKid;  /**<  DDK to advance to */
} MSG_DDK_DATA;

#define BIN_TABLE_ID_SIZE_OF_MSG 32

/**
 * This structure is part of HTDesMsg structure
 */
typedef PACKED_SPEC struct {
	unsigned char ui8BinTableId[BIN_TABLE_ID_SIZE_OF_MSG];
} MSG_BIN_DATA;

/**
 * Generic Message Structure 
 * Used to create various message sent from requester to the processor.
 */
typedef PACKED_SPEC struct {
	long clientPid; /**< process ID of who should receive response */
	long message_type;
	PACKED_SPEC union {
		MSG_MSR_DATA sMsrData;
		MSG_PAN_DATA sPanData;
		MSG_HTDES_STATUS sHTdesStatus;
		MSG_PASSWORD_DATA sPasswordData;
		MSG_EPARMS_DATA sEParmData;
		MSG_DIAG_DATA sDiagInfo;
		MSG_DDK_DATA sDDKdata;
		MSG_BIN_DATA sBinTableId;
#ifdef WIN32
		char   null[2];
#else
#if (VDES_VERSION != VERIFONE_VERIX_V)
		char   null[0];
#else
		char   null[4];
#endif
#endif
	}  data;
} HTDesMsg;

#ifdef __cplusplus
}
#endif

#endif /* SEMTEKHTDES_H */

#ifdef WIN32
#pragma pack()
#endif
