/***************************************************************************/
/*      Copyright 2000-2003 by VeriFone, Inc. All Rights Reserved.         */
/*                                                                         */
/*  Accessing, using, copying, modifying or distributing this source code  */
/*  is expressly not permitted without the prior written authorization of  */
/*  VeriFone, Inc.  Engaging in any such activities without authorization  */
/*  is an infringement of VeriFone's copyrights and a misappropriation of  */
/*  VeriFone's trade secrets in this source code,  which will subject the  */
/*  perpetrator to certain civil damages and criminal penalties.           */
/***************************************************************************/

/*
;============================================================================+
;| FILE NAME:  | keystore.h
;|-------------+-------------------------------------------------------------+
;| DESCRIPTION:| Header file for the key storage manager.
;|-------------+-------------------------------------------------------------+
;| VERSION     | $Modtime:   21 May 2009 15:21:54  $
;|-------------+-------------------------------------------------------------+
;| COMMENTS:   |
;|             |
;|===========================================================================+
*/
#ifndef __KEYSTORE_H__
#define __KEYSTORE_H__

#include <stdint.h>

/*
;|===========================================================================+
;| NOTE: | SYSTEM INCLUDE
;|===========================================================================+
*/

//#include "error.h"

/*
;|===========================================================================+
;| NOTE: | DEFINITION
;|===========================================================================+
*/
#define SINGLE_LEN      8
#define DOUBLE_LEN      16


// DeleteAllKeys() flags:
#define CONDITIONAL     0       // Delete all keys loaded prior to last reboot
#define STRICT          1       // Delete all keys
#define KS_EXTERNAL     (1<<7)  // External (public side) call
                                // This flag is raised on call from public side by vaultHandler,
                                // No need to raise it explicitely
                                // Flag is reset (0) if called from root process
#define KEEP_PP_KEYS    (1<<6)  // Do NOT delete pinpad keys


#define STOREKEY        0
#define GETKEY          1
#define DELETEKEY       2

// Key types for ComputeKeyAreaIndex()
//#define KT_RKLRSA      0x00     // RKL RSA key pair - not used - from Vx
#define KT_PPMS        0x10     // PINpad Master/Session
#define KT_PPDUKPT     0x11     // PINpad DUKPT
#define KT_VSSKLK      0x20     // VSS KLK
#define KT_VSSCMC      0x21     // VSS data/key blocks
#define KT_VSSDUKPT    0x22     // VSS DUKPT
#define KT_ADEDUKPT    0x30     // Account Data Encryption DUKPT
#define KT_APACS40     0x40     // Association for Payment Clearing Services
#define KT_PP_AUTH     0x50     // MagIC3 External PINpad Authentication Symmetric Keys
#define KT_PP_RSA      0x51     // MagIC3 External PINpad Authentication RSA Key

#define S_F_MODE (S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH) /*0664*/
/* TODO Temp change for this release nededs to be revisited*/
#define S_FKEY_MODE (S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH) /*0666*/


// Key area indices - Legacy alpha format
#define KI_VSSKLK       0   // GISKE Type - VSS KLK
#define KI_IPPDUKPT0    1   // DUKPT Engine Type - IPP
#define KI_IPPDUKPT1    2
#define KI_IPPDUKPT2    3
//#define KI_IPPDUKPT3    4   // Not Used
#define KI_VSSCMC0      5   // VSS Keys Type - 0 to 254 1DES keys
#define KI_VSSCMC1      6
#define KI_VSSCMC2      7
#define KI_VSSCMC3      8
#define KI_VSSCMC4      9
#define KI_VSSCMC5      10
#define KI_VSSCMC6      11
#define KI_VSSCMC7      12
//#define KI_PP_AUTH      13  // Not Used
#define KI_PP_RSA       14  // MagIC3 External PINpad Authentication RSA Key
#define KI_IPPMS        15  // GISKE Type - IPP
//#define KI_APACS40      16  // Not Used
//#define KI_EMVPIN       17  // Not Used
#define KI_VSSDUKPT0    18  // DUKPT Engine Type - VSS
#define KI_VSSDUKPT1    19
#define KI_VSSDUKPT2    20
#define KI_VSSDUKPT3    21
#define KI_VSSDUKPT4    22
#define KI_VSSDUKPT5    23
#define KI_VSSDUKPT6    24
#define KI_VSSDUKPT7    25
#define MAX_ALPHA_INDEX 25

// Key area indices - Numeric format
#define KI_ADEDUKPT0    27
#define KI_ADEDUKPT1    28
#define KI_ADEDUKPT2    29
#define KI_ADEDUKPT3    30
#define KI_ADEDUKPT4    31
#define KI_ADEDUKPT5    32
#define KI_ADEDUKPT6    33
#define KI_ADEDUKPT7    34
#define KI_ADEDUKPT8    35
#define KI_ADEDUKPT9    36
#define KI_VSSCMC8      37
// ...
#define KI_VSSCMC63     92
#define KI_VSSDUKPT8    93
// ...
#define KI_VSSDUKPT63  148
#define KI_PP_AUTH0    149
#define KI_PP_AUTH1    150
#define KI_PP_AUTH2    151
#define KI_PP_AUTH3    152
#define KI_ADEDUKPT10_NONSRED  153  // ADE TDES DUKPT keys 10 - 19 are NON SRED Keys 0 - 9
// ...
#define KI_ADEDUKPT19_NONSRED  162

// Available key areas:  163 to 255

#define LAST_KEY_AREA  162


// Number of keys
#define NUM_OF_IPP_MK               10  // 0 to 9
#define NUM_OF_IPP_DUKPT            3   // 0 to 2
#define NUM_OF_VSS_SCRIPTS          8   // 0 to 7
#define NUM_OF_VSS_SCRIPTS8         56  // 8 to 63
#define NUM_OF_VSS_SCRIPTS_TOTAL    (NUM_OF_VSS_SCRIPTS+NUM_OF_VSS_SCRIPTS8)
#define NUM_OF_VSS_KEYS_PER_SCRIPT  256 // 0 to 255
#define NUM_OF_VSS_KEYS             (NUM_OF_VSS_SCRIPTS*NUM_OF_VSS_KEYS_PER_SCRIPT)
#define NUM_OF_VSS_KEYS8            (NUM_OF_VSS_SCRIPTS8*NUM_OF_VSS_KEYS_PER_SCRIPT)
#define NUM_OF_ADE_DUKPT            10  // 0 to 9
#define NUM_OF_USRS                 16  // 0 to 15 (10(0x0A) - 15(0x0F)) GIDs
#define NUM_OF_USR_KEYS_PER_USR     10  // 0 to 9
#define NUM_OF_USR_KEYS             (NUM_OF_USRS*NUM_OF_USR_KEYS_PER_USR)
// For Poseidon Keys
#define NUM_OF_POS_KEYLOAD_KEYS     256 // 0 to 255
#define NUM_OF_POS_KEYUSAGE_KEYS    13  // 0 to 12
#define NUM_OF_ADE_DUKPT10_KEYS     10  // 10 to 19

#define MAX_VSS_KEYS                255 // 0 to 255

/*
;|===========================================================================+
;| NOTE: | TYPEDEF
;|===========================================================================+
*/
typedef struct {
    unsigned char   ucNbBlocksTotal;
    unsigned char   ucNbBlocksLoadable;
    unsigned char   ucCellSize;
    unsigned char   ucReadyFlag;
    //unsigned char   * pKeyArea;
    uint32_t   pKeyArea;

} TKeyAreaHeader;

/*
;|===========================================================================+
;| NOTE: | EXTERNAL VARIABLES
;|===========================================================================+
*/

/*
;|===========================================================================+
;| NOTE: | GLOBAL VARIABLES
;|===========================================================================+
*/

/*
;|===========================================================================+
;| NOTE: | CONSTANT - STRING
;|===========================================================================+
*/

/*
;|===========================================================================+
;| NOTE: | EXTERNAL FUNCTIONS
;|===========================================================================+
*/

/*
;|===========================================================================+
;| NOTE: | PUBLIC FUNCTIONS
;|===========================================================================+
*/

//////////////////////////////////////////////////////////////////////////////
//   PROCESS LEGACY KEYS
//
// - This functions are used to process keys that are stored both on
//   Secure Side and filesystem
// - Legacy implementation of key storage
//
// - Every key is stored both on Secure Side (Vault) and on filesystem
// - On filesystem keys are stored in different locations because of historic reasons
// - To delete the key it must be removed from Secure Side,
//   otherwise the key will be recreated on filesystem from its record on Secure Side
// - Key can be addressed on Secure side under certain "KeyAreaIndex"
// - "KeyAreaIndex" can be identified by corresponding "keyType" and "keyID"
// - Sometimes "KeyArea" is split into smaller regions
//   and key must also be addressed by "KeyIndex"
//////////////////////////////////////////////////////////////////////////////
int DeleteKey(unsigned int ucAreaIndex, unsigned int ucKeyIndex);

int  ComputeKeyAreaIndex(int keyType, int keyID, unsigned char* keyAreaIndex);
int  CheckKeyAreaIntegrity(void);
int  AllocateKeyArea (unsigned char ucAreaIndex,
                      unsigned char ucNbKeys,
                      unsigned char ucNbLoadableKeys,
                      unsigned char ucCellSize);
int  FreeKeyArea (unsigned char ucAreaIndex);
void FreeKeyType(int keyType);
int  DeleteKeyArea (unsigned char ucAreaIndex);
int  DeleteKeyArea_public (unsigned char ucAreaIndex);
int  DeleteAllKeys (unsigned char ucCondition);
int  IsKeyAreaValid (unsigned char ucAreaIndex);
int  IsKeyLoadable (unsigned char ucAreaIndex,
                    unsigned char ucKeyIndex);

int update_secmode_key_area(unsigned char area_index,
							unsigned char *keydata,
							int size);
int load_existing_key_area(int keyType, int keyID);
int load_secmode_key_area(int keyType,
						  int keyID,
						  unsigned char ucNbKeys,
						  unsigned char ucNbLoadableKeys,
						  unsigned char ucCellSize);
void DeleteAllKeyAreas(void);
int ClearKeyArea(unsigned char area_index);

//////////////////////////////////////////////////////////////////////////////
//   PROCESS KEYS ON FILESYSTEM
//
// - This functions are used to process keys that are stored only on filesystem
// - Newer implementation of key storage
// - Key storage location differs from legacy implementation
//
// - Every key is stored on filesystem under:
//   /mnt/flash/system/{engine}keys/usr{user}/key{slot}|{friendly_label}.xyz
// - To remove the key it's files must be deleted from the filesystem
//////////////////////////////////////////////////////////////////////////////
int delete_keys_from_filesystem(const char *user, const char *engine, const char *slot, const char *friendly_label);
int delete_keys_from_filesystem_for_engine(const char engine);
#endif
/*
;|===========================================================================+
;|          END   END   END   END     END    END   END   END  END  END
;|===========================================================================+
*/
