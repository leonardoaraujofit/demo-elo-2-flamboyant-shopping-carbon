/****************************************************************************
* FILE NAME:   PICC.h                                                       *
* MODULE NAME: PICC                                                         *
* PROGRAMMER:  Uri Rattner                                                  *
* DESCRIPTION:                                                              *
* REVISION:                                                                 *
****************************************************************************/
#ifndef ___PICC_H
  #define   ___PICC_H

#ifdef __cplusplus
extern "C"{
#endif

          /*==========================================*
           *         I N T R O D U C T I O N          *
           *==========================================*/
#ifdef CL_L1_NOS_BASE
  #include L1LIB_api
#endif

#ifndef CL_L1_NOS_BASE
  #ifndef VERIX
    #define CL_L1_LINUX_BASE
  #endif
#endif
          /*==========================================*
           *           D E F I N I T I O N S          *
           *==========================================*/
#define PICC_CARD_OK       1
#define PICC_NO_CARD       2
#define PICC_MULTIPLY_CARD 3
#define PICC_CARD_ERROR    4
#define PICC_IN_DATA_ERROR 5
#define PICC_NOT_SUPPORTED 6
#define PICC_PROTOCOL_ERROR  7
#define PICC_CARD_NOT_ACTIVE 8

#define NO_CARD_TYPE         0
#define ISO14443A_CARD_TYPE  1
#define ISO14443B_CARD_TYPE  2
#define MIFARE_CARD_1K_TYPE  3
#define MIFARE_CARD_UL_TYPE  4
#define MIFARE_AND_ISO14443A 7
#define MIFARE_4K_AND_ISO14443A 8
#define MIFARE_CARD_4K_TYPE  0x13
#define NFC_F_CARD_TYPE      0x20

#define MIFARE_KEY_TYPE_A    1
#define MIFARE_KEY_TYPE_B    2
#define MIFARE_KEY_SIZE      6
#define MIFARE_DATA_SIZE     16
#define MIFARE_UL_DATA_SIZE  4

#define MIFARE_RD_COMMMAND   0x30
#define MIFARE_WR_COMMMAND   0xA0
#define MIFARE_UL_WR_COMMMAND   0xA2
#define MIFARE_RD_TIMEOUT    3500      /* in us */
#define MIFARE_WR_TIMEOUT_1  1500      /* in us */
#define MIFARE_WR_TIMEOUT_2  5500      /* in us */


#define PICC_INITIATOR_MODE_BIT           0x10
#define NFC_ACTIVE_MODE_BIT     	      0x80

#define PICC_ISO14443A_CARD  ISO14443A_CARD_TYPE
#define PICC_ISO14443B_CARD  ISO14443B_CARD_TYPE
//#define PICC_TYPE_A_TARGET   (ISO14443A_CARD_TYPE|PICC_TARGET_MODE_BIT)
//#define PICC_TYPE_B_TARGET   (ISO14443B_CARD_TYPE|PICC_TARGET_MODE_BIT)

#define PICC_IS_CARD_EXIST                 0
#define PICC_GET_PARAMETERS                1
#define PICC_ISO_APDU_EXCHANGE             2
#define PICC_HALT                          3
#define PICC_RF_ANTENNA_ON                 4
#define PICC_RF_ANTENNA_OFF                5
#define PICC_MIFARE_LOAD_KEY               6
#define PICC_MIFARE_AUTHENTICATE_1         7
#define PICC_SINGLE_TRANSCEIVE             8
#define PICC_MIFARE_AUTHENTICATE_BLOCK     9
#define PICC_MIFARE_READ_BLOCKS            10
#define PICC_MIFARE_WRITE_BLOCKS           11
#define PICC_MIFARE_INCREMENT_BLOCK        12
#define PICC_MIFARE_DECREMENT_BLOCK        13
#define PICC_MIFARE_RESTORE_BLOCK          14
#define PICC_MIFARE_TRANSFER_BLOCK         15
#define PICC_POLL_WITHOUT_ACTIVATE         16
#define PICC_CARD_ACTIVATE                 17
#define PICC_NXP_CHIP_VERSION              18
#define PICC_L1NXPEX_VERSION               19
#define PICC_NFC_TAG4_EXCHANGE			   20
#define PICC_MIFARE_ERLOG_SETUP			   21	 /* in - 4 bytes of error data address, 4 bytes of error size address, 2 bytes of error logger size */
#define PICC_CARD_ACTIVATE_ATS             22
#define PICC_NFC_F_OPEN                    0x20	 /* 32 */
#define PICC_NFC_F_RELEASE                 0x21
#define PICC_NFC_F_FIELD_ON                0x22
#define PICC_NFC_F_FIELD_OFF               0x23
#define PICC_NFC_F_FIELD_RESET             0x24
#define PICC_NFC_F_FIELD_TX_RX             0x25
#define PICC_NFC_CHECK_EXTERNAL_FIELD      0x26
#define PICC_NFC_F_READ_HW_REG             0x27
#define PICC_NFC_F_WRITE_HW_REG            0x28
#define PICC_NFC_F_HW_RESET                0x29
#define PICC_NFC_F_212KBS                  0x2a
#define PICC_NFC_F_424KBS                  0x2b
#define PICC_NFC_F_CHANGE_REG_SETUP        0x2c
#define PICC_NFC_F_READ_REG_SETUP          0x2d
#define PICC_NFC_F_RESTORE_DEFAULT_SETUP   0x2e
/* Custom Polling commands */
#define PICC_CUSTOM_POLLING                0x30
/* TarnsitPay PassThough commands */	 
#define TRANSIT_PAY_OUT_CHANGE			   0x40	 /* in - 1-st byte is output bitmap for set to '0', 2-d byte is output bitmap for set to '1'*/  
#define TRANSIT_PAY_INPUT_READ			   0x41	 /* out - 1 byte, which is bitmap of input pins status */
#define TRANSIT_PAY_LED_FLASH			   0x42	 /* in - 1 byte is led out bit, 1 byte is blink number, 2 bytes is on time in ms, 2 bytes is off time in ms*/
#define TRANSIT_PAY_TURNSTILE_PULSE		   0x43	 /* in - 2 bytes is on time in ms*/
/* QX700 PassThough commands */	 		   
#define QX700_DS2780_DATA_READ			   0x50	 /* out - 11 of most bits of first 2 bytes is temperature in 0.125 C units with sign */
												 /* out - 11 of most bits of second 2 bytes is battery volage  in 4.88 mV units with sign */
#define PICC_NP512_POWER_DOWN_ENABLE	   0x51	 /*	Set PN512 chip to power down mode. If the feature not supported by L1, return PICC_NOT_SUPPORTED. In/Out parateters are 0.*/
#define PICC_NP512_POWER_DOWN_DISABLE	   0x52	 /*	Set PN512 chip to normal power mode. If the feature not supported by L1, return PICC_NOT_SUPPORTED. In/Out parateters are 0.*/
#define PICC_READ_NP512_ERROR_TRACE		   0x53	 /* out - 16 bytes of last status of PN512 ErrorReg. If the feature not supported by L1, return PICC_NOT_SUPPORTED.*/

/* LCD mode setting command (for MX925) */  
#define PICC_NP512_LCD_MODE_SET     	   0x60  /* in - 1 byte is LCD mode for MX925 */  
#define PICC_NFC_F_LCD_DEFAULT_MODE_SET    0x61  /* in - 1 byte is LCD mode for MX925 type F */  
#define PICC_NFC_F_LCD_CURRENT_MODE_SET    0x62  /* in - 1 byte is LCD mode for MX925 type F */  



/* Command for PICC_L1_Analog_Test()  */
#define NXP512_SET_ANALOG_TEST_REG         0x81  /* Set byte to AnalogTestReg */
#define NXP512_SET_POWER_DOWN_MODE         0x82
#define NXP512_SET_NORMAL_POWER_MODE       0x83
#define NXP512_GET_ERROR_REG_TRACER        0x84



/* TarnsitPay output pins */	 
#define TRANSIT_PAY_TURNSTILE_OUT          0x01
#define TRANSIT_PAY_GREEN_LED_OUT          0x02
#define TRANSIT_PAY_RED_LED_OUT            0x04
#define TRANSIT_PAY_IO_OUT                 0x08
#define TRANSIT_PAY_VOLUME_OUT             0x40
/* TarnsitPay input pins */
#define TRANSIT_PAY_SWITCH_IP_INPUT        0x01
#define TRANSIT_PAY_IO_INPUT               0x02



/*---------- Mifare error logger records names and format --------*/
#define MFLOG_TX						   'T'	/* T <3 bytes of time in us> <1 bytes of Tx data length> <Tx data> */
#define MFLOG_RX						   'R'	/* R <3 bytes of time in us> <1 bytes of error code> <0 or nuber bit in last byte> <Rx data length> <Rx data> */
#define MFLOG_AUTHEN_TX					   'S'	/* S <3 bytes of time in us> <10 bytes of data: key type, block num, key, card UID> */
#define MFLOG_AUTHEN_RESULT				   'Q'	/* Q <3 bytes of time in us> <Authenticate result: ERR_NO_ERROR, ERR_NOCARD or ERR_PROTOCOL_ERROR*/
#define MFLOG_HALT						   'H'	/* H <3 bytes of time in us> */
#define MFLOG_FIELD_ON					   'X'	/* X <3 bytes of time in us> */ 
#define MFLOG_FIELD_OFF					   'Y'  /* Y <3 bytes of time in us> */

/*---------- Bit map params for PICC_POLL_WITHOUT_ACTIVATE command --------*/
/* If bit PICC_POLL_ENABLE=0, don't poll this card type.*/
#define PICC_POLL_ENABLE_A              0x00000001
#define PICC_POLL_ENABLE_B              0x00000002
#define PICC_POLL_ENABLE_F              0x00000004
#define PICC_POLL_ENABLE_D              0x00000008
/* Make the collision resolve for this card type, if more as one cards*/
#define PICC_COLLISION_IF_MULTI_A       0x00000010
#define PICC_COLLISION_IF_MULTI_B       0x00000020
#define PICC_COLLISION_IF_MULTI_F       0x00000040
#define PICC_COLLISION_IF_MULTI_D       0x00000080
/* Make the HALT command after polling for this card type, if one of conditions is TRUE */
/* If some card type is more as one, make the HALT for this type always */ 
/* If it is condition, which request Field Reset after polling, make Reset */ 
/* If some card type has error and can not be activate, but it is not  */
/* Reset condition, make the HALT command */ 
#define PICC_HALT_A_IF_OTHER_TYPE       0x00000100
#define PICC_HALT_A_ALWAYS              0x00000200
#define PICC_HALT_B_IF_OTHER_TYPE       0x00000400
#define PICC_HALT_B_ALWAYS              0x00000800
#define PICC_HALT_F_IF_OTHER_TYPE       0x00001000
#define PICC_HALT_F_ALWAYS              0x00002000
#define PICC_HALT_D_IF_OTHER_TYPE       0x00004000
#define PICC_HALT_D_ALWAYS              0x00008000
/* Make the Field Reset after polling, if one of conditions is TRUE */
#define PICC_RESET_IF_A_ERROR           0x00010000
#define PICC_RESET_IF_A_AND_OTHER       0x00020000
#define PICC_RESET_IF_B_ERROR           0x00040000
#define PICC_RESET_IF_B_AND_OTHER       0x00080000
#define PICC_RESET_IF_F_ERROR           0x00100000
#define PICC_RESET_IF_F_AND_OTHER       0x00200000
#define PICC_RESET_IF_D_ERROR           0x00400000
#define PICC_RESET_IF_D_AND_OTHER       0x00800000
/* Make the Field Reset after polling, if one of conditions is TRUE */
#define PICC_SECOND_POLL_AFTER_ERROR    0x01000000

/*---------------------------------------------------------------------------*/

/*-------------- Bit map params for PICC_CARD_ACTIVATE command --------------*/
#define PICC_ACTIVATE_CARD_INDX_MASK     0x00000007
#define PICC_ACTIVATE_TYPE_MASK          0x00000030
#define PICC_ACTIVATE_TYPE_A             0
#define PICC_ACTIVATE_TYPE_B             0x00000010
/* Bit map params for PICC_CARD_ACTIVATE command for Type A only */
 /* Don't Send RATS during type activate */
#define PICC_RATS_DISABLE                0x00000100
#define PICC_WUP_BEFORE_ACTIVATE         0x00000200
#define PICC_ANY_TYPE_B_ACTIVATE         0x00000400
/*---------------------------------------------------------------------------*/

/*------ LCD setting parameters for MX925 by PICC_NP512_LCD_MODE_SET command ------*/  
#define PICC_LCD_STOP_IN_POLL_AND_TxRx	    (PICC_LCD_STOP_IN_POLL_ONLY|PICC_LCD_STOP_IN_TxRx_ONLY)
#define PICC_LCD_STOP_IN_POLL2_AND_TxRx	    (PICC_LCD_STOP_IN_POLL2_ONLY|PICC_LCD_STOP_IN_TxRx_ONLY)
#define PICC_LCD_SYNC_IN_POLL_STOP_IN_TxRx	(PICC_LCD_SYNC_IN_POLL_ONLY|PICC_LCD_STOP_IN_TxRx_ONLY)
#define PICC_LCD_STOP_IN_TxRx_ONLY			 0x01
#define PICC_LCD_SYNC_IN_POLL_ONLY			 0x02
#define PICC_LCD_STOP_IN_POLL_ONLY			 0x04
#define PICC_LCD_STOP_IN_POLL2_ONLY			 0x08  /* make second polling with LCD stop, if first polling with card error*/
#define PICC_LCD_MODE_NOT_USED				 0x00
#define PICC_LCD_IN_POLL_MASK			     0x0e  /* make second polling with LCD stop, if first polling with card error*/
/*------ LCD setting parameters for MX925 by PICC_PassThrow for type F ------*/  
#define PICC_NFC_F_LCD_STOP_IN_TxRx			 0x01
#define PICC_NFC_F_LCD_SYNC_IN_TxRx			 0x02

#define NXP_LCD_DEFAULT_MODE_ADR	    0xff
#define NXP_LCD_CURRENT_MODE_ADR	    0xfe

#define PBOC_L1_MODE_ON		  0xAA
#define PBOC_L1_MODE_OFF	  0xD5
/*---------------------------------------------------------------------------*/


#define PICC_RF_FIELD_OFF    0xA32B

typedef struct
{
    byte     NumOfCardsA;
    byte     ErrorOfCardsA;  /* by CL_Defines.def */ 
    byte     NumOfCardsB;
    byte     ErrorOfCardsB;  /* by CL_Defines.def */ 
    byte     NumOfCardsC;
    byte     ErrorOfCardsC;  /*TBD*/
    byte     NumOfCardsD;
    byte     ErrorOfCardsD;  /*TBD*/
    void     *CollisionParamA;       /*ISO14443A_CollisionDataStr*/
    void     *CollisionParamB;       /*TBD*/
    void     *CollisionParamC;       /*TBD*/
    void     *CollisionParamD;       /*TBD*/
}PiccPollResultStr;

#ifdef CL_L1_LINUX_BASE	 
typedef struct
#else
typedef packed struct
#endif
{
  byte  UID[16];
  byte  UID_Len;
  byte  CardType;      /*Type A General, Type A Mifare 1kbyte, ....*/
  byte  CID;           /* by PayPass always 0 */
  byte  CID_Support;   /* now not supported */
  byte  SAK;           /* for type A only */
  byte  NAD;           /* not supported      */
  byte  BlockNum;
  byte  BaudRate;     /* indicating 106 kbits/s baud from and to the PICC */
  usint CardBufSize;   /* max frame size which the Card can handle  */
  usint ReaderBufSize; /* max frame size which the Reader can handle */
  ulint FWT;          /* frame waiting time in ETU */
  ulint D_FWT;        /* delta FWI - frame waiting time in ETU */
  ulint FWT_us;       /* frame waiting time in microsec. */
}
PiccParamStruct;

typedef struct
{
  byte  *DataFromCard; /*   response data, no NULL!!!!!                                 */
  byte  *DataToCard;   /*   (in a command)                                              */
  usint Le;            /*   (in a command) Max length of DataFromCard (length expected) */
  usint Lr;            /*   (Nowhere) Actual length of DataFromCard                     */
  usint SW1_SW2;       /*   (in a response)                                             */
  byte  Lc;            /*   (in a command) Length of DataToCard  (length of command)    */
  byte  Class;         /*   (in a command: xxxxxx--)                                    */
  byte  Instruct;      /*   (in a command)                                              */
  byte  P1;            /*   (in a command)                                              */
  byte  P2;            /*   (in a command)                                              */
  byte  card_type;     /*   ISO14443A_CARD_TYPE or ISO14443B_CARD_TYPE                  */
}
PICC_IccInstructStruct;

typedef struct
{
  unsigned char CardType;
  unsigned char CardSubType; /*Type A General, Type A Mifare 1kbyte, ....*/
  boolean       Active;
  byte          BlockNum;
  byte          MifareKeyType;
  usint         MaxFrameSize;
  usint         RFStatus; /* holds the status of the RF (off when equal to PICC_RF_FIELD_OFF, on otherwise ) */
  byte          MifareKey[MIFARE_KEY_SIZE];
}
PICC_struct;


           /*=========================================*
            *   P U B L I C     F U N C T I O N S     *
            *=========================================*/


#ifdef CL_L1_LINUX_BASE
/* --------------------------------------------------------------------------
* FUNCTION NAME: PICC_Open
* DESCRIPTION:   Init and open driver.
* RETURN:        CL L1 Driver Handler, if OK or -1, if error.
* NOTES:         none.
* ------------------------------------------------------------------------ */
int PICC_Open(void);
unsigned char PICC_SetLcdMode(byte mode);
byte PICC_GetLcdMode(void);

/* --------------------------------------------------------------------------
* FUNCTION NAME: PICC_Close
* DESCRIPTION:   Init and open driver.
* RETURN:        0, if OK, or -1, if error.
* NOTES:         none.
* ------------------------------------------------------------------------ */
int PICC_Close(int DriverHandler);

/* --------------------------------------------------------------------------
* FUNCTION NAME: PICC_IOCommands
* DESCRIPTION:   Send command to HW level.
* RETURN:        void
* NOTES:         none.
* ------------------------------------------------------------------------ */
void PICC_IOCommands(byte Commamd, int Data);

#endif

/* --------------------------------------------------------------------------
* FUNCTION NAME: PICC_IsCardPresent.
*
* DESCRIPTION:   This function checks whether a contactless card is present
*                and can be activated.
*
* PARAMETERS:    none
*
* RETURN:        PICC_CARD_OK          - Card was found and was activated.
*                PICC_CARD_ERROR       - Error in card activation.
*                PICC_NO_CARD          - No card was found.
*                PICC_MULTIPLY_CARD    - More than one card was found (collision).
*
* NOTES:         By PayPass - ISO/IEC 14443 Implementation Specification,
*                Figure 9.2 - Polling and Collision Detection.
* ------------------------------------------------------------------------ */
unsigned char PICC_IsCardPresent(void);

/* --------------------------------------------------------------------------
* FUNCTION NAME: PICC_LastCommandStatus
*
* DESCRIPTION:   This function return the status of last previous PICC_Command()
*                function. RxLen is relevant after previous PICC_Command() without
*                error. Else RxLen=0. RxLen includes all received data and SW1/2.  
*
* RETURN:        error status of last previous PICC_Command():
*                PICC_CARD_OK          - no error.
*                PICC_CARD_ERROR       - communication error (CRC, parity, ....).
*                PICC_NO_CARD          - no response from card.
*                PICC_MULITPLY_CARD    - collision error during response receiving.
*                PICC_PROTOCOL_ERROR   - error in protocol format of response.
*                PICC_CARD_NOT_ACTIVE  - card not activated before PICC_Command().
*                PICC_IN_DATA_ERROR    - error in PICC_Command() parameters.
*
* NOTES:         none.
* ------------------------------------------------------------------------ */
unsigned char  PICC_LastCommandStatus(usint* RxLen);

/* --------------------------------------------------------------------------
* FUNCTION NAME: PICC_Command
*
* DESCRIPTION:   This function sends a command to a card which was previously
*                activated by PICC_IsCardPresent function and receives
*                the card response.
*
* PARAMETERS:    PICC_IccInstructStruct Cmd:
*                   byte* DataFromCard 	- Pointer to response data (retrieved from the card).  Must not be NULL.
*                   byte* DataToCard		- Pointer to command data (passed to the card).
*                   usint Le;	          - Length expected:  Maximum length of DataFromCard buffer.
*                   usint Lr;	          - Actual length of DataFromCard  which was read.
*                   usint SW1_SW2;      - Response data status bytes.
*                   byte  Lc;           - Length of DataToCard  (length of command).
*                   byte  Class;        - (in a command)
*                   byte  Instruct;     - (in a command)
*                   byte  P1;           - (in a command)
*                   byte  P2;           - (in a command)
*                   byte  card_type;    -  ISO14443A_CARD_TYPE or ISO14443B_CARD_TYPE.
*
* RETURN:        TRUE   - Send succeeded.
*                FALSE  - Send failed.
*
* NOTES:         none.
* ------------------------------------------------------------------------ */
boolean PICC_Command(PICC_IccInstructStruct* Cmd);

/* --------------------------------------------------------------------------
* FUNCTION NAME: PICC_CardRemoval
*
* DESCRIPTION:   This function deactivates the card, resets the RF field and
*                waits until the card is removed.
*
* RETURN:        none.
*
* NOTES:         By chapter 9.5 - ISO/IEC 14443 Implementation Specification v1.1
* ------------------------------------------------------------------------ */
void PICC_CardRemoval(void);

/* --------------------------------------------------------------------------
* FUNCTION NAME: PICC_Reset
*
* DESCRIPTION:   Turns off the RF field.
*
* RETURN:        none.
*
* NOTES:         none.
* ------------------------------------------------------------------------ */
void PICC_Reset(void);

/* --------------------------------------------------------------------------
* FUNCTION NAME: PICC_GetVersion.
*
* DESCRIPTION:   This function gets the reader L1 software version.
*
* RETURN:        none.
*
* NOTES:         none.
* ------------------------------------------------------------------------ */
#ifndef CL_L1_LINUX_BASE
  void PICC_GetVersion (LipMinVerStr *version);
#else
  void PICC_GetVersion (char *version);
#endif

/* --------------------------------------------------------------------------
* FUNCTION NAME: PICC_GetCardType
* DESCRIPTION:   Return the type of the card which was activated.
* RETURN:        none.
* NOTES:         none.
* ------------------------------------------------------------------------ */
byte PICC_GetCardType(void);

/* --------------------------------------------------------------------------
* FUNCTION NAME: PICC_LED.
* DESCRIPTION:   Controls the reader's LEDs.
* RETURN:        none.
* NOTES:         none.
* ------------------------------------------------------------------------ */
void PICC_LED (byte LEDMap);

/* --------------------------------------------------------------------------
* FUNCTION NAME: PICC_FieldOn.
* DESCRIPTION:
* RETURN:        none.
* NOTES:         none.
* ------------------------------------------------------------------------ */
void PICC_FieldOn(void);

/* --------------------------------------------------------------------------
* FUNCTION NAME: PICC_FieldOff.
* DESCRIPTION:
* RETURN:        none.
* NOTES:         none.
* ------------------------------------------------------------------------ */
void PICC_FieldOff(void);

/* --------------------------------------------------------------------------
* FUNCTION NAME: PICC_PassThrow
* DESCRIPTION:
* RETURN:        error code.
* NOTES:         none.
* ------------------------------------------------------------------------ */
unsigned char PICC_PassThrow(byte Command,
                             void* InData, usint *InDataLen,
                             void* OutData, usint *OutDataLen);

/* --------------------------------------------------------------------------
* FUNCTION NAME: PICC_Polling.
* DESCRIPTION:   This function checks whether a contactless card is present 
*                by polling params.
* PARAMETERS:    Polling process parameters as bit map.
* OUTPUT:        Polling result as PiccPollResultStr   
* RETURN:        PICC_CARD_OK          - Minimum one card was found and 
*                                        can be activated.
*                PICC_NO_CARD          - No card was found.
*                PICC_MULITPLY_CARD    - More than one card was found and the 
*                                        collision resolve is disable or 
*                                        some card error was found and no card
*                                        can be activated. Make RF reset
*                                        by polling params.
* ------------------------------------------------------------------------ */
unsigned char PICC_Polling(ulint PollParam,PiccPollResultStr *Res);


#ifdef L1_CARD_SIMULATE_SUPPORT
/* --------------------------------------------------------------------------
* FUNCTION NAME: PICC_ListenB
* DESCRIPTION:   Wait WUPB and ATTRIB command.
* PARAMETERS:	 ToutMs - in ms
*                ATQB_TargetData - PUPI (4bytes), Appl.Info (4bytes), Protocol Info (3bytes).
*				 PcdApplData  - Higher layer data received in ATTRIB command
*				 PiccApplData - Higher layer data must be sent in ATTRIB responce.
* RETURN:        PICC_CARD_OK          - Card was found and was activated.
*                PICC_CARD_ERROR       - Error in card activation.
*                PICC_NO_CARD          - No card was found.
* NOTES:         none.
* ------------------------------------------------------------------------ */
unsigned char PICC_ListenB(ulint ToutMs,const byte *ATQB_TargetData,byte *PcdApplData,ulint *PcdDataLen,
                           const byte *PiccApplData,ulint PiccDataLen);
#endif  /*#ifdef L1_CARD_SIMULATE_SUPPORT*/

/* --------------------------------------------------------------------------
* FUNCTION NAME: PICC_ListenA
* DESCRIPTION:   Wait WUPA command.
* PARAMETERS:	 ToutMs - in ms
* RETURN:        PICC_CARD_OK          - Card was found and was activated.
*                PICC_CARD_ERROR       - Error in card activation.
*                PICC_NO_CARD          - No card was found.
* NOTES:         none.
* ------------------------------------------------------------------------ */
unsigned char PICC_ListenA(ulint ToutMs);


#if defined (__NOS__) || defined (VERIX) || defined (LINUXCL1DRV)
  #include picc_nos
#endif

#ifdef __cplusplus
}       /* extern "C" */
#endif

#endif /* #ifdef ___PICC_H*/
