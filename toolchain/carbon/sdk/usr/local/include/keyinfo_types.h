
#ifndef __KEYINFO_TYPES_H__
#define __KEYINFO_TYPES_H__

// constants

#define	KEYINFO_API_VERSION				1

/**
 * @name Key information type
 */
enum {
	KEY_INFO_TYPE_INVALID				= 0,
	KEY_INFO_TYPE_RSA					= 1,
	KEY_INFO_TYPE_ECC					= 2
};

/**
 * @name Crypto engine types
*/
/** \{ */
enum {
	KEY_ENGINE_IPP_DUKPT				= 'd',
	KEY_ENGINE_IPP_MS					= 'm',
	KEY_ENGINE_VSS						= 'v',
	KEY_ENGINE_VSS_KLK					= 'k',
	KEY_ENGINE_RSA_KEY					= 'r',
	KEY_ENGINE_ECC_KEY					= 'e',
	KEY_ENGINE_VSP						= 'p',
	KEY_ENGINE_ADE_DUKPT        		= 'a',
	KEY_ENGINE_USR						= 'u',
	KEY_ENGINE_OTHER					= 'o'
};
/** \} */

#define RSA_TIMESTAMP_SIZE				14	// YYYYMMDDhhmmss

// structures

/*SVC_STRUCT*/
/**
 * Key info record
 */
struct keyRSAinfo {
	int user_id;						/**< USER ID 0 ... 9 */
	int key_id;							/**< KEY  ID 0 ... 9 */
	int status;							/**< KEYs status */
	char type;							/**< Key type. U - User(uKey). R - System(rKey), V- VRK */
	char ts[RSA_TIMESTAMP_SIZE + 1];	/**< Sertificat's timestamp */
};

/**
 * Security key information
 */
struct securityKey {
	struct securityKey *next;			/**< pointer to next item in linked list */
	int version;						/**< API version */
	long int install_date;				/**< date of installaton (typedef long int time_t;) */
	const char *name;					/**< friendly name */
	const char *file_path;				/**< path to key file */
	const char *file_path_sha256;		/**< path to SHA256 file */
	const char *serial_number;			/**< serial number of the key */
	int info_type;						/**< type of the key specific information */
	union {
		struct keyRSAinfo rsa;
		/* struct keyECCinfo ecc; */
	} *info;							/**< key specific information */
};

/**
 * TR-31/34 key block header
 */
struct tr31kbh {
	char block_version;					/**< key block version ID */
	int block_length;					/**< key block length */
	short key_usage;					/**< key usage */
	char algorithm;						/**< algorithm */
	char mode;							/**< mode of use */
	short key_version;					/**< key version number */
	char exportability;					/**< exportability */
	short optional_blocks;				/**< number of optional blocks */
	short reserved;						/**< reserved for future use */
};

/**
 * Payment key information
 */
struct paymentKey {
	struct paymentKey *next;			/**< pointer to next item in linked list */
	int version;						/**< API version */
	long int install_date;				/**< date of installaton (typedef long int time_t;) */
	const char *name;					/**< friendly name */
	int instance_id;					/**< instance ID of an engine is currently supported */
	int engine_id;						/**< crypto engine type ID */
	int slot_id;						/**< slot ID */
	int user_id;						/**< user ID */
	int status;							/**< status of the key (1-good, 0-bad) */
	const char *key_check;				/**< cryptograpic checksum for the key */
	const struct tr31kbh *kbh;			/**< TR-31/34 key block header */
};

#endif
