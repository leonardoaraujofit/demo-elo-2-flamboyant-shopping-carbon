#ifndef __SYSM_INFO__
#define __SYSM_INFO__

#include <sysmode_ui/sysm_ui.h>
#include <sysmode_ui/sysm_lang.h>

/*!
    The mask is used to filter the menu items. Some 
    functionality should be available only to certain
    devices, but not to all.
    NOTE: Mask supports 16 devices. If more required,
          Then the filter in sysm_menu.cc should be
          analyzed. If no more users exists, then
          1 more byte can be added to store device
          mask 
*/
typedef enum {
    MSK_UNKNOWN         = 0x00000,
    MSK_ETH             = 0x00001,
    MSK_WIFI            = 0x00002,
    MSK_GPRS            = 0x00004,
    MSK_BT              = 0x00008,
    MSK_SPEAKER         = 0x00010,
    MSK_BATTERY         = 0x00020,
    MSK_TOUCH           = 0x00040,
    MSK_PRINTER         = 0x00080,   // Local printer
    MSK_MODEM           = 0x00100,
    MSK_DEVELOPMENT     = 0x00200,   // For development menus only
    MSK_MANUFACTURE     = 0x00400,   // For manufacturing
    MSK_VATS            = 0x00800,
    MSK_EXTENSIONS      = 0x01000,
    MSK_PINPAD          = 0x02000,
    MSK_SD              = 0x04000,
    MSK_NOT_PROD        = MSK_DEVELOPMENT | MSK_MANUFACTURE,
    MSK_3G	            = 0x08000,
    MSK_DEV_ALL         = 0x1FFFF,
    MSK_GPS             = 0x10000,
    MSK_CNT             = 0x20000,
    MSK_DWC3_USB        = 0x40000,
    MSK_IBEACON         = 0x80000,
    MSK_GEOLOC          = 0x100000,
    MSK_CTLS      	    = 0x200000,
} DEVICE_MASK;

DEVICE_MASK get_device();

void info_basic_cb(const SYSM_UI::t_ui_menu_entry *);
void info_system_cb(const SYSM_UI::t_ui_menu_entry *);
void info_ports_cb(const SYSM_UI::t_ui_menu_entry *);
void info_sw_cb(const SYSM_UI::t_ui_menu_entry *);
void info_modem_cb(const SYSM_UI::t_ui_menu_entry *);
void info_memory_cb(const SYSM_UI::t_ui_menu_entry *);
void info_cntrs_cb(const SYSM_UI::t_ui_menu_entry *);
void info_pinpad_cb(const SYSM_UI::t_ui_menu_entry *);
void info_firmware_cb(const SYSM_UI::t_ui_menu_entry *);
void info_battery_connected();
void init_devmask();

#endif // __SYSM_INFO__
