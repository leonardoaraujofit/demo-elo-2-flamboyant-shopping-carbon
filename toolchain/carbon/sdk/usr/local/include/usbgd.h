
/*
 * usbgd.h
 * Copyright (c) 2015 Verifone Inc.
 *
 * All Rights Reserved. No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or
 * translated into any language or computer language, in any form
 * or by any means electronic, mechanical, magnetic, optical,
 * chemical, manual or otherwise, without the prior permission of
 * VeriFone Inc.
 */

#ifndef	USBGD_H
#define	USBGD_H

#ifdef __cplusplus
extern "C" {
#endif


#define USBGD_STR_LEN			128
#define USBGD_SERVER_PATH		"/tmp/.usbgd_server"
#define USBGD_MAX_INST			8
#define USBGD_NUM_FUNCTIONS		8
#define USBGD_FUNCTION_MASK		0x0FF

#define USBGD_MAX_ACM			4
#define USBGD_MAX_RNDIS			1
#define USBGD_MAX_CONFIG_FUNCTIONS	7

#define SVC_NET_LIBRARY_LOCATION	"/usr/local/lib/svcmgr/libsvc_net.so"
#define XML_ATTRIB_INT			1

#define G_ACM                   0x002
#define G_ECM                   0x008
#define G_NCM                   0x020
#define G_EEM                   0x040
#define G_RNDIS                 0x080

enum usbgd_error {
	USBGD_SUCCESS = 0,
	USBGD_ERROR_NO_MEM = -1,
	USBGD_ERROR_NO_ACCESS = -2,
	USBGD_ERROR_INVALID_PARAM = -3,
	USBGD_ERROR_NOT_FOUND = -4,
	USBGD_ERROR_IO = -5,
	USBGD_ERROR_EXIST = -6,
	USBGD_ERROR_NO_DEV = -7,
	USBGD_ERROR_BUSY = -8,
	USBGD_ERROR_NOT_SUPPORTED = -9,
	USBGD_ERROR_PATH_TOO_LONG = -10,
	USBGD_ERROR_INVALID_FORMAT = -11,
	USBGD_ERROR_MISSING_TAG = -12,
	USBGD_ERROR_INVALID_TYPE = -13,
	USBGD_ERROR_INVALID_VALUE = -14,
	USBGD_ERROR_RECV = -15,
	USBGD_ERROR_SEND = -16,
	USBGD_ERROR_OPEN = -17,
	USBGD_ERROR_SYMLINK = -18,
	USBGD_ERROR_READ = -19,
	USBGD_ERROR_PARSE_CONFIG = -20,
	USBGD_ERROR_STR = -21,
	USBGD_ERROR_MASK = -22,
	USBGD_ERROR_UNKNOWN = -23,
	USBGD_ERROR_OTHER_ERROR = -99
};

static const char *usbgd_error_strings[] = {
	"Success",
	"Out of memory",
	"Permission denied",
	"Invalid argument",
	"Not found (file or directory removed)",
	"Input/output error",
	"Already exist",
	"No such device (illegal device name)",
	"Busy (gadget enabled)",
	"Function not supported",
	"Created path was too long to process it",
	"Given file has incompatible format",
	"One of mandatory tags is missing",
	"One of attributes has incompatible type",
	"Incorrect value provided as attribute",
	"Receive error",
	"Send error",
	"Unable to open file",
	"Unable to create symlink",
	"Unable to read file",
	"Unable to parse config",
	"String error",
	"Wrong mask error",
	"Unknown Error",
	"Other error",
};

static const char *usbgd_error_names[] = {
	"USBGD_SUCCESS",
	"USBGD_ERROR_NO_MEM",
	"USBGD_ERROR_NO_ACCESS",
	"USBGD_ERROR_INVALID_PARAM",
	"USBGD_ERROR_NOT_FOUND",
	"USBGD_ERROR_IO",
	"USBGD_ERROR_EXIST",
	"USBGD_ERROR_NO_DEV",
	"USBGD_ERROR_BUSY",
	"USBGD_ERROR_NOT_SUPPORTED",
	"USBGD_ERROR_PATH_TOO_LONG",
	"USBGD_ERROR_INVALID_FORMAT",
	"USBGD_ERROR_MISSING_TAG",
	"USBGD_ERROR_INVALID_TYPE",
	"USBGD_ERROR_INVALID_VALUE",
	"USBGD_ERROR_RECV",
	"USBGD_ERROR_SEND",
	"USBGD_ERROR_OPEN",
	"USBGD_ERROR_SYSMLINK",
	"USBGD_ERROR_READ",
	"USBGD_ERROR_PARSE_CONFIG",
	"USBGD_ERROR_STR",
	"USBGD_ERROR_MASK",
	"USBGD_ERROR_UNKNOWN",
	"USBGD_ERROR_OTHER_ERROR",
};

static const char *usbgd_function_names[] = {
	"gser",
	"acm",
	"obex",
	"ecm",
	"geth",
	"ncm",
	"eem",
	"rndis",
	"phonet",
	"ffs",
	NULL,
};

enum usbgd_cmd_number {
	USBGD_LOAD_GADGET = 0,
	USBGD_SET_DEFAULT_CONFIG,
	USBGD_GET_CURRENT_CONFIG,
	USBGD_GET_DEFAULT_CONFIG,
};

struct usbgd_cmd {
	int cmd;
	unsigned int mask;
	int ret;
};

static inline const char *usbgd_strerror(int err)
{
	/* This specific case is used to keep compatibility with libusbg */
	if (err == USBGD_ERROR_OTHER_ERROR)
		return usbgd_error_strings[-USBGD_ERROR_UNKNOWN + 1];
	else if ((err < USBGD_ERROR_UNKNOWN || err > 0))
		return usbgd_error_strings[-USBGD_ERROR_UNKNOWN];

	return usbgd_error_strings[-err];
}

static inline const char *usbgd_error_name(int err)
{
	/* This specific case is used to keep compatibility with libusbg */
	if (err == USBGD_ERROR_OTHER_ERROR)
		return usbgd_error_names[-USBGD_ERROR_UNKNOWN + 1];
	else if ((err < USBGD_ERROR_UNKNOWN || err > 0))
		return usbgd_error_names[-USBGD_ERROR_UNKNOWN];

	return usbgd_error_names[-err];
}

/*
 * TODO:
 * Specific code to parse code from config, will disapear into another include
 * in the future
 */
struct xml_parse_attrs {
            char *name; /* tag of the attribute */
            void *value; /* value of the attribute */
            int value_count; /* maximum number of value storage */
	    int type; /* type of the attribute */
};


#ifdef __cplusplus
}
#endif

#endif /* USBGD_H */
