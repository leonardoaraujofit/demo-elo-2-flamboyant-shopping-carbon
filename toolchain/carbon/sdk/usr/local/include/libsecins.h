
#ifndef _LIBSECINS_H__
#define _LIBSECINS_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/types.h>
#include <verifone/vfsys_defs.h>
#include <inttypes.h>

/**
 * @name libsecins return error codes
 */
/** \{ */
/** [SECINS_ERROR_CODES] */
#define ERROR_SECINS                        100                  /**< Base error code. All error RC are: ERROR_SECINS + <x> */
#define ERROR_SECINS_NO_MEMORY              (ERROR_SECINS + 0)   /**< Memory allocation failure */
#define ERROR_SECINS_MKDIR                  (ERROR_SECINS + 1)   /**< Failed to make directory */
#define ERROR_SECINS_FILE_AUTH              (ERROR_SECINS + 2)   /**< Failed to authenticate package */
#define ERROR_SECINS_MOUNT_RFS              (ERROR_SECINS + 3)   /**< Failed to mount file system */
#define ERROR_SECINS_INVALID_INIT_FILE      (ERROR_SECINS + 4)   /**< System init file not found */
#define ERROR_SECINS_PATHNAME_TOO_LONG      (ERROR_SECINS + 5)   /**< Pathname too long */
#define ERROR_SECINS_CHROOT                 (ERROR_SECINS + 6)   /**< Switch root operation failed */
#define ERROR_SECINS_EXEC_INIT              (ERROR_SECINS + 7)   /**< Failed to run the system init file */
#define ERROR_SECINS_OPENDIR                (ERROR_SECINS + 8)   /**< Failed to open directory */
#define ERROR_SECINS_RENAME_DIR             (ERROR_SECINS + 9)   /**< Failed to rename directory */

#define ERROR_SECINS_CHMOD                  (ERROR_SECINS + 10)  /**< Failed to set file permissions */
#define ERROR_SECINS_CHOWN                  (ERROR_SECINS + 11)  /**< Failed to set file ownership */
#define ERROR_SECINS_FILE_CREATE            (ERROR_SECINS + 12)  /**< Failed to create file */
#define ERROR_SECINS_FILE_OPEN              (ERROR_SECINS + 13)  /**< Failed to open file */
#define ERROR_SECINS_FILE_WRITE             (ERROR_SECINS + 14)  /**< Failed to write file */
#define ERROR_SECINS_NO_SIG                 (ERROR_SECINS + 15)  /**< No package signature file */
#define ERROR_SECINS_NO_PKG                 (ERROR_SECINS + 16)  /**< No matching package file with signature */
#define ERROR_SECINS_INVALID_FILENAME       (ERROR_SECINS + 17)  /**< Invalid p7s signature filename */
#define ERROR_SECINS_INVALID_INSTALL        (ERROR_SECINS + 18)  /**< Installed package is invalid */
#define ERROR_SECINS_INVALID_INI_SECTION    (ERROR_SECINS + 19)  /**< Invalid ini file section */

#define ERROR_SECINS_MKNOD                  (ERROR_SECINS + 20)  /**< Filed to create device node */
#define ERROR_SECINS_INVALID_INI_PROPERTY   (ERROR_SECINS + 21)  /**< Invalid ini file property */
#define ERROR_SECINS_NO_CONTROL_FILE        (ERROR_SECINS + 22)  /**< No control file in package */
#define ERROR_SECINS_INVALID_CONTROL_FILE   (ERROR_SECINS + 23)  /**< Invalid control file in package */
#define ERROR_SECINS_CREATE_CONTROL_FILE    (ERROR_SECINS + 24)  /**< Failed to install control file */
#define ERROR_SECINS_OLD_PKG_VERSION        (ERROR_SECINS + 25)  /**< More up-to-date package already installed */
#define ERROR_SECINS_INVALID_PKG_TYPE       (ERROR_SECINS + 26)  /**< Invalid package type */
#define ERROR_SECINS_EXTRACT_PKG_CONTROL    (ERROR_SECINS + 27)  /**< Failed to extract pkg CONTROL dir */
#define ERROR_SECINS_EXTRACT_PKG            (ERROR_SECINS + 28)  /**< Failed to extract package archive */
#define ERROR_SECINS_INVALID_PKG            (ERROR_SECINS + 29)  /**< Invalid package */

#define ERROR_SECINS_PKG_LIST               (ERROR_SECINS + 30)  /**< Failed to read pkg contents */
#define ERROR_SECINS_PKG_FILENAME_TOO_LONG  (ERROR_SECINS + 31)  /**< Package filename too long */
#define ERROR_SECINS_INSTALL_CONTROL_FILE   (ERROR_SECINS + 32)  /**< Failed to install pkg control file */
#define ERROR_SECINS_INSTALL_PKG            (ERROR_SECINS + 33)  /**< Failed to install pkg */
#define ERROR_SECINS_INSTALL_SIG            (ERROR_SECINS + 34)  /**< Failed to install pkg signature */
#define ERROR_SECINS_PKG_NOT_FOUND          (ERROR_SECINS + 35)  /**< Incomplete pkg install */
#define ERROR_SECINS_ADD_GROUP              (ERROR_SECINS + 36)  /**< Failed to add group */
#define ERROR_SECINS_ADD_USER               (ERROR_SECINS + 37)  /**< Failed to add user */
#define ERROR_SECINS_ADD_SHADOW             (ERROR_SECINS + 38)  /**< Failed to add shadow password entry */
#define ERROR_SECINS_GET_USER               (ERROR_SECINS + 39)  /**< User not found */

#define ERROR_SECINS_GET_GROUP              (ERROR_SECINS + 40)  /**< Group not found */
#define ERROR_SECINS_PROCESS_FORK           (ERROR_SECINS + 41)  /**< */
#define ERROR_SECINS_CHECK_GRSEC            (ERROR_SECINS + 42)  /**< Grsecuity policy check failed */
#define ERROR_SECINS_START_GRSEC            (ERROR_SECINS + 43)  /**< Failed to enable Grsecurity */
#define ERROR_SECINS_FORK                   (ERROR_SECINS + 44)  /**< Failed to create child process */
#define ERROR_SECINS_GET_RESOURCE           (ERROR_SECINS + 45)  /**< Failed to get system resource limits */
#define ERROR_SECINS_SETSID                 (ERROR_SECINS + 46)  /**< Failed to create new session */
#define ERROR_SECINS_START_LINE_TOO_LONG    (ERROR_SECINS + 47)  /**< Start file line too long */
#define ERROR_SECINS_START_LINE_INVALID     (ERROR_SECINS + 48)  /**< Start file line invalid */
#define ERROR_SECINS_START_ENTRY_UNKNOWN    (ERROR_SECINS + 49)  /**< Start file entry unknown */

#define ERROR_SECINS_EXEC_APP               (ERROR_SECINS + 50)  /**< Failed to start app */
#define ERROR_SECINS_MODE_APP               (ERROR_SECINS + 51)  /**< Start file entry not executable */
#define ERROR_SECINS_SYMLINK                (ERROR_SECINS + 52)  /**< Failed to create symlink */
#define ERROR_SECINS_INVALID_INSTALL_FILE   (ERROR_SECINS + 53)  /**< Invalid install archive type */
#define ERROR_SECINS_EXTRACT_INSTALL_FILE   (ERROR_SECINS + 54)  /**< Failed to extract download file */
#define ERROR_SECINS_INVALID_PKG_FORMAT     (ERROR_SECINS + 55)  /**< Invalid package file type */
#define ERROR_SECINS_AUTH_GID               (ERROR_SECINS + 56)  /**< Invalid filetype for authing */
#define ERROR_SECINS_ADD_CERT               (ERROR_SECINS + 57)  /**< Failed to add Certificate */
#define ERROR_SECINS_PATCH_VERSION          (ERROR_SECINS + 58)  /**< Patch SrcVersion does not match the installed pkg */
#define ERROR_SECINS_PATCH                  (ERROR_SECINS + 59)  /**< Failed to patch package */

#define ERROR_SECINS_START_SVCSEC           (ERROR_SECINS + 60)  /**< Failed to start svcsec application */
#define ERROR_SECINS_INVALID_CERT           (ERROR_SECINS + 61)  /**< Invalid cert file */
#define ERROR_SECINS_PUT_ENV                (ERROR_SECINS + 62)  /**< Failed to process package config file */
#define ERROR_SECINS_SET_USER               (ERROR_SECINS + 63)  /**< Failed to set user id */
#define ERROR_SECINS_INVALID_PKG_CATEGORY   (ERROR_SECINS + 64)  /**< Invalid package category specified */
#define ERROR_SECINS_INVALID_CMD            (ERROR_SECINS + 65)  /**< Invalid command in remove file */
#define ERROR_SECINS_INVALID_BUNDLE_FILE    (ERROR_SECINS + 66)  /**< Invalid bundle file */
#define ERROR_SECINS_EXTRACT_BUNDLE_CONTROL (ERROR_SECINS + 67)  /**< Failed to extract bundle control file */
#define ERROR_SECINS_INVALID_BUNDLE_USER    (ERROR_SECINS + 68)  /**< Invalid bundle user specified */
#define ERROR_SECINS_INVALID_PKG_USER       (ERROR_SECINS + 69)  /**< Package user does not match bundle user */

#define ERROR_SECINS_NO_BUNDLE              (ERROR_SECINS + 70)  /**< No bundle file matching bundle p7s signature */
#define ERROR_SECINS_OPEN_SOCKET            (ERROR_SECINS + 71)  /**< Failed to open socket */
#define ERROR_SECINS_BIND_FAILURE           (ERROR_SECINS + 72)  /**< Failed to bind unix domain address */
#define ERROR_SECINS_UNKNOWN_MSG            (ERROR_SECINS + 73)  /**< Unknown secure installer msg */
#define ERROR_SECINS_MSG_SND                (ERROR_SECINS + 74)  /**< Failed to send API response msg */
#define ERROR_SECINS_MSG_RCV                (ERROR_SECINS + 75)  /**< Failed to read API msg */
#define ERROR_SECINS_MSG_SIZE               (ERROR_SECINS + 76)  /**< API message too big */
#define ERROR_SECINS_RESP_SIZE              (ERROR_SECINS + 77)  /**< API response message too big */
#define ERROR_SECINS_INVALID_RESP           (ERROR_SECINS + 78)  /**< Invalid API response */
#define ERROR_SECINS_FILE_READ              (ERROR_SECINS + 79)  /**< Failed to read file */

#define ERROR_SECINS_FILE_RENAME            (ERROR_SECINS + 80)  /**< Failed to rename file */
#define ERROR_SECINS_USER_APP_START         (ERROR_SECINS + 81)  /**< Failed to start app */
#define ERROR_SECINS_GET_CWD                (ERROR_SECINS + 82)  /**< Failed to get current working dir */
#define ERROR_SECINS_INVALID_PARAM          (ERROR_SECINS + 83)  /**< Invalid parameter specified */
#define ERROR_SECINS_NOT_ALLOWED            (ERROR_SECINS + 84)  /**< Permission denied */
#define ERROR_SECINS_SYSMODE_START          (ERROR_SECINS + 85)  /**< No sysmode loaded */
#define ERROR_SECINS_INVALID_PKGLIST        (ERROR_SECINS + 86)  /**< Package list file invalid */
#define ERROR_SECINS_SOCK_LISTEN            (ERROR_SECINS + 87)  /**< Failed to setup API socket */
#define ERROR_SECINS_CONNECT_FAIL           (ERROR_SECINS + 88)  /**< Failed to connect to secins API socket */
#define ERROR_SECINS_PATCH_APPLIED          (ERROR_SECINS + 89)  /**< Low Layer patch already applied */

#define ERROR_SECINS_RELOAD_GRSEC           (ERROR_SECINS + 90)  /**< Failed to reload the grsecurity policy */
#define ERROR_SECINS_CREATE_GRSEC_FILE      (ERROR_SECINS + 91)  /**< Failed to create grsec policy file */
#define ERROR_SECINS_GRSEC_SUBJECT          (ERROR_SECINS + 92)  /**< Subject in grsec file does not exist */
#define ERROR_SECINS_INVALID_POLICY_FILE    (ERROR_SECINS + 93)  /**< Invalid grsec policy file */
#define ERROR_SECINS_INVALID_OBJECT         (ERROR_SECINS + 94)  /**< Object in grsec file invalid or not allowed */
#define ERROR_SECINS_INVALID_OBJECT_MODE    (ERROR_SECINS + 95)  /**< Object mode in grsec file not allowed */
#define ERROR_SECINS_NO_POLICY_FILE         (ERROR_SECINS + 96)  /**< System grsec policy file not found */
#define ERROR_SECINS_INVALID_CAPABILITY     (ERROR_SECINS + 97)  /**< Capability in grsec file not allowed */
#define ERROR_SECINS_OBJECT_NOT_HIDDEN      (ERROR_SECINS + 98)  /**< Object in grsec file must be hidden */
#define ERROR_SECINS_OBJECT_NOT_FOUND       (ERROR_SECINS + 99)  /**< Required object in grsec file not found */

#define ERROR_SECINS_FILE_CAPS              (ERROR_SECINS + 100) /**< Failed to add file capabilities */
#define ERROR_SECINS_SUBJECT_MODE           (ERROR_SECINS + 101) /**< Invalid subject mode in grsec file */
#define ERROR_SECINS_REMOVE_USER            (ERROR_SECINS + 102) /**< Failed to remove user */
#define ERROR_SECINS_REMOVE_GROUP           (ERROR_SECINS + 103) /**< Failed to remove group */
#define ERROR_SECINS_PID_NOT_FOUND          (ERROR_SECINS + 104) /**< Process does not exist */
#define ERROR_SECINS_SIGNAL_FAIL            (ERROR_SECINS + 105) /**< Failed to send signal */
#define ERROR_SECINS_LOAD_VSS               (ERROR_SECINS + 106) /**< Failed to load VSS script to vault */
#define ERROR_SECINS_UNSIGNED_PKG_USER      (ERROR_SECINS + 107) /**< Unsigned pkg being installed by invalid user */
#define ERROR_SECINS_PATCH_AUTH             (ERROR_SECINS + 108) /**< Failed to auth patched pkg */
#define ERROR_SECINS_EXTRACT_PATCH_CONTROL  (ERROR_SECINS + 109) /**< Failed to extract control file from patched pkg */

#define ERROR_SECINS_GRSEC_ENTRY_UNKNOWN    (ERROR_SECINS + 110) /**< grsec file entry unknown */
#define ERROR_SECINS_FILE_DELETE            (ERROR_SECINS + 111) /**< Failed to delete file */
#define ERROR_SECINS_NO_SRC_PKG             (ERROR_SECINS + 112) /**< Package to be patched does not exist */
#define ERROR_SECINS_NO_SRC_BUNDLE          (ERROR_SECINS + 113) /**< Bundle to be patched does not exist */
#define ERROR_SECINS_BUNDLE_VERSION         (ERROR_SECINS + 114) /**< Higher bundle version already installed */
#define ERROR_SECINS_BUNDLE_SRCVERSION      (ERROR_SECINS + 115) /**< Bundle SrcVersion does not match the installed bundle */
#define ERROR_SECINS_WRONG_BUNDLE           (ERROR_SECINS + 116) /**< Package being installed already exists in a different bundle */
#define ERROR_SECINS_TOKEN_FAIL             (ERROR_SECINS + 117) /**< Failed to install feature enablement token */
#define ERROR_SECINS_BUNDLE_CHANGED         (ERROR_SECINS + 118) /**< Reloading same bundle version with different pkg versions */
#define ERROR_SECINS_BUNDLE_NOT_CHANGED     (ERROR_SECINS + 119) /**< New bundle version with same pkg versions */

#define ERROR_SECINS_MANU_MODE              (ERROR_SECINS + 120) /**< Not allowed when unit in manufacture mode */
#define ERROR_SECINS_MANU_MODE_REQD         (ERROR_SECINS + 121) /**< Not allowed when unit not in manufacture mode */
#define ERROR_SECINS_INVALID_UMASK          (ERROR_SECINS + 122) /**< Invalid umask in package, must be octal */
#define ERROR_SECINS_FILE_SEEK              (ERROR_SECINS + 123) /**< File seek operation failed */
#define ERROR_SECINS_MODULE_NOT_FOUND       (ERROR_SECINS + 124) /**< Module not installed */
#define ERROR_SECINS_INVALID_HMAC_CFG       (ERROR_SECINS + 125) /**< HMAC config file missing or invalid */
#define ERROR_SECINS_HMAC_SETUP             (ERROR_SECINS + 126) /**< Vault HMAC setup error */
#define ERROR_SECINS_HMAC_CHECK             (ERROR_SECINS + 127) /**< Vault HMAC check error */
#define ERROR_SECINS_CERT_NOT_FOUND         (ERROR_SECINS + 128) /**< Certificate not found */
#define ERROR_SECINS_BUNDLE_NOT_FOUND       (ERROR_SECINS + 129) /**< Bundle not found */

#define ERROR_SECINS_TARGET_ERROR           (ERROR_SECINS + 130) /**< Not installing as target not met */
#define ERROR_SECINS_REG_CALLBACK           (ERROR_SECINS + 131) /**< Failed to register callback(full table) */
#define ERROR_SECINS_SET_RESOURCE           (ERROR_SECINS + 132) /**< Failed to set system resource */
#define ERROR_SECINS_PATCHLIST_FILE			(ERROR_SECINS + 133)
#define ERROR_SECINS_SHA_COMPUTE			(ERROR_SECINS + 134)
#define ERROR_SECINS_SHA_MISMATCH			(ERROR_SECINS + 135)
#define ERROR_SECINS_COMPRESS_PKG			(ERROR_SECINS + 136)
#define ERROR_SECINS_FILE_COPY				(ERROR_SECINS + 137)
#define ERROR_SECINS_IS_APP_DEV				(ERROR_SECINS + 138)
#define ERROR_SECINS_APP_DEV_TOKEN			(ERROR_SECINS + 139)

#define ERROR_SECINS_APP_DEV_CERTS			(ERROR_SECINS + 140)
#define ERROR_SECINS_DEL_CERTFILE			(ERROR_SECINS + 141)
#define ERROR_SECINS_APP_DEV_SVID			(ERROR_SECINS + 142)

#define ERROR_SECINS_SPARSE_PATCH			(ERROR_SECINS + 143)

#define ERROR_SECINS_HMAC_KEY_TOO_LONG		(ERROR_SECINS + 146)
#define ERROR_SECINS_VER_NOT_SUPPORTED      (ERROR_SECINS + 147)
/** [SECINS_ERROR_CODES] */
/** \} */


#define SECINS_BIT(n)						(1 << n)

// Bitmapped outflags for Secins_install_software API.
#define SECINS_REBOOT_REQD_BIT				SECINS_BIT(0)
#define SECINS_RESTART_APPS_REQD_BIT		SECINS_BIT(1)
#ifdef RAPTOR
#define SECINS_L1BOOT_REQD_BIT				SECINS_BIT(2)
#endif

#define MAX_PATH_FILE			    256

/**
 * @name Application termination cause
 */
/** \{ */
#define NORMALLY_TERMINATED         0x01 /**< Application terminated by exit(2) or returned from main */
#define KILLED_BY_SIGNAL            0x02 /**< Application killed by system signal */
#define UNKOWN_CAUSE_OF_DEATH       0x03 /**< Should not receive this normaly */
/** \} */

/**
 * @name Application termination cause
 */
typedef struct
{
    int cause_of_death; /**< Application termination cause */
    int death_details;  /**< Signal No / RC & 0xFF (0 ... 255 range) */
    pid_t pid;          /**< Application pid */
} child_death_cert;


/**
 * @name Max user/group names allowed in Secins_add_user/Secins_add_group
 */
/** \{ */
#define SECINS_MAX_USERNAME_LEN         16
#define SECINS_MAX_GROUPNAME_LEN        16
/** \} */


/**
 * User/Group ID range
 */
typedef struct
{
    int start_uid;      /**< Start User ID in the assigned range */
    int num_uids;       /**< Number User IDs assigned. Valid UID range [start_uid:num_uids-1] */
    int start_gid;      /**< Start Group ID in the assigned range */
    int num_gids;       /**< Number Group IDs assigned. Valid UID range [start_gid:num_gids-1] */
} UID_GID_RANGE;

# define HMAC_MAX_HMAC      32
# define EVP_MAX_KEY        64
typedef struct
{
	unsigned char key[HMAC_MAX_HMAC];
	unsigned int keysize;
	int mode;
} SYS_INFO_OS_HMAC_SHA256_tx;

typedef struct
{
	unsigned char hmac[HMAC_MAX_HMAC];
	unsigned int hmacsize;
} SYS_INFO_OS_HMAC_SHA256_rx;


/**
 * Called recursively to retrieve the list of installed packages, which returns in pkginfo
 *
 * @param[out] pkginfo - SecinsPkgInfo data structure containing package information
 * @param[in] pkginfosize - sizeof SecinsPkgInfo
 *
 * @return
 * Struct SecinsPkgInfo - package informtion
 *
 * @li NULL no more entries are available.
 */
SecinsPkgInfo *Secins_read_pkglist_entry(SecinsPkgInfo *pkginfo, int pkginfosize);


/**
 * Close package list, opened by Secins_read_pkglist_entry()
 */
void Secins_close_pkglist(void);


/**
 * Called recursively to retrieve the list of installed bundles, which returns in bdlinfo
 *
 * @param[out] bdlinfo - SecinsBdlInfo data structure containing package information
 * @param[in] bdlinfosize - Sizeof SecinsBdlInfo
 *
 * @return
 * Struct SecinsBdlInfo - Bundle informtion
 *
 * @li NULL no more entries are available.
 */
SecinsBdlInfo *Secins_read_bdllist_entry(SecinsBdlInfo *bdlinfo, int bdlinfosize);


/**
 * Close bundle list, opened by Secins_read_bdllist_entry()
 */
void Secins_close_bdllist(void);


/**
 * Send test string <str> and get it back in <echostr>
 *
 * @param[out] echostr - Responce buffer, should be big enough to hold <str>
 * @param[in] size - Size of echostr
 * @param[in] str - Arbitrary test string
 *
 * @return
 * 0 if passed test or one of the ERROR_SECINS error codes otherwise
 */
int Secins_echo_test(char *echostr, int size, const char *str);


/**
 * Install packages from the download file to /mnt/flash/install/dl
 *
 * @param[out] reboot_reqd - if not NULL, set to 1 if reboot is required, 0 otherwise
 *
 * @return
 * 0 on success or one of the ERROR_SECINS error codes otherwise
 */
int Secins_install_pkgs(int *reboot_reqd);


/**
 * Install packages from the download file to /mnt/flash/install/dl
 *
 * @param[out] outflags - Bitmapped outflags for Secins_install_software API.
 * @param[in] inflags - not used
 *
 * @return
 * 0 on success or one of the ERROR_SECINS error codes otherwise
 */
int Secins_install_software(int *outflags, int inflags);


/**
 * Reboot the device.
 * This call first send SIGTERM to all user processes, followed by SIGKILL
 *
 * @return
 * 0 on success or one of the ERROR_SECINS error codes otherwise
 */
int Secins_reboot(void);


/**
 * Check vault HMAC
 *
 * @return
 * 0 on success or one of the ERROR_SECINS error codes otherwise
 */
int Secins_hmac_check(void);


/**
 * Power off the device.
 * SIGTERM is first sent to all user processes, followed by SIGKILL
 *
 * @return
 * 0 on success or one of the ERROR_SECINS error codes otherwise
 */
int Secins_poweroff(void);


/**
 * Abort secins's power off.
 *
 * @return
 * 0 on success or one of the ERROR_SECINS error codes otherwise
 */
int Secins_abort_poweroff(void);


/**
 * Format card
 *
 * @return
 * 0 on success or one of the ERROR_SECINS error codes otherwise
 */
int Secins_format_card(void);


/**
 * Start user application.
 *
 * @param[in] file - Executable to be started
 *
 * @return
 * 0 on success or one of the ERROR_SECINS error codes otherwise
 */
int Secins_start_app(const char *file);


/**
 * Start user applications (used by sysmode).
 *
 * @return
 * 0 on success or one of the ERROR_SECINS error codes otherwise
 */
int Secins_start_user_apps(void);


/**
 * Start user applications (used by sysmode).
 *
 * @return
 * 0 on success or one of the ERROR_SECINS error codes otherwise
 *
 * @note all user app processes (usr1 ... usr16) will be stopped before sysmode is started
 */
int Secins_start_sysmode(void);


/**
 * Disables the sysmode stimulus. Prevents launching of sysmode.
 *
 * @return
 * 0 on success or one of the ERROR_SECINS error codes otherwise
 */
int Secins_disable_sysmode(void);


/**
 * Enables the sysmode stimulus. Allows launching of sysmode
 *
 * @return
 * 0 on success or one of the ERROR_SECINS error codes otherwise
 */
int Secins_enable_sysmode(void);


/**
 * Check if development mode is enabled
 *
 * @param[out] dev_mode - Set to 1 if enabled, 0 otherwise
 *
 * @return
 * 0 on success or one of the ERROR_SECINS error codes otherwise
 */
int Secins_get_dev_mode(int *dev_mode);


/**
 * Get the range of users and groups assigned to a primary user.
 *
 * All uids/gids in the API's that follow must be in the range returned here.
 *
 * @param[out] range - UID_GID_RANGE data structure containing the uid/gid range
 * @param[in] rangesize - Size of the UID_GID_RANGE structure.
 *
 * @return 0 on success or one of the ERROR_SECINS error codes on error.
 */
int Secins_get_uid_gid_range(UID_GID_RANGE *range, int rangesize);


/**
 * Add a group
 *
 * Only a primary user (usr1, usr2, etc) may use this function.
 * The specified gid must be in range assigned to the primary user.
 *
 * @param[in] gid - The group id
 * @param[in] groupname - Group name (SECINS_MAX_GROUPNAME_LEN (16) chars max).
 *
 * @return 0 on success or one of the ERROR_SECINS error codes on error.
 */
int Secins_add_group(int gid, const char *groupname);


/**
 * Add a user
 *
 * Only a primary user (usr1, usr2, etc) may use this function.
 * The specified uid and gid must be in range assigned to the primary user.
 *
 * @param[in] uid - The user id
 * @param[in] username - User name (SECINS_MAX_USERNAME_LEN (16) chars max).
 * @param[in] gid - Primary group for this user.
 *
 * @return 0 on success or one of the ERROR_SECINS error codes on error.
 */
int Secins_add_user(int uid, const char *username, int gid);

/**
 * Add user uid to group gid
 *
 * Only a primary user (usr1, usr2, etc) may use this function.
 * Both the user and group must already exist and must be in the range assigned
 * to the primary user.
 *
 * @param[in] gid - the group id to add to.
 * @param[in] uid - the user to be added to group gid.
 *
 * @return 0 on success or one of the ERROR_SECINS error codes on error.
 */
int Secins_add_group_member(int gid, int uid);

/**
 * Remove a user.
 *
 * Only a primary user (usr1, usr2, etc) may use this function.
 * The specified user must exist and must be in range assigned to the primary
 * user.
 *
 * @param[in] uid - the user id.
 *
 * @return 0 on success or one of the ERROR_SECINS error codes on error.
 */
int Secins_remove_user(int uid);

/**
 * Remove a group.
 *
 * Only a primary user (usr1, usr2, etc) may use this function.
 * The specified group must exist and must be in range assigned to the primary
 * user.
 *
 * @param[in] gid - the group id.
 *
 * @return 0 on success or one of the ERROR_SECINS error codes on error.
 */
int Secins_remove_group(int gid);

/**
 * Start an application (cmdline), with specified uid and gid.
 *
 * Only a primary user (usr1, usr2, etc) may use this function.
 * The specified user and group must exist and must be in range assigned to the
 * primary user.
 * The executable file specified in cmdline must be owned by the primary user.
 *
 * @param[out] app_pid - the process id of the application.
 * @param[in] cmdline - the command line to use to start the application.
 * @param[in] uid - the app runs with this user id.
 * @param[in] gid - the app runs with this group id.
 *
 * @return 0 on success or one of the ERROR_SECINS error codes on error.
 */
int Secins_start_app_uid(pid_t *app_pid, const char *cmdline, int uid, int gid);

/**
 * Start an application, with specified uid and gid.
 *
 * Only a primary user (usr1, usr2, etc) may use this function.
 * The specified user and group must exist and must be in range assigned to the
 * primary user.
 * The executable file specified in argp[0] must be owned by the primary user.
 * envp may be NULL if no environment strings required.
 *
 * @param[out] app_pid - the process id of the application.
 * @param[in] argp - array of argument strings, terminated by NULL ptr.
 * @param[in] envp - array of environment strings, terminated by NULL ptr.
 * @param[in] uid - the app runs with this user id.
 * @param[in] gid - the app runs with this group id.
 *
 * @return 0 on success or one of the ERROR_SECINS error codes on error.
 */
int Secins_exec_app_uid(pid_t *app_pid,
						char *argp[],
						char *envp[],
						int uid,
						int gid);
/**
 * Send the signal sig to the process pid
 *
 * The primary user may signal processes running as any of the users assigned
 * to it. Other users may only signal the primary user.
 *
 * @param[in] pid - the process id.
 * @param[in] sig - Signal to send.
 *
 * @return 0 on success or one of the ERROR_SECINS error codes on error.
 */
int Secins_signal_app(pid_t pid, int sig);

/**
 * Change file ownership
 *
 * Only a primary user (usr1, usr2, etc) may use this function.
 * The target file must be owned by the primary user or one of the users
 * assigned to it.
 * Both the user and group must already exist and must be in the range assigned
 * to the primary user.
 *
 * @param[in] path - pathname
 * @param[in] uid - the required owner.
 * @param[in] gid - the required group.
 *
 * @return 0 on success or one of the ERROR_SECINS error codes on error.
 */
int Secins_chown(const char *path, int uid, int gid);


/**
 * Removes the bundle specified in bdlinfo from the system.
 *
 * @param[in] bdlinfo - specified bundle info
 *
 * @return
 * 0 on success or one of the ERROR_SECINS error codes otherwise
 *
 * @note This API may only be used by system signed applications
 */
int Secins_remove_bdl_entry(SecinsBdlInfo *bdlinfo);


/**
 * Can be called by a user application to remove all installed bundles/packages for that user i.e.
 * the application removes itself completely.
 *
 * @return
 * 0 on success or one of the ERROR_SECINS error codes otherwise
 */
int Secins_removeall_user(void);


/**
 * Removes the bundle containing the specified 'file'.
 *
 * @param[in] file - file from bundle
 * @param[in] pkgtype - package type
 *
 * @return
 * 0 on success or one of the ERROR_SECINS error codes otherwise
 */
int Secins_remove_file_bundle(const char *file, const char *pkgtype);


/**
 * Removes the bundle 'bundlename'.
 *
 * @param[in] bundlename - bundle name
 *
 * @return
 * 0 on success or one of the ERROR_SECINS error codes otherwise
 */
int Secins_remove_user_bundle(const char *bundlename);


/**
 * Returns a pointer to the error message associated with the non-zero error code
 *
 * @param[in] err - Error code
 *
 * @return
 * Error message string
 */
const char *Secins_strerror(int err);


/**
 * Returns the gid of the group created by the secure installer to allow sharing between the
 * primary user and its secondary users i.e. The primary group and all the secondary users it
 * creates are members of this group.
 *
 * @return
 * Group ID on success, -1 on error
 */
int Secins_secondary_share_gid(void);


/**
 * Returns the group name of the group created by the secure installer to allow sharing between
 * the primary user and its secondary users.
 *
 * @param[out] groupname - buffer where to store group name
 * @param[in] namesize - sizeof <groupname>
 *
 * @return
 * Group ID on success, -1 on error
 */
int Secins_get_secondary_share_group(char *groupname, int namesize);


/**
 * @return
 * COMMON_GID
 */
gid_t Secins_share_gid(void);


/**
 * @return
 * SYSTEM_GID
 */
gid_t Secins_system_gid(void);


/**
 * @return
 * SIGNER_GID
 */
gid_t Secins_users_gid(void);


/**
 * Check if the caller is an user app.
 *
 * @return
 * @li 1 - the caller is a user application (i.e. usr1, usr2, ..., usr16)
 * @li 0 - otherwise
 */
unsigned int Secins_user_app(void);


/**
 * Check if the caller is a system app.
 *
 * @return
 * @li 1 - the caller is a system application (i.e. sys1, sys2, ..., sys16)
 * @li 0 - otherwise
 */
unsigned int Secins_sys_app(void);

#ifndef RAPTOR
/**
 * Returns the system share group id for the specified user (usr1 ... usr16).
 *
 * @return
 * Group ID or -1 on error
 */
gid_t Secins_usrsys_gid(uid_t uid);
#endif


/**
 * Same as Secins_usrsys_gid(), except it returns the system share group id for the calling user (usr1 ... usr16).
 *
 * @return
 * Group ID or -1 on error
 *
 * @note this group was originally for sharing with the sysmode user but now includes all system users
 * @note For users other than usr1 ... usr16, the current effective group id is returned.
 */
gid_t Secins_sysmode_share_gid(void);


/**
 * Returns the system share group id for the specified configfile.
 *
 * @param[in] configfile - Configuration file (config.usr1, config.usr2, config.sys1, config.network ...)
 *
 * @return
 * Group ID or -1 on error
 */
gid_t Secins_config_file_share_gid(const char *configfile);

/**
 * Start usr/sys application
 * SYS users can start any USR application. USR user can start only same USR
 * applications (e.g. SYS4, can start: SYS4, USR1, USR2 ... . USR3 only USR3)
 * Application path should be specified in argp[0]
 *
 * @param[out] app_pid  - (Optional) PID of application
 * @param[in]  argp     - array of argument strings, terminated by NULL ptr
 * @param[in]  envp     - array of environment strings, terminated by NULL ptr
 * @param[in]  cb       - (Optional) callback triggered upon application
 *                        termination
 * @param[in]  arg      - (Optional) pass an argument to callback
 *
 * @return 0 on success or one of the ERROR_SECINS error codes on error.
 */
int Secins_start_app_argv(pid_t *app_pid,
                          char *argp[],
                          char *envp[],
                          int (*cb)(child_death_cert cert, void *arg),
                          void *arg);


/**
 * Kill(check) usr/sys application by pid
 *
 * @param[in]  app_pid  - application PID
 * @param[in]  sig      - signal to send
 *  @li  0   - check if application is alive
 *  @li  >0  - signal id
 * @param[out] status   - (Optional) pass an argument to callback
 *  @li  1   - application alive/killed
 *  @li  0   - application dead/allready terminated (double termination attempt)
 *
 * @return 0 on success or one of the ERROR_SECINS error codes on error.
 */
int Secins_kill_app_pid(pid_t app_pid, int sig, int *status);




/**
 * Delete all Users(usr1 .. usr16), user VCL, VSS nad IPP keys
 * Can be used
 *
 * @return 0 on success or one of the ERROR_SECINS error codes on error.
 *
 * @note Allowed to be used only by sys4(sysmode)
 */
int Secins_delete_all_users();

int Secins_hmac_sha256(uint8_t* hash, int* hash_size, uint8_t* keyin, int keyinsz, int mode);

int Secins_add_cert(const char *certfile);
int Secins_SetKLDDetamper(const char *detamper_option);
int Secins_app_dev_convert(const char *token_file,
						   const char *token_sig,
						   const char *cert_dir);
int Secins_ExecuteToken(const char *token,
		const char *token_signature,
		const char *token_type,
		int options);

#ifdef __cplusplus
}
#endif

#endif
