/*
 *	Copyright, 2008 VeriFone Inc.
 *	2099 Gateway Place, Suite 600
 *	San Jose, CA.  95110
 *
 *  All Rights Reserved. No part of this software may be reproduced,
 *  transmitted, transcribed, stored in a retrieval system, or
 *  translated into any language or computer language, in any form
 *  or by any means electronic, mechanical, magnetic, optical,
 *  chemical, manual or otherwise, without the prior permission of
 *  VeriFone Inc.
 */
 
#ifndef _VFICORE_H_
#define _VFICORE_H_

#ifdef __cplusplus
extern "C" {
#endif
 
#include <time.h>
#include <sys/signal.h>
//#include "svcInfoAPI.h"
//#include "svcNetwork.h"
#include <syslog.h>

/*
 * Keys for IPC resources
 *
 * IPC resource keys will start at 0x46410000
 * with semaphores from 0x46410000 to 0x46410FFF
 *      shared mem from 0x46411000 to 0x46411FFF
 *      msg queues from 0x46412000 to 0x46412FFF
 *
 * Services which use any resources should define them here.
 */

// List of Semaphore Keys
#define _vficore_PUTENV_SEMAPHORE_KEY    0x46410000
#define _vficore_FE_SEMAPHORE_KEY        0x46410010

// List of Shared Memory Keys
#define _vficore_INITABLE_SHM_KEY        0x46411000
    // note, The Event Service reserves 1E00 thru 1EFF

// List of Message Queues

/*
 * End : IPC resource keys
 */

// Maximum length of a filename including path
#define _vficore_SVC_MAX_FILENAME_LEN 0x200

// Data size for Section:label = value
#define _vficore_MAX_SECTION_LEN	0x20
#define _vficore_MAX_LABEL_LEN	0x20
#define _vficore_MAX_VALUE_LEN	0x200

//#ifndef LED_SC
#define _vficore_LED_SC		(1<<7)
//#endif

//For OS Expiration
#define SECONDS_IN_DAY			86400
#define DEFAULT_OS_EXPIRE_DAYS	30


// Log flags

	/* SVC DEFINES */

	/* Compressed file extensions */

	/* svcRunAPP variable argument definitions */

// Feature Enablement Token Definitions

// Used with enablementTokenDetail()


    /* Structure Definitions */

    // Define feature enablement token

	/* Prototypes */
/**
* @param[in] section
* @param[in] label
* @param[in] value
*/
int _vficore_putEnvFile(char *section, char *label, char *value);

/**
* @param[in] section
* @param[in] label
* @param[in] value
* @param[in] filename
*/
int _vficore_putEnvFilename(char *section, char *label, char *value, char* filename);

/**
* @param[in] section
* @param[in] label
* @param[in] value
* @param[in] user
* @param[in] f_redirectSupport
*/
int _vficore_putEnvFileUsr(char *section, char *label, char *value, char *user, int f_redirectSupport);

/**
* @param[in] section
* @param[in] label
* @param[in] value
* @param[in] vlen
*/
int _vficore_getEnvFile(char *section, char *label, char *value, int vlen);

/**
* @param[in] section
* @param[in] label
* @param[in] value
* @param[in] vlen
* @param[in] filename
*/
int _vficore_getEnvFilename(char *section, char *label, char *value, int vlen, char* filename);

/**
* @param[in] section
* @param[in] label
* @param[in] value
* @param[in] vlen
* @param[in] user
* @param[in] f_redirectSupport
*/
int _vficore_getEnvFileUsr(char *section, char *label, char *value, int vlen, char *user, int f_redirectSupport);

/**
* @param[in] user
* @param[in] mode
*/
int _vficore_delEnvFile(char *user,int mode);

/**
* @param[in] user
*/
void *_vficore_loadEnvFileUsr(char *user);

/**
* Search for file in directory
*
* @param[in] dir
* @param[in] fSpec
* @param[out] buff
*
* @return buff (same one that was given in input)
*/
char *fileFind(char *dir,char *fSpec,char *buff);

/**
* Read the terminal serial.
*
* @param[out] buffer Buffer should be at least 12 characters long.
*
* @note
* Serial number is in format XXX-XXX-XXX, where X is a positive integer.
*/
void _vficore_svcInfoSerialNum(char *buffer);
	
	// svcExpand...() and loadOSFiles() should be replaced with svcSecureInstall()!

	// vfiservices processed commands

	// perform package management command and place results in fileName

    // Enablement APIs
    // new Enablement API to get status, including unit (for DAYS and USES)


	// Derived Functions

	// Defined Functions for RF Contactless Reader

	// Defined Functions for Signature Capture

	// Function to support OS expire feature.
	//
/**
* Intended to be called by the secure installer only.
*
* @param[in] days Number of desired elapsed days.
* @param[in] bStr String to match in build (/etc/issue), 
* 	NULL = Expire regardless of OS build, pointer to NULL string = Use default
* 	value "-rc"
*
* @return -1 error
* 	0 expired
* 	2 String didn't match, OS does not expire
*/
int runDays(unsigned int days, char *bStr);

/*SVC_SERVICE:utility*/
/**
   * @name External Storage Types
   */
/** \{ */
#define EXTERNAL_STORAGE_USB            1    /**< USB Memory device */
#define EXTERNAL_STORAGE_MICRO_SD       2    /**< MicroSD Memory device */
/** \} */

/**
 * Structure to contain information on a single external storage device
 */
struct vficore_AnExternalStorage {
  int type;                     /**< Type of storage: 1=USB, 2=microSD, */
  char mountPoint[64];         /**< Mount point */
};

/**
 * Structure to contain information on all available external storage devices
 */
struct vficore_ExternalStorage {
  int storage_count;
  struct vficore_AnExternalStorage *storage;
};

/**
 * Functions from externalStorage.c
 */
struct vficore_ExternalStorage vficore_externalStorage(void);
void vficore_externalStorage_release(struct vficore_ExternalStorage exstor);
unsigned char vficore_get_file_type(char const* const path,
	   	char const* const name );
int vficore_ends_with(char const* const file_path,  char const* const ext);
int vficore_copy_file(char const* const src_dir, char const* const dst_dir,
	   	char const* file_name);
int copy_dir_archives(const char* const root_dir, const char* const sub_dir,
	   	const char* const install_dir, int only_check);

#ifdef __cplusplus
}
#endif
#endif // vficore.h
