/*
 *  Copyright, 2016 VeriFone Inc.
 *
 *  Author: Patryk Marek Oterski <patryk.oterski@verifone.com>
 *
 *  All Rights Reserved. No part of this software may be reproduced,
 *  transmitted, transcribed, stored in a retrieval system, or
 *  translated into any language or computer language, in any form
 *  or by any means electronic, mechanical, magnetic, optical,
 *  chemical, manual or otherwise, without the prior permission of
 *  VeriFone Inc.
 */

#ifndef __GSMBASIC_H__
#define __GSMBASIC_H__

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifndef GSMBASIC_VERSION_MAJOR
#define GSMBASIC_VERSION_MAJOR 0
#endif /* GSMBASIC_VERSION_MAJOR */

#ifndef GSMBASIC_VERSION_MINOR
#define GSMBASIC_VERSION_MINOR 0
#endif /* GSMBASIC_VERSION_MINOR */

#ifndef GSMBASIC_VERSION_RELEASE
#define GSMBASIC_VERSION_RELEASE 0
#endif /* GSMBASIC_VERSION_RELEASE */

#define GSMBASIC_DEV_BOARD_MODE				0
#define GSMBASIC_TRACE_MODE_DEFAULT			GSMBASIC_TRACE_MODE_NONE
#define GSMBASIC_HIGH_COMMAND_TIMEOUT_S		180
#define GSMBASIC_LOW_COMMAND_TIMEOUT_S		60
#define GSMBASIC_VERBOSE_SIZE				128
#define GSMBASIC_INFO_SIZE					33
#define GSMBASIC_OPERATOR_LONG_NAME_SIZE	65
#define GSMBASIC_OPERATOR_SHORT_NAME_SIZE	17
#define GSMBASIC_CELL_ID_SIZE				9
#define GSMBASIC_CELL_MCC_SIZE				5
#define GSMBASIC_CELL_MNC_SIZE				5
#define GSMBASIC_CELL_LAC_SIZE				6

typedef enum {
	GSMBASIC_RESULT_OK = 0,
	GSMBASIC_RESULT_INCORRECT_PARAMETERS,
	GSMBASIC_RESULT_NO_COMMUNICATION,
	GSMBASIC_RESULT_TIMEOUT_OCCURRED,
	GSMBASIC_RESULT_NOT_SUPPORTED,
	GSMBASIC_RESULT_NOT_ALLOWED,
	GSMBASIC_RESULT_TEMPORARY_NOT_ALLOWED,
	GSMBASIC_RESULT_SIM_PIN_REQUIRED,
	GSMBASIC_RESULT_SIM_PUK_REQUIRED,
	GSMBASIC_RESULT_SIM_PIN_2_REQUIRED,
	GSMBASIC_RESULT_SIM_PUK_2_REQUIRED,
	GSMBASIC_RESULT_INCORRECT_PASSWORD,
	GSMBASIC_RESULT_SIM_FAILURE,
	GSMBASIC_RESULT_SIM_BUSY,
	GSMBASIC_RESULT_SIM_WRONG,
	GSMBASIC_RESULT_SIM_NOT_INSERTED,
	GSMBASIC_RESULT_BUSY,
	GSMBASIC_RESULT_NO_DIALTONE,
	GSMBASIC_RESULT_NO_ANSWER,
	GSMBASIC_RESULT_NO_CARRIER,
	GSMBASIC_RESULT_INTERNAL_ERROR,
	GSMBASIC_RESULT_UNKNOWN_ERROR
} gsmbasic_result;

typedef enum {
	GSMBASIC_TRACE_MODE_NONE,
	GSMBASIC_TRACE_MODE_ERROR,
	GSMBASIC_TRACE_MODE_DEBUG
} gsmbasic_trace_mode;

typedef enum {
	GSMBASIC_CHANNEL_COMMAND,
	GSMBASIC_CHANNEL_DATA,
	GSMBASIC_CHANNEL_COUNT
} gsmbasic_channel;

typedef enum {
	GSMBASIC_POWER_MODE_OFF,
	GSMBASIC_POWER_MODE_ON,
	GSMBASIC_POWER_MODE_SUSPEND,
	GSMBASIC_POWER_MODE_RESUME
} gsmbasic_power_mode;

typedef enum {
	GSMBASIC_PASSWORD_TYPE_PIN,
	GSMBASIC_PASSWORD_TYPE_PUK,
	GSMBASIC_PASSWORD_TYPE_PIN_2,
	GSMBASIC_PASSWORD_TYPE_PUK_2
} gsmbasic_password_type;

typedef enum {
	GSMBASIC_SIM_SLOT_FIRST,
	GSMBASIC_SIM_SLOT_SECOND
} gsmbasic_sim_slot;

typedef enum {
	GSMBASIC_SIM_STATUS_READY,
	GSMBASIC_SIM_STATUS_PIN_REQUIRED,
	GSMBASIC_SIM_STATUS_PUK_REQUIRED,
	GSMBASIC_SIM_STATUS_PIN_2_REQUIRED,
	GSMBASIC_SIM_STATUS_PUK_2_REQUIRED,
	GSMBASIC_SIM_STATUS_FAILURE,
	GSMBASIC_SIM_STATUS_BUSY,
	GSMBASIC_SIM_STATUS_WRONG,
	GSMBASIC_SIM_STATUS_NOT_INSERTED,
	GSMBASIC_SIM_STATUS_UNKNOWN
} gsmbasic_sim_status;

typedef enum {
	GSMBASIC_OPERATOR_STATUS_UNKNOWN,
	GSMBASIC_OPERATOR_STATUS_AVAILABLE,
	GSMBASIC_OPERATOR_STATUS_REGISTERED,
	GSMBASIC_OPERATOR_STATUS_FORBIDDEN
} gsmbasic_operator_status;

typedef enum {
	GSMBASIC_REG_MODE_AUTOMATIC,
	GSMBASIC_REG_MODE_MANUAL,
	GSMBASIC_REG_MODE_AUTOMATIC_MANUAL
} gsmbasic_reg_mode;

typedef enum {
	GSMBASIC_REG_STATUS_NOT_REGISTERED,
	GSMBASIC_REG_STATUS_REGISTRATION_DENIED,
	GSMBASIC_REG_STATUS_SEARCHING_FOR_A_NEW_OPERATOR,
	GSMBASIC_REG_STATUS_REGISTERED_TO_FOREIGN_NETWORK,
	GSMBASIC_REG_STATUS_REGISTERED_TO_HOME_NETWORK,
	GSMBASIC_REG_STATUS_UNKNOWN
} gsmbasic_reg_status;

typedef enum {
	GSMBASIC_RAT_GSM,
	GSMBASIC_RAT_GSM_UMTS_DUAL_MODE,
	GSMBASIC_RAT_UTRAN,
	GSMBASIC_RAT_GSM_EGPRS,
	GSMBASIC_RAT_UTRAN_HSDPA,
	GSMBASIC_RAT_UTRAN_HSUPA,
	GSMBASIC_RAT_UTRAN_HSDPA_HSUPA
} gsmbasic_rat;

struct gsmbasic_library_info {
	char library_version[GSMBASIC_INFO_SIZE];
};

struct gsmbasic_module_info {
	char manufacturer[GSMBASIC_INFO_SIZE];
	char model[GSMBASIC_INFO_SIZE];
	char revision[GSMBASIC_INFO_SIZE];
	char imei[GSMBASIC_INFO_SIZE];
};

struct gsmbasic_sim_info {
	char id[GSMBASIC_INFO_SIZE];
	char imsi[GSMBASIC_INFO_SIZE];
};

struct gsmbasic_operator {
	gsmbasic_operator_status operator_status;
	char long_name[GSMBASIC_OPERATOR_LONG_NAME_SIZE];
	char short_name[GSMBASIC_OPERATOR_SHORT_NAME_SIZE];
	unsigned int lai;
	gsmbasic_rat rat;
};

struct gsmbasic_operators_list {
	struct gsmbasic_operator *operators_list;
	unsigned int count;
};

struct gsmbasic_cell {
	gsmbasic_rat rat;
	char id[GSMBASIC_CELL_ID_SIZE];
	char mcc[GSMBASIC_CELL_MCC_SIZE];
	char mnc[GSMBASIC_CELL_MNC_SIZE];
	char lac[GSMBASIC_CELL_LAC_SIZE];
	unsigned int channel;
	int rssi;
};

struct gsmbasic_cells_list {
	struct gsmbasic_cell *cells_list;
	unsigned int count;
};

gsmbasic_result gsmbasic_get_library_info(struct gsmbasic_library_info *library_info);
gsmbasic_result gsmbasic_set_trace_mode(gsmbasic_trace_mode trace_mode);
gsmbasic_result gsmbasic_get_trace_mode(gsmbasic_trace_mode *trace_mode);
gsmbasic_result gsmbasic_get_device_path(gsmbasic_channel channel, char **device_path);
gsmbasic_result gsmbasic_set_sim_slot(gsmbasic_sim_slot sim_slot);
gsmbasic_result gsmbasic_get_sim_slot(gsmbasic_sim_slot *sim_slot);
gsmbasic_result gsmbasic_set_power_mode(gsmbasic_power_mode power_mode);
gsmbasic_result gsmbasic_get_power_mode(gsmbasic_power_mode *power_mode);
gsmbasic_result gsmbasic_execute_command(gsmbasic_channel channel, const char *command, char **response, unsigned int timeout);
gsmbasic_result gsmbasic_execute_command_and_process_response(gsmbasic_channel channel, const char *command, const char *end_line_string, unsigned int source_line_index, const char *source_line_prefix, bool use_destination_ptr, char **destination_ptr, char *destination, size_t destination_size, unsigned int timeout);
gsmbasic_result gsmbasic_get_module_info(struct gsmbasic_module_info *module_info);
gsmbasic_result gsmbasic_get_sim_status(gsmbasic_sim_status *sim_status);
gsmbasic_result gsmbasic_get_password_counter(gsmbasic_password_type password_type, const char *password, unsigned int *password_counter);
gsmbasic_result gsmbasic_enter_password(gsmbasic_password_type password_type, const char *password, const char *new_password);
gsmbasic_result gsmbasic_change_password(gsmbasic_password_type password_type, const char *password, const char *new_password);
gsmbasic_result gsmbasic_get_sim_info(struct gsmbasic_sim_info *sim_info);
gsmbasic_result gsmbasic_get_operators_list(struct gsmbasic_operators_list *operators_list);
gsmbasic_result gsmbasic_select_operator_by_lai(gsmbasic_reg_mode reg_mode, unsigned int lai);
gsmbasic_result gsmbasic_query_operator_lai(unsigned int *lai);
gsmbasic_result gsmbasic_query_operator_long_name(char **long_name);
gsmbasic_result gsmbasic_query_operator_short_name(char **short_name);
gsmbasic_result gsmbasic_get_reg_status(gsmbasic_reg_status *reg_status);
gsmbasic_result gsmbasic_get_cells_list(struct gsmbasic_cells_list *cells_list);
gsmbasic_result gsmbasic_get_rssi(int *rssi);
gsmbasic_result gsmbasic_set_rat(gsmbasic_rat rat, gsmbasic_rat rat_pref);
gsmbasic_result gsmbasic_get_rat(gsmbasic_rat *rat, gsmbasic_rat *rat_pref);
gsmbasic_result gsmbasic_csd_connect(const char *called_address);
gsmbasic_result gsmbasic_csd_send_data(const char *data, size_t data_size, size_t *sent);
gsmbasic_result gsmbasic_csd_receive_data(char *data, size_t data_size, size_t *received, unsigned int timeout);
gsmbasic_result gsmbasic_csd_disconnect(void);
gsmbasic_result gsmbasic_gprs_connect(const char *apn, const char *called_address);
gsmbasic_result gsmbasic_gprs_disconnect(void);
gsmbasic_result gsmbasic_dev_test(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GSMBASIC_H__ */
