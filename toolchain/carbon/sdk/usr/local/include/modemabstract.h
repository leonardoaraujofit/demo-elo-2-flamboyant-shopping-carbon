 /*******************************************************************************
 * modemabstract.h - Upper Layer interface
 *
 *  Created on:  Nov 21, 2012
 *  Modified on: Jan 16, 2014
 *  Author: GuntherP1
 ******************************************************************************/

#ifndef MODEMABST_H_
#define MODEMABST_H_

#ifdef RAPTOR
#include <modem.h>	// PSTN interface
#else
#include <linux/modem.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define MDM_UART_MODEM_DEVICE_ENV		"modem_uart"
#define MDM_UXPSTN_MODEM_DEVICE_ENV		"modem_uxpstn"
#define MDM_UXISDN_MODEM_DEVICE_ENV		"modem_uxisdn"

// pathes used in libvfimodempstnX, libvfimodemisdnX
#define MDM_UART_CONSOLE_PATH 			"/dev/ttyAMA0"
#define MDM_UART_EXT_COM_PATH 			"/dev/ttyAMA2"
#define MDM_MODEM_LOG_FILE_PATH			"/var/log/modem"
#define MDM_MODEMABS_LOG_FILE_PATH		"/var/log/modemabstract"


/* modem availability */

#define MDM_NO_MODEM_AVAILABLE	0
#define MDM_PSTN_AVAILABLE		1
#define MDM_ISDN_AVAILABLE		2
#define MDM_MODEM_AVAILABLE	   11

/* transmission protocols (ISDN) */
#define MDM_PROT_HDLC_SYNC_TO_ASYNC			 3	// -> ATB3
#define MDM_PROT_HDLC_TRANSPARENT			 4	// -> ATB4
#define MDM_PROT_X75						10	// -> ATB10
#define MDM_PROT_X31_B_CHL					20	// -> ATB20

#define MDM_ISDN_NUMBER_MAX_LEN		50
#define MDM_X25_PACKET_SIZE_LEN		 5
#define MDM_FACILITIES_MAX_LEN		20
#define MDM_NUI_MAX_LEN				20
#define MDM_X25_NUMBER_MAX_LEN		20
#define MDM_USER_DATA_MAX_LEN		20

struct st_dial_data_isdn{
	char	number[MDM_ISDN_NUMBER_MAX_LEN];			// incl. subaddr
	char	x25_packet_size[MDM_X25_PACKET_SIZE_LEN];	// 64 - 2048)
	char 	facilities[MDM_FACILITIES_MAX_LEN];			// 'G'
	char	nui[MDM_NUI_MAX_LEN];
	char	x25_number[MDM_X25_NUMBER_MAX_LEN];
	char	user_data[MDM_USER_DATA_MAX_LEN];			// have to start with 'D' or 'P'
};

struct st_dial_params_isdn{
	char	max_retries;			// dial retries
	char	connect_timeout;		// waiting for answer, if expired no more retries
	int		country_code;			// currently without meaning
};

struct st_answer_params_isdn{
	char 	number_of_rings;		// 0 - for auto answer disable
	char 	connect_timeout;		// since start answering
};


// *****************************************************************************
// common functions
// *****************************************************************************
int modem_availability(void);
int modem_release(void);
int modem_abstract_trace(int mode); // MDM_TRACE_...
int modem_abstract_reset(void);

// *****************************************************************************
// PSTN functions (additional to modem.h)
// *****************************************************************************
int modem_download_firmware(char* fwloader,char* fwFile);

// *****************************************************************************
// ISDN functions
// *****************************************************************************
int isdn_start(char *usr_commands_file);
int isdn_get_modem_info( struct st_modem_info *modem_info_ptr, int *size);
int isdn_get_status(void);
int isdn_get_last_call_info(struct st_last_connect_info *last_connect_info_ptr, int *size);
int isdn_is_ringing(void);
int isdn_is_line_connected(void);
int isdn_dial(int isdn_prot, struct st_dial_data_isdn *dial_data_ptr, struct st_dial_params_isdn *dial_param_ptr);
int isdn_set_answer_params(struct st_answer_params_isdn *answer_params_ptr);
int isdn_get_answer_params(struct st_answer_params_isdn *answer_params_ptr, int *size);
int isdn_set_answer_protocol(int isdn_prot);
int isdn_answer_call(void);
int isdn_reset(void);
int isdn_hang_up(void);
int isdn_send_data(char *buff, int size);
int isdn_receive_data(char *buff, int size, int timeout_millsec);
int isdn_data_flush(int buffers_flag);
int isdn_pipe_commands(char *tx, char *rx, int rx_size, int max_retries, int response_timeout);
int isdn_trace_mode(int mode);
int isdn_download_firmware(char* fwFile);
int isdn_stop(void);

#ifdef __cplusplus
}
#endif

#endif /* MODEMABST_H */
