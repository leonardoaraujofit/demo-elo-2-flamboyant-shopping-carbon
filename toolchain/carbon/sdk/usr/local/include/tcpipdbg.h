#ifndef _TCPIPDBG_H
#define _TCPIPDBG_H

#ifndef FALSE
/**
 * @name Return value
*/
/** \{ */
#define FALSE 0
/** \} */
#endif

#ifndef TRUE
/**
 * @name Return value
*/
/** \{ */
#define TRUE 1
/** \} */
#endif

/**
 * @name Defines might be used by application
 *
 * @note Check README_tcpipdbg.TXT for example
*/
/** \{ */
#define NO_DEBUG        0
#define CONSOLE_DEBUG   1
#define ETHERNET_DEBUG  2
/** \} */

#define INVALID_SOCKET      -1 // !not used as return value, should be moved to .c file

/**
 * @name Debug message maximal size
*/
/** \{ */
#define MAX_DEBUG_MSG       2048+1
/** \} */


/**
 * Open debug socket to debug host (SocketListener)
 *
 * @return
 * @li TRUE Success
 * @li FALSE Error or already opened
 */
extern int iOpenDebugSocket(void);


/**
 * Close debug socket
 *
 * @return
 * @li 0 Success
 * @li -1 Error
 */
extern int iCloseDebugSocket(void);


/**
 * Send null-terminated debug string to debug host (SocketListener)
 *
 * @param[in] pszMessage buffer with debug message
 *
 */
extern void DebugMsg(char *pszMessage);


/**
 * Send debug string of specified length to debug host (SocketListener)
 *
 * @param[in] puchMessage buffer with debug message
 * @param[in] ushMsgLen debug message length
 *
 */
extern void DebugMsgEx(unsigned char *puchMessage, unsigned short ushMsgLen);


#ifdef DEBUGOPT // 23-Nov-10: added
/**
 * Function to save debug logs to flash/USB stick on app closure
 *
 * @note Enabled only if DEBUGOPT macro is defined
 *
 */
extern void SaveDebugLogs(void);
#endif // DEBUGOPT

#endif /* _TCPIPDBG_H */
