
/** @addtogroup semtekhtdes SemtekHTDes */
/** @{ */
/** @addtogroup semtekhtdesapistatus SemtekHTDes : API status  */
/** @{ */
/** 
 *  @file SemtekHTDesStatus.h 
 *
 *	@brief SemtekHTDes API status
 *
 * 
 */



#ifndef HTDES_STATUS_H_
#define HTDES_STATUS_H_

#include "SemtekDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * This message is associated with msg IDs MSG_STATUS_ANSWER (from semtekd)
 * note that no message is required on input to semtekd. (msg is null)
 */
typedef PACKED_SPEC struct {
	unsigned char ucStatus; /**< htDES module status */
	unsigned char ucModuleState; /**< see below for values */
	unsigned char ucEncryptionStatus; /**< most recent card swipe's encryption status */
	unsigned char ucRootAuthenticationKeyStatus; /**< Uses MSG_HTDES_KEY_* values */
	unsigned char ucProvisionerAuthenticationKeyStatus; /**< Uses MSG_HTDES_KEY_* values */
	unsigned char ucPanKeyStatus; /**< Uses MSG_HTDES_KEY_* values */
	unsigned char ucTerminalKeyStatus; /**< Uses MSG_HTDES_KEY_* values */
	unsigned char ucEncryptionEnabled; /**< indicates if encryption is enabled for the platform 00- Encryption disable 01- encryption enabled */
	unsigned char ucVersionString[10]; /**< 9 character string + null terminator.  Format is X.YY.ZZZZ  Leading zeros on fields may or may not be present on each tuple. */
	unsigned char ucReserved[5]; /**< reserved */

#ifdef WIN32
	char dummy1[1]; /**< dummy variable so windows sizes match linux sizes */
#endif

} MSG_HTDES_STATUS;

/**
 * MSG_HTDES_STATUS - these status are returned in ucStatus.
 */
#define MSG_HTDES_OK										0x00 
#define MSR_ERR_FILE_ERR									0x06 /**< file error moved to htdes */
#define MSR_ERR_NVRAM_ERR									0x07 /**< file error */
#define MSR_ERR_OPERATION_FAILED							0x09 /**< API function failed on Verix API function */
#define MSR_ERR_CMD_CARD_BAD_FUNCTION_CODE					0x0a
#define MSR_ERR_CMD_CARD_CRC								0x20
#define MSR_ERR_CMD_CARD_BAD_AUTH_ROLE						0x21
#define MSR_ERR_CMD_CARD_INAPPROPRIATE_CLR_CMD				0x22
#define MSR_ERR_CMD_CARD_FAILED_MATCH_RECORD_INSERT			0x25 /**< Failed on insert */
#define MSR_ERR_CMD_CARD_BAD_RIGHTS							0x26 /**< Supplied BinRng rights aren't sufficient. */
#define MSR_ERR_CMD_CARD_INITIAL_FILE_PARSE_ERROR			0x27 /**< config file error - parsing */
#define MSR_ERR_CMD_CARD_INITIAL_FILE_PARSE_AUTH_ERROR		0x28 /**< config file error - bad signature */
#define MSR_ERR_CMD_CARD_MASTER_KCV_NOMATCH					0x29 /**< config file error - bad master keys */
#define MSR_ERR_CMD_CARD_LINE_KCV_NOMATCH					0x2a /**< config file error - KCV line */
#define MSR_ERR_CMD_CARD_BAD_PARAMETER						0x2b /**< parameter out of range */
#define MSR_ERR_RKDS_BAD_STATE								0x2c /**< system state incorrect */

/**
 * These states are returned in ucModuleState
 */

#define SEMTEKD_STATE_INITIAL    		0
#define SEMTEKD_STATE_XOR_KEY1			1
#define SEMTEKD_STATE_XOR_KEY2			2
#define SEMTEKD_STATE_H_FILE_UNLOCKED	3
#define SEMTEKD_NORMAL_OPERATIONS		4
#define SEMTEKD_SRED_MODE				5

/**
 * These states are returned in ucEncryptionStatus
 */
#define	SEMTEKD_ENCRYPTION_OFF			0
#define SEMTEKD_ENCRYPTION_LIMITED		1
#define SEMTEKD_ENCRYPTION_ON			2

/**
 * Status for individual keys
 */
#define MSG_HTDES_KEY_OK			0
#define MSG_HTDES_KEY_CHKSUM_FAIL	1
#define MSG_HTDES_KEY_NOT_SET		2
#define MSG_HTDES_KEY_ZEROIZED		3


#ifdef __cplusplus
}
#endif

#endif /*HTDES_STATUS_H_*/

/** \} */
/** \} */
