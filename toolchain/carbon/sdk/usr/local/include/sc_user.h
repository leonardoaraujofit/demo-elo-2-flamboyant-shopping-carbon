/****************************************************************************
* This source code is confidential proprietary information of VeriFone Inc.	*
* and is an unpublished work or authorship protected by the copyright laws  *
* of the United States. Unauthorized copying or use of this document or the *
* program contained herein, in original or modified form, is a violation of *
* Federal and State law.                                                    *
*                                                                           *
*                           Verifone                                        *
*                      Rocklin, CA 95765                                    *
*                        Copyright 2003                                     *
****************************************************************************/

/*
;|==========================================================================+
;| FILE NAME:  | sc_user.h 
;|-------------+------------------------------------------------------------+
;| DESCRIPTION:| user part of the smart card driver API
;|-------------+------------------------------------------------------------+
;| TYPE:       | 
;|-------------+------------------------------------------------------------+
;| COMMENTS:   | 
;|==========================================================================+
*/
/*
$Archive:   $
$Author:    Thierry Payet
$Date:      04/08/14
$Modtime:   $
$Revision:  0.1
$Log:       $
*/

#ifndef __SC_USER_H__
#define __SC_USER_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "ScrLib.h"


          /*==========================================*
           *           D E F I N I T I O N S          *
           *==========================================*/


       /*=================================================*
        *   H W    S U P P O R T    F U N C T I O N S     *
        *================================================*/

// driver Init function prototypes
int                     ICC_DrvOpen(void);
int                     ICC_DrvClose(int);

// EMV Protocol functions prototypes 
SCR_STATUS              ICC_PowerUp(ScrATRStruct *);
SCR_STATUS              ICC_ShutDown(unsigned char);
SCR_STATUS              ICC_PowerDown(unsigned char);
SCR_STATUS              ICC_IsoTransceive(ScrInstructStruct *);

// generic functions prototypes
void                    ICC_Version(ScrVersionStruct *);
int 					ICC_SlotCount(void);
SCR_STATUS              ICC_SlotStatus(ScrStatusStruct *);
SCR_STATUS              ICC_GpioRead(unsigned char, unsigned char *);
SCR_STATUS              ICC_GpioWrite(unsigned char, unsigned char);
void 				 	ICC_SetSigHandler(int);

// synchronous functions prototypes
int 					IccSyn_RunScript(IccSynScriptStruct *, unsigned char);

#ifdef __cplusplus
}
#endif
#endif /*of __SC_USER_H__ */
