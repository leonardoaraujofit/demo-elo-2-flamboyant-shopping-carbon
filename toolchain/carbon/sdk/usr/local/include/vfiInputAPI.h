/*********************************************************************
* $Log:   /h03_u01/pvcs/Combined/PVCS8.0/Engg_Projects/archives/Spectrum/input/vfiInputAPI.h-arc  $
 * 
 *    Rev 1.2   16 Mar 2006 09:33:12   kevin_s1
 * Fixed cplusplus wrapper to be
 * inside header file define.
 * 
 *    Rev 1.1   08 Mar 2006 09:31:26   kevin_s1
 * Added cplusplus wrapper.
 * 
 *    Rev 1.0   15 Dec 2005 15:26:18   kevin_s1
 * Initial revision.
 *
 *    Rev 1.0   14 Dec 2005 21:21:58   kevin_s1
 * Initial revision.
*
*		Copyright, 2004 VeriFone Inc.
*		2099 Gateway Place, Suite 600
*		San Jose, CA.  95110
*
*  All Rights Reserved. No part of this software may be reproduced,
*  transmitted, transcribed, stored in a retrieval system, or
*  translated into any language or computer language, in any form
*  or by any means electronic, mechanical, magnetic, optical,
*  chemical, manual or otherwise, without the prior permission of
*  VeriFone Inc.
*
***********************************************************************/

/**
 * @file vfiInputAPI.h
 */

#ifndef _VFI_INPUT_API_
#define _VFI_INPUT_API_

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @name Keypad key values
 */
/** \{ */ 
#define VFI_TOUCHPAD		0x0000
#define VFI_USB_KBD		0x0001
#define VFI_USB_SCANNER		0x0002
#define VFI_USB_MOUSE		0x0003

#define VFI_KEY_F1		0x80
#define VFI_KEY_F2		(VFI_KEY_F1 + 1)
#define VFI_KEY_F3		(VFI_KEY_F2 + 1)
#define VFI_KEY_F4		(VFI_KEY_F3 + 1)
#define VFI_KEY_F5		(VFI_KEY_F4 + 1)
#define VFI_KEY_F6		(VFI_KEY_F5 + 1)
#define VFI_KEY_F7		(VFI_KEY_F6 + 1)
#define VFI_KEY_F8		(VFI_KEY_F7 + 1)
#define VFI_KEY_F9		(VFI_KEY_F8 + 1)
#define VFI_KEY_F10		(VFI_KEY_F9 + 1)
#define VFI_KEY_F11		(VFI_KEY_F10 + 1)
#define VFI_KEY_F12		(VFI_KEY_F11 + 1)

#define VFI_KEY_INS		0x90
#define VFI_KEY_DEL		(VFI_KEY_INS + 1)
#define VFI_KEY_HOME		(VFI_KEY_DEL + 1)
#define VFI_KEY_END		(VFI_KEY_HOME + 1)
#define VFI_KEY_PGUP		(VFI_KEY_END + 1)
#define VFI_KEY_PGDN		(VFI_KEY_PGUP + 1)
#define VFI_KEY_LFT		(VFI_KEY_PGDN + 1)
#define VFI_KEY_UP		(VFI_KEY_LFT + 1)
#define VFI_KEY_DN		(VFI_KEY_UP + 1)
#define VFI_KEY_RT		(VFI_KEY_DN + 1)

#define VFI_KEY_PRNT		(VFI_KEY_RT + 1)
#define VFI_KEY_PAUSE		(VFI_KEY_PRNT + 1)
#define VFI_KEY_LWIN		(VFI_KEY_PAUSE + 1)
#define VFI_KEY_RWIN		(VFI_KEY_LWIN + 1)
#define VFI_KEY_MENU		(VFI_KEY_RWIN + 1)
/** \} */

/** 
 * Opens the USB device. Only one device can be open at a time.
 * 
 * @param[in] vfi_device The device desired to open as defined in vfiInputAPI.h header file
 *                       as follows:
 *                       * VFI_USB_KBD
 *                       * VFI_USB_SCANNER
 * 
 * @return 
 * @li > 0 Successful execution corresponding to the handle of the opened device.
 * @li -1 And errno set to:
 *     * -EINVAL = More than one or no devices plugged in to the USB host port.
 *     * -EBUSY = One device plugged in to the USB port opened
 *       successfully; opening subsequent devices was attempted but failed.
 */
int inputOpen(int vfi_device);

/**
 * Capture read events. Currently, touchpad and mouse events are not captured.
 * Only keyboard and scanner data is read. If inputRead() is called in a loop, data
 * is continually read from the keyboard or scanner device, allowing data to be
 * captured as it is entered/scanned.
 * 
 * @param[in] inHdl The device handle obtained from opening the device with inputOpen().
 * 
 * @return
 * Returns the ASCII value or the raw scancode of the key pressed or data scanned.
 * @li 0 No data read from the device.
 * @li Negative value The device corresponding to the handle cannot be found.
 */
int inputRead(int inHdl);

/**
 * Close the device. When the device is closed, lit LEDs are shut off.
 *
 * @param inHdl The device handle obtained from opening the device with inputOpen().
 * 
 * @return
 * @li 0 Success
 * @li Negative value An error occurred while trying to close the device. 
 */
int inputClose(int inHdl);

#ifdef __cplusplus
}
#endif
#endif
