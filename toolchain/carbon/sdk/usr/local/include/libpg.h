#ifndef _LIBPG_H_
#define _LIBPG_H_

#ifdef __cplusplus
extern "C" {
#endif


/*
;|==========================================================================+
;| FUNCTION   |int getFileName( const char * signatureFile
;|            | char * fileName){
;|------------+-------------------------------------------------------------
;| DESCRIPTION|retrieve from the PKCS7 signature the name of the file to
;|            |be checkedint getX509Info( byte * pBERStream , unsigned int sizeBERStream ,  X509Info * pX509info )
;|            |
;|------------+-------------------------------------------------------------
;| PARAMETERS |const char * signatureFile: The name of the file  that contains the
;|            |                               PKCS7 signature
;|            |char *   fileName: The pointer where the name of the file will be copied
;|            |           if  the pointer is NULL, the file name will be not returned
;|            |           but the length will be returned
;|------------+-------------------------------------------------------------
;| RETURN     | -returm:The length of the file name
;|            |                   or
;|            |           -1: Error  - File does not exist or bad PKCS7 signature format
;|            |
;|            |
;|===========================================================================+
*/


int getFileName( const char * signatureFile, char * fileName);


/* -----------------3/12/99 1:46PM-------------------
 Using the Authority Identifier and the Subject
 Identifier fields of the new Certificate place
 the new Certificate in the Certificate heirarchy.

 Inputs:    filename                    name of file containing
                                        certificate to add.sizeof(diagOption)
 Outputs:   none

 Result:    0   successful
        ERR_CERTIFICATES_CORRUPT    certificates are corrupted
        ERR_INVALID_CERT        invalid new certificate
        ERR_CERT_FILE_FULL      out of certificate space
        ERR_CERT_REJECTED_WIDE  attempted to add peer to level
                                    occupied by a wide certificate
 --------------------------------------------------*/

/****************************************************************************************/
/****************************************************************************************/
/* Function                                        */
/****************************************************************************************/
/****************************************************************************************/
short vfiSec_CMDiag (short diagOption, char *data, short *dataLength);

/*
 * This function to be used, if You want to make sure that vfiSec_CMDiag starts
 * with the beginning of the certificate tree
 */
void vfiSec_DropCMIndex(void);

int get_last_cert_used(unsigned char * psernum, int * psernumlen, unsigned char * psig_auth_attr, int * pauth_attr_len);


// RAPTOR additions.
int Secmode_pgtest(char *test_str);
int Secmode_load_app(unsigned int *index,
					 unsigned long *addr,
					 const char *filename,
					 int app_id);
int Secmode_load_bind_app(unsigned int *index,
						unsigned long *ldaddr,
						const char *sigfile,
						const char *appfile,
						unsigned int app_id);
int Secmode_unload_app(unsigned int index);
int Secmode_get_load_addr(unsigned long *addr, unsigned int index);
int Secmode_get_app_id(unsigned int index);
int Secmode_get_app_name_ver(char *namebuf,
							 int namebufsize,
							 char *verbuf,
							 int verbufsize,
							 int app_id);
void Secmode_set_msg_debug(int value);
int nonbertlv_secmodser(int app_id, int req_id, int arg1, int arg2, int arg3);

/* Function to auth pedguard */
int auth_pedguard(const char *pedguard_signed_img);


#ifdef __cplusplus
}
#endif

#endif /* _LIBPG_H_ */
