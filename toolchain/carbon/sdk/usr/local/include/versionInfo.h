/** @addtogroup */
/** @{ */
/** @addtogroup */
/** @{ */
/** 
 *  @file versionInfo.h 
 *
 *	@brief Headers for obtain version info on OS software and installed software
 *      @author kevin_w1
 *
 * 
 */
/***********************************************************************
*
*		Copyright, 2004 VeriFone Inc.
*		2099 Gateway Place, Suite 600
*		San Jose, CA.  95110
*
*  All Rights Reserved. No part of this software may be reproduced,
*  transmitted, transcribed, stored in a retrieval system, or
*  translated into any language or computer language, in any form
*  or by any means electronic, mechanical, magnetic, optical,
*  chemical, manual or otherwise, without the prior permission of
*  VeriFone Inc.
*
***********************************************************************/


#ifdef __cplusplus
extern "C" {
#endif
 
#ifndef _VERSIONINFO_
#define _VERSIONINFO_

#include <time.h>

  typedef int (*osversion_list_callback)( char *name, char *dest, char *version, char *timestamp, long size, void *userdata );

// Library methods for obtaining various OS version strings

/**
 * Obtain OS kernel ersion string
 *
 * @param[in] cblist Callback list
 * @param[in] userdata User data
 *
 * @return 
 * @li 0 = Success
 * @li < 0 = See errno
 *
 */
int os_kernel_version( osversion_list_callback cblist, void *userdata );

/**
 * Obtain OS altkernel version string
 *
 * @param[in] cblist Callback list
 * @param[in] userdata User data
 *
 * @return
 * @li 0 = Success
 * @li < 0 = See errno
 *
 */
int os_altkernel_version( osversion_list_callback cblist, void *userdata );

/**
 * Obtain OS rfs version string
 *
 * @param[in] cblist Callback list
 * @param[in] userdata User data
 *
 * @return
 * @li 0 = Success
 * @li < 0 = See errno
 *
 */
int os_rfs_version( osversion_list_callback cblist, void *userdata );

/**
 * Obtain OS SDK version string
 *
 * @param[in] cblist Callback list
 * @param[in] userdata User data
 *
 * @return
 * @li 0 = Success
 * @li < 0 = See errno
 *
 */
int os_sdk_version( osversion_list_callback cblist, void *userdata );

/**
 * Obtain OS toolchain version string
 *
 * @param[in] cblist Callback list
 * @param[in] userdata User data
 *
 * @return
 * @li 0 = Success
 * @li < 0 = See errno
 *
 */
int os_toolchain_version( osversion_list_callback cblist, void *userdata );

/**
 * Obtain OS vault version string
 *
 * @param[in] cblist Callback list
 * @param[in] userdata User data
 *
 * @return
 *
 */
int os_vault_version( osversion_list_callback cblist, void *userdata );

/**
 * Obtain OS u-boot version string
 *
 * @param[in] cblist Callback list
 * @param[in] userdata User data
 *
 * @return
 * @li 0 = Success
 * @li < 0 = See errno
 *
 */
int os_uboot_version( osversion_list_callback cblist, void *userdata );

/**
 * Obtain OS HW version string
 *
 * @param[in] cblist Callback list
 * @param[in] userdata User data
 *
 * @return
 * @li 0 = Success
 * @li < 0 = See errno
 *
 */
int os_hardware_version( osversion_list_callback cblist, void *userdata );
  
  // method to listed installed packages (if package==NULL list all,
  //    else only obtain information for packageName (if present))
  
/**
 * Obtain OS installed packages version string
 *
 * @param[in] cblist Callback list
 * @param[in] userdata User data
 *
 * @return
 * @li 0 = Success
 * @li < 0 = See errno
 *
 */
int installed_packages_version( osversion_list_callback cblist, void *userdata, char *packageName );

/*VERSIONINFO_STRUCT*/
/**
 * Package owner enumeration
 */
  typedef enum {
	PACKAGEOWNER_ALL, /**< Look at all installed packages */
	PACKAGEOWNER_OS,  /**< Look at ONLY OS installed packages */
	PACKAGEOWNER_USR, /**< Look at ONLY user installed packages */
	PACKAGEOWNER_max
  } packageOwner_t;

/**
 * Obtain number of installed packages
 *
 * @param[in] packageOwner Which packages to look at
 *
 * @return Number of installed packages
 *
 */
int installedPackageCount( packageOwner_t packageOwner );

/**
 * Determine if a specified package is installed (or not)
 *
 * @param[in] name Package name
 * @param[in] packageOwner Which packages to look at
 *
 * @return
 * @li 0 = Success
 * @li-1 = Failure 
 *
 */
int isPackageInstalled( char *name, packageOwner_t packageOwner );


#endif  // _VERSIONINFO_

#ifdef __cplusplus
}
#endif

/// @} */
/// @} */
