#ifndef __SYSM_DEV__
#define __SYSM_DEV__

#include <sysmode_ui/sysm_ui.h>
#include <sysmode_ui/sysm_lang.h>

typedef struct bndl_list_opts t_bndl_list_opts;

struct bndl_list_opts {
    const char * usr;
    int hide_vss_bundles;
};

void dev_detamper_cb(const SYSM_UI::t_ui_menu_entry * e);
void dev_uplocal_cb(const SYSM_UI::t_ui_menu_entry * e);
void dev_execute_cb(const SYSM_UI::t_ui_menu_entry * e);
void dev_remove_cb(const SYSM_UI::t_ui_menu_entry * e);
void dev_remove_usr_bndl_cb(const SYSM_UI::t_ui_menu_entry * e);
#endif // __SYSM_DEV__
