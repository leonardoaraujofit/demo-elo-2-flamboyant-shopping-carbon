/****************************************************************************
 * FILE NAME:   VCL_VAULT.H
 * MODULE NAME: VCL_VAULT            
 * PROGRAMMER:          
 * DESCRIPTION: VCL Vault interface file			   
 * REVISION:    01.00.00  1/17/2011
 ****************************************************************************/

    /*=========================================*
     *   P U B L I C     S T R U C T U R E S   *
     *=========================================*/
#ifndef _VCL_VAULT_H_
#define _VCL_VAULT_H_
#define VclBufferLen	94

typedef enum
{
	VCL_Crypto,
	VCL_Encrypt_VAES,
	VCL_Derive_key,
	VCL_Store_PAN,
	VCL_Encrypt_VAES1
//	VCL_Encrypt_VAES_With_Handle,  //PCI_4_Requirement
} eVclOpCode;


// error codes
#define	VCL_SUCCESS		0x00000000
#define	VCL_ERR_UNKNOWN_OPCODE 	0xffffffff

typedef struct
{												//	VCL_Crypto,								VCL_encrypt_aes,		VCL_derive_key
	eVclOpCode		eOpCode;					//	VCL_crypto
	unsigned long	ui32ArgCmdRsp;				// VSP_Store, etc
	unsigned long	ui32Arg1;					// ucSrcSlot
	unsigned long	ui32Arg2;					// ucDstSlot
	unsigned char	ui8Key;						// ucKey
	unsigned char	ui8BufferLen;				// number of valid bytes in acDataBuffer
	unsigned char	acDataBuffer[VclBufferLen];	// ucDataBlock
	unsigned char	ui8Buffer2Len;
	unsigned char	acDataBuffer2[VclBufferLen];	// twk buffer
	unsigned char	acWrappedKeyBuffer[16];
} VCLOperation;

#endif
