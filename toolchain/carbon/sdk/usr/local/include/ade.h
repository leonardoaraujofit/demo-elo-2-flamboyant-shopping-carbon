// This file was copied from Vx on 20140108
// ade.h
// Original creation:  Thomas_R5 2013-01-13
// Documentation:  Vx eVo OS ADE ERS

#ifndef ADE_H
#define ADE_H



// ADE Encryption Algorithms
#define ADE_ALG_TDEA    0

// ADE Encryption Modes of Operation
#define ADE_MODE_ECB    0
#define ADE_MODE_CBC    1

// ADE IV Settings
#define ADE_IV_NONE     0
#define ADE_IV_ZERO     1
#define ADE_IV_RAND     2

// ADE Padding Schemes
#define ADE_PAD_NONE    0
#define ADE_PAD_PKCS7   1
#define ADE_PAD_X923    2
#define ADE_PAD_ISO7816 3

// ADE Return Codes
#define ADE_SUCCESS 0
#define ADE_ERR_PM_PTR  -1
#define ADE_ERR_PM_LEN  -2
#define ADE_ERR_PM_KEY  -3
#define ADE_ERR_PM_ALG  -4
#define ADE_ERR_PM_MODE -5
#define ADE_ERR_PM_IV   -6
#define ADE_ERR_PM_PAD  -7
#define ADE_ERR_NO_KEY  -11
#define ADE_ERR_OFF     -12
#define ADE_ERR_GENERIC -99

// ADE Status Codes (bitmap)
#define ADE_STS_ENG0    1<<0
#define ADE_STS_ENG1    1<<1
#define ADE_STS_ENG2    1<<2
#define ADE_STS_ENG3    1<<3
#define ADE_STS_ENG4    1<<4
#define ADE_STS_ENG5    1<<5
#define ADE_STS_ENG6    1<<6
#define ADE_STS_ENG7    1<<7
#define ADE_STS_ENG8    1<<8
#define ADE_STS_ENG9    1<<9
#define ADE_STS_ON      1<<10
#define ADE_STS_ENABLED 1<<11
#define ADE_STS_ENG_ALL 0x03FF

#define MAX_DATA_LEN    384
#define DES_BLOCK_SZ    8



typedef struct
{
    unsigned char* ptext;   // Plaintext
    int ptextLen;           // Plaintext Length
    int keyIndex;           // Key Index
    int encAlg;             // Encryption Algorithm
    int encMode;            // Encryption Mode of Operation
    int iv;                 // IV Setting
    int pad;                // Padding Scheme
} ade_encrypt_in;

typedef struct
{
    unsigned char* ctext;   // Ciphertext
    int ctextLen;           // Ciphertext Length
    unsigned char* ivData;  // IV Data
    unsigned char* supData; // Supplementary Data
} ade_encrypt_out;



int storeAdeDukptKey( int slot, unsigned char* ksn,
                      unsigned char* ik, int clearKeys );
int ade_encrypt( ade_encrypt_in* in, ade_encrypt_out* out );
int ade_status( void );
int ade_active( void );



#endif  // ADE_H



