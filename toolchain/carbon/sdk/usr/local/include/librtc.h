#ifndef _LIBRTC_H
#define _LIBRTC_H

#include <sys/types.h>
#include <time.h>
#include <linux/rtc.h>

#ifdef __cplusplus
extern "C" {
#endif

#define LIBRTC_OK	      0
#define LIBRTC_FAIL          -1
#define LIBRTC_INVALID_PARAM -2
#define LIBRTC_TIMEOUT       -3

typedef enum _rtc_event
{
//#define RTC_PF 0x40
//#define RTC_AF 0x20
//#define RTC_UF 0x10
  RTC_INT_UIE =  0x10,
  RTC_INT_ALARM = 0x20,
  RTC_INT_PERIODIC = 0x40
}librtc_event;

typedef struct { u_char sec,min,hr,date,month,day,year,century; } rtc_tm_t;
typedef struct { u_char min,hr; } __attribute__((packed)) alarm_t;

int rtc_open(void);
int rtc_close(void);
int rtc_get_system_tm(struct tm *t);
int rtc_get_hw_tm(struct tm *t);
int rtc_get_RTC_tm(struct timeval *tv2);
int rtc_set_tm(struct tm *t, int set_);
int rtc_set_system_tm(struct tm *t);
int rtc_read_alarm(alarm_t *alarm);
int rtc_write_alarm(alarm_t alarm);
int rtc_enable_alarm(void);
int rtc_disable_alarm(void);
int rtc_periodic_frequency_read(int *freq);
int rtc_periodic_frequency_write(int freq);
int rtc_enable_periodic(void);
int rtc_disable_periodic(void);
int rtc_enable_seconds_counter(void);
int rtc_disable_seconds_counter(void);
int rtc_wait_int(unsigned int timeout, librtc_event event);
int rtc_read_alarm_active(struct rtc_wkalrm * alarm);
int rtc_set_alarm(struct rtc_wkalrm * alarm);
int rtc_time_to_tm(unsigned long time, struct rtc_time *tm);
int  rtc_tm_to_time(struct rtc_time *tm, unsigned long *time);
int sync_mcu_rtc();
int rtc_resync_rtc_tm(unsigned int *offset_buf, int resync_revert);

/* used by rtc_set_tm function */
#define LIBRTC_SET_LINUX_TIME        1 /* set only linux systen time 	 */
#define LIBRTC_SET_RTC_LINUX_TIME    2 /* set rtc and linux system time */

#define RTC_RESYNC_OLD_OFFSET 0
#define RTC_RESYNC_NEW_OFFSET 1
#define RTC_RESYNC_BUF_LEN    2

#define RTC_RESYNC_SET        0
#define RTC_RESYNC_REVERT     1
#ifdef __cplusplus
}
#endif

#endif // _LIBRTC_H
