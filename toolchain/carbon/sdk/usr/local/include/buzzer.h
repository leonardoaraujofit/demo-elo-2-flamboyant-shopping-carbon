/** @addtogroup */
/** @{ */
/** @addtogroup */
/** @{ */
/**
 *  @file buzzer.h
 *
 *	@brief Buzzer
 *
 *
 */


#ifndef ___BUZZER_H
#define ___BUZZER_H

/**
 * Init and open driver
 *
 * @return
 * @li 0 = Success
 * @li -1 = Error, check errno
 *
 */
/*BUZZER_PROTOTYPE*/ int Buzzer_Open(void);
#ifdef RAPTOR
/* --------------------------------------------------------------------------
* FUNCTION NAME: Buzzer_Close
* DESCRIPTION:   Close the buzzer driver.
* RETURN:        0, if OK, or -1, if error.
* NOTES:         none.
* ------------------------------------------------------------------------ */
int Buzzer_Close(int DriverHandler);

/* --------------------------------------------------------------------------
* FUNCTION NAME: Buzzer_Off
* DESCRIPTION:   Stops play tone
* RETURN:        0, if OK, or -1, if error.
* NOTES:         none.
* ------------------------------------------------------------------------ */
int Buzzer_Off(void);

/* --------------------------------------------------------------------------
* FUNCTION NAME: Buzzer_Play
* DESCRIPTION:   Play a tone
* PARAMETERS:	 frequency: tone frequency, duration: duration of tone,
* 		 volume(currently not supported)
* RETURN:        0, if OK, or -1, if error.
* NOTES:         none.
* ------------------------------------------------------------------------ */
int Buzzer_Play(int frequency, int duration, int volume);

/* --------------------------------------------------------------------------
* FUNCTION NAME: Buzzer_SetVolume
* DESCRIPTION:   Sets buzzer volume
* PARAMETERS:	 volume: buzzer volume, range 0-100%
* RETURN:        0, if OK, or -1, if error.
* NOTES:         none.
* ------------------------------------------------------------------------ */
int Buzzer_SetVolume(int volume);
#else

/**
 * Init and close driver
 *
 * @param[in] DriverHandler Driver handler
 *
 * @return
 * @li 0 = Success
 * @li -1 = Error, check errno
 *
 */
/*BUZZER_PROTOTYPE*/ int Buzzer_Close(int DriverHandler);

/**
 * Turn buzzer off
 *
 */
/*BUZZER_PROTOTYPE*/ void Buzzer_Off(void);

/**
 * Play buzzer
 *
 * @param[in] frequency Buzzer frequency
 * @param[in] duration Buzzer duration time (ms)
 * @param[in] volume Buzzer volume
 *
 */
/*BUZZER_PROTOTYPE*/ void Buzzer_Play(int frequency, int duration, int volume);

/**
 * Set buzzer volume
 *
 * @param[in] volume Buzzer volume
 *
 */
/*BUZZER_PROTOTYPE*/ void Buzzer_SetVolume(int volume);

/**
 * Set Keypad Buzzer enable/disable configuration
 *
 * @param[in] enable 1 if yes, 0 if no
 *
 * @return
 * @li 0 = Success
 * @li -1 = Error, check errno
 *
 */
/*BUZZER_PROTOTYPE*/ int Keypad_Buzzer_On_Off( int enable );

#endif /* RAPTOR */
#endif /* #ifdef ___BUZZER_H*/
/// @} */
/// @} */
