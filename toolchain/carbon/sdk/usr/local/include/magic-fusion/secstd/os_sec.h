/****************************************************************************
 *
 * @brief	Secure Standard Library
 *
 * @par		Descrition:
 *          	Standard Secure APIs
 *
 * @date      	26 Jan 2011 (creation)
 *
 * @author    	$Author: ledauphin $
 *
 * @version   	$Revision: 1.00 $
 *
 * @par       	Copyright:
 *            	(c) Copyright Verifone 2011 unpublished work.
 *            	This computer program includes confidential, proprietary 
 *            	information and is a trade secret of Verifone. All use, 
 *            	disclosure and/or reproduction is prohibited unless
 *            	authorized in writing.
 *            	All rignts reserved.
 *
 ***************************************************************************/

#ifndef __OS_SEC_H__
#define __OS_SEC_H__

 /* ------------------------ Included files ------------------------------ */

#include <stdio.h>
#include "os/pub_sc.h"

 /* ------------------------ Public defines ------------------------------ */

#define MAX_RSA_SIZE 256
/* Enter Point for Save Log group of commands */
#define GROUP_SAVELOG 0
#define MAX_EVENTNAME_LEN 11

/* Flash slot types */

/**
 * @addtogroup GenericFunction
 * @{ 
 */

typedef struct /* 10 Bytes */
{
	uint32  uMagICWord;
	uint16  wTotalItemNb;
	uint16  wCurrentItemNb;
	uint16  wOverWriteFlag;
} __attribute__ ((packed)) SLOG_HDR; 

typedef struct //16 Bytes
{
	uint8    bDay;
	uint8    bMonth;
	uint8    bYear;
	uint8    bHour;
	uint8    bMinute;
	uint8    pbEventName[MAX_EVENTNAME_LEN];
} __attribute__ ((packed)) SLOG_ITEM;

#define T_PUBKEY_AXALTO_CA    0x00      /*!< Top level certification public key */
#define T_PUBKEY_TERM_CA      0x01      /*!< Terminal manufacturer CA key       */
#define T_PUBKEY_AUTH_SP      0x02      /*!< Firmware authentication key        */
#define T_PUBKEY_AUTH_CUST    0x03      /*!< Customer authentication key        */
#define T_PUBKEY_UNLOCK       0x04      /*!< Terminal unlocking key             */
#define T_PUBKEY_TERM         0x05      /*!< Terminal authentication public key */
#define T_PUBKEY_INJ          0x06      /*!< Customer injection public key      */
#define T_PUBKEY_SERV_CA      0x07      /*!< Server CA public key               */
#define T_PUBKEY_SERV         0x08      /*!< Server public key                  */
#define T_PUBKEY_PROMPT_A     0x09      /*!< Public key system prompt           */
#define T_PUBKEY_PROMPT_B     0x0A      /*!< Public key customer prompt         */
#define T_PUBKEY_RESET_B      0x0B      /*!< Public key for key reset               */
#define T_PUBKEY_AUTH_MP_A    0x0C      /*!< Public key for axalto package authentication */
#define T_PUBKEY_AUTH_MP_B    0x0D      /*!< Public key for customer package authentication */
#define T_PUBKEY_RESET_A      0x0E      /*!< Public key for key reset               */
#define T_PUBKEY_TERM_PP      0x0F      /*!< Public key for PP authentication   */

#define T_PUBKEY_AUTH_CUST_EXT 0x28     /*!< 2nd Customer authentication key  */
#define T_PUBKEY_AUTH_PP_EXT   0x29     /*!< AUTH_PP Public key of external device  */
#define T_PUBKEY_TERM_PP_EXT   0x2A     /*!< AUTH_TERM Public key of external device    */

#define T_PUBKEY_TERM_CA_CERT   0x11    /*!< Terminal manuf. CA certificate   */
#define T_PUBKEY_AUTH_SP_CERT   0x12    /*!< Firmware authentication certif.  */
#define T_PUBKEY_AUTH_CUST_CERT 0x13    /*!< Customer authen. certificate     */
#define T_PUBKEY_UNLOCK_CERT    0x14    /*!< Terminal unlocking certificate   */
#define T_PUBKEY_TERM_CERT      0x15    /*!< Terminal authen. certificate     */
#define T_PUBKEY_INJ_CERT       0x16    /*!< Customer injection certificate   */
#define T_PUBKEY_PROMPT_A_CERT  0x19    /*!< System prompt certificate        */
#define T_PUBKEY_PROMPT_B_CERT  0x1A    /*!< Customer prompt certificate      */
#define T_PUBKEY_RESET_B_CERT   0x1B    /*!< Reset certificate                */
#define T_PUBKEY_AUTH_MP_A_CERT 0x1C    /*!< Axalto package auth. certificate */
#define T_PUBKEY_AUTH_MP_B_CERT 0x1D    /*!< Customer package auth. certificate */
#define T_PUBKEY_RESET_A_CERT   0x1E    /*!< Reset certificate                */
#define T_PUBKEY_TERM_PP_CERT   0x1F    /*!< Terminal auth. certificate for PP */


#define T_PUBKEY_AUTH_CUST_EXT_CERT   0x38   /*!< 2nd customer authen. certificate  */


#define T_KEK_DATA              0x50

#define T_PRIVKEY_TERM          0x20    /*!< Terminal private key               */
#define T_PRIVKEY_TERM_PP       0x21    /*!< Terminal auth. priv. for PP          */
#define T_PUBKEY_AUTH_PP_A      0x22    /*!< M3 Pinpad mutual auth key A or B     */
#define T_PUBKEY_AUTH_PP_B      0x23    /*!< M3 Pinpad mutual auth key A or B     */
#define T_PUBKEY_AUTH_PP_A_CERT 0x24    /*!< M3 pinpad mutual auth cert A */
#define T_PUBKEY_AUTH_PP_B_CERT 0x25    /*!< M3 pinpad mutual auth cert B */
#define T_PUBKEY_TERM_M3PA_CERT 0x26    /*!< Terminal public key M3P certificate  */
#define T_PUBKEY_TERM_M3PB_CERT 0x27    /*!< Terminal public key M3P certificate  */

/* -- Content Type Definitions - ENCRYPTED BY TRK -- */

#define T_CUSTOMER_TMK        0x60      /*!< TMK customer                       */
#define T_CUSTOMER_MK         0x61      /*!< MK from customer                   */
#define T_SYSTEM_MK           0x62      /*!< MK system                          */
#define T_DUKPT               0x63      /*!< DUKPT key from customer            */

/* Offline PIN modes */

#define  SECSTD_EMV_PLAINTEXT        1
#define  SECSTD_EMV_CIPHERED         2
#define  SECSTD_EMV_PLAINTEXT_MONEO  3  /* TRK-648 : add */
#define  SECSTD_EMV_TEST          0x80

/* RAM slot key usage */
#define  PRIV_USE_PIN        0x02       /*!<  PIN key  */
#define  PRIV_USE_MAC        0x03       /*!<  MAC key  */
#define  PRIV_USE_PROMPT     0x04       /*!<  PROMPT key   */

/* RAM slot key usage (Continue)) */
#define  PRIV_USE_ENC_MSG    0x07       /*!<  ENC MSG key  */
#define  PRIV_USE_DEC_MSG    0x08       /*!<  DEC MSG key  */

/* PP authentication specific KeyIDs */
#define  MAK_PP_KEY_ID       0x05
#define  PAK_PP_KEY_ID       0x06
#define  OPK_PP_KEY_ID       0x07
#define  USBK_PP_KEY_ID  	  0x09

/* Padding Mode for Data Formation */
#define  PADD_ISO9797_METH_1  0x01
#define  PADD_ISO9797_METH_2  0x02
#define  PADD_ISO9797_METH_3  0x03

/* Method used to import key */
#define IMPORT_METHOD_DES_ECB  0x01
#define IMPORT_METHOD_DES_CBC  0x02 
#define IMPORT_METHOD_TR31 	 0x03

/* Message Encrypt/Decrypt Option */
#define  MSG_OPT_ENC        0x00
#define  MSG_OPT_DEC        0x01

/* Mode for Generate MAC */
#define  MAC_MODE_1         0x01
#define  MAC_MODE_3         0x03

/*! @} */

/* -------------------- Public types and enums -------------------------- */

typedef enum
{
   OP_DECRYPT = 0,              /* Use decryption operation */
   OP_ENCRYPT                   /* Use encryption operation */
} OP_ENC_DEC_TYPE;

typedef enum
{
   DUKPT_MAC_REQ = 0, /**< DUKPT MAC, Request or both ways*/
   DUKPT_MAC_RES, /**< DUKPT MAC, Response */
} DUKPT_MAC_KEY_VAR;

typedef enum
{
   DUKPT_DATA_NO_PAD = 0, /**< No padding method applied*/
   DUKPT_DATA_PAD_1, /**< Pad method 1 (ISO/IEC 9797-1:1999)*/
   DUKPT_DATA_PAD_2, /**< Pad method 2 (ISO/IEC 9797-1:1999)*/
} DUKPT_DATA_PAD;

typedef enum
{
   DUKPT_ENC_DATA_REQ = 0, /**< DUKPT Encrypt Data, Request or both ways*/
   DUKPT_ENC_DATA_RES, /**< DUKPT Encrypt Data, Response */
} DUKPT_DATA_KEY_VAR;

/** 
@ingroup    SECSTD_TYPES
@brief      TDES modes of operation
*/
typedef enum
{
   ECB = 0,		/**< ECB Mode. Default value */
   CBC,     /**< CBC mode */
} MODE_OPT;

/** 
@ingroup    SECSTD_TYPES
@brief      Types of MAC algorithm
*/

typedef enum
{
   DUKPT_MAC_NO_PAD = 0, /**< No padding method applied*/
   DUKPT_MAC_PAD_1, /**< Pad method 1 (ISO/IEC 9797-1:1999)*/
   DUKPT_MAC_PAD_2, /**< Pad method 2 (ISO/IEC 9797-1:1999)*/
} DUKPT_MAC_PAD;

/** 
@ingroup    SECSTD_TYPES
@brief      Length of MAC for MK Injection
*/
typedef enum
{
   MK_MAC_4 = 0, 	/**< 4-byte MAC Default value */
   MK_MAC_3,   /**< 3-byte MAC */
} MK_MAC_LEN;


typedef struct
{
   uint32 uKeyID;               /* Key unique identifier     */
   uint32 uExponent;            /* Public key exponent       */
   uint16 wModuloLength;        /* Public key modulo         */
   uint8 tbModulus[1];          /* Public key modulus        */
} __attribute__ ((packed)) PUBKEY_TYPE;


typedef struct
{
   uint32 uKeyID;               /* Key unique identifier     */
   uint16 wPrivExpLength;       /* Private Exponent length   */
   uint8 tbPrivExp[1];          /* Private Exponent          */
} __attribute__ ((packed)) PRIVKEY_TYPE;


typedef struct
{
   uint32 uCertifiedID;         /* Certified key ID         */
   uint32 uCertifyingID;        /* Certifying key ID        */
   uint32 uValidFrom;           /* Valid from ddmmyyyy      */
   uint32 uValidTo;             /* Valid to ddmmyyyy        */
   uint16 wSignatureSize;       /* Signature size in bytes  */
   uint8 tbSignature[1];        /* Certificate data         */
} __attribute__ ((packed)) PUBCERT_TYPE;

/* Slot allocation table (SAT) structure - Slot 0 */
typedef struct
{
   uint8 bVersion;
   uint16 wMagicWord;
   uint16 wNumBlocks;
   uint16 wFreeBlocks;
   uint8 bNumSlots;
   uint8 bFreeSlots;
   uint8 bRFU[1];
}  __attribute__ ((packed)) SAT_SLOT0;

/* Slot allocation table (SAT) structure - Slot n */
typedef struct
{
   uint8 bContentType;
   uint8 bOwnerID;
   uint16 wCrc;
   uint32 uKeyID;
   uint16 wStartBlock;
} __attribute__ ((packed)) SAT_SLOTN;

/* Master Key - Session Key Structure */
typedef struct
{
   uint8 bXORVector[8];
   uint16 wSKSize;
   uint8 bDecryptMode;
   uint8 bEncryptedSK[1];
}   __attribute__ ((packed)) MK_SK_STRUCT;

/* Session Key - Data Structure */
typedef struct
{
   uint8 bXORVector[8];
   uint8 bEncDecOpt;
   uint8 bEncryptMode;
   uint8 bPaddingMethod;
}   __attribute__ ((packed)) SK_DT_STRUCT;


/**
 * @addtogroup GenericFunction
 * @{ 
 */


/*! @} */

 /* ----------------- Public constants and variables --------------------- */

 /* ------------------ Public functions declaration ---------------------- */

#if defined(__cplusplus) && !defined(__DEBUG__)
extern "C"
{
#endif

OS_STATUS OS_Sec_LoadCertificate( uint8 f_bCertType, uint16 f_wCertLength,
											 uint8 * f_pbCertificate );

OS_STATUS OS_Sec_LoadPublicKey( uint8 f_bKeyType, uint16 f_wPubKeyLength,
										  uint8 * f_pbPublicKey );

OS_STATUS OS_Sec_GetCertif( uint8 f_bType, uint16 * f_pwCertLength,
									 uint8 * f_pbCertificate );

OS_STATUS OS_Sec_GetPublicKey( uint8 f_bType, uint16 * f_pwPubKeyLenth,
										 uint8 * f_pbPubKey );

OS_STATUS OS_Sec_GenerateKEK( uint8 f_bType, uint16 * f_pwLength,
										uint8 * f_pbeKEK );

OS_STATUS OS_Sec_GetE2PROMKeySlot( uint8 f_bSlot, uint8 * f_pbContentType,
											  uint8 * f_pbStatus,
											  uint16 * f_pwInfoLen,
											  uint8 * f_pbInfo );

OS_STATUS OS_Sec_GetSPStatus( uint16 * f_pwSPStatus, uint32 * f_puUKSR,
										uint32 * f_uSECIRQ, uint32 * f_puUKSRCur,
										uint32 * f_uSECIRQCur );

OS_STATUS OS_Sec_GetSPStatus_Ext( uint32 * f_puData );

OS_STATUS OS_Sec_GetSPMode( uint8  * f_bMode ) ;

OS_STATUS OS_Sec_LoadTMK( uint8 f_bKeyOwner, uint16 f_wKeySize,
								  uint8 * f_pbKeyData, uint8 * f_pbMAC );

OS_STATUS OS_Sec_LoadMK( uint8 f_bKeyOwner, uint8 f_bKeyID,
								 uint8 f_bKeyUsage, uint16 f_wKeySize,
								 uint8 * f_pbKeyData, uint8 * f_pbMAC );

OS_STATUS OS_Sec_LoadDUKPT( uint8 f_bKeyOwner, uint8 f_bKeyID,
									 uint16 f_wKeySize, uint8 * f_pbKeyData,
									 uint8 * f_pbKSN, uint8 * f_pbMAC );

OS_STATUS OS_Sec_EncryptPIN_Online( uint8 f_bKeyOwnerID, uint8 f_bKeyID,
												uint8 * f_pbPAN, uint16 f_wKeySize,
												uint8 * f_pbEncryptedKey,
												uint8 * f_pbePINBlock );

OS_STATUS OS_Sec_EncryptPIN_Online_Ext( uint8 f_bKeyOwnerID, uint8 f_bKeyID,
											uint8 * f_pbPAN,
											uint8 f_bImportMethod,
											uint16 f_wKeySize,
											uint8 * f_pbEncryptedKey,
											uint8 * f_pbePINBlock );
											
OS_STATUS OS_Sec_GenerateMAC( uint8 f_bKeyOwnerID, uint8 f_bKeyID,
										uint8 f_bMode, uint16 f_wKeySize,
										uint8 * f_pbEncryptedKey,
										uint16 f_wDataLength, uint8 * f_pbData,
										uint8 * f_pbMAC );

OS_STATUS OS_Sec_EncryptPIN_Dukpt( uint8 f_bKeyOwnerID, uint8 f_bKeyID,
											  uint8 * f_pbPAN, uint8 * f_pbSMID,
											  uint8 * f_pbePINBlock );

OS_STATUS OS_Sec_EncryptPIN_Offline( uint8 f_bPINMode,
                                     uint8* f_pIccRandom, 
                                     uint8 * f_pbModulo, uint16 f_wModuloLength,
                                     uint32 f_uExponent, 
                                     SC_STATUS * f_sc_status,
                                     SWORD * f_sw1sw2 );

OS_STATUS OS_Sec_Unlock( uint8 * f_pbChallenge );

OS_STATUS OS_Sec_Reset( uint8 * f_pbResetPattern );

OS_STATUS OS_Sec_SetSecureDate( uint32 f_uSecureDate );

OS_STATUS OS_Sec_GenerateRSAKeyPair( uint8 f_bType, uint32 f_uExponent,
												 uint16 f_wSize,
												 uint8 f_bForceRenew );

OS_STATUS OS_Sec_GetRandom( uint8 * f_pbBuffer, uint16 f_wLength );

OS_STATUS OS_Sec_GetEMVVersion( uint32 * f_puEMVVersion );

OS_STATUS OS_Sec_PP_LoadKEK( uint8 f_bPPType, uint8 * f_pbeKEK,
									  uint16 f_wKeyLength );

OS_STATUS OS_Sec_PP_LoadMK( uint8 f_bPPType, uint8 f_bMKType,
									 uint8 * f_pbeMK, uint16 f_wKeyLength,
									 uint8 * f_pbMAC );

OS_STATUS OS_Sec_PP_AuthPPStatus( uint8 f_bPPType );


OS_STATUS OS_Sec_ResetCounters( );
OS_STATUS OS_Sec_UpdateCounters( uint32 * f_pdwData , uint16 f_wDataSize  );
OS_STATUS OS_Sec_GetCounters( uint32 * f_pbData , uint16 f_wDataSize  );
OS_STATUS OS_Sec_RecordEvent( uint8 * f_pbBuffer, uint16 f_wLength );

OS_STATUS OS_Sec_GetKCV( uint8 f_bKeyOwnerID, uint32 f_dwKeyID,
								 uint8 f_bKVCMode, uint16* f_pwKVCLen, uint8* f_pbKVC);

OS_STATUS OS_Sec_ImportKey( uint8 f_bWrappingKeyOwnerID, uint32 f_dwWrappingKeyID,
									uint8 f_bKeyOwnerID, uint32 f_dwKeyID,
									uint8 f_bImportMethod, uint8 f_bKeyType, uint8 f_bKeyUsage,			
									uint16 f_wOptLen, uint8* f_pbOpt, uint16 f_wKeyLen, uint8* f_pbKey,	
									uint16 f_wMacLen, uint8* f_pbMac );
 
OS_STATUS OS_Sec_SetKeyVariant_DukptMAC( DUKPT_MAC_KEY_VAR f_bKeyVariant );

OS_STATUS OS_Sec_GenerateMAC_Dukpt( uint8 f_bKeyOwnerID,
												 uint16 f_bKeyID,
												 uint8 f_bPadMode,
												 uint8 f_bAlgMode,
												 uint16 f_wDataLength,
												 uint8 * f_pbData,
												 uint8 * f_pbSMID,
												 uint8 * f_pbeMAC );

OS_STATUS OS_Sec_EncryptData_Dukpt( uint8 f_bKeyOwnerID, 
                                    uint8 f_bKeyID,                                     
                                    DUKPT_DATA_KEY_VAR f_bKeyVariant,
                                    DUKPT_DATA_PAD f_bPadMethod, 
                                    MODE_OPT f_bEncMode,
                                    uint8 *f_pbInitVector,
                                    uint16 f_wInputDataLength, 
                                    uint8 * f_pbInputData,
                                    uint8 * f_pbSMID,
                                    uint16 * f_wOutputDataLength, 
                                    uint8 * f_pbOutputData   );
												
OS_STATUS OS_Sec_GetNextSMID( uint8 f_bKeyOwnerID, uint16 f_bKeyID, uint8 * f_pbSMID);

OS_STATUS OS_Sec_SetModeOperation( MODE_OPT f_bMode, MK_MAC_LEN f_bLength );

OS_STATUS OS_Sec_ImportPIN( );


#if defined(__cplusplus) && !defined(__DEBUG__)
}
#endif


#endif
