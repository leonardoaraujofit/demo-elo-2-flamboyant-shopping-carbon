/****************************************************************************************/
/* The only purpose of this file is to provide the root of doxygen documentation module */
/****************************************************************************************/
/**  
 * @defgroup LIBGUI_ROOT GUI library
 *  
 * @brief GUI library
 *  
 * @par Detailed description  
 *    This library provides high-level APIs to display graphical 
 *    widgets, with or without user interaction. 
 *  
 */
