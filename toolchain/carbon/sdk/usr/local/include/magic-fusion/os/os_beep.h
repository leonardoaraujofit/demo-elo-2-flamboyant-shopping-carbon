#include "./os/os_types.h"

#ifndef __API_BEEP_H__
#define __API_BEEP_H__


/** 
 *@brief Beep information structure
 *@ingroup OSSONO 
 */
typedef struct __attribute__ ( ( __packed__ ) )
{
    uint16  wFrequency; /**< Freqency in Hz  @ingroup OSSONO */
    uint16  wTimeout;   /**< Delay in milliseconds during which frequency will remain   @ingroup OSSONO */
} BUZZER_STATE;

#if defined(__cplusplus)
extern "C"
{
#endif

    OS_STATUS OS_Beep( uint16 timeout );
    OS_STATUS OS_BeepToFrequency( uint16 frequency, uint16 timeout );
    OS_STATUS OS_PlayBeepSequence ( BUZZER_STATE* f_pstBuzzerSates, uint8 f_bNbBeeps );

#if defined(__cplusplus)
}
#endif

#endif