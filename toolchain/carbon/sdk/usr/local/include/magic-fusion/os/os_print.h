/**
 * file    os_print.h
 *
 * @brief   MagIC3 Printer API
 *
 * @par     Descrition:
 *          This file contains definitions for the MagIC3 API
 *
 * @author
 *
 * @par     Copyright:
 *          Copyright (c) 2012 Verifone Inc.
 *          All Rights Reserved. No part of this software may be reproduced,
 *          transmitted, transcribed, stored in a retrieval system, or
 *          translated into any language or computer language, in any form
 *          or by any means electronic, mechanical, magnetic, optical,
 *          chemical, manual or otherwise, without the prior permission of
 *          VeriFone Inc.
 */

#ifndef __OS_PRINT_H__
#define __OS_PRINT_H__

#include <os/os_types.h>
#include <os/os_font.h>

#define PRINTER_OFF     FALSE
#define PRINTER_ON      TRUE

/**
 * @ingroup OSPRINT 
 * @brief  Printer type
 **/
typedef enum PrinterType
{
    PRN_THERMAL1,                /**< X series & Vx520 product printer */
    PRN_THERMAL2,                /**< C series product printer */
    PRN_IMPACT,                  /**< deprecated*/
    PRN_EXTERNAL                 /**< deprecated*/
} PRINTER_TYPE;

/**
 * @ingroup OSPRINT  
 * @brief Printer information structure
 **/
typedef struct
{
    PRINTER_TYPE prn_type;   /**< may be PRN_THERMAL1, PRN_THERMAL2, PRN_IMPACT or PRN_EXTERNAL */
    int16 dots_per_line;     /**< */
    int16 chars_per_line;    /**< */
    int8 last_char;          /**< */
    uint8 current_contrast;  /**< */
    uint8 max_contrast;      /**< */
    int16 current_font;      /**< 0 = main font; 1 = alternate font */
    int16 scale_x;           /**< */
    int16 scale_y;           /**< */
    EVT_ID prn_evt;          /**< deprecated*/
    int16 error_mask;        /**< deprecated*/
    int16 ready_mask;        /**< deprecated*/
    int16 empty_mask;        /**< deprecated*/
    int16 almost_empty_mask; /**< deprecated*/
} PRINTER_INFO_STRUCT;


/**
 * @ingroup OSPRINT  
 * @brief Printer job status 
 **/
typedef enum PrinterJobStatus
{
   PRINTER_JOB_COMPLETE,    /**< All printings are completed*/
   PRINTER_JOB_PRINTING,    /**< Printing in progress*/
   PRINTER_JOB_NOPAPER      /**< No paper*/
} PRINTER_JOB_STATUS;

/* Size of an entry of the index table */
#define INDEX_TABLE_TYPE unsigned short

/**
 * @ingroup OSPRINT  
 * @brief Printer font ID 
 **/
typedef enum PrinterFontID
{
   MAIN_FONT,           /**< */
   ALTERNATE_FONT       /**< */
} PRINTER_FONT_ID;


/* for printer flow redirection */ 
#define MAX_FILEPATH_SIZE 300

#define MAX_FILE_STORAGE_SIZE     (  128 * 1024  )
#define MAX_FILE_STORAGE_SIZE_TH  (  100 * 1024  )  


#define STR_MODE_SIZE                 4
#define PRT2F_OFF_MODE           0x0000
#define PRT2F_ON_MODE            0x0001
#define PRT2F_DUAL_MODE          0x0002
#define PRT2F_PERSISTENT_MODE    0x0100

#define IS_PRT2F_OFF_MODE(w)          ( ( w & PRT2F_ON_MODE )   == PRT2F_OFF_MODE  )
#define IS_PRT2F_ON_MODE(w)           ( ( w & PRT2F_ON_MODE )   == PRT2F_ON_MODE )
#define IS_PRT2F_SINGLE_FILE_MODE(w)  ( ( w & PRT2F_DUAL_MODE)  == 0 )
#define IS_PRT2F_DUAL_MODE(w)         ( ( w & PRT2F_DUAL_MODE ) == PRT2F_DUAL_MODE )
#define IS_PRT2F_PERSISTENT_MODE(w)   ( ( w & PRT2F_PERSISTENT_MODE ) == PRT2F_PERSISTENT_MODE   )
#define IS_PRT2F_VOLATILE_MODE(w)     ( ( w & PRT2F_PERSISTENT_MODE ) == 0   )
#define IS_PRT_THERMAL_REQUEST(w)      ( IS_PRT2F_OFF_MODE(w) || IS_PRT2F_DUAL_MODE(w) )

typedef struct
{
    uint8 szPath [ MAX_FILEPATH_SIZE  + 1 ] ;
    uint8 szMode [ STR_MODE_SIZE      + 1 ] ;
}Print2FileParams;

/* -- */

#if defined(__cplusplus) && !defined(__DEBUG__)
extern "C"
{
#endif
   OS_STATUS OS_PrinterInit( void );
   OS_STATUS OS_PrinterClose( void );   /* Internal use */
   OS_STATUS OS_PrintString( const uint8 *p_string, uint16 length );
   OS_STATUS OS_PrintBmp( const uint8 *p_bitmap, uint8 width, uint8 height8, uint8 *p_text, uint16 length );
   OS_STATUS OS_PrinterDefineFont( PRINTER_FONT_ID fontID, const uint8 *p_font, uint32 size );
   OS_STATUS OS_PrinterScaleFont( PRINTER_FONT_ID fontID, int16 scaleX, int16 scaleY );
   OS_STATUS OS_PrinterContrast( uint8 contrast );
   OS_STATUS OS_PrinterInfo( PRINTER_INFO_STRUCT *p_info );
   OS_STATUS OS_PrinterPaper( uint32 timeout );
   OS_STATUS OS_PrinterFlush( void );
   OS_STATUS OS_PrinterWaitJobComplete( PRINTER_JOB_STATUS *p_jobstatus, uint32 timeout );
   OS_STATUS OS_Print2FileSetMode( const uint8 *p_filePath, uint16 mode );
   OS_STATUS OS_Print2FileInfo( uint8 *p_filePath, uint16 *p_mode );
   OS_STATUS OS_Print2FileSetMode_Tf( const uint8 *p_filePath, uint16 mode );
   OS_STATUS OS_Print2FileInfo_Tf( uint8 *p_filePath, uint16 *p_mode );

#if defined(__cplusplus) && !defined(__DEBUG__)
}
#endif

#endif
