/**
 * @file    os_disp.h
 *
 * @brief   MagIC Display API
 *
 * @par     Description:
 *
 * @par     Copyright:
 *          Copyright (c) 2012 Verifone Inc.
 *          All Rights Reserved. No part of this software may be reproduced,
 *          transmitted, transcribed, stored in a retrieval system, or
 *          translated into any language or computer language, in any form
 *          or by any means electronic, mechanical, magnetic, optical,
 *          chemical, manual or otherwise, without the prior permission of
 *          VeriFone Inc.
 *
 */
#ifndef __API_DISP_H__
#define __API_DISP_H__

#include "./os/os_types.h"
#include "./os/os_font.h"
#include "./os/agp2conv.h"


/** @brief Display type 
 *@ingroup OSDISP */
typedef enum
{
    DISPLAY_UNKNOWN,  /**< Unknown display type */
    DISPLAY_TEXT,     /**< Display type TEXT */
    DISPLAY_GRAPHICS  /**< Display type GRAPHICS */
} DISPLAY_TYPE;


typedef enum
{
    FONT_TYPE_DEFAULT,   /* Default font type */
    FONT_TYPE_COMBINED,  /* It is a font that use combined characters */
} FONT_TYPE;

typedef uint16 DISP_TYPE;

/********** Font define values ***************/
/* System fonts */
#define SMALL_FONT         0x00 /**< Font slot system: Small Font (8x8) */
#define LARGE_FONT         0x01 /**< Font slot system: Large Font (16x8) */
#define LARGE_MINI_FONT    0x02 /**< Font slot system: 8x8 font centered on 16 pixels heigth */
/* User fonts */
#define USER_FONT1         0x08 /**< Font slot custom: used by AM, big numbers (time) */
#define USER_FONT2         0x09 /**< Font slot custom: free */
#define USER_FONT3         0x0A /**< Font slot custom: free */
#define USER_FONT4         0x0B /**< Font slot custom: free */
#define USER_FONT5         0x0C /**< Font slot custom: free */
#define USER_FONT6         0x0D /**< Font slot custom: free */
#define USER_FONT7         0x0E /**< Font slot custom: free */
#define USER_FONT8         0x0F /**< Font slot custom: free */
/* font attributes */
#define REVERSE            0x10 /**< Font attribute: reverse mode (white on black) */
#define UNDERLINE          0x20 /**< Font attribute: underline mode */
#define CLEAR              0x80 /**< Font attribute: clear mode */


/** @brief Icon IDs 
 *@ingroup OSDISP */
typedef enum
{
    DISP_ICON_RADIO_L0,  /**< Radio icon, level 0 */
    DISP_ICON_RADIO_L1,  /**< Radio icon, level  1 */
    DISP_ICON_RADIO_L2,  /**< Radio icon, level  2 */
    DISP_ICON_RADIO_L3,  /**< Radio icon, level  3 */
    DISP_ICON_RADIO_L4,  /**< Radio icon, level  4 */
    DISP_ICON_POWER,     /**< Power icon */
    DISP_ICON_BATTERY_L0,   /**< Battery icon, level 0 */
    DISP_ICON_BATTERY_L1,   /**< Battery icon, level 1 */
    DISP_ICON_BATTERY_L2,   /**< Battery icon, level 2 */
    DISP_ICON_BATTERY_L3,   /**< Battery icon, level 3 */
    DISP_ICON_BATTERY_L4,   /**< Battery icon, level 4 */
    DISP_ICON_KEYLOCK,   /**< Keylock icon */
    DISP_ICON_PP,   /**< Pinpad icon*/
    DISP_ICON_USB,   /**< USB icon */
    DISP_ICON_LAN,   /**< LAN/Ethernet icon */
    DISP_ICON_PHONE_ACTIVE,   /**< Modem icon (dialing or connected) */
    DISP_ICON_PHONE_LINE,   /**< Modem icon (line present) */
    DISP_ICON_SECURITY,   /**< Security icon */
    DISP_ICON_ACTIVITY_LEFT,   /**< Activity icon (left) */
    DISP_ICON_ACTIVITY_RIGHT,   /**< Activity icon (right) */
    DISP_ICON_INVALID           /**< used for consistency check */
} DISP_ICON_TYPE;



/** 
 * @brief Display information structure
 * @ingroup OSDISP  
 */
#pragma pack(1)
typedef struct
{
    /** Current font ID */
    uint16 wFontID;
    /** Maximum number of lines (with current font) */
    uint8 bMaxLines;
    /** Maximum number of columns (with current font) */
    uint8 bMaxColumns;
    /** Current font character width in pixels */
    uint8 bCharWidth;
    /** Current font character height in pixels */
    uint8 bCharHeight;
    /** ASCII offset of first character in current font */
    uint8 bFirstChar;
    /** Number of ASCII character in current font */
    uint8 bNbChar;
    /** Display type (see DISPLAY_TYPE) */
    uint8 bDispType;
    /** Width of the screen in pixels - only for graphic display */
    uint8 bScreenWidth;
    /** Heigth of the screen in pixels  - only for graphic display */
    uint8 bScreenHeight;
    /** Current contrast setting of the display */
    uint8 bCurrentContrast;
    /** Minimum contrast setting for the display */
    uint8 bMinContrast;
    /** Maximum contrast setting for the display */
    uint8 bMaxContrast;
    /** Current backlight setting of the display */
    uint8 bCurrentBacklight;
    /** Minimum backlight setting for the display */
    uint8 bMinBacklight;
    /** Maximum backlight setting for the display */
    uint8 bMaxBacklight;
    /** Current backlight timer in ms */
    uint32 uCurrentBklDelay;
    /** Current horizontal position of the cursor */
    uint8 bCursorCol;
    /** Current vertical position of the cursor */
    uint8 bCursorRow;
} DISPLAY_INFO_STRUCT;
#pragma pack()

/* this is for compatibility with older programs and is being phased out. */
//typedef DISPLAY_INFO_STRUCT MATRIX_CHAR_STRUCT;

   /*--- Function Prototypes ----*/

#if defined(__cplusplus) && !defined(__DEBUG__)
extern "C"
{
#endif

    OS_STATUS OS_DisplayInit( void );
    OS_STATUS OS_DisplayInfo( DISPLAY_INFO_STRUCT * info_ptr );

    OS_STATUS OS_DisplayString( uint16 line, uint16 column, uint16 length, const uint8 * string );
    OS_STATUS OS_DisplayStr( uint16 line, uint16 column, uint16 fontAttr, const uint8 * string );
    OS_STATUS OS_DisplayAdvancedString( uint16 line, uint16 posX, uint16 fontAttr, uint16 len,
                                        BOOLEAN isXabsoluteCoord,
                                        const uint8 * string );
    OS_STATUS OS_DisplayFlush( void );
    OS_STATUS OS_DisplayCursorPos( uint16 line, uint16 column );
    OS_STATUS OS_DisplayAbsoluteCursorPos( uint16 line, uint16 column );

    /* Graphic display */
    OS_STATUS OS_DisplayBitmap( uint16 posy, uint16 posx, uint8 width, uint16 total_length, const uint8 *bitmap );
    OS_STATUS OS_DisplayCaptureBitmap( uint16 posy, uint16 posx, uint8 width, int16 total_length, uint8 * bitmap );
    
    OS_STATUS OS_DisplayDefineNewFont( uint8 slot, const uint8 *fontData, uint32 size );
    OS_STATUS OS_DisplayDefineNewPromptFont( uint8 slot, const uint8 * fontData, uint32 size );
    OS_STATUS OS_DisplayAuthFont( uint8 slot, const uint8 *signature, uint32 size );
    OS_STATUS OS_DisplaySetDefaultFont( uint8 FontID );
    OS_STATUS OS_DisplayGetDefaultFont( uint8 *fontAttr );
    OS_STATUS OS_DisplaySwapDefaultFont( uint8 fontAttrNew, uint8 *fontAttrOld );

    OS_STATUS OS_DisplaySetContrast( uint8 contrast );

    OS_STATUS OS_DisplayBacklight( uint8 state );
    OS_STATUS OS_DisplayBacklightDelay( uint32 timeout );
    OS_STATUS OS_DisplayDisableBacklight( void );

    OS_STATUS OS_DisplayIcon( uint8 iconId, BOOLEAN enabled );

    /* Utf8 and combining char functions */
    OS_STATUS OS_DisplayDispLen( const uint8 *string, uint16 size, uint16 *count );
    OS_STATUS OS_DisplayByteLen( const uint8 *string, uint16 maxCount, uint16 *size);
    OS_STATUS OS_DisplayDispCpy( uint8 *outBuffer, const uint8 *inBuffer, uint16 copySize, uint16 *copied );

    /* the following are for compatibility with previous names only.
    * The OS_Display* names are preferable.
    */
    //no more supported: OS_SelectFont
    OS_STATUS OS_SetContrast( uint8 contrast );

#if defined(__cplusplus) && !defined(__DEBUG__)
}
#endif
#endif                          /* __API_DISP_H__ */
