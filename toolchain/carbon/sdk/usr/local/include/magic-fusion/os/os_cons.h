/**
 * file     $Source: /home/cvs/cobra/linux/rootfs/libos/include/os/os_cons.h,v $
 *
 * @brief   Definitions for OS information API
 *
 * @par     Descrition:
 *          This file declares basic types used for prompts in OS library. 
 *             
 *
 * @date          01 Jan 2006 (creation)
 *                $Date: 2010-06-18 14:11:00 $
 *
 * @author        $Author: besson $
 *
 * @version       $Revision: 1.36 $
 *
 * @par    Copyright:
 *         (c) Copyright Verifone 2012 unpublished work.
 *         This computer program includes confidential, proprietary 
 *         information and is a trade secret of Verifone. All use, disclosure
 *         and/or reproduction is prohibited unless authorized in writing.
 *         All rignts reserved.
 *
 */
#ifndef __API_CONS_H__
#define __API_CONS_H__

#include "./os/os_prompt.h"


#endif
