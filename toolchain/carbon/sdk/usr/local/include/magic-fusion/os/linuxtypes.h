/**
 * file linuxtypes.h
 * 
 * @brief	Linux data types
 *
 * @par		Descrition:
 *          	This file contains data types (structures) for linux OS
 *
 * @date      	20 Dec 2004 (creation)
 *            	$Date: 2005-10-05 16:39:10 $ (last modification)
 *
 * @author    	$Author: hameau $
 *
 * @version   	$Revision: 1.3 $
 *
 * @par       	Copyright:
 *            	(c) Copyright Verifone 2012 unpublished work.
 *            	This computer program includes confidential, proprietary 
 *            	information and is a trade secret of Verifone. All use, disclosure
 *            	and/or reproduction is prohibited unless authorized in writing.
 *            	All rignts reserved.
 *
 */

#ifndef LINUXTYPES_H_
#define LINUXTYPES_H_

#include "./os/os_types.h"
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>

#if __cplusplus
extern "C"
{
#endif

   /* fix weird bug with doxygen */
   typedef enum
   {
      azertyuiopqsdfghjklmwxcvbn
   } ABUABUABUABU;

   typedef struct
   {
      pthread_t *linux_id;
      TASK_ID smx_id;
      uint8 *name;
      uint32 stacksize;
      void ( *mainTask ) ( void );
      pthread_attr_t *attrTh;
      uint8 priority;
      BOOLEAN isLaunched;
   } TaskData;

   typedef struct
   {
      int32 linux_id;
      SEM_ID smx_id;
      uint8 *name;
   } SemData;

   typedef struct
   {
      //uint32 linux_id;
      int *tube;
      MBX_ID smx_id;
      uint8 *mbx_name;
   } MbxData;

   struct msgForm
   {
      long mtype;
      void *mtext;
   };

   typedef struct
   {
      uint16 msg_id;
      void *data;
      uint16 size;
      uint8 *name;
   } MsgData;

   // A CHANGER!!
   typedef struct
   {
      uint16 *smx_id;
      FILE *desc;
      uint8 *name;
   } FileData;

   typedef struct
   {
      uint16 smx_id;
      pthread_cond_t *linux_id;
      pthread_mutex_t *mutex;
      uint16 value;
      uint8 *name;
      BOOLEAN itThread;
      BOOLEAN itBroadcast;
   } EvtData;

   struct EvtTest
   {
      BOOLEAN and_mask;
      APP_EVT_MASK *result;
      EvtData *evtData;
      APP_EVT_MASK mask;
      pthread_t tid;
   };

#if __cplusplus
}
#endif

#endif                          /* LINUXTYPES_H_ */
