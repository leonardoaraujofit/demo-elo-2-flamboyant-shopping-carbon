/**
 * file    os_clk.h
 *
 * @brief	Definitions for Clock API
 *
 * @par		Descrition:
 *          	This file contains definitions for Clock API
 *
 * @date      	20 Dec 2004 (creation)
 *            	$Date: 2007-04-12 08:35:20 $ (last modification)
 *
 * @author    	$Author: hameau $
 *
 * @version   	$Revision: 1.9 $
 *
 * @par       	Copyright:
 *          (c) Copyright Verifone 2012 unpublished work.
 *          This computer program includes confidential, proprietary 
 *          information and is a trade secret of Verifone. All use, disclosure
 *          and/or reproduction is prohibited unless authorized in writing.
 *          All rignts reserved.
 *
 */


#include "./os/os_types.h"

#ifndef __API_CLK_H__
#define __API_CLK_H__

/*----- Time and timeout definitions -------------------*/
#define ZERO_TIMEOUT       0UL
#define INFINITE_TIMEOUT   0xFFFFFFFFUL
#define T_MSEC             1UL
#define T_SEC              1000UL
#define _50MS              50UL
#define _100MS             100UL
#define SEC_TIMEOUT        104  /* EMV00' 104 ticks per second */

/** @brief The TIMEOUT_TYPE enum is no more used in Cobra */
typedef enum
{
   NO_TIME = 0,
   NORMAL_TIME = 1,
   WRAP_AROUND_TIME = 2,
   INFIN_TIME = 3
} TIMEOUT_TYPE;

/** 
 *@brief This structure is used for the OS timer routines 
 *@ingroup OSCLK
 */
typedef struct
{
   TIMEOUT_TYPE t_class;        /** no effect, for compatibility only */
   uint16 target_time;          /** no effect, for compatibility only */
   uint32 linux_time;           /** current linux time in ms */
   uint32 timeout_wanted;       /** timeout wanted in ms */
} TIME_STRUCT;


#define JANUARY         1
#define FEBRUARY        2
#define MARCH           3
#define APRIL           4
#define MAY             5
#define JUNE            6
#define JULY            7
#define AUGUST          8
#define SEPTEMBER       9
#define OCTOBER         10
#define NOVEMBER        11
#define DECEMBER        12

#define UNUSED          0
#define NB_JANUARY      31
#define NB_FEBRUARY     28
#define NB_MARCH        31
#define NB_APRIL        30
#define NB_MAY          31
#define NB_JUNE         30
#define NB_JULY         31
#define NB_AUGUST       31
#define NB_SEPTEMBER    30
#define NB_OCTOBER      31
#define NB_NOVEMBER     30
#define NB_DECEMBER     31

#define SUNDAY          0
#define MONDAY          1
#define TUESDAY         2
#define WEDNESDAY       3
#define THURSDAY        4
#define FRIDAY          5
#define SATURDAY        6

/* the START_YEAR MUST be a leap year! */
/* This must match the base year used in the BIOS */
#define START_YEAR   1992
#define DAY_OF_WEEK_01_01_1992   WEDNESDAY

/***************************      structures    ****************************/

#if defined(__cplusplus)
extern "C"
{
#endif
   int16 openClkDriver( void );
   int16 closeClkDriver( int16 fd );
   OS_STATUS OS_ClkSetTime( uint16 hour, uint16 minute, uint16 second );
   OS_STATUS OS_ClkReadTime( uint16 * hour, uint16 * minute,
                             uint16 * second );
   OS_STATUS OS_ClkSetDate( uint16 day_of_week, uint16 day_of_month,
                            uint16 month, uint16 year );
   OS_STATUS OS_ClkReadDate( uint16 * day_of_week, uint16 * day_of_month,
                             uint16 * month, uint16 * year );
   OS_STATUS OS_ClkTimerSet( TIME_STRUCT * timer_ptr, uint32 timeout );
   BOOLEAN OS_ClkTimerExpired( TIME_STRUCT * timer_ptr );
   OS_STATUS OS_ClkSetAlarm( EVT_ID et, APP_EVT_MASK event, int16 hour,
                             int16 minute, int16 second );
   OS_STATUS OS_ClkResetAlarm( void );
#if defined(__cplusplus)
}
#endif

#endif
