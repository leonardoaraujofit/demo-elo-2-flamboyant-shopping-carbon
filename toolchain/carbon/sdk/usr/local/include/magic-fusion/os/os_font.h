/**
 * file    os_font.h
 *
 * @brief	Definitions for font structure (printer or display)
 *
 * @par		Descrition:
 *          This file contains definitions for font structure
 *
 * @date      	20 Dec 2004 (creation)
 *            	$Date: 2010-06-18 14:11:59 $ (last modification)
 *
 * @author    	$Author: besson $
 *
 * @version   	$Revision: 1.7 $
 *
 * @par       	Copyright:
 *          (c) Copyright Verifone 2012 unpublished work.
 *          This computer program includes confidential, proprietary 
 *          information and is a trade secret of Verifone. All use, disclosure
 *          and/or reproduction is prohibited unless authorized in writing.
 *          All rignts reserved.
 *
 */

#ifndef __OS_FONT_H__
#define __OS_FONT_H__

#include "./os/os_types.h"
#include "./os/os_disp.h"


/* First displayable character in the ascii table */
#define FIRST_ASCII_DISP_CHAR   0x18

/** @brief Font descriptor structure*/
typedef struct
{
   unsigned short wID;            /** Font ID */
   unsigned short wType;          /** Font type  (=0) */
   unsigned char bWidth;          /** Font width in bits   */
   unsigned char bByteHeight;     /** Font height in bytes */
   unsigned char bUnderlineMask;  /** Underline mask */
   unsigned char bFirstAsciiChar; /** ASCII offset of first character */
   unsigned char bAsciiCharNb;    /** Number of ASCII character */
   unsigned short wExtCharNb;     /** Number of extended characters */
   unsigned short wCombCharNb;    /** Number of base combining characters */
} __attribute__ ( ( packed ) ) FONT_DESC;

typedef unsigned short BASE_TABLE_TYPE;
typedef unsigned short COMB_TABLE_TYPE;


#endif
