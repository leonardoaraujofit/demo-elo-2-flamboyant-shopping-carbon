/**
 * @file
 * @brief   Magnetic stripe card reader API
 *
 * @date
 *
 * @author
 *
 * @version
 *
 * @par    Copyright:
 *         Copyright (c) 2012 Verifone Inc.
 *         All Rights Reserved. No part of this software may be reproduced,
 *         transmitted, transcribed, stored in a retrieval system, or
 *         translated into any language or computer language, in any form
 *         or by any means electronic, mechanical, magnetic, optical,
 *         chemical, manual or otherwise, without the prior permission of
 *         VeriFone Inc.
 */

#include "./os/os_types.h"

#ifndef __API_OS_MAGNETIC_H__
#define __API_OS_MAGNETIC_H__

/* track sizes in bytes */
#define TRACK_SIZE_RAW 76           /**< RAW track size (ISO 1,2 and 3) @ingroup OSMAG*/
#define TRACK_SIZE_ASCII_1    79    /**< ASCII ISO1 track size  @ingroup OSMAG*/
#define TRACK_SIZE_ASCII_2    40    /**< ASCII ISO2 track size @ingroup OSMAG*/
#define TRACK_SIZE_ASCII_3    107   /**< ASCII ISO3 track size @ingroup OSMAG*/


/** Mag track types 
 *@ingroup OSMAG */
typedef enum
{
   MAG_TRACK_1, /**< ASCII track 1 */
   MAG_TRACK_2, /**< ASCII track 2 */
   MAG_TRACK_3, /**< ASCII track 3 */
   RAW_TRACK_1, /**< Raw track 1   */
   RAW_TRACK_2, /**< Raw track 2   */
   RAW_TRACK_3  /**< Raw track 3   */
} TRACK_TYPE;

/** 
 * The MAG_INFO_STRUCT is used for retreiving information about
 * the mag stripe reader setup.
 * @ingroup OSMAG 
 */
typedef struct
{
   BOOLEAN track1; /**< Does reader can read track 1? (TRUE/FALSE)*/
   BOOLEAN track3; /**< Does reader can read track 3? (TRUE/FALSE)*/
} MAG_INFO_STRUCT;

#if defined(__cplusplus)
extern "C"
{
#endif
   OS_STATUS OS_MagInit( BOOLEAN track1, BOOLEAN track3 );
   BOOLEAN OS_MagCardSwiped( uint32 timeout );
   OS_STATUS OS_MagRead( TRACK_TYPE track, uint8 * buffer, uint32 timeout );
   OS_STATUS OS_MagFlush( void );
   OS_STATUS OS_MagInfo( MAG_INFO_STRUCT * info_ptr );
#if defined(__cplusplus)
}
#endif

#endif
