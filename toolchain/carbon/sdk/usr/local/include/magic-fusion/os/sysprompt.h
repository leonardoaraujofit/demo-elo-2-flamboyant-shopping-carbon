/**
 * file     $Source: /home/cvs/cobra/linux/rootfs/libos/include/os/sysprompt.h,v $
 *
 * @brief   Definitions of system prompts list
 *
 * @par     Descrition:
 *          This file declares system prompt table
 *             
 *
 * @date          01 Jan 2006 (creation)
 *                $Date: 2010-05-11 14:42:43 $
 *
 * @author        $Author: w8 $
 *
 * @version       $Revision: 1.15 $
 *
 * @par    Copyright:
 *         (c) Copyright Verifone 2012 unpublished work.
 *         This computer program includes confidential, proprietary 
 *         information and is a trade secret of Verifone. All use, disclosure
 *         and/or reproduction is prohibited unless authorized in writing.
 *         All rignts reserved.
 *
 */
#ifndef __SYS_PROMPT_H__
#define  __SYS_PROMPT_H__

/* System Prompt ID */
typedef enum
{
   SYS_PR_PHONE_NUMBER,
   /*
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_KEEP_LAST_KEY, PR_FKEY_COR,
      PR_KEYSET_NUMERIC + PR_KEYSET_SPACE,
      "<L>PHONE NUMBER?</L>\n<R><DLN[NNNNNNNNNNNNNNNNNNNNNNNN]></R>"}
    */

   SYS_PR_DIAL_PREFIX_ID,
   /*
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_KEEP_LAST_KEY, PR_FKEY_COR,
      PR_KEYSET_NUMERIC + PR_KEYSET_SPACE + PR_KEYSET_PHONE,
      "<L>DIAL PREFIX:</L>\n<R><DRNNNNNNNNNN></R>"}
    */

   SYS_PR_DIAL_POSTFIX,
   /*
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_KEEP_LAST_KEY, PR_FKEY_COR,
      PR_KEYSET_NUMERIC + PR_KEYSET_SPACE,
      "<L>DIAL POSTFIX:</L>\n<R><DRNNNNNNNNNN></R>"}
    */

   SYS_PR_PAD_NUMBER,
   /*
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_KEEP_LAST_KEY, PR_FKEY_COR,
      PR_KEYSET_NUMERIC + PR_KEYSET_SPACE,
      "<L>PAD NUMBER?</L>\n<R><DLN[NNNNNNNNNNNNNNNNNNNNNNNN]></R>"}
    */

   SYS_PR_SCENARIO_NUMBER,
   /*
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_KEEP_LAST_KEY, PR_FKEY_COR,
      PR_KEYSET_NUMERIC + PR_KEYSET_SPACE + PR_KEYSET_ALPHA_UP +
      PR_KEYSET_ALPHA_LOW + PR_KEYSET_SPECIALS,
      "<L>SCENARIO NUMBER?</L>\n<R><DRNNNNN></R>"}
    */

   SYS_PR_REMOTE_SERVER_NAME,
   /*
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_KEEP_LAST_KEY, PR_FKEY_COR,
      PR_KEYSET_SPACE + PR_KEYSET_ALPHA_UP + PR_KEYSET_ALPHA_LOW +
      PR_KEYSET_NUMERIC + PR_KEYSET_SPECIALS,
      "<L>REMOTE SERVER:</L>\n<L><DL[AAAAAAAAAAAAAAAAAAAAAAAAAAAAA]></L>"}
    */

   SYS_PR_IP_ADDRESS_CONFIG,
   /*                                                                            
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_NONE, PR_FKEY_COR,                                           
      PR_KEYSET_NUMERIC + PR_KEYSET_SPACE,                                                                
      "<U><L><IP CONFIGURATION></L></U>\n<L><PRAA[AAAAAAAAA]></L>\n<R><DL[NNN.NNN.NNN.NNN]></R>"}         
    */


   SYS_PR_USER_NAME,
   /*
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_KEEP_LAST_KEY, PR_FKEY_COR,
      PR_KEYSET_SPACE + PR_KEYSET_ALPHA_UP + PR_KEYSET_ALPHA_LOW +
      PR_KEYSET_NUMERIC,
      "<L>USER NAME:</L>\n<L><DL[AAAAAAAAAAAAAAAAAAAAAAAAAAAAA]></L>"}
    */

   SYS_PR_PASSWORD,
   /*
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_KEEP_LAST_KEY, PR_FKEY_COR,
      PR_KEYSET_SPACE + PR_KEYSET_ALPHA_UP + PR_KEYSET_ALPHA_LOW +
      PR_KEYSET_NUMERIC,
      "<L>PASSWORD:</L>\n<L><DL[AAAAAAAAAAAAAAAAAAAAAAAAAAAAA]></L>"}
    */

   SYS_PR_TCP_PORT_NUMBER_ID,
   /*
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_KEEP_LAST_KEY, PR_FKEY_COR,
      PR_KEYSET_NUMERIC,
      "<L>TCP PORT NUMBER:</L>\n<L><DRN[NNNN]></L>"}
    */

   SYS_PR_ENTER_DATE,
   /*
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_KEEP_LAST_KEY, PR_FKEY_COR,
      PR_KEYSET_NUMERIC,
      "<L>DATE [YYYY/MM/DD]</L>\n<R><DR0000/00/00></R>"}
    */

   SYS_PR_ENTER_TIME,
   /*
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_KEEP_LAST_KEY, PR_FKEY_COR,
      PR_KEYSET_NUMERIC,
      "<L>TIME [HH:MM:SS]</L>\n<R><DRNN:NN:NN></R>"}
    */

   SYS_PR_NUMERIC_PARAM,
   /*
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_KEEP_LAST_KEY, PR_FKEY_COR,
      PR_KEYSET_NUMERIC,
      "<L>ENTER PARAMETER</L>\n<L><PLAAAAAAAAAA[AAAAAA]></L>\n<R><DLNNN[NNNNNNNNNNNNN]></R>"}
    */

   SYS_PR_ALPHANUMERIC_PARAM,
   /*
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_KEEP_LAST_KEY, PR_FKEY_COR,
      PR_KEYSET_NUMERIC,
      "<L>ENTER PARAMETER</L>\n<L><PLAAAAAAAAAA[AAAAAA]></L>\n<R><DLAAA[AAAAAAAAAAAAA]></R>"}
    */

   SYS_PR_MAC_ADDRESS,
   /*
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_KEEP_LAST_KEY, PR_FKEY_COR,
      PR_KEYSET_NUMERIC,
      "<L>MAC ADDRESS:</L>\n<R><DRNN:NN:NN:NN:NN:NN></R>"}
    */

   SYS_PR_SERIAL_NUMBER,
   /*
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_KEEP_LAST_KEY, PR_FKEY_COR,
      PR_KEYSET_NUMERIC,
      "<L>SERIAL NUMBER:</L>\n<R><DRNNNNNNNNNN></R>"}
    */

   SYS_PR_INIT_MODEM,
   /*
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_KEEP_LAST_KEY, PR_FKEY_COR,
      PR_KEYSET_NUMERIC + PR_KEYSET_SPACE + PR_KEYSET_ALPHA_UP +
      PR_KEYSET_ALPHA_LOW + PR_KEYSET_SPECIALS,
      "<L>MODEM COMMAND:</L>\n<R><DRAA[AAAAAAAAAAAAAAAAAAAAAAAAAAAA]></R>"}
    */


   SYS_PR_UNLOCK_RESET_PIN_ID,
   /*
      {PR_TYPE_PIN, DEFAULT_FONT_ID, PR_OPT_COR_CLEAR_ALL, 0,
      PR_KEYSET_NUMERIC,
      "<C>ENTER <PLAAAAA[A]> PIN 1:</C>\n<R><DR[****]****></R>"}
    */

   SYS_PR_REMOTE_IP,
   /*                                                                                    
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_KEEP_LAST_KEY, PR_FKEY_COR,                
      PR_KEYSET_NUMERIC, 0x00,                                                           
      "<U><L><REMOTE IP></L></U>\n<R><DL[NNN.NNN.NNN.NNN]></R>\0"}                       
    */

   SYS_PR_LOCAL_IP,
   /* {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_KEEP_LAST_KEY, PR_FKEY_COR,                
      PR_KEYSET_NUMERIC, 0x00,                                                           
      "<U><L><LOCAL IP></L></U>\n<R><DL[NNN.NNN.NNN.NNN]></R>\0"}                        
    */

   SYS_PR_GATEWAY,
   /*                                                                                    
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_KEEP_LAST_KEY, PR_FKEY_COR,                
      PR_KEYSET_NUMERIC, 0x00,                                                           
      "<U><L>GATEWAY</L></U>\n<R><DL[NNN.NNN.NNN.NNN]></R>\0"}                           
    */


   SYS_PR_BROADCAST,
   /*                                                                                    
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_KEEP_LAST_KEY, PR_FKEY_COR,                
      PR_KEYSET_NUMERIC, 0x00,                                                           
      "<U><L>BROADCAST</L></U>\n<R><DL[NNN.NNN.NNN.NNN]></R>\0"}                         
    */

   SYS_PR_NETMASK,
   /*                                                                                    
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_KEEP_LAST_KEY, PR_FKEY_COR,                
      PR_KEYSET_NUMERIC, 0x00,                                                           
      "<U><L>NETMASK</L></U>\n<R><DL[NNN.NNN.NNN.NNN]></R>\0"}                           
    */

   SYS_PR_DNS,
   /*                                                                                    
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_KEEP_LAST_KEY, PR_FKEY_COR,                
      PR_KEYSET_NUMERIC, 0x00,                                                           
      "<U><L>DNS</L></U>\n<R><DL[NNN.NNN.NNN.NNN]><PLAA></R>\0"}                         
    */

   SYS_PR_TIMEOUT,
   /*                                                                                    
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_KEEP_LAST_KEY, PR_FKEY_COR,                
      PR_KEYSET_NUMERIC, 0x00,                                                           
      "<U><L>TIMEOUT</L></U>\n<R><DR[NNN]NNN></R>\0"}                                    
    */
   SYS_PR_UNLOCK_RESET_PIN2_ID,
   /*
      {PR_TYPE_PIN, DEFAULT_FONT_ID, PR_OPT_COR_CLEAR_ALL, 0,
      PR_KEYSET_NUMERIC,
      "<C>ENTER <PLAAAAA[A]> PIN 2:</C>\n<R><DR[****]****></R>"}
    */
   SYS_PR_PIN_CODE_ID,
   /* 
      {PR_TYPE_PIN2, DEFAULT_FONT_ID, PR_OPT_COR_CLEAR_ALL, 0,
      PR_KEYSET_NUMERIC,
      "<C>ENTER PIN CODE:</C>\n<R><DR[****]****></R>"}     
    */

   SYS_PR_PART_NUMBER_ID,
   /*
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_COR_CLEAR_ALL, 0,
      PR_KEYSET_NUMERIC | PR_KEYSET_ALPHA_UP,
      "<L>PART NUMBER?</L>\n<R><DRAAAA></R>"}
    */
   SYS_PR_TERMINAL_ID,
   /*
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_NONE, 0,
      PR_KEYSET_SPACE + PR_KEYSET_ALPHA_UP + PR_KEYSET_ALPHA_LOW +
      PR_KEYSET_NUMERIC + PR_KEYSET_SPECIALS,
      "<L>TERMINAL ID?</L>\n<R><DR[AAAAAAAAAAAAAAA]></R>"}
    */
   SYS_PR_COUNTRY_CODE,
   /*
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_NONE, 0,
      PR_KEYSET_ALPHA_UP,
      "<L>COUNTRY CODE?</L>\n<R><DRAA></R>"}
    */

   SYS_PR_GPRS_APN_NAME,
   /* 
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_NONE, 0,
      PR_KEYSET_SPACE + PR_KEYSET_ALPHA_UP + PR_KEYSET_ALPHA_LOW +
      PR_KEYSET_NUMERIC + PR_KEYSET_SPECIALS,
      "<L>GPRS APN:</L>\n<L><DL[AAAAAAAAAAAAAAAAAAAAAAAAAAAAA]></L>"},
    */

   SYS_PR_GPRS_NUMBER,
   /*
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_NONE, 0,
      PR_KEYSET_SPACE + PR_KEYSET_ALPHA_UP + PR_KEYSET_ALPHA_LOW +
      PR_KEYSET_NUMERIC + PR_KEYSET_SPECIALS,
      "<L>GPRS NUMBER:</L>\n<L><DL[AAAAAAAAAAAAAAAAAAAAAAAAAAAAA]></L>"},
    */

   SYS_PR_GPRS_SIM_PIN,
   /*
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_COR_CLEAR_ALL, 0,
      PR_KEYSET_NUMERIC,
      "<C>SIM PIN CODE?</C>\n<R><DR[****]****></R>"},  
    */

   SYS_PR_GPRS_OPERATOR,
   /*
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_NONE, 0,
      PR_KEYSET_SPACE + PR_KEYSET_ALPHA_UP + PR_KEYSET_ALPHA_LOW +
      PR_KEYSET_NUMERIC + PR_KEYSET_SPECIALS,
      "<L>OPERATOR ID:</L>\n<L><DLNNNNN></L>"},    
    */

   SYS_PR_WIFI_SSID,
   /*
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_NONE, 0,
      PR_KEYSET_SPACE + PR_KEYSET_ALPHA_UP + PR_KEYSET_ALPHA_LOW +
      PR_KEYSET_NUMERIC + PR_KEYSET_SPECIALS,
      "<L>SSID:</L>\n<L><DLAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA></L>"},   
    */

   SYS_PR_WIFI_PASSPHRASE,
   /*
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_NONE, 0,
      PR_KEYSET_SPACE + PR_KEYSET_ALPHA_UP + PR_KEYSET_ALPHA_LOW +
      PR_KEYSET_NUMERIC + PR_KEYSET_SPECIALS,
      "<L>Passphrase:</L>\n<L><DLA[A127]></L>"},       
    */

   SYS_PR_WIFI_WEPKEY,
   /*
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_NONE, 0,
      PR_KEYSET_HEXA,
      "<L>Wep key:</L>\n<L><DLN10[N16]></L>"},    
    */

   SYS_PR_WIFI_PRIORITY,
   /*
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_NONE, 0,
      PR_KEYSET_NUMERIC,
      "<L>Priority:</L>\n<L><DLNNN></L>"},
    */

   SYS_PR_TMS_NUMBER,
   /*
      {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_KEEP_LAST_KEY, PR_FKEY_COR,
      PR_KEYSET_NUMERIC + PR_KEYSET_SPACE + PR_KEYSET_ALPHA_UP +
      PR_KEYSET_ALPHA_LOW + PR_KEYSET_SPECIALS,
      "<L>SCENARIO NUMBER?</L>\n<R><DLNNNNN.NNNNNNNNNN></R>"}
    */
   
   SYS_PR_BT_PIN,
   /*
     {PR_TYPE_OTHER, DEFAULT_FONT_ID, PR_OPT_COR_CLEAR_ALL, 0,
      PR_KEYSET_NUMERIC,
      "<C>BLUETOOTH PIN:</C>\n<R><DR[************]****></R>"},   
   */
   SYS_PR_MAX_ID,               /* This ID must stay at the end of the enum */
} SYSTEM_PROMPT_ID;

#endif
