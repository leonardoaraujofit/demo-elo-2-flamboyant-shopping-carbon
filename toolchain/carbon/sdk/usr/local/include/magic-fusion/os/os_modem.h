/**
 * file    os_modem.h
 *
 * @brief MagIC Modem API
 *
 * @par Descrition:
 *      This file contains definitions of MagIC Modem API
 *
 * @par  Copyright:
 *      Copyright (c) 2012 Verifone Inc.
 *      All Rights Reserved. No part of this software may be reproduced,
 *      transmitted, transcribed, stored in a retrieval system, or
 *      translated into any language or computer language, in any form
 *      or by any means electronic, mechanical, magnetic, optical,
 *      chemical, manual or otherwise, without the prior permission of
 *      VeriFone Inc.
 *
 */


#ifndef __OS_MODEM_H__
#define __OS_MODEM_H__

#include "os_types.h"
#include "os_ser.h"

// For use by OS_ModemDial()
#define DIAL_TONE       0x54
#define DIAL_PULSE      0x50
#define DIAL_NO_WAIT    0x01

/* modem version and type strings length, including ending '\0' */
#define MODEM_VERSION_LENGTH        32
#define MODEM_TYPE_SIZE             10

// Maximum phone-number string size accepted by OS_ModemDial()
#define PHONE_NUMBER_SIZE 32


// OS_ModemFunction base command numbers
#define FUNC_CODE_BASE_SI2415 0x0000    // SI2415 specific commands
typedef enum
{
   RUN_SI2415_PATCH = FUNC_CODE_BASE_SI2415 + 1,     /**< Activate Si2415 specific patch that fixes connection errors */
   RESET_MODEM = FUNC_CODE_BASE_SI2415 + 2          /**< Reset modem chip */
} MODEM_FUNC_CODE;

/** @brief Modem connection information 
 *@ingroup OSMODEM*/
typedef struct
{
    uint8   modulation_str[16]; /**< Modulation string returned by modem i.e. "V22B", "V34" ...*/
    uint32  baudrate_tx;        /**< Emission rate in bit/s  */
    uint32  baudrate_rx;        /**< Reception rate in bit/s */
    uint8   xfer_7bits;         /**< 7-bits transfer emulation. 0 : disabled (8-bits mode), 1 : enabled*/
    uint8   xfer_7bits_parity;  /**< 7-bits transfer parity. 0 : even, 1 : odd */       
}MODEM_CONNECTION_INFO_STRUCT;

#if defined(__cplusplus) && !defined(__DEBUG__)
extern "C"
{
#endif
   OS_STATUS OS_ModemInit( const COMM_PARAM_STRUCT *p_params );
   OS_STATUS OS_ModemInfo( COMM_PARAM_STRUCT *p_params );
   OS_STATUS OS_ModemCommand( const uint8 *p_command, uint16 length );
   OS_STATUS OS_ModemSend( const uint8 *p_data, uint16 length );
   OS_STATUS OS_ModemReceive( uint8 *p_outBuffer, uint16 size, uint16 *p_count, uint32 timeoutFirstChar, uint32 timeoutInterChar );
   OS_STATUS OS_ModemConnect( uint32 timeout );
   OS_STATUS OS_ModemDial( const uint8 *p_dialingCommand, uint16 length, uint8 dialType, uint32 timeout);
   OS_STATUS OS_ModemFlush( BUFFER_TYPE buffers );
   OS_STATUS OS_ModemDisconnect( void );
   OS_STATUS OS_ModemAbort( void );
   OS_STATUS OS_ModemStatus( uint32 timeout );
   OS_STATUS OS_ModemVersion( uint8 *p_outVersion );
   OS_STATUS OS_ModemSleep( void );
   OS_STATUS OS_ModemFunction( MODEM_FUNC_CODE code, ... );
   OS_STATUS OS_ModemResult( uint32 timeout );
   
   OS_STATUS OS_ModemGetConnectionInfo( MODEM_CONNECTION_INFO_STRUCT *p_connInfo );
#if defined(__cplusplus) && !defined(__DEBUG__)
}
#endif
#endif
