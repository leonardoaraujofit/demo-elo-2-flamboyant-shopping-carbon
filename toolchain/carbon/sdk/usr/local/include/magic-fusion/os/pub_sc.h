/**
 * @file
 * $Source: magic-fusion/libos/include/os/pub_sc.h,v $
 *
 * @brief	Smart Card Reader APIs
 *
 * @par		Descrition:
 *			This file contains functions that manage the Smart Card Reader
 *
 * @date	11 Avr 2011 (creation)
 *
 * @author	$Author: ledauphin $
 *
 * @version	$Revision: 1.00 $
 *
 * @par		Copyright:
 *			(c) Copyright Verifone 2012 unpublished work.
 *			This computer program includes confidential, proprietary 
 *			information and is a trade secret of Verifone. All use, 
 *			disclosure and/or reproduction is prohibited unless
 *			authorized in writing.
 *			All rignts reserved.
 */
#ifdef __KERNEL__
#include <gemalto/sprotocol/agp2conv.h>
#else
#include "agp2conv.h"
#endif


#ifndef _PUB_SCLIB_
#define _PUB_SCLIB_

/* Constants defining the Command Case (T=0 & T=1 protocols) */
#define       C_CASE_0                   0 /**< @ingroup OSSC */
#define       C_CASE_1                   1 /**< @ingroup OSSC */
#define       C_CASE_2S                0x2 /**< @ingroup OSSC */
#define       C_CASE_3S                0x3 /**< @ingroup OSSC */
#define       C_CASE_4S                0x4 /**< @ingroup OSSC */
#define       C_CASE_2E                0x2E /**< @ingroup OSSC */
#define       C_CASE_3E                0x3E /**< @ingroup OSSC */
#define       C_CASE_4E                0x4E /**< @ingroup OSSC */


/* Followings Cases are particulars. See low-level SC API documentation @ingroup OSSC */
#define       C_CASE_3SU               0x83     /**< Special Case Format to T=1 @ingroup OSSC */
#define       C_CASE_INIT              0xFF     /**< Specifics initializations @ingroup OSSC */
#define       C_INIT_CLAGETRESPONSE    0xFE     /**< Specific to T=0 @ingroup OSSC */

// Followings Cases are specific, see
// SC API documentation.
#define C_CASE_4S_CHIPPER     0x14    /**< Specific to Chipper Card @ingroup OSSC */
#define C_CASE_3SU            0x83    /**< Special Case Format to T=1 @ingroup OSSC */
#define C_CASE_INIT           0xFF    /**< Specifics initializations @ingroup OSSC */
#define C_INIT_CLAGETRESPONSE 0xFE    /**< Specific to T=0 @ingroup OSSC */
#define C_CASE_4CB            0x4C    /**< Commande BA Test FIME @ingroup OSSC */


/* Constants for T=0 & T=1 */
#define APDU_MAX_SIZE          266 /**< @ingroup OSSC */

/* ATR maximum size */
#define ATR_MAXLEN         34 /**< @ingroup OSSC */

/* number of the reader in the terminal */
#define MAX_READER        5 /**< @ingroup OSSC */

/* Define the numer of availabe readers */
#define SAM_SLOT_NUMBER   4 /**< @ingroup OSSC */

/** Status Word 1&2  @ingroup OSSC */
typedef struct __attribute__ ( ( __packed__ ) )
{
   uint8 bSw1; /**< @ingroup OSSC */
   uint8 bSw2; /**< @ingroup OSSC */
} SW1SW2;

/** Status words SW1-SW2 access @ingroup OSSC */
typedef union __attribute__ ( ( __packed__ ) )
{
   uint16 wSw;      /**< access @ingroup OSSC */
   SW1SW2 wSwByte; /**< status word @ingroup OSSC */
} SWORD;

#define SC_OK                 1 /**<success. no error encountered @ingroup OSSC */
#define SC_ERROR              2 /**<Error occurred with the SC rdr hardware @ingroup OSSC */
#define SC_CARD_REMOVED       3 /**<card not present @ingroup OSSC */
#define SC_CARD_PRESENT       4 /**<card is present @ingroup OSSC */
#define SC_ERROR_VCC_DROP     5 /**<VCC power down during SC communication @ingroup OSSC */
#define SC_TIMEOUT            6 /**<internal timeout during card access @ingroup OSSC */
#define SC_ERROR_CARD_UNKNOWN 7 /**<card type not handled @ingroup OSSC */
#define SC_ERROR_BAD_PARAM    8 /**<Bad input parameter @ingroup OSSC */
#define SC_ERROR_PROTO        9 /**<Protocol error @ingroup OSSC */
#define SC_ERROR_SYNC1        10 /**< @ingroup OSSC */
#define SC_ERROR_SYNC2        11 /**< @ingroup OSSC */
#define SC_ERROR_SYNC3        12 /**< @ingroup OSSC */
#define SC_NO_BAY             13 /**< @ingroup OSSC */
#define SC_BAD_CARD_TYPE      14 /**< @ingroup OSSC */
#define SC_REJECT             15 /**< @ingroup OSSC */
#define SC_RESET_OK           16 /**< @ingroup OSSC */
#define SC_WARM_REJECT        17 /**< @ingroup OSSC */
#define SC_PARITY             18 /**< @ingroup OSSC */
#define SC_NO_ANWSER          19 /**< @ingroup OSSC */
#define SC_COM_ERROR          20 /**< @ingroup OSSC */
#define SC_TARGET_ERROR       21 /**< @ingroup OSSC */
typedef unsigned char SC_STATUS; /**< @ingroup OSSC */

#define SAM_1_SC_RDR    0 /**< @ingroup OSSC */
#define SAM_2_SC_RDR    1 /**< @ingroup OSSC */
#define SAM_3_SC_RDR    2 /**< @ingroup OSSC */
#define SAM_4_SC_RDR    3 /**< @ingroup OSSC */
#define CUSTOMER_SC_RDR 4 /**< @ingroup OSSC */
typedef unsigned char SC_READER; /**< @ingroup OSSC */

#define SC_WARM_RESET   0 /**< @ingroup OSSC */
#define SC_COLD_RESET   1 /**< @ingroup OSSC */
typedef unsigned char SC_RESET_MODE; /**< @ingroup OSSC */

#define ISO 0                   /**< ISO/IEC 7816 standard @ingroup OSSC */
#define EMV 1                   /**< EMV'96 standard @ingroup OSSC */
typedef unsigned char SC_STANDARD; /**< @ingroup OSSC */


// Type of protocol to apply to async cards; used with 'Smart Card Connection'
// (INT 55H, Funct 01H)

#define T0_PROTOCOL 0x00
#define T1_PROTOCOL 0x01
typedef unsigned char SC_PROTOCOL;

#define SCLIB_MiniCash     0x01
#define SCLIB_Cybermark    0x02
#define SCLIB_B0prime      0x04
#define SCLIB_EmvL2Test    0x08


/* Mask for sync cards */
#define IO_MASK   0x01
#define CLK_MASK  0x02
#define RST_MASK  0x04
#define C4_MASK   0x08
#define C8_MASK   0x10
#define VCC_MASK  0x20

#define SYNC_SET_PIN   0x01
#define SYNC_CLEAR_PIN 0x02
#define SYNC_READ_PIN  0x03

#define SYNC_MSB_FIRST 1
#define SYNC_LSB_FIRST 2

/** VCC values @ingroup OSSC */
typedef enum
{
   VCC_18 = 0x01, /**< @ingroup OSSC */
   VCC_03 = 0x02, /**< @ingroup OSSC */
   VCC_05 = 0x03 /**< @ingroup OSSC */
} SC_VCC;

// Data Structure used in PPS Protocol precedure
typedef struct
{
   uint8 bFn;                   // Clock rate conversion factor [0..6,9..13] && Fd<=Fn<=Fi
   uint8 bDn;                   // Bit Rate Adjustement factor      [1..6,8..9]  && Dd<=Dn<=Fi
   uint8 bPn;                   // Protocol Type to be selected     in { TD1, TD2, TD3 }
} PPS_STRUCT;

#define VERSION_STR_SIZE   32 /**< Size of version string @ingroup OSSC */
#define IFM_VERSION_SIZE   32 /**< Size of latest ifm certified sclib version @ingroup OSSC */

/** @ingroup OSSC */
typedef struct
{
   char version[VERSION_STR_SIZE]; /**< @ingroup OSSC */
} SC_INFO_STRUCT;

/** @ingroup OSSC */
typedef struct
{
   char ifm_version[IFM_VERSION_SIZE]; /**< @ingroup OSSC */
} SC_IFM_VERSION_STRUCT;

//SC_INFO_ST structure is a parameter of SC_Info() function
typedef struct __attribute__ ( ( __packed__ ) )
{
   uint8 bProto;
   uint8 bReset;
   uint8 bOption;
} SC_INFO_ST;

// Max size for data exchange with remote or local sclib
#define SC_DATA_MAX_SIZE 300


#if defined(__cplusplus) && !defined(__DEBUG__)
extern "C"
{
#endif

   /****************************************************************************/
   /****** WARNING: this API is deprecated. Use SC_CommandCard instead!!! ******/
   /****************************************************************************/
   SC_STATUS SC_CardCommand( SC_READER /* fwReader */ ,
                             uint8 * /*fpBuffer */ ,
                             uint16 /*fwLength */ , SWORD * /*fpSw1Sw2 */  )
      __attribute__ ( ( deprecated ) );
   /****************************************************************************/

   SC_STATUS SC_CardInit( SC_READER );
   SC_STATUS SC_CardDetect( SC_READER /* fwReader */  );
   BOOLEAN SC_CardInserted( SC_READER /* card_slot */ ,
                            uint16 /* timeout */  );
   BOOLEAN SC_CardRemoved( SC_READER /* card_slot */ ,
                           uint16 /* timeout */  );
   SC_STATUS SC_CardReset( SC_READER /* fwReader */ ,
                           SC_RESET_MODE /* fwMode */ ,
                           SC_STANDARD /* fwSpec */ ,
                           uint8 * /* fpBuffer */  );
   SC_STATUS SC_CommandCard( SC_READER, uint8 *, uint16, uint16, SWORD * );
   SC_STATUS SC_CardDisconnect( SC_READER /* fwReader */  );
   SC_STATUS SC_SelectReader( SC_READER /* fwReader */  );
   
   SC_STATUS SC_Info( SC_READER f_wReader, SC_INFO_ST * f_stInfo );
   SC_STATUS SC_CardInfo( SC_READER /*fwReader */ ,
                          SC_INFO_STRUCT * /*card_info_ptr */  );
   SC_STATUS SC_CardIFMVersion( SC_READER /*fwReader*/,
                          SC_IFM_VERSION_STRUCT * /*ifm_version_ptr */);
   SC_STATUS SC_Abort( SC_READER /* f_wReader */ );
   SC_STATUS SC_GetAtr( SC_READER /* f_wReader */, uint8 * /* f_pbAtr */, uint16  /* f_wMaxLen */);
   void SC_CardLock( void );
   void SC_CardUnlock( void );

#if defined(__cplusplus) && !defined(__DEBUG__)
}
#endif

#endif
