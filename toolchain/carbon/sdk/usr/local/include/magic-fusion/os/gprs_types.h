/**
 @file
 $Source: /home/cvs/cobra/linux/rootfs/libos/include/os/gprs_types.h,v $

 @brief		Secure processor basic types definition

 @par		Descrition:
          	This file declares basic types used for secure processor
			confoming to Cobra coding rules

 @date      11 Avr 2005 (creation)
            $Date: 2010-06-18 14:10:25 $

 @author    $Author: besson $

 @version   $Revision: 1.7 $

 @par       Copyright:
			(c) Copyright Verifone 2012 unpublished work.
            This computer program includes confidential, proprietary 
            information and is a trade secret of Verifone. All use, 
            disclosure and/or reproduction is prohibited unless
            authorized in writing.
            All rignts reserved.
*/

#include "os/pub_os.h"

#ifndef _GPRS_TYPES_
#define _GPRS_TYPES_



/** GPRS Tag Type
 * @ingroup OSGPRS */
typedef enum
{
   OP_AUTO = 0,         /**< Automatic connection */
   OP_MANUAL = 1,      /**< Manual connection */
   OP_MANUAL_AUTO = 4 /**< Manual/Automatic connection */
} OP_SELECTION_MODE;

/** GenCom Operator selection Type
 * @ingroup OSGENCOM */
typedef struct
{
   uint32 operator_id;         /**< Location Area Identification name */
   OP_SELECTION_MODE mode;     /**< Operato's mode to use for connection (Name or LAI) = OP_FORMAT_TYPE */
} OPERATOR_SELECTION;

#define OPER_NAME_SIZE   32

#define MODULE_INFO_SIZE 32

/** GPRS Operator registration info
 * @ingroup OSGPRS */
typedef struct
{
   uint8 RegisterStatus;        /* Registration status */
   uint8 Operator[OPER_NAME_SIZE];      /* Operator's name */
   uint32 operator_id;                  /* GSM Location Area Identification */
} OPERATOR_INFO;

/* Pin code max length */
#define MAX_PIN_LENGTH     9

/** GenCom bearer Type
 * @ingroup OSGENCOM */
typedef enum
{
   GSM_BEARER_AUTO,
   GSM_BEARER_2400_V22BIS,
   GSM_BEARER_4800_V32,
   GSM_BEARER_9600_V32,
   GSM_BEARER_14400_V34,
   GSM_BEARER_2400_V110,
   GSM_BEARER_4800_V110,
   GSM_BEARER_9600_V110,
   GSM_BEARER_14400_V110
} BEARER_TYPE;

/** GenCom QoS Type
 * @ingroup OSGENCOM */
typedef enum
{
   QoSMIN,
   QoSREQ
} QoSType;

/** GenCom QoS Data
 * @ingroup OSGENCOM */
typedef struct
{
   uint16 precedence;
   uint16 delay;
   uint16 reliability;
   uint16 peak;
   uint16 mean;
} QoSDATA;

typedef OS_STATUS( *SIM_VERIFYPIN ) ( uint8 * newpin );
typedef OS_STATUS( *SIM_PRESENCE ) ( void );
typedef OS_STATUS( *SIM_PINREMAININGATTEMPTS ) ( uint16 * f_attempt );
typedef OS_STATUS( *SIM_CHANGEPIN ) ( uint8 * oldpin, uint8 * newpin );
typedef OS_STATUS( *SIM_UNBLOCKPIN ) ( uint8 * puk, uint8 * newpin );
typedef OS_STATUS( *SIM_ERASEPINFILE ) ( void );
typedef OS_STATUS( *SIM_CHECKPIN ) ( void );
typedef OS_STATUS( *GPRS_REQUESTSCID ) ( uint8 * scid, uint16 dataLen );
typedef OS_STATUS( *GPRS_REQUESTIMSI ) ( uint8 * imsi, uint16 dataLen );
typedef OS_STATUS( *GPRS_REQUESTIMEI ) ( uint8 * imei, uint16 dataLen );
typedef OS_STATUS( *GPRS_POWERUPMODULE ) (  );
typedef OS_STATUS( *GPRS_POWERDOWNMODULE ) (  );
typedef OS_STATUS( *GPRS_ENTERSLEEPMODE ) (  );
typedef OS_STATUS( *GPRS_QUITSLEEPMODE ) (  );
typedef OS_STATUS( *GPRS_SIGNALQUALITY ) ( uint16 * percentage, int *riss );
typedef OS_STATUS( *GPRS_REGISTERTOGSMNETWORK ) ( OPERATOR_SELECTION *
                                                  op_sel,
                                                  uint8 *
                                                  f_CurrentOperator );
typedef OS_STATUS( *GPRS_ISGSMREGISTERED ) ( void );
typedef OS_STATUS( *GPRS_UNREGISTERFROMGSMNETWORK ) (  );
typedef OS_STATUS( *GPRS_ATTACH ) ( uint8 * f_apn_name, uint8 * f_number );
typedef OS_STATUS( *GPRS_ISGPRSATTACHED ) ( void );
typedef OS_STATUS( *GPRS_DETACH ) (  );
typedef OS_STATUS( *GPRS_OPERATORNAME ) ( uint8 * output );
typedef OS_STATUS( *GPRS_OPERATORSLIST ) ( OPERATOR_INFO * f_OpInfo,
                                           uint8 f_bOpInfoSize,
                                           uint8 * f_nbOper );
typedef OS_STATUS( *GPRS_SETGSMBEARERSERVICE ) ( BEARER_TYPE bearer );
typedef OS_STATUS( *GPRS_SETQUALITYOFSERVICE ) ( QoSType f_Type,
                                                 QoSDATA * f_QoS );
typedef OS_STATUS( *GPRS_STARTALWAYSON ) ( void );
typedef OS_STATUS( *GPRS_STOPALWAYSON ) ( void );

typedef OS_STATUS( *GPRS_MODULEINFORMATION ) ( uint8 * f, uint8 s );

typedef struct
{
   SIM_VERIFYPIN SIM_VerifyPIN;
   SIM_PRESENCE SIM_Presence;
   SIM_PINREMAININGATTEMPTS SIM_PINRemainingAttempts;
   SIM_CHANGEPIN SIM_ChangePIN;
   SIM_UNBLOCKPIN SIM_UnblockPIN;
   SIM_ERASEPINFILE SIM_ErasePINFile;
   SIM_CHECKPIN SIM_CheckPIN;
   GPRS_REQUESTSCID GPRS_RequestSCID;
   GPRS_REQUESTIMSI GPRS_RequestIMSI;
   GPRS_REQUESTIMEI GPRS_RequestIMEI;
   GPRS_POWERUPMODULE GPRS_PowerUpModule;
   GPRS_POWERDOWNMODULE GPRS_PowerDownModule;
   GPRS_ENTERSLEEPMODE GPRS_EnterSleepMode;
   GPRS_QUITSLEEPMODE GPRS_QuitSleepMode;
   GPRS_SIGNALQUALITY GPRS_SignalQuality;
   GPRS_REGISTERTOGSMNETWORK GPRS_RegisterToGSMNetwork;
   GPRS_ISGSMREGISTERED GPRS_IsGSMRegistered;
   GPRS_UNREGISTERFROMGSMNETWORK GPRS_UnRegisterFromGSMNetwork;
   GPRS_ATTACH GPRS_Attach;
   GPRS_ISGPRSATTACHED GPRS_IsGPRSAttached;
   GPRS_DETACH GPRS_Detach;
   GPRS_OPERATORNAME GPRS_OperatorName;
   GPRS_OPERATORSLIST GPRS_OperatorsList;
   GPRS_SETGSMBEARERSERVICE GPRS_SetGSMBearerService;
   GPRS_SETQUALITYOFSERVICE GPRS_SetQualityOfService;
   GPRS_STARTALWAYSON GPRS_StartAlwaysON;
   GPRS_STOPALWAYSON GPRS_StopAlwaysON;
} GPRS_API_LIST;


typedef enum
{
   SIM_VERIFYPIN_ID,
   SIM_PRESENCE_ID,
   SIM_PINREMAININGATTEMPTS_ID,
   SIM_CHANGEPIN_ID,
   SIM_UNBLOCKPIN_ID,
   SIM_ERASEPINFILE_ID,
   SIM_CHECKPIN_ID,
   GPRS_REQUESTSCID_ID,
   GPRS_REQUESTIMSI_ID,
   GPRS_REQUESTIMEI_ID,
   GPRS_POWERUPMODULE_ID,
   GPRS_POWERDOWNMODULE_ID,
   GPRS_ENTERSLEEPMODE_ID,
   GPRS_QUITSLEEPMODE_ID,
   GPRS_SIGNALQUALITY_ID,
   GPRS_REGISTERTOGSMNETWORK_ID,
   GPRS_ISGSMREGISTERED_ID,
   GPRS_UNREGISTERFROMGSMNETWORK_ID,
   GPRS_ATTACH_ID,
   GPRS_ISGPRSATTACHED_ID,
   GPRS_DETACH_ID,
   GPRS_OPERATORNAME_ID,
   GPRS_OPERATORSLIST_ID,
   GPRS_SETGSMBEARERSERVICE_ID,
   GPRS_SETQUALITYOFSERVICE_ID,
   GPRS_STARTALWAYSON_ID,
   GPRS_STOPALWAYSON_ID,
   GPRS_MODULEINFORMATION_ID,
   GPRS_MAX_API_ID,
} GPRS_API_ID;

#endif
