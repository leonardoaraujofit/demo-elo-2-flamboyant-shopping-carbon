/**
 * file    os_linux.h
 *
 * @brief	Definitions for Linux encapsulation API
 *  
 * @par		Descrition:
 *          This file contains definitions for Linux API
 *
 * @date      	20 Dec 2004 (creation)
 *            	$Date: 2007-07-13 11:42:20 $ (last modification)
 *
 * @author    	$Author: gerard $
 *
 * @version   	$Revision: 1.7 $
 *
 * @par       	Copyright:
 *          (c) Copyright Verifone 2012 unpublished work.
 *          This computer program includes confidential, proprietary 
 *          information and is a trade secret of Verifone. All use, disclosure
 *          and/or reproduction is prohibited unless authorized in writing.
 *          All rignts reserved.
 *
 */

#ifndef __API_OS_LINUX_H__
#define __API_OS_LINUX_H__

#include "./os/os_types.h"
#include "./os/os_debug.h"
#include <pthread.h>

/* Application allowed priorities for tasks */
#define LOW_PRIORITY      2     //=LO
#define MEDIUM_PRIORITY   3     //=NORM
#define HIGH_PRIORITY     4     //=HI

/* Event mask definitions */
#define OR_MASK            0    /* Test for OR of flags */
#define AND_MASK           0x8000       /* Test for AND of flags */
#define AND_EVT            TRUE /* used in OS_EvtTest */
#define OR_EVT             FALSE        /* used in OS_EvtTest */
#define SUSPEND_TASK       TRUE /* used in OS_EvtTest */

/* The following defines pertain to the OS event table. These defines can
* be used to create event masks.
*/
#define   KEY_ERROR_EVT       0x0000
#define   KEY_PRESSED_EVT     0x0001
#define   KEY_RELEASED_EVT    0x0002

#define   MAG_SWIPED_EVT      0x0002
#define   PORT1_RX_EVT        0x0008
#define   PORT1_TX_EVT        0x0010
#define   PORT1_ST_EVT        0x0020
#define   PORT2_RX_EVT        0x0040
#define   PORT2_TX_EVT        0x0080
#define   PORT2_ST_EVT        0x0100
#define   PORT3_RX_EVT        0x0200
#define   PORT3_TX_EVT        0x0400
#define   PORT3_ST_EVT        0x0800
#define   PORT4_RX_EVT        0x1000
#define   PORT4_TX_EVT        0x2000
#define   PORT4_ST_EVT        0x4000

#ifdef __cplusplus
extern "C"
{
#endif



/*--- Semaphore API routines ------*/

   OS_STATUS OS_SemCreate( SEM_ID * id_ptr, uint8 threshold,
                           const uint8 * sem_name );
   OS_STATUS OS_SemObtain( SEM_ID id_ptr, uint32 timeout, BOOLEAN suspend );
   OS_STATUS OS_SemSignal( SEM_ID id_ptr );
   OS_STATUS OS_SemDelete( SEM_ID id_ptr );


/*--- Task API routines -----------*/

   OS_STATUS OS_TaskCreate( TASK_ID * id, uint8 priority,
                            void ( *task_main ) ( void ),
                            uint32 stack_size, const uint8 * task_name );
   OS_STATUS OS_TaskRecordSelf( TASK_ID * id_t, const uint8 * task_name );
   TASK_ID OS_TaskCurrent( void );
   OS_STATUS OS_TaskStart( TASK_ID id );
   OS_STATUS OS_TaskSuspend( TASK_ID id, uint32 timeout );
   OS_STATUS OS_TaskStop( TASK_ID id_ptr, uint32 timeout );
   OS_STATUS OS_TaskResume( TASK_ID id );
   OS_STATUS OS_TaskLock( void );
   OS_STATUS OS_TaskUnlock( void );
   BOOLEAN OS_TaskLocked( void );
   OS_STATUS OS_TaskDelete( TASK_ID id );
   OS_STATUS OS_TaskPriority( TASK_ID id, uint8 new_priority );
   BOOLEAN OS_TaskStarted( TASK_ID tid );
   OS_STATUS OS_TaskIdToLinuxID( TASK_ID tid, pthread_t * linux_id );
   OS_STATUS OS_TaskSuspendSelf( uint32 timeout );
   /* OS_TaskSleep = OS_TaskSuspendSelf */
   OS_STATUS OS_TaskSleep( uint32 timeout );

/*--- Event table API routines -----*/

   OS_STATUS OS_EvtCreate( EVT_ID * id_ptr, uint16 task_slots,
                           const uint8 * table_name );
   OS_STATUS OS_EvtClear( EVT_ID id_ptr, APP_EVT_MASK mask );
   OS_STATUS OS_EvtSet( EVT_ID id_ptr, APP_EVT_MASK mask );
   OS_STATUS OS_EvtTest( EVT_ID id_ptr, APP_EVT_MASK mask,
                         BOOLEAN, APP_EVT_MASK * result,
                         uint32 timeout, BOOLEAN suspend );

   OS_STATUS OS_EvtDelete( EVT_ID id_ptr );


/*--- Mailbox and message API routines ---*/

   OS_STATUS OS_MsgCreate( MSG_ID * id_ptr,
                           const void *msg_value_ptr,
                           uint16 msg_size, const uint8 * msg_name );
   OS_STATUS OS_MsgRead( MSG_ID id_ptr, void *msg_ptr, uint16 size );
   OS_STATUS OS_MsgDelete( MSG_ID id_ptr );
   OS_STATUS OS_MbxCreate( MBX_ID * id_ptr, const uint8 * mbx_name );
   OS_STATUS OS_MbxReceive( MBX_ID id_ptr, MSG_ID * message_ptr,
                            uint32 timeout, BOOLEAN suspend );
   OS_STATUS OS_MbxSend( MBX_ID id_ptr, MSG_ID message );
   OS_STATUS OS_MbxDelete( MBX_ID id_ptr );
   OS_STATUS OS_MbxNbMessages( MBX_ID id_ptr, uint16 * num );

#ifdef __cplusplus
}
#endif

#endif
