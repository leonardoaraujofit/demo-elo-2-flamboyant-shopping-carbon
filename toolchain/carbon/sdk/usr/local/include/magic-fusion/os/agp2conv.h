/****************************************************************************
 * @file	   $Source: /home/cvs/cobra/linux/kernel/linux-2.6.17/include/gemalto/sprotocol/agp2conv.h,v $
 *
 * @brief	Header of redefined types for COBRA platform
 *
 * @par		Descrition:
 *          	This header contains new types to use with COBRA platform,
 *            	and redefined variables for AGP2 appli compatibility.
 *
 * @date      	20 Dec 2004 (creation)
 *            	$Date: 2008-06-02 16:03:06 $ (last modification)
 *
 * @author    	$Author: hameau $
 *
 * @version   	$Revision: 1.2 $
 *
 * @par       	Copyright:
 *          (c) Copyright Verifone 2012 unpublished work.
 *          This computer program includes confidential, proprietary 
 *          information and is a trade secret of Verifone. All use, disclosure
 *          and/or reproduction is prohibited unless authorized in writing.
 *          All rignts reserved.
 *
 ***************************************************************************/


#ifndef _AGP2CONV_H_
#define _AGP2CONV_H_

/* added for Eclipse parser, SDKv3.2 */
#ifndef __BORLANDC__
#ifndef _ARM_PLATFORM_
#define _ARM_PLATFORM_
#endif
#endif


/******************************************************************************/
/**************************** ARM COMPILATION *********************************/
/******************************************************************************/

#define far
#define _far
#define __far
#define near
#define _near
#define __near
#define huge
#define _huge
#define __huge
#define _loadds
#define __loadds

/* __CHAR_UNSIGNED__ is set by CPP according to gcc command line options */

#ifdef __CHAR_UNSIGNED__
typedef char uint8;
typedef signed char int8;
#else
typedef unsigned char uint8;
typedef char int8;
#endif

typedef signed short int16;
typedef unsigned short uint16;

typedef signed long int32;
typedef unsigned long uint32;

typedef signed long long int64;
typedef unsigned long long uint64;

typedef uint16 BOOLEAN;
#ifndef FALSE
#define FALSE  ((BOOLEAN)0)
#endif
#ifndef TRUE
#define TRUE   ((BOOLEAN)1)
#endif
#ifndef NONE
#define NONE   ((BOOLEAN)2)
#endif


// Avoid type collision with MagIC fusion
#ifndef FUSION

/* old AGP2 types */
typedef uint8 BYTE;
typedef uint8 *BYTEPTR;
typedef unsigned short  WORD;
typedef unsigned long   DWORD;
typedef unsigned char   UCHAR;
typedef unsigned short  UINT;
typedef unsigned long   ULONG;

#endif

/* MAIN.C user_service cast */
#define AM_USER_SERVICE_CAST

/* MENU type */
typedef int8 INPUT_MENU_STATUS;


#endif /* ifdef _AGP2CONV_H_*/
