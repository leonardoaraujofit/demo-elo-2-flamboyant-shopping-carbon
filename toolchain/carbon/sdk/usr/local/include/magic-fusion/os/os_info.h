/**
 * @file    os_info.h
 *
 * @brief   Definitions for OS information API
 *
 * @par     Descrition:
 *        
 *             
 *
 * @date          29 Mar 2005 (creation)
 *                $Date: 2012-01-04 15:14:00 $
 *
 * @author        $Author: zhang $
 *
 * @version       $Revision: 1.29 $
 *
 * @par    Copyright:
 *         (c) Copyright Verifone 2012 unpublished work.
 *         This computer program includes confidential, proprietary 
 *         information and is a trade secret of Verifone. All use, disclosure
 *         and/or reproduction is prohibited unless authorized in writing.
 *         All rignts reserved.
 *
 */

#include "os/os_types.h"

#ifndef __API_OS_INFO_H__
#define __API_OS_INFO_H__

#include <platforminfo_api.h>

/**
 * @addtogroup OSINFO
 */
/*@{*/
#define SERIAL_SIZE           PI_SERIAL_NUMBER_SIZE /**< Size of serial number field */
#define HWTYPE_SIZE           PI_MIB_PART_NUMBER_SIZE /**< Size of part number field */
#define MACADDR_SIZE          6  /**< Size of MAC address field */
#define PCI_ID_SIZE           17 /**< Size of PCI identifier field */
#define TERM_MODEL_SIZE       PI_CIB_MODEL_ID_SIZE  /**< Size of terminal model field */
#define COUNTRY_CODE_SIZE     PI_MIB_COUNTRY_SIZE /**< Size of terminal country code */
#define DEFAULT_MAN_AREA_SIZE 30 /**< Size of manufacturer dedicated area */
#define TERM_ID_SIZE          10 /**< Terminal ID size */

typedef uint8 SERIAL_NUM[SERIAL_SIZE + 1]; /**< Terminal serial number type */
typedef uint8 HWTYPE[HWTYPE_SIZE + 1]; /**< Terminal part number type */
typedef uint8 MACINFO[MACADDR_SIZE + 1]; /**< Ethernet MAC address type */
typedef uint8 COUNTRY_CODE[COUNTRY_CODE_SIZE]; /**< Terminal country code type */
typedef uint8 TERM_MODEL[TERM_MODEL_SIZE]; /** < Terminal model > */
typedef uint8 TERM_ID[TERM_ID_SIZE + 1];

#define MACADDR_STR_SIZE 17 /* Size of the ASCII MAC address size : xx:xx:xx:xx:xx:xx */
typedef uint8 MACINFO_STR[MACADDR_STR_SIZE + 1]; /**< Ethernet ASCII format MAC address */

/* Flag for the magnetic header present */
#define MAG_HEAD_NONE 0x00 /**< No magnetic card reader */
#define MAG_HEAD_ISO1 0x01 /**< Track 1 can be read     */
#define MAG_HEAD_ISO2 0x02 /**< Track 2 can be read     */
#define MAG_HEAD_ISO3 0x04 /**< Track 3 can be read     */

/* Flag for the card slots present */
#define SC_CONFIG_NONE     0x00 /**< No smart card reader */
#define SC_CONFIG_SAM1     0x01 /**< SAM 1 reader present */
#define SC_CONFIG_SAM2     0x02 /**< SAM 2 reader present */
#define SC_CONFIG_SAM3     0x04 /**< SAM 3 reader present */
#define SC_CONFIG_SAM4     0x08 /**< SAM 4 reader present */
#define SC_CONFIG_FULL_SAM 0x10 /**< Full SAM reader present */
#define SC_CONFIG_CAM      0x20 /**< Full CAM reader present */

/* Flag for the keyboard type */
#define KB_TYPE_STANDARD   0x00 /**< Standard keyboard layout */
#define KB_TYPE_PERSIAN    0x01 /**< Persian keyboard layout  */
#define KB_TYPE_NO_BKL     0x80 /**< Keyboard has no backlight */

/* Flag for the display type */
#define DISP_TYPE_NO_BKL   0x80 /**< Display has no backlight */
#define DISP_TYPE_NO_ICON  0x40 /**< Display has no icons */

/* Flag for the printer type */
#define PRT_TYPE_NO_PRT    0x80 /**< No printer on this terminal */
#define PRT_TYPE_MODEL_A   0x00 /**< Printer model A or compatible (FTP-628MCL101) */
#define PRT_TYPE_MODEL_B   0x01 /**< Printer model B or compatible (FTP-428MCL501) */

#define PRT_TYPE_NO_PRT_STR "00"

/* Flag for the devices present */
#define DEV_COMM_SER1      0x0001   /**< Serial port 1 available */
#define DEV_COMM_SER2      0x0002   /**< Serial port 2 available */
#define DEV_COMM_USB1      0x0004   /**< USB host 1 available */
#define DEV_COMM_USB2      0x0008   /**< USB host 2 available */
#define DEV_COMM_USBDEV    0x0010   /**< USB device available */
#define DEV_COMM_ETH       0x0020   /**< Ethernet available */
#define DEV_COMM_RTCMOD    0x0040   /**< RTC modem available */
#define DEV_COMM_GPRSMOD   0x0080   /**< GPRS modem available */
#define DEV_COMM_UMTSMOD   0x0100   /**< UMTS modem available */
#define DEV_COMM_WIFI      0x0200   /**< WIFI available */
#define DEV_COMM_DECT      0x0400   /**< DECT handset */
#define DEV_COMM_IR        0x0800   /**< IR handset */

/* Terminal model */
#define TERM_MODEL_X5       "TX5" /**< MagIC3 X5  */
#define TERM_MODEL_X8       "TX8" /**< MagIC3 X8  */
#define TERM_MODEL_C5       "TC5" /**< MagIC3 C5  */
#define TERM_MODEL_M3G      "TMA" /**< MagIC3 M3G */

/* Pinpad model */
// PCI v1
#define PINPAD_MODEL_P3     "P3"  /**< MagIC3 P3 pinpad PCI v1*/
#define PINPAD_MODEL_P5     "P5"  /**< MagIC3 P5 smart pinpad PCI v1*/
// PCI v2
#define PINPAD_MODEL_P4     "P4" /**< MagIC3 P3 pinpad PCI v2*/
#define PINPAD_MODEL_P6     "P6" /**< MagIC3 P5 smart pinpad PCI v2*/
#define PINPAD_MODEL_PC     "PC" /**< MagIC3 P3 contactless pinpad PCI v2*/
#define PINPAD_MODEL_PE     "PE" /**< MagIC3 P5 smart and contactless pinpad PCI v2*/

/* Secure processor SN size */
#define PINPAD_SN_SIZE      10
#define PINPAD_PN_SIZE      4
#define PINPAD_PCI_SIZE     3
#define PINPAD_MODEL_SIZE   2

/* Default values */

/* String used to call information module */
#define SYSFS_INFORMATION_FILE "/sys/information/sys_info"

/* Country code type */

/** Pinpad IDCARD */
typedef struct
{
   uint8 abPN[PINPAD_PN_SIZE+1];       /**< Pinpad part number */
   uint8 abSN[PINPAD_SN_SIZE+1];       /**< Pinpad serial number */
   uint8 abPCI[PINPAD_PCI_SIZE+1];     /**< Pinpad PCI ID */
   uint8 abMODEL[PINPAD_MODEL_SIZE+1]; /**< Pinpad model (P3D, P5D) */
} __attribute__ ( ( packed ) ) PINPAD_IDCARD;

/** Terminal ID data structure */
typedef struct
{
   uint16 wSize;                               /**< Size of ID card structure */
   uint8 bMagHeadType;                         /**< Type of magnetic head */
   uint8 bSimConfig;                           /**< Smart card reader configuration */
   uint8 bKeyboardType;                        /**< Keyboard type */
   uint8 bDisplayType;                         /**< Display type */
   uint8 bPrinterType;                         /**< Printer model */
   uint8 bRamSize;                             /**< Total RAM size in Mb */
   uint8 bFlashSize;                           /**< Total Flash size in Mb */
   uint16 wCommPorts;                          /**< Communication port mapping */
   uint8 abSerialNum[SERIAL_SIZE];             /**< Terminal serial number (not NULL terminated !) */
   uint8 abHwType[HWTYPE_SIZE];                /**< Terminal part number */
   uint8 abMacAddress[MACADDR_SIZE];           /**< Ethernet MAC address (if applicable) */
   uint8 abPciId[PCI_ID_SIZE];                 /**< PCI identifier */
   uint8 abTermModel[TERM_MODEL_SIZE];         /**< Terminal type (NULL terminated) */ 
} __attribute__ ( ( packed ) ) TERMINAL_IDCARD_STRUCT;

typedef enum
{
   FEATURE_PRINTER,
   FEATURE_MOBILE,
   FEATURE_RS,
   FEATURE_RS0,
   FEATURE_RS1,
   FEATURE_RS2,
   FEATURE_RS3,
   FEATURE_RS4,
   FEATURE_RS5,
   FEATURE_RS6,
   FEATURE_RS7,
   FEATURE_RS8,
   FEATURE_RS9,
   FEATURE_MODEM,
   FEATURE_MODEM0,
   FEATURE_MODEM1,
   FEATURE_MODEM2,
   FEATURE_MODEM3,
   FEATURE_MODEM4,
   FEATURE_MODEM5,
   FEATURE_MODEM6,
   FEATURE_MODEM7,
   FEATURE_MODEM8,
   FEATURE_MODEM9,
   FEATURE_GPRS,
   FEATURE_GPRS0,
   FEATURE_GPRS1,
   FEATURE_GPRS2,
   FEATURE_GPRS3,
   FEATURE_GPRS4,
   FEATURE_ETH,                 /* Ethernet */
   FEATURE_ETH0,
   FEATURE_ETH1,
   FEATURE_ETH2,
   FEATURE_ETH3,
   FEATURE_ETH4,
   FEATURE_WLAN,                /* Wifi */
   FEATURE_WLAN0,
   FEATURE_WLAN1,
   FEATURE_WLAN2,
   FEATURE_WLAN3,
   FEATURE_WLAN4,
   FEATURE_BLUETOOTH,          /* bluetooth*/
   FEATURE_BLUETOOTH0,
   FEATURE_BLUETOOTH1,
   FEATURE_BLUETOOTH2,
   FEATURE_BLUETOOTH3,
   FEATURE_BLUETOOTH4,
   FEATURE_PINPAD,              /* pinpad */
   
   __LAST_FEATURE__,            /* Keep it at the end of the enum */
} FEATURE_TYPE;

typedef enum
{
   PINPAD_MODEL_UNKNOWN,
   P3_PCI_V1,
   P5_PCI_V1,
   P3_PCI_V2,
   P5_PCI_V2,
   P3_CONTACTLESS_PCI_V2,
   P5_CONTACTLESS_PCI_V2,
   __LAST_PINPAD_MODEL__,         /* Keep it at the end of the enum */
}PINPAD_MODEL;

#define SECURE_IDCODE_SIZE 50
#define SECURE_FAMILY_SIZE 10
#define SECURE_ID_SIZE  12
#define BB_SERIAL_DEVICE "/dev/RS2" // Device to communicate with bluetooth base through USB

/**
 *
 *   @ingroup OSINFO
 *
 *     @brief   Main device information structure
 *
 *     
 **/
#pragma pack(1)
typedef struct
{
   /** Firmware package IDCODE for terminal management server */
   uint8 abIdCode[SECURE_IDCODE_SIZE];       /** Security familly (PCI, INTERAC, others...) */
   uint8 abFamily[SECURE_FAMILY_SIZE];      /** PCI or equivalent certficiation ID */
   uint8 abSecureId[SECURE_ID_SIZE];
} SECURE_INFO_STRUCT;
#pragma pack()


/** Manufacturer dedicated data structure */
typedef uint8 MANUFACTURER_STRUCT[DEFAULT_MAN_AREA_SIZE];
/*@}*/

#if defined(__cplusplus)
extern "C"
{
#endif
   OS_STATUS OS_InfoVersion( uint32 * );      
   OS_STATUS OS_InfoCrc( uint16 * );
   OS_STATUS OS_InfoSerial( uint8 * );
   OS_STATUS OS_InfoHwType( uint8 * );   
   OS_STATUS OS_InfoMac( MACINFO f_szMac );   
   OS_STATUS OS_InfoGetIdCard( TERMINAL_IDCARD_STRUCT * );
   OS_STATUS OS_InfoSetIdCard( TERMINAL_IDCARD_STRUCT * );
   OS_STATUS OS_InfoSetManufPart( const MANUFACTURER_STRUCT * f_pstManuf );
   OS_STATUS OS_InfoGetManufPart( MANUFACTURER_STRUCT * f_pstManuf );
   
   OS_STATUS OS_InfoGetCountry( COUNTRY_CODE f_szCountry );
   OS_STATUS OS_InfoSetCountry( COUNTRY_CODE f_szCountry );
   OS_STATUS OS_InfoSetTerminalID( uint8 * f_szTerminalID );
   OS_STATUS OS_InfoGetTerminalID( uint8 * f_szTerminalID, uint8 f_bSize );

   OS_STATUS OS_InfoIsFeature( FEATURE_TYPE f_enFeature );

   OS_STATUS OS_InfoGetPinpadID( PINPAD_IDCARD *f_stIdcard );
   OS_STATUS OS_InfoSetPinpadID( const PINPAD_IDCARD *f_stIdcard );
   OS_STATUS OS_InfoGetPinpadModel( PINPAD_MODEL * );
   
   OS_STATUS OS_InfoSignalQuality( uint8 *f_pbQualityPer );
   OS_STATUS OS_InfoSerialBase( SERIAL_NUM f_szSerial );
   OS_STATUS OS_InfoVersionBase( uint32 * f_puVersion );
#if defined(__cplusplus)
}
#endif

#endif
