/****************************************************************************
 * @file    $Source: /home/cvs/cobra/linux/rootfs/libos/include/os/os_keybd.h,v $
 *
 * @brief   Definitions for Keyboard API
 *
 * @par     Descrition:
 *        
 *             
 *
 * @date          29 Mar 2005 (creation)
 *                $Date: 2011-07-19 12:54:15 $
 *
 * @author        $Author: ledauphin $
 *
 * @version       $Revision: 1.15 $
 *
 * @par    Copyright:
 *         (c) Copyright Verifone 2012 unpublished work.
 *         This computer program includes confidential, proprietary 
 *         information and is a trade secret of Verifone. All use, disclosure
 *         and/or reproduction is prohibited unless authorized in writing.
 *         All rignts reserved.
 *
 ***************************************************************************/

#include "./os/os_types.h"

#ifndef __API_KEYBD_H__
#define __API_KEYBD_H__

#include "os/os_beep.h"

/**
 * @ingroup OSKEYBD 
 *@brief Refer to key values 
 */
typedef enum
{
   MAGIC_KEY_NONE = 0x00,             /*!< No keys */
   MAGIC_KEY_1 = 0x01,                /*!< key code 1 */
   MAGIC_KEY_2 = 0x02,                /*!< key code 2 */
   MAGIC_KEY_3 = 0x03,                /*!< key code 3 */
   MAGIC_KEY_4 = 0x04,                /*!< key code 4 */
   MAGIC_KEY_5 = 0x05,                /*!< key code 5 */
   MAGIC_KEY_6 = 0x06,                /*!< key code 6 */
   MAGIC_KEY_7 = 0x07,                /*!< key code 7 */
   MAGIC_KEY_8 = 0x08,                /*!< key code 8 */
   MAGIC_KEY_9 = 0x09,                /*!< key code 9 */
   MAGIC_KEY_0 = 0x0A,                /*!< key code 0 */
   MAGIC_KEY_STAR = 0x0B,             /*!< key code star */
   MAGIC_KEY_00 = 0x0C,               /*!< key code 00 */
   MAGIC_KEY_CANCEL = 0x0D,           /*!< key code cancel */
   MAGIC_KEY_CLEAR = 0x0E,            /*!< key code clear */
   MAGIC_KEY_ENTER = 0x0F,            /*!< key code enter */
   MAGIC_KEY_ENCRYPT = 0x10,          /*!< key code encrypt */
   MAGIC_KEY_POUND   = 0x1A,          /*!< key code pound */   
   /* 3 most significant bits of KEY_TYPE are used for F1, F2, F3 buttons */
   MAGIC_KEY_F1 = 0x20,         /*!< key code F1 (0x20 bitmask) */
   MAGIC_KEY_F2 = 0x40,         /*!< key code F2 (0x40 bitmask) */
   MAGIC_KEY_F3 = 0x80,         /*!< key code F3 (0x80 bitmask) */
} MAGIC_KEY_TYPE;

/**  
 * @brief Refer to key mask values
 * Used by UiDisplayModalDialog  
 * @ingroup GUIDIALOG 
 */
typedef enum
{
   MAGIC_KEY_MASK_NONE = 0x00,        /*!< No keys */
   MAGIC_KEY_MASK_STAR = 0x01,        /*!< key code star */
   MAGIC_KEY_MASK_00 = 0x02,          /*!< key code 00 */
   MAGIC_KEY_MASK_CANCEL = 0x04,      /*!< key code cancel */
   MAGIC_KEY_MASK_CLEAR = 0x08,       /*!< key code clear */
   MAGIC_KEY_MASK_ENTER = 0x10,       /*!< key code enter */
   MAGIC_KEY_MASK_F1 = 0x20,    /*!< key code F1 (0x20 bitmask) */
   MAGIC_KEY_MASK_F2 = 0x40,    /*!< key code F2 (0x40 bitmask) */
   MAGIC_KEY_MASK_F3 = 0x80,    /*!< key code F2 (0x80 bitmask) */
   MAGIC_KEY_MASK_ENCRYPT = 0x100       /*!< key code encrypt */
} MAGIC_KEY_MASK;

/* keyboard mapping */
#define N_COLUMN                3
#define N_ROW                   5
typedef uint8 KEY_MAP[N_ROW][N_COLUMN];

/** 
 *@brief Keyboard information structure 
 *@ingroup OSKEYBD 
 */
#pragma pack(1)
typedef struct
{
   /** Is the terminal in secure mode ? */
   uint8 bSecureMode;
   /** Keystroke beep frequency (0 = desactivated) */
   uint16 wBeepFrequency;
   /** Number of function keys on the keyboard */
   uint8 bFunctionKeyNb;
   /** Number of normal keys on the keyboard */
   uint8 bNormalKeyNb;
   /** Number of normal key rows */
   uint8 bRowNb;
   /** Number of normal key columns */
   uint8 bColNb;
    /** Keyboard map */
   KEY_MAP stKeyMap;
} KEYBD_INFO_STRUCT;
#pragma pack()

#if defined(__cplusplus)
extern "C"
{
#endif
   OS_STATUS OS_KeybdInit( void );
   OS_STATUS OS_KeybdRestrictedGet( MAGIC_KEY_TYPE * key, uint32 timeout );
   OS_STATUS OS_KeybdWatchKey( MAGIC_KEY_TYPE * key, uint32 timeout );
   OS_STATUS OS_KeybdSet( MAGIC_KEY_TYPE key );
   OS_STATUS OS_KeybdLength( uint16 * size );
   OS_STATUS OS_KeybdFlush( void );
   BOOLEAN OS_KeybdPressed( uint32 timeout );
   OS_STATUS OS_KeybdBklgt( BOOLEAN fState );
   OS_STATUS OS_KeybdBeep( uint16 frequency );   

   OS_STATUS OSF_KeybdGet( int * key, uint32 timeout );

#define OS_BklgtKeyboard  OS_KeybdBklgt
#if defined(__cplusplus)
}
#endif


#endif
