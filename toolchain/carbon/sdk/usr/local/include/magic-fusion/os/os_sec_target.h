/**
 @brief		Secure processor basic types definition

 @par		Descrition:
          	This file declares basic types used for secure processor
			confoming to Cobra coding rules

 @date      11 Avr 2005 (creation)
            $Date: 2011-02-17 10:05:48 $

 @author    $Author: ledauphin $

 @version   $Revision: 1.5 $

 @par       Copyright:
            (c) Copyright Verifone 2012 unpublished work.
            This computer program includes confidential, proprietary 
            information and is a trade secret of Verifone. All use, 
            disclosure and/or reproduction is prohibited unless
            authorized in writing.
            All rignts reserved.
*/

#include "os/os_types.h"

#ifndef _SEC_TARGET_
#define _SEC_TARGET_

#define SEC_MAX_TARGET 2

/** @ingroup TGSE */
typedef enum
{
   TARGET_INTERNAL = 0,    /**< Internal Secure Processor */
   TARGET_EXTERNAL,        /**< PP or SP Secure Processor */
   TARGET_AUTO,            /**< Automatic selection */
   TARGET_NONE            /**< No target selected  */
} TARGET_TYPE;

/** @ingroup TGSE */
typedef enum
{
   EXT_TYPE_NONE = 0,      /**<  No external Secure Processor */
   EXT_TYPE_PP = 1,        /**<  External Secure Processor is a PINPAD */
   EXT_TYPE_SPP = 2,       /**< External Secure Processor is a SMART PINPAD */
   EXT_TYPE_UX = 3,        /**< External Secure Processor is a UX */
} TARGET_EXT_TYPE;

/** @ingroup TGSE */
typedef enum
{
   DEVICE_MAIN,         /**< Device Main (Hw info, status, beep... )*/
   DEVICE_SC,           /**< Smart card device */
   DEVICE_CONS,         /**< Console device (Prompt, display, keyboard)*/
   DEVICE_SEC,    		/**< Secure device (PIN, algo..) */
   DEVICE_CLESS,        /**< Contactless device */
   TARGET_MAX_DEVICES
} DEVICE_TYPE;

/** @ingroup TGSE */
typedef enum
{
   TARGET_MASK_NONE  = 0 << 0,            /** <No device selected */
   TARGET_MASK_MAIN  = 1 << DEVICE_MAIN,  /**<Main secure target ID */
   TARGET_MASK_SC    = 1 << DEVICE_SC,    /**<Smart card present in the target*/
   TARGET_MASK_CONS  = 1 << DEVICE_CONS,  /**<Console present in the target*/
   TARGET_MASK_SEC   = 1 << DEVICE_SEC,   /**<Security functions not persent in the targe*/
   TARGET_MASK_CLESS = 1 << DEVICE_CLESS, /**<Contacless functions not persent in the targe*/
   TARGET_MASK_ALL = 0xFFFFFFFF,
} TARGET_DEVICE_ID;

/** @ingroup TGSE */
typedef uint32 TARGET_MASK_TYPE; /** < Mask type */

#if defined(__cplusplus) && !defined(__DEBUG__)
extern "C"
{
#endif

   OS_STATUS OS_SetActiveTarget( TARGET_TYPE f_enTarget );
   OS_STATUS OS_SetActiveTargetExt( TARGET_TYPE f_enTarget,
                                    TARGET_MASK_TYPE dev_mask );

   OS_STATUS OS_GetActiveTarget( TARGET_TYPE * f_enTarget );
   OS_STATUS OS_GetActiveTargetExt( TARGET_TYPE * f_enTarget,
                                    TARGET_MASK_TYPE * f_dev_mask,
                                    TARGET_EXT_TYPE * f_pptype );


   OS_STATUS OS_GetCurrentDeviceTarget( TARGET_MASK_TYPE f_dev_id,
                                        TARGET_TYPE * f_enTarget );

   OS_STATUS OS_TargetGetSecureMask( TARGET_TYPE f_enTarget,
                                     TARGET_MASK_TYPE * f_dev_mask );
   OS_STATUS OS_TargetGetSecureVersion( TARGET_TYPE f_enTarget,
                                        uint8 * f_szVersion );
   OS_STATUS OS_TargetUseDefaultExternal( TARGET_MASK_TYPE f_dev_id );
   OS_STATUS OS_TargetIsDefaultExternal( TARGET_MASK_TYPE f_dev_id );
   OS_STATUS OS_TargetExternalType( TARGET_EXT_TYPE * f_pptype );
   OS_STATUS OS_TargetResetDefault();
   OS_STATUS OS_TargetIsExternal( TARGET_MASK_TYPE f_dev_id );


#if defined(__cplusplus) && !defined(__DEBUG__)
}
#endif

#endif
