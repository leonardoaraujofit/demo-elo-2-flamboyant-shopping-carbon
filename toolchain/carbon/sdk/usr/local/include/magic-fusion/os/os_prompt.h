/**
 * file     $Source: /home/cvs/cobra/linux/rootfs/libos/include/os/os_cons.h,v $
 *
 * @brief   Definitions for OS information API
 *
 * @par     Descrition:
 *          This file declares basic types used for prompts in OS library. 
 *             
 *
 * @date          01 Jan 2006 (creation)
 *                $Date: 2010-06-18 14:11:00 $
 *
 * @author        $Author: besson $
 *
 * @version       $Revision: 1.36 $
 *
 * @par    Copyright:
 *         (c) Copyright Verifone 2012 unpublished work.
 *         This computer program includes confidential, proprietary 
 *         information and is a trade secret of Verifone. All use, disclosure
 *         and/or reproduction is prohibited unless authorized in writing.
 *         All rignts reserved.
 *
 */
#ifndef __OS_PROMPT_H__
#define __OS_PROMPT_H__

#include "./os/os_types.h"
#include "./os/os_keybd.h"

#define INDEX_MAX_SIZE       	256
#define MAC_SIZE	           	8
#define READ_BUFFER_MAX_SIZE 	512
#define PROMPT_MAX_SIZE      	120
#define UTF8_PROMPT_MAX_SIZE	328
#define DEFAULT_FONT_ID           1

#define LINUX_RESSOURCE_PATH  "./flash/res/"


/** Prompt types 
 @ingroup OSPROMPT */
typedef enum
{
   PR_TYPE_OTHER, 	/**< Type OTHER */
   PR_TYPE_PIN,    	/**< Type PIN */
   PR_TYPE_PIN2,    /* Not supported any more */
   PR_TYPE_PASSWORD /**< Sysmode password */
} PR_TYPES;


/** Prompt options 
 @ingroup OSPROMPT */
typedef enum
{
   PR_OPT_NONE = 0x00000000,    				/**< No option */
   PR_OPT_CLEAR_FIRST = 0x00000001,     		/**< Clear default data entry value when first key is pressed */
   PR_OPT_COR_CLEAR_ALL = 0x00000002,   		/**< Correction key clears all data entry */
   PR_OPT_KEY_BEEP_ON = 0x00000004,     		/**< Force keyboard beep on during prompt */
   PR_OPT_KEEP_LAST_KEY = 0x00000008,   		/**< Use last entered digit as first data entry */
   PR_OPT_AUTO_ENTER = 0x00000010,      		/**< Automatically exit prompt when data entry size equals max size */
   PR_OPT_COR_DISABLE = 0x00000020,     		/**< Disable use of correction key */
   PR_OPT_FORMATED_VAL = 0x00000040,    		/**< Return formatted entry value */
   PR_OPT_HORZ_SCROLL = 0x00000080,     		/**< Use one-line horizontal scroll to display long data entry that do not fit on screen width */
   PR_OPT_CURSOR = 0x00000100,  				/**< Display a blinking cursor in data entry */
   PR_OPT_DEL_FIRST_INPUT_ZEROS = 0x00000200,   /**< Delete the first input zeros */
   PR_OPT_PRIVATIVE = 0x00000400,       		/**< Indicates privative app. PIN entry */
   PR_OPT_TIMEOUT_IN_SEC = 0x00000800,  		/**< Indicates that the timeout are defined in seconds */
} PR_OPTIONS;

#define PR_OPT_FOMATED_VAL PR_OPT_FORMATED_VAL

/** Exit function keys
 @ingroup OSPROMPT */
typedef enum
{
   PR_FKEY_NONE = 0x00000000,   /**< No function key used */
   PR_FKEY_COR = 0x00000001,    /**< Exit on correction key press */
   PR_FKEY_STAR = 0x00000002,   /**< Exit on '*' press */
   PR_FKEY_00 = 0x00000004,     /**< Exit on '00' key press */
   PR_FKEY_UP = 0x00000008,     /**< Exit on up arrow key press */
   PR_FKEY_DOWN = 0x00000010,   /**< Exit on down arrow key press */
   PR_FKEY_SELECT = 0x00000020  /**< Exit on selection key press */
} PR_FKEY;

/** Valid keys in alphanumeric data entry 
 @ingroup OSPROMPT */
typedef enum
{
   PR_KEYSET_ALPHA_UP = 0x00000001,     /**< 'A' to 'Z' */
   PR_KEYSET_ALPHA_LOW = 0x00000002,    /**< 'a' to 'z' */
   PR_KEYSET_NUMERIC = 0x00000004,      /**< '0' to '9' */
   PR_KEYSET_SPACE = 0x00000008,        /**< '' */
   PR_KEYSET_SPECIALS = 0x00000010,     /**< 0x21 to 0x7F Ascii characters */
   PR_KEYSET_EXTENDED = 0x00000020,     /**< 0x80 to 0xFF */
   PR_KEYSET_HEXA = 0x00000040, 		/**< 'A' to 'F' */
   PR_KEYSET_PHONE = 0x00000080,        /**< '*', '#', ',' */
   PR_KEYSET_00 = 0x00000100    		/**< '00' */
} PR_KEYSET;

/** Prompt keymap separator
 @ingroup OSPROMPT */
typedef enum
{
   PR_KEYMAP_ALPHA_LOW,
   PR_KEYMAP_SPECIALS,
   PR_KEYMAP_EXTENDED,
   PR_KEYMAP_END,
} PR_KEYMAP_SEPARATOR;

/* Justification and display mode types */
typedef enum
{
   PR_DISP_MODE_CENTER_EMBEDDED_TEXT = 0x00000001,
   PR_DISP_MODE_ALIGN_LEFT_EMBEDDED_TEXT = 0x00000002,
   PR_DISP_MODE_ALIGN_RIGHT_EMBEDDED_TEXT = 0x00000004,

   PR_DISP_MODE_UNDERLINE_EMBEDDED_TEXT = 0x00000010,
   PR_DISP_MODE_REVERSE_VIDEO_EMBEDDED_TEXT = 0x00000020,

   PR_DISP_MODE_INSERT_NEXT_PARAM_TAG = 0x00000100,
   PR_DISP_MODE_INSERT_DATA_ENTRY_TAG = 0x00000200,

   PR_DISP_MODE_JUSTIFY_BEFORE_DISPLAY = 0x00010000,
   PR_DISP_MODE_DISPLAY_BEFORE_JUSTIFY = 0x00020000,
} PR_DISP_MODE;

typedef enum
{
	ONLINE_PIN_MODE,
	OFFLINE_PIN_MODE,
	UNKNOWN_PIN_MODE
} PIN_MODE; 


/** @ingroup OSPROMPT 
  *@brief Prompt structure 
  */
  
typedef struct
{
   uint8 cType;         /**< Prompt type (PIN or OTHER) */
   uint32 iFontID;      /**< Font id */
   uint32 uOPtions;     /**< Prompt options */
   uint32 uFKeyMask;    /**< Key allowed to exit the prompt */
   uint32 uAlphaKeys;   /**< Kind of input (numeric, alphanumeric,...) */
   uint16 wPromptArrayLength;          /**< Prompt array length (if 0, strlen is used) */
   uint8 abPrompt[PROMPT_MAX_SIZE];    /**< Prompt fomrated string */
} __attribute__ ( ( packed ) ) PROMPT_STRUCT;

/** @ingroup OSPROMPT 
  *@brief UTF8 Prompt structure 
  */
typedef struct
{
   uint8 cType;         /**< Prompt type (PIN or OTHER) */
   uint32 iFontID;      /**< Font id */
   uint32 uOPtions;     /**< Prompt options */
   uint32 uFKeyMask;    /**< Key allowed to exit the prompt */
   uint32 uAlphaKeys;   /**< Kind of input (numeric, alphanumeric,...) */
   uint16 wPromptArrayLength;          /**< Prompt array length (if 0, strlen is used) */
   uint8 *pbPrompt;     /**< Prompt fomrated string */
}
UTF8_PROMPT_STRUCT;             /* 19 bytes min */

typedef struct
{
   uint8 cType;                 /*Prompt type (PIN or OTHER) */
   uint32 iFontID;              /*Font id */
   uint32 uOPtions;             /*Prompt options */
   uint32 uFKeyMask;            /*Key allowed to exit the prompt */
   uint32 uAlphaKeys;           /*Kind of input (numeric, alphanumeric,...) */
   uint16 wPromptArrayLength;   /*Prompt array length (if 0, strlen is used) */

} __attribute__ ( ( packed ) ) PROMPT_HEADER_STRUCT;    /*20 bytes */

typedef struct
{
   PROMPT_STRUCT szPrompt;
   char **szParameters;
   char *szDataEntry;
   uint16 wMaxBuffSize;
   uint16 wFirstTimeout;
   uint16 wInterTimeOut;

} __attribute__ ( ( packed ) ) PROMPT_ARRAY_STRUCT;     /*20 bytes */

typedef enum
{
   FILE_STAT_CODE_OK = 52,
   FILE_STAT_CODE_ERR_OPEN,
   FILE_STAT_CODE_OPEN_PROMPT_FILE_ERROR,
   FILE_STAT_CODE_READ_PROMPT_FILE_ERROR,
   FILE_STAT_CODE_OPEN_CER_FILE_ERROR,
   FILE_STAT_CODE_CREATE_MAC_FILE_ERROR,
   FILE_STAT_CODE_READ_MAC_FILE_ERROR,
   FILE_STAT_CODE_REMOVE_MAC_FILE_ERROR,
   FILE_STAT_CODE_PROMPT_INDEX_ERROR
} FILE_STAT_CODE;

typedef enum
{
   PROMPT_STAT_CODE_OK = 0,
   PROMPT_STAT_CODE_BUSY = 5,   /*a prompt entry is already in progress */
   PROMPT_STAT_CODE_UNKNOWN_FONT_ID = 7,

/*for all prompt functions */
   PROMPT_STAT_CODE_ERROR = 40, /*internal error/parsing error */
   PROMPT_STAT_CODE_ERROR_NONE, /*no error */
   PROMPT_STAT_CODE_PROMPT_NOT_PRESENT,

/*prompt display functions status*/
   PROMPT_STAT_CODE_BAD_PARAM,  /*missing parameters in list */
   PROMPT_STAT_CODE_CANCEL,     /*prompt entry was canceled by user pressing CANCEL key */
   PROMPT_STAT_CODE_TIMEOUT,    /*either first or inter character timeout elapsed */
   PROMPT_STAT_CODE_ALPHANUM_TIMEOUT,
   PROMPT_STAT_CODE_FKEY,       /*a function key was pressed */
   PROMPT_STAT_CODE_ABORTED,    /*the prompt entry was aborted */

/*prompt abort function status*/
   PROMPT_STAT_CODE_NO_PROMPT = 50,     /*no prompt was currently displayed */
   PROMPT_STAT_CODE_GLOBAL_ERROR
} PROMPT_STAT_CODE;


extern uint8 g_aReadBuffer[READ_BUFFER_MAX_SIZE];

#if defined(__cplusplus) && !defined(__DEBUG__)
extern "C"
{
#endif

   int16 updatePromptBinaryFile( const char *f_szFileName, uint8 * f_pbMAC );
										   
	
   OS_STATUS OS_ConsoleShowCustomPromptST( 	const PROMPT_STRUCT * f_szPrompt,
											uint16 f_sPromptIndex,
											const uint8 ** f_szParameters,
											MAGIC_KEY_TYPE * pFuncExitKey,
											char *f_szDataEntry,
											uint16 f_sMaxBuffSize,
											uint16 f_sFirstTimeout,
											uint16 f_sInterTimeout );

   OS_STATUS OS_ConsoleShowCustomUTF8PromptST( const UTF8_PROMPT_STRUCT *
                                               f_szPrompt,
                                               uint16 f_sPromptIndex,
                                               const uint8 ** f_szParameters,
                                               MAGIC_KEY_TYPE * pFuncExitKey,
                                               char *f_szDataEntry,
                                               uint16 f_sMaxBuffSize,
                                               uint16 f_sFirstTimeout,
                                               uint16 f_sInterTimeout );

   OS_STATUS OS_ConsoleShowCustomPrompt( const uint8 *f_szFileName,
                                         uint16 f_sPromptIndex,
                                         const uint8 **f_szParameters,
                                         MAGIC_KEY_TYPE * pFuncExitKey,
                                         char *f_szDataEntry,
                                         uint16 f_sMaxBuffSize,
                                         uint16 f_sFirstTimeout,
                                         uint16 f_sInterTimeout );

   OS_STATUS OS_ConsoleShowSystemPrompt( uint16 f_sPromptIndex,
                                         const uint8 **f_szParameters,
                                         uint8 *f_szDataEntry,
                                         uint16 f_sMaxBuffSize,
                                         uint16 f_sFirstTimeout,
                                         uint16 f_sInterTimeout );

   OS_STATUS OS_ConsoleFormatData( const uint8 *f_szMaskFormat,
                                   const uint8 *f_szInputData,
                                   uint8 *f_szOutputData,
                                   uint16 f_sMaxSize,
                                   uint16 f_sMaxOutputSize );

   OS_STATUS OS_ConsoleDefineKeymap( MAGIC_KEY_TYPE f_key,
                                     const int8 * f_pbKeySequence,
                                     int16 f_iSequenceSize );

   OS_STATUS OS_ConsoleDefineExtendedTable( const int8 * f_pbTable,
											int16 f_iTableSize );
																						
   OS_STATUS OS_ConsoleDefineAndAuthFont( int8 f_bSlot,
                                          const uint8 * f_szFontCertificate,
                                          const uint8 * f_szFontFile );
										  
   OS_STATUS OS_ConsloleDefineSignedFontForDisplay( int8 f_bSlot, 
													const uint8 * f_szFontFile );
										  
   OS_STATUS OS_ConsolePromptAbort( void );
   extern uint8 **KEY_validatePrompt( const char *f_szFileName );

   void OS_ConsolePromptType( PROMPT_STRUCT * s_pstCustomPrompts,
                              uint8 * s_szPathPromptFile );
							  
   void OS_ConsolePromptTypeUTF8( 	UTF8_PROMPT_STRUCT * s_pstCustomPromptsUTF8,
									uint8 * s_szPathPromptFile );
						   
   OS_STATUS OS_ConsoleShowPromptExt( uint16 f_sPromptIndex,
                                      const char **f_szParameters,
                                      MAGIC_KEY_TYPE * pFuncExitKey,
                                      char *f_szDataEntry,
                                      uint16 f_sMaxBuffSize,
                                      uint16 f_sFirstTimeout,
                                      uint16 f_sInterTimeout );

   void OS_ConsolePromptType( PROMPT_STRUCT * s_pstCustomPrompts, uint8 * s_szPathPromptFile );
   
   OS_STATUS OS_ConsoleGetCustomPromptInfos(	const uint8 * f_sFileName,
												uint16 f_wPromptIdentifier,
												PROMPT_HEADER_STRUCT * f_pstPromptHeader,
												uint8 * f_pszPromptString, 
												uint16 * f_pwPromptStructLength );

   /* for instance it is a stub*/
   void OS_ConsolePromptTypeUTF8( UTF8_PROMPT_STRUCT * s_pstCustomPromptsUTF8, uint8 * s_szPathPromptFile );

   int OS_isPromptEngineOn();
   
   void OS_ConsoleSetPinMode( PIN_MODE f_enPinMode );

#if defined(__cplusplus) && !defined(__DEBUG__)
}
#endif
#endif
