/**
 * @file    os_debug.h
 *
 * @brief	Debug functions for COBRA
 *  
 * @par		Descrition:
 *          This file contains debug functions for COBRA
 *
 * @date      	20 Dec 2004 (creation)
 *            	$Date: 2011-02-17 10:05:48 $ (last modification)
 *
 * @author    	$Author: cless $
 *
 * @version   	$Revision: 1.4 $
 *
 * @par       	Copyright:
 *          (c) Copyright Verifone 2012 unpublished work.
 *          This computer program includes confidential, proprietary 
 *          information and is a trade secret of Verifone. All use, disclosure
 *          and/or reproduction is prohibited unless authorized in writing.
 *          All rignts reserved.
 *
 */


#ifndef _OS_DEBUG_H_
#define _OS_DEBUG_H_

#include <stdio.h>

#include "os/os_types.h"

#ifndef NDEBUG
#define LIBOS_TRACE(...) fprintf(stderr, __VA_ARGS__)
#define LIBOS_TRACE_BUFF(a,b,c) os_print_buffer(stderr, a, b, c)
#else
#define LIBOS_TRACE(...)
#define LIBOS_TRACE_BUFF(a,b,c)
#endif

#if defined(__cplusplus) && !defined(__DEBUG__)
extern "C"
{
#endif
/* functions used to debug OS */
   OS_STATUS Disp_Error( OS_STATUS stat );
   OS_STATUS Disp_Error2( OS_STATUS stat, const char *msg );
   OS_STATUS Disp_Error3( OS_STATUS status, const char *msg, int16 no );
	
   void os_print_buffer(FILE* fp, const char* msg, unsigned char* buff, size_t n);


#if defined(__cplusplus) && !defined(__DEBUG__)
}
#endif

#endif
