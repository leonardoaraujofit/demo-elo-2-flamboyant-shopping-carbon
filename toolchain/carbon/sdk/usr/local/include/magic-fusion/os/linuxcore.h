/**
 * file    linuxcore.h
 *
 * @brief	Define types for linux OS
 *
 * @par		Descrition:
 *          This include file is used to define the linux macros,
 *          constants, typedefs, and external functions. 
 *
 * @date      	20 Dec 2004 (creation)
 *            	$Date: 2005-12-20 14:54:55 $ (last modification)
 *
 * @author    	$Author: hameau $
 *
 * @version   	$Revision: 1.2 $
 *
 * @par       	Copyright:
 *          (c) Copyright Verifone 2012 unpublished work.
 *          This computer program includes confidential, proprietary 
 *          information and is a trade secret of Verifone. All use, disclosure
 *          and/or reproduction is prohibited unless authorized in writing.
 *          All rignts reserved.
 *
 */


#ifndef __LINUXCORE_H__
#define __LINUXCORE_H__

#include <pthread.h>
#include <stdlib.h>

#ifndef LINUXTYPES_H_
#include    "linuxtypes.h"      /* linux type and structure definitions */
#endif

/* Priority Levels. 
 * MAX is the highest application task priority 
 * SHELL mus be used only to keep the shell responding
 * LOCK must be above all in order to provide non-preemptible run
 */
typedef enum
{ MIN = 1, LO = 2, NORM = 3, HI = 4, MAX = 5, SHELL = 6, LOCK = 7
} PRIORITIES;

#define COBRA_SCHED_POLICY SCHED_RR     //SCHED_FIFO

#if __cplusplus
extern "C"
{
#endif

#if __cplusplus
}
#endif

#endif
