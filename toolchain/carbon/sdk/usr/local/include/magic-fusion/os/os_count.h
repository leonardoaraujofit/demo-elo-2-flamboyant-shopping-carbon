/**
 * @file    os_count.h
 *
 * @brief   Definitions for OS counters API
 *
 * @par     Descrition:
 *        
 *             
 *
 * @date          29 Mar 2005 (creation)
 *                $Date: 2010-06-18 14:11:17 $
 *
 * @author        $Author: besson $
 *
 * @version       $Revision: 1.4 $
 *
 * @par    Copyright:
 *         (c) Copyright Verifone 2012 unpublished work.
 *         This computer program includes confidential, proprietary 
 *         information and is a trade secret of Verifone. All use, disclosure
 *         and/or reproduction is prohibited unless authorized in writing.
 *         All rignts reserved.
 *
 */

#include "os/os_types.h"

#ifndef __API_OS_COUNT_H__
#define __API_OS_COUNT_H__

/**
 *@brief Counter tag IDs
 *@ingroup OSCOUNT
 */ 
typedef enum
{
   COUNT_TAG_MAG_SWIPES,   /**< */
   COUNT_TAG_MAG_ISO1_ERR,   /**< */
   COUNT_TAG_MAG_ISO2_ERR,   /**< */
   COUNT_TAG_MAG_ISO3_ERR,   /**< */
   COUNT_TAG_CAM_INSERTIONS,   /**< */
   COUNT_TAG_CAM_RESETS,   /**< */
   COUNT_TAG_CAM_RESET_ERR,   /**< */
   COUNT_TAG_SAM_INSERTIONS,   /**< */
   COUNT_TAG_SAM_RESETS,   /**< */
   COUNT_TAG_SAM_RESET_ERR,   /**< */
   COUNT_TAG_KEY_PRESSED,   /**< */
   COUNT_TAG_MESH_INTRUSIONS,   /**< */
   COUNT_TAG_SWITCH_INTRUSION,   /**< */
   COUNT_TAG_CPU_RESET,   /**< */
   COUNT_TAG_SEC_UNLOCK,   /**< */
   COUNT_TAG_SEC_RESET,   /**< */
   COUNT_TAG_RFU1,   /**< */
   COUNT_TAG_RFU2,   /**< */
   COUNT_TAG_RFU3,   /**< */
   COUNT_TAG_RFU4,   /**< */
   COUNT_TAG_RFU5,   /**< */
   COUNT_TAG_RFU6,   /**< */
   COUNT_TAG_RFU7,   /**< */
   COUNT_TAG_RFU8,   /**< */
   COUNT_TAG_RFU9,   /**< */
   COUNT_TAG_RFU10,   /**< */
   COUNT_TAG_MP_RESET,   /**< */
   COUNT_TAG_PRT_CHAR,   /**< */
   COUNT_TAG_USBKEY_DLOAD,   /**< */
   COUNT_TAG_USBKEY_DLOAD_ERR,   /**< */
   COUNT_TAG_USBDEV_DLOAD,   /**< */
   COUNT_TAG_USBDEV_DLOAD_ERR,   /**< */
   COUNT_TAG_FTP_DLOAD,   /**< */
   COUNT_TAG_FTP_DLOAD_ERR,   /**< */
   COUNT_TAG_TMS_DLOAD,   /**< */
   COUNT_TAG_TMS_DLOAD_ERR,   /**< */
   COUNT_TAG_RTC_MODEM_CNX,   /**< */
   COUNT_TAG_RTC_MODEM_OK,   /**< */
   COUNT_TAG_RTC_MODEM_ERR,   /**< */
   /* Keep at the end of the list */
   COUNT_TAG_MAX                 /**< */  
} COUNTER_TAG;

#if defined(__cplusplus)
extern "C"
{
#endif
   OS_STATUS OS_CounterInit( void );
   OS_STATUS OS_CounterGet( uint32 *, uint32 );
   OS_STATUS OS_CounterIncrease( COUNTER_TAG, uint32 );
#if defined(__cplusplus)
}
#endif

#endif
