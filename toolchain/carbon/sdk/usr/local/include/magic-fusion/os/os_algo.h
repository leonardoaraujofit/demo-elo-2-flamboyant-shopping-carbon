/**
 * @file    os_algo.h
 *
 * @brief	Algorithms APIs
 *
 * @par		Descrition:
 *          Definitions for algorithms used in the COBRA platform
 *   
 * @date      	08 Avr 2005 (creation)
 *            	$Date: 2010-06-18 14:10:41 $ (last modification)
 *
 * @author    	$Author: besson $
 *
 * @version   	$Revision: 1.7 $
 *
 * @par       	Copyright:
 *          (c) Copyright Verifone 2012 unpublished work.
 *          This computer program includes confidential, proprietary 
 *          information and is a trade secret of Verifone. All use, disclosure
 *          and/or reproduction is prohibited unless authorized in writing.
 *          All rignts reserved.
 *
 */
#include "./os/os_types.h"
#include "stdlib.h"

#ifndef __OS_ALGO_H__
#define __OS_ALGO_H__

/** @ingroup OSALGO */
typedef enum
{
   DES_MODE_ECB, /**< */
   DES_MODE_CBC  /**< */
} DES_MODE_TYPE;

#if defined(__cplusplus)
extern "C"
{
#endif
   void OS_ShaInit( void );
   void OS_Sha( uint8 * Input, uint32 Length, uint8 * Output );
   void OS_ShaUpdate( const uint8 * input, uint32 length );
   void OS_ShaResult( uint8 * output );

   int16 OS_Rsa( uint8 * key, int16 keyLen, uint8 * data, int16 dataLen,
                 uint8 * result );
   int16 OS_RsaN( uint32 exp, uint8 * key, int16 keyLen, uint8 * data,
                  int16 dataLen, uint8 * result );
   int16 OS_RsaPriv( uint8 * pbModulo, const uint8 * pbInputData,
                     uint8 * pbOutputData, uint8 * pbExponent,
                     int16 ModuloSize, int16 InputSize );

   void OS_DesKey( uint8 * newkey );
   void OS_DesEncrypt( uint8 * cleartext );
   void OS_DesDecrypt( uint8 * ciphertext );

   void OS_3DesInit( DES_MODE_TYPE mode,
                     const uint8 * key1,
                     const uint8 * key2,
                     const uint8 * key3, const uint8 * init_iv );
   void OS_3DesEncrypt( uint8 * cleartext );
   void OS_3DesDecrypt( uint8 * ciphertext );

   void OS_Sftkey( uint8 * newkey );
   void OS_Sftencrypt( uint8 * cleartext );
   void OS_Sftdecrypt( uint8 * ciphertext );
#define  OS_Sftkey      OS_DesKey
#define  OS_Sftencrypt  OS_DesEncrypt
#define  OS_Sftdecrypt  OS_DesDecrypt


   void OS_Crc32Init( uint32 init_value );
   uint32 OS_Crc32update( uint8 c, uint32 Crc32 );
   uint32 OS_Crc32value( void );

   void OS_Crc16Init( uint16 init_value );
   uint16 OS_Crc16update( uint8 c, uint16 Crc16 );
   uint16 OS_Crc16value( void );


/*
   crc32_polynom = 0x4c11db7;
   crc32_crcinit = 0xffffffff;
   crc32_crcxor = 0xffffffff;
   crc32_refin = 1;
   crc32_refout = 1;
*/
#define  OS_Crc32Init_CCITT() OS_Crc32Init_Generic( 0x4c11db7, 0xffffffff, 0xffffffff, 1, 1 )

/*
   crc32_polynom = 0x1021;
   crc32_crcinit = 0xffff;
   crc32_crcxor = 0;
   crc32_refin = 0;
   crc32_refout = 0;
*/
#define  OS_Crc16Init_CCITT() OS_Crc16Init_Generic( 0x1021, 0xffff, 0, 0, 0 )


   void OS_Crc32Init_Generic( uint32 f_polynom, uint32 f_crcinit,
                              uint32 f_crcxor, int f_refin, int f_refout );
   uint32 OS_Crc32Update( uint8 * p, uint32 len );

   void OS_Crc16Init_Generic( uint16 f_polynom, uint16 f_crcinit,
                              uint16 f_crcxor, int f_refin, int f_refout );
   uint16 OS_Crc16Update( uint8 * p, uint32 len );


#if defined(__cplusplus)
}
#endif

#endif
