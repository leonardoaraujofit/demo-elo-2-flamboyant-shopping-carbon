/**
 *
 * @brief   Contactless Library
 *
 * @par      Descrition:
 *             This module provides the description of APIs developed for Contactless.
 *
 * @date         23 April 2012 (creation)
 *
 * @author       $Author: ledauphin $
 *
 * @version      $Revision: 1.0 $
 *
 * @par          Copyright:
 *               (c) Copyright Verifone 2012 unpublished work.
 *               This computer program includes confidential, proprietary
 *               information and is a trade secret of Gemelto. All use,
 *               disclosure and/or reproduction is prohibited unless
 *               authorized in writing.
 *               All rignts reserved.
 *
 */

#ifndef __OS_CLESS_H__
#define __OS_CLESS_H__

#include "agp2conv.h"
#include "os/os_beep.h"

#define LED1_ON 0x01
#define LED2_ON 0x02
#define LED3_ON 0x04
#define LED4_ON 0x08

/** bEvent parameter in CLESS_DisplayMessageLedBuzzer function */
#define CLESS_EVENT_NOW               0x00
#define CLESS_EVENT_COLLISION         0x01
#define CLESS_EVENT_TRANSACTION_END   0x02

/** APDU Maximum buffer size */

/**
@ingroup    OS_CLESS
@brief      APDU Maximum buffer size
*/
#define CLESS_APDU_MAX 255

#define PCD_VERSION_SIZE   32 /**< Size of latest PCD certified clesslib version @ingroup OS_CLESS */
#define CRC_STR_SIZE   16 /**< Size of latest PCD certified clesslib CRC @ingroup OS_CLESS */


/**
@ingroup    OS_CLESS
@brief      Status returned by contactless public functions
*/

typedef enum
{
   CLESS_STAT_OK = 0,             /**< @ingroup OS_CLESS */// 0
   CLESS_STAT_ERROR,              /**< @ingroup OS_CLESS */// 1
   CLESS_STAT_BAD_PARAMETER,      /**< @ingroup OS_CLESS */// 2
   CLESS_STAT_COLLISION,          /**< @ingroup OS_CLESS */// 3
   CLESS_STAT_TIMEOUT,            /**< @ingroup OS_CLESS */// 4
    CLESS_STAT_PROTOCOL_INVALID,   /**< @ingroup OS_CLESS */// 5
    CLESS_STAT_TARGET_ERROR,       /**< @ingroup OS_CLESS */// 6
    CLESS_STAT_COM_ERROR,           /**< @ingroup OS_CLESS */// 7
    CLESS_STAT_INIT_ERROR,           /**< @ingroup OS_CLESS */// 8
    CLESS_STAT_NO_ANSWER,           /**< @ingroup OS_CLESS */// 9
    CLESS_STAT_CRC_ERROR,           /**< @ingroup OS_CLESS */// 10
    CLESS_STAT_PARITY_ERROR,       /**< @ingroup OS_CLESS */// 11
    CLESS_STAT_PROTOCOL_ERROR,     /**< @ingroup OS_CLESS */// 12
    CLESS_STAT_ABORTED,               /**< @ingroup OS_CLESS */// 13
    CLESS_STAT_ACTIVATION_ERROR,   /**< @ingroup OS_CLESS */// 14
    CLESS_STAT_SC_INSERTED,            /**< @ingroup OS_CLESS */// 15
    CLESS_STAT_SIG_END,              /**< @ingroup OS_CLESS */// 16
   CLESS_STAT_CARD_NOT_SUPPORTED  /**< @ingroup OS_CLESS */// 17
} CLESS_STATUS;

/** Indicates the type of the card @ingroup OS_CLESS */
typedef enum
{
   CLESS_CARD_TYPE_NONE,
   CLESS_CARD_TYPE_A,         /** ISO 14443 Type A @ingroup OS_CLESS */
   CLESS_CARD_TYPE_B,         /** ISO 14443 Type B @ingroup OS_CLESS */
   CLESS_CARD_MIFARE,         /** Mifare card @ingroup OS_CLESS */
} CLESS_CARD_TYPE;

/** Indicates the current underlying protocol - CLESS_MODE_EMV_LEVEL_1 is the default one @ingroup OS_CLESS */
typedef enum
{
   CLESS_MODE_EMV           = 0, /** EMV level 1 is used (default value) @ingroup OS_CLESS */
   CLESS_MODE_ISO_14443_4AB=1,   /** ISO 14443 Parts 1, 2, 3 and 4 is used @ingroup OS_CLESS */
   CLESS_MODE_NFC_ISO_DEP=2,     /** NFC Forum - ISO-DEP protocol is used @ingroup OS_CLESS */
}CLESS_SPEC_MODE;

/** Selects the maximum bit rates capabilities @ingroup OS_CLESS */
typedef enum
{
   CLESS_BITRATES_106=0,   /** Bit rates is 106kb/s (default value) @ingroup OS_CLESS */
   CLESS_BITRATES_212=1,   /** Bit rates is negociated with the card (106kb/s or 212kb/s is used) @ingroup OS_CLESS */
   CLESS_BITRATES_424=2    /** Bit rates is negociated with the card (106kb/s or 212kb/s or 424kb/sis used) @ingroup OS_CLESS */
}CLESS_BITRATES;

/** LED state definition @ingroup OS_CLESS */
typedef struct  __attribute__ ( ( __packed__ ) )
{
    uint8   bState;            /** Sate of LEDs 1 to 4 @ingroup OS_CLESS */
    uint32  dwTimeout;         /** Delay in milliseconds during which state will remain @ingroup OS_CLESS */
} CLESS_LED_STATE;

typedef enum
{
   CLESS_OPT_GET_REGISTER = 0,      /** OptionData = addr(1 byte)|value(1 byte) @ingroup OS_CLESS */
   CLESS_OPT_SET_REGISTER = 1,      /** OptionData = addr(1 byte)|value(1 byte) @ingroup OS_CLESS */
   CLESS_OPT_TEST_POLLING_EMV=2,    /** OptionData = NULL @ingroup OS_CLESS */
   CLESS_OPT_TEST_POLLING_LOOP=3,   /** OptionData = NULL @ingroup OS_CLESS */
   CLESS_OPT_TEST_EMV_LEVEL1=4,     /** OptionData = NULL @ingroup OS_CLESS */
   CLESS_OPT_FLUSH_REGISTER=5,      /** OptionData = NULL @ingroup OS_CLESS */
   CLESS_OPT_TEST_RESTRICT_TYPE=6,  /** OptionData[in] = NULL; OptionData = NULL @ingroup OS_CLESS */
   CLESS_OPT_TEST_RESET=7,          /** OptionData[in] = NULL; OptionData = NULL @ingroup OS_CLESS */
   CLESS_OPT_DETECT_CARD=8,
   CLESS_OPT_TEST_RF_OFF=9,           /** OptionData[in] = NULL; OptionData = NULL @ingroup OS_CLESS */
   CLESS_OPT_SET_SPECIFICATION=10,  /** load the current specification @ingroup OS_CLESS */
   CLESS_OPT_SET_BITRATES=11,       /** load the current bit rates @ingroup OS_CLESS */
   CLESS_OPT_LEVEL_1_TEST_CONFIG=12, /** Enable EMV Level 1 test configuration @ingroup OS_CLESS */
   CLESS_OPT_RF_MANUAL_CONFIG=13,   /** We manually configure the registers */
   CLESS_OPT_TIMEOUT_REMOVAL=14,    /** Card removal timeout */
}CLESS_OPTION_TYPE;

/** @ingroup OS_CLESS */
typedef struct
{
   uint8 * msg_line1;
   uint8 * msg_line2;
   uint8 * msg_line3;
   uint8 * msg_line4;
   uint8  Attrib;
}CLESS_DISP_MSG;

/** Contactless level 1 software version @ingroup OS_CLESS */
typedef struct
{
   char pcd_version[PCD_VERSION_SIZE]; /**< @ingroup OS_CLESS */
   char crc[CRC_STR_SIZE]; /**< @ingroup OS_CLESS */
} CLESS_PCD_VERSION_STRUCT;

#if defined(__cplusplus) && !defined(__DEBUG__)
extern "C"
{
#endif

/**
  @ingroup OS_CLESS

  @brief  Initialize contactless device

  @par    Detailed description
          This function is used to load the contactless context. This function SHALL be called before calling any other CLESS API.

  @retval CLESS_STAT_OK       Function successfully completed
  @retval CLESS_STAT_ERROR    Unexpected error occurred

*/
CLESS_STATUS CLESS_Init( void );


/**
  @ingroup OS_CLESS

  @brief  This functions is used to indicates which protocol is used. If not specified EMV Level is used

  @param[in] f_stSpec specification name (CLESS_MODE_EMV_LEVEL_1 or CLESS_MODE_ISO_14443_4AB or CLESS_MODE_NFC_ISO_DEP)

  @par    Detailed description
          This functions is used to indicates which protocol is used. If not specified EMV Level is used

  @retval CLESS_STAT_OK         Function successfully completed
*/
CLESS_STATUS CLESS_SetSpecification( CLESS_SPEC_MODE f_stSpec );

/**
  @ingroup OS_CLESS

  @brief  This functions is used to indicates the highest speed level the PCD is allowed to negociate
          (106kb/s is used if no speed is specified)

  @param[in] CLESS_BITRATES bit rates values (CLESS_BITRATES_106 or CLESS_BITRATES_212 or CLESS_BITRATES_424)

  @par    Detailed description
          This functions is used to indicates which protocol is used. If not specified EMV Level is used
          (106kb/s is used if no speed is specified)

  @retval CLESS_STAT_OK         Function successfully completed
*/
CLESS_STATUS CLESS_SetBitRatesCapabilities( CLESS_BITRATES f_stBitRates );

 /**
  @ingroup OS_CLESS

  @brief  Activate contactless antenna

  @par    Detailed description
          This function is used to activate contactless antenna.

  @retval CLESS_STAT_OK       Function successfully completed
  @retval CLESS_STAT_ERROR    Unexpected error occurred

*/
CLESS_STATUS CLESS_ActivateRF( void );

 /**
  @ingroup OS_CLESS

  @brief  Deactivate contactless antenna and wait for the Cless card removal

  @par    Detailed description
          This function is used to deactivate contactless antenna and waits for card removal.

          The maximum period during which the reader waits for the card removal can be
          configured by setting option CLESS_OPT_TIMEOUT_REMOVAL, default value is set to 60s
          and reinitialized on each CLESS_Init() call.

  @retval CLESS_STAT_OK       Function successfully completed
  @retval CLESS_STAT_TIMEOUT  Card removal timeout (card still in the field)
  @retval CLESS_STAT_ERROR    Unexpected error occurred

*/
CLESS_STATUS CLESS_RemoveCard( void );

/**
  @ingroup OS_CLESS

  @brief  Deactivate contactless antenna

  @par    Detailed description
          This function is used to deactivate contactless antenna.

  @retval CLESS_STAT_OK       Function successfully completed
  @retval CLESS_STAT_ERROR    Unexpected error occurred

*/
CLESS_STATUS CLESS_DeactivateRF( void );

 /**
  @ingroup OS_CLESS

  @brief  Reset contactless antenna (RF OFF/ON)

  @par    Detailed description
          This function is used to Reset contactless antenna.

  @retval CLESS_STAT_OK         Function successfully completed
  @retval CLESS_STAT_ERROR     Unexpected error occurred

*/
CLESS_STATUS CLESS_ResetRF( uint32 f_uDelayWait );

/**
  @ingroup OS_CLESS

  @brief  Remove the RF (RF OFF)

  @par    Detailed description
          This function is used to remove the RF

  @retval CLESS_STAT_OK         Function successfully completed
  @retval CLESS_STAT_ERROR     Unexpected error occurred
*/
CLESS_STATUS CLESS_RemoveRF( void );

 /**
  @ingroup OS_CLESS

  @brief  Check card presence

  @param[out] f_pCardType       the card type detected (CLESS_CARD_TYPE_NONE is returned in case of collision)
  @param[out] f_pbATS 32-bytes buffer used to store the Answer To Select. The first byte indicates the number of the following bytes
  @param[in]  f_dwtimeout       timeout in milliseconds to wait for the card detection.

  @par    Detailed description
          This function is used to check for a card presence in the P5 operating
          volume.

          In case of success, it returns the detected card type and its Answer
          To Select ATS (similar to the ATR for contact cards). The first byte
          of the ATS buffer indicates the number of the following bytes in the
          buffer. In case of collision, CLESS_STAT_COLLISION status code is
          returned.

  @retval CLESS_STAT_OK             Function successfully completed
  @retval CLESS_STAT_BAD_PARAMETER  Bad input parameters
  @retval CLESS_STAT_ABORTED        Card detection aborted
  @retval CLESS_STAT_COLLISION      Two contactless cards detected in the field
  @retval CLESS_STAT_TIMEOUT        Card timeout occurred
  @retval CLESS_STAT_ERROR          Unexpected error occurred

*/
CLESS_STATUS CLESS_WaitCard( CLESS_CARD_TYPE *f_penCardType, uint8* f_pbInfo, uint16 f_wMaxLen, uint16 *f_pwInfoLen, uint32 f_dwtimeout );

 /**
  @ingroup OS_CLESS

  @brief  Abort previous command

  @par    Detailed description
          This function is used to terminate current card detection.

          If this function is not used during WaitCard processing,
          it has no effect and return ok.

  @retval CLESS_STAT_OK  Function successfully completed
*/
CLESS_STATUS CLESS_Abort( void );

 /**
  @ingroup OS_CLESS

  @brief  Send a command to the Contactless smart card.

  @param[in/out] f_pbBuffer contains the application command to send to the card. It also stores the card’s response
  @param[in] f_wCmdLength number of bytes of f_pbBuffer that compose the APDU command
  @param[in] f_wLength is the f_pbBuffer maximum content size
  @param[in] f_pwOutLength points on command response length
  @param[out] f_pSw1Sw2 card response status words SW1-SW2

  @par    Detailed description
          This function is used to exchange APDU commands with a contactless card.

          For the sake of simplicity f_pBuffer APDU format is identical to the
          SC_CommandCard one [Please refer to MagIC³ SDK documentation].

  @retval CLESS_STAT_OK                Function successfully completed
  @retval CLESS_STAT_BAD_PARAMETER     Bad input parameters
  @retval CLESS_STAT_PROTOCOL_ERROR    protocol error occurred during communication with the card
  @retval CLESS_STAT_TIMEOUT           Card timeout occurred
  @retval CLESS_STAT_ERROR             Unexpected error occurred
*/
CLESS_STATUS CLESS_SendCommand( uint8 *f_pBuffer, uint16 f_wCmdLength, uint16 f_wLength, uint16* f_pwOutLength, uint16 *f_wpSw1Sw2 );

/**
  @ingroup OS_CLESS

  @brief  Send the last command of the transaction.

  @param[in/out] f_pbBuffer contains the application command to send to the card. It also stores the card’s response
  @param[in] f_wCmdLength number of bytes of f_pbBuffer that compose the APDU command
  @param[in] f_wLength is the f_pbBuffer maximum content size
  @param[in] f_pwOutLength points on command response length
  @param[out] f_pSw1Sw2 card response status words SW1-SW2

  @par    Detailed description
             This function works like CLESS_SendCommand.

          When the response is received from the card, the animation marked as CLESS_EVENT_TRANSACTION_END
             is played before returning the response to the application.

  @retval CLESS_STAT_OK             Function successfully completed
  @retval CLESS_STAT_BAD_PARAMETER  Bad input parameters
  @retval CLESS_STAT_TIMEOUT        timeout occurred before card detection
  @retval CLESS_STAT_ERROR          Unexpected error occurred

*/
CLESS_STATUS CLESS_SendLastCommand( uint8 *f_pBuffer, uint16 f_wCmdLength, uint16 f_wLength, uint16* f_pwOutLength, uint16 *f_wpSw1Sw2 );

/**
  @ingroup OS_CLESS

  @brief  Preaload a select PPSE command to be presented after a card detection

  @param[in] f_pbBuffer contains the application command to send to the card.
  @param[in] f_wCmdLength number of bytes of f_pbBuffer that compose the APDU command

  @par    Detailed description
          This function is used to preaload a select PPSE command to be presented after a card detection

  @retval CLESS_STAT_OK              Function successfully completed
  @retval CLESS_STAT_BAD_PARAMETER   Bad input parameters
  @retval CLESS_STAT_PROTOCOL_ERROR  protocol error occurred during communication with the card
  @retval CLESS_STAT_TIMEOUT         Card timeout occurred
  @retval CLESS_STAT_ERROR           Unexpected error occurred

*/
CLESS_STATUS CLESS_SelectPPSEPreload(  const uint8 *f_pBuffer, uint16 f_wCmdLength );

/**
  @ingroup OS_CLESS

  @brief  Retrieve the previous select PPSE execution result

  @param[out] f_pbBuffer contains the application command to send to the card. It also stores the card’s response
  @param[in] f_wLength is the f_pbBuffer maximum content size
  @param[in] f_pwOutLength points on command response length
  @param[out] f_pSw1Sw2 card response status words SW1-SW2

  @par    Detailed description
          This function is used to retrieve the previous select PPSE execution result

  @retval CLESS_STAT_OK              Function successfully completed
  @retval CLESS_STAT_BAD_PARAMETER   Bad input parameters
  @retval CLESS_STAT_PROTOCOL_ERROR  protocol error occurred during communication with the card
  @retval CLESS_STAT_TIMEOUT         Card timeout occurred
  @retval CLESS_STAT_ERROR           Unexpected error occurred

*/
CLESS_STATUS CLESS_SelectPPSEGetResult( uint8 *f_pBuffer, uint16 f_wLength, uint16* f_pwOutLength, uint16 *f_wpSw1Sw2 );

/**
  @ingroup OS_CLESS

  @brief  Generate a random buffer

  @param[in] f_wSize number of bytes to generate
  @param[out] f_pbRandom contains f_wSize random bytes when succeeded

  @par    Detailed description
          This function is used to denerate a random buffer

  @retval CLESS_STAT_OK       Function successfully completed
  @retval CLESS_STAT_ERROR    Unexpected error occurred

*/
CLESS_STATUS CLESS_GenerateRandom( uint16 f_wSize, uint8* f_pbRandom ) ;

 /**
   @brief   Play a combination of LED, BEEP and Message.

   @param[in]   CLESS_DISP_MSG  contains the message to display
   @param[in]   f_Attrib        attributes (center and flush) (CENTERED_LINE1,CENTERED_LINE2, CENTERED_LINE3, CENTERED_LINE4, CLEAR_DISPLAY )
   @param[in] f_pstLEDsSates contains list of successive LED state to display.
   @param[in] f_bNbState number of states defined in f_pstLEDsSates
   @param[in] f_pstBuzzerSates contains list of successive Buzzer states.
   @param[in] f_bNbBeeps number of states defined in f_pstBuzzerSates

   @par    Detailled description:
   @verbatim
   This function plays a combination of LED, BEEP and Message.

   @endverbatim

   @retval   CLESS_STATUS
*/
CLESS_STATUS CLESS_DisplayMessageLedBuzzer( uint8 bEvent, CLESS_DISP_MSG *f_pstMessage,
                     CLESS_LED_STATE * f_pstLEDsSates, uint8 f_bNbState,
                     BUZZER_STATE* f_pstBuzzerSates, uint8 f_bNbBeeps );

/**
  @ingroup OS_CLESS

  @brief  Play a LED sequence

  @param[in] f_pstLEDsSates contains list of successive LED state to display.
  @param[in] f_bNbState number of states defined in f_pstLEDsSates

  @par    Detailed description
          This function is used to manage the LED system.

  @retval CLESS_STAT_OK               Function successfully completed
  @retval CLESS_STAT_BAD_PARAMETER    Bad input parameters
  @retval CLESS_STAT_ERROR            Unexpected error occurred

*/
CLESS_STATUS CLESS_PlayLEDSequence( CLESS_LED_STATE *f_pstLEDsSates, uint8 f_bNbState ) ;

/**
  @ingroup OS_CLESS

  @brief  Play a Led and Buzzer sequence

 @param[in] f_pstLEDsSates contains list of successive LED state to display.
 @param[in] f_bNbState number of states defined in f_pstLEDsSates
 @param[in] f_pstBuzzerSates contains list of successive Buzzer states.
 @param[in] f_bNbBeeps number of states defined in f_pstBuzzerSates

  @par    Detailed description
          This function is used to play a combination of LED and Buzzer.

  @retval CLESS_STAT_OK             Function successfully completed
  @retval CLESS_STAT_BAD_PARAMETER  Bad input parameters
  @retval CLESS_STAT_ERROR          Unexpected error occurred

*/
CLESS_STATUS CLESS_PlayLEDBuzzer( CLESS_LED_STATE * f_pstLEDsSates, uint8 f_bNbState, BUZZER_STATE* f_pstBuzzerSates, uint8 f_bNbBeeps  );

/**
  @ingroup OS_CLESS

  @brief  Get/Set an option from the cless device

  @par    Detailed description
          This function is used to get or set an option from the cless device

  @retval CLESS_STAT_OK             Function successfully completed
  @retval CLESS_STAT_BAD_PARAMETER  Bad input parameters
  @retval CLESS_STAT_ERROR          Unexpected error occurred

*/
CLESS_STATUS CLESS_Option( CLESS_OPTION_TYPE f_enOption, uint8 *f_pOptionData, uint16 f_wInputLen, uint16 *f_wOutpLen );

/**
  @ingroup OS_CLESS

 @brief Returns PCD certification version

 @param[out] ifm_version_ptr pointer used to store the IFM version

 @par   Detailed description:
 @verbatim
    This function returns latest PCD certification version number with
    its CRC value.
 @endverbatim

 @retval CLESS_STAT_OK    Function performed correctly
 @retval CLESS_STAT_ERROR wrong parameter value
 */
CLESS_STATUS CLESS_GetVersion( CLESS_PCD_VERSION_STRUCT* f_pstPcdVersion );

#if defined(__cplusplus) && !defined(__DEBUG__)
}
#endif

#endif
