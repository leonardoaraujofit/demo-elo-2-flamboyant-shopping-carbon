/****************************************************************************
 * @file	   $Source: /home/cvs/cobra/linux/rootfs/libos/include/os/slstrmem.h,v $
 *
 * @brief	String and memory processing
 *
 * @par		Descrition:
 *          	
 *
 * @date      	05 juin 2005 (creation)
 *
 * @author    	$Author: gerard $
 *
 * @version   	$Revision: 1.5 $
 *
 * @par       	Copyright:
 *            	(c) Copyright Verifone 2012 unpublished work.
 *            	This computer program includes confidential, proprietary 
 *            	information and is a trade secret of Verifone. All use, 
 *            	disclosure and/or reproduction is prohibited unless
 *            	authorized in writing.
 *            	All rignts reserved.
 *
 ***************************************************************************/

#ifndef _SLSTRMEM_H
#define _SLSTRMEM_H


/* ------------------------ Included files ------------------------------ */
//#include "./am/typedef.h"
#include "./os/os_types.h"
#include "./os/os_disp.h"

 /* ------------------------ Public defines ------------------------------ */


 /* -------------------- Public types and enums -------------------------- */


 /* ----------------- Public constants and variables --------------------- */

 /* ------------------ Public functions declaration ---------------------- */
#ifdef __cplusplus
extern "C"
{
#endif
   uint8 *StrRightPad( const uint8 *, uint16, uint8 *, uint8 );
   uint8 *StrLeftPad( const uint8 *, uint16, uint8 *, uint8 );
   uint8 *StrCentre( const uint8 *, uint16, uint8 * );
   uint8 *StrTrim( const uint8 *, uint8 *, uint8 );

   uint8 *StrAscToEbc( const uint8 *, uint8 * );
   uint8 *StrEbcToAsc( const uint8 *, uint8 * );
   uint16 StrAscToBcd( const uint8 *, uint8 * );
   uint16 StrBcdToAsc( uint16, const uint8 *, uint8 * );

   uint8 *StrFind( const uint8 *, uint8 * );

   BOOLEAN StrIsDigit( const uint8 * );
   BOOLEAN StrIsAlpha( const uint8 * );
   BOOLEAN StrIsAlNum( const uint8 * );

   uint16 MemAscToBcd( uint16, const uint8 *, uint8 * );
   uint16 MemBcdToAsc( uint16, const uint8 *, uint8 * );

   uint8 *MemFind( uint16, const uint8 *, uint16, const uint8 * );

   uint8 *MemDupChr( uint16, const uint8 *, uint16 *, uint8 *, uint8 );
   uint8 *MemRemDupChr( uint16, const uint8 *, uint16 *, uint8 *, uint8 );
   int8 GetNbBytePadded( const uint8 * f_pbText, DISP_TYPE f_wDispPadLen,
                         uint16 * f_pwNbBytePad );
#ifdef __cplusplus
}
#endif


#endif
/*$--- End Of File : slstrmem.h ---*/
