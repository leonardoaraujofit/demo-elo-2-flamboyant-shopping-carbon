/**
 * file    os_ser.h
 *
 * @brief MagIC Serial Port API
 *
 * @par Descrition:
 *      This file contains definitions for MagIC Serial Port
 *
 * @par  Copyright:
 *      Copyright (c) 2012 Verifone Inc.
 *      All Rights Reserved. No part of this software may be reproduced,
 *      transmitted, transcribed, stored in a retrieval system, or
 *      translated into any language or computer language, in any form
 *      or by any means electronic, mechanical, magnetic, optical,
 *      chemical, manual or otherwise, without the prior permission of
 *      VeriFone Inc.
 *
 */


#ifndef __OS_SER_H__
#define __OS_SER_H__

#ifndef __OS_TYPES_H__
#include "./os/os_types.h"
#endif


/*--- Serial port definitions ------------------------------*/

#define COMM_BUFF_SIZE     1024 /* set the size for comm buffers when  */
                                 /* doing buffered communication        */
                                 /* Both the Tx and Rx buffers will be  */

/* Define Signal Mask Values */
#define NO_SIGNAL_TYPE  0x00
#define COMM_Tx_RDY     0x01    /* Transmitter Ready                       */
                             /*  0 - Transmitter is busy                */
                             /*  1 - Transmitter is rdy to accept new int8 */
#define COMM_Rx_RDY     0x02    /* Receiver Ready                          */
                             /*  0 - Receiver is empty                  */
                             /*  1 - Receiver has a character rdy to read */
#define COMM_RTS        0x04    /* Request to Send                         */
                             /*  0 - RTS* force output high             */
                             /*  1 - RTS* force output to its active    */
                             /*      (low) state                        */
                             /*  NOTE: NOT VALID for PORT 3, USART      */
#define COMM_CTS        0x08    /* Clear to Send                           */
                             /*  0 - CTS* input is high                 */
                             /*  1 - CTS* input is low                  */
                             /*  NOTE: NOT VALID for PORT 3, USART      */
#define COMM_DTR        0x10    /* Data Terminal Ready                     */
                             /*  0 - DTR* force output high             */
                             /*  1 - DTR* force output to its active    */
                             /*      (low) state                        */
                             /*  NOTE: NOT VALID for PORT 3, USART      */
#define COMM_DSR        0x20    /* Data Set Ready                          */
                             /*  0 - the DSR* input is high             */
                             /*  1 - the DSR* input is low              */
#define COMM_BREAK      0x40    /* BREAK                                   */
                             /*   Note: for status reads, this bit      */
                             /*         represents the RX state         */
                             /*  0 - normal operation                   */
                             /*  1 - RxD is being forced low            */
#define COMM_DCD        0x80    /* Data Carrier Detect                     */
                             /*  0 - the DCD* input is high             */
                             /*  1 - the DCD* input is low              */


/** 
 * @brief Values for Serial port ID 
 * @ingroup OSSER
 */
typedef enum
{
   /* assign numbers for port's to be sequential   */
   /* so the define can be used in an array index   */
   /*  DO NOT CHANGE THESE VALUES                  */
   PORT_1 = 0,       /**<   internal UART or USB-Serial adapter 1 */
   PORT_2 = 1,       /**<   internal UART or USB-Serial adapter 2 */
   PORT_3 = 2,       /**<   internal UART or USB-Serial adapter 3 */
   PORT_4 = 3,       /**<   internal UART or USB-Serial adapter 4 */
   PORT_5 = 4,       /**<   internal UART or USB-Serial adapter 5 */
   PORT_6 = 5,       /**<   internal UART or USB-Serial adapter 6 */
   PORT_7 = 6,       /**<   internal UART or USB-Serial adapter 7 */
   PORT_8 = 7,       /**<   internal UART or USB-Serial adapter 8 */
   PORT_9 = 8,       /**<   internal UART or USB-Serial adapter 9 */
   PORT_10 = 9,      /**<   internal UART or USB-Serial adapter 10 */
   PORT_COUNT,       /* do NOT assign a value.  It must follow the last valid port number */

} PORT_TYPE;

#define NB_MAX_PORTS PORT_COUNT


/*      parts of the code in the system assume that the above PORT values
**      are in numberic order.  If changes to the above structure are
**      made, do not insert between these limits; and grep on use of boundaries
**      (e.g., look for validity checks on PORT_4 as the upper bound, and
**      change them to PORT_5 or whatever)
*/

/** 
 * @brief Serial port Interface type 
 * (only RS232 is supported on Cobra)
 * @ingroup OSSER 
 */
typedef enum
{
   UNUSED_INTERFACE = 1, /**<   not supported   */
   RS232_INTERFACE,  /**<  only value supported: RS232 port */
   RS485_INTERFACE,  /**<   not supported   */
   TAILGATE_INTERFACE  /**<   not supported   */
} INTERFACE_TYPE;

/*      only the following baud rates are valid selections  */

/** 
 * @brief Serial port Baud rate 
 * @ingroup OSSER 
 */
typedef enum
{                               /* DO NOT CHANGE THESE VALUES    ******* */
    /**  */
    BPS_50 = 1,
    /**  */
    BPS_75 = 2,
    /**  */
    BPS_110 = 3,
    /**  */
    BPS_300 = 4,
    /**  */
    BPS_600 = 5,
    /**  */
    BPS_1200 = 6,
    /**  */
    BPS_2400 = 7,
    /**  */
    BPS_4800 = 8,
    /**  */
    BPS_9600 = 9,
    /**  */
    BPS_19200 = 10,
    /**  */
    BPS_38400 = 11,
    /**  */
    BPS_93750 = 12,
    /**  */
    BPS_187500 = 13,
    /**  */
    BPS_57600 = 14,
    /**  */
    BPS_115200 = 15,
    /**  */
    BPS_EXT_X1 = 32,
    /**  */
    BPS_EXT_X16 = 33,
    /**  */
    BPS_EXT_X64 = 34
} BAUD_TYPE;


/** 
 * @brief Serial port Parity type.
 * Parity controls the parity of the communication channel.
 * @ingroup OSSER 
 */
typedef enum
{                               /*   DO NOT CHANGE THESE VALUES   ******* */
    NO_PARITY = 0,   /**< */
    ODD_PARITY = 1,  /**< */
    EVEN_PARITY = 2   /**< */
} PARITY_TYPE;

/** 
 * @brief Serial port Stop bits.
 * It specifies the transmitted stop bits.
 * @ingroup OSSER 
 */
typedef enum
{
    ONE_STOP_BIT,               /**< 1 stop bit */
    ONE_POINT_FIVE_STOP_BITS,   /**< 1.5 stop bit. NOT SUPPORTED by serial ports. */
    TWO_STOP_BITS,              /**< 2 stop bits. */
} STOP_BITS_TYPE;


/** @brief Serial port Character length (in bits) 
 *@ingroup OSSER*/
typedef enum
{                               /*   DO NOT CHANGE THESE VALUES   ********* */
    FIVE_BITS = 0,  /**< char length = 5bits */
    SIX_BITS = 1,  /**< char length = 6bits */
    SEVEN_BITS = 2,  /**< char length = 7bits */
    EIGHT_BITS = 3  /**< char length = 8bits */
} CHAR_LEN_TYPE;



/**   
 * No more used
 * @ingroup OSSER   
 */
typedef enum
{
    NO_READ_ERR = 0,
    OVERRUN_ERR = 1,
    PARITY_ERR = 2,
    FRAMING_ERR = 4,
} READ_ERR_TYPE;


/** @brief Serial port initialization parameters 
 *@ingroup OSSER*/
typedef struct
{
    /** Serial port Interface type (only one supported) */
    INTERFACE_TYPE interface;
    /** Serial port Baud rate */
    BAUD_TYPE baud_rate;
    /** Serial port Parity */
    PARITY_TYPE parity;
    /** Serial port Stop bits */
    STOP_BITS_TYPE stop_bits;
    /** Serial port Length of characters */
    CHAR_LEN_TYPE char_len;
    /** No more used */
    BOOLEAN tx_buffer_enable;
    /** No more used */
    BOOLEAN rx_buffer_enable;
} COMM_PARAM_STRUCT;



/** 
 *  When selecting between the Tx and Rx buffers, 
 *  for example when flushing the buffers, 
 *  use the following values:
 *  @ingroup OSSER
*/
typedef enum
{
    READ_BUFFER = 1,             /**< Rx buffer                              */
    WRITE_BUFFER,                /**< Tx buffer                              */
    READ_AND_WRITE_BUFFERS,      /**< both Rx and Tx buffers                  */
} BUFFER_TYPE;

/**
 *
 * Flow control type selected in OS_PortSetFlowControl
 * @ingroup OSSER
 *  
 */
typedef enum
{
    PORT_FLOW_CONTROL_NONE,  /**< No flow control */
    PORT_FLOW_CONTROL_RTSCTS /**< Hardware RTS/CTS flow control */
} PORT_FLOW_CONTROL_TYPE;

/** @brief Serial hardware type 
 *@ingroup OSSER
 */
typedef enum
{
    SERIAL_DEVICE_UNKNOWN = 0,   /**< Unknwon device hardware            */
    SERIAL_DEVICE_UART,          /**< Internal UART                      */
    SERIAL_DEVICE_USB,           /**< USB-Serial adapter device          */
    SERIAL_DEVICE_USB_DEVICE,    /**< Internal USB Device port used as serial interface*/
    SERIAL_DEVICE_USB_CDCACM,    /**< USB CDC ACM interface        */  
    SERIAL_DEVICE_BT_RFCOMM,     /**< Bluetooth serial wireless port (rfcomm) */
} SERIAL_DEVICE_HDW;

/** @brief System information on serial/modem device 
 *@ingroup OSSER
 */
typedef struct
{
    PORT_TYPE port;         /**<  port number       */
    char AliasName[15];     /**<  System alias name */
    char DeviceName[15];    /**<  device node name  */
    SERIAL_DEVICE_HDW hdw;  /**<  hardware type     */
} SERIAL_DEVICE_INFO;


#if defined(__cplusplus) && !defined(__DEBUG__)
extern "C"
{
#endif

OS_STATUS OS_PortInit( PORT_TYPE port, const COMM_PARAM_STRUCT *params );
OS_STATUS OS_PortConfig( PORT_TYPE port, const COMM_PARAM_STRUCT *params );
OS_STATUS OS_PortInfo( PORT_TYPE port, COMM_PARAM_STRUCT *params );
OS_STATUS OS_PortLength( PORT_TYPE port, BUFFER_TYPE buffer_type, uint16 *buffer_size );
OS_STATUS OS_PortWriteString( PORT_TYPE port, const uint8 *data, uint16 length );
OS_STATUS OS_PortReadString( PORT_TYPE port, uint16 toRead, uint8 *buffer, uint16 *charsInBuffer, uint32 timeout, uint32 timeoutInter );
OS_STATUS OS_PortSendChar( PORT_TYPE port, uint8 data );
OS_STATUS OS_PortReadChar( PORT_TYPE port, uint8 *out, READ_ERR_TYPE *read_err );
OS_STATUS OS_PortReadSignals( PORT_TYPE port, uint8 *signal, READ_ERR_TYPE *read_err );
OS_STATUS OS_PortSendSignals( PORT_TYPE port, uint8 signals );
OS_STATUS OS_PortFlush( PORT_TYPE port, BUFFER_TYPE buffer_type );
OS_STATUS OS_PortGetHandle( PORT_TYPE port, int *handle );
OS_STATUS OS_PortSetFlowControl( PORT_TYPE port, PORT_FLOW_CONTROL_TYPE flow_control );
OS_STATUS OS_PortTxDrain( PORT_TYPE port );
OS_STATUS OS_getSerialDeviceInfo( PORT_TYPE port, SERIAL_DEVICE_INFO *deviceInfo );
OS_STATUS OS_PortClose( PORT_TYPE port );

#if defined(__cplusplus) && !defined(__DEBUG__)
}
#endif

#endif
