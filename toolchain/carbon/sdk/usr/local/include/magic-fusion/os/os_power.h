/**
 * @file    os_power.h
 *
 * @brief	Definitions for Power Management API
 *
 * @par		Descrition:
 *          Definitions for Power Management API
 *   
 * @date      	08 Avr 2005 (creation)
 *            	$Date: 2010-06-18 14:12:57 $ (last modification)
 *
 * @author    	$Author: besson $
 *
 * @version   	$Revision: 1.10 $
 *
 * @par       	Copyright:
 *          (c) Copyright Verifone 2012 unpublished work.
 *          This computer program includes confidential, proprietary 
 *          information and is a trade secret of Verifone. All use, disclosure
 *          and/or reproduction is prohibited unless authorized in writing.
 *          All rignts reserved.
 *
 */
#include "./os/os_types.h"

#ifndef __API_POWER_H__
#define __API_POWER_H__

/** Battery status 
 *@ingroup OSPOW */
typedef enum
{
   BATTERY_LOW,     /**< Battery is low (below 15%) */
   BATTERY_MEDIUM,  /**< Battery medium (between 15 and 85%) */
   BATTERY_HIGH     /**< Battery is full (above 85%) */
} BATTERY_STATUS;

/** Battery flow
 @ingroup OSPOW */
typedef enum
{
   BAT_FLOW_EMPTY,       /**< Battery is empty */
   BAT_FLOW_DISCHARGING, /**< Battery is discharging */
   BAT_FLOW_CHARGING,    /**< Battery is charging */
   BAT_FLOW_FULL,        /**< Battery is full */
   BAT_ERROR,            /**< Unknown battery pack */
} BATTERY_FLOW;

/** Battery information 
 @ingroup OSPOW */
typedef struct
{
   uint8 szSN[8+1];        /**< Battery pack serial number */
   uint8 szManufDate[6+1]; /**< Battery pack manufacturing date (DDMMYY) */
   uint16 wCapacity;       /**< Battery pack design capacity in mAh */
   uint16 wCycles;         /**< Number of discharge cycles */
} POWER_INFO;

#if defined(__cplusplus)
extern "C"
{
#endif
   OS_STATUS OS_PowerOff( void );
   OS_STATUS OS_PowerBatteryCharge( uint8 * charge_percentage,
                                    BOOLEAN * charging );                                       
   BOOLEAN   OS_PowerIsAvailable(void);
   OS_STATUS OS_PowerBatteryFlow( BATTERY_FLOW * fBatteryFlow );   
   OS_STATUS OS_PowerSleep( void );  
   BOOLEAN   OS_IsOnBase( void );
#if 0   
   OS_STATUS OS_PowerInfo(POWER_INFO *);
   OS_STATUS OS_WaitForBase( void );
   OS_STATUS OS_PowerSleep( void );
   OS_STATUS OS_PowerBatteryLevel( BATTERY_STATUS * fBatteryLevel );
   OS_STATUS OS_PowerInit( BOOLEAN reset );
   OS_STATUS OS_PowerGetVoltage( uint16 * Voltage );
   OS_STATUS OS_PowerGetTemperature( int16 * Temperature );
   OS_STATUS OS_PowerGetTime( uint16 * run_time, uint16 * time_to_full );
   OS_STATUS OS_IsOnBaseDisplayInfo( uint8 * msg, uint16 msglen,
                                     uint16 timeout );
#endif
#if defined(__cplusplus)
}
#endif

#endif
