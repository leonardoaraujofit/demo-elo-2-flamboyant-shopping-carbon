/**
 * @file    os_mem.h
 *
 * @brief   Memory management prototypes
 *
 * @par     Descrition:
 *          Memory management prototypes
 *             
 *
 * @date          21 Mar 2005 (creation)
 *                $Date: 2010-06-18 14:12:43 $
 *
 * @author        $Author: besson $
 *
 * @version       $Revision: 1.9 $
 *
 * @par    Copyright:
 *         (c) Copyright Verifone 2012 unpublished work.
 *         This computer program includes confidential, proprietary 
 *         information and is a trade secret of Verifone. All use, disclosure
 *         and/or reproduction is prohibited unless authorized in writing.
 *         All rignts reserved.
 *
 */
#include "./os/os_types.h"

#ifndef __API_OS_MEM_H__
#define __API_OS_MEM_H__

#define MTD_PROC_NAME "/proc/mtd"
#define IOMEM_PROC_NAME "/proc/iomem"
#define MEMINFO_PROC_NAME "/proc/meminfo"
#define JFFS2_MOUNT_PATH "/opt/local"

#define MEMINFO_TOTAL_NEEDLE      "MemTotal:"
#define MEMINFO_FREE_NEEDLE      "MemFree:"
#define MTD0_NEEDLE "mtd0:"
#define IOMEM_NEEDLE "System RAM"

/** Memory information structure 
 *@ingroup OSMEM */
typedef struct
{
   uint32 ram_size;  /**< */
   uint32 ram_free;  /**< */
   uint32 flash_size;  /**< */
   uint32 flash_free;  /**< */
} MEM_INFO_STRUCT;

#if defined(__cplusplus) && !defined(__DEBUG__)
extern "C"
{
#endif
 /*--- Memory Allocation Functions -----*/
   OS_STATUS OS_MemCalloc( uint16 number, uint16 size, void **mem_ptr );
   OS_STATUS OS_MemFree( void *mem_ptr );
   OS_STATUS OS_MemMalloc( uint16 size, void **mem_ptr );
   OS_STATUS OS_MemRealloc( void *mem_ptr, uint16 size, void **new_mem_ptr );
   OS_STATUS OS_MemCheck( uint32 * size );
   OS_STATUS OS_MemInfo( MEM_INFO_STRUCT * mem_info );
#if defined(__cplusplus) && !defined(__DEBUG__)
}
#endif
#endif
