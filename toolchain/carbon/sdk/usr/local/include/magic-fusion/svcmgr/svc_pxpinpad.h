#ifndef SVC_PX_PINPAD_H
#define SVC_PX_PINPAD_H

#include "svcmgrSvcDef.h"

/*SVC_SERVICE:pxpinpad*/

/**
 * @name Type of Card reset requested
*/
/** \{ */
#define PP_SC_WARM_RESET   0
#define PP_SC_COLD_RESET   1
/** \} */

/**
 * @name Standard supported
*/
/** \{ */
#define ISO 0  /**< ISO/IEC 7816 standard */
#define EMV 1  /**< EMV'96 standard */
/** \} */

/**
 * @name VCC values
*/
/** \{ */
#define VCC_18 0x01
#define VCC_03 0x02
#define VCC_05 0x03
/** \} */

/**
 * @name Parameter values passed to LEDs APIs
*/
/** \{ */
#define PP_LED1_ON 1
#define PP_LED2_ON 2
#define PP_LED3_ON 4
#define PP_LED4_ON 8
/** \} */

/**
 * @name Event parameter values in CLESS_DisplayMessageLedBuzzer function
*/
/** \{ */
#define PP_CLESS_EVENT_NOW              0
#define PP_CLESS_EVENT_COLLISION        1
#define PP_CLESS_EVENT_TRANSACTION_END  2
/** \} */

/**
 * @name Parameters parameter values in CLESS_CARD_TYPE function
*/
/** \{ */
#define PP_CLESS_CARD_TYPE_NONE 0
#define PP_CLESS_CARD_TYPE_A    1        /** ISO 14443 Type A */
#define PP_CLESS_CARD_TYPE_B    2        /** ISO 14443 Type B */
#define PP_CLESS_CARD_MIFARE    3        /** Mifare card */
/** \} */

/**
 * @name Indicates the current underlying protocol - CLESS_MODE_EMV_LEVEL_1 is the default one
*/
/** \{ */
#define PP_CLESS_MODE_EMV            0  /** EMV level 1 is used (default value) */
#define PP_CLESS_MODE_ISO_14443_4AB  1  /** ISO 14443 Parts 1, 2, 3 and 4 is used */
#define PP_CLESS_MODE_NFC_ISO_DEP    2  /** NFC Forum - ISO-DEP protocol is used */
/** \} */

/**
 * @name Indicates the maximum bit rates capabilities
*/
/** \{ */
#define PP_CLESS_BITRATES_106  0    /** Bit rates is 106kb/s (default value) */
#define PP_CLESS_BITRATES_212  1    /** Bit rates is negociated with the card (106kb/s or 212kb/s is used)*/
#define PP_CLESS_BITRATES_424  2    /** Bit rates is negociated with the card (106kb/s or 212kb/s or 424kb/sis used)*/
/** \} */

/**
 * @name Indicates keypad codes 
*/
/** \{ */
#define PP_KEY_NONE    0x00    /*!< No keys */
#define PP_KEY_1       0x01    /*!< key code 1 */
#define PP_KEY_2       0x02    /*!< key code 2 */
#define PP_KEY_3       0x03    /*!< key code 3 */
#define PP_KEY_4       0x04    /*!< key code 4 */
#define PP_KEY_5       0x05    /*!< key code 5 */
#define PP_KEY_6       0x06    /*!< key code 6 */
#define PP_KEY_7       0x07    /*!< key code 7 */
#define PP_KEY_8       0x08    /*!< key code 8 */
#define PP_KEY_9       0x09    /*!< key code 9 */
#define PP_KEY_0       0x0A    /*!< key code 0 */
#define PP_KEY_STAR    0x0B    /*!< key code star */
#define PP_KEY_00      0x0C    /*!< key code 00 */
#define PP_KEY_CANCEL  0x0D    /*!< key code cancel */
#define PP_KEY_CLEAR   0x0E    /*!< key code clear */
#define PP_KEY_ENTER   0x0F    /*!< key code enter */
#define PP_KEY_ENCRYPT 0x10    /*!< key code encrypt */
#define PP_KEY_F1      0x20    /*!< key code F1 (0x20 bitmask) */
#define PP_KEY_F2      0x40    /*!< key code F2 (0x40 bitmask) */
#define PP_KEY_F3      0x80    /*!< key code F3 (0x80 bitmask) */
/** \} */

/**
 * @name Indicates the maximum bit rates capabilities
*/
/** \{ */
#define PP_EMV_PLAINTEXT    1   /** EMV plaintext PIN verification mode */
#define PP_EMV_CIPHERED     2   /** EMV ciphered PIN verification mode  */
#define PP_MONEO_PLAINTEXT  3   /** MONEO plaintext PIN verification mode */
/** \} */

/*SVC_STRUCT*/
/**
 * P-Series ID card
 */
struct ppIdCard
{
   char pn[5];    /**< pxpinpad part number */
   char sn[11];   /**< pxpinpad serial number */
   char pci[4];   /**< pxpinpad PCI ID */
   char model[3]; /**< pxpinpad model (P3D, P5D) */
};

/*SVC_STRUCT*/
/**
 * P-Series information structure
 */
struct ppVersion
{
   char id_code[50];     /** Software package name */
   char family[10];      /** PCI or equivalent certficiation ID */
   char version[12];     /** version */
};

/*SVC_STRUCT*/
/**
 * P-Series information structure
 */
struct ppVersionExtended
{
   char id_code[50];     /** Software package name */
   char family[10];      /** PCI or equivalent certficiation ID */
   char version[12];     /** version */
   unsigned int crc;     /** Checksum (CRC16) */
   unsigned int date;    /** date */
};

/*SVC_STRUCT*/
/**
 * Define a Beep
 */
struct ppBeepState
{
    int frequency; /**< Beep frequency */
    int timeout; /**<  Beep duration */
};

/*SVC_STRUCT*/
/**
 * Define a beep sequence
 */
struct ppBeepSequence
{
    struct ppBeepState *beeps; /**< list of successive beeps */
    int beeps_count; /**< number of beeps */
};

/*SVC_STRUCT*/
/**
 * Define a LED animation
 */
struct ppDispInfo
{
   int font_id;         /**<  Current font ID */
   unsigned char max_lines;       /**< Maximum number of lines (with current font) */
   unsigned char max_columns;     /**< Maximum number of columns (with current font) */
   unsigned char char_width;      /**< Current font character width in pixels */
   unsigned char char_height;     /**< Current font character height in pixels */
   unsigned char first_char;      /**< ASCII offset of first character in current font */
   unsigned char nb_char;         /**< Number of ASCII character in current font */
   unsigned char disp_type;       /**< Display type (see DISPLAY_TYPE) */
   unsigned char screen_width;     /**< Width of the screen in pixels - only for graphic display */
   unsigned char screen_height;   /**< Heigth of the screen in pixels  - only for graphic display */
   unsigned char current_contrast;   /**< Current contrast setting of the display */
   unsigned char min_contrast;   /**< Minimum contrast setting for the display */
   unsigned char max_contrast;   /**< Maximum contrast setting for the display */
   unsigned char current_backlight;   /**< Current backlight setting of the display */
   unsigned char min_backlight;   /**< Minimum backlight setting for the display */
   unsigned char max_backlight;   /**< Maximum backlight setting for the display */
   int current_backlight_delay;   /**< Current backlight timer in ms */
   unsigned char cursor_col;      /**< Current horizontal position of the cursor */
   unsigned char cursor_row;      /**< Current vertical position of the cursor */
};

/*SVC_STRUCT*/
/**
 * Keyboard information structure
 */
struct ppKeybdStruct
{
   unsigned char secure_mode;     /** Is the keyboard in secure mode ? */
   int beep_frequency;            /** Keystroke beep frequency (0 = desactivated) */
   unsigned char nb_function_key; /** Number of function keys on the keyboard */
   unsigned char nb_normal_key;   /** Number of normal keys on the keyboard */
   unsigned char nb_row;          /** Number of normal key rows */
   unsigned char nb_col;          /** Number of normal key columns */
   unsigned char key_map[3*5];    /** Keyboard map */
};

/*SVC_STRUCT*/
/**
 * Smart card information
 */
struct ppScInfo
{
   unsigned char proto;
   unsigned char reset;
   unsigned char option;
};

/*SVC_STRUCT*/
/**
 * Define card command
 */
struct ppCardCommand
{
    void* data;
    int data_count;
};

/*SVC_STRUCT*/
/**
 * Define card respponse
 */
struct ppCardResponse
{
    unsigned char sw1Sw2[2];
    void* data;
    int data_count;
};

/*SVC_STRUCT*/
/**
 * Define a configuration of pxpinpad LEDs
 */
struct ppLedState
{
    unsigned char state; /**<  State of LEDs 1 to 4 */
    int timeout; /**<  Delay in milliseconds during which state will remain */
};

/*SVC_STRUCT*/
/**
 * Define a LED animation
 */
struct ppLedAnim
{
    struct ppLedState *led_states; /**< list of successive led states */
    int led_states_count; /**< number of led states */
};

/*SVC_STRUCT*/
/**
 * Define a message to display on PP screen
 */
struct ppDispMsg
{
    char* msg_line1;
    char* msg_line2;
    char* msg_line3;
    char* msg_line4;
    unsigned char attrib;
};

/*SVC_STRUCT*/
/**
 * Define Smart card reader info
 */
struct ppScVersion
{
    char ifm_version[32];
};

/*SVC_STRUCT*/
/**
 * Define Contacless reader info
 */
struct ppPcdVersion
{
    char pcd_version[32];
    char crc[16];
};

/*SVC_STRUCT*/
/**
 * Define card information
 */
struct ppCardInfo
{
    int type;
    void* info;
    int info_count;
};

/*SVC_STRUCT*/
/**
 * Define a buffer
 */
struct ppByteBuffer
{
    void* data;
    int data_count;
};

/*SVC_STRUCT*/
/**
 * Define a DUKPT PIN structure
 */
struct ppDukptPIN
{
    unsigned char smid[10];
    unsigned char pin_block[8];
};

/*SVC_STRUCT*/
/**
 * Define a Master session PIN structure
 */
struct ppMsPIN
{
    unsigned char pin_block[8];
};

/*SVC_STRUCT*/
/**
 * Define a Master session PIN structure
 */
struct ppPromptParam
{
    void* param;
    int param_count;
};

/*SVC_STRUCT*/
/**
 * Define a prompt result
 */
struct ppPromptResult
{
    int exit_key;
    struct ppByteBuffer str;
};

/*SVC_STRUCT*/
/**
 * Define Bank key structure
 */
struct ppKeySlot
{
   unsigned char owner;
   unsigned int id;
};

/*SVC_STRUCT*/
/**
 * Define Bank key structure
 */
struct ppBankKey
{
   unsigned char format;	// 1: DES_ECB, 2:DES_CBC, 3:TR-31
   unsigned char type;		// TMK, MK, DUKPT 	
   unsigned char usage;		// PIN encryption, MAC...
   unsigned char* option;	// bKeyType dependant
   int option_count;		   // bKeyType dependant
   unsigned char* key;
   int key_count;	
   unsigned char* mac;
   int mac_count;
};

/*SVC_STRUCT*/
/**
 * Security status structure
 */
struct ppSecStatus
{
   unsigned int sp_status;          /** SP status */
   unsigned int uksr_saved;         /** Saved UKSR */
   unsigned int irq_saved;          /** Saved IRQ */
   unsigned int uksr_curr;          /** Current UKSR */
   unsigned int irq_curr;           /** Current IRQ */
   unsigned int opening_cnt;        /** Opening counter */
   unsigned char is_attacked;       /** Is tampered? (Yes:1, No:0) */
   unsigned char is_personalized;   /** Is personalized? (Yes:1, No:0) */
   unsigned char is_authenticated;  /** Is authenticated? (Yes:1, No:0) */
};

/*SVC_STRUCT*/
/**
 * EMV information
 */
struct ppEmvVersion
{
   unsigned int version;     /** EMV version */
   unsigned int checksum;     /** EMV checksum */
};

/************************************************************************
                            SERVICE API
 **********************************************************************/

/**
 * Obtain the version of the net service
 *
 * @return version struct defined in svcmgrSvcDef.h
 */
/*SVC_PROTOTYPE*/ struct version pxpinpad_getVersion( void );

/**
 * Reset the pinpad
 *
 * @return 0 upon successful request, or -1 if unable to complete request
 */
/*SVC_PROTOTYPE*/ int pxpinpad_reset(void);

/**
 * Refresh the pinpad watchdog
 *
 * @return 0 upon successful request, or -1 if unable to complete request
 */
/*SVC_PROTOTYPE*/ int pxpinpad_refresh(void);

/**
 * Reset the authentication status.
 *
 * @return 0 upon successful request, or -1 if unable to complete request
 */
/*SVC_PROTOTYPE*/ int pxpinpad_resetPairing( );

/**
 * Get authentication status
 *
 * @param[in]  renew   Force key renewal ( 1:yes, 0:no )
 *
 * @return 0 authentication successful, 1 when authentication is not required and 2 when authentication failed
 */
/*SVC_PROTOTYPE*/ int pxpinpad_checkPairing( int renew );

/************************************************************************
                             INFO APIs
 ***********************************************************************/

/** Read pinpad ID card.
 * @return struct ppIdCard - contains ID card information. Check Errno for errors
 */
/*SVC_PROTOTYPE*/ struct ppIdCard pxpinpad_info_getIdCard( void );

/** Write pinpad ID card.
 *
 * @param[in]  idcard  Id card structure
 *
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_info_setIdCard( struct ppIdCard idcard);

/** Get the list of pinpad devices
 * @return bit mask defining device presence. Check Errno for errors
 */
/*SVC_PROTOTYPE*/ int pxpinpad_info_getDeviceList( );

/**Get the software version of pinpad
 * @return struct ppDispInfo - contains software version of pinpad. Check Errno for errors
 */
/*SVC_PROTOTYPE*/ struct ppVersion pxpinpad_info_getSecureVersion( );

/** Get the software version (extended) of pinpad
 * @return struct ppDispInfoExtended - contains software version of pinpad. Check Errno for errors
 */
/*SVC_PROTOTYPE*/ struct ppVersionExtended pxpinpad_info_getSecureVersionExtended( );

/************************************************************************
                            BUZZER APIs
 ***********************************************************************/

/** Emits a beep
 *
 * @param[in]  frequency    frequency beep in Hz
 * @param[in]  timeout      beep duration in milliseconds
 *
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_buzzer_setBeep( unsigned int frequency, unsigned int timeout );

/** Play a sequence of beeps.
 *
 * @param[in]  beep_seq  structure defining successive beeps.
 *
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_buzzer_setBeepSequence( struct ppBeepSequence beep_seq );

/************************************************************************
                            DISPLAY APIs
 ***********************************************************************/

/** Initialize display device. Shall be called once (at least).
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_disp_init( );

/** Get information on display
 * @return struct ppDispInfo - contain information on display device. Check Errno for errors
 */
/*SVC_PROTOTYPE*/ struct ppDispInfo pxpinpad_disp_getInfo(  );

/** Clear the display screen
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_disp_flush( );

/** Display a bitmap
 *
 * @param[in] line Line number from the top starting to 0 (a line is 8 pixel height).
 * @param[in] col Column number from the left starting to 0.
 * @param[in] width Number of pixels across
 * @param[in] bitmap Bitmap to display
 *
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_disp_setBitmap( int line, int col, int width, struct ppByteBuffer bitmap);

/** Get a bitmap of screen area. 
 *
 * @param[in] line Line number from the top starting to 0.
 * @param[in] col Column number from the left starting to 0.
 * @param[in] width Number of pixels across
 * @param[in] len Maximum size to read
 *
 * @return struct ppByteBuffer - Bitmap data. Depending on data length, msg may have data returned. Check Errno for errors
 * IMPORTANT NOTES:
 * @li Application must free output buffer 'data' member when non-NULL.
 */
/*SVC_PROTOTYPE*/ struct ppByteBuffer pxpinpad_disp_getBitmap( int line, int col, int width, int len);

/** Set the screen contrast
 *
 * @param[in] level contrast value
 *
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_disp_setContrast( int level );

/** Turn On or off display
 *
 * @param[in]  state    0 = off, 1 = on
 *
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_disp_setBacklight( int state );

/** Set up delay that turns the display backlight on or off
 *
 * @param[in]  delay   Time in milliseconds of backlight activation (0 means infinite)
 *
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_disp_setBacklightDelay( unsigned int delay );

/** Register a new user Font
 *
 * @param[in]  slot   Font slot to redefine
 * @param[in]  font   Font buffer
 *
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_disp_defineFont( int slot, struct ppByteBuffer font );

/** Authenticate a downloaded font.
 *
 * @param[in]  slot   Slot to authenticate
 * @param[in]  font   Signature buffer
 *
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_disp_authFont( int slot, struct ppByteBuffer font );

/************************************************************************
                            KEYBOARD APIS
 ***********************************************************************/

/** Initialize keyboard device. Shall be called once (at least).
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_keybd_init(  );

/** Get keyboard information.
 * @return struct ppKeybdStruct contain keyboard info. Check Errno for errors
 */
/*SVC_PROTOTYPE*/ struct ppKeybdStruct pxpinpad_keybd_getInfo(  );

/** Clear the keyboard buffer.
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_keybd_flush(  );

/** Gets the next available key from the buffer.
 *
 * @param[in] timeout Time (in milliseconds) to wait for a key strike.
 *
 * @return key value, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_keybd_getKey( unsigned int timeout );

/** Watch the next available key from the buffer.
 *
 * @param[in] timeout Time (in milliseconds) to wait for a key strike.
 *
 * @return key value, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_keybd_watchKey( unsigned int timeout );

/** Simulates a key strike.
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_keybd_setKey( int key );

/** Get the number of keystrokes in the buffer.
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_keybd_length( );

/** Determines whether any keystrokes are in the keystroke buffer
 *
 * @param[in] timeout Time (in milliseconds) to wait for a key strike.
 *
 * @return 0 if key has been pressed, or -1 if timeout expires
 */
/*SVC_PROTOTYPE*/ int pxpinpad_keybd_isKeyPressed( unsigned int timeout );


/** Set beep frequency on key strike.
 *
 * @param[in] frequency Beep frequency in Hz, 0 to disable
 *
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_keybd_setBeep( unsigned int frequency );

/************************************************************************
                            SMARTCARD APIs
 ***********************************************************************/

/** Initialize smartcard device. Shall be called once (at least).
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_sc_init( );

/** Check if card is inserted
 * @return 0 if card is detected, or -1 if not.
 */
/*SVC_PROTOTYPE*/ int pxpinpad_sc_detect( );

/** Wait for card insertion
 *
 * @param[in] timeout Time (in milliseconds) to wait for the card detection.
 *
 * @return 0 if card has been inserted before timeout, or -1 if timeout expires.
 */
/*SVC_PROTOTYPE*/ int pxpinpad_sc_waitInsertion( unsigned int timeout );

/** Wait for card removal
 *
 * @param[in] timeout Time (in milliseconds) to wait for the card detection.
 *
 * @return 0 if card has been removed before timeout, or -1 if timeout expires.
 */
/*SVC_PROTOTYPE*/ int pxpinpad_sc_waitRemoval( unsigned int timeout );

/** Reset the card and get the ATR
 *
 * @param[in] mode Perform warm (0) or cold (1) reset
 * @param[in] spec ISO (0) or EMV (1)
 *
 * @return struct ppCardInfo - Contain ATR buffer. Depending on info length, msg may have an ATR returned. Check Errno for errors
 * IMPORTANT NOTES:
 * @li Application must free 'info' member when non-NULL.
 */
/*SVC_PROTOTYPE*/ struct ppCardInfo pxpinpad_sc_reset( int mode, int spec );

/**  Return protocol and last reset type.
 *
 * @return struct ppScInfo - Contain protocol T=0:0, T=1:1 and reset type warm:0, cold:1. Check Errno for errors
 */
/*SVC_PROTOTYPE*/ struct ppScInfo pxpinpad_sc_getCardInfo( );

/** Send command and get the response from the card.
 *
 * @param[in] cmd card command
 *
 * @return struct ppCardResponse - status word is defined by 'sw1Sw2' member. Depending on response length, msg may have data returned. Check Errno for errors
 * IMPORTANT NOTES:
 * @li Application must free response buffer 'data' member when non-NULL.
 */
/*SVC_PROTOTYPE*/ struct ppCardResponse pxpinpad_sc_xchgCommand( struct ppCardCommand cmd );

/** Disconnect the smart card.
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_sc_disconnect( );

/** Abort card detection.
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_sc_abort( );

/** Returns IFM certification version
 * @return struct ppScVersion - Contain IFM version (ASCII) - Check Errno for errors
 */
/*SVC_PROTOTYPE*/ struct ppScVersion pxpinpad_sc_getVersion( );

/** Change the SCLIB options
 *
 * @param[in] set flag to be set
 * @param[in] reset flag to be reset
 *
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_sc_setOptions( unsigned char set, unsigned char reset );

/************************************************************************
                            CONTACTLESS APIs
 ***********************************************************************/

/*SVC_PROTOTYPE*/ struct ppPcdVersion pxpinpad_cless_getPCDVersion( );

/** Initialize contactless device: Clear all configured parameters, MUST be called before EACH transaction.
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_cless_init( void );

/** Check for card presence.
 *
 * @param[in] timeout Time (in milliseconds) to wait for the card detection.
 *
 * @return struct ppCardInfo - card type is defined by 'type' member. Depending on info length, msg may have an ATS returned.
 * IMPORTANT NOTES:
 * @li Application must free 'info' member when non-NULL.
 */
/*SVC_PROTOTYPE*/ struct ppCardInfo pxpinpad_cless_waitCard( unsigned int timeout );

/** Send command and get the response from the card.
 *
 * @param[in] command card command
 *
 * @return struct ppCardResponse - status word is defined by 'sw1Sw2' member. Depending on response length, msg may have data returned.
 * IMPORTANT NOTES:
 * @li Application must free response buffer 'data' member when non-NULL.
 */
/*SVC_PROTOTYPE*/ struct ppCardResponse pxpinpad_cless_xchgCommand( struct ppCardCommand command );

/** Abort contactless card detection (pxpinpad_cless_waitCard).
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_cless_abort( void );

/** Wait for card removal and deactivate RF antenna.
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_cless_removeCard( void );

/** Wait for card removal and deactivate RF antenna.
 *
 * @param[in] event  event when message/led/beep will be displayed/started
 * @param[in] message  message to display
 * @param[in] led_anim list of successive LED state to display.
 * @param[in] beep_seq list of successive Buzzer states.
 *
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_cless_displayMessageLedBuzzer( unsigned char event, struct ppDispMsg message,
                            struct ppLedAnim led_anim, struct ppBeepSequence beep_seq );

/** Send the last command and get the response from the card  and start transaction end animation on success
 *
 * @param[in] command card command
 *
 * @return struct ppCardResponse - status word is defined by 'sw1Sw2' member. Depending on response length, msg may have data returned.
 * IMPORTANT NOTES:
 * @li Application must free response buffer 'data' member when non-NULL.
 */
/*SVC_PROTOTYPE*/ struct ppCardResponse pxpinpad_cless_xchgLastCommand( struct ppCardCommand command );

/** Preload card command that will be send on card detection
 *
 * @param[in] command card command
 *
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_cless_preloadCommand( struct ppCardCommand command );

/** Get the response of the preloaded command
 *
 * @return struct ppCardResponse - status word is defined by 'sw1Sw2' member. Depending on response length, msg may have data returned.
 * IMPORTANT NOTES:
 * @li Application must free response buffer 'data' member when non-NULL.
 */
/*SVC_PROTOTYPE*/ struct ppCardResponse pxpinpad_cless_getResult( );

/** Activate RF antenna.
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_cless_activateRF( void );

/** Deactivate RF antenna.
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_cless_deactivateRF( void );

/** Configure contactless device.
 *
 * @param[in] option Option to set
 * @param[in] input Set options value
 *
 * @return struct ppByteBuffer - Output data. Depending on data length, msg may have data returned. Check Errno for errors
 * IMPORTANT NOTES:
 * @li Application must free output buffer 'data' member when non-NULL.
 */
/*SVC_PROTOTYPE*/ struct ppByteBuffer pxpinpad_cless_setOption( unsigned char option, struct ppByteBuffer input );

/************************************************************************
                            SECURITY APIs
 ***********************************************************************/

/** Generate a random number
 *
 * @param[in] size Length of the random number
 *
 * @return struct ppByteBuffer - Output data. Depending on data length, msg may have data returned. Check Errno for errors
 * IMPORTANT NOTES:
 * @li Application must free output buffer 'data' member when non-NULL.
 */
/*SVC_PROTOTYPE*/ struct ppByteBuffer pxpinpad_sec_generateRandom( int size );

/** Load a certificate
 *
 * @param[in] type Public key type used to authenticate certificate
 * @param[in] certif Certificate structure
 *
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_sec_loadCertificate( int type, struct ppByteBuffer certif );

/** Load a certificate
 *
 * @param[in] type Public key type
 * @param[in] key Public key structure
 *
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_sec_loadPublicKey( unsigned char type, struct ppByteBuffer key );

/** Read a certificate.
 *
 * @param[in] type Certificate type
 *
 * @return struct ppByteBuffer - certificate. Depending on data length, msg may have data returned.
 * IMPORTANT NOTES:
 * @li Application must free 'data' member when non-NULL.
 */
/*SVC_PROTOTYPE*/ struct ppByteBuffer pxpinpad_sec_readCertificate( unsigned char type );

/** Read a public key
 *
 * @param[in] type Public key type
 *
 * @return struct ppByteBuffer - public key. Depending on data length, msg may have data returned.
 * IMPORTANT NOTES:
 * @li Application must free 'data' member when non-NULL.
 */
/*SVC_PROTOTYPE*/ struct ppByteBuffer pxpinpad_sec_readPublicKey( unsigned char type );

/** Display a custom prompt
 *
 * @param[in]    f_szFileName:    Name of the binary file which contains all prompts
 * @param[in]    f_sPromptIndex:  Prompt index in the binary file.
 * @param[in]    f_szParameters:  String to be displayed using prompt format.
 * @param[in]    pFuncExitKey:    This parameter determines which function key has been pressed before prompt exit.
 * @param[in]    f_szDataEntry:   Default data entry to be displayed
 * @param[in]    f_sMaxBuffSize:  Maximum number of bytes that can be stored in output buffer
 * @param[in]    f_sFirstTimeout: Time to wait for first key press (in milli-sec)
 * @param[in]    f_sInterTimeout: Time to wait for next key press (in milli-sec)
 *
 * @return struct ppByteBuffer - public key. Depending on data length, msg may have data returned.
 * IMPORTANT NOTES:
 * @li Application must free 'data' member when non-NULL.
 */
/*SVC_PROTOTYPE*/ struct ppPromptResult pxpinpad_disp_showPrompt(
                        char* f_szFileName, int f_sPromptIndex, struct ppPromptParam f_stParameters,
                        char* f_szDataEntry, int f_sMaxBuffSize,
                        int f_sFirstTimeout, int f_sInterTimeout );

/** Abort a running prompt
 *
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */                   
/*SVC_PROTOTYPE*/ int pxpinpad_disp_abortPrompt();


/** Perform a DUKPT PIN online encryption
 *
 * @param[in]  f_bKeyOwnerID MK key owner ID
 * @param[in]  f_bKeyID MK key ID
 * @param[in]  f_pbPAN PAN
 * @param[in]  f_bKeyFormat Session key format ( 1:DES_ECB, 2:DES_CBC, 3:TR31 )
 * @param[in]  f_pbEncryptedKey Encrypted session key
 *
 * @return struct ppMsPIN - Encrypted PIN block.
 */
/*SVC_PROTOTYPE*/ struct ppMsPIN pxpinpad_sec_onlinePIN_MS(
                        unsigned char f_bKeyOwnerID, unsigned char f_bKeyID,
                        struct ppByteBuffer f_pbPAN,
                        unsigned char f_bKeyFormat,
                        struct ppByteBuffer f_pbEncryptedKey );

/** Encrypt PIN using DUKPT key.
 *
 * @param[in]  f_bKeyOwnerID DUKPT key owner ID
 * @param[in]  f_bKeyID DUKPT key ID
 * @param[in]  f_pbPAN Customer PAN
 *
 * @return struct ppDukptPIN - ecnrypted PIN block.
 */
/*SVC_PROTOTYPE*/ struct ppDukptPIN pxpinpad_sec_onlinePIN_DUKPT(
                        unsigned char f_bKeyOwnerID, unsigned char f_bKeyID,
                        struct ppByteBuffer f_pbPAN );

/** Process EMV PIN offline verification.
 *
 * @param[in] f_bPINMode PIN mode (1:PP_EMV_PLAINTEXT, 2: PP_EMV_CIPHERED, 3:PP_MONEO_PLAINTEXT)
 * @param[in] f_uExponent Public exponent
 * @param[in] f_pbModulo Public key modulo
 *
 * @par Detailled description:
 *      A PIN prompt shall be previously requested.
 *      
 *      On Ciphered mode, a GET CHALLENGE command is sent if challenge size is 0.
 *      Then the PIN is retrieved from the last prompt and formatted to send VERIFY PIN command. 
 *
 *      On plain text mode, the PIN block is formatted using the requested format and send VERIFY PIN command.
 *
 * @return int - Status word returned by VERIFY PIN APDU. Check errno for status:
 *     0: Process succeeded, VERIFY PIN status word is returned.
 *     EINVAL : bad input parameters      
 *     EACCES : PIN disabled 
 *     EXDEV:  Get challenge error, status word of GET CHALLENGE APDU is returned
 */
/*SVC_PROTOTYPE*/ int pxpinpad_sec_offlinePIN_EMV( int f_bMode,
                        struct ppByteBuffer f_pbChallenge,
                        unsigned int f_uExponent,
                        struct ppByteBuffer f_pbModulo );

/** Load Bank key in pinpad
 *
 * @param[in] wrapping_key transport key slot
 * @param[in] wrapped_key key slot
 * @param[in] key_blob bank key blob
 *
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int pxpinpad_sec_loadBankKey( struct ppKeySlot wrapping_key, struct ppKeySlot wrapped_key, struct ppBankKey key_blob);

/** Retrieve security status.
 *
 * @return struct ppSecStatus - Security status.
 */
/*SVC_PROTOTYPE*/ struct ppSecStatus pxpinpad_sec_getStatus( );

/** Retrieve EMV information.
 *
 * @return struct ppEmvVersion - EMV version and checksum.
 */
/*SVC_PROTOTYPE*/ struct ppEmvVersion pxpinpad_sec_getEmvInfo( );

#endif
