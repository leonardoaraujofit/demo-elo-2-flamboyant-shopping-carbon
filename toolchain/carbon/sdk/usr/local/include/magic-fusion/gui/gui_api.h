/**
 * file	   $Source: /home/cvs/cobra/linux/rootfs/libgui/include/gui/gui_api.h,v $
 *
 * @brief	OS and AM GUI API calls
 *
 * @par		Descrition:
 *          	This file contains the OS and AM old and new apis for GUI.
 *
 * @date      	06 Jun 2005 (creation)
 *            	$Date: 2009-05-04 14:39:08 $ (last modification)
 *
 * @author    	$Author: gerard $
 *
 * @version   	$Revision: 1.52 $
 *
 * @par       	Copyright:
 *          (c) Copyright Axalto 2005 unpublished work.
 *          This computer program includes confidential, proprietary 
 *          information and is a trade secret of Axalto. All use, disclosure
 *          and/or reproduction is prohibited unless authorized in writing.
 *          All rignts reserved.
 * 
 */
#ifndef __GUI_API__
#define __GUI_API__

#include "./os/os_disp.h"
#include "./os/os_keybd.h"
#include "./gui/kbdapi.h"
#include "./os/slstrmem.h"

#define MENU_DISABLED (int)0x80
extern uint16 gDISP_WIDTH;
extern uint16 gDISP_HEIGHT;


/** Display Options 
 @ingroup IHM_MENU */
typedef enum
{
   MENU_NB_LEFT = 0x0,              /**< default (numbers on the left) */
   MENU_NB_TOP_RIGHT_SINGLE = 0x1,  /**< single menu numbers in top right */
   MENU_NB_TOP_RIGHT_MULTI = 0x2,   /**< multiple menu numbers in top right */
   MENU_NO_NB = 0x3                 /**< no number */
} MENU_NB_DISP_OPT;


typedef enum
{
   MENU_TYPE_NORMAL = 0,
   MENU_TYPE_SINGLE_ICON = 1,
   MENU_TYPE_MULTI_ICON = 2, 
   MENU_TYPE_HORIZONTAL_ICON = 3,
   MENU_TYPE_TEXT_EXT = 4,
} MENU_TYPE;

/** basic entity of a heriarchy for the menus 
 @ingroup IHM_MENU */
typedef struct
{
   uint16 level;                /**< Depth for the label */
   const uint8 *name;                 /**< Name of the label */
     int8( *function ) ( void );        /**< Function to execute when selected. (warning, return type has changed since AGP2) */
} HIERARCHY;

/** List of Icon pathes corresponding to menu items 
 * @ingroup IHM_MENU */
typedef const uint8 *MENU_ICONS[MAX_ITEMS];

/** basic entity of a heriarchy for menu with ICONS 
 @ingroup IHM_MENU */
typedef struct
{
   uint8 level;                 /**< Depth for the label */
   const uint8 *icon_path;            /**< GIF icon path (can be NULL) */
   const uint8 *name;                 /**< Name of the label */
     int8( *function ) ( void );        /**< Function to execute when selected. */
} HIERARCHY_ICON;

/** Parameters for Modal Dialog
 *@ingroup GUIDIALOG */
typedef struct
{
   const uint8 *text;                 /**< text to display */
   uint16 ctlKeyMask;           /**< mask of keys, from type CTL_MAGIC_KEY_MASK, associated to this text */
} MASKED_MODAL_PARAM;

/* arguments for thread that animates GIF */
struct args_tga
{
   struct animatedBmp *abmp;    //animated GIF to display
   uint8 line;                  //where to display (line, top left of bmp)
   uint8 column;                //where to display (column, top left of bmp)
};
/* arguments for thread that animates text */
struct args_trt
{
   const uint8 *text;                 //long text to display
   uint8 font;                  //font to use
   uint8 line;                  //where to display (line)
   uint8 column;                //where to display (column)
   DISP_TYPE wDispLen;          //text length
};

/* TRUE if a scrollbar is used */
extern BOOLEAN g_isScrollBar;

#ifndef SPACE
#define SPACE ' '
#endif

/* max heigth (in pixels) for the multi-icon mode */
#define YLIMIT_FOR_MULTI_ICON 24

/* maximum depth for menus */
#define MAX_LEVEL 5
/* timeout to wait in a menu, for a keypress, before cancel (in sec.) */
#define MENU_TIMEOUT    40

/* Arrow Scroll mode defines */
#define MODAL_SCROLL_UNUSED 0
#define MODAL_SCROLL_KEY1   1
#define MODAL_SCROLL_KEY3   3

/** Menu type for shortcuts @ingroup IHM_MENU */
typedef uint8 MENU_SHORTCUT[MAX_LEVEL];
/* deprecated but used for old apis... */
typedef uint8 *HOTKEY_MENU[5];

/** Hotkey menu for magic (title + 3 choice name)
 *@ingroup  AM_IHMFUNCS */
typedef uint8 *MAGIC_HOTKEY_MENU[4];

#define DC1 0x11
#define DC2 0x12
#define DC3 0x13
#define DC4 0x14
#define ICON_HEIGHT 24
#define ICON_WIDTH 24
#define MIN_FONT_HEIGHT 8
#define MAX_FONT_HEIGHT 16
#define MAX_DISPLAY_NB_LINES gDISP_HEIGHT / MIN_FONT_HEIGHT
#define MIN_DISPLAY_NB_LINES gDISP_HEIGHT / MAX_FONT_HEIGHT

/** Parameters for UiDisplayScrollText: Define which icon will be displayed at the beginning of the text to scroll.
 *@ingroup GUIDIALOG */
typedef enum
{
   TEXT_TYPE_STANDARD,
   TEXT_TYPE_WARNING,
   TEXT_TYPE_FORBIDDEN,
   TEXT_TYPE_ERROR,
   TEXT_TYPE_INFORMATION,
} TEXT_TYPE;

#define BLINK_TIMEOUT_MS 500
/* Blinking status of the text */
typedef enum
{
   BLINK_STAT_UNKNOWN,
   BLINK_STAT_ENABLE,
   BLINK_STAT_DISABLE,
} BLINK_STAT;
/* Blink command to activate or not the blinking */
typedef enum
{
   BLINK_CMD_ON,
   BLINK_CMD_OFF,
} BLINK_CMD;

typedef int8( *GUI_HANDLER ) ( MENU_SHORTCUT shortcut, uint8 * name );

// DisplayMessage attribute
#define CENTERED_LINE1  0x01
#define CENTERED_LINE2  0x02
#define CENTERED_LINE3  0x04
#define CENTERED_LINE4  0x08
#define CLEAR_DISPLAY   0x80

#ifdef __cplusplus
extern "C"
{
#endif

/* OS APIs (from os_display.h) -- already included */
/* AM APIs (from appman.h) */
/* UI functions (from sluimsg.h) */

/* New API */
   int8 UiInitGui( GUI_HANDLER );

   int8 UiSetMenuHandler( MAGIC_KEY_TYPE, GUI_HANDLER );

   /* ManMenuRun */
   int8 ManMenuRun( const HIERARCHY * menu );

   /* Save the default level */
   int8 UISetDefaultLevel( uint8 f_Level );
   int8 UIGetMemoryLevel( uint8 * f_Level );

   /* ManMenuRun2 */
   int8 UiDisplayHierarchyMenu( const HIERARCHY *, MENU_SHORTCUT shortcut,
                                MENU_NB_DISP_OPT numberDisplayOption,
                                uint32 timeout );
   /* KeyBoardMenu2 */
   int8 UiDisplayMenu( uint8 fNbItem, MENU fMenu, MENU_ICONS icons,
                       uint8 * fFirstItem, uint8 fOldItem,
                       MENU_NB_DISP_OPT nbDispOption, uint32 fTimeOut );

   int8 UiDisplayMenuHztal( uint8 fNbItem, MENU fMenu, MENU_ICONS icons,
                            uint8 * fFirstItem, uint8 fOldItem,
                            MENU_NB_DISP_OPT nbDispOption, uint32 fTimeOut );

   /* ManMenuIconRun */
   void UiGetHierarchyInfo( uint16 * level, uint8 * hierachy );

   int8 UiDisplayHierarchyMenuIcon( const HIERARCHY_ICON * menu,
                                    MENU_SHORTCUT shortcut,
                                    MENU_NB_DISP_OPT nbDispOption,
                                    uint32 timeout );
   int8 UiDisplayHierarchyMenuIconHztal( const HIERARCHY_ICON * menu,
                                    MENU_SHORTCUT shortcut,
                                    MENU_NB_DISP_OPT nbDispOption,
                                    uint32 timeout );
   void UiDisplayAdvancedMsg( const uint8 * title, const uint8 * text, BOOLEAN doBeep,
                              uint32 timeout );
   int8 UiDisplayModalDialog( const uint8 * title, const uint8 * icon_path, const uint8 * text,
                              const MASKED_MODAL_PARAM * mParam, uint8 nbMParam,
                              uint32 timeout, BOOLEAN isModal,
                              uint8 defaultChoice );

   void UiDisplayProgressBar( const uint8 * title, const uint8 * text, uint32 maxi,
                              BOOLEAN isFrameBox );
   void UiProgressBarUpdate( uint32 value, const uint8 * text );
   int32 UiProgressBarValue(  );

   void UiDisplayDualProgressBars( const uint8 * title,
                                   const uint8 * text1, uint32 maxi1,
                                   const uint8 * text2, uint32 maxi2 );
   void UiProgressBarDualUpdate( uint32 value1, const uint8 * text1, uint32 value2,
                                 const uint8 * text2 );
   int8 UiDisplayNumberedSpinBox( const uint8 * title, const uint8 * text,
                                  const uint8 * optName, int16 min, int16 max,
                                  uint8 step, int16 * initialValue,
                                  uint32 timeout);

   int8 UiDisplayNumberedSpinBoxImpl( const uint8 * title, const uint8 * text,
                                  const uint8 * optName, int min, int max,
                                  unsigned int step, int * initialValue,
                                  uint32 timeout, 
                                  MAGIC_KEY_TYPE inc_key,
                                  MAGIC_KEY_TYPE dec_key, 
                                  const char * inc_text,
                                  const char * dec_text);

   int8 UiDisplayGraphicalSpinBox( const uint8 * title, const uint8 * optName,
                                   uint16 mini, uint16 maxi,
                                   uint16 * initialValue, uint32 timeout );

   int8 UiDisplayListSpinBox( const uint8 * title, uint8 * list[],
                              uint8 nbItem, int16 * initialValue,
                              uint32 timeout );

   int8 UiDisplayIconSpinBox( uint8 * list[], int16 nbItem,
                              int16 * firstIndex, uint32 timeout );

/* API Helpers */
   int8 KeyBoardMenu( uint8 fNbItem, MENU fMenu, uint8 * fFirstItem,
                      uint8 fOldItem, uint8 fTimeOut );
   void DisplayMenu( uint8 fNbItem, MENU fMenu, MENU_ICONS fIcons,
                     uint8 * fFirstItem, uint8 fNewItem, uint32 * timeout );
   void DisplayTitle( const uint8 * title, BOOLEAN centered );

   int8 DisplayList( uint8 ** list, uint8 nbItem, int16 initialValue,
                     uint32 timeout, int16 * newItem );

   void DisplayChoices( const MASKED_MODAL_PARAM * mParam, uint8 nbMParam,
                        uint8 optSelect, uint32 timeout );
   void DisplayModalDialog( const uint8 * title, struct animatedBmp *animBmp,
                            uint8 scrollMode, const uint8 * text,
                            const MASKED_MODAL_PARAM * mParam, uint8 nbMParam,
                            uint32 * timeout );
   int8 LaunchGIFAnimator( struct animatedBmp *abmp,
                           uint8 line, uint8 column, uint32 * timeout );
   void DisplayTextToScreen( const uint8 * text, DISP_TYPE f_wDispLineLen,
                             uint8 heightAvailable, uint8 dx, uint8 dy );
   /* Imported from the AM */
   void UiEraseLine( uint8 );
   OS_STATUS FillPhysicalLine( uint16, uint8 );
   OS_STATUS FillPhysicalPartOfLine( uint16, uint8, uint8 );
   OS_STATUS FillLine( uint16, uint8 );
   OS_STATUS FlushAndDisplay( uint8 * );
   void ClearDisplay( void );
   void DisplayMessage( uint8 *, uint8 *, uint8 *, uint8 *, uint8, uint8 );
   void DisplayHotKeyMenu( MAGIC_HOTKEY_MENU fHkMenu );
   uint8 HotKeyChoice( MAGIC_HOTKEY_MENU fHkMenu, uint8 fKey );
   MAGIC_KEY_TYPE KeyBoardValid( const uint8 * fPrompt, const uint8 * fMessage1,
                           const uint8 * fMessage2,const uint8 * fMsgHotKey1,
                           const uint8 * fMsgHotKey2, const uint8 * fMsgHotKey3,
                           uint8 unused, uint16 fTimeOut );
   OS_STATUS SetLargeFontSlot( uint8 f_bFontId );
   OS_STATUS SetLargeMiniFontSlot( uint8 f_bFontId );
   OS_STATUS SetSmallFontSlot( uint8 f_bFontId );
   int16 UiDisplayScrollText( const uint8 * f_pbTitle, const uint8 * f_pbText,
                              TEXT_TYPE f_enType, uint8 f_bFastScroll,
                              uint8 f_bFormat, uint32 f_dwFirstTimeout,
                              uint32 f_dwInterTimeout );
#ifdef __cplusplus
}
#endif

#endif
