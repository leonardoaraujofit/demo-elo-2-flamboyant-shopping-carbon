/**
 * @addtogroup IHM_MENU
 */
/*@{*/
#ifndef _KBDAPI_H_
#define _KBDAPI_H_

#include "./os/os_keybd.h"

// A mettre en commentaire si ON NE FAIT PAS de tests sur le simulateur
// #define DEBUG_MODE

#define MAX_DISP_WIDTH (20+1)


/* Input return code */
#define  INPUT_OK       (int8)   0     /**< No error */
#define  INPUT_BAD      (int8)  -1     /**< Bad input */
#define  INPUT_TIMEOUT  (int8)  -2     /**< Timeout */
#define  INPUT_CANCEL   (int8)  -3     /**< Canceled */
#define  INPUT_CLEAR    (int8)  -4     /**< RFU*/
#define  INPUT_PROGRESS (int8)  -5     /**< RFU*/
#define  INPUT_CHANGED  (int8)  -6     /**< used in Spinboxes, GUI API */
#define  INPUT_EXIT     (int8)  -7     /**< used in Menus, GUI API */
#define  INPUT_BACK     (int8)  -8     /**< used in Menus, GUI API */


#define MAX_ITEMS 10 /**< Max items in a menu */
typedef const uint8 *MENU[MAX_ITEMS]; /**< Menu type */

/*@}*/

#if defined(__cplusplus) && !defined(__DEBUG__)
extern "C"
{
#endif
   int8 KeyBoardMenu( uint8 fNbItem, MENU fMenu, uint8 * fFirstItem,
                      uint8 fOldItem, uint8 fTimeOut );

#if defined(__cplusplus) && !defined(__DEBUG__)
}
#endif

#endif
