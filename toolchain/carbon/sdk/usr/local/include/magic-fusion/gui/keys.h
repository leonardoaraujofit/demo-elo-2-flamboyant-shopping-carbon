/****************************************************************************
 * @file	   $Source: /home/cvs/cobra/linux/rootfs/libgui/include/gui/keys.h,v $
 *
 * @brief	Keyboard - High Level Services
 *
 * @par		Descrition:
 *          	Full description for file.h
 *
 * @date      	06 july 2005 (creation)
 *
 * @author    	$Author: hameau $
 *
 * @version   	$Revision: 1.3 $
 *
 * @par       	Copyright:
 *            	(c) Copyright Axalto 2005 unpublished work.
 *            	This computer program includes confidential, proprietary 
 *            	information and is a trade secret of Axalto. All use, 
 *            	disclosure and/or reproduction is prohibited unless
 *            	authorized in writing.
 *            	All rignts reserved.
 *
 ***************************************************************************/

#ifndef _KEYS_H_
#define _KEYS_H_

#include "./os/os_types.h"

//extern uint8 gMODE_LENGTH;
#define FONT_KEY_PLUSMINUS       0x18
#define FONT_KEY_DOTS            0x19
#define FONT_KEY_SPLITTER        0x1A
#define FONT_KEY_ARROW_RIGHT     0x1B
#define FONT_KEY_ARROW_LEFT      0x1C
#define FONT_KEY_ARROW_UP_DOWN   0x1D
#define FONT_KEY_ARROW_DOWN      0x1E
#define FONT_KEY_ARROW_UP        0x1F


extern uint8 gMAX_OUTPUT_GUI;

/* Following constants are initialized by main */
/* using OS_DisplayInfo function */
//extern uint8 gMAX_LINES;
//extern uint8 gMAX_OUTPUT_GUI;

#endif


/*$--- End of file : keys.h ---*/
