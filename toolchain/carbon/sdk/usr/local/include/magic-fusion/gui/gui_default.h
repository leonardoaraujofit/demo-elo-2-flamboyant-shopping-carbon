/*
 * $Source: /home/cvs/cobra/linux/rootfs/os_bin/include/os_menu/os_menu.h,v $
 *
 * $Date: 2010-05-11 14:39:52 $
 *
 * $Author: w8 $
 *
 * $Revision: 1.45 $
 *
 * (c) Copyright Axalto 2005 unpublished work.
 * This computer program includes confidential, proprietary 
 * information and is a trade secret of Axalto. All use, 
 * disclosure and/or reproduction is prohibited unless
 * authorized in writing.
 * All rignts reserved.
 *
 */

#ifndef __GUI_DEFAULT_H__
#define __GUI_DEFAULT_H__


/* MAGIC compatibility: Originally these value were included in os_menu/os_menu.h 
                                    Now since the os_menu doesn't exist anymore they are part of the gui library */

/* Menu  Title */
#define MENU_TITLE "OS MENU"
#define CONFIG_MENU_TITLE             "Configuration"
#define AUTHENTIFICATION_TITLE        "Authentification"
#define ACTIVATION_IFACE_TITLE        "Iface activation"
#define SETUP_INTERFACE_TITLE         "Interface"
#define SETUP_CHARLEN_TITLE           "Char length"
#define SETUP_STOPBITS_TITLE          "Stop bits"
#define SETUP_PARITY_TITLE            "Parity"
#define SETUP_BAUDRATE_TITLE          "Baud rate"
#define SETUP_MODULATION_TITLE        "Modulation"
#define PROTOCOL_TITLE                "Protocol"
#define DIAL_TYPE_TITLE               "Dial type"
#define USE_DHCP_MSG                  "Use DHCP"
#define USE_DNS_MSG                   "Use DNS"
#define SETUP_SUCCESS_MSG             "Media setup succeeded"
#define SETUP_ERROR_MSG               "Media setup failed"
#define ACTIVATION_SUCCESS_MSG        "Media activation succeeded"
#define ACTIVATION_ERROR_MSG          "Media setup failed"
#define MEDIA_NOT_FOUND_MSG           "Media not found"
#define PROTOCOL_NOT_FOUND_MSG        "Protocol not found"
#define SETUP_IP_ERROR_MSG            "IP address error"
#define SAVE_TERMINAL_ID_OK_MSG       "Terminal ID OK"
#define SAVE_TERMINAL_ID_ERROR_MSG    "Terminal ID ERROR"
#define SELECT_PPP_OPTION             "Set PPP options"

/* Default timeout */
#define BEEP_TIMEOUT_MS 100
#define GETKEY_TIMEOUT_MS 500
#define OSMENU_TIMEOUT INFINITE_TIMEOUT
#define OSMENU_CONFIG_TIMEOUT 10000
#define TIMEOUT_MS_MSG 1500
#define MODAL_TIMEOUT_MS 10000
#define PROMPT_FIRST_TIMEOUT 15000
#define PROMPT_INTER_TIMEOUT 10000
#define SPIN_TIMEOUT_MS 10000

#endif
