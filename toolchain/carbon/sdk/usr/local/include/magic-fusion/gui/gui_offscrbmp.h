/**
 * @file
 * $Source: /home/cvs/cobra/linux/rootfs/libgui/include/gui/gui_offscrbmp.h,v $
 *
 * @brief	IHM helper definitions
 *
 * @par		Descrition:
 *          	This file contains defines to use the IHM helpers.
 *
 * @date      	11 Aug 2005 (creation)
 *            	$Date: 2010-06-18 14:01:04 $ (last modification)
 *
 * @author    	$Author: besson $
 *
 * @version   	$Revision: 1.8 $
 *
 * @par       	Copyright:
 *          (c) Copyright Axalto 2005 unpublished work.
 *          This computer program includes confidential, proprietary 
 *          information and is a trade secret of Axalto. All use, disclosure
 *          and/or reproduction is prohibited unless authorized in writing.
 *          All rignts reserved.
 *
 */
#ifndef __GUI_HELPER_H__
#define __GUI_HELPER_H__

#include <stdio.h>
#include <string.h>

#include "./gui/gui_api.h"
#include "./gui/gif2bmp.h"
#include "./os/os_types.h"
#include "./gui/keys.h"

/** Back buffer used to draw pixel by pixel inside
 *@ingroup GUI_OSBMP */
struct offscrBMP
{
   uint8 *bmp;    /**< bitmap encoded in MagIC format */
   uint8 height8; /**< height in pixel/8 */
   uint8 width8;  /**< width in pixel/8 */
};

#ifdef __cplusplus
extern "C"
{
#endif

   void CreateOffScrBmp( struct offscrBMP *sbmp, uint8 height8,
                         uint8 width8 );
   void FreeOffScrBmp( struct offscrBMP *sbmp );
   void DispSpriteIntoOffScrBmp( struct offscrBMP *sbmp, uint8 y, uint8 x,
                                 uint8 * sprite );
   void DisplayFrameBox( uint8 y_offset );
   void SetPixelIntoOffScrBmp( struct offscrBMP *sbmp, uint8 x, uint8 y,
                               uint8 isBlack );
   void GetPixelIntoOffScrBmp( struct offscrBMP *sbmp, uint8 x, uint8 y,
                               uint8 * isBlack );
#ifdef __cplusplus
}
#endif

#endif
