/**
 * @file
 * $Source: /home/cvs/cobra/linux/rootfs/libgui/include/gui/gif2bmp.h,v $
 *
 * @brief	GIF to BMP header
 *
 * @par		Descrition:
 *          	This file contains API for the GIF converter.
 *
 * @date      	01 Aug 2005 (creation)
 *            	$Date: 2010-06-18 14:00:20 $ (last modification)
 *
 * @author    	$Author: besson $
 *
 * @version   	$Revision: 1.9 $
 *
 * @par       	Copyright:
 *          (c) Copyright Axalto 2005 unpublished work.
 *          This computer program includes confidential, proprietary 
 *          information and is a trade secret of Axalto. All use, disclosure
 *          and/or reproduction is prohibited unless authorized in writing.
 *          All rignts reserved.
 *
 */
#ifndef __GIF2BMP_H_
#define __GIF2BMP_H_

#include "./gui/gif_lib.h"
#include "./os/agp2conv.h"


/** This structure will contains animated bitmaps */
struct animatedBmp
{
   int nbImg;
   int width;
   int height;
   int length;
   unsigned char **bmp; 
   unsigned long *delay;
   int reversed;
};

#ifdef __cplusplus
extern "C"
{
#endif
/* convert a GIF file into BMP format */
   int gif2bmp( uint8 * filePath, struct animatedBmp *abmp );
   int gif2bmp_struct( GifFileType * icon, struct animatedBmp *abmp );
   int getGifInfo( uint8 * filePath, uint8 * height, uint8 * width,
                   uint8 * nbImages );
   void cleanAnimatedBmp( struct animatedBmp *abmp );
/* print bmps into file */
   void printfBmp( struct animatedBmp *abmp, int id );
#ifdef __cplusplus
}
#endif


#endif
