/****************************************************************************************/
/* The only purpose of this file is to provide the root of doxygen documentation module */
/****************************************************************************************/
/**  
 * @defgroup LIBOS_ROOT OS library
 *  
 * @brief OS library
 *  
 * @par Detailed description  
 *    This library provides low-level APIs to deal directly with peripherals
 *    or to interact with the operating system.  
 *  
 */
