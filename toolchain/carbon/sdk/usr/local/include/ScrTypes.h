/****************************************************************************
* This source code is confidential proprietary information of VeriFone Inc. *
* and is an unpublished work or authorship protected by the copyright laws  *
* of the United States. Unauthorized copying or use of this document or the *
* program contained herein, in original or modified form, is a violation of *
* Federal and State law.                                                    *
*                                                                           *
*                           Verifone                                        *
*                      Rocklin, CA 95765                                    *
*                        Copyright 2003                                     *
****************************************************************************/

/*
;|===========================================================================+
;| FILE NAME:  | ScrCtypes.h
;|-------------+-------------------------------------------------------------+
;| DESCRIPTION:| Smart Card Uart low level routines
;|-------------+-------------------------------------------------------------+
;| TYPE:       | 
;|-------------+-------------------------------------------------------------+
;| COMMENTS:   | 
;|===========================================================================+
*/

/*
$Archive:   $
$Author:    Thierry Payet
$Date:      04/08/14
$Modtime:   $
$Revision:  0.1
$Log:       $
*/

#ifndef     _Scrtypes_H_
#define     _Scrtypes_H_

#define     BOOLEEN     char

#define     FALSE       0
#define     TRUE        1

#define     UCHAR       unsigned char
#define     SINT_8      char
#define     SINT_16     int
#define     UINT        unsigned int
#define     ULONG       unsigned long

#endif
/*********************************************************_FILE_HEADER_END_*/
