
#ifndef __KEYINFO_H__
#define __KEYINFO_H__

#include "keyinfo_types.h"

const struct securityKey *getWarrantiedKeys(
		int version);
const struct securityKey *getExpectedKeys(
		int version);
void deleteSecurityKeys(
		const struct securityKey *keys);

const struct paymentKey *getPaymentKeys(
		int version);
void deletePaymentKeys(
		const struct paymentKey *keys);

#endif
