
#ifndef  _PINENTRY_H_
#define  _PINENTRY_H_

#define GETPIN_MIN          4
#define GETPIN_MAX          12
#define GETPWD_MIN          5

/* Pin Entry Algos */
#define EMV_PIN             0x0A
#define VSS_PIN             0x0B
#define POST_PROC_PIN       VSS_PIN
#define IPP_PIN             0x0C
#define SYS_PWD             0x0D
#define EMV_PASS            SYS_PWD

/* To send to public world for Apps */
#define NORM_BEEP_MSG       0x70
#define ERR_BEEP_MSG        0xF0
#define ECHO_CHAR_MSG       0x01
#define BS_CHAR_MSG         0x02
#define CLR_CHAR_MSG        0x03
#define EOT_CHAR_MSG        0x04
#define OTHER1_CHAR_MSG     0x05
#define OTHER2_CHAR_MSG     0x06
#define TS_NUM_SEL_MSG      0xD1  /* Numeric touchscreen hotspot selected */
#define TS_CAN_SEL_MSG      0xD2  /* CANCEL touchscreen hotspot selected */
#define TS_CLR_SEL_MSG      0xD3  /* CLEAR/BACKSPACE touchscreen hotspot selected */
#define TS_ENT_SEL_MSG      0xDE  /* ENTER touchscreen hotspot selected */

/* Pin entry states */
#define PIN_DONE            0x00
#define PIN_IDLE            0x01
#define PIN_COLLECTING      0x02
#define PIN_ABORTED         0x05
#define PIN_NULL_PIN        0x06
#define PIN_NO_MS_KEY       0x07
#define PIN_NO_DUKPT_KEY    0x08
#define PIN_MISC_ERROR      0x09
#define PIN_CLEAR_CANCEL    0x0A
#define PIN_PROG_CANCEL     0x0B
#define PIN_TIMED_OUT       0x0C

/* Pin entry options */
#define PIN_EN_AutoEnter    0x01 /* Automatically end PIN entry when the maximum
                                    number of allowable PIN digits entered */
#define PIN_EN_NULLPIN      0x02 /* Allow Null PIN entry */
#define PIN_EN_TS_ACCESS    0x04 /* Set Navigator Accessibility mode (Touchscreen ONLY) */
#define PIN_EN_Clr2BS       0x08 /* Translates the CLEAR to a BACKSPACE */
#define PIN_EN_Clr2Cancel   0x10 /* If no PIN digits entered, pressing CLEAR will
                                    CANCEL the PIN entry session */
#define PIN_EN_RATE_LIMIT   0x20 /* Limit the rate of PIN encrytions to no more
                                    than 1 per 30 seconds */
#define PIN_EN_MORE_CODES   0x40 /* Allow more key codes to be reported:
                                    no key, too many digits, and all other keys */
#define PIN_EN_TIMED_OUT    0xFE /* Pin session timed out */

/* Keypad modes */
#define SECURE_KEYPAD   1
#define PUBLIC_KEYPAD   0


typedef struct {
    unsigned char   ucMin;
    unsigned char   ucMax;
    unsigned char   ucEchoChar;
    unsigned char   ucDefChar;
    unsigned char   ucOption;
} PINPARAMETER;


typedef struct {
    unsigned char   nbPinDigits;
    unsigned char   encPinBlock[16];
} PINRESULT;


int SetupPinParams(PINPARAMETER * psKeypadSetup);
int SetPinBypassKey(unsigned char ucPinBypassKey);
int SelectPinAlgo(unsigned char ucNewPinAlgo);
int StartPinEntry(unsigned char * pPan);
int GetPinResponse(int * piStatus, PINRESULT * pRetEncPin);
int CancelPinEntry(void);

void set_pe_callback(void *callback);
#endif
