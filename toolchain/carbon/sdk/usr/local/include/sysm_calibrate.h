#ifndef __SYSM_CALIBRATE__
#define __SYSM_CALIBRATE__

#include <sysmode_ui/sysm_ui.h>
#include <sysmode_ui/sysm_lang.h>

int calibrate_touchpanel();

int calibration_required();

void calibrate_cb(const SYSM_UI::t_ui_menu_entry *);

#endif // __SYSM_CALIBRATE__
