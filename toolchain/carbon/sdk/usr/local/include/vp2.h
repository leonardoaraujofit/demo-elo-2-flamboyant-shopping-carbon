/*
 *	Copyright, 2004 VeriFone Inc.
 *	2099 Gateway Place, Suite 600
 *	San Jose, CA.  95110
 *
 *  All Rights Reserved. No part of this software may be reproduced,
 *  transmitted, transcribed, stored in a retrieval system, or
 *  translated into any language or computer language, in any form
 *  or by any means electronic, mechanical, magnetic, optical,
 *  chemical, manual or otherwise, without the prior permission of
 *  VeriFone Inc.
 */

/** @addtogroup vfisvc Services */
/** @{ */
/** @addtogroup payware Services : PAYWare Vision Library Functions */
/** @{ */
/** 
 *  @file vp2.h 
 *
 *	@brief VP2 API
 *
 * 
 */


#ifndef _VP_H
#define _VP_H

#ifdef __cplusplus
extern "C" {
#endif
 

#define MAX_FIELDS		300
#define MAX_KEY_SIZE	50
#define MAX_VALUE_SIZE	256

/**
 * vp_field_t
 */
 typedef struct 
{ 
    char    key[MAX_KEY_SIZE];
    char    value[MAX_VALUE_SIZE]; 
} vp_field_t;

/**
 * vp_filetag_t
 */
typedef struct {
	char	name[96];			// name
	char	fname[96];			// filename
	char	ftype[3];			// OS,SV,PK,AP,FM, or PP
	char	ver[32];			// version
	char	dltype;				// F=full, P=partial
	char	rboot;				// Y or N
	char	fsize[16];			// size of file in bytes
	char	apply[32];			// apply-on date
	char	ini[96];			// ini for apps
} vp_filetag_t;

/**
 * vp_filelist_t
 */
typedef struct vp_filelist {
	vp_filetag_t file;
	struct	vp_filelist *pnext;
} vp_filelist_t;

/**
 * vp2_parm_t
 */
typedef struct 
{ 
    int     iOptions;
    char    *pchAddMsg;
    void    (*fnUpData)(unsigned short, int, char *, int);
    void    (*fnUpDisconnect)(void);
    int     (*fnDownReq)(int, int, vp_field_t *);
    void    (*fnFileStatus)(int, char *);
    void    (*fnTimedOut)(void);
} vp2_parm_t;

/**
 * vp_file_t
 */
typedef struct 
{ 
    char    name[32];
    char    version[32]; 
} vp_file_t;

/**
 * @name vp2Init options
*/
/** \{ */
#define VP_OPT_FORCE_DL         1 /**< Force download */
#define VP_OPT_NO_AUTO_ACK      2	//
#define VP_OPT_SSL				4	//
#define VP_OPT_NO_XTRMCFG       8	//
#define VP_OPT_NO_XDEVREG       8	//
#define VP_OPT_CLOSE_UP			0x0010 /**< Disconnect Up Channel connection for V2 */
#define VP_OPT_TEST_ONLY		0x0020 /**< Testing Connection only */
#define VP_OPT_OSBASE			0x8000 /**< OS BASE */
/** \} */

/**
 * @name vp2SendPacket() options
*/
/** \{ */
#define VP_OPT_WAIT_ACK         2
/** \} */

#define ACK						0x06
#define NAK						0x15


#define STX						2
#define ETX						3
#define GS						0x1d	// group seperator
#define	FS						0x1c	// field seperator

/**
 * @name RETVAL values
*/
/** \{ */
#define VP_SUCCESS				0
#define VP_ERROR				1	// generic
#define VP_INVALID_MSGLEN		2
#define VP_INVALID_MSGNUM		3
#define VP_INVALID_CMD			4
#define VP_MISSING_PARM			5
#define VP_INVALID_PARM			6
#define VP_NO_PERMISSION		7
#define VP_APP_BUSY				8
#define VP_INVALID_FTYPE		9
/** \} */

/**
 * @name fnDownReq callback returns for permission
*/
/** \{ */
#define VP_DREQ_ALLOW			1
#define VP_DREQ_DONE_BY_APP		0
#define VP_DREQ_DENY			-1
#define VP_DREQ_BUSY			-2
#define VP_DREQ_FAIL_BY_APP		-3
#define VP_DREQ_INVALID_FTYPE	-4
/** \} */

/**
 * @name vfiLogMsg option argument definitions
*/
/** \{ */
#define VPLOGMSG_FLUSH_TMP         0x80
#define VPLOGMSG_VPSECLIB_WHO_ERR  "Oe"
#define VPLOGMSG_VPSECLIB_WHO_INFO "Oi"
#define VPLOGMSG_SYSMODE_WHO       "Sm"
/** \} */


/**
 * int vp2Init(vp_parm_t *vp)
 *
 * Connect to the VP Server (Up Channel) and send the XTRMCFG packet, if set.
 * If connection is successful, start the vp_monitor_up_vpSO thread for Up Channel 
 * data.  No matter what, always start the vp_listener_vpSO thread for Down Channel!
 *
 *  vp:   
 *  Return:     the socket descriptor of the Up Channel
 *				-ENOLINK	= link not up
 *				-EBUSY		= already running
 *				-EINVAL		= invalid parameters
 *				-ENOENT		= no required entry (VP IPaddr or port)
 *              -1			= generic error
 *				-EMSGSIZE	= msg length too big
 *				-ETIMEDOUT	= timedout waiting for response (or ETIMER?)
 *				-ENOTCONN	= error in connection or SSL handshake
 *				-CONNRESET	= connection closed by peer
 *				-EFAULT		= peer SSL certificate error
 *
 */
int vp2Init(vp2_parm_t *vp);

/**
 * int vp2SendPacket(int iFd, int iOptions, char *pchOutBuf, short iLength)
 *
 * This is called by the application and vpListener routines -- has semaphore checking so
 * as not to clobber current processing.
 *
 *  Inputs:
 *
 *  iOptions:   
 *  Return:     number of bytes sent, if successful
 *              -1 = timedout
 *				-ENOMEM = No memory
 *				-ECONNRESET = No connection
 */

int vp2SendPacket(int iFd, int iOptions, char *pchOutBuf, unsigned short ushLength);

/**
 * int vpCloseUp(int socketfd) - shut down the Up Channel connection and related tasks
 *
 * Return: 0 = Success
 *
 */

int vpCloseUp(int socketfd);

/**
 * int vpExit(int socketfd) - shut down the vp connection and related tasks
 *
 * Return: 0 = Success
 *
 */
int vpExit(int socketfd);

/**
* int vpParseFields()
* Return the number of items in the pszArray[]
* pszInBuffer	Input buffer
* pshArray		Array to output field and value
* pszSeparator Separator String (i.e. <FS>)
*
* Note that the first field is the command in key with no value.
*/
int vpParseFields(char *pszInBuffer, vp_field_t *pszField, char *pszSeparator);

/**
*
* vpVersion - returns library version string xx.xx.xx
*
*/
void vpVersion(char *pszVersion);

/*
 * int vpCloseDown(void) - shut down the Down Channel connection and related tasks
 *
 * Return:  0 = Success
 */
int vpCloseDown(void);

/**
 *  vpCreateXDEVREGStringCreate
 */
void vpCreateXDEVREGString(char *pchObuf, char *pchCustom, int iOptions);

/**
 * int vpGetFile(int ushDownloadMode, char *pchFilename) - perform secure
 *	file transfer from SFTP server.
 *	Parameters:	ushDownloadMode	0=partial, 1=Full
 *				pstVPXfer		ptr to FTP info structure.  If NULL, clear data
 *
 * Return:  0 for no parameter error
 *			< 0 for parameter error
 */
int vpGetFile(int ushDownloadMode, char *pchFilename);


/**
 * int vpAppBase(char *pchUsr) 
 *	Set environment variable to signal system mode to remove application files
 *		and reboot
 *	Parameters:	pchUsr	usrx
 *
 * Return:  n/a
 */
void vpAppBase(char *pchUsr);

/**
 * int vpOSBase(char *pchUsr) 
 *	Set environment variable to signal system mode for OS BASE and reboot
 *	Parameters:	pchUsr	usrx
 *
 * Return:  n/a
 */
 void vpOSBase(char *pchUsr);

/**
* vpGetFileList
*/
int vpGetFileList(vp_filetag_t *pchFiles);

/**
* vpLogMsg - Logs the data pointed to by msg of length msgLength to a temporary file
* in sdram. Once this file reaches a 4k threshold, it is appended to a
* rotating log file in /mnt/flash/userdata/share/
*
* @who[in] two-byte field, first for source, second for state
* @msg[in] pointer to data to be logged
* @msgLength[in] length, in bytes, of data to be logged
* @options[in] one-byte input to specify logging options
* 
* @return 0 on success, <0 on error
*/
int vpLogMsg(char who[2], const char *msg, unsigned int msgLength, unsigned char options);

/**
 * int vpPutFile(char *pchFilename) - perform secure file transfer to SFTP server.
 *	Parameters:	pchFilename		ptr to file to transfer
 *
 * Return:  0 for no parameter error
 *			= -1 for missing SFTP parameter
 *			= -EBUSY (-16), file transfer already taken place
 *			= -ENOENT (-2), specified file not found
 */
int vpPutFile(char *pchFilename);

#ifdef __cplusplus
}
#endif
#endif /* _VP_H */

/// @} */
/// @} */
