#ifndef __SYSM_MINIUSB__
#define __SYSM_MINIUSB__

#include <map>
#include <event_engine/event_listener.h>
#include <event_engine/event_engine.h>
#include <sysmode_ui/sysm_scroll.h>
#include <sysmode_ui/sysm_ui.h>
#include <sysmode_ui/setting.h>
#include <sysmode_ui/settings_page.h>

#include <vector>



typedef struct modprobe_param {
    string key;
    string value;
} t_modprobe_param;

enum {
    EVENT_MINIUSB_LIST  =  1,
    EVENT_MINIUSB_CONFIG,
    EVENT_MINIUSB_RESET = 4
};


class miniusb_config_c : public SYSM_UI::settings_page_c
{
    int              m_changed;
    event_engine_c * m_engine;

    /*
     * modprobe conf file name and some field names within file
     */
    static const char * CFG_FILENAME;
    static const char * OPTIONS_FIELD;
    static const char * XOPTIONS_FIELD;
    static const char * MODULE_NAME;

    /*
     * This is retrieved
     * from file in modprobe.d ,
     */
    vector <t_modprobe_param> m_usb_params;

public:
    miniusb_config_c();
    void display();
    size_t get_params_size() const;

protected:
    void on_event(const event_c *event);
    int modprobe_parser(void);
    int modprobe_update(bool conf_enabled);
};


void net_miniusb_cb(const SYSM_UI::t_ui_menu_entry *e);
#endif
