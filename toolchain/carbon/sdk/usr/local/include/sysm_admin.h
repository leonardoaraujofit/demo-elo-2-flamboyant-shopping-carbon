#ifndef __SYSM_ADMIN__
#define __SYSM_ADMIN__

#include <sysmode_ui/sysm_ui.h>
#include <sysmode_ui/sysm_lang.h>
#include <sysmode_language/sysm_language.h>
#include <sysmode_ui/settings_page.h>
#include <string>
#include <pthread.h>
#include <vector>
#include <unistd.h>
#include <FL/Fl_Select_Browser.H>
#include <stdint.h>
#include <sys/types.h>

extern "C" {
    #include <stdio.h>
    #include <sys/time.h>
    #include <sys/select.h>
    #include "svcInfoAPI.h"
    #include <svc.h>
    #include <svcmgr/svc_net.h>
    #define class class_
    #include <svcmgr/svc_bluetooth.h>
    #undef class
    #include <svcmgr/svc_utility.h>
    #include <svcmgr/svc_sysinfo.h>
    #include <svcmgr/svc_led.h>
    #include <svcmgr/svc_powermngt.h>
    #include "platforminfo_api.h"
    #include <errno.h>
    #include <libsecins.h>
}

extern const int NB_MAX_PACKAGE;

extern const char * ADMIN_IP_STR;
extern const char * ADMIN_SET_IP_STR;
extern const char * ADMIN_NO_BOUNDLES_STR;
extern const char * ADMIN_SOFTWARE_STR;
extern const char * ADMIN_SET_PORT_STR;
extern const char * ADMIN_SET_URL_STR;

void admin_datetime_cb(const SYSM_UI::t_ui_menu_entry *);
void admin_timezone_cb(const SYSM_UI::t_ui_menu_entry *);
void admin_remove_cb(const SYSM_UI::t_ui_menu_entry *);
void admin_battery_cb(const SYSM_UI::t_ui_menu_entry *);

enum {
	PWR_EVENT_LISTENER_LIST = 1,
	PWR_EVENT_LISTENER_APM,
	PWR_EVENT_LISTENER_INACTIVE,
	PWR_EVENT_LISTENER_STBY,
	PWR_EVENT_LISTENER_AUTO,
	PWR_EVENT_LISTENER_AUTOSHUTDOWN,
	PWR_EVENT_LISTENER_AUTOWAKE,
};

class power_conf_c: public SYSM_UI::settings_page_c
{
	struct apm_config m_pwr_config;
	bool m_changed;
public:
	power_conf_c();
	void display();
protected:
	void on_event(const event_c *);
};

	
void admin_pwrmode_cb(const SYSM_UI::t_ui_menu_entry *);
void admin_plane_cb(const SYSM_UI::t_ui_menu_entry *);

enum {
	DISPLAY_EVENT_LISTENER_LIST = 1,
	DISPLAY_EVENT_LISTENER_BRIGHTNESS
};

enum {
	LANG_EVENT_LISTENER_LIST = 1,
	LANG_EVENT_LISTENER_DEF,
	LANG_EVENT_LISTENER_USR
};

class display_conf_c: public SYSM_UI::settings_page_c 
{
public:
	display_conf_c();
protected:
	void on_event(const event_c *);
};

class lang_conf_c: public SYSM_UI::settings_page_c 
{
public:
	lang_conf_c();
protected:
	void on_event(const event_c *);
};

void admin_display_cb(const SYSM_UI::t_ui_menu_entry *);
void admin_lang_cb(const SYSM_UI::t_ui_menu_entry *);


enum {
	SOUND_EVENT_LISTENER_LIST = 1,
	SOUND_EVENT_LISTENER_VOLUME
};

class sound_conf_c: public SYSM_UI::settings_page_c
{
public:
	sound_conf_c();
protected:
	void on_event(const event_c *);
};

void admin_change_volume_cb(const SYSM_UI::t_ui_menu_entry *);

#endif // __SYSM_INFO__
