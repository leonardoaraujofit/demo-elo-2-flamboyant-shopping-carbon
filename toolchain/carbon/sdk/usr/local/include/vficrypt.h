#ifndef _VFI_CRYPT_H_
#define _VFI_CRYPT_H_

#include <stdint.h>
#include <stdio.h>
#include <linux/types.h>
#include "vcl.h"
#include "ade.h"
#include "vf_crypto.h"
#include <verifone/vf_pubsecif.h>

#define MAX_SHA_CONTEXT 256

typedef enum
{
	SHALIB_MD5 = 0,
	SHALIB_SHA1,
	SHALIB_SHA_224,
	SHALIB_SHA_256,
	SHALIB_SHA_384,
	SHALIB_SHA_512
} shaLibAlgo_t;

#define     VFI_CRYPTO_READ     0
#define     VFI_CRYPTO_WRITE    1

#define     TAMPER_TABLE_SIZE   (9*6)

#define MAX_RSA_MODULUS_BITS    4096 
#define MAX_RSA_MODULUS_LEN     ((MAX_RSA_MODULUS_BITS + 7) / 8)
#define MAX_RSA_PRIME_BITS      ((MAX_RSA_MODULUS_BITS + 1) / 2)
#define MAX_RSA_PRIME_LEN       ((MAX_RSA_PRIME_BITS   + 7) / 8)

#define SECURITY_OPAQUE_MODE_FALSE          0   // disable opaque mode
#define SECURITY_OPAQUE_MODE_TRUE           1   // enable opaque mode

#define SECURITY_OPAQUE_KEYTYPE_NONE        0
#define SECURITY_OPAQUE_KEYTYPE_VRK_PAIR    1   // also known as "VRK RSA key pair" or "RSA key pair"
#define SECURITY_OPAQUE_KEYTYPE_VRK         2

#define VAULT_OPAQUE            (0x82)
#define VAULT_VERSION_MAX_SIZE 32

/* RSA key. */
typedef struct
{
    int  pad;
    long version;
    int  KeyLength;
    uint8_t *n;    uint32_t n_bitlen;
    uint8_t *e;    uint32_t e_bitlen;
    uint8_t *d;    uint32_t d_bitlen;
    uint8_t *p;    uint32_t p_bitlen;
    uint8_t *q;    uint32_t q_bitlen;
    uint8_t *dmp1; uint32_t dmp1_bitlen;
    uint8_t *dmq1; uint32_t dmq1_bitlen;
    uint8_t *iqmp; uint32_t iqmp_bitlen;
} RSA_KEY;

typedef struct 
{
    int type;
    int save_type;
    int references;
    union
    {
        char *ptr;
        RSA_KEY *rsa;    /* RSA  */
        RSA_KEY *dsa;    /* DSA  */
        RSA_KEY *dh;     /* DH   */
        RSA_KEY *ec;     /* ECC  */
    } pkey;
    int  save_parameters;
    char *attributes;
} GENKEY;

typedef struct
{
  int  Type;
  int  IDLen;
  char *KeyID;
} KEYTAG;

#define SHA1_TYPE    0
#define SHA256_TYPE  1
#define SHAHMAC_TYPE 2
#define SHA512_TYPE 3

/************************************************************************************************************/
/* FUNCTION NAME:   vfiSec_SYM_CRYPT                                                                        */
/* DESCRIPTION:     Performs general DES/AES operation                                                      */
/* INPUT:           cryptHandle:    Cryptographic handler - Not in use yet                                  */
/*                  encrypt:        VFI_ENCRYPT / VFI_ENCRYPT                                               */
/*                  Mode:           Can be one of the following: VFI_MODE_ECB / VFI_MODE_CBC                */
/*                  keyData:        Pointer to the key                                                      */
/*                  ucKeySize:      Key size in bytes, can be VFI_DES_56/VFI_DES_112/VFI_DES_168            */
/*                                                            VFI_AES_128/VFI_AES_192/VFI_AES_256           */
/*                  auth_key:       Not in use yet                                                          */
/*                  iv:             Pointer to 8/16 bytes initial vector. Used for VFI_MODE_CBC             */
/*                  srcData:        Pointer to the plaintext buffer                                         */
/* OUTPUT:          dstData:        Pointer to the encrypted/decrypted buffer                               */
/* RETURN:          0:              The operation was performed successfully                                */
/*                  <0:             The operation failed                                                    */
/* NOTES:           None                                                                                    */
/************************************************************************************************************/
int vfiSec_SYM_CRYPT( int         cryptHandle,    uint8_t     cryptType,
                                uint8_t     encrypt,        uint8_t     Mode,
                                uint8_t    *keyData,        uint8_t     ucKeySize,
                                uint8_t    *auth_key,       uint8_t    *iv, 
                                uint8_t    *srcData,        uint8_t    *dstData,   unsigned int size);
                                

/*****************************/
/* R S A   F U N C T I O N S */
/*****************************/
/*---------------------------------------------------------------
 * Name    : vfiSec_rsa_mod_exp
 * Purpose : Perform an RSA mod exponentiation
 * Input   : pointers to the data parameters and crypto context
 			 x: the plain text message
 			 m: the modulus
 			 e: the exponent
 * Output  : y: = x**e mod m, y length is find_mod_size(m_bitlen)
 * Return  : Appropriate status
 * Remark  :
 			 If the callback parameter is NULL, the operation is synchronous
 			 The modulus will be round up according to a table
 			 The exponent will be round up to the nearest 32 bits
 			 The exponent and modulus parameter sizes may not be the same
 			 x_len must <= m_len (right?)
 *--------------------------------------------------------------*/
  int vfiSec_rsa_mod_exp (  int cryptHandle,
                                    uint8_t *x, uint32_t x_bitlen,
                                    uint8_t *m, uint32_t m_bitlen,
                                    uint8_t *e, uint32_t e_bitlen,
                                    uint8_t *y, uint32_t *y_bytelen    );

/*---------------------------------------------------------------
 * Name    : vfiSec_rsa_mod_exp_crt
 * Purpose : Perfrom an RSA Chinese Remainder Theorem operation
 * Input   : pointers to the data parameters and crypto context
 			 x: the encrypted message
 			 p
 			 q
 			 edp, pinv: actual bit length must <= p bit length, and padded to p bit length
 			 edq: actual bit length must <= q bit length, and padded to q bit length
 * Output  : y, length is find_mod_size(q_bitlen*2)
 * Return  : Appropriate status
 * Remark  :
 			 Hardware uses pinv, so upper layer code will swap p and q, dp and dq, if they use qinv.
 *--------------------------------------------------------------*/
  int vfiSec_rsa_mod_exp_crt (
                    int cryptHandle,
                    uint8_t *x,    uint32_t x_bitlen,
                    uint8_t *edq,
                    uint8_t *q,    uint32_t q_bitlen,
                    uint8_t *edp,
                    uint8_t *p,    uint32_t p_bitlen,
                    uint8_t *pinv,
                    uint8_t *y,    uint32_t *y_bytelen  );

int  vfiSec_rsa_public_encrypt (
                    int cryptHandle,
                    int flen,
                    const uint8_t *from,
                    uint8_t *to,
                    RSA_KEY *rsa,
                    int padding);


int  vfiSec_rsa_public_decrypt (
                    int cryptHandle,
                    int flen,
                    const uint8_t *from,
                    uint8_t *to,
                    RSA_KEY *rsa,
                    int padding);

int  vfiSec_rsa_private_encrypt(
                    int cryptHandle,
                    int flen,
                    const uint8_t *from,
                    uint8_t *to,
                    RSA_KEY *rsa,
                    int padding);

/**

 FUNCTION   :  vfiSec_rsa_private_decrypt

 DESCRIPTION:  decrypt input buffer with private key.
               First find private key in a private key database, using
               submitted public key,
               after decryption of input buffer, save result in a symmetryc
               key database, generate random key, to search it and output this
               search key as a result

 PARAMETERS : int cryptHandle (in)  - handle of crypto device
              int flen        (in)  - length of input encrypted buffer
              uint8_t *from   (in)  - input encrypted buffer
              uint8_t *to     (out) - output random key to searck decrypted buffer
              RSA_KEY *krsa   (in)  - key (public part) to use in decryption
              int padding     (in)  - flafs



 RETURN     : int length of decrypted buffer - correct
              <=0 decryption error
*/
int  vfiSec_rsa_private_decrypt(
                    int cryptHandle,
                    int flen,
                    const uint8_t *from,
                    uint8_t *to,
                    RSA_KEY *krsa,
                    int padding);

int  vfiSec_rsa_sign(
                    int cryptHandle,
                    int type,
                    const unsigned char *m,
                    unsigned int m_length,
                    unsigned char *sigret,
                    unsigned int *siglen,
                    const RSA_KEY *rsa);

int  vfiSec_rsa_verify(
                    int cryptHandle,
                    int dtype,
                    const unsigned char *m,
                    unsigned int m_length,
                    const unsigned char *sigbuf,
                    unsigned int siglen,
                    const RSA_KEY *rsa);

int vfiSec_rsa_keygen( int  cryptHandle, KEYTAG *KeyTag , GENKEY * Key, int modulus_bit_length,  unsigned long e_value);

int vfiSec_rsa_generic_pub_op ( unsigned char  *msg,
                                unsigned int    msglen,
                                unsigned char  *mod,
                                unsigned int    modlen,
                                unsigned char  *exp,
                                unsigned int    elen,
                                unsigned int    padding,
                                unsigned char *result   );
int pkaRSA(unsigned char *msg, unsigned char *mod, unsigned int mlen, unsigned char *exp, unsigned int elen, unsigned char *result);
/*******************************/
/* LOAD KEY  F U N C T I O N S */
/*******************************/


/*!

    Load general key from vault. Currently only RSA key loading is supported

    \param[in] cryptHandle          Obsolete parameter, ignored
    \param[in] KeyTag               Pointer to key tag structure which identifies key to be loaded
                                    For RSA key:
                                        Field KeyID should point to NULL-terminated string with path to key file.
                                        Other fields are ignored
    \param[inout] Key               Pointer to general key structure where loaded key should be stored
                                    Components of the key will be allocated from heap and should be released by caller.
                                    For RSA key:
                                        Only public RSA components N and E are returned by vault.
                                        Private components do not leave vault for security reasons.
    \return                          1 - Success
                                    -1 - Failure
    \note    General key components should be released by caller even if this method returns with failure
*/
int vfiSec_LoadKey ( int cryptHandle, KEYTAG *KeyTag, GENKEY * Key );
int vfiSec_get_EC_key(char **ec_key, unsigned int *ec_key_len);
int vfiSec_check_private_key(const char *keyfile);
int vfiSec_check_public_key(const char *keyfile);

/*****************************/
/* D S A   F U N C T I O N S */
/*****************************/
/*-------------------------------------------------------------------------------
 * Name    : vfiSec_dsa_sign
 * Purpose : Perform a DSA signature
 * Input   : pointers to the data parameters
 *			 hash: SHA1 hash of the message, length is 20 bytes
 * 			 p: 512 bit to 1024 bit prime, length must be exact in 64 bit increments
 * 			 q: 160 bit prime
 * 			 g: = h**(p-1)/q mod p, where h is a generator. g length is same as p length
 * 			 random: random number, length is 20 bytes, 
                                 generated internally if specified NULL
 * 			 x: private key, length is 20 bytes
 * Output  : rs: the signature, first part is r, followed by s, each is q length
 * Return  : Appropriate status
 * Remark  : If the callback parameter is NULL, the operation is synchronous
 *------------------------------------------------------------------------------*/
  int vfiSec_dsa_sign (
		  int cryptHandle,
                  uint8_t *hash,
                  uint8_t *random,
                  uint8_t *p,      uint32_t p_bitlen,
                  uint8_t *q,
                  uint8_t *g,
                  uint8_t *x,
                  uint8_t *r,     uint32_t *r_bytelen,
                  uint8_t *s,     uint32_t *s_bytelen );

/*---------------------------------------------------------------
 * Name    : vfiSec_dsa_verify
 * Purpose : Verify a DSA signature (private key: x, public key: p, q, g, y.
 * Input   : pointers to the data parameters
 *			 p: 512 bit to 1024 bit prime, in 64 bits increment
 *			 q: 160 bit prime
 *			 g: = h**(p-1)/q mod p, where h is a generator, g length is same as p length
 *           y: = g**x mod p, length is same as p length
 *			 r,s: signature
 *           For parameters q, r, s, they don't have a length field, since it's fixed 20 bytes long.
 * Return  : v. If v == r then verify success
 * Remark  : If the callback parameter is NULL, the operation is synchronous
 *--------------------------------------------------------------*/
  int vfiSec_dsa_verify (
                    int cryptHandle,
                    uint8_t *hash,
                    uint8_t *p,    uint32_t p_bitlen,
                    uint8_t *q,
                    uint8_t *g,
                    uint8_t *y,
                    uint8_t *r,    uint8_t  *s,
                    uint8_t *v,    uint32_t *v_bytelen  );

/***********************************************/
/* D E F F I   H E L M A N   F U N C T I O N S */
/***********************************************/
/*---------------------------------------------------------------
 * Name    : vfiSec_diffie_hellman_generate
 * Purpose : Generate a Diffie-Hellman key pair
 * Input   : pointers to the data parameters
 * 			 x: the secret value, has actual bit length (IN/OUT)
 *			 xvalid: 1 - manual input of private key pointed by x, 
 *			         0 - generate private key internally.
 *			 g: generator, has actual bit length
 *			 m: modulus, has actual bit length
 * Output  : y: the public value. y = g**x mod m, y length is find_mod_size(m_bitlen)
 * Return  : Appropriate status
 * Remark  : If the callback parameter is NULL, the operation is synchronous
 			 g and x are rounded up to 32 bits
 *--------------------------------------------------------------*/
  int vfiSec_diffie_hellman_generate (
                    int cryptHandle,
                    uint8_t *x,      uint32_t x_bitlen,
                    uint32_t xvalid,
                    uint8_t *y,      uint32_t *y_bytelen,
                    uint8_t *g,      uint32_t g_bitlen,
                    uint8_t *m,      uint32_t m_bitlen      );

/*---------------------------------------------------------------------------
 * Name    : vfiSec_diffie_hellman_shared_secret
 * Purpose : Generate a Diffie-Hellman shared secret
 * Input   : pointers to the data parameters
 			 x: the secret value, has actual bit length
 			 y: the other end public value, has actual bit length
 			 m: modulus, has actual bit length
 * Output  : k: the shared secret. k = y**x mod m, k length is find_mod_size(m_bitlen)
 * Return  : Appropriate status
 * Remark  : If the callback parameter is NULL, the operation is synchronous
 *--------------------------------------------------------------------------*/
  int vfiSec_diffie_hellman_shared_secret (
 					 int cryptHandle,
                     uint8_t *x, uint32_t x_bitlen,
                     uint8_t *y, uint32_t y_bitlen,
                     uint8_t *m, uint32_t m_bitlen,
                     uint8_t *k, uint32_t *k_bytelen    );

/*****************************/
/* S H A   F U N C T I O N S */
/*****************************/
  int vfiSec_SHA1_Init(int cryptHandle, SHAcontext *c);
  int vfiSec_SHA1_Update(int cryptHandle, SHAcontext *c, const void *data, unsigned int len);
  int vfiSec_SHA1_Final(int cryptHandle, unsigned char *md, SHAcontext *c);
  unsigned char *vfiSec_SHA1(int cryptHandle, const unsigned char *d, unsigned int n, unsigned char *md);

  int vfiSec_SHA256_Init(int cryptHandle, SHAcontext *c);
  int vfiSec_SHA256_Update(int cryptHandle, SHAcontext *c, const void *data, unsigned int len);
  int vfiSec_SHA256_Final(int cryptHandle, unsigned char *md, SHAcontext *c);
  unsigned char *vfiSec_SHA256(int cryptHandle, const unsigned char *d, unsigned int n,unsigned char *md);

  int vfiSec_SHA512_Init(int cryptHandle, SHAcontext *c);
  int vfiSec_SHA512_Update(int cryptHandle, SHAcontext *c, const void *data, unsigned int len);
  int vfiSec_SHA512_Final(int cryptHandle, unsigned char *md, SHAcontext *c);

  int vfiSec_SHAPrv(int cryptHandle, int action, unsigned char *buffer, unsigned long nb, unsigned char * sha, unsigned int is256);
  int vfiSec_SHA_File(int cryptHandle, const char *pFileName, unsigned char *pDigest,unsigned int is256);
  int vfiSec_SHA(int action, unsigned char *message, unsigned int messageLen, unsigned char * hash,  unsigned int* hashLen, unsigned int hashOp);

  int vfiSec_SHAHMAC_Key(int cryptHandle, SHAcontext *c, const unsigned char *key, unsigned int keyLen);
  int vfiSec_SHAHMAC_Update(int cryptHandle, SHAcontext *c, const void *data, unsigned int len);
  int vfiSec_SHAHMAC_Final(int cryptHandle, unsigned char *md, SHAcontext *c);

/***************************************************************/
/* B A C K W A R D S   C O M P A T I B L E   F U N C T I O N S */
/***************************************************************/
/******************************************************************************
* Function Name:    vfiSec_GetRnd
* Description:      Get a random number pull
* Parameters:       RandomBuf - pointer to output random buffer
*                   length    - length of buffer
* Return Value:     Number of random bytes created
* NOTES:            None
******************************************************************************/
  int vfiSec_GetRnd (int cryptHandle, unsigned int *pRandomBuf, unsigned int length);
  int vfiSec_GetRndEx (int cryptHandle, unsigned char *pRandomBuf, unsigned int length, unsigned int options);
  int vfiSec_GetVaultVersion(unsigned char * pVersion);
  int vfiSec_get_secure_applets_info(char *buf, int bufsize);

  short CRC(void * ptr ,short crc_lg, short seed);
  #define SVC_CRC_CCITT_L_GEN(a,b,c) ((unsigned short)CRC(a,b,c))

  int vfiSec_pkaRSA(unsigned char *msg, unsigned char *mod, unsigned int mlen, unsigned char *exp, unsigned int elen, unsigned char *result);

/***********************************************/
/* S P E C I F I C   F U N C T I O N A L I T Y */
/***********************************************/
/******************************************************************************
* Function Name:    vfiSec_DiagCall
* Description:      Makes a call to the nvusr secureapplet, the API being
*		    defined in nvusr_api.h
* Parameters:       cmd: call to perform
*		    index: index of the nvram to access
*		    v/vp: extra arguments
* Return Value:     0: Operation sucessfull
*		    <0: See nvusr error codes
* NOTES:            None
******************************************************************************/
int vfiSec_DiagCall(unsigned int command, __s16 index, void *vp);


/******************************************************************************
* Function Name:    vfiSec_UpdateSVID
* Description:      Updates the value of the SVID value efuse
* Parameters:       para: Buffer containing the new level and signature
*		    para_len: length of para
*		    sig: p7s signatue of the para buffer
*		    sig_len: length of sig
* Return Value:     0: Operation sucessfull
*		    <0: See lifecycle error codes
* NOTES:            None
******************************************************************************/
int vfiSec_UpdateSVID(void *para, long para_len, void *sig, long sig_len);

/******************************************************************************
* Function Name:    vfiSec_ManToProdSVID
* Description:      Change the value of SVID from 0x0 to 0x1, or Manufacturing to Production
* Return Value:     0: Operation sucessfull
*		    <0: See lifecycle error codes
* NOTES:            None
******************************************************************************/
int vfiSec_ManToProdSVID(void);

/******************************************************************************
* Function Name:    vfiSec_ToNonPayment
* Description:      Convert unit to nonpayment mode
* Return Value:     0: Operation successful
*		    <0: See lifecycle error codes
* NOTES:            None
******************************************************************************/
int vfiSec_ToNonPayment(void);

/*************************************************/
/* T A M P E R I N G   F U N C T I O N A L I T Y */
/*************************************************/
int vfiSec_AttackStatus(int * SecureBarrierFlag, int * TamperStatusReg);

/* Service Switch / Stand Removal Switch Functionality */
int vfiSec_ServiceSwitchControl(int action);

/******************************************************************************
* Function Name:    vfiSec_CryptoReadWrite
* Description:      Reads/Writes encrypted data from/to file
* PARAMETERS:      int rw   - VFI_CRYPTO_READ/VFI_CRYPTO_READ
*                  int pos  - position of the file pointer which is currently handled.
*                  unsigned char *data -output/input buffer for read/write operations
*                  int len  - len of data buffer.
* RETURN VALUE  :   -2 if data == NULL or len < 0
*                   -1 error. 
*                   -5 if CRYPTO_READ and the unit is currently tamper.
*                    >0 Success - number of bytes written or read
* NOTES:            None
******************************************************************************/
int vfiSec_CryptoReadWrite (int rw, int pos, unsigned char *data, int len);

/************************************************************************************************************/
/* FUNCTION NAME:   vfiSec_Set_Opaque_Mode                                                                  */
/* DESCRIPTION:     Sets the Vault Opaque Mode                                                              */
/* INPUT:           uc_opaque_mode: Opaque Mode                                                             */
/* OUTPUT:          None                                                                                    */
/* RETURN:          0               Success                                                                 */
/*                  <0              Failed to pass the request to the Vault device                          */
/* NOTES:           None                                                                                    */
/************************************************************************************************************/
int vfiSec_Set_Opaque_Mode(unsigned char uc_opaque_mode);

/************************************************************************************************************/
/* FUNCTION NAME:   vfiSec_Set_Opaque_Keytype                                                               */
/* DESCRIPTION:     Sets the Vault Opaque Keytype                                                           */
/* INPUT:           opaque_keytype: Opaque Keytype                                                          */
/* OUTPUT:          None                                                                                    */
/* RETURN:          0               Success                                                                 */
/*                  <0              Failed to pass the request to the Vault device                          */
/* NOTES:           None                                                                                    */
/************************************************************************************************************/
int vfiSec_Set_Opaque_Keytype(int opaque_keytype);

/************************************************************************************************************/
/* FUNCTION NAME:   vfiSec_store_tr31_key                                                                   */
/* DESCRIPTION:     Store TR31 Key                                                                          */
/* INPUT:           Buffer containing TR31DATA, Key Data and zulutime                                       */
/* OUTPUT:          None                                                                                    */
/* RETURN:          0               Success                                                                 */
/*                  <0              Failed to pass the request to the Vault device                          */
/* NOTES:           None                                                                                    */
/************************************************************************************************************/
int vfiSec_store_tr31_key(unsigned char *buffer);

/*!

    Store contents of a TR34 blob.

    \param[in] buf                  Pointer to buffer containing TR34 blob
    \param[in] payload_size         Length of TR34 buffer
    \param[out] KBH                 Buffer that KBH retrieved from TR34 blob will be returned in
    \param[out] KBH_len             Length of returned KBH
    \return                          0 - Success
                                    <0 - Failure
*/
int vfiSec_store_tr34_blob(const unsigned char *buffer, const unsigned int len, unsigned char **KBH_buf, uint32_t *KBH_len);

/*!

    Store contents of a TR31 blob. Geobridge/Encrypted Key + CMAC style.

    \param[in] buffer               Pointer to buffer containing TR31 blob
    \param[in] len                  Length of TR31 buffer
    \param[out] KBH                 Buffer that KBH retrieved from TR31 blob will be returned in if not NULL
    \param[out] KBH_len             Length of returned KBH, if not NULL
    \return                          0 - Success
                                    <0 - Failure
*/
int vfiSec_store_tr31_blob(const unsigned char *buffer, const unsigned int len, unsigned char **KBH_buf, unsigned int *KBH_len);


int vfiSec_VAES ( VCLOperation *pOp);
int vfiSec_VAES_crypt(VCLOperation *pOp);

/*!

    Perform Elliptic Curve verification on a message digest.

    \param[in] digest				Pointer to message digest to be verified
    \param[in] digest_len			Length of message digest
    \param[in] DER_sig				Pointer to DER encoded signature of message digest to be verified
    \param[in] DER_SIG_len			Length of DER encoded signature
    \param[in] DER_key				Pointer to DER encoded public key to be used for verification
    \param[in] DER_key_len			Length of DER encoded public key
    \return                          0 - Success
                                    <0 - Failure
*/
int vfiSec_ec_verify(const unsigned char *digest, const int digest_len,
		const unsigned char *DER_sig, const int DER_sig_len,
		const unsigned char *DER_key, const int DER_key_len);


/************************************************************************************************************/
/* FUNCTION NAME:   vfiSec_DeTamper                                                                   */
/* DESCRIPTION:     DeTamper unti.                                                                         */
/* INPUT:           Currently input is not used   (call with   vfiSec_DeTamper(NULL,0)                      */
/* OUTPUT:          None                                                                                    */
/* RETURN:          0               Success                                                                 */
/*                  <0              Failed to pass the request to the Vault device                          */
/* NOTES:           Applicable only for DEV units or Manufacturing units (SVID == 0) or (SVID == 8)         */                                                               
/************************************************************************************************************/
int vfiSec_DeTamper(void * Param,int lparam);
/*Set vault log mode :
 * LogMode can be one of the following: 
 * LOGSYS_MODE_INFO    0x01 - For log info enable
 * LOGSYS_MODE_WARNING 0x02  -For warnnings
 * LOGSYS_MODE_ERROR   0x04 - For log error enable
 * LOGSYS_MODE_DEBUG   0x08 - For debug info
 * LOGSYS_MODE_ALL     0x0F - ALL.
 * the function return privious log mode
 */
int vfiSec_SetVaultLogMode(unsigned int LogMode);

/******************************************************************************
* Function Name:    vfiSec_CreateSessionKey
* Description:      Creates <len> bytes session key and return <len> bytes context
* NOTES:            None
******************************************************************************/
int vfiSec_CreateSessionKey (unsigned char *sessionCtx, unsigned int len);

int vfiSec_HMACCalcSetup(void);
int vfiSec_HMACCalcCompare(void);
int vfiSec_SHAHMAC_Init(int cryptHandle, SHAcontext *c);

int vfiSec_ControlFileAuth(unsigned int action);

/*---------------------------------------------------------------
 * Name    : int vfiSec_ResetAndBootFromUSB
 * Purpose : Reset unit and force boot from DISK ON KEY
 * Input   : NONE
 * Return  : on success - not return 
 *           -1 if fail.
 *--------------------------------------------------------------*/
int vfiSec_ResetAndBootFromUSB(void);



/******************************************************************************
* Function Name:    vfiSec_GetChallenge
* Description:      GetChallenge inside Vault
* Return Value:     0 - Challenge generated successfully
*                   <0 error
* NOTES:            None
******************************************************************************/
int vfiSec_GetChallenge ( char* challenge, size_t challenge_size ) ;

/******************************************************************************
* Function Name:    vfiSec_ExecuteToken
* Description:      Execute token inside Vault
* Return Value:     0 - Token executed successfully
*                   <0 error
* NOTES:            None
******************************************************************************/
int vfiSec_ExecuteToken ( const char* token, const char* token_signature, const char* token_type, int options ) ;

// ADE APIs
int vfiSec_ade_encrypt( ade_encrypt_in* in, ade_encrypt_out* out );
int vfiSec_ade_status( void );
int vfiSec_ade_active( void );
int vfiSec_vf_crypto(vf_crypto_in *in, vf_crypto_out *out);

/*---------------------------------------------------------------
 * Name    : vfiSec_set_vault_rand_method
 * Purpose : Setup Openssl random method that reads from the Vault.
 * Input   : NONE
 * Return  : void
 *--------------------------------------------------------------*/
void vfiSec_set_vault_rand_method(void);
int vfiSec_GetTamperTable(uint8_t *table);
int vfiSec_FileSignOTP(unsigned char *data, unsigned int size, unsigned int resultDataSize, unsigned char* resultData);
int vfiSec_VAES_crypt(VCLOperation *pOp);

int vfiSec_ecdh_compute_key(unsigned char *out,
							unsigned int outlen,
							unsigned char *eckey,
							unsigned int eclen,
							unsigned char *pub,
							unsigned int publen,
							unsigned char *kdf_ukm,
							unsigned int kdf_ukm_len);
int vfiSec_wrap_cipher(unsigned char *out,
					   unsigned char *in,
					   int inlen,
					   unsigned char *iv,
					   unsigned char *key,
					   int keylen,
					   int decrypt);


/** @brief AES Generic Interface
 *
 *
 *  @param[in] cryptHandle			not used.
 *  @param[in] vf_AesDir				0=Encyption 1=Decryption.
 *  @param[in] vf_AesMode					The Aes mode selected (
 *  											VFI_MODE_ECB,
 *  											VFI_MODE_CBC,
 *  											VFI_MODE_CTR,
 *  											VFI_MODE_CBCMAC,
 *  											VFI_MODE_GCM,
 *  											VFI_MODE_CCM,
 *  											VFI_MODE_OFB,
 *  											VFI_MODE_CMAC,
 *  											VFI_MODE_CFB
 *  													  ).
 *  @param[in] keyData				The buffer's address where is stored the main key.
 *  @param[in] vf_KeySize			The main key size ( VFI_AES_128, VFI_AES_192, VFI_AES_256 bits length supported).
 *  @param[in] auth_key				The buffer's address where is stored the auth key (for GCM and CCM).
 *  @param[in] auth_keysize			The auth key bytes size.
 *  @param[in] nonce				The nonce array.
 *  @param[in] nonce_size			The nonce array size.
 *  @param[in] srcData				The buffer's address of the plaintext for encryption.
 *  									or encrtext for decryption.
 *  @param[in] destData				The buffer's address of the encrtext for encryption.
 *  									or plaintext for decryption.
 *  @param[in] srcSize					The bytes size of srcData buffer.
 *  @param[in/out] dstSize			The bytes size pointer of destData buffer.
 *  @param[in/out] auth_tag			The buffer's address of the output tag (for GCM and CCM) or MAC (for CMAC).
 *  @param[in/out] auth_tagsize 	The bytes size of auth_tag. (for VFI_MODE_GCM or  VFI_MODE_CCM tag size may be any one of the following
 *  								 five values: 128, 120, 112, 104, 96, 64 or 32 bits.)
 *
 *  @return int32_t.	not zero is an error.
 */
int vfiSec_AES_generic(int       cryptHandle,
                     uint8_t     vf_AesDir,     uint8_t     vf_AesMode,
                     uint8_t    *keyData,       uint8_t     vf_KeySize,
                     uint8_t    *auth_key,		uint32_t   auth_keysize,
                     uint8_t    *nonce,			uint32_t    nonce_size,
					 uint8_t    *srcData,	 	uint8_t    *dstData,
                     uint32_t   srcSize,	 	uint32_t   *dstSize,
                     uint8_t    *auth_tag,		uint32_t    *auth_tagsize);


#endif /* _VFI_CRYPT_H_ */
