/****************************************************************************
 *
 * @brief   Definitions of the ux100 os library
 *
 * @par		Descrition:
 *          	The ux100 service is made of two libraries:
 * 					- the ux100 os library (low level)
 * 					- the ux100 wrapper library (high level)
 * 				The ux100 wrapper library is based on the ux100 os library. 	
 *
 * @date      	04 Jan. 2013 (creation)
 *
 * @author    	$Author: Benoit Costes $
 *CMD_UX100_KEYBD_BUZZER_INFO
 * @version   	$Revision: 1.00 $
 *
 * @par       	Copyright:
 *            	(c) Copyright Verifone 2013 unpublished work.
 *            	This computer program includes confidential, proprietary
 *            	information and is a trade secret of Verifone. All use,
 *            	disclosure and/or reproduction is prohibited unless
 *            	authorized in writing.
 *            	All rignts reserved.
 *
 ***************************************************************************/

#ifndef __UX100_OS_LIB_H__
#define __UX100_OS_LIB_H__


/* ------------------------ Included files ------------------------------ */
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

/* ------------------------ Public defines ------------------------------ */
#ifndef FALSE
#define FALSE  ((BOOLEAN)0)
#endif
#ifndef TRUE
#define TRUE   ((BOOLEAN)1)
#endif
#ifndef NONE
#define NONE   ((BOOLEAN)2)
#endif


#define MAX_RSA_SIZE 256

/* CMD_GROUP_UX100_SAVELOG ID */
#define CMD_GROUP_UX100_SAVELOG		0x00	// Ux100 SaveLog Group

/* CMD_GROUP_UX100_SAVELOG subgroups */
#define CMD_SLOG_UX100_EVENT  		 0x00
#define CMD_SLOG_UX100_EXTRA   		 0x01

/* CMD_GROUP_UX100_SAVELOG commands (EVENT subgroup) */
#define CMD_SLOG_UX100_EVENT_GET_HDR        0
#define CMD_SLOG_UX100_EVENT_GET_ITEM       1 
#define CMD_SLOG_UX100_EVENT_RECORD_ITEM    2
#define CMD_SLOG_UX100_EVENT_RESET_HDR      3

/* CMD_GROUP_UX100_SAVELOG commands (EXTRA subgroup) */
#define CMD_SLOG_UX100_EXTRA_GET_ATM_SN     0
#define CMD_SLOG_UX100_EXTRA_GET_RTC        1
#define CMD_SLOG_UX100_EXTRA_SET_RTC        2

/* For Savelog */
#define SLOG_COUNTER_SIZE     4
#define SLOG_COUNTER_NB       64

#define MAX_EVENTNAME_SIZE   9
#define ATMEL_SN_SIZE        9
#define SAVE_LOG_HDR_LEN 	sizeof(SAVE_LOG_HDR)  /* 10 */
#define SAVE_LOG_ITEM_LEN   sizeof(SAVE_LOG_ITEM) /* 16 */

/* UX100 Group ID */
#define CMD_GROUP_UX100       0x04   // Ux100 group

#define MAX_MSG_LEN_MULTI8 240

#define CMD_ENCRYTPTED 0
#define CMD_PLAINTEXT  1

/* CMD_GROUP_UX100 commands */
#define     CMD_UX100_START_PIN_ENTRY	0       /* Ux100 starts to get PIN by setting Keybd mode to "PIN entry mode"*/
#define     CMD_UX100_GET_PIN_BLOCK     1       /* Ux100 Gets Enc PINblock*/
#define     CMD_UX100_DELETE_PIN        2       /* Delete PIN digits of all or of n entered */
#define     CMD_UX100_ABORT_PIN         3       /* Abort PIN entry */
#define     CMD_UX100_SET_ID_CARD       4       /* Set Ux100 ID CARD info*/
#define     CMD_UX100_BUZZER_INFO       5       /* Ux100 Gets Buzzer Info*/
#define     CMD_UX100_GET_ID_CARD       6       /* Get Ux100 ID CARD info */
#define		CMD_UX100_INIT_DATA_LINK	7		/* Setup ciphered link parameters */
#define		CMD_UX100_RESET_DATA_LINK	8		/* Reset ciphered link parameters */
#define     CMD_UX100_GET_PIN_MODE      9       /* Get Ux100 PIN mode info */
#define     CMD_UX100_GET_PIN_DIGIT_NB  10       /* Get Ux100 entered PIN digit number info */
#define     CMD_UX100_GET_UNIT_INOF_UX1X0  11   /* Get unit type Ux100/110 info*/
#define     CMD_UX100_KEYBD_BUZZER_INFO 12       /* Ux100 Gets Buzzer Info*/
#define		CMD_UX100_TAMPER_NUM_ENTRY	13		/* (De)activate numeric entry in tamper mode */
#define     CMD_UX100_SET_KEBD_BUZZER	14		 /* UX100 Set the buzzer param when a key is pressed */	
#define     CMD_UX100_START_PW_ENTRY	15      /* Ux100 starts to get PW by setting Keybd mode to "PW entry mode"*/
#define     CMD_UX100_GET_PW_BLOCK      16      /* Ux100 Gets Enc PWblock*/
#define     CMD_UX100_DELETE_PW         17      /* Delete PW digits of all or of n entered */
#define     CMD_UX100_ABORT_PW          18      /* Abort PW entry */
#define     CMD_UX100_GET_PW_DIGIT_NB   19      /* Get Ux100 entered PW digit number info */
#define     CMD_UX100_GET_PW_MODE   	20      /* Get Ux100 PW mode info */

/* Flash slot types */

#define T_PUBKEY_ROOT					0x00	/* Top level certification public key   	*/
#define T_PUBKEY_PRIMARY				0x02	/* Firmware authentication key         		*/
#define T_PUBKEY_CUST_AUTH				0x22	/* Public, PINPad Authentication key   		*/
#define T_PUBKEY_CTRL_CUST_AUTH			0x29	/* Public, Controller Authentication Key    */
#define T_PUBKEY_PED_ENC				0x0F	/* Public, PINPad Encryption Key        	*/
#define T_PUBKEY_CTRL_ENC				0x2A	/* Public, Controller Encryption Key		*/

#define T_PUBKEY_PRIMARY_CERT			0x12	/* Firmware authentication key certificate	*/
#define T_PUBKEY_CUST_AUTH_CERT			0x24	/* PINPad Authentication key certificate 	*/
#define T_PUBKEY_CTRL_CUST_AUTH_CERT	0x39   	/* Controller Auth Key certificate          */
#define T_PUBKEY_PED_ENC_CERT			0x26	/* PINPad Encryption Key certificate 		*/
#define T_PUBKEY_CTRL_ENC_CERT			0x3A	/* Controller Encryption Key certificate	*/

#define T_PRIVKEY_PED_ENC				0x21   	/* Private, PINPad Encryption Key           */

/* -- Content Type Definitions - ENCRYPTED BY TRK -- */

#define T_SYSTEM_MK           0x62      /*!< MK system                          */


/* PP authentication specific KeyIDs */
#define PKEK_PP_KEY_ID      0x00
#define CAK_PP_KEY_ID       0x01
#define PAK_PP_KEY_ID       0x02
#define USBK_PP_KEY_ID  	0x03
#define ARSK_PP_KEY_ID  	0x04

/* keyboard mapping */
#define N_COLUMN                4
#define N_ROW                   4

/* Secure processor SN size */
#define PINPAD_SN_SIZE      10
#define PINPAD_PN_SIZE      4
#define PINPAD_PCI_SIZE     3
#define PINPAD_MODEL_SIZE   2

#define SECURE_IDCODE_SIZE 50
#define SECURE_FAMILY_SIZE 10
#define SECURE_ID_SIZE  12

/* Ux100 ID Card info */
#define UX100_PN_SIZE              32   /*!< Maximum length of part number */
#define UX100_SN_SIZE              11   /*!< Maximum length of serial number */
#define UX100_HW_VER_SIZE          15   /*!< Maximum length of hardware version */
#define UX100_PCI_HW_VER_SIZE      4    /*!< Maximum length of PCI hardware version */
#define UX100_MN_SIZE              12   /*!< Maximum length of model number */
#define UX100_CN_SIZE              12   /*!< Maximum length of Country name */
#define UX100_PTID_SIZE            10   /*!< Maximum length of Permanent Terminal ID */


/* used for antiremoval switch */
#define Ux100_RS_CHALLENGE_SIZE		8

/* User IDs*/
#define USER_ID_ROOT	0
#define USER_ID_USR1	500
#define USER_ID_SYS1	600
#define USER_ID_SYS16	615

/* -------------------- Public types and enums -------------------------- */

/* Secure processor general status code type */
typedef enum
{
   SEC_UX100_CODE_OK = 0,
   SEC_UX100_CODE_BAD_SIZE = 1,
   SEC_UX100_CODE_BAD_FORMAT = 2,
   SEC_UX100_CODE_BAD_DEVICE = 3,
   SEC_UX100_CODE_DEV_ERROR = 4,
   SEC_UX100_CODE_DEV_BUSY = 5,
   SEC_UX100_CODE_FCT_UNKNOWN = 6,
   SEC_UX100_CODE_FCT_PARAM = 7,
   SEC_UX100_CODE_NO_ANSWER = 8,
   SEC_UX100_CODE_IO_ERROR = 9,
   SEC_UX100_CODE_GEN_ERROR = 10,
   SEC_UX100_CODE_BAD_CRC = 11,
   SEC_UX100_CODE_NO_KEY = 12,
   SEC_UX100_CODE_BAD_KEY = 13,
   SEC_UX100_CODE_KEY_DISABLED = 14,
   SEC_UX100_CODE_KEY_MEMORY_FULL = 15,
   SEC_UX100_CODE_KEY_PRESENT = 16,
   SEC_UX100_CODE_KEY_NOT_PRESENT = 17,
   SEC_UX100_CODE_KEY_BAD_MAC = 18,
   SEC_UX100_CODE_KEY_VERIF_NOK = 19,
   SEC_UX100_CODE_KEY_ERROR = 20,
   SEC_UX100_CODE_KEY_EXPIRED = 21,
   SEC_UX100_CODE_KEY_RELOAD = 22,
   SEC_UX100_CODE_AKNOLEDGED = 23,
   SEC_UX100_CODE_KEY_CHLNG_ERROR = 24,
   SEC_UX100_CODE_KEY_COUNTER_ERR = 25,
   /* MAC status */
   SEC_UX100_CODE_MAC_OK = 26,   /* MAC generation or verification OK */
   SEC_UX100_CODE_CRYPTO_NOK = 27,       /* Cryptogram failed */
   SEC_UX100_CODE_KEY_INACTIVE = 28,
   SEC_UX100_CODE_KEY_CONFLICT = 29,

/*for all prompt functions */
   SEC_UX100_CODE_ERROR = 40,    /*internal error/parsing error */
   SEC_UX100_CODE_ERROR_NONE = 41,       /*no error */
   SEC_UX100_CODE_PROMPT_NOT_PRESENT = 42,

/*prompt display functions status*/
   SEC_UX100_CODE_BAD_PARAM = 43,        /*missing parameters in list */
   SEC_UX100_CODE_CANCEL = 44,   /*prompt entry was canceled by user pressing CANCEL key */
   SEC_UX100_CODE_TIMEOUT = 45,  /*either first or inter character timeout elapsed */
   SEC_UX100_CODE_ALPHANUM_TIMEOUT = 46,
   SEC_UX100_CODE_FKEY = 47,     /*a function key was pressed */
   SEC_UX100_CODE_ABORTED = 48,  /*the prompt entry was aborted */
   SEC_UX100_CODE_BUSY = 49,     /*a prompt entry is already in progress */
   SEC_UX100_CODE_NO_PROMPT = 50,        /*no prompt was currently displayed */
   SEC_UX100_CODE_GLOBAL_ERROR = 51,
   SEC_UX100_CODE_SPECIAL_CHAR_AUTHORIZED = 52,  /*special characters authorized such as phone char for exple */
   SEC_UX100_CODE_OUT_OF_ORDER = 53,
   
   /* Ux100 functions status */
   SEC_UX100_CODE_KEY_TIMEOUT = 54,    /* Key press timeout */
   SEC_UX100_CODE_PIN_TIMEOUT = 55,    /* PIN entry timeout */
   SEC_UX100_CODE_SEC_MODE_ERR = 56,   /* Get PIN block but under no PIN mode */
   SEC_UX100_CODE_MUST_DEL_KEY_ERR = 57, /* It must do 'delete PIN' after a COR key */
   SEC_UX100_CODE_INVALID_PIN_LEN = 58,  /* VAL pressed before PIN lngth => MIN for PIN block*/
                                        /* Invalide PIN length for Start PIN */
   SEC_UX100_CODE_INVALID_KEY_PRESS = 59, /* An invalid key pressed */
   SEC_UX100_CODE_PIN_BYPASS = 60, /* PIN BYPASS */

   /* ux100 <==> ux300 status code */
   SEC_UX100_CODE_MUTUAL_AUTHEN_ERR = 70, /* Mutual Authentication Error */
   SEC_UX100_CODE_VAULT_GEN_PIN_PW_KEY_ERR = 71,  /* Vault Generating PIN/PW key error */
   SEC_UX100_CODE_PIN_PW_BLK_TO_VAULT_ERR = 72,  /* Load PIN/PW Block to Vault error */   
   
   /* keyboard status code */
   SEC_UX100_CODE_KBD_ERR = 80,
   SEC_UX100_CODE_KBD_INIT_FAILED = 81,
   SEC_UX100_CODE_KBD_PARAM_ERR = 82,
   
   /* display status code */
   SEC_UX100_CODE_DISP_ERR = 90, 
   
   /* buzzer status code */
   SEC_UX100_CODE_BUZZER_ERR = 100, 
   
   SEC_UX100_CODE_UNKNOWN_ERR  = 0xFF,
} SEC_UX100_STATUS;

/* __CHAR_UNSIGNED__ is set by CPP according to gcc command line options */
#ifdef __CHAR_UNSIGNED__
typedef char uint8;
typedef signed char int8;
#else
typedef unsigned char uint8;
typedef char int8;
#endif

typedef signed short int16;
typedef unsigned short uint16;

typedef signed long int32;
typedef unsigned long uint32;

typedef signed long long int64;
typedef unsigned long long uint64;

typedef uint16 BOOLEAN;

typedef struct
{
   uint8 bOwnerID;
   uint8 bKeyID;
   uint8 bUsage;
   uint8 bLength;
   uint8 abKey[1];
} __attribute__ ((packed)) TMK_MK_TYPE;

typedef struct
{
   uint32 uKeyID;               /* Key unique identifier     */
   uint32 uExponent;            /* Public key exponent       */
   uint16 wModuloLength;        /* Public key modulo         */
   uint8 tbModulus[1];          /* Public key modulus        */
} __attribute__ ((packed)) PUBKEY_TYPE;

typedef struct
{
   uint32 uCertifiedID;         /* Certified key ID         */
   uint32 uCertifyingID;        /* Certifying key ID        */
   uint32 uValidFrom;           /* Valid from ddmmyyyy      */
   uint32 uValidTo;             /* Valid to ddmmyyyy        */
   uint16 wSignatureSize;       /* Signature size in bytes  */
   uint8 tbSignature[1];        /* Certificate data         */
} __attribute__ ((packed)) PUBCERT_TYPE;

typedef struct
{
   uint32 uKeyID;               /* Key unique identifier     */
   uint16 wPrivExpLength;       /* Private Exponent length   */
   uint8 tbPrivExp[1];          /* Private Exponent          */
} __attribute__ ((packed)) PRIVKEY_TYPE;

typedef struct
{			
   uint8 tbSha256[32];      /* SHA-256 hash*/												
   uint8 bHashType;    			/* Type of additional Hash   */	 	
   uint8 bKeySlot; 		        /* Key usage                 */	
   uint8 bByte0;                /* Zero byte  				 */
   uint8 bRDT1;                 /* Recover data trailer ABh  */
   uint32 uCertifiedID;         /* Certified key ID          */
   uint32 uCertifyingID;        /* Certifying key ID         */
   uint32 uValidFrom;           /* Valid from ddmmyyyy       */
   uint32 uValidTo;             /* Valid to ddmmyyyy         */
   uint32 uExponent;            /* Public key exponent       */
   uint8 bRDT2;                 /* Recover data trailer CDh  */
   uint8 tbSha1[20];         /* SHA-1 hash  (Deprecated, replaced by SHA-256) */
} __attribute__ ((packed)) SIGNATURE_TYPE;

/* Slot allocation table (SAT) structure - Slot 0 */
typedef struct
{
   uint8 bVersion;
   uint16 wMagicWord;
   uint16 wNumBlocks;
   uint16 wFreeBlocks;
   uint8 bNumSlots;
   uint8 bFreeSlots;
   uint8 bRFU[1];
}  __attribute__ ((packed)) SAT_SLOT0;

/* Slot allocation table (SAT) structure - Slot n */
typedef struct
{
   uint8 bContentType;
   uint8 bOwnerID;
   uint16 wCrc;
   uint32 uKeyID;
   uint16 wStartBlock;
} __attribute__ ((packed)) SAT_SLOTN;

typedef struct __attribute__ ( ( __packed__ ) )
{
    unsigned int currentFrequency;	// current frequency of the buzzer
    unsigned int currentTime;		// current time (duration) of the buzzer
    unsigned int currentVolume;		// current volume of the buzzer	
} BUZZER_INFO_STRUCT;

/**
 * @brief Display information structure
 * @ingroup OSDISP
 */
typedef struct __attribute__ ( ( __packed__ ) )
{
   /** Current font ID */
   uint16 wFontID;
   /** Maximum number of lines (with current font) */
   uint8 bMaxLines;
   /** Maximum number of columns (with current font) */
   uint8 bMaxColumns;
   /** Current font character width in pixels */
   uint8 bCharWidth;
   /** Current font character height in pixels */
   uint8 bCharHeight;
   /** ASCII offset of first character in current font */
   uint8 bFirstChar;
   /** Number of ASCII character in current font */
   uint8 bNbChar;
   /** Display type (see DISPLAY_TYPE) */
   uint8 bDispType;
   /** Width of the screen in pixels - only for graphic display */
   uint8 bScreenWidth;
   /** Heigth of the screen in pixels  - only for graphic display */
   uint8 bScreenHeight;
   /** Current contrast setting of the display */
   uint8 bCurrentContrast;
   /** Minimum contrast setting for the display */
   uint8 bMinContrast;
   /** Maximum contrast setting for the display */
   uint8 bMaxContrast;
   /** Current backlight setting of the display */
   uint8 bCurrentBacklight;
   /** Minimum backlight setting for the display */
   uint8 bMinBacklight;
   /** Maximum backlight setting for the display */
   uint8 bMaxBacklight;
   /** Current backlight timer in ms */
   uint32 uCurrentBklDelay;
   /** Current horizontal position of the cursor */
   uint8 bCursorCol;
   /** Current vertical position of the cursor */
   uint8 bCursorRow;
} DISPLAY_INFO_STRUCT;

/**
 * @ingroup UX100KEYBD
 *@brief Refer to key values
 */
typedef enum
{
   UX100_KEY_NONE = 0x00,             /*!< No keys */
   UX100_KEY_1 = 0x01,                /*!< key code 1 */
   UX100_KEY_2 = 0x02,                /*!< key code 2 */
   UX100_KEY_3 = 0x03,                /*!< key code 3 */
   UX100_KEY_4 = 0x04,                /*!< key code 4 */
   UX100_KEY_5 = 0x05,                /*!< key code 5 */
   UX100_KEY_6 = 0x06,                /*!< key code 6 */
   UX100_KEY_7 = 0x07,                /*!< key code 7 */
   UX100_KEY_8 = 0x08,                /*!< key code 8 */
   UX100_KEY_9 = 0x09,                /*!< key code 9 */
   UX100_KEY_0 = 0x0A,                /*!< key code 0 */
   UX100_KEY_DOWN = 0x0B,             /*!< key code star */
   UX100_KEY_UP = 0x0C,               /*!< key code 00 */
   UX100_KEY_STOP = 0x0D,           /*!< key code stop */
   UX100_KEY_CORR = 0x0E,            /*!< key code correction */
   UX100_KEY_INFO = 0x0F,            /*!< key code info */
   UX100_KEY_OK = 0x10,             /*!< key code ok */
   UX100_KEY_ENCRYPT = 0x11,          /*!< key code encrypt */
   UX100_KEY_INVALID = 0x12,          /*!< key code invalid */
   /* Following definitions are to return back key press status for PIN mode */
   UX100_PINMODE_KEY_STATUS_COR_PRESSED = 0x21,   /*!< In PIN mode, COR is pressed, it must do Delete PIN before all  */
   UX100_PINMODE_KEY_STATUS_PIN_TIMEOUT = 0x22,   /*!< In PIN mode, timeout occured for PIN session */
   UX100_PINMODE_KEY_STATUS_VAL_PINLEN_LESS_MIN = 0x23,   /*!< In PIN mode, VAL is pressed before PIN Len >= MIN */
   UX100_PINMODE_KEY_STATUS_NUM_PINLEN_GREAT_MAX = 0x24,   /*!< In PIN mode, Numeric Key is pressed after PIN len = MAX */
}UX100_KEY_TYPE;

/** 
   @ingroup SVC_UX100_SECURITY_API
   @brief   PINPAD Secutity Status definition
*/
typedef enum
{
   /** The TRK (Terminal Random Key) customer PRIMARYpk and PED_ENCca are present. All sensitive functions are allowed */
   SP_UNLOCKED,
   /** The TRK (Terminal Random Key) is present, but customer PRIMARYpk is not present. Sensitive features (i.e.:PIN) not allowed */
   SP_TEST,
   /** The TRK (Terminal Random Key) is not present. Tampered or not initialized */
   SP_TAMPERED
} SP_STATUS;

/** 
   @ingroup SVC_UX100_SECURITY_API
   @brief   PINPAD Operational Mode definition
*/
typedef enum
{
	MODE_FIELD = 0,				// Field mode
	MODE_PROD1 = 1,				// Production mode #1
	MODE_PROD2 = 2,				// Production mode #2
	MODE_RESTR = 3,				// Restricted mode
	MODE_PERSO = 4				// Controller Personalization Mode
} OP_MODE;

typedef uint8 ROW_TYPE[N_COLUMN];
typedef ROW_TYPE KEY_MAP[N_ROW]; 

/**
 *@brief Keyboard information structure
 *@ingroup UX100KEYBD
 */
typedef struct __attribute__ ( ( __packed__ ) )
{
   // Is the terminal in secure mode ? 
   uint8 bSecureMode;
   // Keystroke beep frequency (0 = desactivated)
   uint16 wBeepFrequency;
   // Number of function keys on the keyboard
   uint8 bFunctionKeyNb;
   // Number of normal keys on the keyboard 
   uint8 bNormalKeyNb;
   // Number of normal key rows 
   uint8 bRowNb;
   // Number of normal key columns
   uint8 bColNb;
   // Keyboard map 
   KEY_MAP stKeyMap;
   /* Keystroke beep duration in milli second (0 = desactivated) */
   uint16 wBeepTime;   
} KEYBD_INFO_STRUCT;

/* Console information structure */
typedef struct __attribute__ ( ( __packed__ ) )
{
   DISPLAY_INFO_STRUCT stDispInfo;
   KEYBD_INFO_STRUCT stKeybdInfo;
} LOCAL_CONSOLE_INFO;

/** Pinpad IDCARD */
typedef struct
{
   uint8 abPN[PINPAD_PN_SIZE+1];     /**< Pinpad part number */
   uint8 abSN[PINPAD_SN_SIZE+1];     /**< Pinpad serial number */
   uint8 abPCI[PINPAD_PCI_SIZE+1];   /**< Pinpad PCI ID */
   uint8 abMODEL[PINPAD_MODEL_SIZE+1]; /**< Pinpad model (P3D, P5D) */
} __attribute__ ( ( packed ) ) PINPAD_IDCARD;

/** Ux100 IDCARD */
typedef struct
{
   uint32 dwDM;                                 /**< UX100 device mode */
   uint8 abPN[UX100_PN_SIZE+1];                /**< UX100 part number */
   uint8 abSN[UX100_SN_SIZE+1];                /**< UX100 serial number */
   uint8 abHWVER[UX100_HW_VER_SIZE+1];         /**< UX100 hardware version */
   uint8 abPCIHWVER[UX100_PCI_HW_VER_SIZE+1];  /**< UX100 PCI hardware version */
   uint8 abMN[UX100_MN_SIZE+1];                /**< UX100 Model number */
   uint8 abCN[UX100_CN_SIZE+1];                /**< UX100 Contry name */
   uint8 abPTID[UX100_PTID_SIZE+1];            /**< UX100 Permanent Terminal ID*/
} __attribute__ ( ( packed ) ) UX100_IDCARD;


typedef struct __attribute__ ( ( __packed__ ) )
{
   /** Firmware package IDCODE for terminal management server */
   uint8 abIdCode[SECURE_IDCODE_SIZE];       /** Security familly (PCI, INTERAC, others...) */
   uint8 abFamily[SECURE_FAMILY_SIZE];      /** PCI or equivalent certficiation ID */
   uint8 abSecureId[SECURE_ID_SIZE];
} SECURE_INFO_STRUCT;

typedef uint32 TARGET_MASK_TYPE; /** < Mask type */

/* Log Events IDs */
typedef enum
{
	LOGEVT_RECORD_EVENT					= 0x00,
	LOGEVT_LOAD_FIRMWARE				= 0x01,
	LOGEVT_LOAD_PUBLIC_KEY 				= 0x02,
	LOGEVT_GENERATE_RSA_KEYS			= 0x03,
	LOGEVT_LOAD_PUBLIC_KEY_CERT			= 0x04,
	LOGEVT_MUTUAL_AUTH_STATUS			= 0x05,
	LOGEVT_INTEGRITY_CHECK				= 0x06,
	LOGEVT_ANTI_REMOVAL_SWITCH_STATE	= 0x07,
	LOGEVT_ANTI_REMOVAL_SWITCH_RESET	= 0x08,
	LOGEVT_SEC_SENSORS_ALARM			= 0x09,
	LOGEVT_SEC_SENSORS_RESET			= 0x0A,
	LOGEVT_ENVIRONMENTAL_ALARM			= 0x0B,
	LOGEVT_CHANGE_PARAMETERS			= 0x0C,
	LOGEVT_COMMUNICATION_ERROR			= 0x0D,
	LOGEVT_SOFTWARE_ERROR				= 0x0E,
	LOGEVT_HARDWARE_ERROR				= 0x0F,
	LOGEVT_CTRL_EVENT					= 0x10,
} LOG_EVT_ID;

typedef struct __attribute__ ( ( __packed__ ) ) /* Event Zone Header. 10 Bytes */
{
	uint32  uMagICWord;
	uint16  wTotalItemNb;
	uint16  wCurrentItemNb;
	uint16  wOverWriteFlag;
} SAVE_LOG_HDR; 

typedef struct __attribute__ ( ( __packed__ ) ) /* Event Item. 16 Bytes */ 
{
	uint8    bDay;      //Time when Event takes place
	uint8    bMonth;
	uint8    bYear;
	uint8    bHour;
	uint8    bMinute;
	uint8    bEvtID;
	uint8    bStatus;
	uint8    pbEventName[MAX_EVENTNAME_SIZE]; //Event Information
} SAVE_LOG_ITEM;

typedef struct __attribute__ ( ( __packed__ ) ) /* Key or CRT information */
{
	uint8 bType;
	uint32 uID;
} SAVE_LOG_KEY;

/* Diagnostic Counter Subcommands */
typedef enum
{
	UX100_DIAG_CNT_GET = 0,
	UX100_DIAG_CNT_SET = 1
} UX100_DIAG_CNT_CMD;

/* UX100 Diagnostic Counter */
typedef struct __attribute__ ( ( __packed__ ) )
{
	char text[17];
	unsigned int flags;
	int value;
	short index;
} UX100_DIAG_CNT;

typedef struct
{
	int attached;	// UX1 device is attached
	int bus_num;	// USB bus number, where the UX1 is attached
	int vid;		// Vendor ID of attached device
	int pid;		// Product ID of attached device
	int type;		// UX1 model (0-unknown, 100-UX100, 110-UX110)
	int parent_vid;	// Parent Vendor ID of attached device
	int parent_pid;	// Parent Product ID of attached device
} UX1_DEV_INFO;

 /* ----------------- Public constants and variables --------------------- */

 /* ------------------ Public functions declaration ---------------------- */

void set_ux100_errno( SEC_UX100_STATUS status );

/* For Ux100 control */
SEC_UX100_STATUS ppRefresh( );
SEC_UX100_STATUS ppReset( );
SEC_UX100_STATUS ppHeaterBoost( );
SEC_UX100_STATUS ppHeaterMode( int mode );

/* For Ux100 security */
SEC_UX100_STATUS ppSecGetE2PROMKeySlot( unsigned char f_bSlot, unsigned char * f_pbContentType, unsigned char * f_pbStatus, uint16 * f_pwInfoLen, unsigned char * f_pbInfo );
SEC_UX100_STATUS ppSecGetSPStatus( uint8 * f_pbSPStatus, uint8 * f_pbOpMode, int * f_puUKSR, int * f_uSECIRQ, int * f_puUKSRCur, int * f_uSECIRQCur );
SEC_UX100_STATUS ppSecGetSPMode( char  * f_bMode ) ;
SEC_UX100_STATUS ppSecLoadCertificate( int f_bCertType, uint16 f_wCertLength, unsigned char * f_pbCertificate );
SEC_UX100_STATUS ppSecLoadCertificateMaster( unsigned char f_bCertType, uint16 f_wCertLength, unsigned char * f_pbCertificate );
SEC_UX100_STATUS ppSecLoadPublicKey( int f_bKeyType, uint16 f_wPubKeyLength, unsigned char * f_pbPublicKey );
SEC_UX100_STATUS ppSecGetCertif( int f_bType, uint16 * f_pwCertLength, unsigned char * f_pbCertificate );
SEC_UX100_STATUS ppSecGetPublicKey( int f_bType, uint16 * f_pwPubKeyLenth, unsigned char * f_pbPubKey );
SEC_UX100_STATUS ppSecSetSecureDate( uint32 f_uSecureDate );
SEC_UX100_STATUS ppSecGenerateRSAKeyPair( unsigned char f_bType, uint32 f_uExponent, uint16 f_wSize, int f_bForceRenew );
SEC_UX100_STATUS ppSecGetRandom( unsigned char * f_pbBuffer, uint16 f_wLength );
SEC_UX100_STATUS ppSecMutualAuthentication( unsigned char f_bStep, unsigned char* f_pbDataIn, uint16 f_wDataInLen, unsigned char* f_pbDataOut, uint16* f_pwDataOutLen );
SEC_UX100_STATUS ppSecPerformParing( int forceRenewal, int * errType );
SEC_UX100_STATUS ppSecAuthResolveChlng( uint8 * f_pbChallenge, uint8 * f_pbeWK, uint16 f_wKeyLen );
SEC_UX100_STATUS ppSecAuthGenerateChlng( uint8 * f_pbChallenge, uint8 * f_pbeWK, uint16 * f_wKeyLen );
SEC_UX100_STATUS ppSecAuthVerifyChlng( uint8 * f_pbChallenge );
SEC_UX100_STATUS ppSecAuthPPStatus( void );
SEC_UX100_STATUS ppInitCipheredLink( void );
SEC_UX100_STATUS ppInitCipheredLinkPM( uint8 * f_pbParameters, uint16 f_wSize );
SEC_UX100_STATUS ppResetCipheredLink( void );
SEC_UX100_STATUS ppSecTamperNumEntHandl( uint8 f_bType, uint8 * f_pbStatus );

/* For Ux100 PIN */
SEC_UX100_STATUS  ux100PinStartPinEntry( unsigned char  pinLenMin, unsigned char pinLenMax, int timeout );
SEC_UX100_STATUS  ux100PinGetEnteredDigitsNb( int *nbEnteredDigit );
SEC_UX100_STATUS  ux100PinDelete( int nbDigit );
SEC_UX100_STATUS  ux100PinAbort( void );
SEC_UX100_STATUS  ux100PinSendEncPinBlockToVault( void );                        
SEC_UX100_STATUS  ux100PinMode( int *pinMode );

/* For Ux100 PW */
SEC_UX100_STATUS  ux100PwStartPwEntry( unsigned char  pwLenMin, unsigned char pwLenMax, int timeout );
SEC_UX100_STATUS  ux100PwGetEnteredDigitsNb( int *nbEnteredDigit );
SEC_UX100_STATUS  ux100PwDelete( int nbDigit );
SEC_UX100_STATUS  ux100PwAbort( void );
SEC_UX100_STATUS  ux100PwSendEncPwBlockToVault( void );                        
SEC_UX100_STATUS  ux100PwMode( int *pinMode );

/* For Ux100 Buzzer */
SEC_UX100_STATUS ppBuzzerSetBeep( unsigned int frequency, unsigned int time, unsigned int volume );
SEC_UX100_STATUS ppBuzzerSetKeyboardBeep(unsigned int frequency, unsigned int time, unsigned int volume);
SEC_UX100_STATUS ppBuzzerGetInfo( BUZZER_INFO_STRUCT *pBuzzerInfo_ptr );
SEC_UX100_STATUS ppKeybdBuzzerGetInfo( BUZZER_INFO_STRUCT *pKeybdBuzzerInfo_ptr );

/* For Ux100 display */ 
SEC_UX100_STATUS ppDisplayInit( void );
SEC_UX100_STATUS ppDisplayFlush( void );
SEC_UX100_STATUS ppDisplayCursorPos( uint16 line, uint16 column );
SEC_UX100_STATUS ppDisplayAbsoluteCursorPos( uint16 line, uint16 xpos );
SEC_UX100_STATUS ppDisplayInfo( DISPLAY_INFO_STRUCT * info_ptr );
SEC_UX100_STATUS ppDisplayBitmap( uint16 line, uint16 column, uint8 BitmapWidth, uint16 BitmapSize, const uint8 * BitmapPtr );
SEC_UX100_STATUS ppDisplaySetContrast( uint8 contrast );
SEC_UX100_STATUS ppDisplayIcon( uint8 iconId, BOOLEAN enabled );
SEC_UX100_STATUS ppDisplayDefineNewPromptFont( uint8 f_bSlot, const uint8 * f_pFont, uint32 f_wSize );
SEC_UX100_STATUS ppDisplayAuthFont( uint8, const uint8 * FontPtr, uint32 Size );
SEC_UX100_STATUS ppDisplaySetBacklightMode ( int mode, unsigned int time );

/* For Ux100 keyboard */ 
SEC_UX100_STATUS ppKeybdInit( void );
SEC_UX100_STATUS ppKeybdWatchKey( UX100_KEY_TYPE * key, uint32 timeout );
SEC_UX100_STATUS ppKeybdLength( uint16 * size );
SEC_UX100_STATUS ppKeybdFlush( void );
SEC_UX100_STATUS ppKeybdPressed( uint32 timeout );
SEC_UX100_STATUS ppKeybdInfo( KEYBD_INFO_STRUCT * info_ptr );
SEC_UX100_STATUS ppKeybdGetKey( int timeout, unsigned char * key );
SEC_UX100_STATUS ppKeybdFilterControl( int on_off_flag );
SEC_UX100_STATUS ppKeybdInputControl( int lock_flag );

/* For Ux100 information */ 
SEC_UX100_STATUS ppInfoGetPinpadID( PINPAD_IDCARD *f_stIdcard );
SEC_UX100_STATUS ppInfoSetPinpadID( const PINPAD_IDCARD *f_stIdcard );
SEC_UX100_STATUS ppInfoGetSecureMask( TARGET_MASK_TYPE * f_dev_mask );
SEC_UX100_STATUS ppInfoGetSecureInfo( SECURE_INFO_STRUCT * f_pstSecInfo );
SEC_UX100_STATUS ppInfoGetCoinChargingStatus( int* f_psStatus );
SEC_UX100_STATUS ppInfoGetSensorTemperature( int* f_psStatus );
SEC_UX100_STATUS ppInfoGetConnectionStatus( unsigned int timeout );
SEC_UX100_STATUS ppInfoSetIDCard( const UX100_IDCARD *f_stIdcard );
SEC_UX100_STATUS ppInfoGetIDCard( UX100_IDCARD *f_stIdcard );
SEC_UX100_STATUS ppInfoGetUintType( uint8 * unittype );
SEC_UX100_STATUS ppInfoGetHeaterMode( int* f_psHeaterMode );
SEC_UX100_STATUS ppInfoGetHeaterStatus( int* f_psHeaterStat );
SEC_UX100_STATUS ppInfoGetBootVersion( char * f_pBootVer, int f_sLengthMax );
SEC_UX100_STATUS ppInfoDiagCounter( UX100_DIAG_CNT_CMD f_eCmd, int f_sIndex, int f_sValue, UX100_DIAG_CNT* f_stDiagCnt );
SEC_UX100_STATUS ppInfoUSBDeviceInfo( UX1_DEV_INFO* f_stDevInfo );


/* For Ux100 antiremoval switch */
SEC_UX100_STATUS ppInfoGetRemovalSwitchStatus( int* f_psStatus );
SEC_UX100_STATUS ppSecGetRemovalSwitchChallenge( uint8 * f_pbData, uint16 * f_pwDataLen );
SEC_UX100_STATUS ppSecResetRemovalSwitch( uint8 * f_pbData, uint16 f_wDataLen );


/* For Ux100 event log */
SEC_UX100_STATUS ppLogGetHeader( SAVE_LOG_HDR* f_stLogHDR );
SEC_UX100_STATUS ppLogResetHeader( );
SEC_UX100_STATUS ppLogGetItem( uint16 f_wItemIndex, uint8 * f_pbData );
SEC_UX100_STATUS ppLogRecordItem( uint8 * f_pbData, uint16 f_wDataLen );
SEC_UX100_STATUS ppLogGetAtmelSN( uint8 * f_pbData );
SEC_UX100_STATUS ppLogGetRTC( uint8 * f_pbData );
SEC_UX100_STATUS ppLogSetRTC( uint8 * f_pbData );

SEC_UX100_STATUS ppLogPrint(const char *format, ...);

#ifdef __cplusplus
}
#endif

#endif // __API_BUZZER_H__

