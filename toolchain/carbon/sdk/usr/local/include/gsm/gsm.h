/*
 * libgsm.h
 *
 */

#ifndef _LIBGSM_H_
#define _LIBGSM_H_

#ifdef __cplusplus
extern "C" {
#endif

#define GSM_DATA_PATH		0
#define GSM_ATC_PATH		1	//NOT USED AT THIS TIME
#define GSM_URC_PATH		2	//NOT USED AT THIS TIME
#define GSM_NMEA_PATH		3	//NOT USED AT THIS TIME

#define GSM_FALSE		0
#define GSM_TRUE		1

#define GSM_SIM_1 		1
#define GSM_SIM_2 		2

#define STR_INFO_LEN1	16
#define STR_INFO_LEN2	32
#define STR_INFO_LEN3	64
#define STR_SMS_MSG		160

#define GSM_STT_TURN_OFF			0 // During gsm_power(GSM_MODE_OFF);
#define GSM_STT_OFF					1 // Radio is off
#define GSM_STT_INIT				2 // During initialization 
#define GSM_STT_IMEI_NEEDED			3 // Need tu burn IMEI to Radio (done by factory)
#define GSM_STT_NO_SIM				4 // No SIM inserted
#define GSM_STT_SIM_AUTH_NEEDED		5 // PIN/PUK are requierd
#define GSM_STT_IDLE				6 // Idle mode 
#define GSM_STT_DURING_CONNECT		7 // Connecting to GPRS/CSD/HSPA
#define GSM_STT_GPRS_CONNECTED		8 // Connected to GPRS
#define GSM_STT_3G_CONNECTED		8 // Connected to HSPA
#define GSM_STT_CSD_CONNECTED		9 // Connected to CSD
#define GSM_STT_DURING_DISCONNECT	10 // During CSD disconnect
#define GSM_STT_GET_SCANNING		11 // Scanning for available operators
#define GSM_STT_LOCKED				12 // N/A
#define GSM_STT_UNKNOWN_ERROR		13 // Unknown error
#define GSM_STT_SIM_BLOCKED		14
#define GSM_STT_UNKNOWN				99 // error in STM (restart)


#define GSM_MODE_OFF					1
#define GSM_MODE_ON						2
#define GSM_MODE_AUTO					3
#define GSM_MODE_MANUAL					4
#define GSM_MODE_MANUAL_AUTO			5
#define	GSM_MODE_ACTIVATE				6
#define GSM_MODE_DEACTIVATE				7
#define GSM_MODE_DEACTIVATED			8
#define GSM_MODE_ACTIVATED				9
#define GSM_MODE_ALREADY_ACTIVATED		10
#define GSM_MODE_ALREADY_DEACTIVATED	11
#define GSM_MODE_UNLOCKED				12
#define GSM_MODE_LOCKED					13
#define GSM_MODE_PIN_CHANGED			14
#define GSM_MODE_QUERY					15
#define GSM_MODE_DELETE					16
#define GSM_MODE_ATTACH					17
#define GSM_MODE_DETACH					18
#define GSM_MODE_ATTACHED				19
#define GSM_MODE_DETACHED				20
#define GSM_MODE_SATNAV_OFF				"21"
#define GSM_MODE_SATNAV_ON				"22"

#define GSM_OPERATOR_AUTO 			0
#define GSM_OPERATOR_MANUAL 		1
#define GSM_OPERATOR_DEREG			2
#define GSM_OPERATOR_UNKNOWN		3
#define GSM_OPERATOR_MANUAL_AUTO 	4
#define GSM_OPERATOR_QUERY 			5
#define GSM_OPERATOR_AVAILABLE 		6
#define GSM_OPERATOR_CURRENT 		7
#define GSM_OPERATOR_FORBIDDEN 		8
#define GSM_OP_NAME_NA 				9
#define GSM_LAI_NA 				9

#define GSM_REG_NONE		0
#define GSM_REG_DENIED		1
#define GSM_REG_HOME		2
#define GSM_REG_ROAM		3

#define GSM_CODE_SIMPIN		1
#define GSM_CODE_SIMPUK		2

#define GSM_CONFIGURE_AUTH_CB  1

#define GSM_TRACE_NONE  	0
#define GSM_TRACE_CONSOLE  	1
#define GSM_TRACE_FILE 	 	2

#define GSM_BANDS_850		1
#define GSM_BANDS_900 	 	2
#define GSM_BANDS_1800		4
#define GSM_BANDS_1900		8



#define GSM_CONERR_NONE			1
#define GSM_CONERR_NO_ANSWER	2
#define GSM_CONERR_NO_CARRIER	3
#define GSM_CONERR_NO_DIALTONE	4
#define GSM_CONERR_USR_HANGUP	5
#define GSM_CONERR_BUSY			6
#define GSM_CONERR_SETUP_ERROR	7
#define GSM_CONERR_TIMEOUT		8
#define GSM_CONERR_UNKNOWN		9
#define GSM_CONERR_SERIAL		10



#define GSM_LAST_MESSAGE				1
#define GSM_OK							0
#define GSM_FAIL						-1
#define GSM_INVALID_PARAM				-2
#define GSM_BUSY						-3
#define GSM_NOT_CONNECTED				-4
#define GSM_NOT_SUPPORTED				-5
#define GSM_DEV_STR_TOO_LONG			-6
#define GSM_NOT_READY 					-7
#define GSM_WRONG_PASSWORD				-8
#define GSM_TIMEOUT						-9
#define GSM_MISSING_PARAM				-10
#define GSM_MSG_TOO_LONG				-11
#define GSM_STR_TOO_LONG 				-12
#define GSM_FILE_WRITE_ERR				-13
#define GSM_NO_DATA						-14
#define GSM_SERIAL_ERROR 				-15
#define GSM_CELL_NOT_AVAILABLE			-16
#define GSM_ILLIGAL_PDP_CNTX			-17
#define GSM_APN_TOO_LONG				-18
#define GSM_INVALID_CMD					-19
#define GSM_CANT_FIND_SUITABLE_CELL		-20
#define GSM_NOT_REGISTERED				-21


#define GSM_IMEI_SUCCESS			0
#define GSM_IMEI_INVALID			1
#define GSM_IMEI_FAILED			2
#define GSM_IMEI_FREEZ_FAIL		3
#define GSM_IMEI_ALREADY_WRITTEN	4

#define GSM_RES_PIN_NEEDED			"SIM PIN"
#define GSM_RES_PUK_NEEDED			"SIM PUK"

#define UART_BAUDRATE_115200 B115200

#define GSM_CODE_SIMPIN		1
#define GSM_CODE_SIMPUK		2


#define MAX_MCC_SIZE	4
#define MAX_MNC_SIZE 	4
#define MAX_LAC_SIZE 	5
#define MAX_CELL_SIZE 	5
#define MAX_BSIC_SIZE 	3
#define MAX_CHANN_SIZE 	5
#define MAX_RSSI_SIZE 	4
#define MAX_C1_SIZE 	4
#define MAX_C2_SIZE 	4

#define GSM_OPR_RAT_NOT_CONNECTED			-1
#define GSM_OPR_RAT_GSM 					0
#define GSM_OPR_RAT_UTRAN 					2
#define GSM_OPR_RAT_GSM_W_EGPRS				3
#define GSM_OPR_RAT_UTRAN_W_HSDPA			4
#define GSM_OPR_RAT_UTRAN_W_HSDPA_HSUPA	6


#define GSM_QOS_TR_CLASS_CONVERSATIONAL	0
#define GSM_QOS_TR_CLASS_STREAMING			1
#define GSM_QOS_TR_CLASS_INTERACTIVE		2
#define GSM_QOS_TR_CLASS_BACKGROUND			3
#define GSM_QOS_TR_CLASS_SUBSCRIBED_VAL	4

#define GSM_QOS_DEL_ORDER_NO				0
#define GSM_QOS_DEL_ORDER_YES				1
#define GSM_QOS_DEL_ORDER_SUBSCRIBED_VAL	2


#define GSM_QOS_DEL_ERR_SDUS_NO					0
#define GSM_QOS_DEL_ERR_SDUS_YES				1
#define GSM_QOS_DEL_ERR_SDUS_NO_DETECT			2
#define GSM_QOS_DEL_ERR_SDUS_SUBSCRIBED_VAL		3

#define GSM_APN_MAX_LENGTH 			20
#define GSM_PDP_TYPE_MAX_LENGTH 	2
#define GSM_PDP_ADDR_MAX_LENGTH 	20

#define GSM_CMD_SIZE 128

#define GSM_CELL_SET_SIZE 		3
#define GSM_CELL_CHMOD_SIZE		10
#define GSM_CELL_ACT_SIZE		4
#define GSM_CELL_PHSCH_SIZE		5



#define GSM_TYPE_2G 2
#define GSM_TYPE_3G 3


struct s_gsm_pdp_context{
  int 	cid;
  char 	pdp_type[GSM_PDP_TYPE_MAX_LENGTH+1];
  char	apn[GSM_APN_MAX_LENGTH+1];
  char 	pdp_addr[GSM_PDP_ADDR_MAX_LENGTH];
  int 	pdp_data_compression;		//the Value will always be 0 for GPRS
  int	pdp_header_compression;		//the Value will always be 0 for GPRS
};

typedef struct gsm_pdp_context_list{
     int count;
     struct s_gsm_pdp_context *pdp_context;
}pdp_list;

struct cell_id{
  char mcc[MAX_MCC_SIZE]; 	//Mobile Country Code
  char mnc[MAX_MNC_SIZE]; 	//Mobile Network Code
  char lac[MAX_LAC_SIZE]; 	//Locatio Area Code
  char cell[MAX_CELL_SIZE]; 	//Cell id
  char bsic[MAX_BSIC_SIZE];	//Base Station ID Code
  char chann[MAX_CHANN_SIZE];	//ARFCN (Absolute Frequency Cannel Number)
  char rssi[MAX_RSSI_SIZE];	//Received signal level
  char c1[MAX_C1_SIZE];		//Coefficient for BT reselection
  char c2[MAX_C2_SIZE];		//Coefficient for BT reselection
};

struct gsm_2G_nbr_cell{
  int 	ARFCN;			// Absolute Radio Freq Chan Num of the BCCH carrier
  int 	rssi; 			// [0-63]
  int	dBm;			// Receiving level in dBm
  int 	MCC;			// Mobile Country Code 
  int	MNC;			// Mobile Network Code
  int	NTCC;			// (NCC)Network Color Code
  int	BCC;			// Base station colour code
  int	C1;				// Cell selection Criterion
  int 	C2;				// Cell reselection Criterion
  long int	LAC;			// location Area Code
  long int	cell;			// cell identifier
  int	C31;			// Cell reselection Criterion
  int	C32;			// Cell reselection Criterion
  int 	SRxLev;			// RX Level value for base station selection in dB
  int 	rank;			// rank of this cell as neighbour for inter-RAT cell reselection
};

struct gsm_3G_nbr_cell{
  int 	UARFCN;						// Absolute Radio Freq Chan Num of the BCCH carrier
  int 	PSC;						// Primary Scrambling Code
  int 	Ec_n0;						// Carrier to noise ratio in dBm = measured Ec/lo value in dB
  int 	RSCP;						// Receive Signal Code Power in dBm
  int	SQual;						// Quality value for base station in dB
  int 	SRxLev;						// RX Level value for base station selection in dB
  char 	set[GSM_CELL_SET_SIZE]; 	// 3G neighbour cell set (AS: ASET, SN: Sync Nset, AN: Async Nset)
  int 	rank;						// rank of this cell as neighbour for inter-RAT cell reselection
};

struct gsm_2G_srv_cell{
  char ACT[GSM_CELL_ACT_SIZE];		//access technology (2G, 3G)
  int ARFCN;						// ARFCN (Absolute Radio Frequency Channel Number) of the BCCH carrier
  long int BCCH;								// Receiving level of the BCCH carrier in dBm
  int MCC;								// Mobile Country Code (first part of the PLMN code)
  int MNC;								// Mobile Network Code (second part of the PLMN code)
  long int LAC;							// location Area Code
  long int cell;							// cell identifier
  long int C1;							// Coefficient for base station selection
  long int C2;							// Coefficient for base station selection
  int NCCD;							// PLMN colour code
  int BCC;							// Base station colour code
  char GPRS[16];					// GPRS state
  int PWR;							//	Maximal power level used on RACH channel in dBm
  long int RxLev;						// Minimal receiving level (in dBm) to allow registration
  int ARFCN_2;						// ARFCN (Absolute Radio Frequency Channel Number) of the BCCH carrier
  int TS;							// Timeslot number
  int timAdv;						// Timing advance in bits
  int dBm;							// Receiving level of the traffic channel carrier in dBm
  int Q;							// Receiving quality (0-7)
  char ChMod[GSM_CELL_CHMOD_SIZE];	// Channel mode (--: Signalling, S_HR: Half rate, S_FR: Full rate, S_EFR: Enhanced Full Rate, A_HR: AMR Half rate, A_FR: AMR Full rate )
};
 
struct gsm_3G_srv_cell{
  char ACT[GSM_CELL_ACT_SIZE];		//access technology (2G, 3G)
  long int UARFCN;						// UARFCN (UTRAN Absolute Radio Frequency Channel Number) of the BCCH carrier
  int PSC;							// Primary Scrambling Code
  double EC_n0;						// Carrier to noise ratio in dB = measured Ec/Io value in dB. Please refer to 3GPP 25.133, section 9.1.2.3, Table 9.9 for details on the mapping from EC/n0 to EC/Io.
  double RSCP;							// Received Signal Code Power in dBm
  int MCC;							// Mobile Country Code (first part of the PLMN code)
  int MNC;							// Mobile Network Code (second part of the PLMN code)
  int LAC;							// location Area Code
  long int cell;							// cell identifier
  int SQual;						// Quality value for base station selection in dB (see 3GPP 25.304)
  int SRxLev;						// RX level value for base station selection in dB (see 3GPP 25.304)
  char PhysCh[GSM_CELL_PHSCH_SIZE];	// Physical Channel Type (DPCH, FDPCH)
  int SF;							// Spreading Factor (4,8,16,32,64,128,256,512)
  int Slot;							// Slot Format for DPCH (0-16) (see 3GPP TS 25.211 V7.10.0 Table 11)| Slot Format for FDPCH (0-9) (see 3GPP TS 25.211 V7.10.0 Table 16C)
  double EC_n0_2;						// Carrier to noise ratio in dB = measured Ec/Io value in dB. Please refer to 3GPP 25.133, section 9.1.2.3, Table 9.9 for details on the mapping from EC/n0 to EC/Io.
  double RSCP_2;							//	Received Signal Code Power in dBm
  int ComMod;						// Compressed Mode (0-1) (indicates valid transmission gap pattern)
  int HSUPA;						// HSUPA type indicated by xy: x = capability: 0 - no, 1 - yes. Please consider that some providers don't support this flag. y = status: 0 - off, 1 - on, 2 - not available
  int HSDPA;						// HSDPA type indicated by xy: x = capability: 0: no, 1: yes, 2: HSDPA+ capable. Please consider that some providers don't support this flag.y = status: 0: off, 1: on, 2: not available
};

struct serving_cell_3G{
  int srv_cell_type; 							// GSM_TYPE_2G, GSM_TYPE_3G
  struct gsm_2G_srv_cell srv_cell_struct_2G;
  struct gsm_3G_srv_cell srv_cell_struct_3G;
};
  

typedef struct neighbour_cell_list_3G{
  int nbr_cell_type;				// GSM_TYPE_2G, GSM_TYPE_3G
  struct gsm_2G_nbr_cell *cell_struct_2G;
  int count_2G;
  struct gsm_3G_nbr_cell *cell_struct_3G;
  int count_3G;
}nbr_cell_list_3G;

typedef struct cellid_list{
  int count;
  struct cell_id *cell_id;
}cell_list;
  


// stored on static data
struct gsm_info{
	char library_version[STR_INFO_LEN1];   // e.g. 1.0.0
	char manufacturer	[STR_INFO_LEN1];  	// (+CGMI) e.g. SIEMENS
	char model			[STR_INFO_LEN1];  	// (+CGMM) e.g. MC55i
	char revision		[STR_INFO_LEN1];	// (+CGMR) e.g. xx.yy
	char imei			[STR_INFO_LEN1];	// (+CGSN) e.g. 123456789012345
	char sim_card_id	[STR_INFO_LEN2];   // (+CXXCID) e.g. 123456789012345678
	char imsi			[STR_INFO_LEN1];	// (+CIMI) e.g. 123456789012345
};

struct gsm_network_status{
	int registration;			// GSM_REG_NONE, GSM_REG_HOME, GSM_REG_ROAM, GSM_REG_DENIED
	int rssi;					//
	int ber;					//
	int rat;				// Radio Access Technology: see values under "GSM_OPR_RAT_xxxx" , Valid only for 3G!!!
};

struct gsm_operator{
	char alphanumeric[STR_INFO_LEN3];
	int location_area_id;
	int rat;   				//see values under "GSM_OPR_RAT_xxxx"
	int op_status;				//see values under "GSM_OPERATOR_xxxx"
};

typedef struct gsm_operator_list{
     int count;
     struct gsm_operator *operators;
}op_list;



struct gsm_quality_of_service{
	char precedence;
	char delay;
	char reability;
	char peak;
	char mean;
};


/*
struct gsm_3g_quality_of_service{
    int traffic_class; 			//see defined values above - GSM_QOS_TR_CLASS_xxxx
    int maximum_bitrate_ul; 		//0..5760
    int maximum_bitrate_dl;		//1..14000
    int guaranteed_bitrate_ul;		//1..5760
    int guaranteed_bitrate_dl;		//1..14000
    int delivery_order;			//see defined values above - GSM_QOS_DEL_ORDER_xxx
    int maximum_sdu_size;  		//0 - subscribed value , 10..1520 - (value needed to be divisible by 10), 1502 .
    int sdu_error_ratio;		//0E0, 1E2, 7E3, 1E3, 1E4, 1E5, 1E6, 1E1
    int residual_bit_err_ratio;		//0E0, 5E2, 1E2, 5E3, 4E3, 1E3, 1E4, 1E5, 1E6, 6E8
    int delivery_of_erroneous_sdus;	//see defined values above - GSM_QOS_DEL_ERR_SDUS_xxxxx
    int transfer_delay;			//0 - subscribed value , 100..150**,200-950**, 1000..4000** ->  ** (value needed to be divisible by 10).
    int traffic_handling_priority;	//0, 1, 2, 3
};*/

/*
struct gsm_setup{
	int sleep_mode;
	struct gsm_operation_selection operation_selection;
};
*/


struct s_at_cmd{
  char	cmd[GSM_CMD_SIZE];		// the command
  char	*response;				// response (alocated by libgsm
  int	response_size;			// size of response
  int	timeout;				// some response may take a while longet than others, this is the max time to wait (in seconds)
};

struct gsm_last_connection_info{
	char connect_error;		// GSM_CONERR_NO_CARRIER, GSM_CONERR_NO_DIALTONE..
};
/*
struct gsm_3g_configuration{
     char apn[STR_INFO_LEN3];
     struct gsm_3g_quality_of_service quality_of_service_min;
     struct gsm_3g_quality_of_service quality_of_service_req;
     char configuration_commands_file[STR_INFO_LEN3];
};
*/
struct gsm_gprs_configuration{
     char apn[STR_INFO_LEN3];
     struct gsm_quality_of_service quality_of_service;
     char configuration_commands_file[STR_INFO_LEN3];
};
  
struct gsm_csd_configuration{
	  char dial_str[STR_INFO_LEN3]; 
	  char configuration_commands_file[STR_INFO_LEN3];
};


struct s_gsm_lai{
  int lai;
  int roaming_mode;
};

/****************************
 * 		GNSS DEFINES		*
 ****************************/
#define GSM_SIZE_PARAM_NAME		11
#define GSM_SIZE_PARAM_VALUE	11
#define GSM_SIZE_PARAM_SIZE		10
#define GSM_AGPS_SET			1
#define GSM_AGPS_GET			0
#define GSM_UTC_GET				1
#define GSM_UTC_SET				0
#define GSM_SIZE_GNSS_PARAMS	8
#define GSM_SIZE_VALUES			7

typedef struct
{
	char name[GSM_SIZE_PARAM_NAME];
	int Length;
	char value[GSM_SIZE_PARAM_VALUE];
}S_GSM_GNSS_PARAMS;

typedef struct
{
	char value[16];
	int Length;
}S_GSM_PARAM_VALS;

typedef enum
{
	/* AT CMDs */
	E_GSMAT_GNSS_INFO,
	E_GSMAT_ENGINE 				= 1,
	E_GSMAT_FREQUENCY,
	E_GSMAT_GLONASS,
	E_GSMAT_OUTPUT,
	E_GSMAT_ANTENNA				= 5,
	E_GSMAT_ODM,
	E_GSMAT_PSM,
	E_GSMAT_START,
	E_GSMAT_AGPS,
	E_GSMAT_UTC_SET				= 10,
	E_GSMAT_UTC_GET,
	E_GSMAT_COUNT,				/* Must be the last element in the sequence of params */
	/* Other CMDs */
	E_GSMAT_DEF_PARAMS,
	E_GSMAT_VALUES_LIST_INFO,
	E_GSMAT_POWER_SATNAV
}E_GSMAT_GNSS_CMDS;

/** ***************************************************************************** 
 * gsm_request_auth_cb_t - for gsm_configure_auth_callback() function
 * 						   Handles PIN/PUK in case the radio is locked
 * 		 				   This is instead of waiting for GSM_STT_LOCKED, libgsm will automaticly call the users function
 * 						   than hadles PIN/PUK Unlock.
 * code_type - GSM_CODE_SIMPIN, GSM_CODE_SIMPUK ... 
 * attempts - remaining attempts 
 * code_str - pointer to auth code (allocated by libgsm, filled by application)
 * code_max_length - max auth code string size 
 *******************************************************************************/

typedef int (*gsm_request_auth_cb_t)(int attempts, int code_type, char* pin,  char* puk, int code_max_length);


/** ***************************************************************************** 
 * gsm_sms_in_cb_t - for gsm_configure_auth_callback() function
 * 					 Notify the user of incomming SMS

 *******************************************************************************/

typedef int (*gsm_sms_in_cb_t)(void);


/** ***************************************************************************** 
 * gsm_undrVoltage_cb_t - for gsm_configure_auth_callback() function
 * 					 Notify the user of undrVoltage that will cause radio automatic shutdown

 *******************************************************************************/

typedef int (*gsm_undrVoltage_cb_t)(void);

/** ******************************************************************************************************************
 *  Function Name	: gsm_activation
 * 	Return Value	:  GSM_MODE_LOCKED, GSM_MODE_UNLOCKED, GSM_WRONG_PASSWORD, 
 * 					   GSM_FAIL , GSM_OK, GSM_BUSY, GSM_INVALID_PARAM.
 *  params			: action - activate, deactivate, query : GSM_MODE_ACTIVATE, GSM_MODE_DEACTIVATE, GSM_MODE_QUERY
 * 					  password - password used to activate/deactivate the sim pin
 * 	Description 	: handles sim pin activation, deactivation , all actions should be done in idle state!
 * 					  GSM_BUSY - the api was used not in idle state
 * 					  GSM_FAIL - failed to send the command to the radio (radio restart might be needed)
 * 					  GSM_INVALID_PARAM - for some reason no valid reply arived from radio, retry   
 * 											and if fails again, restart radio.
 * 					  !! if attempting to activate/deactivate when already activated/deactivated, 
 * 						no matter if the password is correct or not, there return value will be "success" !!

** ********************************************************************************************************************/
 
int gsm_pin_activation(int action, char *password);

/** ************************************************************************************************
 *  Function Name	: gsm_pin_unlock
 * 	Return Value	: GSM_OK, GSM_FAIL, GSM_INVALID_PARAM (in case of mixing pin with puk)
 *  params			: action - GSM_CODE_SIMPIN, GSM_CODE_SIMPUK
 * 					  pin - pin in case of GSM_CODE_SIMPIN or GSM_CODE_SIMPUK
 * 					  puk - puk in case of GSM_CODE_SIMPUK
 * 	Description 	: unlocking the pin/puk.
 * 					  case puk needed, the user must send pin & puk
 * 					  this function does not supply a feedback to the user whether the unlock was
 * 					  successfull or not. in order to get a feedback , the user has to check 
 * 					  the status and make sure that it is no longer under "GSM_STT_SIM_AUTH_NEEDED".
 * 
 * 					  !!!! the callback has a higher priority then gsm_pin_unlock, therefore, if
 *						 the callback is registered, the API will not work !!!!  

** ************************************************************************************************/
 
int gsm_pin_unlock(int mode, char* pin, char* puk);

/** ************************************************************************************************
 *  Function Name	: gsm_is_locked
 * 	Return Value	: GSM_MODE_UNLOCKED, GSM_MODE_LOCKED, GSM_BUSY, GSM_INVALID_PARAM
 * 	return params	: attemts - how many attempt left for pin/puk
 * 					  lock_type : GSM_CODE_SIMPIN, GSM_CODE_SIMPUK
 * 	Description 	: query status of the locked sim pin/puk   
 * 					  GSM_BUSY - can't check status.

** ************************************************************************************************/

int gsm_is_locked(int *lock_type, int *attempts);

/** ************************************************************************************************
 *  Function Name	: gsm_radio_start
 * 	Return Value	: GSM_OK, GSM_FAIL, GSM_BUSY, GSM_MODE_ALREADY_ACTIVATED 
 * 	Description 	: This function starts the "libgsm"
 * 					  This function should only be called onece 
 * 					  if the returned value = GSM_FAIL -> no radio or CIB error
 * 					  if the returned value = GSM_BUSY, libgsm is already loaded by another process!!!
 * 					  if the returned value = GSM_MODE_ALREADY_ACTIVATED, libgsm is already loaded (localy)

** ************************************************************************************************/
 
int gsm_radio_start(void); 

/** ************************************************************************************************
 *  Function Name	: gsm_power
 * 	Return Value	: GSM_OK, GSM_INVALID_PARAM, GSM_MODE_OFF, GSM_MODE_ON
 * 	Params			: GSM_MODE_ON, GSM_MODE_OFF, GSM_MODE_QUERY
 * 	Description 	: This sends radio power up/down , requests, and return OK/FAIL 
 * 
** ************************************************************************************************/

int gsm_power(int mode);			/* GSM_MODE_ON, GSM_MODE_OFF, GSM_MODE_QUERY */     
	
/** ************************************************************************************************
 *  Function Name	: gsm_sleep_mode
 * 	Return Value	: GSM_INVALID_PARAM, GSM_OK, GSM_MODE_OFF, GSM_MODE_ON, GSM_NOT_READY, GSM_BUSY, GSM_INVALID_PARAM
 * 	Params			: GSM_MODE_ON, GSM_MODE_OFF, GSM_MODE_QUERY
 * 	Description 	: setting the radio's sleep mode on/off
 * 					!!! due to constrains in the radio HW, the radio can only enter sleep mode 3 minutes
 *						after the radio was turned on (gsm_power(GSM_MODE_ON)!!!! 
 * 						(in this case, the rturn val will be "GSM_NOT_READY")
 * 					GSM_INVALID_PARAM -  the parameter given as “mode” is invalid. 
 * 					case mode = GSM_MODE_QUERY, the return params will be "GSM_MODE_OFF" or "GSM_MODE_ON"
 * 					GSM_BUSY - previeus sleep request is being proccessed, try again.
 * 					
 * 					
** ************************************************************************************************/

int gsm_sleep_mode(int mode);		/* GSM_MODE_ON, GSM_MODE_OFF, GSM_MODE_QUERY */     
 
/** ************************************************************************************************
 *  Function Name	: gsm_scan_available_operators
 * 	Return Value	: GSM_OK, GSM_BUSY
 * 	Params			: NONE
 * 	Description 	: scan for available operators , can only be done when status = idle
 * 					  after the API was called the status should change to GSM_STT_GET_SCANNING
 * 					  and remain in this status untill the scanning procedur is complete (return to idle)
 * 					  
 * 					  GSM_BUSY - status != idle
 * 					  GSM_OK - scan will start shortly
 * 					  

** ************************************************************************************************/

int gsm_scan_available_operators(void);

/** ************************************************************************************************
 *  Function Name	: gsm_get_scan_results
 * 	Return Value	: GSMAT_OK, GSM_NOT_READY, GSM_MISSING_PARAM
 * 	Params			: pointer to a struct gsm_operator_list 
 * 	Description 	: return the scan results done by "gsm_scan_available_operators"
 * 					  the user must free the memory allocated by the libgsm for the 
 * 					  struct gsm_operator_list -->  member - struct gsm_operator *operators.
 * 					  (example:  free(operator_list.operators) )
 * 					  GSM_MISSING_PARAM will be returned in case *operator_list = NULL
 * 					  GSM_NOT_READY - will only be returned if the was no scan done after libgsm started
 * 									  otherwise, gsm_get_scan_results will always return the results
 * 									  genarated by the last scan (gsm_scan_available_operators)

** ************************************************************************************************/

int gsm_get_scan_results(struct gsm_operator_list *operator_list);  

/** ************************************************************************************************
 *  Function Name	: gsm_operator_selection
 * 	Return Value	: GSM_OK, GSM_FAIL, GSM_BUSY, GSM_INVALID_PARAM, GSM_MISSING_PARAM
 * 	Params			: 1) int mode: GSM_OPERATOR_AUTO, GSM_OPERATOR_MANUAL, GSM_OPERATOR_DEREG, 
 * 					  	 		   GSM_OPERATOR_MANUAL_AUTO, 
 * 					  2) int*  location_area_id: location area id requierd for Manual, Auto/manual
 * 						 modes
 * 					  
 * 	Description 	: chose operator selection mode (see above) 
 * 					  GSM_INVALID_PARAM - will be returned in case of a wrong "mode"
 * 					  GSM_MISSING_PARAM - will be returned in case of *location_area_id = 0

** ************************************************************************************************/

int gsm_operator_selection(int mode, int *location_area_id); // GSM_OPERATOR_AUTO, GSM_OPERATOR_MANUAL, GSM_OPERATOR_DEREG, GSM_OPERATOR_MANUAL_AUTO */


/** ************************************************************************************************
 *  Function Name	: gsm_operator_query
 * 	Return Value	: GSM_OK, GSM_FAIL, 
 * 					  GSM_BUSY - radio not ready yet
 * 					  GSM_MISSING_PARAM - location_area_id or roaming_mode are NULL
 * 					  GSM_OP_NAME_NA  - case of Auto,Manual or auto/manual mode, and no available op_name
 *										the roaming mode value will be valid, but the operator name will be blank 
 * 
 * 	Params			: 1) char*  operator_name: output param for returning the "operator name")
 * 					  2) int *roaming_mode - GSM_OPERATOR_AUTO, GSM_OPERATOR_MANUAL, GSM_OPERATOR_DEREG, 
 * 					  	 		   GSM_OPERATOR_MANUAL_AUTO,
 * 					  
 * 	Description 	: used to check the current operator's name, and registration (roaming) mode.

** ************************************************************************************************/

int gsm_operator_query(char *location_area_id, int *roaming_mode);

/** ************************************************************************************************
 *  Function Name	: gsm_get_lai
 * 	Return Value	: GSM_OK, GSM_FAIL, 
 * 					  GSM_BUSY - radio not ready yet
 * 					  GSM_MISSING_PARAM - location_area_id or roaming_mode are NULL
 * 					  GSM_LAI_NA  - case of Auto,Manual or auto/manual mode, and no available LAI
 *										the roaming mode value will be valid, but the LAI will be blank 
 * 	Params:			  1)struct s_gsm_lai	
 * 	  						lai: location area id 
 * 					 	    roaming_mode - GSM_OPERATOR_AUTO, GSM_OPERATOR_MANUAL, GSM_OPERATOR_DEREG, 
 * 					  	 		   GSM_OPERATOR_MANUAL_AUTO,
 * 					  2)size - sizeof( 	struct s_gsm_lai)
 * 					  
 * 	Description 	: used to check the current operator's lai, and registration (roaming) mode.

** ************************************************************************************************/

int gsm_get_lai(struct s_gsm_lai *s_lai, int size);

/** ************************************************************************************************
 *  Function Name	: gsm_bands
 * 	Return Value	: 
 * 	Params			: 1) GSM_MODE_ON , GSM_MODE_QUERY; 
 * 					  2) GSM_BANDS_900, GSM_BANDS_1800, GSM_BANDS_1900
 * 	Description 	: setting the GSM frequency bands

** ************************************************************************************************/

// int gsm_bands(int mode, int *bands);	// GSM_MODE_ON , GSM_MODE_QUERY; GSM_BANDS_900, GSM_BANDS_1800...  //

/** ************************************************************************************************
 *  Function Name	: gsm_pin_change
 * 	Return Value	: GSM_BUSY- will be returned when radio is not ready (off, mux is not loaded yet)
 * 					  GSM_FAIL - faild, try restarting the radio
 * 					  GSMAT_MODE_PIN_CHANGED - success - pin has changed
 * 					  GSM_WRONG_PASSWORD - wrong pin
 * 					  GSM_MISSING_PARAM - pin or new_pin are NULL
 * 	Params			: 1) pin
 * 					  2) new_pin
 * 	Description 	: changing the SIM's pin
 * 					  

** ************************************************************************************************/
int gsm_pin_change(char *pin, char *new_pin);

/** ************************************************************************************************
 *  Function Name	: gsm_get_info
 * 	Return Value	: GSM_OK, GSM_BUSY
 * 	Params			: 1) pointer to struct gsm_info
 * 					  2) size - (sizeof(struct gsm_info)
 * 	Description 	: this must be used after initial boot (after turning the radio on for the first time) 
 * 					  as ended (after gsm_get_status() return "GSM_STT_IDLE" for the first time )
 *	   				  returns : library_version, radio manufacturer, radio model, radio revision
 * 								imei, sim_card_id, imsi.

** ************************************************************************************************/
	
int gsm_get_info(struct gsm_info *info, int size);

/** ************************************************************************************************
 *  Function Name	: gsm_get_network_status
 * 	Return Value	: GSM_OK, GSM_FAIL, GSM_BUSY, GSM_INVALID_PARAM
 * 	Params			: 1) pointer to struct gsm_network_status
 * 					  2) size - (sizeof(struct gsm_network_status))
 * 	Description 	: return network status:
 * 					  # rssi
 * 					  # ber
 * 					  # registration : GSM_REG_NONE, GSM_REG_HOME, GSM_REG_DENIED, GSM_REG_ROAM
 * 					  # rat -  Radio Access Technology: see values under "GSM_OPR_RAT_xxxx"
		  
** ************************************************************************************************/

int gsm_get_network_status(struct gsm_network_status  *network_status, int size);


/** ************************************************************************************************
 * 
 *  Function Name	: gsm_get_status
 * 	Return Value	: GSM_STT_TURN_OFF, GSM_STT_OFF, GSM_STT_INIT, GSM_STT_IMEI_NEEDED, 
 *					  GSM_STT_NO_SIM, GSM_STT_SIM_AUTH_NEEDED, GSM_STT_IDLE,
 *					  GSM_STT_DURING_CONNECT, GSM_STT_GPRS_CONNECTED/GSM_STT_3G_CONNECTED,
 *					  GSM_STT_CSD_CONNECTED, GSM_STT_DURING_DISCONNECT,
 *					  GSM_STT_GET_SCANNING, GSM_STT_LOCKED,
 * 					  GSM_STT_UNKNOWN_ERROR, GSM_STT_UNKNOWN
 * 	Description 	: return the radio status
 * 
** ************************************************************************************************/

int gsm_get_status(void);

/** ************************************************************************************************
 *  
 * 	Function Name	: gsm_gprs_connect
 * 	Return Value	: GSM_OK, GSM_BUSY, GSM_NOT_READY, GSM_DEV_STR_TOO_LONG, GSM_INVALID_PARAM
 * 	Params			: 1) apn
 * 					  2) struct gsm_quality_of_service
 * 					  3) configuration_commands_file
 * 					  4) gprs_dev_str 
 * 					  5) gprs_dev_str_max_size
 * 
 * 	Description 	: 		!!!! at this time QoS is not supported, TBD in the future...
 * 					recieve from user - connection info (apn, QOS) 
 * 					    also buffer for dev path, and buffer size
 * 					    and return the dev path for the pppd (example: "/dev/ttygsm1")
 * 					  * configuration_commands_file is the path to the configuration command file. 
 *						should be located at :"/mnt/flash/userdata/share/"
 * 						the file should include user specific AT commands requierd to connect to gprs and the expected resault 
 *						every line in the file should be in the following format:
 *						command$response (example: "ATD*99***1#$CONNECT") 
 * 						in case the "standard" commands not supported by the carrier.
 * 						case return value = GSM_INVALID_PARAM --> configuration_commands_file is incorrect or 
 * 																  there is a problem opening the file
 * 

** ************************************************************************************************/

int gsm_gprs_connect(char *apn, struct gsm_quality_of_service *quality_of_service, char *configuration_commands_file, char *gprs_dev_str, int gprs_dev_str_max_size);


/** ************************************************************************************************
 *  Function Name	: gsm_csd_connect
 * 	Return Value	: 
 * 	Params			: 1) dial_str 
 * 					  2) configuration_commands_file
 * 	Description 	: establish CSD call

** ************************************************************************************************/

 int gsm_csd_connect(char *dial_str, char *configuration_commands_file);

/** ************************************************************************************************
 *  Function Name	: gsm_csd_send_data
 * 	Return Value	: on success size of sent data,
 *			  on failuer:
 *			  GSM_FAIL 
 *			  GSM_NOT_CONNECTED - not connected/During disconnect
 * 	Params			: 1) data - data buffer pointer
 * 					  2) size - size to send
 * 	Description 	: send data over CSD connection

** ************************************************************************************************/

 int gsm_csd_send_data(char* data, int size);

/** ************************************************************************************************
 *  Function Name	: gsm_csd_receive_data
 * 	Return Value	: GSM_FAIL or size of received data
  * 						* GSM_INVALID_PARAM - Negative or zero timeout
 * 						* GSM_MISSING_PARAM - size <= 0 or no memory allocated for data
 * 						* GSM_NOT_CONNECTED - CSD is not connected/during disconnect
 * 						* GSM_SERIAL_ERROR - unable to read data from serial device
 * 						* GSM_TIMEOUT - timeout with no incoming data
 * 	Params				: 1) data - receive data buffer
 * 					  2) size - data buffer size
 * 					  3) timeout - how long to wait for the data to be available(in seconds)

 * 	Description 	: check for incoming data over CSD connection

** ************************************************************************************************/

int gsm_csd_receive_data(char* data, int size, int timeout); 

/** ************************************************************************************************
 *  Function Name	: gsm_csd_disconnect
 * 	Return Value	: 
 * 	Params			: NONE
 * 	Description 	: disconnect CSD call

** ************************************************************************************************/

int gsm_csd_disconnect(void);

/** ************************************************************************************************
 *  Function Name	: gsm_csd_answer(int mode, int rings_to_answer)
 * 	Return Value	: 
 * 	Params			: NONE
 * 	Description 	: enable answering CSD calls

** ************************************************************************************************/

int gsm_csd_answer(int mode, int rings_to_answer);


/** ************************************************************************************************
 *  Function Name	: gsm_get_last_connection_info
 * 	Return Value	: GSM_INVALID_PARAM
 * 	Params			: 1) last_connection_info struct pointer
 * 					  2) size - of "last_connection_info" struct
 * 	Description 	: get the last connection error

** ************************************************************************************************/

int gsm_get_last_connection_info(struct gsm_last_connection_info *last_connection_info, int size);


/** ****************************************************************************************
 * 
 * Function Name: gsm_burn_imei
 * Input : imei_str   
 * Return Value : GSM_FAIL- failed to send message, restart the radio and try again
 * 				  GSM_INVALID_PARAM - unknown response from the radio
 * 
 * this function recieve the IMEI from the user, 
 * and burn it into the radio. 
 * and returnes:
 * GSM_IMEI_SUCCESS 		- Customer IMEI successfully written.
 * GSM_IMEI_INVALID 		- Invalid IMEI. Wrong check digit (see GSM 02.16).
 * GSM_IMEI_FAILED 			- Writing the IMEI failed.
 * GSM_IMEI_FREEZ_FAIL 		- Freezing the IMEI failed.
 * GSM_IMEI_ALREADY_WRITTEN - Customer IMEI is already set and locked. No other
 * write access is allowed.
 * 
 * GSM_MISSING_PARAM - imei_str is a NULL pointer.
 * 
 *******************************************************************************************/

int gsm_burn_imei(char* imei_str);

/** ************************************************************************************************
 *  Function Name	: gsm_configure_auth_callback
 * 	Return Value	: 
 * 	Params			: application auth function address
 * 	Description 	: Configure applicaton function for auth handling events.
 * 					  
** ************************************************************************************************/ 

int gsm_configure_auth_callback(gsm_request_auth_cb_t user_auth_request_func);

/** ************************************************************************************************
 *  Function Name	: gsm_configure_sms_callback
 * 	Return Value	: 
 * 	Params			: application auth function address
 * 	Description 	: Configure applicaton function to receive notification on incomming sms.
 * 					  
** ************************************************************************************************/ 

int gsm_configure_sms_callback(gsm_sms_in_cb_t user_sms_request_func);

/** ************************************************************************************************
 *  Function Name	: gsm_configure_undrVoltage_callback
 * 	Return Value	: 
 * 	Params			: application auth function address
 * 	Description 	: Configure applicaton function o receive notification undrVoltage.
 * 					  
** ************************************************************************************************/ 

int gsm_configure_undrVoltage_callback(gsm_undrVoltage_cb_t user_undrVoltage_func);

/** ************************************************************************************************
 *  
 * 	Function Name	: gsm_trace_mode
 * 	Return Value	: GSM_OK, GSM_NOT_SUPPORTED
 * 	Params			: 1) log_mode - GSM_TRACE_DISABLE, GSM_TRACE_CONSOLE, GSM_TRACE_FILE, GSM_TRACE_SYSLOG
 * 					  2) file_path (not supported at this time)
 * 	Description 	: enable/disable trace, at this time in console mode only  

** ************************************************************************************************/

int gsm_trace_mode(int log_mode, char *file_path); /* GSM_LOG_DISABLE, GSM_LOG_CONSOLE, GSM_LOG_FILE */  

/** ************************************************************************************************
 *  
 * 	Function Name	: gsm_unload_libgsm
 * 	Return Value	: GSM_OK
 * 	Params			: none
 * 	Description 	: unloading libgsm, in order to enable different process to use it! 
 * 					  when using this API, the user should wait for "gsm_get_status() == GSM_STT_OFF"

** ************************************************************************************************/

int gsm_unload_libgsm(void);


/** ************************************************************************************************
 *  
 * 	Function Name	: gsm_send_sms
 * 	Return Value	: GSM_OK , 
 * 					  GSM_MISSING_PARAM -  *number or *message are NULL
 * 					  GSM_BUSY - radio not ready to send SMS
 * 					  GSM_MSG_TOO_LONG - the message lengthe is over 160 chars
 * 	Params			: number - number to which the SMS is sent to
 * 					  message - the actual text to send.
 * 	Description 	: enables sending text messages 

** ************************************************************************************************/

 int gsm_send_sms(char *number , char *message);

 
/** ************************************************************************************************
 *  
 * 	Function Name	: gsm_sms_receive_enable
 * 	Return Value	: GSM_OK , 
 * 					  GSM_MISSING_PARAM -  *sms_file_path is NULL
 * 					  GSM_INVALID_PARAM - mode is incorrect
 * 					  GSM_STR_TOO_LONG - sms_file_path is too long (should be under 64 Chars)
 * 					  GSM_FILE_WRITE_ERR - this will only be returned in case "gsm_sms_receive_enable"
 * 										   is called for the second (or more) time with "GSM_MODE_ACTIVATE".
 * 										   it states that libgsm, tried to save SMS to file and failed (permission
 * 										   issues)
 * 										   this is a way to make sure that the file created for incoming SMS
 * 										   has the correct permissions.
 * 										   to rest this error, call "gsm_sms_receive_enable(GSM_MODE_DEACTIVATE,);
 * 	Params			: mode - GSM_MODE_ACTIVATE, GSM_MODE_DEACTIVATE
 * 					  sms_file_path - the full path and name of the SMS file.
 * 	Description 	: enables saving received SMS to file

** ************************************************************************************************/
 
int gsm_sms_receive_enable(int mode,char *sms_file_path);

/** ************************************************************************************************
 *  
 * 	Function Name	: gsm_monitor_cells
 * 	Return Value	: GSM_OK , 
 * 			 GSM_NOT_READY -  SIM Auth. is not compleate yet
 * 			 GSM_FAIL - Failed to ceck for cells (could be serial error)
 * 			 GSM_MISSING_PARAM - memory was not allocated for list_of_cells by the user.
 * 			 GSM_CELL_NOT_AVAILABLE - unable to find cells (no reception)
 * 	Params			: struct cellid_list *list_of_cells {count, *cell_id) count - number of cells found
 *										      *cell_id  points to buffer that has x=count 
 *										      structs of struct cell_id
										      
 * 	Description 	: return info of all available cells
 *    			  the user must free the "list_of_cells.cell_id" (memory that was allocated by libgsm)
** ************************************************************************************************/

int gsm_monitor_cells(struct cellid_list *list_of_cells);

/********************************************************************
 * Name:			gsm_Manage3gCmds()
 * Description: 	Sets and gets values using AT commands and libgsm capabilities. Used by net services
 * Input:			id: 	Parameter ID (AT command number)
 * 					param:	Pointer to a buffer holding a value
 * 					size: 	Pointer to Size of param void pointer
 * Output:			size: 	Size of param void pointer
 * 					value:	Buffer holding a value
 * return:		 	GSM_OK
 * 					<1 - Error
 ********************************************************************/
int gsm_Manage3gCmds(int id, void *param, int *size);

/** ************************************************************************************************
 *  
 * 	Function Name	: gsm_attach
 * 	Return Value	: GSM_OK , 
 * 			 GSMAT_BUSY -  Radio in not in or after init state.
 * 			 GSM_INVALID_PARAM - illigal mode value.
 * 			 GSMAT_FAIL - failed to send the command to the radio (radio reset may be requiered )
 * 			 if mode == GSM_MODE_QUERY:
 *					GSM_MODE_ATTACHED - attached 
 *					GSM_MODE_DETACHED - detached
 * 	Params		 mode: 
 *				GSM_MODE_ATTACH - attach 
 *				GSM_MODE_DETACH - detache 
 *				GSM_MODE_QUERY  - return the current GPRS service state.
										      
 * 	Description 	: GPRS attach or detach
** ************************************************************************************************/

int gsm_attach(int mode);
  
/** ************************************************************************************************
 *  
 * 	Function Name	: gsm_gprs_disconnect
 * 	Return Value	: GSM_OK , 
 * 			 GSMAT_NOT_CONNECTED -  Radio is not connected/during connection to GPRS
 *
 * 	Description 	: disconnect from GPRS using AT command
** ************************************************************************************************/

int gsm_gprs_disconnect(void);

/** ************************************************************************************************
 *  
 * 	Function Name	: gsm_send_at_cmd
 * 	Return Value	: GSM_OK - success
 * 			 		  GSM_MISSING_PARAM -  cmd was not Initialized 
 * 					  GSM_NOT_READY - radio initialization is not done yet
 * 					  GSM_INVALID_CMD - invalid/illigal command (not in the white list)
 * 					  GSMAT_SERIAL_ERROR - faild to send the command to the radio (write/read error)
 * 					  GSMAT_FAIL - general error - retry 
 * 					  GSM_TIMEOUT - reached timeout before the response arrived 
 * 
 *  params:			struct s_at_cmd
 * 						char	cmd[GSM_CMD_SIZE];		// the command
 *						char	*response;				// response (alocated by libgsm
 *						int		response_size;			// size of response
 *						int		timeout;				// some response may take a while longet than others, this is the max time to wait (in seconds)
 * 						
 *
 * 	Description 	: Send AT command to the radio
 * 					  The command must be in the libgsm "white list"!
 * 					  !!!!!!------    The User must free the memory allocated for the response (free(s_at_cmd.response)) ----!!!!!!
 * 
** ************************************************************************************************/

int gsm_send_at_cmd(struct s_at_cmd *cmd);

/** ************************************************************************************************
 *  
 * 	Function Name	: gsm_3G_monitor_neighbour_cells
 * 	Return Value	: GSM_OK - success
 * 			 		  GSMAT_NOT_SUPPORTED	-  not a 3G radio
 * 	`				  GSM_NOT_READY			- radio is during initialization
 *					  GSM_MISSING_PARAM		- user didn't allocate memory for list_of_cells 
 * 					  GSM_FAIL 				- failed to send command
 * 					  GSM_CELL_NOT_AVAILABLE - no cell found / parsing issue
 * 
 *  params:			nbr_cell_list_3G
 * 						  int nbr_cell_type;						// GSM_TYPE_2G, GSM_TYPE_3G
 *						  struct gsm_2G_nbr_cell *cell_struct_2G;  // data for 2G neighbour cells
 *						  int count_2G;								// number of 2G cells found
 *						  struct gsm_3G_nbr_cell *cell_struct_3G;	// data for 3G neighbour cells
 * 						  int count_3G;								// number of 3G cells found
 * 					size : sizeof(	nbr_cell_list_3G)
 *
 * 	Description 	: scan for neigbour cells. and return a list of available 2G and 3G cells
 * 					  libgsm allocates memory depending on the number of available cells.
 * 					  the user must free the memory after calling the function.
 * 					  example:
 * 						    free(g_list_3G.cell_struct_2G);
 * 							free(g_list_3G.cell_struct_3G);
 * 
** ************************************************************************************************/

int gsm_3G_monitor_neighbour_cells(nbr_cell_list_3G *list_of_cells, int size);

/** ************************************************************************************************
 *  
 * 	Function Name	: gsm_3G_monitor_serving_cell
 * 	Return Value	: GSM_OK - success &  connected 
 * 					  GSM_NOT_REGISTERED - UE is camping on a UMTS cell but not registered to the network (PhysCh(3G),GPRS(2G) == "LIMSRV")
 * 					  GSM_NOT_CONNECTED - UE is camping on a UMTS (3G)/GSM(2G) cell and registered to the network (PhysCh(3G),GPRS(2G) == "NOCONN")
 * 										  there is no connection established or a dedicated channel in use
 * 					  GSM_CANT_FIND_SUITABLE_CELL - The MS is searching, but could not (yet) find a suitable cell. 
 * 											  This output appears after restart of the MS or after loss of coverage. (PhysCh(3G),GPRS(2G) == "SEARCH")
 * 			 		  GSMAT_NOT_SUPPORTED	-  not a 3G radio
 * 	`				  GSM_NOT_READY			- radio is during initialization
 *					  GSM_MISSING_PARAM		- user didn't allocate memory for list_of_cells 
 * 					  GSM_FAIL 				- failed to send command
 * 					  GSM_CELL_NOT_AVAILABLE - no cell found / parsing issue
 * 					
 * 
 *  params:			struct serving_cell_3G{
 * 						  int srv_cell_type; 							// GSM_TYPE_2G, GSM_TYPE_3G
 * 						  struct gsm_2G_srv_cell srv_cell_struct_2G;
 *						  struct gsm_3G_srv_cell srv_cell_struct_3G;
 * 					!!!! the struct that should be read by the user is chosen accordinf to the "srv_cell_type" param !!!!!!!!!!!!!!!!!!
 * 					size : sizeof(	struct serving_cell_3G)
 *
 * 	Description 	: Get the Serving cell info. 
 * 					 case of a srv_cell_type =  GSM_TYPE_2G --> check srv_cell_struct_2G 
 * 					 case of a srv_cell_type =  GSM_TYPE_3G --> check srv_cell_struct_3G 
 * 
 * 
** ************************************************************************************************/

int gsm_3G_monitor_serving_cell(struct serving_cell_3G *list_of_cells,int size);

/** ************************************************************************************************
 *  
 * 	Function Name	: gsm_sim_select
 * 	Return Value	: GSM_OK - success
 * 			 		  GSMAT_NOT_SUPPORTED	-  radio doesn't support dual SIM
 * 	`				  GSM_NOT_READY			- radio isn't off
 *					  GSM_INVALID_PARAM		- SIM number isincorrect
 * 					  GSM_FAIL 			- failed to send request to the driver
 * 
 *  params:			sim_number - GSM_SIM_1, GSM_SIM_2
 *
 * 	Description 	: for devices that support dual SIM, this function enable SIM selection
			  !!! after reboot the SIM will switch back to SIM number 1. it's up to the user to remember
			      the selected SIM and to switch back to it in case of SIM 2  !!!!
 * 
** ************************************************************************************************/

int gsm_sim_select(int sim_number);


/** ************************************************************************************************
 *  
 * 	Function Name	: gsm_get_dev_path
 * 	Return Value	: GSM_OK - success
 * 			 		  GSM_MISSING_PARAM	-  dev_path == NULL 
 * 	`				  GSM_NOT_READY		- radio isn't on/ device not ready yet
 *					  GSM_DEV_STR_TOO_LONG	- memory allocated by user for dev path is to small(short)
 * 					  GSM_INVALID_PARAM 	- "type" is not valid
 * 
 *  params:			type - at this time only GSM_DATA_PATH == 0  is supported 
				dev_path - pointer to memory allocated by the user for the device path
				dev_path_length - size (max length) of the bufffer allocated for dev_path
 *
 * 	Description 	: this function return the full path for the device used for all data connection (GPRS/3G/CSD)
 * 
** ************************************************************************************************/

int gsm_get_dev_path(int type, char *dev_path, int dev_path_length);

#ifdef __cplusplus
}
#endif

#endif /* _LIBGSM_H_ */

