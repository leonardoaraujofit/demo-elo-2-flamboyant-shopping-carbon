#ifndef __SYSM_IP_INPUT__
#define __SYSM_IP_INPUT__

#include <string>
#include <sysmode_ui/input.h>

namespace SYSM_UI
{
	class input_ip_c: public input_c
	{
	private:
		std::string get_last_octet(const char *text, int position);
		int get_number_of_dots(const char *text);
	protected: 
		int handle(int event);
	public:
		input_ip_c(int, int, int, int, const char *, unsigned int);
	};
};

#endif

