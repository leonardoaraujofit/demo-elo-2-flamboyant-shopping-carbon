#ifndef __SYSM_SETTING_SLIDER__
#define __SYSM_SETTING_SLIDER__

#include <sysmode_ui/setting.h>
#include <event_engine/event.h>
#include <FL/Fl_Slider.H>

namespace SYSM_UI
{
	struct slider_params {
		double min;
		double max;
		double step;
		double default_val;
	};

	class setting_slider_c: public setting_c
	{
		Fl_Slider *m_slider;
	public:
		setting_slider_c(const char *name,
			event_listener_id_t listener_id,
			event_listener_id_t list_listener_id,
			struct slider_params);
		~setting_slider_c();

		void set_value(double);
		double  get_value() const;

		using event_listener_c::send_event;
		bool send_event(const double new_val);
	protected:
		void on_event(const event_c *event);
		int handle(int event);
	};
};
#endif
