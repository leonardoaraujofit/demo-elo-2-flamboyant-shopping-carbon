/**
* @file sysm_extension.h
*/

#pragma once

/*!
    \brief the function is called before the main() function of the
     extension, initializing the socket connection. While the socket
     is connected, the launcher application will assume, that
     the extension is being executed
*/
void _extension_init(void);

#ifdef __SYSM_EXTENSION
    __attribute__((constructor)) void _einit(void) { _extension_init(); };
#endif

