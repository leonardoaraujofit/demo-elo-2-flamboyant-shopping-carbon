#ifndef __SYSMODE_UI_ALPHA_KEYPAD__
#define __SYSMODE_UI_ALPHA_KEYPAD__

#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Browser.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Scrollbar.H>
#include <FL/Fl_PNG_Image.H>
#include <FL/Fl_Table.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Secret_Input.H>
#include <FL/Fl_Select_Browser.H>
#include <FL/Fl_Text_Display.H>
#include <FL/Fl_Choice.H>

#include <vector>
#include <pthread.h>
#include <string>
#include <time.h>
#include <map>

#include <sysmode_ui/sysm_ui.h>
#include <sysmode_ui/property.h>



namespace SYSM_UI
{

	class Sysm_AlphaNum_Key : public Sysm_Button
	{
	private:
		void *data;
	protected:
		int handle(int e);
	public:
		void callback(void *d = 0);
		Sysm_AlphaNum_Key(int x, int y, int w, int h, const char * t,
					const char * fi = NULL,
					const char * ui = NULL):
					Sysm_Button(x,y,w,h,t,fi,ui)
					{
						this->data=0;
					};
	};
	
	class Sysm_Keypad_AlphaNum : public Fl_Group
	{
	public:
		
		typedef enum {
			TK_DASH = 0,
			TK_ONE,
			TK_TWO,
			TK_THREE,
			TK_FOUR,
			TK_FIVE,
			TK_SIX,
			TK_SEVEN,
			TK_EIGHT,
			TK_NINE,
			TK_ZERO,
			TK_BACKSPACE,
			TK_EQUAL,
			TK_Q,
			TK_W,
			TK_E,
			TK_R,
			TK_T,
			TK_Y,
			TK_U,
			TK_I,
			TK_O,
			TK_P,
			TK_ESCAPE,
			TK_CAPS,
			TK_A,
			TK_S,
			TK_D,
			TK_F,
			TK_G,
			TK_H,
			TK_J,
			TK_K,
			TK_L,
			TK_SEMICLN,
			TK_ENTER,
			TK_SHIFT,
			TK_Z,
			TK_X,
			TK_C,
			TK_V,
			TK_B,
			TK_N,
			TK_M,
			TK_COMMA,
			TK_PERIOD,
			TK_SLASH,
			TK_SPACE,
			TK_MAX_KEYS
		} t_key_id;
		
		typedef struct callback_data {
			Fl_Widget *widget;
			Sysm_Keypad_AlphaNum *keypad;
			t_key_id key_id;
		} t_callback_data;
		
		typedef struct key_param {
			int x;
			int y;
			int w;
			int h;
			char label[16];
			t_callback_data cb_data;
		} t_key_param;
		
		
		/*
		 * x, y, w, h defines location and total size of keypad, all key_param_info will be fit within w,h
		 */
		Sysm_Keypad_AlphaNum(int x, int y, int w, int h,
					Fl_Widget *input,
					const char *t=0); // keypad title
		~Sysm_Keypad_AlphaNum() { }
		
		t_key_param *get_key_param(t_key_id key_id);
		void process_key(Fl_Window* widget, t_key_id key_id);
		void shift_selected();
		void caps_selected();
		bool isShiftSelected() {return shift_on;};
		
	protected:
	
	private:
		Fl_Box *frame;
		Fl_Box *title;
		t_key_param key_param_info[TK_MAX_KEYS];
		Sysm_AlphaNum_Key* key_list[TK_MAX_KEYS];
		bool shift_on;
		bool caps_on;
	};
};


#endif // __SYSMODE_UI_KEYPAD__
