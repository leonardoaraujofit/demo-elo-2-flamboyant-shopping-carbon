#ifndef __SYSM_SETTING_BUTTON__
#define __SYSM_SETTING_BUTTON__

#include <sysmode_ui/sysm_ui.h>
#include <event_engine/event.h>
#include <event_engine/event_types.h>
#include <string>

namespace SYSM_UI
{
	class setting_button_c : public setting_c
	{
		Sysm_Button * m_button;
		std::string m_label_copy;
	public:
		setting_button_c(const char *title, 
				 event_listener_id_t lisneter_id, 
				 event_listener_id_t list_listener_id);
		~setting_button_c();

		using event_listener_c::send_event;
		bool send_event();

		void update_label_from_copy();
	protected:
		void on_event(const event_c *event);
	};
};
#endif
