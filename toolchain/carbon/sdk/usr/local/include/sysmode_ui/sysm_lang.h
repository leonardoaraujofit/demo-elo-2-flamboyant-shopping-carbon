#ifndef __SYSM_LANG__
#define __SYSM_LANG__

extern "C" {
    #include <libintl.h>
}

#define gettext_noop(String) String

#endif
