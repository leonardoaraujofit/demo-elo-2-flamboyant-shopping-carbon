#ifndef __SYSMODE_UI__
#define __SYSMODE_UI__

#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Browser.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Scrollbar.H>
#include <FL/Fl_PNG_Image.H>
#include <FL/Fl_Table.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Secret_Input.H>
#include <FL/Fl_Select_Browser.H>
#include <FL/Fl_Text_Display.H>
#include <FL/Fl_Choice.H>

#include <vector>
#include <pthread.h>
#include <string>
#include <time.h>
#include <map>

#include <sysmode_ui/input_char.h>
#include <sysmode_ui/sysm_switch_button.h>
#include <sysmode_ui/sysm_scroll.h>
#include <sysmode_ui/setting.h>
#include <sysmode_ui/setting_onoff.h>
#include <sysmode_ui/sysm_lang.h>

using std::string;
using std::vector;
using std::map;

// macro to avoid warnings on not used parameters
#define UNUSED(x)	(void)(x)

#define ACCESS_LIST_SIZE    1025        // Limited by iniparser!
#define NUM_LOGINS          4
#define WINDOW_CHECK_TIME	1

/*!
    As the UI, is a different part from everything else
    It is useful to place definitions into separate namespace
    so it would be seen in the code, where actually we work
    with UI
*/
namespace SYSM_UI
{
	extern const char * BATTERY_HIGH ;
	extern const char * BATTERY_LOW;
	extern const char * BATTERY_CRITICAL;
	extern const char * BATTERY_CHARGING;
	extern const char * BATTERY_ABSENT;
	extern const char * BATTERY_FULL;
	extern const char * BATTERY_UNKNOWN;
	extern const char * CANCEL_PROGR_STR;
	extern const char * EMPTY_DISPLAY;
    
    extern const char * LABEL_OK;
    extern const char * LABEL_CANCEL;
	extern const char * LABEL_YES;
	extern const char * LABEL_NO;

    const int KEY_M0     = FL_F+1; // F1
    const int KEY_M1     = FL_F+2; // F2
    const int KEY_M2     = FL_F+3; // F3
    const int KEY_M3     = FL_F+4; // F4
    const int KEY_INFO   = FL_Home; // UX300 Info key
    const int KEY_SUBMIT = FL_Insert; // VX675 Submit key
    const int KEY_ASTR   = 0x002a; // '*'
    const int KEY_HASH   = 0x0023; // '#'

    const unsigned int SYSM_OK       = 1;
    const unsigned int SYSM_FAILED   = 0;

    const unsigned int MAX_PATH_LEN = 1024;
    const unsigned int MAX_FILE_LEN = 128;
    const unsigned int MAX_URL_LEN   = 100;
    const unsigned int PORT_LEN      = 5;

    const unsigned int V_DECREASE_RATE = 20;
    const float        V_MIN         = 0.0005;
    const unsigned int V_COEFFICIENT = 30;

	extern char accessList[NUM_LOGINS][ACCESS_LIST_SIZE]; // For policy filter
    
	enum {
		WINDOW_NON_CLOSABLE = 0,
		WINDOW_CLOSABLE = 1,
	};

    typedef void (* widget_cb )(Fl_Widget*, void*);

    class Sysm_Window : public Fl_Double_Window
    {
        int closable;
    protected:
        int handle(int event);
    public:
        Sysm_Window(int x, int y, int w, int h, const char * t = 0, int closable = 0):
            Fl_Double_Window(x, y, w, h, gettext(t))
        {
            this->closable = closable;
        }

	void set_closable(int closable) { this->closable = closable; };
    };

    // Background image
    class Sysm_Bkg : public Fl_Widget
    {
        Fl_PNG_Image * bkg;
        void draw();
    public:
        Sysm_Bkg(int, int, int, int, const char * t = 0);
        ~Sysm_Bkg();
    };

	extern Sysm_Bkg * SPINNER_BKG;
    // Status bar
    typedef enum {
        SB_ARROW_LEFT  = 0,
        SB_ARROW_RIGHT,
        SB_ARROW_UP,
        SB_ARROW_DOWN,
    } t_sbar_arrow_id;


    typedef struct sbar_arrow{
        Fl_PNG_Image * arrow;
        unsigned int   visible;

        sbar_arrow(const char * img, unsigned int state):
            arrow(new Fl_PNG_Image(img)), visible(state)
        {};

        ~sbar_arrow()
        { delete(arrow); };
        
    } t_sbar_arrow;


    typedef enum {
        SB_LOGO  = 0,
        SB_LOGO_BACK
    } t_sbar_logo_id;

    typedef struct sbar_logo {
        Fl_PNG_Image * logo;
        unsigned int   visible;

        sbar_logo(const char * img, unsigned int state):
            logo(new Fl_PNG_Image(img)), visible(state)
        {};

        ~sbar_logo()
        { delete(logo); };
    } t_sbar_logo;


    class Sysm_Statusbar : public Fl_Box
    {
        Fl_PNG_Image * bkg;
        vector<t_sbar_logo *> logos;
        vector<t_sbar_arrow *> arrows;
        Fl_Window *handler_push;
        void draw();

    protected:
        int handle(int);

    public:
        Sysm_Statusbar(int, int, int, int, const char * = "");
        void show_logo(t_sbar_logo_id logo_id);
        void hide_logo(t_sbar_logo_id logo_id);
        void hide_logo_all(void);
        void show_arrow(t_sbar_arrow_id arrow_id);
        void hide_arrow(t_sbar_arrow_id arrow_id);
        void set_handler(Fl_Window *w);
        void clear_handler(void);
        ~Sysm_Statusbar();
    };

    /*!
        The implementation of the button class, specificaly suitable for
        the list menu. As FLTK framework doesn't provide some of the
        features, that we need, it is necessary to subclass the standard
        button.
        The current button changes color when focused
    */
    class Sysm_Button : public Fl_Button
    {
    protected:
        void draw();
        char * full_label;
        char * vis_label;
        unsigned int label_char_pos;
        Fl_Image * focus_icon;
        Fl_Image * unfocus_icon;
        int handle(int e);
        void set_shrink_label();
    public:
        void label_move_one_char(); 
        int end_of_label_visible();
        Sysm_Button(int, int, int, int, const char * t,
                                        const char * fi = NULL, 
                                        const char * ui = NULL);
        const char * get_full_label();
        void icon_focus(Fl_Image * img);
        void icon_unfocus(Fl_Image *img);
        ~Sysm_Button();
    };



    typedef vector<string> table_row_t;
    typedef vector<table_row_t*> table_content_t;

    class Sysm_Table: public Sysm_Scroll
    {
    public:
        Sysm_Table(int, int, int, int, const char *);
        void add_row(const table_row_t &row);
    };

    /*!
        The carousel class. Will display elements, which can
        be scrolled horizontally in a loop.
    */
    class Sysm_Carousel : public Fl_Group
    {
        Fl_Group        * scrollarea;
        int             handle(int);
        int             trans_interrupt;
        Fl_Widget       * current;
        Fl_Widget       * next;
        Fl_Widget       * prev;
        
        clock_t         s_start_time;
        // This value will be iterated every move
        int             s_start_x;

        // Those two values will be constant during
        // the whole gesture, and used to send the
        // vertical drag to child widget
        int             s_begin_x;
        int             s_begin_y;
        int             h_push_sent;

        int             s_prev_x;
        int             s_drag_x;
        void            slide(Fl_Widget *, Fl_Widget *, int);
        void            reposition();

    public:

        int             set_current();
        int             in_drag;
        float           final_v;
        int             k_scroll(int);
        float           velocity();
        int             timeout_set;

        void            next_transition();
        void            prev_transition();
        void            add_e(Fl_Widget *);
        Sysm_Carousel(int, int, int, int, const char *);
        ~Sysm_Carousel();
    };


    /*!
        The class, which implements the viewing of the text. It does not
        wrap the text, but rather gives the user a possibility to pan
        the text in different directions. It also supports the possibility
        for text search, which will highlight the required words
    */
    class Sysm_Text_Viewer : public Fl_Group
    {
        int start_mouse_x;
        int start_mouse_y;
        int start_x;
        int start_y;
        
        Fl_Text_Display * txt_display;
        int handle(int);
        
    public:
        Sysm_Text_Viewer(int, int, int, int, const char *);
        
    };


    class Sysm_Battery_Indicator : public Fl_Widget
    {
        void draw(void);
        int percentage;
        const char * state;
        Fl_Color battery_color;
    public:
        int(* updater)(const char **, int *);
        Sysm_Battery_Indicator(int, int, int, int, int(*)(const char **, int *));
        ~Sysm_Battery_Indicator();
        void set_state(const char *, int);
    };

    /*
        The implementation of the system clock, which can shows
        system date/time and provide possibility to change it
    */
    class Sysm_Clock : public Fl_Group
    {
        Fl_Box       *title;
        Sysm_Button  *day_button;
        Sysm_Button  *month_button;
        Sysm_Button  *year_button;
        Sysm_Button  *time_button;
        Sysm_Button  *tmzone_button;
        Fl_Box       *title_reboot;
        Sysm_Button  *time_reboot_button;
        int handle(int);
    public:

        int edit_mode; 
        unsigned int allowed_to_exit;

        Sysm_Clock(int x, int y, int w, int h,
                   widget_cb day_cb,widget_cb month_cb,
                   widget_cb year_cb,widget_cb time_cb,
                   widget_cb tmzone_cb,
                   widget_cb time_reboot_cb,
                   const char *l=0, const char *l_24h=0);

        ~Sysm_Clock();
        void set_timer_value( const char* day,const char* month,
                             const char* year,const char* time,
                             const char* tmzone);
        void set_timer_24h_value(const char* time);
        static void timer_idle( void* );
    };

    typedef enum {
        IP_INPUT              = 0,
        URL_INPUT,
        PORT_INPUT,
    } NETWORK_INPUT_TYPE;


    /*!
       Class implements the input for network services, like IP address,
        Port, etc.
    */
    class Sysm_Network_Input : public Fl_Input
    {
    NETWORK_INPUT_TYPE type;
    public:
        Sysm_Network_Input (int x,int y,int w,int h,const char* l,
                       NETWORK_INPUT_TYPE type):Fl_Input(x,y,w,h,l){
                        this->type = type;};
        int handle(int event);
    };

	/*!
       Class implements the input for generic type
    */
    class Sysm_Input : public Fl_Input
    {
    public:
        Sysm_Input (int x,int y,int w,int h,const char* l):Fl_Input(x,y,w,h,l){ };
        int handle(int event);
    };

    /*!
       Class implements the input for secret password services. 
    */
    class Sysm_Password_Input : public Fl_Secret_Input
    {
    public:
        Sysm_Password_Input (int x,int y,int w,int h,const char* l):Fl_Secret_Input(x,y,w,h,l){};
        int handle(int event);
    };   

    /*!
       Class implements the progress wheel which is spinning
        indicating, that something is in progress 
    */
    class Sysm_Progress_Spinner: public Fl_Widget
    {
        double cur_degree1;
        double cur_degree2;
        void draw();
    public:
        Sysm_Progress_Spinner(int, int, int, int);
        ~Sysm_Progress_Spinner();
        void set_new_degree1(double);
        void set_new_degree2(double);
        double get_degree1();
        double get_degree2();
        void start_spin();
        void zero_both_degrees();
    };


    typedef enum
    {
        DATE_TIME_HM,            // HH:MM
        DATE_TIME_HMS,           // HH:MM:SS
    } DATE_MODE;
          

    class Sysm_Time_Input: public Fl_Input
    {
        DATE_MODE mode;
        struct tm time;
        int handle(int event);
        void update_time();
        void update_tm_struct();
    public:
        unsigned int get_seconds;
        void set_mode(DATE_MODE mode);
        void set_time_conf_format(unsigned int conf_time);
        unsigned int get_time_conf_format();
        Sysm_Time_Input(int, int, int, int, unsigned int);
    };

    typedef enum
    {
        ON_OFF      = 1,
        RONLY,
        INPUT,
        DROPDOWN,
        SCANLIST,
        PASS,
        TIME,
        SLIDER,
        IP_ADDRESS,
        IP_ADDRESS_RONLY
    } SETTING_TYPE;


    /*!
        Class implements the element which is stored in the settings list
    */
     /* The type describes different types of modal windows. Variable
        of this type is used for display_modal_win(), which, depending
        on the argument passed displays different type of modal window,
        with different number of buttons
        TODO: The complete list should be defined
    */
    typedef enum {
        QUESTION               = 0,
        TIMEOUT,
        INFO,
        ERROR,
        YESNO,
    } MODAL_TYPE;


    /*!
        The type describes different types of input. Depending on the value
        passed to the function, which displays input window, it will either
        perform input checks or hide the input
        TODO: The complete list should be define
    */
    typedef enum {
        INPUT_TYPE_ALPHANUM,
        INPUT_TYPE_NUMERIC,
        INPUT_TYPE_PASSWORD,
        INPUT_TYPE_GPRS_PIN,  // plain pin, no Vault
        INPUT_TYPE_IP,
        INPUT_TYPE_PORT,
        INPUT_TYPE_URL,
        INPUT_TYPE_PASSWORD_PCI4
    } INPUT_TYPE;


    /*!
        The type is used for input field checks. This is return type for
        input field check callbacks, which are defined by developers
    */
    typedef enum {
        CHECK_FAILED          = 0,
        CHECK_OK, 
        CHECK_CHNGD,
        CHECK_CANCEL,
        CHECK_TIMEOUT,
        CHECK_NO_TOKENS,
        CHECK_MIN_LEN
    } INPUT_CHECK_STATUS;

    /*!
        This is a pointer to function type for the input check
        callback
    */
    typedef INPUT_CHECK_STATUS (input_check)(const char *pwd);

    /*!
        The type of structure, which defines the callbacks,
        that can be passed to the input field function. Any of
        values in the structure can be NULL.
    */
    typedef struct input_checks {
        input_check * pre_check;
        input_check * submit_check;
    } t_input_checks;


    typedef struct ui_menu_entry t_ui_menu_entry;

    typedef struct ui_menu_list  t_ui_menu_list;

    typedef void (* f_menu_cb)(const t_ui_menu_entry *);

    typedef int (* f_menu_entry_filter)(const t_ui_menu_entry *);
    
    typedef vector<t_ui_menu_entry> entry_list_t;

    struct ui_menu_entry {
        int                       id;
        const char              * title;
        f_menu_cb                 menu_cb;
        void                    * usr_data;
        const t_ui_menu_list    * child;
        t_ui_menu_list          * parent; // For back navigation from callback
        const char              * icon_path;
        const char              * icon_inact_path;
    };

    struct ui_menu_list {
        int                       id;
        const char              * title;
        entry_list_t            * list;
        unsigned int              len;
        const t_ui_menu_list    * parent;
        f_menu_entry_filter       entry_filter;
    };


    /*!
        Function to display the menu list. Menu items are listed
        one by one. User can navigate the menu list, going up or down.
        Selected menu item either displays another menu list, or launches
        the callback. There is possibility to go to a parent menu
        on certain key stroke.
        \param  title of the menu list, which is displayed on top of the menu
        \param  menu_entries a list of menu items to be displayed
        \param  the length of the menu list
    */
    int display_menu_list(const t_ui_menu_list *, int block_flags = 0);

    /*!
        Function, which displays the modal window. It is a configurable
        window. It may display either the question, presenting the user
        several buttons, or information/error presenting user only
        one button to close the window.
        Function is blocking, which means, it won't return until user presses
        the button on the window. Function will return either 0 or 1 depending
        on the user choice.
        The type of the window is controled with the MODAL_TYPE enum value
        \note if it is information window, the return value may be ignored
        \param  msg the text to be presented to the user
        \param  type the type of the window (see MODAL_TYPE);
        \param  timeout for the window which should close after certain time
        \return the value, chosen by user
    */
    int display_modal_win(const char *,
                          MODAL_TYPE = INFO, 
                          double timeout = 0.0);

    /*!
        Function displays a modal window with buttons.
        Function is blocking, it won't return until user presses
        the button on the window. Returns index of pressed button.
        \param  msg the text to be presented to the user
        \param  btns text on buttons;
        \param  btns_count btns length
        \param  timeout for the window which should close after certain time use 0.0 if timeout is not needed
        \return button index
    */
    int display_modal_win_btns(const char *msg,
                               const char ** btns,
                               size_t btns_count,
                               double timeout);

    /*!
        Function, which displays the input prompt, on the whole screen.
        It is configurable via INPUT_TYPE argument, to present
        different types of input (alpha-numeric, numeric, password, etc.)
        Function is non-blocking, it will just display the input field,
        and return
        \param title a name of the field, which is displayed on the top
        \param type the type of input, which field accepts
        \param callback to check for constraints
        \param callback which is called, when input is submitted
    */
    INPUT_CHECK_STATUS display_input(
                      const char            * title,
                      INPUT_TYPE              type,
                      char                  * buffer,
                      int                     buffer_size,
                      const char            * default_value = "",
                      const t_input_checks  * input_checks  = NULL,
                      unsigned char         * sha_value     = NULL);


    table_row_t * entry(const char * title);
    table_row_t * entry(const char * title, const char * value);
    table_row_t * entry_format(const char *title, const char *format, ...);

    void free_table_entries(table_content_t &table_content);

    void display_table(const table_content_t &table_content);

    void display_text(const char *);

    void display_banner(const char *);

    void display_timer(Sysm_Clock* clock_widget);

    void display_browser(vector<string> &list, const char* name, 
                                          widget_cb b_cb = NULL);

    /*
        For certain purposes, we want to limit the user to path a fixed
        length array. This eases the processing, and is less error-prone
        As we know the length of the array, and will not allow any overflow
    */
    int display_files(const char *,
                       char (&)[MAX_PATH_LEN], 
                       const char *,
                       char (&)[MAX_FILE_LEN]);

    typedef vector<table_content_t*> carousel_t;

    void display_info_carousel(const carousel_t &carousel);

    void free_carousel_entries(carousel_t &carousel);

    void display_battery(const char * state, int percentage, 
                        int(*)(const char **, int *));

    typedef int (* f_action_cb)(void *, Fl_Box *);

    typedef void (* timer_idle_func )(void *);

    typedef struct {
        f_action_cb     action;
        void            *data;
        Fl_Box          *msg_box;
    }s_action_cb;

    int display_progress_win(const char *, f_action_cb, 
                        void * data = NULL, 
                        bool cancle = false,
                        void (*cancel_cb)(void *) = NULL,
                        void * cb_data = NULL);

    void display_calibration_win();
    void calibration_cb(int);
    void add_calibration_point(int, int);
    void hide_calibration_win();

    unsigned int get_screen_bpp();
    void init_main_win(int closable = 0);
    void show_main_win();
    void hide_main_win();
	Sysm_Window * get_main_win();

    Sysm_Statusbar * statusbar();

    void Sysm_System(char *input);
	
	void disableTouchCalibration();
	void enableTouchCalibration();
	bool getAllowTouchCalibration();
	
	void setTouchCalibrationStarted(bool state);
	bool getTouchCalibrationStatus();
	
    int event_key(void);

    /* Select what files to be displayed in file browser */
    void set_file_browser_filter(std::vector<const char*> *show_pattern,
                                 std::vector<const char*> *hide_pattern);
	
	int getCurrentMenuID(void);
	void setCurrentMenuID(int menu_id);

    void set_file_manager(bool);
};


void 			sysmUi_saveLastDirPath(unsigned char _save);
unsigned char		sysmUi_isToSaveLastDirPath(void);

#endif // __SYSMODE_UI__
