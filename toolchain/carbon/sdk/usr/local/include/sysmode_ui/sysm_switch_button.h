#ifndef __SYSM_SWITCH_BUTTON__
#define __SYSM_SWITCH_BUTTON__

#include <FL/Fl_PNG_Image.H>

namespace SYSM_UI
{

    class Sysm_Switch_Button: public Fl_Button
    {
        Fl_PNG_Image * state_imgs[2];
        Fl_PNG_Image * state_image;
        void draw(void);
        int handle(int e);
        unsigned int state;

    public:
        void set_state(const unsigned int);
	void set_state_value(const unsigned int); // No redraw
        unsigned int get_state(void);
        Sysm_Switch_Button(int, int, int , int, const char * title = 0, 
                                                const char * img_on = 0, 
                                                const char * img_off = 0,
                                                unsigned int state = 0);
        ~Sysm_Switch_Button();
    };
}

#endif
