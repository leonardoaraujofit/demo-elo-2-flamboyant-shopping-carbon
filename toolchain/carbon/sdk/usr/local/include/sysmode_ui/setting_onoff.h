#ifndef __SYSM_SETTING_ONOFF__
#define __SYSM_SETTING_ONOFF__

#include <sysmode_ui/sysm_switch_button.h>

#include <event_engine/event.h>
#include <event_engine/event_types.h>


namespace SYSM_UI
{
	void switch_callback(Fl_Widget *, void *);

	class setting_onoff_c : public setting_c
	{
		Sysm_Switch_Button *swtch;
	public:
		setting_onoff_c(const char *name,
				event_listener_id_t listener_id, 
				    event_listener_id_t list_listener_id,
				unsigned int default_value = 0,
				const char *img_on = 0, 
				const char *img_off = 0);

		~setting_onoff_c();

		using event_listener_c::send_event;
		bool send_event(const unsigned int new_val);
		unsigned int get_state(void);
		void set_value(const unsigned int value);

	protected:
		void on_event(const event_c *event);

	};
}

#endif
