#ifndef __SYSM_PROGRESS_WINDOW__
#define __SYSM_PROGRESS_WINDOW__

#include <sysmode_ui/sysm_ui.h>
#include <string>

namespace SYSM_UI
{
	class progress_window_c: public Sysm_Window
	{
		std::string m_current_msg;
		Fl_Box *m_msg;
		Sysm_Progress_Spinner *m_spinner;
	public:
		progress_window_c(int x, int y, int w, int h);
		~progress_window_c();
		void set_msg(const char *msg);
	};
};
#endif
