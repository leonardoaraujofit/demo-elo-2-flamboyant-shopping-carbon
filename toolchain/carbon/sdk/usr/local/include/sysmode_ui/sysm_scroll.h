#ifndef __SYSM_SCROLL__
#define __SYSM_SCROLL__

#include <FL/Fl_Scrollbar.H>
#include <FL/Fl_Group.H>
#include <time.h>
#include <sysmode_ui/sysm_lang.h>

namespace SYSM_UI
{
    typedef enum {
        ESCAPE_BLOCK 	= 0x0001,
        CALLBACK_BLOCK 	= 0x0002,
    }  BLOCK_FLAG;

    /*!
        The implementation of the scroll class, which moves scroll
        automatically, when the focused item appers to be out
        of sight.
    */
    void scroll_cb(Fl_Widget *w, void *data);
    class Sysm_Scroll : public Fl_Group
    {
        clock_t         s_start_time;
        int             s_start_y;
        int             s_prev_y;
        int             s_drag_y;
        int             s_drag_start;
        int             s_drag_start_y;
        int             selected;
        int             over_push_barrier;
        unsigned int    s_block;
        BLOCK_FLAG      unblock_event;

    protected:
        int             handle(int);

    public:
        unsigned int    num_navi;
        int             vsep;
        int             in_drag;
        float           final_v;
        int             k_scroll(int);
        float           velocity();
        int             timeout_set;
        Fl_Scrollbar    * scrollbar;
        Fl_Group        * scrollarea;
        int             line_h;
        void            show_transition();
        void            hide_transition();
        void            add_e(Fl_Widget *);
        void            custom_push_event();
        void            custom_drag_event();
        void            custom_down_event();
        void            custom_release_event();
        void            set_callback_data(void *);
        int             block(int flags);
        int             blocked();
        int             unblock(BLOCK_FLAG flag);
	void		display();
	void		display_animated();

	void		set_selected(int);
	int		get_selected(void);

        Sysm_Scroll(int, int, int, int, const char *);
        ~Sysm_Scroll();
    };
}
#endif
