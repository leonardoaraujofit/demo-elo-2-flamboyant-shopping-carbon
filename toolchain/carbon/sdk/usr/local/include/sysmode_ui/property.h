/*
 *	Copyright, 2015 VeriFone Inc.
 *	88 West Plumeria Dr.
 *	San Jose, CA.  95134
 *
 *  All Rights Reserved. No part of this software may be reproduced,
 *  transmitted, transcribed, stored in a retrieval system, or
 *  translated into any language or computer language, in any form
 *  or by any means electronic, mechanical, magnetic, optical,
 *  chemical, manual or otherwise, without the prior permission of
 *  VeriFone Inc.
 */
#ifndef __SYSM_PROPERTY__
#define __SYSM_PROPERTY__

#include <string>
#include "svcmgr/svc_sysinfo.h"

#include "../include/sysm_info.h"
#include <FL/fl_draw.H>
#include <vector>
using std::vector;

#define MAX_FBDEV_LEN  16
/*
 * Usage:
 *   property * prp = property::getInstance();
 *   property::displayAttribute * display = prp->display;
 *   int display_width = display->width();
 *   int display_height = display->height();
 *
 */

/*
 * This is a temporary measure that is to be removed once the diag counters API
 *  has been cleaned up to allow for multiple systems to access correctly without
 *  the use of '#ifdefs's and the like.
 */
#ifdef RAPTOR
#define SYSTEM_MODE_ENTRY_FAILED_CNT_INC	diag_counter_inc(DIAG_COUNT_SYSTEM_MODE_ENTRY_FAILED)
#define SYSTEM_MODE_ENTERED_CNT_INC			diag_counter_inc(DIAG_COUNT_SYSTEM_MODE_ENTERED)
#define SYSTEM_MODE_PASSWORD_CHANGE_TIME	diag_counter_set_time( DIAG_COUNT_SYSTEM_MODE_PASSWORD_CHANGE_TIME);
#else
#define SYSTEM_MODE_ENTRY_FAILED_CNT_INC
#define SYSTEM_MODE_ENTERED_CNT_INC
#define SYSTEM_MODE_PASSWORD_CHANGE_TIME
#endif

static const unsigned int USB1_ENABLE_MASK   = 1;
static const unsigned int USB2_ENABLE_MASK   = 2;

namespace SYSM_UI
{
	
	// This is a singleton class
	class property
	{
	public:
		class objectAttribute
		{
			friend class property;
		public:
			Fl_Color color(void) {return s_color;}
			Fl_Color fontcolor(void) {return s_fontcolor;}
			Fl_Font font(void) {return s_font;}
			int fontsize(void) {return s_fontsize;}
			int x(void) {return s_x;}
			int y(void) {return s_y;}
			int w(void) {return s_w;}
			int h(void) {return s_h;}
			char *imagepath(void) {return s_imagepath;}
			bool isHidden(void) {return s_is_hidden;}
			bool isHorizPageFlow(void) {return s_is_horizpageflow;}
			Fl_Align alignment(void) {return s_alignment;}
		private:
			Fl_Color s_color; // color of item (can use fl_rgb_color(r, g, b);)
			Fl_Color s_fontcolor; // color of font (can use fl_rgb_color(r, g, b);)
			Fl_Font s_font; //font type to use 
			int s_fontsize; // size of font
			int s_x; // x pixel location or offset (upper left)
			int s_y; // y pixel location or offset (upper left)
			int s_w; // width in pixels
			int s_h; // height in pixels
			char *s_imagepath; // path/filename of background image
			bool s_is_hidden; // true if object must be hidden
			bool s_is_horizpageflow; //true if page uses horiztonal page scrolling (like Software Info)
			Fl_Align s_alignment; // alignment of text/bitmap/box within box or group
		};

		class displayAttribute
		{
			friend class property;
		public:
			bool haveDisplay(void) {return s_have_display;}
			bool iscolor(void) {return s_color;}
			bool touchpanel(void) {return s_have_touchpanel;}
			bool isresistiveTouchpanel(void) {return s_resistive_touchpanel;}
			bool calibrateTouchpanel(void) {return s_calibrationRequired_touchpanel;}
			bool calibrateOnStart(void) {return s_calibrateOnStart;}
			bool isLandscape(void) {return s_isLandscape;}
			int  aspectRatio(void) {return s_aspect;}
			int  density(void) {return s_density;}
			int  width(void) {return s_width;}
			int  height(void) {return s_height;}
			char *fbDevice(void) {  char *ptr = 0;
									if (s_fbDevice[0] != 0) {
										ptr = s_fbDevice;
									}
									return ptr;
								}
			int  bitsPerPixel(void) {return s_bitsPerPixel;}
		private:
			bool s_have_display; // true if display present
			bool s_color; // true if color display
			bool s_have_touchpanel; // true if touchpanel present on display
			bool s_resistive_touchpanel; // true if touchpanel is resistive
			bool s_calibrationRequired_touchpanel; // true if calibration of touchpanel is possible/required
			bool s_calibrateOnStart; // true if calibration of touchpanel after reflash is required
			bool s_isLandscape; // true if display is landscape
			double s_aspect; // aspect ratio of display (width/height)
			int s_density; // pixel density - dots/centimeter in x direction (typically same in y)
			int s_bitsPerPixel; // number of bits per pixel
			int s_width; // number of pixels in width
			int s_height; // number of pixels in height
			char s_fbDevice[MAX_FBDEV_LEN];
		};

		class keypadAttribute
		{
			friend class property;
		public:
			bool haveKeypad(void) {return s_have_keypad;}
			typedef enum keypadtype {
				KEYPAD_NONE = 0,
				KEYPAD_STANDARD,
				KEYPAD_STANDARD2
			} t_keypadtype;
			t_keypadtype type(void) {return s_type;}
		private:
			bool s_have_keypad; // true if keypad present
			t_keypadtype s_type; // type of keypad
		};

		class fontAttribute
		{
			friend class property;
		public:
			unsigned int getSystemFont() {return system_font;}
			unsigned int getDefaultFont() {return default_font;}
			unsigned int getSystemFontBold() {return system_font_bold;}
			unsigned int getDefaultFontBold() {return default_font_bold;}
		private:
			unsigned int system_font;
			unsigned int system_font_bold;
			unsigned int default_font;
			unsigned int default_font_bold;
		};
		
		class deviceInfoAttribute
		{
			friend class property;
		public:
			bool allowLoginBypass(void) {return s_loginBypass;}
			bool haveMiniUsb(void) {return s_haveMiniUsb;}
			bool isPortable(void) {return s_isPortable;}
			bool allowAlphaNum(void) {return s_allowAlphaNum;}
			bool soundSupported(void) {return s_soundSupported;}
			bool videoSupported(void) {return s_videoSupported;}
			bool haveMacAddress(void) {return s_haveMacAddress;}
			bool allowMsrTransfer(void) {return s_allowMsrTransfer;}
			bool showInputLabels(void) {return s_showInputLabels;}
			bool allowChngTmZnInDateTime(void) {return s_allowChngTmZnInDateTime;}
		private:
			bool s_loginBypass; // true if bypass of login screens allowed, DEV UNITS ONLY!
			bool s_haveMiniUsb; // true if device has mini USB
			bool s_isPortable; // true if device can run on battery, requires power management
			bool s_allowAlphaNum; // true if device can use the on screen alphanumeric keypad
			bool s_soundSupported; // true if device supports sound
			bool s_videoSupported; // true if device supports video 
			bool s_haveMacAddress; // true if device has a MAC address
			bool s_allowMsrTransfer; // true if device allows MSR Transfer on Main screen
			bool s_showInputLabels; // true if input boxes include labels
			bool s_allowChngTmZnInDateTime; // true if you allow a user to change the timezone from Date/Time
		};

		typedef enum objecttype {
			APP = 0,
			ARROW_DOWN,
			ARROW_LEFT,
			ARROW_RIGHT,
			ARROW_UP,
			AUDIO_ICON_UNFOCUSED,
			AUDIO_PLAY,
			BANNER,
			BATTERY,
			BATTERY_CRIT,
			BATTERY_LOW,
			BATTERY_EMPTY,
			BUTTON,
			BUTTON_SEP,
			BUTTON_UNFOCUSED,
			CALIBRATION_WIN,
			CONSTRUCT_BTN,
			CARD_DIAG,
			DATE_TIME,
			DATE_TIME_TITLE,
			DATE_TIME_DAY,
			DATE_TIME_MONTH,
			DATE_TIME_YEAR,
			DATE_TIME_TIME,
			DATE_TIME_TMZONE,
			DATE_TIME_REBOOT,
			DATE_TIME_REBOOT_TITLE,
			DIRCTORY_ICON_UNFOCUSED,
			DIRECTORY_ICON,
			DROPDOWN,
			FILE_ICON,
			FILE_ICON_UNFOCUSED,
			HASH_MODE,
			HEADER,
			INPUT,
			INPUT_BOX_ALPHANUM,
			INPUT_BOX_NUMERIC,
			INPUT_BOX_OFFSET,
			INPUT_WINDOW,
			INPUT_TEXT,
			IP_DIALOG,
			IP_DIALOG_TEXT,
			KEYPAD,
			KEYPAD_ALPHANUM,
			KEYPAD_KEY,
			LOGO,
			LOGO_BACK,
			M_BUTTON,
			M_BUTTON_FOCUS,
			MENU_BKG,
			MENU_FOCUS,
			MENU_UNFOCUS,
			MODAL_BOX,
			MODAL_WINDOW,
			MOUSE_WINDOW,
			MOUSE_DIAG,
			ONOFF,
			OFF_IMG,
			ON_IMG,
			PASSWORD_ENTRY,
            PIC_ICON_UNFOCUSED,
			PROGRESS_WINDOW,
			PRG_WIN_CNCL_BTN,
			SCROLLBAR,
			SLIDER,
			SOFTWARE_INFO,
			SPINNER,
			STATUSBAR,
			STATUSBAR_LINE,
			SUBSCRIPT,
			TABLE,
			TABLE_ENTRY,
			TEXT,
			TGZ_ICON,
			TGZ_ICON_UNFOCUSED,
			USR_ICON,
			USR_ICON_UNFOCUSED,
			VID_ICON_UNFOCUSED,
			RM_ICON,
			RM_ICON_UNFOCUSED,
			RM_USR_ICON,
			RM_USR_ICON_UNFOCUSED,
			OBJECTTYPE_COUNT
		} t_objecttype;

		// add new platforms to enum
		typedef enum platformType {
			PLATFORM_MX915,
			PLATFORM_MX925,
			PLATFORM_P200,
			PLATFORM_P400,
			PLATFORM_UX300,
			PLATFORM_V200,
			PLATFORM_VX675,
			PLATFORM_VX820,
			PLATFORM_SWRDFSH,
			PLATFORM_M400,
			PLATFORM_V400M,
			PLATFORM_UNKNOWN
		} t_platform;

		t_platform platform(void) {return s_platform;}
		bool isRaptorPlatform(void);
		bool isMxPlatform(void);
		bool audioCodecAvailable(void);
		unsigned int getUsbConfigMask(void) {return usb_config_mask;}
		unsigned int getUsbDefaultsMask(void) {return usb_device_defaults_mask;}
		bool isCPtestConvertSupported(void) {return cptest_conversion_supported;}
		bool isCPtestDefaultUsbSelected(void) {return cptest_default_usb_selected;}

		static property* getInstance();
		~property()
		{
			instanceFlag = false;
		}
		objectAttribute * obj(t_objecttype type) { objectAttribute * p = &(displayObjects[type]); return p;}
		displayAttribute * display;
		keypadAttribute * keypad;
		fontAttribute * font;
		deviceInfoAttribute * deviceInfo;
		DEVICE_MASK bt_support_mask;

	private:
		static bool instanceFlag;
		static property *single;
		t_platform s_platform;
		bool s_isRaptor;
		property();
		vector<objectAttribute> displayObjects;

		// used for USB port HOST/DEVICE configuration (RAPTOR). If bit is set then menu option is displayed
		unsigned int usb_config_mask;
		// defaults for USB port HOST/DEVICE configuration (RAPTOR). Same as in Device Tree: Bit set to 0 - HOST, 1 - DEVICE
		unsigned int usb_device_defaults_mask ;

		bool cptest_conversion_supported; // true if cp appdev coversion is supported
		bool cptest_default_usb_selected; // true if using the default usb ( usb1 ) for cp conversion

		void platformSetup(bool preSetup);

		// property methods specific for each platform
		void property_mx925(bool preSetup);
		void property_ux300(bool preSetup);
		void property_p200(bool preSetup);
		void property_p400(bool preSetup);
		void property_v200(bool preSetup);
		void property_vx820(bool preSetup);
		void property_vx675(bool preSetup);
		void property_swrdfsh(bool preSetup);
		void property_m400(bool preSetup);
		void property_v400m(bool preSetup);

	};

	inline int propertyAttribute_x(property::t_objecttype type)
	{
		property *prp = property::getInstance();
		property::objectAttribute *attr = prp->obj(type);
		return attr->x();
	}
	inline int propertyAttribute_y(property::t_objecttype type)
	{
		property *prp = property::getInstance();
		property::objectAttribute *attr = prp->obj(type);
		return attr->y();
	}

	inline int propertyAttribute_w(property::t_objecttype type)
	{
		property *prp = property::getInstance();
		property::objectAttribute *attr = prp->obj(type);
		return attr->w();
	}
	inline int propertyAttribute_h(property::t_objecttype type)
	{
		property *prp = property::getInstance();
		property::objectAttribute *attr = prp->obj(type);
		return attr->h();
	}
};
#endif
