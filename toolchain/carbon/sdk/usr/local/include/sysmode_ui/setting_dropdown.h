#ifndef __SYSM_SETTING_DROPDOWN__
#define __SYSM_SETTING_DROPDOWN__

#include <event_engine/event.h>
#include <event_engine/event_types.h>
#include <vector>

#include <sysmode_ui/setting.h>
#include <sysmode_ui/sysm_ui.h>

namespace SYSM_UI
{
	class setting_dropdown_c : public virtual setting_c
	{
	private:
		Fl_Choice *mvalue;
		Fl_Box    *arrow_left;
		Fl_Box    *arrow_right;
	public:
		setting_dropdown_c(const char *name, 
				   event_listener_id_t listener_id, 
				   event_listener_id_t list_listener_id,
				   const std::vector<const char *> dd_list,
				   const unsigned int default_value = 0);

		~setting_dropdown_c();
		
		using event_listener_c::send_event;
		bool send_event(const unsigned int new_val);
	protected:
		int handle(int e);
		void on_event(const event_c *event);	
	};
};

#endif
