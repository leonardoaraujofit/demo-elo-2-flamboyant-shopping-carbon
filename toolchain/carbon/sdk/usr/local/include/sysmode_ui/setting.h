#ifndef __SYSM_SETTING__
#define __SYSM_SETTING__

#include <FL/Fl.H>
#include <FL/Fl_Group.H>

#include <event_engine/event_listener.h>
#include <event_engine/event_types.h>
#include <event_engine/event.h>

#include <string>

class event_uint_c : public event_c
{
private:
	unsigned int m_new_value;	
public:	
	event_uint_c(unsigned int new_value,
		event_type_id_t type_id, 
		event_listener_id_t listener_id):
		event_c(type_id, listener_id),
		m_new_value(new_value) 		{};

	unsigned int get_new_value() const
	{
		return m_new_value;
	}
};


class event_double_c : public event_c
{
private:
	double m_new_value;	
public:	
	event_double_c(double new_value,
		event_type_id_t type_id, 
		event_listener_id_t listener_id):
		event_c(type_id, listener_id),
		m_new_value(new_value) 		{};

	double get_new_value() const
	{
		return m_new_value;
	}
};


class event_char_c : public event_c
{
private:
	std::string m_new_value;
public:
	event_char_c(const char * new_value, 
		event_type_id_t type_id,
		event_listener_id_t listener_id):
		event_c(type_id, listener_id),
		m_new_value(new_value) 		{};

	const char * get_new_value() const
	{
		return m_new_value.data();
	}
};


namespace SYSM_UI
{
	enum {
		EVENT_TYPE_ONOFF	= 10,
		EVENT_TYPE_DROPDOWN,	
		EVENT_TYPE_TEXT,
		EVENT_TYPE_RONLY,
		EVENT_TYPE_BUTTON,
		EVENT_TYPE_SLIDER,
		EVENT_TYPE_REF_COUNT_DOWN,
	};

	class setting_c: public Fl_Group, 
		public event_listener_c
	{
	public:
		setting_c(const char * title, 
			event_listener_id_t event_listener_id,
			event_listener_id_t list_listener_id);

		virtual ~setting_c();

		event_listener_id_t get_list_listener() const;
		const char* get_title();

	protected:
		event_listener_id_t list_listener_id; // ID of the parent list listener
		int handle(int event);
		bool send_ref_dec();
	};

}

#endif

