#ifndef __SYSM_TIME_INPUT__
#define __SYSM_TIME_INPUT__

#include <sysmode_ui/input.h>

namespace SYSM_UI
{
	class input_time_c : public input_c
	{
	protected:
		int handle(int event);
	public:
		input_time_c(int, int, int, int, const char *, unsigned int);
	};
}
#endif
