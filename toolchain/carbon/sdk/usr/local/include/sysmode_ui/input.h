#ifndef __SYSM_INPUT__
#define __SYSM_INPUT__

#include <string>
#include <FL/Fl_Input.H>

namespace SYSM_UI
{
	enum {
		INPUT_INIT_RONLY		= 0x01,
		INPUT_INIT_LOCKED		= 0x02,
		INPUT_INIT_LOWER		= 0x04,
		INPUT_INIT_UPPER		= 0x08,
		INPUT_INIT_NUM			= 0x10,
		INPUT_INIT_TEXT			= 0x20,
		INPUT_INIT_IP			= 0x40,
		INPUT_INIT_TIME			= 0x80,
	};

	class input_c : public Fl_Input
	{
	private:
		std::string m_val_copy;
	protected:
		virtual int handle(int event);
	public:
		input_c(int x, int y, int w, int h, 
			const char * default_value, unsigned int flags);
		
		void set_value_copy(const char *t);
		void update_value_from_copy();
	};
};
#endif
