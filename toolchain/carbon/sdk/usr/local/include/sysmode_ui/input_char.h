#ifndef __SYSM_CHAR_INPUT__
#define __SYSM_CHAR_INPUT__

#include <sysmode_ui/input.h>

namespace SYSM_UI
{
	enum {
		INPUT_CHAR_MODE_LOWER = 0,
		INPUT_CHAR_MODE_UPPER,
		INPUT_CHAR_MODE_NUM
	};

	class input_char_c: public input_c 
	{
		unsigned int	m_mode;
		unsigned int 	m_locked;	
		const char 	*m_mode_str;
		int 		m_prev_key;
		unsigned int 	m_timeout;
		unsigned int 	m_repeat;
	protected:
		int handle(int event);
		void draw(void);
	public:
		void set_flags(unsigned int);
		input_char_c(int, int, int, int, const char *, unsigned int);
		void set_keypad_type(char type);
	};

}

#endif
