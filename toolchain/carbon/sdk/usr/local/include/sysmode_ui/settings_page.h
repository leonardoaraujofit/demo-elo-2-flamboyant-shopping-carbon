#ifndef __SYSM_SETTINGS_PAGE__
#define __SYSM_SETTINGS_PAGE__

#include <pthread.h>
#include <sysmode_ui/sysm_ui.h>
#include <sysmode_ui/sysm_scroll.h>
#include <sysmode_ui/setting.h>
#include <sysmode_ui/progress_window.h>
#include <event_engine/event_engine.h>
#include <event_engine/event_listener.h>
#include <event_engine/event_types.h>

namespace SYSM_UI {

	class settings_page_c: public Sysm_Scroll, public event_listener_c
	{
		std::string m_msg;
		std::string m_progress_msg;
		progress_window_c *m_progress_win;
		
		pthread_mutex_t m_send_event_ref_mutex;
		unsigned int	m_send_event_ref;

	protected:
		event_engine_c *m_engine;
		int handle(int event);
		void send_event_ref(event_c *event);
		void on_event_ref(const event_c *);
	public:
		settings_page_c(event_listener_id_t, const char *);
		~settings_page_c();
		
		void add_e(SYSM_UI::setting_c *);
		void display();

		void display_message(const char *);
		void start_setting_progress();
		void set_progress_msg(const char *);
		void update_progress_msg();
		void stop_setting_progress();
		void retrieve_focus();

		void set_progress_window(progress_window_c *);
		progress_window_c * get_progress_window();
	};
};

#endif
