#ifndef __SYSM_IP_INPUT_ALPHANUM__
#define __SYSM_IP_INPUT_ALPHANUM__

#include <string>
#include <sysmode_ui/input.h>

namespace SYSM_UI
{
	class input_ip_alphanum_c: public input_c
	{
	protected: 
		int handle(int event);
	public:
		input_ip_alphanum_c(int, int, int, int, const char *, unsigned int);
	};
};

#endif

