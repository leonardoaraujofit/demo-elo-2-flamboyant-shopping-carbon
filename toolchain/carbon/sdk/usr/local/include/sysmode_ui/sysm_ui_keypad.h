#ifndef __SYSMODE_UI_KEYPAD__
#define __SYSMODE_UI_KEYPAD__

#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Browser.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Scrollbar.H>
#include <FL/Fl_PNG_Image.H>
#include <FL/Fl_Table.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Secret_Input.H>
#include <FL/Fl_Select_Browser.H>
#include <FL/Fl_Text_Display.H>
#include <FL/Fl_Choice.H>

#include <vector>
#include <pthread.h>
#include <string>
#include <time.h>
#include <map>

#include <sysmode_ui/sysm_ui.h>
#include <sysmode_ui/property.h>



namespace SYSM_UI
{

    class Sysm_Key : public Sysm_Button
    {
    private:
    	void *data;
    protected:
        int handle(int e);
    public:
		void callback(void *d = 0);
		Sysm_Key(int x, int y, int w, int h, const char * t,
					const char * fi = NULL,
					const char * ui = NULL):
					Sysm_Button(x,y,w,h,t,fi,ui)
					{
						this->data=0;
					};
     };

	class Sysm_keypad_num : public Fl_Group
	{
	public:

		typedef enum {
			TK_ONE = 0,
			TK_TWO,
			TK_THREE,
			TK_FOUR,
			TK_FIVE,
			TK_SIX,
			TK_SEVEN,
			TK_EIGHT,
			TK_NINE,
			TK_ESCAPE,
			TK_ZERO,
			TK_ENTER,
			TK_BACKSPACE,
			TK_MAX_KEYS
		} t_key_id;

		typedef struct callback_data {
			Fl_Widget *widget;
			t_key_id key_id;
		} t_callback_data;

		typedef struct key_param {
			int x;
			int y;
			int w;
			int h;
			char label[16];
			t_callback_data cb_data;
		} t_key_param;


		/*
		 * x, y, w, h defines location and total size of keypad, all keys will be fit within w,h
		 */
		Sysm_keypad_num(int x, int y, int w, int h,
					Fl_Widget *input,
					const char *t=0); // keypad title
		~Sysm_keypad_num() { }

		t_key_param * get_key_param(t_key_id key_id);

    protected:

	private:
		Fl_Box       *frame;
		Fl_Box       *title;
		t_key_param keys[TK_MAX_KEYS];
	};
};


#endif // __SYSMODE_UI_KEYPAD__
