#ifndef __SYSM_SETTING_TEXT__
#define __SYSM_SETTING_TEXT__

#define ALPHA_TYPE_FILE_PATH	"/mnt/flash/sysdata/sys4/alpha_type"

enum alpha
{
	KEYPAD_STD	=	'3',
	KEYPAD_STD2	=	'A'
};

#ifdef __cplusplus

#include <sysmode_ui/sysm_ui.h>
#include <sysmode_ui/input.h>
#include <sysmode_ui/setting.h>
#include <event_engine/event.h>

extern  "C"
{
	int set_alpha_type(enum alpha);
}

namespace SYSM_UI
{

class setting_text_c: public setting_c
	{
	public:
		setting_text_c(const char * name, 
			event_listener_id_t listener_id,
			event_listener_id_t list_listener_id,
			const char * default_value = 0, 
			unsigned int flags = INPUT_INIT_TEXT);

		~setting_text_c();
		
		using event_listener_c::send_event;
		bool send_event(const char * name);
		unsigned int get_static_init_flags(void);
		
		input_c *input;
		Sysm_Button *text_button;
		unsigned int static_init_flags;
		
	protected:
		void on_event(const event_c *event);
	};
};

#else
int set_alpha_type(enum alpha);
#endif //__cplusplus

#endif
