#ifndef _SVCMGR_SVCDEF_H_
#define _SVCMGR_SVCDEF_H_

#ifdef __cplusplus
extern "C" {
#endif

/*SVC_STRUCT*/
struct pair {
	char* name;
	char* value;
};

/*SVC_STRUCT*/
struct pairlist {
	struct pair * list;
	int list_count;
};

/*SVC_STRUCT*/
struct version {
    int major;
    int minor;
    int maint;
    char build[16];
};

#define FIXEDPAIR_NAME_SIZE   32
#define FIXEDPAIR_VALUE_SIZE  64

/*SVC_STRUCT*/
struct fixedpair {
	char name[FIXEDPAIR_NAME_SIZE];   // NULL terminated utf-8 string
	char value[FIXEDPAIR_VALUE_SIZE]; // NULL terminated utf-8 string
};

/*SVC_STRUCT*/
struct fixedpairlist {
	struct fixedpair *list;
	int list_count;
};

#ifdef __cplusplus
}
#endif
#endif //_SVCMGR_SVCDEF_H_
