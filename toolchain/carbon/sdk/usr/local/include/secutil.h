#ifndef _SECUTIL_H_
#define _SECUTIL_H_

#ifdef __cplusplus
extern "C" {
#endif

#define TAMPER_LOG_FILE_PATH      "/mnt/flash/logs/system/tamper.log"
#define TAMPER_TEXT_LOG_FILE_PATH "/mnt/flash/logs/system/tamper_p.log"

/*Tamper logs */
#define TAMPER_LOG_FILE_SIGNATURE                 0x123abfde
#define TAMPER_RECORD_SIZE                        12
#define TAMPER_NUMBER_OF_RECORDS_IN_TABLE         10
#define TAMPER_VAULT_TABLE_SIZE        (TAMPER_RECORD_SIZE * TAMPER_NUMBER_OF_RECORDS_IN_TABLE)

#define svcWait vfiSec_svcWait
#define svcGetSysMicrosec vfiSec_svcGetSysMicrosec
#define svcGetSysMillisec vfiSec_svcGetSysMillisec


typedef struct 
{
  unsigned int Signature;
  unsigned int NumbersOfRecords;
  unsigned int RecordSize;
}SecureTamperLogFileHeader;
/**********************************************************************
 * This function need to be called on boot up
 * It call to Vault to get tamper log table (6 record).
 * Tamper records will be written to /logs/system/tamper.log as raw data.
 * Tamper logs will be written to /logs/system/tamper_p.log as text data
 * *******************************************************************/
void Secutil_CheckTamperLog(unsigned char * TamperTable);

/*This is implement here due to build dependencies with libfvisvc*/
void vfiSec_svcWait(int mstm);
// Returns microseconds elapsed since the Epoch (currently Jan 1, 1970)
unsigned long long vfiSec_svcGetSysMicrosec(void);
// Returns milliseconds elapsed since the Epoch (currently Jan 1, 1970)
unsigned long long vfiSec_svcGetSysMillisec(void);
int Secutil_GetLastActiveTamper(unsigned int *status,void *tm);

/* Function to remove user app certs from the cert tree */
int rmAppCerts(unsigned char *cmdbuf);


#ifdef __cplusplus
}
#endif

#endif /* _SECUTIL_H_ */
