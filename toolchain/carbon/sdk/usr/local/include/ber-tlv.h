/** @addtogroup */
/** @{ */
/** @addtogroup */
/** @{ */
/** 
 *  @file ber-tlv.h 
 *
 *	@brief BER_TLV
 *
 * 
 */

/* CiR - 13May2003. */

/* -----------------------------------------------------------------------------
 *
 *
 * $Log: ber-tlv.h,v $
 * Revision 1.6  2007/11/28 14:51:53  pmurray
 * Added pstTLV_resize_element
 *
 * Revision 1.5  2007/01/30 16:07:46  crehill
 * Extended support for constructed elements.
 *
 *
 */


#ifndef __BER_TLV_H
#define __BER_TLV_H

#include <stdlib.h>


#ifndef FALSE
#define FALSE 		0
#endif

#ifndef TRUE
#define TRUE  		1
#endif

#ifndef SUCCESS
#define   SUCCESS   0
#endif


typedef u_short ERROR_CODE;


/**
 * @name ERROR_CODEs returned by API functions below
*/
/** \{ */
#define ERR_BER_TLV_BUFFER_OVERRUN			1
#define ERR_BER_TLV_FORMAT					2
#define ERR_BER_TLV_ELEMENT_NOT_PRESENT		3
#define ERR_BER_TLV_ELEMENT_NOT_EXPECTED	4
/** \} */

/**
 * @name Masks on tag bytes
*/
/** \{ */
#define BER_TLV_TAG_MASK_CLASS	0xC0	/**< 0b11000000 */
#define BER_TLV_TAG_MASK_FORM	0x20	/**< 0b00100000 */
#define BER_TLV_TAG_MASK_NUMBER	0x1F	/**< 0b00011111 */
#define BER_TLV_TAG_MASK_MORE	0x80	/**< 0b10000000 */
/** \} */

/**
 * @name Masks on first length byte
*/
/** \{ */
#define BER_TLV_LEN_MASK_MULTI	0x80	/**< 0b10000000 */
#define BER_TLV_LEN_MASK_LENGTH	0x7F	/**< 0b01111111 */
/** \} */

/**
 * @name Tag function defines
*/
/** \{ */
#define bIsConstructed(byTag)	((byTag & BER_TLV_TAG_MASK_FORM) != 0)
#define byTagClass(byTag)	(u_char) (((u_char) byTag & BER_TLV_TAG_MASK_CLASS) >> 6)
#define TAG_DET(abyT)	abyT, sizeof abyT
/** \} */

typedef enum
{
	LEN_OK = 0,
	LEN_INDEFINITE,
	LEN_INVALID

} LENGTH_STATUS;

/**
 * @name BER-TLV size tag
*/
/** \{ */
#define BER_TLV_SIZE_TAG	4
/** \} */

/**
 * Tag, length and value triplet as in ISO8825
 */
typedef struct BER_TLV
{
	/*
	 * Tag, length and value triplet as in ISO8825.
	 *
	 * The length of the tag field is limited to the size of the abyTag array
	 * below (currently four bytes).  The handling code is written so that if
	 * extra space is required, increasing the size of this array is all that
	 * needs to be done.
	 *
	 * The length is limited to that which can be stored in four bytes (sizeof
	 * (u_long)) in this implementation, even though the standard allows a length
	 * specifier up to 127 bytes long.  This reduces the range of lengths from
	 * 2^1016-1 to just 2^32-1, which is still plenty (4GB) for all practical
	 * applications.
	 */
	u_char abyTag[BER_TLV_SIZE_TAG];
	u_long ulnLen;
	u_char *pbyValue;

	/*
	 * Constructed elements have zero or more child elements, each of which is a
	 * BER-TLV element and can have either simple or constructed form.
	 */
	struct BER_TLV *pstChild;

	/*
	 * When an element is a child of a constructed element, this pstNext will
	 * point to the next child element of the constructed element.
	 */
	struct BER_TLV *pstNext;

	/* The amount of memory allocated to the tag. */
	u_char byTagLen;

} BER_TLV;

typedef ERROR_CODE (*EL_VARIABLE_FN)(void *pvVar, BER_TLV *pstEl);

typedef enum
{
	AT_REQUIRED,
	AT_OPTIONAL

} ALLOW_TYPE;

/**
 * Allow entry structure
 */
typedef struct
{
	const u_char *pbyTag;
	u_char byTagLen;
	ALLOW_TYPE eReqd;
	EL_VARIABLE_FN pfnVarFn;

} ALLOW_ENTRY;

/**
 * Tag equivalent structure
 */
typedef struct
{
	const u_char *pbyTag;
	int nTagLen;
	const u_char *pbyTagEquiv;
	int nTagEquivLen;

} TAG_EQUIV;


typedef void *(*TLV_CALLOC)(size_t nmemb, size_t size);
typedef void (*TLV_FREE)(void *ptr);
typedef void *(*TLV_REALLOC)(void *ptr, size_t size);

/**
 * Set calloc
 *
 * @param[in] pfnCalloc Calloc
 * @param[in] pfnFree Free
 *
 */
void vTLV_set_calloc(TLV_CALLOC pfnCalloc, TLV_FREE pfnFree);

/**
 * Interpret a string of bytes as an ISO8825 tag specifier
 *
 * @param[in] pstTLV Tag-length-value-triplet-containing structure. Tag length field used to
 * indicate whether tag is valid. It will be non-zero for a valid tag.
 * @param[in] pulnBufUsed Number of bytes of the buffer used to make the tag, undefined if not successful
 * @param[in] pbyBuf Pointer to the start of the buffer
 * @param[in] ulnBufLen Length of buffer (pointed to by pbyBuf) available
 *
 * @return 
 * @li 0 = Success
 * @li ERROR_CODE = Failure
 * 
 */
ERROR_CODE usnTLV_get_BER_tag
(
	BER_TLV *pstTLV,
	u_long *pulnBufUsed,
	u_char *pbyBuf,
	u_long ulnBufLen
);

/**
 * Interpret the specified array as an array with TLV triplets
 *
 * @param[in] pstTLV Tag-length-value-triplet-containing structure. Tag length field used to
 * indicate whether tag is valid. It will be non-zero for a valid tag.
 * @param[in] pbyBuf Pointer to the start of the buffer
 * @param[in] ulnBufLen Length of buffer (pointed to by pbyBuf) available
 *
 * @return 
 * @li 0 = Success
 * @li ERROR_CODE = Failure
 * 
 */
ERROR_CODE usnTLV_parse_buffer
(
	BER_TLV *pstTLV,
	u_char *pbyBuf,
	u_long ulnBufLen
);

/**
 * Convert the TLV tree into a flat, array-based format
 *
 * @param[in] pbyBuf Buffer for flattened output
 * @param[in] pulnBufUsed Number of bytes of buffer used
 * @param[in] ulnBufLen Length of buffer (pointed to by pbyBuf) available
 * @param[in] pstEl TLV tree to be flattened
 * @param[in] bFollow All same-level elements flag
 *
 * @return 
 * @li 0 = Success
 * @li ERROR_CODE = Failure
 * 
 */
ERROR_CODE usnTLV_flatten_element
(
	u_char *pbyBuf,
	u_long *pulnBufUsed,
	u_long ulnBufLen,
	BER_TLV *pstEl,
	u_char bFollow
);

/**
 * Allocate space for a new, empty TLV element
 *
 * @return 
 * @li Pointer to BER_TLV empty structure on success
 * @li NULL on failure
 * 
 */
BER_TLV *pstTLV_make_element(void);

/**
 * Add a new, empty element after the specified element
 *
 * @param[in] pstEl Pointer to the TLV element after which to add element
 *
 * @return 
 * @li Pointer to BER_TLV added structure on success
 * @li NULL on failure
 * 
 */
BER_TLV *pstTLV_insert_element(BER_TLV *pstEl);

/**
 * Add the specified element after the specified element
 *
 * @param[in] pstEl Pointer to the TLV element after which to add element
 * @param[in] pstNew TLV element to be inserted
 *
 * @return 
 * @li Pointer to BER_TLV added structure on success
 * @li NULL on failure
 * 
 */
BER_TLV *pstTLV_insert_this_element
(
	BER_TLV *pstEl,	/* In : TLV element before insertion point.	*/
	BER_TLV *pstNew	/* In : TLV element to be inserted.			*/
);

/**
 * Add a new, empty element after the last element in the specified tree
 *
 * @param[in] pstEl Pointer to the TLV tree
 *
 * @return 
 * @li Pointer to BER_TLV added empty structure on success
 * @li NULL on failure
 * 
 */
BER_TLV *pstTLV_append_element(BER_TLV *pstEl);

/**
 * Add the specified element after the last element in the specified tree
 *
 * @param[in] pstEl Pointer to the TLV tree
 * @param[in] pstNew TLV element to be appended
 *
 * @return 
 * @li Pointer to BER_TLV added structure on success
 * @li NULL on failure
 * 
 */
BER_TLV *pstTLV_append_this_element
(
	BER_TLV *pstEl,	/* In : TLV tree to be appended to.	*/
	BER_TLV *pstNew	/* In : TLV element to be appended.	*/
);

/**
 * Free up memory used by entire TLV tree, starting at the specified element
 *
 * @param[in] pstEl Pointer to the TLV element
 *
 */
void vTLV_destroy_element(BER_TLV *pstEl);

/**
 * Remove the element from a TLV tree.  Do not use with last element (i.e. no
 * "next" element) at any level in a tree.
 *
 * @param[in] pstEl Pointer to the TLV element
 *
 */
void vTLV_remove_element(BER_TLV *pstEl);


/**
 * Show the specified TLV tree on stdout, debug function only
 *
 * @param[in] pstEl Pointer to the TLV tree
 * @param[in] byLvl ???
 * @param[in] bFollow All same-level elements flag
 *
 */
void vTLV_print_element(BER_TLV *pstEl, u_char byLvl, u_char bFollow);

/**
 * Show the specified TLV tree on file stream, debug function only
 *
 * @param[in] Fp Pointer to the file
 * @param[in] pstEl Pointer to the TLV tree
 * @param[in] byLvl ???
 * @param[in] bFollow All same-level elements flag
 *
 */
void vTLV_fprint_element
(
	FILE *Fp,
	BER_TLV *pstEl,
	u_char byLvl,
	u_char bFollow
);

/**
 * Search the specified TLV tree for elements with the specified tag (this level only: do not descend through children)
 *
 * @param[in] pstEl Pointer to the TLV tree
 * @param[in] pbyTag Tag which to search by
 * @param[in] byTagLen Tag length which to search by
 *
 * @return 
 * @li Pointer to BER_TLV structure on success
 * @li NULL on failure
 * 
 */
BER_TLV *pstTLV_find_element(BER_TLV *pstEl, const u_char *pbyTag, u_char byTagLen);

/**
 * Find the number of bytes required to transmit the specified TLV tree
 *
 * @param[in] pstEl TLV tree to be measured
 * @param[in] bFollow All same-level elements flag
 *
 * @return Number of bytes required to transmit the specified TLV tree
 * 
 */
u_long ulnTLV_flat_length
(
	BER_TLV *pstEl,		/* In : TLV tree to be measured.		*/
	u_char bFollow		/* In : All same-level elements flag.	*/
);

/**
 * Resize/change the length and value of a previously allocated element
 *
 * @param[in] pstEl Pointer to the TLV element
 * @param[in] pbyNewValue New value
 * @param[in] ulnNewLen New length
 *
 * @return 
 * @li Pointer to BER_TLV structure on success
 * @li NULL on failure
 * 
 */
BER_TLV *pstTLV_resize_element
(
	BER_TLV *pstEl,
	u_char *pbyNewValue,
	u_long ulnNewLen
);

/**
 * Fill in the tag, length and value for a previously allocated non-constructed element
 *
 * @param[in] pstEl Pointer to the TLV element
 * @param[in] pbyTag Tag
 * @param[in] byTagLen Tag length
 * @param[in] pbyValue Value
 * @param[in] ulnLen Length
 * 
 */
void vTLV_set_up_primitive_element
(
	BER_TLV *pstEl,
	const u_char *pbyTag,
	u_char byTagLen,
	u_char *pbyValue,
	u_long ulnLen
);

/**
 * Allocate space for and fill in the tag, length and value for a non-constructed element
 *
 * @param[in] pbyTag Tag
 * @param[in] byTagLen Tag length
 * @param[in] pbyValue Value
 * @param[in] ulnLen Length
 *
 * @return 
 * @li Pointer to BER_TLV structure on success
 * @li NULL on failure
 * 
 */
BER_TLV *pstTLV_make_primitive_element
(
	const u_char *pbyTag,
	u_char byTagLen,
	u_char *pbyValue,
	u_long ulnLen
);

/**
 * Add the new element as a child of an existing constructed element, existing children are not overwritten
 *
 * @param[in] pstEl TLV parent to be added to
 * @param[in] pstNew TLV child to be added
 * 
 */
void vTLV_add_to_constructed_element
(
	BER_TLV *pstEl,	/* In : TLV parent to be added to.	*/
	BER_TLV *pstNew	/* In : TLV child to be added.		*/
);


/**
 * Add a tag to an existing, empty constructed element, add the specified child element to that constructed element
 *
 * @param[in] pstEl Uninitialised, allocated TLV element
 * @param[in] pbyTag Tag to be put on above element
 * @param[in] byTagLen Length of tag
 * @param[in] pstChild Element to be added to above element
 *
 */
void vTLV_set_up_constructed_element
(
	BER_TLV *pstEl,			/* In : Uninitialised, allocated TLV element.	*/
	const u_char *pbyTag,	/* In : Tag to be put on above element.			*/
	u_char byTagLen,		/* In : Length of tag.							*/
	BER_TLV *pstChild		/* In : Element to be added to above element.	*/
);


/**
 * Allocate space for a new constructed element with the specified tag and add the specified element as a child of that new element
 *
 * @param[in] pbyTag Tag to be put on above element
 * @param[in] byTagLen Length of tag
 * @param[in] pstChild Element to be added to above element
 *
 * @return 
 * @li Pointer to BER_TLV structure on success
 * @li NULL on failure
 *
 */
BER_TLV *pstTLV_make_constructed_element
(
	const u_char *pbyTag,
	u_char byTagLen,
	BER_TLV *pstChild
);

/**
 * Make a new element as copy of an existing element
 *
 * @param[in] pstEl Pointer to the TLV element
 * @param[in] bFollow All same-level elements flag
 *
 * @return 
 * @li Pointer to BER_TLV structure on success
 * @li NULL on failure
 *
 */
BER_TLV *pstTLV_make_copy(BER_TLV *pstEl, u_char bFollow);

/**
 * Copy one TLV element into another
 *
 * @param[in] pstDestEl Pointer to the destination TLV element
 * @param[in] pstSrceEl Pointer to the source TLV element
 * @param[in] bFollow All same-level elements flag
 *
 */
void vTLV_copy_element(BER_TLV *pstDestEl, BER_TLV *pstSrceEl, u_char bFollow);

/**
 * Check that all the required elements in the list of allowed elements are present in the command and that all the elements in the command are present in the list
 *
 * @param[in] pstCmd Pointer to the commant
 * @param[in] pstList Pointer to the list
 *
 * @return 
 * @li 0 = Success
 * @li ERROR_CODE = Failure
 * 
 */
ERROR_CODE usnTLV_check_tags(BER_TLV *pstCmd, const ALLOW_ENTRY *pstList);

/**
 * Fill command arguments
 *
 * @note When using this function, make sure any memory allocated by the called functions is freed after use in the calling function
 *
 * @param[in] pvVar Data
 * @param[in] pstCmd Pointer to the commant
 * @param[in] pstList Pointer to the list
 *
 * @return 
 * @li 0 = Success
 * @li ERROR_CODE = Failure
 * 
 */
ERROR_CODE usnTLV_fill_command_arguments
(
	void *pvVar,
	BER_TLV *pstCmd,
	const ALLOW_ENTRY *pstList
);


/*
 * Convert the tag field of a BER_TLV structure according to the specified tag
 * equivalent table.
 *
 * Returns,
 * SUCCESS if conversion is successful. Oherwise an error code is returned.
 *
 * Parameters.
 * pbyTags Out.
 *   Tag-length-value-triplet-containing structure to be updated.
 *
 * pstTagEquivTable In.
 *   Table of BER-TLV tags with equivalent tags for conversion purposes.
 *
 *   Sample Table.
 *
 *   static const uint8_t abyDF01[] = { 0xDF, 0x01 };
 *   static const uint8_t abyDF02[] = { 0xDF, 0x02 };
 *   static const uint8_t abyDF8101[] = { 0xDF, 0x81, 0x01 };
 *   static const uint8_t abyDF8102[] = { 0xDF, 0x81, 0x02 };
 *
 *   static const TAG_EQUIV astTagConvTable[] =
 *   {
 *       { TAG_DET(abyDF01), TAG_DET(abyDF8101) },
 *       { TAG_DET(abyDF02), TAG_DET(abyDF8102) },
 *
 *       { NULL, 0, NULL, 0 } // NB: This entry is required.
 *   };
 *
 * bReverse In.
 *   If bReverse is FALSE, tags in the input BER_TLV string found in column 1
 *   of the conversion table will be converted to the equivalent tag from
 *   column 2. Setting bReverse will reverse the procedure.
 *
 * Example.
 *   Using the above table, the input string
 *
 *       DF01 01 AA DF03 01 BB DF02 02 CCDD
 *
 *   is converted to
 *
 *       DF8101 01 AA DF03 01 BB DF8102 02 CCDD
 *
 *	assuming bReverse is FALSE.
 *
 *  Settng bReverse will convert the output back to the original input again.
 *
 */

/**
 * Convert the tag field of a BER_TLV structure according to the specified tag equivalent table
 *
 * @param[out] pstTags Tag-length-value-triplet-containing structure to be updated
 * @param[in] pstTagEquivTable Table of BER-TLV tags with equivalent tags for conversion purposes
 * @param[in] bReverse If bReverse is FALSE, tags in the input BER_TLV string found in column 1
 * of the conversion table will be converted to the equivalent tag from
 * column 2. Setting bReverse will reverse the procedure.
 *
 * @return 
 * @li 0 = Success
 * @li ERROR_CODE = Failure
 * 
 */
ERROR_CODE usnTLV_convert_BER_tags
(
	BER_TLV *pstTags,
	const TAG_EQUIV *pstTagEquivTable,
	u_char bReverse
);


/*
 * Convert the tag field of a BER_TLV string according to the specified tag
 * equivalent table.
 *
 * Returns,
 * SUCCESS if conversion is successful. Oherwise an error code is returned.
 *
 * Parameters.
 * pbyOutTags Out.
 *   The resulting BER-TLV string.
 *
 * pulnOutTagLen Out.
 *   The resulting BER-TLV string length.
 *
 * ulnMaxOutTagLen In.
 *   The maximum allowed output BER-TLV string length.
 *
 * pbyInTags In.
 *   The BER_TLV string to be converted.
 *
 * ulnInTagLen In.
 *   Length of the BER_TLV string to be converted.
 *
 * pstTagEquivTable In.
 *   Table of BER-TLV tags with equivalent tags for conversion purposes.
 *
 *   Sample Table.
 *
 *   static const uint8_t abyDF01[] = { 0xDF, 0x01 };
 *   static const uint8_t abyDF02[] = { 0xDF, 0x02 };
 *   static const uint8_t abyDF8101[] = { 0xDF, 0x81, 0x01 };
 *   static const uint8_t abyDF8102[] = { 0xDF, 0x81, 0x02 };
 *
 *   static const TAG_EQUIV astTagConvTable[] =
 *   {
 *       { TAG_DET(abyDF01), TAG_DET(abyDF8101) },
 *       { TAG_DET(abyDF02), TAG_DET(abyDF8102) },
 *
 *       { NULL, 0, NULL, 0 } // NB: This entry is required.
 *   };
 *
 * bReverse In.
 *   If bReverse is FALSE, tags in the input BER_TLV string found in column 1
 *   of the conversion table will be converted to the equivalent tag from
 *   column 2. Setting bReverse will reverse the procedure.
 *
 * Example.
 *   Using the above table, the input string
 *
 *       DF01 01 AA DF03 01 BB DF02 02 CCDD
 *
 *   is converted to
 *
 *       DF8101 01 AA DF03 01 BB DF8102 02 CCDD
 *
 *	assuming bReverse is FALSE.
 *
 *  Settng bReverse will convert the output back to the original input again.
 *
 */

/**
 * Convert the tag field of a BER_TLV string according to the specified tag equivalent table
 *
 * @param[out] pbyOutTags The resulting BER-TLV string
 * @param[out] ulnOutTagLen he resulting BER-TLV string length
 * @param[in] ulnMaxOutTagLen The maximum allowed output BER-TLV string length
 * @param[in] pbyInTags The BER_TLV string to be converted
 * @param[in] ulnInTagLen Length of the BER_TLV string to be converted
 * @param[in] pstTagEquivTable Table of BER-TLV tags with equivalent tags for conversion purposes 
 * @param[in] bReverse If bReverse is FALSE, tags in the input BER_TLV string found in column 1
 * of the conversion table will be converted to the equivalent tag from
 * column 2. Setting bReverse will reverse the procedure
 *
 * @return 
 * @li 0 = Success
 * @li ERROR_CODE = Failure
 * 
 */
u_short usnTLV_convert_tags
(
	u_char *pbyOutTags,
	u_long *ulnOutTagLen,
	u_long ulnMaxOutTagLen,
	u_char *pbyInTags,
	u_long ulnInTagLen,
	const TAG_EQUIV *pstTagEquivTable,
	u_char bReverse
);

#endif

/// @} */
/// @} */
