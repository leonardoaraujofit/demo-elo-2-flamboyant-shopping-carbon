#ifndef __SYSM_LOAD_MIB__
#define __SYSM_LOAD_MIB__

#include <sysmode_ui/sysm_ui.h>
#include <sysmode_ui/sysm_lang.h>
#include <sysmode_ui/property.h>
#include <FL/Fl_Text_Display.H>
#include <stdarg.h>

extern "C" {
    #include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h>
    #include <sys/wait.h>
    #include <pthread.h>
}

#define TOUCH_EVENT 11

void load_mib_cb(const SYSM_UI::t_ui_menu_entry *e );

class TextDisplayMIB : public Fl_Text_Display
{
	Fl_Text_Buffer *m_text_buffer;
	bool exit_allowed;
	
public:
	TextDisplayMIB(int X, int Y, int W, int H, const char *l = 0) :
		Fl_Text_Display(X,Y,W,H,l),
		exit_allowed(false)
	{
		//property class
		SYSM_UI::property *prp = SYSM_UI::property::getInstance();
		SYSM_UI::property::objectAttribute *menu_unfoc_prp = prp->obj(SYSM_UI::property::MENU_UNFOCUS);
		SYSM_UI::property::objectAttribute *app_prp = prp->obj(SYSM_UI::property::APP);
		this->labeltype(FL_NO_LABEL);
		this->box(FL_NO_BOX);
		this->color(menu_unfoc_prp->color());
		this->textcolor(menu_unfoc_prp->fontcolor());
		this->textfont(app_prp->font());
		this->textsize(app_prp->fontsize());
		m_text_buffer = new Fl_Text_Buffer;
		if (m_text_buffer)
			this->buffer(m_text_buffer);
	}
	
	void append_data(const char* data)
	{
		if (m_text_buffer) {
			m_text_buffer->append(data);
			this->redraw();
			Fl::check();
		}
	}
	
	void clear()
	{
		if (m_text_buffer)
			m_text_buffer->text("");
	}
	
	void allow_exit()
	{
		exit_allowed = true;
	}
	
	void disable_exit()
	{
		exit_allowed = false;
	}
	
	int handle(int event)
	{
		// allow touch event to exit screen, but only if exit_allowed is true
		if (event == TOUCH_EVENT && exit_allowed){
			Fl::e_keysym = FL_Escape;
			Fl::handle(FL_KEYBOARD, (Fl_Window*) this);
		}
	}
};

#endif
