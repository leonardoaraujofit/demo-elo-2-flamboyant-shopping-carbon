
#ifndef __KEYINFO_SVC_H__
#define __KEYINFO_SVC_H__

#if !defined(SVC_SECURITY_H) && !defined(KEYINFO_SOURCE)
	#error This header must be included from security service only.
#endif

#include "keyinfo_types.h"

// structures

struct keyDirs {
	const char *path;
	char engine;
};

// definitions

#define MAX_RSA_USERS				10
#define MAX_RSA_KEYS_PER_USER		10
#define MAX_RSA_KEYS				(MAX_RSA_USERS * MAX_RSA_KEYS_PER_USER)

#define	RESULT_SUCCESS				0
#define	RESULT_ERROR				-1

// NOTE: these definitions were stolen from security service
#define SECURITY_NO_KEYS_RKL        0       /**< RKL - Both files DO NOT exist */
#define SECURITY_PUBLIC_KEY_RKL     1       /**< RKL - Only /root/rkl_keys/rkl_cert.crt exist */
#define SECURITY_PRIVATE_KEY_RKL    2       /**< RKL - Only /root/rkl_keys/rkl_priv_key.der exist */
#define SECURITY_BOTH_KEYS_RKL      3       /**< RKL - Both files exist */

// constants

extern const struct keyDirs KEY_DIRECTORIES[];
extern const int KEY_DIRECTORIES_COUNT;

// macros

#ifdef __GNUC__
	#define __unused__ 		__attribute__((unused))
#else
	#define __unused__
#endif

// #define LOG_CLIENT_ENABLED	// uncomment the line to enable client side logs
// #define LOG_SERVER_ENABLED	// uncomment the line to enable server side logs

#ifdef LOG_CLIENT_ENABLED
	#define LOG_CLIENT(format, ...)							\
			printf(format, ##__VA_ARGS__)
#else
	#define LOG_CLIENT(format, ...)							\
			do {} while (0)
#endif

#ifdef LOG_SERVER_ENABLED
	#define LOG_SERVER(format, ...)							\
			printf(format, ##__VA_ARGS__)
#else
	#define LOG_SERVER(format, ...)							\
			do {} while (0)
#endif

#define ATTACH_TO_SLLIST(list, item)						\
		do {												\
			item->next = list;								\
			list = item;									\
		} while (0)

#define REVERSE_SLLIST(list)								\
		({													\
			typeof(list) reversed_list = NULL;				\
			typeof(list) item = list;						\
			while (item) {									\
				typeof(list) next_item = item->next;		\
				item->next = reversed_list;					\
				reversed_list = item;						\
				item = next_item;							\
			}												\
			reversed_list;									\
		})

// inline functions

inline char *copy_string(
		const char *string);

// functions

char *create_string(
		const char *format,
		...);

void getRSAts(
		const char *path,
		char *tsBuff);

int getVRKkeyInfo(
		struct keyRSAinfo *VRKkey);

#endif
