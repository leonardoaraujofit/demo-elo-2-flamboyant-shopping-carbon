#ifndef _VAULT_IO_H_
#define _VAULT_IO_H_

#ifdef __cplusplus
extern "C" {
#endif


#define VAULTIO_FORCE_CLOSE     (0x01)
#define VAULTIO_STAY_OPENED     (0x02)

/************************************************************************************************************/
/* FUNCTION NAME:   VaultIO_openVaultDevice                                                                 */
/* DESCRIPTION:     Opens the Vault device which interacts with the Vault kernel module                     */
/* INPUT:           None                                                                                    */
/* OUTPUT:          None                                                                                    */
/* RETURN:          >= 0 Handler to the Vault device                                                        */
/*                  <0   Error opening the Vault device                                                     */
/* NOTES:           None                                                                                    */
/************************************************************************************************************/
int VaultIO_openVaultDevice(void);

/************************************************************************************************************/
/* FUNCTION NAME:   VaultIO_closeVaultDevice                                                                */
/* DESCRIPTION:     Closes the Vault device which interacts with the Vault kernel module                    */
/* INPUT:           handler:        Handler of the Vault device to be closed                                */
/* OUTPUT:          None                                                                                    */
/* RETURN:          None                                                                                    */
/* NOTES:           None                                                                                    */
/************************************************************************************************************/
void VaultIO_closeVaultDevice(int handler);

/************************************************************************************************************/
/* FUNCTION NAME:   VaultIO_closeOpenedVaultDevice                                                          */
/* DESCRIPTION:     Closes the Vault device (if opened) which interacts with the Vault kernel module        */
/* INPUT:           handler:        Handler of the Vault device to be closed                                */
/* OUTPUT:          None                                                                                    */
/* RETURN:          None                                                                                    */
/* NOTES:           None                                                                                    */
/************************************************************************************************************/
void VaultIO_closeOpenedVaultDevice(void);

/************************************************************************************************************/
/* FUNCTION NAME:   VaultIO_setCloseVaultBehaviour                                                          */
/* DESCRIPTION:     sets the Vault device behaviour when closing the device                                 */
/* INPUT:           behaviour:      VAULTIO_FORCE_CLOSE - Close the Vault device when requested             */
/*                                  VAULTIO_STAY_OPENED - Let the device stay opened                        */
/* OUTPUT:          None                                                                                    */
/* RETURN:          0    The operation was performed successfully                                           */
/*                  <0   Unknown input                                                                      */
/* NOTES:           None                                                                                    */
/************************************************************************************************************/
int VaultIO_setCloseVaultBehaviour(int behaviour);

/************************************************************************************************************/
/* FUNCTION NAME:   VaultIO_prepareBuffer                                                                   */
/* DESCRIPTION:     Allocates and returns a buffer which transports the input and output from/to Vault      */
/* INPUT:           command:        Command identifier which identifies the function requested              */
/*                  inDataSize:     Total input data size in bytes                                          */
/*                  outDataSize:    Total output data size in bytes                                         */
/* OUTPUT:          dataStart:      Pointer to the starting position of the input data                      */
/* RETURN:          Pointer to the buffer or NULL if the allocation was not successfull                     */
/* NOTES:           None                                                                                    */
/************************************************************************************************************/
unsigned char *VaultIO_prepareBuffer(   unsigned int command,       unsigned int    inDataSize, 
                                        unsigned int outDataSize,   unsigned char  **dataStart   );

/************************************************************************************************************/
/* FUNCTION NAME:   VaultIO_passRequestToVault                                                              */
/* DESCRIPTION:     Passes the reuest in the buffer to the Vault device                                     */
/* INPUT:           handle:         handle to the Vault device                                              */
/*                  buff:           Pointer to the buffer holding the request                               */
/*                  outDataSize:    Total output data size in bytes                                         */
/* OUTPUT:          None                                                                                    */
/* RETURN:          0               Success                                                                 */
/*                  <0              Failed to pass the request to the Vault device                          */
/* NOTES:           None                                                                                    */
/************************************************************************************************************/
int VaultIO_passRequestToVault(int handle, void *buff);

/************************************************************************************************************/
/* FUNCTION NAME:   VaultIO_addValueToBuffer                                                                */
/* DESCRIPTION:     Adds a parameter to the buffer and returns pointer the next available space in the      */
/*                  buffer                                                                                  */
/* INPUT:           buffer:         Pointer to the buffer                                                   */
/*                  valPtr:         Pointer to the data to be added                                         */
/*                  valSize:        Size of the data to be added                                            */
/* OUTPUT:          None                                                                                    */
/* RETURN:          Pointer to the next available place on the buffer                                       */
/* NOTES:           None                                                                                    */
/************************************************************************************************************/
unsigned char *VaultIO_addValueToBuffer(unsigned char *buffer, void *valPtr, unsigned int valSize);

/************************************************************************************************************/
/* FUNCTION NAME:   VaultIO_getValueFromBuffer                                                              */
/* DESCRIPTION:     Gets a parameter to the buffer and returns pointer the next available parameter in the  */
/*                  buffer                                                                                  */
/* INPUT:           buffer:         Pointer to the buffer                                                   */
/*                  valPtr:         Pointer to the data to be returned from buffer                          */
/*                  valSize:        Size of the data to be returned                                         */
/* OUTPUT:          None                                                                                    */
/* RETURN:          Pointer to the next available parameter on the buffer                                   */
/* NOTES:           None                                                                                    */
/************************************************************************************************************/
unsigned char *VaultIO_getValueFromBuffer(unsigned char *buffer, void *valPtr, unsigned int valSize);
#if 0
/************************************************************************************************************/
/* FUNCTION NAME:   VaultIO_set_userId                                                                      */
/* DESCRIPTION:     Sets the Linux User ID                                                                  */
/* INPUT:           i_userId:        Linux User ID                                                          */
/* OUTPUT:          None                                                                                    */
/* RETURN:          None                                                                                    */
/* NOTES:           None                                                                                    */
/************************************************************************************************************/
void VaultIO_set_userId(int i_userId);
#endif

#ifdef __cplusplus
}
#endif

#endif /* _VAULT_IO_H_ */
