/**
 * @file	led.h
 * @brief 	led shared object test program definitions
 * @attention  Should only be used by those who
 *             know what they are doing.
 * @author 	Gal Yahel
 * @date 	June 29, 2011
 * @version 0.1
 */

#ifndef __LIBLED_H__
#define __LIBLED_H__

#define  SUCCESS 0
#define  FAIL    -1 
#define LED_KP			(1<<1)
#define LED_LOGO		(1<<2)
#define LED_BOOT_ERROR		(1<<3)
#define LED_MSR			(1<<4)
#define LED_MSR2		(1<<5)
#define	LED_MSR3		(1<<6)
#define LED_SC			(1<<7)

//this color are for mx9. for vx520 CTLS please use CTLS_FIRST,CTLS_SECOND,CTLS_THIRD,CTLS_FOURTH
#define CTLS_FIRST_LED_BIT (8)
#define CTLS_RED_LED	   (1<<CTLS_FIRST_LED_BIT)
#define CTLS_YELLOW_LED	   (1<<(CTLS_FIRST_LED_BIT+1))
#define CTLS_GREEN_LED	   (1<<(CTLS_FIRST_LED_BIT+2))
#define CTLS_BLUE_LED	   (1<<(CTLS_FIRST_LED_BIT+3))
#define CTLS_LOGO_LED	   (1<<(CTLS_FIRST_LED_BIT+4))
#define CTLS_LED_MASK	   (0x1f<<CTLS_FIRST_LED_BIT)

#define CTLS_FIRST  CTLS_RED_LED
#define CTLS_SECOND CTLS_YELLOW_LED
#define CTLS_THIRD  CTLS_GREEN_LED
#define CTLS_FOURTH CTLS_BLUE_LED

#define LED_SYSTEM_GREEN (1<<(CTLS_FIRST_LED_BIT+5))
#define LED_SYSTEM_RED (1<<(CTLS_FIRST_LED_BIT+6))

#define MAX_LED_MAP		   ((1<<(CTLS_FIRST_LED_BIT+7))-1)




enum result  {
  ERROR_INIT=1<<0,
  ERROR_KP=1<<1,
  ERROR_LOGO=1<<2,
  ERROR_BOOT_ERROR=1<<3,
  ERROR_MSR=1<<4,
  ERROR_MSR2=1<<5,
  ERROR_MSR3=1<<6,
  ERROR_SC=1<<7,
  LEDS_SUCCESS=1<<8,
  ERROR_CTLS=1<<9,
  ERROR_SYSTEM_GREEN=1<<10,
  ERROR_SYSTEM_RED=1<<11,
  };
/**
 * @brief   led file descriptor.
 */

/**
 * initialization led file descriptor
 * @return led file descriptor
 */
int led_init (void);
/**
 * close led file descriptor
 */
int led_release(void);
/**
 * this function turn the led on
 * @param int led - leds to turn on
 * @return enum result
 */
enum result led_ledOn(int leds);
/**
 * this function turn the led off
 * @param int led - leds to turn off
 * @return enum result
 */
enum result led_ledOff(int leds);
/**
 * turn on led interface
 * @param int leds bitmap leds to blink 
 * @param char unsigned int duration_on in milliseconds
 * @param char unsigned int duration_off in milliseconds
 * @param char unsigned int timeout in seconds
 * @return enum result
 */
enum result led_start_blinking(int leds, unsigned int duration_on, unsigned int duration_off,unsigned int timeout);
/**
 *stop the blinking 
 * @param leds bitmap leds to blink 
 * @return enum result
*/
enum result led_stop_blinking(char leds);
enum result led_stop_blinking2(int leds);
/**
 * this function read led information from the led driver
 * @param int leds -led to turn on or off
 * @param unsigned char *read_res the data about the led status return from the driver
 * @return enum result
 */
enum result led_read_led(int led, unsigned char *cmd);
enum result led_read_ctls_led(int leds, int *read_res);

/**
 * if the led you want to turn on or off have more than one color this function cause functions to use the suitable color 
 * @param int leds -led to switch color
 * @param char 0 = color 1,1=color 2
 * @return enum result
 */
enum result led_switch_color(int leds,char color);
//#define DEBUG
/**
 * This function will set the brightness of leds - fails if level > led_get_max_brightness()
 * @param int leds -led to set brightness
 * @param int level -  brightness level
 * @return enum result
 */
enum result led_set_brightness(int leds, int level);

/**
 * This function gets the brightness value for the LED specified in leds
 * @param int leds - led to get brightness (only a single led can be selcted)
 * @param char* brightness_str - Optional string to get a string representation of the brightness level. If not Null there must be MAX_BRIGHTNESS_STR_LEN+1 bytes available.  
 * @return int - returns the brightness level as an int
 * 
 *  NB: This function is currently only supported on RAPTOR hardware
 */
int led_get_brightness(int leds, char* brightness_str);

/**
 * This function gets the max brightness for the LED specified in leds
 * @param int leds - led to get max brightness (only a single led can be selcted)
 * @param char* brightness_str - Optional string to get a string representation of the max brightness level. If not Null there must beMAX_BRIGHTNESS_STR_LEN+1 bytes available.  
 * @return int - returns the brightness level as an int
 * 
 * NB: This function is currently only supported on RAPTOR hardware
 */
int led_get_max_brightness(int leds, char* brightness_str);



#endif /*#ifndef   __LED_H__*/
