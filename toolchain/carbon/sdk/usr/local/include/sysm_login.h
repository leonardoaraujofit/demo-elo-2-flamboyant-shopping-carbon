#ifndef __SYSMODE_LOGIN__
#define __SYSMODE_LOGIN__

#include "sysm_util.h"
#include <sysmode_ui/sysm_ui.h>
#include <sysmode_ui/sysm_lang.h>

// Bit flags for users start from LSB. Using this approach, 
// we have support for 30 users (not counting 2 keyloads, which
// are defined later)

extern const char * USR1_NAME;
extern const char * USR2_NAME;
extern const char * USR3_NAME;
extern const char * USR4_NAME;
extern const char * KEYLOAD1_NAME;
extern const char * KEYLOAD2_NAME;

extern const char * PWD_DEFAULT;
extern const char * KEYLOAD1_DEFAULT;
extern const char * KEYLOAD2_DEFAULT;
extern const char * KLD1_TITLE;
extern const char * KLD2_TITLE;

typedef enum
{
    PWD_NORMAL                         = 0,
    PWD_OLD,
    PWD_NEW,
    PWD_CONFIRM,
} PWD_CHECK;

enum RUN_MODES
{
    RUN_MODE_MANUFACTURING              = 0,
    RUN_MODE_PRODUCTION                 = 1,
    RUN_MODE_APPDEV                     = 7,
    RUN_MODE_DEVELOPMENT                = 8,
};

typedef enum 
{
    USR1                                = 0x0001,
    USR2                                = 0x0002,
    USR3                                = 0x0004,
    USR4                                = 0x0008,
    KEYLOAD1                            = 0x0010,
    KEYLOAD2                            = 0x0020,
    ALL                                 = 0x0FFF, /* MSB must be equal to 0 in order to support DEVICE_MASK of 5 Hex digits */
} USR_MASK;

typedef enum 
{
    PWD_OK,
    PWD_FAILED,
    PWD_CHANGE_FAILED, 
    PWD_NULL,
} PWD_STATUS;

typedef enum 
{
    SS_UNKNOWN,
    SS_SECURE       = 0x0,
    SS_EXPIRED,
    SS_PENDING,
    SS_DEPLOY,
    SS_PWDCHANGE,
    SS_PWDCHANGE_SUPERUSER,
} SECURITY_STATE;

typedef struct
{
    USR_MASK        usr_mask;
    const char *    usr_name;
} USR;


typedef struct ctrl_struct {
    USR * current_user;
    int run_mode;
} t_usr_ctrl;

USR  * get_current_user(void);

void set_current_user(USR *);

int get_run_mode(void);

// login flags
void init_run_mode(void);

// Initializes default passwords
void init_login(void);

void display_usr_prompt(SYSM_UI::f_menu_cb = NULL, 
                        SYSM_UI::t_ui_menu_list * = NULL);

void display_kld_prompt(SYSM_UI::f_menu_cb,
                        SYSM_UI::t_ui_menu_list * = NULL);

static const char * PWD_TITLE_NORMAL    = gettext("Password");
int prompt_password(SECURITY_STATE, const char * title = PWD_TITLE_NORMAL);

void force_logout();

void logout_cb(const SYSM_UI::t_ui_menu_entry * e = NULL);

SECURITY_STATE get_usr_sstate(const char *usr);
#endif // __SYSMODE_LOGIN__
