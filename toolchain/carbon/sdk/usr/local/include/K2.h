#ifndef __K2_H__
#define __K2_H__

//K2 user interface registers

#define HIFREG__DEBUG_SETUP_COUNT		0	// 0x00

#define HIFREG__GESTURE_OPTIONS			1	// 0x01	   (default 0x28)
#define RAW_FINGER_Z_ENABLE			0x01
#define RAW_PEN_Z_ENABLE			0x02
#define GESTURE_ENABLE			   	0x08	// enables gestures
#define SECURE_PEN_MODE_ENABLE			0x20	// enables PCI PTS secure pen scanning--requires extra time so pen response is slower

#define HIFREG__XSCALE				2	// 0x02
#define HIFREG__YSCALE				3	// 0x03

#define HIFREG__IWDG_TIMER			4	// 0x04

#define HIFREG__FCAL_AND_OTHER_SETTINGS		5	// 0x05
#define SCALE_POWERON_CHECK_VALUE		0x01
#define SCALE_POWERON_CHECK_VALUE_MULTIPLIER	   3
#define REPORT_FACTORY_CAL_AS_POWER_ON_CAL	0x02
#define OVERRIDE_FACTORY_CAL			0x04

#define DEFAULT_FCAL_AND_OTHER_SETTINGS		0x04 // Was 0x03

#define RESERVED_6				6 	// 0x06

#define HIFREG__EF_CHANNEL_MASK			7 	// 0x07
#define EF_CHANNEL_2				0x01
#define EF_CHANNEL_3				0x02
#define EF_CHANNEL_4				0x04
#define EF_CHANNEL_5				0x08
#define EF_CHANNEL_6				0x10
#define EF_CHANNEL_7				0x20
#define EF_CHANNEL_9				0x40
#define EF_CHANNEL_11				0x80

// #define HIFREG__FINGER_SMOOTHING_LENGTH	8 	// 0X08
// #define FINGER_SMOOTHING_LENGTH_DEFAULT	5

#define HIFREG__SHUTDOWN_WAKEUP_KEY		8	// 0x08
#define SHUTDOWN_WAKEUP_KEY_VALUE		0xAB

// #define HIFREG__PEN_ZSCALER			9	// 0x09 // this is the factor that the raw Z is divided by before being reported
// #define PEN_ZSCALER_DEFAULT			180
// #define PEN_ZSCALER_DEFAULT_VFI			(PEN_ZSCALER_DEFAULT / 2) // Original value was too high for stylus to work

#define HIFREG__SHUTDOWN_COUNTER		9	// 0x09

// For firmware versions 73_21_31 and above
#define HIFREG__OTHER_SETTINGS			10	// 0x0A
#define UNGROUNDED_SETTINGS_ENABLE		0x01
#define FINGER_SIGNATURE_MODE_ENABLE		0x02
#define ENABLE_RAW_NOISE_MEASUREMENTS		0x04
#define DISABLE_NOISE_CHARACTERIZATION		0x08
#define RAW_NOISE_USE_ALL_8_CHANNELS		0x10
#define PERFORM_NOISE_MEASUREMENTS		0x20

#define HIFREG__FINGER_ZSCALER			10 	// 0x0A // this is the factor that the raw Z is divided by before being reported
#define FINGER_ZSCALER_DEFAULT			100

#define RESERVED_11				11 	// 0x0B

#define BACKGROUNDCOMP_MAX_DIFF			12 	// 0x0C // This value, left shift 4 (*16) is the max amount allowed to be "comped in" by BGComp
#define BACKGROUNDCOMP_MAX_DIFF_DEFAULT	25

#define HIFREG__BACKGROUNDCOMP_INTERVAL_COUNT	13	// 0x0D
#define BACKGROUNDCOMP_INTERVAL_COUNT_DEFAULT	200	// This value multiplied by 16 is the real value

// #define BACKGROUNDCOMP_TIMER_INTERVAL		13 	// 0x0D // This value, left shift 3 (same as multiplied by 8) is the count before BGComp kicks in.
// #define BACKGROUNDCOMP_TIMER_INTERVAL_DEFAULT	50

// For firmware versions XX_20_XX only
#define HIFREG__FINGER_IMAGETRACKING_MIN_ON_X	17	// 0x11 // This number * 8 is the threshold for interpolation of the raw image
#define FINGER_IMAGETRACKING_MIN_ON_X_DEFAULT	80
#define HIFREG__FINGER_IMAGETRACKING_MIN_ON_Y	18	// 0x12 // This number * 8 is the threshold for interpolation of the raw image
#define FINGER_IMAGETRACKING_MIN_ON_Y_DEFAULT	60
#define HIFREG__FINGER_IMAGETRACKING_MIN_OFF_X	19	// 0x13 // This number * 8 is the threshold for interpolation of the raw image
#define FINGER_IMAGETRACKING_MIN_OFF_X_DEFAULT	60
#define HIFREG__FINGER_IMAGETRACKING_MIN_OFF_Y	20	// 0x14 // This number * 8 is the threshold for interpolation of the raw image
#define FINGER_IMAGETRACKING_MIN_OFF_Y_DEFAULT	40

#define HIFREG__FINGER_WIDE_TRACKMIN_X		21	// 0x15	// This value *4 is the threshold for going from finger seek mode to finger tracking mode
#define FINGER_WIDE_TRACKMIN_X_DEFAULT		25
#define HIFREG__FINGER_WIDE_TRACKMIN_Y		22 	// 0x16	// This value *4 is the threshold for going from finger seek mode to finger tracking mode
#define FINGER_WIDE_TRACKMIN_Y_DEFAULT		25

#define HIFREG__PEN_WIDE_TRACKMIN_X		23 	// 0x17 // This value *4 is the threshold for going from pen seek mode to pen tracking mode
#define PEN_WIDE_TRACKMIN_X_DEFAULT		150
#define HIFREG__PEN_WIDE_TRACKMIN_Y		24	// 0x18	// This value *4 is the threshold for going from pen seek mode to pen tracking mode
#define PEN_WIDE_TRACKMIN_Y_DEFAULT		150

#define HIFREG__DEBUG_DATA_1			25 	// 0x19
#define HIFREG__DEBUG_DATA_2			26 	// 0x1A
#define HIFREG__DEBUG_DATA_3			27 	// 0x1B
#define HIFREG__DEBUG_DATA_4			28 	// 0x1C

#define HIFREG__DEVICE_INPUT			29 	// 0x1D // Shows what the XYZ input is derived from
#define DEVICE_INPUT_PEN			0x00
#define DEVICE_INPUT_FINGER			0x01
#define DEVICE_INPUT_FINGERS			0x02

// Registers for electrode status tests
// For firmware versions 73_21_10 and beyond
#define HIFREG__OPENTEST_X0_X7			14	// 0x0E
#define HIFREG__OPENTEST_X8_X15			15	// 0x0F
#define HIFREG__OPENTEST_Y0_Y7			16	// 0x10
#define HIFREG__OPENTEST_Y8_Y11			17	// 0x11

#define HIFREG__SHORTTEST_X0_X7			18	// 0x12
#define HIFREG__SHORTTEST_X8_X15		19	// 0x13
#define HIFREG__SHORTTEST_Y0_Y7			20	// 0x14
#define HIFREG__SHORTTEST_Y8_Y11		21	// 0x15

// Registers for Wide tracking thresholds
// For firmware versions 73_21_10 and beyond
// Note the order (X,Y,Y,X,Y,X,Y,X) seems a little strange but Cirque confirmed it
#define HIFREG__PEN_NARROW_X_MIN 		22   	// 0x16
#define PEN_NARROW_X_MIN_DEFAULT 		44	// This value multiplied by 16 is the real value
#define HIFREG__PEN_NARROW_Y_MIN		23 	// 0x17
#define PEN_NARROW_Y_MIN_DEFAULT		44	// This value multiplied by 16 is the real value.
#define HIFREG__PEN_WIDE_Y_MIN			24	// 0x18
#define PEN_WIDE_Y_MIN_DEFAULT			50	// This value multiplied by 16 is the real value.
#define HIFREG__PEN_WIDE_X_MIN			25	// 0x19
#define PEN_WIDE_X_MIN_DEFAULT			50	// This value multiplied by 16 is the real value.

#define HIFREG__FINGER_NARROW_Y_MIN		26	// 0x1A
#define FINGER_NARROW_Y_MIN_DEFAULT		50	// This value multiplied by 16 is the real value.
#define HIFREG__FINGER_NARROW_X_MIN		27	// 0x1B
#define FINGER_NARROW_X_MIN_DEFAULT		50	// This value multiplied by 16 is the real value.
#define HIFREG__FINGER_WIDE_Y_MIN		28	// 0x1C
#define FINGER_WIDE_Y_MIN_DEFAULT		75	// This value multiplied by 16 is the real value.
#define HIFREG__FINGER_WIDE_X_MIN		29	// 0x1D
#define FINGER_WIDE_X_MIN_DEFAULT		110	// This value multiplied by 16 is the real value.

// register 30 -- firmare sub revision number
#define HIFREG__FIRMWARE_SUB_VER		30 	// 0x1E

// register 31 -- options 2
#define HIFREG__CIRQUE_OPTIONS_2		31	// 0x1F
#define LIFTOFF_XY_FFF_DISABLE			0x01
#define PEN_PRESENT				0x02
#define LC_CORRECTION_FINGER_X_DISABLE		0x04
#define LC_CORRECTION_FINGER_Y_DISABLE		0x08
#define ENABLE_RAW_DATA_AVERAGING		0x20
#define XPEN_PRESSURE_FEED_DISABLE		0x40
#define AUTO_MODE_SWITCH_DISABLE		0x80	// Cirque test mode only
#define PRESSURE_SENSOR_ENABLE			0x80	// Old

//register 32 - firmwareID
#define HIFREG__FIRMWARE_ID			32	// 0x20
#define K2_FIRMWARE_ID				0x72
#define K2_FIRMWARE_ID_PCI			0x73

//register 33 - firmware rev number
#define HIFREG__FIRMWARE_VER			33	// 0x21

//register 34 - status
#define HIFREG__STATUS				34	// 0x22
#define SW_DR					0x04
#define COMMAND_COMPLETE			0x08
#define FINGER_WIDES_DR				0x40
#define BG_COMP_DR				0x80
// Note: As of version 1F_01, writing only to regs 31,36,37,38,43,44,45,46,47,48,49,50,51,59,60,63 sets COMMAND_COMPLETE bit

//register 35 - setup stages
#define HIFREG__SETUP_STAGE			35	// 0x23
#define NOT_READY_FOR_SETUP			0x00
#define SETTING_UP				0x01
#define READY_FOR_SETUP	 			0x02
#define SETUP_COMPLETE				0x04
//#define TRACK_DISABLE				0x10 	// test
//#define FEED_DISABLE				0x20 	// test

//register 36 - setup modes 
#define HIFREG__SETUP_MODE			36	// 0x24
#define POWER_ON_DEFAULT			0x00
#define FINGER_ONLY				0x01
#define SIX_MM_PEN_ONLY				0x02
#define FOUR_MM_PEN_ONLY 			0x04
#define TWO_MM_PEN_ONLY				0x08
#define SIX_MM_PEN_AND_FINGER			0x10 
#define FOUR_MM_PEN_AND_FINGER			0x20
#define TWO_MM_PEN_AND_FINGER			0x40	// power on default
#define AUTO_SWITCH_INPUT_LOCK			0x80

//register 37 - sample rate
#define HIFREG__SAMPLE_RATE			37	// 0x25
#define SPS_100					0x01
#define	SPS_200					0x02
#define	SPS_300					0x03	// power on default
#define SPS_400					0x04
#define SPS_500					0x05  

//register 38
#define HIFREG__FILTER_LENGTH			38	// 0x26	
#define FILTER_LENGTH_DEFAULT			0x0A
#define FILTER_LENGTH_MAX			0x1E
#define FILTER_LENGTH_CIRQUE			0x00

//register 39 - command
#define HIFREG__COMMAND				39	// 0x27
#define NO_COMMAND				0
#define WRITE_X_LINEAR_CORRECTION		1 
#define READ_X_LINEAR_CORRECTION		2 
#define WRITE_Y_LINEAR_CORRECTION		3 
#define READ_Y_LINEAR_CORRECTION		4 
#define WRITE_Z_CORRECTION			5 
#define READ_Z_CORRECTION			6 
#define ERASE_EEPROM				7 
#define WRITE_PINNACLE_REGISTER			8 	// not used
#define READ_PINNACLE_REGISTER			9 	// not used
#define WRITE_CURRENT_CAL_MATRIX		10
#define READ_CURRENT_CAL_MATRIX			11 	// V17+
#define WRITE_FACTORY_CAL_MATRIX		12
#define READ_FACTORY_CAL_MATRIX			13 	// V17+
#define WRITE_CURRENT_CAL_PEN_MATRIX		14
#define READ_CURRENT_CAL_PEN_MATRIX		15
#define WRITE_FACTORY_CAL_PEN_MATRIX		16
#define READ_FACTORY_CAL_PEN_MATRIX		17

#define WRITE_OEM_DATA				18
#define OEM_DATA_EF_CHANNEL_MASK_OFFSET		0x0E
#define OEM_DATA_UNIT_MODEL_OFFSET		0x0F
#define OEM_DATA_UNIT_MODEL_MX_RAPTOR_BB	0x00
#define OEM_DATA_UNIT_MODEL_SWORDFISH		0x01
#define READ_OEM_DATA				19

#define WRITE_REXT_OEM_DATA			20 	// V1A+
#define READ_REXT_OEM_DATA			21 	// V1A+
#define WRITE_MODE_EEPROM			25
#define READ_MODE_EEPROM			26
#define WRITE_SAMPLE_RATE_EEPROM		27
#define READ_SAMPLE_RATE_EEPROM			28
#define WRITE_FILTER_LENGTH_EEPROM		29
#define READ_FILTER_LENGTH_EEPROM		30
#define WRITE_Z_LEVELS_EEPROM			31
#define READ_PEN_Z_LEVELS_EEPROM		32
#define READ_FINGER_Z_LEVELS_EEPROM		33
// #define WRITE_PEN_GAIN_LEVEL			34
#define WRITE_PEN_GAIN				34
#define READ_PEN_GAIN				35
#define WRITE_FINGER_GAIN			36
#define READ_FINGER_GAIN			37
#define WRITE_APERTURE_WIDTH			38	// # of 125 nsec increments into sampling clock we start looking for change
#define READ_APERTURE_WIDTH			39
#define WRITE_SAMPLE_LENGTH			40	// values 1...3
#define READ_SAMPLE_LENGTH			41
#define PERFORM_CHECKSUM			42
#define READ_FLASH_CHECKSUM_BYTE		43
#define SET_SIG_CAPTURE_MODE			44	// not used
#define CLEAR_SIG_CAPTURE_MODE			45	// not used
#define WRITE_K2_SETUP_CONFIG			46

#define READ_K2_SETUP_CONFIG			47
#define K2_SETUP_CONFIG_AVAILABLE		((Firmware_Info >= 0x721E00) && (Firmware_Info < 0x72B000 || Firmware_Info >= 0x72B500))
#define MAX_FACTORY_COMP_DIFFERENCE_OFFSET	0
#define SYNC_CMD_COMPLETE_WITH_HW_DR_OFFSET	1
#define K2_SYNC_CMD_COMPLETE_WITH_HW_DR_IMPLEMENTED 0
#define PEN_GAIN_OFFSET				2
#define FINGER_GAIN_OFFSET			3
#define SAMPLE_LENGTH_OFFSET			4
#define APERTURE_WIDTH_OFFSET			5
#define FINGER_SAMPLE_LENGTH_OFFSET		6
#define FINGER_APERTURE_WIDTH_OFFSET		7
#define COMP_CHECK_PATTERN_OFFSET		8
#define COMP_CHECK_ALL				0x00 	// all electrodes
#define COMP_CHECK_Y_WIDES			0x01 	// Y wides
#define COMP_CHECK_X_WIDES			0x02 	// X wides
#define MIXED_TRACK_COMP_OFFSET			9
#define MIXED_TRACK_COMP_DEFAULT		ENABLED	// default enabled
#define MIXED_TRACK_COMP_TIMER_OFFSET		10
#define MIXED_TRACK_COMP_TIMER_DEFAULT		15	// default timer 15 seconds
#define MIXED_TRACK_COMP_TIMER_MAX		120	// max timer 120 seconds
#define SET_TO_DEFAULT_VALUE			0xFF
#define MAX_SETUP_DATA_OFFSET			0x32 	// 50

#define SEND_COMP_VALUES			48
#define READ_GPIO_VALUES			49
#define WRITE_GPIO_VALUE			50	// not used
#define WRITE_FINGER_SAMPLE_LENGTH		51	// values 1...3
#define READ_FINGER_SAMPLE_LENGTH		52
#define WRITE_FINGER_APERTURE_WIDTH		53
#define READ_FINGER_APERTURE_WIDTH		54
#define SHUTDOWN				55	// puts in low-power, sleep mode											
#define WAKEUP					56	// must wake up periodically for background comp to work
#define PEN_PRESSURE_SENSOR_ENABLE		57

//						58-85	// Internal commands
//							// Commands added by Cirque on 2016/04/01
#define WRITE_STILL_COMP_TIMER			80	// Data * 32 is approx. number of 10ms intervals
#define READ_STILL_COMP_TIMER			81	// Data * 32 is approx. number of 10ms intervals
#define WRITE_STILL_COMP_MAX_DIFF		82	// Data represents number of raw pixels on side of square 
#define READ_STILL_COMP_MAX_DIFF		83	// Data represents number of raw pixels on side of square

#define SET_BACKGROUND_COMP_TIME		87
#define GET_BACKGROUND_COMP_TIME		88
#define SET_BACKGROUND_COMP_MAX_DIFF		89
#define GET_BACKGROUND_COMP_MAX_DIFF		90
#define WRITE_PEN_PRESSURE_ON_LEVEL		91
#define READ_PEN_PRESSURE_ON_LEVEL		92
#define WRITE_PACKETS_ON_COUNT			93
#define READ_PACKETS_ON_COUNT			94

#define SCALE_AXIS_SCALAR			96
//100-112 internal commands

#define MAX_COMMAND_VALUE 			127 // Arbitrary value (in practice K2 checks command validity)

#define MAX_FACTORY_COMP_DIFFERENCE_SCALING	50 // This is NOT a command but a scaling factor.

//register 40 - data
#define HIFREG__DATA				40	// 0x28

//register 41 - data offset
#define HIFREG__DATA_OFFSET			41	// 0x29
#define Z_ON					0x01
#define Z_OFF					0x02

//register 42 - error codes
#define HIFREG__ERROR_CODE			42	// 0x2A
#define NO_ERROR				0
#define INVALID_SETUP_MODE			1
#define INVALID_SAMPLE_RATE			2
#define INVALID_FILTER_LENGTH			3
#define INVALID_X_LCORR_DATA			4
#define INVALID_Y_LCORR_DATA			5
#define INVALID_X_LCORR_OFFSET			6
#define INVALID_Y_LCORR_OFFSET			7
#define INVALID_Z_CORR_DATA			8
#define INVALID_Z_CORR_OFFSET			9
#define INVALID_ZPEN_ON_OFF_CMD			12
#define INVALID_ZPEN_ON_OFF_DATA		13
#define INVALID_ZFINGER_ON_OFF_CMD		14
#define INVALID_ZFINGER_ON_OFF_DATA		15
#define INVALID_ENFORCE_CAL_DATA 		16
#define INVALID_PINNACLE_WRITE			17
#define COMP_COMPARES__UNIT_BROKEN		18
#define NO_FACTORY_CAL_PERFORMED		19
#define INVALID_ZON_COUNT_DATA			20
#define INVALID_ZIDLE_COUNT_DATA		21
#define COMP_VALUES_NOT_IN_EEPROM		22
#define	INVALID_OEM_DATA_OFFSET	 		23
#define	OEM_VALUES_NOT_IN_EEPROM	 	24
#define INVALID_SAMPLE_LENGTH			25
#define INVALID_APERTURE_WIDTH			26
#define	INVALID_PEN_GAIN			27 // Was INVALID_COMMAND in version 0x1A
#define	INVALID_FINGER_GAIN			28
#define	INVALID_REXT_DATA			29
#define	REXT_VALUE_NOT_SET			30
#define INVALID_CHECKSUM_OFFSET			31
#define CHECKSUM_NOT_PERFORMED 			32
#define INVALID_K2_SETUP_CONFIG_OFFSET		33 // Was INVALID_COMMAND in version 0x1C
#define INVALID_K2_SETUP_CONFIG_DATA		34
#define	SETUP_CONFIG_VALUES_NOT_IN_EEPROM 	35     
#define INVALID_GPIO_SETTING			36
#define INVALID_COMMAND				37 // Was 27 in version 1A, 33 in version 0x1C
#define INVALID_BACKGROUND_COMP_TIME		38
#define INVALID_BACKGROUND_COMP_MAX_DIFF	39
#define INVALID_AXIS_SCALAR			40
#define CHECK_XPEN				41
//						42-45 internal commands20 Secure only
#define	NEXT ERROR				46 
#define	INVALID_EFS				47 
//						48 internal command
#define NFC_CAL_NOT_PERFORMED			48
#define PRESSURE_SENSOR_NOT_ENABLED		49

//register 43 - ZPEN_ON_LEVEL
#define HIFREG__Z_PEN_ON_LEVEL			43	// 0x2B
#define ZPEN_ON_DEFAULT				10
#define ZPEN_ON_MIN				2
#define ZPEN_ON_MAX				63

//register 44 - ZPEN_ON_LEVEL
#define HIFREG__Z_PEN_OFF_LEVEL			44	// 0x2C
#define ZPEN_OFF_DEFAULT			8
#define ZPEN_OFF_MIN				0
#define ZPEN_OFF_MAX				61

//register 45 - ZFINGER_ONLEVEL
#define HIFREG__Z_FINGER_ON_LEVEL		45	// 0x2D
#define ZFINGER_ON_DEFAULT			20
#define ZFINGER_ON_MIN				2
#define ZFINGER_ON_MAX				63

//register 46 - ZFINGER_ONLEVEL
#define HIFREG__Z_FINGER_OFF_LEVEL		46	// 0x2E
#define ZFINGER_OFF_DEFAULT			18
#define ZFINGER_OFF_MIN				0
#define ZFINGER_OFF_MAX				61

//register 47 - ZON_COUNT
#define HIFREG__ZON_COUNT			47	// 0x2F
#define ZON_COUNT				0x7F
#define ZON_COUNT_DISABLE			0x80
#define ZON_COUNT_DEFAULT			0x01
#define ZON_COUNT_MAX				127

//register 48 - ZIDLE_COUNT
#define HIFREG__ZIDLE_COUNT			48	// 0x30
#define ZIDLE_COUNT				0x7F
#define ZIDLE_COUNT_DISABLE			0x80
#define ZIDLE_COUNT_DEFAULT			0x1F
#define ZIDLE_COUNT_MAX				127

//register 49 - feed config
#define HIFREG__FEED_CONFIG			49	// 0x31
#define X_AXIS_INVERT				0x01
#define Y_AXIS_INVERT				0x02
#define TRACK_DISABLE				0x04 // Was X_AXIS_FLIP	in early versions
#define Z_SMOOTHING_FILTER_DISABLE		0x08 // Was Y_AXIS_FLIP	in early versions
#define SMOOTHING_FILTER_DISABLE		0x10
#define LC_CORRECTION_DISABLE			0x20
#define Z_CORRECTION_DISABLE		  	0x40
#define PRESSURE_SENSOR_DISABLE			0x80 // Versions <= 1E
#define FEED_DISABLE				0x80 // Versions >= 1F
#define FEED_CONFIG_DEFAULT_MASK		(X_AXIS_INVERT | Y_AXIS_INVERT | LC_CORRECTION_DISABLE | Z_CORRECTION_DISABLE)
#define FEED_CONFIG_DEFAULT			X_AXIS_INVERT

//register 50 - tracking mode
//only modify if ddirected by Cirque
#define HIFREG__TRACKING_MODE			50	// 0x32
#define PATTERN_12_X_16_NON_GAPPED		0x01
#define PATTERN_12_X_16_GAPPED			0x02
#define PATTERN_12_X_16_DOUBLE_GAPPED		0x04
#define PATTERN_11_X_15				0x08
#define X_Y_REPORT_SWITCHING_DISABLE		0x10
#define PATTERN_12_X_16_CDMA			0x20
#define PATTERN_6_X_8_GAPPED			0x40
#define ENABLE_SELF_TRACKING			0x80
// #define PATTERN_12_X_16_RAZOR_EVT1		0X94

//register 51 - Built in self test
#define HIFREG__BIST				51	// 0x33
#define BIST_NOISE_TEST_BITS			0xF0
#define BIST_MARGINAL_NOISE_PERFORMANCE	0x10	
#define BIST_FAIL_NOISE_TEST			0x20
#define BIST_PASS_NOISE_TEST			0x40
#define BIST_PANEL_TEST_BITS			0x0F
#define BIST_NO_TESTS_PERFORMED			0x00
#define BIST_FAIL_PANEL_TEST			0x01
#define BIST_PASS_PANEL_TEST			0x02
#define K2_RUN_BIST				0x80

#define HIFREG__PACKETBYTE1_X_LOW_BYTE		52	// 0x34
#define HIFREG__PACKETBYTE2_Y_LOW_BYTE		53	// 0x35
#define HIFREG__PACKETBYTE3_XY_HIGH_NIBBLE	54	// 0x36
#define HIFREG__PACKETBYTE4_Z			55	// 0x37
#define HIFREG__GESTURE_BYTE_1			56	// 0x38
#define HIFREG__GESTURE_BYTE_2			57	// 0x39
#define HIFREG__PRESSURE_1			58	// 0x3A

#define HIFREG__GESTURE_ANGLE			HIFREG__GESTURE_BYTE_1
#define HIFREG__GESTURE_LENGTH			HIFREG__GESTURE_BYTE_2
#define HIFREG__PRESSURE_HIGH_BITS		HIFREG__PRESSURE_1

#define HIFREG__CALIBRATION			59	// 0x3B
#define ENFORCE_FACTORY_CAL			0x01
#define FACTORY_CALIBRATION 			0x02
#define FACTORY_CALIBRATION_PERFORMED		0x04
#define CALIBRATION				0x10
#define CALIBRATION_PERFORMED			0x20
#define REPLACE_FACTORY_CAL			0x40
#define REPLACE_FACTORY_CAL_PERFORMED		0x80

//register 60 - por setup		   
#define HIFREG__POR_SETUP			60	// 0x3C		// 300ms
#define MIN_CALIBRATION_ATTEMPT_BITS		0x0F
#define MIN_CALIBRATION_ATTEMPTS		(Firmware_Info <= 0x722000 ? 0x10 : 0x05)
#define K2_SET_DR				0x20
#define K2_BOOT_LOADER_RESET			0x40
#define K2_POR_RESET				0x80

//register 61 - por result		   
#define HIFREG__POR_RESULT			61	// 0x3D
#define CALIBRATION_ATTEMPTS			0x0F
#define CALIBRATION_POR				0x10
#define CALIBRATION_FACTORY			0x20
#define CALIBRATION_NOT_ENFORCED		0x40

// register 62 - lower 2 bits of 10 bit pressure data
#define HIFREG__PRESSURE_2			62	// 0x3E

// register 63 -- filters on/offset
#define	HIFREG__CIRQUE_OPTIONS_1		63	// 0x3F
#define GENERAL_ID				0x00
#define ENABLE_FILTERING			0x01
#define JITTER_DISABLE				0x02
#define EXPONENTIAL_FILTER_DISABLE		0x04
#define CROSS_RATE_SMOOTHING_DISABLE		0x08
#define SIGNATURE_CAPTURE_MODE			0x10
#define Z_CORRECTION_FROM_X_AXIS		0x20
#define NUM_ZDEBOUNCE_PACKETS			0x40
#define X_Y_OFFSET_DISABLE			0x80

#define HIFREG__FILTERS_ENABLE			HIFREG__CIRQUE_OPTIONS_1 // Old

#define HIFREG__FILLER_BYTE             	0x7B
#define HIFREG__ADDR_INCREMENT          	0x7C 	// Note address wraps from 0x3f to x1f on K2 versions < 20

// K2 r/w commands
#define K2_WRITE(reg) 				(0x80|(reg))
#define K2_READ(reg)  				(0xC0|(reg))

#define K2_CORRECTION_DATA_SIZE 		(128 + 4)
#define K2_OEM_DATA_SIZE		 	16
#define K2_NUMBER_OF_REGS		 	64 // Number of K2 registers
#define K2_CAL_DATA_SIZE			64
#define K2_CAL_DATA_CHECK_OFFSET		(Firmware_Info >= 0x721F01 ? 60 : 0) // Need check only last 4 bytes (Y Global) for later versions
#define K2_BAD_CAL_LIMIT			30000
#define K2_BAD_CAL_LIMIT_SMALL_PANEL            28000
#define K2_RAILED_CAL_LIMIT			32481
#define K2_MISSING_PANEL_CAL_LIMIT		1000

//!!!!only change these gain values if directed by Cirque!!!!
#define GAIN_1X							0xC0
#define GAIN_2X							0x80
#define GAIN_3X							0x40
#define GAIN_4X							0x00

#define ADCCTRL_SAMPLES_32		0x00
#define ADCCTRL_SAMPLES_128		0x01 	// 2mm default
#define ADCCTRL_SAMPLES_256		0x02	// 7" panel 2mm pen (is better)
#define ADCCTRL_SAMPLES_512		0x03
#define ADCCTRL_SAMPLES_TEST         	0x04
#define ADCCTRL_SAMPLES_DEFAULT        	0xFF
		   
#define ADCAWIDTH_APERTURE_OPEN			0x00
#define ADCAWIDTH_APERTURE_125NS		0x01
#define ADCAWIDTH_APERTURE_250NS		0x02
#define ADCAWIDTH_APERTURE_375NS		0x03
#define ADCAWIDTH_APERTURE_500NS		0x04
#define ADCAWIDTH_APERTURE_625NS		0x05
#define ADCAWIDTH_APERTURE_750NS		0x06
#define ADCAWIDTH_APERTURE_875NS		0x07
#define ADCAWIDTH_APERTURE_1000NS		0x08
#define ADCAWIDTH_APERTURE_1125NS		0x09
#define ADCAWIDTH_APERTURE_1250NS		0x0A
#define ADCAWIDTH_APERTURE_1375NS		0x0B
#define ADCAWIDTH_APERTURE_1500NS		0x0C
#define ADCAWIDTH_APERTURE_1625NS		0x0D
#define ADCAWIDTH_APERTURE_1750NS		0x0E
#define ADCAWIDTH_APERTURE_1875NS		0x0F
#define ADCAWIDTH_APERTURE_DEFAULT		0xFF

//GPIOs
#define ADC1_GPIO				0x01 //B0
#define ADC2_GPIO				0x02 //B1
#define STDBY_BTN3_GPIO				0x04 //A2
#define SCL_BTN2_GPIO				0x10 //B4
#define SDA_BTN1_GPIO				0x20 //B5
#define HST_INTFC_SL_GPIO			0x80 //A1

#endif /*#ifndef __K2_H__*/
