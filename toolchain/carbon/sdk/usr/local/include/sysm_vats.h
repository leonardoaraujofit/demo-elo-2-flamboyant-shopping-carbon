#ifndef __SYSM_VATS__
#define __SYSM_VATS__

#include <sysmode_ui/settings_page.h>
#include <sysmode_ui/sysm_ui.h>
#include <sysmode_ui/sysm_lang.h>

enum {
	VATS_EVENT_LISTENER_LIST 	= 1,
	VATS_EVENT_LISTENER_STATUS
};

class vats_config_c: public SYSM_UI::settings_page_c
{
public:
	vats_config_c();
	void on_event(const event_c *event);
};


void vats_cb(const SYSM_UI::t_ui_menu_entry * e);

#endif // __SYSM_VATS__
