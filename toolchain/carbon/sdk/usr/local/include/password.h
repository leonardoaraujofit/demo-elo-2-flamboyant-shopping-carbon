#ifndef _PASSWORD_H_
#define _PASSWORD_H_

// only for testing
//#define TEST_PWD_SHOW

#include "pinentry.h"

#ifdef __cplusplus
extern "C" {
#endif

#define PASS_OK 0
#define PASS_FAIL -1
#define PASS_OPEN_FILE_FAIL -5

#define PASS_PENDING 1
#define PASS_NORMAL  0

/* Service Id's in the Keypad secure applet */
#define SA_GET_SHA_PASSWORD         4
#define SA_GET_SHA_PASSWORD_EXT     5

/*struct for write and read information about the user password from the password file*/
typedef struct
{
   char GUID[80];  
   int Pending;
}PasswordDBKey;

typedef struct
{
   PasswordDBKey key;
   unsigned char PasswordSHA[32];
}PasswordDBEntry;

typedef struct
{
   PasswordDBKey key;
   unsigned char PasswordSHA[32];
   unsigned char Salt[8];
}PasswordDBEntryExt;

/*
;|===========================================================================+
;| FUNCTION   | int Password_EnterPassword(void * Callback, PINPARAMETER  * PasswordParams, unsigned char *EnteredPassSha);
;|------------+---------------------------------------------------------------
;| DESCRIPTION| call this function to begin enter password (if salt is used call function Password_EnterPassword_Ext())
;|------------+---------------------------------------------------------------
;| PARAMETERS | Callback       - this callback function is called when the user finish enter his password (press the enter key.
;|            | PasswordParams - parameter about the password
;|            | EnteredPassSha - The function return the sha password after the user enter his password. 
;|            |                  This must be an array of at least 32 unsigned char's.  
;|------------+---------------------------------------------------------------
;| return     | PASS_OK   - A password was sucessfully entered and is available via 'EnteredPassSha'.
;|            | PASS_FAIL - A password was sucessfully entered, but Error occured retrieving password from vault.
;|            | Or Salt cannot be processed.
;|            | Or one of the following 'Exit Status' values, which indicate exit without valid password entered.
;|            | Note: These are iPS_GetPINResponse() defined return vaules.
;|            | 0x01      - Unit is idle, No longer in PIN entry mode, (possible iPS_CancelPIN() occured)
;|            | 0x05      - Aborted by user, The Cancel 'X' key was pressed.
;|            | 0x06      - No PIN entered.
;|            | 0x0A      - Aborted by user. Backspace pressed with empty pwd,
;|            | 0x0C      - Pin Session timed out.
;|------------+---------------------------------------------------------------
;| COMMENTS   |
;|===========================================================================+
*/
int Password_EnterPassword(void * Callback, PINPARAMETER * PasswordParams, unsigned char *EnteredPassSha);

/*
;|===========================================================================+
;| FUNCTION   |int Password_AddToDB(PasswordDBEntry * PassEntry)
;|------------+---------------------------------------------------------------
;| DESCRIPTION| add password struct to password file (if salt is used call function Password_AddToDB_Ext())
;|------------+---------------------------------------------------------------
;| PARAMETERS | PassEntry - the password struct
;|------------+---------------------------------------------------------------
;| return     | PASS_OPEN_FILE_FAIL or PASS_OK
;|            | PASS_FAIL - Salt cannot be processed.
;|------------+---------------------------------------------------------------
;| COMMENTS   |
;|===========================================================================+
*/
int Password_AddToDB(PasswordDBEntry * PassEntry);

/*
 |===========================================================================+
 | FUNCTION    | Password_GetEntry(PasswordDBEntry * PassKey);
 |----------------------------------------------------------------------------
 | DESCRIPTION | returns the ‘Entry’ given the ‘Key’ (if salt is used call function Password_GetEntry_Ext())
 |---------------------------------------------------------------------------
 | PARAMETERS  | PassKey - the PasswordDBEntry for input and output
 |----------------------------------------------------------------------------
 | return      |  PASS_OK for success     
 |             |  PASS_FAIL - Salt cannot be processed.
;|===========================================================================+
*/
int Password_GetEntry(PasswordDBEntry * PassKey);

/*
;|===========================================================================+
;| FUNCTION   | int Password_EnterPassword_Ext(void * Callback, PINPARAMETER  * PasswordParams, unsigned char* Salt, unsigned char *EnteredPassSha);
;|------------+---------------------------------------------------------------
;| DESCRIPTION| call this function to begin enter password (corresponding to Password_EnterPassword() extended by salt processing)
;|------------+---------------------------------------------------------------
;| PARAMETERS | Callback       - this callback function is called when the user finish enter his password (press the enter key.
;|            | PasswordParams - parameter about the password
;|            | Salt		   - salt for hash calculation
;|            | EnteredPassSha - The function return the sha password after the user enter his password. 
;|            |                  This must be an array of at least 32 unsigned char's.  
;|------------+---------------------------------------------------------------
;| return     | PASS_OK   - A password was sucessfully entered and is available via 'EnteredPassSha'.
;|            | PASS_FAIL - A password was sucessfully entered, but Error occured retrieving password from vault.
;|            | Or Salt is not available.
;|            | Or one of the following 'Exit Status' values, which indicate exit without valid password entered.
;|            | Note: These are iPS_GetPINResponse() defined return vaules.
;|            | 0x01      - Unit is idle, No longer in PIN entry mode, (possible iPS_CancelPIN() occured)
;|            | 0x05      - Aborted by user, The Cancel 'X' key was pressed.
;|            | 0x06      - No PIN entered.
;|            | 0x0A      - Aborted by user. Backspace pressed with empty pwd,
;|            | 0x0C      - Pin Session timed out.
;|------------+---------------------------------------------------------------
;| COMMENTS   |
;|===========================================================================+
*/
int Password_EnterPassword_Ext(void* Callback, PINPARAMETER* PasswordParams, unsigned char* Salt, unsigned char* EnteredPassSha);

/*
;|===========================================================================+
;| FUNCTION   |int Password_AddToDB_Ext(PasswordDBEntryExt * PassEntry)
;|------------+---------------------------------------------------------------
;| DESCRIPTION| add password struct to password file (corresponding to Password_AddToDB() extended by Salt processing)
;|------------+---------------------------------------------------------------
;| PARAMETERS | PassEntry - the password struct
;|------------+---------------------------------------------------------------
;| return     | PASS_OPEN_FILE_FAIL or PASS_OK 
;|            | PASS_FAIL - Salt is not available.
;|------------+---------------------------------------------------------------
;| COMMENTS   |
;|===========================================================================+
*/
int Password_AddToDB_Ext(PasswordDBEntryExt* PassEntry);

/*
 |===========================================================================+
 | FUNCTION    | Password_GetEntry_Ext(PasswordDBEntryExt * PassKey);
 |----------------------------------------------------------------------------
 | DESCRIPTION | returns the ‘Entry’ given the ‘Key’ corresponding to Password_GetEntry() extended by Salt processing)
 |---------------------------------------------------------------------------
 | PARAMETERS  | PassKey - the PasswordDBEntryExt for input and output
 |----------------------------------------------------------------------------
 | return      | PASS_OK for success     
 |             | PASS_FAIL - Salt is not available.
;|===========================================================================+
*/
int Password_GetEntry_Ext(PasswordDBEntryExt* PassKey);

/*
 |===========================================================================+
 | FUNCTION    | Password_UpgradeDB(void);
 |----------------------------------------------------------------------------
 | DESCRIPTION | in password DB all entries are extended with Salt zero
 |---------------------------------------------------------------------------
 | PARAMETERS  | -
 |----------------------------------------------------------------------------
 | return      | PASS_OPEN_FILE_FAIL or PASS_OK     
;|===========================================================================+
*/
int Password_UpgradeDB(void);

/*
 |===========================================================================+
 | FUNCTION    | Password_SaltUsed(void);
 |----------------------------------------------------------------------------
 | DESCRIPTION | check if password DB entries include Salt
 |---------------------------------------------------------------------------
 | PARAMETERS  | -
 |----------------------------------------------------------------------------
 | return      | 1, if Salt is available, 0 else
;|===========================================================================+
*/
int Password_SaltUsed(void);

/*
 |===========================================================================+
 | FUNCTION    | Password_SaltEnabled(void);
 |----------------------------------------------------------------------------
 | DESCRIPTION | check if Salt processing is mandatory
 |---------------------------------------------------------------------------
 | PARAMETERS  | -
 |----------------------------------------------------------------------------
 | return      | 1, if Salt is mandatory, 0 if Salt must not be used
;|===========================================================================+
*/
int Password_SaltEnabled(void);

#ifdef TEST_PWD_SHOW
int Password_ShowDB(void);	// only for testing
#endif


#ifdef __cplusplus
}
#endif

#endif
