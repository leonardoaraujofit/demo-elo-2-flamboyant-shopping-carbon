/*
 * secmode_utils.h
 *
 *  Created on: 27 Nov 2014
 *      Author: robert
 */

#ifndef SECMODE_UTILS_H_
#define SECMODE_UTILS_H_

#include "ber-tlv.h"

#define KEY_DIR_ACCESS	(S_IRWXU | S_IRWXG | S_IRWXO)
#define SECMODE_FILE_ACCESS (S_IRUSR | S_IWUSR | S_IRGRP)
#define CTX_FILE_ACCESS (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH)
#define ROOT_UID        0

void print_hex(const char *label, const unsigned char *data, int len);
unsigned int is_reg_file(const char *path);
unsigned char *malloc_file(unsigned int *filesize, const char *filename);
char *malloc_text_file(unsigned int *filesize, const char *filename);
unsigned char *malloc_file_suffix(unsigned int *filesize,
								  const char *filename,
								  const char *suffix);
unsigned char *copy_files_to_buf(unsigned int *sigsz,
                                unsigned int *binsz,
                                const char *sigfile,
                                const char *binfile);
void Secmode_set_msg_debug(int value);
int make_key_dir(const char *dir, mode_t mode, uid_t uid, gid_t gid);
int make_key_dir_recursive(const char *dir, const char *prefix, mode_t mode, uid_t uid, gid_t gid);
int setup_file_access(const char *file, mode_t mode, uid_t uid, gid_t gid,
	   					unsigned int (*checkmode_cb)(mode_t st_mode));

int handle_secmode_files(BER_TLV *resp);
int handle_secmode_files_with_mode(BER_TLV *resp, mode_t mode);
#endif
