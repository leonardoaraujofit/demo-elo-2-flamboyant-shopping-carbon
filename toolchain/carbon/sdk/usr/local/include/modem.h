/*
 * modem_drv.h - Upper Layer interface
 *
 *  Created on: May 2, 2010
 *  Author: Gil
 */

#ifndef MODEM_H_
#define MODEM_H_

/* modem status */
#define MDM_STT_RESET				1
#define MDM_STT_INIT				2
#define MDM_STT_IDLE				3
#define MDM_STT_DAILING				4
#define MDM_STT_DURING_CONNECT		5
#define MDM_STT_CONNECTED			6
#define MDM_STT_HANG_UP				7
#define MDM_STT_SETUP				8
#define MDM_STT_LIB_PAUSE               	9
#define MDM_STT_NOT_EXIST                   10
#define MDM_STT_STOP				98
#define MDM_STT_UNKNOWN				99

/* last connection error */
#define MDM_CONERR_NONE				1
#define MDM_CONERR_NO_ANSWER		2
#define MDM_CONERR_NO_CARRIER		3
#define MDM_CONERR_NO_DIALTONE		4
#define MDM_CONERR_USR_HANGUP		5
#define MDM_CONERR_BUSY				6
#define MDM_CONERR_SETUP_ERROR		7
#define MDM_CONERR_TIMEOUT			8

/* trace mode */
#define MDM_TRACE_NONE		0
#define MDM_TRACE_CONSOLE	1
#define MDM_TRACE_COM		2
#define MDM_TRACE_FILE		3		// /var/log/modem

/* trace mode */
#define MDM_POWER_IDLE		0
#define MDM_POWER_SLEEP		1
#define MDM_POWER_DEEP_SLEEP	2
#define MDM_POWER_STOP		3	

/* return values */
#define MDM_OK				0
#define MDM_FAIL			-1
#define MDM_INVALID_PARAM	-2
#define MDM_BUSY			-3
#define MDM_NOT_CONNECTED	-4
#define MDM_NOT_SUPPORTED	-5

/* call mode */
#define MDM_MODE_SYNC		1
#define MDM_MODE_ASYNC		2

#define MDM_TONE_DIAL		1
#define MDM_PULSE_DIAL		2

/* error correction */
#define MDM_ERRCOR_DISABLE			1
#define MDM_ERRCOR_V42_MNP			2
#define MDM_ERRCOR_V42_MNP_ONLY		3
#define MDM_ERRCOR_V42_ONLY			4
#define MDM_ERRCOR_MNP_ONLY			5

/* compression */
#define MDM_COMPRESSION_DISABLE		1
#define MDM_COMPRESSION_V42_MNP  	2 /* error correction must not be disabled */

#define MDM_DISABLE			0
#define MDM_ENABLE			1

#define MDM_FALSE			0
#define MDM_TRUE			1

//s37 + ATB0 ?
/* connection rates */
#define MDM_RATE_300			1// V21
#define MDM_RATE_300_B			2// Bell103
#define MDM_RATE_1200			3// V22
#define MDM_RATE_1200_FAST		4// V22 + AT\F0
#define MDM_RATE_1200_B			6// Bell212A
#define MDM_RATE_2400			7// V22B
#define MDM_RATE_4800			8// V32
#define MDM_RATE_14400			12// V32B
#define MDM_RATE_33600			14// V34
#define MDM_RATE_56000			15// V92

// more flags
#define MDM_TX_BUFF 1
#define MDM_RX_BUFF 2

#define MDM_STR_INFO_SIZE			16
#define MDM_STR_LONG_INFO_SIZE		32
#define MAX_PHONE_NUM_LENGTH		32

#define MDM_MAX_COUNTRY_NAME_SIZE 	40
#define MDM_MAX_COUNTRY_CODE_SIZE 	3

#ifdef RAPTOR
#define MDM_RAPTOR_TTY_NAME "/dev/ttyPSTN0"
#endif

//#define MDM_DEV_INTERFACE "/dev/modem"

struct st_modem_info{
	char manufacture	[MDM_STR_INFO_SIZE]; 		/// e.g. conexant
	char model			[MDM_STR_INFO_SIZE];		/// e.g. V90
	char product_code	[MDM_STR_INFO_SIZE];		/// e.g. 56000
	char firmware		[MDM_STR_LONG_INFO_SIZE];	/// e.g. CX93001-EIS_V0.2013-V92
	int  country_code;								/// according annex a T.35
	char library_version	[MDM_STR_INFO_SIZE];	/// libmodem version
	char patch    [MDM_STR_LONG_INFO_SIZE];              /// patch loading 
};

// Supported T.35 County Codes
// (00,07,09,0A,0F,16,1B,20,25,26,27,2D,2E,31,36,3C,3D,42,46,50,51,52,53,54,57,58,59,61,62,64,69,6C,73,77,7B,7E,82,84,89,8A,8B,8E,98,99,9C,9F,A0,A1,A5,A6,A9, AD,AE,B3,B4,B5,B7,B8,C1,F9,FA,FB,FC,FD,FE)

struct st_answer_params{
	char 	number_of_rings;	// 0 - for auto answer disable
	char 	mode;				// async / sync
	char 	max_line_rate;		// MDM_RATE_ ...
	char 	connect_timeout;	// since start answering
	char 	error_correction;		// MDM_ERRCOR_ ...
	char 	compression;			// MDM_COMPRESSION_.... error_correction must be enable !
};

struct st_dial_params{
	char	dial_type;				// tone / pulse
	char	mode;					// async / sync
	char	max_retries;			// dial retries
	char	max_line_rate;			// MDM_RATE_....
	char	connect_timeout;		// waiting for answer, if expired no more retries
	char 	error_correction;		// MDM_ERRCOR_ ...
	char 	compression;			// MDM_COMPRESSION_.... error_correction must be enable !
};

struct st_connect_info{
	char	modulation[MDM_STR_INFO_SIZE];		// example "V92"
	int		tx_rate;							// example 14400
	int 	rx_rate;
};

struct st_last_connect_info{
	char connect_error;			// MDM_CONERR_....
};

int modem_start(char *usr_commands_file);

int modem_stop(void);

//int modem_close(void);

int modem_get_country_profile(char* country_id, int buf_size);

int modem_set_country_profile(char* country_id);

int modem_get_modem_info( struct st_modem_info *modem_info_ptr, int *size);

int modem_get_status(void);

int modem_get_last_call_info(struct st_last_connect_info *last_connect_info_ptr, int *size);

int modem_is_ringing(void);

int modem_is_line_connected(void);

int modem_dial(char *dial_str, struct st_dial_params *dial_param_ptr);

int modem_set_answer_params(struct st_answer_params *answer_params_ptr);

int modem_set_force_answer_params(struct st_answer_params *answer_params_ptr);

int modem_get_answer_params(struct st_answer_params *answer_params_ptr, int *size);

int modem_answer_call(void);

int modem_set_low_power_mode(int iMode, int iSleepInactivityTimer);

int modem_reset(void);

int modem_hang_up(void);

int modem_send_data(char *buff, int size);

int modem_test_pause(void);

int modem_receive_data(char *buff, int size, int timeout_millsec );

int modem_data_flush(int buffers_flag); // MDM_TX_BUFF , MDM_RX_BUFF

int modem_get_connect_info(struct st_connect_info *connect_info_ptr, int *size);

int modem_pipe_commands(char *tx, char *rx, int rx_size, int max_retries, int response_timeout);

int modem_trace_mode(int mode); // MDM_TRACE_...

int modem_get_dev_path(int type, char *dev_path, int dev_path_length);

/* Returns either country_code or country_name
 * depends if other parameter == NULL
 */
int modem_get_country_info_from_list(char *country_code, int sizeCountryCode, char *country_name, int sizeCountryName);


#endif /* MODEM_H_ */
