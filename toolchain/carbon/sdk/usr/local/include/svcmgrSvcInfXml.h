/** @addtogroup utils Utils */
/** @{ */
/**
 *  @file svcmgrSvcInfXml.h
 *
 *  @brief Service interface XML API
 *
 */

/*
*  Copyright, 2015 VeriFone Inc.
*  2099 Gateway Place, Suite 600
*  San Jose, CA.  95110
*
* All Rights Reserved. No part of this software may be reproduced,
* transmitted, transcribed, stored in a retrieval system, or
* translated into any language or computer language, in any form
* or by any means electronic, mechanical, magnetic, optical,
* chemical, manual or otherwise, without the prior permission of
* VeriFone Inc.
*
*/
#ifndef _SVCMGR_SVCINFUTILSXML_H_
#define _SVCMGR_SVCINFUTILSXML_H_

#ifdef __cplusplus
extern "C" {
#endif



/**
* @name SMX element types
*/
/** \{ */
#define SMX_ELEMTYPE_UNKNOWN		(0)	/*!< */
#define SMX_ELEMTYPE_SIMPLE			(1) /*!< */
#define SMX_ELEMTYPE_STRING			(2) /*!< */
#define SMX_ELEMTYPE_CONTAINER		(3) /*!< */
#define SMX_ELEMTYPE_LIST			(4) /*!< */
#define SMX_ELEMTYPE_PAIRLIST		(5) /*!< */
#define SMX_ELEMTYPE_BINARY			(6) /*!< */
#define SMX_ELEMTYPE_FIXEDPAIRLIST	(7) /*!< */
/** \} */

/**
* @brief Return types for SMX methods
*/
typedef void* H_DOC;
typedef void* H_ELEM;

/**
* @note
* All fn's that return char* are malloc'd internally and must be
* free'd by the caller.
*/

/**
* Used for parsing and building xml.
*
* @param[in] hDoc XML document pointer.
*
* @return handle to <method> Element of xml, or NULL in case of error.
*/
H_ELEM smx_MethodElement(H_DOC hDoc);


/**
* Begin parsing of incoming svc_cmd xml.
*
* @param[in] cmd svc_cmd to parse.
*
* @return handle to document, or NULL in case of error.
*/
H_DOC smx_BeginCmd(char* cmd);

/**
* Finish parsing of incoming svc_cmd xml.
*
* @param[in] hDoc Document handle.
*/
void smx_EndCmd(H_DOC hDoc);

/**
* Get service name.
*
* @param[in] hDoc Document handle.
*
* @return service name, or NULL in case of error.
*/
char* smx_GetService(H_DOC hDoc);

/**
* Get method name.
*
* @param[in] hDoc Document handle.
*
* @return method name, or NULL in case of error.
*/
char* smx_GetMethod(H_DOC hDoc);

/**
* Get child element.
*
* @param[in] hElem Element handle.
*
* @return H_ELEM
*
* @note
* Use smx_MethodElement() as input to this function to begin.
*/
H_ELEM smx_ChildElementStart(H_ELEM hElem);

/**
* Get next element.
*
* @param[in] hElem Element handle.
*
* @return H_ELEM, or NULL if no more sibling Elements.
*
*/
H_ELEM smx_ElementNext(H_ELEM hElem);


/**
* Get element parent.
*
* @param[in] hElem Element handle.
*
* @return H_ELEM
*
*/
H_ELEM smx_ElementParent(H_ELEM hElem);


/**
* Get element name.
*
* @param[in] hElem Element handle.
*
* @return element name as string
*/
char* smx_GetElementName(H_ELEM hElem);

/**
* Get element content text.
*
* @param[in] hElem Element handle.
*
* @return element content text as string
*/
char* smx_GetElementText(H_ELEM hElem);

/**
* Get element type.
*
* @param[in] hElem Element handle.
*
* @return returns one of the SMX_ELEMTYPE_XXX values
*/
int   smx_GetElementType(H_ELEM hElem);

/**
* Get element text as integer value.
*
* @param[in] hElem Element handle.
*
* @return atoi() of Element text.
*/
int   smx_GetElementTextAsInt(H_ELEM hElem);

/**
* Start building outgoing svc_rtn xml
*
* @param[in] service Service name.
* @param[in] method Service method name.
*
* @return document handle, or NULL in case of error
*/
H_DOC smx_BeginRtn(char* service, char* method);

/**
* Finish building outgoing svc_rtn xml.

* @param[in] hDoc Document handle.
*
* @return XML document as string
*/
char* smx_EndRtn(H_DOC hDoc);


/**
* Add element to existing parent element.
*
* @param[in] hParentElem
* @param[in] szName
* @param[in] szText
* @param[in] szAttrName
* @param[in] szAttrValue
*
* @return handle of the newly created RootElement, or NULL in case of error.
*
* @note
* there should only be one 'return' Element (child of MethodElement).
* for this single 'return' Element - 
* This fn is written in a generic fashion for possible future changes
* but currently 'szName' should always be "return".  Also 'szAttrName'
* should either be NULL (no attr) or "type", with 'szAttrValue' being
* one of the five defined types :
*      "string", "container", "list", "pairlist", "fixedpairlist" "binary".
* For no attr leave 'szAttrName' & 'szAttrValue' NULL.
*/
H_ELEM smx_AddElement(H_ELEM hParentElem, char* szName, char* szText, char* szAttrName, char* szAttrValue);

	// Simple return builder fn's

/**
* Build return xml.
*
* @param[in] service Service name.
* @param[in] method Method name.
* @param[in] rtn
*
* @return string of built xml, or NULL in case of error
*/
char* smx_BuildRtnXml(char* service, char* method, char* rtn);

/**
* Build errorcode xml.
*
* @param[in] service Service name.
* @param[in] method Method name.
* @param[in] errcode Error code
*
* @return string of built xml, or NULL in case of error
*/
char* smx_BuildErrcodeRtnXml(char* service, char* method, int errcode);

/**
* Build callcode xml.
*
* @param[in] service Service name.
* @param[in] method Method name.
* @param[in] callcode Call code.
*
* @return string of built xml, or NULL in case of error
*/
char* smx_BuildCallcodeRtnXml(char* service, char* method, int callcode);

/**
* Create new xml document.

* @return handle to newly created document, or NULL in case of error.
*/
H_DOC smx_DocNew(void);

/**
* Free an existing XML document.
*
* @param[in] doc Document handle.
* 
* @return xml document in string forma
*/
char* smx_DocFree(H_DOC doc);

/**
* Encode the given string with base64 algorithm.
*
* @param[in] data String data to encode with base64.
* @param[in] input_len Input data length.
* @param[out] output_len Output data length.
*
* @return base64 encoded string, or NULL in case of error
*/
char* base64_encode(const char *data, unsigned int input_len, unsigned int *output_len);

/**
* Decode the given string with base64 algorithm.
*
* @param[in] data String data encoded with base64
* @param[in] input_len Input data length.
* @param[out] output_len Output data length.
*
* @return decoded base64 data string
*/
char* base64_decode(const char *data, unsigned int input_len, unsigned int *output_len);


#ifdef __cplusplus
}
#endif

#endif // _SVCMGR_SVCINFUTILSXML_H_
///@}
