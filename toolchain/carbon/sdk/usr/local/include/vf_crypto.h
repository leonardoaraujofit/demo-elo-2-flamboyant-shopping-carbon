
// vf_crypto.h
// Original creation:  tim_m6 2015-10-20

#ifndef VF_CRYPTO_H
#define VF_CRYPTO_H

#ifdef __cplusplus
extern "C" {
#endif

#include <verifone/vf_pubsecif.h>

typedef struct
{
    unsigned char* data;      // Data
    int dataLen;              // Data Length
    int keyType;              // Key Type
    int keyIndex;             // Key Index
    int keyVariant;           // Key Variant
    int alg;                  // Algorithm
    int encrypt_flag;         // Encrypt Flag
    int mode;                 // Mode of Operation
    int iv;                   // IV Setting
    unsigned char* iv_data;   // IV Data
    int pad;                  // Padding Scheme
    int DUKPT_KSN_key_state;  // Set DUKPT KSN Key State
    unsigned char* MAC_value_to_verify;
    int MAC_value_to_verify_len;  // Valid length is 4 to 16 bytes
} vf_crypto_in;

typedef struct
{
    unsigned char* data;    // Data
    int dataLen;            // Data Length
    unsigned char* ivData;  // IV Data
    unsigned char* supData; // Supplementary Data
    int MAC_value_verified;
} vf_crypto_out;


int vf_crypto( vf_crypto_in* in, vf_crypto_out* out );
const char *vf_crypto_strerror(int vf_crypto_err);

#ifdef __cplusplus
}
#endif

#endif  // VF_CRYPTO_H



