/*********************************************************************
*
*		Copyright, 2004 VeriFone Inc.
*		2099 Gateway Place, Suite 600
*		San Jose, CA.  95110
*
*  All Rights Reserved. No part of this software may be reproduced,
*  transmitted, transcribed, stored in a retrieval system, or
*  translated into any language or computer language, in any form
*  or by any means electronic, mechanical, magnetic, optical,
*  chemical, manual or otherwise, without the prior permission of
*  VeriFone Inc.
*
***********************************************************************/

/***********************************************************************
*
*               Title:  wrenchAPI.h
*
*		This file contains serial to spi interface driver
*		defines and declarations for Wrenchman serial/ecr device
*		that will be used by applications.
*
***********************************************************************/
#ifndef WRENCH_API
#define WRENCH_API

#ifdef __cplusplus
extern "C" {
#endif
 
#define WRENCH_IHEX_FLAGS 2
#define WRENCH_IHEX_OVERHEAD 11
#define WRENCH_IHEX_MAX_DATA 16 * 2
#define WRENCH_IHEX_MAX_REC (WRENCH_IHEX_FLAGS + WRENCH_IHEX_OVERHEAD + WRENCH_IHEX_MAX_DATA)
#define WRENCH_IHEX_FINAL_REC ":00000001FF"

#define MODE_CLOSE_CHANNEL	0x00	/* Select Controller Mode: Close Channel*/
#define MODE_DEMAND			0x80	/* Select Controller Mode: Demand Access*/
#define MODE_R232_INT		0x81	/* mode parameter for Select Controller Mode command */
									/* sets interrupt, no watchdog timer, RS232 mode */
#define MODE_R485_INT		0x82	/* mode parameter for Select Controller Mode command */
									/* sets interrupt , no watchdog timer, RS485 mode */
#define MODE_SIO_INT		0x83	/* mode parameter for Select Controller Mode command */
									/* sets interrupt , no watchdog timer, SIO Device mode */

#define ECR_SET_HIGH_BIT	(unsigned char)0x80
#define MAX_TGCMD_CMDLEN	(16)

/* Valid Device Addresses */
#define SIO_DEVADDR_64	0x64
#define SIO_DEVADDR_65	0x65
#define SIO_DEVADDR_68	0x68
#define SIO_DEVADDR_69	0x69

/* SIO Handshake defines */
#define SIOHS_RX_ON		0x80
#define SIOHS_TX_HS		0x40
#define SIOHS_AUX_ON	0x20
#define SIOHS_RTS_ON	0x10
#define SIOHS_CTS		0x02
#define SIOHS_READY_ON	0x08
#define SIOHS_RTS_WAIT	SIOHS_RX_ON | SIOHS_TX_HS | SIOHS_RTS_ON | SIOHS_READY_ON
#define SIOHS_RTS_READY	SIOHS_RX_ON | SIOHS_READY_ON

/* Defines for Wrenchman Errors */
#define ERR_CMD_MSG_SIZE	0x01
#define ERR_CMD_UNKNOWN		0x02
#define ERR_CMD_MODE		0x03
#define ERR_CMD_PARM		0x04
#define ERR_MSG_STATE		0x05
#define ERR_RX_PARITY		0x06
#define ERR_RX_FRAMING		0x07
#define ERR_DATA_FMT		0x08
#define ERR_MSG_TIMEOUT		0x09

/* Defines for Wrenchman Tally Counters */
#define WRENCH_OTHER_POLLS	0x01
#define WRENCH_LOCAL_POLLS	0x02
#define WRENCH_MSG_ERRORS	0x03
#define WRENCH_SEQ_ERRORS	0x04
#define WRENCH_POR_ERRORS	0x05

/* ECR/Wrenchman IOCTL Defines*/
#define IOCTLCOM3		0xC300
#define IOCTLC3SCM		0xC301
#define IOCTLC3FLB		0xC303
#define IOCTLC3RES		0xC304
#define IOCTLC3ESM		0xC305
#define IOCTLC3RFV		0xC306
#define IOCTLC3RTI		0xC308
#define IOCTLC3RST		0xC309
#define IOCTLC3CAN		0xC30F
#define IOCTLC3SETCCFG		0xC310
#define IOCTLC3SRD		0xC311
#define IOCTLC3SCD		0xC312
#define IOCTLC3RRT		0xC313
#define IOCTLC3BFI		0xC314
#define IOCTLC3SHS		0xC315
#define IOCTLC3STD		0xC318
#define IOCTLC3SDA		0xC31A
#define IOCTLC3SEC		0xC31B
#define IOCTLC3SIOHS		0xC31C
#define IOCTLC3MTO		0xC31D
#define IOCTLC3PLD		0xC3AB
#define IOCTLC3LD		0xC37F
#define IOCTLC3DCMD		0xC3DC
#define IOCTLC3DCQM		0xC3DD
#define IOCTLC3DEBUG            0xC3DE
#define IOCTLC3SETRXSIZE        0xC3E0

/* ECR/Wrenchman IOCTL structure */
struct wrenchIoctl {
	long			wrenchIoctlCmd;
	unsigned int	wrenchArgSize;
	unsigned char	*wrenchIoctlArg;
	int				wrenchIoctlRetVal;
};

/* Prototypes */
#ifdef __cplusplus
}
#endif
#endif
