#ifndef __SYSM_UPDATE__
#define __SYSM_UDPATE__

#include <sysmode_ui/sysm_lang.h>
#include <sysmode_ui/sysm_ui.h>

typedef struct serial_config {
    const char *  device;
    int     rate;
} t_serial_config;

typedef enum _eExStorage
{
	SYSM_STORAGE_USB = 0,
	SYSM_STORAGE_SD
}eExStorageType;

extern const char * NO_UARTS_FOUND;
extern const char * ERR_UARTS_INFO;
extern const int MAX_UART;

void up_nfs_cb(const SYSM_UI::t_ui_menu_entry * e);
void up_usb_cb(const SYSM_UI::t_ui_menu_entry * e);
void up_com_cb(const SYSM_UI::t_ui_menu_entry * e);
void up_net_cb(const SYSM_UI::t_ui_menu_entry * e);
void up_sd_cb(const SYSM_UI::t_ui_menu_entry * e);
void reboot_cb(const SYSM_UI::t_ui_menu_entry * e);
#endif // __SYSM_UPDATE__
