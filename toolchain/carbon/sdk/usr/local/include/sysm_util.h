#ifndef __SYSM_UTIL__
#define __SYSM_UTIL__

#include <stdio.h>
#include <sysmode_ui/sysm_ui.h>
#include <sysmode_ui/sysm_lang.h>

extern "C" {
    #include <libsecins.h>
}

#define  SHA_SIZE   32
#define  HIDE_VSS_BUNDLES 1

struct uart_info {
    const char * name;
    char         path[16];
};

extern const char * INSTALL_DIR;

void calc_sha(unsigned char *sha, const char *data, int datalen);
void run_apps(const SYSM_UI::t_ui_menu_entry *);

void extension_cb(const SYSM_UI::t_ui_menu_entry *e);
void extend_menus(vector<SYSM_UI::t_ui_menu_list *> * menu_lists);

char * read_file(const char *, int);
int    write_file(const char *, const char *);
int    copy_file(const char *, const char *, const char *);
int    is_empty(const char * );

int install_pkgs(void *, Fl_Box * = NULL);
void sec_run_apps(void);

void * xmalloc(size_t size);
void * xrealloc(void *, size_t);

void get_pkg_list(SecinsPkgInfo *, unsigned int, unsigned int *);
void get_bndl_list(SecinsBdlInfo *, unsigned int, unsigned int*, const char *, int hide_vss_bundles = 0);

const char * get_country_name(int t35_code);

using std::vector;
vector<struct uart_info> get_uarts_info();

#endif // __SYSM_UTIL__
