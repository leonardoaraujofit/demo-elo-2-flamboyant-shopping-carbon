
#ifndef __EVENT_TYPES_H__
#define __EVENT_TYPES_H__

#include <stddef.h>
#include <event_engine/event_debug.h>

typedef int event_id_t;
typedef int event_type_id_t;
typedef int event_listener_id_t;

class event_c;
class event_engine_c;
class event_listener_c;

// special event listener id
enum {
	EVENT_LISTENER_ENGINE		= 0,
	EVENT_LISTENER_ALL			= -1,
	EVENT_LISTENER_INVALID		= -2,
};

// event id
enum {
	EVENT_TYPE_ENGINE_STARTED,
	EVENT_TYPE_ENGINE_STOPPED,
	EVENT_TYPE_LISTENER_ATTACHED,	// listener, listener_id
	EVENT_TYPE_LISTENER_DETACHED,	// listener, listener_id
	EVENT_TYPE_EVENT_PROCESSED,		// event_id, result
	EVENT_TYPE_EVENT_NOT_DELIVERED,
};


#endif

