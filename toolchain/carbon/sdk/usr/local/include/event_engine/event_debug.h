
#ifndef __EVENT_DEBUG_H__
#define __EVENT_DEBUG_H__

#ifdef __DEBUG__

#include <stdio.h>

#define __EVENT_DEBUG__
#define __EVENT_ENGINE_PRINTF(text, ...)		printf(text, ##__VA_ARGS__)

#else

#define __EVENT_ENGINE_PRINTF(text, ...)		do {} while(false)

#endif

#endif

