
#ifndef __EVENT_LISTENER_H__
#define __EVENT_LISTENER_H__

#include <event_engine/event_types.h>

class event_listener_c
{
	int m_listener_id;
	event_engine_c *m_engine;

public:
	event_listener_c(
			int event_listener_id = 0);
	virtual ~event_listener_c();

	int get_listener_id() const;
	bool is_attached() const;

protected:
	bool send_event(
			event_c *event);
	virtual void on_event(
			const event_c *event) = 0;

#ifdef __EVENT_DEBUG__
	virtual void print() const = 0;
#endif

private:
	bool attach_engine(
			event_engine_c *event_engine);
	bool detach_engine();

	friend class event_engine_c;
};

#endif

