
#ifndef __EVENT_H__
#define __EVENT_H__

#include <event_engine/event_types.h>

class event_c
{
	event_type_id_t m_type_id;					///< type of the event (it shall be used to cast event_c class)
	event_id_t m_id;							///< unique event ID set by event engine
	event_listener_id_t m_source_id;			///< listener ID of sender
	event_listener_id_t m_destination_id;		///< listener ID of destination

public:
	event_c(event_type_id_t type_id);
	event_c(event_type_id_t type_id,
			event_listener_id_t destination_id);
#ifdef __EVENT_DEBUG__
	event_c(event_type_id_t type_id,
			event_listener_id_t destination_id,
			event_listener_id_t source_id);
#endif
	virtual ~event_c();

	event_type_id_t get_type_id() const;
	event_id_t get_id() const;
	event_listener_id_t get_source_id() const;
	event_listener_id_t get_destination_id() const;

#ifdef __EVENT_DEBUG__
	virtual void print() const;
#endif

private:
	void set_id(
			event_id_t id);
	void set_source_id(
			event_listener_id_t source_id);

	friend class event_engine_c;
	friend class event_listener_c;
};

#endif

