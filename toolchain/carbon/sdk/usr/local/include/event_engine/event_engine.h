
#ifndef __EVENT_ENGINE_H__
#define __EVENT_ENGINE_H__

#include <event_engine/event_types.h>
#include <map>
#include <queue>
#include <pthread.h>

class event_engine_c
{
	typedef std::map<event_listener_id_t, event_listener_c*> event_listeners_map_t;

	event_listeners_map_t m_event_listeners;

	mutable pthread_mutex_t m_event_listeners_mutax;

	std::queue<event_c*> m_events;
	mutable pthread_mutex_t m_events_mutax;
	event_id_t m_event_id;

	bool m_is_running;
	pthread_t m_thread_id;

public:
	event_engine_c();
	virtual ~event_engine_c();

	bool attach(
			event_listener_c *event_listener);
	bool detach(
			event_listener_c *event_listener);

	bool start();
	void stop();

	bool trylock_listeners() const;
	void unlock_listeners() const;

	event_id_t send_event(
			event_c *event);
	virtual void on_event(
			const event_c *event);

#ifdef __EVENT_DEBUG__
	void print_listeners() const;
#endif

private:
	static void *event_engine_thread(
			void *arg);
};

#endif

