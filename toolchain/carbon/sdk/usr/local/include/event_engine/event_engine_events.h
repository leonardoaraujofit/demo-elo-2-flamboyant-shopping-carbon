
#ifndef __EVENTS_H__
#define __EVENTS_H__

#include <event_engine/event.h>

class event_engine_started_c : public event_c
{
public:
	event_engine_started_c();

#ifdef __EVENT_DEBUG__
	void print() const;
#endif
};

class event_engine_stopped_c : public event_c
{
public:
	event_engine_stopped_c();

#ifdef __EVENT_DEBUG__
	void print() const;
#endif
};

#endif

