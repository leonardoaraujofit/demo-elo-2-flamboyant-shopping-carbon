/*******************************************************************
 *
 *  LLU - Low Level installer
 *  File: lli.h
 *  Description: LLI SO header structure declarations
 *
 *  Version: 01_00
 *
 *****************************************************************************/

#ifndef _LLI_H_
#define _LLI_H_


#include <verifone/sbi.h>
#include <verifone/boot.h>

          /*==========================================*
           *           D E F I N I T I O N S          *
           *==========================================*/

/*
 * Image type
 */


#define LLI_SBI 		0x1
#define LLI_SBI2 		0x2
#define LLI_CIB 		0x3
#define LLI_VAULT 		0x4
#define LLI_UBOOT 		0x5

#define LLI_SPL_MLO		0x6
#define LLI_BOOT_CERT	0x7
#define LLI_SPL			0x8
#define LLI_MLO         0x9

#define LLI_MIB			0xA
#define LLI_BSI			0xB

/*
 * Layout Information
 */

#if (RAPTOR)
#define NUM_OF_MTD      8
#else
#define NUM_OF_MTD      3
#endif
#define SBI_BLOCKS      3
#define MIB_BLOCKS      3
#define BOOT_BLOCKS    (32 - (SBI_BLOCKS + MIB_BLOCKS + 2)) // 2 is for safety margin




/*
 * LLI return values
 */

#define LLI_ERROR 	-1
#define LLI_OK 	     0

#define LLI_VERSION      0
#define LLI_VERSION_LEN  16

#define UBOOT_VERSION_LEN 25

#define GENERIC_VERSION_SIZE		50

          /*==========================================*
           *      T Y P E   D E C L A R A T I O N     *
           *==========================================*/

typedef struct
{
   union
   {
      char               LibVersion[LLI_VERSION_LEN];
      BootInfo           boot_info;
      SBISpecificHeader  spec_hdr;      // Verifone part of header
      SBI_HEADER         sbi;           // BCM part of header
	  char				 genericVersion [GENERIC_VERSION_SIZE];
   }t;

} LLIHeaderInfo;

          /*=========================================*
           *   P U B L I C     F U N C T I O N S     *
           *=========================================*/

/* --------------------------------------------------------------------------
 *
 * FUNCTION NAME: LLI_GetVersion
 *
 * DESCRIPTION:   Get Component Version from flash
 *
 * RETURN:        LLI_OK if found
 *                LLI_ERROR on any other error
 * NOTES:         none.
 *
 * ------------------------------------------------------------------------ */
int LLI_GetVersion(int image, LLIHeaderInfo* VersionInfo);

/* --------------------------------------------------------------------------
 *
 * FUNCTION NAME: LLI_GetVersion_from_file
 *
 * DESCRIPTION:   Get Component Version from file
 *
 * RETURN:        LLI_OK if found
 *                LLI_ERROR on any other error
 * NOTES:         none.
 *
 * ------------------------------------------------------------------------ */
int LLI_GetVersion_from_file (int image_type, LLIHeaderInfo * VersionInfo, char *image_file);

/* --------------------------------------------------------------------------
 *
 * FUNCTION NAME: LLI_ReplaceImage
 *
 * DESCRIPTION:   Replace component into NAND
 *
 * RETURN:        LLI_OK if OK
 *                LLI_ERROR on any other error
 * NOTES:         none.
 *
 * ------------------------------------------------------------------------ */
int LLI_ReplaceImage(int image, char* image_file);

/* --------------------------------------------------------------------------
 *
 * FUNCTION NAME: LLI_ReadImage
 *
 * DESCRIPTION:   Read component to output file
 *
 * RETURN:        LLI_OK if found
 *                LLI_ERROR on any other error
 * NOTES:         none.
 *
 * ------------------------------------------------------------------------ */
int LLI_ReadImage(int image, char* outputfile);

/* --------------------------------------------------------------------------
 *
 * FUNCTION NAME: LLI_GetVersion
 *
 * DESCRIPTION:   Set File pointers for LLI to log out
 *
 * RETURN:        LLI_OK
 *
 * NOTES:         none.
 *
 * ------------------------------------------------------------------------ */
int LLI_InstallLog(FILE* UseStdOut, FILE* out);

/* --------------------------------------------------------------------------
 *
 * FUNCTION NAME: LLI_MapNAND
 *
 * DESCRIPTION:   Get List of all installed components
 *
 * RETURN:        LLI_OK if found
 *                LLI_ERROR on any other error
 * NOTES:         Map len Must be at least 32 bytes.
 *
 * ------------------------------------------------------------------------ */
int LLI_MapNAND(unsigned char* Map);

/* --------------------------------------------------------------------------
 *
 * FUNCTION NAME: LLI_GetUBootVersion
 *
 * DESCRIPTION:   Get UBoot version
 *
 * RETURN:        LLI_OK if found
 *                LLI_ERROR on any other error
 * NOTES:         Version must be up to size of 20 bytes
 *
 * ------------------------------------------------------------------------ */
int LLI_GetUBootVersion(char *version);


#endif /* _LLI_H_ */
