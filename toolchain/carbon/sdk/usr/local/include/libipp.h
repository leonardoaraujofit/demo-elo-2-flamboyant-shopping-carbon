#ifndef _LIBIPP_H_
#define _LIBIPP_H_

#include <verifone/vfnvm_defs.h>

#ifdef __cplusplus
extern "C" {
#endif

void initPhaseIISecurity();
void ippInitialize(void);
int ippOpenInt(char *pathname, int options);

/***************************************************/
/* K E Y   M A N A G E M E N T   F U N C T I O N S */
/***************************************************/
int  SetNumberOfMsKeysNR(unsigned char ucNewNumOfPinKeys,
                         unsigned char ucNewNumOfMacKeys);

int  DeleteKeyArea (unsigned char ucAreaIndex);

int LoadSystemKey   (   unsigned char ucKeyType, 
                        unsigned char *pKey,
                        unsigned int  isClear   );

int LoadMasterKey  (    unsigned char usLoadingType, 
                        unsigned char ucAreaIndex,
                        unsigned char ucKeyIndex, 
                        unsigned char *pKey         );
int CheckMasterKey (unsigned char ucAreaIndex, unsigned char ucKeyIndex, unsigned char *pKVC);
int vfiSec_isAttacked (void);
int vfiSec_ippClose(int hdl);
int vfiSec_ippRead(int hdl,char *usrbuf, int usrbuflen);
int vfiSec_ippWrite(int hdl,char *usrbuf, int usrbuflen);
int vfiSec_ippControl(int hdl, int type, char *params);
void PinStatus(int *status, int *count, int *lastNonNumericKey);
int vfiSec_ippMKI(int action, int *com2ipp, int *ipp2com);
int GetNVMParameters(S_nvm *nvm);
/**
 *
 *\brief This function reads the installed key count typically used during keyloading
 *\	     bypassmode
 *       The key count is always reset during the call
 *       MS, Dukpt, and ADE keys are totalled
 *
 *\param
 *
 *\return error code:
 *       E_*** : failure in comms, failure within function
 *       S_PASS          : On success\n
 **/
int vfiSec_key_status( void );
#if 0
void vfiSec_setSecurePINDisplayParameters(struct touch_hs_s *hotspot, void *callback);
#endif

int vfiSec_ippCommand(unsigned char *outpkt,
					  int *outpktlen,
					  int outpktsize,
					  unsigned char *inpkt,
					  int inpktlen);

/**
 * \brief  Returns the status of the Navigator module.
 *
 * Navigator PIN entry will be turned on if a current valid FE license for Navigator
 * PIN entry is installed.
 * This API is a Boolean check indicating if the Navigator system is fully functional.
 *
 * \return  0: The Navigator system is not currently usable  !0: Navigator is on.
 **/
int vfiSec_Nav_active(void);

#ifdef __cplusplus
}
#endif

#endif /* _LIBIPP_H_ */
