/*
 *      Copyright, 2011 VeriFone Inc.
 *      2099 Gateway Place, Suite 600
 *      San Jose, CA.  95110
 *
 *  All Rights Reserved. No part of this software may be reproduced,
 *  transmitted, transcribed, stored in a retrieval system, or
 *  translated into any language or computer language, in any form
 *  or by any means electronic, mechanical, magnetic, optical,
 *  chemical, manual or otherwise, without the prior permission of
 *  VeriFone Inc.
 */

/*!
 * \file svcserver_io.h
 * \brief This is the service server IO API header file.
 */

/** @defgroup svcserver_io_api SVCSERVER_IO API
 * The Service Server IO API contains the high level methods that are envoked to interface
 * from a service client to the service server component.
 *  @{
 */
#ifndef _GNU_SOURCE
#define _GNU_SOURCE /* to enable defines for credentials */
#endif
#include <sys/socket.h>
#include <sys/un.h>

#include "svcmgrSvcDef.h"

#ifndef SVCSERVER_IO_H_
#define SVCSERVER_IO_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 * For parameters in server side methods (invoked through svcclient_call_server_function()),
 * this define can be used to avoid seeing compiler warnings for unused variables such as
 * param, paramlen, rtnparam, rtnparamlen
 */
#ifndef UNUSED
#define UNUSED(x)	(void)(x)
#endif

//#define DEBUG  // uncomment to enable debug
//#define SRVR_DEBUG // uncomment to enable server side debug

/*
 * All service server init/exit functions will start with service name then the following is
 * appended to create full server init/exit function name.
 */
#define SERVICE_ONBOOT  "_serverOnBoot" // service_serverOnBoot() called at system startup
#define SERVICE_INIT	"_serverInit" // service_serverInit() called on first server side request if service_serverOnBoot() doesn't exist
#define SERVICE_EXIT	"_serverExit" // service_serverExit() called on server termination signal


/** svcclient_call_server_function - envoked by service client side functions to
 * request a server side function call.
 *
 * Note: rtnData must be free()'d by caller (if non-null)
 * 
 * @param[in] service_name name of service this function belongs to.
 * @param[in] fn_name name of function to be called via server process for this service.
 * @param[in] commandArgs binary data to be passed to function. Format of
 *  commandArgs is unspecified (no string format expected or implied).
 * @param[in] argsSize is number of bytes contained in commandArgs to be sent.
 * @param[out] rtnData is pointer to location to place returned binary data from called
 *  function. Format of rtnData is unspecified (no string format expected or implied).
 * @param[out] rtnSize is amount of returned data placed in rtnData.
 * @return 0 on success, -1 on error.
 *
 * On error return, errno:
 * @li EINVAL - invalid arguments passed
 * @li ENODEV - socket to talk to Service Manager is missing (internal error)
 * @li ENOTCONN - Unable to connect to Service Server Side (able to talk to Server)
 * @li ECONNREFUSED -Connection refused to socket to Service Manager (internal error)
 */
int svcclient_call_server_function(const char * service_name, const char *fn_name,
		const void *commandArgs, const int argsSize, void **rtnData, int *rtnSize);

/** svcserver_client_credentials_get - a client's server can obtain client side credentials
 *
 * @param[in] pCred will be loaded with credential properties from client side.
 *            pCred->pid contains process ID of client making server side call
 *            pCred->uid contains user ID of client making server side call
 *            pCred->gid contains group ID of client making server side call
 *
 * @return 0 on success, -1 on error.
*/
int svcserver_client_credentials_get(struct ucred *pCred);


/** svcmgr_postServerSentEvent - post a Server Sent Event (SSE) using eventSource API to fastcgi server.
 * This may be envoked by an application, a service client side function or a service server side function.
 *
 * @param[in] event is a user defined string (null terminated) used to identify or name this event
 * @param[in] id is a user defined string used as a secondary "id" for this event
 * @param[in] pairlist is an array of name,value pairs containing the event data to be posted
 * @return 0 on success, -1 on error.
 */
int svcmgr_postServerSentEvent(char *event, char *id, struct fixedpairlist pairlist);


#ifdef __cplusplus
}
#endif
#endif /*SVCSERVER_IO_H_*/

/* @} */// end defgroup
