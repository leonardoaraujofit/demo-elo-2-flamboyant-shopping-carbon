/****************************************************************************
* This source code is confidential proprietary information of VeriFone Inc. *
* and is an unpublished work or authorship protected by the copyright laws  *
* of the United States. Unauthorized copying or use of this document or the *
* program contained herein, in original or modified form, is a violation of *
* Federal and State law.                                                    *
*                                                                           *
*                           Verifone                                        *
*                      Rocklin, CA 95765                                    *
*                        Copyright 2003                                     *
****************************************************************************/

/*
;|===========================================================================+
;| FILE NAME:  | ScrLib.h
;|-------------+-------------------------------------------------------------+
;| DESCRIPTION:| Smart Card Uart low level routines
;|-------------+-------------------------------------------------------------+
;| TYPE:       |
;|-------------+-------------------------------------------------------------+
;| COMMENTS:   |
;|===========================================================================+
*/

/*
$Archive:   $
$Author:    Thierry Payet
$Date:      04/08/14
$Modtime:   $
$Revision:  0.1
$Log:       $
*/

#ifndef _SCRLIB_H_
#define _SCRLIB_H_

#include "ScrTypes.h"

#define SCR_COLD_ATR                   0
#define SCR_WARM_ATR                   1
#define SCR_PROT_T0                    0x00
#define SCR_PROT_T1                    0x01
#define SCR_PROT_T14                   0x14
#define SCR_PROT_NONE                  0xFF

#define SCR_SDWN_ON                    1
#define SCR_SDWN_OFF                   0

#define SCR_INTR_NUM                   1

// Max slot number
#define MAX_CARD_NB                    4

// Max ATR Length
#define MAX_ATR_LEN                    32

// Max APDU
#define MAX_APDU_LEN                   264

// thierry_p1, 2015-06-23: extend APDU buffer to support smart card with large data buffers
//#define MAX_TXRX_DATA_LEN			   4096
#define MAX_TXRX_DATA_LEN			   0xFFFF

// Max Retry
#define FIVE_RX_RETRY_MAX              5
#define FIVE_TX_RETRY_MAX              4	// Four retry allowed
#define FOUR_RX_RETRY_MAX              4
#define FOUR_TX_RETRY_MAX              3	// Three retry allowed
#define RX_NO_RETRY                    0
#define TX_NO_RETRY                    0

// Smart Card GPIO number
#define VF6111_GPIO_1                  0x00
#define VF6111_GPIO_2                  0x01
#define VF6111_GPIO_3                  0x02
#define VF6111_GPIO_4                  0x03

// ATR Activation Modes
#define SCR_MODE_ISO                   1
#define SCR_MODE_EMV                   2
#define SCR_MODE_NONISO                4

// ATR Activation Option 0
// bMoreInfo field
#define SCR_MODE_INFO_NONE            	0
#define SCR_MODE_INFO_VCC1V8           	1
#define SCR_MODE_INFO_VCC3V           	2
#define SCR_MODE_INFO_ATR38400         	4
#define SCR_MODE_INFO_ATRVIRTUAL       	8
#define SCR_MODE_INFO_PPSAUTO			0x10
#define SCR_MODE_INFO_OVERWRITE_PPS		0x80

#define SCR_STATE_MACHINE_MASK			0x1F00
#define SCR_STATE_MACHINE_ATR			0x100
#define SCR_STATE_MACHINE_T0			0x200
#define SCR_STATE_MACHINE_T1			0x400
#define SCR_STATE_MACHINE_PPS			0x800
#define SCR_STATE_MACHINE_SYNC			0x1000

// TRANSCEIVE Modes
#define SCR_APDU_CMD					0
#define SCR_IFSD_CMD					1
#define SCR_PPS_CMD						2
#define SCR_APDU_PIN					3
#define SCR_APDU_CIPHER_PIN				4

// Returned Command Status for both fnScrTransceive() and fnScrPowerUp() function
// These status are returned from pICC_Struct->Atr.bCmdStat.bStatus for ATR process and
// from pICC_Struct->Cmd.bCmdStat.bStatus for Transcieve process.
#define SCR_INACTIVE					0
#define SCR_FAILED						1
#define SCR_IN_PROGRESS					2
#define SCR_SUCCESS						3

//  Returned API process status
// These status may be returned by the smart card API functions
// SCR_STATUS list
#define SCR_PROCESS_OK					0
#define SCR_PROCESS_NOK                -1
#define SCR_PROCESS_ONGOING            -2
#define SCR_PROCESS_BAD_PARAMETER      -3
#define SCR_PROCESS_I2CCOM_ERROR       -4
#define SCR_PROCESS_SCR_ABSENT         -5
#define SCR_PROCESS_SCR_INIT_REQUIRED  -6

#define SCR_CALLBACK_PENDING          -10

#define SCR_STATUS                      int

/********************* APIs Errors definition ***********************/
#define SCR_NO_ERROR                   0
#define SCR_NO_ALARM                   0

// generic error definition
#define SCR_VF6111_DET				   0x01
#define SCR_CARD_WITHDRAWAL			   0x02
#define SCR_VCC_PROT                   0x04
#define SCR_EXCHANGE_BUFFER_TOO_SHORT  0x05
#define SCR_PARAMETER_ERROR            0x06
#define SCR_OTP_SHUTDOWN               0x07
#define SCR_SUP_ACTIVATED              0x09
#define SCR_FRAME_ERROR				   0x0A
#define SCR_CLK_SWITCHED			   0x0B
#define SCR_VCC_RAMPUP_FAIL		   	   0x0C
#define SCR_UNKNOWN_ERROR              0x0F

// Error definition for ATR
#define SCR_ATR_MUTE                   0x10
#define SCR_EARLY_ANSWER               0x11
#define SCR_ATR_PARITY_ERROR           0x12
#define SCR_ATR_WWT_EXCEEDED           0x13
#define SCR_ATR_DURATION_EXCEEDED      0x14
#define SCR_TB1_NOT_ZERO               0x15
#define SCR_TB1_NOT_PRESENT            0x16
#define SCR_NO_T0_NO_T1                0x17
#define SCR_B5_OF_TA2_SET              0x18
#define SCR_TB2_PRESENT                0x19
#define SCR_WRONG_WI                   0x1A
#define SCR_PROTOCOL_MISMATCH          0x1B
#define SCR_WRONG_IFSC                 0x1C
#define SCR_WRONG_CWI                  0x1D
#define SCR_WRONG_BWI                  0x1E
#define SCR_WRONG_TC1_CWT              0x1F
#define SCR_TB3_ABSENT                 0x20
#define SCR_WRONG_TC3                  0x21
#define SCR_BAD_FiDi                   0x22
#define SCR_ATR_CHECKSUM_ERROR         0x23
#define SCR_ATR_LEN_EXCEEDED           0x24
#define SCR_TS_NEITHER_3B_OR_3F        0x25
#define SCR_ATR_NOT_SUPPORTED          0x26
#define SCR_WARM_RESET_REQUIRED        0x27

// Error definition for T=0 protocol
#define SCR_TOO_SHORT_APDU             0x30
#define SCR_WRONG_APDU                 0x31
#define SCR_WWT_EXCEEDED               0x32
#define SCR_INS_ERROR                  0x33
#define SCR_T0_PARITY_ERROR            0x34

// Error definition for T=1 protocol
#define SCR_CARD_MUTE_NOW              0x50
#define SCR_RESYNCHRONISED             0x51
#define SCR_CHAIN_ABORT                0x52
#define SCR_BAD_NAD                    0x53
#define SCR_IFSD_NOT_ACCEPTED          0x54
#define SCR_T1_PARITY_OR_LRC_ERROR     0x55
#define SCR_WRONG_RLEN_T1              0x56
#define SCR_IO_CONTENTION              0x57
#define SCR_T1_PROCESS_ERROR		   0x58

// Error definition for PPS negotiation
#define SCR_PARAM_ERROR                0x70
#define SCR_PPS_NOT_ACCEPTED           0x71
#define SCR_RESPONSE_ERROR             0x72
#define SCR_PCK_ERROR                  0x73
#define SCR_PPS_PARITY_ERROR           0x74
#define SCR_PPS_TOUT_ERROR             0x75

/**
* \enum scr_smartcard_clock
* \brief Coding SamartCard Clock Frequency
**/
enum scr_smartcard_clock {
	USIM_SMARTCARD_CLOCK_3_3MHZ		= 0x1,
	USIM_SMARTCARD_CLOCK_4MHZ		= 0x2,
	USIM_SMARTCARD_CLOCK_5MHZ		= 0x3,
	USIM_SMARTCARD_CLOCK_6_6MHZ		= 0x4,
	USIM_SMARTCARD_CLOCK_10MHZ		= 0x5,
	USIM_SMARTCARD_CLOCK_20MHZ		= 0x6,
};

typedef struct {
	unsigned char *txBuf; /*!< Buffer which holds the bytes to be
				   transmitted */
	int txLen; /*!< Number of bytes to transmit from TX buffer. Max length
                   could be 260 bytes */
	int rxExpLen; /*!< Expected number of RX bytes. This is not used in
                       T1 protocol and LEN field in the RX block prolog would
                       be used. This field should be zero
                       only if TX chaining is used otherwise reception would
                       start immediately */
	unsigned char *rxBuf; /*!< Buffer to store received bytes.
				   This buffer size should be atleast
				   rxExpLen */
	int rxLen; /*!< Number of actual bytes received. Max length could
                    be 260 bytes. Also, USIM library does not handle case
                    in T0 where expected length (rxExpLen) is smaller
                    than actual length (rxLen) */
} ScrCmdTxRx;

typedef struct _ScrCntxStruct{
	int cardAsync;
	int txRxDone;
	int txDone; /* transmit completed */
	int txPendingCnt; /*!< number of bytes to be sent
                       after TX_BOCK_REQ int */
	int txBlockLen; /*!< snapshot of TX block len field
                             from TX_BLOCK reg */
    enum scr_smartcard_clock clock;
	ScrCmdTxRx txRx;
} ScrCntxStruct;


typedef struct _ScrCmdStatStruct{
	UCHAR                bStatus;
	UCHAR                bError;
} ScrCmdStatStruct;

typedef struct _ScrATRStruct{
	UCHAR						bSlot;
	ScrCmdStatStruct			bCmdStat;
	UCHAR						bMode;
	UCHAR						bMoreInfo;
	UCHAR						bIfsd;
	UINT						wLength;
	ScrCmdTxRx					aData;
} ScrATRStruct;

typedef struct _ScrInstructStruct{
	UCHAR						bSlot;
	ScrCmdStatStruct			bCmdStat;
	UCHAR						bMode;
	UCHAR						bCmdType;
	UCHAR						bNad;
	UINT						wLength;
	ScrCmdTxRx					aData;
} ScrInstructStruct;

typedef struct _ScrAtrParamStruct{
	UCHAR						bMode;
	UCHAR						bProt;
	UCHAR						bFirstProt;
	UCHAR						bSecondProt;
	UCHAR						bFiDi;
	UCHAR						bCurFiDi;
	UCHAR						bPpsMode;
	UCHAR						bUx;
	UCHAR						bHistoLen;
	UCHAR						bHistoOffset;
	UCHAR						bFlagWarm;
	ScrCmdTxRx					aData;
} ScrAtrParamStruct;

typedef struct _ScrStatusStruct{
	UCHAR						bSlot;
	UCHAR						bCardPres;
	UCHAR						bActive;
	UCHAR						bHwAlarm;
	UCHAR						bHwError;
	UCHAR						bCardType;
	UCHAR						bCardPresL;	
	ScrAtrParamStruct			pAtrParam;
} ScrStatusStruct;

typedef union
{
    ScrATRStruct				Atr;
    ScrInstructStruct			Cmd;
} ICC_DATA_UNION;

typedef union
{
    ULONG FullVersion;
    struct
    {
		unsigned char DateY;
        unsigned char DateM;
        unsigned char VersL;
        unsigned char VersH;
    } Version;
} VERSION_UNION;

typedef struct _ScrVersionStruct{
	VERSION_UNION	lPhyVer;
	VERSION_UNION	lUsimVer;
	VERSION_UNION	lSwVer;
} ScrVersionStruct;

typedef struct
{
	unsigned char   *ScriptCmnd;       
	unsigned short  ScriptLen;	 /* Max script len is 2048 bytes */	
	unsigned char   *DataToCard;   /* Data for scDATA_WRITE_LO/HI commands. May be NULL, if script doesn't include scDATA_WRITE_LO/HI commands. */
	unsigned short  DataToLen;	 /* Number of bits in DataToCard array before after script running. Max write data len is 8192 bits, 1024 bytes */
	unsigned char   *DataFromCard; /* Data result of scDATA_READ_LO/HI commands. May be NULL, if script doesn't include scDATA_READ_LO/HI commands. */
	unsigned short  DataFromLen;   /* out - Number of bits in DataFromCard array after script running. Max read data len is 8192 bits, 1024 bytes*/
	unsigned short  LastScrIndx;   /* out - Index in Script array, where scSTOP_SCRIPT command or error was recognized */
} IccSynScriptStruct;

typedef enum
{
	scSTOP_SCRIPT,		/* this byte has to be the last byte in the script */
	scSTART_LOOP,		/* starts the loop, next one or two bytes is number of iterations*/
	scEND_LOOP,			/* ends the loop */
	scCONTINUE,			/* void command, do nothing */

	scDELAY_5us,		/* make a delay 5 microseconds */
	scDELAY_50us,		/* make a delay 50 microseconds */

	scPOWER_UP,			/* 5v powers the card up in sync mode(use before answer-to-reset */

	scRESET_SET,		/* set RESET to high */
	scRESET_CLR,		/* set RESET to low  */
	scRESET_10us,		/* RESET to high for 10us, then to low for 10us */

	scCLOCK_SET,		/* set CLOCK to high */
	scCLOCK_CLR,		/* set CLOCK to low  */
	scCLOCK_10us,		/* CLOCK to high for 10us, then to low for 10us */

	scDATA_SET,			/* set I/O to high */
	scDATA_CLR,			/* set I/O to low  */
	scDATA_READ_LO,		/* read the I/O from LSB to MSB: *p=((*p)>>1)|0x80 */
	scDATA_READ_HI,		/* read the I/O from MSB to LSB: *p=((*p)<<1)|1    */

	scVPP_IDLE,			/* make VPP idle  (n/a) */
	scVPP_12V,			/* set VPP to 12V (n/a) */
	scVPP_15V,			/* set VPP to 15V (n/a) */
	scVPP_21V,			/* set VPP to 21V (n/a) */

	scC4_SET,			/* set C4 to high */	
	scC4_CLR,			/* set C4 to low  */

	scC8_SET,			/* set C8 to high */	
	scC8_CLR,			/* set C8 to low  */

	scPOWER_DOWN,		/*  the card power turn off */
	scDELAY_us,			/* make a delay in microseconds, next one or two bytes is delay in us, max delay is 0x7fff (~32ms) */

	scDATA_WRITE_LO,	/* set the I/O from LSB to MSB: I/O=*p&0x01; *p=(*p)>>1; */
	scDATA_WRITE_HI,	/* set the I/O from MSB to LSB: I/O=*p&0x80; *p=(*p)<<1; */

	scSET_CLASS,		/* set card activation voltage: SCR_V5, SCR_V3 or SCR_V1_8	*/
	scSET_MODE,			/* set activation type: SYNC_MANUAL_ACT, SYNC_AUTO_TYPE1_ACT or SYNC_AUTO_TYPE2_ACT	*/
	
	scDATA_READ_PROT,	/* read protected bit */
} ScriptCommand;
#endif
/*********************************************************_FILE_HEADER_END_*/
