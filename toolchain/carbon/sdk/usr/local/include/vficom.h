/* $Log:   /h03_u01/pvcs/Combined/PVCS8.0/Engg_Projects/archives/Spectrum/common/vficom/vficom.h-arc  $
 *
 *    Rev 1.0   10 Jun 2005 15:05:56   kevin_s1
 * Initial revision.
 *
 *
 *
 *	Copyright, 2004 VeriFone Inc.
 *	2099 Gateway Place, Suite 600
 *	San Jose, CA.  95110
 *
 *  All Rights Reserved. No part of this software may be reproduced,
 *  transmitted, transcribed, stored in a retrieval system, or
 *  translated into any language or computer language, in any form
 *  or by any means electronic, mechanical, magnetic, optical,
 *  chemical, manual or otherwise, without the prior permission of
 *  VeriFone Inc.
 */

/** @addtogroup */
/** @{ */
/** @addtogroup */
/** @{ */
/** 
 *  @file vficom.h 
 *
 *	@brief VFI COM
 *
 * 
 */
 
#ifndef _VFICOM_H
#define _VFICOM_H

#ifdef __cplusplus
extern "C" {
#endif

#include <linux/tty.h> // This double checks the VFI_GADGET_... #defines below

// These need to be here to get included in SDK even though already in </linux/tty.h>.

/**
 * @name VFI_GADGET checks
*/
/** \{ */
#define VFI_GADGET_DISCONNECT			0x10000000 /**< Connect after disconnect, latched; cleared on read */
#define VFI_GADGET_CONNECTED			0x10000001 /**< Instantaneous connection state, not latched */
#define VFI_GADGET_CONNECT_DISCONNECT	0x10000002 /**< Disconnect after connect, latched; cleared on read */
#define VFI_GADGET_FRAME_NUMBER			0x10000003 /**< For special use if above calls don't solve issues */
#define VFI_GADGET_GS_DISCONNECT		0x10000004 /**< For special use force gs_disconnect on disconnect */
#define VFI_GADGET_DISCONNECT_ERROR		0x10000005 /**< For special use force read error on disconnect */
#define VFI_GADGET_CLEANUP				0x10000006 /**< Clean up after communication problems */
/** \} */


/**
 * @name Communication port possible values
*/
/** \{ */
#define COM1	"/dev/ttyAMA0"  /**< ttyAMA0 */
#define COM2	"/dev/ttyAMA1"  /**< ttyAMA1 */
#define COM3	"/dev/ttyWR0"   /**< ttyWR0 */
#define COM4	"/dev/ttyAMA2"  /**< ttyAMA2 */
#define COM5	"/dev/ttyGS0"   /**< ttyGS0 */
#define COM6	"/dev/ttyAMA3"  /**< ttyAMA3 */
/** \} */


/**
 * @name Com port signal info possible values
*/
/** \{ */
#define COM_DTR		0x80	/**< DTR */
#define COM_RTS		0x40	/**< RTS */
#define COM_CTS		0x20	/**< CTS */
#define COM_RING	0x10	/**< Ring indicator */
#define COM_CD		0x08	/**< Carrier detect */
#define COM_DSR		0x04	/**< DSR */
//Reserved			0x02
//Reserved			0x01
/** \} */

/* --------------------- General Opn_Blk Defines ------------------------- */

/**
 * @name Rate Codes for Opn_Blk.rate
*/
/** \{ */
#define Rt_300   			0   /**< Rt_300 */
#define Rt_600   			1   /**< Rt_600 */
#define Rt_1200  			2   /**< Rt_1200 */
#define Rt_2400  			3   /**< Rt_2400 */
#define Rt_4800  			4   /**< Rt_4800 */
#define Rt_9600  			5   /**< Rt_9600 */
#define Rt_19200 			6   /**< Rt_19200 */
#define Rt_38400 			7   /**< Rt_38400 */
#define Rt_115200 			8   /**< Rt_115200 */
#define Rt_57600 			9   /**< Rt_57600  */
/** \} */


/**
 * @name Format codes for Opn_Blk.format
*/
/** \{ */
#define Fmt_A7E1 			0				/**< async, 7 data bits, even parity, one stop */
#define Fmt_A7N1 			1				/**< async, 7 data bits,   no parity, one stop */
#define Fmt_A7O1 			2				/**< async, 7 data bits,  odd parity, one stop */
#define Fmt_A8E1 			3				/**< async, 8 data bits, even parity, one stop */
#define Fmt_A8N1 			4				/**< async, 8 data bits,   no parity, one stop */
#define Fmt_A8O1 			5				/**< async, 8 data bits,  odd parity, one stop */
#define Fmt_2stp 			0x80			/**< (SIO) Add, if two stop bits desired */
#define Fmt_auto 			0x20			/**< (SIO) Add, if Auto-enables for flow ctl */
#define Fmt_addr 			0x40			/**< (SIO) Add, if Address bit control */
#define Fmt_DTR  			0x10			/**< (SIO) Add, if DTR to be asserted */
#define Fmt_RTS  			0x08			/**< (SIO) Add, if RTS to be asserted */
/** \} */


/**
 * @name Equates for communication protocols
*/
/** \{ */
#define P_char_mode    		0				/**< RS232 character mode */
#define P_packet_mode  		1				/**< RS232 packet mode */
#define P_ECRtailgate  		2				/**< ECR 4683 tailgate mode */
#define P_rs485_mode   		4				/**< supports multi drop config */
#define P_ECRtailgate_ctlr	8				/**< ECR 4683 tailgate controller mode */
/** \} */


/**
 * @name COM2 Handshake Monitor
*/
/** \{ */
#define	MON_DCD			1 /**< DCD */
#define MON_CTS			2 /**< CTS */
/** \} */

/**
 * Define vficom error counts
 */
struct vficomErrCounts
{
	int frame_err;
	int over_err;
	int parity_err;
};

/**
 * Define packet parameter info
 */
typedef struct
{
	unsigned char stx_char;
	unsigned char etx_char;
	unsigned char count;
	int max_pkt;
	void (*pPktHandler) (char *, int);
} packet_parm_t;

/**
 * Define ECR tailgate parameters
 */
typedef struct
{
	unsigned char poll_addr;
} ECRtailgate_parm_t;

/**
 * Define trailer
 */
struct Opn_Blk
 {
	unsigned int rate;
	unsigned int format;
	unsigned int protocol;
	unsigned int parameter;
	packet_parm_t packet_parms;
	union {
		/* Mutually exclusive protocols go here */
		ECRtailgate_parm_t ECRtailgate_parms;
	} trailer;
};

/**
 * Allows an application to perform a local DL/Zontalk/VeriTalk download. The application must open and configure the port before calling this function.
 *
 * @param[in] com_port Communication port where the download is received.
 * @param[in] *dspCallback Callback function called when an information/status message is received from the Zontalk/DL server.
 * @param[in] type P = Partial download
 * @param[in] *endCallback Callback function called when the download completes successfully or failed
 *
 * @return 
 * @li 0 = Success
 * @li < 0 = Download failed
 *
 */
short svcZontalkRcv(short com_port, void (*dspCallback) (char *), unsigned char type,void (*endCallback) (int));

/**
 * Allows an application to perform a local DL/Zontalk/VeriTalk download. The application must open and configure the port before calling this function.
 * 
 * @param[in] com_port Communication port where the download is received.
 * @param[in] *dspCallback Callback function called when an information/status message is received from the Zontalk/DL server.
 * @param[in] type P = Partial download
 * @param[in] *endCallback Callback function called when the download completes successfully or failed
 * @param[in] *ob Pointer to an Open_Blk structure
 *
 * @return
 * @li 0 = Success
 * @li < 0 = Download failed
 *
 */
short svcZontalkRcvExtended(short com_port, void (*dspCallback) (char *), unsigned char type,void (*endCallback) (int), struct Opn_Blk *ob);

/**
 * Cancel a Zontalk/DL download (initiated by svcZontalkRcv)
 *
 * @return
 * @li 0 = Success
 * @li < 0 = Error occurred or a download is not in progress
 */
int zontalkCancel(void);

/**
 * Accept a pointer to an Open_Blk structure populated by the application with the desired port settings. Takes these settings, map them to Linux serial port settings, and set the port to these values.
 *
 * @param[in] fd File descriptor returned when opening a serial port using either open or ecrOpen
 * @param[in] *pOpenBlock Pointer to the open block structure populated with the desired settings
 *
 * @return
 * @li 0 = Success
 * @li < 0 = See errno
 *
 */
short svcSetOpenBlock(int fd, struct Opn_Blk *pOpenBlock);

/**
 * Get port signal value and error count
 *
 * @param[in] inFd File descriptor associated with the open port device
 * @param[in] inSigBuf buffer that returns signal value interpretation
 * @param[in] *inErrCounts Pointer to vficomErrCounts struct
 *
 * @return
 *
 *
 */
void svcGetPortSignalInfo(int inFd, unsigned int *inSigBuf, struct vficomErrCounts *inErrCounts);

/**
 * Returns information about port configuration.
 *
 * @param[in] inFd File descriptor associated with the open port device
 * @param[in] buf 4 integer buffer that returns port signal status information
 *
 * @return
 * @li buf[0] Number of received bytes pending the input queue
 * @li buf[1] Number of available (free) bytes in the output queue
 * @li buf[2] Signal information
 * @li buf[3] Reserved
 *
 */
struct vficomErrCounts *svcGetPortStatus(int inFd, unsigned int *buf);

/**
 * Returns the number of bytes pending in the port’s input queue
 *
 * @param[in] inFd File descriptor associated with the open port device
 *
 * @return Number of bytes pending in the port’s input queue
 *
 */
int svcGetInQ(int inFd);

/**
 * Returns the number of bytes pending in the port’s output queue
 *
 * @param[in] inFd File descriptor associated with the open port device
 *
 * @return Number of bytes pending in the port’s output queue
 *
 */
int svcGetOutQ(int inFd);


/* COM Port 3 Service Functions */
/**
 * Allow the application to manually set the IO handshake for the COM3 port
 *
 * @param[in] fd File descriptor returned when opening a serial port with either <i>open</i> or <i>ecrOpen</i>
 * @param[in] sioHS Value indicating the handshake mode to set the port to
 *
 * @NOTE: typically used when in tailgate mode
 *
 * @return
 * @li 0 = Success
 * @li < 0 = See erro
 *
 */
short svcCom3SetIOHandShake(int fd, char sioHS);
/**
 * Allow the application to manually set the mode for the COM3 port
 *
 * @param[in] fd File descriptor returned when opening a serial port with either <i>open</i> or <i>ecrOpen</i>
 * @param[in] ioctlMode Value indicating the mode to set the port to
 *
 * @return
 * @li 0 = Success
 * @li < 0 = See erro
 *
 */
short svcCom3SetMode(int fd, char ioctlMode);

/**
 * Return the value of the extended status report to the buffer pointed to in esBuf
 *
 * @param[in] fd File descriptor returned when opening a serial port with either <i>open</i> or <i>ecrOpen</i>
 * @param[in] *esBuf Pointer to buffer
 *
 * @return
 * @li 0 = Success
 * @li -1 = Wrenchman IOCTLC3SCM ERROR
 * @li < 0 = See errno
 *
 */
short svcCom3ReqExtStatus(int fd, char *esBuf);

/**
 * Request the Tally Record report comprised of a listing of counters that track ECR events
 *
 * @param[in] fd File descriptor returned when opening a serial port with either <i>open</i> or <i>ecrOpen</i>
 * @param[in] *tiBuf Pointer to a buffer with the Tally Record
 *
 * @return
 * @li 0 = Success
 * @li < 0 = See errno
 *
 */
short svcCom3ReqTallyInfo(int fd, char *tiBuf);

/**
 * Reset all tally counters to zero
 *
 * @param[in] fd File descriptor returned when opening a serial port with either <i>open</i> or <i>ecrOpen</i>
 *
 * @return
 * @li 0 = Success
 * @li < 0 = See errno
 *
 */
short svcCom3ResTallyData(int fd);

/**
 * Get the current firmware version within the communication processor
 *
 * @param[in] fd File descriptor returned when opening a serial port with either <i>open</i> or <i>ecrOpen</i>
 * @param[in] *fwBuf Pointer to a minimum 16-byte buffer that contains the null-terminated communication processor firmware version string
 *
 * @return
 * @li 0 = Success
 * @li < 0 = See errno
 *
 */
short svcCom3ReqFirmVers(int fd, char *fwBuf);

/**
 * Set the handshake byte to the specified value
 *
 * @param[in] fd File descriptor returned when opening a serial port with either <i>open</i> or <i>ecrOpen</i>
 * @param[in] hs A 1-byte value that indicates the handshake control
 *
 * @return
 * @li 0 = Success
 * @li < 0 = See errno
 *
 */
short svcCom3SetHandshake(int fd, char hs);

/**
 * Manually flush the receive buffer internal to the communication processor
 *
 * @param[in] fd File descriptor returned when opening a serial port with either <i>open</i> or <i>ecrOpen</i>
 *
 * @return
 * @li 0 = Success
 * @li < 0 = See errno
 *
 */
short svcCom3FlushRxBuf(int fd);

/**
 * Set the Receive Record Threshold (RRT) used to set the minimum size of receive records returned to the host from the communication processor
 *
 * @param[in] fd File descriptor returned when opening a serial port with either <i>open</i> or <i>ecrOpen</i>
 * @param[in] rrt A 1-byte value that indicates the RRT
 *
 * @return
 * @li 0 = Success
 * @li < 0 = See errno
 *
 */
short svcCom3SetRxRecThresh(int fd, char rrt);

/**
 * Set the Buffer Flush Interval (BFI) to indicate when a record less in size than the RRT is to be released to the host
 *
 * @param[in] fd File descriptor returned when opening a serial port with either <i>open</i> or <i>ecrOpen</i>
 * @param[in] bfi A 1-byte value that indicates the BFI to set
 *
 * @return
 * @li 0 = Success
 * @li < 0 = See errno
 *
 */
short svcCom3SetBufFlushInt(int fd, char bfi);

/**
 * Add status change events to the <i>ecrTailgateCommands</i> queue and delivered to an application through a callback
 *
 * @param[in] fd File descriptor returned when opening a serial port with either <i>open</i> or <i>ecrOpen</i>
 * @param[in] A 1-byte bit mask of the status change events to monitor
 *
 * @return
 * @li 0 = Success
 * @li < 0 = See errno
 *
 */
short svcCom3SetXStatMask(int fd, char mask);

/**
 * Perform ioctl() to Com3 open port
 *
 * @param[in] fd File descriptor returned when opening a serial port with either <i>open</i> or <i>ecrOpen</i>
 * @param[in] cmd command to be sent
 * @param[in] ioctlArg argument to be sent along with command
 *
 * @return
 * @li 0 = Success
 * @li < 0 = See errno
 *
 */
short svcCom3Ioctl(int fd, long cmd, char ioctlArg);

#ifdef WRENCH_API
short svcCom3WrenchIoctl(int fd, struct wrenchIoctl *pIoctl);
#endif


/* version is returned in malloc'd space. If non-NULL, must free() when done */
char *svcCom3FWversion(void);

short svcCom3FWLoad(char *pintelHexLoadFile, void (*com3FWdspCallback) (char *));


#ifdef __cplusplus
}
#endif
#endif
