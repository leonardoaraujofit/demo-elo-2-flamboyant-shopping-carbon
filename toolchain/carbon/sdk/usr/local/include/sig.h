
/**************************************************************************
 * FILE NAME:   sig.h                                                  *
 * MODULE NAME: sig.h                                                  *
 * PROGRAMMER:  Don Ward                                                  *
 * DESCRIPTION: Signature Capture API header                                  *
 
 **************************************************************************/
#ifndef __SIG_H__
#define __SIG_H__

#ifdef RAPTOR
#include <linux/sigtiff.h>
#include "cap_touch.h"
#include "K2.h"
#else
#include <linux/sigtiff.h>
#include "cap_touch.h"
#include "K2.h"
#include "K2lib.h"
#endif

#endif /*#ifndef __SIG_H__*/
