/***************************************************************************/
/*      Copyright 2000-2003 by VeriFone, Inc. All Rights Reserved.         */
/*                                                                         */
/*  Accessing, using, copying, modifying or distributing this source code  */
/*  is expressly not permitted without the prior written authorization of  */
/*  VeriFone, Inc.  Engaging in any such activities without authorization  */
/*  is an infringement of VeriFone's copyrights and a misappropriation of  */
/*  VeriFone's trade secrets in this source code,  which will subject the  */
/*  perpetrator to certain civil damages and criminal penalties.           */
/***************************************************************************/

/*
;|===========================================================================+
;| FILE NAME:  | vsscript.h
;|-------------+-------------------------------------------------------------+
;| DESCRIPTION:| Header file for vsscript.c
;|-------------+-------------------------------------------------------------+
;| VERSION     | $Modtime:   17 Apr 2008 15:41:44  $
;|-------------+-------------------------------------------------------------+
;| COMMENTS:   |
;|             |
;|===========================================================================+
*/
#ifndef _LIBVSS_H_
#define _LIBVSS_H_

/*
;|===========================================================================+
;| NOTE: | APPLICATION INCLUDE
;|===========================================================================+
*/
//#include "VSS_def.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
;|===========================================================================+
;| NOTE: | DEFINITION
;|===========================================================================+
*/
#define NO              0
#define YES             1

#define VSSPRESENT      0x80

#define PP1000_SUPPORT  0x01
#define MS_SUPPORT      0x02
#define DUKPT_SUPPORT   0x04

/**
 * DEFINITIONS FOR ATOS POSEIDON ZVT
 */

/** Key blocks information for Key Loading */
#define POS_KEYLOAD_BLOCKS_TOTAL			0xFF
#define POS_KEYLOAD_BLOCKS_LOADABLE			0xFF
#define POS_KEYLOAD_KEY_INDEX				0x00

/** Key blocks information for Poseidon Key usage */
#define POSEIDON_BLOCKS_TOTAL				0x20
#define POSEIDON_BLOCKS_LOADABLE			0x0C
#define POSEIDON_KEY_INDEX					0x02

/** KeyUsage Poseidon operations */
#define POSEIDON_INIT1						0x01
#define POSEIDON_INIT2						0x02
#define POSEIDON_SET_DYN					0x03
#define POSEIDON_GET_STATE					0x04
#define POSEIDON_RESET						0x05
#define POSEIDON_GENERATE_SESSSION_KEYS		0x06
#define POSEIDON_GENERATE_MAC				0x07
#define POSEIDON_GENERATE_PAC				0x08
#define POSEIDON_ENCRYPT_BMP				0x09
#define POSEIDON_DECRYPT_BMP				0x0A
#define POSEIDON_RESET_Y					0x0B
#define POSEIDON_ENCRYPT_E2E				0x0C

/** KeyLoad Poseidon operations */
#define POSEIDON_DERIVE_CMAC_SUBKEYS		0x10
#define POSEIDON_DECRYPT_KEY_BLOCK			0x11
#define POSEIDON_CALCULATE_CMAC_3DES		0x12
#define POSEIDON_CALCULATE_CMAC_1DES		0x13
#define POSEIDON_TRANSCODE_KEY_BLOCK		0x14
#define POSEIDON_BUILD_KCV					0x15
#define POSEIDON_GET_KEY_LOAD_STATUS		0x16


/* Script install location changed from /etc/vss/usr1 to /tmp/etc/vss/usr1
	(Trident to Raptor), need to supplement with 4 bytes. */
#define VSS_FILENAME_LEN	(2+1+32+1+4)

/* CM: Originated from mDukpt.h  We use only this define from that file. */
#define SIZE_OF_DUKPT_KEY_SET  22

/*
;|===========================================================================+
;| NOTE: | TYPEDEF
;|===========================================================================+
*/
typedef struct {
        unsigned char   presence;
        unsigned char   gid;
        unsigned char   restriction;
		char            filename[VSS_FILENAME_LEN];
        unsigned char   name[8];
        int             crc;
} TScriptTable;

/*
;|===========================================================================+
;| NOTE: | FUNCTION PROTOTYPES
;|===========================================================================+
*/
int IsScriptValid(unsigned char ucScriptId, TScriptTable *ptr);
int LoadSecurityScript(char *pucINName);
int GetScriptInfo(unsigned char ucScriptId, unsigned short* pusDataLen, unsigned char *pData);

int RemoveSecurityScript(char *pucINName);

int RemoveSecurityScriptsByUserID(unsigned char userID);

int ExecuteMacroCommand(unsigned char ucScriptNumber,
                        unsigned char ucMacroID,
                        unsigned short usINDataSize,
                        unsigned char* pucINData,
                        unsigned short usMaximumOUTDataSize,
                        unsigned short* pusOUTDataSize,
                        unsigned char* pucOUTData);

void GetVSSVersion(char* buffer);

/*******************************************************/
/* Internal function definitions for ATOS Posedion ZVT */
/*******************************************************/
/**
 * Initialise system for loading Poseidon keyloading
 * and usage keys
 * @return 0 on success, non-zero on failure
 */
int InitPoseidonKey(unsigned char nbBlocksTotal,
					unsigned char nbBlocksLoadable,
					unsigned char ucKeyIndex);
/**
 * Execute supported Posedion operations
 * @return 0 on success, non-zero on failure
 */
int PoseidonExec(unsigned char ucOperation,
				 unsigned short usINDataSize,
				 unsigned char *pucINData,
				 unsigned short usMaximumOUTDataSize,
				 unsigned short *pusOUTDataSize,
				 unsigned char *pucOUTData);
/**
 * Delete Poseidon keyloading and usage keys
 * @return 0 on success, non-zero on failure
 */
int DeletePoseidonKeys(unsigned char ucKeyIndex);


#ifdef __cplusplus
}
#endif

#endif /* _LIBVSS_H_ */
