
#ifndef __SYSM_PINPAD__
#define __SYSM_PINPAD__

#include <sysmode_ui/sysm_ui.h>
#include <sysmode_ui/sysm_lang.h>

int pp_getDeviceList();
void pp_perso_cb(const SYSM_UI::t_ui_menu_entry *e);
void sec_pplink_cb(const SYSM_UI::t_ui_menu_entry *e);

#endif
