#ifndef _VFI_BLUETOOTH_H_
#define _VFI_BLUETOOTH_H_

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @name Function pointer typedefs
 */
/** \{ */
typedef int (*vfibt_requestPasskeyNumber_t)(unsigned int *passkeyNumber); /**< Password key request callback */
typedef int (*vfibt_requestPincodeString_t)(char *pinCodeString);         /**< Pincode request callback */
typedef int (*vfibt_requestConfirmation_t)(unsigned int confirmationKey); /**< Confirmation request callback */
/** \} */

/**
 * Define bluetooth request callbacks
 */
struct vfibt_requests_t
{
	vfibt_requestPasskeyNumber_t requestPasskey;
	vfibt_requestPincodeString_t requestPincode;
	vfibt_requestConfirmation_t requestConfirmation;
};

/**
 * Register bluetooth request callbacks
 *
 * @param[in] requests pointer to a structure with defined request callbacks
 *
 * @return
 * @li 0 Success
 * @li -1  Error
 */
int vfibt_requestsRegister(struct vfibt_requests_t *requests);

/**
 * Unregister bluetooth request callbacks registred by vfibt_requestsRegister()
 *
 * @return
 * @li 0 Success
 * @li -1  Error
 */
int vfibt_requestsUnregister(void);


#ifdef __cplusplus
}
#endif

#endif /* _VFI_BLUETOOTH_H_ */
