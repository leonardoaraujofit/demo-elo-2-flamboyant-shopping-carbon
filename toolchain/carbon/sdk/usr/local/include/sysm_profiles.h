#ifndef __SYSM_PROFILES__
#define __SYSM_PROFILES__

#include <sysmode_ui/sysm_ui.h>
#include <sysmode_ui/sysm_lang.h>

extern const char * PROFILE_SQA;
extern const char * PROFILE_ADC;
extern const char * PROFILE_DEV;
extern const char * PROFILE_PROD;

void profile_cb(const SYSM_UI::t_ui_menu_entry *);

#endif // __SYSM_PROFILES__
