/** @addtogroup */
/** @{ */
/** @addtogroup */
/** @{ */
/** 
 *  @file libvoy.h 
 *
 *	@brief Voyager library smart card interface
 *
 * 
 */
 
/*****************************************************************************/
/* This source code is confidential proprietary information of VeriFone Inc. */
/* and is an unpublished work or authorship protected by the copyright laws  */
/* of the United States. Unauthorized copying or use of this document or the */
/* program contained herein, in original or modified form, is a violation of */
/* Federal and State law.                                                    */
/*                                                                           */
/* LIBVOY.H - Voyager library smart card interface                           */
/*                                                                           */
/*                             Verifone Inc.                                 */
/*                     1400 West Stanford Ranch Road                         */
/*                           Rocklin, CA 95765                               */
/*                        Copyright 1999- 2009                               */
/*****************************************************************************/

#ifndef __LIBVOY__
#define __LIBVOY__

#ifdef __cplusplus
extern "C" {
#endif

/*=======================================================================*/
/*                              TYPEDEF                                  */
/*=======================================================================*/
#ifndef BYTE_COUNT  // ACT 2000 also defines BYTE
#ifndef __BYTE__
#define __BYTE__
typedef unsigned char BYTE;
#endif
#endif

#ifndef __WORD__
#define __WORD__
typedef unsigned short  WORD;
#endif
#ifndef __DWORD__
#define __DWORD__
typedef unsigned long DWORD;
#endif
#ifndef __RESPONSECODE__
#define __RESPONSECODE__
typedef DWORD         RESPONSECODE;
#endif

/**
 * @name Sync descriptor defines
 */
/** \{ */
#define SYNC_DESCRIPTOR_SIZE 16     /**< ASCII string, 10 byte descriptor + 4 byte version X.xx */
#define MAX_SYNC_DRVRS  15          /**< Maximum number of sync drivers loaded at one time */
/** \} */


/*LIBVOY_STRUCT*/
/**
 * Sync driver descriptor list structure
 */
struct S_SYNC_LIST                      // Data format returned by IFD_Get_Capabilities(Tag_Sync_List)
{
   int numInstalled;
   struct
   {
      int cardType;
      unsigned char desc[SYNC_DESCRIPTOR_SIZE];

   }descList[MAX_SYNC_DRVRS];
};

/**
 * @name Voyager defines
 */
/** \{ */
#define GemCore_Module                  (DWORD)0  /**< For legacy support */
#define Predator_Module                 (DWORD)0  /**< VerixV platforms with NXP TDA8007 IC */
#define Saturn_Module                   (DWORD)1
#define VF2101_Module                   (DWORD)2  /**< VF2101 Verix platfroms */

#define STANDARD_EMV311                 (DWORD)0x0000
#define STANDARD_EMV                    STANDARD_EMV311         
#define ISO_7816                        (DWORD)0x0001
#define VISACASH                        (DWORD)0x0002
#define NO_STANDARD                     (DWORD)0x0003
#define NOT_ISO_38400                   (DWORD)0x0004

#define ICC_CNT                         6

#define VOYAGER_OPEN                    1
#define VOYAGER_CLOSE                   0
#define VOYAGER_SELECTED                2

#define CLASS_A                         (DWORD)0x0001
#define CLASS_B                         (DWORD)0x0002
#define CLASS_AB                        (DWORD)0x0003
#define VOLT1_8                         (DWORD)0x0004

#define MANUAL_PTS                      (DWORD)0x0001
#define AUTO_PTS                        (DWORD)0x0002
#define NO_PTS                          (DWORD)0x0003

#define IFD_POWER_UP                    (WORD)1  /**< Cold_ATR()  */
#define IFD_POWER_DOWN                  (WORD)2  /**< Power_Off() */
#define IFD_RESET                       (WORD)3  /**< Warm_ATR()  */

#define BYTE_ABSENT                     0xFFFF
#define PROTOC_T0                       1
#define PROTOC_T1                       2

#define NEGOTIATE_PTS1                  0x01
#define NEGOTIATE_PTS2                  0x02
#define NEGOTIATE_PTS3                  0x04

#define UNKNOWN                         (BYTE)0
#define ASYNC_7816                      (BYTE)1
#define SYNC_7816                       (BYTE)2

#define ASYNC                           (DWORD)0x00

#define VENDOR_NAME_SIZE                32
#define VENDOR_IFDTYPE_SIZE             32
#define VENDOR_SERIAL_NO_SIZE           32
#define ATR_SIZE                        33 /**< Should be 32 as per PC/SC as per ISO 33 */
/** \} */


/**
 * @name Card slots, for use in this file ONLY
 */
/** \{ */
#define UNSUPPORTEDSLOT                 (DWORD)0
#define CARDSLOT1                       (DWORD)1
#define CARDSLOT2                       (DWORD)2
#define CARDSLOT3                       (DWORD)3
#define CARDSLOT4                       (DWORD)4
#define CARDSLOT5                       (DWORD)5
#define CARDSLOT6                       (DWORD)6
/** \} */


/**
 * @name Card slots for remapping, to remap name to slot number change these values so that the name matches the slot
 */
/** \{ */
#define CUSTOMER_CARD                   CARDSLOT1
#define MERCHANT_FULL_SIZE_CARD         UNSUPPORTEDSLOT
#define MERCHANT_SLOT_1                 CARDSLOT2
#define MERCHANT_SLOT_2                 CARDSLOT3
#define MERCHANT_SLOT_3                 CARDSLOT4
#define MERCHANT_SLOT_4                 CARDSLOT5
/** \} */


/**
 * @name Tags
 */
/** \{ */
#define Tag_Logical_Handle              (DWORD)0x020C
#define Tag_Vendor_Name                 (DWORD)0x0100
#define Tag_Vendor_IFD_Type             (DWORD)0x0101
#define Tag_Vendor_IFD_Version          (DWORD)0x0102
#define Tag_IFD_Serial_Number           (DWORD)0x0103
#define Tag_Channel_ID                  (DWORD)0x0110
#define Tag_Asynch_protocol_types       (DWORD)0x0120
#define Tag_Default_CLK                 (DWORD)0x0121
#define Tag_Max_CLK                     (DWORD)0x0122
#define Tag_Default_Data_Rate           (DWORD)0x0123
#define Tag_Max_Data_Rate               (DWORD)0x0124
#define Tag_Max_IFSD                    (DWORD)0x0125
#define Tag_Synch_protocol_types        (DWORD)0x0126
#define Tag_Power_Management            (DWORD)0x0131
#define Tag_UserToCard_Auth_Device      (DWORD)0x0140
#define Tag_User_Auth_Device            (DWORD)0x0142
#define Tag_Mechanical_Characteristics  (DWORD)0x0150
#define Tag_Open_SCard_Reader           (DWORD)0x0180
#define Tag_Close_SCard_Reader          (DWORD)0x0181
#define Tag_CLK_Stop_Indicator          (DWORD)0x0182 /**< From Mx Voyager */
#define Tag_Current_IFM_Version         (DWORD)0x0183
#define Tag_Override_Card_ATR           (DWORD)0x0184
#define Tag_Reader_IFSD                 (DWORD)0x0185
#define Tag_Open_ICC                    (DWORD)0x0188
#define Tag_Close_ICC                   (DWORD)0x0189
#define Tag_Select_ICC                  (DWORD)0x0190
#define Tag_Nad_Management              (DWORD)0x0191
#define Tag_Convention                  (DWORD)0x0192
#define Tag_WTX_Management              (DWORD)0x0193
#define Tag_Class_Management            (DWORD)0x0194
#define Tag_Pts_Management              (DWORD)0x0195
#define Tag_Error_Management            (DWORD)0x0196
#define Tag_Standard                    (DWORD)0x0197
#define Tag_Card_Type                   (DWORD)0x01A0
#define Tag_ICC_Structure               (DWORD)0x01B0
#define Tag_ICC_Structure_Size          (DWORD)0x01B2
#define Tag_PSCR_Insertion_Count        (DWORD)0x01D5
#define Tag_Sync_List                   (DWORD)0x01E0
#define Tag_Delta2_Speeds	        (DWORD)0x01EA /**< From Mx Voyager */

#define Tag_ICC_Presence                (DWORD)0x0300
#define Tag_ICC_Interface_Status        (DWORD)0x0301
#define Tag_ATR_String                  (DWORD)0x0303
#define Tag_ICC_Type                    (DWORD)0x0304
#define Tag_ATR_Length                  (DWORD)0x0305

#define Tag_Current_Protocol_Type       (DWORD)0x0201
#define Tag_Current_CLK                 (DWORD)0x0202
#define Tag_Current_F                   (DWORD)0x0203
#define Tag_Current_D                   (DWORD)0x0204
#define Tag_Current_N                   (DWORD)0x0205
#define Tag_Current_W                   (DWORD)0x0206
#define Tag_Current_IFSC                (DWORD)0x0207
#define Tag_Current_IFSD                (DWORD)0x0208
#define Tag_Current_BWT                 (DWORD)0x0209
#define Tag_Current_CWT                 (DWORD)0x020A
#define Tag_Current_EBC                 (DWORD)0x020B
/** \} */


/**
 * @name IFD results
 */
/** \{ */
#define IFD_Success                     0
#define IFD_Failure                     1
#define IFD_Error_Tag                   2  /**< Tag does not exist */
#define IFD_Error_Not_Supported         3  /**< Tag not supported */
/** \} */


/**
 * @name Set and Get Capabilities
 */
/** \{ */
#define IFD_Error_Set_Failure           5  /**< Operation failed */
#define IFD_Error_Value_Read_Only       6  /**< Value cannot be modified */
/** \} */


/**
 * @name PTS management
 */
/** \{ */
#define IFD_Error_PTS_Failure           7  /**< PTS Operation failed */
#define IFD_Protocol_Not_Supported      8  /**< protocol not supported */
/** \} */



/**
 * @name Power management
 */
/** \{ */
#define IFD_Error_Power_Action          9  /**< Action not carried out  */
/** \} */


/**
 * @name Swallow management
 */
/** \{ */
#define IFD_Error_Swallow              10 /**< Card not swallowed */
#define IFD_Error_Eject                11 /**< Card not ejected */
#define IFD_Error_Confiscate           12 /**< Card not confiscated */
/** \} */


/**
 * @name Transmit management
 */
/** \{ */
#define IFD_Communication_Error        13 /**< Request not send */
#define IFD_response_TimeOut           14 /**< IFD time-out waiting the ICC */
#define IFD_Error_BadFormat            15 /**< input message bad format */
#define IFD_Error_BadParam             16 /**< invalid function parameter */
/** \} */


/**
 * @name Voyager cmd timeout
 */
/** \{ */
#define Voy_Cmd_Timeout                (30*1000) /**< 30 sec default timeout used by TransmitReceive */
/** \} */


/**
 * Signal the insertion of an ICC
 * 
 * @return
 * @li IFD_Success = ICC is present
 * @li IFD_Failure = ICC is not present
 * 
 */
extern RESPONSECODE IFD_Is_ICC_Present (void);

/**
 * Signal the removal of an ICC
 * 
 * @return
 * @li IFD_Success = ICC is not present
 * @li IFD_Failure = ICC is present
 *
 */
extern RESPONSECODE IFD_Is_ICC_Absent (void);

/**
 * Get the parameters specified by Tag
 * 
 * @param[in] Tag Value of the Tag
 * @param[in] Value Value of the parameter to set
 * 
 * @return
 * @li IFD_Success = Parameter was succsesfully set
 * @li IFD_Error_Tag = Tag does not exist
 * @li IFD_Error_Not_Supported = Tag not supported in this system
 * 
 */
extern RESPONSECODE IFD_Get_Capabilities (DWORD Tag, BYTE *Value);

/**
 * Set the parameters specified by Tag
 * 
 * @param[in] Tag Value of the Tag
 * @param[in] Value Value of the parameter to set
 * 
 * @return
 * @li IFD_Success = Parameter was cuccsesfully set
 * @li IFD_Error_Set_Failure = Operation failed
 * @li IFD_Error_Tag = Tag does not exist
 * @li IFD_Error_Value_Read_Only = Value cannot be modified
 * @li IFD_Error_Not_Supported = Tag not supported in this system
 * 
 */
extern RESPONSECODE IFD_Set_Capabilities (DWORD Tag, BYTE *Value);

/**
 * Power ICC
 * 
 * @param[in] ActionRequested Action to complete
 * 
 * @return
 * @li IFD_Success = Operation succeeded
 * @li IFD_Failure = Operation failed
 * @li IFD_Communication_Error = Communication error
 *
 */
extern RESPONSECODE IFD_Power_ICC (WORD ActionRequested);

/**
 * Transmit data to ICC
 * 
 * @param[in] CommandData Value of the command data
 * @param[in] ResponseData Pointer to where the response will be written
 * 
 * @return
 * @li IFD_Success = Operation succeeded
 * @li IFD_Failure = Operation failed
 * @li IFD_Communication_Error = Communication error
 * 
 */
extern RESPONSECODE IFD_Transmit_to_ICC (BYTE *CommandData,  BYTE *ResponseData);

/**
 * Transmit transparent
 * 
 * @param[in] CommandData Value of the command data
 * @param[in] ResponseData Pointer to where the response will be written
 * 
 * @return
 * @li IFD_Success = Operation succeeded
 * @li IFD_Failure = Operation failed
 * @li IFD_Communication_Error = Communication error
 *
 */
extern RESPONSECODE IFD_Transmit_Transparent( BYTE *CommandData, BYTE *ResponseData );

/**
 * Set protocol parameters
 * 
 * @param[in] ProtocolType Protocol type
 * @param[in] SelectionFlags Selection flags
 * @param[in] PTS1 PTS1 flag
 * @param[in] PTS2 PTS2 flag
 * @param[in] PTS3 PTS3 flag
 * 
 * @return
 * @li IFD_Success = Operation succeeded
 * @li IFD_Failure = Operation failed
 * @li IFD_Communication_Error = Communication error
 *
 */
extern RESPONSECODE IFD_Set_Protocol_Parameters (DWORD ProtocolType,
                                                 BYTE  SelectionFlags,
                                                 BYTE  PTS1,
                                                 BYTE  PTS2,
                                                 BYTE  PTS3);

/**
 * Confiscate ICC
 * 
 * @return
 * @li IFD_Success = Operation succeeded
 * @li IFD_Failure = Wrong input parameters
 * @li IFD_Communication_Error = Communication error
 * 
 */
extern RESPONSECODE IFD_Confiscate_ICC (void);

/**
 * Eject ICC
 * 
 * @return
 * @li IFD_Success = Operation succeeded
 * @li IFD_Failure = Wrong input parameters
 * @li IFD_Communication_Error = Communication error
 * 
 */
extern RESPONSECODE IFD_Eject_ICC (void);

/**
 * Swallow ICC
 * 
 * @return
 * @li IFD_Success = Operation succeeded
 * @li IFD_Failure = Wrong input parameters
 * @li IFD_Communication_Error = Communication error
 * 
 */
extern RESPONSECODE IFD_Swallow_ICC (void);

/**
 * Clear the text PIN
 * 
 * @param[in] RespVerifyPIN Pointer to where the verification response will be written
 * 
 * @return
 * @li IFD_Success = Operation succeeded
 * @li IFD_Failure = Wrong input parameters
 * @li IFD_Communication_Error = Communication error
 * 
 */
extern RESPONSECODE IFD_PresentClearTextPIN(BYTE *RespVerifyPIN);

/**
 * return ICC status
 * 
 * @param[in] Status byte
 * 			bit zero = Card Presence (all slots)
 * 			bit one  = Card Activation State (all slots)
 * 			bit two  = User Card Presence Latched (ie. usercard card either inserted or withdrawn)
 * @return
 * @li IFD_Success = Operation succeeded
 * @li IFD_Failure = Wrong input parameters
 * 
 */
extern RESPONSECODE IFD_Get_ICC_Status( BYTE *);

/**
 * Encrypt the EMV PIN
 * 
 * @param[in] INICCRandom 8 byte random number from ICC
 * @param[in] PubKeyData Public key data
 * @param[in] PubKeyLen Public key length
 * @param[in] Exponent Exponent
 * @param[in] ResponseData Pointer to where the response will be written
 * 
 * @return
 * @li IFD_Success = Operation succeeded
 * @li IFD_Failure = Wrong input parameters
 * @li IFD_Communication_Error = Communication error
 * 
 */
extern RESPONSECODE IFD_PresentEncryptEMVPIN(BYTE * INICCRandom,
                                         BYTE * PubKeyData,
                                         int PubKeyLen,
                                         int Exponent,
                                         BYTE *ResponseData);

/**
 * Check the HW configuration on NAND and return the result or return the slot type
 * 
 * @param[in] slot If 0, HW configuration is checked, else - the slot type is returned
 * 
 * @return Type of the slot (CUSTOMER_CARD or MERCHANT_SLOT_1,2,3,4)
 * 
 */
extern int get_icc_device(int slot);

/**
 * @name Library error codes
 */
/** \{ */
#define NO_ERROR                0x10000000      /**< Un-reported Library error */
/** \} */


/**
 * @name Generic error codes
 */
/** \{ */
#define CARD_DEACTIVATED        0x10000001
#define CARD_MOVED              0x10000002
#define CARD_NOT_PRESENT        0x10000003      /**< VerixV only */
/** \} */


/**
 * @name Verix OS11 hardware errors
 */
/** \{ */
#define SUPERVSR_ACTIVATED      0x10000003      /**< VxOS11 only */
#define VCC_PROTECTION          0x10000004      /**< VxOS11 only */
#define EXCHANGE_BUF_OVERFLOW   0x10000005      /**< VxOS11 only */
#define PARAMETER_ERROR         0x10000006      /**< VxOS11 only */
#define UART_OVERFLOWED         0x10000007      /**< VxOS11 only */
#define UNKNOW_ERROR            0x1000000F      /**< VxOS11 only */
/** \} */


/**
 * @name Error definition for ATR
 */
/** \{ */
#define ATR_MUTE                0x10000010
#define EARLY_ANSWER    		0x10000011
#define ATR_PARITY_ERROR  		0x10000012
#define ATR_WWT_EXCEEDED        0x10000013
#define ATR_DURATION_EXCEEDED   0x10000014
#define TB1_NOT_ZERO    		0x10000015
#define TB1_NOT_PRESENT 		0x10000016
#define NO_T0_NO_T1     		0x10000017
#define B5_OF_TA2_SET           0x10000018
#define TB2_PRESENT     		0x10000019
#define WRONG_WI        		0x1000001A
#define PROTOCOL_MISMATCH       0x1000001B
#define WRONG_IFSC      		0x1000001C
#define WRONG_CWI       		0x1000001D
#define WRONG_BWI       		0x1000001E
#define WRONG_TC1_CWT   		0x1000001F
#define TB3_ABSENT      		0x10000020
#define WRONG_TC3       		0x10000021
#define BAD_FiDi        		0x10000022
#define ATR_CHECKSUM_ERROR      0x10000023
#define ATR_LEN_EXCEEDED        0x10000024
#define TS_NEITHER_3B_OR_3F     0x10000025
#define ATR_NOT_SUPPORTED   	0x10000026
#define WARM_RESET_REQUIRED     0x10000027      /**< VxOS11 only */
/** \} */


/**
 * @name Error definition for T=0 protocol
 */
/** \{ */
#define TOO_SHORT_APDU          0x10000030
#define WRONG_APDU              0x10000031
#define WWT_EXCEEDED            0x10000032
#define INS_ERROR               0x10000033
#define T0_PARITY_ERROR  		0x10000034
/** \} */


/**
 * @name Error definition for T=1 protocol
 */
/** \{ */
#define CARD_MUTE_NOW   		0x10000050
#define RESYNCHRONISED  		0x10000051
#define CHAIN_ABORT     		0x10000052
#define BAD_NAD         		0x10000053
#define IFSD_NOT_ACCEPTED		0x10000054
#define T1_PARITY_OR_LRC_ERROR  0x10000055      /**< VxOS11 only */
#define WRONG_RLEN_T1           0x10000056      /**< VxOS11 only */
#define ERR_SCR_IO_CONTENTION   0x10000057      /**< VxOS11 only */
/** \} */


/**
 * @name Error definition for PPS negotiation
 */
/** \{ */
#define PARAM_ERROR     		0x10000070
#define PPS_NOT_ACCEPTED 		0x10000071
#define RESPONSE_ERROR  		0x10000072
#define PCK_ERROR       		0x10000073
#define PPS_PARITY_ERROR        0x10000074
/** \} */


/**
 * @name Hardware errors
 */
/** \{ */
#define CARD_ERROR              0x100000E0      /**< VerixV only */
#define BAD_CLOCK_CARD          0x100000E1      /**< VerixV only */
#define UART_OVERFLOW           0x100000E2      /**< VerixV only */
#define SUPERVISOR_ACTIVATED    0x100000E3      /**< VerixV only */
#define TEMPERATURE_ALARM       0x100000E4      /**< VerixV only */
#define FRAMING_ERROR           0x100000E9      /**< VerixV only */
/** \} */


/**
 * @name Verix OS errors
 */
/** \{ */
#define TOO_MANY_CARDS_POWERED  0x00000109
#define READER_TIMED_OUT        0x0000010A
/** \} */

#ifdef __cplusplus
}
#endif

#endif // __LIBVOY__
/// @} */
/// @} */
