#ifndef __VFI_PERSO_H__
#define __VFI_PERSO_H__

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

#define MAGIC_KEY_DIRECTORY_SYS "/mnt/flash/system/magic_keys"
#define MAGIC_KEY_DIRECTORY_USR "flash/share/magic_keys"

#define MAGIC_PUBKEY_AXALTO_CA    0x00      /*!< Top level certification public key */
#define MAGIC_PUBKEY_TERM_CA      0x01      /*!< Terminal manufacturer CA key       */
#define MAGIC_PUBKEY_AUTH_SP      0x02      /*!< Firmware authentication key        */
#define MAGIC_PUBKEY_AUTH_CUST    0x03      /*!< Customer authentication key        */
#define MAGIC_PUBKEY_UNLOCK       0x04      /*!< Terminal unlocking key             */
#define MAGIC_PUBKEY_TERM         0x05      /*!< Terminal authentication public key */
#define MAGIC_PUBKEY_INJ          0x06      /*!< Customer injection public key      */
#define MAGIC_PUBKEY_SERV_CA      0x07      /*!< Server CA public key               */
#define MAGIC_PUBKEY_SERV         0x08      /*!< Server public key                  */
#define MAGIC_PUBKEY_PROMPT_A     0x09      /*!< Public key system prompt           */
#define MAGIC_PUBKEY_PROMPT_B     0x0A      /*!< Public key customer prompt         */
#define MAGIC_PUBKEY_RESET_B      0x0B      /*!< Public key for key reset               */
#define MAGIC_PUBKEY_AUTH_MP_A    0x0C      /*!< Public key for axalto package authentication */
#define MAGIC_PUBKEY_AUTH_MP_B    0x0D      /*!< Public key for customer package authentication */
#define MAGIC_PUBKEY_RESET_A      0x0E      /*!< Public key for key reset               */
#define MAGIC_PUBKEY_TERM_PP      0x0F      /*!< Public key for PP authentication   */

#define MAGIC_PUBKEY_AUTH_CUST_EXT 0x28     /*!< 2nd Customer authentication key  */
#define MAGIC_PUBKEY_AUTH_PP_EXT   0x29     /*!< AUTH_PP Public key of external device  */
#define MAGIC_PUBKEY_TERM_PP_EXT   0x2A     /*!< AUTH_TERM Public key of external device    */

#define MAGIC_PUBKEY_TERM_CA_CERT   0x11    /*!< Terminal manuf. CA certificate     */
#define MAGIC_PUBKEY_AUTH_SP_CERT   0x12    /*!< Firmware authentication certif.    */
#define MAGIC_PUBKEY_AUTH_CUST_CERT 0x13    /*!< Customer authen. certificate       */
#define MAGIC_PUBKEY_UNLOCK_CERT    0x14    /*!< Terminal unlocking certificate     */
#define MAGIC_PUBKEY_TERM_CERT      0x15    /*!< Terminal authen. certificate       */
#define MAGIC_PUBKEY_INJ_CERT       0x16    /*!< Customer injection certificate     */
#define MAGIC_PUBKEY_PROMPT_A_CERT  0x19    /*!< System prompt certificate          */
#define MAGIC_PUBKEY_PROMPT_B_CERT  0x1A    /*!< Customer prompt certificate        */
#define MAGIC_PUBKEY_RESET_B_CERT   0x1B    /*!< Reset certificate                  */
#define MAGIC_PUBKEY_AUTH_MP_A_CERT 0x1C    /*!< Axalto package auth. certificate   */
#define MAGIC_PUBKEY_AUTH_MP_B_CERT 0x1D    /*!< Customer package auth. certificate */
#define MAGIC_PUBKEY_RESET_A_CERT   0x1E    /*!< Reset certificate                  */
#define MAGIC_PUBKEY_TERM_PP_CERT   0x1F    /*!< Terminal auth. certificate for PP  */

#define MAGIC_PUBKEY_AUTH_CUST_EXT_CERT   0x38   /*!< 2nd customer authen. certificate  */

#define MAGIC_KEK_DATA              0x50

#define MAGIC_PRIVKEY_TERM          0x20    /*!< Terminal private key               */
#define MAGIC_PRIVKEY_TERM_PP       0x21    /*!< Terminal auth. priv. for PP        */
#define MAGIC_PUBKEY_AUTH_PP_A      0x22    /*!< M3 Pinpad mutual auth key A or     */
#define MAGIC_PUBKEY_AUTH_PP_B      0x23    /*!< M3 Pinpad mutual auth key A or B   */
#define MAGIC_PUBKEY_AUTH_PP_A_CERT 0x24    /*!< M3 pinpad mutual auth cert A       */
#define MAGIC_PUBKEY_AUTH_PP_B_CERT 0x25    /*!< M3 pinpad mutual auth cert B       */
#define MAGIC_PUBKEY_TERM_M3PA_CERT 0x26    /*!< Terminal public key M3P certificate*/
#define MAGIC_PUBKEY_TERM_M3PB_CERT 0x27    /*!< Terminal public key M3P certificate*/

typedef struct
{
   unsigned int uKeyID;               /* Key unique identifier     */
   unsigned int uExponent;            /* Public key exponent       */
   unsigned short wModuloLength;        /* Public key modulo         */
   unsigned char tbModulus[1];          /* Public key modulus        */
} __attribute__ ((packed)) MAGIC_PUBKEY_TYPE;

typedef struct
{
   unsigned int uCertifiedID;         /* Certified key ID         */
   unsigned int uCertifyingID;        /* Certifying key ID        */
   unsigned int uValidFrom;           /* Valid from ddmmyyyy      */
   unsigned int uValidTo;             /* Valid to ddmmyyyy        */
   unsigned short wSignatureSize;       /* Signature size in bytes  */
   unsigned char tbSignature[1];        /* Certificate data         */
} __attribute__ ((packed)) MAGIC_PUBCERT_TYPE;

typedef struct
{
   unsigned char tbSha256[32];          /* SHA-256 hash*/
   unsigned char bHashType;             /* Type of additional Hash   */	
   unsigned char bKeySlot;              /* Key usage                 */	
   unsigned char bByte0;                /* Zero byte                 */
   unsigned char bRDT1;                 /* Recover data trailer ABh  */
   unsigned int uCertifiedID;           /* Certified key ID          */
   unsigned int uCertifyingID;          /* Certifying key ID         */
   unsigned int uValidFrom;             /* Valid from ddmmyyyy       */
   unsigned int uValidTo;               /* Valid to ddmmyyyy         */
   unsigned int uExponent;              /* Public key exponent       */
   unsigned char bRDT2;                 /* Recover data trailer CDh  */
   unsigned char tbSha1[20];            /* SHA-1 hash  (Deprecated, replaced by SHA-256) */
} __attribute__ ( ( packed ) ) MAGIC_SIGNATURE_TYPE;

// Error value returned (compliant with Magic returned type)
enum {
   P_OK = 1,
   P_BAD_PARAM = 900,
   P_KEY_NOT_PRESENT = 902,
   P_KEY_PRESENT = 912,
   P_ERROR = -1,
};

int perso_readPublicKey( const int f_bType, unsigned short * f_pwPubKeyLenth, unsigned char * f_pbPubKey );
int perso_readCertificate( const int f_bType, unsigned short * f_pwCertLength, unsigned char * f_pbCertificate );
int perso_writePublicKey( const int f_bKeyType, const unsigned short f_wPubKeyLength, const unsigned char * f_pbPublicKey );
int perso_writeCertificate( const int f_bCertType, const unsigned short f_wCertLength, const unsigned char * f_pbCertificate );

int personalization_process (char *port, FILE *fp_msg);

int comm_connect (const char *port);
ssize_t comm_receive (char *buffer, size_t size);
ssize_t comm_send (const char *buffer, size_t size);
int comm_close (void);


#ifdef __cplusplus
}
#endif

#endif /* __VFI_PERSO_H__ */
