#ifndef __SYSM_NETWORK__
#define __SYSM_NETWORK__

#include <map>
#include <event_engine/event_listener.h>
#include <event_engine/event_engine.h>
#include <sysmode_ui/sysm_scroll.h>
#include <sysmode_ui/sysm_ui.h>
#include <sysmode_ui/sysm_lang.h>
#include <sysmode_ui/setting.h>
#include <sysmode_ui/settings_page.h>
#include <svcmgr/svc_net.h>

#ifdef RAPTOR
#define DEFAULT_WPASUPP_FILELOC "/mnt/flash/config/wificfg.conf"
#else
#define DEFAULT_WPASUPP_FILELOC "/mnt/flash/etc/config/wificfg.conf"
#endif
#define WIFI_DEFAULT_PSK "Vfiguest"
#define WIFI_DEVICE "wlan0"
#define MAX_BT_PIN_LEN 8
#define BT_DEFAULT_PIN "0000"

extern "C" {
    #define class btClass
    #include <svcmgr/svc_bluetooth.h>
    #undef class
    #ifdef RAPTOR
    #include <modem.h>
    #else
    #include <linux/modem.h>
    #endif
}

enum {
	EVENT_LISTENER_LIST = 1,
	EVENT_LISTENER_STATUS,
	EVENT_LISTENER_MODE,
	EVENT_LISTENER_SPEED,
	EVENT_LISTENER_AUTOSTART,
	EVENT_LISTENER_IP,
	EVENT_LISTENER_BC,
	EVENT_LISTENER_MASK,
	EVENT_LISTENER_GW,
	EVENT_LISTENER_DNS_1,
	EVENT_LISTENER_DNS_2,
	EVENT_LISTENER_IFACE,
	EVENT_LISTENER_RONLY,
	EVENT_LISTENER_MAC,
	EVENT_LISTENER_MTU,
	EVENT_LISTENER_BRIDGE,
	EVENT_LISTENER_USB_GADGET,
	EVENT_LISTENER_USB_GADGET_PROTOCOL,
	EVENT_LISTENER_LIST_IPV6,  //-----------IPV6-----------------------
	EVENT_LISTENER_STATUS_IPV6,
	EVENT_LISTENER_MODE_IPV6,
	EVENT_LISTENER_SPEED_IPV6,
	EVENT_LISTENER_AUTOSTART_IPV6,
	EVENT_LISTENER_LINKLOCAL_IPV6,
	EVENT_LISTENER_LINKLOCAL_MASK_IPV6,
	EVENT_LISTENER_SITELOCAL_IPV6,
	EVENT_LISTENER_SITELOCAL_MASK_IPV6,
	EVENT_LISTENER_UNIQUEADD_IPV6,
	EVENT_LISTENER_UNIQUEADD_MASK_IPV6,
	EVENT_LISTENER_GLOBALADD_IPV6,
	EVENT_LISTENER_GLOBALADD_MASK_IPV6,
	EVENT_LISTENER_DEFAULTGW_IPV6,
	EVENT_LISTENER_DNS_1_IPV6,
	EVENT_LISTENER_DNS_2_IPV6,
	EVENT_LISTENER_IFACE_IPV6,
	EVENT_LISTENER_WPA_IPV6,
	EVENT_LISTENER_MAC_IPV6,
};

class eth_config_c : public SYSM_UI::settings_page_c
{
	bool			m_changed;	
	event_engine_c 		*m_engine;

	/*
	 * This is the struct retrieved by
	 * net_interfaceGet(), and represents
	 * XML settings
	 */
	struct netIfconfig 	m_ifconfig;
	char m_mtu[16];

	/*
	 * This struct reflects data returned by
	 * net_interfaceStatus()
	 */
	struct netIfconfig	m_ifstatus;
public:
	eth_config_c(const char* iface_name);
	void display();
	struct netIfconfig get_ifconfig() const;
	bool settings_changed() const;

protected:
	void on_event(const event_c *event);

	void update_addr_ronly(unsigned int value);
	void update_addr_value(struct netIfconfig);
	void update_status();
	void error_fallback_status(const char *, 
				struct netIfconfig,
				unsigned int);
};

class eth_ipv6_config_c : public SYSM_UI::settings_page_c
{
	bool			m_changed;
	event_engine_c 		*m_engine;

	/*
	 * This is the struct retrieved by
	 * net_interfaceGet(), and represents
	 * XML settings
	 */
	struct netIfconfigIpv6 	m_ifconfigIpv6;

	/*
	 * This struct reflects data returned by
	 * net_interfaceStatusIpv6()
	 */
	struct netIfconfigIpv6	m_ifstatusIpv6;
public:
	eth_ipv6_config_c(const char* iface_name);
	void display();
	struct netIfconfigIpv6 get_ifconfigIpv6() const;
	bool settings_changed() const;

protected:
	void on_event(const event_c *event);

	void update_addr_ronly(unsigned int value);
	void update_addr_value(struct netIfconfigIpv6);
	void update_status();
	void error_fallback_status(const char *,
				struct netIfconfigIpv6,
				unsigned int);
};

enum {
	WIFI_EVENT_LISTENER_LIST = 1,
	WIFI_EVENT_LISTENER_STATUS,
	WIFI_EVENT_LISTENER_LINK_STATUS,
	WIFI_EVENT_LISTENER_MODE,
	WIFI_EVENT_LISTENER_SPEED,
	WIFI_EVENT_LISTENER_ACTIVATE,
	WIFI_EVENT_LISTENER_IP,
	WIFI_EVENT_LISTENER_BC,
	WIFI_EVENT_LISTENER_MASK,
	WIFI_EVENT_LISTENER_GW,
	WIFI_EVENT_LISTENER_DNS_1,
	WIFI_EVENT_LISTENER_DNS_2,
	WIFI_EVENT_LISTENER_WPA_CONF,
	WIFI_EVENT_LISTENER_COUNTRY,
	WIFI_EVENT_LISTENER_MTU,
	WIFI_EVENT_LISTENER_IFACE,
	WIFI_EVENT_LISTENER_MAC,
	WIFI_EVENT_LISTENER_LIST_IPV6, //-----------IPV6----------------------
	WIFI_EVENT_LISTENER_STATUS_IPV6,
	WIFI_EVENT_LISTENER_LINK_STATUS_IPV6,
	WIFI_EVENT_LISTENER_MODE_IPV6,
	WIFI_EVENT_LISTENER_SPEED_IPV6,
	WIFI_EVENT_LISTENER_ACTIVATE_IPV6,
	WIFI_EVENT_LISTENER_LINKLOCAL_IPV6,
	WIFI_EVENT_LISTENER_LINKLOCAL_MASK_IPV6,
	WIFI_EVENT_LISTENER_SITELOCAL_IPV6,
	WIFI_EVENT_LISTENER_SITELOCAL_MASK_IPV6,
	WIFI_EVENT_LISTENER_UNIQUEADD_IPV6,
	WIFI_EVENT_LISTENER_UNIQUEADD_MASK_IPV6,
	WIFI_EVENT_LISTENER_GLOBALADD_IPV6,
	WIFI_EVENT_LISTENER_GLOBALADD_MASK_IPV6,
	WIFI_EVENT_LISTENER_GW_IPV6,
	WIFI_EVENT_LISTENER_DNS_1_IPV6,
	WIFI_EVENT_LISTENER_DNS_2_IPV6,
	WIFI_EVENT_LISTENER_WPA_CONF_IPV6,
	WIFI_EVENT_LISTENER_COUNTRY_IPV6,
	WIFI_EVENT_LISTENER_MTU_IPV6,
	WIFI_EVENT_LISTENER_IFACE_IPV6,
	WIFI_EVENT_LISTENER_MAC_IPV6,
};

class wifi_iface_c : public SYSM_UI::settings_page_c {
	bool m_changed;
	event_engine_c *m_engine;
	struct netIfconfig m_ifconfig;
	struct netIfconfig m_ifstatus;
	char m_country[3];
	char m_mtu[16];

public:
	wifi_iface_c();
	~wifi_iface_c();
	void display();
	struct netIfconfig get_ifconfig() const;
	bool settings_changed() const;
	int on_refresh();
	int save_settings();

protected:
	int start_wifi(void);
	int stop_wifi(void);
	void on_event(const event_c *event);
	void update_addr_ronly(unsigned int value);
	void update_addr_value(struct netIfconfig);
};

class wifi_iface_ipv6_c : public SYSM_UI::settings_page_c {
	bool m_changed;
	event_engine_c *m_engine;
	struct netIfconfigIpv6 m_ifconfigIpv6;
	struct netIfconfigIpv6 m_ifstatusIpv6;
	char m_country[3];
	char m_mtu[16];

public:
	wifi_iface_ipv6_c();
	~wifi_iface_ipv6_c();
	void display();
	struct netIfconfigIpv6 get_ifconfigIpv6() const;
	bool settings_changed() const;
	int on_refresh();
	int save_settings();

protected:
	int start_wifi(void);
	int stop_wifi(void);
	void on_event(const event_c *event);
	void update_addr_ronly(unsigned int value);
	void update_addr_value(struct netIfconfigIpv6);
};

enum {
	WIFI_STATUS_EVENT_LISTENER_LIST = 1,
	WIFI_STATUS_EVENT_LISTENER_SSID,
	WIFI_STATUS_EVENT_LISTENER_BSSID,
	WIFI_STATUS_EVENT_LISTENER_STRENGTH,
	WIFI_STATUS_EVENT_LISTENER_TYPE
};

class wifi_status_page_c : public SYSM_UI::settings_page_c {
	bool m_changed;
	event_engine_c *m_engine;
	struct netWifiInfo m_wfinfo;

public:
	wifi_status_page_c();
	~wifi_status_page_c();
	int on_refresh();

protected:
	void on_event(const event_c *event);
	void update_wifi_value(struct netWifiInfo);
	void update_addr_value(struct netIfconfig);
};

enum {
	WIFI_SCAN_EVENT_LISTENER_LIST = 1,
};

class wifi_scan_page_c : public SYSM_UI::settings_page_c {
	std::map<event_listener_id_t, struct netSiteSurvey *> m_found_nets;
	struct netSiteSurveyList m_site_survey_list;

public:
	SYSM_UI::t_ui_menu_entry *m_menu_entry;
	wifi_scan_page_c();
	~wifi_scan_page_c();
	void display();
	void display_network(struct netSiteSurvey *site);

protected:
	void on_event(const event_c *event);
};

#define SUPPLICONF_LEN (32)

struct wifi_suppliconf {
	char ssid[SUPPLICONF_LEN];
	char proto[SUPPLICONF_LEN];
	char group[SUPPLICONF_LEN];
	char pairwise[SUPPLICONF_LEN];
	char keymgmt[SUPPLICONF_LEN];
	char psk[SUPPLICONF_LEN];
};

enum {
	WIFI_CONFIGURATION_EVENT_LISTENER_LIST = 1,
	WIFI_CONFIGURATION_EVENT_LISTENER_SSID,
	WIFI_CONFIGURATION_EVENT_LISTENER_BSSID,
	WIFI_CONFIGURATION_EVENT_LISTENER_ENCRYPTION,
	WIFI_CONFIGURATION_EVENT_LISTENER_AUTHENTICATION,
	WIFI_CONFIGURATION_EVENT_LISTENER_PSK
};

class wifi_configuration_page_c : public SYSM_UI::settings_page_c {
public:
	struct netSiteSurvey *m_site;
	wifi_configuration_page_c(struct netSiteSurvey *site);
	~wifi_configuration_page_c();

protected:
	void on_event(const event_c *event);

private:
	char m_psk[64];
	int save_suppliconf();
};

enum {
	GPRS_EVENT_LISTENER_LIST = 1,
	GPRS_EVENT_LISTENER_POWER,
	GPRS_EVENT_LISTENER_STATUS,
	GPRS_EVENT_LISTENER_PIN_VERIFY,
	GPRS_EVENT_LISTENER_SIGNAL,
	GPRS_EVENT_LISTENER_PHONE,
	GPRS_EVENT_LISTENER_MODE,
	GPRS_EVENT_LISTENER_APN,
	GPRS_EVENT_LISTENER_OP_ID,
	GPRS_EVENT_LISTENER_GPRS_SAVE,
	GPRS_EVENT_LISTENER_USER,
	GPRS_EVENT_LISTENER_PASS,
	GPRS_EVENT_LISTENER_PPP_SAVE,
	GPRS_EVENT_LISTENER_CONNECT,
};

class gprs_conf_page_c : public SYSM_UI::settings_page_c
{
	struct netGprsInfo m_gprs_info;
	struct netIfconfig m_gprs_iface;
public:
	gprs_conf_page_c();
	virtual ~gprs_conf_page_c();
	void update_fields();
protected:
	void on_event(const event_c *event);
};


enum{
	
	MODEM_EVENT_LISTENER_LIST=1,	
	MODEM_EVENT_LISTENER_COUNTRY_ID,
	MODEM_EVENT_LISTENER_COUNTRY_NAME,
};

class modem_conf_page_c : public SYSM_UI::settings_page_c
{
	struct st_modem_info stModemInfo;
	char countryCode[MDM_MAX_COUNTRY_CODE_SIZE];
	bool m_changed;
	bool isModemInit;
public:
	modem_conf_page_c();
	virtual ~modem_conf_page_c();
	void update_country_code();
	int isMdmInit();
	void display();
	char* modem_get_country_name();
protected:
	void on_event(const event_c *event);
};


enum {
	BT_CONF_EVENT_LISTENER_LIST = 1,
	BT_CONF_EVENT_LISTENER_STATUS, 
	BT_CONF_EVENT_LISTENER_VISIBLE,
	BT_CONF_EVENT_LISTENER_NAME, 
	BT_CONF_EVENT_LISTENER_ADDRESS, 
	BT_CONF_EVENT_LISTENER_VERSION,
};

class bt_conf_page_c : public SYSM_UI::settings_page_c
{
	struct btConf m_config;
public:
	bt_conf_page_c();
protected:
	void on_event(const event_c *event);
};


enum {
	BT_DEV_EVENT_LISTENER_LIST = 1,
	BT_DEV_EVENT_LISTENER_STATUS,
	BT_DEV_EVENT_LISTENER_NAME,
	BT_DEV_EVENT_LISTENER_ADDRESS,
	BT_DEV_EVENT_LISTENER_AUTOSTART,
	BT_DEV_EVENT_LISTENER_PAN,
	BT_DEV_EVENT_LISTENER_DUN,
	BT_DEV_EVENT_LISTENER_RSSI,
	BT_DEV_EVENT_LISTENER_LQ,
	BT_DEV_EVENT_LISTENER_IP,
	BT_DEV_EVENT_LISTENER_MODE,
	BT_DEV_EVENT_LISTENER_ETHERNET,
	BT_DEV_EVENT_LISTENER_VERSION,
	BT_DEV_EVENT_LISTENER_SERIAL,
};

class bt_dev_page_c : public SYSM_UI::settings_page_c
{
	struct netBluetoothInfo m_conf;
	char* m_dev_id;
	pthread_t m_sig_thread;
public:
	bt_dev_page_c(const char* dev_id, struct netBluetoothInfo & info);
	~bt_dev_page_c();
	void on_refresh();
protected:
	void on_event(const event_c *event);
private:
	void startTimer( );
	void stopTimer( );
};

enum {
	BT_PAIR_PAGE_LISTENER_LIST = 1,
	BT_PAIR_PAGE_LISTENER_PAIRMODE,
	BT_PAIR_PAGE_LISTENER_PIN,
	BT_PAIR_PAGE_LISTENER_SAVE
};

class bt_pair_page_c : public SYSM_UI::settings_page_c
{
	bool state_is_ok;
	unsigned int mode;
	char pin[MAX_BT_PIN_LEN + 1];
	SYSM_UI::settings_page_c *parent;
public:
	bt_pair_page_c(SYSM_UI::settings_page_c *e, int sspauto);
	~bt_pair_page_c();
	void get_pin(char * buffer);
	int get_state();
	unsigned int get_mode();
protected:
	void on_event(const event_c *event);
private:
	int save_pairing_settings(bt_pair_page_c * page);
	void update_pin_value(const char * value);
	void update_pin_ronly(unsigned int value);
};

enum {
	BT_SCAN_EVENT_LISTENER_LIST = 1,
};

class bt_scan_page_c : public SYSM_UI::settings_page_c
{
	struct btDeviceList m_device_list;
public:
	bt_scan_page_c();
	~bt_scan_page_c();
	int scan();
protected:
	void on_event(const event_c *event);
};

void net_eth_cb(const SYSM_UI::t_ui_menu_entry *);
void net_eth_ipv6_cb(const SYSM_UI::t_ui_menu_entry *);

void net_usb_eth_gadget_cb(const SYSM_UI::t_ui_menu_entry *);

void net_wifi_iface_cb(const SYSM_UI::t_ui_menu_entry *);
void net_wifi_iface_ipv6_cb(const SYSM_UI::t_ui_menu_entry *);
void net_wifi_scan_cb(const SYSM_UI::t_ui_menu_entry *);
void net_wifi_configuration_cb(const SYSM_UI::t_ui_menu_entry *);
void net_wifi_status_cb(const SYSM_UI::t_ui_menu_entry *);

void net_wifi_ipv6_conf_cb(const SYSM_UI::t_ui_menu_entry *);
void net_wifi_ipv6_stat_cb(const SYSM_UI::t_ui_menu_entry *);
void net_wifi_ipv6_scan_cb(const SYSM_UI::t_ui_menu_entry *);

void net_wifi_wpa_supp_conf_cb(const SYSM_UI::t_ui_menu_entry *);
void net_wifi_wpa_supp_stat_cb(const SYSM_UI::t_ui_menu_entry *);
void net_wifi_wpa_supp_scan_cb(const SYSM_UI::t_ui_menu_entry *);

void net_wifi_wpa_supp_ipv6_conf_cb(const SYSM_UI::t_ui_menu_entry *);
void net_wifi_wpa_supp_ipv6_stat_cb(const SYSM_UI::t_ui_menu_entry *);
void net_wifi_wpa_supp_ipv6_scan_cb(const SYSM_UI::t_ui_menu_entry *);

void net_gprs_cb(const SYSM_UI::t_ui_menu_entry *);
void net_modem_cb(const SYSM_UI::t_ui_menu_entry *);

void net_bt_conf_cb(const SYSM_UI::t_ui_menu_entry *);
void net_bt_dev_cb(const SYSM_UI::t_ui_menu_entry *);
void net_bt_scan_cb(const SYSM_UI::t_ui_menu_entry *);
void net_bt_ibeacon_cb(const SYSM_UI::t_ui_menu_entry *);

#endif
