/*********************************************************************
* $Log:   /h03_u01/pvcs/Combined/PVCS8.0/Engg_Projects/archives/Spectrum/common/protocol/protPktAPI.h-arc  $
 * 
 *    Rev 1.2   16 Mar 2006 09:37:52   kevin_s1
 * Fixed cplusplus wrapper to be
 * inside header file define.
 * 
 *    Rev 1.1   08 Mar 2006 09:29:52   kevin_s1
 * Added cplusplus wrapper.
 * 
 *    Rev 1.0   09 Nov 2005 16:48:50   kevin_s1
 * Initial revision.
*
*		Copyright, 2004 VeriFone Inc.
*		2099 Gateway Place, Suite 600
*		San Jose, CA.  95110
*
*  All Rights Reserved. No part of this software may be reproduced,
*  transmitted, transcribed, stored in a retrieval system, or
*  translated into any language or computer language, in any form
*  or by any means electronic, mechanical, magnetic, optical,
*  chemical, manual or otherwise, without the prior permission of
*  VeriFone Inc.
*
***********************************************************************/

/** @addtogroup */
/** @{ */
/** @addtogroup */
/** @{ */
/** 
 *  @file protPktAPI.h 
 *
 *	@brief Protocol Packet API
 *
 * 
 */
 
#ifndef PKTMODEAPI
#define PKTMODEAPI

#ifdef __cplusplus
extern "C" {
#endif


/**
 * Initialize Packet mode
 *
 * @param[in] hCom Handle of an open serial port
 * @param[in] openBlock Serial communication control structure
 *
 * @return 
 * @li 0 = Success
 * @li < 0 = Check errno
 *
 */
int startPktMode(short hCom, struct Opn_Blk *pOpnBlk);

/**
 * Free the Packet mode buffers at session completion
 *
 */
void endPktMode(void);

/**
 * Enable a counter increment on transitions on the CTS and/or DCD lines
 *
 * @note It is VERY IMPORTANT to call comMonitorHandshake() with a NULL for the callback function parameter immediately before closing the COM port
 *
 * @param[in] void(*cmdsCallback)(int, int, int) Pointer to a callback function that must accept three parameters:
 * cts = Number of CTS transitions since last callback
 * dcd = Number of DCD transitions since last callback
 * status = State of DCD and CTS lines
 * @param[in] fd File descriptor for COM2
 * @param[in] mask Handshake lines to monitor, bits
 *
 * @return 
 * @li 0 = Success
 * @li < 0 = Check errno
 *
 */
int comMonitorHandshake(void(*cmdsCallback)(int, int, int),int fd, int mask);

#ifdef __cplusplus
}
#endif
#endif
/// @} */
/// @} */
