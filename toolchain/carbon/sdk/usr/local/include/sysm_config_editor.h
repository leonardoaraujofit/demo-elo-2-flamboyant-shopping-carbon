#ifndef __SYSM_CONFIG_EDITOR__
#define __SYSM_CONFIG_EDITOR__

#include <sysmode_ui/sysm_ui.h>
#include <sysmode_ui/property.h>
#include <sysmode_ui/settings_page.h>
#include <vector>
#include "svcmgr/svc_utility.h"

enum {
	CNFG_EDTR_EVENT_LISTENER_LIST = 1
};

class config_editor_c : public SYSM_UI::settings_page_c
{
public:
	
	typedef enum {
		TK_SECTION = 0,
		TK_ENTRY
	} t_btn_type;
	
	typedef struct callback_data {
		t_btn_type btn_type;
		const char *section;
		const char *filename;
		char *name;
		char *value;
	} t_callback_data;
	
	config_editor_c(const char* filename);
	void cleanup(void);
protected:
	void on_event(const event_c *event);
private:
	vector<t_callback_data *> callback_data_list;
	struct utilityIniContents ic;
	int addNewVar(t_callback_data *cb_data_tmp);
	int modifyVar(t_callback_data *cb_data_tmp);
	int deleteVar(t_callback_data *cb_data_tmp);
};

void config_editor_cb(const SYSM_UI::t_ui_menu_entry *e );


#endif
