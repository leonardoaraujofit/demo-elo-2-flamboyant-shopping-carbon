/*
 *      Copyright, 2013 VeriFone Inc.
 *      2099 Gateway Place, Suite 600
 *      San Jose, CA.  95110
 *
 *  All Rights Reserved. No part of this software may be reproduced,
 *  transmitted, transcribed, stored in a retrieval system, or
 *  translated into any language or computer language, in any form
 *  or by any means electronic, mechanical, magnetic, optical,
 *  chemical, manual or otherwise, without the prior permission of
 *  VeriFone Inc.
 */

/*!
 * \file pxmgr.h
 * \brief This is the Proxy Manager API header file.
 */

/** @defgroup pxmgr_api PXMGR API
 * The Proxy Manager API contains the low level methods that are invoked to interface
 * from a stub library to the proxy server component.
 *  @{
 */
#include <stdio.h>
#include <ber-tlv.h>

#ifndef PXMGR_H_
#define PXMGR_H_
#define reqsizeof(val) pxm_getSizeOfValue(val)
#define ICC_BUF_SIZE 	300
#define ICC_Tx_Rx_BUF_SIZE  480

/**
 Errno returned by proxymnager in case of error:
 - ENOTCONN: Remote device not connected
 - ENOMEM: Memory allocation errror
 - EINVAL: invalid input parameters
 - EKEYEXPIRED: Unable to process outgoing encryption
 - EKEYREJECTED: Unable to process incoming decryption
 - ENOENT: Read/write error
 - ENXIO: Port resolution request failed
 - ENODEV: Device (server) not found 
 - EBUSY: Shared ressource is busy
 - EPROTO: Protocol error
 */

//List of svc_sysinfo function IDs
enum {
    FUNC_GET_VERSION,
	FUNC_MEMORY_USAGE,
	FUNC_SYSINFO_PLATFORM,
	FUNC_SYSINFO_BUNDLES_INFO,
	FUNC_SYSINFO_SOFTWARE,
	FUNC_SYSINFO_SOFTWARE_EXTENDED,
	FUNC_SYSINFO_PACKAGES,
	FUNC_SYSINFO_PACKAGES_EXTENDED,
	FUNC_BATTERY_STATUS,
	FUNC_MIBI_INFO
};


//List of svc_utility function IDs
enum {
    FUNC_GETVERSION,
    FUNC_COPYTIME_LINUX_RTC,
    FUNC_GETTIME,
	FUNC_GETENVFILENAME,
	FUNC_PUTENVFILENAME,
	FUNC_INSTALL_FILE,
	FUNC_RUN_APPLICATION,
	FUNC_INI_CONTENTS,
	FUNC_REBOOT,
	FUNC_EXTERNAL_STORAGE,
	FUNC_DUMPLOGS,
	FUNC_SETTIME
};


// List of voyager function IDs
enum {
    FUNC_ICC_PRESENT,
    FUNC_ICC_ABSENT,
    FUNC_GET_CAPABILITIES,
    FUNC_SET_CAPABILITIES,
	FUNC_POWER_ICC,
	FUNC_TRANSMIT_TRANSPARENT,
	FUNC_SET_PROTOCOL_PARAMETERS,
	FUNC_CONFISCATE_ICC,
	FUNC_EJECT_ICC,
	FUNC_SWALLOW_ICC,
	FUNC_PRESENT_CLEAR_TEXT,
	FUNC_PRESENT_ENCRYPT_EMV_PIN,
	FUNC_TRANSMIT_ICC
};

// List of netloader function IDs
enum {
    FUNC_NETLOADER_GETVERSION,
    FUNC_NETLOADER_START,
    FUNC_NETLOADER_STOP,
    FUNC_NETLOADER_SENDFILE,
    FUNC_NETLOADER_SENDFILEGETEVENT,
    FUNC_NETLOADER_SENDFILECANCELALL
};

// List of libled function IDs
enum {
// 	FUNC_LED_OPENSMRTCARD,
// 	FUNC_LED_CLSSMRTCARD,
	FUNC_LED_INIT,
	FUNC_LED_RELEASE,
	FUNC_LED_LEDON,
	FUNC_LED_LEDOFF,
	FUNC_LED_STRTBLNK,
	FUNC_LED_STOPBLNK,
	FUNC_LED_READLED,
	FUNC_LED_READCTLSLED,
// 	FUNC_LED_ONOFFSCR,
// 	FUNC_LED_CHKSMRTCRD,
	FUNC_LED_SWTCHCOLOR
};


enum {
    PXM_DEVICE_SEC_ID = 0,
    PXM_DEVICE_VOY_ID = 1,
    PXM_DEVICE_MSR_ID = 2,
	PXM_DEVICE_SVC_UTILITY_ID=3,
	PXM_DEVICE_SVC_SYSINFO_ID=4,
	PXM_DEVICE_SVC_NETLOADER_ID=5,
	PXM_DEVICE_RFCR_A_ID=6,
	PXM_DEVICE_RFCR_B_ID=7,
	PXM_DEVICE_LED_ID = 8
};


typedef enum {
    PXM_OPT_NONE = 0, /** Plain text (no encryption) */
    PXM_OPT_CMAC = 1, /** Data signed using MAC */
    PXM_OPT_CMAC_VAULT = 2, /** Data signed using MAC in Vault */
    PXM_OPT_ENC_AES = 4, /** Data encrypted using AES */
    PXM_OPT_ENC_AES_VAULT = 8, /** Data encrypted using AES in Vault */
} PXM_OPTIONS;

typedef enum {
    PXM_SERVER_THREAD=0, /** Server will be created in a separate thread */
    PXM_SERVER_PROCESS, /** Server will be created in a separate process  */
} PXM_SERVER_OPTIONS;

typedef enum {
    PAIRING_NOT_DONE=0, /** Pairing not done (cable not plugged) */
    PAIRING_IN_PROGRESS, /** Mutual authentication in progress */
    PAIRING_NOK, /** Mutual authentication failed, link is not secured */
    PAIRING_OK /** Mutual authentication succeed, link is secured */
} PXM_PAIRING_STATUS;

/** pxm_serverCallback - function called on server for each client request.
 *
 * Note: input/output are freed by the caller if non NULL (proxymanager)
 *
 * @param[in] function_id ID of function requested by client
 * @param[in] data_in  data to be passed to function.
 * @param[in,out] options  pointer to options for output data, initialized with options set by incoming request.
 * @return output data, errno is set (!0) when error occurs.
 */
typedef BER_TLV* (*pxm_serverCallback)( int functionId, BER_TLV* data_in, PXM_OPTIONS *options );

#ifdef __cplusplus
extern "C" {
#endif

/** pxm_callServer - invoked a function on server side.
 *
 * Note: input/output data must be free()'d by caller (if non-null)
 *
 * @param[in] device_id ID of device this function belongs to.
 * @param[in] function_id ID of function to be called via server process.
 * @param[in] option security options set on client request
 * @param[in] data_in  data to be passed to function.
 * @return output data, errno is set (!0) when error occurs.
 */
BER_TLV* pxm_callServer( int device_id, int function_id, PXM_OPTIONS options, BER_TLV* data_in);

/** pxm_registerDevice - Start a proxy server
 *
 * @param[in] device_id ID to associate with the device
 * @param[in] callback function executed on client requests.
 * @return 0 in case of success, -1= error - Check errno
 */
int pxm_registerDevice( int device_id, pxm_serverCallback callback );

/** pxm_registerDeviceOpt - Start a proxy server with options
 *
 * @param[in] device_id ID to associate with the device
 * @param[in] callback function executed on client requests.
 * @param[in] Server options ( See type definition )
 * @return 0 in case of success, -1= error � Check errno
 */
int pxm_registerDeviceOpt( int device_id, pxm_serverCallback callback, PXM_SERVER_OPTIONS options );

/** pxm_unregisterDevice - Stop a proxy server
 *
 * @param[in] device_id ID to associate with the device
 * @return 0 in case of success, -1= error - Check errno
 */
int pxm_unregisterDevice( int device_id );

/** pxm_unregisterAll - Stop all proxy of current process
 *
 * @return 0 in case of success, -1= error - Check errno
 */
int pxm_unregisterAll( );

/** pxm_getPairingStatus - get current status of Ux2/Ux3 link
 *
 * @return current pairing status
 */
PXM_PAIRING_STATUS pxm_getPairingStatus();

/** pxm_getSizeOfValue - Compute number of bytes required to code an number value
 *
 * Can be used to reduce number of bytes used to transfer an integer
 *
 * @param[in] value integrer value to transfer
 * @return 1, 2, 3 or sizeof(int)
 */
int pxm_getSizeOfValue( unsigned int value );

/** pxm_addPID - Add a process to be signalled if the Ux200 to Ux300 link goes down.
 *
 * @param[in] pid Process id
 * @param[in] signal value to raise (from signal.h)
 * @return -1 = Error, >=0 Signal table index
 */
int pxm_addPID( pid_t pid, int signal );

/** pxm_removePID - Remove a process to be signalled if the Ux200 to Ux300 link goes down.
 *
 * @param[in] pid Process Id (index must be set to -1 for this parameter to be used)
 * @param[in] index Signal table index from pxm_addPID (must be 0 or greater)
 * @return NONE
 */
void pxm_removePID( pid_t pid, int index );

#ifdef __cplusplus
}
#endif
#endif /*PXMGR_H_ */

/* @} */// end defgroup
