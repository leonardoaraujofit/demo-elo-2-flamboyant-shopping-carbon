/** @addtogroup */
/** @{ */
/** @addtogroup */
/** @{ */
/** 
 *  @file RFCR_NGapi.h 
 *
 *	@brief Prototypes for a set of general functions that interface
 * with the RF Contactless reader on an Mx 870
 *
 * 
 */

/*============================================================================
	Copyright (C) 2005-2006 by VeriFone Inc. All rights reserved.

	No part of this software may be used, stored, compiled, reproduced,
	modified, transcribed, translated, transmitted, or transferred, in
	any form or by any means  whether electronic, mechanical,  magnetic,
	optical, or otherwise, without the express prior written permission
	of VeriFone, Inc.
==============================================================================*/

#ifndef _RFCR
#define _RFCR

#ifdef __cplusplus
extern "C" {
#endif
 
/**
 * @name ACK/NAK return values
*/
/** \{ */
#define	RFCR_ACK_OK					0
#define	RFCR_NAK_WRONG_FRAMETAG		1
#define	RFCR_NAK_WRONG_FRAMETYPE	2
#define	RFCR_NAK_UNKNOWN_FRAMETYPE	3
#define	RFCR_NAK_UNKNOWN_CMD		4
#define	RFCR_NAK_UNKNOWN_SUBCMD		5
#define	RFCR_NAK_CRC_ERROR			6
#define	RFCR_NAK_FAILED				7
#define	RFCR_NAK_TIMEOUT			8
#define	RFCR_NAK_WRONG_PARAM		9
#define	RFCR_NAK_UNSUPPORTED_CMD	10
#define	RFCR_NAK_UNSUPPORTED_SUBCMD	11
#define	RFCR_NAK_UNSUPPORTED_PARAM	12
#define	RFCR_NAK_ABORTED			13
#define	RFCR_NAK_CMD_NOT_ALLOWED	14
#define	RFCR_NAK_SUBCMD_NOT_ALLOWED	15
/** \} */

/**
 * @name Error return values (mostly from RFCRUpdateFW)
*/
/** \{ */
#define	RFCR_ERROR_NOFILE			-401
#define	RFCR_ERROR_BADFILE			-402
#define	RFCR_ERROR_ENTER_ISP		-403
#define RFCR_ERROR_AUTOBAUD			-404
#define	RFCR_ERROR_FREQ				-405
#define	RFCR_ERROR_ERASE			-406
#define	RFCR_ERROR_RCVTIMEOUT		-407
#define	RFCR_ERROR_FILE_READ		-408
#define	RFCR_ERROR_BIG_REC			-409
#define	RFCR_ERROR_BURN				-410
#define	RFCR_ERROR_CLEANUP			-411
#define	RFCR_INVALID_DATA_FRAME 	-420
#define	RFCR_INVALID_CARD_PAYLOAD 	-422
#define	RFCR_BUFF_TOO_SMALL			-430	// returned by build packet functions only
#define RFCR_UNKNOWN 				-431
#define	RFCR_ZERO_BYTES_RCVD		-432
#define	RFCR_ERROR_WRITE			-433
#define	RFCR_ERROR_READ				-434
#define	RFCR_ERROR_CMD_NOT_SUPPORTED -435
#define RFCR_ERROR_CMD_FAILED		-436
/** \} */

/**
 * @name NFC error return values
*/
/** \{ */
#define NFC_ERROR_THREAD_CREATE		-451
#define NFC_ERROR_THREAD_RUNING		-452
#define NFC_ERROR_TIMEOUT			-453
#define	NFC_ERROR_COLLISION			-454
#define	NFC_ERROR_NO_RESPONSE		-455
/** \} */

/**
 * @name RTC source
*/
/** \{ */
#define NO_RTC 				0
#define RTC_VIVO_READER 	1
#define RTC_EXTERNAL_SOURCE 2
/** \} */

/**
 * @name Buzzer definitions
*/
/** \{ */
#define BUZZER_ONE_SHORT	1
#define BUZZER_ONE_LONG		2
#define BUZZER_TWO_SHORT	3
#define BUZZER_TWO_LONG		4
#define BUZZER_TREE_SHORT	5
#define BUZZER_TREE_LONG	6
/** \} */

/**
 * @name LED definitions
*/
/** \{ */
#define ALL_LED_OFF			0x00
#define ALL_LED_ON			0xF0
#define FIRST_LED			0x80
#define SECOND_LED			0x40
#define THIRD_LED			0x20
#define FOURT_LED			0x10
/** \} */

/**
 * @name Macros
*/
/** \{ */
#define MX870              	1
#define MX8SERIES          	2
#define MX8SERIES3         	3
#define MXFUSION			4
#define RFCR_TYPE_UNKNOWN  	99

#define POLL_ON_DEMAND 		1
#define AUTO_POLL 			0

#define BURST_MODE_ENABLE 	0x01
#define BURST_MODE_DISABLE 	0x00

#define FAST_UI_DISABLE 	0
#define FAST_UI_ENABLE 		1

#define ENC(c) ((c) ? ((c) & 0x3F) + ' ': '`')
/** \} */

/**
 * @name FWUpdateCallback message types
*/
/** \{ */
#define ERROR_MESSAGE		1
#define INFO_MESSAGE    	2
#define INFO_MESSAGE_ADD    3

#define CARD_PAYLOAD_PACKET 1
#define STATUS_PACKET		2
#define CARD_VISA2_PACKET	3
#define VIVO_NFC_PACKET		4
/** \} */

typedef unsigned char uchar;

//-------------------------------------------------------------
// The RFCR functions will use and maintain the following 
// handle variable.  It is made public for applications who
// need to directly access the device for functionality not
// supported in the RFCR functions.
//-------------------------------------------------------------

/**
 * Get the library version number in "xx.xx.xx" format
 * 
 * @note This is not the FW version
 *
 * @param[out] libVersion Pointer to library version buffer string
 *
 * @return Length of library version string
 *
 */
int RFCRlibVersion( char *libVersion );

/**
 * Get GEN3 RFCR type for MX9xx
 * 
 * @note Required for FA backward compatibility
 *
 * @return GEN3 RFCR type
 *
 */
int RFCRGetType (void);

/**
 * Set GEN3 RFCR indicator
 * 
 * @note Required for FA backward compatibility
 *
 * @param[in] led For LED indocator
 * @param[in] buzz For buzzer indicator
 *
 * @return 
 * @li 0 = Success
 * @li Else check errno
 *
 */
int RFCRSetIndicator (int led, int buzz);

/**
 * Device initialization, open and configure port and handle some basic RFCR configurations (beeper, etc.)
 * 
 * @return 
 * @li Handle to the opened interface with CTLS = Success
 * @li < 0 = Check errno
 *
 */
int RFCRInit(void); 

/**
 * Close device, handle was returned from RFCRInit()
 * 
 * @note Does NOT power off the module
 *
 * @param[in] handle Handle returned from RFCRInit()
 *
 * @return 
 * @li 0 = Success
 * @li Else check errno
 *
 */
int RFCRClose(int handle); 

/**
 * Purge any pending input from the reader
 * 
 */
void RFCRPurge (void);

/**
 * Send IOCTL to serial port driver and check if input
 * 
 * @return 
 * @li Count if data is pending
 * @li < 0 = Check errno
 *
 */
int RFCRInputPending (void);

/**
 * Write the data to serial port
 * 
 * @param[in] paToWrite Buffer containing data
 * @param[in] iSizeToWrite Length of the data
 *
 *
 * @return Number of bytes written successfully
 *
 */
int RFCRRawWrite (char *paToWrite, int iSizeToWrite);

/**
 * Read the data from serial port and encrypt track data
 * 
 * @param[in] strData Buffer containing data
 * @param[in] iSize Length of the data
 * @param[in] iMilSec Timeout in milliseconds
 *
 * @return Number of bytes read successfully
 *
 */
int RFCRRawRead (unsigned char *strData, int iSize, int iMilSec);

//==========================================================================
// Helper functions provided for convenience.
// These APIs can be used to build packets for the RF Contactless Reader,
// read response frames, and interpret responses.
//==========================================================================

/**
 * Card payload packet structure
 */
typedef struct {
    char status;
    char cardType;
    short trk1Start;
    short trk1Length;
    short trk2Start;
    short trk2Length;
	short trk1ExtraStart;
    short trk1ExtraLength;
	short trk2ExtraStart;
    short trk2ExtraLength;
	char *ptrBuff;
    unsigned char crc1;
    unsigned char crc2;
	char packetType;
} CardPayload;

/**
 * Calculate the CRC and adds at specified offset of the buffer
 * 
 * @param[in] buff Buffer for which CRC to be calculated 
 * @param[in] offset Offset at which CRC to be stored, this also serves as the size for the CRC calculation
 * 
 */
void RFCRAddCRC(char* buff, int offset);

/**
 * Check the CRC on the buffer
 * 
 * @param[in] buff Buffer to be verified
 * @param[in] size Size of the buffer
 * @param[in] calc Calculated CRC value
 * 
 * @return 
 * @li 1 = Success
 * @li 0 = Check errno
 * 
 */
int RFCRCheckCRC (char* buff, short size, unsigned short* calc); 

/**
 * Receive the ACK frame from RFCR
 * 
 * @param[out] buff Buffer containing ACK frame
 * 
 * @return Number of bytes received successfully
 * 
 */
int RFCRReceiveACKFrame (char* buff);
						   
/**
 * Read the data from serial port and check if it's valid packet
 * 
 * @param[out] buff Buffer containing ACK frame
 * @param[in] sz Size of the buffer
 * 
 * @return Number of bytes received successfully
 * 
 */
int RFCRReceiveDataFrame (char* buff, int sz);

/**
 * Read the data and check if it's card payload data
 * 
 * @param[out] buff Buffer containing card data
 * @param[in] max Maximum size of the buffer
 * 
 * @return 
 * @li Number of bytes received successfully
 * @li RFCR_INVALID_CARD_PAYLOAD = Failure
 * 
 */
int RFCRGetCardPayload(char* buff, int max);

/**
 * Parse the payload packet
 * 
 * @param[in] payLoad Pointer to the CardPayload structure
 * @param[out] buff Buffer containing input payload data
 * @param[in] size Size of the buffer
 * 
 * @return The first byte of buffer
 * 
 */
int RFCRParseCardPayload(CardPayload* payLoad, char* buff, int size);

/**
 * Get card name based on the id
 * 
 * @param[in] id For which card name has to be mapped
 * 
 * @return String containing card name
 * 
 */
char* RFCRGetCardTypeName (int id);

/**
 * Get card description based on the specified status
 * 
 * @param[in] status Status for which description has to be mapped
 * 
 * @return String containing description
 * 
 */
char* RFCRGetCardStatusDescription(int status);

/**
 * Determine if VivOtech module is alive
 * 
 * @return 
 * @li 0 = Success
 * @li Else check errno
 * 
 */
int RFCRPing(void);

/**
 * Handle "Version" menu option and shell "ver" command
 * 
 * @param[out] version Buffer to hold the version string returned by reader
 * 
 * @return 
 * @li 0 = Success
 * @li Else check errno
 * 
 */
int RFCRGetVersion(char *version);

/**
 * Reset the device
 * 
 * @param[in] onOffPulse ???
 * 
 * @return 
 * @li 0 = Success
 * @li Else check errno
 * 
 */
int RFCRReset(int onOffPulse);

/**
 * Handle menu command to set antenna and antenna shell cmd
 * 
 * @param[in] onOff Antenna control
 * 
 * @return 
 * @li 0 = Success
 * @li Else check errno
 * 
 */
int RFCRSetAntenna(int onOff);

/**
 * Set the Polling mode of Reader
 * 
 * @param[in] mode Polling mode
 * 
 * @return ACK or NAK response from reader
 * 
 */
int RFCRSetPollMode(int mode);

/**
 * Read the payload data from RFCR
 * 
 * @param[out] buff Buffer containing returned data
 * @param[in] max Maximum size of the buffer
 * 
 * @return 
 * @li Number of bytes received successfully
 * @li RFCR_INVALID_CARD_PAYLOAD = Failure
 * 
 */
int RFCRGetTransactionResult(char* buff, int max);

/**
 * Set Indicator configuration
 * 
 * @param[in] message For message indicator
 * @param[in] buzzer For buzzer indicator
 * @param[in] ledno Led number
 * @param[in] ledstatus Led status
 * 
 * @return 
 * @li 0 = Success
 * @li Else check errno
 * 
 */
int RFCRSetMessage(int message, int buzzer, int ledno, int ledstatus);

/**
 * Update the balance
 * 
 * @param[in] status Status of the update balance command
 * 
 * @return 
 * @li Number of bytes received successfully
 * @li RFCR_INVALID_CARD_PAYLOAD = Failure
 * 
 */
int RFCRUpdateBalance(unsigned char status);

/**
 * Send activate transaction command to reader
 * 
 * @param[in] data Buffer containing command data
 * @param[in] length Size of the command data
 * 
 * @return Status of the activate transaction command
 * 
 */
int RFCRActivateTransaction(char *data, int length);

/**
 * Send activate transaction command to reader
 * 
 * @param[in] data Buffer containing command data
 * @param[in] length Size of the command data
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of the buffer
 * 
 * @return Status of the activate transaction command
 * 
 */
int RFCRActivateTransactionGetData(char *data, int length, char *rspData, int rspMax);

/**
 * Send cancel transaction command to reader
 * 
 * @return Status of the cancel transaction command
 * 
 */
int RFCRCancelTransaction(void);

/**
 * Get the full track data from reader
 * 
 * @param[out] data Buffer containing returned data
 * @param[in] length Size of the byffer
 * 
 * @return Number of bytes received successfully
 * 
 */
int RFCRGetFullTrackData(char *data, int length);

/**
 * Set the EMV parameters
 * 
 * @param[in] data Buffer containing command data
 * @param[in] dataLen Size of the command data
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRSetEMVParameters(char *data,int dataLen);

/**
 * Get the EMV parameters
 * 
 * @param[out] Buffer containing returned data
 * 
 * @return Number of bytes in data returned from reader
 * 
 */
int RFCRGetEMVParameters (char *data);

/**
 * Get the EMV parameters from reader
 * 
 * @param[in] count Number of variable arguments
 * @param[out] data Buffer containing command data
 * @param[in] max Maximum size of the buffer (optional)
 * 
 * @return Number of bytes in data returned from reader
 * 
 */
int RFCRGetEMVParametersVa (int count, char *data, ...);

/**
 * Set the Burst mode
 * 
 * @param[in] burstMode Burst mode
 * 
 */
int RFCRSetBurstMode(int burstMode);

/**
 * Get the Burst mode
 * 
 * @return Burst Mode
 * 
 */
int RFCRGetBurstMode (void);

/*============================================================================ 
    NOTE: The arguments in the function prototypes bellow are:
     
    @param - cmdData: 	Buffer containing command data (terminal->RFCR).
    @param - cmdLen: 	Length of command data (terminal->RFCR). 
    @param - rspData: 	Buffer containing response data (RFCR->terminal).
    @param - rspMax: 	Maximum size of the response buffer (RFCR->terminal).    
    @return - int: 		Number of bytes in response packet from RFCR. 

==============================================================================*/

/**
 * Set the Pass-Through mode of Reader
 * 
 * @param[in] mode Mode to set
 * 
 * @return 
 * @li 0 = Success
 * @li Else check errno
 * 
 */
int RFCRSetPassThroughMode (int mode);

/**
 * Poll for token from reader
 * 
 * @param[in] sec Timeout in seconds
 * @param[in] msec Timeout in milliseconds
 * @param[out] Buffer containing returned data
 * @param[in] rspMax Maximum size of the buffer
 * 
 * @return Mumber of bytes in data returned from reader
 * 
 */
int RFCRPollForToken (int sec, int msec, char *rspData, int rspMax);

/**
 * Control LED
 * 
 * @param[in] led LED to controll (0-LED0, 1-LED1, 2-LED2, 3-all three LED)
 * @param[in] mode LED mode to set (0-off, 1-on)
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRLedControl (int led, int mode);

/**
 * Control buzzer
 * 
 * @param[in] mode Mode to set (1-N short beeps, 2-single long beep)
 * @param[in] beeps Number of beeps (1) or duration (2)
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRBuzzerControl (int mode, int beeps);

/**
 * Send application-level APDU
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in response packet from reader
 * 
 */
int RFCRIsoApduExchange (char *cmdData, int cmdLen, char *rspData, int rspMax);

/**
 * PCD Single Command Exchange (Protocol-2)
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in response packet from reader
 * 
 */
int RFCRPcdSingleExchange (char *cmdData, int cmdLen, char *rspData, int rspMax);

/**
 * Get PCD and PICC from reader
 * 
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in response packet from reader
 * 
 */
int RFCRGetPcdPiccParam (char *rspData, int rspMax);

/**
 * Send Mifare authenticate block command
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRMifareAuthenticateBlock (char *cmdData, int cmdLen);

/**
 * Send Mifare read block command
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in response packet from reader
 * 
 */
int RFCRMifareReadBlocks (char *cmdData, int cmdLen, char *rspData, int rspMax);

/**
 * Write Mifare blocks command
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRMifareWriteBlocks (char *cmdData, int cmdLen);

/**
 * Send Mifare ePurse command
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRMifarePurseCommand (char *cmdData, int cmdLen);

/**
 *  Send High Level Halt command
 * 
 * @param[in] halt Halt (01h for Type-A card, 02h for Type-B card)
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRHighLevelHaltCommand (char halt);

/**
 * Get the RTC source from Reader
 * 
 * @return RTC source from Reader
 * 
 */
int RFCRGetRTCSource (void);

/**
 * Store LCD message
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRStoreLCDMessage (char *cmdData, int cmdLen);

/**
 * Get LCD message
 * 
 * @param[in] msgId Message ID
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in response from reader
 * 
 */
int RFCRGetLCDMessage (int msgId, char *rspData, int rspMax);

/**
 * Get Configurable AID
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in response packet from reader
 * 
 */
int RFCRGetConfigurableAID (char *cmdData, int cmdLen, char *rspData, int rspMax);

/**
 * Get All AIDs
 * 
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in response from reader
 * 
 */
int RFCRGetAllAIDs (char *rspData, int rspMax);

/**
 * Get Configurable Group
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in response packet from reader
 * 
 */
int RFCRGetConfigurableGroup (char *cmdData, int cmdLen, char *rspData, int rspMax);

/**
 * Get All Groups
 * 
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in response packet from reader
 * 
 */
int RFCRGetAllGroups (char *rspData, int rspMax);

/**
 * Set Configurable AID
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRSetConfigurableAID (char *cmdData, int cmdLen);

/**
 * Set Configurable Group
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRSetConfigurableGroup (char *cmdData, int cmdLen);

/**
 * Delete Configurable AID
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRSDeleteConfigurableAID (char *cmdData, int cmdLen);

/**
 * Delete configurable group
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRDeleteConfigurableGroup (char *cmdData, int cmdLen);

/**
 * Get CA Public key
 * 
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in response packet from reader
 * 
 */
int RFCRGetCAPublicKey (char *rspData, int rspMax);

/**
 * Get serial number
 * 
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in response packet from reader
 * 
 */
int RFCRGetSerialNumber (char *rspData, int rspMax);

/**
 * Set serial number
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRSetSerialNumber (char *cmdData, int cmdLen);

/**
 * Flush Track data
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRFlushTrackData (void);

/**
 * Set CA Public Key
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRSetCAPublicKey (char *data, int dataLen);

/**
 * Delete CA Public key
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRDeleteCAPublicKey (char *data, int dataLen);

/**
 * Delete all CA Public keys
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRDeleteAllCAPublicKeys (void);

/**
 * Set RTC time
 * 
 * @param[in] hour Hour
 * @param[in] min Minutes
 * 
 * @return 
 * @li 0 = Success
 * @li Else check errno
 * 
 */
int RFCRSetRTCTime (int hour, int min);

/**
 * Get RTC time
 * 
 * @return Time from reader
 * 
 */
int RFCRGetRTCTime (void);

/**
 * Set RTC date
 * 
 * @param[in] year Year
 * @param[in] month Month
 * @param[in] day Day
 * 
 * @return 
 * @li 0 = Success
 * @li Else check errno
 * 
 */
int RFCRSetRTCDate (int year, int month, int day);

/**
 * Get RTC date
 * 
 * @return Date from reader
 * 
 */
int RFCRGetRTCDate (void);

/**
 * Send Baudrate command packet
 * 
 * @param[in] baud Baud rate to be set
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRSetBaudRate (int baud);

/**
 * Copy data received from card to buffer
 * 
 * @param[out] buff Destination buffer
 * @param[in] size Size of buffer
 * 
 */
void RFCRGetResponseData (char *buff, int size);

// Gen3 specific commands (not compatible with ViVOpay protocol)
/**
 * Get Additional AID Parameters
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in response packet from reader
 * 
 */
int RFCRGetAdditionalAIDParams (char *cmdData, int cmdLen, char *rspData, int rspMax);

/**
 * Get Revocation Parameter
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in response packet from reader
 * 
 */
int RFCRGetRevocationParam (char *cmdData, int cmdLen, char *rspData, int rspMax);

/**
 *  Get All Additional AID Parameters
 * 
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in data from reader
 * 
 */
int RFCRGetAllAdditionalAIDParams (char *rspData, int rspMax);

/**
 * Get All Revocation Parameters
 * 
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in data from reader
 * 
 */
int RFCRGetAllRevocationParams (char *rspData, int rspMax);

/**
 * Get All Exception Parameters
 * 
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in data from reader
 * 
 */
int RFCRGetAllExceptionParams (char *rspData, int rspMax);

/**
 * Get Additional Reader Parameters
 * 
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in data from reader
 * 
 */
int RFCRGetAdditionalReaderParams (char *rspData, int rspMax);

/**
 * Get VFI Version
 * 
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in data from reader
 * 
 */
int RFCRGetVFIVersion (char *rspData, int rspMax);

/**
 * Get Type Approval Versions
 * 
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in data from reader
 * 
 */
int RFCRGetTypeApprovalVersions (char *rspData, int rspMax);

/**
 * Set Exist Additional AID Parameters
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRSetExistAdditionalAIDParams (char *cmdData, int cmdLen);

/**
 * Set New Additional AID Parameters
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRSetNewAdditionalAIDParams (char *cmdData, int cmdLen);

/**
 * Delete Additional AID Parameters
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRDeleteAdditionalAIDParams (char *cmdData, int cmdLen);

/**
 * Set Exist Revocation Parameter
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRSetExistRevocationParam (char *cmdData, int cmdLen);

/**
 * Set New Revocation Parameter
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRSetNewRevocationParam (char *cmdData, int cmdLen);

/**
 * Delete Revocation Parameter
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRDeleteRevocationParam (char *cmdData, int cmdLen);

/**
 * Set New Exception Parameter
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRSetNewExceptionParam (char *cmdData, int cmdLen);

/**
 * Delete Exception Parameter
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRDeleteExceptionParam (char *cmdData, int cmdLen);

/**
 * Set Additional Reader Parameter
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRSetAdditionalReaderParam (char *cmdData, int cmdLen);

/**
 * Restore Default Configuration
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRRestoreDefaultConfig (char *cmdData, int cmdLen);

/**
 * Get Firmware Full Version
 * 
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return 
 * @li Number of bytes read successfully
 * @li Else check errno
 * 
 */
int RFCRGetFwFullVersion (char *rspData, int rspMax);

// VFI CTLS ViVOpay Encrypted commands
/**
 * Encryption Init Communication command
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in response packet from reader
 * 
 */
int RFCREncryptInit (char *cmdData, int cmdLen, char *rspData, int rspMax);

/**
 * Encryption Mutual Authentication command
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRMutualAuthentication (char *cmdData, int cmdLen);

/**
 * Encryption Generate Key command
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * 
 * @return ACK or NAK from reader
 * 
 */
int RFCRGenerateKey (char *cmdData, int cmdLen);

/**
 * Encryption command
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in response packet from reader
 * 
 */
int RFCREncryptionCommand (char *cmdData, int cmdLen, char *rspData, int rspMax);

// NFC Embedded Application commands
/**
 * Send Embedded App Ping command
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in response packet from reader
 * 
 */
int NFCEmbAppPing (char *cmdData, int cmdLen, char *rspData, int rspMax);

/**
 * Send Embedded App Config command
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in response packet from reader
 * 
 */
int NFCEmbAppConfig (char *cmdData, int cmdLen, char *rspData, int rspMax);

/**
 * Send Embedded App Clear Config command
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in response packet from reader
 * 
 */
int NFCEmbAppClearConfig (char *cmdData, int cmdLen, char *rspData, int rspMax);

/**
 * Send Embedded App Abort command
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in response packet from reader
 * 
 */
int NFCEmbAppCancel (char *cmdData, int cmdLen, char *rspData, int rspMax);

/**
 * Send Embedded App Get Version command
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in response packet from reader
 * 
 */
int NFCEmbAppGetVersion (char *cmdData, int cmdLen, char *rspData, int rspMax);

typedef int HostAppCallback (int iStatus);

/**
 * Send Embedded App Discovery command
 * 
 * @param[in] options Options flag
 * @param[in] timeout Timeout
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * @param[in] pHostCallback Host callback
 * 
 * @return Number of bytes in response packet from reader
 * 
 */
int NFCEmbAppTechDiscovery (int options, int timeout, char *cmdData, int cmdLen, char *rspData, int rspMax, HostAppCallback *pHostCallback);

/**
 * Get the discovery thread running status
 * 
 * @return 
 * @li 1 = Thread is running
 * @li 0 = Thread is not running
 * 
 */
int NFCDiscoveryThreadRunning (void);

/**
 * Send Embedded App Get CD Version command
 * 
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in response packet from reader
 * 
 */
int NFCEmbAppGetCDVersion (char *rspData, int rspMax);

/**
 * Send Embedded App Get Wallets Data command
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in response packet from reader
 * 
 */
int NFCEmbAppGetWalletsData (char *cmdData, int cmdLen, char *rspData, int rspMax);

/**
 * Send Embedded App FileManager command
 * 
 * @param[in] cmdData Buffer containing command data
 * @param[in] cmdLen Length of command data
 * @param[out] rspData Buffer containing returned data
 * @param[in] rspMax Maximum size of buffer
 * 
 * @return Number of bytes in response packet from reader
 * 
 */
int NFCEmbAppFileManager (char *cmdData, int cmdLen, char *rspData, int rspMax);

// VSP encryption API
/**
 * Encrypt the card track data
 * 
 * @param[in] payLoad Pointer to the CardPayload structure
 * @param[in] buff Buffer containing input payload data
 * @param[in] size Size of the buffer
 * 
 * @return First byte of buffer
 * 
 */
int RFCREncryptCardPayload (CardPayload* payLoad, char* buff, int size);

// CTLS antenna module handlers (four LEDs and buzzer)
/**
 * Control CTLS LEDs
 * 
 * @param[in] leds Each bit corresponds to one CTLS LED
 * 
 */
void RFCRLedHandler (unsigned char leds);

/**
 * Control CTLS buzzer
 * 
 * @param[in] sound Which sound pattern to play
 * 
 */
void RFCRBuzzerHandler (int sound);

// Host App UI callback prototypes
typedef void HostTextCallback (char *text);
typedef void HostLEDCallback (char *leds);
typedef void HostBuzzCallback (int freq, int pause, int dur, int times);

/**
 * Define host callback information
 */
typedef struct
{
	HostTextCallback *pTextCallback;
	HostLEDCallback  *pLEDCallback;
	HostBuzzCallback *pBuzzCallback;
} strHostCallbacks;

/**
 * Set Host App UI callbacks
 * 
 * @param[in] pHostCallback Pointer to structure to hold callback pointers
 * 
 */
void RFCRHostSetCallbacks (strHostCallbacks *pHostCallback);

/**
 * Function to get who controls buzzer and four hard LEDs
 * 
 * @return 
 * @li 0 = CTLS controls LEDs and buzzer
 * @li 1 = CTLS controls LEDs, APP controls buzzer (default)
 * @li 2 = APP controls LEDs, CTLS controls buzzer
 * @li 3 = APP controls LEDs and buzzer
 * 
 */
int RFCRgetRfcrhwValue (void);

/**
 * Function to set who controls buzzer and four hard LEDs
 * 
 * @param[in] val Who controls buzzer and four hard LEDs
 * 
 * @return 
 * @li iRfcrHw = Success
 * @li -1 = Failure, check errno
 * 
 */
int RFCRsetRfcrhwValue (int val);

/**
 * Set Fast UI mode (1-to enable, 0-to disable)
 * 
 * @param[in] mode UI mode
 * 
 * @return 
 * @li 0 = Success
 * @li -1 = Error, check errno
 * 
 */
int RFCRsetFastUImode (int mode);

/**
 * Get Fast UI mode
 * 
 * @return 
 * @li 1 = Enabled
 * @li 0 = Disabled
 * 
 */
int RFCRgetFastUImode (void);

/**
 * Get antenna status
 * 
 * @return 
 * @li 0 = OFF
 * @li 1 = ON
 * 
 */
int RFCRAntennaStatus (void);

/**
 * Set TTQ parameter
 * 
 * @param[in] mode Mode to set (1-enable Visa 2, 0-disable Visa 2)
 * 
 * @return 
 * @li 0 = Success
 * @li Else check errno
 * 
 */
int RFCRsetTTQ (int mode);

/**
 * Get VCL encryption
 * 
 * @return 
 * @li 0 = Disabled
 * @li 1 = Enabled
 * 
 */
int RFCRgetVCLenable (void);

/**
 * Set VCL encryption
 * 
 * @param[in] iEnable Mode to set (1-to enable, 0-to disable)
 * @param[in] 
 * 
 * @return 
 * @li 0 = Success
 * @li < 0 = Check errno
 * 
 */
int RFCRsetVCLenable (int iEnable);

/**
 * Reset the VCL encryption module and return status
 * 
 * @return 
 * @li 0 = Success
 * @li ERR_ERR_ENCRYPT = Failure
 * 
 */
int RFCRresetVSPmodule (void);

/**
 * Get the the clear PAN for the given encrypted PAN (null terminated)
 * 
 * @param[in] pData Encrypted PAN (null terminated)
 * 
 * @return 
 * @li 0 = Success
 * @li ERR_ERR_ENCRYPT or MSR_ERR_ENOBUFS = Failure
 * 
 */
int RFCRgetVCLclearPan (char *pData);

#ifdef __cplusplus
}
#endif
#endif
/// @} */
/// @} */
