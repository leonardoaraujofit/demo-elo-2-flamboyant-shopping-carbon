#ifndef  _SVCSEC_H_
#define  _SVCSEC_H_
#include "pinentry.h"
#include <ade.h>
#include <vf_crypto.h>
#ifdef __cplusplus
extern "C" {
#endif
#include <stdint.h>

/*
;|===========================================================================+
;| NOTE: | ERROR CODES
;|===========================================================================+
*/
#define LIBERROR                    ((int)0xC0000000)

/* Key Management & Pin Entry */
#define E_LKM                       (LIBERROR+0x00060000)
#define E_KM_NO_KEY_LOADED          (E_LKM+0x0400)
#define E_KM_NO_ALGO_SELECTED       (E_KM_NO_KEY_LOADED)
#define E_KM_NO_DUKPT_INIT          (E_KM_NO_KEY_LOADED)
#define E_KM_KEY_INTEGRITY_ERROR    (E_LKM+0x0800)
#define E_KM_FEATURE_DISABLED       (E_LKM+0x0C00)
#define E_KM_OUT_OF_RANGE           (E_LKM+0x1000)
#define E_KM_INVALID_PARAMETER      (E_KM_OUT_OF_RANGE)
#define E_KM_BAD_SEQUENCE           (E_LKM+0x1400)
#define E_KM_DUKPT_EOL              (E_LKM+0x1800)
#define E_KM_BUSY                   (E_LKM+0x1C00)
#define E_KM_HW_PB                  (E_LKM+0x2000)
#define E_KM_ACCESS_DENIED          (E_LKM+0x2400)
#define E_KM_SYSTEM_ERROR           (E_LKM+0x8000)

/* VeriShield Security Script */
#define E_LVS                       (LIBERROR+0x00070000)
#define E_VS_SCRIPT_NOT_LOADED      (E_LVS+0x0400)
#define E_VS_MACRO_NOT_EXIST        (E_LVS+0x0800)
#define E_VS_MACRO_EXECUTION        (E_LVS+0x0C00)
#define E_VS_BAD_LENGTH             (E_LVS+0x1000)
#define E_VS_BAD_CHAINING           (E_LVS+0x1400)
#define E_VS_LOADING                (E_LVS+0x1800)
#define E_VS_ERR_RET                (E_LVS+0x2000)
#define E_VS_SYSTEM_ERROR           (E_LVS+0x8000)

/* Crypto Algorithms */
#define E_LCA                       (LIBERROR+0x00090000)
#define E_LCA_KEY_PARITY_ERROR      (E_LCA+0x0400)
#define E_LCA_MEMORY_ERROR          (E_LCA+0x0800)
#define E_LCA_WRONG_SIGNATURE       (E_LCA+0x0C00)
#define E_LCA_INVALID_PARAMETER     (E_LCA+0x1000)

/* RKL */
#define E_RKL                           (LIBERROR+0x000A0000)
#define E_RKL_MEMORY_ERROR              (E_RKL+0x0400)
#define E_RKL_KEY_FILE_READ_ERROR       (E_RKL+0x0800)
#define E_RKL_NO_KEYS                   (E_RKL+0x0C00)
#define E_RKL_INVALID_KEY               (E_RKL+0x1000)
#define E_RKL_INVALID_TIMESTAMP         (E_RKL+0x1400)
#define E_RKL_INVALID_KEY_BLOCK_HEADER  (E_RKL+0x1800)
#define E_RKL_STORE_KEY_ERROR           (E_RKL+0x1C00)
#define E_RKL_KEY_FILE_PERMS            (E_RKL+0x2000)
#define E_RKL_KEY_FILE_OWNER            (E_RKL+0x2400)

/*
;|===========================================================================+
;| NOTE: | DEFINITION
;|===========================================================================+
*/
/* iPS_LoadMasterClearKey() */
/* iPS_LoadMasterEncKey() */
#define LD_VSS0         0
#define LD_VSS1         1
#define LD_VSS2         2
#define LD_VSS3         3
#define LD_VSS4         4
#define LD_VSS5         5
#define LD_VSS6         6
#define LD_VSS7         7
#define LD_VSS8         8
#define LD_VSS9         9
#define LD_VSS10       10
#define LD_VSS11       11
#define LD_VSS12       12
#define LD_VSS13       13
#define LD_VSS14       14
#define LD_VSS15       15
#define LD_VSS16       16
#define LD_VSS17       17
#define LD_VSS18       18
#define LD_VSS19       19
#define LD_VSS20       20
#define LD_VSS21       21
#define LD_VSS22       22
#define LD_VSS23       23
#define LD_VSS24       24
#define LD_VSS25       25
#define LD_VSS26       26
#define LD_VSS27       27
#define LD_VSS28       28
#define LD_VSS29       29
#define LD_VSS30       30
#define LD_VSS31       31
#define LD_VSS32       32
#define LD_VSS33       33
#define LD_VSS34       34
#define LD_VSS35       35
#define LD_VSS36       36
#define LD_VSS37       37
#define LD_VSS38       38
#define LD_VSS39       39
#define LD_VSS40       40
#define LD_VSS41       41
#define LD_VSS42       42
#define LD_VSS43       43
#define LD_VSS44       44
#define LD_VSS45       45
#define LD_VSS46       46
#define LD_VSS47       47
#define LD_VSS48       48
#define LD_VSS49       49
#define LD_VSS50       50
#define LD_VSS51       51
#define LD_VSS52       52
#define LD_VSS53       53
#define LD_VSS54       54
#define LD_VSS55       55
#define LD_VSS56       56
#define LD_VSS57       57
#define LD_VSS58       58
#define LD_VSS59       59
#define LD_VSS60       60
#define LD_VSS61       61
#define LD_VSS62       62
#define LD_VSS63       63

/* iPS_DeleteKeys() */
#define DEL_SYSTEM      (unsigned long)0x00000001  
#define DEL_DUKPT0      (unsigned long)0x00000002
#define DEL_DUKPT1      (unsigned long)0x00000004
#define DEL_DUKPT2      (unsigned long)0x00000008
#define DEL_VSS0        (unsigned long)0x00000020
#define DEL_VSS1        (unsigned long)0x00000040
#define DEL_VSS2        (unsigned long)0x00000080
#define DEL_VSS3        (unsigned long)0x00000100
#define DEL_VSS4        (unsigned long)0x00000200
#define DEL_VSS5        (unsigned long)0x00000400
#define DEL_VSS6        (unsigned long)0x00000800
#define DEL_VSS7        (unsigned long)0x00001000
#define DEL_IPP         (unsigned long)0x00008000
#define DEL_ALL         (unsigned long)0xFFFFFFFF

/* iPS_DeleteKeys2() */
#define DEL_VSS8        (unsigned long)0x00000001
#define DEL_VSS9        (unsigned long)0x00000002
#define DEL_VSS10       (unsigned long)0x00000004
#define DEL_VSS11       (unsigned long)0x00000008
#define DEL_VSS12       (unsigned long)0x00000010
#define DEL_VSS13       (unsigned long)0x00000020
#define DEL_VSS14       (unsigned long)0x00000040
#define DEL_VSS15       (unsigned long)0x00000080
#define DEL_VSS16       (unsigned long)0x00000100
#define DEL_VSS17       (unsigned long)0x00000200
#define DEL_VSS18       (unsigned long)0x00000400
#define DEL_VSS19       (unsigned long)0x00000800
#define DEL_VSS20       (unsigned long)0x00001000
#define DEL_VSS21       (unsigned long)0x00002000
#define DEL_VSS22       (unsigned long)0x00004000
#define DEL_VSS23       (unsigned long)0x00008000
#define DEL_VSS24       (unsigned long)0x00010000
#define DEL_VSS25       (unsigned long)0x00020000
#define DEL_VSS26       (unsigned long)0x00040000
#define DEL_VSS27       (unsigned long)0x00080000
#define DEL_VSS28       (unsigned long)0x00100000
#define DEL_VSS29       (unsigned long)0x00200000
#define DEL_VSS30       (unsigned long)0x00400000
#define DEL_VSS31       (unsigned long)0x00800000
#define DEL_VSS32       (unsigned long)0x01000000
#define DEL_VSS33       (unsigned long)0x02000000
#define DEL_VSS34       (unsigned long)0x04000000
#define DEL_VSS35       (unsigned long)0x08000000
#define DEL_VSS36       (unsigned long)0x10000000
#define DEL_VSS37       (unsigned long)0x20000000
#define DEL_VSS38       (unsigned long)0x40000000
#define DEL_VSS39       (unsigned long)0x80000000

/* iPS_DeleteKeys3() */
#define DEL_VSS40       (unsigned long)0x00000001
#define DEL_VSS41       (unsigned long)0x00000002
#define DEL_VSS42       (unsigned long)0x00000004
#define DEL_VSS43       (unsigned long)0x00000008
#define DEL_VSS44       (unsigned long)0x00000010
#define DEL_VSS45       (unsigned long)0x00000020
#define DEL_VSS46       (unsigned long)0x00000040
#define DEL_VSS47       (unsigned long)0x00000080
#define DEL_VSS48       (unsigned long)0x00000100
#define DEL_VSS49       (unsigned long)0x00000200
#define DEL_VSS50       (unsigned long)0x00000400
#define DEL_VSS51       (unsigned long)0x00000800
#define DEL_VSS52       (unsigned long)0x00001000
#define DEL_VSS53       (unsigned long)0x00002000
#define DEL_VSS54       (unsigned long)0x00004000
#define DEL_VSS55       (unsigned long)0x00008000
#define DEL_VSS56       (unsigned long)0x00010000
#define DEL_VSS57       (unsigned long)0x00020000
#define DEL_VSS58       (unsigned long)0x00040000
#define DEL_VSS59       (unsigned long)0x00080000
#define DEL_VSS60       (unsigned long)0x00100000
#define DEL_VSS61       (unsigned long)0x00200000
#define DEL_VSS62       (unsigned long)0x00400000
#define DEL_VSS63       (unsigned long)0x00800000

/* DES() */
#define DESX1KE     0x02    /* DEAX encryption with single-length key */
#define DESX1KD     0x03    /* DEAX decryption with single-length key */
#define DESX2KE     0x04    /* DEAX encryption with double-length key */
#define DESX2KD     0x05    /* DEAX decryption with double-length key */
#define DESX3KE     0x06    /* DEAX encryption with triple-length key */
#define DESX3KD     0x07    /* DEAX decryption with triple-length key */
#define DESE        0x08    /* DEA encryption with single-length key  */
#define DESD        0x09    /* DEA decryption with single-length key  */
#define TDES2KE     0x0C    /* TDEA encryption with double-length key */
#define TDES2KD     0x0D    /* TDEA decryption with double-length key */
#define TDES3KE     0x0E    /* TDEA encryption with triple-length key */
#define TDES3KD     0x0F    /* TDEA decryption with triple-length key */

/* AES() */
#define AES128E     0x04    /* AES encryption using a 128-bit key */
#define AES128D     0x05    /* AES decryption using a 128-bit key */
#define AES192E     0x06    /* AES encryption using a 192-bit key */
#define AES192D     0x07    /* AES decryption using a 192-bit key */
#define AES256E     0x08    /* AES encryption using a 256-bit key */
#define AES256D     0x09    /* AES decryption using a 256-bit key */

/* SHA1() */
#define SHA1INIT    (1<<0)   
#define SHA1TERM    (1<<1)
#define SHA1BUFF    (0)
#define SHA1ALL     (SHA1INIT | SHA1TERM)

/* Touchscreen */
#define MAX_NUM_HOTSPOTS    20
/*
;|===========================================================================+
;| NOTE: | TYPEDEF
;|===========================================================================+
*/

struct touch_hotspot_info
{
   unsigned short  x1;         /* starting x of hotspot */
   unsigned short  y1;         /* starting y of hotspot */
   unsigned short  x2;         /* ending x of hotspot */
   unsigned short  y2;         /* ending y of hotspot */
   char            result;     /* ASCII value to return */
   char            options;    /* RESERVED */
};

struct touch_hs_s
{
   short   num_hotspots;       /* the number of active hotspots */
   struct  touch_hotspot_info  touch_spot[MAX_NUM_HOTSPOTS];
};

typedef enum  /* enumeration for RKL file status */
{
    NO_KEYS_RKL,     /* Both files DO NOT exist */
    PUBLIC_KEY_RKL,  /* Only /root/rkl_keys/rkl_cert.crt exist */
    PRIVATE_KEY_RKL, /* Only /root/rkl_keys/rkl_priv_key.der exist */
    BOTH_KEYS_RKL    /* Both files exist */
}rkl_key_status_t;

/*
;|===========================================================================+
;| NOTE: | PUBLIC FUNCTION PROTOTYPES
;|===========================================================================+
*/
/* Key Loading */
int iPS_DeleteKeys (unsigned long ulKeyType);
int iPS_DeleteKeys2 (unsigned long ulKeyType);
int iPS_DeleteKeys3 (unsigned long ulKeyType);
int iPS_LoadSysClearKey (unsigned char ucKeyID, unsigned char *pucINKeyValue);
int iPS_LoadSysEncKey (unsigned char ucKeyID, unsigned char *pucINKeyValue);
int iPS_LoadMasterClearKey (unsigned char ucKeySetID, unsigned char ucKeyID,
                            unsigned char *pucINKeyValue);
int iPS_LoadMasterEncKey (unsigned char ucKeySetID, unsigned char ucKeyID,
                          unsigned char *pucINKeyValue);
int iPS_CheckMasterKey (unsigned char ucKeySetID, unsigned char ucKeyID,
                        unsigned char *pucINKVC);
/* PIN entry */
int iPS_SetPINParameter (PINPARAMETER *psKeypadSetup);
int iPS_SelectPINAlgo (unsigned char ucPinFormat);
int iPS_RequestPINEntry (unsigned char ucPANDataSize, 
                         unsigned char *pucINPANData);
int iPS_GetPINResponse (int *piStatus, PINRESULT *pOUTData);
int iPS_CancelPIN (void);
int iPS_SetPinBypassKey (unsigned char ucPinBypassKey);

/* VeriShield Security Script */
int iPS_GetScriptStatus (unsigned char ucScriptNumber, 
                         unsigned char *pucINName);
int iPS_InstallScript (void);
int iPS_UninstallScript (unsigned char ucScriptNumber);
int iPS_ExecuteScript (unsigned char ucScriptNumber, unsigned char ucMacroID,
                       unsigned short usINDataSize, unsigned char *pucINData,
                       unsigned short usMaximumOUTDataSize,
                       unsigned short *pusOUTDataSize, 
                       unsigned char *pucOUTData);
char* pcPS_GetVSSVersion (void);


/**********************************************/
/* Function definitions for ATOS Posedion ZVT */
/**********************************************/

/**
 * Initialise system for loading Poseidon keyloading keys
 * @return 0 on success, non-zero on failure
 */
int iPS_Poseidon_InitKeyLoading(void);

/**
 * Initialise system for loading Poseidon usage keys
 * @return 0 on success, non-zero on failure
 */
int iPS_Poseidon_InitKeyUsage(void);

/**
 * Call a Poseidon operation
 * @return 0 on success
 *
 * @param[in] ucOperation				- macro name
 * @param[in] usINDataSize				- i/p data length
 * @param[in] pucINData					- ptr to i/p data
 * @param[in] usMaximumOUTDataSize		- max o/p data size
 * @param[out] pusOUTDataSize			- o/p data size
 * @param[out] pucOUTData				- o/p data
 *
 * @return Appropriate status
 */
int iPS_PoseidonExec(unsigned char ucOperation,
					 unsigned short usINDataSize,
					 unsigned char *pucINData,
					 unsigned short usMaximumOUTDataSize,
					 unsigned short *pusOUTDataSize,
					 unsigned char *pucOUTData);

/**
 * Delete Poseidon keyloading keys
 * @return 0 on success , non-zero on failure
 */
int iPS_Poseidon_DeleteLoadingKeys(void);

/**
 * Delete Poseidon usage keys
 * @return 0 on success, non-zero on failure
 */
int iPS_Poseidon_DeleteUsageKeys(void);


/* Miscellaneous Crypto Functions */
int vfi_SHA1 (int action, unsigned char *input_buffer, 
          unsigned long nb, unsigned char *sha20);
int vfi_SHA256 (int action, unsigned char *input_buffer,
          unsigned long nb, unsigned char *sha32);
int rsa_calc(unsigned char *msg, unsigned char *mod, int len, int exp, unsigned char *result);
int DES(unsigned char ucDeaOption, unsigned char *pucDeaKey8N,
        unsigned char *pucInputData, unsigned char *pucOutputData);
int AES(unsigned char ucAesOption, unsigned char *pucAesKey8N,
        unsigned char *pucInputData, unsigned char *pucOutputData);

/* PRNG */
int GenerateRandom (unsigned char *random8);

/* File Encryption */
int cryptoWrite (int hdl, const char *buf, int len);
int cryptoRead (int hdl, char *buf, int len);

/* Attack Flag */
int isAttacked (void);

/* File authentication */
int authFile (const char *filename);
int addCert(char *filename);
int scanCert(int GID);
int authFileExtended(const char *filename, const char *type);

/* IPP emulation */
int ippOpen(void);
int ippClose(void);
int ippRead(char *usrbuf, int usrbuflen);
int ippWrite(char *usrbuf, int usrbuflen);
int ippCommand(unsigned char *outpkt,
			   int *outpktlen,
			   int outpktsize,
			   unsigned char *inpkt,
			   int inpktlen);
int ippPinEntryStatus(int *count, int *lastNonNumericKey);
int ippTerminatePinEntry(void);

/* Security module versions */
int secVersion(char *pchModVersion, char *pchLibVersion);

void setSecurePINDisplayParameters(struct touch_hs_s *hotspot_table, void *callback);

/**
 * Returns the status of the Navigator module.
 * @return 0: The Navigator system is not currently usable  !0: Navigator is on.
 **/
int Nav_active(void);

int osHash (int mode, unsigned char * macKey, int macKeyLen, unsigned char option);

int htdesState( int state );

/* RKL key status and copy KRD key to /tmp */
rkl_key_status_t getKeyStatus_RKL(void);
int copyPublicKey_RKL(void);

/* MagIC3 P-Series external PINpad support */
int pp_generateRSAKeyPair(int size, int expValue, int forceRenew);
int pp_readPublicKey(int maxModLen, unsigned char *modValue, int *modLen, int *expValue);
int pp_initiateNS(int nsMode, int modLen, unsigned char *modValue, int expValue, unsigned char *crypto1);
int pp_finalizeNS(int nsMode, int cryptoLen, unsigned char *crypto2, unsigned char *crypto3);
int pp_generateChallenge(int mode, unsigned char *challenge, unsigned char *workingKey);
int pp_resolveChallenge(int mode, unsigned char *challenge, unsigned char *workingKey, unsigned char *result);
int pp_verifyChallenge(int mode, unsigned char *challenge);
int pp_encryptData(int mode, unsigned char *inData, int inDataLen, unsigned char *outData, int *outDataLen);
int pp_decryptData(int mode, unsigned char *inData, int inDataLen, unsigned char *outData);
int pp_initCipheredlink(int mode, unsigned char* outData);
int pp_cipherData(int mode, int counter, unsigned char* inData, int inDataLen, unsigned char* outData);
int pp_macData(int mode, int counter, unsigned char* inData, int inDataLen, unsigned char* outMAC);
int pp_getPinKey(int mode,  unsigned char *workingKey);
int pp_loadPin(int mode, int pinLen, unsigned char *pin);
int pp_isPinInjected(int *injected);
int pp_createARSVchallenge(int mode, unsigned char *challenge);
int pp_returnARSVchallenge(int mode, unsigned char *challenge, unsigned char *outputdata);

#ifdef __cplusplus
}
#endif
#endif /* _SVCSEC_H */
