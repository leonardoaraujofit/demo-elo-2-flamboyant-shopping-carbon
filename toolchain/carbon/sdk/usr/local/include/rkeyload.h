/** @addtogroup */
/** @{ */
/** @addtogroup */
/** @{ */
/** 
 *  @file rkeyload.h
 *
 *	@brief Remote key load
 *
 * 
 */

#ifndef __RKEYLOAD_H__
#define __RKEYLOAD_H__

/**
 * @name Remote Key Load public and private keys
*/
/** \{ */
#define PUBLIC_KEY_FILE_RKL      "/mnt/flash/system/rkl_keys/rkl_cert.crt"
#define PRIVATE_KEY_FILE_RKL     "/mnt/flash/system/rkl_keys/rkl_priv_key.der"
/** \} */

// ------------- remoteKeyLoad_install() ----------------------------
// params: os_dir = directory to look for key to load
//               msg = character string to place result message string
//               msg_maxlen = max char size of msg
//
// returns: -1 = error loading key
//                0 = no remote key to load
//                1 = remote key loaded successfully
//----------------------------------------------------------------------------

/**
 * @name Load remote key
 *
 * @param[in] os_dir Path to the directory where to look for key to load
 * @param[in] msg Character string to place result message string
 * @param[in] msg_maxlen Max char size of msg
 *
 * @return
 * @li -1 = Error loading key
 * @li 0 = No remote key to load
 * @li 1 = Remote key successfully loaded
 *
 */
int remoteKeyLoad_install(const char *os_dir, char *msg, int msg_maxlen);

/**
 * @name Copy public key
 *
 * @return
 * @li -1 = Error copying
 * @li 0 = The file already exists
 * @li 1 = Remote key successfully copied
 *
 */
long remoteKeyLoad_copyPublicKey(void);

/**
 * @name Get key status
 *
 * @return
 * @li NO_KEYS_RKL = Both files do not exist
 * @li PUBLIC_KEY_RKL = Only rkl_cert.crt exists
 * @li PRIVATE_KEY_RKL = Only rkl_priv_key.der exists
 * @li BOTH_KEYS_RKL = Both files exist
 *
 */
long remoteKeyLoad_getKeyStatus(void);

/**
 * @name Check private key
 *
 */
void remoteKeyLoad_checkPrivKey(void);

/**
 * @name Store application keys
 *
 * @param[in] digest256 ???
 * @param[in] Key_file Path to key file
 *
 * @return
 * @li 0 = Success
 * @li E_RKL_KEY_FILE_READ_ERROR = Key file read error
 * @li E_RKL_NO_KEYS = No key files found
 * @li E_RKL_INVALID_TIMESTAMP = Invalid key timestamp
 * @li E_RKL_INVALID_KEY_BLOCK_HEADER = Invalid key block header
 * @li E_RKL_STORE_KEY_ERROR = Store key error
 * @li E_RKL_MEMORY_ERROR = Out of memory
 * @li E_RKL_INVALID_KEY = Invalid key
 * @li E_RKL_KEY_FILE_PERMS = Access denied
 * @li E_RKL_KEY_FILE_OWNER = Access denied
 *
 */
long remoteKeyLoad_storeAppKeys(unsigned char *digest256, const char *key_file);

#endif
/// @} */
/// @} */
