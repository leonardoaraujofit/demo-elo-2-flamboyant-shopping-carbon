#ifndef __SYSM_FILE_MANAGER__
#define __SYSM_FILE_MANAGER__

#include <sysmode_ui/sysm_lang.h>
#include <sysmode_ui/sysm_ui.h>
#include <sysmode_ui/property.h>

#include <FL/Fl_Box.H>
#include <FL/Fl_PNG_Image.H>
#include <FL/Fl_JPEG_Image.H>
#include <FL/Fl_GIF_Image.H>
#include <FL/Fl_BMP_Image.H>

enum imageType{
    PNG = 0,
    JPEG,
    GIF,
    BMP
};

class DisplayBox : public SYSM_UI::Sysm_Window
{
    Fl_Box*          background;
    Fl_PNG_Image*    png_image;
    Fl_JPEG_Image*   jpeg_image;
    Fl_GIF_Image*    gif_image;
    Fl_BMP_Image*    bmp_image;
    Fl_PNG_Image*    play_image;

    void display_image_cb(char*, char*);
    void display_video_cb(char*, char*);
    void display_audio_cb(char*, char*);
    int handle(int event);

    public:
        DisplayBox(int x, int y, int w, int h, char* =0):
                                SYSM_UI::Sysm_Window(x, y, w, h)
        {
            background      = new Fl_Box(x,y,w,h);
            png_image       = NULL;
            jpeg_image      = NULL;
            gif_image       = NULL;
            bmp_image       = NULL;
            play_image      = NULL;
        }
        ~DisplayBox()
        {
            delete(background);
            delete(png_image);
            delete(jpeg_image);
            delete(gif_image);
            delete(bmp_image);
            delete(play_image);
            background      = NULL;
            png_image       = NULL;
            jpeg_image      = NULL;
            gif_image       = NULL;
            bmp_image       = NULL;
            play_image      = NULL;
        }
        void set_bg_color(Fl_Color color)
        {
            this->color(color);
            this->redraw();
        }
        void display_image(const char* image, imageType type)
        {
            switch (type) {
                case PNG:
                    png_image = new Fl_PNG_Image(image);
                    background->image(png_image);
                    break;
                case JPEG:
                    jpeg_image = new Fl_JPEG_Image(image);
                    background->image(jpeg_image);
                    break;
                case GIF:
                    gif_image = new Fl_GIF_Image(image);
                    background->image(gif_image);
                    break;
                case BMP:
                    bmp_image = new Fl_BMP_Image(image);
                    background->image(bmp_image);
                    break;
            }
            this->color(FL_BLACK);
            this->add(background);
            this->redraw();
        }

        void display_play()
        {   
            SYSM_UI::property *prp = SYSM_UI::property::getInstance();
            SYSM_UI::property::objectAttribute *audio_play_prp = prp->obj(SYSM_UI::property::AUDIO_PLAY);
            play_image = new Fl_PNG_Image(audio_play_prp->imagepath());

            background->image(play_image);
            this->color(FL_BLACK);
            this->add(background);
            this->redraw(); 
        }
};

void file_manager_cb(const SYSM_UI::t_ui_menu_entry * e);
#endif // __SYSM_FILE_MANAGER__
