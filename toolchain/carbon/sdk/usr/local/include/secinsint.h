
#ifndef _SECINSINT_H__
#define _SECINSINT_H__

#include <sys/types.h>
#include <libsecins.h>

/**
 * @name API command codes(IDs)
 */
/** \{ */
#define SECINS_ECHO_TEST			0
#define SECINS_INSTALL_PKGS			1
#define SECINS_START_USER_APPS		2
#define SECINS_REBOOT				3
#define SECINS_START_APP			4
#define SECINS_START_SYSMODE		5
#define SECINS_DEV_START_APP		6
#define SECINS_CREATE_BUNDLE_LIST	7
#define SECINS_REMOVE_BUNDLE		8
#define SECINS_CREATE_PACKAGE_LIST	9
#define SECINS_REMOVEALL_USER		10
#define SECINS_REMOVE_FILE_BUNDLE	11
#define SECINS_REMOVE_USER_BUNDLE	12
#define SECINS_DISABLE_SYSMODE		13
#define SECINS_POWEROFF				14
#define SECINS_HMAC_CHECK			15
#define SECINS_ADD_CERT				16
#define SECINS_APP_DEV_CONVERT		17
#define SECINS_HMAC					18
#define SECINS_ABORT_POWEROFF		19

#define SECINS_UID_GID_RANGE		64
#define SECINS_ADD_USER				65
#define SECINS_ADD_GROUP			66
#define SECINS_ADD_GROUP_MEMBER		67
#define SECINS_REMOVE_USER			68
#define SECINS_REMOVE_GROUP			69
#define SECINS_START_APP_UID		70
#define SECINS_SEND_SIGNAL			71
#define SECINS_CHOWN_FILE			72
#define SECINS_FORMAT_CARD			73
#define SECINS_START_APP_ARGV       74
#define SECINS_KILL_APP_APP         75
#define SECINS_DEL_ALL_USERS        76

#define SECINS_EXECUTETOKEN       77
/** \} */


/**
 * @name Buffer sizes
 */
/** \{ */
#define SECINS_MAX_CMD_DATA_LEN		256
#define SECINS_MAX_RESP_DATA_LEN	128
#define TOKEN_TYPE_LEN				(3)
#define KLD_MAX_FILENAME_SIZE		(128)
#define KLD_SIG_SIZE				(4)
#define SECINS_API_MAX_PATH_SIZE	80
#define SECINS_MAX_NAME_SIZE        64
/** \} */


/**
 * @name Child management TCP/IP address (used to trigger callbacks)
 */
/** \{ */
#define CHILD_MGMT_IP_ADDR 0x7F414101 /**< 127.65.65.1 */
/** \} */


typedef struct
{
    int pid;        /**< Application PID */
    int status;     /**< exit status */

} SECINS_CHILD_TERM_STATUS_T;

/**
 * @name start application wtih callback registration
 */
typedef struct
{
    int cb; /**< callback registration flag */
    char cmdline[SECINS_MAX_CMD_DATA_LEN - (sizeof(int) * 2)]; /**< Payload */

} START_APP_CB;




/**
 * Command header
 */
typedef struct
{
    int cmd;        /**< Command ID */
    int datalen;    /**< Payload length */

} SECINS_API_CMD_HDR;


/**
 * Command
 */
typedef struct
{
    SECINS_API_CMD_HDR cmdhdr;                      /**< Command header */
    unsigned char cmddata[SECINS_MAX_CMD_DATA_LEN]; /**< Payload */

} SECINS_API_CMD;


/**
 * Responce command header
 */
typedef struct
{
    int cmd;        /**< Command ID */
    int err;        /**< Server side callback return error code */
    int datalen;    /**< Responce payload length */

} SECINS_API_RESP_HDR;


/**
 * Responce message
 */
typedef struct
{
    SECINS_API_RESP_HDR resphdr;                        /**< Responce command header */
    unsigned char respdata[SECINS_MAX_RESP_DATA_LEN];   /**< Responce payload */

} SECINS_API_RESP;


/**
 * Command SECINS_ADD_GROUP payload
 */
typedef struct
{
    int gid;                                /**< Group ID */
    char groupname[SECINS_MAX_NAME_SIZE];   /**< Group name */

} ADD_GROUP;


/**
 * Command SECINS_ADD_USER payload
 */
typedef struct
{
    int uid;                                /**< User ID */
    int gid;                                /**< Group ID */
    char username[SECINS_MAX_NAME_SIZE];    /**< User name */

} ADD_USER;


/**
 * Command SECINS_ADD_GROUP_MEMBER, SECINS_REMOVE_USER, SECINS_REMOVE_GROUP payload
 */
typedef struct
{
    int gid;    /**< Group ID */
    int uid;    /**< User ID */

} UID_GID_ARGS;


/**
 * Command SECINS_START_APP_UID payload
 */
typedef struct
{
    int uid;            /**< User ID */
    int gid;            /**< Group ID */
    char cmdline[SECINS_MAX_CMD_DATA_LEN - (sizeof(int) * 2)];  /**< CMD line */

} START_APP;


/**
 * Command SECINS_START_APP_UID responce payload
 */
typedef struct
{
    pid_t pid;  /**< Process ID */

} START_APP_RESP;


/**
 * Command SECINS_SEND_SIGNAL payload
 */
typedef struct
{
    pid_t pid;  /**< Process ID */
    int sig;    /**< Linux signal */

} SIGNAL_APP;


/**
 * Command SECINS_CHOWN_FILE payload
 */
typedef struct
{
    int uid;        /**< User ID */
    int gid;        /**< Group ID */
    char path[80];  /**< Path to files */

} CHOWN_FILE;


/**
 * Command SECINS_REMOVE_BUNDLE payload
 */
typedef struct
{
    char name[MAX_PKG_NAME_LEN + 1];        /**< Bundle name */
    char version[MAX_PKG_VERSION_LEN + 1];  /**< Bundle version */
    char user[MAX_PKG_USER_LEN + 1];        /**< Bundle user */

} REMOVE_BUNDLE;

typedef struct
{
	char token_file[KLD_MAX_FILENAME_SIZE];
	char token_signature[KLD_SIG_SIZE + 1];
	char token_type[TOKEN_TYPE_LEN + 1];
	int options;

} TOKEN_BUNDLE;

/**
 * Command SECINS_REMOVE_FILE_BUNDLE payload
 */
typedef struct
{
    char pathname[128];                 /**< file path */
    char type[MAX_PKG_TYPE_LEN + 1];    /**< package type */

} REMOVE_FILE_BUNDLE;



typedef struct
{
	char token_file[SECINS_API_MAX_PATH_SIZE];
	char token_sig[SECINS_API_MAX_PATH_SIZE];
	char cert_dir[SECINS_API_MAX_PATH_SIZE];

} APP_DEV_CONVERT;


/**
 * Send message to secure installer
 *
 * @param[out] respdata     - responce buffer
 * @param[in]  respdatalen  - responce buffer size
 * @param[in]  timeout_secs - timeout[s]
 * @param[in]  cmd_id       - command ID
 * @param[in]  cmddata      - command payload
 * @param[in]  cmddatalen   - command payload size
 *
 * @return 0 on success or one of the ERROR_SECINS error codes on error.
 */
int Secins_send_msg(unsigned char *respdata,
                    int *respdatalen,
                    int maxrespdatalen,
                    int timeout_secs,
                    int cmd_id,
                    const unsigned char *cmddata,
                    int cmddatalen);


/**
 * Server side request receive function
 *
 * @param[out] cmd      - where to store request command data
 * @param[in]  fd       - open socket's file descriptor
 *
 * @return 0 on success or one of the ERROR_SECINS error codes on error.
 */
int Secins_recv_api_cmd(SECINS_API_CMD *cmd, int fd);


/**
 * Server side send responce
 *
 * @param[in] resp      - responce message
 * @param[in] fd        - open socket's file descriptor
 *
 * @return 0 on success or one of the ERROR_SECINS error codes on error.
 */
int Secins_send_api_resp(SECINS_API_RESP *resp, int fd);


/**
 * Start application request, developement mode only
 *
 * @param[in] cmdline - CMD line to start
 *
 * @return 0 on success or one of the ERROR_SECINS error codes on error.
 */
int Secins_dev_start_app(const char *cmdline);


/**
 * Returns the gid of the group created by the secure installer to allow sharing between the
 * <uid> user and its secondary users i.e. The primary group and all the secondary users it
 * creates are members of this group.
 *
 * @return
 * Group ID on success, -1 on error
 */
int Secins_user_sec_share_gid(int uid);


/**
 * Returns the group name of the group created by the secure installer to allow sharing between
 * the <uid> user and its secondary users.
 *
 * @param[out] groupname    - buffer where to store group name
 * @param[in]  namesize     - sizeof <groupname>
 * @param[in]  uid          - user ID
 *
 * @return
 * Group ID on success, -1 on error
 */
int Secins_get_user_sec_share_group(char *groupname, int namesize, int uid);


/**
 * Check if application with <uid> UID is system
 *
 * @param[in] uid - user ID
 *
 * @return
 * @li 1 - is a system application
 * @li 0 - is not a system application
 */
unsigned int Secins_is_sys_app(uid_t uid);


/**
 * Check if application with <uid> UID is user
 *
 * @param[in] uid - user ID
 *
 * @return
 * @li 1 - is a user application
 * @li 0 - is not a user application
 */
unsigned int Secins_is_user_app(uid_t uid);


/**
 * Get token
 *
 * @param[out] next
 * @param[in]  str
 *
 * @return
 */
char *Secins_get_token(char **next, char *str);


/**
 * Check if string is a digit
 *
 * @param[in] str - string to check
 *
 * @return
 * @li 1 str is a digit
 * @li 0 str is not a digit or NULL
 */
unsigned int Secins_str_is_digits(const char *str);

#endif /* _SECINSINT_H__ */
