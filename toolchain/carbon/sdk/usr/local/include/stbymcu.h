/*
 * stbymcu.h - Upper Layer interface
 *
 *  Created on: Oct 30, 2012
 *  Author: thomasm4
 */

#ifndef STBYMCU_H_
#define STBYMCU_H_

#include <stdint.h>
#include <time.h>

#ifdef __cplusplus
extern "C" {
#endif

#define FROMBCD(x)          (((x) >> 4) * 10 + ((x) & 0xf))
#define TOBCD(x)            (((x) / 10 * 16) + ((x) % 10))

typedef struct
{
    uint8_t timestamp[7];
    int8_t temperature;
} StbymcuTempLog_t;

// event masks used by stbymcu_wait_timed_event()
#define STBYMCU_EV_MDB_WAKE             (1<<0)
#define STBYMCU_EV_CARD_INSERTED        (1<<1)
#define STBYMCU_EV_PROXIMITY_SENSOR     (1<<2)
#define STBYMCU_EV_TX_POLL_TIMEOUT      (1<<3)
#define STBYMCU_EV_TX_SUCCESS           (1<<4)
#define STBYMCU_EV_TX_FAILED            (1<<5)
#define STBYMCU_EV_POLL_ACTIVE          (1<<6)
#define STBYMCU_EV_POLL_TIMEOUT         (1<<7)

// Max len is 254 (256 - CMD-byte and - CS-byte)
#define STBYMCU_MAX_MESG_SIZE           (256 - 2)
// Max temp-log is 4 times a day for 7 days
#define STBYMCU_MAX_TEMP_RECORDS        (4 * 7)

#define STBYMCU_CMD_READ_TEMP           0x01
#define STBYMCU_CMD_CLEAR_LOG           0x02
#define STBYMCU_CMD_SET_RTC             0x03
#define STBYMCU_CMD_READ_RTC            0x04 
#define STBYMCU_CMD_ENTERING_SLEEP      0x05
#define STBYMCU_CMD_SETCLEAR_MDB_WAKE   0x06
#define STBYMCU_CMD_READ_INT            0x07
#define STBYMCU_CMD_CONFIG_UART_RS485   0x08
#define STBYMCU_CMD_SET_MODE            0x09
#define STBYMCU_CMD_READ_MODE           0x0A
#define STBYMCU_CMD_READY_DATA_PUSH     0x0B
#define STBYMCU_CMD_SET_WAKE_SOURCES    0x0C
#define STBYMCU_CMD_SET_WAKE_TIME       0x0D

#define STBYMCU_CMD_MDB_SET_CONFIG      0x30
#define STBYMCU_CMD_MDB_READ_VOLT       0x31
#define STBYMCU_CMD_MDB_SET_ADDR        0x32
#define STBYMCU_CMD_MDB_SET_RESP_TIME   0x33
#define STBYMCU_CMD_MDB_FLUSH_BUF       0x34
#define STBYMCU_CMD_MDB_READ_STATUS     0x35
#define STBYMCU_CMD_MDB_CLEAR_STATUS    0x36
// no 0x37 defined
#define STBYMCU_CMD_MDB_READ_LAST_POLL  0x38
#define STBYMCU_CMD_MDB_CHECK_POLL      0x39
#define STBYMCU_CMD_MDB_SEND_JUST_RESET 0x3A
#define STBYMCU_CMD_MDB_READ_ADDR       0x3B
#define STBYMCU_CMD_GET_FW_VER          0x50
#define STBYMCU_CMD_SET_CURRENT_TEMP    0x51
#define STBYMCU_CMD_READ_CURRENT_TEMP   0x52
#define STBYMCU_CMD_READ_REED_SWITCH    0x53


#define STBYMCU_SLEEP_DEEP              0x01
#define STBYMCU_SLEEP_LOW               0x02
#define STBYMCU_SLEEP_HIB               0x03
#define STBYMCU_SLEEP_NONE              0x04
#define STBYMCU_SLEEP_STANDBY           0x05

#define STBYMCU_WAKE_TIMER_DISABLE      0x00
#define STBYMCU_WAKE_TIMER_ENABLE       0x01

#define STBYMCU_WAKE_SOURCE_MDBWAKE     (1<<0)
#define STBYMCU_WAKE_SOURCE_CARDIN      (1<<1)
#define STBYMCU_WAKE_SOURCE_PROXIMITY   (1<<2)
#define STBYMCU_WAKE_SOURCE_SERIAL      (1<<3)
//
#define _STBYMCU_WAKE_SOURCE_VALID      (STBYMCU_WAKE_SOURCE_MDBWAKE | STBYMCU_WAKE_SOURCE_CARDIN | \
                                                STBYMCU_WAKE_SOURCE_PROXIMITY | STBYMCU_WAKE_SOURCE_SERIAL)

#define STBYMCU_MDB_STATUS_BUS_RESET    (1<<0)
#define STBYMCU_MDB_STATUS_RESERVE      (1<<1)
#define STBYMCU_MDB_STATUS_FIFO_FULL    (1<<2)

#define STBYMCU_MODE_MDB                (1<<0)
#define STBYMCU_MODE_RS485              0
#define STBYMCU_MODE_AUTOACK            (1<<1)
#define STBYMCU_MODE_NO_AUTOACK         0
#define STBYMCU_MODE_AUTOSETUP          (1<<2)
#define STBYMCU_MODE_NO_AUTOSETUP       0
#define STBYMCU_MODE_AUTORESET          (1<<3)
#define STBYMCU_MODE_NO_AUTORESET       0


int stbymcu_open(void);
int stbymcu_close(void);
int stbymcu_read_temperature_log(uint8_t count, StbymcuTempLog_t *records);
int stbymcu_clear_temperature_log(void);
int stbymcu_set_current_temperature(int8_t temp);
int stbymcu_read_current_temperature(int8_t *temp);
int stbymcu_read_Reed_switch(uint8_t *switch_val);
int stbymcu_set_rtc(struct tm *time);
int stbymcu_read_rtc(struct tm *time);
int stbymcu_entering_sleep(uint8_t mode);
int stbymcu_entering_sleep_ex(uint8_t mode, uint8_t time_msb, uint8_t time_lsb);
int stbymcu_set_mdb_wake_line(uint8_t toggle);
int stbymcu_read_int_register(unsigned char *int_reg);
int stbymcu_config_uart_RS485(uint16_t baudrate, uint8_t parity, uint8_t databits, uint8_t stopbits);
int stbymcu_set_wake_timer(uint8_t mode, uint8_t hour, uint8_t min, uint8_t sec);
int stbymcu_set_wake_sources(uint8_t sources);
int stbymcu_set_mode(uint8_t mode);
int stbymcu_read_mode(uint8_t *mode);

#define STBYMCU_FW_VER_BUF_SIZE	20
int stbymcu_get_fw_version(unsigned char fw_version[STBYMCU_FW_VER_BUF_SIZE]);

// export function for notification events from kernel module
int stbymcu_wait_timed_event(uint8_t mask, uint8_t *retmask, unsigned short timeout_msec);

/*
 * export functions specific for MDB usage
 */

int stbymcu_mdb_ready_data_push(void);
// thomasm4, removed for now as this function is not actively supported atm
// int stbymcu_mdb_set_config_data(uint8_t count, unsigned char *config_resp);
int stbymcu_mdb_read_volt(uint16_t *voltage);
int stbymcu_mdb_set_address(uint8_t address);
int stbymcu_mdb_read_address(uint8_t *address);
int stbymcu_mdb_set_response_time(uint8_t max_resp_time, uint8_t max_inter_byte_time);
int stbymcu_mdb_flush_buffer(void);
int stbymcu_mdb_read_status(uint8_t *status);
int stbymcu_mdb_clear_status(void);
int stbymcu_mdb_read_time_since_last_poll(uint16_t *time);
int stbymcu_mdb_check_poll(uint16_t time);
int stbymcu_mdb_send_just_reset(void);
int stbymcu_wakeup_mdb_devices(void);


#ifdef __cplusplus
}
#endif

#endif /* STBYMCU_H_ */
