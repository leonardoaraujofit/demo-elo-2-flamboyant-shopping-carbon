/**
 *-----------------------------------------------------------------------------
 *
 *    Copyright (c) 2013 Verifone, Inc
 *
 *-----------------------------------------------------------------------------
 * \brief InFusion Logging API: printf-like API
 * \author Andrey Galkin
 *-----------------------------------------------------------------------------
 * \note Define LOGAPI_ENABLE_DEBUG to enable DBG*() macros
 * \note Enable -Wall for GCC compiler to check formatting
 *
 * Public API:
 *   printf()-like message log
 *     LOGF_ERROR( format, ... )
 *     LOGF_WARN( format, ... )
 *     LOGF_INFO( format, ... )
 *     LOGF_TRACE( format, ... )
 * 
 *     DBGF_ERROR( format, ... )
 *     DBGF_WARN( format, ... )
 *     DBGF_INFO( format, ... )
 *     DBGF_TRACE( format, ... )
 *-----------------------------------------------------------------------------
 */
#pragma once

/*--*/
#include "log/_common.h"

/*--*/
#if defined( __cplusplus )
extern "C" {
#endif
/*--*/
#   include "stdarg.h"

/*-----------------------------------*/


/**---------------------------------------------------------------------------
 * \brief Log variable argument helper
 * \note Should be use in legacy logging API wrappers only
 */
LOGAPI_PUBLIC void LogAPI_vprintf(
    LogAPI_Levels log_level, const char *file, unsigned int line,
    const char* format, va_list args
);

/**---------------------------------------------------------------------------
 * \brief Log message in printf-like style
 * \note Avoid direct use
 */
LOGAPI_PUBLIC void LogAPI_printf(
    LogAPI_Levels log_level, const char *file, unsigned int line,
    const char* format, ...
) LOGAPI_GCC_ATTR(( format( printf, 4, 5 ) ));


#if defined( LOGAPI_GCC ) && 0

    /**
     * \brief helper macro
     * \note Avoid direct use
     */
#   define LOGAPI_PRINTF( level, format, ... ) \
        { LogAPI_printf( level, LOGAPI_FILE, LOGAPI_LINE, format, ##__VA_ARGS__ ); }

    /**
     * \brief printf in error level
     */
#   define LOGF_ERROR( format, ... ) \
        LOGAPI_PRINTF( LOGAPI_ERROR, format, ##__VA_ARGS__ )
    /**
     * \brief printf in warning level
     */
#   define LOGF_WARN( format, ... ) \
        LOGAPI_PRINTF( LOGAPI_WARN, format, ##__VA_ARGS__ )
    /**
     * \brief printf in info level
     */
#   define LOGF_INFO( format, ... ) \
        LOGAPI_PRINTF( LOGAPI_INFO, format, ##__VA_ARGS__ )
    /**
     * \brief printf in trace level
     */
#   define LOGF_TRACE( format, ... ) \
        LOGAPI_PRINTF( LOGAPI_TRACE, format, ##__VA_ARGS__ )

#else
    /**
     * \note Created for non-GCC (ARM) compiler or disabled extensions
     * \see http://en.wikipedia.org/wiki/Variadic_macro
     *
     * \brief helper macro
     * \note Avoid direct use
     */
#   define LOGAPI_PRINTF( level, ... ) \
        { LogAPI_printf( level, LOGAPI_FILE, LOGAPI_LINE, __VA_ARGS__ ); }

    /**
     * \brief printf in error level
     */
#   define LOGF_ERROR( ... ) \
        LOGAPI_PRINTF( LOGAPI_ERROR, __VA_ARGS__ )
    /**
     * \brief printf in warning level
     */
#   define LOGF_WARN( ... ) \
        LOGAPI_PRINTF( LOGAPI_WARN, __VA_ARGS__ )
    /**
     * \brief printf in info level
     */
#   define LOGF_INFO( ... ) \
        LOGAPI_PRINTF( LOGAPI_INFO, __VA_ARGS__ )
    /**
     * \brief printf in trace level
     */
#   define LOGF_TRACE( ... ) \
        LOGAPI_PRINTF( LOGAPI_TRACE, __VA_ARGS__ )
#endif


#if defined( LOGAPI_ENABLE_DEBUG )
    #define DBG_PRINTF  LOGAPI_PRINTF
    #define DBGF_ERROR  LOGF_ERROR
    #define DBGF_WARN   LOGF_WARN
    #define DBGF_INFO   LOGF_INFO
    #define DBGF_TRACE  LOGF_TRACE
#else
    #define DBG_PRINTF(...)     LOGAPI_EMPTYSTMT
    #define DBGF_ERROR(...)     LOGAPI_EMPTYSTMT
    #define DBGF_WARN(...)      LOGAPI_EMPTYSTMT
    #define DBGF_INFO(...)      LOGAPI_EMPTYSTMT
    #define DBGF_TRACE(...)     LOGAPI_EMPTYSTMT
#endif

/*-----------------------------------*/
#if defined( __cplusplus )
} /* extern "C" */
#endif

/*--*/
