//----------------------------------------------------------------------------
//
//    Copyright (c) 2013 Verifone, Inc
//
//----------------------------------------------------------------------------
//! \brief InFusion Logging API: C++ stream-like API
//! \author Andrey Galkin
//----------------------------------------------------------------------------
//! \note Define LOGAPI_ENABLE_DEBUG to enable DBG*() macros
//!
//! Public API:
//!   C++ iostream-like message log
//!     LOGS_ERROR( right_expr_to_<<_op )
//!     LOGS_WARN( right_expr_to_<<_op )
//!     LOGS_INFO( right_expr_to_<<_op )
//!     LOGS_TRACE( right_expr_to_<<_op )
//!
//!     DBGS_ERROR( right_expr_to_<<_op )
//!     DBGS_WARN( right_expr_to_<<_op )
//!     DBGS_INFO( right_expr_to_<<_op )
//!     DBGS_TRACE( right_expr_to_<<_op )
//----------------------------------------------------------------------------
#pragma once

//--
#include <iostream>
#include "log/_common.h"


namespace LogAPI {
//-------------------------------------
//! \brief Helper class
//! \note Do not use directly, unless you know what are you doing
//-------------------------------------
class LOGAPI_PUBLIC OnStackLogger :
    public std::ostream
{
    public:
        //-----------------------------
        OnStackLogger( LogAPI_Levels level, const char *source_file,
                       unsigned int source_line );

    private:
        OnStackLogger( const OnStackLogger& );
        const OnStackLogger& operator= ( const OnStackLogger& );

        //-----------------------------
        struct StreamBuf :
            public std::streambuf
        {
            StreamBuf();
            ~StreamBuf();

            char msg_buffer_[LOGAPI_MSG_MAX_LEN+1];
            LogAPI_MessageData log_msg_;
        };

        StreamBuf buffer_;
};



//! \brief helper macro
//! \note Avoid direct use
#define LOGAPI_STREAM( level, ... ) \
    { if ( int( level ) <= int( LogAPI_GetLevel() ) ) { \
        using namespace LogAPI; \
        int gcc_pre47_workaround[( sizeof( OnStackLogger ) + sizeof( int ) - 1 ) / sizeof( int ) ]; \
        OnStackLogger* on_stack_logger_ = new ( ( void* ) gcc_pre47_workaround ) OnStackLogger( level, LOGAPI_FILE, LOGAPI_LINE ); \
        *on_stack_logger_ << __VA_ARGS__; \
        on_stack_logger_->~OnStackLogger(); \
    } }

//! \brief Hex dump in error level
#define LOGS_ERROR( ... ) \
    LOGAPI_STREAM( LOGAPI_ERROR, __VA_ARGS__ )
//! \brief Hex dump in warning level
#define LOGS_WARN( ... ) \
    LOGAPI_STREAM( LOGAPI_WARN, __VA_ARGS__ )
//! \brief Hex dump in info level
#define LOGS_INFO( ... ) \
    LOGAPI_STREAM( LOGAPI_INFO, __VA_ARGS__ )
//! \brief Hex dump in trace level
#define LOGS_TRACE( ... ) \
    LOGAPI_STREAM( LOGAPI_TRACE, __VA_ARGS__ )


#if defined( LOGAPI_ENABLE_DEBUG )
    #define DBG_STREAM  LOGAPI_STREAM
    #define DBGS_ERROR  LOGS_ERROR
    #define DBGS_WARN   LOGS_WARN
    #define DBGS_INFO   LOGS_INFO
    #define DBGS_TRACE  LOGS_TRACE
#else
    #define DBG_STREAM(...)     LOGAPI_EMPTYSTMT
    #define DBGS_ERROR(...)     LOGAPI_EMPTYSTMT
    #define DBGS_WARN(...)      LOGAPI_EMPTYSTMT
    #define DBGS_INFO(...)      LOGAPI_EMPTYSTMT
    #define DBGS_TRACE(...)     LOGAPI_EMPTYSTMT
#endif

//-------------------------------------
} // LogAPI

//--
