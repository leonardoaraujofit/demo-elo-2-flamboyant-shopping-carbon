/**
 *-----------------------------------------------------------------------------
 *
 *    Copyright (c) 2013 Verifone, Inc
 *
 *-----------------------------------------------------------------------------
 * \brief InFusion Logging API: Hint manual API
 * \author Andrey Galkin
 *-----------------------------------------------------------------------------
 * \note Define your own LOGAPI_HINT() for hint manual creation
 *
 * Public API:
 *      LOGAPI_HINT( normal_log_command, hint_string )
 *
 * Example:
 *     LOGAPI_HINT(
 *        DBGS_INFO( "Testing hint" ),
 *        "Fix this nasty thing!"
 *     )
 *   
 *-----------------------------------------------------------------------------
 */
#pragma once

/*--*/

#if !defined( LOGAPI_HINT )
#   define LOGAPI_HINT( log_cmd, hint ) log_cmd
#endif


/*--*/
