/**
 *-----------------------------------------------------------------------------
 *
 *    Copyright (c) 2013 Verifone, Inc
 *
 *-----------------------------------------------------------------------------
 * \brief InFusion Logging API: Module/sub-channel formalization API
 * \author Andrey Galkin
 *-----------------------------------------------------------------------------
 * \note Define your own LOGAPI_MOD(), if required
 *
 * Public API:
 *      LOGAPI_MOD( module, msg_string )
 *
 * Example:
 *      DBGF_DEBUG( LOGAPI_MOD( UI, “Msg %s” ), str );
 *      --> DBGF_DEBUG( “[UI] Msg %s“, str );
 *      DBGS_DEBUG( LOGAPI_MOD( COMM, “” ) << some_msg << some_prm );
 *      --> DBGS_DEBUG( “[COMM] ”  << some_msg << some_prm );
 *
 *-----------------------------------------------------------------------------
 */
#pragma once

/*--*/

#if !defined( LOGAPI_MOD )
#   define LOGAPI_MOD( mod, str ) "[" #mod "] " str
#endif


/*--*/
