/**
 *-----------------------------------------------------------------------------
 *
 *    Copyright (c) 2013 Verifone, Inc
 *
 *-----------------------------------------------------------------------------
 * \brief InFusion Logging API: assertion API
 * \author Andrey Galkin
 *-----------------------------------------------------------------------------
 * \note Define LOGAPI_ENABLE_DEBUG to enable DBG*() macros
 *
 * Public API:
 *      LOGAPI_ASSERT( expression )
 *      LOGAPI_VERIFY( expression )
 *      DBG_ASSERT( expression )
 *      DBG_VERIFY( expression )
 *
 *-----------------------------------------------------------------------------
 */
#pragma once

/*--*/
#include "log/_printf.h"

/*--*/

#define LOGAPI_VERIFY( exp ) \
    { if ( !(exp) ){ LOGF_ERROR( "VERIFY: %s", #exp ) } }

#define LOGAPI_ASSERT( exp ) \
    { if ( !(exp) ){ LOGF_ERROR( "ASSERT: %s", #exp ); \
      LOGAPI_VERBOSE_ABORT(); \
    } }


#if defined( LOGAPI_ENABLE_DEBUG )
    #define DBG_VERIFY LOGAPI_VERIFY
    #define DBG_ASSERT LOGAPI_ASSERT
#else
    #define DBG_VERIFY(...)
    #define DBG_ASSERT(...)
#endif


/*--*/
