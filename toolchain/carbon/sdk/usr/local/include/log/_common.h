/**
 *-----------------------------------------------------------------------------
 *
 *    Copyright (c) 2013 Verifone, Inc
 *
 *-----------------------------------------------------------------------------
 * \brief InFusion Logging API: common API
 * \author Andrey Galkin
 *-----------------------------------------------------------------------------
 * \note Define LOGAPI_ENABLE_DEBUG to enable DBG*() macros
 *
 * Public API:
 *     LOGAPI_ENABLE_DEBUG - define macro in preprocessor to enable DEBUG level
 *
 *   Initialize:
 *     LOGAPI_INIT( channel )
 *
 *   Set maximal log level
 *     LOGAPI_SETLEVEL( level )
 *
 *   Dump title + address + size, start separator HEX and ASCII representation, end separator
 *     LOGAPI_HEXDUMP_ERROR( title, data, size )
 *     LOGAPI_HEXDUMP_WARN( title, data, size )
 *     LOGAPI_HEXDUMP_INFO( title, data, size )
 *     LOGAPI_HEXDUMP_TRACE( title, data, size )
 *     DBG_HEXDUMP_ERROR( title, data, size )
 *     DBG_HEXDUMP_WARN( title, data, size )
 *     DBG_HEXDUMP_INFO( title, data, size )
 *     DBG_HEXDUMP_TRACE( title, data, size )
 *
 *   Dump HEX and ASCII representation only
 *     LOGAPI_HEXDUMP_RAW_ERROR( data, size )
 *     LOGAPI_HEXDUMP_RAW_WARN( data, size )
 *     LOGAPI_HEXDUMP_RAW_INFO( data, size )
 *     LOGAPI_HEXDUMP_RAW_TRACE( data, size )
 *     DBG_HEXDUMP_RAW_ERROR( data, size )
 *     DBG_HEXDUMP_RAW_WARN( data, size )
 *     DBG_HEXDUMP_RAW_INFO( data, size )
 *     DBG_HEXDUMP_RAW_TRACE( data, size )
 *
 *   Misc. API
 *     LOGAPI_DUMP_SYS_INFO()
 *     LOGAPI_PRINT_CALLSTACK( level )
 *     LOGAPI_VERBOSE_ABORT()
 *
 *-----------------------------------------------------------------------------
 */
#pragma once

/*--*/

/*--*/
#if defined( __cplusplus )
extern "C" {
#endif
/*-----------------------------------*/

/**
 * \brief Sane limits
 */
enum
{
    LOGAPI_MSG_MAX_LEN = 80,
    LOGAPI_CHANNEL_MAX_LEN = 16,
    LOGAPI_SOURCE_MAX_LEN = 16
};

#define LOGAPI_FILE __FILE__
#define LOGAPI_LINE __LINE__

#if defined( __GNUC__ ) || defined( __GNUG__ )
#   define LOGAPI_GCC
#   define LOGAPI_GCC_ATTR( x ) __attribute__(x)
#else
#   define LOGAPI_GCC_ATTR( x )
#endif

#define LOGAPI_PUBLIC LOGAPI_GCC_ATTR(( visibility("default") ))

/* Required due to concern raised in certain unsafe coding style situations */
#define LOGAPI_EMPTYSTMT {}


/*============================================================================
* Message log API
*============================================================================*/

/**---------------------------------------------------------------------------
 * \brief Basic Logging API levels with strictly defined IDs
 * \note In most cases, use DBG*_ERROR() ...DBG*_TRACE() macros
 *
 */
typedef enum
{
    LOGAPI_OFF      = 0x00,
    LOGAPI_ERROR    = 0x01,
    LOGAPI_WARN     = 0x02,
    LOGAPI_INFO     = 0x03,
    LOGAPI_TRACE    = 0x04
} LogAPI_Levels;


/**---------------------------------------------------------------------------
 * \brief Low level message data
 * \note It's designed for easy extensions and backward compatibility in shared libraries
 *
 */
typedef struct
{
    const char *msg; /**< Pre-formatted message of LOGAPI_MSG_MAX_LEN characters max + 1 for zero termination */
    unsigned int msg_len; /**< don't depend on zero termination here */
    
    const char *channel; /**< LOGAPI_CHANNEL_MAX_LEN characters max + 1 for zero termination */
    
    const char *source_file; /**< Use LOGAPI_FILE macro here */
    unsigned int source_line; /**< Use LOGAPI_LINE macro here */
    /** \note "source_file:source_line" is truncated to last LOGAPI_SOURCE_MAX_LEN characters */
    
    LogAPI_Levels log_level; /**< Log level */
    
    unsigned int struct_end_indicator; /**< Should always be 0, unless the structure is extended */
} LogAPI_MessageData;


/**---------------------------------------------------------------------------
 * \brief Platform-specific message logging implementation
 * \note Use DBGF_*() or DBGS_*() macros
 *
 */
LOGAPI_PUBLIC void LogAPI_os_message( const LogAPI_MessageData* msg_data );



/*============================================================================
* Configure log API
*============================================================================*/

/**---------------------------------------------------------------------------
 * \brief Platform-specific initialization
 * \note Do not use directly, not exported
 * \note Must be protected inside from repeated calls
 */
void LogAPI_os_system_init( void );

/**---------------------------------------------------------------------------
 * \brief Initialize logging library with a specified channel
 * \param channel Application name or anything meaningful of up to LOGAPI_CHANNEL_MAX_LEN
 * \note Use LOGAPI_INIT() macro
 */
LOGAPI_PUBLIC void LogAPI_Init( const char *channel );

#define LOGAPI_INIT( channel ) \
    { LogAPI_Init( channel ); }


/**---------------------------------------------------------------------------
 * \brief Get current log channel
 */
LOGAPI_PUBLIC const char *LogAPI_GetChannel( void );

/**---------------------------------------------------------------------------
 * \brief Set log level filter
 * \note Use LOGAPI_SETLEVEL() macro
 */
LOGAPI_PUBLIC void LogAPI_SetLevel( LogAPI_Levels log_level );

/**---------------------------------------------------------------------------
 * \brief Get current filter's log level
 */
LOGAPI_PUBLIC LogAPI_Levels LogAPI_GetLevel( void );

#define LOGAPI_SETLEVEL( level ) \
    { LogAPI_SetLevel( level ); }



/*============================================================================
* Hex dump API
*============================================================================*/

/**---------------------------------------------------------------------------
 * \brief Universal hexdump
 * \note Use LOGAPI_HEX() macro
 *
 * Dump title + address + size, start separator HEX and ASCII representation, end separator
 */
LOGAPI_PUBLIC void LogAPI_hexdump(
    LogAPI_Levels log_level, const char *title, const void *data, unsigned int size,
    const char *file, unsigned int line
);

/**---------------------------------------------------------------------------
 * \brief helper macro
 * \note Avoid direct use
 */
#define LOGAPI_HEXDUMP( level, title, data, size ) \
    { LogAPI_hexdump( level, title, data, size, LOGAPI_FILE, LOGAPI_LINE ); }

/**---------------------------------------------------------------------------
 * \brief Hex dump in error level
 */
#define LOGAPI_HEXDUMP_ERROR( title, data, size ) \
    LOGAPI_HEXDUMP( LOGAPI_ERROR, title, data, size )
/**---------------------------------------------------------------------------
 * \brief Hex dump in warning level
 */
#define LOGAPI_HEXDUMP_WARN( title, data, size ) \
    LOGAPI_HEXDUMP( LOGAPI_WARN, title, data, size )
/**---------------------------------------------------------------------------
 * \brief Hex dump in info level
 */
#define LOGAPI_HEXDUMP_INFO( title, data, size ) \
    LOGAPI_HEXDUMP( LOGAPI_INFO, title, data, size )
/**---------------------------------------------------------------------------
 * \brief Hex dump in trace level
 */
#define LOGAPI_HEXDUMP_TRACE( title, data, size ) \
    LOGAPI_HEXDUMP( LOGAPI_TRACE, title, data, size )


#if defined( LOGAPI_ENABLE_DEBUG )
    #define DBG_HEXDUMP         LOGAPI_HEXDUMP
    #define DBG_HEXDUMP_ERROR   LOGAPI_HEXDUMP_ERROR
    #define DBG_HEXDUMP_WARN    LOGAPI_HEXDUMP_WARN
    #define DBG_HEXDUMP_INFO    LOGAPI_HEXDUMP_INFO
    #define DBG_HEXDUMP_TRACE   LOGAPI_HEXDUMP_TRACE
#else
    #define DBG_HEXDUMP(...)        LOGAPI_EMPTYSTMT
    #define DBG_HEXDUMP_ERROR(...)  LOGAPI_EMPTYSTMT
    #define DBG_HEXDUMP_WARN(...)   LOGAPI_EMPTYSTMT
    #define DBG_HEXDUMP_INFO(...)   LOGAPI_EMPTYSTMT
    #define DBG_HEXDUMP_TRACE(...)  LOGAPI_EMPTYSTMT
#endif



/**---------------------------------------------------------------------------
 * \brief Universal hexdump
 * \note Use LOGAPI_HEX_RAW() macro
 *
 * Dump HEX and ASCII representation only
 */
LOGAPI_PUBLIC void LogAPI_hexdump_raw(
        LogAPI_Levels log_level, const void *data, unsigned int size,
        const char *file, unsigned int line
);

/**---------------------------------------------------------------------------
 * \brief helper macro
 * \note Avoid direct use
 */
#define LOGAPI_HEXDUMP_RAW( level, data, size ) \
    { LogAPI_hexdump_raw( level, data, size, LOGAPI_FILE, LOGAPI_LINE ); }

/**---------------------------------------------------------------------------
 * \brief Hex dump in error level
 */
#define LOGAPI_HEXDUMP_RAW_ERROR( data, size ) \
    LOGAPI_HEXDUMP_RAW( LOGAPI_ERROR, data, size )
/**---------------------------------------------------------------------------
 * \brief Hex dump in warning level
 */
#define LOGAPI_HEXDUMP_RAW_WARN( data, size ) \
    LOGAPI_HEXDUMP_RAW( LOGAPI_WARN, data, size )
/**---------------------------------------------------------------------------
 * \brief Hex dump in info level
 */
#define LOGAPI_HEXDUMP_RAW_INFO( data, size ) \
    LOGAPI_HEXDUMP_RAW( LOGAPI_INFO, data, size )
/**---------------------------------------------------------------------------
 * \brief Hex dump in trace level
 */
#define LOGAPI_HEXDUMP_RAW_TRACE( data, size ) \
    LOGAPI_HEXDUMP_RAW( LOGAPI_TRACE, data, size )


#if defined( LOGAPI_ENABLE_DEBUG )
    #define DBG_HEXDUMP_RAW         LOGAPI_HEXDUMP_RAW
    #define DBG_HEXDUMP_RAW_ERROR   LOGAPI_HEXDUMP_RAW_ERROR
    #define DBG_HEXDUMP_RAW_WARN    LOGAPI_HEXDUMP_RAW_WARN
    #define DBG_HEXDUMP_RAW_INFO    LOGAPI_HEXDUMP_RAW_INFO
    #define DBG_HEXDUMP_RAW_TRACE   LOGAPI_HEXDUMP_RAW_TRACE
#else
    #define DBG_HEXDUMP_RAW(...)        LOGAPI_EMPTYSTMT
    #define DBG_HEXDUMP_RAW_ERROR(...)  LOGAPI_EMPTYSTMT
    #define DBG_HEXDUMP_RAW_WARN(...)   LOGAPI_EMPTYSTMT
    #define DBG_HEXDUMP_RAW_INFO(...)   LOGAPI_EMPTYSTMT
    #define DBG_HEXDUMP_RAW_TRACE(...)  LOGAPI_EMPTYSTMT
#endif

/*============================================================================
* Misc. API
*============================================================================*/

/**---------------------------------------------------------------------------
 * \brief Platform-specific system information dump implementation
 * \note Use LOGAPI_DUMP_SYS_INFO() macro
 */
LOGAPI_PUBLIC void LogAPI_os_dump_sysinfo( LogAPI_Levels log_level );

/**---------------------------------------------------------------------------
 * \brief Dump system information at info level
 */
#define LOGAPI_DUMP_SYS_INFO() \
    { LogAPI_os_dump_sysinfo( LOGAPI_INFO ); }


/**---------------------------------------------------------------------------
 * \brief Platform-specific callstack dump
 * \note Use LOGAPI_PRINT_CALLSTACK() macro
 */
LOGAPI_PUBLIC void LogAPI_os_print_callstack( LogAPI_Levels log_level );


#define LOGAPI_PRINT_CALLSTACK( level ) \
    { LogAPI_os_print_callstack( level ); }

/**---------------------------------------------------------------------------
 * \brief LOGAPI_PRINT_CALLSTACK( LOGAPI_ERROR ) and abort()
 * \note Use LOGAPI_VERBOSE_ABORT() macro
 */
LOGAPI_PUBLIC void LogAPI_AbortWithCallstack();


#define LOGAPI_VERBOSE_ABORT() \
    { LogAPI_AbortWithCallstack(); }



/**---------------------------------------------------------------------------
 * \brief Expand to statement, if LogAPI debug is enabled
 */
#if defined( LOGAPI_ENABLE_DEBUG )
#   define LOGAPI_WHEN_DEBUG( stmt )    stmt
#else
/** \note don't use LOGAPI_EMPTYSTMT as this macro can be used in very hackish
 * form like "if ( LOGAPI_WHEN_DEBUG( test1 && ) test2 )"*/
#   define LOGAPI_WHEN_DEBUG( stmt )
#endif

/*-----------------------------------*/
#if defined( __cplusplus )
} /* extern "C" */
#endif

/*--*/
