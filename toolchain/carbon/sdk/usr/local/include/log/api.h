/**
 *-----------------------------------------------------------------------------
 *
 *    Copyright (c) 2013 Verifone, Inc
 *
 *-----------------------------------------------------------------------------
 * \brief InFusion Logging API: include all
 * \author Andrey Galkin
 *-----------------------------------------------------------------------------
*/
#pragma once

/*--*/
#include "log/_common.h"
#include "log/_printf.h"

#if defined( __cplusplus )
#   include "log/_stream.hpp"
#endif

#include "log/_assert.h"
#include "log/_hint.h"
#include "log/_module.h"

// bytes: major.minor.build
#define LOGAPI_VERSION 0x010001

/*--*/
