/*
 *	COPYRIGHT (C) 2015 by VeriFone, Inc.  All rights reserved.
 *
 *	Filename:		        svc_printer.h
 *	Module Description:		(internal) Printer Service Library
 *	Component of:			Printer
 *	Authors(s):			    SMF
 *	Function:			    User API library that interfaces to WDE XML application or C application.
 *
 */

 /***************************************************************************
  *
  *	#include files required to compile this source file here
  *
  ***************************************************************************/
#ifndef SVC_PRINTER_H
#define SVC_PRINTER_H

#include "svcmgrSvcDef.h"

#ifdef __cplusplus
extern "C" {
#endif

/*SVC_SERVICE:printer*/

/**
 * @name Printer Paper Cut Modes
*/
/** \{ */
#define PRINTER_NO_CUT		0		/**< Do not cut paper */
#define PRINTER_FULL_CUT	1		/**< Fully cut paper */
#define PRINTER_PARTIAL_CUT	2		/**< Partially cut paper */
/** \} */


/**
 * @name Print Options (bit field encoded)
*/
/** \{ */
#define PRINTER_NO_OPTIONS			0	/**< No options selected (0x00) */
#define PRINTER_DELETE_FILE			1	/**< Delete the specified file once printing is complete (0x01) */
#define PRINTER_FEED_CUTTER			2	/**< Feed paper after print to clear cutter (0x02) */
/** \} */


/*SVC_STRUCT*/
/**  Returned Printer Status Structure
 */
struct  printerStatus_s {
	int outOfPaper; /**< indicates if printer is out of paper (non-zero) */
	int overTemp; /**< printer in overtemp condition (non-zero) */
	int internalError; /**< Internal printer error (non-zero, service required) */
	int cutterError; /**< Indicates a cutter error and the error type (non-zero) */
	int voltageError; /**< Indicates a voltage error (too low (low battery), or too high */
	unsigned long int flags; /**< for future additional status flags */
};


/** Obtain the version of the RMP service
 * @return version struct defined in svcmgrSvcDef.h
 */
/*SVC_PROTOTYPE*/ struct version printer_getVersion(void);


/** Print a Microsoft BMP file
 * 
 * @param[in] filename - bmp file to be printed (with pathing)
 * @param[in] cutMode - paper cut options (see defines above for Printer Paper Cut Options)
 * @param[in] options - print options (see defines above for Print Options)
 *
 * @return 
 * @li 0 Success
 * @li -1 Error
 *
 * <b> Errno Values:</b>
 * @li ENOENT - Error opening bmp file
 * @li EBADF - Error reading bmp file (bad bmp format)
 * @li EPIPE - Error writing to FIFO
 * @li ENOMEDIUM - Out of Paper Error
 * @li EIO - Internal Error (malloc failed, bad parameters, etc)
 * @li EOVERFLOW - Over Temperature Error
 * @li ECOMM - Cutter Error
 * @li ENOANO - Voltage error (power too low (or too high) to print)
 */
/*SVC_PROTOTYPE*/ int printer_printMSBMPFile(char *filename, int cutMode, int options);


/*SVC_STRUCT*/
/**
 * structure for handle binary image data
 */
struct binaryImageData {
	void *data; /**< space holding image data */
	int   data_count; /**< size of image data (in bytes) */
};


/** Print a Microsoft BMP image
 *
 * @param[in] imageData - structure containing reference to image data and size
 * @param[in] filename - (with pathing) if specified (not NULL), save image data to file (allows future reprinting w/o re-downloading image data)
 * @param[in] cutMode - paper cut options (see defines above for Printer Paper Cut Options)
 * @param[in] options - print options (see defines above for Print Options)
 *
 * Notes:
 * @li If filename == NULL, then a internal temporary file is used (deleted after print completes).
 * @li filename should contain a directory where application and sys2 has access (/tmp is recommended).
 *
 * @return
 * @li 0 Success
 * @li -1 Error
 *
 * <b> Errno Values:</b>
 * @li EINVAL - Invalid passed parameters
 * @li EPIPE - Error writing to FIFO
 * @li ENOMEDIUM - Out of Paper Error
 * @li EIO - Internal Error (malloc failed, bad parameters, etc)
 * @li EOVERFLOW - Over Temperature Error
 * @li ECOMM - Cutter Error
 * @li ENOANO - Voltage error (power too low (or too high) to print)
 */
/*SVC_PROTOTYPE*/ int printer_printBMPData(struct binaryImageData imageData, char *filename, int cutMode, int options);


/**
 * @name Font Face
*/
/** \{ */
#define FONT_FACE_VERA			1	/**< Vera type face (default) */
#define FONT_FACE_UTAHOTS		2	/**< Utah OTS type face */
/** \} */


/**
 * @name Font Weight
 * Hairline
 * Thin
 * Ultra-light
 * Extra-light
 * Light
 * Book
 * Normal / regular / plain
 * Medium
 * Demi-bold / semi-bold
 * Bold
 * Extra-bold / extra
 * Heavy
 * Black
 * Extra-black
 * Ultra-black / ultra
*/
/** \{ */
#define FONT_WEIGHT_HAIRLINE	1	/**< Currently none installed */
#define FONT_WEIGHT_THIN		2	/**< Currently none installed */
#define FONT_WEIGHT_ULTRA_LIGHT	3	/**< Currently none installed */
#define FONT_WEIGHT_EXTRA_LIGHT	4	/**< Currently none installed */
#define FONT_WEIGHT_LIGHT		5	/**< Currently none installed */
#define FONT_WEIGHT_BOOK		6	/**< Currently none installed */
#define FONT_WEIGHT_NORMAL	 	7
#define FONT_WEIGHT_MEDIUM		8	/**< Currently none installed */
#define FONT_WEIGHT_DEMI_BOLD	9	/**< Currently none installed */
#define FONT_WEIGHT_BOLD		10
#define FONT_WEIGHT_HEAVY		11	/**< Currently none installed */
#define FONT_WEIGHT_BLACK		12	/**< Currently none installed */
#define FONT_WEIGHT_EXTRA_BLACK	13	/**< Currently none installed */
#define FONT_WEIGHT_ULTRA_BLACK	14	/**< Currently none installed */
/** \} */

/**
 * @name Font Style
*/
/** \{ */
#define FONT_STYLE_NORMAL		1	/**< Default */
#define FONT_STYLE_ITALIC		2
#define FONT_STYLE_OBLIQUE		3 	/**< Currently none installed */
/** \} */


/*SVC_STRUCT*/
/**  Font Attributes Structure
 */
struct fontAttributes {
	int face;	/**< face of the font: FONT_FACE_VERA, FONT_FACE_UTAHOTS, ... (Default: FONT_FACE_VERA) */
	int weight;	/**< FONT_WEIGHT_NORMAL, FONT_WEIGHT_BOLD, ... (Default: FONT_WEIGHT_NORMAL) */
	int style; 	/**< FONT_STYLE_NORMAL, FONT_STYLE_ITALIC, ... (Default: FONT_STYLE_NORMAL) */
};


/** Set default font
 *
 * @param[in] font - font attributes to be applied as default font for text printing
 * @param[in] fontSize - size of the font (Default: 4, min = 2, max = 18)
 *
 * @return
 * @li 0 Success default font changed to that specified
 * @li -1 Error, default font set back to original default
 *
 * Note:
 *   The original default font is: FONT_FACE_VERA, FONT_WEIGHT_NORMAL, FONT_STYLE_NORMAL, 4
 *
 * <b> Errno Values:</b>
 * @li EINVAL - Invalid values passed within structure (not a supported font) and/or fontSize
 * @li EBADF - Error specified font not found
 * @li ERANGE - font size not supported by specified font
 */
/*SVC_PROTOTYPE*/ int printer_setDefaultFont(struct fontAttributes font/*REQ*/, int fontSize/*REQ*/);


/** Print a text string (must be NULL terminated)
 *
 * @param[in] text - pointer to text string to be printed
 * @param[in] cutMode - paper cut options (see defines above for Printer Paper Cut Options)
 * @param[in] useDefaultFont - if non-zero, ignore font and fontSize params and use the current default font (see printer_setDefaultFont())
 * @param[in] font - font attributes to be applied when printing (If useDefault == 0)
 * @param[in] fontSize - size of the font (Default: 4, min = 2, max = 18) (If useDefault == 0)
 *
 *	Notes:
 *	@li Due to processing constraints, a CRLF will be inserted in text every 320 characters.
 *	@li If unable to set to new font/size specified, prior font and size will be restored.
 *
 * @return
 * @li 0 Success
 * @li -1 Error
 *
 * <b> Errno Values:</b>
 * @li EINVAL - Invalid text (Can't be null pointer) or invalid font attributes, etc
 * @li EMSGSIZE - text too big (must be < 2048 characters)
 * @li EBADF - Error specified font not found
 * @li ERANGE - font size not supported by specified font
 * @li ENOENT - Error invalid font attributes or font not found
 * @li EPIPE - Error writing to FIFO
 * @li ENOMEDIUM - Out of Paper Error
 * @li EIO - Internal Error (malloc failed, bad parameters, etc)
 * @li EOVERFLOW - Over Temperature Error
 * @li ECOMM - Cutter Error
 * @li ENOANO - Voltage error (power too low (or too high) to print)
 */
/*SVC_PROTOTYPE*/ int printer_printText(char *text/*REQ*/, int cutMode/*0*/, int useDefaultFont/*1*/, struct fontAttributes font, int fontSize);


/** Print Horizontal Separator line
 *
 * @param[in] width - pixel width of separator
 * @param[in] inverse - 0 print white separator (can act as form feed), 1 print in black
 * @param[in] cutMode - paper cut options (see defines above for Printer Paper Cut Options)
 *
 * @return
 * @li 0 Success
 * @li -1 Error
 *
 * <b> Errno Values:</b>
 * @li ENOENT - Error invalid width and/or inverse
 */
/*SVC_PROTOTYPE*/ int printer_printSeparator(int width/*REQ*/, int inverse/*0*/, int cutMode/*0*/);


/** Get current paper status - non-blocking
 *
 * @return 
 * @li struct printerStatus_s - only valid if errno = 0
 * @li on Error - errno != 0
 *
 * On error return, errno:\n
 * <b>Errno values</b>:
 * @li EBADF - Failure when requesting printer status
 */
/*SVC_PROTOTYPE*/ struct  printerStatus_s printer_getStatus(void);


/** Returns printerStatus when a change occurs - blocking
 * 
 * Setting flags=0 will perform the default action of retrieving the next non-previously
 * retrieved event, blocking if needed waiting for the occurrence of the next event.
 *
 * @param[in] flags Specify blocking/non-blocking/cancel/purge/last (see flags below)
 * @param[in] timeout (0 = wait forever) - max time in seconds to wait for event.
 *
 * @return 
 * @li >= 0 - paper status:
 * @li    0 out of paper
 * @li    1 paper present
 * @li -1 Error - Unable to complete request
 *
 * @note
 * flags:
 * @li BLOCK      (0x00) - Get event, if no event block, waiting for event
 * @li NONBLOCK   (0x01) - Get event, don't block, return empty if no event
 * @li LAST       (0x02) - Get the last event that was received (and previously read)
 * @li CANCEL     (0x04) - Cancel/unblock any process waiting on this event key
 * @li PURGE      (0x08) - Purge/flush and pending events and return last event read
 *
 * other:
 * @li If data != NULL, user/caller is responsible to free data (malloc'd memory).
 * @li Max data size is 4000 bytes
 * @li if flags & GETEVENT_PURGE, max data size is 2048 bytes
 * @li GETEVENT_PURGE will purge any pending events and return the last (newest) event
 * @li GETEVENT_PURGE|GETEVENT_NONBLOCK will return right away with errno=ENODATA if no events pending
 * @li GETEVENT_PURGE and GETEVENT_NONBLOCK not specified, will wait for event if no event pending
 * @li timeout parameter only used if flags == GETEVENT_BLOCK
 * @li Upon timeout, errno == EINTR (same as cancel), and event.event == ENOENT
 *
 * <b>Errno Values:</b>
 *   EINTR  - event_getEvent() canceled (by another thread/user). event field of returned
 *            structure may contain cause (see event_cancelWithCause())
 *   EBADF - failure communicating to printer hardware to get status.
 *   see event_getEvent() in svc_event.h for other possible errno values
 */
/*SVC_PROTOTYPE*/ struct  printerStatus_s printer_statusGetEvent(int flags, int timeout);



#ifdef __cplusplus
}
#endif

#endif //SVC_PRINTER_H
