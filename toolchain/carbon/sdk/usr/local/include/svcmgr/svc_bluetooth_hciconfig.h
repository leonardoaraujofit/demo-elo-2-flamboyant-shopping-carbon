#ifndef SVC_BLUETOOTH_HCICONFIG_H
#define SVC_BLUETOOTH_HCICONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

/*SVC_STRUCT*/
/**
 *  Local advanced Bluetooth configuration structure
 */
struct btHciInfo
{
   unsigned char auth;
   unsigned char encrypt;
   unsigned char scan;			/** PSCAN ISAN PISCAN or NOSCAN */
   unsigned char afh_mode;	
   unsigned char ssp_mode;
   unsigned int ptype;			/** DM1 DM3 DM5 DH1 DH3 DH5 HV1 HV2 HV3 2-DH1 2-DH3 2-DH5 3-DH1 3-DH3 3-DH5*/
   unsigned char link_mode;		/** NONE ACCEPT	MASTER AUTH ENCRYPT TRUSTED RELIABLE  SECURE */
   unsigned char link_policy;	/** NONE RSWITCH HOLD SNIFF PARK*/
   unsigned int dev_class;		/** Info on SERVICE_CLASS MINOR_DEVICE_NAME */
   unsigned int voice;			
   unsigned int iac;			/** between LIAC - 0x9e8b00 and GIAC- 0x9e8b33*/
   unsigned char inqtpl;		/** value of the transmit power level */ 
   unsigned char inq_mode;		/** STANDARD RSSI EXTENDED UNKNOWN */
   unsigned char inq_type;		/** INTERLACED STANDARD */
   unsigned int inq_parms;		/** contains inquiry interval and inquiry window */
   unsigned int page_parms;		/** cotains page interval and page window*/
   unsigned int page_to;		/** Page timeout */
};



	  
#define SVC_BT_NAME(x)			((x).name)

#define SVC_BT_IS_AUTH_EN(x)	(((x).auth) ? (1) : (0))
#define SVC_BT_IS_ENCR_EN(x)	(((x).encrypt) ? (1) : (0))

#define SVC_BT_NOSCAN	0x00
#define SVC_BT_ISCAN	0x01
#define SVC_BT_PSCAN	0x02
#define SVC_BT_PISCAN	(SVC_BT_ISCAN | SVC_BT_PSCAN)
#define SVC_BT_IS_PISCAN(x)	(((x).scan & SVC_BT_PISCAN) ? (1) : (0))
#define SVC_BT_IS_PSCAN(x)	(((x).scan & SVC_BT_PSCAN) ? (1) : (0))
#define SVC_BT_IS_ISCAN(x)	(((x).scan & SVC_BT_ISCAN) ? (1) : (0))
#define SVC_BT_IS_NOSCAN(x)	((x).scan ? (0) : (1))

#define SVC_BT_IS_AFH_EN(x)	(((x).afh_mode) ? (1) : (0))
#define SVC_BT_IS_SSP_EN(x)	(((x).ssp_mode) ? (1) : (0))

#define SVC_BT_PTYPE_DM1	(1<<0)
#define SVC_BT_PTYPE_DM3	(1<<1)
#define SVC_BT_PTYPE_DM5	(1<<2)
#define SVC_BT_PTYPE_DH1	(1<<3)
#define SVC_BT_PTYPE_DH3	(1<<4)
#define SVC_BT_PTYPE_DH5	(1<<5)
#define SVC_BT_PTYPE_HV1	(1<<6)
#define SVC_BT_PTYPE_HV2	(1<<7)
#define SVC_BT_PTYPE_HV3	(1<<8)
#define SVC_BT_PTYPE_2_DH1	(1<<9)
#define SVC_BT_PTYPE_2_DH3	(1<<10)
#define SVC_BT_PTYPE_2_DH5	(1<<11)
#define SVC_BT_PTYPE_3_DH1	(1<<12)
#define SVC_BT_PTYPE_3_DH3	(1<<13)
#define SVC_BT_PTYPE_3_DH5	(1<<14)

#define SVC_BT_LMODE_NONE		(0<<0) 
#define SVC_BT_LMODE_ACCEPT		(1<<0)
#define SVC_BT_LMODE_MASTER		(1<<1)
#define SVC_BT_LMODE_AUTH		(1<<2)
#define SVC_BT_LMODE_ENCRYPT	(1<<3)
#define SVC_BT_LMODE_TRUSTED	(1<<4)
#define SVC_BT_LMODE_RELIABLE	(1<<5)
#define SVC_BT_LMODE_SECURE  	(1<<7)

#define SVC_BT_LPOLICY_NONE		(1<<0)	
#define SVC_BT_LPOLICY_RSWITCH  (1<<1)
#define SVC_BT_LPOLICY_HOLD	    (1<<2)
#define SVC_BT_LPOLICY_SNIFF	(1<<3)
#define SVC_BT_LPOLICY_PARK	    (1<<4)

#define SVC_BT_CLASS_SRV_POSITIONING		(1<<0)
#define SVC_BT_CLASS_SRV_NETWORKING			(1<<1)
#define SVC_BT_CLASS_SRV_RENDERING			(1<<2)
#define SVC_BT_CLASS_SRV_CAPTURING			(1<<3)
#define SVC_BT_CLASS_SRV_OBJECT_TRANSFER	(1<<4)
#define SVC_BT_CLASS_SRV_AUDIO				(1<<5)
#define SVC_BT_CLASS_SRV_TELEPHONY			(1<<6)
#define SVC_BT_CLASS_SRV_INFORMATION 		(1<<7)

#define SVC_BT_CLASS_MAJOR_MISCELLANEOUS	(0<<0)
#define SVC_BT_CLASS_MAJOR_COMPUTER         (1<<0)
#define SVC_BT_CLASS_MAJOR_PHONE            (1<<1)
#define SVC_BT_CLASS_MAJOR_LAN_ACCESS       (1<<2)
#define SVC_BT_CLASS_MAJOR_AUDIO_VIDEO      (1<<3)
#define SVC_BT_CLASS_MAJOR_PERIPHERAL       (1<<4)
#define SVC_BT_CLASS_MAJOR_IMAGING          (1<<5)
#define SVC_BT_CLASS_MAJOR_UNCATEGORIZED    (1<<6)

#define SVC_BT_CLASS(s, m)				((s << 16) | (m << 8))
#define SVC_BT_CLASS_HAS_SRV(x,y)		((((x).dev_class>>16) & y) ? (1) : (0))
#define SVC_BT_CLASS_HAS_MAJOR(x,y)		((((x).dev_class>>8) & y) ? (1) : (0))

#define SVC_BT_GIAC 	0x9e8b33
#define SVC_BT_LIAC		0x9e8b00



#define SVC_BT_INQ_MODE_STANDARD	0x00
#define SVC_BT_INQ_MODE_WITH_RSSI	0x01
#define SVC_BT_INQ_MODE_EXTENDED	0x02
#define SVC_BT_INQ_MODE_UNKNOWN		0x04

#define SVC_BT_INQ_TYPE_INTERLACED(x)	((x).inq_type ? (1) : (0))
#define SVC_BT_INQ_TYPE_STANDARD(x)		((x).inq_type ? (0) : (1))

#define SVC_BT_INTERVAL_MASK	0xFFFF0000
#define SVC_BT_WINDOW_MASK		0x0000FFFF

#define SVC_BT_INQ_INTERVAL(x)		(((x).inq_parms & SVC_BT_INTERVAL_MASK) >> 16)
#define SVC_BT_INQ_WINDOW(x)		(((x).inq_parms & SVC_BT_WINDOW_MASK))

#define SVC_BT_PAGE_INTERVAL(x)		(((x).page_parms & SVC_BT_INTERVAL_MASK) >> 16)
#define SVC_BT_PAGE_WINDOW(x)		(((x).page_parms & SVC_BT_WINDOW_MASK))

#ifdef __cplusplus
}
#endif

#endif //SVC_BLUETOOTH_HCICONFIG_H
