#ifndef SVC_NET_H
#define SVC_NET_H

#include "svcmgrSvcDef.h"

#ifdef __cplusplus
extern "C" {
#endif

/*SVC_SERVICE:net*/

/**
 * @name Default network configuration file for wpa_supplicant
*/
/** \{ */
#define	NET_WPA_SUPPLICANT_DEFAULT_CONFIG_FILE "/mnt/flash/etc/config/svcnet/wpa-supplicant.conf" /**< wpa_supplicant default network configuration file location */
#define NET_WPA_SUPPLICANT_DEFAULT_KEY_MANAGEMENT "WPA-PSK" /**< key management default network configuration value **/
#define NET_WPA_SUPPLICANT_DEFAULT_PAIRWISE "CCMP" /**< pairwise cipher default network configuration value **/
#define NET_WPA_SUPPLICANT_DEFAULT_GROUP "CCMP" /**< Group cipher default network configuration value **/
#define NET_WPA_SUPPLICANT_DEFAULT_PROTO "RSN" /**< Protocol default network configuration value **/
/** \} */


/**
 * @name Define XML default file
*/
/** \{ */
#define	NET_XML_DEFAULT "/mnt/flash/etc/config/svcnet/netconf.xml" /**< XML default file location */
#define NET_XML_DEFAULT_DEFAULT "/mnt/flash/etc/config/svcnet/netconfdef.xml" /**< Use to update XML default file location */
/** \} */

/**
 * @name Parameter values for ipVersion passed to net_startDHCP, net_renewDHCP, net_releaseDHCP, ...
*/
/** \{ */
#define	NET_IP_V4   1 /**< IPv4 is requested */
#define	NET_IP_V6   2 /**< IPv6 is requested */
/** \} */

#define IPV6_CHAR_EXTENDED_SIZE 40 //for holding xxxx:xxxx:xxxx:xxxx:xxxx:xxxx:xxxx:xxxx
#define IPV6_FILE_SUFIX  "_ipv6"
#define IPV6_MASK_MAX_STRING (5) // for holding /128 for ex
#define IPV6_MASK_MIN_VAL  (0) /* mask min value accepted by Linux */
#define IPV6_MASK_MAX_VAL  (128) /* mask max value accepted by Linux */
#define IPV6_IP_GROUP_COUNT  (8) /* Number of digit groups in a full IP */
#define IPV6_IP_GROUP_LENGTH  (4)  /* Number of digits in an IP group */
#define IPV6_DEF_ROUTE_METRIC "0" /*value for the metric for the default route; used when the gateway is read from the file /tmp/net/gateway; TODO check why they used value + 1 in pub_route. Normaly value should be 1 here*/
#define IPV6_METRIC_DEFAULT "default" /* In case no special metric is needed */

#define IPV4_AND_IPV6_FILE_SUFIX  "_all"
#define USR_SBIN_PATH   "/usr/sbin/"
#define USR_LOCAL_SBIN_PATH   "/usr/local/sbin/"
#define SYSCTL_CHECK_INTERFACE_PRESENT_PATH "/proc/sys/net/ipv6/conf/"
#define SYSCTL_DISABLE_IPV6_FILE "/disable_ipv6"
#define IPV6_LINK_AUTO         "auto" /* Indicate if the link local is configured automatically */

/**
 * @name possible values returned by net_pppGetExitStatus function
*/
/** \{ */
#define NET_PPP_EXIT_OK	                 0
#define NET_PPP_EXIT_FATAL_ERROR	     1
#define NET_PPP_EXIT_OPTION_ERROR	     2
#define NET_PPP_EXIT_NOT_ROOT	         3
#define NET_PPP_EXIT_NO_KERNEL_SUPPORT	 4
#define NET_PPP_EXIT_USER_REQUEST	     5
#define NET_PPP_EXIT_LOCK_FAILED	     6
#define NET_PPP_EXIT_OPEN_FAILED	     7
#define NET_PPP_EXIT_CONNECT_FAILED	     8
#define NET_PPP_EXIT_PTYCMD_FAILED	     9
#define NET_PPP_EXIT_NEGOTIATION_FAILED	10
#define NET_PPP_EXIT_PEER_AUTH_FAILED	11
#define NET_PPP_EXIT_IDLE_TIMEOUT	    12
#define NET_PPP_EXIT_CONNECT_TIME	    13
#define NET_PPP_EXIT_CALLBACK	        14
#define NET_PPP_EXIT_PEER_DEAD	        15
#define NET_PPP_EXIT_HANGUP	            16
#define NET_PPP_EXIT_LOOPBACK	        17
#define NET_PPP_EXIT_INIT_FAILED	    18
#define NET_PPP_EXIT_AUTH_TOPEER_FAILED	19
/** \} */

/**
 * @name svc net max sizes
*/
/** \{ */
#define	NET_IFACE_MAX          32 /**< Interface name max size */
#define	NET_NAMEID_MAX         32 /**< XML entry name max size */
#define	NET_IPV4_MAX           16 /**< IPv4 max length */
#define	NET_IPV6_MAX           48 /**< IPv6 max length */
#define	NET_MAC_MAX            17 /**< MAC address max length */
#define	NET_DHID_MAX           16 /**< DHCP id max length */
#define	NET_HOST_MAX          128 /**< DHCP id max length */
#define	NET_SPEED_MAX           8 /**< Ethernet speed parameter max length */
#define	NET_PROTO_MAX           8 /**< Protocol name max size */
#define	NET_PPP_USER_MAX       32 /**< PPP username max length */
#define	NET_PPP_PASSWD_MAX     32 /**< PPP password max length */
#define	NET_PPP_AUTH_MAX        8 /**< PPP authentication mode max length */
#define	NET_PPP_OPTION_MAX    128 /**< PPP options max length */
#define	NET_WIFI_WPACONF_MAX  128 /**< WPA supplicant configuration file max length */
#define NET_WIFI_SSID_MAX      64 /**< ssid string max length */
#define	NET_WIFI_ENCTYPE_MAX   32 /**< WIFI encryption type max size */
#define	NET_WIFI_AUTHMODE_MAX  32 /**< WIFI authentication mode max size */
#define NET_WIFI_FW_NAME_MAX   256 /**< Wifi fw name max length */
#define	NET_MODEM_NUMBER_MAX   32 /**< Modem phone number max size */
#define	NET_OPTION_MAX        256 /**< Option list max size */
#define	NET_GPRS_NUMBER_MAX         32 /**< GPRS phone number max size */
#define	NET_GPRS_BEARER_MAX         32 /**< GSM Bearer max size */
#define	NET_GPRS_OPERATOR_MAX       16 /**< Max size of an operator's name */
#define NET_MDM_ISDN_NUMBER_MAX_LEN	50 /**< Modem ISDN number max size */
#define NET_MDM_X25_PACKET_SIZE_LEN	 5 /**< Modem ISDN x25 packet max number */
#define NET_MDM_FACILITIES_MAX_LEN	20 /**< Modem ISDN Facilities max size */ 
#define NET_MDM_NUI_MAX_LEN			20 /**< Modem ISDN NUI max size */ 
#define NET_MDM_X25_NUMBER_MAX_LEN	20 /**< Modem ISDN X25 Number max size */ 
#define NET_MDM_USER_DATA_MAX_LEN	20 /**< Modem ISDN User data max size */ 
#define NET_SATNAV_DATA_MAX  	   247//320 /**< Sat Nav maximum data size */
#define NET_MCC_SIZE_MAX	         4 /**< Mobile Country Code max size */
#define NET_MNC_SIZE_MAX 	         4 /**< Mobile Network Code max size */
#define NET_LAC_SIZE_MAX 	         5 /**< Location Area Code max size */
#define NET_CELL_SIZE_MAX 	         5 /**< Cell id max size */
#define NET_CELL_3G_SIZE_MAX 	 8 /**< 3G Cell id max size */
#define NET_BSIC_SIZE_MAX 	         3 /**< Base Station ID Code max size */
#define NET_CHANN_SIZE_MAX 	         5 /**< ARFCN (Absolute Frequency Cannel Number) max size */
#define NET_RSSI_SIZE_MAX 	         4 /**< Received signal level max size */
#define NET_C1_SIZE_MAX 	         4 /**< Coefficient for BT reselection max size */
#define NET_C2_SIZE_MAX 	         4 /**< Coefficient for BT reselection max size */
/** \} */

/**
 * @name values for networkType in netWifiInfo & netSiteSurvey
*/
/** \{ */
#define	NET_WIFI_NETWORK_MANAGED 1
#define	NET_WIFI_NETWORK_ADHOC   2 
/** \} */


/**
 * @name values for speed in netIfconfig structure
*/
/** \{ */
#define	NET_ETH_AUTO 0 /**<Auto speed negociation*/
#define	NET_ETH_100F 1 /**<100Mb full duplex */
#define	NET_ETH_10F  2  /**<10Mb full duplex */
#define	NET_ETH_100H 3 /**<100Mb half duplex */
#define	NET_ETH_10H  4 /**<10Mb half duplex */
/** \} */

/**
 * @name Parameter values for comtype passed to net_sessionSetTag, net_sessionGetTag
*/
/** \{ */
#define	NETCOM_SERLINK      0 /**< Serial communication type */
#define	NETCOM_MODLINK      1 /**< Modem communication type */
#define	NETCOM_PPPPROTO     2 /**< PPP communication type */
#define	NETCOM_ETHLINK      3 /**< Ethernet communication type */
#define	NETCOM_WIFI         4 /**< WIFI communication type */
#define	NETCOM_SOCKPROTO    5 /**< Socket communication type */
#define	NETCOM_SSL          6 /**< SSL communication type */
#define	NETCOM_SESSION      7 /**< Session communication type */
#define	NETCOM_OPTION       8 /**< Option communication type */
#define	NETCOM_GPRS         9 /**< GPRS communication type */
#define	NETCOM_MODISDNLINK 10 /**< UX ISDN communication type */
#define	NETCOM_BTLINK      11 /**< Bluetooth communication type */

#ifdef SVC_USE_IPV6
/*
 * It is used in conjunction with g_strDefaultMediaList from privnet_xmlcom and is hardcoded
 * in the sense the tags must match the order from there, they are not use to make the init of that vector.
 */
#define NETCOM_ETHLINK_IPV6   (12) /**< IPv6 Ethernet communication type */
#define NETCOM_WIFI_IPV6   (13) /**< IPv6 WIFI communication type */
#endif

#define NETCOM_NONE     14 /**< No communication type was defined  */
#define NETCOM_MAX      15 /**< Maximum number of communication types supported by the net service in case of ipv6*/




/** \} */


/**
 * @name maximum number of entries per media
*/
/** \{ */
#define NETCOM_MAX_PERTYPE       10
/** \} */

/**
 * @name NETCOM_CONNECT_TIMEOUT Default socket connection timeout in seconds
*/
/** \{ */
#define NET_CONNECT_TIMEOUT  30 
/** \} */

/**
 * @name Parameter values for sessionType passed to net_sessionConnect
*/
/** \{ */
#define	NET_FIRST_SESSION   1 /**< Connect to the first session */
#define	NET_CURRENT_SESSION 2 /**< Try to reconnect to the current session */
#define	NET_NEXT_SESSION    3 /**< Connect to the next session according to the priority */
/** \} */

/**
 * @name Parameter values for dial_type passed to netModemInfo
 */
/** \{ */
#define	NETMDM_TONE_DIAL            1 
#define	NETMDM_PULSE_DIAL           2 
/** \} */

/**
 * @name Parameter values for mode passed to netModemInfo
 */
/** \{ */
#define	NETMDM_MODE_SYNC            1 
#define	NETMDM_MODE_ASYNC           2 
/** \} */

/**
 * @name Parameter values for error_correction passed to netModemInfo
*/
/** \{ */
#define	NETMDM_ERRCOR_DISABLE       1
#define	NETMDM_ERRCOR_V42_MNP       2
#define	NETMDM_ERRCOR_V42_MNP_ONLY  3
#define	NETMDM_ERRCOR_V42_ONLY      4
#define	NETMDM_ERRCOR_MNP_ONLY      5
/** \} */

/**
 * @name Parameter values for compression passed to netModemInfo
 */
/** \{ */
#define	NETMDM_COMPRESSION_DISABLE   1 
#define	NETMDM_COMPRESSION_V42_MNP   2 /**< error correction must not be disabled */
/** \} */


/**
 * @name values for modem interfaces names
*/
/** \{ */
#define NET_MODEM_PSTN "modem_uart"     /**< Default internal PSTN modem name */
#define NET_MODEM_UXPSTN "modem_uxpstn" /**< UX PSTN Modem name */
#define NET_MODEM_UXISDN "modem_uxisdn" /**< UX ISDN Modem name */
#define NET_GPRS_LAYER "gprs_layer"     /**< GPRS Interface name */
#define NET_PPP_DEFAULT "ppp0"          /**< Default name for PPP interface */
#define NET_PPP_GPRS_DEFAULT "ppp1"     /**< Default name for PPP interface over GPRS */
#define NET_MODEM_BASEPSTN "modem_base" /**< Base vx675 Modem name */
#define NET_ETHERNET_DEFAULT "eth0"     /**< Default ethernet interface */
#define NET_ETHERNET_1 "eth1"           /**< Second Ethernet interface */
#define NET_WIFI_DEFAULT "wlan0"        /**< Default Wifi interface */
#define NET_SERIAL_DEFAULT "serial_port" /**< Default Serial interface */
#define NET_PAN_DEFAULT "bnep0"         /**< Default ethernet over Bluetooth interface */
#define NET_BTBASE_DEFAULT "bt_base0"   /**< Default name for BT base */
#define NET_USBETHERNET_DEFAULT "usb0"  /**< Default name for USB over Ethernet */
#define NET_BRIDGE_DEFAULT "br0"	/**< Default name for Bridges */
/** \} */


/**
 * @name values for modem interfaces names (!!!DEPRICATED)
*/
/** \{ */
#define	NET_MONDEM_PSTN      NET_MODEM_PSTN  /**< (!!!DEPRICATED: Use NET_MODEM_PSTN ) Default internal PSTN modem name */
#define	NET_MONDEM_UXPSTN    NET_MODEM_UXPSTN /**< (!!!DEPRICATED: Use NET_MODEM_UXPSTN ) UX PSTN Modem name */
#define	NET_MONDEM_UXISDN  	 NET_MODEM_UXISDN /**< (!!!DEPRICATED: Use NET_MODEM_UXISDN) UX ISDN Modem name */
#define	NET_MONDEM_BASEPSTN  NET_MODEM_BASEPSTN /**< (!!!DEPRICATED: Use NET_MODEM_BASEPSTN) Base vx675 Modem name */
/** \} */

/**
 * @name Parameter values for reg_mode passed to gprsDescriptor and to roaming_mode in netOperator
 */
/** \{ */
#define	NET_GPRS_REG_AUTO          0 /**< Network Automatic Connection */ 
#define	NET_GPRS_REG_MANUAL        1 /**< Manual connection (Only connect to the specified operator) */
#define	NET_GPRS_REG_AUTO_MANUAL   4 /**< Try manual connection first, if not try automatic connection */
/** \} */



/**
 * @name Possible values returned by net_gprsGetStatus functions
 */
/** \{ */
#define NET_GSM_STT_TURN_OFF			0
#define NET_GSM_STT_OFF					1
#define NET_GSM_STT_INIT				2
#define NET_GSM_STT_IMEI_NEEDED			3
#define NET_GSM_STT_NO_SIM				4
#define NET_GSM_STT_SIM_AUTH_NEEDED		5
#define NET_GSM_STT_IDLE				6
#define NET_GSM_STT_DURING_CONNECT		7
#define NET_GSM_STT_GPRS_CONNECTED		8
#define NET_GSM_STT_CSD_CONNECTED		9
#define NET_GSM_STT_DURING_DISCONNECT	10
#define NET_GSM_STT_GET_SCANNING		11
#define NET_GSM_STT_LOCKED				12
#define NET_GSM_STT_UNKNOWN_ERROR		13
#define NET_GSM_STT_MAX_ERROR   		14
#define NET_GSM_STT_UNKNOWN				99
/** \} */


/**
 * @name Possible values returned by net_gprsGetRadioAccessTechnology function
 */
/** \{ */

#define NET_GSM_OPR_RAT_NOT_CONNECTED	   -1
#define NET_GSM_OPR_RAT_GSM 				0 /**< GSM */
#define NET_GSM_OPR_RAT_GSM_UMTS_DUAL_MODE	1 /**< GSM / UMTS Dual Mode */
#define NET_GSM_OPR_RAT_UTRAN 				2 /**< UMTS */
#define NET_GSM_OPR_RAT_GSM_W_EGPRS			3 /**< GSM with EGPRS */
#define NET_GSM_OPR_RAT_UTRAN_W_HSDPA		4 /**< UMTS with HSDPA */
#define NET_GSM_OPR_RAT_UTRAN_W_HSUPA		5 /**< UMTS with HSUPA */
#define NET_GSM_OPR_RAT_UTRAN_W_HSDPA_HSUPA	6 /**< UMTS with HSDPA / HSUPA */
/** \} */

/**
 * @name Possible values for net_gprsSelectSIM
*/
/** \{ */
#define NET_GSM_SIM_1  1 /**< First SIM Slot */
#define NET_GSM_SIM_2  2 /**< Second SIM slot */
/** \} */

#define NET_GPRS_SIM_READY			0 /* SIM is ready and is "unlocked" */
#define NET_GPRS_SIM_NEEDS_PIN		1 /* PIN is needed, SIM is not ready */
#define NET_GPRS_SIM_NEEDS_PUK		2 /* PUK is needed, SIM is locked */
#define NET_GPRS_SIM_BUSY			3 /* Currently the status cannot be obtained, try again later */
#define NET_GPRS_SIM_NOT_PRESENT	4 /* There is currently no SIM present */

/**
 * @name Possible values for ISDN transmission protocols (isdn_prot)
 */
/** \{ */
#define NET_MDM_PROT_HDLC_SYNC_TO_ASYNC 3 /**< ATB3 */
#define NET_MDM_PROT_HDLC_TRANSPARENT   4 /**< ATB4 */
#define NET_MDM_PROT_X75		  	   10 /**< ATB10 */
#define NET_MDM_PROT_X31_B_CHL		   20 /**< ATB20 */
#define NET_MDM_PROT_X31_D_CHL		   21 /**< ATB21 */
/** \} */

/**
 * @name Possible values for eventType in netEvent struct
 */
/** \{ */
#define NET_EVENT_NONE               0  
#define NET_EVENT_INTERFACE_ONLINE   1 /**< Physical link on. Example Cable plugged */
#define NET_EVENT_INTERFACE_OFFLINE  2 /**< Physical link on. Example Cable unplugged */
#define NET_EVENT_INTERFACE_UP       3 /**< Network established. Example Ethernet Up */
#define NET_EVENT_INTERFACE_DOWN     4 /**< Network disconnected. Example Ethernet Down */		
#define NET_EVENT_INTERFACE_ENABLE   5 /**< Interface on (WIFI on) */	
#define NET_EVENT_INTERFACE_DISABLE  6 /**< Interface on (WIFI off) */	
/** \} */



/**
 * @name values for event interfaces names
*/
/** \{ */
#define	NET_EVENT_MODEM   "modem" /**< Default internal PSTN modem name */
#define	NET_EVENT_GSM_CSD "gsm"  /**< GSM CSD Interface name */
#define	NET_EVENT_GPRS    "gprs"  /**< GPRS Interface name */
/** \} */


/**
 * @name Define the brigde default name
*/
/** \{ */
#define	NET_BRIDGE_NAME "netbridge" /**< Bridge default name */
/** \} */


/**
 * @name Define usb gadget config file (should be used by cibloader at boot)
*/
/** \{ */
#define NET_USB_CONF_FILE "/mnt/flash/etc/config/svcnet/usbgadget.conf"
/*
 * !!!!!!!!!!!!!!!!!
 * This is done to support legacy code in raptor, the usage of this files is now
 * DEPRECATED
 * !!!!!!!!!!!!!!!!!
 */
#ifdef RAPTOR
#define NET_USB_CONF_FILE_READ "/mnt/flash/etc/config/svcnet/usbgadget_loaded.conf"
#else
#define NET_USB_CONF_FILE_READ "/mnt/flash/etc/config/svcnet/usbgadget.conf"
#endif
#define NET_USB_CONF_FILE_MAX 32
/** \} */

/**
 * @name Possible values to load an usb gadget driver.
 */
/** \{ */
#define NET_USB_GADGET_DEFAULT          0 /** choose to load default gadget at boot time (defined by cibloader, gadget serial at the moment) */
#define NET_USB_GADGET_ETHERNET         1 /** choose to load gadget ethernet at boot time */
#define NET_USB_GADGET_SERIAL           2 /** choose to load gadget serial at boot time */
#define NET_USB_GADGET_COMPOSITE        3 /** choose to load gadget cdc (ethernet + serial) at boot time */

#define NET_USB_GADGET_ETHERNET_STR        "ether"
#define NET_USB_GADGET_SERIAL_STR          "serial"
#define NET_USB_GADGET_COMPOSITE_STR       "cdc"

#define NET_USB_GADGET_ACM_STR				"ACM"
#define NET_USB_GADGET_RNDIS_STR			"RNDIS"
#define NET_USB_GADGET_ECM_STR				"ECM"
/** \} */


/**
 * @name Possible values for net_bluetoothPower function
*/
/** \{ */
#define NET_BT_PWR_DOWN   	0  /**< Bluetooth power down 	*/
#define NET_BT_PWR_UP  		1  /**< Bluetooth power up 	*/
#define NET_BT_PWR_SUSPEND  2  /**< Bluetooth suspend 	*/
#define NET_BT_PWR_RESUME  	3  /**< Bluetooth resume	*/
/** \} */

/**
 * @name Possible profiles for Bluetooth survey
*/
/** \{ */
#define NET_BT_SEARCH_OBEX     0x01 /**< OBEX Profile search mask */
#define NET_BT_SEARCH_PAN      0x02 /**< PAN Profile search mask */
#define NET_BT_SEARCH_DUN      0x04 /**< DUN Profile search mask */
#define NET_BT_SEARCH_SP       0x08 /**< Serial Port Profile search mask */
/** \} */

/**
 * @name Maximum number of paired devices
*/
/** \{ */
#define NET_BTPAIR_MAX          4
/** \} */ 

/**
 * @name Possible values for pairing method
*/
/** \{ */
#define NET_BT_PAIRING_TYPE_LEGACY      	0 /**< Legacy PIN pairing method */
#define NET_BT_PAIRING_TYPE_SSP_NO_PASSKEY	1 /**< SSP no pass key pairing method */
#define NET_BT_PAIRING_TYPE_SSP_PASSKEY		2 /**< SSP with passkey pairing method */
#define NET_BT_PAIRING_TYPE_SSP_AUTO		3 /**< SSP with automatic passkey pairing method */
#define NET_BT_PAIRING_TYPE_SSP_CONFIRM		4 /**< SSP with code confirmation */
/** \} */

/**
 * @name Possible values for activate parameter in netIfconfig structure
*/
/** \{ */
#define NET_ACTIVATE_OFF      0 /**< Network interface is not activated */
#define NET_ACTIVATE_ON       1 /**< Network interface is activated and reactivated after reboot or after suspend */
#define NET_ACTIVATE_ON_ONCE  2 /**< Network interface is activated but NOT reactivated after reboot. Meanwhile, the interface is also reactivated after suspend */
/** \} */

/**
 * @name Possible power events to treat in net service
*/
/** \{ */
#define NET_EVENT_SLEEP        0x01 /**< Sleep event */
#define NET_EVENT_WAKE         0x02 /**< Wake-up event */
#define NET_EVENT_LOW_BATTERY  0x03 /**< Low battery event */
#define NET_EVENT_HIGH_BATTERY 0x04 /**< Battery sufficient event */
/** \} */


/**
 * @name Possible MTU values
*/
/** \{ */
#define NET_IPV4_MTU_MIN         68 /**< Minimum MTU value for IPv4 */
#define NET_IPV4_MTU_DEFAULT   1500 /**< Default MTU value for IPv4 */
/** \} */



/**
* @name net debug log level
*/
/** \{ */
#define   NET_LOG_INFO     0x01 
#define   NET_LOG_DEBUG    0x02
#define   NET_LOG_ALL      0x04 
/** \} */


/**
* @name net debug log file location
*/
#define NET_LOG_FILE "/mnt/flash/etc/config/svcnet/netlog.cfg"

/**
 * @name Radio type
*/
/** { */
#define NET_RADIO_BT       (1 << 0)          /** Bluetooth */
#define NET_RADIO_WIFI     (1 << 1)
#define NET_RADIO_CELLULAR (1 << 2)
/** } */

/**
@name Indicate if a Radio is on or off
*/
/** { */
#define NET_RADIO_ERROR  0  /** Radio not present or power state cannot be read */
#define NET_RADIO_ON  1
#define NET_RADIO_OFF 2
/** } */

/**
@name Indicate interface  is up, down or cannot be determined
*/
/** { */
#define NET_INTERFACE_ERROR  0  /**Interface status cannot be read */
#define NET_INTERFACE_UP  1
#define NET_INTERFACE_DOWN 2
#define NET_INTERFACE_UNKNOWN 3
/** } */

/*SVC_STRUCT*/
/**
 * Define an IP interface information
 */
struct netIfconfig
{
	char interface[NET_IFACE_MAX];/**< interface name (eth0, ppp0, wlan0...)*/
	char mac[NET_MAC_MAX+1]; /**<MAC address of the interface (Read Only)*/
	char local_ip[NET_IPV4_MAX]; /**< current interface IP address (IPv4) */
	char broadcast[NET_IPV4_MAX];	/**< broadcast */	
	char netmask[NET_IPV4_MAX]; /**< netmask */
	char gateway[NET_IPV4_MAX]; /**< gateway */
	char dns1[NET_IPV4_MAX];	  /**< dns primary server */
	char dns2[NET_IPV4_MAX];	  /**< dns secondary server */	
	char suppliconf[NET_WIFI_WPACONF_MAX];/**< WPA_Supplicant configuration file for WIFI interfaces */
	int usedhcp;/**< 1=DHCP is used, 0 if not */
	char dhcpid[NET_DHID_MAX];/**< client id string for dhcp (typically ?? ? not specified) */
	char clienthostname[NET_HOST_MAX];/**< client hostname string for dhcp (typically ?? ? not specified) */
	int speed;/**< defines for 10F, 100F, 10T, 100T, AUTO (default is AUTO)  - used to set interface and report current interface speed */	
	int activate;/**< NET_ACTIVATE_ON[_ONCE]=Bring up the interface at boot, NET_ACTIVATE_OFF do nothing */
	char user[NET_PPP_USER_MAX];/**< username for PPP*/
	char password[NET_PPP_PASSWD_MAX];/**< password for PPP*/
	char auth[NET_PPP_AUTH_MAX];/**< authentication mode for PPP (CHAP or PAP)*/
	char pppOption[NET_PPP_OPTION_MAX];/**< PPP options*/	
	char pppPort[NET_IFACE_MAX];/**< link used for PPP (Modem, Serial, GPRS)*/		
};

/*SVC_STRUCT*/
/**
 * Define an IP interface information
 */
struct netIfconfigIpv6
{
	char interface[NET_IFACE_MAX];/**< interface name (eth0, ppp0, wlan0...)*/
	char mac[NET_MAC_MAX+1]; /**<MAC address of interface (Read Only)*/
	char linkLocal[NET_IPV6_MAX]; /**< address on the link of the interface, "if linkLocal is IPV6_LINK_AUTO, the link addr is assigned automatically" */
	unsigned char linkLocalMask;/**< Network Mask of the linkLocal address */
	char siteLocal[NET_IPV6_MAX];	/**< IPv6 unicast site-local address */
	unsigned char siteLocalMask;/**< Network Mask of the siteLocal address */
	char uniqueAddress[NET_IPV6_MAX]; /**< Pv6 unicast Unique address */
	unsigned char uniqueAddressMask;/**< Network Mask of the Unique address */
	char globalAddress[NET_IPV6_MAX];	/**< Routable IPv6 address (public address) */	
	unsigned char globalAddressMask;/**< Network Mask of the globalAddress */
    char defaultGateway[NET_IPV6_MAX]; /**< Default gateway */
	char dns1Ipv6[NET_IPV6_MAX];	  /**< dns primary server for IPv6 domain name resolution */
	char dns2Ipv6[NET_IPV6_MAX];	  /**< dns secondary server for IPv6 domain name resolution */
	char suppliconf[NET_WIFI_WPACONF_MAX];/**< WPA_Supplicant configuration file for WIFI interfaces */
	int usedhcp;/**< 1=DHCP is used, 0 if not */
	char dhcpid[NET_DHID_MAX];/**< client id string for dhcp (typically ?? ? not specified) */
	char clienthostname[NET_HOST_MAX];/**< client hostname string for dhcp (typically ?? ? not specified) */
	int speed;/**< defines for 10F, 100F, 10T, 100T, AUTO (default is AUTO)  - used to set interface and report current interface speed */
	int activate;/**< NET_ACTIVATE_ON[_ONCE]=Bring up the interface at boot, NET_ACTIVATE_OFF do nothing */
};

/*SVC_STRUCT*/
/**
 * Define a list of interface configuration
 */
struct netIfconfigList
{
	struct netIfconfig *ifconfig;/**< list of interfaces IPv4 configured */
	int ifconfig_count; /**< number of interfaces in the list IPv4 configured */		
	struct netIfconfigIpv6 *ifconfigIpv6;/**< list of interfaces IPv6 configured */
	int ifconfigIpv6_count; /**< number of interfaces in the list IPv6 configured */		
};

/*SVC_STRUCT*/
/**
 * Define a list of interface names
 */
struct netIfNameList
{
	char ifName[NETCOM_MAX_PERTYPE][NET_IFACE_MAX];/**< list of net interface names */
	int ifname_count; /**< number of net interfaces */
};

/*SVC_STRUCT*/
/**
 * Define a route information to an IPv4 address
 */
struct netRoute
{
	char interface[NET_IFACE_MAX];/**< interface name (eth0, ppp0, wlan0...)*/
	char destination[NET_IPV4_MAX]; /**< route destination*/
	char gateway[NET_IPV4_MAX];	/**< route gateway*/	
	char netmask[NET_IPV4_MAX];	/**< network route netmask */
};

/*SVC_STRUCT*/
/**
 * Define a route to IPv6 host
 */
struct netRouteIpv6
{
	char interface[NET_IFACE_MAX];/**< interface name (eth0, ppp0, wlan0...)*/
	char globalHostAddress[NET_IPV6_MAX];	/**< global address */	
	unsigned char globalHostAddressMask; /**< Network mask */
	char IPv6Gateway[NET_IPV6_MAX];	/**< Gateway to globalHostAddress */	
};

/*SVC_STRUCT*/
/**
 * Define a list of routes
 */
struct netRouteTable
{
	struct netRoute *route;/**< list of routes to IPv4 addresses */
	int route_count; /**< number of IPv4 routes in the list */	
	struct netRouteIpv6 *routeIpv6;/**< list of routes to IPv6 addresses */
	int routeIpv6_count; /**< number of IPv6 routes in the list */	
};

/*SVC_STRUCT*/
/**
 * Define ping statsitics
 */
struct netPingInfo
{
	unsigned char ntransmitted;/**< number of transmitted packets */	
	unsigned char nreceived;/**< number of received packets */	
	double tsum;/**< total round trip */	
	double tmin;/**< max round trip */	
	double tmax;/**< min round trip */	
};

/*SVC_STRUCT*/
/**
 * WiFi site survey Structure
 */
struct netSiteSurvey 
{
	char ssid[NET_WIFI_SSID_MAX]; /**< ssid string */
	int  channel; /**< channel number 1 - 14 */
	int  signalStrength; /**< signal strength 0 - 100 (max) */
	char bssid[NET_MAC_MAX+1]; /**< bssid string */
	char encrypType[NET_WIFI_ENCTYPE_MAX]; /**< encryption type: none,wep,tkip,aes */
	char authMode[NET_WIFI_AUTHMODE_MAX]; /**< authentication mode: open,shared,wep,wpa-personal,wpa2-personal,wpa-none,wpa-enterprise,wpa2-enterprise */
	int  networkType; /**< adhoc, managed (infrastructure) */
};

/*SVC_STRUCT*/
/**
 * Define a list of site survey elements
 */
struct netSiteSurveyList
{
	struct netSiteSurvey *siteSurvey;/**< list of site survey elements */
	int siteSurvey_count; /**< number of elements in list */	
};

/*SVC_STRUCT*/
/**
 * WiFi connection info struct
 */
struct netWifiInfo
{
	char ssid[NET_WIFI_SSID_MAX]; /**< ssid string */
	char bssid[NET_MAC_MAX+1]; /**< bssid string */
	int  networkType; /**< adhoc, infrastructure */
	int  signalStrength; /**< signal strength 0 - 100 (max) */
};

/*SVC_STRUCT*/
/**
 * WiFi link signal statistics struct
 */
struct netWifiSignalStats
{
	char isLinkQValid; /**< LinkQ availability. 0 = not available. */
	char isLevelValid; /**< Reception level availability. 0 = not available. */
	char isNoiseValid; /**< Noise availability. 0 = not available. */
	char isDBM; /**< Units for reception level and noise values. 0=none, 1=dBm . */
	int  linkQ; /**< Link quality. */
	int  linkQRangeMax; /**< Link quality range max. Not available if min=max. */
	int  linkQRangeMin; /**< Link quality range min. Not available if min=max. */
	int  level; /**< Reception level i.e. signal strength. */
	int  levelRangeMax; /**< Level range max. Not available if min=max. */
	int  levelRangeMin; /**< Level range min. Not available if min=max. */
	int  noise; /**< Noise. */
	int  noiseRangeMax; /**< Noise range max. Not available if min=max. */
	int  noiseRangeMin; /**< Noise range min. Not available if min=max. */
};


/*SVC_STRUCT*/
/**
 * Modem connection info struct (see parameters definition above)
 */
struct netModemInfo
{
	char number[NET_MODEM_NUMBER_MAX];/**<phone number to dial */
	char dial_type;/**<tone / pulse */
	char mode;/**<async / sync */
	char max_retries;/**<dial retries */
	char max_line_rate;/**<Modem Rate */
	char connect_timeout;/**<waiting for answer, if expired no more retries */
	char error_correction;/**<error_correction must be enable*/
	char compression; /**Compression activated, yes or no */
};

/*SVC_STRUCT*/
/**
 * Modem ISDN info struct (see parameters definition above)
 */
struct netModemIsdnInfo
{
	char number[NET_MDM_ISDN_NUMBER_MAX_LEN];			
	int isdn_prot; /**<See above for isdn_prot values */
	char x25_packet_size[NET_MDM_X25_PACKET_SIZE_LEN];	/**< packet size for X.25 connection (Value from 64 - 2048) */
	char facilities[NET_MDM_FACILITIES_MAX_LEN];			/**< access to X.25 Closed User Group , starting with 'G' */
	char nui[NET_MDM_NUI_MAX_LEN]; /**< NUI (Network User Identification) and password with call setup, allowed chars : a-z, A-Z, 0-9  */
	char x25_number[NET_MDM_X25_NUMBER_MAX_LEN];/**< dialed X.25 call number, (X.25 B channel only) */
	char user_data[NET_MDM_USER_DATA_MAX_LEN];/**< user data starting with D(without protocol ID, data length max. 16 char). P:  with protocol ID, data length max. 12 char */
	char max_retries; /**<dial retries */
	char connect_timeout; /**<waiting for answer, if expired no more retries */
	int	country_code; /**<currently without meaning */
};	


/*SVC_STRUCT*/
/**
 * Define a descriptor for a SESSION entry in a session XML file
 */
struct sessionDescriptor
{   
   char name[NET_NAMEID_MAX];/**< Indentify the current session in the XML file */
   int priority;/**< Define the current session priority */
   char nextMedia[NET_NAMEID_MAX];/**< indentify the protocol used by the session */
   double timeout;/**< timeout for the connection */
   int retry;/**< number of retries for the connection */
};

/*SVC_STRUCT*/
/**
 * Define a descriptor for a SOCKET entry in a session XML file
 */
struct socketDescriptor
{
   char name[NET_NAMEID_MAX];/**< Indentify the current socket in the XML file */
   char protocol[NET_PROTO_MAX];/**< socket type (possible values: TCP or UDP */
   char remote_ip[NET_HOST_MAX];/**< host dnsname/IPv4 address format: IP:PORT */
   char nextMedia[NET_NAMEID_MAX];/**< indentify the physical layer (WIFI, ETHERNET) used by this protocol */
};

/*SVC_STRUCT*/
/**
 * Define a descriptor for an ETHERNET entry in a session XML file
 */
struct ethernetDescriptor
{
	char name[NET_IFACE_MAX];/**< Indentify the current ethernet entry in the XML file */   
	struct netIfconfig ifconfig;/**< IP interface configuration */    
};

/*SVC_STRUCT*/
/**
 * Define a descriptor for a WIFI entry in a session XML file
 */
struct wifiDescriptor
{
	char name[NET_IFACE_MAX];/**< Indentify the current ethernet entry in the XML file */   
	struct netIfconfig ifconfig;/**< IP interface configuration */	
};


/*SVC_STRUCT*/
/**
 * Define a descriptor for a Serial entry in a session XML file
 */
struct serialDescriptor
{
   char name[NET_NAMEID_MAX];/**< Indentify the current Serial entry in the XML file */
};

/*SVC_STRUCT*/
/**
 * Define a descriptor for a Modem entry in a session XML file
 */
struct modemDescriptor
{
   char name[NET_NAMEID_MAX];/**< Indentify the current Modem entry in the XML file */
   struct netModemInfo mdmInfo;/**< Contains the modem parameters */
};


/*SVC_STRUCT*/
/**
 * Define a descriptor for an ISDN Modem entry in a session XML file
 */
struct modemIsdnDescriptor
{
   char name[NET_NAMEID_MAX];/**< Indentify the current Modem entry in the XML file */
   struct netModemIsdnInfo mdmInfo;/**< Contains the modem parameters */
};

/*SVC_STRUCT*/
/**
 * Define a descriptor for a PPP entry in a session XML file
 */
struct pppDescriptor
{
   char name[NET_NAMEID_MAX];/**< Indentify the current PPP entry in the XML file */
   struct netIfconfig ifconfig;/**< IP interface configuration */
};

/*SVC_STRUCT*/
/**
 * Define a descriptor for a Option entry in a session XML file
 */
struct optionDescriptor
{
   char name[NET_NAMEID_MAX];/**< Indentify the current Option entry in the XML file */
   char option[NET_OPTION_MAX];/**<Used to store the current option list>*/
};

/*SVC_STRUCT*/
/**
 * Define a descriptor for an SSL entry in a session XML file
 */
struct sslDescriptor
{
   char name[NET_NAMEID_MAX];/**< Indentify the current Option entry in the XML file */
   char remote_ip[NET_HOST_MAX];/**< host dnsname/IPv4 address format: IP:PORT */
   char server_profile[NET_HOST_MAX];/**< identify the place where the Server CA is stored */
   char client_profile[NET_HOST_MAX];/**< identify the place where the Client Certificate and the Client Private Key ara stored */
   int  session_ttl;/**< Indicate is seconds how log to keep the Resume Session before to perform a complete Handshake */
   char nextMedia[NET_NAMEID_MAX];/**< indentify the physical layer (WIFI, ETHERNET) or the protocol used by this SSL layer */
};			


/*SVC_STRUCT*/
/**
 * Define a descriptor for an GPRS entry in a session XML file
 */
struct netGprsInfo
{
   char APN[NET_HOST_MAX];/**< GPRS Access Point Name */
   int operator_id;/**< Operator LAI number */
   int reg_mode;/**< Network Registration Mode (NET_GPRS_REG_AUTO, NET_GPRS_REG_MANUAL, NET_GPRS_REG_AUTO_MANUAL)  */
   char number[NET_GPRS_NUMBER_MAX];/**< Phone number used for GPRS activation or for GSM data calls */
   char bearer[NET_GPRS_BEARER_MAX];/**< GSM Data modulation (NOT IMPLEMENTED!) */
};	

/*SVC_STRUCT*/
/**
 * GPRS connection info struct (see parameters definition above)
 */
struct gprsDescriptor
{
   char name[NET_NAMEID_MAX];/**< Indentify the current Modem entry in the XML file */
   struct netGprsInfo gprsInfo;/**< Contains the modem parameters */
};


/*SVC_STRUCT*/
/**
 * Define a Bluetooth information
 */
struct netBluetoothInfo
{
   char name[256];/**< Bluetooth device name */
   char address[18];/**< Bluetooth device address. Format: xx:xx:xx:xx:xx:xx */
   int pan_profile;/**< 1 if PAN profile, 0 otherwise */
   int dun_profile;/**< 1 if DUN profile, 0 otherwise */
   int obex_profile;/**< 1 if OBEX profile, 0 otherwise */
   int sp_profile;/**< 1 if SPP profile, 0 otherwise */
   int dun_channel;/**< RFCOMM Channel number for DUN profile */
   int obex_channel;/**< RFCOMM Channel number for OBEX profile */
   int sp_channel;/**< Channel number for SP profile */
   int auto_start;/**< 1 if automatically start on boot, 0 otherwise */
};

/**
 * Define extended Bluetooth information
 */
struct netBluetoothInfoExt
{
   struct netBluetoothInfo bt_info; /**< Bluetooth device name, address and profiles */
   int rssi; /**< Bluetooth RSSI */
};

/**
 * Define a list of operators (names and LAIs)
 */
struct netBluetoothInfoExtList
{
     struct netBluetoothInfoExt *device;
     int device_count;
};


/*SVC_STRUCT*/
/**
 * Define Bluetooth base information (VFI profile)
 */
struct netBluetoothApInfo
{
   char fw_version[80];/**< Firmware version. Format xx.xx.xx */
   char eth_status[8];/**< Ethernet cable status. Values 'up' or 'down' */
   char ip_address[40];/**< IP address. Format: xxx.xxx.xxx.xxx */
   char net_mode[16];/**< Network mode. Values 'bridge' or 'router' */
   char serial_number[12];/**< Serial number. Format: xxx.xxx.xxx */
};

/*SVC_STRUCT*/
/**
 * Define Bluetooth base information (VFI profile)
 */
struct netBluetoothSSPRequest
{
	int     type;				//**< Pairing type
	char    address[18];		//**< Remote Bluetooth device address
	char    name[256];			//**< Remote Bluetooth device name
	char    pairing_code[16];	//**< Request code.
};

/*SVC_STRUCT*/
/**
 * Define a descriptor for a Bluetooth entry
 */
struct bluetoothDescriptor
{
   char name[NET_NAMEID_MAX];/**< Indentify the current Bluetooth entry */
   struct netBluetoothInfo info;/**< Contains the Bluetooth parameters */
};

/*SVC_STRUCT*/
/**
 * Hold the current and the next media information
 */
struct netSessionInfo
{
   int comtype;/**< This is the current media type */
   char name[NET_NAMEID_MAX];/**< This is the current interface name*/
};

/*SVC_STRUCT*/
/**
 * Used to convoy net session data 
 */
struct netDataTransfer
{
	void *data;/**< array of bytes to transfer */
	int data_count; /**< number of bytes to transfer */
};


/*SVC_STRUCT*/
/**
 * Used to convoy information about GSM environment (module+sin+user)
 */
struct netGsmInfo
{
	char library_version[NET_NAMEID_MAX];/**< GSM library Version */
	char manufacturer	[NET_NAMEID_MAX];/**< GSM Module manufacturer */
	char model			[NET_NAMEID_MAX];/**< GSM Module model */
	char revision		[NET_NAMEID_MAX];/**< GSM Module version */
	char imei			[NET_NAMEID_MAX];/**< Module IMEI */
	char sim_card_id	[NET_NAMEID_MAX];/**< SIM card */
	char imsi			[NET_NAMEID_MAX];/**< User IMSI (PIN Needed)*/
};

/*SVC_STRUCT*/
/**
 * Get the GSM RSSI level
 */
struct netGsmRssi
{
	int rssi; /**< signal strenght */
};



/*SVC_STRUCT*/
/**
 * Define an entry for an operator (name, LAI and registration mode )
 */
struct netOperator
{
	char alphanumeric[NET_GPRS_OPERATOR_MAX];/**< Operator's name */
	int location_area_id;/**< LAI code of the operator */
	int roaming_mode; /**< Registration mode: NET_GPRS_REG_AUTO, NET_GPRS_REG_MANUAL, NET_GPRS_REG_AUTO_MANUAL */
};

/*SVC_STRUCT*/
/**
 * Used to convoy information about GSM cell information.
 * Note: This structure is obsolete as the cell field is not large enough to contain 3G cell ID number.
 *       Use this netCellIdExt structure instead !!! 
 */
struct netCellId{
  char mcc[NET_MCC_SIZE_MAX+1]; /**< Mobile Country Code */
  char mnc[NET_MNC_SIZE_MAX+1]; /**< Mobile Network Code */
  char lac[NET_LAC_SIZE_MAX+1]; /**< Location Area Code */
  char cell[NET_CELL_SIZE_MAX+1]; /**< Cell id */
  char bsic[NET_BSIC_SIZE_MAX+1];	/**< Base Station ID Code  - < !!! NOT USED - Empty >*/
  char chann[NET_CHANN_SIZE_MAX+1];/**< [U]ARFCN [UTRAN] Absolute Frequency Cannel Number  */
  char rssi[NET_RSSI_SIZE_MAX+1];	/**< Received signal level  - < !!! NOT USED - Empty >*/
  char c1[NET_C1_SIZE_MAX+1];	/**< Coefficient for BT reselection   - < !!! NOT USED - Empty >*/
  char c2[NET_C2_SIZE_MAX+1]; /**< Coefficient for BT reselection  - < !!! NOT USED - Empty >*/
};

/*SVC_STRUCT*/
/**
 * Used to convoy information about GSM/3G cell information
 */
struct netCellIdExt{
  char mcc[NET_MCC_SIZE_MAX+1]; /**< Mobile Country Code */
  char mnc[NET_MNC_SIZE_MAX+1]; /**< Mobile Network Code */
  char lac[NET_LAC_SIZE_MAX+1]; /**< Location Area Code */
  char cell[NET_CELL_3G_SIZE_MAX+1]; /**< Cell id */
  char bsic[NET_BSIC_SIZE_MAX+1];	/**< Base Station ID Code  - < !!! NOT USED - Empty >*/
  char chann[NET_CHANN_SIZE_MAX+1];/**< [U]ARFCN [UTRAN] Absolute Frequency Cannel Number  */
  char rssi[NET_RSSI_SIZE_MAX+1];	/**< Received signal level  - < !!! NOT USED - Empty >*/
  char c1[NET_C1_SIZE_MAX+1];	/**< Coefficient for BT reselection   - < !!! NOT USED - Empty >*/
  char c2[NET_C2_SIZE_MAX+1]; /**< Coefficient for BT reselection  - < !!! NOT USED - Empty >*/
};


/*SVC_STRUCT*/
/**
 * Define a list of operators (names and LAIs)
 */
struct netOperatorList
{
     struct netOperator *operators;
     int operators_count;    
};

/*SVC_STRUCT*/
/**
 * Define a list of Cells
 * Note: This structure is obsolete as the cell field of netCellId structure is not large enough to contain 3G cell ID number. 
 *       Use this netCellListExt structure instead !!!
 */
struct netCellList
{
     struct netCellId *cells;
     int cells_count;    
};

/*SVC_STRUCT*/
/**
 * Define a list of Cells
 */
struct netCellListExt
{
     struct netCellIdExt *cells;
     int cells_count;    
};

/*SVC_STRUCT*/
/**
 * Network event
 */
struct netEvent
{
	int eventType; /**< Event type */
	char interface[NET_IFACE_MAX];	/**< Interface name */
};

/*SVC_STRUCT*/
/**
 * Used to convoy the satnav commands
 */
struct netSatNavCommand
{
     int id; /**< parameter ID (AT command number) */
     char param[NET_SATNAV_DATA_MAX]; /**< pointer to a buffer holding a value */
     int size; /**< size of param void pointer */
};

/*SVC_STRUCT*/
/**
 * Define a list of Bluetooth devices information
 */
struct netBluetoothList
{
    struct netBluetoothInfo *device;
    int device_count;    
};

/*SVC_STRUCT*/
/**
 * Define status of a Bluetooth device
 */
struct netBluetoothStatus
{
   int is_paired;
   int is_pan_connected;
   int is_dun_connected;
};

/*SVC_STRUCT*/
/**
 * Define a Bluetooth device
 */
struct netBluetoothDev
{
   char name[256];/**< Bluetooth device name */
   char address[18];/**< Bluetooth device address. Format: xx:xx:xx:xx:xx:xx */
};
/*SVC_STRUCT*/
/**
 * Define a list of Bluetooth devices
 */
struct netBluetoothDevList
{
    struct netBluetoothDev *device;
    int device_count;    
};

/*SVC_STRUCT*/
/**
 * Define beacon value that is going to be advertised by: int net_ibeaconStartAdvertise()
 */
struct netIBeacon
{
	char uuid[16];  /**< Service UUID */
	char major[2];  /**< Service major ID */
	char minor[2];  /**< Service minor ID */
};

/*SVC_STRUCT*/
/**
 * Define a descriptor for an IPv6 ETHERNET entry in a session XML file
 */
struct ethernetDescriptorIpv6
{
  char name[NET_IFACE_MAX];/**< Indentify the current ethernet entry in the XML file */
  struct netIfconfigIpv6 ifconfig;/**< IPv6 interface configuration */
};

/*SVC_STRUCT*/
/**
 * Define a descriptor for a IPv6 WIFI entry in a session XML file
 */
struct wifiDescriptorIpv6
{
  char name[NET_IFACE_MAX];/**< Indentify the current ethernet entry in the XML file */
  struct netIfconfigIpv6 ifconfig;/**< IPv6 interface configuration */
};


/*SVC_STRUCT*/
/**
 * Define a descriptor for network capabilities/permissions
 */
struct netCapabilities
{
  unsigned int enable_ipv6;/**< IPv6 interface enable/disable */
  unsigned int enable_wl;/**< IPv6 interface configuration */
};

/*SVC_STRUCT*/
/**
 * Radio power on/off state
 */
struct netRadioPwrState
{
	int bt; /**< Bluetooth radio  */
	int wifi; /**< bssid string */
	int cellular; /**<  cellular */
};


/** 
 * Obtain the version of the net service
 *
 * @return version struct defined in svcmgrSvcDef.h
 */
/*SVC_PROTOTYPE*/ struct version net_getVersion(void);

/** 
 * Write IP interface configuration in the default XML file
 *
 * @param[in] config convoy data to store in the XML file (structure config contains element interface that contains name of interface to set) 
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters   
 * @li ESRCH XML file error
 *
 */
/*SVC_PROTOTYPE*/ int net_interfaceSet( struct netIfconfig config );

/** 
 * Read IP interface configuration from the default XML file
 *
 * @param[in] p_iface interface name (eth0, wlan0, ppp0...) for which to read information
 *
 * @return struct netIfconfig - the interface 
 *
 * @li errno = 0, in case of success
 * @li errno = EINVAL bad input parameters   
 * @li errno = ESRCH, the interface is not present in the XML file
 *
 */
/*SVC_PROTOTYPE*/ struct netIfconfig net_interfaceGet( char *p_iface );

/**
 * Set a tag attribute in default XML file
 *
 * @param[in] comtype entry type (COM_SESSION, COM_SOCKET, COM_WIFI, COM_SSL...)
 * @param[in] name_id identifies the comtype entry
 * @param[in] attrib indicates the attribute name
 * @param[in] value indicates the attribute value 
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li errno = 0, in case of success
 * @li errno = EINVAL bad input parameters 
 * @li errno = ESRCH Indicated tag not found
 *
 */
/*SVC_PROTOTYPE*/ int net_interfaceSetAttr( int comtype, char *name_id, char *attrib, char *value );

/**
 * Get a tag attribute from the default session file
 *
 * @param[in] comtype entry type (COM_SESSION, COM_SOCKET, COM_WIFI, COM_SSL...)
 * @param[in] name_id identifies the comtype entry
 * @param[in] attrib indicates the attribute name
 *
 * @return Tag attribute value when found (must be freed), NULL when an error occurred - Check Errno
 *
 * @li errno = 0, in case of success
 * @li errno = EINVAL bad input parameters 
 * @li errno = ESRCH Indicated tag not found
 *
 */
/*SVC_PROTOTYPE*/ char* net_interfaceGetAttr( int comtype, char *name_id, char *attrib );

/** 
 * Write Modem interface configuration in the default XML file
 *
 * @param[in] mdnInfo convoy modem parameters to store in the XML file
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters   
 * @li ESRCH XML file error
 *
 */
/*SVC_PROTOTYPE*/ int net_interfaceModemSet( struct netModemInfo mdnInfo );


/** 
 * Read Modem configuration from the default XML file
 * 
 *
 * @return struct netModemInfo - the interface 
 *
 * @li errno = 0, in case of success
 * @li errno = EINVAL bad input parameters   
 * @li errno = ESRCH, the interface is not present in the XML file
 *
 */
/*SVC_PROTOTYPE*/ struct netModemInfo net_interfaceModemGet();


/** 
 * Write Modem interface configuration in the default XML file
 *
 * @param[in] mdnInfo convoy modem parameters to store in the XML file
 * @param[in] iface indicates the modem name
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters   
 * @li ESRCH XML file error
 *
 */
/*SVC_PROTOTYPE*/ int net_interfaceModemExtSet( char *iface, struct netModemInfo mdnInfo );


/** 
 * Read Modem configuration from the default XML file 
 *  
 * @param[in] iface indicates the modem name
 *  
 * @return struct netModemInfo - the interface 
 *
 * @li errno = 0, in case of success
 * @li errno = EINVAL bad input parameters   
 * @li errno = ESRCH, the interface is not present in the XML file
 *
 */
/*SVC_PROTOTYPE*/ struct netModemInfo net_interfaceModemExtGet( char *iface );


/** 
 * Write Modem ISDN interface configuration in the default XML file
 *
 * @param[in] mdnInfo convoy modem parameters to store in the XML file
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters   
 * @li ESRCH XML file error
 *
 */
/*SVC_PROTOTYPE*/ int net_interfaceModemIsdnSet( struct netModemIsdnInfo mdnInfo );


/** 
 * Read Modem ISDN configuration from the default XML file
 * 
 *
 * @return struct netModemIsdnInfo - the interface 
 *
 * @li errno = 0, in case of success
 * @li errno = EINVAL bad input parameters   
 * @li errno = ESRCH, the interface is not present in the XML file
 *
 */
/*SVC_PROTOTYPE*/ struct netModemIsdnInfo net_interfaceModemIsdnGet();


/** 
 * Write GPRS interface configuration in the default XML file
 *
 * @param[in] gprsInfo convoy GPRS parameters to store in the XML file
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters   
 * @li ESRCH XML file error
 *
 */
/*SVC_PROTOTYPE*/ int net_interfaceGprsSet( struct netGprsInfo gprsInfo );


/** 
 * Read GPRS configuration from the default XML file
 * 
 *
 * @return struct gprsDescriptor - the interface 
 *
 * @li errno = 0, in case of success
 * @li errno = EINVAL bad input parameters   
 * @li errno = ESRCH, the interface is not present in the XML file
 *
 */
/*SVC_PROTOTYPE*/ struct netGprsInfo net_interfaceGprsGet();


/** 
 * Write options for an Interface into the default XML file
 *
 * @param[in] p_iface Interface for WIFI to write options
 * @param[in] p_option options to write to the XML file
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters   
 * @li ESRCH XML file error
 *
 */
/*SVC_PROTOTYPE*/ int net_optionSet( char *p_iface, char *p_option );

/** 
 * Read options for an Interface from the default XML file
 *
 * @param[in] p_iface Interface for WIFI to write options
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters   
 * @li ESRCH XML file error
 *
 */
/*SVC_PROTOTYPE*/ char *net_optionGet( char *p_iface );


/** 
 * Bring network up for a given interface according to the parameter in the default file
 * If activate attribute in the XML file is set to NET_ACTIVATE_ON[_ONCE] then bring up the network, otherwise do nothing
 * 
 * @param[in] p_iface interface (eth0, wlan0, ppp0...) to configure.
 * @param[in] ipVersion can be one of the following values: 
 *
 * @li NET_IP_V4 configure IPv4 interface
 * @li NET_IP_V6 configure IPv6 interface
 * 
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li ENETDOWN The Interface is not online (Ethernet cable not plugged for example)
 * @li EBADF Error when reading the default file
 * @li ESRCH Error while changing the Ethernet speed
 */
/*SVC_PROTOTYPE*/ int net_interfaceUp( char *p_iface, int ipVersion );

/** 
 * Bring network down for a given IP interface.
 * Activate parameter in the XML file is not impacted when calling net_interfaceDown
 * 
 * @param[in] p_iface interface (eth0, wlan0, ppp0...) to bring down
 * @param[in] ipVersion can be one of the following values: 
 *
 * @li NET_IP_V4 de-configure IPv4 interface
 * @li NET_IP_V6 de-configure IPv6 interface
 * 
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li ENONET no interface UP 
 * @li ENETDOWN The Interface is not online (Ethernet cable not plugged for example)
 * @li EBADF Error when reading the default configuration file 
 */
/*SVC_PROTOTYPE*/ int net_interfaceDown( char *p_iface, int ipVersion );

/** 
 * Read the interface link status and return the current Interface IP information. 
 * mac address is filled whatever the interface status is. 
 * activate parameter is updated according to it's value in the XML file
 * 
 * @param[in] p_iface interface (eth0, wlan0, ppp0...) for which to read information
 *
 * @return struct netIfconfig - IP information of the interface
 *
 * @li errno = 0, in case of success
 * @li errno = ENONET, if the network is down
 * @li errno = ENETDOWN The interface is not online (ethernet cable not plugged for example, or WIFI network disconnect)
 * @li errno = ENOTCONN The device is not on (WIFI module is down)
 * @li errno = ENOEXEC The authentictor is down (ex. WPA supplicant not running)
 *
 */
/*SVC_PROTOTYPE*/ struct netIfconfig net_interfaceStatus( char *p_iface );

/**
 * !! Reserved to APM only !!
 * Bring network up for a all interfaces according to the parameters in the default file
 * If activate attribute in the XML file is set to NET_ACTIVATE_ON[_ONCE] then bring up the network, otherwise do nothing
 *
 * @param[in] ipVersion can be one of the following values: 
 *
 * @li NET_IP_V4 de-configure IPv4 interface
 * @li NET_IP_V6 de-configure IPv6 interface
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li ENETDOWN The Interface is not online (Ethernet cable not plugged for example)
 * @li EBADF Error when reading the default file
 * @li ESRCH Error while changing the Ethernet speed
 * @li ENOTCONN we failed to configure both Ethernet and the WIFI
 */
/*SVC_PROTOTYPE*/ int net_networkUp( int ipVersion );

/**
 * !! Reserved to APM only !!
 * Bring network down for a given IP interface.
 * Activate parameter in the XML file is not impacted when calling net_interfaceDown
 *
 * @param[in] ipVersion can be one of the following values: 
 *
 * @li NET_IP_V4 de-configure IPv4 interface
 * @li NET_IP_V6 de-configure IPv6 interface
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li ENONET no interface UP 
 * @li ENETDOWN The Interface is not online (Ethernet cable not plugged for example)
 * @li EBADF Error when reading the default configuration file 
 */
/*SVC_PROTOTYPE*/ int net_networkDown( int ipVersion );

/** 
 * Read the interfaces configuration
 *
 * @return struct netIfconfigList - a list of interfaces configuration structure
 *
 * @li errno = 0, in case of success
 * @li errno = ENONET, any active was not found
 *
 */
/*SVC_PROTOTYPE*/ struct netIfconfigList net_interfaceStatusList( void );

/**
 * Get the list of available Ethernet interfaces
 *
 * @return struct netIfNameList - a list of Ethernet interface names
 *
 * @li errno = 0, in case of success
 *
 */
/*SVC_PROTOTYPE*/ struct netIfNameList net_interfaceAvailableList( void );

/**
 * Read the system routing table
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EBADF error while reading the routing table	
 * @li ENOEXEC routing table column is missing
 * @li EDESTADDRREQ no route was found in the routing table
 */
/*SVC_PROTOTYPE*/ struct netRouteTable net_getTable( void );

/** 
 * Read all the routes to a specific destination
 *
 * @param[in] p_dest IP destination for which to load the routes (p_dest is IPv4 or IPv6 address)
 *
 * @return struct netRouteTable - a table route information structure
 *
 */
/*SVC_PROTOTYPE*/ struct netRouteTable net_getRoutes( char *p_dest );

/** 
 * IPv4 - Add a static route to a specific host
 *
 * @param[in] p_dest host IP address
 * @param[in] p_gateway gateway to this host
 * @param[in] p_iface IP interface (eth0,ppp0...)
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li EFAULT invalid IP address was passed
 * @li EBADF error when reading the routing table	
 * @li ENOEXEC routing table column is missing
 * @li EDESTADDRREQ no route was found
 * @li EHOSTUNREACH hostname resolution error
 */
/*SVC_PROTOTYPE*/ int net_addHostRoute( char *p_dest , char *p_gateway, char *p_iface );

/** 
 * IPv4 - Delete a static route to a specific host
 * 
 * @param[in] p_dest host IP address	   
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li EFAULT invalid IP address was passed
 * @li EBADF error when read the routing table	
 * @li ENOEXEC routing table column is missing
 * @li EDESTADDRREQ no route was found	
 */
 /*SVC_PROTOTYPE*/ int net_delHostRoute( char *p_dest ); 
   
 /** 
 * IPv4 - Add a static route to a network
 * 
 * @param[in] p_net network IP address
 * @param[in] p_netmask network mask
 * @param[in] p_gateway gateway to use
 * @param[in] p_iface IP interface (eth0,ppp0...)
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li EFAULT invalid IP address was passed
 * @li EBADF error when read the routing table	
 * @li ENOEXEC routing table column is missing
 * @li EDESTADDRREQ no route was found	
 * @li ENETDOWN interface was not found in the active interfaces list		
 */ 
/*SVC_PROTOTYPE*/ int net_addNetRoute( char *p_net, char *p_netmask, char *p_gateway, char *p_iface );

/** 
 * IPv4 - Delete a static route to a network
 *
 * @param[in] p_net network IP address
 * @param[in] p_netmask network mask	
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li EFAULT invalid IP address was passed
 * @li EBADF error when read the routing table	
 * @li ENOEXEC routing table column is missing
 * @li EDESTADDRREQ no route was found	
 * @li ENETDOWN interface was not found in the active interfaces list		
 */
/*SVC_PROTOTYPE*/ int net_delNetRoute( char *p_net, char *p_netmask );

/** 
 * Select the default route according to the priority policy (eth0,wlan0,ppp0...)
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @retval ENONET no active interface found	
 */
/*SVC_PROTOTYPE*/ int net_autoSetDefaultRoute( void );

/** 
 * Set the default route on a given interface
 *
 * @param[in] p_iface IP interface (eth0,ppp0...)
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li EBADF error when reading the default route file
 * @li ENONET no active interface found	
 */
/*SVC_PROTOTYPE*/ int net_setDefaultRoute( char *p_iface );

/** 
 * Get the default route interface name
 *
 * @return Interface name (eth0,wlan0,ppp0) which holds the default route = success, null string = Error - Check Errno
 * 
 * @li EINVAL bad input parameters
 * @li EBADF error when read the default route file
 * @li ENONET no active interface found	
 */
/*SVC_PROTOTYPE*/ char* net_getDefaultRouteInfo();

/** 
 * Start a DHCP client for a given interface (If already up with dhcp, do nothing. If up with static IP, start dhcp and switch dynamic)
 * 
 * @param[in] p_iface interface (eth0, wlan0, ppp0...) on which to run the DHCP session
 * @param[in] ipVersion can be one of the following values: 
 *
 * @li NET_IP_V4 Start IPv4 DHCP client on this interface
 * @li NET_IP_V6 Start IPv6 DHCP client on this interface
 * 
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li ENETDOWN The Interface is not online (Ethernet cable not plugged for example)
 */
/*SVC_PROTOTYPE*/ int net_startDHCP( char *p_iface, int ipVersion );

/** 
 * Causes udhcpc to renew the current lease or, if it does not have one, obtain a new lease. 
 * 
 * @param[in] p_iface interface (eth0, wlan0, ppp0...) for which to request the IP renewal
 * @param[in] ipVersion can be one of the following values: 
 *
 * @li NET_IP_V4 ask the DHCPv4 client to renew the IPv4 address for this interface
 * @li NET_IP_V6 ask the DHCPv6 client to renew the IPv6 address for this interface
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li ENETDOWN The Interface is not online (Ethernet cable not plugged for example)
 * @li ESRCH The Interface is not online (Ethernet cable not plugged for example)
 */
/*SVC_PROTOTYPE*/ int net_renewDHCP( char *p_iface, int ipVersion );

/** 
 * Causes udhcpc to release the current lease (to get a new lease, you must call net_renewDHCP())
 * 
 * @param[in] p_iface interface (eth0, wlan0, ppp0...)for which to release the IP address
 * @param[in] ipVersion can be one of the following values: 
 *
 * @li NET_IP_V4 ask the DHCPv4 client to release the IPv4 address for this interface
 * @li NET_IP_V6 ask the DHCPv6 client to release the IPv6 address for this interface
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li ESRCH The Interface is not online (Ethernet cable not plugged for example)
 */
/*SVC_PROTOTYPE*/ int net_releaseDHCP( char *p_iface, int ipVersion );

/** 
 * Stop a DHCP client for a given interface
 * 
 * @param[in] p_iface interface (eth0, wlan0, ppp0...) for which to stop the DHCP client
 * @param[in] ipVersion can be one of the following values: 
 *
 * @li NET_IP_V4 Stop the DHCPv4 client
 * @li NET_IP_V6 Stop the DHCPv6 client
 * 
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li ESRCH The Interface is not online (Ethernet cable not plugged for example)
 */
/*SVC_PROTOTYPE*/ int net_stopDHCP( char *p_iface,  int ipVersion );
																	 
/** 
 * Specify DNS servers for a given interface
 *
 * @param[in] p_iface interface on which to add DNS server
 * @param[in] p_dns1 Domain Name Server 1 (IPv4 or IPv6)
 * @param[in] p_dns2 Domain Name Server 2 (IPv4 or IPv6)
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li EFAULT invalid IP address was provided
 * @li EDESTADDRREQ no route was found	
 * @li ENETDOWN no valid gateway was found
 *
 */
/*SVC_PROTOTYPE*/ int net_setDNS( char *p_iface, char *p_dns1, char *p_dns2 );

/** 
 * Add static routes from an XML file
 *
 * @param[in] p_fileName XML file that contains static routes
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters   
 * @li EBADF error when reading the XML file
 *
 */
/*SVC_PROTOTYPE*/ int net_addRouteFromXML( char *p_fileName );

/** 
 * Perform DNS resolution on a given interface for IPv4
 *
 * @param[in] p_dnsName DNS name to resolve
 * @param[in] p_iface newtork interface on which to make the domain name resolution
 *
 * @return string - IP address
 *
 * @return IP obtained from DNS resolution = success, null string = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li EFAULT the parameter provided is not an IP address
 * @li EBADF error while reading the dns file
 * @li ENONET IP interface is up
 * @li ENOLINK error while creating the DNS link file
 *
 */
/*SVC_PROTOTYPE*/ struct netIfconfig net_nsLookup( char *p_dnsName, char *p_iface );

/** 
 * Remove all stored DNS resolution results
 * 
 * @return 0 = Success, -1 = Error - Check Errno
 *
 */
/*SVC_PROTOTYPE*/ int net_flushDNSCache( void );

/** 
 * Remove all stored DNS resolution results for a given server
 *
 * @param[in] p_dns dns name for which to clean all the entries (IPv4 or IPv6)
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 */
/*SVC_PROTOTYPE*/ int net_removeDNSCache( char *p_dns );

/** 
 * Send an ping to a given host
 *
 * @param[in] p_host ping destination (IPv4 or IPv6)
 * @param[in] pingCount number of ping to send
 * 
 * @return struct netPingInfo - a ping results information structure
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li ENOMSG no packet was transmitted
 * @li EHOSTUNREACH no packet was received
 * @li EBADMSG nupber of received packets is lower that transmitted one 
 * @li EFAULT the p_host is not valid
 *
 */
/*SVC_PROTOTYPE*/ struct netPingInfo net_ping( char *p_host, unsigned char pingCount );

/** 
 * Start WIFI supplicant according to parameters stored in the default file
 * Supplicant configuration file has to follow PCI-compliant security settings:
 *   network = {
 *     proto=RSN or WPA2
 *     group=CCMP
 *     pairwise=CCMP
 *     key_mgmt=WPA-PSK or WPA-EAP or both
 *   }
 *
 * @param[in] startwpa if = 1, power up the module and start the WPA Supplicant, if = 0 only power up the module
 * 
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EBADF wifi configuration file not found
 * @li EINVAL bad input parameters   
 *
 */
/*SVC_PROTOTYPE*/ int net_wifiStart( int startwpa );

/** 
 * Stop the WIFI supplicant
 *
 * @param[in] stopmodule 1=WPA_Supplicant and the WIFI module, 0=Stop the WPA_Supplicant only
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 */
/*SVC_PROTOTYPE*/ int net_wifiStop( int stopmodule );

#ifdef RAPTOR
struct netWifiCustomCommand {
	char command[256];
	char outfile[256];
};

int net_wifiRunCustomWL(struct netWifiCustomCommand custom_cmd);
#endif

/**
 * Obtain a WiFi site survey list
 * IMPORTANT: WIFI is unregistred during WIFI scan and automatically registered back again.
 *
 * @return struct netIfconfigList - a list of sites discovered
 *
 * @li errno = ENOEXEC scan command is not ready
 * @li errno = ETIMEDOUT no wifi network found
 * @li errno = EBADF error while parsing the scan results
 * @li errno = ENOTCONN The device is not on (WIFI module is down)
 *
 */
/*SVC_PROTOTYPE*/ struct netSiteSurveyList net_wifiSiteSurvey( void );

/** 
 * Obtain a WiFi information
 *
 * @param[in] p_iface interface (wlan0, wlan1) for which to get the information 
 *
 * @return struct netWifiInfo - structure containing Connected WiFi infor
 *
 * @li errno = ENETDOWN no wifi network found
 * @li errno = ENOEXEC command execution error
 * @li errno = ENOTCONN The device is not on (WIFI module is down)
 *
 */
/*SVC_PROTOTYPE*/ struct netWifiInfo net_wifiInfo( char *p_iface );


/** 
 * Get WiFi signal statistics
 *
 * @param[in] p_iface interface (wlan0, wlan1) for which to get the information 
 *
 * @return struct netWifiSignalStats - structure containing WiFi signals stats
 *
 * @li errno = ENETDOWN Network disconnected
 * @li errno = ENODEV   Interface not found
 * @li errno = ENOTSUP  Operation not supported on this interface
 * @li errno = EIO      Unable to read information
 *
 */
/*SVC_PROTOTYPE*/ struct netWifiSignalStats net_wifiGetSignalStats( char *p_iface );

/** 
 * Set NTP hostname
 *
 * @param[in] p_hostname Network Time Protocol Hostname. If hostname is NULL pool.ntp.org is used
 * 
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters   
 * @li EFAULT the hostname is not valid 
 *
 */
/*SVC_PROTOTYPE*/ int net_setNTP( char *p_hostname );

/** 
 * Open a communication session File
 *
 * @param[in] p_xmlFile XML file that stores the current session information
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters   
 * @li EBADF error when reading the XML file
 *
 */
/*SVC_PROTOTYPE*/ int net_sessionOpen( char *p_xmlFile );

/** 
 * Start the current communication session according to the session file
 *
 * @param[in] sessionType can be one of the following values:
 *
 * @li NET_FIRST_SESSION Connect the session with lowest priority
 * @li NET_NEXT_SESSION Connect the following session in the XML file
 * @li NET_CURRENT_SESSION reconnect to the current session 
 *
 * @return > 0 Success, the connection handle is then returned, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters   
 * @li EFAULT invalid IP address was passed
 * @li EHOSTUNREACH enable to reach the host
 * @li ETIMEDOUT connection timeout
 */
/*SVC_PROTOTYPE*/ int net_sessionConnect( int sessionType );


/** 
 * Connect to a session given its priority
 *
 * @param[in] priority priority number to connect to (>0) 
 *
 * @return > 0 Success, the connection handle is then returned, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters   
 * @li EFAULT invalid IP address was passed
 * @li EHOSTUNREACH enable to reach the host
 * @li ETIMEDOUT connection timeout
 */
/*SVC_PROTOTYPE*/ int net_sessionConnectExt( int priority );

/** 
 * Read data from the host
 *
 * @param[in] handle communication handle returned by net_sessionConnect function
 * @param[in] count number of bytes to read
 * @param[in] timeout first timeout in seconds to wait for data reception
 *
 * @return struct netDataTransfer - data received
 *
 */
/*SVC_PROTOTYPE*/ struct netDataTransfer net_sessionRead( int handle, int count, int timeout );

/** 
 * Send data to the host
 *
 * @param[in] handle communication handle returned by net_sessionConnect function
 * @param[in] data convoy data to send to the host
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters   
 * @li ENONET no active interface found	 
 *
 */
/*SVC_PROTOTYPE*/ int net_sessionWrite( int handle, struct netDataTransfer data );

/** 
 * Change a tag entry in the current opened XML file
 *
 * @param[in] comtype entry type (COM_SESSION, COM_SOCKET, COM_WIFI, COM_SSL...)
 * @param[in] nameid identifies the entry in the entire XML file
 * @param[in] data contains a pointer to the tag descriptor (sessionDescriptor, socketDescriptor...)
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters   
 * @li ESRCH XML file error
 *
 */
/*SVC_PROTOTYPE*/ int net_sessionSetTag( int comtype, char *nameid, struct netDataTransfer data );

/** 
 * Get a tag entry in the current opened XML file
 *
 * @param[in] comtype entry type (COM_SESSION, COM_SOCKET, COM_WIFI, COM_SSL...)
 * @param[in] nameid identifies the entry in the entire XML file 
 *
 * @return struct netDataTransfer - data received
 *
 */
/*SVC_PROTOTYPE*/ struct netDataTransfer net_sessionGetTag( int comtype, char *nameid );

/**
 * Set a tag attribute in the opened session file
 *
 * @param[in] comtype entry type (COM_SESSION, COM_SOCKET, COM_WIFI, COM_SSL...)
 * @param[in] name_id identifies the comtype entry
 * @param[in] attrib indicates the attribute name
 * @param[in] value indicates the attribute value 
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li errno = 0, in case of success
 * @li errno = EINVAL bad input parameters 
 * @li errno = ESRCH Indicated tag not found
 *
 */
/*SVC_PROTOTYPE*/ int net_sessionSetAttr( int comtype, char *name_id, char *attrib, char *value );

/**
 * Get a tag attribute from the opened session file
 *
 * @param[in] comtype entry type (COM_SESSION, COM_SOCKET, COM_WIFI, COM_SSL...)
 * @param[in] name_id identifies the comtype entry
 * @param[in] attrib indicates the attribute name
 *
 * @return Tag attribute value when found (must be freed), NULL when an error occurred - Check Errno
 *
 * @li errno = 0, in case of success
 * @li errno = EINVAL bad input parameters 
 * @li errno = ESRCH Indicated tag not found
 *
 */
/*SVC_PROTOTYPE*/ char* net_sessionGetAttr( int comtype, char *name_id, char *attrib );

/** 
 * Terminate the current data connection
 *
 * @param[in] handle communication handle returned by net_sessionConnect function 
 * 
 * @return 0 = Success, -1 = Error - Check Errno
 *
 */
/*SVC_PROTOTYPE*/ int net_sessionDisconnect( int handle );

/** 
 * Get the session information of a given communication handle
 *
 * @param[in] handle communication handle returned by net_sessionConnect function
 * @param[in] current contain the current media information (name and communication type)
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters   
 * @li EBADF error when reading the XML file
 * @li ESRCH the XML file is not well formed
 *
 */
/*SVC_PROTOTYPE*/ struct netSessionInfo net_sessionGetInfoNext( int handle, struct netSessionInfo current );


/** 
 * Get the next media information from the current media
 *
 * @param[in] handle communication handle returned by net_sessionConnect function
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters   
 * @li EBADF error when reading the XML file
 * @li ESRCH the XML file is not well formed
 *
 */
/*SVC_PROTOTYPE*/ struct netSessionInfo net_sessionGetInfo( int handle );

/** 
 * Get the list of session handles sorted by SESSION priority
 *
 * @return struct netDataTransfer - data received
 * 
 */
/*SVC_PROTOTYPE*/ struct netDataTransfer net_getHandles();

/** 
 * Save the password used to protect Private Key 
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters 
 *
 */
/*SVC_PROTOTYPE*/ int net_sessionSetPassword( char *password );




/** 
 * Save the current configuration into an XML file
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters   
 * @li EBADF error when reading the XML file
 *
 */
/*SVC_PROTOTYPE*/ int net_sessionSave( char *xmlFile );


/** 
 * Terminate the current communication session and release all the ressources 
 *
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters   
 * @li EBADF error when reading the XML file
 *
 */
/*SVC_PROTOTYPE*/ int net_sessionClose( void );



/** 
 * returns the handle we are going to connect to
 *
 * @param[in] sessionType can be one of the following values:
 *
 * @li NET_FIRST_SESSION Connect the session with lowest priority
 * @li NET_NEXT_SESSION Connect the following session in the XML file
 * @li NET_CURRENT_SESSION reconnect to the current session 
 *
 * @return > 0 Success, the connection handle is then returned, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters   
 * @li EFAULT invalid IP address was passed
 * @li EHOSTUNREACH enable to reach the host
 * @li ETIMEDOUT connection timeout
 */
/*SVC_PROTOTYPE*/ int net_sessionGetNextHandle( int sessionType );



/** 
 * returns the last SSL error
 *  
 *
 * @return the last SSL error during Certificate Loading or after the handshake
 * 
 */
/*SVC_PROTOTYPE*/ int net_sessionGetSSLError();


/** 
 * Let the net service know about the passphrase to use for a specific profile 
 *
 * @param[in] profile Profile for which to store the passphrase
 * @param[in] password Passphrase to use for this specific profile
 * @param[in] gshared if 1, the passphrase is shared with the Group
 *                    if 0, the passphrase is private to the owner
 * 
 * 
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters 
 *
 */
/*SVC_PROTOTYPE*/ int net_sessionStorePassword( char *profile, char *password, int gshared );


/** 
 * Clear the passphrase corresponding to a given profile
 *
 * @param[in] profile Profile for which to clear the passphrase
 * 
 * 
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters 
 *
 */
/*SVC_PROTOTYPE*/ int net_sessionClearPassword( char *profile );


/** 
 * Define the cipher list
 *
 * @param[in] profile Profile for which to store the passphrase
 * @param[in] cipher contains the SSL cipher list
 * @param[in] gshared if 1, the cipher list is shared with the Group
 *                    if 0, the cipher list is private to the owner
 * 
 * 
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters 
 *
 */
/*SVC_PROTOTYPE*/ int net_sessionCipherList( char *profile, char *cipher, int gshared );


/** 
 * Clear the cipher list corresponding to a given profile
 *
 * @param[in] profile Profile for which to clear the cipher list
 * 
 * 
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters 
 *
 */
/*SVC_PROTOTYPE*/ int net_sessionClearCipherList( char *profile );

/** 
 * Unlock the SIM by providing the PIN value
 *
 * @param[in] pin value of the PIN to send to the SIM card to unlock it
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters   
 * @li 
 *
 */
/*SVC_PROTOTYPE*/ int net_gprsVerifyPin( char *pin );


/** 
 * Change the SIM PIN by providing a PIN value
 *
 * @param[in] oldpin old PIN value
 * @param[in] newPin old PIN value
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters   
 * @li EFAULT NO Sim card detected on the slot
 * @li ENOTCONN he GPRS module is off
 * @li ENOEXEC command execution error 
 * @li ETIMEDOUT Timeout occured
 * 
 */
/*SVC_PROTOTYPE*/ int net_gprsChangePin( char *oldpin, char *newPin );

/** 
 * Unlock the SIM when the number of wrong attempts reaches 0
 *
 * @param[in] pin SIM PIN value
 * @param[in] puk SIM PUK value
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters   
 * @li EFAULT NO Sim card detected on the slot
 * @li ENOTCONN he GPRS module is off
 * @li ENOEXEC command execution error 
 * @li ETIMEDOUT Timeout occured
 * 
 */
/*SVC_PROTOTYPE*/ int net_gprsUnlockPin( char *pin, char *puk );

/**
 * Get the SIM card status
 *
 * @return NET_GPRS_SIM_[XXX] = Success, -1 = Error - Check Errno
 * @li EINVAL - Bad input parameters
 * @li ENOEXEC - Command execution error
 */
/*SVC_PROTOTYPE*/ int net_gprsGetSimStatus();

/** 
 * Get the GPRS Module status (SIM present, PIN Ready, GPRS Connected ...) 
 *
 * @return NET_GSM_STT_[XXX] = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters   
 * @li errno = ENOEXEC command execution error 
 */
/*SVC_PROTOTYPE*/ int net_gprsGetStatus();

/** 
 * Return information about the GSM module
 *
 *
 * @return struct netGsminfo - GSM module information
 *
 */
/*SVC_PROTOTYPE*/ struct netGsmInfo net_gprsGetInfo( );

/** 
 * Obtain the GPRS Information ( manufacturer, imei...)
 *
 * @return struct netGsminfo - structure containing Connected WiFi infor
 *
 * @li errno = EINVAL bad input parameters
 * @li errno = ENOEXEC command execution error 
 *
 */
/*SVC_PROTOTYPE*/ struct netGsmRssi net_gprsGetRssi();

/** 
 * Power on the GPRS Module
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li ENOEXEC Unable to power on the module 
 */
/*SVC_PROTOTYPE*/ int net_gprsPowerOnModule();

/** 
 * Obtain a GSM site operator list
 *
 * @return struct netOperatorList - a list of operators discovered
 *
 * @li errno = ENOEXEC scan command is not ready
 * @li errno = ETIMEDOUT no GPRS network found
 * @li errno = EBADF error while parsing the scan results
 * @li errno = ENOTCONN The device is not on (GPRS module is down) 
 */
/*SVC_PROTOTYPE*/ struct netOperatorList net_gprsSiteSurvey( void );


 /** 
 * Obtain a GSM site Cell list
 * Note: This function is deprecated. Use net_gprsCellSurveyExt instead.
 *
 * @return struct netCellList - a list of cells discovered
 *  
 * @li errno = ENOEXEC scan command error 
 */
/*SVC_PROTOTYPE*/ struct netCellList net_gprsCellSurvey( void );

 /** 
 * Obtain a GSM/3G site Cell list
 *
 * @return struct netCellListExt - a list of cells discovered
 *  
 * @li errno = ENOEXEC scan command error 
 */
/*SVC_PROTOTYPE*/ struct netCellListExt net_gprsCellSurveyExt( void );

/** 
 * Obtain the GSM site operator's name
 *
 * @return struct netOperator - contains the current operator's name
 *
 * @li errno = ETIMEDOUT no operators found
 * @li errno = ENOEXEC scan command error 
 */
/*SVC_PROTOTYPE*/ struct netOperator net_gprsOperatorName( void );


/** 
 * Obtain the GSM site operator's name (LAI format)
 *
 * @return struct netOperator - contains the current operator's name (LAI format)
 *
 * @li errno = ETIMEDOUT no operators found
 * @li errno = ENOEXEC scan command error 
 */
/*SVC_PROTOTYPE*/ struct netOperator net_gprsOperatorLAI( void );

/** 
 * Power off the GPRS Module
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li ENOEXEC Unable to power off the module 
 */
/*SVC_PROTOTYPE*/ int net_gprsPowerOffModule();

/** 
 * Get the the PIN remaining attempts for the SIM
 * 
 *
 * @return >= 0 the PIN remaining attempts, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters   
 * @li EFAULT NO Sim card detected on the slot
 * @li ENOTCONN he GPRS module is off
 * @li ENOEXEC command execution error 
 * @li ETIMEDOUT Timeout occured
 * 
 */
/*SVC_PROTOTYPE*/ int net_gprsGetPinAttempts();


/** 
 * Send the AT command to the GSM Module
 *
 * @param[in] tx AT commands to send 
 * @param[in] response_timeout timeout in seconds 
 *  
 * @return 0 = rx response, null = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li ENOMSG AT command is not supported
 * @li ENOEXEC AT command execution error
 */
/*SVC_PROTOTYPE*/ char* net_gprsSendATCommands( char *tx, int response_timeout );

/**
 * Get the last PPP exit status
 *
 * @return NET_PPP_EXIT_[XXX] = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li errno = ENOEXEC command execution error
 */
/*SVC_PROTOTYPE*/ int net_pppGetExitStatus();

/**
 * Send an SMS
 *
 * @param[in] number destination
 * @param[in] message message to send
 *
 * @return 0 = Success, -1 = Error - Check Errno 
 *
 * @li EINVAL bad input parameters
 * @li EBADF SMS message is too long
 * @li ENOMSG Radio not ready to send the SMS
 */
/*SVC_PROTOTYPE*/ int net_gprsSendSMS( char *number, char *message );

/**
 * Get the current Radio Access Technology (2G, 3G)
 *
 * @return NET_GSM_OPR_RAT_[XXX] = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li errno = ENOEXEC command execution error 
 */
/*SVC_PROTOTYPE*/ int net_gprsGetRadioAccessTechnology();

/**
 * @brief Create a network bridge between two Ethernet interfaces
 *
 * @param[in] iface0 first Ethernet Interface
 * @param[in] iface1 second Ethernet Interface
 *
 * @par Detailed function description:
 * @verbatim
 * This function is used to create a network bridge between two Ethernet interfaces
 * @endverbatim
 *    
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters 
 *
*/
/*SVC_PROTOTYPE*/ int net_createBridge( char *iface0, char *iface1 );

/**
 * @brief Delete a network bridge between two Ethernet interfaces
 *
 * @param[in] iface0 first Ethernet Interface
 * @param[in] iface1 second Ethernet Interface
 *
 * @par 	Detailed function description:
 * @verbatim
 * This function is used to delete a network bridge between two Ethernet interfaces
 * 
 * The bridge MAC address is set to the iface0 one
 * 
 * @endverbatim
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters 
 *
*/
/*SVC_PROTOTYPE*/ int net_deleteBridge( char *iface0, char *iface1 );

/** 
 * Start/Stop the ifplugd daemon when a new interface is dynamically connected
 *
 * @param[in] p_iface Ethernet interface on which to start/stop the ifplugd daemon
 * @param[in] ethon if 1 start the ifplugd and if 0, stop the ifplugd
 *  
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li ENETDOWN The Interface is not online (Ethernet cable not plugged for example)
 */
/*SVC_PROTOTYPE*/ int net_ethernetOnOff( char *p_iface, int ethon );


/** 
 * {!DEPRECATED}
 *
 * @param[in] p_iface WIFI interface name
 * @param[in] wifion 1 when connected message is received or 0 otherwise
 *  
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li ENETDOWN The Interface is not online
 */
/*SVC_PROTOTYPE*/ int net_wifiOnOff( char *p_iface, int wifion );



/** 
 * 
 * {!DEPRECATED}
 *
 * Called when an Online/Offline event is captured (Ethernet Cable plug in/out, WIFI AP dis/connected, USB in/out ... )
 *
 * @param[in] p_iface the interface name (eth0, wlan0, bnep0, usb0 ... )
 * @param[in] eventtype possible values are defined by eventType enum (network online, offline, up, down)
 *  
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li ENETDOWN The Interface is not online
 */
/*SVC_PROTOTYPE*/ int net_hotplugAction( char *p_iface, int eventtype );

/** 
 * DEPRECATED! use svc_usbg!
 * Indicate the USB gadget module that will be loaded at boot.
 *
 * @param[in] gadget can be one of the following values:
 *
 * @li NET_USB_GADGET_DEFAULT load the default gadget (default is gadget Serial)
 * @li NET_USB_GADGET_SERIAL load the gadget serial
 * @li NET_USB_GADGET_ETHERNET load the gadget Ethernet
 * @li NET_USB_GADGET_COMPOSITE load the gadget composite
 *
 * @return 0 = Success, -1 = Error
 * 
 * @li EINVAL bad input parameters
 */
/*SVC_PROTOTYPE*/ int net_usbGadgetSetCfg( int gadget );

/** 
 * DEPRECATED! use svc_usbg!
 * Retrieve the USB gadget module that will be loaded at boot.
 *
 * @return > 0 = Success, -1 = Error
 * @li NET_USB_GADGET_DEFAULT load the default gadget (default is gadget Serial)
 * @li NET_USB_GADGET_SERIAL load the gadget serial
 * @li NET_USB_GADGET_ETHERNET load the gadget Ethernet
 * @li NET_USB_GADGET_COMPOSITE load the gadget composite
 * 
 * @li EINVAL bad input parameters or error related to conf file.
 */
/*SVC_PROTOTYPE*/ int net_usbGadgetGetCfg();


/** 
 * Initialize the modem when issuing AT commands
 *
 * @param[in] modemType type for modem (NET_MONDEM_PSTN, NET_MONDEM_UXPSTN, NET_MONDEM_UXISDN );
 * @param[in] usr_commands_file AT commands file used with modem_start function (may be null)
 * @param[in] forPPP (1-When using net_interfaceUp("ppp0") or 0 if using net_sessionConnect() afterward).
 *  
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li ENETDOWN The Interface is not online (Ethernet cable not plugged for example)
 */
/*SVC_PROTOTYPE*/ int net_modemInitializeATCommands( char *modemType, char *usr_commands_file, int forPPP );

/** 
 * Send the AT command to the modem
 *
 * @param[in] tx AT commands to send
 * @param[in] max_retries number of retries
 * @param[in] response_timeout timeout in ms
 * @param[in] forPPP (1-When using net_interfaceUp("ppp0") or 0 if using net_sessionConnect() afterward).
 *  
 * @return 0 = rx response, null = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li ENETDOWN The Interface is not online (Ethernet cable not plugged for example)
 */
/*SVC_PROTOTYPE*/ char* net_modemSendATCommands( char *tx, int max_retries, int response_timeout, int forPPP );


/** 
 * Select the SIM to be used
 *
 * @param[in] sim_number SIM number to use ( NET_GSM_SIM_1 or NET_GSM_SIM_2). After booting the terminal, the default SIM is SIM-1, switching SIMs can only be done when the radio is off.
 *  
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li ENOEXEC SIM selection failed
 */
/*SVC_PROTOTYPE*/ int net_gprsSelectSIM( int sim_number );

/**
 * @brief Switch Bluetooth power
 *
 * @param[in] operation - power switching operation: NET_BT_PWR_UP or NET_BT_PWR_DOWN
 *
 * @par Detailed function description:
 * @verbatim
 * This function is used to power on or off the internal Bluetooth module.
 *
 * On power down, it disconnects all existing connections to remote Bluetooth devices if any.
 * On power up, pairing is restored with previous paired base but no connection is established.
 * @endverbatim
 *
 * @return 0 when operation was successful, or -1 on failure with errno
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINVAL Invalid input parameter
*/
/*SVC_PROTOTYPE*/ int net_bluetoothPower( int operation );

/**
 * @brief Change pairing method
 *
 * @param[in] method pairing method (NET_BT_PAIRING_TYPE_LEGACY, NET_BT_PAIRING_TYPE_SSP_NO_PASSKEY, NET_BT_PAIRING_TYPE_SSP_PASSKEY, NET_BT_PAIRING_TYPE_SSP_AUTO)
 *
 * @par Detailed function description:
 * @verbatim
 * This function change the pairing method used for next net_bluetoothPair calls
 * Legacy, just-works and SSP+passkey are standards Bluetooth pairing method. Auto SSP is specific
 * to Verifone that derives the passkey used from the Vx base serial number
 * @endverbatim
 *
 * @return 0 when operation was successful, or -1 on failure with errno
 * @li EINVAL bad input parameters
*/
/*SVC_PROTOTYPE*/ int net_bluetoothSetPairingMethod( int method );

/**
 * @brief Pair with a Bluetooth device
 *
 * @param[in] pin PIN code for the device pairing
 * @param[in] bt_device Bluetooth device address or its registered name id (ex: 'bt_base0')
 *
 * @par Detailed function description:
 * @verbatim
 * This function is used to pair the handset with another Bluetooth device.
 * If bt_device is NULL, it pairs with the first registered BT device (i.e. 'bt_base0')
 *
 * PIN must be set accoding to the pairing method previously set by net_bluetoothSetPairingMethod:
 * - NET_BT_PAIRING_TYPE_LEGACY : Legacy PIN
 * - NET_BT_PAIRING_TYPE_SSP_NO_PASSKEY : value ignored
 * - NET_BT_PAIRING_TYPE_SSP_PASSKEY : passkey 
 * - NET_BT_PAIRING_TYPE_SSP_AUTO : Bluetooth base serial number, format 'XXX-XXX-XXX'
 * @endverbatim
 *
 * @return 0 when the pairing was successful, or -1 on failure with errno
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINVAL Invalid parameters
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with Bluetooth device
 * @li EEXIST The Bluetooth device is already paired
 * @li ENOMEM Allocation failed   
*/
/*SVC_PROTOTYPE*/ int net_bluetoothPair( char *pin, char *bt_device );

/**
 * @brief Unpair with a Bluetooth device
 *
 * @param[in] bt_device Bluetooth device address or its registered name id (ex: 'bt_base0')
 *
 * @par Detailed function description:
 * @verbatim
 * This function is used to unpair the handset with a Bluetooth device
 * If bt_device is NULL, it pairs with the first registered BT device (i.e. 'bt_base0')
 * @endverbatim
 *
 * @return 0 on success if unpairing was successful, or errno for failure. errno:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINVAL Invalid parameters
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with Bluetooth device
 * @li ENETDOWN The Bluetooth device is not paired with the remote device
 * @li ENOMEM Allocation failed
*/
/*SVC_PROTOTYPE*/ int net_bluetoothUnpair( char *bt_device );

/**
 * @brief Connect PAN profile
 *
 * @param[in] bt_device Bluetooth device address or its registered name id (ex: 'bt_base0')
 *
 * @par Detailed function description:
 * @verbatim
 * This function is used to connect PAN profile to a paired device.
 * On success, bnep0 connection can be used as an Ethernet interface.
 * If bt_device is NULL, it connects to the first registered BT device (i.e. 'bt_base0')
 *
 * Only one PAN connection is allowed at once, new PAN connection will disconnect
 * the existing PAN connection if any.
 * This profile can be shared and several handsets can connect PAN to the same remote device.
 * PAN is automatically disconnected when entering into sleep mode and when the remote device
 * become out of range. It is automatically restored when when signal is back and on wake up 
 * if auto_start="1" and pan_profile="1".
 * @endverbatim
 *
 * @return 0 when the connection was successful, or -1 on failure with errno
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINVAL Invalid Bluetooth address
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with Bluetooth device
 * @li ENOMEM Allocation failed
 * @li EEXIST Device already connected
 * @li ENOEXEC PAN connection unsuccessful
 * @li ENETDOWN Devices are not paired
*/
/*SVC_PROTOTYPE*/ int net_bluetoothConnectPAN( char *bt_device );

/**
 * @brief Disconnect PAN profile
 *
 * @param[in] bt_device Bluetooth device address or its registered name id (ex: 'bt_base0')
 *
 * @par Detailed function description:
 * @verbatim
 * This function is used to disconnect PAN profile.
 * If bt_device is NULL, it disconnects from the first registered BT device (i.e. 'bt_base0')
 * @endverbatim
 *
 * @return 0 when the disconnection was successful, or -1 on failure with errno
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINVAL Invalid Bluetooth address
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with Bluetooth device
 * @li ENOMEM Allocation failed
 * @li ENOEXEC PAN connection not killed
 * @li EHOSTDOWN bnep0 interface does not exist
*/
/*SVC_PROTOTYPE*/ int net_bluetoothDisconnectPAN( char *bt_device );

/**
 * @brief Connect DUN profile
 *
 * @param[in] bt_device Bluetooth device address or its registered name id (ex: 'bt_base0')
 *
 * @par Detailed function description:
 * @verbatim
 * This function is used to connect DUN profile to a paired device.
 * If bt_device is NULL, it connects to the first registered BT device (i.e. 'bt_base0').
 * On success, the remote modem can be used for svc_net sessions (use 'modem_base' name id).
 *
 * This profile is exclusive and will fail if another handset is already connected 
 * to DUN on the same remote device.
 * DUN profile is automatically disconnected when entering into sleep mode
 * and when the remote device become out of range and not automatically restored.
 * @endverbatim
 *
 * @return 0 when the connection was successful, or -1 on failure with errno
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINVAL Invalid parameters
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with Bluetooth device
 * @li ENOMEM Allocation failed
 * @li ENETDOWN Devices are not paired
 * @li ENOEXEC Error in SPP connection
 * @li EEXIST Modem device already exists
*/
/*SVC_PROTOTYPE*/ int net_bluetoothConnectDUN( char *bt_device );

/**
 * @brief Disconnect DUN profile
 *
 * @param[in] bt_device Bluetooth device address or its registered name id (ex: 'bt_base0')
 *
 * @par Detailed function description:
 * @verbatim
 * This function is used to disconnect DUN profile
 * If bt_device is NULL, it disconnects from the first registered BT device (i.e. 'bt_base0')
 * @endverbatim
 *
 * @return 0 when the disconnection was successful, or -1 on failure with errno
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINVAL Iinvalid parameters
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with Bluetooth device
 * @li ENOMEM Allocation failed
 * @li ENOEXEC Error in disconnecting DUN
*/
/*SVC_PROTOTYPE*/ int net_bluetoothDisconnectDUN( char *bt_device );

/**
 * @brief Connect SPP profile
 *
 * @param[in] bt_device Bluetooth device address or its registered name id (ex: 'bt_base0')
 * @param[in] channel SPP channel
 *
 * @par Detailed function description:
 * @verbatim
 * This function is used to connect SPP profile to a paired device.
 * If bt_device is NULL, it connects to the first registered BT device (i.e. 'bt_base0').
 *
 * This profile is exclusive and will fail if another handset is already
 * connected to SPP on the same remote device and channel.
 * SPP profile is automatically disconnected when entering into sleep mode
 * and when the remote device become out of range and not automatically restored.
 * @endverbatim
 *
 * @return 0 when the connection was successful, or -1 on failure with errno
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINVAL Invalid parameters
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with Bluetooth device
 * @li ENOMEM Allocation failed
 * @li EHOSTDOWN Host is down
 * @li ETIMEDOUT Connection attempt has timed out
 * @li ENOEXEC Error in connecting SPP
*/
/*SVC_PROTOTYPE*/ int net_bluetoothConnectSPP( char *bt_device, int channel );

/**
 * @brief Disconnect SPP profile
 *
 * @param[in] bt_device Bluetooth device address or its registered name id (ex: 'bt_base0')
 * @param[in] channel SPP channel
 *
 * @par Detailed function description:
 * @verbatim
 * This function is used to disconnect SPP profile.
 * If bt_device is NULL, it disconnects from the first registered BT device (i.e. 'bt_base0')
 * @endverbatim
 *
 * @return 0 when the disconnection was successful, or -1 on failure with errno
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINVAL Invalid parameters
 * @li ENOMEM Memory management error
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with service manager server
 * @li ENOTCONN No SPP connection for the given address-channel pair
*/
/*SVC_PROTOTYPE*/ int net_bluetoothDisconnectSPP( char *bt_device, int channel );

/**
 * @brief Write Bluetooth device configuration
 *
 * @param[in] bt_device name id of a Bluetooth device ('bt_base0')
 * @param[in] btDevInfo convoy Bluetooth device configuration to store
 *
 * @par Detailed function description:
 * @verbatim
 * This function is used to write configuration of a remote Bluetooth device.
 * If bt_device is NULL, it returns configuration of the first registered BT device (i.e. 'bt_base0').
 *
 * This configuration is used on start-up and when resuming from suspend mode to power on Bluetooth
 * when auto_start property is '1' and to connect PAN if pan_profile is '1'.
 * @endverbatim
 *
 * @return 0 = Success, -1 = Error - Check Errno
 * @li ESRCH Configuration file error
 */
/*SVC_PROTOTYPE*/ int net_bluetoothSetInfo( char *bt_device, struct netBluetoothInfo btDevInfo );

/**
 * @brief Read Bluetooth device configuration
 *
 * @param[in] bt_device name id of a Bluetooth device (ex: 'bt_base0')
 *
 * @par Detailed function description:
 * @verbatim
 * This function is used to retrieve registered configuration of a remote Bluetooth device.
 * If bt_device is NULL, it returns configuration of the first registered BT device (i.e. 'bt_base0').
 * @endverbatim
 *
 * @return struct netBluetoothInfo - Bluetooth device configuration
 * @li errno = 0, in case of success
 * @li errno = EINVAL bad input parameters
 * @li errno = ESRCH, the device address is not referenced
 */
/*SVC_PROTOTYPE*/ struct netBluetoothInfo net_bluetoothGetInfo( char *bt_device );

/**
 * @brief Get pairing/connection status
 *
 * @param[in] bt_device name id of a Bluetooth device (ex: 'bt_base0')
 *
 * @par Detailed function description:
 * @verbatim
 * This function is used to retrieve current pairing and connection status with a remote Bluetooth device.
 * If bt_device is NULL, it returns status with the first registered BT device (i.e. 'bt_base0')
 * @endverbatim
 *
 * @return struct netBluetoothInfo - the interface
 * @li errno = 0, in case of success
 * @li errno = EINVAL bad input parameters
 * @li errno = ESRCH, the device address is not referenced
 */
/*SVC_PROTOTYPE*/ struct netBluetoothStatus net_bluetoothGetStatus( char *bt_device );

/**
 * @brief Get RSSI of Bluetooth signal
 *
 * @param[in] bt_device name id of a Bluetooth device (ex: 'bt_base0')
 *
 * @par Detailed function description:
 * @verbatim
 * This function is used to get RSSI of Bluetooth signal with a remote Bluetooth device.
 * If bt_device is NULL, it returns status with the first registered BT device (i.e. 'bt_base0')
 * @endverbatim
 *
 * @return RSSI value, check errno for errors
 * @li errno = 0, in case of success
 * @li errno = EINVAL bad input parameters
 * @li errno = ESRCH, the device address is not referenced
 * @li errno = ENOEXEC, the remote device is unreachable
 */
/*SVC_PROTOTYPE*/ int net_bluetoothGetRssi( char *bt_device );

/**
 * @brief Get link quality of Bluetooth signal
 *
 * @param[in] bt_device name id of a Bluetooth device (ex: 'bt_base0')
 *
 * @par Detailed function description:
 * @verbatim
 * This function is used to get link quality of Bluetooth signal with a remote Bluetooth device.
 * If bt_device is NULL, it returns status with the first registered BT device (i.e. 'bt_base0')
 * @endverbatim
 *
 * @return LQ value, check errno for errors
 * @li errno = 0, in case of success
 * @li errno = EINVAL bad input parameters
 * @li errno = ESRCH, the device address is not referenced
 * @li errno = ENOEXEC, the remote device is unreachable
 */
/*SVC_PROTOTYPE*/ int net_bluetoothGetLinkQuality( char *bt_device );

/**
 * Get link quality of Bluetooth signal
 * 
 * @param[in] bt_device Remote bluetooth address or XML id
 *
 * @return struct netBluetoothApInfo, check errno for errors
 *
 * @li errno = 0, in case of success
 * @li errno = EINVAL bad input parameters
 * @li errno = ESRCH, the device adress is not referenced
 *
 */
/*SVC_PROTOTYPE*/ struct netBluetoothApInfo net_bluetoothGetApInfo( char *bt_device );

/**
 * @brief Gather Bluetooth device information (SDP)
 *
 * @param[in] deviceAddr Bluetooth address of the interrogated device
 * @param[in] profile mask of the searched profiles (1:OBEX, 2:PAN, 4:DUN, 8:SP)
 * @param[in] inquiry_time search timeout
 *
 * @par Detailed function description:
 * @verbatim
 * This function interrogate remote paired device to get its information/capabilities using
 * Software Discovery Protocol.
 * If deviceAddr is NULL, all nearby devices in discoverable state are interrogated (16 MAX).
 * @endverbatim
 *
 * @return struct netBluetoothList - list of Bluetooth devices discovered
 * @li ENODEV Bluetooth not available
 * @li ENOENT Bluetooth SW not found
 * @li EHOSTDOWN Bluetooth is down
 * @li EAGAIN The system lacked the necessary resources
 * @li EINPROGRESS Browsing is still in progress
 * @li ECOMM SDP connection failed. Only when address was specified
 */
/*SVC_PROTOTYPE*/ struct netBluetoothList net_bluetoothSurvey( char *deviceAddr, int profile, int inquiry_time );

/**
 * @brief Scan nearby discoverable Bluetooth devices
 *
 * @param[in] timeout search timeout ( Not supported - RFU ) 
 *
 * @par Detailed function description:
 * @verbatim
 * This function is used to scan all nearby Bluetooth devices in discoverable state.
 * @endverbatim
 *
 * @return struct netBluetoothList - a list of Bluetooth devices discovered
 * @li ENODEV Bluetooth not available
 * @li ENOENT Bluetooth SW not found
 * @li EHOSTDOWN Bluetooth is down
 * @li EAGAIN The system lacked the necessary resources
 * @li EINPROGRESS Browsing is still in progress
 * @li ECOMM SDP connection failed. Only when address was specified
 */
/*SVC_PROTOTYPE*/ struct netBluetoothDevList net_bluetoothScan( int timeout );

/**
 * @brief Retrieve a list of paired devices
 *
 * @par Detailed function description:
 * @verbatim
 * This function is used to get the list of Bluetooth devices paired with the terminal.
 * @endverbatim
 *
 * @return struct netBluetoothList - a list of Bluetooth devices discovered
 * @li ENODEV Bluetooth not available
 * @li ENOENT Bluetooth SW not found
 * @li EHOSTDOWN Bluetooth is down
 * @li ECOMM Communication error with Bluetooth daemon
 */
/*SVC_PROTOTYPE*/ struct netBluetoothDevList net_bluetoothPairedList( );


/**
 * @brief Enable inquiry scan (Trident only)
 *
 * @param[in] discoveryTimeout visibility timeout in seconds (30-600s)
 *
 * @par Detailed function description:
 * @verbatim
 * This function activates inquiry scan during given timeout.
 * @endverbatim
 *
 * @return 0 = Success, -1 = Error - Check Errno
 * @li ENODEV Bluetooth not available
 * @li ENOENT Bluetooth SW not found
 * @li EHOSTDOWN Bluetooth is down
 * @li ECOMM Communication error with Bluetooth daemon
 */
int net_bluetoothEnableIncomingSSP(int discoveryTimeout);

/**
 * @brief Disable inquiry scan (Trident only)
 *
 * @par Detailed function description:
 * @verbatim
 * This function deactivates inquiry scan
 * @endverbatim
 *
 * @return 0 = Success, -1 = Error - Check Errno
 * @li ENODEV Bluetooth not available
 * @li ENOENT Bluetooth SW not found
 * @li EHOSTDOWN Bluetooth is down
 * @li ECOMM Communication error with Bluetooth daemon
 */
int net_bluetoothDisableIncomingSSP(void);

/**
 * @brief Get incoming pairing request (Trident only)
 *
 * @par Detailed function description:
 * @verbatim
 * This function retrieves incoming pairing request from a remote Bluetooth device.
 * @endverbatim
 *
 * @return struct netBluetoothSSPRequest - incoming pairing request information if exists, empty structure on error with errno:
 * @li ENODEV Bluetooth not available
 * @li ENOENT Bluetooth SW not found
 * @li EHOSTDOWN Bluetooth is down
 * @li ECOMM Communication error with Bluetooth daemon
 * @li ENOTCONN Discovery mode not enabled
 * @li ENOMSG no request available 
 */
struct netBluetoothSSPRequest net_bluetoothGetSSPRequest(void);

/**
 * @brief Accept pairing request (Trident only)
 *
 * @par Detailed function description:
 * @verbatim
 * This function validates a pairing request coming from a remote Bluetooth device
 * @endverbatim
 *
 * @return 0 = Success, -1 = Error - Check Errno:
 * @li ENODEV Bluetooth not available
 * @li ENOENT Bluetooth SW not found
 * @li EHOSTDOWN Bluetooth is down
 * @li ECOMM Communication error with Bluetooth daemon
 * @li ENOTCONN Discovery mode not enabled
 */
int net_bluetoothAcceptSSPRequest(const char *remoteDeviceAddress, const char *pairingCodeString);

/**
 * @brief Reject pairing request (Trident only)
 *
 * @par Detailed function description:
 * @verbatim
 * This function rejects a pairing request coming from a remote Bluetooth device.
 * @endverbatim
 *
 * @return 0 = Success, -1 = Error - Check Errno:
 * @li ENODEV Bluetooth not available
 * @li ENOENT Bluetooth SW not found
 * @li EHOSTDOWN Bluetooth is down
 * @li ECOMM Communication error with Bluetooth daemon
 * @li ENOTCONN Discovery mode not enabled
 */
int net_bluetoothRejectSSPRequest(const char *remoteDeviceAddress);

/**
 * @brief Start iBeacon advertising
 *
 * @param[in] ibeacon - advertisement data
 * @param[in] activate - NET_ACTIVATE_ON restore after reboot, NET_ACTIVATE_ON_ONCE only once
 *
 * @par Detailed function description:
 * @verbatim
 * Start advertising the netIBeacon value passed as parameter
 * @endverbatim
 *
 * @return 0 = Success, -1 = Error - Check Errno
 * @li ENODEV Bluetooth not available
 * @li ENOENT Bluetooth SW not found
 * @li EHOSTDOWN Bluetooth is down
 * @li EAGAIN The system lacked the necessary resources
 * @li EINPROGRESS Advertising already in progress
 *
 */
/*SVC_PROTOTYPE*/ int net_ibeaconStartAdvertise( struct netIBeacon ibeacon, int activate );

/**
 * Return the current netIBeacon value which is currently advertised if any
 *
 * @return struct netIBeacon - advertisement data, or empty structure if no advertising
 * @li ENODEV Bluetooth not available
 * @li ENOENT Bluetooth SW not found
 * @li EHOSTDOWN Bluetooth is down
 * @li EAGAIN The system lacked the necessary resources
 *
 */
/*SVC_PROTOTYPE*/ struct netIBeacon net_ibeaconGetInfo();

/*
 * Stop IBeacon advertising
 *
 * @return 0 = Success, -1 = Error - Check Errno
 * @li ENODEV Bluetooth not available
 * @li ENOENT Bluetooth SW not found
 * @li EHOSTDOWN Bluetooth is down
 * @li EAGAIN The system lacked the necessary resources
 */
/*SVC_PROTOTYPE*/ int net_ibeaconStopAdvertise();

/** 
 * Sets and gets values using AT commands and libgsm capabilities
 *
 * @param[in] id Parameter ID (AT command number)
 * @param[in] param Pointer to a buffer holding a value
 * @param[in] size Pointer to Size of param void pointer 
 *  
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters 
 */
/*SVC_PROTOTYPE*/ struct netSatNavCommand net_satNavCommand( int id, char *param, int size );

/** 
 * Start watching the network events
 *
 *  
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL general error occured 
 */
/*SVC_PROTOTYPE*/ int net_eventAttach();

/** 
 * Stop watching the network events
 *
 *  
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL general error occured 
 */
/*SVC_PROTOTYPE*/ int net_eventDetach();

/** 
 * Monitor network events
 *
 * @param timeout timeout in seconds to wait for an event (if timeout is 0, read already available event en exit immediately)
 *  
 * @return struct netEvent - contains the current event if errno is 0 (success)
 *
 * @li EINVAL general error occured 
 * @li EBADF event monitoring is not attached
 * @li ETIMEDOUT no event captured within timeout seconds
 * @li EINVAL general error occured
 */
/*SVC_PROTOTYPE*/ struct netEvent net_eventGet( int timeout );


/** 
 * Post a network event
 *
 * @param event contains event information to send
 *  
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL general error occured 
 * @li EBADF event monitoring is not attached
 */
/*SVC_PROTOTYPE*/ int net_postEvent( struct netEvent event );

/**
 * !! Reserved to APM only !!
 * Notify net service of a power event
 *
 * @param[in] event Event ID (Power event number)
 *  
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters 
 */
/*SVC_PROTOTYPE*/ int net_notifyEvent( int event );

/** 
 * Set the current net syslog level
 *
 * @param level New log level (values: NET_LOG_INFO, NET_LOG_DEBUG or NET_LOG_ALL)
 *  
 * @return 0 = Success, -1 = Error - Check Errno
 *
 */
/*SVC_PROTOTYPE*/ int net_setSyslogLevel( int level );

/** 
 * Get the current net syslog level (default is NET_LOG_INFO )
 *
 * @return 0 = Success, -1 = Error - Check Errno
 */
/*SVC_PROTOTYPE*/ int net_getSyslogLevel();


 /**
 * Creates a wpa_supplicant configuration file for WPA-PSK networks using provided ssid and psk.
 * If the file already exists, it will be overwritten.
 *
 * @param[in] filePath pathname of the file to be created. If NULL or "" then NET_WPA_SUPPLICANT_DEFAULT_CONFIG_FILE is used.
 * @param[in] ssid network name as an ASCII string with double quotation, or as hex string.
 * @param[in] psk 64 hex-digit passkey string, or 8 to 63 ASCII characters passphrase string with double quotation.
 *
 * @return 0 = success, -1 = Error. errno is set
 *
 * @li EINVAL bad input parameters
 * @li ENOMEM not enough memory
 * @li EBADF file access error
 * @li EIO file I/O error
 */
/*SVC_PROTOTYPE*/ int net_defSupplicantFileCreate( char* filePath, char* ssid, char* psk );


 /**
 * Creates a wpa_supplicant configuration file for WiFi networks using provided parameters.
 * If the file already exists, it will be overwritten.
 *
 * @param[in] filePath pathname of the file to be created. If NULL or "" then NET_WPA_SUPPLICANT_DEFAULT_CONFIG_FILE is used.
 * @param[in] ssid network name as an ASCII string with double quotation, or as hex string.
 * @param[in] psk 64 hex-digit passkey string, or 8 to 63 ASCII characters passphrase string with double quotation.
 * @param[in] key_mgt Key management to be used. Allowable values are WPA-PSK and/or WPA-EAP. If "" or NULL, then NET_WPA_SUPPLICANT_DEFAULT_KEY_MANAGEMENT will be used
 * @param[in] pairwise Pairwise cipher to be used. Allowable values are CCMP and/or TKIP. If "" or NULL, then NET_WPA_SUPPLICANT_DEFAULT_PAIRWISE will be used.
 * @param[in] group Group cipher to be used. Allowable values are CCMP and/or TKIP. If "" or NULL, then NET_WPA_SUPPLICANT_DEFAULT_GROUP will be used.
 * @param[in] proto Protocol to be used. Allowable values are RSN, WPA2, and/or WPA. If "" or NULL, then NET_WPA_SUPPLICANT_DEFAULT_PROTO will be used.
 *
 * @return 0 = success, -1 = Error. errno is set
 *
 * @li EINVAL bad input parameters
 * @li ENOMEM not enough memory
 * @li EBADF file access error
 * @li EIO file I/O error
 **/
/*SVC_PROTOTYPE*/ int net_defSupplicantFileCreateExt( char* filePath, char* ssid, char* psk, char *key_mgmt, char *pairwise, char *group, char *proto);

 /**
 * Returns the value of a variable from a wpa_supplicant network configuration file
 *
 * @param[in] filePath pathname of the file to read. If NULL or "" then NET_WPA_SUPPLICANT_DEFAULT_CONFIG_FILE is used.
 * @param[in] name name of the variable to search for.
 *
 * @return the variable value as ASCII string or empty string if not found. Quoted values are returned with quotes. 
 * @return On error, NULL is returned and errno is set.
 *
 * @li EINVAL bad input parameters
 * @li ENOMEM not enough memory
 * @li EBADF file access error
 * @li EIO file I/O error
 */
/*SVC_PROTOTYPE*/ char* net_defSupplicantFileGetValue( char* filePath, char* name );



#ifdef SVC_USE_IPV6

/**
 * Perform DNS resolution on a given interface for IPv6
 *
 * @param[in] p_dnsName DNS name to resolve
 * @param[in] p_iface newtork interface on which to make the domain name resolution
 *
 * @return string - IP address
 *
 * @return IP obtained from DNS resolution = success, null string = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li EFAULT the parameter provided is not an IP address
 * @li EBADF error while reading the dns file
 * @li ENONET IP interface is up
 * @li ENOLINK error while creating the DNS link file
 *
 */
/*SVC_PROTOTYPE*/ struct netIfconfigIpv6 net_nsLookupIpv6( char *p_dnsName, char *p_iface );
  
/**
 * IPv6 - Write IP interface configuration in the default XML file
 *
 * @param[in] config convoy data to store in the XML file (structure config contains element interface that contains name of interface to set)
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li errno = EINVAL bad input parameters
 * @li errno = ESRCH XML file error
 *
 */
/*SVC_PROTOTYPE*/ int net_interfaceSetIpv6( struct netIfconfigIpv6 config );


/**
 * IPv6 - Read IP interface configuration from the default XML file
 *
 * @param[in] p_iface interface name (eth0, wlan0...) for which to read information
 *
 * @return struct netIfconfig - the interface
 *
 * @li errno = 0, in case of success
 * @li errno = ESRCH, the interface is not present in the XML file
 *
 */
/*SVC_PROTOTYPE*/ struct netIfconfigIpv6 net_interfaceGetIpv6( char *p_iface );

/**
 * IPv6 - Read the interface link status and return the current Interface IP information
 * mac address is filled whatever the interface status is.
 * activate parameter is updated according to it's value in the XML file
 *
 * @param[in] p_iface interface (eth0, wlan0, ppp0...) for which to read information
 *
 * @return struct netIfconfigIpv6 - IP information of the interface
 *
 * @li errno = 0, in case of success
 * @li errno = ENONET, if the network is down
 * @li errno = ENETDOWN The Interface is not online (Ethernet cable not plugged for example)
 *
 */
/*SVC_PROTOTYPE*/ struct netIfconfigIpv6 net_interfaceStatusIpv6( char *p_iface );

/**
 * Start WIFI supplicant for IPv6 interface,
 * according to parameters stored in the default file
 *
 * @param[in] startwpa if = 1, power up the module and start the WPA Supplicant,
 *                     if = 0 only power up the module
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EBADF wifi configuration file not found
 * @li EINVAL bad input parameters
 *
 */
/*SVC_PROTOTYPE*/ int net_wifiStartIpv6( int startwpa );

/**
 * Stop the WIFI supplicant for IPv6 interface
 *
 * @param[in] stopmodule 1=WPA_Supplicant and the WIFI module,
 *                       0=Stop the WPA_Supplicant only
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 */
/*SVC_PROTOTYPE*/ int net_wifiStopIpv6( int stopmodule );


/**
 * Perform DNS resolution on a given interface for IPv6
 *
 * @param[in] p_dnsName DNS name to resolve
 * @param[in] p_iface newtork interface on which to make the domain name resolution
 *
 * @return string - IP address
 *
 * @return IP obtained from DNS resolution = success, null string = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li EFAULT the parameter provided is not an IP address
 * @li EBADF error while reading the dns file
 * @li ENONET IP interface is up
 * @li ENOLINK error while creating the DNS link file
 *
 */
/*SVC_PROTOTYPE*/ struct netIfconfigIpv6 net_nsLookupIpv6( char *p_dnsName, char *p_iface );

/**
 * Start/Stop the ifplugd daemon when a new interface is dynamically connected
 *
 * @param[in] p_iface Ethernet interface on which to start/stop the ifplugd daemon
 * @param[in] ethon if 1 start the ifplugd and if 0, stop the ifplugd
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li ENETDOWN The Interface is not online (Ethernet cable not plugged for example)
 */
 /*SVC_PROTOTYPE*/ int net_ethernetOnOffIpv6( char *p_iface, int ethon );

/**
 * IPv6 - Add a static route to a specific host
 *
 * @param[in] p_dest host IP address (IPv6)
 * @param[in] p_gateway gateway to this host (IPv6)
 * @param[in] p_iface IP interface (eth0,ppp0...)
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li EFAULT invalid IP address was passed
 * @li EBADF error when reading the routing table
 * @li ENOEXEC routing table column is missing
 * @li EDESTADDRREQ no route was found
 * @li EHOSTUNREACH hostname resolution error
 */
/*SVC_PROTOTYPE*/ int net_addHostRouteIpv6( char * p_dest, char * p_gateway, char * p_iface );

/**
 * Delete a static route to a specific IPv6 host
 *
 * @param[in] p_dest host IP address (IPv6)
 * @param[in] p_gateway gateway to this host (IPv6)
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li EFAULT invalid IP address was passed
 * @li EBADF error when read the routing table
 * @li ENOEXEC routing table column is missing
 * @li EDESTADDRREQ no route was found
 */
/*SVC_PROTOTYPE*/ int net_delHostRouteIpv6( char * p_dest, char * p_gateway );

/**
  * IPv6 - Add a static route to a network
  *
  * @param[in] p_net network IP address
  * @param[in] netmask network mask
  * @param[in] p_gateway gateway to use
  * @param[in] p_iface IP interface (eth0,wlan0...)
  *
  * @return 0 = Success, -1 = Error - Check Errno
  *
  * @li EINVAL bad input parameters
  * @li EFAULT invalid IP address was passed
  * @li EBADF error when read the routing table
  * @li ENOEXEC routing table column is missing
  * @li EDESTADDRREQ no route was found
  * @li ENETDOWN interface was not found in the active interfaces list
  */
/*SVC_PROTOTYPE*/ int net_addNetRouteIpv6(char * p_net, const unsigned char netmask,
   char * p_gateway, char * p_iface);

/**
  * IPv6 - Delete a static route to a network
  *
  * @param[in] p_net network IP address
  * @param[in] netmask network mask
  *
  * @return 0 = Success, -1 = Error - Check Errno
  *
  * @li EINVAL bad input parameters
  * @li EFAULT invalid IP address was passed
  * @li EBADF error when read the routing table
  * @li ENOEXEC routing table column is missing
  * @li EDESTADDRREQ no route was found
  * @li ENETDOWN interface was not found in the active interfaces list
  */
/*SVC_PROTOTYPE*/ int net_delNetRouteIpv6(char * p_net, const unsigned char netmask);

/**
  * Select the default route according to the priority policy (eth0,wlan0,ppp0...)
  *
  * @return 0 = Success, -1 = Error - Check Errno
  *
  * @retval ENONET no active interface found
  */
/*SVC_PROTOTYPE*/ int net_autoSetDefaultRouteIpv6(void);

/**
  * Set the default route on a given interface
  *
  * @param[in] p_iface IP interface (eth0,wlan0...)
  *
  * @return 0 = Success, -1 = Error - Check Errno
  *
  * @li EINVAL bad input parameters
  * @li EBADF error when reading the default route file
  * @li ENONET no active interface found
  */
/*SVC_PROTOTYPE*/ int net_setDefaultRouteIpv6(char * p_iface);



/**
 * int net_isIpv6Address(char * p_net, char *buffer) returns 0 if it found a valid ipv6 address and it expands it in buffer
 *
 * @param[in] p_net ipv6 ip
 * @param[out] buffer: char array of at least 40 chars in which the expanded address is returned.
 *
 * @return 0 = Success, -1 = Error
 *
 */
/*SVC_PROTOTYPE*/ int net_isIpv6Address(char * p_net, char *buffer);

 /* Get the default route interface name for the ipv6 protocol
 *
 * @return Interface name (eth0,wlan0) which holds the default route = success, null string = Error - Check Errno
 *
 * @li EINVAL bad input parameters
 * @li EBADF error when read the default route file
 * @li ENONET no active interface found
 */
char* net_getDefaultRouteInfoIpv6();


#endif /* SVC_USE_IPV6 */

/**
 * int net_radioPowerOn(int radios) Turn  one or mode radios on
 *
 * @param[in] int radios  radio(s) to be turned on ( Bluetooth, wifi, .... )
 *
 * @return 0 = Success, -1 = Error
 *
 */
/*SVC_PROTOTYPE*/ int net_radioPowerOn( int radios );

/**
 * int net_radioPowerOff(int radios) Turn one or more radios off
 *
 * @param[in] int radios  radio(s) to be turned off ( Bluetooth, wifi, .... )
 *
 * @return 0 = Success, -1 = Error
 *
 */
/*SVC_PROTOTYPE*/ int net_radioPowerOff( int radios );

/**
 * int net_radioEnterSleep(int radios) Set radios  into sleep state
 *
 * @param[in] int radios radio(s) to set to sleep ( Bluetooth, wifi, .... )
 *
 * @return 0 = Success, -1 = Error
 *
 */
/*SVC_PROTOTYPE*/ int net_radioEnterSleep( int radios );

/**
 * int net_radioExitSleep(int radios) Get radios to exit sleep state
 *
 * @param[in] int radios  radio(s) exit sleep ( Bluetooth, wifi, .... )
 *
 * @return 0 = Success, -1 = Error
 *
 */
/*SVC_PROTOTYPE*/ int net_radioExitSleep( int radios );

/**
 * int pnet_radioGetPowerState( int radio ) Get radios status
 *
 * @return int The power state of the selected radio:
 *         RADIO ON, RADIO_OFF or RADIO_ERROR where RADIO_ERROR
 *         means either state couldn't be read, radio is not present
 *         or invalid/out of range input parameter.
 *
 */
/*SVC_PROTOTYPE*/ int net_radioGetPowerState( int radio );


/**
 * int net_getLinkStatus Get link status for the given interface
 *
 * @param[in]  char p_iface interface (eth0, wlan0, ppp0...) for which to read information
 *
 * @return int The link status  NET_INTERFACE_UP | NET_INTERFACE_DOWN |
 *		NET_INTERFACE_UNKNOWN |NET_INTERFACE_ERROR
 *		NET_INTERFACE_UNKNOWN is returned when inferface is present but  status cannot be determined
 *		NET_INTERFACE_ERROR is returned if link inferface cannot be determined due to it not being
 *			present or to some other error
 *
 * @li errno = 0, in case of success
 * @li errno = EINVAL  invalid argument(s)
 * @li errno = ENONET, if the network link is not detected
 * @li errno = ENETDOWN The interface is present but is not online (ethernet cable not plugged for example)
 */
/*SVC_PROTOTYPE*/ int net_getLinkStatus( char *p_iface );

/**
 * int net_getLinkQuality  Get information about the radios link quality for a given radio.
 *
 * @param[in]  int radio ( radio = NET_RADIO_BT, NET_RADIO_WIFI or NET_RADIO_CELLULAR) for which to read information
 *
 * @return int The percentage  ( w.r.t max RSSI dBm value for the  given radio) of the current radio RSSI
 *             or -1 if RSSI value cannot be read
 *
 */
/*SVC_PROTOTYPE*/  int net_getLinkQuality( int radio );

/**
 * int net_BluetoothGetPairScanInfo  Get information about the inrange paired BT devices.
 *
 * @return struct netBluetoothInfoExtList - list containing information on all inranged paired devices
 *             or -1 if RSSI value cannot be read
 *
 * @li errno = EINVAL bad input parameters
 * @li errno = ENOEXEC command execution error
 *
 */
struct netBluetoothInfoExtList  net_BluetoothGetPairScanInfo( void );


/**
 * int net_getBaseLinkInfo Get information about the base link information
 *
 * @param[in] p_iface interface for which to get the status
 * @param[in] address indicates the base address from which to get the information.
 *
 * @return 0 = Success, -1 = Error - Check Errno
 *
 * @li errno = 0, in case of success
 * @li errno = ENONET, if the network is down
 * @li errno = ENETDOWN The interface is not online (ethernet cable not plugged for example, or WIFI network disconnect)
 * @li errno = ENOTCONN The device is not on (ex. WIFI/Bluetooth module is down)
 */
/*SVC_PROTOTYPE*/  int net_getBaseLinkInfo( char *p_iface, char *address );

#ifdef RAPTOR

/*SVC_STRUCT*/
/**
 * Used to convoy information about a specific WiFi Firmware
 */
struct netWifiFw
{
	char name[NET_WIFI_FW_NAME_MAX];
};

/*SVC_STRUCT*/
/**
 * Define a list of Firmwares
 */
struct netWifiFwList
{
	struct netWifiFw *fw;
	int fw_count;
};

/**
 * struct net_wifiGetFwList Get information about the wifi firmware available
 *
 * @return struct netWifiFwList.fw != 0 Success, else Error - Check Errno
 *
 * @li errno = 0, in case of success
 * @li errno = ENOMEM, out of memory error
 * @li errno = EBADF, unable to access the fw directory
 */
/*SVC_PROTOTYPE*/ struct netWifiFwList net_wifiGetFwList(void);

/**
 * struct net_wifiFreeFwList frees list after using it
 */
/*SVC_PROTOTYPE*/ void net_wifiFreeFwList(struct netWifiFwList *fw_list);

/**
 * int set wifi firmware
 *
 * @return 0 on Success, -1 on Error - Check Errno
 *
 * @li errno = 0, in case of success
 * @li errno = ENONET, unable to write to cfg file
 */
/*SVC_PROTOTYPE*/ int net_wifiSetFw(struct netWifiFw fw);

/**
 * int get wifi firmware
 *
 * @return 0 on Success, -1 on Error - Check Errno
 *
 * @li errno = 0, in case of success
 * @li errno = EBADF, Unable to read cfg file
 */
/*SVC_PROTOTYPE*/ struct netWifiFw net_wifiGetFw(void);
#endif

#ifdef __cplusplus
}
#endif
#endif //SVC_NET_H


