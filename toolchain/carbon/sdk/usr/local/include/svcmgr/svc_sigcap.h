/** @addtogroup ui UI */
/** @{ */
/** @addtogroup sigcaptureservice  UI - Signature capture: Services */
/** @{ */
/** 
 *  @file svc_sigcap.h 
 *
 *  @brief  Signature capture
 *
 *	This header file contains information about the Signature capture.  
 *
 */

#ifndef SVC_SIGCAP_H
#define SVC_SIGCAP_H

#include "svcmgrSvcDef.h"

#ifdef __cplusplus
extern "C" {
#endif


/*SVC_SERVICE:sigcap*/



/*SVC_STRUCT*/
/**
 * Return image create Status
 */
struct imageCreateStatus_s {
  int result;			/** @li < 0 = Image creation complete @li < 0 Error (see errno) */
  char filename[256];	/**< Name of image file created with full pathing (if result == 0) */
};


/*SVC_STRUCT*/
/**
 * Returned Status Structure
 */
struct sigcapBeginRegionDrawReturn {
	int status;         /**< Return status if call to sigcap_beginRegionDraw() */
	char eventkey[32];	/**<  Event key (if requested via eventFlag param to sigcap_beginRegionDraw()), empty string otherwise */
};


/*SVC_STRUCT*/
/**
 * Sigcap Begin parameters
 */
struct sigcapBeginDisplayParams {
	unsigned long int points/*0*/;  /**< Amount of points (300 dps) to be used for image capture (0 - to use maxSeconds for calculation) */
	int  eventflag/*0*/;              /**< Nonzero value to enable event status for timeout and cancel notifications (see sigcap_beginRegionDraw()) */
	int  maxSeconds/*0*/;             /**< (0 - 180) maximum number of seconds to capture signature data (0 - to use points for calculation), (starts at first pen down) */
	int  timeoutPenDown/*0*/;         /**< Maximum seconds to wait until first pen down (0 - no timeout) */
	int  timeoutPenUp/*0*/;           /**< Maximum seconds to wait from a pen up until next pen down (0 - no timeout) */
	int  stylusOnlyRequested/*0*/;    /**< If stylus only will be used to capture signature data (none-zero) */
	int  lineWidth/*1*/;              /**< Width in pixels of signature line: 1 ,2 , 3 (default if > 3 specified) */
	int  red/*0*/;                    /**< Pin color, red component (0 - 255, 0=darkest, 255=lightest) */
	int  green/*0*/;                  /**< Pin color, green component (0 - 255, 0=darkest, 255=lightest) */
	int  blue/*0*/;                   /**< Pin color, blue component (0 - 255, 0=darkest, 255=lightest) */
	int  x_origin/*0*/;               /**< Pixel position for upper-left corner */
	int  y_origin/*0*/;               /**< y pixel position for upper-left corner */
	int  width/*0*/;                  /**< Width in pixels of video (0 - default to actual display screen width) */
	int  height/*0*/;                 /**< Height in pixels of video (0 - default to actual display screen height) */
};

/** Obtain the version of the sigcap service
 *
 * @return 
 * Version struct defined in svcmgrSvcDef.h
 */
/*SVC_PROTOTYPE*/ struct version sigcap_getVersion(void);


/** Set palm rejection
 *
 * @param[in] state @li  0 - to turn off palm rejection @li non-zero  - to turn on
 *
 * @return 0
 */
/*SVC_PROTOTYPE*/ int sigcap_setPalmRejection(int state);



/** Get palm rejection indicator
 *
 * @return 
 * @li 0 - palm rejection off
 * @li >= 1 - if palm rejection on
 */
/*SVC_PROTOTYPE*/ int sigcap_getPalmRejection(void);




/** Set stylus only mode (disables finger)
 *
 * @param[in] state @li 0 - to turn off stylus only @li non-zero - to turn on
 *
 * @return 0
 */
/*SVC_PROTOTYPE*/ int sigcap_setStylusOnlyMode(int state);


/** Get stylus only mode
 *
 * @return 
 * @li 0 - stylus only mode off
 * @li >= 1 - if stylus only mode on
 */
/*SVC_PROTOTYPE*/ int sigcap_getStylusOnlyMode(void);


/** Get stylus presence indicator
 *
 * @return 
 * @li 1 - no stylus
 * @li 0 - if stylus is present
 */
/*SVC_PROTOTYPE*/ int sigcap_getStylus(void);



/** Enable Realtime Data transfer of signature capture to TCP/IP port
 *
 * @param[in] ip  IP address (xxx.xxx.xxx.xxx) to make connection to (where to send realtime signature data)
 * @param[in] port  TCP/IP port where connection is to be made
 *
 * @return 
 * @li 0 Success
 * @li -1 Error
 *
 * <b>Errno values</b>:
 *  @li EINVAL - Invalid argument(s) passed.
 *
 * @note
 * @li If actively capturing signature, this request will take affect starting with the next signature capture.
 * @li The remote socket connection will "open" when sigcap_beginRegionDraw() is called and close at sicap_end().
 *
 * \n  <b>Following is the data format:</b>
 * @li All values are short integer represented
 * @li At start of a signature capture, the following is sent: -1-1 Xorigin Yorigin width height
 *
 * Where:
 *
 * 		 -1-1 - 0xFFFF used to indicate start/header for signature
 * 		 Xorigin - X coordinate for top-left corner of signing region (short integer)
 * 		 Yorigin - Y coordinate for top-left corner of signing region (short integer)
 * 		 width - width in pixels for signing region (short integer)
 * 		 height - height in pixels for signing region (short integer)
 *
 * @li Signature data follows using format: X Y X Y X Y X Y ... X Y X Y -1-1
 *
 * Where:
 *
 *  	 X - x coordinate of point in signature (short integer)
 *  	 Y - y coordinate of point in signature (short integer)
 *  	 -1-1 - 0xFFFF used to indicate end of signature
 *
 */
/*SVC_PROTOTYPE*/ int sigcap_enableRealTimeData(char ip[16], int port);



/** Disable Realtime Data transfer
 *
 * @return 0 (there is no failure case).
 */
/*SVC_PROTOTYPE*/ int sigcap_disableRealTimeData(void);



/** Begin capturing signature data - no display output
 *
 * @param[in] points  Amount of points (300 dps) to be used for image capture (0 - to use maxSeconds for calculation)
 * @param[in] maxSeconds  Maximum number of seconds to capture signature data (0 - to use points for calculation)
 * @param[in] stylusOnlyRequested If stylus only will be used to capture signature data (none-zero)
 *
 * @return 
 * @li 0 Success
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li ENOMEM - Unable to malloc space for data.
 * @li EINVAL - Invalid argument(s) passed.
 * 
 * @note
 * If both points and maxSeconds are zero, a default of 60 seconds is used (with 300 dps), max is 180 seconds.
 *
 * For stylus Only, Palm Rejection must be disabled, and stylus attached.
 *
 */
/*SVC_PROTOTYPE*/ int sigcap_begin(unsigned long int points, int maxSeconds, int stylusOnlyRequested);



/** Begin capturing signature data within Region - no display output
 *
 * @param[in] points  Amount of points (300 dps) to be used for image capture (0 - to use maxSeconds for calculation)
 * @param[in] maxSeconds  (0 - 180) maximum number of seconds to capture signature data (0 - to use points for calculation)
 * @param[in] stylusOnlyRequested  If stylus only will be used to capture signature data (none-zero)
 *
 * @param[in] x  Upperleft x pixel location for capture "box"
 * @param[in] y  Upperleft y pixel location for capture "box"
 * @param[in] w  Width in pixels for capture "box"
 * @param[in] h  Height in pixels for capture "box"
 *
 * @return
 * @li 0 Success
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li ENOMEM - Unable to malloc space for data.
 * @li EINVAL - Invalid argument(s) passed.
 *
 * @note
 * If both points and maxSeconds are zero, a default of 60 seconds is used (with 300 dps).
 *
 * For stylus Only, Palm Rejection must be disabled, and stylus attached.
 *
 */
/*SVC_PROTOTYPE*/ int sigcap_beginRegion(unsigned long int points, int maxSeconds, int stylusOnlyRequested, int x, int y, int w, int h);



/** Read event from a sigcap_beginRegionDraw operation
 * 
 * @param[in] eventkey  Event key used to obtain region draw status (obtained from sigcap_beginRegionDraw() returned struct sigcapBeginRegionDrawReturn)
 * @param[in] flags  Get Event Flags:
 *                      @li 0 = Blocking
 *                      @li 1 = Non-block
 *                      @li 2 = Last 
 *                      @li 4 = Cancel
 *                      @li 8 = Purge previous events (options can be or'd)
 * @return 
 * 1. Status >= 0 indicates which timeout expired or if operation cancelled.
 *  
 * 2. Status >= 0:
 * @li 1 - canceled or commanded to end
 * @li 2 - max timeout exceeded
 * @li 3 - max timeout until first pen/finger down exceeded
 * @li 4 - max timeout from a pen/finger up until next pen down exceeded
 * @li 5 - indicates user just did a pen/finger down within region (only occurs on first pen/finger down)
 * @li 21 - realtime signature date xfer wanted, but failed to connect to host
 * @li 22 - failed to send some of signature data to host (realtime data)
 *
 * <b>Errno values</b>:
 * @li EFAULT - Invalid eventkey, or unable to cancel previous sigcap_getDrawEvent() in progress.
 * @li EAGAIN - No last event (when flags requesting last).
 * @li ENODATA - No event data available (when flags set to non-block and no event).
 * @li EBUSY - event cancel already in progress, and attempting to cancel event (flags == 4).
 * @li ENODEV - Internal device error.
 * @li EINTR  - getEvent canceled (by another thread/user).
 * @li ENOMEM - Unable to allocate needed memory to carryout request
 */
/*SVC_PROTOTYPE*/ int sigcap_getDrawEvent(char *eventkey /*REQ*/, int flags /* 0 */);



/** Close draw event
 * 
 * @param[in] eventkey  Event key to be closed (obtained from sigcap_beginRegionDraw() returned struct sigcapBeginRegionDrawReturn)
 *
 * @return 
 * @li If >= 0 Success
 * @li If = -1 Error
 *
 * <b>Errno values</b>:
 * @li EINVAL - Invalid eventkey passed.
 * @li EFAULT - Error while closing event.
 */
/*SVC_PROTOTYPE*/ int sigcap_closeDrawEvent(char *eventkey /*REQ*/);



/** Begin a sigcap with display of signature on given region, pen color, and width
 *
 * @param[in] params  Structure containing parameters (see struct sigcapBeginDisplayParams)
 *
 * @return 
 * Struct sigcapBeginRegionDrawReturn
 * @li status = 0 on success
 * @li -1 on error
 *
 * <b>Errno values</b>:
 * @li ENOMEM - Unable to malloc space for data.
 * @li EINVAL - Invalid argument(s) passed.
 *
 * @note
 * If both points and maxSeconds are zero, a default of 60 seconds is used (with 300 dps).
 *
 * For stylus Only, Palm Rejection must be disabled, and stylus attached.
 *
 * MaxSeconds timeout exceeded will end signature capture, Pen Down/Up timeouts do not.
 */
/*SVC_PROTOTYPE*/ struct sigcapBeginRegionDrawReturn sigcap_beginRegionDraw(struct sigcapBeginDisplayParams params);



/** End captured signature (if still active) and clear the display of signature
 *  This method can be used to end signature capture that was started
 *  from: sigcap_beginWithDisplay(), sigcap_begin(), or sigcap_beginRegion()
 *  If sigcap_beginRegionDraw() was used, the signature display will be set back
 *  to image before sigcap began (within the signature capture region)
 *
 * @return 0
 */
/*SVC_PROTOTYPE*/ int sigcap_clearDraw(void);


/** End capturing signature, data captured will remain available
 *  This method is used to end signature capture that was started
 *  from: sigcap_begin(), sigcap_beginRegion(), or sigcap_beginRegionDraw()
 *  If sigcap_beginRegionDraw() was used, the signature display remains visible,
 *    use sigcap_clearDraw() to remove any visible signature.
 *
 * @return 0
 */
/*SVC_PROTOTYPE*/ int sigcap_end(void);


/** Cancel captured signature and toss any image data captured
 *  This method is used to cancel signature capture that was started
 *  from: sigcap_begin(), sigcap_beginRegion(), or sigcap_beginRegionDraw()
 *  If sigcap_beginRegionDraw() was used, the signature display remains visible,
 *    use sigcap_clearDraw() to remove any visible signature.
 *
 * @return 0
 */
/*SVC_PROTOTYPE*/ int sigcap_cancel(void);


/** Create tiff image file from captured signature data
 *
 * @return 
 * Struct imageCreateStatus_s
 *
 * @return
 * On crateStatus.result < 0 return, errno
 *
 * <b>Errno values</b>:
 * @li ENOMEM - Unable to malloc space for data.
 * @li ENODATA - No image data available to create image from
 *
 */
/*SVC_PROTOTYPE*/ struct imageCreateStatus_s sigcap_tiffCreate(void);


/** Create png image file from captured signature data
 *
 * @return 
 * Struct imageCreateStatus_s
 *
 * @return
 * On crateStatus.result < 0 return, errno
 *
 * <b>Errno values</b>:
 * @li ENOMEM - Unable to malloc space for data.
 * @li ENODATA - No image data available to create image from
 *
 */
/*SVC_PROTOTYPE*/ struct imageCreateStatus_s sigcap_pngCreate(void);


/** Create raw touch point file from captured signature data
 *
 *  The file is binary and contains a series of 4-byte (32 bit) little-endian values.
 *  Each 32 bit value represents one touch point. The 8 most significant bits are z,
 *   the next 12 are y and the least significant 12 bits are x.
 *  The x and y values are with reference to the top left of the touch panel (0,0).
 *  Range for x is 0 to 2047, for y is 0 to 1535 and for z is 0 to 63.
 *  A pen/finger UP is represented by x and y being -1.
 *
 *  The raw data file contains ALL touch points that occur from the time sigcap begins
 *  (eg. sigcap_begin()) until sigcap is ended (via call or timeout).
 *
 *  There is always a pen/finger UP at the start of the file and at the end (unless
 *   a timeout occurred before the user lifted the pen/finger).
 *
 *   This is an example struct that could be used for raw data parsing:
 *   \code
 * <code><pre>
 *     typedef struct
 *     {
 *         long x : 12;
 *         long y : 12;
 *         long z : 8;
 *      } __attribute__((packed)) xyz_t;
 *
 *      #define IS_PENUP(p) ((p).x < 0 && (p).y < 0)
 *   </pre></code>
 *\endcode
 *
 * @return 
 * Struct imageCreateStatus_s
 *
 * @return 
 * On crateStatus.result < 0 return, errno
 *
 * <b>Errno values</b>:
 * @li ENOMEM - Unable to malloc space for data.
 * @li ENODATA - No image data available to create file
 *
 */
/*SVC_PROTOTYPE*/ struct imageCreateStatus_s sigcap_rawCreate(void);


/** re-compensate the touch panel
 *
 * @return 0
 */
/*SVC_PROTOTYPE*/ int sigcap_recompTouchPanel(void);


/** Obtain the touch controller chip firmware information
 *
 * @return 
 * K2 chip firmware info as 0x00IIVVSS where II is chip ID, VV is firmware version, SS is firmware sub-version
 *
 */
/*SVC_PROTOTYPE*/ int sigcap_controllerFW(void);

#ifdef __cplusplus
}
#endif
#endif //SVC_SIGCAP_H
/// @}*/
/// @}*/