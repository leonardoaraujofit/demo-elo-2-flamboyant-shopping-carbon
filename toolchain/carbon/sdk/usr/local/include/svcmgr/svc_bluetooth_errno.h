#ifndef SVC_BLUETOOTH_ERRNO_H
#define SVC_BLUETOOTH_ERRNO_H

#ifdef __cplusplus
extern "C" {
#endif


#define EALPAIR  10000  	/* Already paired */
#define EUNPAIR  10001  	/* Device not paired */
#define EDACON   10002 		/* Device is already connected */
#define EPANCUN  10003 		/* PAN connection unsuccessful */
#define EPANCNK  10004 		/* PAN connection not killed */
#define EBNEP0NE 10005 		/* bnep0 interface does not exist */
#define EDUNTIMEOUT 10006   /* Dun timeout */
#define ESPPCON    10007 	/* Error in SPP connection */
#define ESPPDISCON 10008 	/* Error in disconnecting SPP */
#define EDUNRFCOMM 10009 	/* Specified rfcomm interface already exists */
#define ETIMEOUT   10010 	/* Timeout error */
#define ENCON      10011 	/* Not connected */
#define ETTYFAILURE 10012	/* Failure in creating /dev/ttyModem  */
#define ETTYUNLINK 10013    /* Error in unlinking /dev/ttyModem */
#define EPAN 	   10014 	/* An error occured in PAN request */
#define EINPUT     10015	/* Invalid input */
#define EFAI       10017    /* Failure */


#ifdef __cplusplus
}
#endif

#endif //SVC_BLUETOOTH_ERRNO_H
