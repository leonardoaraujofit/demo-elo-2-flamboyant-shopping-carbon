/** @addtogroup ui UI */
/** @{ */
/** @addtogroup keyboard_service UI - Keyboard: Services */
/** @{ */
/** 
 *  @file svc_kbd.h 
 *
 *  @brief  Keyboard service
 *
 *	This header file contains information about the Keyboard service.  
 *
 */

   /*
  * VeriFone, Inc.
  */



#ifndef SVC_KBD_H
#define SVC_KBD_H

#ifdef __cplusplus
extern "C" {
#endif

/*SVC_SERVICE:kbd*/
#include "svcmgrSvcDef.h"  /*This header file is defined and maintained by VeriFone to provide necessary
                              infrastructure components. The file must be #included in the Service Interface
                              header file as it contains the definition for struct version, which is returned by
                              the {service}_getVersion( ) function required by every Service.
                              svcmgrSvcDef.h also contains structures to support Pairlists.*/


/** Obtain the version of the kbd service
 *
 * @return 
 * Version struct defined in svcmgrSvcDef.h
 */
/*SVC_PROTOTYPE*/ struct version kbd_getVersion(void);


/** Tell the kbd service to start listening for keypad events. Supports numeric and alpha-numeric entry.
 *
 * 	@param[in] timeout  Number of seconds to listen for keypad events
 * 	@param[in] language  Language in xx[-yy] format - xx = language yy = country code - country code is optional e.g. "en-us"
 * 	@param[in] maxlen  Max number of keypad events to listen for
 * 	@param[in] type   "numeric" or "alpha"
 * 	@param[in] interCharTimeout   Only used when type="alpha". number of ms between character rollover.
 * 
 * 	@return 
 *  @li 0 on success
 *
 */
/*SVC_PROTOTYPE*/int kbd_open(int timeout, char * language, int maxlen, char* type, int interCharTimeout);

/*SVC_STRUCT*/
struct KbdEvent
{
	int keycode;
	int err;
	char keyName[4+1];
};

/*SVC_STRUCT*/
struct saveData
{
	char * prompt;
	char * data;
	char * softKey;
	int errorCode;
};


/** gGt key press via events
 *
 * This method uses events and allows caller to get the last key press,
 *  get key with wait, get key w/o wait, or cancel key wait (used to unblock
 *  any other process waiting on key press). This method also makes sure
 *  caller is authenticated to obtain key. This should only be used AFTER kbd_open()
 *
 * @param[in] flags  Get Event Flags:
 *   @li 0 = Block
 *   @li 1 = Non-block 
 *   @li 2 = Last 
 *   @li 4 = Cancel (options can be or'd)
 *
 * @return 
 * KbdEvent:\n
 * 	  check errno == 0 and KbdEvent.err == 0, no errors, then KbdEvent.keycode is equal to the linux keycode and keyName is a string holding its descriptive name.
 *   
 * <b>Errno values</b>:
 * @li ETIME   Timeout occured waiting for keypress/release
 * @li EINVAL  Invalid flags values
 * @li EACCES  When access is denied
 * @li EINTR   Get key event canceled
 * @li ENOENT  When no key data is available
 */
/*SVC_PROTOTYPE*/struct KbdEvent kbd_getKeyEvent(int flags);


/** Stop the action that was initiated in kbd_open
 *
 * @return 
 * @li 0 on success
 *	
 * <b>Errno values</b>:
 * @li EAGAIN - unable to stop the process, try again
 */
/*SVC_PROTOTYPE*/int kbd_close();


/** Close the keyboard and save some data string along side a prompt. Use kbd_restore to get this data back out.
 *
 * @param[in] prompt String containing the prompt that was displayed when the data was retrieved
 * @param[in] data  String containing data that was retrieved
 * @param[in] softKey String containing the name of the softKey that was used to terminate data entry
 * @param[in] errorCode  A code to indicate what type of error occurred, if any, during data entry
 *
 * @return 
 * @li 0 on success
 *
 *
 */
/*SVC_PROTOTYPE*/int kbd_closeAndSave(char * prompt, char * data, char * softKey, int errorCode);


/** Restore the data saved in kbd_closeAndSave()
 *
 *
 * @return 
 * SaveData structure contains prompt, data, and softKey passed into kbd_closeAndSave()
 *
 * If data begins with "##" this means the data has been encrypted and stored in a file of the same name as the remainder of the data string.
 *
 * This fille will reside in /var/tmp/kbd/sdr/
 * 
 */
/*SVC_PROTOTYPE*/struct saveData kbd_restore();


/** Used to open keyboard device and enable capturing of key presses
 *
 *    See: kbd_getKeyEventKbd(), kbd_closeKbd()
 *
 * @return 
 * Struct openKbdReturn - contains reference to key device and event for key presses
 *
 * @param[in] innerKeyTimeout Maximum timeout in milliseconds to wait for a key press (0 = infinite). This
 *                              affects when/what kbd_getEventKbd() returns. Must be >= 0.
 *                              Between digits, timeout is between key release and the next key press. No
 *                              timeout occurs while a key is held/pressed.
 * @param[in] getReleaseEvents  Non-zero to be notified when a key is released (see kbd_getKeyEventKbd()),
 *                               else only notified when a key is pressed (getReleaseEvent = 0).
 *
 * @return 
 * @li If = 0 then Success
 * @li  If = -1 then error
 *
 * <b>Errno values</b>:
 * @li EINVAL - invalid parameters
 * @li EBUSY  - when the keyboard device is already open.
 * @li EACCES - access denied, unable to open keyboard device.
 */
/*SVC_PROTOTYPE*/ int kbd_openKbd(int innerKeyTimeout, int getReleaseEvents);


/*SVC_STRUCT*/
/**
 * Structure to store keycode event
 */
struct keyValueData {
  char keyValue[4];	   /**< KeyValue string null terminated */
};

/** Get key press via events
 *
 *    See: kbd_openKbd(), kbd_closeKbd()
 *
 * This method uses events and allows caller to get the last key press,
 *  get key with wait, get key w/o wait, or cancel key wait (used to unblock
 *  any other process waiting on key press). This method also must make sure
 *  caller is authenticated to obtain key.
 *
 * @param[in] flags  Get Event Flags:
 * @li - 0 = block
 * @li 1 = non-block
 * @li 2 = Last 
 * @li 4 = cancel (options can be or'd)
 * 
 * @return 
 * keyValueData:\n
 * 	  check errno == 0, no errors, and valid keyValue returned. \n
 * 
 * Note: keyValue = "" when errno != 0\n
 *    keyValue > 0 keyValue of key being pressed/activated.\n
 *    keyValue = empty string, indicates key released, this allows caller to implement repeat key if desired.\n
 *    values for keyValue are: 
 * @li "0" - "9"
 * @li "ESC" (escape key)
 * @li "BS" (backspace key)
 * @li "CR" (carriage return)
 * @li "REL" (key released/up).
 *    
 * <b>Errno values</b>:
 * @li ETIME - timeout occured waiting for keypress/release
 * @li EINVAL - invalid flags values
 * @li EACCES - when access is denied
 * @li EINTR  - get key event canceled
 * @li ENOENT  - when no key data is available
 */
/*SVC_PROTOTYPE*/ struct keyValueData kbd_getKeyEventKbd(int flags /* 0 */);


/** Used to disable capturing of key presses.
 *
 *    See: kbd_openKbd(), kbd_getKeyEventKbd()
 *
 * This method closes key board and event. Any pending key press events will be released.
 *
 * @return 
 * @li If >= 0 then Success
 * @li If = -1 then error
 * 
 * <b>Errno values</b>:
 *   EBADF - invalid handle
 */
/*SVC_PROTOTYPE*/ int kbd_closeKbd(void);


#ifdef __cplusplus
}
#endif

#endif //SVC_KBD_H
///@}
///@}
