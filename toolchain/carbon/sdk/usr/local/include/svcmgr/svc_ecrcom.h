
/** 
 *  @file svc_ecrcom.h 
 *
 *  @brief ECR communications service
 *
 *	This header file contains information about ECR (Electronic cash register) communications service.  
 *
 *	
 */


   /*
  * VeriFone, Inc.
  */

#ifndef SVC_ECRCOM_H
#define SVC_ECRCOM_H

#include "svcmgrSvcDef.h" /*This header file is defined and maintained by VeriFone to provide necessary  infrastructure components. The file must be #include�d in the Service Interface
                            header file as it contains the definition for struct version, which is returned by the {service}_getVersion( ) function required by every Service. svcmgrSvcDef.h 
                            also contains structures to support Pairlists.*/

#ifdef __cplusplus
extern "C" {
#endif

/*SVC_SERVICE:ecrcom*/

 
#define ECRCOM_NO_STATUS                -2020   /**< No status message available */
#define ECRCOM_NO_STATUS_NOT_ACTIVE     -2040   /**< No status message and port not active */
#define ECRCOM_MSG_AVAILABLE            9999    /**< Status message available - Check msg pointer */
#define ECRCOM_DONE                     10000   /**< No more messages - Event handler closed */


/** Returned Status Structure
*/
/*SVC_STRUCT*/
struct ecrcomStatusReturn {
    int status;             /**< Return state value */
    char *msg;              /**< Pointer to return message */
};


/** Obtain the version of the ECR service 
 * 
 * @return 
 * Version struct defined in svcmgrSvcDef.h
 */
/*SVC_PROTOTYPE*/ 
struct version ecrcom_getVersion(void);

 
 
/** Start a ECR download
 *
 * ECR configuration parameters must be set in config.system
 *
 * @return 
 * @li 0  Success, returned value is communication port handle
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li ENODEV - When the device is not valid
 */
/*SVC_PROTOTYPE*/ 
int ecrcom_download(void);



/** Cancel ECR download
 * 
 * @param[in] handle  Handle to port performing ECR download
 *
 * @return 
 * @li 0 Success, returned value is communication port handle
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li EBUSY - When device is busy - Cancel not possible at this time, try again
 * @li ESRCH - When there is no download process to cancel
 */
/*SVC_PROTOTYPE*/ 
int ecrcom_downloadAbort(int handle /*REQ*/);

 
 
/** Read event from a ECR Port operation
 *
 * @param[in] handle Handle to ECR port
 * @param[in] flags  Get Event Flags:
 *      @li -1 = Non-block
 *      @li 2 = Last 
 *      @li 4 = Cancel (options can be or'd)
 *
 * @return 
 * Struct statusReturn - state may reflect current download state, Depending on state, msg may have an ASCII status string send from the download server.
 * 
 * This state may have funny values. \n
 *
 * @note
 * IMPORTANT! Application must free ecrStatusReturn msg.
 */
/*SVC_PROTOTYPE*/ 
struct ecrcomStatusReturn ecrcom_getEvent(int flags, int handle /*REQ*/);



/** Obtain the F/W version for the COM3 controller 
 *
 * @return 
 * Null terminated version string.
  *
 * @note
 * Returned version string must be freed!
 */
/*SVC_PROTOTYPE*/ 
char *ecrcom_controllerFW(void);




#ifdef __cplusplus
}
#endif

#endif //SVC_ECRCOM_H

