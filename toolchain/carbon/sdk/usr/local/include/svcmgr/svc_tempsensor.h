/** @addtogroup utils Utils */
/** @{ */
/** @addtogroup tempsensorseervice  Utils - TempSensor: Services */
/** @{ */
/** 
 *  @file svc_tempsensor.h 
 *
 *  @brief   System Information service interface methods
 *
 *	 The System Information (tempsensor) service contains methods
 *  for obtaining system information for such things as: software versions,
 *  hardware features/options, etc.
 *
 */

/*
 *      Copyright, 2011 VeriFone Inc.
 *      2099 Gateway Place, Suite 600
 *      San Jose, CA.  95110
 *
 *  All Rights Reserved. No part of this software may be reproduced,
 *  transmitted, transcribed, stored in a retrieval system, or
 *  translated into any language or computer language, in any form
 *  or by any means electronic, mechanical, magnetic, optical,
 *  chemical, manual or otherwise, without the prior permission of
 *  VeriFone Inc.
 */



#ifndef SVC_SERVICE_TEMPSENSOR_H
#define SVC_SERVICE_TEMPSENSOR_H

#include "svcmgrSvcDef.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @name Defines for high temp shutdown
*/
/** \{ */
#define HTS_TEMP_DANGER          90 
#define HTS_TEMP_WARN            86 
#define HTS_TEMP_NORMAL          75 
#define HTS_EVENT_DANGER          2 
#define HTS_EVENT_WARN            1 
#define HTS_EVENT_NORMAL          0 
#define HTS_EVENT_KEY    "tempService"
/** \} */

#define LOGTEMPERATURE_SIZE      (4 * 7)  // storage for 4 temperature reading per day, for 7 days..
#define TEMPERATURE_INITVAL      0x7F     // temperature log will be initialized with this value..


/*SVC_SERVICE:tempsensor*/

/*SVC_STRUCT*/
/**
 * Structure returned when event fires
 */
struct tempEventStatus {
  unsigned char  eventType;     /** Identifies type of temperature event */
  int temp;                      /** Temperature read  */
};

/*SVC_STRUCT*/
/**
 * Temperature log Structure
 */
struct TemperatureLog
{
   unsigned char  sec;
   unsigned char  min;
   unsigned char  hr;
   unsigned char  day;
   unsigned char  date;
   unsigned char  mth;
   unsigned char  year;
   unsigned char  temperature;

};

/*SVC_STRUCT*/
/**
 * Ring buffer structure for temperature logging
 */
typedef struct
{
   struct TemperatureLog LogTemp[LOGTEMPERATURE_SIZE];
   unsigned char wr;
   unsigned char rd;
}LogTempBufferType;


/** Obtain service version
 *
 * @return 
 * Struct version containing version.
 *
 * @note For XML interface, see xml:\ref tempsensor_getVersion
 */
/*SVC_PROTOTYPE*/ struct version tempsensor_getVersion(void);



/** Initialise temperature sensor
 *
 *  This function is provided for backward compatability with apps
 *
 *  For new apps, it's not neccessary to call the init function
 */
/*SVC_PROTOTYPE*/ void tempsensor_init(void);


/** Set Mode
 *
 *  @li 0 - Automatic i.e. periodically reading temperature and setting fan on or off and fan speed. 
 *          When temperature is 40 degrees and above, fan is on. The higher the temperature, the faster the fan speed. When temperature is 35 degrees and below, fan is off.
 *  @li 1 - Force Fan On
 *  @li 2 - Force Fan Off
 *
 * @return 
 * 0 if success

 */
/*SVC_PROTOTYPE*/ int tempsensor_setMode(int mode);

/** Query Mode
 *
 * @return  
 *  @li 0 - Automatic i.e. periodically reading temperature and setting fan on or off and fan speed
 *  @li 1 - Force Fan pn
 *  @li 2 - Force Fan off
 *  @li 3 - Set Fan Speed Manual Mode
 */
/*SVC_PROTOTYPE*/ int tempsensor_queryMode(void);


/** Obtain current temperature reading in degrees celsius
 *
 * @return 
 * Temperature reading
 */
/*SVC_PROTOTYPE*/ int tempsensor_read(void);



/** Obtain current fan status
 *
 * @return 
 * Fan status.\n
 * @li 1 - Fan is on
 * @li 0 - Fan is off
 */
/*SVC_PROTOTYPE*/ int tempsensor_readFanStatus(void);


/** Set Fan Speed Manually. This function is only valid for DVT board onwards.
 *
 *  The input parameter is the PWM duty cycle. Valid input values are from 90 to 100 inclusive.\n
 *  Calling this function will disable the automatic mode of periodically checking temperature and setting fan speed based on temperature.\n
 *  To revert back to the automatic mode, call tempsensor_setMode.\n
 */
/*SVC_PROTOTYPE*/ void tempsensor_setFanSpeedManualMode(int speed);


/** Read the current fan speed. This function is only valid for DVT board onwards.
 * 
 * @return 
 * Current fan speed in duty cycle.
 */
/*SVC_PROTOTYPE*/ int tempsensor_readFanSpeed(void);


/** Set USB Hub Power On/Off
 *
 * @param [in] shut (1 - shut, 0 - resume)
 *
 *  Check errno for success status
 */
/*SVC_PROTOTYPE*/ void tempsensor_setHtsShutUSBHubPower(unsigned char shut);


/** Set Media board On/Off
 * 
 * @param[in] shut (1 - shut, 0 - resume)
 *
 *  Check errno for success status
 */
/*SVC_PROTOTYPE*/ void tempsensor_setHtsShutMedia(unsigned char shut);


/** UX410 Read Temperature log
 * Read 1 record from current rd pointer.
 * If it is empty, then return record with initialized value ie, timestamp as 0,
 * temperature as TEMPERATURE_INITVAL
 * 
 * @note IMPORTANT
 * @li Application must free TemperatureLog structure!
 */
/*SVC_PROTOTYPE*/ struct TemperatureLog * tempsensor_LogTemp_Read(void);



#ifdef __cplusplus
}
#endif

#endif //SVC_SERVICE_TEMPSENSOR_H

/// @}*/
/// @}*/
