/** @addtogroup media Media */
/** @{ */
/** @addtogroup soundservice  Media - Sound: Services */
/** @{ */
/** 
 *  @file svc_sound.h 
 *
 *  @brief  Sound service
 *
 *	This header file contains information about the Sound service.  
 *
 */


/*
 *	Copyright, 2012 VeriFone Inc.
 *	2099 Gateway Place, Suite 600
 *	San Jose, CA.  95110
 *
 *  All Rights Reserved. No part of this software may be reproduced,
 *  transmitted, transcribed, stored in a retrieval system, or
 *  translated into any language or computer language, in any form
 *  or by any means electronic, mechanical, magnetic, optical,
 *  chemical, manual or otherwise, without the prior permission of
 *  VeriFone Inc.
 */
 
 
#ifndef SVC_SOUND_H
#define SVC_SOUND_H

#include "svcmgrSvcDef.h"

#ifdef __cplusplus
extern "C" {
#endif

/*SVC_SERVICE:sound*/


#define SOUND_SPEAKER_STATE_ON   1     /**< Set speaker to ON */
#define SOUND_SPEAKER_STATE_OFF  0    /**< Set speaker to OFF */


/**
 * @name Defines for sound status (see struct soundStatusReturn and sound_getPlayEvent())
*/
/** \{ */
#define SOUND_STATUS_END              1 /**< Audio ended, if repeat set, indicates repeating sound */
#define SOUND_STATUS_PAUSE            2 /**< Audio has been paused */
#define SOUND_STATUS_RESUME           3 /**< Audio has been resumed */
#define SOUND_STATUS_TERMINATED       4 /**< Audio terminated by sound_stop(), sound_stopall(), or another sound_play() */
#define SOUND_STATUS_ERROR_IO        10 /**< Invalid audio file format/type or access error */
#define SOUND_STATUS_ERROR_FILE_FMT  11 /**< Invalid file format */
/** \} */


/**
 * @name Defines for Ux200 sound source (see struct soundMixStruct and sound_mix)
*  
* These were renumbered to be the same as codec stream selector values 
*/
/** \{ */
#define SOUND_SOURCE_MEDIA_PROCESSOR		1	/**< Ux200 Media processor board audio */
#define SOUND_SOURCE_MICROPHONE			2	    /**< Ux200 Microphone */
#define SOUND_SOURCE_DIGITAL			3	    /**< Ux200 Trident playing e.g. a .wav file */
#define SOUND_SOURCE_FUNCTION_GENERATOR	8	    /**< Codec's built in function generator */
/** \} */


/**
 * @name Defines for Ux200 loudness adaptor (LA) aka ambient noise adjustment control (see soundLAStruct and sound_la) -- Note: intended for use by opsys rather than applications
*   
*/
/** \{ */
#define SOUND_LA_ENABLE				0	    /**< 0 disables, 1 enables LA */
#define SOUND_LA_SET_DEFAULTS			1	/**< Reset all LA params below to their default boot-time values */
#define SOUND_LA_SELECT_SOURCE			2	/**< SOUND_SOURCE_MICROPHONE for normal use, SOUND_SOURCE_FUNCTION_GENERATOR can be used for testing */

#define SOUND_LA_REF_LV_MIN			10	    /**<  Noise below this level is ignored: 1dB steps 0...40 = -80...-40dB (default 30 = -50dB)  */
#define SOUND_LA_REF_LV_MAX			11	    /**<  Noise above this level is ignored: 1dB steps 0...40 = -60...-20dB (default 15 = -44dB)  */
#define SOUND_LA_PWR_MARGIN			12	    /**<  Sets hysteresis between LA active and inactive: 1/4dB steps 0-60 = +0...+15dB (default 12)  */
#define SOUND_LA_PWR_DYN_RANGE		13	    /**<  Max gain for large increase in noise level: 1/2dB steps 0-30 = +3...+18dB (default 18)  */
#define SOUND_LA_STEP_UP			14	    /**<  Quantum for output gain increase: 1/256dB steps 1-128 = 1/256...1/2dB (default 128 = +1/2dB)  */
#define SOUND_LA_STEP_DOWN			15	    /**<  Quantum for output gain decrease: 1/256dB steps 1-128 = -1/2dB...-1/256dB (default 1 = -1/2dB)  */
#define SOUND_LA_UPDATE_RATE			16	/**<  Output gain update period: 4ms blocks 0...127 = 4ms...512ms (default 8 = 32ms)  */
#define SOUND_LA_UPDATE_RATE_SCALE		17	/**<  Fraction for averaging noise over update period: 0...126 = 1/128...1/1 4ms blocks (default 16)  */
							/* Note: Must divide exactly into 32768 */
#define SOUND_LA_REF_STABLE			    18	/**<  Delay before measuring noise: 1...8192 update periods (default 125)  */
#define SOUND_LA_STABILITY_COUNTS		19	/**<  Responsiveness to noise changes: 1...4096 update periods (default 50 = 1.6sec)  */
/** \} */

/**
 * @name Defines for Ux200 function generator (FG) control (see soundFGStruct and sound_fg) -- Note: intended for use by opsys rather than applications
*   
*/
/** \{ */
#define SOUND_FG_ENABLE				0	    /**<0 disables, 1 enables FG  */
#define SOUND_FG_SET_DEFAULTS		1	    /**< Resets all FG params below to their default boot-time values  */
#define SOUND_FG_RATE				2	    /**< Chooses clocking rate of FG: 0 = 16KHz, (1 = 32HKz), 2 = 48KHz  */
#define SOUND_FG_NORMAL				3	    /**< Configures FG to clock at 48KHz and generate an approx. 1KHz sine wave at volume specified by value  */
#define SOUND_FG_LA_TEST			4	    /**< Configures FG to clock at 16KHz and generate white noise at volume specified by value parameter  */
#define SOUND_FG_A0_LEFT			10	    /**< Controls volume or volume and shape of waveform depending on waveform selected  */
#define SOUND_FG_A1_LEFT			11	    /**< Depends on waveform selected  */
#define SOUND_FG_A2_LEFT			12	    /**< Ditto */
#define SOUND_FG_A3_LEFT			13	    /**< Ditto */
#define SOUND_FG_A4_LEFT			14	    /**< Ditto */
#define SOUND_FG_A5_LEFT			15	    /**< Ditto */
#define SOUND_FG_CONTROL_LEFT		16	    /**< Selects waveform: @li 0 none @li 1 DC @li 2 sawtoioth @li 3 square wave @li 4 dual tone sine @li 5 DTMF @li 6 white noise @li 7-14 none @li 15 reserved  */
#define SOUND_FG_DTMF_FREQ_LEFT		17	    /**< Selects frequency pair for DTMF waveforms  */
#define SOUND_FG_TIME_LEFT			18	    /**< Waveform duration: ms 1-255; 0 = infinite   */
#define SOUND_FG_A0_RIGHT			20	    /**< Controls volume or volume and shape of waveform depending on waveform selected  */
#define SOUND_FG_A1_RIGHT			21	    /**< Depends on waveform selected  */
#define SOUND_FG_A2_RIGHT			22	    /**< Ditto */
#define SOUND_FG_A3_RIGHT			23	    /**< Ditto */
#define SOUND_FG_A4_RIGHT			24	    /**< Ditto */
#define SOUND_FG_A5_RIGHT			25	    /**< Ditto */
#define SOUND_FG_CONTROL_RIGHT		26	    /**< Selects waveform: @li 0 - none @li 1 - DC @li 2 - sawtoioth @li 3 - square wave @li 4 - dual tone sine @li 5 - DTMF @li 6 - white noise @li 7-14 - none @li 15 - copy left channel */
#define SOUND_FG_DTMF_FREQ_RIGHT	27	    /**< Selects frequency pair for DTMF waveforms */
#define SOUND_FG_TIME_RIGHT			28	    /**< Waveform duration: ms 1-255; 0 = infinite */
/** \} */



/*SVC_STRUCT*/
struct soundDeviceSupport {
	int audio;		/**< non-zero if codec present with speaker and/or headphone */
	int buzzer;		/**< non-zero if buzzer device present */
	int headphone;	/**< non-zero if headphone jack present (this may not be known for all platforms) */
	int extSpkr;	/**< non-zero if external speaker jack present (this may not be known for all platforms) */
};


/**
 * Returned Status Structure
 */
/*SVC_STRUCT*/ 
struct soundStatusReturn {
	int status;		/**< Return state value */
	char msg[80];	/**< Return message (may be empty) */
};


/**
 * Returned Status Structure
 */
/*SVC_STRUCT*/ 
struct soundPlayReturn {
	int handle;         /**< Reference to sound object for close, pause, resume, etc */
	char eventkey[32];	/**<  Event key (if requested via eventFlag param to sound_play()), empty string otherwise */
};

/** Obtain the version of the sound service
 * 
 * @return 
 * Version struct defined in svcmgrSvcDef.h
 */
/*SVC_PROTOTYPE*/ struct version sound_getVersion(void);


/** Obtain the sound supported info
 *
 * @return
 * soundDeviceSupport struct
 */
struct soundDeviceSupport sound_getSupportInfo(void);


/** Set the internal speaker state (enabled/disabled)
 *
 * @param[in] state  State of speaker:
 * @li 0 = Disabled (mute)
 * @li != 0 Enabled
 *
 * @return 
 * @li If >= 0 Success
 * @li If = -1 Error
 * @li ENXIO - audio not supported on platform
 */
/*SVC_PROTOTYPE*/ int sound_speaker(int state /* 0 */);


/** Get the internal speaker state (enabled/disabled)
 *
 * @return
 * @li If >= 0 Success: 0 = Disabled (mute), 1 = Enabled
 * @li If = -1 Error
 * @li ENXIO - audio not supported on platform
 */
/*SVC_PROTOTYPE*/ int sound_getSpeakerState(void);


/** Set the headphone state (enabled/disabled)
 *
 * @param[in] state  State of headphone jack:
 * @li 0 = Disabled (mute)
 * @li != 0 Enabled
 *
 * @return
 * @li If >= 0 Success
 * @li If = -1 Error
 * @li ENXIO - audio not supported on platform
 */
/*SVC_PROTOTYPE*/ int sound_headphone(int state /* 0 */);


/** Get the headphone state (enabled/disabled)
 *
 * @return
 * @li If >= 0 Success: 0 = Disabled (mute), 1 = Enabled
 * @li If = -1 Error
 * @li ENXIO - audio not supported on platform
 */
/*SVC_PROTOTYPE*/ int sound_getHeadphoneState(void);


/** Get current value for the Headphones Detect state - non-blocking
 *
 * @return
 * @li  0 - headphone not connected
 * @li  1 - headphone connected
 * @li -1 - Error, unable to complete request
 *
 * On error return, errno:\n
 * <b>Errno values</b>:
 * @li EBADF - Failure when requesting headphone detect status
 * @li ENXIO - audio not supported on platform
 */
/*SVC_PROTOTYPE*/ int sound_headphoneDetect_getState(void);


/** Set/get the internal speakers volume (0 to 100)
 *
 * @param[in] volume  Volume of internal speakers
 * @li 0 = Off
 * @li 100 = Max
 * @li 50 = Typical
 * @li if < 0 Current volume returned
 *
 * @return 
 * @li If >= 0 Volume level returned
 * @li If = -1 Error
 * 
 * <b>Errno Values:</b>
 * @li EINVAL - When input volume is > 100.
 * @li ENXIO - audio not supported on platform
 */
/*SVC_PROTOTYPE*/ int sound_volume(int volume /* 50 */);


/** Set/get the headphone volume (0 to 100)
 *
 * @param[in] volume  Volume of headphone
 * @li 0 = Off
 * @li 100 = Max
 * @li 50 = Typical
 * @li if < 0 Current volume returned
 *
 * @return
 * @li If >= 0 Volume level returned
 * @li If = -1 Error
 *
 * <b>Errno Values:</b>
 * @li EINVAL - When input volume is > 100.
 * @li ENXIO - audio not supported on platform
 */
/*SVC_PROTOTYPE*/ int sound_headphoneVolume(int volume /* 50 */);


/** Read event from a play operation
 * 
 * @param[in] eventkey  Event key used to obtain sound play status (obtained from sound_play() returned struct soundPlayReturn)
 * @param[in] flags  Get Event Flags:
 * @li -1 = Non-block
 * @li 2 = Last
 * @li 4 = Cancel (options can be or'd)
 *
 * @return
 * Struct soundStatusReturn - state will reflect current play state, Depending on state, msg may have an ASCII status string.
 * 
 * @note IMPORTANT!
 * Application must free soundStatusReturn msg.
 */
/*SVC_PROTOTYPE*/ struct soundStatusReturn sound_getPlayEvent(char *eventkey /*REQ*/, int flags /* 0 */);


/** Close play event
 *
 * @param[in] eventkey  Event key to be closed (obtained from sound_play() returned struct soundPlayReturn)
 *
 * @return 
 * @li If >= 0  Success
 * @li If = -1  Error
 */
/*SVC_PROTOTYPE*/ int sound_closePlayEvent(char *eventkey /*REQ*/);


/** Play a sound file beginning from offset (based on whence)
 * 
 *  Play a audio file of format: wav or mp3
 *
 * @param[in] filename  Name of file (full pathing) to be played (must be < 512 characters and readable by sys2)
 * @param[in] offset New position, measured in seconds, is obtained by adding offset seconds to the position
 *                     specified by whence
 * @param[in] whence  Set to SEEK_SET (0), SEEK_CUR (1), or SEEK_END (2), the offset is relative to the start of the file,
 *                     the current position indicator, or end-of-file, respectively.
 * @param[in] eventflag  Nonzero value to enable event status for playback notification (see sound_getPlayEvent())
 *
 * @return
 * Struct soundPlayReturn - status whether play (initialization, etc) succeeded, and the event key if eventFlag != 0.
 * @li handle >= 0 Success
 * @li = -1 Error
 *
 * <b>Errno Values:</b>
 * @li ENOENT - When filename is unknown.
 * @li EIO - File access error or media driver error trying to play audio file
 * @li ENOEXEC - Media file not a recognized audio format
 * @li EACCES - When internal error
 * @li EINVAL - When offset is invalid for given audio file
 * @li ENXIO - audio not supported on platform
 */
/*SVC_PROTOTYPE*/ struct soundPlayReturn sound_play(char *filename /*REQ*/, double offset /* 0 */, int whence /* 0 */, int eventflag /* 0 */);


/** Play a sound file and repeat
 *
 * @param[in] filename  Name of file (full pathing) to be played (must be < 512 characters and readable by sys2)
 * @param[in] eventflag  Nonzero value to enable event status for playback notification (see sound_getPlayEvent())
 *
 * @return 
 * Struct soundPlayReturn - status whether play (initialization, etc) succeeded, and the event key if eventFlag != 0.
 * @li handle >= 0 Success
 * @li = -1 Error
 *
 * <b>Errno Values:</b>
 * @li ENOENT - When filename is unknown.
 * @li EIO - File access error or media driver error trying to play audio file
 * @li EACCES - When internal error
 * @li ENXIO - audio not supported on platform
 */
/*SVC_PROTOTYPE*/ struct soundPlayReturn sound_playRepeat(char *filename /*REQ*/, int eventflag /* 0 */);


/** Stop any playing sound
 *
 * @param[in] handle  Reference to sound object returned by sound_play()
 *
 * @return 
 * @li If >= 0 Success
 * @li If = -1 Error
 *
 * <b>Errno Values:</b>
 *  @li EINVAL - Invalid handle
 */
/*SVC_PROTOTYPE*/ int sound_stop(int handle /*REQ*/);


/** Stop all playing sound including beep/buzzer
 *
 * @return 
 * @li If >= 0  Success
 * @li If = -1 Error
 */
/*SVC_PROTOTYPE*/ int sound_stopall(void);


/** Pause any playing sound
 *
 * @param[in] handle  Reference to sound object returned by sound_play()
 * 
 * @return 
 * @li If >= 0 Success
 * @li If = -1 Error
 * 
 * <b>Errno Values:</b>
 *  @li EINVAL - Invalid handle
 */
/*SVC_PROTOTYPE*/ int sound_pause(int handle /*REQ*/);


/** Resume any playing sound
 *
 * @param[in] handle  Reference to sound object returned by sound_play()
 * 
 * @return 
 * @li If >= 0 Success
 * @li If = -1 Error
 * 
 * <b>Errno Values:</b>
 *  @li EINVAL - Invalid handle
 */
/*SVC_PROTOTYPE*/ int sound_resume(int handle /*REQ*/);


/** Repeat any playing sound
 *
 * If repeat mode is on, when a playback has reached the end of the media, it will restart the playback from the beginning.
 *
 * @param[in] handle  Reference to sound object returned by sound_play()
 * @param[in] state  Repeat state to set: @li 1 = Enable repeat @li 0 = Disable
 *
 * @return 
 * @li If >= 0  Success
 * @li If = -1 Error
 * 
 * <b>Errno Values:</b>
 *  @li EINVAL - Invalid handle
 */
/*SVC_PROTOTYPE*/ int sound_repeat(int handle /*REQ*/, int state /* 1 */);


/** Resume any playing sound
 *
 * @param[in] handle  Reference to sound object returned by sound_play()
 * @param[in] offset  New position, measured in seconds, is obtained by adding offset seconds to the position
 *                     specified by whence
 * @param[in] whence  Set to SEEK_SET (0), SEEK_CUR (1), or SEEK_END (2), the offset is relative to the start of the file,
 *                     the current position indicator, or end-of-file, respectively.
 * @return 
 * @li If >= 0  Success
 * @li If = -1 Error
 *
 *<b>Errno Values:</b>
 *  @li EINVAL - invalid handle
 */
/*SVC_PROTOTYPE*/ int sound_seek(int handle /*REQ*/, double offset /* 0 */, int whence /* 0 */);


/* NOTE:
 *  sound_pause(), sound_resume(), sound_repeat(), sound_seek() are not operational
 *  on the following special purpose sound generation methods.
 */


/** Stop a tone on the beeper device
 *    This method is used to stop a active tone on the beeper
 *
 * @return
 * @li If >= 0 Success
 * @li If = -1 Error
 *
 * <b>Errno Values:</b>
 * EINVAL - Invalid frequency, duration, or volume specified
 * @li ENXIO - beeper not supported on platform
 */
/*SVC_PROTOTYPE*/ int sound_beepStop(void);


/** Play a tone on the beeper device
 *    This method is identical to sound_buzzer() and included to
 *    help remove confusion on some application developers
 *
 * @param[in] frequency  Frequency in Hertz for tone (500 to 3000)
 * @param[in] duration  Duration of buzzer sound in msec (20 to 15000)
 * @param[in] volume  Volume of beeper 0=unchanged, (1 to 10 (loudest)) for 10 scale, or 10 to 100 for 100 scale (see sound_volume())
 *
 * Note: if volume is different from current volume as set by sound_volume(), then setting the Codec volume
 *  is time consuming (takes ~100ms to set, and 500ms to restore after beep completes). It is recommended to leave
 *  volume unchanged if possible (volume == 0 is unchanged)
 *
 * @return
 * @li If >= 0 Success
 * @li If = -1 Error
 *
 * <b>Errno Values:</b>
 * EINVAL - Invalid frequency, duration, or volume specified
 * @li ENXIO - beeper not supported on platform
 */
/*SVC_PROTOTYPE*/ int sound_beep(int frequency, int duration, int volume);


/** Play a tone on the buzzer device (no .wav file used)
 *
 * @param[in] frequency  Frequency in Hertz for tone (400 to 3000)
 * @param[in] duration  Duration of buzzer sound in msec (20 to 15000)
 * @param[in] volume  Volume of buzzer 0=unchanged, (1 to 10 (loudest)) for 10 scale, or 10 to 100 for 100 scale (see sound_volume())
 *
 * Note: if volume is different from current volume as set by sound_volume(), then setting the Codec volume
 *  is time consuming (takes ~100ms to set, and 500ms to restore after beep completes). It is recommended to leave
 *  volume unchanged if possible (volume == 0 is unchanged)
 *
 * @return 
 * @li If >= 0 Success
 * @li If = -1 Error
 * 
 * <b>Errno Values:</b>
 * EINVAL - Invalid frequency, duration, or volume specified
 * @li ENXIO - buzzer not supported on platform
 */
/*SVC_PROTOTYPE*/ int sound_buzzer(int frequency /* 1000 */, int duration /* 300 */, int volume /* 5 */);


/** Play normal beep sound
 *
 * @return 
 * @li If >= 0  Success
 * @li If = -1  Error
 *
 * <b>Errno Values:</b>
 * @li ENXIO - audio not supported on platform
 */
/*SVC_PROTOTYPE*/ int sound_normalBeep(void);


/** Play error beep sound
 *
 * @return
 * @li If >= 0  Success
 * @li If = -1  Error
 *
 * <b>Errno Values:</b>
 * @li ENXIO - audio not supported on platform
 */
/*SVC_PROTOTYPE*/ int sound_errorBeep(void);


/** Play normal beep for MSR sound
 *
 * @return 
 * @li If >= 0  Success
 * @li If = -1  Error
 *
 * <b>Errno Values:</b>
 * @li ENXIO - audio not supported on platform
 */
/*SVC_PROTOTYPE*/ int sound_normalBeepMSR(void);


/** Play error beep for MSR sound
 *
 * @return 
 * @li If >= 0  Success
 * @li If = -1  Error
 *
 * <b>Errno Values:</b>
 * @li ENXIO - audio not supported on platform
 */
/*SVC_PROTOTYPE*/ int sound_errorBeepMSR(void);


/** Enable/Disable keypad beep
 *
 * @param[in] enable, non-zero to enable, 0 to disable
 *
 * @return
 * @li If >= 0 Success
 * @li If = -1 Error
 *
 * <b>Errno Values:</b>
 * @li ENOENT - no keypad beep control device found
 * @li ENXIO - buzzer not supported on platform
 */
/*SVC_PROTOTYPE*/ int sound_setKeypadBeep(int enable);


/** Play diag sound file
 *
 * @return
 * @li If >= 0  Success
 * @li If = -1  Error
 *
 * <b>Errno Values:</b>
 * @li ENXIO - audio not supported on platform
 */
/*SVC_PROTOTYPE*/ int sound_diagFile(void);


/** Enable fastPlay support
 *
 * This enables fastPlay support and creates persistent player process.
 * Because of this, enable is used to avoid any overhead when not needed/used.
 *
 * @return
 * @li If >= 0  Success
 * @li If = -1  Error
 *
 * <b>Errno Values:</b>
 * @li ENXIO - audio not supported on platform
 */
/*SVC_PROTOTYPE*/ int sound_enableFastPlay(void);


/** Play a sound file start-to-end
 *
 *  fastPlay has the following capabilities and restrictions
 *  @li Play a audio file of format: wav or mp3
 *  @li This play method does not allow: seeking, stopping, pausing, event notification
 *  @li The purpose of fastPlay is to play the sound file with minimal overhead and delay.
 *  @li Only one sound file at a time is played using fastPlay, any new request will terminate the current and begin play of the new file
 *
 * @param[in] filename  Name of file (full pathing) to be played (must be readable by sys2)
 *
 * @return
 * Struct soundPlayReturn - status whether play (initialization, etc) succeeded, and the event key if eventFlag != 0.
 * @li handle >= 0 Success
 * @li = -1 Error
 *
 * <b>Errno Values:</b>
 * @li EINVAL - When filename not specified
 * @li ENOENT - When filename is unknown.
 * @li EIO - File access error or media driver error trying to play audio file
 * @li ENOEXEC - Media file not a recognized audio format
 * @li EACCES - When internal error
 * @li ENXIO - fastPlay hasn't been enabled (see sound_enableFastPlay())
 */
/*SVC_PROTOTYPE*/ int sound_fastPlay(char *filename);



#ifdef __cplusplus
}
#endif
#endif //SVC_SOUND_H

/// @}*/
/// @}*/
