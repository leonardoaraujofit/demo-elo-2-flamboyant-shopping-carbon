

/** 
 * @file svc_powermngt.h 
 *	
 * @brief Power Management service
 *
 */


  /*
  * VeriFone, Inc.
  */


#ifndef SVC_POWER_MGMT_H
#define SVC_POWER_MGMT_H

/* Maximum text width (128) ***************************************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

#include "svcmgrSvcDef.h"  /*This header file is defined and maintained by VeriFone to provide necessary
                            infrastructure components. The file must be #included in the Service Interface
                            header file as it contains the definition for struct version, which is returned by
                            the {service}_getVersion( ) function required by every Service.
                            svcmgrSvcDef.h also contains structures to support Pairlists.*/


/*SVC_SERVICE:powermngt*/


/** @defgroup REF_APMCONFIG_CONFIG APM Configuration
 *  Configuration parameters and functions. @n
 *  Configuration can be read by selecting several parameters and using powermngt_GetConfig() as well as one parameter can be 
 *  read with powermngt_GetConfigParam(). The same to set a configuration with powermngt_SetConfig() and
 *  powermngt_SetConfigParam(). Use functions powermngt_GetGetConfigCapabilities() and powermngt_GetSetConfigCapabilities()
 *  to retrieve which exactly parameters is available for configuration reading and writing.
 */

/** @defgroup REF_APM_MODES Available APM modes
 *  Available APM modes. @n
 *  APM power saving modes can be used both automatically (by configuration) and forced directly by appropriate function.
 *  There is a possibility to define the critical code sections where power saving modes are not available.
 */

/** @defgroup REF_APMCONFIG_PERMIT Permitted power saving mode
 *  Permitted power saving mode. @n
 *  There is a possibility for application to declare the maximum available power saving level for the system.
 */

/** @defgroup REF_WAKEUPSOURCES Wake-up sources
 *  Wake-up sources. @n
 *  There are miscellaneous devices available to wake-up the system. Some devices availability depends from terminal model.
 *  For example, Standby MCU wake-up sources are available only for Ux300 and Ux301 terminals.
 */

/** @defgroup REF_DATETIME Date and time formats
 *  Date and time macros and functions. @n
 *  APM use APM time format that specifies hours, minutes and seconds as one @a unsigned @a int value. However application can
 *  use svc_power_DateTime structure as well, and use powermngt_DateTimeToApmTime() to convert it to the APM time format.
 */

/** @defgroup REF_EVENTS Events
 *  Events. @n
 */

/** @defgroup REF_MISC Miscellaneous
 *  Miscellaneous functions. @n
 */

/** @defgroup REF_APMCONFIG_ERROR Error processing.
 *  The values returned by functions and a macros for their processing. @n
 */

/** @defgroup REF_OBSOLETE Obsolete and deprecated
 *  Obsolete and deprecated data structures, functions and macros. @n
 */


/** @ingroup REF_MISC
 *  @{ */
#define ADVANCED_POWER_MANAGEMENT_API_VERSION   2   /**< Advanced Power Management API version.
    Advanced Power Management Configuration SVC API version (will be referenced like 'APM API v...' in this document). */
#define ENABLE_PMV1_DEPRECATED_FUNCTIONS        1   /**< Enables APM API v1 functions. */
/** @} */


/******************************************************************************
 * Duplicated from apm_config.h
 * If changes are necessary, make them in both files.
 */

/** @ingroup REF_APMCONFIG_ERROR
 *  @{ */
#define APMCONFIG_ERROR_NONE                    0   /**< No error. */
#define APMCONFIG_ERROR                         -1  /**< Error. */
#define APMCONFIG_ERROR_AUTOMODE_TIME_TOO_SHORT -2  /**< Auto Mode: Sleep/wake-up time shift should be at least 3 minutes. */
#define APMCONFIG_ERROR_AUTOMODE_CONFIG_FAILURE -3  /**< Auto Mode: Configuration fails. */
#define APMCONFIG_ERROR_AUTOMODE_MODE           -4  /**< Auto Mode: Invalid Auto Mode specified. */
//
#define APMCONFIG_SUCCESS                       APMCONFIG_ERROR_NONE        /**< No error */
/** @} */


/** @ingroup REF_APMCONFIG_ERROR
 *  @{ */
#define APMCONFIG_RET_OK(x)             ((x) == APMCONFIG_SUCCESS)  /**< Checks the function returns the success value.
     Returns @a TRUE on success and @a FALSE on fail. */
#define APMCONFIG_RET_ERROR(x)          ((x) != APMCONFIG_SUCCESS)  /**< Checks the function returns the fail value.
     Returns @a TRUE on fail and @a FALSE on success. */
//
/** Accumulates an error value.
 *  If @a err value already is an error, its value will not be changed after function @a fun call.
 *  If @a err value is not error, it value will be set as function @a fun returned value. */
#define APMCONFIG_RETERR_ACCUM(err, fun)    \
    do                                                                  \
    {                                                                   \
        if (APMCONFIG_RET_ERROR(fun))                                   \
        {                                                               \
            err = (APMCONFIG_RET_ERROR(err) ? err : APMCONFIG_ERROR);   \
        }                                                               \
    } while(0)
/** @} */


/** @ingroup REF_DATETIME
 *  @{ */
/** Retrieves hours value from APM time format value. */
#define APMCONFIG_TIME_H(apm_cfg_time)          (((apm_cfg_time) >> 16) & 0xff)
/** Retrieves minutes value from APM time format value. */
#define APMCONFIG_TIME_M(apm_cfg_time)          (((apm_cfg_time) >> 8) & 0xff)
/** Retrieves seconds value from APM time format value. */
#define APMCONFIG_TIME_S(apm_cfg_time)          (((apm_cfg_time) & 0xff))
/** Converts hours, minutes and seconds to APM time format value. */
#define APMCONFIG_TIME(h, m, s)                 (((h) & 0xff) << 16 | ((m) & 0xff) << 8 | ((s) & 0xff))
/** @} */


/** @ingroup REF_APMCONFIG_PERMIT
 *  @{ */
#define APMCONFIG_PERMIT_NONE           1       /**< No one power saving mode is permitted. */
#define APMCONFIG_PERMIT_STANDBY        2       /**< The higher permitted power saving mode is Standby. */
#define APMCONFIG_PERMIT_SUSPEND        3       /**< Suspend power saving mode is permitted. */
#define APMCONFIG_PERMIT_MAX            APMCONFIG_PERMIT_SUSPEND    /**< All modes of power saving are permitted. */
/** @} */


/** @ingroup REF_MISC
 *  @{ */
#define APMCONFIG_BAT_LEVEL_OK          2
#define APMCONFIG_BAT_LEVEL_LOW         1
#define APMCONFIG_BAT_LEVEL_CRITICAL    0
/** @} */


/** @ingroup REF_MISC
 *  @{ */
#define APMCONFIG_STANDBY_MCU_FW_VER_BUF_SIZE   20  /**< Length of buffer to store Standby MCU firmware version. */
/** @} */


/** @ingroup REF_APMCONFIG_CONFIG
 *  @{ */
#define APMCONFIG_M_0_EMPTY             0x00000000  /**< Used as an APMCONFIG_M_xxx anchor in document. */
#define APMCONFIG_M_APM_STATE           0x00000001  /**< Retrieves or sets the svc_power_apm_config.apm_state. */
#define APMCONFIG_M_APM_INIT_MODE       0x00000002  /**< Retrieves or sets the svc_power_apm_config.apm_init_mode. */
#define APMCONFIG_M_INACTIVITY_TIME     0x00000004  /**< Retrieves or sets the svc_power_apm_config.inactivity_time. */
#define APMCONFIG_M_STANDBY_TIMEOUT     0x00000008  /**< Retrieves or sets the svc_power_apm_config.standby_timeout. */
#define APMCONFIG_M_TIME_TO_SHUTDOWN    0x00000010  /**< Retrieves or sets the svc_power_apm_config.time_to_shutdown. */
#define APMCONFIG_M_SHUTDOWN_ISO_HIBER  0x00000020  /**< Retrieves or sets the svc_power_apm_config.shutdown_iso_hibernate. */
#define APMCONFIG_M_NETWORK_STATE       0x00000040  /**< Retrieves or sets the svc_power_apm_config.network_on. */
#define APMCONFIG_M_AUTO_MODE           0x10000000  /**< Retrieves or sets the svc_power_apm_config.auto_mode. */
#define APMCONFIG_M_AUTO_MODE_HIBERNATE 0x20000000  /**< Retrieves or sets the svc_power_apm_config.auto_mode_hibernate. */
#define APMCONFIG_M_AUTO_MODE_WAKEUP    0x40000000  /**< Retrieves or sets the svc_power_apm_config.auto_wakeup_time. */
#define APMCONFIG_M_AUTO_MODE_SLEEP     0x80000000  /**< Retrieves or sets the svc_power_apm_config.auto_sleep_time. */
#define APMCONFIG_M_WAKEUP_DEVICES      0x00100000  /**< Retrieves or sets the svc_power_apm_config.wakeup_devices. */
//                                        -010004-
/** Retrieves or sets all Auto Mode related parameters. */
#define APMCONFIG_M_MASK_AUTOMODE       (APMCONFIG_M_AUTO_MODE | APMCONFIG_M_AUTO_MODE_HIBERNATE | \
                                        APMCONFIG_M_AUTO_MODE_WAKEUP | APMCONFIG_M_AUTO_MODE_SLEEP)
/** @} */
/** @ingroup REF_APMCONFIG_CONFIG
 *  @{ */
// Must be like bit-flags, in use in pm.c too.
#define AMPCONFIG_AUTO_MODE_DISABLED            0   /**< Auto Mode is disabled. */
#define AMPCONFIG_AUTO_MODE_APM1                1   /**< APM v1 compatible Auto Mode.
    Sleep and Wake-up, uses Suspend as sleep mode. */
#define AMPCONFIG_AUTO_MODE_WAKE_UP_ONLY        2   /**< APM v2 only; Auto Mode: Wake-up.
    Uses the svc_power_apm_config.auto_wakeup_time for wake-up only. */
#define AMPCONFIG_AUTO_MODE_SLEEP_ONLY          4   /**< APM v2 only; Auto Mode: Sleep.
    Uses the svc_power_apm_config.auto_sleep_time for sleep only. */
#define AMPCONFIG_AUTO_MODE_WAKE_UP_AND_SLEEP   8   /**< APM v2 only;  Auto Mode: Sleep and Wake-up.
    Uses both svc_power_apm_config.auto_sleep_time and vc_power_apm_config.auto_wakeup_time. */
/** @} */


#ifdef ENABLE_PMV1_DEPRECATED_FUNCTIONS
/** @ingroup REF_OBSOLETE
 *  @{ */
/** Replaced by @ref APMCONFIG_ERROR_AUTOMODE_TIME_TOO_SHORT */
#define APM_CONFIG_TIME_TOO_SHORT_ERROR         APMCONFIG_ERROR_AUTOMODE_TIME_TOO_SHORT
/** Replaced by @ref APMCONFIG_ERROR_AUTOMODE_CONFIG_FAILURE */
#define APM_CONFIG_FAUILR_ERROR                 APMCONFIG_ERROR_AUTOMODE_CONFIG_FAILURE

#define APM_PERMIT_NONE                         APMCONFIG_PERMIT_NONE       /**< Replaced by @ref APMCONFIG_PERMIT_NONE */
#define APM_PERMIT_STANDBY                      APMCONFIG_PERMIT_STANDBY    /**< Replaced by @ref APMCONFIG_PERMIT_STANDBY */
#define APM_PERMIT_SUSPEND                      APMCONFIG_PERMIT_SUSPEND    /**< Replaced by @ref APMCONFIG_PERMIT_SUSPEND */

/** Replaced by @ref APMCONFIG_BAT_LEVEL_OK */
#define APM_SUFFIENT_POWER_LEVEL_OK             APMCONFIG_BAT_LEVEL_OK
/** Replaced by @ref APMCONFIG_BAT_LEVEL_LOW */
#define APM_SUFFIENT_POWER_LEVEL_LOW_BAT        APMCONFIG_BAT_LEVEL_LOW
/** Replaced by @ref APMCONFIG_BAT_LEVEL_CRITICAL */
#define APM_SUFFIENT_POWER_LEVEL_CRITICAL_BAT   APMCONFIG_BAT_LEVEL_CRITICAL
/** @} */
#endif // ENABLE_PMV1_DEPRECATED_FUNCTIONS

/*
 * Duplicated from apm_config.h
 ******************************************************************************/


/** @ingroup REF_WAKEUPSOURCES
 *  @{ */
#define PM_WAKE_DEVICE_0_EMPTY              0x00000000  /**< All wake-up sources disabled. */
#define PM_WAKE_DEVICE_KEYPAD               0x00000001  /**< Wake up source: keypad. */
#define PM_WAKE_DEVICE_RTC_ALARM            0x00000002  /**< Wake up source: RTC Alarm. */
#define PM_WAKE_DEVICE_TOUCH_SCREEN         0x00000004  /**< Wake up source: Touch Screen. */
#define PM_WAKE_DEVICE_UART0                0x00000008  /**< Wake up source: UART 0. */
#define PM_WAKE_DEVICE_UART1                0x00000010  /**< Wake up source: UART 1. */
#define PM_WAKE_DEVICE_UART2                0x00000020  /**< Wake up source: UART 2. */
#define PM_WAKE_DEVICE_UART3                0x00000040  /**< Wake up source: UART 3. */
#define PM_WAKE_DEVICE_USB_OTG              0x00000080  /**< Not supported. */
#define PM_WAKE_DEVICE_USB_HOST             0x00000100  /**< Wake up source: USB Host. */
#define PM_WAKE_DEVICE_EXTGRP5              0X00000200  /**< Wake up source: sysmode button/MCU/External Power supply. */
#define PM_WAKE_DEVICE_KEYPAD_FUNC          0X00000400  /**< Wake up source: External ATM Keypad. */
/** @} */
/** @ingroup REF_WAKEUPSOURCES
 *  @{ */
// StandBY MCU Wake up sources
#define PM_WAKE_DEVICE_MCU_SERIAL           0x10000000  /**< Standby MCU Wake up source: Serial port. */
#define PM_WAKE_DEVICE_MCU_MDBWAKE          0x20000000  /**< Standby MCU Wake up source: MDB wake-up line. */
#define PM_WAKE_DEVICE_MCU_CARDIN           0x40000000  /**< Standby MCU Wake up source: Card Insertion. */
#define PM_WAKE_DEVICE_MCU_PROXIMITY        0x80000000  /**< Standby MCU Wake up source: Proximity. */
/** @} */


/** @ingroup REF_EVENTS
 *  @{ */
#define APM_SYS_STANDBY                     0x0001
#define APM_SYS_SUSPEND                     0x0002
#define APM_NORMAL_RESUME                   0x0003
#define APM_CRITICAL_RESUME                 0x0004
#define APM_LOW_BATTERY                     0x0005
#define APM_POWER_STATUS_CHANGE             0x0006
#define APM_UPDATE_TIME                     0x0007
#define APM_CRITICAL_SUSPEND                0x0008
#define APM_USER_STANDBY                    0x0009
#define APM_USER_SUSPEND                    0x000a
#define APM_STANDBY_RESUME                  0x000b
#define APM_CAPABILITY_CHANGE               0x000c
#define APM_CRITICAL_BATTERY                0x000d
#define APM_FORCE_SHUTDOWN                  0x000e
#define APM_CRITICAL_BATT_TEMP              0x000f
/** @} */
/** @ingroup REF_EVENTS
 *  @{ */
#define APM_SYS_HIBERNATE                   0x001d
#define APM_USER_HIBERNATE                  0x001e
#define APM_HIBERNATE_RESUME                0x001f
/** @} */


/** @ingroup REF_APMCONFIG_PERMIT
 *  @{ */
#define BUSY_FULL                       0   /**< No one power saving mode is admissible */
#define BUSY_BACKGROUND                 1   /**< The higher admissible power saving mode is Standby */
#define IDLE                            2   /**< All modes of power saving are admissible */
/** @} */


/** @ingroup REF_OBSOLETE
 *  Advanced Power Management v1 configuration structure.
 *  Obsolete and deprecated structure to provide compatibility with APM API v1.
 *  Used for powermngt_get_config() and powermngt_set_config() functions.
 *
 *  @note There were a lot of problems with code backward compatibility during the new features developing, due to error in
 *  service daemon code and because @a auto_suspend_state was inserted in the middle of structure, not at the end,
 *  @a auto_suspend_state was deleted (functionality is still accessible using APM API v2), and structure apm_config is freeze
 *  now, and it is compatible with release-30071200. Backward compatibility with older apm_config structures is not provided.
 */
struct apm_config
{
    unsigned int Signature;                 /**< Not used. */
    unsigned int Size;                      /**< Not used correctly, can be ignored from application side. */
    unsigned int apm_on;                    /**< Automatic Power Management State.
        1 = On (@ref apm_config.InactivityTimeToSusspend "InactivityTimeToSusspend" could not be 0),
        0 = Off (@ref apm_config.InactivityTimeToSusspend "InactivityTimeToSusspend" must be 0). */
    unsigned int InactivityTimeToSusspend;  /**< Inactivity time.
        Specifies inactivity time in minutes after which system is sent to defined power saving state.
        If @ref apm_config.apm_on "apm_on" is 0, must be set to 0 too. */
    unsigned int auto_mode;                 /**< Auto Mode State.
        1 = On, 0 = Off. If Auto Mode is On, automatic shutdown and wake-up times is used. */
    unsigned int AutoTimeToShutDown;        /**< Auto time to shut down.
        In use only if Auto Mode is On. Time in @ref REF_DATETIME "APM time format". */
    unsigned int AutoTimeToWakeUp;          /**< Auto time to wake up.
        In use only if Auto Mode is On. Time in @ref REF_DATETIME "APM time format". */
    unsigned int Standby_timeout;           /**< Standby timeout.
        Specifies how long in minutes terminal should spend in Standby mode, before switching to Suspend mode. */
    unsigned int communication_on;          /**< Network link state during suspend mode.
        1 = Network Up, 0 = Network Down. */
    unsigned int Shutdown_timeout;          /**< Shutdown timeout.
        Specifies how long terminal should spend in Suspend mode, before switching to Hibernate/Shutdown mode. */
    unsigned int permit_state;              /**< Permitted power saving level.
        One of @ref APMCONFIG_PERMIT_MAX "APM_PERMIT_xxx". In APM API v2 this member can be use only to read value! */
};
// STRUCT_SIZE_V1_3, size 44 (0x2C)


/** @ingroup REF_APMCONFIG_CONFIG
 *  Advanced Power Management v2 configuration structure.
 */
struct svc_power_apm_config
{
    unsigned int item_mask;                 /**< Indicates the members to be retrieved or set.
        This member can be one or more of the @ref APMCONFIG_M_0_EMPTY "APMCONFIG_M_xxx". */
    unsigned int apm_state;                 /**< APM state.
        If 0 then disabled, if 1 then enabled. When enabled, system is ready to go to defined power state after specified
        inactivity time.
        @n @ref APMCONFIG_M_APM_STATE must be specified in @ref svc_power_apm_config.item_mask "item_mask"
        to set or retrieve this member. */
    unsigned int apm_init_mode;             /**< APM initial power saving mode.
        Specifies power saving mode that will be activated after inactivity time if APM state is enabled. Following initial
        power modes are available: 0 - default (Standby); 1 - Standby; 2 - Suspend; 3 - Hibernate.
        @n Is the Hibernate available, depends on hardware model.
        @n @ref APMCONFIG_M_APM_INIT_MODE must be specified in @ref svc_power_apm_config.item_mask "item_mask"
        to set or retrieve this member. */
    unsigned int inactivity_time;           /**< Inactivity time, in minutes.
        If APM is in enabled state, after inactivity time the system activates power saving mode specified in
        @ref svc_power_apm_config.apm_init_mode "apm_init_mode".
        @n @ref APMCONFIG_M_INACTIVITY_TIME must be specified in @ref svc_power_apm_config.item_mask "item_mask"
        to set or retrieve this member. */
    unsigned int standby_timeout;           /**< Standby power mode timeout, in minutes.
        Specifies how long terminal should spend in Standby power mode, before switching to Suspend power mode.
        @n @ref APMCONFIG_M_STANDBY_TIMEOUT must be specified in @ref svc_power_apm_config.item_mask "item_mask"
        to set or retrieve this member. */
    unsigned int time_to_shutdown;          /**< Time to shutdown, in minutes.
        @b UX3xx @b models: Time after which the system will initiate the Hibernate/Shutdown after the Suspend mode.
        @n @b Other @b models: Time after which the system will initiate the Shutdown, after the Standby mode
        (??? Vx675 only ???).
        @n If Auto Mode is enabled or @p time_to_shutdown is 0, this member will be ignored and system will still stays in
        Suspend mode.
        @n @ref APMCONFIG_M_TIME_TO_SHUTDOWN must be specified in @ref svc_power_apm_config.item_mask "item_mask"
        to set or retrieve this member.
        @note There is no clarity with possible realization in other models than Ux3xx. */
    unsigned int shutdown_iso_hibernate;    /**< Use Shutdown instead of Hibernate after APM Suspend.
        Specifies action in Suspend power saving state after @ref svc_power_apm_config.time_to_shutdown "time_to_shutdown"
        (if it is specified) if Auto Mode is disabled.
        @n If 0 and hardware have the Hibernate support, the Hibernate mode will be used instead of Shutdown. Otherwise, or if
        @p shutdown_iso_hibernate is 1, Shutdown mode will be forced. Is the Hibernate mode available, depends on hardware
        model.
        @n @ref APMCONFIG_M_SHUTDOWN_ISO_HIBER must be specified in @ref svc_power_apm_config.item_mask "item_mask"
        to set or retrieve this member. */
    unsigned int network_on;                /**< Network link state during suspend mode.
        1 = Network Up, 0 = Network Down.
        @n @ref APMCONFIG_M_NETWORK_STATE must be specified in @ref svc_power_apm_config.item_mask "item_mask"
        to set or retrieve this member. */
    unsigned int auto_mode;                 /**< Auto Mode.
        If 0 (@ref AMPCONFIG_AUTO_MODE_DISABLED) then disabled. Otherwise one of AMPCONFIG_AUTO_MODE_xxx can be specified: @n
        @ref AMPCONFIG_AUTO_MODE_APM1 - Go to suspend at @ref svc_power_apm_config.auto_sleep_time "auto_sleep_time", wake-up
          at @ref svc_power_apm_config.auto_wakeup_time "auto_wakeup_time" (provided compatibility with APM v1); @n
        [@b IN @b DEVELOPMENT] @ref AMPCONFIG_AUTO_MODE_WAKE_UP_ONLY - Wake-up at @ref svc_power_apm_config.auto_wakeup_time "auto_wakeup_time"; @n
        [@b IN @b DEVELOPMENT] @ref AMPCONFIG_AUTO_MODE_SLEEP_ONLY - Go to Suspend or Suspend->Hibernate at
          @ref svc_power_apm_config.auto_sleep_time "auto_sleep_time"; @n
        [@b IN @b DEVELOPMENT] @ref AMPCONFIG_AUTO_MODE_WAKE_UP_AND_SLEEP - Wake-up at
          @ref svc_power_apm_config.auto_wakeup_time "auto_wakeup_time" and stay active till
          @ref svc_power_apm_config.auto_sleep_time "auto_sleep_time". Try Suspend or Suspend->Hibernate in time range between
          @ref svc_power_apm_config.auto_sleep_time "auto_sleep_time" and
          @ref svc_power_apm_config.auto_wakeup_time "auto_wakeup_time"; @n
        Suspend or Suspend->Hibernate depends from @ref svc_power_apm_config.auto_mode_hibernate "auto_mode_hibernate".
        @n @ref APMCONFIG_M_AUTO_MODE must be specified in @ref svc_power_apm_config.item_mask "item_mask"
        to set or retrieve this member. */
    unsigned int auto_mode_hibernate;       /**< Use Hibernate in Auto Mode.
        Used if @ref svc_power_apm_config.auto_mode "auto_mode" is @ref AMPCONFIG_AUTO_MODE_SLEEP_ONLY or
        @ref AMPCONFIG_AUTO_MODE_WAKE_UP_AND_SLEEP.
        At @ref svc_power_apm_config.auto_sleep_time "auto_sleep_time" the system goes to the Suspend state, then:
        if @ref svc_power_apm_config.auto_mode_hibernate "auto_mode_hibernate" is 0 then system stays in Suspend, if 1 - after
        1 min timeout goes to Hibernate.
        @n Is the Hibernate mode available, depends on hardware model.
        @n @ref APMCONFIG_M_AUTO_MODE_HIBERNATE must be specified in @ref svc_power_apm_config.item_mask "item_mask"
        to set or retrieve this member. */
    unsigned int auto_wakeup_time;          /**< Auto Mode Wake-up time.
        Defines system wake-up time for appropriate Auto Mode state. Time value is provided in APM time format.
        @ref REF_DATETIME "APMCONFIG_TIME" macros can be used to convert formats between APM time and hours, minutes and
        seconds. powermngt_DateTimeToApmTime() converts svc_power_DateTime specified time to APM time format.
        @n @ref APMCONFIG_M_AUTO_MODE_WAKEUP must be specified in @ref svc_power_apm_config.item_mask "item_mask"
        to set or retrieve this member. */
    unsigned int auto_sleep_time;           /**< Auto Mode Sleep time.
        Defines time when system must enter to Sleep (Suspend) state for appropriate Auto Mode state. Time value is provided in
        APM time format.
        @ref REF_DATETIME "APMCONFIG_TIME" macros can be used to convert formats between APM time and hours, minutes and
        seconds. powermngt_DateTimeToApmTime() converts svc_power_DateTime specified time to APM time format.
        @n @ref APMCONFIG_M_AUTO_MODE_SLEEP must be specified in @ref svc_power_apm_config.item_mask "item_mask"
        to set or retrieve this member. */
    unsigned int wakeup_devices;            /**< Wake-up devices.
        Specifies the wake-up devices that can wake the system.
        @n @ref APMCONFIG_M_WAKEUP_DEVICES must be specified in @ref svc_power_apm_config.item_mask "item_mask"
        to set or retrieve this member. */
    //
    unsigned int z_reserved_int[19];        /**< Reserved for future use. Pads structure size to 128 (0x80) bytes. */
};


/** @ingroup REF_DATETIME
 *  Replica of the linux structure tm in time.h
 */
struct svc_power_DateTime
{
    int tm_sec;     /**< seconds */
    int tm_min;     /**< minutes */
    int tm_hour;    /**< hours */
    int tm_mday;    /**< day of the month */
    int tm_mon;     /**< month */
    int tm_year;    /**< year */
    int tm_wday;    /**< day of the week */
    int tm_yday;    /**< day in the year */
    int tm_isdst;   /**< daylight saving time */
};


/** @ingroup REF_MISC
 *  APM information structure.
 */
struct svc_apm_info
{
    char driver_version[10];    /**< Driver version */
    int apm_version_major;      /**< APM version major */
    int apm_version_minor;      /**< APM version minor */
    int apm_flags;              /**< APM Flags.
        Combination of @ref APM_16_BIT_SUPPORT, @ref APM_32_BIT_SUPPORT, @ref APM_IDLE_SLOWS_CLOCK, @ref APM_BIOS_DISABLED
        and @ref APM_BIOS_DISENGAGED */
    int ac_line_status;         /**< AC line status, one of @ref SVC_AC_LINE_STATUS_BACKUP "SVC_AC_LINE_STATUS_xxx" */
    int battery_status;         /**< Battery status, one of @ref SVC_BATTERY_STATUS_ABSENT "SVC_BATTERY_STATUS_xxx" */
    int battery_flags;          /**< Battery flags, @ref SVC_BATTERY_FLAGS_CHARGING "SVC_BATTERY_FLAGS_xxx" */
    int battery_percentage;     /**< Battery percentage or @ref SVC_BATTERY_PERCENTAGE_UNKNOWN */
    int battery_time;           /**< Battery time or @ref SVC_BATTERY_TIME_UNKNOWN */
    int using_minutes;
};


/*SVC_STRUCT*/
/** @ingroup REF_MISC
 *  APM battery information structure.
 */
struct svc_apm_battery_info
{
    int battery_flags;          /**< Battery flags, @ref SVC_BATTERY_FLAGS_CHARGING "SVC_BATTERY_FLAGS_xxx" */
    int battery_percentage;     /**< Battery percentage or @ref SVC_BATTERY_PERCENTAGE_UNKNOWN */
    int battery_time;           /**< Battery time or @ref SVC_BATTERY_TIME_UNKNOWN */
    unsigned long int voltage;  /**< Battery voltage level in units of 0.01v) */
    int power_connected;        /**< Connected power status/source, @ref SVC_POWER_CONNECTED_NO */
    signed char temperature;    /**< battery temperature, -10 to 60 deg C */
    char type;                  /**< 1 = Smart battery, 2 = Simple battery (No fuel gauge) */
    unsigned short int max_charge;  /**< Units [mAh], e.g: 2200mAh, 2450mAh */
    char reserved[16];
};

/*
 * Encodings for svc_apm_info and svc_apm_battery_info
 */

/** @ingroup REF_MISC
 *  @{ */
#define APM_16_BIT_SUPPORT              0x0001      /**< svc_apm_info.apm_flags flag */
#define APM_32_BIT_SUPPORT              0x0002      /**< svc_apm_info.apm_flags flag */
#define APM_IDLE_SLOWS_CLOCK            0x0004      /**< svc_apm_info.apm_flags flag */
#define APM_BIOS_DISABLED               0x0008      /**< svc_apm_info.apm_flags flag */
#define APM_BIOS_DISENGAGED             0x0010      /**< svc_apm_info.apm_flags flag */
/** @} */

/** @ingroup REF_MISC
 *  @{ */
#define SVC_AC_LINE_STATUS_OFF          0           /**< svc_apm_info.ac_line_status */
#define SVC_AC_LINE_STATUS_ON           1           /**< svc_apm_info.ac_line_status */
#define SVC_AC_LINE_STATUS_BACKUP       2           /**< svc_apm_info.ac_line_status */
#define SVC_AC_LINE_STATUS_UNKNOWN      0xff        /**< svc_apm_info.ac_line_status */
/** @} */

/** @ingroup REF_MISC
 *  @{ */
#define SVC_BATTERY_STATUS_HIGH         0           /**< svc_apm_info.battery_status */
#define SVC_BATTERY_STATUS_LOW          1           /**< svc_apm_info.battery_status */
#define SVC_BATTERY_STATUS_CRITICAL     2           /**< svc_apm_info.battery_status */
#define SVC_BATTERY_STATUS_CHARGING     3           /**< svc_apm_info.battery_status */
#define SVC_BATTERY_STATUS_ABSENT       4           /**< svc_apm_info.battery_status */
#define SVC_BATTERY_STATUS_FULL         5           /**< svc_apm_info.battery_status */
#define SVC_BATTERY_STATUS_UNKNOWN      0xff        /**< svc_apm_info.battery_status */
/** @} */

/** @ingroup REF_MISC
 *  @{ */
#define SVC_BATTERY_FLAGS_HIGH                      0x0001	/**< svc_apm_info.battery_flags */
#define SVC_BATTERY_FLAGS_LOW                       0x0002	/**< svc_apm_info.battery_flags */
#define SVC_BATTERY_FLAGS_CRITICAL                  0x0004	/**< svc_apm_info.battery_flags */
#define SVC_BATTERY_FLAGS_CHARGING      			0x0008	/**< svc_apm_info.battery_flags */
#define SVC_BATTERY_FLAGS_CHARGING_RATE_TRICKLE     0x0100	/**< svc_apm_info.battery_flags */
#define SVC_BATTERY_FLAGS_CHARGING_RATE_SLOW        0x0200	/**< svc_apm_info.battery_flags */
#define SVC_BATTERY_FLAGS_CHARGING_RATE_MODERATE    0x0400	/**< svc_apm_info.battery_flags */
#define SVC_BATTERY_FLAGS_CHARGING_RATE_FAST        0x0800	/**< svc_apm_info.battery_flags */
#define SVC_BATTERY_FLAGS_ABSENT                    0x0080	/**< svc_apm_info.battery_flags */
#define SVC_BATTERY_FLAGS_REQUIRED                  0x1000  /**< svc_apm_info.battery_flags */

#define SVC_BATTERY_LEVEL_MASK	0x000f /**< mask applied to svc_apm_info.battery_flags to see level only */
#define SVC_BATTERY_CHARGING_MASK	0x0f00 /**< mask applied to svc_apm_info.battery_flags to see changing rate */
/** @} */

/** @ingroup REF_MISC
 *  @{ */
#define SVC_POWER_CONNECTED_NO                      0x0000  /**< svc_apm_battery_info.power_connected */
#define SVC_POWER_CONNECTED_YES                     0x0001  /**< svc_apm_battery_info.power_connected */
#define SVC_POWER_CONNECTED_SOURCE_AC               0x0010  /**< svc_apm_battery_info.power_connected */
#define SVC_POWER_CONNECTED_SOURCE_BASE             0x0020  /**< svc_apm_battery_info.power_connected */
#define SVC_POWER_CONNECTED_SOURCE_USB              0x0040  /**< svc_apm_battery_info.power_connected */

#define SVC_POWER_CONNECTED_SOURCE_MASK             0x00f0  /**< svc_apm_battery_info.power_connected mask to see power source */
/** @} */

/** @ingroup REF_MISC
 *  @{ */
#define SVC_BATTERY_PERCENTAGE_UNKNOWN  (-1)        /**< svc_apm_info.battery_percentage */
#define SVC_BATTERY_TIME_UNKNOWN        (-1)        /**< svc_apm_info.battery_time */
/** @} */


/*SVC_PROTOTYPE*/ struct version powermngt_getVersion(void);

/*
 * Encodings for powermngt_charging_info
 */

/** @ingroup REF_MISC
 *  @ { */
#define SVC_CHARGING_STATUS_OFF      0    /**< powermngt_charging_info.charging_status - charging is off (undocked/unplugged) */
#define SVC_CHARGING_STATUS_ACTIVE   1    /**< powermngt_charging_info.charging_status - charging is active (docked/connected) */
/** @} */

/** @ingroup REF_MISC
 *  Gets power management info.
 *  @n Function is supported in: APM API v1, v2.
 *
 *  @param svc_apm_info_ptr A pointer to the @a svc_apm_info structure that receives data.
 *
 *  @return If the function succeeds, the return value is @ref APMCONFIG_SUCCESS (zero).
 *  If the function fails, the return value is one of @ref APMCONFIG_ERROR "APMCONFIG_ERROR_xxx" (non-zero).
 */
int powermngt_GetInfo(struct svc_apm_info *svc_apm_info_ptr);


/** @ingroup REF_MISC
 *  Gets power management power info.
 *  @n Function is supported in: APM API v2.
 *
 *  @return  svc_apm_battery_info contains battery values (see struct svc_apm_battery_info)
 *
 *  <b>Errno values</b>:
 *  @li ENOMEM - If unable to malloc required space
 *  @li EBADF - If unable to read from apm device
 */
/*SVC_PROTOTYPE*/ struct svc_apm_battery_info powermngt_getPowerInfo(void);


/** @ingroup REF_MISC
 *  Gets power management build version.
 *  @n Function is supported in: APM API v2. It is replacement for obsolete APM API v1 powermngt_getVersion().
 *
 *  @param version_ptr A pointer to the @a version structure (defined in svcmgrSvcDef.h) that receives data.
 *
 *  @return If the function succeeds, the return value is @ref APMCONFIG_SUCCESS (zero).
 *  If the function fails, the return value is one of @ref APMCONFIG_ERROR "APMCONFIG_ERROR_xxx" (non-zero).
 */
int powermngt_GetBuildVersion(struct version *version_ptr);


/** @ingroup REF_APMCONFIG_CONFIG
 *  Gets the APM Get-Configuration capabilities.
 *  Available capabilities is retrieved as the combination of @ref APMCONFIG_M_0_EMPTY "APMCONFIG_M_xxx" flags.
 *  @n Function is supported in: APM API v2.
 *
 *  @param conf_cap_ptr A pointer to the variable that receives a set of available APM Get-Configuration capabilities.
 *
 *  @return If the function succeeds, the return value is @ref APMCONFIG_SUCCESS (zero).
 *  If the function fails, the return value is one of @ref APMCONFIG_ERROR "APMCONFIG_ERROR_xxx" (non-zero).
 */
int powermngt_GetGetConfigCapabilities(unsigned int *conf_cap_ptr);


/** @ingroup REF_APMCONFIG_CONFIG
 *  Gets the APM current configuration.
 *  @n Function is supported in: APM API v2. It is replacement for obsolete APM API v1 powermngt_get_config().
 *
 *  @param cfg_ptr A pointer to the svc_power_apm_config structure that receives configuration.
 *  @ref svc_power_apm_config.item_mask must be set with appropriate parameters @ref APMCONFIG_M_0_EMPTY "APMCONFIG_M_xxx"
 *  flags before function call. Set @a item_mask with capability list retrieved by powermngt_GetGetConfigCapabilities()
 *  in order to get all readable configuration parameters.
 *
 *  @return If the function succeeds, the return value is @ref APMCONFIG_SUCCESS (zero).
 *  If the function fails, the return value is one of @ref APMCONFIG_ERROR "APMCONFIG_ERROR_xxx" (non-zero).
 *  Unreadable @a mask_item member flags will be cleared.
 */
int powermngt_GetConfig(struct svc_power_apm_config *cfg_ptr);


/** @ingroup REF_APMCONFIG_CONFIG
 *  Gets one APM current configuration parameter.
 *  @n Function is supported in: APM API v2.
 *
 *  @param param_flag Identifier of the parameter to retrieve. One of @ref APMCONFIG_M_0_EMPTY "APMCONFIG_M_xxx" flag.
 *  @param param_ptr A pointer to the variable that receives configuration parameter value.
 *
 *  @return If the function succeeds, the return value is @ref APMCONFIG_SUCCESS (zero).
 *  If the function fails, the return value is one of @ref APMCONFIG_ERROR "APMCONFIG_ERROR_xxx" (non-zero).
 */
int powermngt_GetConfigParam(unsigned int param_flag, unsigned int *param_ptr);


/** @ingroup REF_APMCONFIG_CONFIG
 *  Gets the APM Set-Configuration capabilities.
 *  Available capabilities is retrieved as the combination of @ref APMCONFIG_M_0_EMPTY "APMCONFIG_M_xxx" flags.
 *  @n Function is supported in: APM API v2.
 *
 *  @param conf_cap_ptr A pointer to the variable that receives a set of available APM Set-Configuration capabilities.
 *
 *  @return If the function succeeds, the return value is @ref APMCONFIG_SUCCESS (zero).
 *  If the function fails, the return value is one of @ref APMCONFIG_ERROR "APMCONFIG_ERROR_xxx" (non-zero).
 */
int powermngt_GetSetConfigCapabilities(unsigned int *conf_cap_ptr);


/** @ingroup REF_APMCONFIG_CONFIG
 *  Sets the APM current configuration.
 *  @n Function is supported in: APM API v2. It is replacement for obsolete APM API v1 powermngt_set_config().
 *
 *  @param cfg_ptr A pointer to the svc_power_apm_config structure with configuration that be set to APM.
 *  @ref svc_power_apm_config.item_mask must be set with appropriate parameters @ref APMCONFIG_M_0_EMPTY "APMCONFIG_M_xxx"
 *  flags before function call.
 *
 *  @return If the function succeeds, the return value is @ref APMCONFIG_SUCCESS (zero).
 *  If the function fails, the return value is one of @ref APMCONFIG_ERROR "APMCONFIG_ERROR_xxx" (non-zero).
 */
int powermngt_SetConfig(struct svc_power_apm_config *cfg_ptr);


/** @ingroup REF_APMCONFIG_CONFIG
 *  Sets one APM current configuration parameter.
 *  @n Function is supported in: APM API v2.
 *
 *  @param param_flag Identifier of the parameter to set. One of @ref APMCONFIG_M_0_EMPTY "APMCONFIG_M_xxx" flag.
 *  @param param_val Configuration parameter value to be set..
 *
 *  @return If the function succeeds, the return value is @ref APMCONFIG_SUCCESS (zero).
 *  If the function fails, the return value is one of @ref APMCONFIG_ERROR "APMCONFIG_ERROR_xxx" (non-zero).
 */
int powermngt_SetConfigParam(unsigned int param_flag, unsigned int param_val);


/** @ingroup REF_APM_MODES
 *  Forces Standby Power saving mode.
 *  @n Function is supported in: APM API v1, v2.
 *
 *  @return If the function succeeds, the return value is @ref APMCONFIG_SUCCESS (zero).
 *  If the function fails, the return value is one of @ref APMCONFIG_ERROR "APMCONFIG_ERROR_xxx" (non-zero).
 */
int powermngt_Standby(void);


/** @ingroup REF_APM_MODES
 *  Forces Suspend Power saving mode.
 *  @n Function is supported in: APM API v2. It is replacement for obsolete APM API v1 powermngt_Susspend().
 *
 *  @return If the function succeeds, the return value is @ref APMCONFIG_SUCCESS (zero).
 *  If the function fails, the return value is one of @ref APMCONFIG_ERROR "APMCONFIG_ERROR_xxx" (non-zero).
 */
/*SVC_PROTOTYPE*/ int powermngt_Suspend(void);


/** @ingroup REF_APM_MODES
 *  Forces Hibernate Power saving mode.
 *  @n Function is supported in: APM API v1, v2.
 *
 *  @return If the function succeeds, the return value is @ref APMCONFIG_SUCCESS (zero).
 *  If the function fails, the return value is one of @ref APMCONFIG_ERROR "APMCONFIG_ERROR_xxx" (non-zero).
 */
/*SVC_PROTOTYPE*/ int powermngt_Hibernate(void);


/** @ingroup REF_APM_MODES
 *  Forces shut down immediately.
 *  @n Function is supported in: APM API v1, v2.
 *
 *  @return If the function succeeds, the return value is @ref APMCONFIG_SUCCESS (zero).
 *  If the function fails, the return value is one of @ref APMCONFIG_ERROR "APMCONFIG_ERROR_xxx" (non-zero).
 *
 *  @note [?] Unit which does not have AC (external power) connected could not wake up from RTC alarm if it was set before
 *  using powermngt_setwakeuptime().
 */
/*SVC_PROTOTYPE*/ int powermngt_Shutdown(void);

/** @ingroup REF_APM_MODES
 *  Abort the secure installer's power off.
 *  @n Function is supported in: APM API v2.
 *
 *  @return If the function succeeds, the return value is @ref APMCONFIG_SUCCESS (zero).
 *  If the function fails, the return value is one of @ref APMCONFIG_ERROR "APMCONFIG_ERROR_xxx" (non-zero).
 */
/*SVC_PROTOTYPE*/ int powermngt_Abort_Shutdown(void);

/** @ingroup REF_APM_MODES
 *  Enters PM critical section.
 *  Power Management events will be suspended till a call to powermngt_CriticalSectionExit().
 *  @n Function is supported in: APM API v1, v2.
 *
 *  @return If the function succeeds, the return value is @ref APMCONFIG_SUCCESS (zero).
 *  If the function fails, the return value is one of @ref APMCONFIG_ERROR "APMCONFIG_ERROR_xxx" (non-zero).
 *
 *  @warning Obsolete and deprecated function. Use powermngt_CriticalSectionEnter() instead.
 */
int powermngt_CritcalSectionEnter(void);

/** @ingroup REF_APM_MODES
 *  Enters PM critical section.
 *  Power Management events will be suspended till a call to powermngt_CriticalSectionExit().
 *  @n Function is supported in: APM API v1, v2.
 *
 *  @return If the function succeeds, the return value is @ref APMCONFIG_SUCCESS (zero).
 *  If the function fails, the return value is one of @ref APMCONFIG_ERROR "APMCONFIG_ERROR_xxx" (non-zero).
 */
/*SVC_PROTOTYPE*/ int powermngt_CriticalSectionEnter(void);

/** @ingroup REF_APM_MODES
 *  Exits PM critical section.
 *  Power Management events will be resume to its last state.
 *  @n Function is supported in: APM API v1, v2.
 *
 *  @return If the function succeeds, the return value is @ref APMCONFIG_SUCCESS (zero).
 *  If the function fails, the return value is one of @ref APMCONFIG_ERROR "APMCONFIG_ERROR_xxx" (non-zero).
 *
 *  @warning Obsolete and deprecated function. Use powermngt_CriticalSectionExit() instead.
 */
int powermngt_CritcalSectionExit(void);

/** @ingroup REF_APM_MODES
 *  Exits PM critical section.
 *  Power Management events will be resume to its last state.
 *  @n Function is supported in: APM API v1, v2.
 *
 *  @return If the function succeeds, the return value is @ref APMCONFIG_SUCCESS (zero).
 *  If the function fails, the return value is one of @ref APMCONFIG_ERROR "APMCONFIG_ERROR_xxx" (non-zero).
 */
/*SVC_PROTOTYPE*/ int powermngt_CriticalSectionExit(void);

/** @ingroup REF_APM_MODES 
 *  Initiates USB Remote Wakeup to wake up the host
 *
 *  @return
 *  @li 0 Success
 *  @li -1 Error
 *
 *  <b>Errno values</b>:
 *  @li EACCES - Unable to access powermngt interface
 *
 */
/*SVC_PROTOTYPE*/ int powermngt_USBRemoteWakeup(void);

/** @ingroup REF_EVENTS
 *  Power management event callback function event type.
 */
typedef unsigned short int apm_event_t;
/** @ingroup REF_EVENTS
 *  Power management event callback function prototype.
 */
typedef int (*powermngt_event_callback)(apm_event_t event);

/** @ingroup REF_EVENTS
 *  Registers to PM events.
 *  @n Function is supported in: APM API v1, v2.
 *
 *  @param callback_event A pointer to the powermngt_event_callback callback function, cast to unsigned int. If 0, then
 *  the get-event thread will be terminated if it existed before.
 *
 *  @return If the function succeeds, the return value is @ref APMCONFIG_SUCCESS (zero).
 *  If the function fails, the return value is one of @ref APMCONFIG_ERROR "APMCONFIG_ERROR_xxx" (non-zero).
 */
int powermngt_GetEvent(unsigned int callback_event);


/** @ingroup REF_MISC
 *  Gets sufficient battery level.
 *  @n Function is supported in: APM API v2. It is replacement for obsolete APM API v1 powermngt_get_sufficient_battery().
 *
 *  @param suff_bat_level_ptr A pointer to the variable that receives sufficient battery level
 *  (one of @ref APMCONFIG_BAT_LEVEL_CRITICAL "APMCONFIG_BAT_LEVEL_xxx").
 *
 *  @return If the function succeeds, the return value is @ref APMCONFIG_SUCCESS (zero).
 *  If the function fails, the return value is one of @ref APMCONFIG_ERROR "APMCONFIG_ERROR_xxx" (non-zero).
 */
int powermngt_GetSufficientBatteryLevel(unsigned int *suff_bat_level_ptr);


/** @ingroup REF_APMCONFIG_PERMIT
 *  Declares application state.
 *  Function explicitly declares to the server that an application needs a particular power state or higher.
 *  @n Function is supported in: APM API v2. It is replacement for obsolete APM API v1 powermngt_declare_state().
 *
 *  @param proc_id An application process identifier.
 *  @param state One of the following values: @ref IDLE, @ref BUSY_BACKGROUND or @ref BUSY_FULL.
 *
 *  @return If the function succeeds, the return value is @ref APMCONFIG_SUCCESS (zero).
 *  If the function fails, the return value is one of @ref APMCONFIG_ERROR "APMCONFIG_ERROR_xxx" (non-zero).
 */
int powermngt_DeclareState(int proc_id, int state);


/** @ingroup REF_APMCONFIG_PERMIT
 *  Gets permitted power saving mode.
 *  Gets the higher permitted power saving mode on system that depends on previously declared state.
 *  @n Function is supported in: APM API v2. For APM API v1 structure apm_config.permit_state member can be used for this
 *  purpose.
 *
 *  @param permit_ptr A pointer to the variable to store permitted level (one of @ref APMCONFIG_PERMIT_MAX "APM_PERMIT_xxx").
 *
 *  @return If the function succeeds, the return value is @ref APMCONFIG_SUCCESS (zero).
 *  If the function fails, the return value is one of @ref APMCONFIG_ERROR "APMCONFIG_ERROR_xxx" (non-zero).
 */
int powermngt_GetPermitLevel(unsigned int *permit_ptr);


/** @ingroup REF_DATETIME
 *  Converts svc_power_DateTime structure to APM time format.
 *  @n Function is supported in: APM API v2.
 *
 *  @param utildt A pointer to the svc_power_DateTime structure with time to convert to APM time format value.
 *
 *  @return svc_power_DateTime specified time as APM time format value.
 */
unsigned int powermngt_DateTimeToApmTime(struct svc_power_DateTime *utildt);


/** @ingroup REF_WAKEUPSOURCES
 *  Gets which devices will wake up the system.
 *  @n Function is supported in: APM API v2. It is replacement for obsolete APM API v1 powermngt_get_wakeupdevices().
 *
 *  @param wupd_ptr A pointer to the variable to store wake-up devices as combination of @ref PM_WAKE_DEVICE_0_EMPTY
 *  "PM_WAKE_DEVICE_xxx".
 *
 *  @return If the function succeeds, the return value is @ref APMCONFIG_SUCCESS (zero).
 *  If the function fails, the return value is one of @ref APMCONFIG_ERROR "APMCONFIG_ERROR_xxx" (non-zero).
 */
int powermngt_GetWakeUpDevices(unsigned int *wupd_ptr);


/** @ingroup REF_WAKEUPSOURCES
 *  Sets which devices will wake up the system.
 *  @n Function is supported in: APM API v2. It is replacement for obsolete APM API v1 powermngt_set_wakeupdevices().
 *
 *  @param wupd A variable with wake-up devices (combination of @ref PM_WAKE_DEVICE_0_EMPTY "PM_WAKE_DEVICE_xxx") to set.
 *
 *  @return If the function succeeds, the return value is @ref APMCONFIG_SUCCESS (zero).
 *  If the function fails, the return value is one of @ref APMCONFIG_ERROR "APMCONFIG_ERROR_xxx" (non-zero).
 */
int powermngt_SetWakeUpDevices(unsigned int wupd);


/** @ingroup REF_MISC
 *  Gets the Standby MCU firmware version.
 *  @n Function is supported in: APM API v2.
 *
 *  @param mcu_fw_version The buffer to store the Standby MCU firmware version.
 *
 *  @return If the function succeeds, the return value is @ref APMCONFIG_SUCCESS (zero).
 *  If the function fails, the return value is one of @ref APMCONFIG_ERROR "APMCONFIG_ERROR_xxx" (non-zero).
 */
int powermngt_GetStandbyMCUfwVersion(unsigned char mcu_fw_version[APMCONFIG_STANDBY_MCU_FW_VER_BUF_SIZE]);


/*
 * Some additional functions to structure svc_power_apm_config processing.
 */


/** @ingroup REF_APMCONFIG_CONFIG
 *  Zeroes the specified members of svc_power_apm_config structure.
 *  Function will set to zero all members in accordance with @ref APMCONFIG_M_0_EMPTY "APMCONFIG_M_xxx" flags specified in
 *  @ref svc_power_apm_config.item_mask.
 *  @n Function is supported in: APM API v2.
 *
 *  @param cfg_ptr A pointer to the svc_power_apm_config structure.
 *
 *  @return If the function succeeds, the return value is @ref APMCONFIG_SUCCESS (zero).
 *  If the function fails, the return value is one of @ref APMCONFIG_ERROR "APMCONFIG_ERROR_xxx" (non-zero).
 */
int powermngt_CfgStruct_ZeroMasked(struct svc_power_apm_config *cfg_ptr);


/** @ingroup REF_APMCONFIG_CONFIG
 *  Returns the address and additional information of svc_power_apm_config structure member.
 *  @n Function is supported in: APM API v2. Main purpose of this function is for testing and debugging.
 *
 *  @param cfg_ptr A pointer to the svc_power_apm_config structure. Can be NULL.
 *  @param flag One of the @ref APMCONFIG_M_0_EMPTY "APMCONFIG_M_xxx" flags.
 *  @param flag_name A pointer to the buffer to store flag name. Can be NULL.
 *  @param flag_name_size The size of the buffer pointed by @p flag_name. Ignored if @p flag_name is NULL.
 *  @param member_name A pointer to the buffer to store structure member name.  Can be NULL.
 *  @param member_name_size The size of the buffer pointed by @p member_name. Ignored if @p member_name is NULL.
 *
 *  @return If the function succeeds, the return value is the address of the structure member. In case if @p cfg_ptr is NULL,
 *  function return just some non-zero value that cannot be used as address.
 *  If the function fails (member not exist), the return value is NULL.
 */
unsigned int *powermngt_CfgStruct_GetMemberByFlag(struct svc_power_apm_config *cfg_ptr, unsigned int flag,
        char *flag_name, unsigned int flag_name_size, char *member_name,unsigned int member_name_size);



/*
 * Obsolete and deprecated functions
 */

#ifdef ENABLE_PMV1_DEPRECATED_FUNCTIONS
int powermngt_get_config(struct apm_config *apm);
int powermngt_set_config(struct apm_config *apm);
int powermngt_Susspend(void);
int powermngt_get_sufficient_battery(unsigned int *suff_bat_level_ptr);
int powermngt_declare_state(int proc_id, int state);
int powermngt_SetShutDownTime(unsigned int time_to_shutdown);
int powermngt_SetWakeupTime(struct svc_power_DateTime *utildt);
int powermngt_get_wakeupdevices(unsigned int *wupd);
int powermngt_set_wakeupdevices(unsigned int wupd);
#endif // ENABLE_PMV1_DEPRECATED_FUNCTIONS



#ifdef __cplusplus
}
#endif

#endif /* SVC_POWER_MGMT_H */

