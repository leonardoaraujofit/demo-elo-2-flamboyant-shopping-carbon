/**
 *@file svc_mdb.h
 */


#ifndef SVC_MDB_H
#define SVC_MDB_H

#include "svcmgrSvcDef.h"

#ifdef __cplusplus
extern "C" {
#endif

/*SVC_SERVICE:mdb*/

/*SVC_STRUCT*/
/**
 * MDB configuration structure
 */
struct MdbConfig {
  int MdbOn;			/**< 1 if MDB mode is on, 0 if MDB mode is off (needs to be ON for this service to work).  */
  int AutoACK;			/**< 1 for MCU to Auto ACK all MDB commands from VMC, 0 for no auto-ACKing */
  int AutoSetup;		/**<(CURRENTLY NOT IMPLEMENTED IN MCU)1 for MCU to send MDB device setup response automatically. 0 waits for V/OS to respond. */
  int AutoReset;		/**< 1 for MCU to respond to MDB Host RESET command automatically when V/OS is in sleep, 0 otherwise. */
};

#define MDB_CMD_SIZE 							36
#define MDB_WRITE_CMD_SIZE (MDB_CMD_SIZE -1)
#define MDB_FTL_BYTES_PER_BLOCK        			31U
#define MDB_FTL_LAST_SESSION_FLAG  				0x02
#define MDB_FTL_MAX_BLOCKS_PER_SESSION     255UL

/*SVC_STRUCT*/
/**
 * MDB command data structure
 */
struct MdbCommand {
  unsigned char cmd [MDB_CMD_SIZE];   /**< MDB address + data + checksum */
  unsigned int len;         /**< real length of cmd */
};

#define MDB_ADR_CARD_READER 0x10
#define MDB_ADR_CASHLESS_2  0x60


#define MDB_UNKNOWN                -1
#define MDB_NO_COMMAND              0
#define MDB_RESET                   1
#define MDB_SETUP_CONFDATA          2
#define MDB_IDENT                   3
#define MDB_DISABLE                 4
#define MDB_ENABLE                  5
#define MDB_VEND_REQUEST            6
#define MDB_VEND_CANCEL             7
#define MDB_VEND_SUCCESS            8
#define MDB_VEND_FAILURE            9
#define MDB_SESSION_COMPLETE       10
#define MDB_VEND_REQUEST_NEG       11 
#define MDB_VEND_CASH_SALE         12
#define MDB_SETUP_PRICE            13
#define MDB_REVALUE_LIMIT_REQ      14
#define MDB_REVALUE_REQ            15
#define MDB_EXP_DIAG               16
#define MDB_SET_DATE_TIME          17
#define MDB_ENABLE_OPT             18
#define MDB_CANCEL                 19
#define MDB_FTLR_REQ_TO_RCV        20
#define MDB_FTLR_RETRY_DENY        21
#define MDB_FTLR_SEND_BLOCK        22
#define MDB_FTLR_OK_TO_SEND        23
#define MDB_FTLR_REQ_TO_SEND       24
#define MDB_DATA_ENTRY_RESPONSE    25


#define MDB_OPTFEAT_FTL               0x01
#define MDB_OPTFEAT_32BIT             0x02
#define MDB_OPTFEAT_MULTICUR          0x04
#define MDB_OPTFEAT_NEGVEND           0x08
#define MDB_OPTFEAT_DATAENTRY         0x10
#define MDB_OPTFEAT_SELECTIONFIRST    0x20

#define MDB_CONF_OPTIONS_REFUND       0x01
#define MDB_CONF_OPTIONS_MULTIVEND    0x02
#define MDB_CONF_OPTIONS_DISPLAY      0x04
#define MDB_CONF_OPTIONS_VENDCASHSALE 0x08


/* MDB states for out of sequence command */
#define MDB_STATE_NONE              0
#define MDB_STATE_INACTIVE          1
#define MDB_STATE_DISABLE           2
#define MDB_STATE_ENABLE            3
#define MDB_STATE_IDLE              4
#define MDB_STATE_VEND              5
#define MDB_STATE_REVALUE           6
#define MDB_STATE_NEGATIVE_VEND     7

/*Card Options*/
#define MDB_CARDOPT_DISPLAY           0x00
#define MDB_CARDOPT_NO_DISPLAY        0x01
#define MDB_CARDOPT_NO_REFUND         0x00
#define MDB_CARDOPT_REFUND            0x02
#define MDB_CARDOPT_NO_REVALUE        0x00
#define MDB_CARDOPT_REVALUE           0x04

/** Initialize MDB devices (Standby MCU and UART).
 * 
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_setupDevices(void);


/** Releases all MDB device resources in V/OS (Standby MCU and UART).
 * 
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_closeDevices(void);


/** Try to get the next available MDB command from MDB buffer.
 * 
 * @return mdb_command struct on data available, NULL if receive buffer is empty
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ struct MdbCommand * mdb_receive(void);

/** Try to peek the next available MDB command from MDB buffer without advancing the mdb buffer pointer to the next availble command
 * 
 * @return mdb_command struct on data available, NULL if receive buffer is empty
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ struct MdbCommand * mdb_peek(void);

/** Advance the mdb_buffer pointer to the next available command
 * 
 * Notes:
 * This function should be called if we want to skip to the next availble command,
 * ex.. after calling mdb_peek which doesn't advance the buffer pointer
 * 
 */
/*SVC_PROTOTYPE*/ void mdb_advance_buffer_tail(void);

/** Obtain the version of the MDB service 
 * 
 * @return version struct defined in svcmgrSvcDef.h
 */
/*SVC_PROTOTYPE*/ struct version mdb_getVersion(void);


/** Obtain current MDB configuration from Standby MCU. The configuration
 *  is stored permanently in the Standby MCU and does not need to be set
 *  after a power fail.
 * 
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ struct MdbConfig* mdb_getConfig(void);


/** Set new MDB configuration in Standby MCU. The configuration
 *  is stored permanently in the Standby MCU and does not need to be set
 *  after a power fail.
 * 
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_setConfig(struct MdbConfig config);


/** Set the timing parameters for the MDB link. Unit: msec
 * @param [in] block_time The block time is the maximum allowed time between a command and the
 *  first byte of the response message.
 * @param [in] interbyte_time The inter-byte time is the maximum allowed time betwenn two
 *  consecutive bytes within one command or reply message.
 * 
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_setBusTimings(int block_time, int interbyte_time);

/** Allow to enable or disable the MDB link from application point of view.
 *  All incoming MDB requests are discarded.
 * @param [in] disabled 1 - shut down the MDB link, 0 - (re-)enable the MDB link.
 * 
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_setMdbLinkState(int disabled);

/** Returns the time elapsed since the last poll from VMC. This may be
 *  helpful to detect if the VMC is still operational. In battery operated
 *  systems the VMC powers off itself after some seconds without customer
 *  interaction.
 * 
 * @return value >= 0 on success (centisec), -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_getTimeSinceLastPoll(void);

/** Check if the system is polled by the vending machine.
 * 
 * @return 0 if polling is active, 1 if polling is inactive, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_checkPolling(int waittime);


/** Set the used MDB cashless device address.
 * @param [in] address Either MDB_ADR_CARD_READER or MDB_ADR_CASHLESS_2
 * 
 * @return 0 if polling is active, 1 if polling is inactive, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_setCardReaderAddress(int address /*MDB_ADR_CARD_READER*/);


/** Obtain the used MDB cashless device address.
 * @param [in] address Either MDB_ADR_CARD_READER or MDB_ADR_CASHLESS_2
 * 
 * @return Either MDB_ADR_CARD_READER or MDB_ADR_CASHLESS_2. -1 on error.
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_getCardReaderAddress(void);


/** Send MDB command JUST RESET to indicate that the Ux300 has been reset
 *  and has returned to inactive state.
 *  In case V/OS is in sleep mode and the AutoReset feature is enabled in
 *  MCU firmware (@see mdb_setConfig), the MCU will automatically respond
 *  with a JUST RESET command for the first poll command right after
 *  a WAKEUP from VMC has occured.
 * 
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_sendJustReset(void);


/** Send MDB character string to the MCU
 * @param [in] data The character string to be sent 
 * @param [in] length The length of the character string to be sent 
 * @param [in] timeout The time to wait for a response in milliseconds
 * 
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the parameters were unacceptable
 * @li EHOSTDOWN if part of the message was lost in transaction.
 * @li ETIMEDOUT if the request timed out.
 * @li EHOSTDOWN if unable to transfer the data to the MCU.
 * @li EBADF if the filedescriptor of the UART is not open.
 * 
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_send(unsigned char *data, unsigned int length, unsigned int timeout);


/** Flush the internal MDB service buffer
 * 
 * @return 0 on success, no other value will be returned
 * 
 * Notes:
 * errno is always set to 0
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_flush(void);



/** Returns the type of a MDB command
 * 
 * @param [in] cmd Pointer to command
 * @param [in] cmdLen Length of the command
 * 
 * @return command type
 * 
 * Notes:
 * errno is always set to 0
 * 
 */
/*SVC_PROTOTYPE*/ char mdb_cmdType(unsigned char *cmd, unsigned int cmdLen);

/** Sends a text to be showed on the diplay of a vending machine.
 *  The use of language specific characters may differ from the ASCII
 *  definitions and depend on the present vending machine controller.
 * 
 * @param [in] text Display text, 0-terminated ASCII-string, max length 32 bytes
 * @param [in] time Maximum time of text remaining on display (in seconds)
 * 
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_displayText(char* text, unsigned char time);


/** Clears display of a vending machine.
 * 
 * 
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_clearDisplay(void);


/** Send MDB command Reader Config Data to respond to the MDB command SETUP
 *  Configuration Data from the VMC.
 * 
 * @param [in] readerLevel MDB reader level
 * @param [in] countryCode Country/Currency Code (2 bytes packed BCD)
 * @param [in] scaleFactor Scale Factor
 * @param [in] decimalPlaces Decimal Places
 * @param [in] maxResponseTime Application Maximum Response Time (in seconds)
 * @param [in] miscOptions Miscellaneous Options (can be logically OR'd)
 *                  MDB_CONF_OPTIONS_REFUND The reader is capable of restoring funds
 *                                          to the payment media.
 *                  MDB_CONF_OPTIONS_MULTIVEND The reader is multivend capable.
 *                  MDB_CONF_OPTIONS_DISPLAY The reader does have its own display.
 *                  MDB_CONF_OPTIONS_VENDCASHSALE The reader does support the VEND/CASH SALE subcommand
 * @param [in] battery Battery Option Note: A battery flag indicates that our system is
 *                      capable of working in battery operation mode and has the ability to switch off
 *                      itself after each session. In this case battery operation is enabled if the 
 *                      VMC indicates an 8x feature level. This causes a change of the reader level 
 *                      parameter inot the equal battery operated level.
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_sendReaderConfigData(unsigned char readerLevel, 
                                unsigned char *countryCode, unsigned char scaleFactor,
                                unsigned char decimalPlaces, unsigned char maxResponseTime,
                                unsigned char miscOptions, int battery);


/** Sends MDB command Peripheral ID with peripheral ID information to respond to the 
 *  MDB command EXPANSION Request ID from the VMC.
 *  Configuration Data from the VMC.
 * 
 * @param [in] readerLevel MDB reader level
 * @param [in] manufCode Manufacturer Code, 3 Bytes ASCII
 * @param [in] modelNumber Model Number, 12 Bytes ASCII
 * @param [in] softwareVersion Software Version, 2 Bytes BCD coded
 * @param [in] serialNumber Factory assigned serial number
 * @param [in] optFeatures Optional feature bits (used at reader level 3, can be OR'd)
 *                  MDB_OPTFEAT_FTL File transport layer supported
 *                  MDB_OPTFEAT_32BIT 32 bit monetary format
 *                  MDB_OPTFEAT_MULTICUR Multi currency / multi lingual
 *                  MDB_OPTFEAT_DATAENTRY Data entry
 *                  MDB_OPTFEAT_NEGVEND Negative vend
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_sendPeripheralId(unsigned char readerLevel, unsigned char* manufCode, unsigned char* modelNumber,
                    unsigned char* softwareVersion, unsigned char* serialNumber, unsigned int optFeatures);
                    

/** Activate Optional Features in MDB driver.
 * 
 * @param [in] optFeatures Additional optional features (can be OR'd)
 *                  MDB_OPTFEAT_FTL File transport layer supported
 *                  MDB_OPTFEAT_32BIT 32 bit monetary format
 *                  MDB_OPTFEAT_MULTICUR Multi currency / multi lingual
 *                  MDB_OPTFEAT_DATAENTRY Data entry
 *                  MDB_OPTFEAT_NEGVEND Negative vend
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_setOptionalFeatures(unsigned int optFeatures);

/** Sends current system time  through the MDB command Diagnostics
 *  Response Date/Time to the VMC (only level 3 reader)
 * 
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_sendDateTime(void);

/** Send MDB command Time/date request to the VMC. This is used to retrieve
 *  the current date/time of the VMC.
 * 
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_sendDateTimeRequest(void);

/** Send the age verification status to the VMC
 * 
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_sendDravs(unsigned char ucFeatures);

/** Send the malfunction error
 * 
 * @param [in] error - error code
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_sendMalfunctionError(unsigned char error);

/** send Device Specific Diagnostics Data
 * 
 * @param [in] customData - data string
 * @param [in] customData_len - data string length
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * ( Data can be of any length <= MDB_WRITE_CMD_SIZE-1)
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_sendDiagnosticResponse(unsigned char * customData, int customData_len);

/** sends a data entry request response command
 *
 * @param [in] ucDataEntryLength - length of the expected data entry
 * @param [in] bRepeatedReq - flag indicating, if its a repeated request
 * @param [in] pcText - optional display text, NULL may be passed
 * @param [in] ucTime - diplay time, only used if pcText is not NULL
 * @return 0 on success, -1 on error
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li ENODEV unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_sendSendDataEntryRequestResp(unsigned char ucDataEntryLength, unsigned char bRepeatedReq,
                      const char *pcText, unsigned char ucTime);

/** sends a data entry cancel command
 *
 * @return 0 on success, -1 on error
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li ENODEV unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_sendDataEntryCancel(void);

/** sends a file transfer layer retry deny command
 *
 * @param [in] ucDestination - address of the VMC 
 * @param [in] ucWaittime -  A time delay that the sender should wait 
 * 							before trying to re-send the entire data file again
 * @param [in] bWithAck - should be 1
 * @return 0 on success, -1 on error
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li ENODEV unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_sendFTLRetryDeny(unsigned char ucDestination, unsigned char ucWaittime, unsigned char bWithAck);

/** send a block from file to VMC
 *
 * @param [in] ucDestination - address of the VMC 
 * @param [in] ucBlockNum -  block number
 * @param [in] ucDataLen - data length
 * @param [in] pucData - pointer to the data
 * @param [in] bWithAck - should be 1
 * @return 0 on success, -1 on error
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li ENODEV unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_sendFTLSendBlock(unsigned char ucDestination, unsigned char ucBlockNum, unsigned char ucDataLen, unsigned char *pucData, unsigned char bWithAck);

/** confirm to the VMC it is ok to send a file.
 *
 * @param [in] ucDestination - address of the VMC 
 * @return 0 on success, -1 on error
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li ENODEV unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_sendFTLOkToSend(unsigned char ucDestination);

/** send request to send a file to the VMC.
 *
 * @param [in] ucDestination - address of the VMC
 * @param [in] ucFileID - ID of the file
 * @param [in] ucMaxLength - Maximal length
 * @param [in] bLastSession - Control byte
 * @return 0 on success, -1 on error
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li ENODEV unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_sendFTLRequestToSend(unsigned char ucDestination, unsigned char ucFileID,
                    unsigned char ucMaxLength, unsigned char bLastSession);
									
																				
/** receives a file from the VMC.
 *
 * @param [in] ucSourceAddress - address of the source
 * @param [in] ucFileID - ID of the file
 * @param [in] pcFileName - name of the file
 * @param [in] ucMaxBlocks - maximum number of blocks,
 *								taken from the request command
 * @param [in] ucControlByte - control byte from the request cmd
 * @param [in] uiTimeout - max timeout between data blocks
 *							(in 100th of a second, max value 10000)
 * @return 0 on success, -1 on error
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li ENODEV unable to access stbymcu interface.
 * 
 */						
/*SVC_PROTOTYPE*/ int mdb_FTLReceiveFile(unsigned char ucSourceAddress, unsigned char ucFileID, char *pcFileName,  
								unsigned char ucMaxBlocks, unsigned char ucControlByte, unsigned int uiTimeout);

/** receives a file from the VMC.
 *
 * @param [in] ucSourceAddress - address of the source
 * @param [in] ucFileID - ID of the file
 * @param [in] pcFileName - name of the file
 *               (if NULL, data is stored in ucRecData)
 * @param [in] ucMaxBlocks - maximum number of blocks,
 *								taken from the request command
 * @param [in] ucControlByte - control byte from the request cmd
 * @param [in] uiTimeout - max timeout between data blocks
 *							(in 100th of a second, max value 10000)
 * @param [in] ucRecData - pointer, where to store received file
 * @param [in] uiRecDataLen- size of ucRecData
 * @return 0 on success, -1 on error
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li ENODEV unable to access stbymcu interface.
 * 
 */					
/*SVC_PROTOTYPE*/ int mdb_FTLReceiveFileExt(unsigned char ucSourceAddress, unsigned char ucFileID, 
												char *pcFileName,  unsigned char ucMaxBlocks, 
												unsigned char ucControlByte, unsigned int uiTimeout,
												unsigned char *ucRecData, unsigned int *uiRecDataLen);
												
/** respond to a request to receive file from the VMC.
 *
 * @param [in] ucDestination - address of the VMC
 * @param [in] ucFileID - ID of the file
 * @param [in] pcFileName - name of the file
 * @param [in] uiAddDataHeaderLen - additional data header length(can be 0)
 * @param [in] pucAddDataHeader - additional data header(can be NULL)
 * @param [in] uiAddDataTrailerLen - additional data trailer length((can be 0)
 * @param [in] pucAddDataTrailer - additional data trailer(can be NULL)
 * @param [in] ucMaxBlocks - maximum number of blocks
 * @param [in] ucControlByte - control byte
 * @return 0 on success, -1 on error
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li ENODEV unable to access stbymcu interface.
 * 
 */	
 									
/*SVC_PROTOTYPE*/ int mdb_FTLRespondToReceiveRequest(unsigned char ucDestination,
											unsigned char ucFileID,
											char *pcFileName,
											unsigned int uiAddDataHeaderLen,
											unsigned char *pucAddDataHeader,
											unsigned int uiAddDataTrailerLen,
											unsigned char *pucAddDataTrailer,
											unsigned char ucMaxBlocks,
											unsigned char ucControlByte);
										
/** send file to the VMC.
 *
 * @param [in] ucDestination - address of the destination to send the file to
 * @param [in] ucFileID - ID of the file
 * @param [in] pcFileName - name of the file
 * @param [in] uiAddDataHeaderLen - additional data header length(can be 0)
 * @param [in] pucAddDataHeader - additional data header(can be NULL)
 * @param [in] uiAddDataTrailerLen - additional data trailer length(can be 0)
 * @param [in] pucAddDataTrailer - additional data trailer(can be NULL)
 * @return 0 on success, -1 on error
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li ENODEV unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_FTLSendFile(unsigned char ucDestination, 
										unsigned char ucFileID, 
										char *pcFileName,
										unsigned int uiAddDataHeaderLen,
										unsigned char *pucAddDataHeader,
										unsigned int uiAddDataTrailerLen,
										unsigned char *pucAddDataTrailer);

/** Sends MDB command Begin Session to inform the VMC about the card amount and
 *  allow the patron to make a selection. this command informs about the card
 *  amount only. There is no guerantee that this amount can be debit from the card.
 *  MDB command EXPANSION Request ID from the VMC.
 *  Configuration Data from the VMC.
 * 
 * @param [in] readerLevel MDB reader level
 * @param [in] amount Funds (credit) available (scaled)
 * @param [in] paymentMediaId Payment media ID (4 bytes)
 * @param [in] paymentType Type of payment
 * @param [in] paymentData Payment Data (2 bytes)
 * @param [in] userLanguage User language (2 bytes ASCII)
 * @param [in] currencyCode User currency code per ISO 4217 (2 bytes packed BCD)
 * @param [in] cardOptions Card options (can be OR'd)
 *                  MDB_CARDOPT_DISPLAY The VMC displays the credit if it is programmed to do so
 *                  MDB_CARDOPT_NO_DISPLAY The VMC must not display the credit (user option)
 *                  MDB_CARDOPT_NO_REFUND The currently inserted patrons card has no refund capability
 *                  MDB_CARDOPT_REFUND The currently inserted patrons card has refund capability
 *                  MDB_CARDOPT_NO_REVALUE The currently inserted patrons card has no revalue capability
 *                  MDB_CARDOPT_REVALUE The currently inserted patrons card has revalue capability
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_sendBeginSession(unsigned char readerLevel, unsigned int amount,
                  unsigned char *paymentMediaId, unsigned char paymentType,
                  unsigned char *paymentData, unsigned char *userLanguage,
                  unsigned char *currencyCode, unsigned char cardOptions);


/** Send MDB command Session Cancel Request to request the VMC to cancel the session before the patrons selection
 * 
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_sendSessionCancelRequest(void);

/** Sends MDB command Vend Approved in response to the MDB command VEND Vend
 *  Request from the VMC to inform the VMC that the vend amount has been deducted
 *  from the users payment media. The VMC is allowed to dispense the product.
 *  Don't send this command if it is not sure to debit the amount.
 * 
 * @param [in] readerLevel MDB reader level
 * @param [in] amount (deducted) vend amount (scaled)
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_sendVendApproved(unsigned char readerLevel, unsigned int amount);

/** Send MDB command Vend Denied in response to the MDB command VEND Vend Request from the VMC
 *  to deny the approval for the patrons selection and tp prohibit the VMC to dispense any products.
 * 
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_sendVendDenied(void);

/** Sends the MDB command End Session to indicate that the payment media has been removed and our device
 *  has finished vend process and returned to the Enable state. End session is iisued in response to the 
 *  MDB command VEND Session Complete from the VMC.
 * 
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_sendEndSession(void);

/** Sends MDB command Command out of sequence to reject forbidden commands.
 *  This command can be used if Ux300 is unable to execute the received command.
 * 
 * @param [in] readerLevel MDB reader level
 * @param [in] state current state of the MDB state machine
 *             (used for reader level 2 or 3 only, may not be OR'd)
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_sendOutOfSequence(unsigned char readerLevel, unsigned char state);

/** Sends the MDB command Cancelled to respond to tghe MDB commdn READER Cancel from the VMC.
 * 
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_sendCancelled(void);

/** Sends the MDB command Revalue Approved in response to the MDB command REVALUE
 *  Revalue Request from the VMC to accept the revalue request. The reader has to
 *  add the requested value tlo the payment media balance. Only level 2 + 3 readers.
 * 
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_sendRevalueApproved(void);


/** Sends the MDB command Revalue Denied in response to the MDB command REVALUE
 *  Revalue Request from the VMC to deny the revalue request. Only level 2 + 3 readers.
 * 
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_sendRevalueDenied(void);


/** Sends MDB command Revalue Limit Amount tp respond to the MDB command
 *  REVALUE Revalue Limit Requested from the VMC. The Revalue Limit Value is the 
 *  amount the reader will accept. (see MDB specification)
 * 
 * @param [in] readerLevel MDB reader level
 * @param [in] Revalue limit value (scaled)
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to access stbymcu interface.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_sendRevalueLimitAmount(unsigned char readerLevel, unsigned int amount);

/** Stores information about the vending machine display. Normally, these
 *  information are sent by the vending machine in the setup config data command.
 * 
 * @param [in] width Number of characters per row (default 16)
 * @param [in] height Number of rows on display (default 2)
 * @param [in] dispInfo Type of display (see MDB spec)
 * @return 0 on success, -1 on error
 * Notes:
 * On error return, errno:
 * @li EINVAL if the parameters are invalid.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_setDisplayData(unsigned char width, unsigned char height, unsigned char dispInfo);

/** Use to find out if the system has gone into sleep since the function was last called
 * (or the machine has booted)
 * 
 * @return 0 on no wake since last call, 1 on wake occured
 * Notes:
 * On error return, errno:
 * @li EINVAL if the parameters are invalid.
 * 
 */
/*SVC_PROTOTYPE*/ int mdb_hasWoken(void);


#ifdef __cplusplus
}
#endif

#endif //SVC_MDB_H
