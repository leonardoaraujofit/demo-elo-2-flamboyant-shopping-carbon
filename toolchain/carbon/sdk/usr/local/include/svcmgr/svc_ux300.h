#ifndef SVC_UX300_H
#define SVC_UX300_H

#ifdef __cplusplus
extern "C" {
#endif

#include "svcmgrSvcDef.h"

/*SVC_SERVICE:ux300*/

// SECURITY stuff

// Use the same types/define as service security, include required type
// if security service header is not already included
#ifndef SVC_SECURITY_H

/**
 * @name Service Switch Actions
*/
/** \{ */
#define SERVICE_SWITCH_READ             0
#define SERVICE_SWITCH_LATCH            1
#define SERVICE_SWITCH_ARM              2
#define SERVICE_SWITCH_DISABLE_REBOOT   3
#define SERVICE_SWITCH_ENABLE_REBOOT    4
#define SERVICE_SWITCH_REBOOT_STATE     5
/** \} */

/**
 * @name Service Switch Responses
*/
/** \{ */
#define SERVICE_SWITCH_OFF              0
#define SERVICE_SWITCH_ON               1
/** \} */


/**
 * @name Service Switch Actions
*/
/** \{ */
#define SERVICE_SWITCH_READ             0
#define SERVICE_SWITCH_LATCH            1
#define SERVICE_SWITCH_ARM              2
#define SERVICE_SWITCH_DISABLE_REBOOT   3
#define SERVICE_SWITCH_ENABLE_REBOOT    4
#define SERVICE_SWITCH_REBOOT_STATE     5
/** \} */

/**
 * @name Service Switch Responses
*/
/** \{ */
#define SERVICE_SWITCH_OFF              0
#define SERVICE_SWITCH_ON               1
/** \} */

/*SVC_STRUCT*/
/**
 * Attack Status
 */
struct attackStatus {
  int isAttacked;		/**< 0 = False, != 0 True */
  int securityBarrierFlag;	/**< Security Barrier Flag value */
  int tamperStatusReg;		/**< Tamper Status Register */
};

/*SVC_STRUCT*/
/**
 * Single certificate used to pair Ux devices
 */
struct uxCertRecord {
  int level;                        /**< Certificate level,\n 0 = Root/Primary,\n 1 = CA/Customer, \n 2 = Enc */
  unsigned int certified_key_id;    /**< Certiffied key id */
  unsigned int certifying_key_id;   /**< Certifying key id */
  int is_valid;                        /**< 1 = Valid certificate, \n 0 = Invalid certificate */
};

/*SVC_STRUCT*/
/**
 * Collection of certificates used to pair Ux devices - Important certs must be freed!
 */
struct uxCertRecords {
  int certs_count;                 /**< Number of certificates */
  struct uxCertRecord *certs;      /**< certificates */
};

#endif

/** Obtain the version of the Ux300 service
 *
 * @return version struct defined in svcmgrSvcDef.h
*/
/*SVC_PROTOTYPE*/ struct version ux300_getVersion(void);

/** Get Ux300 Pairing status
 *
 * @return 0 = Unplugged, 1 = Pairing in progress, 2 = Pairing failed, 3 = Pairing success
 */
/*SVC_PROTOTYPE*/ int ux300_getPairingStatus(void);

/** Obtain Attack Status
 *
 * @return attackStatus structure containing SBF and Tamper Register
 */
/*SVC_PROTOTYPE*/ struct attackStatus ux300_getAttackStatus(void);

/** Transfer PIN from Ux200 to Ux300
 *
 * @return If >= 0 then Success; If = -1 then error, check errno:
 * @li ENOMEM when unable to malloc space MSR control variables.
 * @li EBUSY when the MSR device is busy.
 * @li EBADF when the file descriptor is bad.
*/
/*SVC_PROTOTYPE*/ int ux300_exportPIN( void );

/** Service Switch operations
* @param[in] action = Operation to perform - See: Service Switch Actions
* @return int 0 = Not Triggered, 1 = Triggered,  < 0 = Error, -4 = Feature not enabled
*/
/*SVC_PROTOTYPE*/ int ux300_serviceSwitch( int action );

/** Obtain Ux300  certificates list used to pair with UX devices
 *
 * @return uxCertRecords structure containing the list of certificates - .certs must be freed.
 */
/*SVC_PROTOTYPE*/ struct uxCertRecords ux300_security_getUxCertificates(void);

/******************************************************************************************/
// SVC UTILITY METHODS
/******************************************************************************************/
#ifndef SVC_SERVICE_UTILITY_H

#define     SVC_STATUS_SUCCESS                      0
#define     SVC_STATUS_COMMUNICATION_ERROR          1
#define     SVC_STATUS_MEM_ALLOC_FAILURE_ERROR      2
#define     SVC_STATUS_UNKNOWN_ERROR                3


#define UTILITY_MAX_LABEL_LEN	(32)
#define UTILITY_MAX_VALUE_LEN	(512)


/**
 * @name External Storage Types
*/
/** \{ */
#define EXTERNAL_STORAGE_USB            1    /**< USB Memory device */
#define EXTERNAL_STORAGE_MICRO_SD       2    /**< MicroSD Memory device */
/** \} */

#endif

/*SVC_STRUCT*/
/**
 * Return Install Status
 */
struct ux300_installStatus {
  int result;			/**< 0 = Install complete, No Reboot, 1 = Install complete, Reboot Required, < 0 Error */
  char msg[256];		/**< Install Error Message (only valid when result < 0) */
};


/*SVC_STRUCT*/
/**
 * Return getEnvFile
 */
struct ux300_utilityGetEnvFile {
  int  count;		   	 /**< >=0 - section/name exists in envFile, < 0 Error */
  char value[UTILITY_MAX_VALUE_LEN]; /**< environment variable for section/name (only valid when count > 0) */
};

/*SVC_STRUCT*/
/**
 * The contents of a single section of a Config (ini) file 
 */
struct ux300_utilityIniSection {
  char* section;		   /**< section name */
  struct pairlist entries; /**< name/value pairs under section */
};

/*SVC_STRUCT*/
/**
 * The commplete contents of a Config (ini) file 
 */
struct ux300_utilityIniContents {
  struct ux300_utilityIniSection* sections; /**< array of ini section data */
  int sections_count;		   		  /**< number of ini sections (array len) */
};


/*SVC_STRUCT*/
/**
 * Return information on a single external storage device
 */
struct ux300_utilityAnExternalStorage {
  int type;                     /**< Type of storage: 1=USB, 2=microSD, */
  char mountPoint[64];         /**< Mount point */
};

/*SVC_STRUCT*/
/**
 * Return information on all available external storage devices
 */
struct ux300_utilityExternalStorage {
  int storage_count; 
  struct ux300_utilityAnExternalStorage *storage;
};


/*SVC_STRUCT*/
/**
 * Replica of the linux structure tm in time.h
 */
  struct ux300_utilityDateTime {
    int tm_sec;     /**< seconds */
    int tm_min;     /**< minutes */
    int tm_hour;    /**< hours */
    int tm_mday;    /**< day of the mont */
    int tm_mon;     /**< month */
    int tm_year;    /**< year */
    int tm_wday;    /**< day of the week */
    int tm_yday;    /**< day in the year */
    int tm_isdst;   /**< daylight saving time */
};


  /*SVC_STRUCT*/
  /**
   * Return dumplog results
   */
    struct ux300_utilityDumpLogStatus {
  	int status;
  	char file_msg[512];	/**< Contains .tgz filename with pathing or Error message (errno != 0) */
  };


/** obtain service version
 *
 * @return struct version containing version.
 *
 *     <center>For XML interface, see xml:\ref ux300_utility_getVersion</center>
 */
/*SVC_PROTOTYPE*/ struct version ux300_utility_getVersion(void);

/** obtain system time
 *
 * @return malloced string containing system time or NULL on error.
 *
 * On NULL return, errno:
 * @li ENOMEM when unable to malloc space for return string.
 *
 *    <center>For XML interface, see xml:\ref ux300_utility_gettime</center>
 */
/*SVC_PROTOTYPE*/ char* ux300_utility_gettime(void);

/** Set system time and RTC
 * 
 * @param[in] time structure ux300_utilityDateTime
 *
 * @return 0 = Success, -1 = Failure
 *
 *    <center>For XML interface, see xml:\ref ux300_utility_settime</center>
 */
/*SVC_PROTOTYPE*/ int ux300_utility_settime(struct ux300_utilityDateTime *utildt);




/** Copy Linux time to RTC
 *
 * @return 0 = Success, -1 = Failure
 *
 */
/*SVC_PROTOTYPE*/ int ux300_utility_copyTime_linuxRTC(void);


/** request system reboots
 *
 * @return 0 upon successful request, or -1 if unable to complete request.
 *
 * On error return, errno:
 * @li ECHILD when unable to fork child process to handle reboot request.
 *
 *    <center>For XML interface, see xml:\ref ux300_utility_reboot</center>
 */
/*SVC_PROTOTYPE*/ int ux300_utility_reboot(void);


/** Obtain value for name in section within user config ini.
 *
 * @param[in] section of interest.
 * @param[in] name to get value for.
 * @return struct getEnvFile
 *
 * Notes:
 * On getEnvFile.count < 0 return, errno:
 * @li ENOENT when name entry not found in section specified in config ini file.
 *
 *    <center>For XML interface, see xml:\ref ux300_utility_getenvfile</center>
 */
/*SVC_PROTOTYPE*/ struct ux300_utilityGetEnvFile ux300_utility_getenvfile(char *section /*NULL*/, char *name /*NULL*/);

/** Obtain value for name in section within specified ini.
 *
 * @param[in] pathname full pathname of ini file, reverts to _getenvfile() when NULL.
 * @param[in] section of interest.
 * @param[in] name to get value for.
 * @return struct getEnvFile
 *
 * Notes:
 * On getEnvFile.count < 0 return, errno:
 * @li ENOENT when name entry not found in section specified in config ini file.
 *
 *    <center>For XML interface, see xml:\ref ux300_utility_getenvfile</center>
 */
/*SVC_PROTOTYPE*/ struct ux300_utilityGetEnvFile ux300_utility_getenvfilename(char* pathname /*NULL*/, char *section /*NULL*/, char *name /*NULL*/);


/** set value for name in section within user config ini.
 *
 * @param[in] section of interest.
 * @param[in] name to set value for.
 * @param[in] value to be set.
 * @return 0 upon successful request, or -1 if unable to complete request.
 *
 * Notes:
 * @li Section MUST be specified, if value specified, name MUST be specified
 * @li If name specified, and value == empty, this removes entry
 *
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to set name in section in env file.
 *
 *    <center>For XML interface, see xml:\ref ux300_utility_putenvfile</center>
 */
/*SVC_PROTOTYPE*/ int ux300_utility_putenvfile(char *section /*NULL*/, char *name /*NULL*/, char *value /*NULL*/);

/** set value for name in section within specified ini.
 *
 * @param[in] pathname full pathname of ini file, reverts to _putenvfile() when NULL.
 * @param[in] section of interest.
 * @param[in] name to set value for.
 * @param[in] value to be set.
 * @return 0 upon successful request, or -1 if unable to complete request.
 *
 * Notes:
 * @li Section MUST be specified, if value specified, name MUST be specified
 * @li If name specified, and value == empty, this removes entry
 *
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li EACCES unable to set name in section in env file.
 *
 *    <center>For XML interface, see xml:\ref ux300_utility_putenvfile</center>
 */
/*SVC_PROTOTYPE*/ int ux300_utility_putenvfilename(char* pathname /*NULL*/, char *section /*NULL*/, char *name /*NULL*/, char *value /*NULL*/);


/** Obtain value for name in section within user config ini.
 *
 * @param[in] pathname full pathname of ini file to retrieve contents.
 * @return struct utilityIniContents
 *
 * Notes:
 *
 * CAUTION: Make sure you call  ux300_utility_iniContents_release to release the memory allocated by this function
 * On error return, errno:
 * @li ENODATA when filename is NULL or empty.
 * @li ENOENT if filename not found or could not be opened.
 * @li ENOMEM if out of memory.
 * @li EFAULT when an internal error prevents successful completion.
 *
 */
/*SVC_PROTOTYPE*/ struct ux300_utilityIniContents ux300_utility_iniContents(char* pathname);


/** Release memory allocated by ux300_utility_iniContents
*
*IMPORTANT: Always invoke this function after using ux300_utility_iniContents
*
*/
/*SVC_PROTOTYPE*/ void ux300_utility_iniContents_release(struct ux300_utilityIniContents ic);


/** install file/package in to system.
 *
 *    <center>For XML interface, see xml:\ref ux300_utility_installfile</center>
 */
/*SVC_PROTOTYPE*/ struct ux300_installStatus ux300_utility_installfile(void);

/** Run Application
 * 
 * @param[in] app - Executable to start, set to NULL to exit System Mode and start default applications
 * @return installStatus - Install status code and error string (if appropriate)
 *
 * Notes:
 *  installStatus.result:
 *  @li 0  success
 *  @li <0  error (see installStatus.msg for explanation).
 *
 *    <center>For XML interface, see xml:\ref ux300_utility_installfile</center>
 */
/*SVC_PROTOTYPE*/ struct ux300_installStatus ux300_utility_runApplication(char *app);


/** External Storage
 * 
 * @return ux300_utility_externalStorage - Return a list of available external devices.
 *    
 */
/*SVC_PROTOTYPE*/ struct ux300_utilityExternalStorage ux300_utility_externalStorage(void);


/** gather log files and installed software versions to a .tgz
 * @param[in] options - sets the options for where/what content to .tgz
 *            0x00 - create .tgz in /tmp/ directory
 *            0x01 - create .tgz in external storage device (typically usbstor1)
 * @return If status == 0, return struct contains result .tgz filename including full path
 *         If error, status is set and return struct contains error message (cause):
 *         @li EINVAL - invalid options specified
 *         @li ENOENT - Unable to generate log dump .tgz file
 *         @li ENXIO - external storage device specified, but no external device found
 */
/*SVC_PROTOTYPE*/ struct ux300_utilityDumpLogStatus ux300_utility_dumplogs(int options/*0*/);



/******************************************************************************************/
// SVC SYSINFO METHODS
/******************************************************************************************/

#ifndef SVC_SERVICE_SYSINFO_H

/**
 * @name I/O Module Features
*/
/** \{ */
#define IOMOD_CTLS          0x00000001      /**< Contactless/RFID */
#define IOMOD_WIFI          0x00000002      /**< WiFi */
#define IOMOD_COM1          0x00000004      /**< COM 1 */
#define IOMOD_COM2          0x00000008      /**< COM 2 */
#define IOMOD_AUDIO         0x00000010      /**< Audio */
#define IOMOD_USB_OTG       0x00000020      /**< USB On-the-go */
#define IOMOD_USB_HOST      0x00000040      /**< USB Host */
#define IOMOD_POE           0x00000080      /**< Power Over Ethernet */
#define IOMOD_ETHERNET      0x00000100      /**< Ethernet */
#define IOMOD_BLUETOOTH     0x00000200      /**< Bluetooth */
#define IOMOD_GPRS          0x00000400      /**< GPRS */
#define IOMOD_TAILGATE      0x00000800      /**< IBM Tailgate */
/** \} */

/*SVC_STRUCT*/
/**
 * @name Module information structure
 */
struct module_s {
	unsigned int features;	    /**< Type of module attached (string) */
	char serialNumber[16];	    /**< S/N of module */
};

/**
 * @name Used to identify type of comm port interface (commType)
*/
/** \{ */
#define COMMTYPE_RS232   0	/**< RS232 Interface */
#define COMMTYPE_RS485   1	/**< RS485 Interface */
/** \} */

/**
 * @name Used to indicate connection state of comm port (connectionStatus)
*/
/** \{ */
#define COMMSTATUS_UNKNOWN       0	/**< Unknown connection state */
#define COMMSTATUS_CONNECTED     1	/**< Connected */
#define COMMSTATUS_DISCONNECTED  2	/**< Disconnected (no cable attached) */
/** \} */


/*SVC_STRUCT*/
/**
 * @name Comm Port Status structure - used by
 */
struct comm_s {
	short int available; /**< A 1 indicates the comm port is present (if not, other params in struct meaningless) */
	short int commType;	/**< Type of comm port interface (RS232/485) (see COMMTYPE_ defines) */
	short int connectionStatus; /**< Indicates if cable attached/present (some ports do not support this) (see COMMSTATUS_ defines) */
};

/**
 * @name Used to identify type of USB interface (usbType)
*/
/** \{ */
#define USBTYPE_NONE   0	/**< No Interface */
#define USBTYPE_DEVICE 1	/**< Device Interface */
#define USBTYPE_HOST   2	/**< Host Interface */
/** \} */


/**
 * @name Max string lengths for version, package/bundle name, and timestamp
*/
/** \{ */
#define MAX_VERSION_STRING_LENGTH       32
#define MAX_PACKAGE_NAME_LENGTH         32
#define MAX_USER_NAME_LENGTH            16
#define MAX_SIGNER_NAME_LENGTH          8
#define MAX_PACKAGE_TYPE_LENGTH         24
#define MAX_TIMESTAMP_STRING_LENGTH     32
#define MAX_PACKAGE_DATE_LEN            16
#define MAX_PACKAGE_CATEGORY_LEN        24
#define MAX_PACKAGE_OPTION_LEN          80

/** \} */


/*SVC_STRUCT*/
/**
 * @name Cable Configuration structure
 */
struct cable_s {
    short int multiPort; /**< 1 = Multiport cable connected, 0 = Not connected */
	short int usbType;	/**< is USB port available, and if so which one (see USBTYPE_ defines) */
	short int ethernet;	/**< is Ethernet port available on cable (0=NO, 1=YES) */
	struct comm_s port1;	/**< configuration of comm port1 */
	struct comm_s port2;	/**< configuration of comm port2 */
	struct comm_s port3;	/**< configuration of comm port3 */
};



/*SVC_STRUCT*/
/**
 * @name Memory Usage structure
 */
struct memory_s {
	unsigned long int total; /**< total amount of memory available */
	unsigned long int free;  /**< total free memory */
	unsigned long int used;  /**< total used memory */
};

/*SVC_STRUCT*/
/**
 *  @name Memory Usage Information structure
 */
struct memoryUsage_s {
	struct memory_s ram; /**< RAM memory usage */
	struct memory_s flash; /**< FLASH memory usage */
};


/*SVC_STRUCT*/
/**
 *  @name Platform Information structure
 */
struct platform_s  {
	char model[8];  /**< Platform model (Mx915, Mx925,...) */
	char serialNumber[16];  /**< Serial number of unit (xxx-xxx-xxx) */
	char unitID[12];  /**< Unit ID */
	char MAC[16];  /**< Units MAC address (aabbccddeeff) */
	struct module_s ioModule;  /**< IO Module feature and S/N */
	int displayPixelX;  /**< Display pixel width */
	int displayPixelY;  /**< Display pixel height */
	struct memoryUsage_s memoryUsage; /**< memory usage */
	struct cable_s cable;  /**< Attached cable information */
};


/*SVC_STRUCT*/
/**
 *  @name bundle information structure
 */
struct bundleInfo_s {
    char name[MAX_PACKAGE_NAME_LENGTH]; /**< name of bundle */
    char version[MAX_VERSION_STRING_LENGTH]; /**< version of bundle */
    char user[MAX_USER_NAME_LENGTH]; /**< user assigned to bundle (root, sys1...sys8, usr1...usr8 */
    char category[MAX_PACKAGE_CATEGORY_LEN];
    char date[MAX_PACKAGE_DATE_LEN];
    char option[MAX_PACKAGE_OPTION_LEN];
};

/*SVC_STRUCT*/
/**
 *  @name bundle collection
 */
struct bundleCollection_s {
    int size; /**< Size of bundleCollection_s structure */
    int bundles_count; /**< number of bundles */
    struct bundleInfo_s *bundles; /**< bundles */
};


/*SVC_STRUCT*/
/**
 *  @name package information extended structure
 */
struct packageInfoExtended_s {
	char name[MAX_PACKAGE_NAME_LENGTH]; /**< name of package */
	char version[MAX_VERSION_STRING_LENGTH]; /**< version of package */
	char user[MAX_USER_NAME_LENGTH]; /**< user assigned to package (root, sys1...sys8, usr1...usr8 */
	char signer[MAX_SIGNER_NAME_LENGTH]; /**< signer of package (os, sys, user) */
	char packageType[MAX_PACKAGE_TYPE_LENGTH]; /**< type of package (os, user, userflash, config, service, ...) */
	char timestamp[MAX_TIMESTAMP_STRING_LENGTH]; /**< timestamp of package */
    char bundleName[MAX_PACKAGE_NAME_LENGTH];   /**< bundle name */
    char bundleVersion[MAX_VERSION_STRING_LENGTH];  /** Bundle version */
    char bundleDate[MAX_PACKAGE_TYPE_LENGTH ];	    /** Bundle date */
};

/*SVC_STRUCT*/
/**
 *  @name package collection extended
 */
struct packageCollectionExtended_s {
    int size;           /**< Size of packageInfoExtended_s */
    int packages_count; /**< number of packages */
    struct packageInfoExtended_s *packages; /**< packages */
};

/*SVC_STRUCT*/
/**
 *  @name package information structure
 */
struct packageInfo_s {
    char name[MAX_PACKAGE_NAME_LENGTH]; /**< name of package */
    char version[MAX_VERSION_STRING_LENGTH]; /**< version of package */
    char user[MAX_USER_NAME_LENGTH]; /**< user assigned to package (root, sys1...sys8, usr1...usr8 */
    char signer[MAX_SIGNER_NAME_LENGTH]; /**< signer of package (os, sys, user) */
    char packageType[MAX_PACKAGE_TYPE_LENGTH]; /**< type of package (os, user, userflash, config, service, ...) */
    char timestamp[MAX_TIMESTAMP_STRING_LENGTH]; /**< timestamp of package */
};

/*SVC_STRUCT*/
/**
 *  @name package collection
 */
struct packageCollection_s {
    int packages_count; /**< number of packages */
    struct packageInfo_s *packages; /**< packages */
};


/*SVC_STRUCT*/
/**
 *  @name OS software version structure
 */
struct softwareVersion_s {
	char sbi_version[MAX_VERSION_STRING_LENGTH]; /**< Secure Boot Image version */
	char vault_version[MAX_VERSION_STRING_LENGTH]; /**< vault version */
	char uboot_version[MAX_VERSION_STRING_LENGTH]; /**< U-Boot version */
	char cib_version[MAX_VERSION_STRING_LENGTH]; /**< Control Information Block version */
	char mib_version[MAX_VERSION_STRING_LENGTH]; /**< Machine Information Block version */
	char kernel_version[MAX_VERSION_STRING_LENGTH]; /**< linux Kernel version */
	char rfs_version[MAX_VERSION_STRING_LENGTH]; /**< Overall RFS version */
	char release_version[MAX_VERSION_STRING_LENGTH]; /**< Build Release version */
	char rfsSecurity_version[MAX_VERSION_STRING_LENGTH]; /**< Application Manager version */
};

/*SVC_STRUCT*/
/**
 *  @name OS software version structure
 */
struct softwareVersionExtended_s {
    char sbi_version[MAX_VERSION_STRING_LENGTH]; /**< Secure Boot Image version */
    char vault_version[MAX_VERSION_STRING_LENGTH]; /**< vault version */
    char uboot_version[MAX_VERSION_STRING_LENGTH]; /**< U-Boot version */
    char cib_version[MAX_VERSION_STRING_LENGTH]; /**< Control Information Block version */
    char mib_version[MAX_VERSION_STRING_LENGTH]; /**< Machine Information Block version */
    char kernel_version[MAX_VERSION_STRING_LENGTH]; /**< linux Kernel version */
    char rfs_version[MAX_VERSION_STRING_LENGTH]; /**< Overall RFS version */
    char release_version[MAX_VERSION_STRING_LENGTH]; /**< Build Release version */
    char rfsSecurity_version[MAX_VERSION_STRING_LENGTH]; /**< Application Manager version */
    char sred_version[MAX_VERSION_STRING_LENGTH]; /**< SRED version */
    char openprotocol_version[MAX_VERSION_STRING_LENGTH]; /**< OpenProtocol version */
};

/*SVC_STRUCT*/
/**
 *  @name MIB Information structure
 */
struct MIBInfo_s {
    char partNumber[MAX_VERSION_STRING_LENGTH];  /**< Hardware P/N */
    char hwRevision[MAX_VERSION_STRING_LENGTH];  /**< Hardware Revision */
    char chipRevision[MAX_VERSION_STRING_LENGTH]; /**< SOC Revision */
};


/*SVC_STRUCT*/
/**
 *  @name Internal battery status structure
 */
struct batteryStatus
{
    unsigned long int voltage;  /**< in units of 0.1v */
    unsigned long int status;   /**< 1 = OK (above 2.5v), 0 = BAD (below 2.5v) */
};

#endif

/** obtain service version
 *
 * @return struct version containing version.
 *
 *     <center>For XML interface, see xml:\ref sysinfo_getVersion</center>
 */
/*SVC_PROTOTYPE*/ struct version ux300_sysinfo_getVersion(void);


/** obtain memory usage information
 *
 * @return struct memory_usage
 */
/*SVC_PROTOTYPE*/ struct memoryUsage_s ux300_sysinfo_memoryUsage(void);


/** obtain platform information
 *
 * @return struct platform
 */
/*SVC_PROTOTYPE*/ struct platform_s ux300_sysinfo_platform(void);


/** obtain OS software version information
 *
 * @return struct softwareVersion_s
 */
/*SVC_PROTOTYPE*/ struct softwareVersion_s ux300_sysinfo_software(void);

/** obtain extended OS software version information
 * sysinfo_softwareExtended is a super set of sysinfo_software but it can grow if needed.
 * IMPORTANT: Returned structure pointer to softwareVerionExtended_s must be freed
 *
 * @return struct SoftwareVersionExtended_s
 */
// IMPORTANT: New modules must be added to the END of the structure!
/*SVC_PROTOTYPE*/ struct softwareVersionExtended_s *ux300_sysinfo_softwareExtended(void);

/** obtain all extended package information
 *
 * @return packageCollectionExtended_s
 */
/*SVC_PROTOTYPE*/ struct packageCollectionExtended_s ux300_sysinfo_packagesExtended(char *signer /*NULL*/);

/** obtain base RFS software package extended information
 *
 * @return packageCollectionExtended_s
 */
/*SVC_PROTOTYPE*/ struct packageCollectionExtended_s ux300_sysinfo_rfsPackagesExtended(void);

/** obtain system software package extended information
 *
 * @return packageCollectionExtended_s
 */
/*SVC_PROTOTYPE*/ struct packageCollectionExtended_s ux300_sysinfo_sysPackagesExtended(void);

/** obtain Application software package extended information
 *
 * @return packageCollectionExtended_s
 */
/*SVC_PROTOTYPE*/ struct packageCollectionExtended_s ux300_sysinfo_appPackagesExtended(void);

/** obtain all package information
 *
 * @return packageCollection_s
 */
/*SVC_PROTOTYPE*/ struct packageCollection_s ux300_sysinfo_packages(char *signer /*NULL*/);

/** obtain base RFS software package information
 *
 * @return packageCollection_s
 */
/*SVC_PROTOTYPE*/ struct packageCollection_s ux300_sysinfo_rfsPackages(void);

/** obtain system software package information
 *
 * @return packageCollection_s
 */
/*SVC_PROTOTYPE*/ struct packageCollection_s ux300_sysinfo_sysPackages(void);

/** obtain Application software package information
 *
 * @return packageCollection_s
 */
/*SVC_PROTOTYPE*/ struct packageCollection_s ux300_sysinfo_appPackages(void);

/** obtain all software bundle information
 *
 * @return bundleCollection_s
 */
/*SVC_PROTOTYPE*/ struct bundleCollection_s ux300_sysinfo_bundles(char *user /*NULL*/);

/** obtain base RFS software bundle information
 *
 * @return bundleCollection_s
 */
/*SVC_PROTOTYPE*/ struct bundleCollection_s ux300_sysinfo_rfsBundles(void);

/** obtain system software bundle information
 *
 * @return bundleCollection_s
 */
/*SVC_PROTOTYPE*/ struct bundleCollection_s ux300_sysinfo_sysBundles(void);

/** obtain Application software bundle information
 *
 * @return bundleCollection_s
 */
/*SVC_PROTOTYPE*/ struct bundleCollection_s ux300_sysinfo_appBundles(void);


/** obtain internal battery status information
 *
 * @return batteryStatus
 */
/*SVC_PROTOTYPE*/struct batteryStatus ux300_sysinfo_batteryStatus(void);


/** obtain MIB (Manufacture Information Block)
 *
 * @return MIBInfo_s *
 */
/*SVC_PROTOTYPE*/struct MIBInfo_s *ux300_sysinfo_MIB(void);




/******************************************************************************************/
// SVC NETLOADER METHODS
/******************************************************************************************/


/*SVC_STRUCT*/
struct ux300_netloader_sendFileHandle {
    int entryID;    /**< entry ID used by netloader_sendFileGetEvent(), netloader_sendFileCancel(),... */
    char key[32];   /**< event key associated with the sendFile request */
};

/*SVC_STRUCT*/
struct ux300_netloader_sendFileEvent{
    int status;   /**< status of netloader_sendFile() (see defines above for value and meaning) */
    int data;   /**< data associated with status, if status = NETLOADER_SNDFILE_PROGRESS, data is % of file sent (0 - 100) */
};

/** get version of netloader service
 *
 * @return version struct containing version.
 *
 *     <center>For XML interface, see xml:\ref netloader_getVersion</center>
 */
/*SVC_PROTOTYPE*/ struct version ux300_netloader_getVersion(void);


/** start netloader (listen on socket for download request)
 *
 * @param[in] delay seconds before broadcasting presence and waiting for
 *            incoming download request(s).
 * @return 0 upon successful request, or -1 if unable to complete request.
 *
 * On error return, errno:
 * @li EINVAL when passed delay value in invalid (less than zero).
 *
 *     <center>For XML interface, see xml:\ref netloader_start</center>
 */
/*SVC_PROTOTYPE*/ int ux300_netloader_start(int delay/*0*/);


/** stop netloader (stop listening on socket for download request)
 *
 * @return 0 upon successful request, or -1 if unable to complete request.
 *
 *     <center>For XML interface, see xml:\ref netloader_stop</center>
 */
/*SVC_PROTOTYPE*/ int ux300_netloader_stop(void);



/** Used to send a file using the netloader protocol via the netloader server
 *   running at ip=ip on port=port.
 *    This method schedules for the file to be sent and returns immediately.
 *    The caller should use netloader_sendFileGetEvent() to get status/success/failure, etc.
 *
 * @param[in] sourcefile - file with pathing to be downloaded/sent
 * @param[in] destfile - file w/pathing for remote end (where to place and what to name at download side).
 *                       If destfile NULL or empty, sourcefile used with pathing removed for destination filename.
 *		Rules for destfile:
 *			1. The default destination directory is set to: /mnt/flash/install/dl/ (regardless of any pathing specified in destfile).
 *			2. /tmp/ if specified in destfile, this overrides Rule 1. No install operation is performed for this destination.
 *			3. If on Ux200, and sourcefile starts with "4media", destination directory is set to: /mnt/mediaSOC/dnld/
 *			4. If environment variable *LOAD2HOME set, destination directory is set to: /home/sys2/flash
 *
 * @param[in] server - server IP address or name of netloader server to receive file (xxx.xxx.xxx.xxx)
 * @param[in] port - port number where netloader server is waiting for connection (if 0, default netloader port used)
 * @param[in] fileMask - mask to set file (chmod), if <= 0, default to 644
 * @param[in] progressEventInterval - when non-zero during the file transfer, events will be sent indicating progress.
 *                                    value indicates % interval for event (example, value = 1, send event every 1%)
 *                                    currently, only 1% (value < 10) and 10% (value >= 10) are used.
 *                                    See netloader_sendFileGetEvent() for how to receive the events.
 *
 * @return struct netloader_sendFileHandle. If errno != 0, then error, errno:
 * @li  ENOMEM - not enough memory to make request/send file
 * @li  EINVAL - unable to stat filename (not found or no permissions)
 * @li  EBADF - filename empty or not a regular file (directory,...)
 * @li  EMFILE - too many sendFile threads active (can only run up to 16 at a given time), use netloader_sendFileCancelAll() to cleanup
 * @li  EFBIG - file too large for remote terminal (not enough space)
 * @li  ENXIO - unable to connect to remote server
 * @li  EPIPE - connection closed by server (before send completed)
 * @li  EIO - read/write to server failed (connection broken, ...)
 */
/*SVC_PROTOTYPE*/ struct ux300_netloader_sendFileHandle ux300_netloader_sendFile(char *sourcefile, char *destfile, char *server, int port, int fileMask, int progressEventInterval/*0*/);


/** Read a netloader sendFile event.
 *   This is used to get status and associated data during the netloader_sendFile() processing
 *
 * @param[in] handle = struct as returned by netloader_sendFile() used to get appropriate event
 * @param[in] flags = Get Event Flags - 0 = block, 1 = non-block, 2 = return Last event, 4 = cancel _getEvent() in progress
 * @return status,data - if errno == 0, return struct is valid and indicates success or failure of transfer and progress if enabled.
 *    If error (errno != 0), called failed for this reasons:
 *      @li  EINVAL - invalid key in handle
 *
 *    For status: NETLOADER_SNDFILE_CLIENT_ERR:
 *        data: contains errno indicating type of error encountered (see /var/log/messages for additional info)
 *               @li EBADF - Problem encountered while reading from filename
 *               @li EPIPE - Problem encountered while writing file data to server (via tcp/ip socket)
 *    For progress:  NETLOADER_SNDFILE_PROGRESS ( this event only occurs when progressEventInterval != 0 in netloader_sendFile())
 *        data: contains % of file transfered to connected netloader server (0 - 100)
 *    For state: NETLOADER_SNDFILE_STATE:
 *        data: contains indication of what state the file transfer is at. Current values are:
 *        		@li NETLOADER_SNDFILE_STATE_SENDING - Sending file in progress
 *        		@li NETLOADER_SNDFILE_STATE_PROCESSING - Transfer complete, other end processing
 *        		@li NETLOADER_SNDFILE_STATE_REBOOTING - Other end of transfer is rebooting
 *        		@li NETLOADER_SNDFILE_STATE_DONE - Other end has completed processing (reboot not performed)
 *
 *    Note: For small files, not all intervals for progress may be sent to the event due to minimum data packet size.
 */
/*SVC_PROTOTYPE*/ struct ux300_netloader_sendFileEvent ux300_netloader_sendFileGetEvent(struct ux300_netloader_sendFileHandle handle, int flags/*0*/);


/** Used to cancel all currently active file transfers
 *    See: netloader_sendFile()
 *
 * This method will cause all netloader_sendFile() transfers in progress to
 *  terminate. If the file has completed the transfer and is being installed,
 *  this will NOT be interrupted.
 *
 * @return If >= 0 then Success; If = -1 then error, errno:
 * @li  EBADF - one (or more) transfer processes could not be ended
 */
/*SVC_PROTOTYPE*/ int ux300_netloader_sendFileCancelAll(void);


/** Register a signal handler to be notified if the Ux200 to Ux300 link goes down
 * @param[in] pid = The process id to register, set to 0 to automatically use the callers PID
 * @param[in] signal = The signal (from signal.h) to send the process when the link goes down 
 * @return -1 = Error, >=0 = signal handler index
 */
/*SVC_PROTOTYPE*/ int ux300_registerSignal(int pid, int signal);


/** Remove a signal handler to be notified if the Ux200 to Ux300 link goes down
 * @param[in] pid = The process id to remove, ignored if index >=0, index must be set to -1 for pid to be used.
 * @param[in] index = Index returned from ux300_registerSignal().  Set index to -1 to use the pid
 * @return None
 */
/*SVC_PROTOTYPE*/ void ux300_removeSignal(int pid, int index);

#ifdef __cplusplus
}
#endif
#endif //SVC_MSR_H
