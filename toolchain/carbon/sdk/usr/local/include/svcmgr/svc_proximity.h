/**
 *@file svc_proximity.h
 */



#ifndef SVC_PROXIMITY_H
#define SVC_PROXIMITY_H

#include "svcmgrSvcDef.h"

#ifdef __cplusplus
extern "C" {
#endif

/*SVC_SERVICE:proximity*/

// defines
#define PROXIMITY_MODE_DISABLE         		0
#define PROXIMITY_MODE_PERIODIC        		1  // server thread will periodically fill event fifo with proximity average.
#define PROXIMITY_MODE_THRESHOLD       		2  // server thread will only fill event fifo when thresholds are crossed (once only).

#define USE_DEFAULT_THRESHOLD				(-1)  // use factory calibrated thresholds

#define MIN_POLL_INTERVAL              10 // ms, since sensor will go thro' proximity ADC cycle, and ambient ADC cycle..

#define MAX_UPPTHRESHOLD               1023
#define MAX_LOWTHRESHOLD               (1023 - 250)


/** Obtain the version of the proximity service 
 * 
 * @return version struct defined in svcmgrSvcDef.h
 */
/*SVC_PROTOTYPE*/ struct version proximity_getVersion(void);


/** The setMode API would enable and disable a service thread that would monitor the proximity.
 * Values for state
 * 0 = Disable monitor thread
 * 1 = Enable monitor thread, send an event every interval.   The interval is defined in milliseconds
 * 2 = Enable monitor thread, send an event when the threshold crosses (in both directions) the value specified by threshold.
 *     An event is sent when ADC value crosses a threshold, and also when it dips below the threshold. e.g. If uppthreshold
 *     is set to 700, an event is sent when ADC value goes above 700, and also when it subsequently drops below 700. Same for
 *     lowthreshold. 
 * 
 * @param[in] state PROXIMITY_MODE_DISABLE, PROXIMITY_MODE_PERIODIC, or PROXIMITY_MODE_THRESHOLD
 * @param[in] uppthreshold upper threshold (0 - 1023) if crossed, will send an event, or USE_DEFAULT_THRESHOLD for factory calibrated default.
 * @param[in] lowthreshold lower threshold (0 - 1023) if fall below, will send an event, or USE_DEFAULT_THRESHOLD for factory calibrated default.
 *            If either upper or lower threshold indicates to use default, both thresholds will be set to factory default. 
 *            If uppthreshold > MAX_UPPTHRESHOLD, uppthreshold will be set to MAX_UPPTHRESHOLD. If lowthreshold > MAX_LOWTHRESHOLD, 
 *            lowthreshold will be set to MAX_LOWTHRESHOLD.
 * @param[in] interval time in msec where proximity server thread will read proximity reading from sensor. If set too large, it
 *            is possible that a threshold crossing may be missed.
 *            If interval is < MIN_POLL_INTERVAL, interval will be set to MIN_POLL_INTERVAL.
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li ECHILD if unable to create server thread.
 * @li EACCES unable to access driver interface.
 *
 */
/*SVC_PROTOTYPE*/ int proximity_setMode(int state, int uppthreshold, int lowthreshold, int interval);



/** The GetModeSettings API retrieves the current operating mode, the threshold settings and interval value.
 *
 * @param[in] pstate storage for operating mode PROXIMITY_MODE_DISABLE, PROXIMITY_MODE_PERIODIC, or PROXIMITY_MODE_THRESHOLD.
 * @param[in] puppthreshold storage for upper threshold.
 * @param[in] plowthreshold storage for lower threshold.
 * @param[in] pinterval storage for interval time in msec.
 * @return 0 on success, -1 on error
 *
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li ENOMEM if unable to allocate memory.
 *
 */
/*SVC_PROTOTYPE*/ int proximity_GetModeSettings(int *pstate, int *puppthreshold, int *plowthreshold, int *pinterval);



/** Get RAW proximity sensor reading. This is ADC reading from sensor and ranges from 0 to 1023.
 * The reading is dependent on the reflectance of the object and its distance from the sensor, so
 * there is no direct correlation between object distance and the ADC value. e.g. The ADC reading
 * will be very different for a dark matt object vs a metallic object even at the same distance.
 * 
 * @return sensor reading on success, -1 on error 
 * 
 * Notes:
 * On error return, errno:
 * @li EFAULT return value invalid
 * @li EACCES unable to access proximity driver
 * @li ENOMEM unable to malloc return value space
 * 
 */
/*SVC_PROTOTYPE*/ int proximity_GetStatus(void);


/** Get running average of proximity sensor (ADC value average ranging from 0 to 1023)
 * See above note on ADC value vs distance for proximity_GetStatus.
 * @param[in] flag GETEVENT_NONBLOCK, GETEVENT_LAST, GETEVENT_CANCEL
 * @return averaged sensor value on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EFAULT data invalid.
 * 
 */
/*SVC_PROTOTYPE*/ int proximity_GetEvent(int flags);





#ifdef __cplusplus
}
#endif

#endif  //SVC_PROXIMITY_H
