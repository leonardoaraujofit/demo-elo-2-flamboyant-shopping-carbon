/** @addtogroup utils Utils */
/** @{ */
/** @addtogroup logmgrservice Utils - LogManager: Services */
/** @{ */
/**
 *  @file svc_logmgr.h
 *
 *  @brief   System logger management API interface
 *
 * 	This API allows to control system logger (currently syslog) behaviour by
 * 	changeing it's configuration values.
 *
 */

/*
*  Copyright, 2015 VeriFone Inc.
*  2099 Gateway Place, Suite 600
*  San Jose, CA.  95110
*
* All Rights Reserved. No part of this software may be reproduced,
* transmitted, transcribed, stored in a retrieval system, or
* translated into any language or computer language, in any form
* or by any means electronic, mechanical, magnetic, optical,
* chemical, manual or otherwise, without the prior permission of
* VeriFone Inc.
*
*/

#ifndef SVC_LOGMGR_H
#define SVC_LOGMGR_H

#ifdef __cplusplus
extern "C" {
#endif

#include <svcmgrSvcDef.h>

/*SVC_SERVICE:logmgr*/

#define LOGMGR_ENABLE		"enable"
#define LOGMGR_DISABLE		"disable"

#define LOGMGR_CHAN_UDP         "udp"
#define LOGMGR_CHAN_FILE        "file"

#define LOGMGR_LEVEL_ALL        "*"
#define LOGMGR_LEVEL_DEBUG      "debug"
#define LOGMGR_LEVEL_INFO       "info"
#define LOGMGR_LEVEL_NOTICE     "notice"
#define LOGMGR_LEVEL_WARNING    "warning"
#define LOGMGR_LEVEL_ERR        "err"
#define LOGMGR_LEVEL_CRIT       "crit"
#define LOGMGR_LEVEL_ALERT      "alert"
#define LOGMGR_LEVEL_EMERG      "emerg"
#define LOGMGR_LEVEL_PANIC      "panic"
#define LOGMGR_LEVEL_NONE       "none"


#define LOGMGR_CFG_PARAM_ENABLED		1
#define LOGMGR_CFG_PARAM_LOG_LEVEL		2
#define LOGMGR_CFG_PARAM_NUMBER_OF_FILES 	3
#define LOGMGR_CFG_PARAM_MAX_LOGFILE_SIZE 	4
#define LOGMGR_CFG_PARAM_DESTINATION 		5
#define LOGMGR_CFG_PARAM_FILE_NAME 		6
#define LOGMGR_CFG_PARAM_UDP_HOST 		7
#define LOGMGR_CFG_PARAM_UDP_PORT 		8
#define LOGMGR_CFG_PARAM_SERIAL_COM 		9
#define LOGMGR_CFG_PARAM_LOG_FORMAT 		10

#define LOGMGR_ERR_INVALID_TYPE			-1
#define LOGMGR_ERR_UNKNOWN_KEY 			-2
#define LOGMGR_ERR_INVALID_VALUE 		-3
#define LOGMGR_ERR_SYS_ERRNO 			-4

/* system errno value of the failed syscall is copied in here */
extern int sys_errno;


/** Obtain the version of the event service.
 *
 * @return 
 * Version struct defined in svcmgrSvcDef.h
 * 
 * @note
 * Every Service is required to support a set of core interface functions to provide
 * uniformity across Services. The Service precompiler checks for these functions
 * and outputs a compliance error when a function is missing.
 *
 * @note
 * Struct version {service}_getVersion(void).
 */
/*SVC_PROTOTYPE*/ 
struct version logmgr_getVersion(void);


/** Enable the logging on the specific channel
 * 
 * @param[in] channel The string, which specifies the name of the channel
 * @param[in] level The string, which specifies the level of the log to be enabled
 * @param[in] endpoint The parameter which specifies the address, in case
 * 		the UDP channel needs to be enabled. For "file" channel the
 *		parameter should be NULL
 *
 * @return 0 in case of success, -errno in case of any error, errno variable is
 *		also set to a specific value
 *
 *<b>Errno values</b>
 * @li EINVAL - Invalid parameter specified
 * @li EINTR  - Secure installer error occured when sending signal to syslogd
 * @li errors related to file open/read/write and connection to svcserver(logged)
 */
/*SVC_PROTOTYPE*/
int logmgr_enable(const char* channel, const char* level, const char* endpoint);


/** Disable the logging on the specific channel
 * 
 * @param[in] channel The string, which specifies the name of the channel
 *
 * @return 0 in case of success, -errno in case of any error, errno variable is
 *		also set to a specific value
 *
 *<b>Errno values</b>
 * @li EINVAL - Invalid parameter specified
 * @li errors related to file open/read/write and connection to svcserver(logged)
 */
/*SVC_PROTOTYPE*/
int logmgr_disable(const char* channel);


/**
 * @param[in] id_key The id of the configuration field to get as integer
 * @param[out] retval The return value containing pointer to integer value
 *
 * @return
 * @li 0  - Success
 * @li < 0 (negative) - Failure. Check return value.
 *
 * <b>Error return values</b>:
 * @li LOGMGR_ERR_UNKNOWN_KEY  - unknown or non existent key
 * @li LOGMGR_ERR_SYS_ERRNO    - error from system call, check sys_errno variable
 *                               for error code.
 */
/*SVC_PROTOTYPE*/
int logmgr_getConfigInt(const unsigned int id_key, int *const retval);


/**
 * @param[in] id_key The id of the configuration field to get in string format
 * @param[out] retval The return value containing pointer to integer value
 * @param[in] ret_size The size of the buffer to be returned
 *
 * @return
 * @li 0  - Success
 * @li < 0 (negative) - Failure. Check return value.
 *
 * <b>Error return values</b>:
 * @li LOGMGR_ERR_UNKNOWN_KEY  - unknown or non existent key
 * @li LOGMGR_ERR_SYS_ERRNO    - error from system call, check sys_errno variable
 *                               for error code.
 */
/*SVC_PROTOTYPE*/
int logmgr_getConfigStr(const unsigned int id_key, char * const ret_str, const int ret_size);


/**
 * @param[in] id_key The id of the configuration field to set in string format
 * @param[in] new_value The new integer value to be set to configuration
 *
 * @return
 * @li 0  - Success
 * @li < 0 (negative) - Failure. Check return value.
 *
 * <b>Errno values</b>:
 * @li LOGMGR_ERR_UNKNOWN_KEY   - unknown or non existent key
 * @li LOGMGR_ERR_INVALID_VALUE - value is not withing allowed range or
 *                                misformated
 */
/*SVC_PROTOTYPE*/
int logmgr_setConfigInt(const unsigned int id_key, const int new_value);


/**
 * @param[in] id_key The id of the configuration field to set in string format
 * @param[in] new_value The new string value to be set to configuration
 *
 * @return
 * @li 0  - Success
 * @li < 0 (negative) - Failure. Check return value.
 *
 * <b>Errno values</b>:
 * @li LOGMGR_ERR_UNKNOWN_KEY   - unknown or non existent key
 * @li LOGMGR_ERR_INVALID_VALUE - value is not withing allowed range or
 *                                misformated
 */
/*SVC_PROTOTYPE*/
int logmgr_setConfigStr(const unsigned int id_key, const char * const new_value);


/**
 * @return
 * @li 0  - Success
 * @li < 0 (negative) - Failure. Check return value.
 *
 * <b>Errno values</b>:
 * @li LOGMGR_ERR_SYS_ERRNO     - error from system call, check sys_errno variable
 *                                for error code.
 */
/*SVC_PROTOTYPE*/
int logmgr_applyConfig(void);


#ifdef __cplusplus
} /* close extern "C" */
#endif

#endif /* SVC_LOGMGR_H */
///@}
///@}
