/*
 *	Copyright, 2013 VeriFone Inc.
 *	2099 Gateway Place, Suite 600
 *	San Jose, CA.  95110
 *
 *  All Rights Reserved. No part of this software may be reproduced,
 *  transmitted, transcribed, stored in a retrieval system, or
 *  translated into any language or computer language, in any form
 *  or by any means electronic, mechanical, magnetic, optical,
 *  chemical, manual or otherwise, without the prior permission of
 *  VeriFone Inc.
 *
 *  NOTE:
 *    This is a private include file used/shared by:
 *      svc_sound.so
 *      svc_mplayer.so
 *      svc_sigcap.so
 *      mediaServer
 *
 */
#ifndef MEDIASERVER_H
#define MEDIASERVER_H

#ifdef __cplusplus
extern "C" {
#endif


#ifndef SEEK_SET
#define SEEK_SET 	0
#define SEEK_CUR 	1
#define SEEK_END 	2
#endif

#define DEFAULT_VIDEO_LAYER  1000  // evas layer where video object resides (if not specified)

#define FIFO_BASEDIR          "/tmp/svcmgr"
#define MEDIA_REQUEST_FIFO    "/tmp/svcmgr/media.request" // message command pipe to daemon
#define AUDIORESPONSE_FIFO    "/tmp/svcmgr/media.audioresponse" // audio message response pipe from daemon
#define AUDIOEVENT_FIFO       "/tmp/svcmgr/media.audioevent" // audio event response pipe from daemon
#define VIDEORESPONSE_FIFO    "/tmp/svcmgr/media.videoresponse" // video message response pipe from daemon
#define VIDEOEVENT_FIFO       "/tmp/svcmgr/media.videoevent" // video event response pipe from daemon


#define STARTAUDIO_CMD        "startAudio"      // start playing an audio file
#define PAUSEAUDIO_CMD        "pauseAudio"      // pause a playing audio
#define REPEATAUDIO_CMD       "repeatAudio"     // repeat a playing audio
#define RESUMEAUDIO_CMD       "resumeAudio"     // resume a paused audio
#define SEEKAUDIO_CMD         "seekAudio"       // seek to a location in audio
#define STOPAUDIO_CMD         "stopAudio"       // stop a playing/paused audio
#define STOPAUDIOALL_CMD      "stopAudioAll"    // stop all playing/paused audio
#define STARTVIDEO_CMD        "startVideo"      // start playing an video file
#define MULTIVIDEO_CMD        "multiVideo"      // start playing video using multiple files
#define PAUSEVIDEO_CMD        "pauseVideo"      // pause a playing video
#define REPEATVIDEO_CMD       "repeatVideo"     // repeat a playing video
#define RESUMEVIDEO_CMD       "resumeVideo"     // resume a paused video
#define SEEKVIDEO_CMD         "seekVideo"       // seek to a location in video
#define SOUNDVIDEO_CMD		  "soundVideo"      // disable/enable sound on a playing video
#define STOPVIDEO_CMD         "stopVideo"       // stop a playing/paused video
#define STOPVIDEOALL_CMD      "stopVideoAll"    // stop all playing/paused video
#define STARTBUZZER_CMD       "startBuzzer"     // start sounding the buzzer
#define DOUBLEBUFFER_CMD      "dblBuffer"       // enable/disable double buffering in fp video
#define EXIT_CMD              "exit"



// sound/mplayer client to daemon server struct (none play calls)
typedef struct {
	char command[32]; // must be first element in struct for use in union below
	int  handle; // reference to sound object (not all commands need/use this)
	int  state; // generic usage for state/options
	double offset; // used by seek and play
	int  whence; // used by seek and play
	// options if any added here
}commandStruct;


// sound/mplayer client to server struct (for play start)
typedef struct {
	char command[32]; // must be first element in struct for use in union below
	char filename[512];
	char eventkey[32]; // event key string
	int  eventflag; // used to indicate if we want to create/use events
	double offset; // used by seek and play
	int  whence; // used by seek and play
	int  repeat; // used to enable repeat (non-zero)
	int  layer; // layer to place video object on (0 == use default video layer (1000))
	int  x_origin; // pixel position for upper-left corner
	int  y_origin; // y pixel position for upper-left corner
	int  width; // width in pixels of video (0 - default to actual video width - w/o scaling)
	int  height; // height in pixels of video (0 - default to actual video height - w/o scaling)
}playCommandStruct;


// mplayer client to server struct (for multiplay start)
typedef struct {
	char command[32]; // must be first element in struct for use in union below
	char master_filename[128]; /* name of master file (full pathing) to be played (top-left) */
	char slave1_filename[128]; /* name of slave-1 file (full pathing) to be played (top-right) */
	char slave2_filename[128]; /* name of slave-2 file (full pathing) to be played (bottom-left) */
	char slave3_filename[128]; /* name of slave-3 file (full pathing) to be played (bottom-right) */
	char eventkey[32]; // event key string
	int  eventflag; // used to indicate if we want to create/use events
	int  repeat; // used to enable repeat (non-zero)
	int  layer; /* layer to place video evas object on (0 == use default video layer (1000)) */
	int  x_origin; /* x pixel position for upper-left corner */
	int  y_origin; /* y pixel position for upper-left corner */
} multiPlayCommandStruct;

// sound client to server struct for buzzer command request
typedef struct {
	char command[32]; // must be first element in struct for use in union below
	int frequency;
	int duration;
	int volume;
}buzzerCommandStruct;


// union of all request structs to media daemon
typedef union {
	commandStruct           command;
	playCommandStruct       playCommand;
	multiPlayCommandStruct  multiPlayCommand;
	buzzerCommandStruct     buzzerCommand;
}daemonCommandUnion;


typedef enum {
	OTHER,
	AUDIO,
	BUZZER,
	VIDEO
}MEDIATYPE;

typedef enum {
	NONE,
	SINGLE_VIDEO,
	MULTI_VIDEO
}SUBMEDIATYPE;


typedef struct {
	MEDIATYPE type;
	SUBMEDIATYPE subtype;
	daemonCommandUnion daemonCommand;
}commandRequestStruct;


// daemon to server response (all)
typedef struct {
	int status;   // error < 0, handle >= 0
	int errnoVal;
}responseStruct;

struct statusReturn {
	int status;		/**< Return state value */
	char msg[80];	/**< return message (may be empty) */
};

typedef struct {
	char eventkey[32];
	struct statusReturn statusReturn; // defined in svc_sound.h
}playEventStruct;


#define MEDIA_STATUS_END             1 /**< ended normally */
#define MEDIA_STATUS_PAUSE           2 /**< has been paused */
#define MEDIA_STATUS_RESUME          3 /**< has been resumed */
#define MEDIA_STATUS_TERMINATED      4 /**< terminated by _stop(), _stopall(), or another _play() */
#define MEDIA_STATUS_ERROR_IO        10 /**< invalid video file format/type or access error */
#define MEDIA_STATUS_ERROR_FILE_FMT  11 /**< invalid file format */


#ifdef __cplusplus
}
#endif

#endif //MEDIASERVER_H
