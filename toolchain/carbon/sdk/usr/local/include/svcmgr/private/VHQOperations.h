///
/// Copyright 2011 Verifone Inc.  All Rights Reserved.
///
/// This program is confidential, proprietary, and unpublished property of
/// Verifone Inc.  It may NOT be copied or distributed in part or in whole
/// on any medium, either electronic or printed, without the express written
/// consent of Verifone Inc.
///
/// Unpublished -- rights reserved under the copyright laws of the 
/// United States and other countries.
///
/// \file	VHQOperations.h
///	        Include file for VHQ operations
/// 
// comments that are not part of documentation begin with // instead of ///

#ifndef _VHQ_OPERATIONS_H
#define _VHQ_OPERATIONS_H

#include "VHQTypes.h"




typedef enum _operation_identifier_t
{
	OPERATION_DOWNLOAD_FILE,
	OPERATION_GET_DATA,
	OPERATION_SET_DATA,
	OPERATION_SET_SERVER_CERT,
	OPERATION_TERMINAL_ACTION,
	OPERATION_UPLOAD_FILE,
	OPERATION_GET_DIAG_DATA,
	OPERATION_REBOOT,
	OPERATION_RESET_POS,
	OPERATION_RESTART_APP,
	OPERATION_SET_CLOCK,
	OPERATION_TS_RECALIBRATION,
	OPERATION_APP_ACTION,
	OPERATION_DELETE_FILE,
	OPERATION_CANCEL_OP,
}operation_identifier_t;


typedef enum _srv_message_type_t
{
	SRV_MSG_HEARTBEAT,
	SRV_MSG_MAINTENANCE_HEARTBEAT,
	SRV_MSG_EVENT,
	SRV_MSG_USER,
	SRV_MSG_STATUS,
	SRV_MSG_AGENT_KEY_EXCHANGE,
	SRV_MSG_TYPE_UNKNOWN,
}srv_msg_type_t;

typedef enum _srv_msg_identifier_t
{
	/* Heartbeat Identifiers */
	SRV_MSG_HEARTBEAT_REGISTER,
	SRV_MSG_HEARTBEAT_ALIVE,
	SRV_MSG_HEARTBEAT_MAINTENANCE,
	SRV_MSG_HEARTBEAT_DIAGNOSTICS,
	SRV_MSG_HEARTBEAT_SOFTWARE_UPDATE,
	SRV_MSG_HEARTBEAT_CONTENT_UPDATE,
	SRV_MSG_HEARTBEAT_DEVICE_PROFILE_UPDATE,
	SRV_MSG_HEARTBEAT_DIAG_PROFILE_UPDATE,

	/* Event Identifiers */
	SRV_MSG_EVENT_TAMPER,

	/* User Msg identitfiers */
	SRV_MSG_USER_TBD,

	SRV_MSG_IDENTIFIER_UNKNOWN,
}srv_msg_identifier_t;


typedef enum _srv_op_type_t
{
	/* Heartbeat Identifiers */
	SRV_OP_TYPE_GET_DEV_PROFILE,
	SRV_OP_TYPE_SET_CLOCK,
	SRV_OP_TYPE_DL_FILE,
	SRV_OP_TYPE_GET_DIAG_DATA,
	SRV_OP_TYPE_GET_OS_LOGS,
	SRV_OP_TYPE_GET_OS_CONFIG_FILE,
	SRV_OP_TYPE_SCREEN_CAP,
	SRV_OP_TYPE_GET_USER_FILE_LIST,
	SRV_OP_TYPE_GET_USER_LOGS,
	SRV_OP_TYPE_REBOOT,
	SRV_OP_TYPE_RESTART_APPS,
	SRV_OP_TYPE_TS_RECAL,
	SRV_OP_TYPE_GET_LIB_DETAILS,
	SRV_OP_TYPE_CANCEL_OP_SET,
	SRV_OP_TYPE_MAINTENANCE,

	SRV_OP_TYPE_UNKNOWN,
}srv_op_type_t;



typedef enum _operation_handler_t
{
	OPERATION_HANDLER_TERMINAL,
}operation_handler_t;

#define DEFAULT_OP_HANDLER			OPERATION_HANDLER_TERMINAL

typedef enum _operation_on_error_t
{
	OP_ON_ERROR_CONTINUE,
	OP_ON_ERROR_SHOW_ERROR,
	OP_ON_ERROR_ABORT,
}operation_on_error_t;


#define DEFAULT_OP_ON_ERROR			OP_ON_ERROR_CONTINUE

typedef struct _operation_t
{
	operation_identifier_t		OpIdentifier;
	char						Version[16];
	char						OpId[64];
	operation_handler_t			OpHandler;
	operation_on_error_t		OpOnError;
	uint32						OpPreReq;
	time_t						OpTime;
	uint32						OpSetId;
	uint32						RecurId;
	void*						Content;
}operation_t;



typedef enum _term_action_t
{
	TERM_ACTION_NONE,
	TERM_ACTION_REBOOT,
	TERM_ACTION_RESET_POS,
	TERM_ACTION_RESTART_APP,
	TERM_ACTION_SET_CLOCK,
	TERM_ACTION_TS_RECALIBRATION,
}term_action_t;

typedef struct _term_act_content_t
{
	term_action_t			action;
	time_t					set_clock_time;
	operation_identifier_t	op_type;
}term_act_content_t;

typedef struct _term_act_parms
{
	operation_t operation;
	term_act_content_t term_act_content;
	int comm_id;
	int message_id;
	srv_msg_type_t srv_msg_type;
	srv_op_type_t srv_op_type;
}term_act_parms;




typedef enum _dl_file_method_t
{
	DL_METHOD_FTP,
	DL_METHOD_HTTP,
	DL_METHOD_WEB_SERVICE,
	DL_METHOD_GET,
}dl_file_method_t;


typedef enum _dl_file_dl_type_t
{
	DL_FILE_DL_TYPE_FILE = 1,
	DL_FILE_DL_TYPE_IPKG,
	DL_FILE_DL_TYPE_TGZ,
	DL_FILE_DL_TYPE_CONTENT,
	DL_FILE_DL_TYPE_CONTENT_BUNDLE_UNSIGNED,
	DL_FILE_DL_TYPE_CONTENT_BUNDLE_MIXED,
	DL_FILE_DL_TYPE_PARAMETER,
	DL_FILE_DL_TYPE_ZIP,

	/* These masks are used for expand on the type of download */
	DL_FILE_DL_TYPE_EXPIRES_MASK = 0x10,
	DL_FILE_DL_TYPE_IS_EXPIRED_MASK = 0x20,
	DL_FILE_DL_TYPE_DOWNLOAD_MASK = 0x40,
	DL_FILE_DL_TYPE_INSTALL_MASK = 0x80,
	DL_FILE_DL_TYPE_MAINTENANCE_MASK = 0x100, /* Make sure the enum is big enough for this mask */
}dl_file_dl_type_t;

#define DL_FILE_DL_TYPE_TYPE_MASK			0x0f



typedef enum _dl_file_hash_algo_t
{
	DL_FILE_HASH_ALGO_SHA_256,
}dl_file_hash_algo_t;

typedef struct _download_file_content_t
{
	dl_file_method_t			Method;
	char						FileName[256];
	uint32						FileSize;
	dl_file_dl_type_t			DownloadType;
	dl_file_hash_algo_t			HashAlgo;
	char						User[64];
	char						PW[64];
	char						URL[256];
	char						username[64];
	time_t						ApplyOnDate;
	time_t						ExpirationDate;
	term_action_t				PostInstallAction;
	bool						SuppressReboot;
	bool						CompressionEnabled;
	char						Receiver[64];
	char						Hash[64+1];		// ascii-hex straight from the XML... 
												// make sure it has room for the null, then put at end 
												// to eliminate internal fragmentation on the struct.
}download_file_content_t;


typedef struct _launch_downloader_parms
{
	operation_t operation;
	download_file_content_t dl_file_content;
	int comm_id;
	int message_id;
	srv_msg_type_t srv_msg_type;
	srv_op_type_t srv_op_type;
}launch_downloader_parms;


typedef enum _upload_type_t
{
	UPLOAD_TYPE_DIR_LIST,
	UPLOAD_TYPE_FILE,
	UPLOAD_TYPE_SCREEN_CAPTURE,
	UPLOAD_TYPE_LOG_FILE_LIST,
	UPLOAD_TYPE_OS_CONFIG_FILES,
	UPLOAD_TYPE_OS_LOG_FILES,
	UPLOAD_TYPE_USR_LOG_FILES,
	UPLOAD_TYPE_PARAM_FILE,
}upload_type_t;

typedef enum _upload_mehod_t
{
	UPLOAD_METHOD_FTP,
	UPLOAD_METHOD_HTTP,
	UPLOAD_METHOD_WEB_SERVICE,
	UPLOAD_METHOD_POST,
}upload_method_t;

typedef enum _upload_file_type_t
{
	UPLOAD_FILE_TYPE_PNG,
	UPLOAD_FILE_TYPE_TGZ,
	UPLOAD_FILE_TYPE_TXT,
	UPLOAD_FILE_TYPE_UNKNOWN,
}upload_file_type_t;

typedef struct _upload_file_content_t
{
	upload_type_t				Type;
	char						FileName[256];
	upload_method_t				Method;
	char						URL[256];
	char						WebMethod[64];
	char						User[64];
	char						PW[64];
	bool						CompressionEnabled;
	char						Receiver[64];
}upload_file_content_t;

typedef struct _uploader_parms
{
	operation_t operation;
	upload_file_content_t ul_file_content;
	int comm_id;
	int message_id;
	srv_msg_type_t srv_msg_type;
	srv_op_type_t srv_op_type;
}uploader_parms;



/* Mask definitions for GetData Operations */
#define GET_DATA_MASK_APPLICATIONS			0x00000001
#define GET_DATA_MASK_DEVICE_PROFILE		0x00000002
#define GET_DATA_MASK_DIAG_COUNTERS			0x00000004
#define GET_DATA_MASK_DISPLAY_CAPABILITIES	0x00000008
#define GET_DATA_MASK_KEYS					0x00000010
#define GET_DATA_MASK_MEMORY				0x00000020
#define GET_DATA_MASK_VSR_CERT_TREE			0x00000040
#define GET_DATA_MASK_VCL_INFO				0x00000080
#define GET_DATA_MASK_DIAG_PROFILE			0x00000100

typedef uint32 get_data_mask_t;

typedef struct _get_data_content_t
{
	get_data_mask_t						Query;
}get_data_content_t;


typedef struct _get_data_parms
{
	operation_t operation;
	get_data_content_t get_data_content;
	int comm_id;
	int message_id;
	srv_msg_type_t srv_msg_type;
	srv_op_type_t srv_op_type;
}get_data_parms;


typedef enum _set_data_type_t
{
	SET_DATA_AGENT,
	SET_DATA_PROXY,
}set_data_type_t;

typedef struct _set_data_element_t
{
	char Section[16];
	char Name[32];
	char Value[100];
}set_data_element_t;

typedef struct _set_data_content_t
{
	uint8 SetDataElementCount;
	set_data_type_t SetDataType;
	set_data_element_t SetDataElementArray[10];

}set_data_content_t;


typedef struct _set_data_parms
{
	operation_t operation;
	set_data_content_t set_data_content;
	int comm_id;
	int message_id;
	srv_msg_type_t srv_msg_type;
	srv_op_type_t srv_op_type;
}set_data_parms;

typedef enum _app_action_type_t
{
	APP_ACTION_NONE,
	APP_ACTION_EOD_CLEARANCE,
	APP_ACTION_DIAGNOSTICS,
	APP_ACTION_MAINTENANCE_END,
}app_action_type_t;

typedef struct _app_action_content_t
{
	app_action_type_t			AppAction;
}app_action_content_t;

typedef struct _app_action_parms
{
	operation_t operation;
	app_action_content_t app_action_content;
	int comm_id;
	int message_id;
	srv_msg_type_t srv_msg_type;
	srv_op_type_t srv_op_type;
}app_action_parms;



typedef struct _del_file_content_t
{
	char receiver[64];
	char file_name[512];
}del_file_content_t;

typedef struct _del_file_parms
{
	operation_t operation;
	del_file_content_t del_file_content;
	int comm_id;
	int message_id;
	srv_msg_type_t srv_msg_type;
	srv_op_type_t srv_op_type;
}del_file_parms;


#endif // _VHQ_OPERATIONS_H
