/*****************************************************************************
* Copyright (c) 2010 VeriFone Inc.
* 2099 Gateway Place, Suite 600
* San Jose, CA. 95110
*
* All Rights Reserved. No part of this software may be reproduced,
* transmitted, transcribed, stored in a retrieval system, or
* translated into any language or computer language, in any form
* or by any means electronic, mechanical, magnetic, optical,
* chemical, manual or otherwise, without the prior permission of
* VeriFone Inc.
*
******************************************************************************/

#ifndef _PRIV_BASE64_
#define _PRIV_BASE64_

#ifdef __cplusplus
extern "C" {
#endif


int base64decode (char *in, size_t inLen, unsigned char *out, size_t *outLen);
int base64encode(const void* data_buf, size_t dataLength, char* result, size_t resultSize);


#ifdef __cplusplus
}
#endif

#endif
