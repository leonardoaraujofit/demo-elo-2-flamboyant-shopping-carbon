#include "privnet_typeconv.h"

#ifndef PRIVNET_UTILS_H_
#define PRIVNET_UTILS_H_

/******************************************************************************/
/** @file privnet_utils.h
*
* @brief Private use utility functions for svc_net
*
* VeriFone, Inc.
*
*/
/*****************************************************************************/


int doSecurityScan(const char *buf,const char *unsec_chars);

/** check if a socket is connected */
uint8 check_socket_status( int socketid );

#endif /* PRIVNET_UTILS_H_ */
