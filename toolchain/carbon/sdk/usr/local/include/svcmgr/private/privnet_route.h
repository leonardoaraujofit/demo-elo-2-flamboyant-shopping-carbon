/*****************************************************************************
* Copyright (c) 2010 VeriFone Inc.
* 2099 Gateway Place, Suite 600
* San Jose, CA. 95110
*
* All Rights Reserved. No part of this software may be reproduced,
* transmitted, transcribed, stored in a retrieval system, or
* translated into any language or computer language, in any form
* or by any means electronic, mechanical, magnetic, optical,
* chemical, manual or otherwise, without the prior permission of
* VeriFone Inc.
*
******************************************************************************/

#ifndef _NET_ROUTE_
#define _NET_ROUTE_

#include "privnet_typeconv.h"
#include "pub_privnet.h"

/* The RTACTION entries must agree with tbl_verb[] below! */
#define RTACTION_ADD		1
#define RTACTION_DEL		2


PRIVNET_STATUS INET_getroutes( const char *dest , struct netRoute *rList, uint16 pMax, uint16 *pSize);
PRIVNET_STATUS INET_setrouteExt( int action, char **args );
int INET_rresolveExt(char *name, size_t len, void *s_in, int numeric, unsigned int netmask);
PRIVNET_STATUS PRIVNET_HostsAreInSameNetwork( const uint8*pHost1, const uint8*pNetmask, const uint8*pHost2 );
PRIVNET_STATUS PRIVNET_HostIsNetwork( const uint8*pNet, const uint8*pNetmask, const uint8*pHost );

PRIVNET_STATUS INET_setrouteExtIpv6( int action, char **args );
PRIVNET_STATUS INET_getroutesIpv6( const char *dest , struct netRouteIpv6 *rList, uint16 maxRouteNumber, uint16 *pRouteNumberFound);
PRIVNET_STATUS PRIVNET_GetExtendedMaskIpv6 (const uint8 netMask, uint16 * extendedNetMask);
PRIVNET_STATUS PRIVNET_HostsAreInSameNetworkIpv6( const uint8*pIpv6Addr1, const uint8 f_pNetmask, const uint8*pIpv6Addr2 );
PRIVNET_STATUS PRIVNET_HostIsNetworkIpv6( const uint8*f_pNet, const uint8 f_pNetmask, const uint8*f_pNet2 );
PRIVNET_STATUS PRIVNET_IPInSameNetworkWithInterfaceIPv6 (const uint8*f_pNet, const uint8 f_pNetmask, const struct netIfconfigIpv6 *pIpv6Config );
PRIVNET_STATUS PRIVNET_HostInSameNetworkWithInterfaceIPv6(const uint8 * pHost,
  const struct netIfconfigIpv6 * pIpv6Config);
PRIVNET_STATUS PRIVNET_DelNetRouteIpv6( const uint8*f_pNet, const uint8 f_pNetmask );
PRIVNET_STATUS INET_getGatewayIpv6( const char * f_pIface, char * gateway);

#endif
