/*****************************************************************************
* Copyright (c) 2010 VeriFone Inc.
* 2099 Gateway Place, Suite 600
* San Jose, CA. 95110
*
* All Rights Reserved. No part of this software may be reproduced,
* transmitted, transcribed, stored in a retrieval system, or
* translated into any language or computer language, in any form
* or by any means electronic, mechanical, magnetic, optical,
* chemical, manual or otherwise, without the prior permission of
* VeriFone Inc.
*
******************************************************************************/

#ifndef _NET_DHCP_
#define _NET_DHCP_

#include "privnet_typeconv.h"

int utilIsDHCPRunning( const uint8 *pIface, int *pPID, int ipVersion );
int utilReadGateway( const uint8 *pIface, uint8 *gateway, uint8 size, int ipVersion );
int utilReadDNS( const uint8 *pIface, uint8 *dns1, uint8 size1, uint8 *dns2, uint8 size2,
  int ipVersion );
  
int PRIVNET_enableIpv6RARSSyctl(const uint8 * pIface, int status);

#endif
