/*****************************************************************************
* Copyright (c) 2010 VeriFone Inc.
* 2099 Gateway Place, Suite 600
* San Jose, CA. 95110
*
* All Rights Reserved. No part of this software may be reproduced,
* transmitted, transcribed, stored in a retrieval system, or
* translated into any language or computer language, in any form
* or by any means electronic, mechanical, magnetic, optical,
* chemical, manual or otherwise, without the prior permission of
* VeriFone Inc.
*
******************************************************************************/

#ifndef _PUB_XMLPARSER_
#define _PUB_XMLPARSER_

#include "privnet_typeconv.h"
#include "pub_privnet.h"
#include "privnet_msg.h"

#if defined(__cplusplus) && !defined(__DEBUG__)
extern "C" {
#endif

#define TAG_NAX_NAME   128
#define XML_ATTRIB_INT   1 
#define XML_ATTRIB_STR   2 
#define XML_ATTRIB_LONG  3
#define XML_ATTRIB_CHAR  4


/** Define default route information
 * @ingroup OSsvc_net */
typedef struct 
{
	char *name;/**< tag of the attribute */
	void *value;/**< value of the attribute */
	int value_count;/**< maximum number of value storage */ 
	int type;/**< type of the attribute (1=char*, 0 = integer ) */			
}XML_PARSE_ATTRIB;

PRIVNET_STATUS xml_getFilesLocation( const uint8 *szxmlFile, const uint8 *tagname, const uint8 *config_id, struct SVC_NET_FILE_LOC *stFileLoc );
int xml_sslLoadCipherSuitePCI( char *f_cipherList, int sizecount );

int xml_getBtLinkKey( char* id, unsigned char* lk, size_t* lk_len, int* type, int* pin_len );
int xml_setBtLinkKey( char* id, unsigned char* lk, size_t lk_len, int type, int pin_len );
int xml_removeLinkKey( char* id );

int xml_setIBeacon( struct netIBeacon *ibeacon, int start_mode );
int xml_getIBeacon( struct netIBeacon *ibeacon, int *start_mode );

int xml_getCapabilities( const char * szxmlFile,  struct netCapabilities *nw_caps );

#if defined(__cplusplus) && !defined(__DEBUG__)
}
#endif

#endif
