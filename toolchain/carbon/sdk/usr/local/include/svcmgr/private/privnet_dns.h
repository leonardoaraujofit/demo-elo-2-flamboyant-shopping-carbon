/*****************************************************************************
* Copyright (c) 2010 VeriFone Inc.
* 2099 Gateway Place, Suite 600
* San Jose, CA. 95110
*
* All Rights Reserved. No part of this software may be reproduced,
* transmitted, transcribed, stored in a retrieval system, or
* translated into any language or computer language, in any form
* or by any means electronic, mechanical, magnetic, optical,
* chemical, manual or otherwise, without the prior permission of
* VeriFone Inc.
*
******************************************************************************/

#ifndef _NET_DNS_
#define _NET_DNS_

#include "privnet_typeconv.h"
#include "pub_privnet.h"

PRIVNET_STATUS utilSelectDNS( const uint8 *pIface, int ipVersion );
int utilReadDNS( const uint8 *pIface, uint8 *dns1, uint8 size1,
												  uint8 *dns2, uint8 size2, int ipVersion );
PRIVNET_STATUS PRIVNET_IsIPAddress( const uint8 * pHost, uint8 *pValue );
PRIVNET_STATUS PRIVNET_AddRouteToDNS( const uint8 *pIface );

PRIVNET_STATUS emptyDNSFile();

PRIVNET_STATUS PRIVNET_IsIPv6Address(const uint8 * pHost, uint16 * pValue, uint8 * pExtended);
PRIVNET_STATUS PRIVNET_GetIPv6AddressType(const uint8 * pHost, int *type);
PRIVNET_STATUS PRIVNET_IsIPv6Mask (const int pNetMask);
PRIVNET_STATUS PRIVNET_AddRouteToDNSIpv6( const uint8 *pIface );
PRIVNET_STATUS checkDNSFile(  const uint8 *f_pIface, int ipVersion );

#endif
