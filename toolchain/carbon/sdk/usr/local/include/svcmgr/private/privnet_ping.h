/*****************************************************************************
* Copyright (c) 2010 VeriFone Inc.
* 2099 Gateway Place, Suite 600
* San Jose, CA. 95110
*
* All Rights Reserved. No part of this software may be reproduced,
* transmitted, transcribed, stored in a retrieval system, or
* translated into any language or computer language, in any form
* or by any means electronic, mechanical, magnetic, optical,
* chemical, manual or otherwise, without the prior permission of
* VeriFone Inc.
*
******************************************************************************/

#ifndef _NET_PING_
#define _NET_PING_

#include "privnet_typeconv.h"
#include "pub_privnet.h"

#ifdef __cplusplus
extern "C" {
#endif

PRIVNET_STATUS call_ping( const uint8 *host, uint8 f_PingCount, struct netPingInfo *f_pInfo );


#ifdef __cplusplus
}
#endif

#endif
