/*****************************************************************************
* Copyright (c) 2010 VeriFone Inc.
* 2099 Gateway Place, Suite 600
* San Jose, CA. 95110
*
* All Rights Reserved. No part of this software may be reproduced,
* transmitted, transcribed, stored in a retrieval system, or
* translated into any language or computer language, in any form
* or by any means electronic, mechanical, magnetic, optical,
* chemical, manual or otherwise, without the prior permission of
* VeriFone Inc.
*
******************************************************************************/

#ifndef _PRIVNET_MSG_H_
#define _PRIVNET_MSG_H_

#include "privnet_typeconv.h"
#include "pub_privnet.h"
#include "svc_net.h"
#include "gsm/gsm.h"

/** Indicates whether this library is running under application or control context */
typedef enum
{
   USE_PRIVNET_LIB,/** service is running under application context */
   USE_PRIVNET_CONTROL,/** service is running under control context */
} PRIVNET_CONTEXT_TYPE;

extern PRIVNET_CONTEXT_TYPE g_enSVCNetUsage;


/* svc_net remote functions
---------------------------------------------------------------------------*/

#define PRIVNET_TASK_TIMEOUT       65000
#define PRIVNET_MAX_IFACE            10
#define SOCKET_ERROR                 -1
#define INVALID_SOCKET               -1
#define getSocketError()          errno
#define WOULD_BLOCK	             EAGAIN

#define PRIVNET_MAX_STR  128

#define DEFAULT_DIR_ACCESS  (S_IRWXU|S_IRWXG|S_IRWXO)
#define DEFAULT_FILE_ACCESS (S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH)

#define PRIVNET_CMD_LENGTH      256
#define PRIVNET_FILE_LENGTH     256
#define PRIVNET_FILE_LENGTH_S   128


/** Modem defines */
#define DIALNOWAIT_MAX            8
#define PREFIX_MAX                8
#define POSTFIX_MAX              16   
#define MDM_DIAL_TIMEOUT          5
#define MDM_INIT_TIMEOUT          5
#define MDM_CONNECT_TIMEOUT      60
#define MDM_DIAL_TIMEOUT          5 


/** GPRS defines */
#define GSM_PIN_TIMEOUT        20
#define SIM_PIN_MAX_SIZE        8
#define SIM_SCID_MAX_SIZE       STR_INFO_LEN2
#define SIM_PIN_FILE            "/mnt/flash/etc/config/svcnet/pinfile"
#define GSM_OPR_TIMEOUT         45
#define GSM_SCAN_TIMEOUT        60
#define GSM_PIN_ATTEMPT_TIMEOUT  5


#define PRIVNET_DNS_LINK        "dns_link"
#define PRIVNET_SCRIPT_NAME     "udhcpend.sh"
#define PRIVNET_DEF_FILE        "default.info"
#define PRIVNET_IPV6_DEF_FILE   "default_ipv6.info"

#define PRIVNET_DNS_ETC_DIR     "etc"
#define PRIVNET_DNS_SUB_DIR     "dns"
#define PRIVNET_PID_SUB_DIR     "pid"
#define PRIVNET_GW_SUB_DIR      "gateway"
#define PRIVNET_ONLINE_SUB_DIR  "online"

/* A command is running */
#define PRIVNET_CMD_PROC         0x12

/* Command has been proceced */
#define PRIVNET_CMD_END          0x34

/* define the shared memory size */
#define PRIVNET_SHARED_SIZE  16*1024

/* define the number of event we can handle */
#define PRIVNET_MAX_EVENT         16

/** Calculate the event storage size */
#define PRIVNET_SHARED_SIZE_EVENT (PRIVNET_MAX_EVENT*sizeof(struct netEvent) + 2*sizeof(uint16)+32)

/* Memory shared key */
#define PRIVNET_KEY_MEM_FILE "/usr/local/lib/svcmgr/libsvc_net.so"
#define PRIVNET_KEY_MEM_FILE_GPRS "/usr/local/sbin/svc_netclient"
#define PRIVNET_KEY_MEM_FILE_EVENT "/usr/local/sbin/svc_netcontrol"

/* Util define the shared memory size */
#define PRIVNET_UTIL_SHARED_SIZE  64

/* Command control position in the shared memmory  space */
#define PRIVNET_CMD_CTRL_POS 0

/* Command type position in the shared memory space */
#define PRIVNET_CMD_TYPE_POS 1

/** Event has been set/cleared */
#define PRIVNET_CMD_EVT_CLEAR  1
#define PRIVNET_CMD_EVT_SET    2

/** Amount of time needed to authenticate the WIFI */
#define NET_WIFI_AUTH_TIMEOUT 5

/** Indicates whether or not we're watching for events */
typedef enum
{
	PRIVNET_EVENT_OFF,
	PRIVNET_EVENT_ON
}PRIVNET_EVENT_SET_TYPE;

/* Semaphore key */
#define PRIVNET_KEY_SEM_FILE "/usr/local/lib/svcmgr/libsvc_net.so"
#define PRIVNET_KEY_SEM_FILE_GPRS "/usr/local/sbin/svc_netclient"
#define PRIVNET_KEY_SEM_FILE_EVENT "/usr/local/sbin/svc_netcontrol"

#define SOCKET_ERROR                   -1
#define INVALID_SOCKET                 -1
#define getSocketError()            errno
#define WOULD_BLOCK	               EAGAIN

#define PRIVNET_TASK_IP       "127.0.0.01"
#define PRIVNET_TASK_PORT            6060
#define PRIVNET_TASK_TIMEOUT        65000
#define PRIVNET_MAX_IFACE              10

typedef enum
{
	PRIVNET_NO_FUNC,						// 0
	PRIVNET_OPEN_FUNC,
	PRIVNET_CLOSE_FUNC,
	PRIVNET_HOST_ROUTE_ADD_FUNC,
	PRIVNET_HOST_ROUTE_DEL_FUNC,
	PRIVNET_NET_ROUTE_ADD_FUNC,				// 5
	PRIVNET_NET_ROUTE_DEL_FUNC,
	PRIVNET_IFCONFIG_FUNC,
	PRIVNET_DHCP_START_FUNC,
	PRIVNET_DHCP_STOP_FUNC,
	PRIVNET_IFACE_SETUP_FUNC,				// 10
	PRIVNET_IFACE_DECONFIG_FUNC,
	PRIVNET_DHCP_RENEW_FUNC,
	PRIVNET_DHCP_RELEASE_FUNC,
	PRIVNET_CHECK_CABLE_FUNC,
	PRIVNET_RESOLV_DNS_FUNC,				// 15
	PRIVNET_SET_DEFAULT_FUNC,
	PRIVNET_GET_DEFAULT_INFO_FUNC,
	PRIVNET_IFACE_INFO_FUNC,
	PRIVNET_PING_FUNC,
	PRIVNET_AUTO_SET_DEFAULT_FUNC,			// 20
	PRIVNET_ROUTE_XML_FUNC,
	PRIVNET_UPDATE_IFACE_FUNC,
	PRIVNET_FLUSH_DNS_FUNC,
	PRIVNET_REMOVE_DNS_FUNC,
	PRIVNET_FILE_TYPE_FUNC,					// 25
	PRIVNET_START_WIFI_CMD_FUNC,
	PRIVNET_STOP_WIFI_CMD_FUNC,
	PRIVNET_XML_SET_CMD_FUNC,
	PRIVNET_XML_GET_CMD_FUNC,
	PRIVNET_CHECK_DHCP_FUNC,				// 30
	PRIVNET_WIFI_INFO_CMD_FUNC,
	PRIVNET_WIFI_SITECOUNT_CMD_FUNC,
	PRIVNET_WIFI_SITELIST_CMD_FUNC,
	PRIVNET_NTP_CMD_FUNC,
	PRIVNET_IFACE_UPDATE_CMD_FUNC,			// 35
	PRIVNET_XML_MODEM_SET_CMD_FUNC,
	PRIVNET_XML_MODEM_GET_CMD_FUNC,
	PRIVNET_MODEM_CONNECT_FUNC,
	PRIVNET_MODEM_STATUS_FUNC,
	PRIVNET_PPP_CONNECT_FUNC,				// 40
	PRIVNET_MODEM_DISCONNECT_FUNC,
	PRIVNET_PPP_DISCONNECT_FUNC,
	PRIVNET_XML_OPTION_SET_CMD_FUNC,
	PRIVNET_XML_OPTION_GET_CMD_FUNC,
	PRIVNET_MOD_CONN_STRUCT_CMD_FUNC,		// 45 ---- Not used
	PRIVNET_PPP_CONN_STRUCT_CMD_FUNC,
	PRIVNET_UPDATE_CONN_CMD_FUNC,
	PRIVNET_GET_CONN_CMD_FUNC,
	PRIVNET_SET_DNS_FUNC,
	PRIVNET_MOD_CONN_STRUCT_OPTION_CMD_FUNC,// 50
	PRIVNET_XML_GPRS_SET_CMD_FUNC,
	PRIVNET_XML_GPRS_GET_CMD_FUNC,
	PRIVNET_GPRS_CONNECT_FUNC,
	PRIVNET_GPRS_DISCONNECT_FUNC,
	PRIVNET_GPRS_VERIFY_PIN_FUNC,			// 55
	PRIVNET_GPRS_GET_STATUS_FUNC,
	PRIVNET_GPRS_GSM_INFO_FUNC,
	PRIVNET_GPRS_GSM_RSSI_FUNC,
	PRIVNET_MOD_ISDN_CONN_STRUCT_OPTION_CMD_FUNC,
	PRIVNET_XML_ISDN_MODEM_GET_CMD_FUNC,	// 60
	PRIVNET_XML_ISDN_MODEM_SET_CMD_FUNC,
	PRIVNET_GPRS_GSM_POWERON_FUNC,
	PRIVNET_CREATE_BRIDGE_FUNC,
	PRIVNET_DELETE_BRIDGE_FUNC,
	PRIVNET_GPRS_GSM_POWEROFF_FUNC,			// 65
	PRIVNET_GPRS_SITE_SURVEY_FUNC,
	PRIVNET_GPRS_OPERATOR_NAME_FUNC,
	PRIVNET_GPRS_CONN_STRUCT_OPTION_CMD_FUNC,
	PRIVNET_GPRS_CHANGE_PIN_FUNC,
	PRIVNET_GPRS_UNLOCK_PIN_FUNC,			// 70
	PRIVNET_ETHERNET_HOTPLUG_FUNC,
	PRIVNET_SEND_AT_CMD_FUNC,
	PRIVNET_MODEM_INIT_CMD_FUNC,
	PRIVNET_GPRS_GET_PIN_COUNTER_FUNC,
	PRIVNET_GPRS_SEND_AT_CMD_FUNC,			// 75
	PRIVNET_GPRS_OPERATOR_LAI_FUNC,
	PRIVNET_GPRS_SEND_CSD_FUNC,
	PRIVNET_GPRS_RECV_CSD_FUNC,
	PRIVNET_GPRS_LAYER_DISCONNECT_FUNC,
	PRIVNET_BLUETOOTH_POWER_ON_FUNC,		// 80
	PRIVNET_BLUETOOTH_PAIR_FUNC,
	PRIVNET_BLUETOOTH_CON_PAN_FUNC,
	PRIVNET_BLUETOOTH_DISCON_PAN_FUNC,
	PRIVNET_BLUETOOTH_UNPAIR_FUNC,
	PRIVNET_BLUETOOTH_CON_DUN_FUNC,			// 85
	PRIVNET_BLUETOOTH_DISCON_DUN_FUNC,
	PRIVNET_BLUETOOTH_CON_SPP_FUNC,
	PRIVNET_BLUETOOTH_DISCON_SPP_FUNC,
	PRIVNET_BLUETOOTH_GET_FUNC,
	PRIVNET_BLUETOOTH_SET_FUNC,				// 90
	PRIVNET_BLUETOOTH_SAVE_KEY,
	PRIVNET_BLUETOOTH_IBEACON_START,
	PRIVNET_BLUETOOTH_IBEACON_STOP,
	PRIVNET_BLUETOOTH_IBEACON_GET_INFO,
	PRIVNET_SEND_SATNAV_CMD_FUNC,
	PRIVNET_GPRS_SELECT_SIM_CMD_FUNC,
	PRIVNET_WIFI_GET_SIGNAL_STATS_CMD_FUNC,
	PRIVNET_GET_PPP_EXIT_CMD_FUNC,			// 95
	PRIVNET_GET_MODEM_EXIT_CMD_FUNC,
	PRIVNET_GPRS_SEND_SMS_CMD_FUNC,
	PRIVNET_GPRS_GET_RAT_CMD_FUNC,
	PRIVNET_GPRS_CELL_SURVEY_FUNC,
	PRIVNET_BLUEOOTH_WAKE_FUNC,
	PRIVNET_USB_GADGET_SET_CFG,
	PRIVNET_USB_GADGET_GET_CFG,
	PRIVNET_XML_SET_ATTR_CMD_FUNC,
	PRIVNET_XML_GET_ATTR_CMD_FUNC,
	PRIVNET_WIFI_STATUS_FUNC,
	PRIVNET_IPV6_XML_SET_CMD_FUNC,
	PRIVNET_IPV6_XML_GET_CMD_FUNC,
	PRIVNET_IPV6_IFACE_SETUP_FUNC,
	PRIVNET_IPV6_AUTO_SET_DEFAULT_FUNC,
	PRIVNET_IPV6_SET_DEFAULT_FUNC,
	PRIVNET_IPV6_IFACE_INFO_FUNC,
	PRIVNET_IPV6_START_WIFI_CMD_FUNC,
	PRIVNET_IPV6_STOP_WIFI_CMD_FUNC, 
	PRIVNET_IPV6_WIFI_SITELIST_CMD_FUNC,
	PRIVNET_IPV6_IFCONFIG_FUNC,
	PRIVNET_IPV6_ETHERNET_HOTPLUG_FUNC,
	PRIVNET_IPV6_RESOLV_DNS_FUNC, 
	PRIVNET_IPV6_NET_ROUTE_DEL_FUNC,
	PRIVNET_IPV6_NET_ROUTE_ADD_FUNC,
	PRIVNET_IPV6_HOST_ROUTE_DEL_FUNC,
	PRIVNET_IPV6_HOST_ROUTE_ADD_FUNC,
	PRIVNET_IPV6_PING_FUNC,
	PRIVNET_IPV6_DHCP_START_FUNC,
	PRIVNET_IPV6_DHCP_STOP_FUNC,
	PRIVNET_IPV6_DHCP_RENEW_FUNC,
	PRIVNET_IPV6_DHCP_RELEASE_FUNC,
	PRIVNET_IPV6_IFACE_UPDATE_CMD_FUNC,
	PRIVNET_IPV6_FLUSH_DNS_FUNC,
	PRIVNET_IPV6_REMOVE_DNS_FUNC,
	PRIVNET_IPV6_IFACE_DECONFIG_FUNC,
	PRIVNET_GET_DEFAULT_INFO_IPV6_FUNC,
	PRIVNET_SET_DNS_IPV6_FUNC,
	PRIVNET_BLUETOOTH_REMOVE_KEY,
#ifdef RAPTOR
	PRIVNET_WIFI_CUSTOM_COMMAND_FUNC,
#endif
	PRIVNET_BLUETOOTH_SET_CABLE_STATUS,
	PRIVNET_WIFI_IFPLUGD_CMD_FUNC,
	PRIVNET_HOTPLUG_ACTION_FUNC,
	PRIVNET_BLUETOOTH_GET_NAME_FUNC,
	PRIVNET_LOGLEVEL_SET_FUNC,
	PRIVNET_GPRS_GET_SIM_STATUS_FUNC,
	PRIVNET_MAX_FUNC
} PRIVNET_FUNC_TYPE;

typedef enum
{
    EXECERR_OK = 0,             ///< no error
    EXECERR_INVAL,              ///< wrong input value
    EXECERR_INIT_FAILED,        ///< command line printf or malloc failed
    EXECERR_PARSE_FAILED,       ///< cmd line argument extraction failed
    EXECERR_ILLEGAL_REDIRECT,   ///< wrong Redirect (> OR >>) placement (must be 1 argument before last)
    EXECERR_TOO_MANY_REDIRECTS, ///< more then one Redirect (> OR >>)
    EXECERR_FORKING_FAILED,     ///< forking in fork-exec sequence failed
    EXECERR_EXEC_FAILED,        ///< cmd line execution failed
    EXECERR_MALLOC_FAILED,
    EXECERR_SPRINTF_FAILED,
    EXECERR_ILLEGAL_CMD_LENGTH,
    EXECERR_REALLOC_FAILED,
    EXECERR_CMD_SYNTAX_ERROR,
    EXECERR_TOO_MANY_ARGS,
    EXECERR_ERASE_SPACES_FAILED,
    EXECERR_ARG_ASSIGNMENT_FAILED,
    EXECERR_FFLUSH_FAILED,
    EXECERR_FILE_OPEN_FAILED,
    EXECERR_FILE_WRITE_FAILED,
    EXECERR_EXECVP_FAILED
} execErrCode_t;

typedef struct
{
   uint8 host[PRIVNET_IP_MAX_SIZE];
   uint8 gateway[PRIVNET_IP_MAX_SIZE];
   uint8 iface[PRIVNET_IFACE_MAX_SIZE];
} PRIVNET_HOST_ROUTE_CMD;

typedef struct
{
   uint8 net[PRIVNET_IP_MAX_SIZE];
   uint8 netmask[PRIVNET_IP_MAX_SIZE];
   uint8 gateway[PRIVNET_IP_MAX_SIZE];
   uint8 iface[PRIVNET_IFACE_MAX_SIZE];
} PRIVNET_NET_ROUTE_CMD;

typedef struct
{
   uint8 host[PRIVNET_IPV6_WITH_PORT_MAX_SIZE];
   uint8 gateway[PRIVNET_IPV6_WITH_PORT_MAX_SIZE];
   uint8 iface[PRIVNET_IFACE_MAX_SIZE];
} PRIVNET_HOST_ROUTE_IPV6_CMD;

typedef struct
{
    uint8 net[PRIVNET_IPV6_WITH_PORT_MAX_SIZE];
    uint8 netmask;
    uint8 gateway[PRIVNET_IPV6_WITH_PORT_MAX_SIZE];
   uint8 iface[PRIVNET_IFACE_MAX_SIZE];
} PRIVNET_NET_ROUTE_IPV6_CMD;

typedef struct
{
	uint8 iface[PRIVNET_IFACE_MAX_SIZE];
}PRIVNET_IFACE_CMD;

typedef struct
{
	uint8 iface[PRIVNET_IFACE_MAX_SIZE];
	int async;
}PRIVNET_IFACE_SETUP_CMD;

typedef struct
{
	uint8 iface[PRIVNET_IFACE_MAX_SIZE];
	int stopInterface;
}PRIVNET_IFACE_DECONF_CMD;

typedef struct
{
	uint8 iface[PRIVNET_IFACE_MAX_SIZE];
	uint8 dns_name[PRIVNET_DNS_MAX_SIZE];
}PRIVNET_DNS_CMD;

typedef struct
{
	struct netIfconfig ifconfig;
}PRIVNET_XML_SET_CMD;

typedef struct
{
	struct netIfconfig ifconfig;
}PRIVNET_IFACE_UPDATE_CMD;

typedef struct
{
	uint8 host[PRIVNET_IP_MAX_SIZE];
	uint8 pingcount;
	int ipversion;
}PRIVNET_PING_CMD;

typedef struct
{
  struct netIfconfigIpv6 ifconfig;
}PRIVNET_IPV6_XML_SET_CMD;

typedef struct
{
  struct netIfconfigIpv6 ifconfig;
}PRIVNET_IPV6_IFACE_UPDATE_CMD;

typedef struct
{
  uint8 host[NET_IPV6_MAX];
  uint8 pingcount;
  int ipversion;
}PRIVNET_IPV6_PING_CMD;

typedef struct
{
	uint8 filename[PRIVNET_FILE_LENGTH];
}PRIVNET_FILE_CMD;

typedef struct
{
	int file_type;
}PRIVNET_FILE_TYPE_CMD;

typedef struct
{
	uint8 wpa_supconf[NET_WIFI_WPACONF_MAX];
	int action;
}PRIVNET_WIFI_CMD;

typedef struct
{
	uint8 iface[PRIVNET_IFACE_MAX_SIZE];
	int eventtype;
}PRIVNET_WIFI_IFPLUGD_CMD;

typedef struct
{
	uint8 iface[PRIVNET_IFACE_MAX_SIZE];
	struct netModemInfo mdmInfo;
}PRIVNET_XML_MODEM_SET_CMD;

typedef struct
{
	uint8 iface[PRIVNET_IFACE_MAX_SIZE];
	struct netModemIsdnInfo mdmInfo;
}PRIVNET_XML_MODEM_ISDN_SET_CMD;

typedef struct
{
	uint8 iface[PRIVNET_IFACE_MAX_SIZE];
	uint8 option[PRIVNET_OPTION_MAX_SIZE];
}PRIVNET_XML_OPTION_SET_CMD;

typedef struct
{
	uint8 com_type;
	uint8 increaseType;
}PRIVNET_UPDATE_CONN_CMD;


/** This sctrucure defines all the files location */
struct SVC_NET_FILE_LOC
{
	uint8 svcnetTempDir[PRIVNET_FILE_MAX_SIZE];
	uint8 fileDefaultXML[PRIVNET_FILE_MAX_SIZE];
	uint8 fileDefaultINI[PRIVNET_FILE_MAX_SIZE];
	uint8 udhcpcAction[PRIVNET_FILE_MAX_SIZE];
	uint8 odhcpcIpv6Action[PRIVNET_FILE_MAX_SIZE];
	uint8 ifplugdAction[PRIVNET_FILE_MAX_SIZE];
};

typedef struct
{
	uint8 iface[PRIVNET_IFACE_MAX_SIZE];
	uint8 dns1[PRIVNET_DNS_MAX_SIZE];
	uint8 dns2[PRIVNET_DNS_MAX_SIZE];
	int ipversion;
}PRIVNET_SET_DNS_CMD;


typedef struct
{
	uint8 option[NET_PPP_OPTION_MAX];
	struct netModemInfo mdmInfo;
}PRIVNET_XML_MODEM_CONN_OPTION_CMD;


typedef struct
{
	uint8 option[NET_PPP_OPTION_MAX];
	struct netGprsInfo gprsInfo;
}PRIVNET_XML_GPRS_CONN_OPTION_CMD;


typedef struct
{
	uint8 option[NET_PPP_OPTION_MAX];
	struct netModemIsdnInfo mdmInfo;
}PRIVNET_XML_MODEM_ISDN_CONN_OPTION_CMD;

typedef struct
{
	uint8 iface[PRIVNET_IFACE_MAX_SIZE];
	struct netGprsInfo gprsInfo;
}PRIVNET_XML_GPRS_SET_CMD;

typedef struct
{
	uint8 pin[16];
}PRIVNET_GPRS_PIN_CMD;


typedef struct
{
	uint8 oldpin[16];
	uint8 newpin[16];
}PRIVNET_GPRS_CHANGE_PIN_CMD;

typedef struct
{
	uint16 indataLength;
	uint16 timeout;
}PRIVNET_GPRS_CSD_READ_CMD;

typedef struct
{
	uint8 iface0[PRIVNET_IFACE_MAX_SIZE];
	uint8 iface1[PRIVNET_IFACE_MAX_SIZE];
}PRIVNET_BRIDGE_CMD;

typedef struct
{
	uint8 gadget;
}PRIVNET_USB_GADGET_CMD;

typedef struct
{
	char tx[PRIVNET_AT_CMD_MAX_SIZE];
	int max_retries;
	int response_timeout;
	int local;
}PRIVNET_MODEM_AT_CMD;

typedef struct
{
	char modemName[PRIVNET_INIT_NAME_SIZE];
	char modemFile[PRIVNET_INIT_FILE_SIZE];
	int local;
}PRIVNET_MODEM_INIT_CMD;

typedef struct
{
	int operation;
}PRIVNET_BT_POWER_ON_CMD;

typedef struct
{
	char pin[16];
	char remoteDeviceAddr[BT_MAX_ADDRESS];
}PRIVNET_BT_PAIR_CMD;

typedef struct
{
	char address[BT_MAX_ADDRESS];
}PRIVNET_BT_CMD;

typedef struct
{
	char name[BT_MAX_NAME];
}PRIVNET_BT_NAME_RESP;

typedef struct
{
	int channel;
	char address[BT_MAX_ADDRESS];
}PRIVNET_BT_CON_SPP_CMD;

typedef struct
{
	struct netIBeacon ibeacon;
	int mode;
	int activate;
}PRIVNET_BT_IBEACON_CMD;

typedef struct
{
	char tx[GSM_CMD_SIZE];
	int response_timeout;
}PRIVNET_GSM_AT_CMD;

typedef struct
{
	char number[NET_GPRS_NUMBER_MAX];
	char message[GSM_STR_SMS_MSG];
}PRIVNET_GSM_SMS_CMD;

#ifdef RAPTOR
typedef struct
{
	struct netWifiCustomCommand custom;
} PRIVNET_WIFI_CUSTOM_CMD;
#endif

typedef struct
{
   int comtype;
   char name_id[NET_NAMEID_MAX];
   char attrib[NET_IFACE_MAX];
   char value[NET_PPP_OPTION_MAX];
} PRIVNET_XML_SET_ATTR_CMD;

typedef struct
{
   int comtype;
   char name_id[NET_NAMEID_MAX];
   char attrib[NET_IFACE_MAX];
} PRIVNET_XML_GET_ATTR_CMD;

typedef struct
{
   char value[NET_PPP_OPTION_MAX];
} PRIVNET_XML_GET_ATTR_RESP;


typedef struct
{
	uint8 iface[PRIVNET_IFACE_MAX_SIZE];
	int param;
	int option;
}PRIVNET_IFACE_GENERIC_CMD;



typedef struct
{
	uint8 iface[PRIVNET_IFACE_MAX_SIZE];
	char address[BT_MAX_ADDRESS];
}PRIVNET_BASE_INFO_CMD;


extern uint8 g_bFilesInitialized;
extern struct SVC_NET_FILE_LOC g_stFileLoc;

/*-------------------------- Command management ------------------------------*/
PRIVNET_STATUS PRIVNET_clientSendCommand( PRIVNET_FUNC_TYPE type, void * in_parm,
										  uint16 in_len, void* out_parm,
										  uint16 out_len );

PRIVNET_STATUS PRIVNET_clientSendCommandGPRS( PRIVNET_FUNC_TYPE type, void * in_parm,
										  uint16 in_len, void* out_parm,
										  uint16 out_len );

/*----------------------------- PRIVNET_Socket functions -----------------------*/
void PRIVNET_WaitMS( int timeout );

/* Semaphore management */
long PRIVNET_SemCreate( const uint8 *semkey);
long PRIVNET_SemObtain( long semid );
long PRIVNET_SemSignal( long semid );
long PRIVNET_SemGetID( const uint8 *semkey );

/** debug traces */
void PRIVNET_PrintTrace( PRIVNET_FUNC_TYPE type, uint8 send );

/** Files location */
int svc_net_init_files( const uint8 *szxmlConfigFile );

/*------------------------------- SOCKET MANAGEMENT -------------------------------*/

void PRIVNET_SetTaskUse();
void PRIVNET_SetLibUse();

/*-------------------------- Network events management -------------------------------*/

PRIVNET_STATUS PRIVNET_clientConfigureEvent( PRIVNET_EVENT_SET_TYPE type );
PRIVNET_STATUS PRIVNET_clientGetEvent( struct netEvent *event );
PRIVNET_STATUS PRIVNET_clientSetEvent( int index, struct netEvent *event );
void PRIVNET_clientFlushEvent( const char *iface );
uint8 PRIVNET_isEventSet();
PRIVNET_STATUS PRIVNET_clientPostEvent( struct netEvent *event );
void PRIVENT_addSyslog( const char *message );
int net_addSyslogInfo( int level, char* Format,...);

/* hardening flags */
execErrCode_t execSysCmd( const char *cmdLine, ... );
int execSysCmdExt( const char *cmd );

int atoiExt( const char *inputString );
long atolExt( const char *inputString );

/* Messages functions */

#ifdef __cplusplus
extern "C" {
#endif

#define NET_IFCONFIG_MAX          7
#define NET_CELLLIST_MAX          7
#define NET_ROUTES_MAX           32
#define NET_IPV6_ROUTES_MAX	     32
#define NET_OPLIST_MAX            8
#define NET_WIFI_SITE_MAX        32
#define NET_BTDEVICE_MAX         16

#define PRIVNET_MAX_TCP_CMD    6000
#define PMSG_FILESOCKET_NAME   "/tmp/netprocsock"

typedef struct
{
	uint8 iface[PRIVNET_IFACE_MAX_SIZE];
	uint8 dhcpid[DHCPID_MAX_EXT];
	uint8 clienthostname[DHCPCLI_MAX_EXT];
	char mtu[DHCPID_MAX_EXT]; 
	int speed;
	int ipversion;
}PRIVNET_DHCP_CMD;

/** Command when sending data from the client to the server (net-process)  */
typedef struct 
{
	uint8 fcname[PRIVNET_FILE_LENGTH_S]; 
    uint16 i_len;
    uint16 o_len;
}pmsgClientCmdHeader;

typedef struct 
{
	pmsgClientCmdHeader header;
	void *i_data;/**< array of bytes to transfer */
	void *o_data;/**< array of bytes to transfer */
}pmsgClientCmdData;

/** Command when sending data from the server to the client  */
typedef struct 
{
    uint16 status;
    uint16 o_len;
}pmsgServerCmdHeader;

typedef struct 
{
	pmsgServerCmdHeader header;
	void *i_data;/**< array of bytes to transfer */
}pmsgServerCmdData;

/**
 * Possible values for event in pmsgIfaceCommand
 */
typedef enum 
{
	PIFACE_EVENT_NONE,
	PIFACE_EVENT_INTERFACE_ONLINE, 
	PIFACE_EVENT_INTERFACE_OFFLINE,
	PIFACE_EVENT_INTERFACE_UP,
	PIFACE_EVENT_INTERFACE_DOWN,
	PIFACE_EVENT_INTERFACE_ENABLE,
	PIFACE_EVENT_INTERFACE_DISABLE,
	PIFACE_EVENT_SESSION_ONLINE,
	PIFACE_EVENT_SESSION_OFFLINE,
	PIFACE_EVENT_MAX
}PIFACE_EVENT;


/**
 * Possible values for event_option in pmsgIfaceCommand struct
 */
typedef enum 
{
	PIFACE_EVENT_OPT_DEFAULT,
	PIFACE_EVENT_OPT_IFDOWN_0, 
	PIFACE_EVENT_OPT_IFDOWN_1,
	PIFACE_EVENT_OPT_SUPPLICANT_0,
	PIFACE_EVENT_OPT_SUPPLICANT_1,
	PIFACE_EVENT_OPT_STOPMODULE_0,
	PIFACE_EVENT_OPT_STOPMODULE_1,
}PIFACE_EVENT_OPTION;

/**
 * Possible values for source in pmsgIfaceCommand struct
 */
typedef enum 
{
	PIFACE_SOURCE_DEFAULT, /**< Event was generated by an API call */
	PIFACE_SOURCE_BOOT, /**< Event was generated during the boot time */
	PIFACE_SOURCE_HOTPLUG, /**< Event was generated automatically (ex. ifplugd) */
	PIFACE_SOURCE_SLEEP, /**< Event was generated when device enter sleep */
	PIFACE_SOURCE_WAKEUP , /**< Event was generated when device exit sleep */
}PIFACE_EVENT_SOURCE;


/* Used to execute a generic interface event (Ethernet Up/Down, WIFI on/off etc */
typedef struct
{
	PIFACE_EVENT event;/**< Action to perfrom */
	PIFACE_EVENT_OPTION event_option;/**< Option associated to this action */	
	PIFACE_EVENT_SOURCE source; /**< Source of the action (boot, hotplug or manual) */
	int ipversion;
	uint8 iface[PRIVNET_IFACE_MAX_SIZE]; /**< Interface on which to perfrom the action */
	uint8 xmlfile[PRIVNET_FILE_LENGTH];/**< XML file (relative path) to use for this action, Default file is used when not passed */
}pmsgIfaceCommand;


typedef enum 
{
	PMSG_MAIN_TASK,/* A generic command sent to the main task */	
	PMSG_DIALUP_TASK, /* A generic command sent to dial up task */	
	PMSG_REMOTE_TASK, /* A generic command sent to a remote task (example: base) */
}pmsgCmdType;

typedef struct
{
	char address[BT_MAX_ADDRESS];
	int profile;
	int inquiry_time;
}PRIVNET_BT_SURVEY_CMD;

/* Indicate who is going to handle a given task */
typedef enum 
{
	netSvcLocal, //The command will be handled the caller
	netSvcRemote //The command will be sent to the remote task	
}netSvcManager;

int is_localCall( netSvcManager handler ); 

/** Wait MS */
int pmeg_expired_ms( int *timeout_ms );


/** Socket commands */
int pmsg_socketMsgBind( const uint8 *socket_file_name );
int pmsg_socketMsgAccept( int socketval );
int pmsg_clientWriteMsg( int socketid, const uint8 *fcname, uint8 *i_parm, uint16 i_len, uint16 o_len );
int pmsg_clientReadMsg( int socketid, uint8 *o_parm, uint16 o_len );
pmsgCmdType pmsg_getCOMSocket( const char* p_iface );
pmsgClientCmdData pmsg_serverReadMsg( int socketid );
int pmsg_serverSendMsg( int socketid, uint16 status, uint8 *o_parm, uint16 o_len );
PRIVNET_STATUS pmsg_command( const uint8 *fcname, pmsgCmdType cmdtype, void * i_parm, uint16 i_len, void* o_parm, uint16 o_len );
PRIVNET_STATUS pmsg_command_address( const uint8 *fcname, const uint8 *address, void * i_parm,
                                          uint16 i_len, void* o_parm,
                                          uint16 o_len );

void pnet_postEvent( const char *evt_iface, int eventType );

/** Used to create a socat between a unix socket and tty device */
int pmsg_socatUnixListen( const char* unixfile, const char *tty );
int pmsg_socatUnixOpen( const char* unixfile, const char *tty );
int pmsg_socatTTYForward( const char *tty0, const char *tty1 );


#define NET_MSG_API (netSvcManager handler,  void *i_param, uint16 i_len, void *o_param, uint16 o_len);

/************** ifconfig functions ************************/
PRIVNET_STATUS pnet_getSyslogLevel_msg         NET_MSG_API;
PRIVNET_STATUS pnet_interfaceSet_msg           NET_MSG_API;
PRIVNET_STATUS pnet_interfaceGet_msg           NET_MSG_API;  
PRIVNET_STATUS pnet_interfaceModemSet_msg      NET_MSG_API; 
PRIVNET_STATUS pnet_interfaceModemGet_msg      NET_MSG_API;
PRIVNET_STATUS pnet_interfaceModemIsdnSet_msg  NET_MSG_API;
PRIVNET_STATUS pnet_interfaceModemIsdnGet_msg  NET_MSG_API;
PRIVNET_STATUS pnet_interfaceGprsSet_msg       NET_MSG_API;
PRIVNET_STATUS pnet_interfaceGprsGet_msg       NET_MSG_API;
PRIVNET_STATUS pnet_optionSet_msg              NET_MSG_API;
PRIVNET_STATUS pnet_optionGet_msg              NET_MSG_API;
PRIVNET_STATUS pnet_interfaceInfo_msg          NET_MSG_API;
PRIVNET_STATUS pnet_interfaceGetList_msg       NET_MSG_API;
PRIVNET_STATUS pnet_interfaceGetListIpv6_msg   NET_MSG_API;
PRIVNET_STATUS pnet_interfaceAvailableList_msg NET_MSG_API;
PRIVNET_STATUS pnet_gprsVerifPin_msg           NET_MSG_API;
PRIVNET_STATUS pnet_gprsGetPinCounter_msg      NET_MSG_API;
PRIVNET_STATUS pnet_gprsChangePin_msg          NET_MSG_API;
PRIVNET_STATUS pnet_gprsUnlockPin_msg          NET_MSG_API;
PRIVNET_STATUS pnet_gprsGetInfo_msg            NET_MSG_API;
PRIVNET_STATUS pnet_gprsGetRssi_msg            NET_MSG_API;
PRIVNET_STATUS pnet_gprsGetSimStatus_msg       NET_MSG_API;
PRIVNET_STATUS pnet_gprsGetStatus_msg          NET_MSG_API;
PRIVNET_STATUS pnet_gprsPowerOn_msg            NET_MSG_API;
PRIVNET_STATUS pnet_gprsPowerOff_msg           NET_MSG_API;
PRIVNET_STATUS pnet_gprsSiteSurvey_msg         NET_MSG_API
PRIVNET_STATUS pnet_gprsCellSurvey_msg         NET_MSG_API;
PRIVNET_STATUS pnet_gprsOperatorName_msg       NET_MSG_API;
PRIVNET_STATUS pnet_gprsOperatorLAI_msg        NET_MSG_API;
PRIVNET_STATUS pnet_gprsSendAtCommand_msg      NET_MSG_API;
PRIVNET_STATUS pnet_getPPPExistStatus_msg      NET_MSG_API;
PRIVNET_STATUS pnet_sendSMS_msg                NET_MSG_API;
PRIVNET_STATUS pnet_getRadioTechnology_msg     NET_MSG_API;
PRIVNET_STATUS pnet_createBridge_msg           NET_MSG_API;
PRIVNET_STATUS pnet_deleteBridge_msg           NET_MSG_API;
PRIVNET_STATUS pnet_gprsSelectSim_msg          NET_MSG_API;
PRIVNET_STATUS pnet_gprsSatNavSendCommand_msg  NET_MSG_API;
PRIVNET_STATUS pnet_usbGadgetSetConfig_msg     NET_MSG_API;
PRIVNET_STATUS pnet_usbGadgetGetConfig_msg     NET_MSG_API;
PRIVNET_STATUS pnet_interfaceGetAttr_msg       NET_MSG_API;
PRIVNET_STATUS pnet_interfaceSetAttr_msg       NET_MSG_API;
PRIVNET_STATUS pnet_getSyslogLevel_msg         NET_MSG_API;
PRIVNET_STATUS pnet_sessionUpdateInterface_msg NET_MSG_API;
PRIVNET_STATUS pnet_addHostRouteIpv6_msg       NET_MSG_API;
PRIVNET_STATUS pnet_delHostRouteIpv6_msg       NET_MSG_API;
PRIVNET_STATUS pnet_addNetRouteIpv6_msg        NET_MSG_API;
PRIVNET_STATUS pnet_delNetRouteIpv6_msg        NET_MSG_API;
PRIVNET_STATUS pnet_setDefaultRouteIpv6_msg    NET_MSG_API;

PRIVNET_STATUS pnet_modem_disconnect_msg       NET_MSG_API;
PRIVNET_STATUS pnet_modem_connect_msg          NET_MSG_API;
PRIVNET_STATUS pnet_gprs_disconnect_msg        NET_MSG_API;
PRIVNET_STATUS pnet_gprs_connect_msg           NET_MSG_API;

PRIVNET_STATUS pnet_isdn_disconnect_msg        NET_MSG_API;
PRIVNET_STATUS pnet_isdn_connect_msg           NET_MSG_API;

PRIVNET_STATUS pnet_interfaceSetIpv6_msg       NET_MSG_API;
PRIVNET_STATUS pnet_interfaceGetIpv6_msg       NET_MSG_API;  
PRIVNET_STATUS pnet_interfaceInfoIpv6_msg      NET_MSG_API;

PRIVNET_STATUS i_eth_connect_msg               NET_MSG_API;
PRIVNET_STATUS i_wifi_connect_msg              NET_MSG_API;
PRIVNET_STATUS i_ethipv6_connect_msg           NET_MSG_API;
PRIVNET_STATUS i_wifiipv6_connect_msg          NET_MSG_API;


/******************************** PUB DHCP FUNCTIONS ********************************************/
PRIVNET_STATUS pnet_dhcpStart_msg              NET_MSG_API;
PRIVNET_STATUS pnet_dhcpRenew_msg              NET_MSG_API;
PRIVNET_STATUS pnet_dhcpRelease_msg            NET_MSG_API;
PRIVNET_STATUS pnet_dhcpStop_msg               NET_MSG_API;
PRIVNET_STATUS pnet_setNTP_msg                 NET_MSG_API;
PRIVNET_STATUS pnet_setSyslogLevel_msg         NET_MSG_API;


/******************************** PUB DNS FUNCTIONS ********************************************/
PRIVNET_STATUS pnet_nslookup_msg               NET_MSG_API;
PRIVNET_STATUS pnet_nslookupIpv6_msg           NET_MSG_API;
PRIVNET_STATUS pnet_setDNS_msg                 NET_MSG_API;
PRIVNET_STATUS pnet_flushDNS_msg               NET_MSG_API;
PRIVNET_STATUS pnet_removeDNS_msg              NET_MSG_API;


/******************************** PUB ROUTE FUNCTIONS ********************************************/
PRIVNET_STATUS pnet_nslookup_msg              NET_MSG_API;
PRIVNET_STATUS pnet_nslookupIpv6_msg          NET_MSG_API;
PRIVNET_STATUS pnet_routeGetTable_msg         NET_MSG_API;
PRIVNET_STATUS pnet_routeGetTableIpv6_msg     NET_MSG_API;
PRIVNET_STATUS pnet_addHostRoute_msg          NET_MSG_API;
PRIVNET_STATUS pnet_delHostRoute_msg          NET_MSG_API;
PRIVNET_STATUS pnet_addNetRoute_msg           NET_MSG_API;
PRIVNET_STATUS pnet_delNetRoute_msg           NET_MSG_API;
PRIVNET_STATUS pnet_autoSetDefault_msg        NET_MSG_API;
PRIVNET_STATUS pnet_setDefaultRoute_msg       NET_MSG_API;
PRIVNET_STATUS pnet_getDefaultRoute_msg       NET_MSG_API;
PRIVNET_STATUS pnet_addRouteXml_msg           NET_MSG_API;


/******************************** PUB PING FUNCTIONS ********************************************/
PRIVNET_STATUS pnet_sendping_msg              NET_MSG_API;
PRIVNET_STATUS pnet_sendpingIpv6_msg          NET_MSG_API;


/******************************** PUB BLUETOOTH FUNCTIONS ********************************************/
PRIVNET_STATUS pnet_bluetoothPower_msg                NET_MSG_API;
PRIVNET_STATUS pnet_bluetoothPairMethod_msg           NET_MSG_API;
PRIVNET_STATUS pnet_bluetoothPair_msg                 NET_MSG_API;
PRIVNET_STATUS pnet_bluetoothConnectPAN_msg           NET_MSG_API;
PRIVNET_STATUS pnet_bluetoothUnpair_msg               NET_MSG_API;
PRIVNET_STATUS pnet_bluetoothDisconnectPAN_msg        NET_MSG_API;
PRIVNET_STATUS pnet_bluetoothConnectDUN_msg           NET_MSG_API;
PRIVNET_STATUS pnet_bluetoothDisconnectDUN_msg        NET_MSG_API;
PRIVNET_STATUS pnet_bluetoothConnectSPP_msg           NET_MSG_API;
PRIVNET_STATUS pnet_bluetoothDisconnectSPP_msg        NET_MSG_API;
PRIVNET_STATUS pnet_bluetoothSetInfo_msg              NET_MSG_API;
PRIVNET_STATUS pnet_bluetoothGetInfo_msg              NET_MSG_API;
PRIVNET_STATUS pnet_bluetoothGetPairedList_msg        NET_MSG_API;
PRIVNET_STATUS pnet_bluetoothSurvey_msg               NET_MSG_API;
PRIVNET_STATUS pnet_bluetoothIBeaconAdvertise_msg     NET_MSG_API;
PRIVNET_STATUS pnet_ibeaconGetInfo_msg                NET_MSG_API;
PRIVNET_STATUS pnet_bluetoothIBeaconStopAdvertise_msg NET_MSG_API;
PRIVNET_STATUS pnet_bluetoothRemoveKey_msg            NET_MSG_API;
PRIVNET_STATUS pnet_bluetoothSaveKey_msg              NET_MSG_API;
PRIVNET_STATUS pnet_bluetoothSetEthCable_msg          NET_MSG_API;
PRIVNET_STATUS pnet_bluetoothGetEthCable_msg          NET_MSG_API;
PRIVNET_STATUS pnet_bluetoothWakeUp_msg               NET_MSG_API;
PRIVNET_STATUS pnet_bluetoothSetRadioPower_msg        NET_MSG_API;
PRIVNET_STATUS pnet_BluetoothGetPairScanInfo_msg      NET_MSG_API;


/******************************** PUB WIFI FUNCTIONS ********************************************/
PRIVNET_STATUS pnet_wifiSiteSurvey_msg       NET_MSG_API;
PRIVNET_STATUS pnet_wifiInfo_msg             NET_MSG_API;
PRIVNET_STATUS pnet_wifiSignalStats_msg      NET_MSG_API;
PRIVNET_STATUS pnet_wifiRunCustomCommand_msg NET_MSG_API;
PRIVNET_STATUS pnet_wifiSiteSurvey_msg       NET_MSG_API;
PRIVNET_STATUS pnet_wifiInfo_msg             NET_MSG_API;
PRIVNET_STATUS pnet_wifiSignalStats_msg      NET_MSG_API;
PRIVNET_STATUS pnet_wifiRunCustomCommand_msg NET_MSG_API;


PRIVNET_STATUS pnet_modemInitialize_msg      NET_MSG_API;
PRIVNET_STATUS pnet_modemSendAtCommand_msg   NET_MSG_API;

PRIVNET_STATUS pnet_netInterfaceAction_msg   NET_MSG_API;
PRIVNET_STATUS pnet_netNetworkUpAction_msg   NET_MSG_API;
PRIVNET_STATUS pnet_netNetworkDownAction_msg NET_MSG_API;

PRIVNET_STATUS pnet_defSupplicantFileCreate_msg NET_MSG_API;
PRIVNET_STATUS pnet_defSupplicantFileGetValue_msg NET_MSG_API;

PRIVNET_STATUS pnet_wifiGetFwList_msg NET_MSG_API;
PRIVNET_STATUS pnet_wifiGetFw_msg NET_MSG_API;
PRIVNET_STATUS pnet_wifiSetFw_msg NET_MSG_API;
PRIVNET_STATUS pnet_wifiFreeFwList_msg NET_MSG_API;

/************************** RADIO POWER MANAGEMENT FUNCTIONS ************************************/
PRIVNET_STATUS pnet_wifiPowerOn_msg             NET_MSG_API;
PRIVNET_STATUS pnet_wifiPowerOff_msg            NET_MSG_API;
PRIVNET_STATUS pnet_radioPowerOn_msg            NET_MSG_API;
PRIVNET_STATUS pnet_radioPowerOff_msg           NET_MSG_API;
PRIVNET_STATUS pnet_radioEnterSleep_msg         NET_MSG_API;
PRIVNET_STATUS pnet_radioExitSleep_msg          NET_MSG_API;

/****************************** GENERIC NET INFO FUNCTIONS **************************************/
PRIVNET_STATUS pnet_net_getLinkStatus_msg       NET_MSG_API;
PRIVNET_STATUS pnet_getLinkQuality_msg          NET_MSG_API;
PRIVNET_STATUS pnet_getBaseLinkInfo_msg         NET_MSG_API;

#ifdef __cplusplus
}
#endif

#endif

