/*****************************************************************************
* Copyright (c) 2010 VeriFone Inc.
* 2099 Gateway Place, Suite 600
* San Jose, CA. 95110
*
* All Rights Reserved. No part of this software may be reproduced,
* transmitted, transcribed, stored in a retrieval system, or
* translated into any language or computer language, in any form
* or by any means electronic, mechanical, magnetic, optical,
* chemical, manual or otherwise, without the prior permission of
* VeriFone Inc.
*
******************************************************************************/

#ifndef _PUB_PRIVNET_
#define _PUB_PRIVNET_

#include "privnet_typeconv.h"
#include "svc_net.h"
#include "gsm/gsm.h"

/** Define the modem names */
#define MDM_PSTN   NET_MONDEM_PSTN
#define MDM_UXPSTN NET_MONDEM_UXPSTN
#define MDM_UXISDN NET_MONDEM_UXISDN
#define GPRS_IFACE NET_GPRS_LAYER

#define IS_LOOPBACK_IFACE(iface) (!strcasecmp((iface), "lo"))
#define IS_ETH_IFACE(iface) (!strncasecmp((iface), "eth", 3))
#define IS_PAN_IFACE(iface) (!strncasecmp((iface), "bnep", 4))
#define IS_USB_IFACE(iface) (!strncasecmp((iface), "usb", 3))
#define IS_BRIDGE_IFACE(iface) ((!strcasecmp((iface), NET_BRIDGE_NAME)) || \
			       (!strncasecmp((iface), "br", 2)))
#define IS_ETHERNET_IFACE(iface) ( IS_ETH_IFACE(iface) || IS_BRIDGE_IFACE(iface) || \
				 IS_PAN_IFACE(iface) || IS_USB_IFACE(iface) )
#define IS_WIFI_IFACE(iface) (!strncasecmp((iface), "wlan", 4))
#define IS_PPP_IFACE(iface) (!strncasecmp((iface), "ppp", 3))
#define IS_MODEM_IFACE(iface) (!strcasecmp((iface), MDM_PSTN ))
#define IS_SERIAL_PORT(iface) (!strcasecmp((iface), "serial_port"))
#define IS_MODEM_UXIFACE(iface) (!strcasecmp((iface), MDM_UXPSTN) || !strcasecmp((iface), MDM_UXISDN ))
#define IS_GPRS_IFACE(iface) (!strcasecmp((iface), GPRS_IFACE))
#define IS_MODEM_BASEIFACE(iface) (!strcasecmp((iface), NET_MONDEM_BASEPSTN ))

#define IS_PPP_SERIAL_TTY( p_iface ) (!strncasecmp( p_iface, "ttyACM", strlen("ttyACM")) ||  !strncasecmp( p_iface, "ttyGS", strlen("ttyGS")))
#define PPP_SERIAL_IFACE "ppp5"


typedef enum
{
	PRIVNET_STAT_OK = 1,//1
	PRIVNET_STAT_YES,
	PRIVNET_STAT_NO,
	PRIVNET_STAT_ERROR,
	PRIVNET_STAT_FILE_ERROR,//5
	PRIVNET_STAT_BAD_PARAMETER,
	PRIVNET_STAT_TABLE_ERROR,
	PRIVNET_STAT_NO_ROUTE,
	PRIVNET_STAT_RESOLVE_ERROR,
	PRIVNET_STAT_SOCKET_ERROR,//10
	PRIVNET_STAT_NO_INTERFACE_UP,
	PRIVNET_STAT_CABLE_NOT_DETECTED,
	PRIVNET_STAT_DNS_LINK_ERROR,
	PRIVNET_STAT_IP_LENGTH_ERROR,
	PRIVNET_STAT_DNS_IP_ERROR,//15
	PRIVNET_STAT_DEF_FILE_ERROR,
	PRIVNET_STAT_DNS_NOT_FOUND,
	PRIVNET_STAT_UNABLE_TO_ADD_DNS,
	PRIVNET_STAT_INTERFACE_NOT_FOUND,
	PRIVNET_STAT_NO_TRANSMIT,//20
	PRIVNET_STAT_PING_NO_ANWSER,
	PRIVNET_STAT_PING_INCOMPLETE,
	PRIVNET_STAT_UNREACHABLE,
	PRIVNET_STAT_INVALID_IP_ADDRESS,
	PRIVNET_STAT_TIMEOUT,//25
	PRIVNET_STAT_DHCP_NOT_RUNNING,
	PRIVNET_STAT_XML_FILE_ERROR,
	PRIVNET_STAT_TASK_ERROR,
	PRIVNET_STAT_MEM_ERROR,	
	PRIVNET_STAT_BAD_HANDLE,//30
	PRIVNET_STAT_BUSY,
	PRIVNET_STAT_SSL_GENERAL_ERROR,
	PRIVNET_STAT_SSL_HANDSHAKE_ERROR,
	PRIVNET_STAT_SSL_CERTIFICATE_ERROR,
	PRIVNET_STAT_SSL_READ_ERROR,//35
	PRIVNET_STAT_WIFI_DISCONNECTED,
	PRIVNET_STAT_COMMAND_NOT_FOUND,
	PRIVNET_STAT_SPEED_ERROR,
	PRIVNET_STAT_MDM_NOLINK,//40
	PRIVNET_STAT_GSM_ERROR,
	PRIVNET_STAT_GSM_NO_SIM,
	PRIVNET_STAT_GSM_OFF,
	PRIVNET_STAT_GSM_PIN_READY,
	PRIVNET_STAT_GSM_TIMEOUT,//45
	PRIVNET_STAT_PIN_NEEDED,
	PRIVNET_STAT_SSL_WRITE_ERROR,
	PRIVNET_STAT_PIN_COUNTER,
	PRIVNET_STAT_BT_NOT_PAIRED,
	PRIVNET_STAT_BT_SW_NOT_FOUND,//50
	PRIVNET_STAT_BT_NOT_AVAIL,
	PRIVNET_STAT_BT_DAEMON_ERR,
	PRIVNET_STAT_BT_DEV_ERR,
	PRIVNET_STAT_BT_ERROR,
	PRIVNET_STAT_EXIST,//55
	PRIVNET_STAT_NO_MEM,
	PRIVNET_STAT_HOST_DOWN,
	PRIVNET_STAT_OP_NOT_SUPPORTED,
	PRIVNET_STAT_IO_ERROR,
	PRIVNET_STAT_NO_DEVICE, //60
	PRIVNET_STAT_MDM_CONERR_NONE,
	PRIVNET_STAT_MDM_CONERR_NO_ANSWER,
	PRIVNET_STAT_MDM_CONERR_NO_CARRIER,
	PRIVNET_STAT_MDM_CONERR_NO_DIALTONE,
	PRIVNET_STAT_MDM_CONERR_USR_HANGUP, //65
	PRIVNET_STAT_MDM_CONERR_BUSY,
	PRIVNET_STAT_MDM_CONERR_SETUP_ERROR,
	PRIVNET_STAT_MDM_CONERR_LINE_IN_USE,
	PRIVNET_STAT_GSM_SMS_TOOLONG,
	PRIVNET_STAT_GSM_SMS_BUSY, //70
	PRIVNET_STAT_NO_EVENTS, //71
	PRIVNET_STAT_UNABLE_TO_ADD_GATEWAY,
	PRIVNET_STAT_INVALID_IPv6_ADDRESS,
	PRIVNET_STAT_INVALID_IPv6_MASK,
	PRIVNET_STAT_XML_EMPTY_FILE_ERROR,
	PRIVNET_STAT_WIFI_ASSOCIATING,
	PRIVNET_STAT_IFACE_DOWN,
	PRIVNET_STAT_IFACE_STARTED,
	PRIVNET_STAT_WIFI_COMPLETED,
	PRIVNET_STAT_DIALUP_CONNECTED,
	PRIVNET_STAT_DIALUP_DISCONNECTED,
	PRIVNET_STAT_DIALUP_OFF,
	PRIVNET_STAT_IFACE_NOT_ACTIVATED,
	PRIVNET_STAT_GPRS_PIN_REQUIRED,
	PRIVNET_STAT_GPRS_PUK_REQUIRED,
	PRIVNET_STAT_GPRS_SIM_ABSENT,
} PRIVNET_STATUS;

typedef enum ipv6_address_
{
  IPV6_ADDR_LINK = 1 << 0,
  IPV6_ADDR_SITE = 1 << 1,
  IPV6_ADDR_UNIQUE = 1 << 2,
  IPV6_ADDR_GLOBAL = 1 << 3,
} ipv6_address_type;
#define IPV6_ADDR_ALL  (IPV6_ADDR_LINK | IPV6_ADDR_SITE | IPV6_ADDR_UNIQUE | IPV6_ADDR_GLOBAL)

#define  PRIVNET_IPV6_MAX_ROUTE  64
#define  PRIVNET_IPV6_WITH_PORT_MAX_SIZE   (IPV6_MASK_MAX_STRING + NET_IPV6_MAX + 8) // 8 is for the port : [ipaddr]:xxxxx []:xxxxx take 8 positions

#define  PRIVNET_MAX_ROUTE       64
#define  PRIVNET_IP_MAX_SIZE     18
#define  PRIVNET_MAX_PIN         16
#define  PRIVNET_IFACE_MAX_SIZE  32
#define  PRIVNET_DNS_MAX_SIZE   128
#define  PRIVNET_FILE_MAX_SIZE  128
#define  PRIVNET_OPTION_MAX_SIZE  NET_OPTION_MAX


/* Modem AT commands functions */
#define  PRIVNET_INIT_NAME_SIZE   32
#define  PRIVNET_INIT_FILE_SIZE  128
#define  PRIVNET_AT_CMD_MAX_SIZE 128

#define PRIVNET_DEFAULT_TMP_NET "/tmp/net"

/* default resolv.conf file */
#define PRIVNET_DEFAULT_RESOLV_CONF "/etc/resolv.conf"	
#define PRIVNET_DEFAULT_TMP_RESOLV_CONF "/tmp/etc/resolv.conf"

/* socket used during the communications */
#define PRIVNET_SOCKET_FILE "/tmp/socketname"
#define PRIVNET_SOCKET_FILE_GPRS "/tmp/socketnamegprs"

/** Number of seconds to wait for the PIN completion */
#define GSM_PIN_TIMEOUT     20
#define GSM_CONNECT_TIMEOUT 45
#define GSM_DISCONNECT_TIMEOUT 30
#define GSM_POWER_MAX_S     120
#define GSM_AT_CMD_MAX_SIZE 128
#define GSM_AT_RESP_MAX_SIZE 4000
#define GSM_STR_SMS_MSG     165

#define BT_MAX_ADDRESS     18
#define BT_MAX_NAME        256

#define DHCPID_MAX_EXT      64
#define DHCPCLI_MAX_EXT     64
#define MTU_MAX_EXT         16
#define COUNTRY_MAX_EXT     32

/** Define default route information
 * @ingroup OSsvc_net */
typedef struct
{
	uint8 interface[PRIVNET_IFACE_MAX_SIZE];/**< device name*/
}PRIVNET_DEF_INFO;


#if defined(__cplusplus) && !defined(__DEBUG__)
extern "C"
{
#endif

/* --------- To dispatch -------------------*/
PRIVNET_STATUS btToNetStat( int err_no );


/*----------------------- Routing Functions ---------------------------------*/
PRIVNET_STATUS PRIVNET_GetTable(struct netRoute *rList, uint16 pMax, uint16 *pSize );	
PRIVNET_STATUS PRIVNET_GetRoutes( const uint8*pDest, struct netRoute *rList, uint16 pMax, uint16 *pSize );
PRIVNET_STATUS PRIVNET_AddHostRoute( const uint8*pDest, const uint8*pGateway, const uint8*pIface );
PRIVNET_STATUS PRIVNET_DelHostRoute( const uint8*pDest );
PRIVNET_STATUS PRIVNET_AddNetRoute( const uint8*pNet, const uint8*pNetmask, const uint8*pGateway, const uint8*pIface );
PRIVNET_STATUS PRIVNET_DelNetRoute( const uint8*pNet, const uint8*pNetmask );

PRIVNET_STATUS PRIVNET_AddNetRouteIpv6( const uint8*pNet, const uint8 netmask, const uint8*pGateway, const uint8*pIface );
PRIVNET_STATUS PRIVNET_DelNetRouteIpv6( const uint8*f_pNet, const uint8 f_pNetmask );
PRIVNET_STATUS PRIVNET_AddHostRouteIpv6( const uint8*pDest, const uint8*pGateway, const uint8*pIface );
PRIVNET_STATUS PRIVNET_DelHostRouteIpv6( const uint8*pDest );
PRIVNET_STATUS PRIVNET_GetTableIpv6( struct netRouteIpv6 *f_pList, uint16 f_wMax, uint16 *f_pwSize );
PRIVNET_STATUS PRIVNET_GetRoutesIpv6( const uint8 * pDest, struct netRouteIpv6 * pList, uint16 max, uint16 * pSize );

/*----------------------- Default route --------------------------------------*/
PRIVNET_STATUS PRIVNET_AutoSetDefaultRoute( void );
PRIVNET_STATUS PRIVNET_SetDefaultRoute( const uint8*pIface );
PRIVNET_STATUS PRIVNET_GetDefaultRouteInfo( PRIVNET_DEF_INFO *pDefInfo );

PRIVNET_STATUS PRIVNET_SetDefaultRouteIpv6( const uint8*f_pIface );
PRIVNET_STATUS PRIVNET_AutoSetDefaultRouteIpv6( void );
PRIVNET_STATUS PRIVNET_GetDefaultRouteIPv6Info( PRIVNET_DEF_INFO *f_pDefInfo );

/*----------------------- Ifconfig functions ---------------------------------*/
PRIVNET_STATUS PRIVNET_Ifconfig( struct netIfconfig *rList, uint16 pMax, uint16 *pSize );
PRIVNET_STATUS PRIVNET_LinkStatus( const uint8 *f_pIface );
PRIVNET_STATUS PRIVNET_InterfaceInfo( const uint8 *f_pIface, struct netIfconfig *f_pInfo );
PRIVNET_STATUS PRIVNET_InterfaceSet( struct netIfconfig *f_pInfo );
PRIVNET_STATUS PRIVNET_InterfaceGet( const uint8 *f_pIface, struct netIfconfig *f_pInfo );
int PRIVNET_isInterfacePresent( const uint8 *f_pIface );
PRIVNET_STATUS PRIVNET_InterfaceModemSet( const char *f_pIface, struct netModemInfo *f_mdmInfo );
PRIVNET_STATUS PRIVNET_InterfaceModemGet( const uint8 *f_pIface, struct netModemInfo *f_mdmInfo );
PRIVNET_STATUS PRIVNET_OptionSet( const char *f_pIface, const char *f_option );
PRIVNET_STATUS PRIVNET_OptionGet( const uint8 *f_pIface, struct optionDescriptor *f_optInfo );
int compareIfconfig( const char *p_iface, struct netIfconfig *ifa, struct netIfconfig *ifb );
PRIVNET_STATUS PRIVNET_InterfaceGetAttr( int comtype, char *name_id, char *attrib, char **value );
PRIVNET_STATUS PRIVNET_InterfaceSetAttr( int comtype, char *name_id, char *attrib, char *value );

PRIVNET_STATUS PRIVNET_HotplugAction( char *p_iface, int eventtype );

PRIVNET_STATUS PRIVNET_IfconfigIpv6( struct netIfconfigIpv6 *f_pList, uint16 f_pMax, uint16 *f_pSize );
PRIVNET_STATUS PRIVNET_InterfaceSetIpv6( struct netIfconfigIpv6 *f_pInfo );
PRIVNET_STATUS PRIVNET_InterfaceGetIpv6( const uint8 *f_pIface, struct netIfconfigIpv6 *f_pInfo );
PRIVNET_STATUS PRIVNET_InterfaceInfoIpv6( const uint8 *f_pIface, struct netIfconfigIpv6 *f_pInfo );
PRIVNET_STATUS PRIVNET_InterfaceEthernetHotplugIpv6( const uint8 *f_pIface, int ethon );

/*-------------------------- DHCP functions ----------------------------------*/
PRIVNET_STATUS PRIVNET_DHCP_Start( const uint8 *pIface );
PRIVNET_STATUS PRIVNET_DHCP_Renew( const uint8 *pIface );
PRIVNET_STATUS PRIVNET_DHCP_Release( const uint8 *pIface );
PRIVNET_STATUS PRIVNET_DHCP_Stop( const uint8 *pIface );
PRIVNET_STATUS PRIVNET_IsDHCPRunning( const uint8 *f_pIface );
PRIVNET_STATUS PRIVNET_InterfaceSetup( const uint8 *pIface, int async );
PRIVNET_STATUS PRIVNET_InterfaceDeconfig( const uint8 *pIface, int stopInterface );
PRIVNET_STATUS PRIVNET_UpdateInterfaceSetup( const uint8 *pIface );
PRIVNET_STATUS PRIVNET_CheckEthernetCable( const uint8 *pIface );
PRIVNET_STATUS PRIVNET_setDefaultFileFormat( int f_fileType );
int PRIVNET_SecurityScan( const char *buf );
PRIVNET_STATUS PRIVNET_doSetNTP( const uint8 *hostname );
void stopIfplugd( const uint8 *f_pIface );
void startIfplugd( const uint8 *f_pIface );
PRIVNET_STATUS PRIVNET_InterfaceUpdate( struct netIfconfig *f_pInfo );
PRIVNET_STATUS PRIVNET_pppConnect( const char *iface );
PRIVNET_STATUS PRIVNET_pppConnectFromStruct( struct netIfconfig *f_pInfo );
PRIVNET_STATUS PRIVNET_modemStatus( int *status );
PRIVNET_STATUS PRIVNET_modemStatusExt( int *status, int local );
PRIVNET_STATUS PRIVNET_modemConnect( const char *pppPort );
PRIVNET_STATUS PRIVNET_pppDisconnect( const char *f_pIface );
PRIVNET_STATUS PRIVNET_modemDisconnect( void );
PRIVNET_STATUS PRIVNET_modemDisconnectExt( int local );
PRIVNET_STATUS PRIVNET_updateConnectCounter( uint8 com_type, int increaseType );
PRIVNET_STATUS PRIVNET_getConnectCounter( uint8 com_type, int *counter );
PRIVNET_STATUS PRIVNET_modemConnectFromStructWithOptions( struct netModemInfo *f_mdmInfo, const uint8 *option, int local );
PRIVNET_STATUS PRIVNET_gprsConnectFromStructWithOptions( struct netGprsInfo *f_gInfo, const uint8 *option, int local );
PRIVNET_STATUS PRIVNET_InterfaceGprsGet( const uint8 *f_pIface, struct netGprsInfo *f_gprsInfo );
PRIVNET_STATUS PRIVNET_InterfaceGprsSet( const char *f_pIface, struct netGprsInfo *f_gprsInfo );
PRIVNET_STATUS PRIVNET_modemIsdnConnectFromStructWithOptions( struct netModemIsdnInfo *f_mdmInfo, const uint8 *option, int local );
PRIVNET_STATUS PRIVNET_InterfaceModemIsdnGet( const uint8 *f_pIface, struct netModemIsdnInfo *f_mdmInfo );
PRIVNET_STATUS PRIVNET_InterfaceModemIsdnSet( const char *f_pIface, struct netModemIsdnInfo *f_mdmInfo );
PRIVNET_STATUS PRIVNET_satnavSendCommand( struct netSatNavCommand *satCmd );
PRIVNET_STATUS PRIVNET_modemInitialize( const uint8 *modemName, uint8 *f_modemParamfiles, int local );
PRIVNET_STATUS PRIVNET_modemSendAtCommand( char *tx, char *rx, int rx_size, int max_retries, int response_timeout, int local );
PRIVNET_STATUS PRIVNET_modemGetLastError();
PRIVNET_STATUS PRIVNET_getPPPExitStatus( int *status );
PRIVNET_STATUS PRIVNET_createDefaultNetconfXML(void);
PRIVNET_STATUS PRIVNET_createDefaultInterfaces(void);

PRIVNET_STATUS PRIVNET_DHCP_StartIpv6( const uint8 *pIface );
PRIVNET_STATUS PRIVNET_DHCP_RenewIpv6( const uint8 *pIface );
PRIVNET_STATUS PRIVNET_DHCP_ReleaseIpv6( const uint8 *pIface );
PRIVNET_STATUS PRIVNET_DHCP_StopIpv6( const uint8 *pIface );
PRIVNET_STATUS PRIVNET_InterfaceSetupIpv6( const uint8 *pIface );
PRIVNET_STATUS PRIVNET_InterfaceDeconfigIpv6( const uint8 *pIface, int stopInterface );
void PRIVNET_InterfaceRemoveAddressIpv6(const uint8 * pIface, ipv6_address_type addr);
PRIVNET_STATUS PRIVNET_InterfaceUpdateIpv6( struct netIfconfigIpv6 * pInfo );
int PRIVNET_disableIpv6Syctl(const uint8 * pIface, int status);
int PRIVNET_removeOldConfigFilesIpv6(const char * f_pIface);

/*-------------------------- DNS functions -----------------------------------*/
PRIVNET_STATUS PRIVNET_Nslookup( const uint8 *pDnsName, const uint8 *pIface, uint8 *pIP, uint8 pIPSize );
PRIVNET_STATUS PRIVNET_SetDNS( const uint8 *f_pIface, const uint8 *f_pDNS1, const uint8 *f_pDNS2 );
PRIVNET_STATUS PRIVNET_FlushDNSCache(int ipVersion);
PRIVNET_STATUS PRIVNET_RemoveDNSCache( const uint8 *f_pDNS, int ipVersion );
PRIVNET_STATUS PRIVNET_ConcatDNSFiles(const uint8 * pIface, int lpVersion );

PRIVNET_STATUS PRIVNET_NslookupIpv6( const uint8 *pDnsName, const uint8 *pIface, uint8 *pIP, uint8 pIPSize );
PRIVNET_STATUS PRIVNET_SetIpv6DNS( const uint8 *f_pIface, const uint8 *f_pDNS1, const uint8 *f_pDNS2 );

/*-------------------------- PING functions -----------------------------------*/
PRIVNET_STATUS PRIVNET_Ping( const char *host, uint8 f_MaxPing, struct netPingInfo *f_pInfo );

/*-------------------------- Socket functions --------------------------------*/
int PRIVNET_tcpConnect( const uint8 *remote_ip, uint32 f_portno, const uint8 *f_szIface, uint32 f_dwTcpBufSize, uint32 f_dwTimeout, uint8* ipout, uint16 ipoutlen );
int PRIVNET_tcpClose( int f_socket );

/*-------------------------- XML file functions ------------------------------*/
PRIVNET_STATUS PRIVNET_AddRouteFromXML( const uint8 *f_szFilename );

/*-------------------------- WIFI functions ------------------------------*/
PRIVNET_STATUS PRIVNET_startWIFI( const char *f_szSuppliConf, int startwpa );
PRIVNET_STATUS PRIVNET_stopWIFI( int stopmodule );
struct netSiteSurveyList PRIVNET_wifiSiteSurvey( uint16 siteCount, PRIVNET_STATUS *net_stat );
PRIVNET_STATUS PRIVNET_wifiInfo( const uint8 *f_pIface, struct netWifiInfo *wifiInfo );
PRIVNET_STATUS PRIVNET_wifiStatus( const uint8 *f_pIface );
PRIVNET_STATUS PRIVNET_localwifiStatus( const uint8 *f_pIface );/* not using the control */
PRIVNET_STATUS PRIVNET_wifiGetSignalStats( const uint8 *f_pIface, struct netWifiSignalStats *wifiStats );
PRIVNET_STATUS PRIVNET_wifiSiteCount( uint8 removefile, uint16 *wifiSiteCount );
int PRIVNET_isWifiPowered(void);
PRIVNET_STATUS PRIVNET_InterfaceWifiHotplug( const uint8 *f_pIface, int wifion );

#ifdef RAPTOR
PRIVNET_STATUS PRIVNET_wifiRunCustomCommand(struct netWifiCustomCommand custom);
#endif

PRIVNET_STATUS PRIVNET_startWiFiIpv6( const char *f_szSuppliConf, int startwpa );
PRIVNET_STATUS PRIVNET_stopWiFiIpv6( int stopmodule );

/*----------------------------- GPRS Functions --------------------------------- */
PRIVNET_STATUS PRIVNET_gprsPPPConnect( const char *f_pppIface );
PRIVNET_STATUS PRIVNET_gprsPPPDisconnect( const char *f_pppIface );
PRIVNET_STATUS PRIVNET_gprsVerifPin( const char *f_pin );
PRIVNET_STATUS PRIVNET_gprsChangePin( const char *f_oldpin, const char *f_newpin );
PRIVNET_STATUS PRIVNET_gprsUnlockPin( const char *f_pin, const char *f_puk );
PRIVNET_STATUS PRIVNET_gprsGetSimStatus( int *status );
PRIVNET_STATUS PRIVNET_gprsGetStatus( int *status );
PRIVNET_STATUS PRIVNET_gprsGetInfo( struct netGsmInfo *gsmInfo );	
PRIVNET_STATUS PRIVNET_gprsGetRssi( struct netGsmRssi *gsmrssi );
PRIVNET_STATUS PRIVNET_gprsPowerOnModule();	
PRIVNET_STATUS PRIVNET_gprsPowerOffModule();	
PRIVNET_STATUS PRIVNET_gprsOperatorName( struct netOperator *f_pOpName );
PRIVNET_STATUS PRIVNET_gprsOperatorLAI( struct netOperator *f_pOpName );
PRIVNET_STATUS PRIVNET_gprsSiteSurvey( struct netOperator *f_pOpList, int f_pMax, uint16 *f_pSize );
PRIVNET_STATUS PRIVNET_InterfaceEthernetHotplug( const uint8 *f_pIface, int ethon );
PRIVNET_STATUS PRIVNET_gprsGetPinCounter( int *f_attempt );
PRIVNET_STATUS PRIVNET_gprsSendAtCommand( char *tx, char *rx, int rx_size, int response_timeout );
PRIVNET_STATUS PRIVNET_gprsLocalDisconnect();
PRIVNET_STATUS PRIVNET_gprsCsdSend( uint8 *data, uint16 dataLength );
PRIVNET_STATUS PRIVNET_gprsCsdRead( uint8 *data, uint16 indataLength, uint16 *outdataLength, uint16 timeout );
PRIVNET_STATUS PRIVNET_gprsSelectSim( int sim_number );
PRIVNET_STATUS PRIVNET_gprsSendSMS( char *number, char *message );
PRIVNET_STATUS PRIVNET_RadioAccessTechnology( int *rat );
PRIVNET_STATUS PRIVNET_gprsCellSurvey( struct netCellIdExt *cell_id, int f_pMax, uint16 *f_pSize );
PRIVNET_STATUS GSM_gprsCellSurvey( struct netCellIdExt *cell_id, int f_pMax, uint16 *f_pSize );
PRIVNET_STATUS GSM_gprsGetPinAttempts( int *attempts );
PRIVNET_STATUS GSM_gprsGetSimStatus( int *status );

/*----------------------------- BRIDGE Functions --------------------------------- */
PRIVNET_STATUS PRIVATE_createBridge( char *iface0, char *iface1 );
PRIVNET_STATUS PRIVATE_deleteBridge( char *iface0, char *iface1 );

/*----------------------------- Bluetooth Functions --------------------------------- */
PRIVNET_STATUS PRIVATE_bluetoothPower( int operation );
PRIVNET_STATUS PRIVATE_bluetoothPair( char *bt_device, char *pin );
PRIVNET_STATUS PRIVATE_bluetoothConnectPAN( char *bt_device );
PRIVNET_STATUS PRIVATE_bluetoothDisconnectPAN( char *bt_device );
PRIVNET_STATUS PRIVATE_bluetoothConnectDUN( char *bt_device );
PRIVNET_STATUS PRIVATE_bluetoothDisconnectDUN( char *bt_device );
PRIVNET_STATUS PRIVATE_bluetoothConnectSPP( char *bt_device, int channel );
PRIVNET_STATUS PRIVATE_bluetoothDisconnectSPP( char *bt_device, int channel );
PRIVNET_STATUS PRIVATE_bluetoothUnpair( char *bt_device );
PRIVNET_STATUS PRIVATE_bluetoothGetInfo( char *bt_device, struct bluetoothDescriptor *btInfo );
PRIVNET_STATUS PRIVATE_bluetoothSetInfo( struct bluetoothDescriptor *btInfo );
PRIVNET_STATUS PRIVATE_bluetoothStatus( char *bt_device, struct netBluetoothStatus* pBtStatus );
PRIVNET_STATUS PRIVATE_bluetoothScan( struct netBluetoothDevList* pList );
PRIVNET_STATUS PRIVATE_bluetoothSurvey( char *deviceAddr, int profile, int inquiry_time, struct netBluetoothList* pList );
PRIVNET_STATUS PRIVATE_bluetoothSaveKey( char* base_id );
PRIVNET_STATUS PRIVATE_bluetoothRemoveKey( char* base_id );
PRIVNET_STATUS PRIVATE_bluetoothGetLQ( char* base_id, int* lq );
PRIVNET_STATUS PRIVATE_bluetoothGetRssi( char* base_id, int* rssi );
PRIVNET_STATUS PRIVATE_bluetoothSetPairingMethod( int method );
PRIVNET_STATUS PRIVATE_bluetoothGetAPInfo( char* bt_device, struct netBluetoothApInfo* nfo );
PRIVNET_STATUS PRIVATE_bluetoothIBeaconStartAdvertise( struct netIBeacon *iBeacon, int startMode );
PRIVNET_STATUS PRIVATE_bluetoothIBeaconGetInfo( struct netIBeacon *iBeacon );
PRIVNET_STATUS PRIVATE_bluetoothIBeaconStopAdvertise( void );
PRIVNET_STATUS PRIVATE_bluetoothPairedList( struct netBluetoothDevList* pList );
PRIVNET_STATUS PRIVATE_bluetoothEnableIncomingSSP( int timeout );
PRIVNET_STATUS PRIVATE_bluetoothDisableIncomingSSP( );
PRIVNET_STATUS PRIVATE_bluetoothGetSSPRequest( struct netBluetoothSSPRequest * req );
PRIVNET_STATUS PRIVATE_bluetoothConfirmSSPRequest( const char *remoteDeviceAddress, const char *pairingCodeString );
PRIVNET_STATUS PRIVATE_bluetoothGetName( const char* address, char* name, size_t name_len);
PRIVNET_STATUS PRIVATE_bluetoothWake( );
PRIVNET_STATUS PRIVATE_bluetoothCheckEthernetCable( const char* pan_iface );
void PRIVATE_bluetoothSetEthernetCableStatus( PRIVNET_STATUS stat );
PRIVNET_STATUS PRIVATE_bluetoothGetEthernetCableStatus( );

/*-------------------------- USB Functions ------------------------------*/
PRIVNET_STATUS PRIVNET_usbGadgetSetConfig( uint8 gadget );
PRIVNET_STATUS PRIVNET_usbGadgetGetConfig( uint8 *gadget );


/** Add ack functions */
int PRIVNET_InterfaceWaitAckGet( const char *piface );
void PRIVNET_InterfaceWaitAckSet( const char *piface, int value );
PRIVNET_STATUS PRIVNET_setSyslogLevel( int level );
int PRIVNET_getSyslogLevel();


/********* NEW FUNCTUIONS ***********/

PRIVNET_STATUS pnet_delHostRoute( const uint8*f_pHost );
PRIVNET_STATUS pnet_addNetRoute( const uint8*f_pNet, const uint8*f_pNetmask, const uint8*f_pGateway, const uint8*f_pIface );
PRIVNET_STATUS pnet_delNetRoute( const uint8*f_pNet, const uint8*f_pNetmask );
PRIVNET_STATUS pnet_autoSetDefaultRoute( void );
PRIVNET_STATUS pnet_addHostRoute( const uint8*f_pHost, const uint8*f_pGateway, const uint8*f_pIface );

PRIVNET_STATUS pnet_addHostRouteIpv6(const uint8 * pHost, const uint8 * pGateway, const uint8 * pIface );
PRIVNET_STATUS pnet_delHostRouteIpv6(const uint8 * pHost);

PRIVNET_STATUS pnet_stopDHCP( const uint8 *f_pIface, int ipVersion );
PRIVNET_STATUS pnet_ifaceUp( const char *p_iface, int showinfo );
PRIVNET_STATUS pnet_assignStaticIPv6( struct netIfconfigIpv6 *f_pDesc );
PRIVNET_STATUS pnet_changeEthernetSpeed( const char *p_iface, int newSpeed );
PRIVNET_STATUS pnet_changeMTU( const char *p_iface, const char *mtu );
void pnet_startIfplugd( const uint8 *f_pIface );
void pnet_stopIfplugd( const uint8 *f_pIface );
int pnet_read_mac( const uint8 *f_pIface, char *mac, int len );
PRIVNET_STATUS pnet_ifaceConfig( struct netIfconfig *f_pDesc, const char *dhcpid,
				 const char *bridge_to, int bridge_uplink );
PRIVNET_STATUS pnet_ifaceDeconfig( struct netIfconfig *f_pDesc, const char *bridge_to, int bridge_uplink );
PRIVNET_STATUS pnet_ifaceDown( const char *p_iface );
PRIVNET_STATUS pnet_ifaceConfigIpv6( struct netIfconfigIpv6 *f_pDesc, const char *dhcpid, const char *clienthostname );
PRIVNET_STATUS pnet_ifaceDeconfigIpv6( struct netIfconfigIpv6 *f_pDesc );
PRIVNET_STATUS pstn_modemConnect( const uint8 *modemname, struct netModemInfo *f_modemInfo, const uint8 *options );
PRIVNET_STATUS isdn_modemConnect( const uint8 *modemname, struct netModemIsdnInfo *f_modemInfo, const uint8 *options );
PRIVNET_STATUS pnet_modemDisconnect( void );
void pnet_gprsDisconnect();
PRIVNET_STATUS pnet_gprsConnect( struct netGprsInfo *gprsInfo, const uint8 *options );
int pnet_cellularExist( void );
PRIVNET_STATUS pnet_pppDown( struct netIfconfig *f_pInfo );
PRIVNET_STATUS pnet_pppUp( struct netIfconfig *f_pInfo, const uint8 *pppdev, int dev_unit, uint8 *p_pppUserOptions );
PRIVNET_STATUS pnet_autoCreateBridge( const char *p_iface );
PRIVNET_STATUS pnet_autoDeleteBridge( const char *p_iface );

PRIVNET_STATUS pnet_dnsUpdateTable( const uint8 *f_pIface );

int pnet_isInterfaceUp( const uint8 *f_pIface );
void pnet_setMacAddress(void);
PRIVNET_STATUS pnet_gprsGetStatus();
int pnet_getEthernetSpeed( const char *p_iface );

void pnet_setInterfaceStarted( const char *piface, int value );
int pnet_getInterfaceStarted( const char *piface );

int pnet_enableIpv6RARSSyctl(const uint8 * pIface, int status);
int pnet_disableIpv6Syctl(const uint8 * pIface, int status);
void pnet_interfaceRemoveAddressIpv6( const uint8 * pIface, ipv6_address_type addr );

PRIVNET_STATUS pnet_readIfconfigIpv6( const char *f_piface, struct netIfconfigIpv6 *f_Desc );
PRIVNET_STATUS pnet_readIfconfig( const char *f_piface, struct netIfconfig *f_Desc );

/*------------------------------- WIFI -------------------------------------------------*/
PRIVNET_STATUS pnet_wifiAuthenticate( const uint8 *f_pIface, uint16 timeout );
PRIVNET_STATUS pnet_wifiStatus( const uint8 *f_pIface );
PRIVNET_STATUS pnet_stopWIFI( int stopmodule );
PRIVNET_STATUS pnet_startWIFI( const char *f_szSuppliConf, int startwpa, const char *f_szCountry );
int pnet_getPowerStateWifi( void );
int pnet_wifiExist( void );


/** Bring all the networks up */
PRIVNET_STATUS pnet_networkDown( void *ifacelist );
PRIVNET_STATUS pnet_networkUp( void *ifacelist );
PRIVNET_STATUS pnet_checkEthernetCable( const char *f_pIface );

/*------------------------------- BLUETOOTH -------------------------------------------------*/
PRIVNET_STATUS pnet_bluetoothWakeUp( struct bluetoothDescriptor *pDesc );
PRIVNET_STATUS pnet_bluetoothIBeaconStartup( void );
int pnet_bluetoothExist( void );

/** XML file parsing */
int pnet_xmldefaultSet( const char *tag_name, const char *attribvalue, void *attrib );
int pnet_xmldefaultGet( const char *tag_name, const char *attribvalue, void *attrib );
int pnet_xmlReadDefIfconfig( const char *nameid, struct netIfconfig *ifconfig );

#if defined(__cplusplus) && !defined(__DEBUG__)
}
#endif

#endif

