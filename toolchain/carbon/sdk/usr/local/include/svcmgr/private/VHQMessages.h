///
/// Copyright 2011 Verifone Inc.  All Rights Reserved.
///
/// This program is confidential, proprietary, and unpublished property of
/// Verifone Inc.  It may NOT be copied or distributed in part or in whole
/// on any medium, either electronic or printed, without the express written
/// consent of Verifone Inc.
///
/// Unpublished -- rights reserved under the copyright laws of the 
/// United States and other countries.
///
/// \file	VHQMsgs.h
///	        Include file for VHQ messages
/// 
// comments that are not part of documentation begin with // instead of ///

#ifndef _VHQ_MESSAGES_H
#define _VHQ_MESSAGES_H

#include <sys/stat.h>
#include <time.h>
#ifndef WIN32
#ifndef VHQ_VX
#include <svcInfoAPI.h>
#endif
#endif
#include "VHQOperations.h"
#include "VHQTypes.h"

extern int responseSend_Qid;
extern int responseRecv_Qid;
#define QUE_VCL						500			// to VCL - for key info
#define QUE_VCLRSP  				501			// from VCL - for key info
#define QUE_VHQ_ROOT_REQUEST		502
#define QUE_VHQ_ROOT_RESPONSE		503
#define QUE_VHQ_RESPONSE_SEND		504
#define QUE_VHQ_RESPONSE_RECEIVE	505
#define QUE_VHQ_APP_IFC_TO_AGENT	506
#define QUE_VHQ_APP_IFC_TO_APP		507

#define VHQ_RESPONSE_QUEUE_GET_FLAGS (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH)

typedef struct _VHQResponseSendMsg_VERSION_1 /* DO NOT EDIT THIS STRUCTURE - CHANGES SHOULD BE MADE IN A NEW STRUCTURE */
{
	long clientPid;	// msgget/msgsend put this at the beginning of the msg
	operation_identifier_t operation_type;
	int comm_id;
	int message_id;
	char operation_id[16];
	srv_msg_type_t srv_msg_type;
	srv_msg_identifier_t srv_msg_identifier;
	vhq_result_t result;
	bool close_comm_id;
	bool responseExpected;
	/* DO NOT EDIT THIS STRUCTURE - CHANGES SHOULD BE MADE IN A NEW STRUCTURE */
}VHQResponseSendMsg_VERSION_1;  /* DO NOT EDIT THIS STRUCTURE - CHANGES SHOULD BE MADE IN A NEW STRUCTURE */


typedef struct _VHQResponseSendMsg_VERSION_2 /* DO NOT EDIT THIS STRUCTURE - CHANGES SHOULD BE MADE IN A NEW STRUCTURE */
{
	long clientPid;	// msgget/msgsend put this at the beginning of the msg
	operation_identifier_t operation_type;
	int comm_id;
	int message_id;
	char operation_id[64];
	srv_msg_type_t srv_msg_type;
	srv_msg_identifier_t srv_msg_identifier;
	vhq_result_t result;
	bool close_comm_id;
	bool responseExpected;
	/* DO NOT EDIT THIS STRUCTURE - CHANGES SHOULD BE MADE IN A NEW STRUCTURE */
}VHQResponseSendMsg_VERSION_2; /* DO NOT EDIT THIS STRUCTURE - CHANGES SHOULD BE MADE IN A NEW STRUCTURE */


typedef struct _VHQResponseSendMsg
{
	long clientPid;	// msgget/msgsend put this at the beginning of the msg
	operation_identifier_t operation_type;
	int comm_id;
	int message_id;
	char operation_id[64];
	srv_msg_type_t srv_msg_type;
	srv_op_type_t srv_op_type;
	vhq_result_t result;
	bool close_comm_id;
	bool responseExpected;

	uint32 operation_set_id;
	uint32 recur_id;
	uint32 download_attempts;
	/* Mitch (10-17-2013): Be sure to add new values to the end of this structure so the
	variable response version handling in the agent will work */
	

}VHQResponseSendMsg;

typedef struct _VHQResponseReceiveMsg
{
	long clientPid;	// msgget/msgsend put this at the beginning of the msg
	int message_id;
}VHQResponseReceiveMsg;


typedef enum _VHQRootRequestType
{
	VHQ_ROOT_REQUEST_DIR_LIST = 1,
	VHQ_ROOT_REQUEST_REBOOT,
	VHQ_GUI_INFO,
	VHQ_REQUEST_MESSAGE_ID,
	VHQ_ROOT_REQUEST_RTC_VAL,
	VHQ_ROOT_REQUEST_COPY_FILE,
	VHQ_ROOT_REQUEST_FILE_LIST,
	VHQ_ROOT_REQUEST_PPP_RESTART,
	VHQ_ROOT_REQUEST_CONTENT_CALLBACK_SETUP,
	VHQ_ROOT_REQUEST_DO_CALLBACK,
	VHQ_ROOT_REQUEST_SET_CONTENT_RESULT,
	VHQ_ROOT_REQUEST_GET_CONTENT_RESULT,
	VHQ_REQUEST_HB,
	VHQ_REGISTER_APP,
	VHQ_REQUEST_CONFIG_UPDATE,
	VHQ_REQUEST_APP_PARAM_UPDATE,
	VHQ_REQUEST_EVENT_NOTIFICATION,
	VHQ_APP_INTERFACE_MESSAGE,
	/* IMPORTANT: Add new request types to the end of the list or VHQ system mode screen will break */
}VHQRootRequestType;


#define MAX_DIRECTORY_LENGTH			256
#define MAX_DIR_LIST_FILENAME_LENGTH	32

typedef struct _MSG_DIR_LIST_DATA
{
	char szOutFileName[MAX_DIR_LIST_FILENAME_LENGTH];
	char szDirectory[MAX_DIRECTORY_LENGTH];
}MSG_DIR_LIST_DATA;

typedef struct _MSG_DIR_LIST_RESPONSE
{
	bool bResult;
}MSG_DIR_LIST_RESPONSE;

typedef struct _VHQGuiInfo
{
	char structVersion;	// 0 for current structure
	uint16 structSize;     // this size of this structure
	char version[16];
	char logpath[128];
	char hbresultpath[128];
	char configfilepath[128];
	bool bHeartbeatsEnabled;
}VHQGuiInfo;

typedef struct _VHQMessageIDReq
{
	int comm_id;
	uint32 rx_msg_id;
	int operation_id;
	bool comm_id_closed;
}VHQMessageIDReq;

typedef struct _VHQMessageIDResp
{
	uint32 msg_id;
}VHQMessageIDResp;

#if !defined(WIN32)

typedef struct _VHQRTCReq
{
	struct timeval rtc_time;
}VHQRTCReq;

typedef struct _VHQRTCResp
{
	struct timeval rtc_time;
}VHQRTCResp;


typedef struct _VHQCopyFileReq
{
	char src[128];
	char dest[128];
}VHQCopyFileReq;

typedef struct _VHQCopyFileResp
{
	int ret_val;
}VHQCopyFileResp;


typedef enum _file_list_t
{
	FILE_LIST_LOG_FILE_LIST,
	FILE_LIST_OS_CONFIG_FILES,
	FILE_LIST_OS_LOG_FILES,
	FILE_LIST_USR_LOG_FILES,
}file_list_t;


typedef struct _VHQFileListReq
{
	file_list_t list_type;
}VHQFileListReq;

typedef struct _VHQFileListResp
{
	bool ret_val;
	char OutFileName[256];
}VHQFileListResp;

typedef struct _VHQContentCBSetupReq
{
	uid_t user_id;
	char content_file[128];
	char content_dest[128];
	int content_type;
	int content_id;
	void (*callback) (char* ContentData);
}VHQContentCBSetupReq;

typedef struct _VHQContentCBSetupResp
{
	int ret_val;
	char content_file[128];
	char content_dest[128];
	int content_type;
	int content_id;
	void (*callback) (char* ContentData);
}VHQContentCBSetupResp;

typedef struct _VHQContentResultReq
{
	uid_t user_id;
	int content_id;
	int content_status;
}VHQContentResultReq;

typedef struct _VHQContentResultResp
{
	int ret_val;
	int content_status;
}VHQContentResultResp;





/* This is the minimum version of the request message supported */
typedef struct _VHQRootRequest_VERSION_1 /* DO NOT EDIT THIS STRUCTURE - CHANGES SHOULD BE MADE IN A NEW STRUCTURE */
{
	long clientPid; // msgget/msgsend put this at the beginning of the msg
	VHQRootRequestType message_type;
	union {
		MSG_DIR_LIST_DATA sDirList;
		VHQMessageIDReq sMdgIdReqData;
		VHQRTCReq sRTCtime;
		VHQCopyFileReq sCopyFiledata;
		VHQFileListReq sFileListData;
		VHQContentCBSetupReq sContentCBData;
		VHQContentResultReq sContentResultReq;
		/* DO NOT EDIT THIS STRUCTURE - CHANGES SHOULD BE MADE IN A NEW STRUCTURE */
	} data;
}VHQRootRequest_VERSION_1; /* DO NOT EDIT THIS STRUCTURE - CHANGES SHOULD BE MADE IN A NEW STRUCTURE */


typedef struct _VHQRootResponseMsg_VERSION_1 /* DO NOT EDIT THIS STRUCTURE - CHANGES SHOULD BE MADE IN A NEW STRUCTURE */
{
	long clientPid; // msgget/msgsend put this at the beginning of the msg
	VHQRootRequestType message_type;
	union {
		MSG_DIR_LIST_RESPONSE sDirListResponse;
		VHQGuiInfo sGuiInfo;
		VHQMessageIDResp sMsgIdResp;
		VHQRTCResp sRTCtime;
		VHQCopyFileResp sCopyFileResp;
		VHQFileListResp sFileListResp;
		VHQContentCBSetupResp sContentCBResp;
		VHQContentResultResp sContentResultResp;
		/* DO NOT EDIT THIS STRUCTURE - CHANGES SHOULD BE MADE IN A NEW STRUCTURE */
	} data;
}VHQRootResponseMsg_VERSION_1; /* DO NOT EDIT THIS STRUCTURE - CHANGES SHOULD BE MADE IN A NEW STRUCTURE */








typedef struct _VHQHBReq
{
	int seconds_to_HB;
}VHQHBReq;

typedef struct _VHQHBResp
{
	int ret_val;
	char HB_resp_file[128];
}VHQHBResp;


typedef struct _VHQRegAppReq
{
	bool reg;
	int event_mask;
	char receiver_name[128];
}VHQRegAppReq;

typedef struct _VHQRegAppResp
{
	int ret_val;
}VHQRegAppResp;



typedef struct _VHQAppParmUpdateReq
{
	char receiver[64];
	char param_file[256];
}VHQAppParmUpdateReq;

typedef struct _VHQAppParmUpdateResp
{
	int ret_val;
}VHQAppParmUpdateResp;

typedef struct _VHQConfigUpdateReq
{
	char new_config_file[256];
}VHQConfigUpdateReq;

typedef struct _VHQConfigUpdateResp
{
	int ret_val;
}VHQConfigUpdateResp;

typedef struct _VHQEventNotifyReq
{
	int event_mask;
	int download_percent;
	char filename[256];
}VHQEventNotifyReq;

typedef struct _VHQEventNotifyResp
{
	int ret_val;
}VHQEventNotifyResp;



typedef struct _VHQAppIfcReq
{
	int status;
	int messageType;
	char fileName[220];
	char fileLocation[64];
	int eventMask;
	int transactionType;
	int downloadPercent;
	int appStatus;
	int numFileEntries;
	unsigned char removeFile;
}VHQAppIfcReq;

typedef struct _VHQAppIfcResp
{
	int status;
	int messageType;
	char fileName[220];
	char fileLocation[64];
	int eventMask;
	int transactionType;
	int downloadPercent;
	int appStatus;
	int numFileEntries;
	unsigned char removeFile;
}VHQAppIfcResp;






typedef struct _VHQRootRequestMsg
{
	/* Mitch (7-30-2013): Be sure to add new values to the end of this structure so the
	variable response version handling in the agent will work */

	long clientPid; // msgget/msgsend put this at the beginning of the msg
	VHQRootRequestType message_type;
	union {
		MSG_DIR_LIST_DATA sDirList;
		VHQMessageIDReq sMdgIdReqData;
		VHQRTCReq sRTCtime;
		VHQCopyFileReq sCopyFiledata;
		VHQFileListReq sFileListData;
		VHQContentCBSetupReq sContentCBData;
		VHQContentResultReq sContentResultReq;
		VHQHBReq sHBReq;
		VHQRegAppReq sRegAppReq;
		VHQConfigUpdateReq sConfigUpdateReq;
		VHQAppParmUpdateReq sAppParmUpdateReq;
		VHQEventNotifyReq sEventNotifyReq;
		VHQAppIfcReq sAppIfcReq;
	} data;
	char receiver[32];
	int messageID;
	bool respondToService;

	/* Mitch (7-30-2013): Be sure to add new values to the end of this structure so the
	variable response version handling in the agent will work */
}VHQRootRequestMsg;







typedef struct _VHQRootResponseMsg
{
	/* Mitch (7-30-2013): Be sure to add new values to the end of this structure so the
	variable response version handling in the agent will work */

	long clientPid; // msgget/msgsend put this at the beginning of the msg
	VHQRootRequestType message_type;
	union {
		MSG_DIR_LIST_RESPONSE sDirListResponse;
		VHQGuiInfo sGuiInfo;
		VHQMessageIDResp sMsgIdResp;
		VHQRTCResp sRTCtime;
		VHQCopyFileResp sCopyFileResp;
		VHQFileListResp sFileListResp;
		VHQContentCBSetupResp sContentCBResp;
		VHQContentResultResp sContentResultResp;
		VHQHBResp sHBResp;
		VHQRegAppResp sRegAppResp;
		VHQConfigUpdateResp sConfigUpdateResp;
		VHQAppParmUpdateResp sAppParmUpdateResp;
		VHQEventNotifyResp sEventNotifyResp;
		VHQAppIfcResp sAppIfcResp;
	} data;
	int messageID;

	/* Mitch (7-30-2013): Be sure to add new values to the end of this structure so the
	variable response version handling in the agent will work */

}VHQRootResponseMsg;
#endif
#endif // _VHQ_MESSAGES_H
