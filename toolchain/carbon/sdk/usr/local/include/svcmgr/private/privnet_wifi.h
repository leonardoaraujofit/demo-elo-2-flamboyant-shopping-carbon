/*
 *	Copyright, 2016 VeriFone Inc.
 *	88 West Plumeria Dr.
 *	San Jose, CA.  95134
 *
 *  All Rights Reserved. No part of this software may be reproduced,
 *  transmitted, transcribed, stored in a retrieval system, or
 *  translated into any language or computer language, in any form
 *  or by any means electronic, mechanical, magnetic, optical,
 *  chemical, manual or otherwise, without the prior permission of
 *  VeriFone Inc.
 */

#ifndef PRIVNET_WIFI_H_
#define PRIVNET_WIFI_H_

#ifdef __cplusplus
extern "C" {
#endif
#include <net/ethernet.h>
#include <linux/wireless.h>
/* by default the survey results list can have DEFAULT_SURVEY_NUMBER entries
 This however is dynamically changed at runtime if too small */
#define DEFAULT_SURVEY_NUMBER 48
#define WLAN_MODE_MASTER				1
#define WLAN_MODE_ADHOC					2

#define WLAN_FLAG_ESSID_VALID			1
#define WLAN_FLAG_FREQUENCY_VALID		2
#define WLAN_FLAG_QUAL_PERCENT_VALID	4
#define WLAN_FLAG_QUAL_DBM_VALID		8
#define WLAN_FLAG_MODE_VALID			10
#define WLAN_FLAG_BSSID_VALID			20

struct wlan_context {
	int ioctl_sock;
	char ifname[10];
	int we_version_compiled;
	int scan_complete_events;
	unsigned char max_level;
	unsigned char max_qual;
};

struct bss_node {
	/* Linked list */
	void *next;

	/* Cell data */
	unsigned char bssid[ETH_ALEN];

	/* common information */
	int		flags;
	int		mode;							// operation mode: ad-hoc or infrastructure
	char	essid[IW_ESSID_MAX_SIZE + 1];	// our found SSID
	int		essid_len;						// length of SSID
	int		freq;							// used Radio frequency in MHz
	int		qual_percent;					// signal quality in percent
	int		qual_dBm;						// reception signal level in dBm
	int		cipher_pairwise;				// pairwise cipher: IW_AUTH_CIPHER_TKIP_* (wireless.h)
	int		cipher_group;					// group cipher: IW_AUTH_CIPHER_TKIP_* (wireless.h)
	int		auth_key_mgmt;					// authentication: IW_AUTH_KEY_MGMT_802_1X or IW_AUTH_KEY_MGMT_PSK
	int		wpa_version;					// 1 for WPA1, 2 for WPA2 and 0 for others (WEP)
	int		encryption_key;					// 1 for on, 0 for off
	int		channel;					// Radio channel ( 1 - 14 ) 
};

/* open a socket for  wifi extended wireless  */
int wlan_context_open(struct wlan_context *ctx, const char* if_name);
/* close the wireless context */
void wlan_context_close(struct wlan_context *ctx);
/* free bss_node list */
void wlan_nodes_free(struct bss_node *results);
/* trigger wifi scan */
int wlan_trigger_scan(struct wlan_context *ctx, const unsigned char *ssid, size_t ssid_len);
/* get scan results */
struct bss_node* wlan_get_scanresults(struct wlan_context *ctx);
/* parse scan results */
struct netSiteSurveyList parseWextScanResults(struct bss_node * p_scanResult );



#ifdef __cplusplus
}
#endif
#endif /* PRIVNET_WIFI_H_ */
