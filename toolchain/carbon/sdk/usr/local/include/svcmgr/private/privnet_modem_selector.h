/*****************************************************************************
* Copyright (c) 2010 VeriFone Inc.
* 2099 Gateway Place, Suite 600
* San Jose, CA. 95110
*
* All Rights Reserved. No part of this software may be reproduced,
* transmitted, transcribed, stored in a retrieval system, or
* translated into any language or computer language, in any form
* or by any means electronic, mechanical, magnetic, optical,
* chemical, manual or otherwise, without the prior permission of
* VeriFone Inc.
*
******************************************************************************/

#ifndef _NET_MODEM_SELECTOR_
#define _NET_MODEM_SELECTOR_

#include "privnet_typeconv.h"
#include "pub_privnet.h"
#ifdef RAPTOR
#include "modem.h"
#else
#include "linux/modem.h"
#endif
#include "modemabstract.h"
#include "svc_net.h"

#ifdef __cplusplus
extern "C" {
#endif

/** define which modem APIs to use */
typedef enum
{
	MODEM_SELECT_NONE,/**< No Modem Is selected */
	MODEM_SELECT_PSTN,/**< Select the standard V/OS modem library */
	MODEM_SELECT_UXPSTN,/**< Select the UX PSTN modem */
	MODEM_SELECT_UXISDN/**< Select the UX ISDN modem */
}MODEM_SELECTOR_TYPE;

/** Indicates which modem is being currently used */
int dyn_modem_selector( MODEM_SELECTOR_TYPE modemtype );

/** Initialize the modem */
int modem_start_selector( char *usr_commands_file );

/** check if the line is connected */
int modem_is_line_connected_selector( void );

/** perform a modem dial for PSTN modems */
int modem_dial_selector( char *dial_str, struct st_dial_params *dial_param_ptr );

/** perform a modem dial for ISDN modem */
int modem_dial_isdn_selector( int isdn_prot, struct st_dial_data_isdn *dial_data_ptr, struct st_dial_params_isdn *dial_param_ptr );

/** get the current modem status */
int modem_get_status_selector(void);

/** Send data to the peer */
int modem_send_data_selector( char *buff, int size );

/** Read data from the peer */
int modem_receive_data_selector( char *buff, int size, int timeout_millsec );

/** terminate the connection */
int modem_hang_up_selector(void);

/** Modem selector for AT commands */
int modem_pipe_commands_selector(char *tx, char *rx, int rx_size, int max_retries, int response_timeout);

/** Get the last modem error */
int modem_get_last_call_info_selector( struct st_last_connect_info *last_connect_info_ptr, int *size );


#ifdef __cplusplus
}
#endif

#endif
