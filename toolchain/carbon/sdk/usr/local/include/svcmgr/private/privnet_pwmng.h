/*****************************************************************************
* Copyright (c) 2010 VeriFone Inc.
* 2099 Gateway Place, Suite 600
* San Jose, CA. 95110
*
* All Rights Reserved. No part of this software may be reproduced,
* transmitted, transcribed, stored in a retrieval system, or
* translated into any language or computer language, in any form
* or by any means electronic, mechanical, magnetic, optical,
* chemical, manual or otherwise, without the prior permission of
* VeriFone Inc.
*
******************************************************************************/

#ifndef _NET_POWER_MANAGEMENT_
#define _NET_POWER_MANAGEMENT_

#ifdef __cplusplus
extern "C" {
#endif
#include "svc_net.h"

void PRIVNET_setLowPowerMode( );
void PRIVNET_setHighPowerMode( );
int PRIVNET_isLowPowerMode( );

/** Get power state */
int pnet_radioGetPowerState( int radio );

#ifdef __cplusplus
}
#endif

#endif
