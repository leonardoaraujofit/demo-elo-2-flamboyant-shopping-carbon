/*****************************************************************************
* Copyright (c) 2010 VeriFone Inc.
* 2099 Gateway Place, Suite 600
* San Jose, CA. 95110
*
* All Rights Reserved. No part of this software may be reproduced,
* transmitted, transcribed, stored in a retrieval system, or
* translated into any language or computer language, in any form
* or by any means electronic, mechanical, magnetic, optical,
* chemical, manual or otherwise, without the prior permission of
* VeriFone Inc.
*
******************************************************************************/

#ifndef _PUB_COM_
#define _PUB_COM_

#include "privnet_typeconv.h"
#include "pub_privnet.h"
#include "privnet_msg.h"
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <pub_xmlparser.h>

/** Define the internal modem name */
#define INT_MDM_NAME  NET_MONDEM_PSTN
#define INT_GPRS_NAME NET_GPRS_LAYER


#define COM_MAX_PERTYPE       NETCOM_MAX_PERTYPE
#define NAME_ID_MAXLENGTH    32 /* has to be equal to NET_NAMEID_MAX */
#define COM_DATA_SIZE       148
#define IPV4_MAXLENGTH       32
#define REMOTEIP_MAXSIZE    128
#define CMD_MAXSIZE         256


#define MAX_EAGAIN_RETRY        10
#define US_EAGAIN_DELAY_TIME  10000
#define TCP_LARGE_BUFFER      65536
#define TCP_SMALL_BUFFER       1024

//Maximum timeout if we bring an interface up
#define COM_WAIT_IFACEUP         10
			
typedef int SVCNET_COMTYPE;


/** Callback for session connections */
struct _SVCNET_COM;
typedef PRIVNET_STATUS ( *SVCNET_STARTUP ) (  struct _SVCNET_COM *, int );
typedef PRIVNET_STATUS ( *SVCNET_CONNECT ) ( struct _SVCNET_COM *, uint16 );
typedef PRIVNET_STATUS ( *SVCNET_DISCONNECT ) ( struct _SVCNET_COM *);
typedef PRIVNET_STATUS ( *SVCNET_WRITE ) ( struct _SVCNET_COM *, const uint8 *, uint16 );
typedef PRIVNET_STATUS ( *SVCNET_READ ) ( struct _SVCNET_COM *,  uint8 *, uint16, uint16 *, uint16);
typedef PRIVNET_STATUS ( *SVCNET_SETTAG ) (  xmlNodePtr, const void *data, uint16  );
typedef PRIVNET_STATUS ( *SVCNET_GETTAG ) (  xmlNodePtr, void *data, uint16  );
typedef PRIVNET_STATUS ( *SVCNET_CHECKDATA ) (  const uint8*, const void *data, uint16  );
typedef PRIVNET_STATUS ( *SVCNET_CHECKATTR ) ( const char* );

/** Define a generic callback function */
typedef struct
{
	SVCNET_STARTUP startup;
	SVCNET_CONNECT connect;
	SVCNET_READ read;
	SVCNET_WRITE write;	
	SVCNET_DISCONNECT disconnect;
	SVCNET_SETTAG settag;
	SVCNET_GETTAG gettag;
	SVCNET_CHECKDATA checkdata;
	SVCNET_CHECKATTR checkattr;
}SVCCOM_FUNCTION;

/** Callback for IP interface connections */
typedef PRIVNET_STATUS ( *SVCNET_ENABLE ) ( xmlNodePtr current, pmsgIfaceCommand *pgencmd );
typedef PRIVNET_STATUS ( *SVCNET_DISABLE ) ( xmlNodePtr current, pmsgIfaceCommand *pgencmd );
typedef PRIVNET_STATUS ( *SVCNET_ONLINE ) ( xmlNodePtr current, pmsgIfaceCommand *pgencmd );
typedef PRIVNET_STATUS ( *SVCNET_OFFLINE ) ( xmlNodePtr current, pmsgIfaceCommand *pgencmd );
typedef PRIVNET_STATUS ( *SVCNET_AUTH ) ( xmlNodePtr current, uint16 timeout, pmsgIfaceCommand *pgencmd  );
typedef PRIVNET_STATUS ( *SVCNET_GETINFO ) ( xmlNodePtr current, void *info, uint16 infolen );
typedef PRIVNET_STATUS ( *SVCNET_UP ) ( xmlNodePtr current, pmsgIfaceCommand *pgencmd );
typedef PRIVNET_STATUS ( *SVCNET_DOWN ) ( xmlNodePtr current, pmsgIfaceCommand *pgencmd );
typedef PRIVNET_STATUS ( *SVCNET_DIALCON ) ( xmlNodePtr current, netSvcManager handler );
typedef PRIVNET_STATUS ( *SVCNET_DIALDIS ) ( xmlNodePtr current, netSvcManager handler );

/** Define a generic callback function for interfaces (Ethernet, WIFI and PPP tags) */
typedef struct
{
	SVCNET_ENABLE enable; /**< Enable the interface (ex. ifconfig eth up) */
	SVCNET_DISABLE disable; /**< Disable the interface (ex. ifconfig eth down) */
	SVCNET_ONLINE online; /**< Request an IP address (ex. start DHCP) */
	SVCNET_OFFLINE offline; /**< Release an IP address (ex. stop DHCP) */
	SVCNET_AUTH auth; /**< NOT MANDATORY: Check if authentication needs to be completed (ex. WIFI, BT) */
	SVCNET_GETINFO info; /**< Get the interface info (ex. Read IP address or interface down ) */	
	SVCNET_UP up; /**< Called when an interface is UP (i.e an IP address is assigned ) */
	SVCNET_DOWN down; /**< Bring down the interface (i.e an IP address is released ) */
	SVCNET_DIALCON dialupcn; /**< Dial up connection function */
	SVCNET_DIALDIS dialupds; /**< Dial up disconnection function */
}SVCCOM_IFACE;

/** Define a com media entry */
typedef struct _SVCNET_COM 
{
	SVCNET_COMTYPE comtype;/**< Indicates this current media type */
	char id[NAME_ID_MAXLENGTH];/**< Store name ID */
	int com_handle;/**< Indicates the session handle this entry is linked to */
	int valid;/**< Indicates whether this entry is valid or not */
	xmlNodePtr node;/**< Current tag */
	SVCCOM_FUNCTION func;/**< Used to connect this specific media */
	SVCCOM_IFACE conf;/**< Used to configure this specific media */
	uint8 data[COM_DATA_SIZE];/**< Convoy data for this entry */
	struct _SVCNET_COM *previous;/**< Points to previous media entry*/
	struct _SVCNET_COM *next;/**< Points to next media entry*/
}SVCNET_COM;


/** Define a list of medias */
typedef struct
{
	SVCNET_COM media[COM_MAX_PERTYPE];
	xmlNodePtr parent;/**< Parent tag of this list */
}SVCNET_COM_LIST;

/** Define a complete file session */
typedef struct
{
	xmlDocPtr doc;		
	SVCNET_COM_LIST list[NETCOM_MAX];
}SVCNET_COM_FILE;

/** Provide a way to explore the XML file */
typedef struct
{
	SVCNET_COMTYPE comtype;/**< This is the current media type */
	uint8 name[NAME_ID_MAXLENGTH];/**< This is the current interface name*/
}SVCNET_INFO;

#if defined(__cplusplus) && !defined(__DEBUG__)
extern "C"
{
#endif

void privnet_setMediaData( SVCNET_COM *media, uint8 *data, int size );
void privnet_getMediaData( SVCNET_COM *media, uint8 *data, int size );

PRIVNET_STATUS privnet_startup();
PRIVNET_STATUS privnet_comInit( SVCNET_COMTYPE comtype, SVCCOM_FUNCTION *g_stFuncCallback );
PRIVNET_STATUS privnet_comInitExt( SVCNET_COMTYPE comtype, SVCCOM_FUNCTION *f_stFuncCallback, SVCCOM_IFACE *f_stIfaceCallback );
PRIVNET_STATUS privnetParseSetNode( xmlNodePtr cur, XML_PARSE_ATTRIB *parse );
PRIVNET_STATUS privnetParseGetNode( xmlNodePtr cur, XML_PARSE_ATTRIB *parse );
PRIVNET_STATUS privnet_openXML( const char *svc_xmlConfigFile );
PRIVNET_STATUS privnet_setTag( SVCNET_COMTYPE com_type, const uint8 *name_id, const void *data, uint16 dataLen );
PRIVNET_STATUS privnet_getTag( SVCNET_COMTYPE com_type, const uint8 *name_id, void *data, uint16 dataLen );
PRIVNET_STATUS privnet_setTagAttr( const SVCNET_COMTYPE com_type, const char *name_id, const char *attrib, const char *value);
PRIVNET_STATUS privnet_getTagAttr( const SVCNET_COMTYPE com_type, const char *name_id, const char *attrib, char **value);
PRIVNET_STATUS privnet_saveXML( const char *svc_xmlConfigFile );
PRIVNET_STATUS privnet_connect( int sessionType, uint16 timeout, int *handle );
PRIVNET_STATUS privnet_connectExt( int priority, uint16 timeout, int *handle );
PRIVNET_STATUS privnet_close( void );
PRIVNET_STATUS privnet_connectNext( SVCNET_COM *current, uint16 timeout );
PRIVNET_STATUS privnet_write( int handle, const uint8 *p_data, uint16 dataLength );
PRIVNET_STATUS privnet_writeNext( SVCNET_COM *current,  const uint8 *p_data, uint16 dataLength  );
PRIVNET_STATUS privnet_read( int handle, uint8 *p_data, uint16 indataLength, uint16 *outdataLength, uint16 uTimeout );
PRIVNET_STATUS privnet_readNext( SVCNET_COM *current, uint8 *p_data, uint16 indataLength, uint16 *outdataLength, uint16 uTimeout );
PRIVNET_STATUS privnet_disconnect( int handle );
PRIVNET_STATUS privnet_disconnectNext( SVCNET_COM *current );

/** read data from the current handle */
PRIVNET_STATUS privnet_getMediaInfo( int handle, SVCNET_INFO *info_current, SVCNET_INFO *info_next );

int setSSLPassPhrase( char *password );
PRIVNET_STATUS privnet_getHandleList( uint8 *handleList, int max_size, int *size );
PRIVNET_STATUS PRIVNET_OptionRead( const uint8 *f_pIface, struct optionDescriptor *f_optInfo );
PRIVNET_STATUS privnet_getNextHandle( int sessionType, int *handle );
PRIVNET_STATUS privnet_restorepan( );
PRIVNET_STATUS privnet_getBluetoothName( const char* address, char* name, size_t name_len);
SVCNET_COMTYPE privnet_getIfaceComType( const char* f_pIface, int ipVersion );


int sslGetLastError();

int sslClearPassphrase( const char *profile ) ;

int sslSavePassphrase( const char *profile, const char *szPassPhrase, int bShareWithGroup ) ;

/** Clear the SSL cipher sui */
int sslClearCipherSuite( const char *profile ); 

/** Save the SSL pass phrase */
int sslSaveCipherSuite( const char *profile, const char *szCipherSuite, int bShareWithGroup );

/* get the interface name */
int privnet_getInterfaceType( SVCNET_COM *current );

void PRIVNET_clearIfaceName();
char* PRIVNET_getIfaceName( );
void PRIVNET_setIfaceName( const char *szIface );

SVCNET_COMTYPE PRIVNET_getIpComType( const char* f_pIface, int ipVersion );
SVCCOM_IFACE *privnet_getInterfaceFunc( const uint8 *name_id );

SVCNET_COM *xmlGetNextNode( xmlNodePtr node, const char *name_id );

/** LAN Interface functions */
PRIVNET_STATUS netiface_enable( pmsgIfaceCommand *pgencmd );
PRIVNET_STATUS netiface_disable( pmsgIfaceCommand *pgencmd );
PRIVNET_STATUS netiface_online( pmsgIfaceCommand *pgencmd );
PRIVNET_STATUS netiface_offline( pmsgIfaceCommand *pgencmd );
PRIVNET_STATUS netiface_auth( uint16 timeout, pmsgIfaceCommand *pgencmd );
PRIVNET_STATUS netiface_info( const uint8 *name_id, void *info, uint16 infolen, int ipversion );
PRIVNET_STATUS netiface_up( pmsgIfaceCommand *pgencmd );
PRIVNET_STATUS netiface_down( pmsgIfaceCommand *pgencmd );
PRIVNET_STATUS netiface_openDef( void );
PRIVNET_STATUS netiface_closeDef( void );
PRIVNET_STATUS common_eth_enable( const char *p_iface, int speed, char *mtu, int activate );
PRIVNET_STATUS common_eth_disable( const char *p_iface );

/** Init functions */
void sessionInit();
void socketInit();
void wifiInit();
void wifiInitIpv6(void);
void ethernetInit();
void ethernetInitIpv6(void);
void sslInit();
void pppInit();
void modemInit();
void optionInit();
void gprsInit();
void modemIsdnInit();
void bluetoothInit();
void ibeaconInit();

#if defined(__cplusplus) && !defined(__DEBUG__)
}
#endif

#endif


