/*****************************************************************************
* Copyright (c) 2010 VeriFone Inc.
* 2099 Gateway Place, Suite 600
* San Jose, CA. 95110
*
* All Rights Reserved. No part of this software may be reproduced,
* transmitted, transcribed, stored in a retrieval system, or
* translated into any language or computer language, in any form
* or by any means electronic, mechanical, magnetic, optical,
* chemical, manual or otherwise, without the prior permission of
* VeriFone Inc.
*
******************************************************************************/


#ifndef _PRIVNET_TYPECONV_H_
#define _PRIVNET_TYPECONV_H_

#ifdef __cplusplus
extern "C" {
#endif



typedef char uint8;
typedef char int8;

typedef signed short int16;
typedef unsigned short uint16;

typedef signed long int32;
typedef unsigned long uint32;

typedef signed long long int64;
typedef unsigned long long uint64;

#define PNOTUSED(parameter) ((void) parameter)


#ifdef __cplusplus
}
#endif

#endif /* ifdef _PRIVNET_TYPECONV_H_*/
