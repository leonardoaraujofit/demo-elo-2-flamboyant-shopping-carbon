/*****************************************************************************
* Copyright (c) 2010 VeriFone Inc.
* 2099 Gateway Place, Suite 600
* San Jose, CA. 95110
*
* All Rights Reserved. No part of this software may be reproduced,
* transmitted, transcribed, stored in a retrieval system, or
* translated into any language or computer language, in any form
* or by any means electronic, mechanical, magnetic, optical,
* chemical, manual or otherwise, without the prior permission of
* VeriFone Inc.
*
******************************************************************************/

#ifndef _NET_IFONFIG_
#define _NET_IFONFIG_

#include "privnet_typeconv.h"
#include "pub_privnet.h"

#ifdef __cplusplus
extern "C" {
#endif

PRIVNET_STATUS INET_readconf( struct netIfconfig *rList, uint16 pMax, uint16 *pSize );
PRIVNET_STATUS INET_interfaceAvailableList( struct netIfNameList *p_ifNameList );
int PRIVNET_readMacAddress( const uint8 *f_pIface, char *mac, int len );

#ifdef SVC_USE_IPV6
PRIVNET_STATUS INET_readconfIpv6( struct netIfconfigIpv6 *rList, uint16 max_count,
  uint16 *pCount );
#endif


#ifdef __cplusplus
}
#endif

#endif
