/*
 *	Copyright, 2012 VeriFone Inc.
 *	2099 Gateway Place, Suite 600
 *	San Jose, CA.  95110
 *
 *  All Rights Reserved. No part of this software may be reproduced,
 *  transmitted, transcribed, stored in a retrieval system, or
 *  translated into any language or computer language, in any form
 *  or by any means electronic, mechanical, magnetic, optical,
 *  chemical, manual or otherwise, without the prior permission of
 *  VeriFone Inc.
 */
#ifndef VHQ_PRIV_H
#define VHQ_PRIV_H

#include "svcmgrSvcDef.h"
#include <svcmgr/svc_vhq.h>

#ifdef __cplusplus
extern "C" {
#endif



/** Set a content event
 * @param[in] user - user that content is intended for
 * @param[in] filename - The filename of the content
 * @param[in] content_type - the type of content available
 * @param[in] content_ID - the ID number associated with this content (provided by content deliverer)
 * @return int status - will return a status value
 */
/*SVC_PROTOTYPE*/ int vhq_setContentEvent(char *user /*REQ*/, char *filename /*REQ*/, char* relative_path /*REQ*/, int content_type /*REQ*/, int content_ID /*REQ*/);

/** Get content result
 * @param[in] user - user that content result is intended for
 * @param[in] content_ID - the content ID to retrieve result for
 * @param[in] flags = Get Event Flags - 1 = non-block, 2 = Last , 4 = cancel (options can be or'd)
 * @return int status - will return a status value
 */
/*SVC_PROTOTYPE*/ int vhq_getContentResult(char *user /*REQ*/, int content_ID /*REQ*/, int flags /* 0 */);


/** Close content result event
 * @param[in] user - user that content result is intended for
 * @param[in] content_ID - the content ID of result to close
 * @return int status - will return a status value
 */
/*SVC_PROTOTYPE*/ int vhq_closeContentResult(char *user /*REQ*/, int content_ID /*REQ*/);





/** Set an application event
 * @param[in] user - user that event is intended for (NULL for all users)
 * @param[in] messageType - message type to pass to application
 * @param[in] messageID - message id to pass to application
 * @param[in] fileName - filename to pass to application
 * @param[in] fileLocation - the file location to pass to application
 * @param[in] eventMask - event mask to pass to application
 * @param[in] transactionType - transaction type to pass to application
 * @return int status - will return a status value
 */
/*SVC_PROTOTYPE*/ int vhq_setAppEvent(const char *user /*REQ*/, const char *receiver /*REQ*/, int messageType /*REQ*/, int messageID /*REQ*/, int result /*REQ*/, const char* fileName /*REQ*/,
									  const char* fileLocation /*REQ*/, int eventMask /*REQ*/, int transactionType /*REQ*/, int downloadPercent /*REQ*/,
									  unsigned char responseExpected /*REQ*/);



/** Used by agent to get the result of an event
 * @param[in] user - user response was coming from
 * @param[in] messageID - message id passed to app in request
 * @param[in] flags = Get Event Flags - 1 = non-block, 2 = Last , 4 = cancel (options can be or'd)
 * IMPORTANT NOTES:  This is used by agent and is not available to applications
 */
/*SVC_PROTOTYPE*/ struct vhqAppResultReturn* vhq_getAppResult(const char *user /*REQ*/, const char *receiver /*REQ*/, int messageID /*REQ*/, int flags /*REQ*/);


/** Used by agent after it stops cheking for result of an event
 * @param[in] user - user response was coming from
 * @param[in] messageID - message id passed to app in request
 * IMPORTANT NOTES:  This is used by agent and is not available to applications
 */
/*SVC_PROTOTYPE*/ int vhq_closeAppResultEvent(const char *user /*REQ*/, const char *receiver /*REQ*/, int messageID /*REQ*/);

#ifdef __cplusplus
}
#endif
#endif //VHQ_PRIV_H
