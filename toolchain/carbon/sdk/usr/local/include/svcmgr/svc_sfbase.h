/** 
 *  @file svc_sfbase.h 
 *
 *	@brief Swordfish Base service.
 * 
 * Service specific to the Swordfish Base platform
 *   
 * 
 */


 /*
 * All Rights Reserved. No part of this software may be reproduced,
 *  transmitted, transcribed, stored in a retrieval system, or
 *  translated into any language or computer language, in any form
 *  or by any means electronic, mechanical, magnetic, optical,
 *  chemical, manual or otherwise, without the prior permission of
 *  VeriFone Inc.
 *
 *   Copyright 2016, VeriFone Systems, Inc.
 */



#ifndef SVC_SFBASE_H
#define SVC_SFBASE_H

#include "svcmgrSvcDef.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef MAX_KEY_LEN
#define MAX_KEY_LEN         (99) /**< Max length for event key names */
#endif


/*SVC_SERVICE:sfbase*/


/**
 * @name Maximum name and/or location length used (see sfbase_setNameLocation())
 *
 * Note: If a longer name and/or location is passed, only the first MAX_SFBASE_NAME_LOCATION_LENGTH
 *       characters will be used.
*/
/** \{ */
#define MAX_SFBASE_NAME_LOCATION_LENGTH	255
/** \} */


/** Get version of netloader service
 *
 * @return 
 * Version struct containing version
 *
 * @note 
 * For XML interface, see xml:\ref netloader_getVersion.
 */
/*SVC_PROTOTYPE*/ struct version sfbase_getVersion(void);


/** Set the Swordfish Base Name and Location - Will be used by the broadcast message
 * 
 * @param[in] name To set value for (length limited by MAX_SFBASE_NAME_LOCATION_LENGTH)
 * @param[in] location To set value for (length limited by MAX_SFBASE_NAME_LOCATION_LENGTH)
 * 
 * @return 0 = Ok,  -1 = Error
 * On error return, errno:\n
 * <b>Errno values</b>:
 * @li EINVAL - Invalid name and/or location
 * @li EACCES - Unable to set name in section in env file
 * @li ECONNREFUSED - not supported on this platform
 */
/*SVC_PROTOTYPE*/ int sfbase_setNameLocation(char *name, char *location);

/*SVC_STRUCT*/
/** Returned get name and location structure
 */
struct sfbaseNameLocationReturn {
	int status; 											/**< Used to indicate status of request */
	char name[MAX_SFBASE_NAME_LOCATION_LENGTH + 1]; 		/**< Name of the Swordfish Base unit */
	char location[MAX_SFBASE_NAME_LOCATION_LENGTH + 1];		/**< Location of the Swordfish Base unit */
};

/** Get current values for the name and location strings of the Swordfish Base unit
 *
 * @return
 *   - name - name of the Swordfish Base unit
 *   - location - location of the Swordfish Base unit
 *   - status - indicates if request was successful or not (see errno)
 *     - status >= 0 then Success
 *     - status = -1 then error, check errno:
 *
 * <b>Errno Values:</b>
 * @li ENOENT - Name / location not yet written to device
 * @li ENOMEM - Out of memory
 *
 */
/*SVC_PROTOTYPE*/ struct sfbaseNameLocationReturn sfbase_getNameLocation(void);

/** Get current value for the Dock Detect state - non-blocking
 *
 * @return 
 * @li 0 Undocked - tablet/payment terminal undocked
 * @li 1 Docked - tablet/payment terminal docked
 * @li -1 Error - Unable to complete request
 *
 * On error return, errno:\n
 * <b>Errno values</b>:
 * @li EBADF - Failure when requesting dock detect status
 */
/*SVC_PROTOTYPE*/ int sfbase_dockDetect_getState(void);


/*SVC_STRUCT*/
/**  Returned event registration Structure
 */
struct sfbaseRegistrationReturn {
	int status; 	/**< Used to indicate status of request */
	char eventkey[MAX_KEY_LEN+1];	    /**<  Event key used by various methods in this service */
};
 

/** used to request event access to the Dock Detect mcast event channel.
 * 
 * Returns 
 *  eventkey - the assigned event key used by sfbase_dockDetect_getEvent()
 *  status - indicates if request was successful or not (see errno)
 * @li status >= 0 then Success
 * @li status = -1 then error, check errno:
 * 
 * <b>Errno Values:</b>
 * @li ENOSPC - Unable to register new event to channel (full)
 */
/*SVC_PROTOTYPE*/ struct sfbaseRegistrationReturn sfbase_dockDetect_eventRequest(void);


/** Removes an event from the mcast event channel that was obtained using sfbase_dockDetect_eventRequest()
 * 
 * @param[in] eventkey Name/key of event as returned by sfbase_dockDetect_eventRequest()
 * 
 * @return 
 * @li 0 Success
 * @li -1 Error - Unable to complete request
 *
 * On error return, errno:\n
 * <b>Errno values</b>:
 * @li EINVAL - Invalid key format
 * @li ENOENT - Internal multicast event channel does not exist
 * @li ENODEV - Specified event does not exist in channel
 */
/*SVC_PROTOTYPE*/ int sfbase_dockDetect_eventRemove(char *eventkey);


/** Returns next dock detect event.
 * 
 * Setting flags=0 will perform the default action of retrieving the next non-previously
 * retrieved event, blocking if needed waiting for the occurrence of the next event.
 *
 * @param[in] eventkey Name/key of event as returned by sfbase_dockDetect_eventRequest()
 * @param[in] flags Specify blocking/non-blocking/cancel/purge/last (see flags below)
 * @param[in] timeout (0 = wait forever) - max time in seconds to wait for event.
 *
 * @return 
 * @li >= 0 - state of dock detect:
 * @li    0 Undocked - tablet/payment terminal docked
 * @li    1 Docked - tablet/payment terminal docked
 * @li -1 Error - Unable to complete request
 *
 * @note
 * flags:
 * @li BLOCK      (0x00) - Get event, if no event block, waiting for event
 * @li NONBLOCK   (0x01) - Get event, don't block, return empty if no event
 * @li LAST       (0x02) - Get the last event that was received (and previously read)
 * @li CANCEL     (0x04) - Cancel/unblock any process waiting on this event key
 * @li PURGE      (0x08) - Purge/flush and pending events and return last event read
 *
 * other:
 * @li If data != NULL, user/caller is responsible to free data (malloc'd memory).
 * @li Max data size is 4000 bytes
 * @li if flags & GETEVENT_PURGE, max data size is 2048 bytes
 * @li GETEVENT_PURGE will purge any pending events and return the last (newest) event
 * @li GETEVENT_PURGE|GETEVENT_NONBLOCK will return right away with errno=ENODATA if no events pending
 * @li GETEVENT_PURGE and GETEVENT_NONBLOCK not specified, will wait for event if no event pending
 * @li timeout parameter only used if flags == GETEVENT_BLOCK
 * @li Upon timeout, errno == EINTR (same as cancel), and event.event == ENOENT
 *
 * <b>Errno Values:</b>
 *   EINVAL - Invalid key format
 *   ENOENT - key isn't active/open (event_open() not called first)
 *   ENODEV - Unable to cancel previous event_getEvent() in progress.
 *   EAGAIN - No last event (when flags requesting last).
 *   ENODATA - No event data available (when flags set to non-block and no event).
 *   EBUSY  - event cancel already in progress, and attempting to cancel event (flags == 4).
 *   EFAULT - event.size specified, but no data available to read.
 *   EINTR  - event_getEvent() canceled (by another thread/user). event field of returned
 *            structure may contain cause (see event_cancelWithCause())
 *   ENOMEM - Unable to allocate internal memory
 */
/*SVC_PROTOTYPE*/ int sfbase_dockDetect_getEvent(char* eventkey, int flags, int timeout);



/** Get value for the Cash Drawer state - non-blocking
 *
 * @return 
 * @li  0 Closed - cash drawer closed
 * @li  1 Open - cash drawer open or disconnected
 * @li -1 Error - Unable to complete request
 *
 * On error return, errno:\n
 * <b>Errno values</b>:
 * @li EBADF - Failure when requesting cash drawer status
 */
/*SVC_PROTOTYPE*/ int sfbase_cashDrawer_getState(void);


/** Open the Cash Drawer using default duration (or last duration set by sfbase_cashDrawer_openDuration())
 *
 * @return 
 * @li 0 - cash drawer open request successful (use sfbase_cashDrawer_getState() or sfbase_cashDrawer_getEvent() to confirm)
 * @li -1 Error - Unable to complete request
 *
 * @note
 * This method may succeed (return 0), but the cash drawer may remain closed to to obstruction, held closed, or disconnected
 * 
 * On error return, errno:\n
 * <b>Errno values</b>:
 * @li EACCES - permission denied - caller doesn't have permission/access to perform this operation
 * @li EBADF - problem writing to trigger to open cash drawer
 * @li ENOENT - unable to open cash drawer trigger
 * @li EAGAIN - cash drawer commanded to open, but failed to open (blocked?, not connected?)
 * @li ECONNREFUSED - not supported on this platform
 */
/*SVC_PROTOTYPE*/ int sfbase_cashDrawer_open(void);


/** Open the Cash Drawer with duration (all subsequent sfbase_cashDrawer_open() will use this duration)
 *
 * @param[in] duration in ms for the pulse to the draw solenoid that opens the draw
 *
 * @return
 * @li 0 - cash drawer open request successful (use sfbase_cashDrawer_getState() or sfbase_cashDrawer_getEvent() to confirm)
 * @li -1 Error - Unable to complete request
 *
 * @note
 * This method may succeed (return 0), but the cash drawer may remain closed to to obstruction, held closed, or disconnected
 *
 * On error return, errno:\n
 * <b>Errno values</b>:
 * @li EINVAL - duration invalid, must be: duration >= 50 and duration <= 250 ms (default 120ms)
 * @li EACCES - permission denied - caller doesn't have permission/access to perform this operation
 * @li EBADF - problem writing to trigger to open cash drawer
 * @li ENOENT - unable to open cash drawer trigger
 * @li EAGAIN - cash drawer commanded to open, but failed to open (blocked?, not connected?)
 * @li ECONNREFUSED - not supported on this platform
 */
/*SVC_PROTOTYPE*/ int sfbase_cashDrawer_openDuration(int duration);


/** used to request event access to the Cash Drawer mcast event channel.
 * 
 * Returns 
 *  eventkey - the assigned event key used by sfbase_cashDrawer_getEvent()
 *  status - indicates if request was successful or not (see errno)
 * @li status >= 0 then Success
 * @li status = -1 then error, check errno:
 * 
 * <b>Errno Values:</b>
 * @li ENOSPC - Unable to register new event to channel (full)
 */
/*SVC_PROTOTYPE*/ struct sfbaseRegistrationReturn sfbase_cashDrawer_eventRequest(void);


/** Removes an event from the mcast event channel that was obtained using sfbase_cashDrawer_eventRequest()
 * 
 * @param[in] eventkey Name/key of event as returned by sfbase_cashDrawer_eventRequest()
 * 
 * @return 
 * @li 0 Success
 * @li -1 Error - Unable to complete request
 *
 * On error return, errno:\n
 * <b>Errno values</b>:
 * @li EINVAL - Invalid key format
 * @li ENOENT - Internal multicast event channel does not exist
 * @li ENODEV - Specified event does not exist in channel
 */
/*SVC_PROTOTYPE*/ int sfbase_cashDrawer_eventRemove(char *eventkey);


/** Returns next cash drawer event (blocking (based on flags, timeout)).
 * 
 * Setting flags=0 will perform the default action of retrieving the next non-previously
 * retrieved event, blocking if needed waiting for the occurrence of the next event.
 *
 * @param[in] eventkey Name/key of event as 
 * @param[in] flags Specify blocking/non-blocking/cancel/purge/last (see below)
 * @param[in] timeout (0 = wait forever) - max time in seconds to wait for event.
 *
 * @return 
 * @li >= 0 - state of cash drawer:
 * @li    0 Closed - cash drawer closed
 * @li    1 Open - cash drawer open or disconnected
 * @li   -1 Error - Unable to complete request
 *
 * @note
 * flags:
 * @li BLOCK      (0x00) - Get event, if no event block, waiting for event
 * @li NONBLOCK   (0x01) - Get event, don't block, return empty if no event
 * @li LAST       (0x02) - Get the last event that was received (and previously read)
 * @li CANCEL     (0x04) - Cancel/unblock any process waiting on this event key
 * @li PURGE      (0x08) - Purge/flush and pending events and return last event read
 * 
 * other:
 * @li If data != NULL, user/caller is responsible to free data (malloc'd memory).
 * @li Max data size is 4000 bytes
 * @li if flags & GETEVENT_PURGE, max data size is 2048 bytes
 * @li GETEVENT_PURGE will purge any pending events and return the last (newest) event
 * @li GETEVENT_PURGE|GETEVENT_NONBLOCK will return right away with errno=ENODATA if no events pending
 * @li GETEVENT_PURGE and GETEVENT_NONBLOCK not specified, will wait for event if no event pending
 * @li timeout parameter only used if flags == GETEVENT_BLOCK
 * @li Upon timeout, errno == EINTR (same as cancel), and event.event == ENOENT
 *
 * <b>Errno Values:</b>
 *   EINVAL - Invalid key format
 *   ENOENT - key isn't active/open (event_open() not called first)
 *   ENODEV - Unable to cancel previous event_getEvent() in progress.
 *   EAGAIN - No last event (when flags requesting last).
 *   ENODATA - No event data available (when flags set to non-block and no event).
 *   EBUSY  - event cancel already in progress, and attempting to cancel event (flags == 4).
 *   EFAULT - event.size specified, but no data available to read.
 *   EINTR  - event_getEvent() canceled (by another thread/user). event field of returned
 *            structure may contain cause (see event_cancelWithCause())
 *   ENOMEM - Unable to allocate internal memory
 */
/*SVC_PROTOTYPE*/ int sfbase_cashDrawer_getEvent(char* eventkey, int flags, int timeout);

#ifdef __cplusplus
}
#endif

#endif //SVC_SFBASE_H
