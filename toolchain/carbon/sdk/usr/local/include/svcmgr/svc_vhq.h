/** @addtogroup maintenance Maintenance */
/** @{ */
/** @addtogroup terminalmanagementservice Maintenance - VHQ: Services */
/** @{ */
/** 
 * @file svc_vhq.h	
 *
 * @brief VeriFone HeadQuarters VHQ (Terminal Management System - TMS)
 *
 */

/*
 *	Copyright, 2012 VeriFone Inc.
 *	2099 Gateway Place, Suite 600
 *	San Jose, CA.  95110
 *
 *  All Rights Reserved. No part of this software may be reproduced,
 *  transmitted, transcribed, stored in a retrieval system, or
 *  translated into any language or computer language, in any form
 *  or by any means electronic, mechanical, magnetic, optical,
 *  chemical, manual or otherwise, without the prior permission of
 *  VeriFone Inc.
 */
#ifndef SVC_VHQ_H
#define SVC_VHQ_H

#include "svcmgrSvcDef.h"

#ifdef __cplusplus
extern "C" {
#endif

/*SVC_SERVICE:vhq*/

/**
 * @name Defines for vhq status (see struct vhqRegisterReturn/vhqEventReturn)
*/
/** \{ */
#define VHQ_STATUS_UNSUPPORTED_FEATURE		-1  /**< Unsupported feature */
#define VHQ_STATUS_SUCCESS					1   /**< Successful status */
#define VHQ_STATUS_ERROR					2   /**< Error status */
#define VHQ_STATUS_CONTENT_AVAILABLE		3   /**< Content available status */
#define VHQ_STATUS_NO_CONSUMER				4   /**< No consumer for this content */
#define VHQ_STATUS_FILENAME_ERROR			5   /**< Filename is too long */
#define VHQ_STATUS_CONTENT_FAILURE			6   /**< Content failed to update */
#define VHQ_STATUS_MSGQ_FAILURE				7   /**< Message queue failure */
#define VHQ_STATUS_MSGSND_FAILURE			8   /**< Sending message to VHQ agent failed */
#define VHQ_STATUS_MSGRCV_FAILURE			9   /**< Sending message to VHQ agent failed */
#define VHQ_STATUS_APP_EVENT_AVAIL			10  /**< App event is available */
#define VHQ_STATUS_REQUESTED				11  /**< App event is available */
/** \} */


/**
 * @name Defines for vhq content type (see struct vhqEventReturn and vhq_getContentEvent())
*/
/** \{ */
#define VHQ_CONTENT_TYPE_UNSIGNED_BUNDLE    1 /**< An unsigned bundle type */
#define VHQ_CONTENT_TYPE_UNSIGNED_FILE      2 /**< A single unsigned file */
#define VHQ_CONTENT_TYPE_MIXED_BUNDLE		3 /**< A mixed bundle type (unsigned and signed packages) */
#define VHQ_CONTENT_TYPE_SIGNED_BUNDLE		4 /**< A signed bundle type */
/** \} */


/*SVC_STRUCT*/
/**
 * Returned Register Structure
 */
struct vhqRegisterReturn {
	int status;		            /**< Return state value */
	char eventkey[99];	        /**<  Event key to use when requesting content updates */
};

#define MAX_FILENAME_LENGTH					256

/*SVC_STRUCT*/
/**
 * Returned Event Structure
 */
struct vhqEventReturn {
	int status;		                                /**< Return state value */
	int contentType;	                            /**< Type of file to process */
	int contentID;	                                /**< An ID associated with the content that is used for status updates from the app */
	char filename[MAX_FILENAME_LENGTH];         	/**< Filename to process (may be empty) */
	char relativePath[MAX_FILENAME_LENGTH];	        /**< Path where the file will go (may be empty) */
};

/** Obtain the version of the vhq service
 * 
 * @return 
 * Version struct defined in svcmgrSvcDef.h
 */
/*SVC_PROTOTYPE*/ struct version vhq_getVersion(void);


/** Register for Content
 *
 * @return 
 * Struct vhqRegisterReturn - will return information about event to request content for
 * 
 * @note IMPORTANT!
 * @li Application must free vhqRegisterReturn msg
 */
/*SVC_PROTOTYPE*/ struct vhqRegisterReturn* vhq_registerForContent(void);


/** Get a content event
 *
 * @param[in] eventkey Event key used to obtain vhq content status
 * @param[in] flags Get Event Flags:
 *                      @li  - 1 = Non-block
 *                      @li 2 = Last 
 *                      @li 4 = Cancel (options can be or'd)
 *
 * @return 
 * Struct vhqEventReturn - will return information for processing the new content, NULL on error
 *
 * <b>Errno values</b>:
 *  @li EINVAL - Invalid argument(s) passed.
 *  @li ENODEV - Invalid eventkey, or unable to cancel previous event_getEvent() in progress.
 *  @li EAGAIN - No last event (when flags requesting last).
 *  @li ENODATA - No event data available (when flags set to non-block and no event).
 *  @li EBUSY  - Event cancel already in progress, and attempting to cancel event (flags == 4).
 *  @li EFAULT - Event.size specified, but no data available to read.
 *  @li EINTR  - Event_getEvent() canceled (by another thread/user). event field of returned
 *            structure may contain cause (see event_cancelWithCause())
 *   @li ENOMEM - Unable to malloc space for return data.
 *
 * @note IMPORTANT!
 * @li Application must free returned vhqEventReturn msg if non-NULL
 */
/*SVC_PROTOTYPE*/ struct vhqEventReturn* vhq_getContentEvent(char *eventkey /*REQ*/, int flags /* 0 */);


/** Set a content result
 *
 * @param[in] content_ID  The content ID this result applies to (returned from vhq_getContentEvent())
 * @param[in] contentStatus  a
 *
 * @return 
 * Int status - will return a status value
 */
/*SVC_PROTOTYPE*/ int vhq_setContentResult(int content_ID /*REQ*/, int contentStatus /*REQ*/);



/* Below methods are for user Agent interface */

/* To process VHQ content updates, we need a user-signed application. Since WDE
 * is not executed as a user, a separate user agent is required. To help with this
 * effort, there are several methods available through this service that are used
 * by the user agent and by javascript (or other application).
 */

/*SVC_STRUCT*/
/**
 * Event Structure containing Content, Requesting approval
 */
struct vhqContentRequest {
	int contentType;                            /**< Type of file to process */
	int contentID;	                            /**< An ID associated with the content that is used for status updates from the app */
	char filename[MAX_FILENAME_LENGTH];	        /**< Filename to process (may be empty) */
};


/*SVC_STRUCT*/
/**
 * Event Structure containing Content, Result of processing
 */
struct vhqContentResult {
	int contentID;	        /**< An ID associated with the content that is used for status updates from the app */
	int status;		        /**< Return state value */
};



/***********************************************************************************************/
/* Approver application methods (javascript, et. al) to interact with User Agent               */
/***********************************************************************************************/




/******************************************************************************
 * Below methods are used by the Content Approver application.
 * This application, decides whether to approve or decline a update request
 *  from the User Agent.
 ******************************************************************************/


/** Register the Content approver for the User Agent with VHQ service
 *  This method is used to register a process to approve/decline updates from the User Agent.
 *  Only one user/process can be registered at a time. You must first unregister before
 *  another user or process can register as the approver.
 *
 *  @note 
 * The use of a User Agent for VHQ is purely optional and is typically only needed with WDE.
 *
 * @return 
 * @li EventKey >= 0 event key used by vhq_UAIgetApprovalRequest() and vhq_UAIsetApproval(). This is a malloc'd response
 * @li EventKey == NULL error
 *
 * @note 
 * The caller is responsible for freeing the malloc'd space for returned eventkey.
 *
 * <b>Errno values</b>:
 * @li EBUSY  - Another process already registered, must call vhq_unregisterUserAgentApprover() first.
 * @li ENOMEM - No memory available to complete request.
 */
/*SVC_PROTOTYPE*/ char *vhq_UAIregisterContentApprover(void);


/** Unregister as Approver for the User Agent with VHQ service
 *  This method is used to unregister any previously registered Approver process.
 *
 * @return 
 * @li Status >= 0 Success
 * @li Status < 0 Error
 *
 * <b>Errno values</b>:
 * @li ENOENT  - No process is currently registered as the approver.
 */
/*SVC_PROTOTYPE*/ int vhq_UAIunregisterContentApprover(void);


/** Wait for any content update approval request (from User Agent).
 *   
 *  The User Agent uses vhq_UAIagentGetApproval(), that sends an event to this method requesting approval.
 *
 *	@note
 *  If no approver is registered, this method has no affect, ie no request is obtained
 *	      and an error is returned (errno == ENXIO).
 *
 * @param[in] eventKey  Eventkey as returned by vhq_UAIregisterContentApprover()
 * @param[in] flags Get Event Flags:
 *                      @li - 0 = Blocking
 *                      @li 1 = Non-block
 *  `                   @li 2 = Last 
 *                      @li 4 = Cancel (options can be or'd)
 *
 * @return 
 * Struct vhqContentRequest contentRequest - this contains what update request is being made and allows the approver process to accept or decline the update request.
 * @li errno = 0 Success
 * @li -1 On error
 *   
 *  <b>Errno values</b>:
 *     @li  ENODATA - Got a request for approval, but no data to populate return struct
 *     @li  EBADF - Wrong amount of data received for approval, can't properly populate return struct
 *     @li  ENXIO - No approver registered
 *     @li  EFAULT - Internal error
 */
/*SVC_PROTOTYPE*/ struct vhqContentRequest vhq_UAIgetApprovalRequest(char *eventKey, int flags);


/** Method used to approve or decline content update request.
 *
 *@note  
 * If no approver is registered, this method has no affect, ie no approval is posted and an error is returned (errno == ENXIO).
 *
 * @param[in] contentID  Obtained from return struct from vhq_UAIgetApprovalRequest()
 * @param[in] approve  @li > 0 To accept the content update @li <= 0 To decline the content update
 *
 * @return 
 * @li 0 Success
 * @li -1 Error
 * 
 * <b>Errno values</b>:
 * @li EINVAL - Invalid argument(s) passed
 * @li ENXIO - No approver registered
 */
/*SVC_PROTOTYPE*/ int vhq_UAIsetApproval(int contentID, int approve);


/** Used by approver process to get end result of content update from User Agent (optional)
 *
 * If approval is granted to the User Agent (see vhq_UAIsetContentApproval(), then the content request
 *  is processed by the User Agent and the results of this operation is available to the approver process
 *  via this call (User Agent uses vhq_UAIagentSetContentResult() to report results).
 *
 * @note  If no approver is registered, this method has no affect, ie no result is obtained and an error is returned (errno == ENXIO).
 *
 * @param[in] flags Get Event Flags:
 *                      @li 0 = Blocking
 *                      @li 1 = Non-block
 *                      @li 2 = Last
 * 
 * @param[in] timeout (0 = wait forever) - max time in seconds to wait for result from User Agent application.
 *
 * @return 
 * Struct vhqContentResult result - contains result of the update as performed by User Agent.
 *
 * 	- status - will return a status value. This value is defined by the User Agent application,
 *             but is typically one of the defines: VHQ_STATUS_xxxx (see above)
 *
 *  - contentID - ID associated with the content that was processed (see vhq_UAIsetApproval())
 * 
 *  <b>Errno values</b>:
 * @li ENXIO - No approver registered
 */
/*SVC_PROTOTYPE*/ struct vhqContentResult vhq_UAIgetContentResult(int flags, int timeout);






/*********************************************************************************/
/* User Agent methods (non-WDE) application                                      */
/*********************************************************************************/




/******************************************************************************
 * Below methods are used by the User Agent application.
 * This application, waits for notification from the VHQ server then if
 * desired, requests for approval and if granted, installs the update
 * component, then reports if successful or not both to VHQ and to the
 * approver (if used).
 *
 ******************************************************************************/


/** Used by User Agent to request approval from the approver application to process the content update.
 *
 * This method internally generates an event that the user approver application (javascript?),
 *   then waits for a approve/decline response (see vhq_UAIresponseContentApproval()).
 * Upon return from this call, the user agent process is to examine the return value
 *   to either process the content update or to decline and in turn notify the VHQ
 *   agent using the method vhq_setContentResult() and notify the approver process via
 *   vhq_UAIagentSetContentResult()
 *
 * @note
 *      1. This is not a VHQ requirement, and it is up to the User Agent developer whether they
 *         want to use this request/feedback mechanism. The User Agent can forego this and
 *         just process the content update and notify VHQ via vhq_setContentResult().
 *      2. If no approver is registered, this method will return 0 (errno == 0).
 *
 * @param[in] contentRequest Contains information about the content update request
 * @param[in] timeout (0 = wait forever) - max time in seconds to wait for approve/decline from approver application.
 *
 * @return 
 * @li approval >0 approve
 * @li approval <= 0 decline
 *
 *  <b>Errno values</b>:
 * @li errno == 0 - registered approver responded with decline
 * @li ENOENT - timeout exceeded w/o response from user
 * @li EBADR - approval response doesn't match our contentID
 */
int vhq_UAIagentGetApproval(struct vhqContentRequest contentRequest, int timeout);


/** Report end result of content update processing by User Agent to approver process
 *
 * If approval is granted to the User Agent (see vhq_UAIagentGetApproval(), then the content request
 *  is processed by the User Agent and the results of this operation is available to the approver process
 *  via this call (approver uses vhq_UAIgetContentResult() to get results).
 *
 * @note
 * If no approver is registered, this method has no affect, ie no result is posted
 *	      and an error is returned (errno == ENXIO).
 *
 * @param[in] contentRequest 
 * @param[in] status  This value is defined by the User Agent application, but is typically
 *                     one of the defines: VHQ_STATUS_xxxx (see above)
 *
 * @return 
 * @li 0 Success
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li ENODEV - No event for key (event_open() using passed key not active)
 * @li ENXIO - No approver registered
 * @li EFAULT - Internal error
 */
int vhq_UAIagentSetContentResult(struct vhqContentRequest contentRequest, int status);


/*------------------------------------------------------------------------------------------------------------------*/

/* Mitch 7-10-2013: New API's for Application Integration */


#define VHQ_EVENT_NOTIFY_REBOOT_DEVICE				0x00000001
#define VHQ_EVENT_NOTIFY_RESTART_APPS				0x00000002
#define VHQ_EVENT_NOTIFY_DOWNLOAD_STARTED			0x00000004
#define VHQ_EVENT_NOTIFY_DOWNLOAD_PERCENT			0x00000008
#define VHQ_EVENT_NOTIFY_DOWNLOAD_COMPLETE			0x00000010
#define VHQ_EVENT_NOTIFY_INSTALL_DOWNLOAD			0x00000020
#define VHQ_EVENT_NOTIFY_MAINTENANCE_END			0x00000040
#define VHQ_EVENT_NOTIFY_HEARTBEAT_RESULT			0x00000080
#define VHQ_EVENT_NOTIFY_CONTACT_SERVER_START		0x00000100
#define VHQ_EVENT_NOTIFY_CONTACT_SERVER_END			0x00000200



/** Used by application to register itself with the VHQ agent
 *
 * @param[in] messageID  Message ID used for synchronization
 * @param[in] receiverName  Name identifier (normally bundle name) to receive parameter updates for
 * @param[in] eventNotifyMask  Event Notification Mask from VHQ_EVENT definitions above (options can be or'd)
 *
 * @return 
 * Struct vhqRegisterReturn - will return information about event to request messages from
 *
 * @note IMPORTANT!
 * @li Application must free vhqRegisterReturn msg
 * @li The service will identify the caller of the registration function so no
 *     indentification parameters are necessary
 */
/*SVC_PROTOTYPE*/ struct vhqRegisterReturn* vhq_registerApplication(int messageID /*REQ*/, const char* receiverName /*REQ*/, int eventNotifyMask /* 0 */);


/** Used by application to unregister itself with the VHQ agent
 *
 * @param[in] messageID Message ID used for synchronization
 * @param[in] receiverName Name identifier (normally bundle name) to receive parameter updates for
 *
 * @return 
 * Int - unregistration status
 */
/*SVC_PROTOTYPE*/ int vhq_unregisterApplication(int messageID /*REQ*/, const char* receiverName /*REQ*/);




/** Used by application to call the management server
 *
 * @param[in] messageID  MessageID for request
 * @param[in] secondsToWait Seconds to wait before calling the server
 *
 * @return 
 * Int - manamagement server call status
 *
 *
 */
/*SVC_PROTOTYPE*/ int vhq_callServer(int messageID /*REQ*/, int secondsToWait /* 0 */);


/** Used by application to set its state (BUSY or NOT_BUSY)
 *
 * @param[in] messageID MessageID for request
 * @param[in] appState Application state
 *
 * @return 
 * Int - return status
 */
/*SVC_PROTOTYPE*/ int vhq_setAppState(int messageID /*REQ*/, int appState /*REQ*/);


/** Used by application to get location of VHQ config file
 *
 * @param[in] messageID  MessageID for request
 *
 * @return
 * Char* - string containing path where VHQ config file is located
 *
 * @note IMPORTANT!
 * @li Application must free returned char*
 */
/*SVC_PROTOTYPE*/ int vhq_getConfigLocation(int messageID /*REQ*/);


/** Used by application to notify VHQ agent a new configuration is available
 *
 * @param[in] messageID MessageID for request
 * @param[in] newVHQConfig String to updated VHQ config file (use default location if NULL)
 *
 * @return 
 * Int - return status of loading new configuration
 */
/*SVC_PROTOTYPE*/ int vhq_newConfigAvail(int messageID /*REQ*/, const char* newVHQConfig /*REQ*/);


#define VHQ_MESSAGE_TYPE_CALL_SERVER			1
#define VHQ_MESSAGE_TYPE_SET_APP_STATE			2
#define VHQ_MESSAGE_TYPE_GET_VHQ_CONFIG			3
#define VHQ_MESSAGE_TYPE_SET_VHQ_CONFIG			4
#define VHQ_MESSAGE_TYPE_SET_APP_INFO			5
#define VHQ_MESSAGE_TYPE_SET_PARM_LIST			6
#define VHQ_MESSAGE_TYPE_GET_FILE				7
#define VHQ_MESSAGE_TYPE_PUT_FILE				8
#define VHQ_MESSAGE_TYPE_DEL_FILE				9
#define VHQ_MESSAGE_TYPE_EVENT_NOTIFY			10
#define VHQ_MESSAGE_TYPE_DO_TRANSACTION			11
#define VHQ_MESSAGE_TYPE_REGISTER_APP			12



#define VHQ_APP_TRANSACTION_DAILY_CLEARANCE		1
#define VHQ_APP_TRANSACTION_DIAGNOSTICS			2


#define VHQ_APP_STATUS_FREE						1
#define VHQ_APP_STATUS_BUSY						2


/*SVC_STRUCT*/
/**
 * Returned Application Event Structure
 */
struct vhqAppMessageReturn {
	int status;		                        /**< Return status value (Pass/Fail or more detailed status) */
	int messageType;	                    /**< Type of message */
	int messageID;	                        /**< An ID associated with the event that is used for status updates from the app */
	char fileName[MAX_FILENAME_LENGTH];     /**< Filename for VHQ_MESSAGE_TYPE_GET_FILE/VHQ_MESSAGE_TYPE_PUT_FILE message */
	char fileLocation[MAX_FILENAME_LENGTH]; /**< File location for VHQ_MESSAGE_TYPE_GET_FILE/VHQ_MESSAGE_TYPE_PUT_FILE message */
	int eventMask;                          /**< Mask of events for VHQ_MESSAGE_TYPE_EVENT_NOTIFY message */
	int transactionType;                    /**< Type of transaction to perform in VHQ_MESSAGE_TYPE_DO_TRANSACTION message */
	int downloadPercent;                    /**< Percentage of download that is complete for VHQ_EVENT_NOTIFY_DOWNLOAD_PERCENT EVENT message */
};


/**
 * @name Defines for custom info type (see struct vhqAppParameter)
*/
/** \{ */
#define VHQ_PARAMETER_TYPE_IDENTIFIER			1       /**< A parameter to send with identifier information */
#define VHQ_PARAMETER_TYPE_DEVICE_PROFILE		2       /**< A parameter to send with device profile information */
#define VHQ_PARAMETER_TYPE_DIAGNOSTIC			3       /**< A parameter to send with diagnostic information */
/** \} */

/*SVC_STRUCT*/
/**
 * Application Info Structure (used by app to pass app specific info to VHQ)
 */
struct vhqAppParameter {
	char parameterName[64];         /**< NULL terminated parameter name - i.e. "TerminalId" for UX300 */
	char parameterValue[256];       /**< NULL terminated parameter Value - i.e. Terminal ID value for UX300 */
	int parameterType;              /**< Parameter type (i.e. Identifier parameter, device profile parameter, or diagnostic parameter) */
};


/*SVC_STRUCT*/
/**
 * Application Info Structure (used by app to pass app specific info to VHQ)
 */
struct vhqAppInfo {
	struct vhqAppParameter* parameterList;  /**< Pointer to an array of vhqAppParameter[] values */
	int parameterCount;                     /**< Number of vhqAppParameter[] values in the array */
};


/* This is used internally and not directly available to applications */
struct vhqAppResultReturn {
	int status;		                        /**< Return status value (Pass/Fail or more detailed status) */
	int messageType;	                    /**< Type of message */
	int messageID;	                        /**< An ID associated with the event that is used for status updates from the app */
	int appStatus;                          /**< Status of application in response to GET_STATE request */
	int numFileEntries;                     /**< Number of entries in a file (applies to VHQ_MESSAGE_TYPE_SET_PARM_LIST/VHQ_MESSAGE_TYPE_SET_APP_INFO response) */
	unsigned char removeFile;              /**< Whether to remove file after it is received (applies to VHQ_MESSAGE_TYPE_GET_FILE response) */
	char fileName[MAX_FILENAME_LENGTH];     /**< Filename for VHQ_MESSAGE_TYPE_SET_PARM_LIST/VHQ_MESSAGE_TYPE_GET_FILE/VHQ_MESSAGE_TYPE_PUT_FILE message */
	char fileLocation[MAX_FILENAME_LENGTH]; /**< File location for VHQ_MESSAGE_TYPE_SET_PARM_LIST/VHQ_MESSAGE_TYPE_GET_FILE/VHQ_MESSAGE_TYPE_PUT_FILE message */
};


/** Used by application to get events from the agent (status updates, requests, etc.)
 * 
 * @param[in] eventkey Event key used to obtain vhq app events
 * @param[in] flags  Get Event Flags:
 *                      @li - 1 = Non-block
 *                      @li 2 = Last 
 *                      @li 4 = Cancel (options can be or'd)
 * 
 * @return 
 * Struct vhqAppMassageReturn - will return information for processing the new message, NULL on error
 *
 *<b>Errno values</b>:
 *  @li  EINVAL - Invalid argument(s) passed.
 *  @li  ENODEV - Invalid eventkey, or unable to cancel previous event_getEvent() in progress.
 *  @li  EAGAIN - No last event (when flags requesting last).
 *  @li  ENODATA - No event data available (when flags set to non-block and no event).
 *  @li  EBUSY  - Event cancel already in progress, and attempting to cancel event (flags == 4).
 *  @li  EFAULT - Event.size specified, but no data available to read.
 *  @li  EINTR  - Event_getEvent() canceled (by another thread/user). event field of returned
 *            structure may contain cause (see event_cancelWithCause())
 *  @li  ENOMEM - Unable to malloc space for return data.
 *
 * @note IMPORTANT!
 * @li Application must free returned vhqAppEventReturn msg if non-NULL
 */
/*SVC_PROTOTYPE*/ struct vhqAppMessageReturn* vhq_getApplicationMessage(char *eventkey /*REQ*/, int flags /* 0 */);







/** Used by application to send customized application information to the VHQ Agent
 *
 * @param[in] messageID Message ID received in the request message
 * @param[in] appInfo  Customized application info for the agent
 *
 * @return 
 * Int - return status of agent receiving application info
 *
 * @note IMPORTANT!
 * This is used by the application to respond to VHQ_MESSAGE_TYPE_GET_APP_INFO request from the agent
 */
/*SVC_PROTOTYPE*/ int vhq_setApplicationInfo(int messageID /*REQ*/, struct vhqAppInfo appInfo /*REQ*/);


/** Used by application to send parameter file information to the VHQ Agent
 *
 * @param[in] messageID  Message ID received in the request message
 * @param[in] numParamFiles Number of parameter files the application is sending info for
 * @param[in] paramInfoFile Filename that contains application parameter information.
 *							  this file will contain a line for each paramter file
 *							  each line will have 3 whitespace separated fields - Name, Hash Algo, Hash
 *							  i.e. "VHQ.ini SHA1 0ad19e6f4424a459b625fb38de402226b012e8d7"
 *
 * @return 
 * Int - return status of agent receiving application parameter info
 *
 * @note IMPORTANT!
 * This is used by the application to respond to VHQ_MESSAGE_TYPE_GET_PARM_LIST request from the agent
 */
/*SVC_PROTOTYPE*/ int vhq_setAppParameterList(int messageID /*REQ*/, int numParamFiles /*REQ*/, const char* paramInfoFile /*REQ*/);




/** Used by application to exchange file to VHQ agent for upload
 *
 * @param[in] messageID Message ID received in the request message
 * @param[in] result SUCCESS/FAILURE of getting file
 * @param[in] fileLocation Path to the file provided by the application
 * @param[in] removeFile Whether the file should be removed after it is read by requestor
 *
 * @return 
 * Int - return status of agent receiving application file
 *
 * @note IMPORTANT!
 * This is used by the application to respond to VHQ_MESSAGE_TYPE_GET_FILE request from the agent
 */
/*SVC_PROTOTYPE*/ int vhq_appGetFileAvail(int messageID /*REQ*/, int result /*REQ*/, const char* fileLocation /*REQ*/, unsigned char removeFile /*REQ*/);



/** Used by application to send result of VHQ_MESSAGE_TYPE_PUT_FILE to the agent
 *
 * @param[in] messageID Message ID received in the request message
 * @param[in] putFileResult  SUCCESS/FAILURE result of getting the file provided by the agent
 *
 * @return 
 * Int - return status of agent receiving result
 *
* @note IMPORTANT!
 * This is used by the application to respond to VHQ_MESSAGE_TYPE_PUT_FILE request from the agent
 */
/*SVC_PROTOTYPE*/ int vhq_setPutFileResult(int messageID /*REQ*/, int putFileResult /*REQ*/);



/** Used by application to send result of VHQ_MESSAGE_TYPE_DEL_FILE to the agent
 *
 * @param[in] messageID  Message ID received in the request message
 * @param[in] deleteFileResult  SUCCESS/FAILURE result of deleting the file specified by the agent
 *
 * @return 
 * Int - return status of agent receiving result
 *
 * @note IMPORTANT!  
 * This is used by the application to respond to VHQ_MESSAGE_TYPE_DEL_FILE request from the agent
 */
/*SVC_PROTOTYPE*/ int vhq_setDeleteFileResult(int messageID /*REQ*/, int deleteFileResult /*REQ*/);


/** Used by application to send result of VHQ_MESSAGE_TYPE_DO_TRANSACTION to the agent
 *
 * @param[in] messageID  Message ID received in the request message
 * @param[in] transactionResult  VHQ_STATUS_SUCCESS/VHQ_STATUS_ERROR result of the transaction
 *
 * @return
 * Int - return status of agent receiving result
 *
 * @note IMPORTANT!
 * This is used by the application to respond to VHQ_MESSAGE_TYPE_DO_TRANSACTION request from the agent
 */
/*SVC_PROTOTYPE*/ int vhq_setTransactionResult(int messageID /*REQ*/, int transactionResult /*REQ*/);


#ifdef __cplusplus
}

#endif
#endif //SVC_VHQ_H
/// @} */
/// @} */