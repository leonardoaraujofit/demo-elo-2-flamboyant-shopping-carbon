/** @addtogroup cardReader CardReader */
/** @{ */
/** @addtogroup cardreaderRfcrservice CardReader - RFCR: Services */
/** @{ */
/** 
 * @file svc_rfcr.h 
 * 
 * @brief  RF card reader service
 *
 * This header file contains information about RF card reader service.  
 */
 

#ifndef SVC_NFC_H
#define SVC_NFC_H

#include "svcmgrSvcDef.h"

#ifdef __cplusplus
extern "C" {
#endif

/*SVC_SERVICE:rfcr*/

/**
 * @name Additional StatusReturn state codes from rfcr_getEvent
*/
/** \{ */
#define RFCR_NO_STATUS		-202	/**< No status message available */
#define RFCR_NOT_ACTIVE		-204	/**< No status message and port not active */
#define RFCR_MSG_AVAILABLE	999		/**< Status message available - Check msg pointer */
#define RFCR_DONE  			1000	/**< No more messages - Event handler closed */
/** \} */

/**
 * Command IDs to send RFCR/NFC command to Reader using rfcr_Command() API
 * Parameter iCmnd in struct rfcrCmnd should be one of below IDs
*/
/** \{ */
#define NFC_SET_POLL_MODE	4	/**< Sets the Polling mode of Reader */
#define NFC_SET_BURST_MODE	6	/**< This function sets the Burst mode */
#define NFC_SET_EMV_PARS	7	/**< Sets the EMV parameters in Reader */
#define NFC_PASS_THROUGH	9	/**< Sets the Pass-Through mode of Reader */
#define NFC_ACTIVATE_TRANS	10	/**< Sends activate transaction command to Reader */
#define NFC_CANCEL_TRANS	11	/**< Sends cancel transaction command to Reader */
#define NFC_MIFARE_AUTHEN	14	/**< Authenticate Mifare block */
#define NFC_MIFARE_WRITE	16	/**< Write Mifare block */
#define NFC_GET_FW_VERSION	17	/**< Get Firmware Version */
#define NFC_UPDATE_BALANCE	18	/**< Update balance */	
#define NFC_MIFARE_PURSE	22	/**< Mifare Purse command */
#define NFC_HIGH_LEVEL_HALT	23	/**< High level halt command */	
#define NFC_SET_CONF_AID	28  /**< Set Configurable AID */
#define NFC_SET_CONF_GROUP	29  /**< Set Configurable Group */
#define NFC_DEL_CONF_AID	30  /**< Delete Configurable AID */
#define NFC_DEL_CONF_GROUP	31  /**< Delete Configurable Group */
#define NFC_SET_SERIAL_NUM	34  /**< Set Serial Number */
#define NFC_FLUSH_TRK_DATA	35  /**< Flush Track Data */
#define NFC_SET_CAPUB_KEY	36  /**< Set CA Public key */
#define NFC_DEL_CAPUB_KEY	37  /**< Delete CA Public key */
#define NFC_DEL_ALLCA_KEYS	38  /**< Delete All CA Public keys */
#define NFC_GET_VFI_VERSION	45  /**< Get VFI Version */
#define NFC_GET_TYPE_APPRO	46  /**< Get Type Approval Versions */
#define NFC_SET_EX_ADD_AID	47  /**< Set Existing Additional AID Parameters */
#define NFC_SET_NEW_ADD_AID	48  /**< Set New Additional AID Parameters */
#define NFC_DEL_ADD_AID_PAR	49  /**< Delete Additional AID Parameters */
#define NFC_SET_EX_REVO_PAR	50  /**< Set Existing Revocation Parameters */
#define NFC_SET_NW_REVO_PAR	51  /**< Set New Revocation Parameters */
#define NFC_DEL_REVO_PAR	52  /**< Delete Revocation Parameters */
#define NFC_SET_NEW_EXCEPT	53  /**< Set New Exception Parameters */
#define NFC_DEL_EXCEP_PAR	54  /**< Delete Exception Parameters */
#define NFC_SET_ADD_RED_PAR	55	/**< Set Additional Reader Parameters */
#define NFC_RES_DEFAULT_CON	56	/**< Restore Default Configuration */
#define NFC_GET_FW_FULL_VER	57	/**< Get Full Firmware Version */
#define NFC_MUTUAL_AUTHENT	59	/**< Mutual Authentication */
#define NFC_GENERATE_KEY	60	/**< Generate Key */
#define NFC_RFCR_LIB_VER 	62	/**< Get RFCR library version */
#define NFC_LED_HANDLER 	63	/**< To control antenna module LEDs */
#define NFC_BUZZER_HANDLER 	64	/**< To control buzzer */
#define NFC_RFCRHW_SET 		65	/**< To set rfcrhw local variable */
#define NFC_RFCRHW_GET 		66	/**< To get rfcrhw local variable */
#define NFC_FASTUI_SET 		67	/**< To set "Fast UI" mode */
#define NFC_FASTUI_GET 		68	/**< To get "Fast UI" mode */
#define NFC_ANTENNA_GET 	69	/**< To get Antenna Status */
#define NFC_SET_TTQ 		70	/**< To set TTQ parameters */
#define NFC_SET_VCL             72      /**< To set VCL encryption */
#define NFC_GET_VCL             73      /**< To get VCL encryption status */
#define NFC_RESET_VCL           74      /**< To reset VCL module and return status */
#define NFC_DISC_THREAD 	77	/**< Returns discovery thread running status */
/** \} */

/**
 * Command IDs to send RFCR/NFC command to Reader using rfcr_CommandBin() API
 * Parameter iCmnd in struct rfcrCmnd should be one of below IDs
*/
/** \{ */
#define NFC_EMB_APP_PING	1	/**< Send Embedded App Ping command */
#define NFC_CLEAR_CONFIG	2	/**< Send Embedded App Clear Config command */
#define NFC_GET_EMB_VER		3	/**< Send Embedded App Get Version command */
#define NFC_GET_TRANS_RES	5	/**< Reads the payload data from Reader */
#define NFC_GET_EMV_PAR		8	/**< Gets the EMV parameters from Reader */
#define NFC_GET_FUL_TRACK	12	/**< Gets the full track data from Reader */
#define NFC_POL_FOR_TOKEN	13	/**< Poll for token */
#define NFC_MIFARE_READ		15	/**< Read Mifare block */
#define NFC_ISO_APDU_EXCH	19	/**< ISO APDU Exchange */
#define NFC_PCD_SINGLE_EXCH	20	/**< PCD Single Exchange */
#define NFC_GET_PCD_PICC_PR	21	/**< Get PCD PICC Parameters */
#define NFC_GET_CONF_AID	24  /**< Get Configurable AID */
#define NFC_GET_ALL_AID		25  /**< Get all AIDs */
#define NFC_GET_CONF_GROUP	26  /**< Get Configurable Group */
#define NFC_GET_ALL_GROUPS	27  /**< Get all Groups */
#define NFC_GET_CAPUB_KEY	32  /**< Get CA Public key */
#define NFC_GET_SERIAL_NUM	33  /**< Get Serial Number */
#define NFC_GET_ADD_AID_PAR	39  /**< Get Additional AID Parameters */
#define NFC_GET_REVOC_PAR	40  /**< Get Revocation Parameters */
#define NFC_GET_ALL_ADD_AID	41  /**< Get All Additional AID Parameters */
#define NFC_GET_ALL_REVOC	42  /**< Get All Revocation Parameters */
#define NFC_GET_ALL_EXCEPT	43  /**< Get All Exception Parameters */
#define NFC_GET_ADD_PARAM	44  /**< Get Additional Reader Parameters */
#define NFC_ENCRYPT_INIT	58	/**< Init Encryption */
#define NFC_ENCRYPTION_CMND	61	/**< Encryption Command */
#define NFC_GET_EMB_CDV		71 	/**< Send Embedded App Get CD Version command */
#define NFC_GET_CLEAR_PAN       75      /**< Get the clear PAN */
#define NFC_ACT_TRS_DATA	76	/**< Sends activate transaction and get data from Reader */
#define NFC_GET_WAL_DATA	78	/**< Sends Get Wallets Data command */
#define NFC_FIL_MGR_CMND	79	/**< Sends File Manager command */
/** \} */

/*SVC_STRUCT*/
/**
 * Returned Status Structure
 */
struct rfcrStatusReturn {
    int iStatus;			/**< Return state value */
    char *pMsg;				/**< Pointer to return message */
};

/*SVC_STRUCT*/
/**
 * Command Structure
 */
struct rfcrCmnd {
	int iCmnd;
	int iTimeout;
	int iOption;
	int iCmndSize;
    char pCmndData[1024];							
};

/*SVC_STRUCT*/
/**
 * Response Structure
 */
struct rfcrResp {
	int iStatus;
	int iRespSize;
    char pRespData[1024];							
};

/*SVC_STRUCT*/
/**
 * Binary Data Structure
 */
struct rfcrBinData {
    void* data;
  	int data_count;
};

/** Obtain the version of the NFC service 
 *
 * @return 
 * Version struct defined in svcmgrSvcDef.h
 */
/*SVC_PROTOTYPE*/ struct version rfcr_getVersion (void);

/** Init method for initializing and configuring CTLS/NFC reader 
 *
 * @return
 * @li 0 Success
 * @li -1 Failure
 */
/*SVC_PROTOTYPE*/ int rfcr_Init (void);

/** Exit method for CTLS/NFC service
 * 
 * @return 
 * @li 0 Success
 * @li -1 Failure
 */
/*SVC_PROTOTYPE*/ int rfcr_Exit (void);

/** Ping method for checking that CTLS/NFC reader is present
 *
 * @return 
 * @li 0  Success
 * @li -1 Failure
 */
/*SVC_PROTOTYPE*/ int rfcr_Ping (void);


/** Antenna method for controling the antenna
 * 
 * @param[in] onOff  @li 0  Disable the RF Antenna  @li 1  Enable the RF Antenna
 *
 * @return 
 * @li 0  Success
 * @li -1 Failure
 */
/*SVC_PROTOTYPE*/ int rfcr_Antenna (int onOff /*REQ*/);



/** Send NFC configuration parameters to CTLS/NFC reader
 * 
 * @param[in] scmnd struct rfcrCmnd
 *
 * @return 
 * @li 0 Success
 * @li -1 Failure
 *
 * <b>Errno values</b>: 
 * @li ENODEV - When the device is not valid.
 */
/*SVC_PROTOTYPE*/ struct rfcrBinData rfcr_Config (struct rfcrCmnd scmnd /*REQ*/);


/** Enable methot starts CTLS/NFC capability 
 * 
 * @param[in] scmnd struct rfcrCmnd
 * 
 * @return 
 * @li 0 Success
 * @li -1 Failure
 * 
 * <b>Errno values</b>: 
 * @li ENODEV - When the device is not valid.
 */
/*SVC_PROTOTYPE*/ struct rfcrBinData rfcr_Enable (struct rfcrCmnd scmnd /*REQ*/);


/** Send cancel command to stop discovery thread to CTLS/NFC reader
 * 
 *  @param[in] scmnd struct rfcrCmnd
 * 
 * @return 
 * @li 0 Success
 * @li -1 Failure
 *
 * <b>Errno values</b>: 
 * @li ENODEV - When the device is not valid.
 */
/*SVC_PROTOTYPE*/ struct rfcrBinData rfcr_Cancel (struct rfcrCmnd scmnd /*REQ*/);



/** Send command specified by command ID to CTLS/NFC reader
 * 
 * @param[in] scmnd struct rfcrCmnd
 * 
 * @return 
 * @li 0 Success
 * @li -1 Failure
 * 
 * <b>Errno values</b>: 
 * @li ENODEV - When the device is not valid.
 */
/*SVC_PROTOTYPE*/ struct rfcrResp rfcr_Command (struct rfcrCmnd scmnd /*REQ*/);



/** Send command specified by command ID to CTLS/NFC reader
 * 
 * @param[in] scmnd struct rfcrCmnd
 * 
 * @return 
 * @li 0 Success
 * @li -1 Failure
 * 
 * <b>Errno values</b>: 
 * @li ENODEV - When the device is not valid.
 */
/*SVC_PROTOTYPE*/ struct rfcrBinData rfcr_CommandBin (struct rfcrCmnd scmnd /*REQ*/);



/** Read data from CTLS card presented to CTLS/NFC reader
 * 
 * @param[in] seconds timeout in seconds
 * 
 * @return 
 * card data 
 * 
 * <b>Errno values</b>: 
 * @li ENODEV - When the device is not valid.
 */
/*SVC_PROTOTYPE*/ struct rfcrBinData rfcr_CardRead (int seconds /*REQ*/);



/** Read event from a CTLS/NFC operation
 * 
 * @param[in] handle  Handle to serial port performing serial download
 * @param[in] flags Get Event Flags:
 *                   @li - 1 = Non-block
 *                   @li 2 = Last 
 *                   @li 4 = Cancel (options can be or'd)
 *
 * @return 
 * Struct statusReturn - state will reflect current download state, Depending on state, msg may have an ASCII status string send from the download server.
 * this state may have funny values. \n
 * 
 * @note  IMPORTANT!
 * Application must free serialStatusReturn msg.
 */
/*SVC_PROTOTYPE*/ struct rfcrStatusReturn rfcr_getEvent (int flags, int handle /*REQ*/);


#ifdef __cplusplus
}
#endif

#endif //SVC_NFC_H
/// @} */
/// @} */
