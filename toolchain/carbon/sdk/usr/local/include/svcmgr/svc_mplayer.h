/** @addtogroup media Media */
/** @{ */
/** @addtogroup mediaplayerservice Media - Mediaplayer: Services */
/** @{ */
/** 
 *  @file svc_mplayer.h 
 *
 *	@brief MPlayer service.
 *
 * 
 */


/*
 *	Copyright, 2012 VeriFone Inc.
 *	2099 Gateway Place, Suite 600
 *	San Jose, CA.  95110
 *
 *  All Rights Reserved. No part of this software may be reproduced,
 *  transmitted, transcribed, stored in a retrieval system, or
 *  translated into any language or computer language, in any form
 *  or by any means electronic, mechanical, magnetic, optical,
 *  chemical, manual or otherwise, without the prior permission of
 *  VeriFone Inc.
 */
#ifndef SVC_MPLAYER_H
#define SVC_MPLAYER_H

#include "svcmgrSvcDef.h"

#ifdef __cplusplus
extern "C" {
#endif

/*SVC_SERVICE:mplayer*/

/** 
 * @name Defines for mplayer status (see struct mplayerStatusReturn and mplayer_getPlayEvent()
 */
/** \{ */
#define MPLAYER_STATUS_END              1 /**< Video ended, if repeat set, indicates repeating video */
#define MPLAYER_STATUS_PAUSE            2 /**< Video has been paused */
#define MPLAYER_STATUS_RESUME           3 /**< Video has been resumed */
#define MPLAYER_STATUS_TERMINATED       4 /**< Video terminated by mplayer_stop() or mplayer_stopall() */
#define MPLAYER_STATUS_ERROR_IO        10 /**< Invalid video file format/type or access error */
#define MPLAYER_STATUS_ERROR_FILE_FMT  11 /**< Invalid file format */
/** \} */


/*SVC_STRUCT*/
/** Returned Status Structure
 * 
 */
struct mplayerStatusReturn {
	int status;		/**< Return state value */
	char msg[80];	/**< Return message (may be empty) */
};


/*SVC_STRUCT*/
/**  Video play parameters
 *
 */
struct mplayerPlayParams {
	char filename[512]/*""*/;   /**< Name of file (full pathing) to be played */
	int eventflag/*0*/;         /**< Nonzero value to enable event status for playback notification (see mplayer_getPlayEvent()) */
	double offset/*0*/;         /**< Offset - new position, measured in seconds, is obtained by adding offset seconds to the position specified by whence */
	int whence/*0*/;            /**< Whence - set to SEEK_SET (0), SEEK_CUR (1), or SEEK_END (2), the offset is relative to the start of the file, the current position indicator, or end-of-file, respectively. */
	int evaslayer/*0*/;         /**< Evas layer to place video evas object on (0 == use default video layer (1000)) */
	int x_origin/*0*/;          /**< x pixel position for upper-left corner */
	int y_origin/*0*/;          /**< y pixel position for upper-left corner */
	int width/*0*/;             /**< Width in pixels of video (0 - default to actual video width - w/o scaling) */
	int height/*0*/;            /**< Height in pixels of video (0 - default to actual video height - w/o scaling) */
};


/*SVC_STRUCT*/
/** Video multiplay play parameters
 */
struct mplayerMultiPlayParams {
	char master_filename[128]/*""*/;    /**< Name of master file (full pathing) to be played (top-left) */
	char slave1_filename[128]/*""*/;    /**< Name of slave-1 file (full pathing) to be played (top-right) */
	char slave2_filename[128]/*""*/;    /**< Name of slave-2 file (full pathing) to be played (bottom-left) */
	char slave3_filename[128]/*""*/;    /**< Name of slave-3 file (full pathing) to be played (bottom-right) */
	int  eventflag/*0*/;                /**< Nonzero value to enable event status for playback notification (see mplayer_getPlayEvent()) */
	int  repeat/*0*/;                   /**< Nonzero valut to enable repeat for video playback */
	int  evaslayer/*0*/;                /**< Evas layer to place video evas object on (0 == use default video layer (1000)) */
	int  x_origin/*0*/;                 /**< x pixel position for upper-left corner (if > 1/2 width of screen, set to 0) */
	int  y_origin/*0*/;                 /**< y pixel position for upper-left corner (if > 1/2 height of screen, set to 0) */
};


/*SVC_STRUCT*/
/**  Returned Status Structure
 */
struct mplayerPlayReturn {
	int handle;             /**< Reference to mplayer object for close, pause, resume, etc */
	char eventkey[32];	    /**<  Event key (if requested via eventFlag param to mplayer_play()), empty string otherwise */
};


/** Obtain the version of the mplayer service
 * 
 * @return 
 * Version struct defined in svcmgrSvcDef.h
 */
/*SVC_PROTOTYPE*/ struct version mplayer_getVersion(void);


/** Read event from a play operation
 * 
 * @param[in] eventkey Event key used to obtain mplayer play status (obtained from mplayer_play() returned struct mplayerPlayReturn)
 * @param[in] flags  Get Event Flags:
 *                     @li - 1 = Non-block
 *                     @li 2 = Last 
 *                     @li 4 = Cancel (options can be or'd)
 * 
 * @return 
 * struct mplayerStatusReturn - state will reflect current play state, Depending on state, msg may have an ASCII status string.
 *
 * @note IMPORTANT! \n
 *  Application must free mplayerStatusReturn msg
 */
/*SVC_PROTOTYPE*/ struct mplayerStatusReturn mplayer_getPlayEvent(char *eventkey /*REQ*/, int flags /* 0 */);


/** Close play event
 * 
 * @param[in] eventkey Event key to be closed (obtained from mplayer_play() returned struct mplayerPlayReturn)
 * 
 * @return
 * @li If >= 0 then Success
 * @li If = -1 then error
 */
/*SVC_PROTOTYPE*/ int mplayer_closePlayEvent(char *eventkey /*REQ*/);


/** Play a mplayer file beginning from offset (based on whence)
 *
 * Base Video Support:\n
 *  - H.264 - Baseline Profile, levels 1-3, 12Mbps
 *  - MPEG-4 - Simple profile, levels 0-5, 8Mbps
 *  - H.263 - Profile 0, levels 10-70, 8Mpbs\n
 *  (VeriFone recommends that the video container be .AVI format)
 *
 * @param[in] params Structure containing parameters (see struct mplayerPlayParams)
 * 
 * @return 
 * Struct mplayerPlayReturn - status whether play (initialization, etc) succeeded, and the event key if
 *  eventFlag != 0.
 * 
 * @li status >= 0 then Success
 * @li  = -1 then error, check errno:
 * 
 *  <b>Errno values</b>:
 * @li ENOENT - when filename is unreadable or unknown.
 * @li ENOEXEC - Media file not a recognized video format
 * @li EIO - media driver error trying to start mplayer
 * @li EACCES - when internal error
 * @li EINVAL - when offset is invalid for given video file
 */
/*SVC_PROTOTYPE*/ struct mplayerPlayReturn mplayer_play(struct mplayerPlayParams params);


/** Play a mplayer video file and repeat
 *
 * Base Video Support:\n
 *  - H.264 - Baseline Profile, levels 1-3, 12Mbps
 *  - MPEG-4 - Simple profile, levels 0-5, 8Mbps
 *  - H.263 - Profile 0, levels 10-70, 8Mpbs\n
 *  (VeriFone recommends that the video container be .AVI format)
 *
 * @param[in] filename Name of file (full pathing) to be played
 * @param[in] params Structure containing parameters (see struct mplayerPlayParams)
 *
 * @brief Note: offset and whence are treated as zero for this method.
 * 
 * @return Struct mplayerPlayReturn - status whether play (initialization, etc) succeeded, and the event key if
 *  eventFlag != 0.
 *
 * @li status >= 0 then Success
 * @li = -1 then error, check errno:
 *
 * <b>Errno values</b>:
 * @li ENOENT - when filename is unreadable or unknown.
 * @li ENOEXEC - Media file not a recognized video format
 * @li EIO - media driver error trying to start mplayer
 * @li EACCES - when internal error
 */
/*SVC_PROTOTYPE*/ struct mplayerPlayReturn mplayer_playRepeat(struct mplayerPlayParams params);


/** Play upto 4 video's stitched together with optional repeat
 *
 * Base Video Support:\n
 *  - H.264 - Baseline Profile, levels 1-3, 12Mbps
 *  - MPEG-4 - Simple profile, levels 0-5, 8Mbps
 *  - H.263 - Profile 0, levels 10-70, 8Mpbs\n
 *  (VeriFone recommends that the video container be .AVI format)
 *
 *  If more than 1 filename is specified, master video must contain the audio track (if audio desired),
 *  slave1 is stitched to the right of the master video, slave2 if specified is stitched below the
 *  master video, and slave3 if specified is stitched to the right of slave2 and below slave1.
 *  Slave video's should not contain a audio track.
 *  Typical use is to play full screen video using the master and slave1 only, placing the origin at [0,0].
 *
 * @param[in] params Structure containing parameters (see struct mplayerMultiPlayParams)
 *
 * @return 
 * Struct mplayerPlayReturn - status whether play (initialization, etc) succeeded, and the event key if
 *  eventFlag != 0.
 * 
 * @li status >= 0 then Success
 * @li  = -1 then error, check errno:
 *
 *<b>Errno values</b>:
 * @li ENOENT - when filename is unknown.
 * @li EIO - media driver error trying to start mplayer
 * @li EACCES - when internal error
 * @li EINVAL - when offset is invalid for given video file
 */
/*SVC_PROTOTYPE*/ struct mplayerPlayReturn mplayer_multiplay(struct mplayerMultiPlayParams params);


/** Stop any playing mplayer
 *
 * @param[in] handle Reference to mplayer object returned by mplayer_play()
 *
 * @return 
 * @li If >= 0 then Success
 * @li If = -1 then error, errno:
 * 
 *<b>Errno values</b>:
 * @li EINVAL - invalid handle
 */
/*SVC_PROTOTYPE*/ int mplayer_stop(int handle /*REQ*/);


/** Stop all playing mplayer
 *
 * @return 
 * @li If >= 0 then Success
 * @li If = -1 then error
 */
/*SVC_PROTOTYPE*/ int mplayer_stopall(void);


/** Pause any playing mplayer
 *
 * @param[in] handle Reference to mplayer object returned by mplayer_play()
 * 
 * @return 
 * @li If >= 0 then Success
 * @li  If = -1 then error, errno:
 * 
 * <b>Errno values</b>:
 *   EINVAL - invalid handle
 */
/*SVC_PROTOTYPE*/ int mplayer_pause(int handle /*REQ*/);


/** Resume any playing mplayer
 *
 * @param[in] handle Reference to mplayer object returned by mplayer_play()
 * 
 * @return 
 * @li If >= 0 then Success
 * @li  If = -1 then error, errno:
 *
 * <b>Errno values</b>:
 * @li EINVAL - invalid handle
 */
/*SVC_PROTOTYPE*/ int mplayer_resume(int handle /*REQ*/);


/** Resume any playing mplayer
 *
 * @param[in] handle Reference to mplayer object returned by mplayer_play()
 * @param[in] offset New position, measured in seconds, is obtained by adding offset seconds to the position
 *                     specified by whence
 * @param[in] whence Set to SEEK_SET (0), SEEK_CUR (1), or SEEK_END (2), the offset is relative to the start of the file,
 *                     the current position indicator, or end-of-file, respectively.
 *
 * @return 
 * @li If >= 0 then Success
 * @li  If = -1 then error, errno:
 *
 * <b>Errno values</b>:
 *   EINVAL - invalid handle
 */
/*SVC_PROTOTYPE*/ int mplayer_seek(int handle /*REQ*/, double offset /* 0 */, int whence /* 0 */);


/** Repeat any playing video
 *
 * If repeat mode is on, when a playback has reached the end of the media,
 *  it will restart the playback from the beginning.
 *
 * @param[in] handle Reference to sound object returned by mplayer_play()
 * @param[in] state Repeat state to set, 1 = enable repeat, 0 = disable
 *
 * @return
 * @li If >= 0 then Success
 * @li If = -1 then error, errno:
 *
 *<b>Errno values</b>:
 * @li  EINVAL - invalid handle
 */
/*SVC_PROTOTYPE*/ int mplayer_repeat(int handle /*REQ*/, int state /* 1 */);


/** Enable/disable sound on any playing video\n
 * If sound is disabled, any sound track included in video is ignored (silent).
 *
 * @param[in] handle Reference to sound object returned by mplayer_play()
 * @param[in] state Sound state to set, 1 = enable sound, 0 = disable
 *
 * @return 
 * @li If >= 0 then Success
 * @li If = -1 then error, errno:
 * 
 * <b>Errno values</b>:
 * @li EINVAL - invalid handle
 */
/*SVC_PROTOTYPE*/ int mplayer_sound(int handle /*REQ*/, int state /* 0 */);


/** Enable/disable video double buffering by fst
 *
 * @param[in] enable Video double buffering, 1 = enable, 0 = disable
 * 
 * @return 
 * @li If >= 0 then success and rtn value is previous double buffering state;
 * @li If = -1 then error
 */
/*SVC_PROTOTYPE*/ int mplayer_videoDoubleBuffer(int state /* 1 */);


#ifdef __cplusplus
}
#endif
#endif //SVC_MPLAYER_H

/// @}*/
/// @}*/
