/** @addtogroup utils Utils */
/** @{ */
/** @addtogroup systeminfservice Utils - SysInfo: Services */
/** @{ */
/** 
 *  @file svc_sysinfo.h 
 *
 *  @brief System information service
 *
 *	This header file contains information about System Information service interface methods.
 *
 * The System Information (sysinfo) service contains methods
 *  for obtaining system information for such things as: software versions,
 *  hardware features/options, etc.	  
 */
  

   /*
    *      Copyright, 2011 VeriFone Inc.
    *      2099 Gateway Place, Suite 600
    *      San Jose, CA.  95110
    *
    *  All Rights Reserved. No part of this software may be reproduced,
    *  transmitted, transcribed, stored in a retrieval system, or
    *  translated into any language or computer language, in any form
    *  or by any means electronic, mechanical, magnetic, optical,
    *  chemical, manual or otherwise, without the prior permission of
    *  VeriFone Inc.
    */

#ifndef SVC_SERVICE_SYSINFO_H
#define SVC_SERVICE_SYSINFO_H

#ifdef __cplusplus
extern "C" {
#endif

#include "svcmgrSvcDef.h"


/*SVC_SERVICE:sysinfo*/


/**
 * @name I/O Module Features
*/
/** \{ */
#define IOMOD_CTLS          0x00000001      /**< Contactless/RFID */
#define IOMOD_WIFI          0x00000002      /**< WiFi */
#define IOMOD_COM1          0x00000004      /**< COM 1 */
#define IOMOD_COM2          0x00000008      /**< COM 2 */
#define IOMOD_AUDIO         0x00000010      /**< Audio */
#define IOMOD_USB_OTG       0x00000020      /**< USB On-the-go */
#define IOMOD_USB_HOST      0x00000040      /**< USB Host */
#define IOMOD_POE           0x00000080      /**< Power Over Ethernet */
#define IOMOD_ETHERNET      0x00000100      /**< Ethernet */
#define IOMOD_BLUETOOTH     0x00000200      /**< Bluetooth */
#define IOMOD_GPRS          0x00000400      /**< GPRS */
#define IOMOD_TAILGATE      0x00000800      /**< IBM Tailgate */
#define IOMOD_MULTI         0x00001000
#define IOMOD_POWER         0x00002000
#define IOMOD_MDB           0x00004000
#define IOMOD_RS485         0x00008000
#define IOMOD_WAN           0x00010000
#define IOMOD_USB_DEVICE    0x00020000      /**< USB IOModule-6 issue */
#define IOMOD_COM3          0x00040000
/** \} */

/*SVC_STRUCT*/
/**
 * @name Module information structure
 */
struct module_s {
	unsigned int features;	    /**< Type of module attached (string) */
	char serialNumber[16];	    /**< S/N of module */
};

/**
 * @name Used to identify type of comm port interface (commType)
*/
/** \{ */
#define COMMTYPE_RS232   0	/**< RS232 Interface */
#define COMMTYPE_RS485   1	/**< RS485 Interface */
/** \} */

/**
 * @name Used to indicate connection state of comm port (connectionStatus)
*/
/** \{ */
#define COMMSTATUS_UNKNOWN       0	/**< Unknown connection state */
#define COMMSTATUS_CONNECTED     1	/**< Connected */
#define COMMSTATUS_DISCONNECTED  2	/**< Disconnected (no cable attached) */
/** \} */


/*SVC_STRUCT*/
/**
 * @name Comm Port Status structure - used by
 */
struct comm_s {
	short int available; /**< A 1 indicates the comm port is present (if not, other params in struct meaningless) */
	short int commType;	/**< Type of comm port interface (RS232/485) (see COMMTYPE_ defines) */
	short int connectionStatus; /**< Indicates if cable attached/present (some ports do not support this) (see COMMSTATUS_ defines) */
};

/**
 * @name Used to identify type of USB interface (usbType)
*/
/** \{ */
#define USBTYPE_NONE   0	/**< No Interface */
#define USBTYPE_DEVICE 1	/**< Device Interface */
#define USBTYPE_HOST   2	/**< Host Interface */
#define USBTYPE_BOTH   3	/**< Both USB Host and USB Device supported */
/** \} */


/**
 * @name Max string lengths for version, package/bundle name, and timestamp
*/
/** \{ */
#define MAX_VERSION_STRING_LENGTH       32
#define MAX_PACKAGE_NAME_LENGTH         32
#define MAX_USER_NAME_LENGTH            16
#define MAX_SIGNER_NAME_LENGTH          8
#define MAX_PACKAGE_TYPE_LENGTH         24
#define MAX_TIMESTAMP_STRING_LENGTH     32
#define MAX_PACKAGE_DATE_LEN            16
#define MAX_PACKAGE_CATEGORY_LEN        24
#define MAX_PACKAGE_OPTION_LEN          80
#define MAX_CRC_STRING_LENGTH     		32

/** \} */


/*SVC_STRUCT*/
/**
 * @name Cable Configuration structure
 * 
 * @note The cable configuration is only available on the Mx9xx platform.
 */
struct cable_s {
    short int multiPort; /**< 1 = Multiport cable connected, 0 = Not connected */
	short int usbType;	/**< Is USB port available, and if so which one (see USBTYPE_ defines) */
	short int ethernet;	/**< Is Ethernet port available on cable (0=NO, 1=YES) */
	struct comm_s port1;	/**< Configuration of comm port1 */
	struct comm_s port2;	/**< Configuration of comm port2 */
	struct comm_s port3;	/**< Configuration of comm port3 */
};



/*SVC_STRUCT*/
/**
 * @name Memory Usage structure
 */
struct memory_s {
	unsigned long int total; /**< Total amount of memory available */
	unsigned long int free;  /**< Total free memory */
	unsigned long int used;  /**< Total used memory */
};

/*SVC_STRUCT*/
/**
 *  @name Memory Usage Information structure
 */
struct memoryUsage_s {
	struct memory_s ram; /**< RAM memory usage */
	struct memory_s flash; /**< FLASH memory usage */
};


/*SVC_STRUCT*/
/**
 *  @name Platform Information structure
 */
struct platform_s  {
	char model[8];                      /**< Platform model (Mx915, Mx925,...) */
	char serialNumber[16];              /**< Serial number of unit (xxx-xxx-xxx) */
	char unitID[12];                    /**< Unit ID */
	char MAC[16];                       /**< Units MAC address (aabbccddeeff) */
	struct module_s ioModule;           /**< IO Module feature and S/N */
	int displayPixelX;                  /**< Display pixel width */
	int displayPixelY;                  /**< Display pixel height */
	struct memoryUsage_s memoryUsage;   /**< Memory usage */
	struct cable_s cable;               /**< Attached cable information (Only available on Mx9xx platform) */
};


/*SVC_STRUCT*/
/**
 *  @name Platform Information Extended structure
 */
struct platformExtended_s  {
	char model[32];                     /**< Platform model (Mx915, Mx925, SWFWIFIBT10,...) */
	char serialNumber[24];              /**< Serial number of unit (xxx-xxx-xxx) */
	char unitID[16];                    /**< Unit ID */
	char MAC[16];                       /**< Units MAC address (aabbccddeeff) */
	int keypad;							/**< Non-zero if keypad present */
	int funcKeys;						/**< Non-zero if function keys present */
	int keyCount;						/**< Number of standard keys */
	int touch;							/**< Non-zero if touch panel present */
	int display;						/**< Non-zero if display present */
	int displayColor;					/**< Non-zero if display is a color display */
	int displayPortrait;				/**< Non-zero if display is Portrait oriented (else Landscape) */
	int displayPixelX;                  /**< Display pixel width */
	int displayPixelY;                  /**< Display pixel height */
	int msr;							/**< Non-zero if msr present */
	int ctls;							/**< Non-zero if CTLS present */
	int smartcard;						/**< Non-zero if smartcard present */
	int wrenchman;						/**< Non-zero if wrenchman interface present */
	struct memoryUsage_s memoryUsage;   /**< Memory usage */
};


/*SVC_STRUCT*/
/**
 *  @name Bundle version information structure
 */
struct bundleVersionInfo_s {
    char version[MAX_VERSION_STRING_LENGTH]; /**< Version of bundle */
};

/*SVC_STRUCT*/
/**
 *  @name Bundle information structure
 */
 struct bundleInfo_s {
    char name[MAX_PACKAGE_NAME_LENGTH];             /**< Name of bundle */
    char version[MAX_VERSION_STRING_LENGTH];        /**< Version of bundle */
    char user[MAX_USER_NAME_LENGTH];                /**< User assigned to bundle (root, sys1...sys8, usr1...usr8 */
    char category[MAX_PACKAGE_CATEGORY_LEN];
    char date[MAX_PACKAGE_DATE_LEN];
    char option[MAX_PACKAGE_OPTION_LEN];
};

/*SVC_STRUCT*/
/**
 *  @name Bundle collection
 */
struct bundleCollection_s {
    int size;                        /**< Size of bundleCollection_s structure */
    int bundles_count;               /**< Number of bundles */
    struct bundleInfo_s *bundles;   /**< Bundles */
};


/*SVC_STRUCT*/
/**
 *  @name Package information extended structure
 */
struct packageInfoExtended_s {
	char name[MAX_PACKAGE_NAME_LENGTH];             /**< Name of package */
	char version[MAX_VERSION_STRING_LENGTH];        /**< Version of package */
	char user[MAX_USER_NAME_LENGTH];                /**< User assigned to package (root, sys1...sys8, usr1...usr8 */
	char signer[MAX_SIGNER_NAME_LENGTH];            /**< Signer of package (os, sys, user) */
	char packageType[MAX_PACKAGE_TYPE_LENGTH];      /**< Tye of package (os, user, userflash, config, service, ...) */
	char timestamp[MAX_TIMESTAMP_STRING_LENGTH];    /**< Timestamp of package */
    char bundleName[MAX_PACKAGE_NAME_LENGTH];       /**< Bundle name */
    char bundleVersion[MAX_VERSION_STRING_LENGTH];  /**< Bundle version */
    char bundleDate[MAX_PACKAGE_TYPE_LENGTH ];	     /**< Bundle date */
};

/*SVC_STRUCT*/
/**
 *  @name Package collection extended
 */
struct packageCollectionExtended_s {    
    int size;                                   /**< Size of packageInfoExtended_s */
    int packages_count;                         /**< Number of packages */
    struct packageInfoExtended_s *packages;     /**< Packages */
};

/*SVC_STRUCT*/
/**
 *  @name Package information structure
 */
struct packageInfo_s {
    char name[MAX_PACKAGE_NAME_LENGTH];                     /**< Name of package */
    char version[MAX_VERSION_STRING_LENGTH];                /**< Version of package */
    char user[MAX_USER_NAME_LENGTH];                        /**< User assigned to package (root, sys1...sys8, usr1...usr8 */
    char signer[MAX_SIGNER_NAME_LENGTH];                    /**< Signer of package (os, sys, user) */
    char packageType[MAX_PACKAGE_TYPE_LENGTH];              /**< Type of package (os, user, userflash, config, service, ...) */
    char timestamp[MAX_TIMESTAMP_STRING_LENGTH];            /**< Timestamp of package */
};

/*SVC_STRUCT*/
/**
 *  @name Package collection
 */
struct packageCollection_s {
    int packages_count;                 /**< Number of packages */
    struct packageInfo_s *packages;     /**< Packages */
};


/**
 * @name Used to identify type of module/controller (moduleType)
*/
/** \{ */
#define MODULETYPE_TOUCH_RESISTIVE	0x0001  /**< Module for resistive touch panel */
#define MODULETYPE_TOUCH_CAPACITIVE	0x0002  /**< Module for capacitive touch panel */
#define MODULETYPE_WRENCHMAN		0x0100	/**< Module for wrenchman protocol */
/** \} */


/*SVC_STRUCT*/
/**
 *  @name firmware information structure
 */
struct firmwareInfo_s {
    int  moduleType;								/**< Module Type, see #defines */
    char name[MAX_PACKAGE_NAME_LENGTH];				/**< Name of firmware/module/driver */
    char hw_version[MAX_VERSION_STRING_LENGTH];		/**< Version of hardware (if separate module) */
    char sw_version[MAX_VERSION_STRING_LENGTH];		/**< Version of firmware */
    char sw_checksum[MAX_CRC_STRING_LENGTH];		/**< Firmware checksum or crc */
    char config_checksum[MAX_CRC_STRING_LENGTH];	/**< Configuration checksum or crc */
};

/*SVC_STRUCT*/
/**
 *  @name firmware collection
 */
struct firmwareCollection_s {
    int firmware_count;                 /**< Number of firmwareInfo_s */
    struct firmwareInfo_s *firmware;    /**< firmware */
};


/*SVC_STRUCT*/
/**
 *  @name OS software version structure
 */
struct softwareVersion_s {
	char sbi_version[MAX_VERSION_STRING_LENGTH];            /**< Secure Boot Image version */
	char vault_version[MAX_VERSION_STRING_LENGTH];          /**< Vault version */
	char uboot_version[MAX_VERSION_STRING_LENGTH];          /**< U-Boot version */
	char cib_version[MAX_VERSION_STRING_LENGTH];            /**< Control Information Block version */
	char mib_version[MAX_VERSION_STRING_LENGTH];            /**< Machine Information Block version */
	char kernel_version[MAX_VERSION_STRING_LENGTH];         /**< Linux Kernel version */
	char rfs_version[MAX_VERSION_STRING_LENGTH];            /**< Overall RFS version */
	char release_version[MAX_VERSION_STRING_LENGTH];        /**< Build Release version */
	char rfsSecurity_version[MAX_VERSION_STRING_LENGTH];    /**< Application Manager version */
};

/*SVC_STRUCT*/
/**
 *  @name OS software version structure
 */
struct softwareVersionExtended_s {
    char sbi_version[MAX_VERSION_STRING_LENGTH];            /**< Secure Boot Image version */
    char vault_version[MAX_VERSION_STRING_LENGTH];          /**< Vault version */
    char uboot_version[MAX_VERSION_STRING_LENGTH];          /**< U-Boot version */
    char cib_version[MAX_VERSION_STRING_LENGTH];            /**< Control Information Block version */
    char mib_version[MAX_VERSION_STRING_LENGTH];            /**< Machine Information Block version */
    char kernel_version[MAX_VERSION_STRING_LENGTH];         /**< Linux Kernel version */
    char rfs_version[MAX_VERSION_STRING_LENGTH];            /**< Overall RFS version */
    char release_version[MAX_VERSION_STRING_LENGTH];        /**< Build Release version */
    char rfsSecurity_version[MAX_VERSION_STRING_LENGTH];    /**< Application Manager version */
    char sred_version[MAX_VERSION_STRING_LENGTH];           /**< SRED version */
    char openprotocol_version[MAX_VERSION_STRING_LENGTH];   /**< OpenProtocol version */
};

/*SVC_STRUCT*/
/**
 *  @name MIB Information structure
 */
struct MIBInfo_s {
    char partNumber[MAX_VERSION_STRING_LENGTH];         /**< Hardware P/N */
    char hwRevision[MAX_VERSION_STRING_LENGTH];         /**< Hardware Revision */
    char chipRevision[MAX_VERSION_STRING_LENGTH];       /**< SOC Revision */
};


/*SVC_STRUCT*/
/**
 *  @name Internal battery status structure
 */
struct batteryStatus
{
    unsigned long int voltage;  /**< In units of 0.1v */
    unsigned long int status;   /**< @li 1 = OK (above 2.5v) @li 0 = BAD (below 2.5v) */
};


/** Obtain service version
 *
 * @return 
 * Struct version containing version.
 *
 * @note For XML interface, see  @code xml:\ref sysinfo_getVersion @endcode
 */
/*SVC_PROTOTYPE*/ struct version sysinfo_getVersion(void);


/** Obtain memory usage information from Linux OS
 *
 * @note
 *   @li This method uses info from /proc/meminfo that is maintained by the Linux Kernel
 *      regarding memory space, total, used, and free.
 *   @li This method includes cached memory when reporting free (available) memory space.
 *
 *@return 
 * Struct memory_usage
 */
/*SVC_PROTOTYPE*/ struct memoryUsage_s sysinfo_memoryUsage(void);


#ifdef	RAPTOR
/** Obtain memory usage information from Linux OS on VOS2
 *
 * @note
 *   @li This method uses info from /proc/meminfo that is maintained by the Linux Kernel
 *      regarding memory space, total, used, and free.
 *   @li This method includes cached memory when reporting free (available) memory space.
 *
 *@return 
 * Struct memory_usage
 */
/*SVC_PROTOTYPE*/ struct memoryUsage_s sysinfo_memoryUsage_vos2(void);
#endif


/** Obtain platform information
 *
 * @return 
 * Struct platform
 */
/*SVC_PROTOTYPE*/ struct platform_s sysinfo_platform(void);


/** Obtain platform extended information
 *
 * @return
 * @li errno != 0 if error
 * @li struct platformExt_s containing extended platform information
 *
 * <b>Errno values</b>:
 * @li EACCES - Unable to access information
 */
/*SVC_PROTOTYPE*/ struct platformExtended_s sysinfo_platformExt(void);


/** obtain OS software version information
 *
 * @return 
 * Struct softwareVersion_s
 */
/*SVC_PROTOTYPE*/ struct softwareVersion_s sysinfo_software(void);



/** Obtain extended OS software version information
 *
 * sysinfo_softwareExtended is a super set of sysinfo_software but it can grow if needed.
 *
 *@note IMPORTANT! Returned structure pointer to softwareVerionExtended_s must be freed
 * 
 * @return 
 * Struct SoftwareVersionExtended_s
 * 
 *  IMPORTANT!  New modules must be added to the END of the structure! 
 */
/*SVC_PROTOTYPE*/ struct softwareVersionExtended_s *sysinfo_softwareExtended(void);



/** Obtain all extended package information
 *
 * @note CAUTION! please invoke  sysinfo_packagesExtended_release afterwards to release allocated memory.
 *
 * @return 
 * PackageCollectionExtended_s
 */
/*SVC_PROTOTYPE*/ struct packageCollectionExtended_s sysinfo_packagesExtended(char *signer /*NULL*/);

/** Release memory allocated by sysinfo_packagesExtended
 *  
 * @note IMPORTANT! ALWAYS call this function after using the struct returned by any of the sysinfo_***packagesExtended functions
 */
/*SVC_PROTOTYPE*/ void sysinfo_packagesExtended_release(struct packageCollectionExtended_s packages);



/** Obtain base RFS software package extended information
 *
 * @note CAUTION! please invoke  sysinfo_packagesExtended_release afterwards to release allocated memory.
 *
 * @return 
 * PackageCollectionExtended_s
 */
/*SVC_PROTOTYPE*/ struct packageCollectionExtended_s sysinfo_rfsPackagesExtended(void);



/** Obtain system software package extended information
 *
 * @note CAUTION! Please invoke  sysinfo_packagesExtended_release afterwards to release allocated memory.
 * 
 * @return 
 * PackageCollectionExtended_s
 */
/*SVC_PROTOTYPE*/ struct packageCollectionExtended_s sysinfo_sysPackagesExtended(void);



/** Obtain Application software package extended information
 *
 *  @note CAUTION! Please invoke  sysinfo_packagesExtended_release afterwards to release allocated memory.
 *
 *@return 
 * PackageCollectionExtended_s
 */
/*SVC_PROTOTYPE*/ struct packageCollectionExtended_s sysinfo_appPackagesExtended(void);



/** Obtain all package information
 * 
 * @note CAUTION: please invoke  sysinfo_packages_release afterwards to release allocated memory.
 *
 * @return 
 * PackageCollection_s
 */
/*SVC_PROTOTYPE*/ struct packageCollection_s sysinfo_packages(char *signer /*NULL*/);

/** Release memory allocated by sysinfo_packages
 *  
 * @note IMPORTANT! ALWAYS call this function after using the struct returned by any of the sysinfo_***packages functions
 */
/*SVC_PROTOTYPE*/ void sysinfo_packages_release(struct packageCollection_s packages);



/** Obtain base RFS software package information
 *
 * @note CAUTION! Please invoke  sysinfo_packages_release afterwards to release allocated memory
 * 
 *@return 
 * PackageCollection_s
 */
/*SVC_PROTOTYPE*/ struct packageCollection_s sysinfo_rfsPackages(void);



/** Obtain system software package information
 *
 * @note CAUTION! Please invoke  sysinfo_packages_release afterwards to release allocated memory.
 *
 * @return 
 * PackageCollection_s
 */
/*SVC_PROTOTYPE*/ struct packageCollection_s sysinfo_sysPackages(void);



/** Obtain Application software package information
 *
 * @note CAUTION! please invoke  sysinfo_packages_release afterwards to release allocated memory
 *
 * @return
 * PackageCollection_s
 */
/*SVC_PROTOTYPE*/ struct packageCollection_s sysinfo_appPackages(void);



/** Obtain all software bundle information
 *
 * @note CAUTION! ALWAYS invoke sysinfo_bundles_release to release memory allocated by this function 
 *
 * @return 
 * BundleCollection_s
 */
/*SVC_PROTOTYPE*/ struct bundleCollection_s sysinfo_bundles(char *user /*NULL*/);


/** Release all memory allocated by sysinfo_bundles
 *
 * @note IMPORTANT! Make sure to invoke this function after using any of the sysinfo_***bundles functions
 */
/*SVC_PROTOTYPE*/void sysinfo_bundles_release(struct bundleCollection_s bundles);



/** Obtain base RFS software bundle information
 *
 * @note CAUTION! ALWAYS invoke sysinfo_bundles_release to release memory allocated by this function.
 * 
 * @return 
 * BundleCollection_s
 */
/*SVC_PROTOTYPE*/ struct bundleCollection_s sysinfo_rfsBundles(void);



/** Obtain system software bundle information
 *
 * @note CAUTION! ALWAYS invoke sysinfo_bundles_release to release memory allocated by this function. 
 *
 * @return
 * BundleCollection_s
 */
/*SVC_PROTOTYPE*/ struct bundleCollection_s sysinfo_sysBundles(void);



/** Obtain Application software bundle information
 *
 * @note CAUTION! ALWAYS invoke sysinfo_bundles_release to release memory allocated by this function.
 * 
 * @return 
 * BundleCollection_s
 */
/*SVC_PROTOTYPE*/ struct bundleCollection_s sysinfo_appBundles(void);



/** Get a bundle's version for a specified bundle
 *
 * @return
 * char*
 */
/*SVC_PROTOTYPE*/ struct bundleVersionInfo_s sysinfo_getBundleVersion(char *name, char *user);



/** Return struct containing info on firmware modules/drivers
 *
 * @return
 * firmwareCollection_s  containing firmwareInfo_s for each available firmware module/driver
 *
 * <b>Errno values</b>:
 * @li ENOENT - No firmware info available (returned structure also has firmware_count = 0, firmware = NULL)
 * @li ENOMEM - Internal malloc failed
 * @li ECONNREFUSED - not supported on this platform
 *
 * @note
 * @li The returned structure pointed to in struct firmwareCollection_s needs to be free'd when finished using it.
 * @li Can call free() or invoke sysinfo_firmwareInfo_release() to release memory allocated by this function
 */
/*SVC_PROTOTYPE*/ struct firmwareCollection_s sysinfo_getFirmwareInfo(void);


/** Release all memory allocated by sysinfo_getFirmwareInfo()
 *
 * @note 
 * @li Make sure to invoke this function after finished using the firmwareCollection_s
 * @li After this call, collection.firmware is no longer valid, any further reference may result in seg. fault
 * 
 * @param[in] collection as returned by sysinfo_getFirmwareInfo()
 * 
 * @return
 * @li 0 Success
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li ENOENT - If collection.firmware == NULL
 */
/*SVC_PROTOTYPE*/ int sysinfo_firmwareInfo_release(struct firmwareCollection_s collection);


/** Obtain internal battery status information
 *
 * @return 
 * BatteryStatus
 */
/*SVC_PROTOTYPE*/struct batteryStatus sysinfo_batteryStatus(void);



/** Obtain MIB (Manufacture Information Block)
 *
 * @return 
 * MIBInfo_s * 
 */
/*SVC_PROTOTYPE*/struct MIBInfo_s *sysinfo_MIB(void);



 /** Obtain PCI version number
 *
 * @return
 * int - pci version number - Possible values are 3 and 4.
 * 
 * @note Can currently only detect PCI version 4 on Mx9 family devices.  All other devices will have a pci version of 3 returned by this function.
 *
 * This function will not return an error.  errno will always be set to 0 after calling this function.
 *
 */
/*SVC_PROTOTYPE*/int sysinfo_pciVersion(void);

#ifdef __cplusplus
}
#endif
#endif //SVC_SERVICE_SYSINFO_H

/// @} */
/// @} */
