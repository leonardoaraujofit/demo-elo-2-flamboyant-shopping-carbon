/*
 *      Copyright, 2011 VeriFone Inc.
 *      2099 Gateway Place, Suite 600
 *      San Jose, CA.  95110
 *
 *  All Rights Reserved. No part of this software may be reproduced,
 *  transmitted, transcribed, stored in a retrieval system, or
 *  translated into any language or computer language, in any form
 *  or by any means electronic, mechanical, magnetic, optical,
 *  chemical, manual or otherwise, without the prior permission of
 *  VeriFone Inc.
 */

/** \file svc_videoswitch.h
 * \brief System Information service interface methods.
 * \details The System Information (videoswitch) service contains methods
 *  for obtaining system information for such things as: software versions,
 *  hardware features/options, etc.
 *
 */

#ifndef SVC_SERVICE_VIDEOSWITCH_H
#define SVC_SERVICE_VIDEOSWITCH_H

#include "svcmgrSvcDef.h"

#ifdef __cplusplus
extern "C" {
#endif

/*SVC_SERVICE:videoswitch*/

// defines
#define AUTOBKLT_MODE_DISABLE         0
#define AUTOBKLT_MODE_ENABLE          1  

/*SVC_STRUCT*/
/**
 * Returned VideoSwitch status Structure
 */
struct vsw_Ioctl_t2
{
   char buf[10]; /**< Buffer for data passing */
};

/*SVC_STRUCT*/
/**
 * Returned Backlight status Structure
 */

struct backlightRtn
{
   int OnOff; /**< On/Off status of backlight */
   int DutyCycle; /**< DutyCycle of backlight */
};

/*SVC_STRUCT*/
/**
 * Returned Video Mode status Structure
 */

struct videoModeRtn
{
   int Mode; /**< Video mode: 0=splash 3=trident 5=media 9=overlay 2=blank */
   int OverlayType; /**< Overlay type: 0=chromakey, 1=Windowing */
};




/*SVC_STRUCT*/
/**
 * Returned Chroma Key Structure
 */

struct chromaKeyRtn
{
   int RedH; /**< Chroma Key Red upper limit */
   int RedL; /**< Chroma Key Red lower limit */
   int GreenH; /**< Chroma Key Green upper limit */
   int GreenL; /**< Chroma Key Green lower limit */
   int BlueH; /**< Chroma Key Blue upper limit */
   int BlueL; /**< Chroma Key Blue lower limit */
};



/*SVC_STRUCT*/
/**
 * Returned Chroma Key Structure
 */

struct windowCoordRtn
{
   int X1H; /**< High Byte of 16 bit top left x-coordinate of window */
   int X1L; /**< Low Byte of 16 bit top left x-coordinate of window */
   int X2H; /**< High Byte of 16 bit bottom right x-coordinate of window */
   int X2L; /**< Low Byte of 16 bit bottom right x-coordinate of window */
   int Y1H; /**< High Byte of 16 bit top left y-coordinate of window */
   int Y1L; /**< Low Byte of 16 bit top left y-coordinate of window */
   int Y2H; /**< High Byte of 16 bit bottom right y-coordinate of window */
   int Y2L; /**< Low Byte of 16 bit bottom right y-coordinate of window */
};



/*SVC_STRUCT*/
/**
 * Returned FPGA Firmware Version Structure
 */

struct fpgaVersionRtn
{
   char V1; /**< 1st char of FPGA FW Version  */
   char V2; /**< 2nd char of FPGA FW Version  */
   char V3; /**< 3rd char of FPGA FW Version  */
   char V4; /**< 4th char of FPGA FW Version  */
};


/** obtain service version
 *
 * @return struct version containing version.
 *
 *     <center>For XML interface, see xml:\ref videoswitch_getVersion</center>
 */
/*SVC_PROTOTYPE*/ struct version videoswitch_getVersion(void);


/** Initialise videoswitch
 *
 */
/*SVC_PROTOTYPE*/ void videoswitch_init(void);


/** Set videoswitch display mode
 *
 */
/*SVC_PROTOTYPE*/ void videoswitch_setmode(int mode);

/** Set videoswitch chroma key value
 *
 */
/*SVC_PROTOTYPE*/ void videoswitch_setcolorkey(int RedH, int RedL,int GreenH, int GreenL, int BlueH, int BlueL );

/** Set videoswitch window coordinate for window overlay mode
 *  Note that the window should be turned off first before setting, changing. Otherwise the previous windows set will not be cleared.
 *  WindowNo valid from 0 - 63
 */
/*SVC_PROTOTYPE*/ void videoswitch_setwindowcoord(int WindowNo, int X1H, int X1L, int X2H, int X2L,int Y1H, int Y1L,int Y2H, int Y2L);

/** Set videoswitch window on/off
 *
 */
/*SVC_PROTOTYPE*/ void videoswitch_setwindowonoff(int WindowNo, int OnOff);

/** Set videoswitch backlight on/off
 *
 */
/*SVC_PROTOTYPE*/ void videoswitch_setbacklightonoff(int OnOff);

/** Set videoswitch dutycycle in percent with minimum of 3% and maximum of 100%
 *
 */
/*SVC_PROTOTYPE*/ void videoswitch_setbacklightduty(int dutyPercent);

/** Autotest videoswitch set functions
 *
 */
/*SVC_PROTOTYPE*/ void videoswitch_autotest(void);

/** Autotest videoswitch reset functions
 *
 */
/*SVC_PROTOTYPE*/ void videoswitch_reset(void);

/** Get videoswitch display mode
 *
 * @return struct vsw_Ioctl_t containing an array of size 10 char as general buffer.
 *
 */
/*SVC_PROTOTYPE*/ struct videoModeRtn videoswitch_getmode(void);

/** Get videoswitch chroma key
 *
 * @return struct vsw_Ioctl_t containing an array of size 10 char as general buffer.
 *
 */
/*SVC_PROTOTYPE*/ struct chromaKeyRtn videoswitch_getchromakey(void);

/** Get videoswitch coordinates of windows set
 *
 * @return struct vsw_Ioctl_t containing an array of size 10 char as general buffer.
 *
 */
/*SVC_PROTOTYPE*/ struct windowCoordRtn videoswitch_getwindowcoord(int WindowNo);

/** Get videoswitch backlight on/off, dutycycle
 *
 * @return struct vsw_Ioctl_t containing an array of size 10 char as general buffer.
 *
 */
/*SVC_PROTOTYPE*/ struct backlightRtn videoswitch_getbacklight(void);

/** Get videoswitch firmware version
 *
 * @return struct vsw_Ioctl_t containing an array of size 10 char as general buffer.
 *
 */
/*SVC_PROTOTYPE*/ struct fpgaVersionRtn videoswitch_getfpgafwversion(void);

/** The setMode API would enable and disable a service thread that would monitor the ambient light level and set the backlight accordingly.
 * Values for state
 * 0 = Disable monitor thread
 * 1 = Enable monitor thread, send an event every interval.   The interval is defined in milliseconds
 * 
 * The following parameters can be set via putenvfile if the behaviour of the automatic backlight brightness behaviour needs to be changed.
 * ablonoff (1 - on 0 - off)
 * ablminlux (min 0 max 1024)  corresponds to via returned by ambient sensor to match min backlight
 * ablmaxlux (min 0 max 1024)  corresponds to via returned by ambient sensor to match max backlight
 * ablminbklt (min 20 max 100) minimum brightness of backlight
 * ablmaxbklt (min 20 max 100) miximum brightness of backlight
 * ablpollinterval   interval in miliseconds to poll the ambient sensor and adjust the backlight accordingly
 * ablmovingavgsamples  samples to average for service to take as ambient sensor value (larger number of samples means that change is less sensitive to transient changes)
 * 
 * e.g. To turn on:  putenvfile *ablonoff 1
 *
 * @param[in] state AUTOBKLT_MODE_DISABLE or AUTOBKLT_MODE_ENABLE
 * @param[in] tolerance amount of difference in sensor reading before a change is reflected (0 < values < 4294967295)
 * @param[in] interval time in msec where videoswitch server thread will read ambient light reading from sensor. (0 < values < 4294967295)
 * @return 0 on success, -1 on error
 * 
 * Notes:
 * On error return, errno:
 * @li EINVAL if the noted above combinations are not met.
 * @li ECHILD if unable to create server thread.
 * @li EACCES unable to access driver interface.
 *
 */
/*SVC_PROTOTYPE*/ int videoswitch_setablmode(int state, unsigned int tolerance, unsigned int interval);


/** Return the current RAW ambient light values
 *
 * @return int values < 0 = Error
 *
 */
/*SVC_PROTOTYPE*/ int videoswitch_getablstatus(void);

/** Read event from ambient light sensor
 *
 * @param[in] flags 1 = Non-block, 2 = Last, 4 = Cancel  (options can be ored) 
 * @return int values < 0 = Error
 *
 */
/*SVC_PROTOTYPE*/ int videoswitch_getablevent(int flags);



#ifdef __cplusplus
}
#endif

#endif //SVC_SERVICE_VIDEOSWITCH_H
