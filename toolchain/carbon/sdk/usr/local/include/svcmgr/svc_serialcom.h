
/**
 * @file svc_serialcom.h
 *
 * @brief Serial Port service
 *
 * This header file contains information about Serial Port service.
 */


#ifndef SVC_SERIALCOM_H
#define SVC_SERIALCOM_H

#ifdef __cplusplus
extern "C" {
#endif

#include "svcmgrSvcDef.h"

/*SVC_SERVICE:serialcom*/

/**
 * @name Additional StatusReturn state codes from serialcom_getEvent
*/
/** \{ */
#define SERIALCOM_NO_STATUS		-2020	        /**< No status message available */
#define SERIALCOM_NO_STATUS_NOT_ACTIVE	-2040	/**< No status message and port not active */
#define SERIALCOM_MSG_AVAILABLE		 9999	    /**< Status message available - Check msg pointer */
#define SERIALCOM_DONE			10000	        /**< No more messages - Event handler closed */
/** \} */


/**
 * Communication port configuration structure
 */
/*SVC_STRUCT*/
struct serialparm {
    int handle;				/**< Handle if port is already open otherwise set to -1 */
    char port[64];			/**< Device name - Examples: @li /dev/ttyAMA0 @li/dev/ttyAMA1 @li /dev/ttyAMA2 @li /dev/ttyGS0 */
    int rate;    			/**< Baud rate: @li -1 = Auto @li 0 = 300 @li 1 = 600 @li 2 = 1200 @li 3 = 2400 @li 4 = 4800 @li 5 = 9600 @li 6 = 19200 @li 7 = 38400 @li 8 = 115200 */
    char mode;				/**< Download type (full or partial): 'F', 'P' or 'p' */
    int options;			/**< For Diagnostics: @li 0 = Simple Result @li 1=Verbose Results */
};


/**
 * Returned Status Structure
 */
/*SVC_STRUCT*/
struct serialcomStatusReturn {
    int status;				/**< Return state value */
    char *msg;				/**< Pointer to return message */
};


/** Obtain the version of the MSR service
 *
 * @return
 * Version struct defined in svcmgrSvcDef.h
 */
/*SVC_PROTOTYPE*/ struct version serialcom_getVersion(void);


/** Start a serial communication download
 *
 * @param[in] sparm  Two options: First, set handle = 0 and specify port, rate and mode.  Second option is to pass the handle of a currently opened and configured port.
 *
 * @return
 * @li If >= 0 Success, returned value is communication port handle
 * @li If = -1 Error
 *
 * <b>Errno Values:</b>
 * @li ENODEV - When the device is not valid.
 */
/*SVC_PROTOTYPE*/ int serialcom_download(struct serialparm sparm /*REQ*/);



/** Cancel serial download abort
 *
 * @param[in] handle   Handle to serial port performing serial download
 *
 * @return
 * @li If >= 0 Success, returned value is communication port handle
 * @li If = -1 Error
 *
 * <b>Errno Values:</b>
 * @li EBUSY - When device is busy - Cancel not possible at this time, try again.
 * @li ESRCH - When there is no download process to cancel.
 */
/*SVC_PROTOTYPE*/ int serialcom_downloadAbort(int handle /*REQ*/);


/** Read event from a Serial Port operation - Either download or diagnostic
 *
 * @param[in] handle Handle to serial port performing serial download
 * @param[in] flags  Get Event Flags:
 *                      @li -1 = Non-block
 *                      @li 2 = Last
 *                      @li 4 = Cancel (options can be or'd)
 * @return
 * Struct statusReturn - state will reflect current download state, Depending on state, msg may have an ASCII status string send from the download server.
 * this state may have funny values. \n
 *
 * @note IMPORTANT!
 * Application must free serialStatusReturn msg.
 */
/*SVC_PROTOTYPE*/ struct serialcomStatusReturn serialcom_getEvent(int flags, int handle /*REQ*/);


 /** Perform a serial port diagnostic
 *
 * @param[in] sparm    Two options: @li First, set handle = 0 and specify port, rate and mode @li Second option is to pass the handle of a currently opened and configured port.
 *
 * @return
 * <I>For options=0:</I>
 * @li If == 0 Test Failed
 * @li If == 1 Test Passed
 * @li If < 0 Error
 *
 * @return
 * <I>For options=1:</I>
 * @li > 0 Test passed and value is handle needed for getting events
 * @li < 0 Test failed and abs(return) is the handle needed for getting events
 *
 * <b>Errno Values:</b>
 * @li ENODEV - When the device is not valid.
 */
/*SVC_PROTOTYPE*/ int serialcom_diag(struct serialparm sparm /*REQ*/);


/** Set or cancel custom baud rate
 *
 * @param[in] handle  Handle to serial port whose custom baud we want to set
 * @param[in] baud  Baud rate we want to set
 *
 * @return
 * @li if >= 0 Success; returned value is baud
 * @li If = -1 Error
 *
 * <b>Errno Values:</b>
 * @li E..... -- errors are determined by Linux serial port calls
 */
/*SVC_PROTOTYPE*/ int serialcom_setCustomBaud(int baud, int handle /*REQ*/);


/** set the serial/usb selector switch state
 *
 * @param[in] state of selector (0 - uart enabled, 1 - usb enable)
 *
 * @return
 * @li = 0 Success
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li EINVAL - Invalid param
 * @li EACCES - Unable to access interface
 * @li ECONNREFUSED - not supported on this platform
 *
 */
/*SVC_PROTOTYPE*/ int serialcom_setSwitchUSB(int state /*0*/);


/** get the serial/usb selector switch state
 *
 * @return
 * @li >= 0, state of selector after call on Success (0 - uart, 1 = usb)
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li EINVAL - Invalid param
 * @li EACCES - Unable to access interface
 * @li ECONNREFUSED - not supported on this platform
 *
 */
/*SVC_PROTOTYPE*/ int serialcom_getSwitchUSB(void);



#ifdef __cplusplus
}
#endif
#endif //SVC_SERIALCOM_H



