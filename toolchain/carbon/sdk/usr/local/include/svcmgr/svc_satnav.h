#ifndef SVC_SATNAV_H
#define SVC_SATNAV_H

#include "svcmgrSvcDef.h"

#ifdef __cplusplus
extern "C" {
#endif

/*SVC_SERVICE:satnav*/

#define SATNAV_MODE_NOT_SEEN	0	/* The GNSS receiver doesn't see any satellites. */
#define SATNAV_MODE_NO_FIX		1	/* The GNSS receiver does see satellites but cannot yet get a fixed location */
#define SATNAV_MODE_2D  		2	/* Good for latitude/longitude */
#define SATNAV_MODE_3D  		3	/* Good for altitude/climb too */

/* Status defines*/
#define SVCSATNAV_STATUS_IDLE				0b00000001	/* For internal use */
#define SVCSATNAV_STATUS_RADIO_HW_ACTIVE	0b00000010	/* Radio (3G module) is active */
#define SVCSATNAV_STATUS_GNSS_CONNECTION_EN	0b00000100	/* Connection to GNSS engine is enabled */
#define SVCSATNAV_STATUS_2D_FIX_AVAILABLE	0b00001000	/* 2 dimensional fix is available (no altitude) */
#define SVCSATNAV_STATUS_3D_FIX_AVAILABLE	0b00010000	/* 3 dimensional fix is available (with altitude) */

/* Parameters ID*/
#define SATNAV_ENGINE			1
#define SATNAV_FREQUENCY		2
#define SATNAV_GLONASS			3
#define SATNAV_OUTPUT			4
#define SATNAV_ANTENNA			5
#define SATNAV_ODM				6
#define SATNAV_PSM				7
#define SATNAV_START			8
#define SATNAV_GNSS_XTRA		9
#define SATNAV_UTC				10
#define SATNAV_DEF_PARAMS		12
/* Geolocation Parameters ID */
#define SATNAV_GEOLOC           50
#define SATNAV_GEOLOC_SERVER    51

/* Parameters values */
#define SATNAV_ENGINE_OFF				"0"
#define SATNAV_ENGINE_ON				"1"
#define SATNAV_ENGINE_XTRA				"2"
#define SATNAV_GLONASS_OFF_STRING		"3"
#define SATNAV_GLONASS_ON_STRING		"4"
#define SATNAV_OUTPUT_OFF_STRING		"3"
#define SATNAV_OUTPUT_ON_STRING			"4"
#define SATNAV_OUTPUT_BUFF_STRING		"5"
#define SATNAV_ANT_OFF_STRING			"3"
#define SATNAV_ANT_ON_STRING			"4"
#define SATNAV_ANT_AUTO_STRING			"6"
#define SATNAV_ODM_OFF					"0"
#define SATNAV_ODM_LOW_POWER			"1"
#define SATNAV_ODM_READY				"2"
#define SATNAV_PSM_OFF					"0"
#define SATNAV_PSM_ON					"1"
#define SATNAV_START_W_AGPS				"0"
#define SATNAV_START_W_LAST_LOCATION	"1"
/* Geolocation parameters values */
#define SATNAV_GEOLOC_OFF               "0"
#define SATNAV_GEOLOC_ON                "1"
#define SATNAV_GEOLOC_SERVER_UNKNOWN    "0"
#define SATNAV_GEOLOC_SERVER_GOOGLE     "1"
#define SATNAV_GEOLOC_SERVER_MOZILLA    "2"
#define SATNAV_GEOLOC_SERVER_OPENCELLID "3"


/* Geofencing parameters ID */
#define SATNAV_GEOFENCE_CREATE_ID          1
#define SATNAV_GEOFENCE_DESTROY_ID         2
#define SATNAV_GEOFENCE_DELETE_ID          3
#define SATNAV_GEOFENCE_DELETE_ALL_ID      4
#define SATNAV_GEOFENCE_SET_ID             5
#define SATNAV_GEOFENCE_SET_FROM_FILE_ID   6
#define SATNAV_GEOFENCE_GET_COUNT_ID       7
#define SATNAV_GEOFENCE_GET_INDEX_ID       8
#define SATNAV_GEOFENCE_GET_NAME_ID        9
#define SATNAV_GEOFENCE_SAVE_TO_FILE_ID    10

#define SATNAV_MAX_PARAM_LEN	18
#define SATNAV_MAX_SAT_NUM		16	/* The maximum number of satellites the NMEA protocol can report of */
#define SATNAV_SIZE_UTC			25

struct S_AGPS_DATA
{
	char Xtra[24];
	int isEnbale;	// 1-Enable 0-Disable
};

/*SVC_STRUCT*/
/**********************************
 * Shared Data Structure with user
 **********************************/
struct S_SHARE_DATA
{
    void   *data;              /* Returned data.
    							  Must be freed after using satnav_GetParam() or satnav_SetParam
    							  It contains a NULL terminating string */
    int 	data_count;        /* Returned data length */
};

/*SVC_STRUCT*/
/**************************************************
 * Position Data
 * Make sure to keep this structure size = 64 Bytes
 **************************************************/
struct S_POSITION
{
	double 	latitude;				/* Latitude in degrees (valid if mode >= 2) 8 Bytes*/
	double 	longitude;				/* Longitude in degrees (valid if mode >= 2) 8 Bytes*/
	double 	altitude;				/* Altitude in meters (valid if mode == 3) 8 Bytes*/
	char   	mode;					/* Mode of fix 1 Byte*/
	char	utc[SATNAV_SIZE_UTC];	/* String of UTC represented in ISO 8601 format 25 Bytes*/
    float   accuracy;               /* Accuracy in meters of the position returned. Not available if equal to 0 */
	char 	reserved[10];
};

/*SVC_STRUCT*/
/**************************************************
 * Status Data (run-time)
 * Make sure to keep this structure size = 64 Bytes
 **************************************************/
struct S_STATUS
{
	char status;					/* Mask of the Status Defines */
	char satFixStatus;				/* What is the fix status: 0,1-No, 2-Yes 2D, 3-Yes 3D (uses the same defines as the position mode) */
	char satNumUsed;				/* Number of satellites used in getting position*/
	char satNumVis;					/* Number of visible satellites (Cannot be lower than 0 or larger than SATNAV_MAX_SAT_NUM) */
	char satPrn[SATNAV_MAX_SAT_NUM];/* PRNs of visible satellites */
	char satSnr[SATNAV_MAX_SAT_NUM];/* SNR[dB/Hz] of visible satellites */
	char reserved[28];
};


/*SVC_STRUCT*/
/****************************
 * Information of static Data
 ****************************/
struct S_Info
{
	char daemonVer[8];				/* daemon version that is used	*/
	char manufacturerId[16];		/* GNSS receiver manufacturer */
	char modelId[16];				/* GNSS receiver model */
	char revisionId[16];			/* GNSS receiver revision (SW) id */
	char imei[16];					/* GNSS receiver IMEI number */
};



//==============================================================================
// GEOFENCING STRUCTURES
//==============================================================================
#define S_GEOF_POLYPT_MAX                12
#define S_GEOF_NAME_MAXSZ                12

/*SVC_STRUCT*/
/****************************
 * Circular geo-fence information
 ****************************/
struct S_GEOF_CIRCLE
{
	double radius;   /* Radius */
	double lat;      /* Latitude */
	double lon;      /* Longitude */
};

/*SVC_STRUCT*/
/****************************
 * Polygon geo-fence information
 ****************************/
struct S_GEOF_POLY 
{
	int num_points;                  /* Number of points/vertices. Range is 3-12 */
	double lat[S_GEOF_POLYPT_MAX];   /* Latitude points */
	double lon[S_GEOF_POLYPT_MAX];   /* Longitude points */
};

/*SVC_STRUCT*/
/****************************
 * Geo-fence definition
 ****************************/
struct S_GEOF_CONF
{
	char name[S_GEOF_NAME_MAXSZ]; /* Geo-fence name */
	int  method;   /* Geo-fence metho. Valid values:
	                  1: circular geo-fence 
	                  2: polygon geo-fence  */

		struct S_GEOF_CIRCLE circle; /* Radius and center points for a circular geo-fence */
		struct S_GEOF_POLY poly;     /* Polygon points for a polygon geo-fence */

};


/********************************************************************
 * Name:			satnav_getVersion()
 * Description: 	Obtain the version of the SATNAV service
 * Input:			N/A
 * Output:			N/A
 * return:		 	version struct defined in svcmgrSvcDef.h
 ********************************************************************/
/*SVC_PROTOTYPE*/ struct version satnav_getVersion(void);

/********************************************************************
 * Name:			satnav_Open()
 * Description: 	Initializes the SATNAV engine and daemon params.
 * Input:			N/A
 * Output:			N/A
 * return:		 	0 - Success
 * 					-1 - Error, check errno:
 * 						ENODEV - SATNAV receiver is not connected
 * 						ECOMM  - Cannot send AT CMD
 * 						EACCES - Permission denied
 * 						ECHILD - Daemonize failure
 ********************************************************************/
/*SVC_PROTOTYPE*/ int satnav_Open(void);

/********************************************************************
 * Name:			satnav_Close()
 * Description: 	Closes the SATNAV engine and the session to the daemon.
 * Input:			N/A
 * Output:			N/A
 * return:		 	0 - Success
 * 					ENODEV - Device not available
 ********************************************************************/
/*SVC_PROTOTYPE*/ int satnav_Close(void);

/********************************************************************
 * Name:			satnav_SetParam()
 * Description: 	Sets SATNAV engine's HW settings.
 * 					If satnav_Open() was not used, radio module will turn ON and set the parameters (GNSS engine will stay OFF).
 * Input:			ParamId: Parameter ID
 * 					ShareData: Structure composed of length and data string that hold the new parameter value
 * Output:			N/A
 * return:		 	0 - Success
 * 					-1 - Error, check errno:
 * 						EINVAL - One of the Input parameters is not valid
 ********************************************************************/
/*SVC_PROTOTYPE*/ int satnav_SetParam(int ParamId, struct S_SHARE_DATA ShareData);

/********************************************************************
 * Name:			satnav_GetParam()
 * Description: 	Gets the default status information corresponding to the ParamId, from the GNSS engine HW.
 * 					If satnav_Open() was not used, radio module will turn ON and retrieve the parameters (GNSS engine will stay OFF).
 * Input:			ParamId: Parameter ID
 * Output:			N/A
 * return:		 	S_SHARE_DATA  - a structure of binary data containing the requested parameter value
 * 					EINVAL - ParamId value is not supported
 * 					ENOMEM - Memory allocation problem
  ********************************************************************/
/*SVC_PROTOTYPE*/ struct S_SHARE_DATA satnav_GetParam(int ParamId);

/********************************************************************
 * Name:			satnav_GetInfo()
 * Description: 	Obtains system information
 * 					If satnav_Open() was not used, radio module will turn ON and retrieve the information (GNSS engine will stay OFF).
 * Input:			N/A
 * Output:			N/A
 * return:		 	S_Info - a structure containing the information strings. If struct is all '0' then a fix has not been established yet.
 * 					Check errno for errors:
 * 					0 - Success
 * 					ENOMEM  - Memory allocation problem
 * 					EINVAL  - Bad input parameters
 * 					ENOEXEC - Command execution error
 ********************************************************************/
/*SVC_PROTOTYPE*/ struct S_Info satnav_GetInfo(void);

/********************************************************************
 * Name:			satnav_GetStatus()
 * Description: 	Obtains run-time information starting from the initialization state to satellite status.
 * Input:			N/A
 * Output:			N/A
 * return:		 	Status structure of run-time information. If struct is all '0' then a fix has not been established yet.
 * 					Check errno for errors:
 * 					0 - Success
 * 					ENOMEM  - Memory allocation problem
 * 					ENODEV 	- SATNAV receiver is not connected
 ********************************************************************/
/*SVC_PROTOTYPE*/ struct S_STATUS satnav_GetStatus (void);

/********************************************************************
 * Name:			satnav_GetPosition()
 * Description: 	Gets the fixed position data as retrieved and parsed by the daemon
 * 					Should be used after satnav_Open().
 * Input:			N/A
 * Output:			N/A
 * return:		 	Position struct defined in svc_satnav.h. If struct is all '0' then a fix has not been established yet.
 * 					Check errno for:
 * 					0 - Success
 * 					ENOMEM - Memory allocation problem
 * 					ENODEV - SATNAV receiver is not connected
 ********************************************************************/
/*SVC_PROTOTYPE*/ struct S_POSITION satnav_GetPosition(void);

/********************************************************************
 * Name:			satnav_CreateGeofence()
 * Description: 	Create a geo-fencing object
 * Input:			N/A
 * Output:			N/A
 * return:		 	Geo-fence object handle
 * 					0  - Success
 * 					-1 - Error, check errno:
 *	 					ENOMEM - Memory allocation problem
 ********************************************************************/
/*SVC_PROTOTYPE*/ int satnav_CreateGeofence( );

/********************************************************************
 * Name:			satnav_DestroyGeofence()
 * Description: 	Delete Geo-fencing object
 * Input:			Geo-fencing object handle
 * Output:			N/A
 * return:		 	0  - Success
 * 					-1 - Error, check errno:
 * 						EINVAL - One of the Input parameters is not valid
 ********************************************************************/
/*SVC_PROTOTYPE*/ int satnav_DestroyGeofence( int hdl );

/********************************************************************
 * Name:			satnav_DeleteGeofence()
 * Description: 	Delete a Geo-fence from object
 * Input:			hdl: Geo-fencing object handle
 *                  name: Name that indicates the geo-fence to delete
 *
 * Output:			N/A
 * return:		 	0  - Success
 * 					-1 - Error, check errno:
 * 						EINVAL - One of the Input parameters is not valid
 * 						EFAULT - Geo-fence to delete has not been found
 ********************************************************************/
/*SVC_PROTOTYPE*/ int satnav_DeleteGeofence( int hdl, const char *name );

/********************************************************************
 * Name:			satnav_SetGeofence()
 * Description: 	Adds or edits a geo-fence
 * Input:			hdl: Geo-fencing object handle
 *                  cfg: Geo-fence to add
 * Output:			N/A
 * return:		 	0  - Success
 * 					-1 - Error, check errno:
 * 						EINVAL - One of the Input parameters is not valid
 * 						E2BIG - Maximum number of points reached
 ********************************************************************/
/*SVC_PROTOTYPE*/ int satnav_SetGeofence( int hdl, struct S_GEOF_CONF cfg );

/********************************************************************
 * Name:			satnav_GetGeofenceCount()
 * Description: 	Returns the number of geo-fences referenced by object
 * Input:			hdl: Geo-fencing object handle
 * Output:			N/A
 * return:		 	>=0 - Number of geo-fences
 * 					-1  - Error, check errno:
 * 						EINVAL - One of the Input parameters is not valid
 ********************************************************************/
/*SVC_PROTOTYPE*/ int satnav_GetGeofenceCount( int hdl );

/********************************************************************
 * Name:			satnav_GetGeofenceByName()
 * Description: 	Retrieves a geo-fence based on its name as specified by parameter name
 * Input:			hdl: Geo-fencing object handle
 *                  name: Name that indicates the geo-fence to retrieve
 * Output:			Geo-fence found
 * return:		 	S_GEOF_CONF structure
 * 					Check errno for errors:
 *						0 - Success
 * 						EINVAL - One of the Input parameters is not valid
 * 						EFAULT - Geo-fence name not found
 ********************************************************************/
/*SVC_PROTOTYPE*/ struct S_GEOF_CONF satnav_GetGeofenceByName( int hdl, const char *name );

/********************************************************************
 * Name:			satnav_GetGeofenceByIndex()
 * Description: 	Retrieves a geo-fence based on its list index as specified by parameter index. Application can use this with geof_get_count() to do a traverse of all geo-fences
 * Input:			hdl: Geo-fencing object handle
 *                  index: Index that indicates the geo-fence to retrieve
 * Output:			Geo-fence found
 * return:		 	S_GEOF_CONF structure
 * 					Check errno for errors: 
 *						0 - Success
 * 						EINVAL - One of the Input parameters is not valid
 * 						EFAULT - Geo-fence name not found 
 ********************************************************************/
/*SVC_PROTOTYPE*/ struct S_GEOF_CONF satnav_GetGeofenceByIndex( int hdl, int index );

/********************************************************************
 * Name:			satnav_CheckGeofence()
 * Description: 	Checks latitude and longitude point if inside or outside of any geo-fence
 * Input:			hdl: Geo-fencing object handle
 *                  lat, long: Coordinates to check
 * Output:			N/A
 * return:		 	!NULL - Success: Point to the name of the geo-fence that contains the point
 * 					NULL  - Error, check errno:
 * 						EINVAL - One of the Input parameters is not valid
 * 						EFAULT - Point is not contained inside geo-fences passed in parameter
 ********************************************************************/
/*SVC_PROTOTYPE*/ char *satnav_CheckGeofence( int hdl, double lat, double lon );


#ifdef __cplusplus
}
#endif

#endif //SVC_SATNAV_H
