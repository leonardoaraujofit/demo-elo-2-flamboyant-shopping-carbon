/** @addtogroup cardReader CardReader */
/** @{ */
/** @addtogroup cardreader_msr CardReader - MSR: Services */
/** @{ */
/** 
 *  @file svc_msr.h 
 *
 *  @brief  Magnetic Stripe Reader service
 *
 *	This header file contains information about the Magnetic Stripe Reader service.  
 *
 */



#ifndef SVC_MSR_H
#define SVC_MSR_H

#ifdef __cplusplus
extern "C" {
#endif

#include "svcmgrSvcDef.h"

#ifndef MAX_KEY_LEN
#define MAX_KEY_LEN         (99) /**< Max length for event key names */
#endif

/*SVC_SERVICE:msr*/

// These bit flags are specific to the Ux300 (which has some special needs due to being a hybrid dip reader).
// Use these with msr_setHybridMode() [see below]
#define SVCMSR_IGNORE_EVENTS		0x01	// Ignore noise on fast removal
#define SVCMSR_WAIT4SWITCH			0x02	// Disable read on insert, wait for smartcard switch before reading.

/*SVC_STRUCT*/
/**
 * Single track MSR data structure
 */
struct msrReadTrack {
  int status; 		/**< Track status: 
			    - 0=Valid Data,
			    - 1=No Data, 
			    - 2=Missing Start Sentinel, 
			    - 3=Missing End Sentinel, 
			    - 4=BCC error, 
			    - 5=Parity error, 
			    - 6=Special error */
  int count;  		/**< Count of data bytes */
  char data[150];	/**< Track data buffer */
};

/*SVC_STRUCT*/
/**
 * Single track MSR data structure
 */
struct msrReadTrackSecure {
  int status;       /**< Track status: 
                - 0=Valid Data,
                - 1=No Data, 
                - 2=Missing Start Sentinel, 
                - 3=Missing End Sentinel, 
                - 4=BCC error, 
                - 5=Parity error, 
                - 6=Special error */
  int count;        /**< Count of data bytes */
};

/*SVC_STRUCT*/
/**
 * Structure to store 3 MSR data tracks
 */
struct msrData {
  struct msrReadTrack msrReadData[3];	/**< Store 3 tracks of data */
};

/*SVC_STRUCT*/
/**
 * Structure returned when event fires
 */
struct msrEventStatus {
  int  returnCode;  /**< @li >= 0 = number of bytes read @li < 0 = Error */
  struct msrReadTrackSecure msrReadResult[3];   /**< Structure containing status and count for each of the 3 tracks */
};



/*SVC_STRUCT*/
/**
 * MSR PAN data structure
 */
struct msrPanData {
  int status;    /**< PAN status: 0=Valid Data, 1=No Data */
  int count;  	 /**< Count of data bytes */
  char data[20]; /**< PAN data buffer */
};


/** Obtain the version of the MSR service 
 *
 * @return 
 * Version struct defined in svcmgrSvcDef.h
 */
/*SVC_PROTOTYPE*/ struct version msr_getVersion(void);


/** Open the MSR service
 * 
 * @param[in] flags  File access: @li O_RDONLY @li O_RDWR @li O_NONBLOCK
 *  
 * @note
 *  	- O_RDONLY = 0
 *  	- O_RDWR   = 1
 *  	- O_NONBLOCK = 16384
 *
 *      flags are OR'd together
 *
 * @return 
 * @li If >= 0 then Success
 * @li If = -1 then error, check errno:
 * 
 * <b>Errno values</b>:
 * @li ENOMEM - when unable to malloc space MSR control variables.
 * @li EBUSY - when the MSR device is busy.
 * @li EBADF - when the file descriptor is bad.
 * 
 * @note IMPORTANT!
 * @li Use O_NONBLOCK for polling and omit O_NONBLOCK for events
 */
/*SVC_PROTOTYPE*/ int msr_open(int flags);


/** Open the MSR service 2
 *
 * @param[in] readwrite  Non-zero enable RDWR, otherwise RDONLY
 * @param[in] nonblock   Non-zero enable NONBLOCK, otherwise blocking
 *
 * @return 
 * @li If >= 0 then Success
 * @li If = -1 then error, check errno:
 *
 * <b>Errno values</b>:
 * @li ENOMEM - when unable to malloc space MSR control variables.
 * @li EBUSY - when the MSR device is busy.
 * @li EBADF - when the file descriptor is bad.
 *
 *  @note IMPORTANT!
 * @li Use nonblock != 0 for polling and nonblock == 0 for events
 */
/*SVC_PROTOTYPE*/ int msr_open2(int readwrite, int nonblock);


/** Read from the MSR service
 * 
 * @return
 * @li If Non-NULL then a pointer to msrData structure
 * @li If NULL then check errno:
 *
 * <b>Errno values</b>:
 * @li ENOMEM - when unable to malloc space for the MSR data
 * @li EACCES - when called from nonsecure source
 * @li EBUSY - when the MSR device is busy.
 * @li EBADF - when the file descriptor is bad or msr not open (see msr_open())
 * @li ENOMSG - when no MSR data available
 * 
 * @note IMPORTANT
 * @li Application must free msrData structure!
 * @li The returned data will include start and stop sentinel but will not include the LRC character.
 * @li This call is restricted by /etc/config/svcmgr/access.xml.
 * @li /home/sys2/msr_read - Executable that can be called from PHP via exec() command to return an array containing 3 tracks of MSR data
 */
/*SVC_PROTOTYPE*/ struct msrData *msr_read();


/** Obtain the direction of the msr decode/swipt
 *
 * @return 
 * Direction: @li 0 (unknown), @li 1 (insert/down) or @li  (remove/up)
 *
 * On error, return -1, check errno:
 * 
 * <b>Errno values</b>:
 * @li EBADF when the file descriptor is bad or msr not open (see msr_open())
 *
 * @note The return of 0 (unknown) occurs when a swipe has occurred,
 *       but no discernible data was decoded for any of the tracks,
 *       consequently, direction could not be decoded.
 */
/*SVC_PROTOTYPE*/ int msr_getDecodeDirection(void);


/** Read the PAN from the MSR service
 *
 * @return 
 * msrPanData structure, on error check errno:
 *
 * <b>Errno values</b>:
 * @li EBUSY - when the MSR device is busy.
 * @li EACCES - when called from nonsecure source
 * @li EBADF  - when the file descriptor is bad or msr not open (see msr_open())
 * @li ENOMSG - when unable to obtain PAN (msr open, but no data (did you swipe card first?))
 * @li ENOENT - card data doesn't contain a PAN (track format invalid for track 1 and 2)
 */
/*SVC_PROTOTYPE*/ struct msrPanData msr_getPan(void);


/** Read the PAN from the MSR service in the clear
 *
 * @return 
 * msrPanData structure, on error check errno:
 * 
 * <b>Errno values</b>:
 * @li EBUSY - when the MSR device is busy.
 * @li EBADF - when the file descriptor is bad or msr not open (see msr_open())
 * @li ENOMSG - when unable to obtain PAN (msr open, but no PAN (did you swipe card first?))
 * @li ENOENT - card data doesn't contain a PAN (track format invalid for track 1 and 2)
 *
 * @note IMPORTANT!
 * @li This call is restricted by /etc/config/svcmgr/access.xml.
 */
struct msrPanData msr_getClearPan(void);


/** Control Drivers License Decode
 * 
 * @param[in] mode @li 0 = Disable decode @li 1 = Enable decode
 *
 * @return 
 * @li If >= 0 then Success @li  If = -1 then error, check errno:
 * 
 * <b>Errno values</b>:
 * @li EINVAL illegal mode value passed.
 */
/*SVC_PROTOTYPE*/ int msr_driversLicenseDecode(int mode);


/** Enable/Disable VCL Encryption
 * 
 * @param[in] mode @li 0 = Disable VCL Encryption @li 1 = Enable VCL Encryption
 *
 * @return
 * @li If >= 0 then Success 
 * @li If = -1 then error, check errno:
 * 
 * <b>Errno values</b>:
 * @li EINVAL illegal mode value passed.
 */
/*SVC_PROTOTYPE*/ int msr_VCLControl(int mode);


/** Read event from a MSR Swipe
* 
 * @param[in] flags  Get Event Flags @li  1 = non-block, @li 2 = Last , @li 4 = cancel (options can be or'd)
 * 
 * @return
 * Struct msrEventStatus - Will include return from libvfimsr msrRead call along with Track status and lengh and an SDR reference if applicable\n
 * @li A call to msr_read is required to get the actual data
 */
/*SVC_PROTOTYPE*/ struct msrEventStatus msr_getEvent(int flags);



/** Close the MSR service
 * 
 * @return
 * @li If 0 then Success
 * @li If -1 then Errorr
 */
/*SVC_PROTOTYPE*/ int msr_close(void);

/** Return the process ID for the MSR service
 *
 * @return Process ID
 *
 */
/*SVC_PROTOTYPE*/ int msr_getPID(void);



/*SVC_STRUCT*/
/**  Returned event registration Structure for msr switch events
 */
struct msrSwitchEvent {
	int status; 	/**< Used to indicate status of request */
	char eventkey[MAX_KEY_LEN+1];	    /**<  Event key used by various methods in this service */
};
 


/** Get value for the msr switch state - non-blocking
 *
 * @return 
 * @li 1 Present - msr card is present in slot
 * @li 0 not - msr card not in slot
 * @li -1 Error - Unable to complete request
 *
 * On error return, errno:\n
 * <b>Errno values</b>:
 * @li EBADF - Failure when requesting msr switch status (not supported?)
 */
/*SVC_PROTOTYPE*/ int msr_switch_getState(void);


/** used to request event access to the msr switch mcast event channel.
 * 
 * Returns 
 *  eventkey - the assigned event key used by msr_switch_getEvent()
 *  status - indicates if request was successful or not (see errno)
 * @li status >= 0 then Success
 * @li status = -1 then error, check errno:
 * 
 * <b>Errno Values:</b>
 * @li ENOSPC - Unable to register new event to channel (full)
 */
/*SVC_PROTOTYPE*/ struct msrSwitchEvent msr_switch_eventRequest(void);


/** Removes an event from the mcast event channel that was obtained using msr_switch_eventRequest()
 * 
 * @param[in] eventkey Name/key of event as returned by msr_switch_eventRequest()
 * 
 * @return 
 * @li 0 Success
 * @li -1 Error - Unable to complete request
 *
 * On error return, errno:\n
 * <b>Errno values</b>:
 * @li EINVAL - Invalid key format
 * @li ENOENT - Internal multicast event channel does not exist
 * @li ENODEV - Specified event does not exist in channel
 */
/*SVC_PROTOTYPE*/ int msr_switch_eventRemove(char *eventkey);


/** Returns next msr switch event (blocking (based on flags, timeout)).
 * 
 * Setting flags=0 will perform the default action of retrieving the next non-previously
 * retrieved event, blocking if needed waiting for the occurrence of the next event.
 *
 * @param[in] eventkey Name/key of event as 
 * @param[in] flags Specify blocking/non-blocking/cancel/purge/last (see below)
 * @param[in] timeout (0 = wait forever) - max time in seconds to wait for event.
 *
 * @return 
 * @li >= 0 - state of msr switch:
 * @li    1 Present - msr card present in slot
 * @li    0 not - msr card not present
 * @li -1 Error - Unable to complete request
 *
 * @note
 * flags:
 * @li BLOCK      (0x00) - Get event, if no event block, waiting for event
 * @li NONBLOCK   (0x01) - Get event, don't block, return empty if no event
 * @li LAST       (0x02) - Get the last event that was received (and previously read)
 * @li CANCEL     (0x04) - Cancel/unblock any process waiting on this event key
 * @li PURGE      (0x08) - Purge/flush and pending events and return last event read
 * 
 * other:
 * @li If data != NULL, user/caller is responsible to free data (malloc'd memory).
 * @li Max data size is 4000 bytes
 * @li if flags & GETEVENT_PURGE, max data size is 2048 bytes
 * @li GETEVENT_PURGE will purge any pending events and return the last (newest) event
 * @li GETEVENT_PURGE|GETEVENT_NONBLOCK will return right away with errno=ENODATA if no events pending
 * @li GETEVENT_PURGE and GETEVENT_NONBLOCK not specified, will wait for event if no event pending
 * @li timeout parameter only used if flags == GETEVENT_BLOCK
 * @li Upon timeout, errno == EINTR (same as cancel), and event.event == ENOENT
 *
 * <b>Errno Values:</b>
 *   EINVAL - Invalid key format
 *   ENOENT - key isn't active/open (event_open() not called first)
 *   ENODEV - Unable to cancel previous event_getEvent() in progress.
 *   EAGAIN - No last event (when flags requesting last).
 *   ENODATA - No event data available (when flags set to non-block and no event).
 *   EBUSY  - event cancel already in progress, and attempting to cancel event (flags == 4).
 *   EFAULT - event.size specified, but no data available to read.
 *   EINTR  - event_getEvent() canceled (by another thread/user). event field of returned
 *            structure may contain cause (see event_cancelWithCause())
 *   ENOMEM - Unable to allocate internal memory
 */
/*SVC_PROTOTYPE*/ int msr_switch_getEvent(char* eventkey, int flags, int timeout);


#ifdef __cplusplus
}
#endif
#endif //SVC_MSR_H

/// @}*/
/// @}*/
