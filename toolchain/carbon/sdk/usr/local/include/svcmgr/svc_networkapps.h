/** @addtogroup comms Comms */
/** @{ */
/** @addtogroup networkappservice Comms - NetworkApps: Services */
/** @{ */
/** 
 * @file svc_networkapps.h 
 *
 * @brief  Net Service API
 *
 * This header file contains information about the Net Service API.  
 */
 
  /*
  * VeriFone, Inc.
  */

#ifndef SVC_NETWORKAPPS_H
#define SVC_NETWORKAPPS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "svcmgrSvcDef.h"


/*SVC_SERVICE:networkapps*/


/** Obtain the version of the Networkapps service
 *
 * @return
 * Version struct defined in svcmgrSvcDef.h
 *
 * On success, errno = 0.
 */
/*SVC_PROTOTYPE*/ struct version networkapps_getVersion(void);


/**
 * @name Return values for networkapps_gvrConfgetEvent
*/
/** \{ */
#define	NETAPP_GVRCONF_ENABLED     1   /**< enabled (when networkapps_gvrConfEnable() called and waiting for press of button) */
#define	NETAPP_GVRCONF_STARTED     2   /**< started (when broadcast message starts to be sent) */
#define	NETAPP_GVRCONF_RCVDIPCMD   3   /**< received IP conf command */
#define NETAPP_GVRCONF_IPSETOK     4   /**< IP reconfiguration succeeded */
#define NETAPP_GVRCONF_IPSETFAIL   5   /**< IP reconfiguration failed */
#define NETAPP_GVRCONF_TIMEOUT     6   /**< max timeout exceeded */
#define NETAPP_GVRCONF_CANCELED    7   /**< canceled */
#define NETAPP_GVRCONF_DONE       10   /**< protocol to end */
/** \} */

/** Read status event for gvrConf  operation
 * @param[in] flags = Get Event Flags - 0 = blocking, 1 = non-block, 2 = Last ,
 *                   4 = cancel, 8 = purge previous events (options can be or'd)
 * @return status >= 0 indicates which timeout expired or if operation canceled.
 *
 * Status >= 0: see defines above (NETAPP_GVRCONF_xxxx)
 *
 * On error return, errno:
 * @li EFAULT - event not active, gvrConf not in progress.
 * @li EAGAIN - No last event (when flags requesting last).
 * @li ENODATA - No event data available (when flags set to non-block and no event or when networkapps_gvrConfEnable() called after).
 * @li EINTR  - getEvent canceled (by another thread/user).
 */
/*SVC_PROTOTYPE*/ int networkapps_gvrConfgetEvent(int flags /* 0 */);


/** Enable the GVR Conf protocol operation
 *   Once enabled, the service waits for sysmode button to be pressed (hold for 2 seconds)
 *
 * @return status >= 0 indicates enabled, -1 for error.
 *
 * On error return, errno:
 * @li EBADF - attempt to enable GVR Conf with eth0 network interface down
 */
/*SVC_PROTOTYPE*/ int networkapps_gvrConfEnable(void);


/** Disable the GVR Conf protocol operation
 *
 * @return status >= 0 indicates enabled, -1 for error.
 *
 * On error return, errno:
 * @li N/A - currently no errors are defined (always succeeds)
 */
/*SVC_PROTOTYPE*/ int networkapps_gvrConfDisable(void);



#ifdef __cplusplus
}
#endif
#endif //SVC_NETWORKAPPS_H
