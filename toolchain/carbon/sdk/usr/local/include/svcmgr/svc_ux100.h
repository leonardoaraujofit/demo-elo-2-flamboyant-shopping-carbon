

/**

   @file	svc_ux100.h
 
   @brief   Ux100 service header file
 
   @date    September, 2012
 
   @author  Ping Zhang

   @version 1.00

   @par    	Copyright:
			(c) 2012 VeriFone, Inc. All rights reserved. 
			VeriFone and the VeriFone logo are either trademarks or 
			registered trademarks of VeriFone in the United States 
			and/or other countries. 
			All other trademarks or brand names are the properties of 
			their respective holders. All features and specifications 
			are subject to change without notice. Reproduction or 
			posting of this document without prior VeriFone approval 
			is prohibited. 
*/

#ifndef SVC_UX100_H
#define SVC_UX100_H

#ifdef __cplusplus
extern "C" {
#endif

#include "svcmgrSvcDef.h"


/** @defgroup SVC_UX100_HEADER_FILE Ux100 Service Header File */

/**   
   @ingroup SVC_UX100_HEADER_FILE
*/

/*SVC_SERVICE:ux100*/

/** \{ */
#define UX100_KEY_NONE    0x00    /*!< No keys */
#define UX100_KEY_1       0x01    /*!< key code 1 */
#define UX100_KEY_2       0x02    /*!< key code 2 */
#define UX100_KEY_3       0x03    /*!< key code 3 */
#define UX100_KEY_4       0x04    /*!< key code 4 */
#define UX100_KEY_5       0x05    /*!< key code 5 */
#define UX100_KEY_6       0x06    /*!< key code 6 */
#define UX100_KEY_7       0x07    /*!< key code 7 */
#define UX100_KEY_8       0x08    /*!< key code 8 */
#define UX100_KEY_9       0x09    /*!< key code 9 */
#define UX100_KEY_0       0x0A    /*!< key code 0 */
#define UX100_KEY_DOWN    0x0B    /*!< key code down */
#define UX100_KEY_UP      0x0C    /*!< key code up */
#define UX100_KEY_CAN     0x0D    /*!< key code cancel */
#define UX100_KEY_COR     0x0E    /*!< key code correction */
#define UX100_KEY_INFO    0x0F    /*!< key code info */
#define UX100_KEY_VAL     0x10    /*!< key code valid */
#define UX100_KEY_SECRET  0x11    /*!< key code encrypt */
#define UX100_KEY_INVALID 0x12    /*!< key code invalid */
/** \} */

/** \{ */
#define UX100_PART_NUMBER_MAX_LEN           32  /*!< Maximum length of part number */
#define UX100_SERIAL_NUMBER_MAX_LEN         11  /*!< Maximum length of serial number */
#define UX100_HARDWARE_VERSION_MAX_LEN		15  /*!< Maximum length of hardware version */
#define UX100_PCI_HARDWARE_VERSION_MAX_LEN	4   /*!< Maximum length of PCI hardware version */
#define UX100_MODEL_NUMBER_MAX_LEN			12  /*!< Maximum length of model number */
#define UX100_COUNTRY_NAME_MAX_LEN			12  /*!< Maximum length of Country name */
#define UX100_PERMANENT_TERMINAL_ID_MAX_LEN 10  /*!< Maximum length of Permanent Terminal ID */
#define UX100_ATMEL_SERIAL_NUMBER_MAX_LEN   9   /*!< Maximum length of Atmel serial number */
#define UX100_DATE_TIME_MAX_LEN             19  /*!< Maximum length of date + time string */
#define UX100_BOOT_VERSION_MAX_LEN          16  /*!< Maximum length of boot version*/
/** \} */

/**   
   @ingroup SVC_UX100_HEADER_FILE
   
    @brief
	Define ID Card structure
*/
/*SVC_STRUCT*/
struct ux100IdCardStruct
{ 
    unsigned long int device_mode;   	 				/**< device mode */
    char part_number[UX100_PART_NUMBER_MAX_LEN + 1];    		/**< part number, max 33 bytes null-terminated string */
    char serial_number [UX100_SERIAL_NUMBER_MAX_LEN + 1];    	/**< serial number, max 12 bytes null-terminated string */
    char hardware_version[UX100_HARDWARE_VERSION_MAX_LEN + 1]; /**<hardware version,  max 16 bytes null-terminated string */
    char pci_hardware_version[UX100_PCI_HARDWARE_VERSION_MAX_LEN + 1];   /**< PCI hardware version,  max 5 bytes null-terminated string */
    char model_number[UX100_MODEL_NUMBER_MAX_LEN + 1];  	 /**< model number,  max 13 bytes null-terminated string */
    char country_name [UX100_COUNTRY_NAME_MAX_LEN + 1];   	/**< country name,  max 13 bytes null-terminated string */
    char permanent_terminal_id[UX100_PERMANENT_TERMINAL_ID_MAX_LEN + 1];   	/**< permanent terminal ID,  max 11 bytes null-terminated string */
};


/** @ingroup SVC_UX100_HEADER_FILE
    @brief
	Secure software version
*/
/*SVC_STRUCT*/
struct ux100SecureVersionStruct
{
   	char id_code[50];     /**< Software package name */
   	char family[10];      /**< PCI or equivalent certification ID */
   	char version[12];     /**< version */
};

/** @ingroup SVC_UX100_HEADER_FILE
    @brief
	Secure software version for VHQ
*/
/*SVC_STRUCT*/
struct ux100SoftwareInfoForVHQStruct
{
	char app_name[50];     	/**< application name */
   	char app_version[12];   /**< application version */
   	char app_type[10];     	/**< application type */
	char bundle_name[50];	/**< bundle name */
	char bundle_version[12];/**< bundle version */
};


/** @ingroup SVC_UX100_HEADER_FILE
    @brief
	Define Display Information structure
*/
/*SVC_STRUCT*/
struct  ux100DisplayInfoStruct      
{
  int currentBacklightMode;		/**< curent backlight mode setting of the display */
  int currentBklTime;			/**< current backlight timer in ms */
  int currentContrast;			/**< current contrast of the display */	
  int MaxBacklight;			/**< maximum backlight setting for the display */
  int MaxContrast;			/**< maximum contast setting for the display */ 	
  int MinBacklight;			/**< minimum backlight setting for the display */
  int MinContrast;			/**< minumim contrast setting for the display */
  int ScreenHeight;			/**< Heigth of the screen in pixels */
  int ScreenWidth;			/**< Width of the screen in pixels */
  int ScreenColorDepth;		/**< Depth of the screen color if any */							
};


/** @ingroup SVC_UX100_HEADER_FILE
    @brief
	Define Display bitmap structure
*/
/*SVC_STRUCT*/
struct ux100BitmapStruct
{   void* data; 	/**< Bitmap to display */
    int data_count; /**<	Number of bytes in the bitmap */	
};


/** @ingroup SVC_UX100_HEADER_FILE
    @brief
	Define Keyboard information structure
*/
/*SVC_STRUCT*/
struct ux100KeybdInfoStruct
{
   unsigned char secure_mode;     /**< Is the keyboard in secure mode ? */
   unsigned char nb_function_key; /**< Number of function keys on the keyboard */
   unsigned char nb_normal_key;   /**< Number of normal keys on the keyboard */
   unsigned char nb_row;          /**< Number of normal key rows */
   unsigned char nb_col;          /**< Number of normal key columns */
   unsigned char key_map[3*5];    /**< Keyboard map */
};


/** @ingroup SVC_UX100_HEADER_FILE
    @brief
	Define Buzzer information structure
*/
/*SVC_STRUCT*/
struct  ux100BuzzerInfoStruct            
{
    unsigned int currentFrequency;	/**< current frequency of the buzzer */
    unsigned int currentTime;		/**< current time (duration) of the buzzer */
    unsigned int currentVolume;		/**< current volume of the buzzer */	
};


/** @ingroup SVC_UX100_HEADER_FILE
    @brief
	Define buffer structure
*/
/*SVC_STRUCT*/
struct uxByteBuffer
{
    void* data;     /**< data */
    int data_count; /**< data size */
};

/** @ingroup SVC_UX100_HEADER_FILE
    @brief
	Define PIN block structure for PIN session
*/  
/*SVC_STRUCT*/
struct ux100EncPinBlockSt     
{ 
    void* data;         /**< Encrytped PIN Block */
    int data_count; 	/**< PIN Block Length */
};

/** @ingroup SVC_UX100_HEADER_FILE
    @brief
	Define Part Number structure
*/
/*SVC_STRUCT*/
struct ux100PartNumberStruct
{
   char part_number[UX100_PART_NUMBER_MAX_LEN + 1];    /**< ux100 part number */
};

/** @ingroup SVC_UX100_HEADER_FILE
    @brief
	Define Serial Number structure
*/
/*SVC_STRUCT*/
struct ux100SerialNumberStruct    
{
   char serial_number[UX100_SERIAL_NUMBER_MAX_LEN + 1];    /**< ux100 serial number */
};

/** @ingroup SVC_UX100_HEADER_FILE
    @brief
	Define Hardware Version structure
*/
/*SVC_STRUCT*/
struct ux100HardwareVersionStruct    
{
   char hardware_version[UX100_HARDWARE_VERSION_MAX_LEN + 1];    /**< ux100 hardware version */
};

/** @ingroup SVC_UX100_HEADER_FILE
    @brief
	Define PCI Hardware Version structure
*/
/*SVC_STRUCT*/
struct ux100PCIHardwareVersionStruct    
{
   char pci_hardware_version[UX100_PCI_HARDWARE_VERSION_MAX_LEN + 1];    /**< ux100 PCI hardware version */
};

/** @ingroup SVC_UX100_HEADER_FILE
    @brief
	Define Model Number structure
*/
/*SVC_STRUCT*/
struct ux100ModelNumberStruct    
{
   char model_number[UX100_MODEL_NUMBER_MAX_LEN + 1];    /**< ux100 model number */
};

/** @ingroup SVC_UX100_HEADER_FILE
    @brief
	Define Model Number structure
*/
/*SVC_STRUCT*/
struct ux100CountryNameStruct    
{
   char country_name [UX100_COUNTRY_NAME_MAX_LEN + 1];  /**< ux100 Country name */
};

/** @ingroup SVC_UX100_HEADER_FILE
    @brief
	Define Permanent Terminal ID structure
*/
/*SVC_STRUCT*/
struct ux100PermanentTerminalIDStruct
{
   char permanent_terminal_id[UX100_PERMANENT_TERMINAL_ID_MAX_LEN + 1];  /**< ux100 Permanent Terminal ID */
};

/** @ingroup SVC_UX100_HEADER_FILE
    @brief
	Define Atmel Serial Number
*/
/*SVC_STRUCT*/
struct ux100AtmelSerialNumber    
{
   char atmel_sn [UX100_ATMEL_SERIAL_NUMBER_MAX_LEN + 1];  /**< ux100 Atmel SN */
};

/** @ingroup SVC_UX100_HEADER_FILE
    @brief
	Define UX100 RTC time string
*/
/*SVC_STRUCT*/
struct ux100DateTime    
{
   char rtc [UX100_DATE_TIME_MAX_LEN + 1];  /**< ux100 RTC value, YYYY-MM-DD HH:MM:SS */
};

/** @ingroup SVC_UX100_HEADER_FILE
    @brief
	Define Ux100 Log structure
*/
/*SVC_STRUCT*/
struct ux100LogEventsStruct
{
    void* data;     /** Ux100 log data in xml format */
    int data_count; /** Size of the data */
};

/** @ingroup SVC_UX100_HEADER_FILE
    @brief
	Define Key List structure
*/ 

/*SVC_STRUCT*/
/**
 * Define a Key Information struct
 */
struct ux100KeyInfo     
{
	unsigned char contentType;	// Content type
	unsigned int  keyID;		// Key ID
}; 

/*SVC_STRUCT*/
/**
 * Define a Key List struct
 */
struct ux100KeyList     
{ 
    int key_count;					// Number of keys in the list
    struct ux100KeyInfo* key_info;	// Key information list.
};

/*SVC_STRUCT*/
/**
 * Define a BOOT version struct
 */
struct ux100BootVersion
{
	char boot_version[UX100_BOOT_VERSION_MAX_LEN];
};

/**
 * Define a Diagnostic Counter struct
 */
/*SVC_STRUCT*/
struct ux100DiagCounter
{
	char         text[17];	// Max of 16 chars + 1 null
	unsigned int flags;
	int          value;
	int          index;
};

/**
 * Define a USB Driver Information struct
 */
/*SVC_STRUCT*/
struct ux100USBDriverInfo
{
	int attached;	// UX1 device is attached
	int bus_num;	// USB bus number, where the UX1 is attached
	int vid;		// Vendor ID of attached device
	int pid;		// Product ID of attached device
	int type;		// UX1 model (0-unknown, 100-UX100, 110-UX110)
	int parent_vid;	// Parent Vendor ID of attached device
	int parent_pid;	// Parent Product ID of attached device
};

/************************************************************************
                            SERVICE API
 **********************************************************************/

/************************************************************************
                             Info API Part
 ***********************************************************************/
/** @defgroup SVC_UX100_INFO_API Ux100 Information API */

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Obtain the version of the software (svc_ux100)
   
   @details  
   This function returns version of software such as major, minor, maint, build numbers.

   @retval  version struct defined in svcmgrSvcDef.h if errno = 0. Otherwise, check errno values.
*/
/*SVC_PROTOTYPE*/
struct version ux100_getVersion( void );

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Get the list of ux100 devices

   @details  
   bit mask defining device presence. Check Errno for errors

   @retval 0 upon successful request, or -1 if failled and check errno for errors
*/
/*SVC_PROTOTYPE*/
int ux100_infoGetDeviceList( );

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Obtain the version of the software
   
   @details  
   This function returns the version of secure firmware such as software package name, 
   PCI or equivalent certficiation ID, and version number.  

 * @retval struct ux100SecureVersionStruct - contains ux100 firmware version if errno =0 upon successful request. 
 * Otherwise, check errno values.
 */
/*SVC_PROTOTYPE*/
struct ux100SecureVersionStruct ux100_infoGetSecureVersion( void );

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   TODO
   
   @details  
   This function returns the version of secure firmware such as software package name, 
   PCI or equivalent certficiation ID, and version number.  

 * @retval struct ux100SecureVersionStruct - contains ux100 firmware version if errno =0 upon successful request. 
 * Otherwise, check errno values.
 */
/*SVC_PROTOTYPE*/
struct ux100SoftwareInfoForVHQStruct ux100_infoSoftwareInfoForVHQ( void );



/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Set up ID card information

   @details  
   This function sets up Ux100's ID card information such as device mode, part number, serial number, 
   hardware version, PCI hardware version, model number, and country name.

   @retval 0 upon successful request, or -1 if failled and check errno for errors
*/
/*SVC_PROTOTYPE*/
int ux100_infoSetIdCard( struct ux100IdCardStruct idcard);


/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Obtain Ux100 device mode

   @details  
   This function returns back Ux100's device mode

    * @retval	0 	Ux100 in Manufacture Mode, if errno = 0
    * @retval	1	Ux100 in Production Mode, if errno = 0
    * @retval	8   Ux100 in Developement Mode, if errno = 0    
    * @retval	-1	Unable to complete request (check errno)
    * @note			The returned value has a meaning only if errno = 0. 
*/
/*SVC_PROTOTYPE*/
int ux100_infoGetDeviceMode( void );

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Obtain Ux100 Part Number

   @details  
   This function returns back Ux100's part number

   @retval  struct ux100PartNumberStruct - contains part number information if errno = 0. Otherwise check errno for errors
   
*/
/*SVC_PROTOTYPE*/
struct ux100PartNumberStruct ux100_infoGetPartNumber( void );

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Obtain Ux100 Serial Number

   @details  
   This function returns back Ux100's serial number

   @retval  struct ux100SNStruct - contains serial number information if errno = 0. Otherwise check errno for errors
   
*/
/*SVC_PROTOTYPE*/
struct ux100SerialNumberStruct ux100_infoGetSerialNumber( void ); 

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Obtain Ux100 Hardware Version

   @details  
   This function returns back Ux100's hardware version

   @retval  struct ux100HardwareVersionStruct - contains hardware version information if errno = 0. Otherwise check errno for errors
   
*/
/*SVC_PROTOTYPE*/
struct ux100HardwareVersionStruct ux100_infoGetHardwareVersion( void ); 

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Obtain Ux100 PCI Hardware Version

   @details  
   This function returns back Ux100's PCI hardware version

   @retval  struct ux100PCIHardwareVersionStruct - contains PCI hardware version information if errno = 0. Otherwise check errno for errors
   
*/
/*SVC_PROTOTYPE*/
struct ux100PCIHardwareVersionStruct ux100_infoGetPCIHardwareVersion( void ); 

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Obtain Ux100 Model Number

   @details  
   This function returns back Ux100's model number

   @retval  struct ux100ModelNumberStruct - contains model number information if errno = 0. Otherwise check errno for errors
   
*/
/*SVC_PROTOTYPE*/
struct ux100ModelNumberStruct ux100_infoGetModelNumber( void ); 

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Obtain Ux100 Country Name

   @details  
   This function returns back Ux100's mcountry name

   @retval  struct ux100CountryNameStruct - contains country name information if errno = 0. Otherwise check errno for errors
   
*/
/*SVC_PROTOTYPE*/
struct ux100CountryNameStruct ux100_infoGetCountryName( void ); 

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Obtain Ux100 Permanent terminal ID 
   @details  
   This function returns back Ux100's Permanent terminal ID that indicates to which country this unit went.

   @retval  struct ux100PermanentTerminalIDStruct - contains Permanent terminal ID information if errno = 0. Otherwise check errno for errors
   
*/
/*SVC_PROTOTYPE*/
struct ux100PermanentTerminalIDStruct ux100_infoGetPermanentTerminalID( void ); 

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Get status of the anti-removal switch of the ux100.

   @details  
   This function returns the status of the anti-removal switch of the ux100.

    * @retval	0 	The state of the anti-removal switch is armed, if errno = 0
    * @retval	1	The state of the anti-removal switch is trigged, if errno = 0
    * @retval	-1	Unable to complete request (check errno)
    * @note			The returned value has a meaning only if errno=0. 
*/
/*SVC_PROTOTYPE*/
int ux100_infoGetRemSwStatus( void );

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Get coin charging status

   @details  
   This function returns the coin charging status of the ux100.

    * @retval	0	The charging status of the coin cell is bad.
    * @retval	1	The charging status of the coin cell is good.
    * @retval	-1	Unable to complete request (check errno)
*/ 
/*SVC_PROTOTYPE*/
int ux100_infoGetCoinChargingStatus( void );

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Get Temperature Sensor value of the ux100.

   @details  
   This function returns the temperature Sensor value of the ux100.

    * @retval	n 	If errno = 0, this temperature value is in range from -40C to +125C. 
             	The accuracy of the measure is 1C.
    * @retval	1	The charging status of the coin cell is good, if errno = 0, 
    * @retval	-1	Unable to complete request (check errno)
    *
    * @note			The returned value has a meaning only if errno=0.    
*/ 
/*SVC_PROTOTYPE*/ 
int ux100_infoGetSensorTemperature( void );

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Get back Ux100 connection status

   @details  
   This function checks whether the Ux100 is connected to the Ux300 (ready to communicate)

   @retval 0: Ux100 is connected, or -1 timeout
*/
/*SVC_PROTOTYPE*/
int ux100_infoGetConnectionStatus( unsigned int timeout);

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Get back Ux100 tampered status

   @details  
   This function checks whether the Ux100 is tampered

   @retval	0	Ux100 is OK
   @retval	1	Ux100 tampered
   @retval	-1	Other error
*/
/*SVC_PROTOTYPE*/
int ux100_infoGetTamperStatus( void );

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Check pairing status (Ux100<-->Ux300)

   @details  
   This function checks the pairing status between Ux100 and Ux300

   @retval	0	Pairing is OK
   @retval	1	Pairing failed due to missing keys in the ux100
   @retval	2	Pairing failed due to missing keys in the ux300
   @retval	3	Pairing failed due to bad PKI in the ux100
   @retval	4	Pairing failed due to bad PKI in the ux300
   @retval	-1	Other error
*/
/*SVC_PROTOTYPE*/
int ux100_infoGetPairingStatus( void );

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Get PINPAD Operational Mode

   @details  
   This function retrieves PINPAD Operational Mode

   @retval	0	Field Mode
   @retval	1	Production Mode #1
   @retval	2	Production Mode #2
   @retval	3	Restricted Mode
   @retval	4	Controller Personalization Mode
   @retval	-1	Fails to get Operational Mode
*/
/*SVC_PROTOTYPE*/
int ux100_infoGetOperationalMode( void );

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Obtain Ux100 Atmel Serial Number 
   @details  
   This function returns back Ux100's Atmel Serial Number.

   @retval  struct ux100AtmelSerialNumber - contains Atmel Serial Number if errno = 0. Otherwise check errno for errors
   
*/
/*SVC_PROTOTYPE*/
struct ux100AtmelSerialNumber ux100_infoGetAtmelSerialNumber( void );

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Reset UX100 Event Log

   @details  
   This function resets the UX100 Event Log. 
   
   @return	0  upon successful request
   @return  -1 unable to complete request (Check Errno) 
*/
/*SVC_PROTOTYPE*/
int ux100_infoResetLogHeader( void );

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Retrieve log from Ux100
 *  
 * @return struct ux100LogEventsStruct - Output data. Depending on data length, structure may have data returned if errno = 0.
 * @return errno != 0 - unable to complete request
 *  
 * @note IMPORTANT NOTES:
 * @li Application must free output structure 'data' member when non-NULL.
 *     
*/
/*SVC_PROTOTYPE*/
struct ux100LogEventsStruct ux100_infoGetLogEvents( void );

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Save event in UX100 Event Log

   @details  
   This function stories new event in the UX100 Event Log. 
   
   @param[in] event	Event to save into UX100 Event Log
   @param[in] size	Event length

   @return	0  upon successful request
   @return  -1 unable to complete request (Check Errno) 
*/
/*SVC_PROTOTYPE*/
int ux100_infoSetLogEvent( char* event, int size );

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Retrieve UX100 time

   @details  
   This function reads the UX100 RTC value and convert it to ASCII string (YYYY-MM-DD HH:MM:SS).
   
   @retval  struct ux100DateTime - contains device date and time string if errno = 0. Otherwise check errno for errors
*/
/*SVC_PROTOTYPE*/
struct ux100DateTime ux100_infoGetRTC( void );

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Set UX100 date and time

   @details  
   This function sets the UX100 date and time. 
   
   @param[in] s	Date and time string (YYYY-MM-DD HH:MM:SS)
   
   @return	0  upon successful request
   @return  -1 unable to complete request (Check Errno) 
*/
/*SVC_PROTOTYPE*/
int ux100_infoSetRTC( char* s );

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Get the Heater Mode

   @details  
   This function retrieves the Heater Mode

   @retval	0	heater off or not supported
   @retval	1	heater on (normal)
   @retval	2	heater boost
   @retval	-1	Fails to get Heater Mode
*/
/*SVC_PROTOTYPE*/
int ux100_infoGetHeaterMode ( void );

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Set the Heater Mode

   @details  
   This function sets the Heater Mode.

   @param[in] mode	Heater mode to set (0 - OFF, 1 - ON, 2 - BOOST).

   @return	0  upon successful request
   @return  -1 unable to complete request (Check Errno) 
*/
/*SVC_PROTOTYPE*/
int ux100_infoSetHeaterMode ( int mode );

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Get the Unit Type

   @details  
   This function retrieves the Unit Type such as Ux100 or Ux110

   @retval	100	Ux100
   @retval	110	Ux110
   @retval	-1	Fails to get the Unit Type
*/
/*SVC_PROTOTYPE*/
int ux100_infoGetUnitType( void );

/**
   @ingroup SVC_UX100_INFO_API

   @brief   Get the Heater Status

   @details
   This function retrieves the Heater Mode

   @retval	0	heater off or heater not available(For Ux110 only)
   @retval	1	heater on
   @retval	-1	Fails to get Heater Status
*/
/*SVC_PROTOTYPE*/
int ux100_infoGetHeaterStatus ( void );

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Retrieve UX100 BOOT version

   @details  
   This function reads the UX100 BOOT version.
   
   @retval  struct ux100BootVersion - contains device BOOT version string if errno = 0. Otherwise check errno for errors
*/
/*SVC_PROTOTYPE*/
struct ux100BootVersion ux100_infoGetBootVersion( void );

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Retrieve UX100 Diagnostic Counter

   @details  
   This function retrieves the Ux100 Diagnostic Counter given by index.
   
   @param[in] index	Index of Diagnostic Counter to retrieve.

   @retval  struct ux100DiagCounter - contains the requested counter information, if errno = 0. 
            Otherwise check errno for errors.
*/
/*SVC_PROTOTYPE*/
struct ux100DiagCounter ux100_infoGetDiagCounter( int index );

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Set UX100 Diagnostic Counter

   @details  
   This function sets the Ux100 Diagnostic Counter given by index.
   
   @param[in] index	Index of Diagnostic Counter to set.
   @param[in] value	New value of Diagnostic Counter to set.

   @retval  struct ux100DiagCounter - contains the modified counter information, if errno = 0. 
            Otherwise check errno for errors.
*/
/*SVC_PROTOTYPE*/
struct ux100DiagCounter ux100_infoSetDiagCounter( int index, int value );

/**   
   @ingroup SVC_UX100_INFO_API

   @brief   Retrieve USB device information

   @details  
   This function reads information about attached UX1 device.
   
   @retval  struct ux100USBDriverInfo - contains major information about attached device if errno = 0. Otherwise check errno for errors
*/
/*SVC_PROTOTYPE*/
struct ux100USBDriverInfo ux100_infoGetUSBDeviceInfo( void );

/************************************************************************
                            Display API Part
 ***********************************************************************/

/** @defgroup SVC_UX100_DISPLAY_API Ux100 Display API */
 
/**   
   @ingroup SVC_UX100_DISPLAY_API

   @brief   Initialize display device

   @details  
   This function returns the status of the anti-removal switch of the ux100.

    * @retval	0 	upon successful request
    * @retval	-1	Unable to complete request (check errno)
    * @note			Initialize display device. Shall be called once (at least).
*/
/*SVC_PROTOTYPE*/
int ux100_dispInit( );

///@}

/**   
   @ingroup SVC_UX100_DISPLAY_API

   @brief   Get display device information

   @details  
   This function returns display device information as defined by ux100DisplayInfoStruct

    * @retval	 struct ux100DisplayInfoStruct - contain information on display device, if errno = 0
    * @retval	-1	Unable to complete request (check errno)
*/
/*SVC_PROTOTYPE*/
struct ux100DisplayInfoStruct ux100_dispGetInfo(  );



/**   
   @ingroup SVC_UX100_DISPLAY_API

   @brief   Set Bitmap for Display

   @details  
   This function sets up bitmap display as follows: 
 * @param[in] posy Line number from the top starting to 0 (a line is 8 pixel height).
 * @param[in] posx Column number from the left starting to 0.
 * @param[in] width Number of pixels across
 * @param[in] bmpSt Bitmap to display
 *  
 * @retval	0 	upon successful request
 * @retval	-1	Unable to complete request (check errno)
*/
/*SVC_PROTOTYPE*/
int ux100_dispSetBitmap( int posy, int posx, int width, struct ux100BitmapStruct bmpSt);

/**   
   @ingroup SVC_UX100_DISPLAY_API

   @brief   Set Display screen contrast

   @details  
   This function gets up bitmap display from ux100, which is set by ux100_dispSetBitmap() 
   
 * @param[in] level contrast level value
 *  
 * @retval	struct ux100BitmapStruct - Bitmap data. Depending on data length, msg may have data returned.
 *
 * @return 0  upon successful request
 * @return  -1 unable to complete request (Check Errno) 
*/
/*SVC_PROTOTYPE*/
int ux100_dispSetContrast( int level );

/**   
   @ingroup SVC_UX100_DISPLAY_API

   @brief   Set display backlight mode

   @details  
   This function Set display backlight mode in three modes: on, off and automatic
   
 * @param[in] mode 0 = off, 1 = on, 2 = automatic.
 * @param[in] time backlight duration
 *  
 * @return 0  upon successful request
 * @return  -1 unable to complete request (Check Errno) 
*/
/*SVC_PROTOTYPE*/
int ux100_dispSetBacklightMode ( int mode, unsigned int time );


/************************************************************************
                            Keyboard API Part
 ***********************************************************************/

/** @defgroup SVC_UX100_KEYBOARD_API Ux100 Keyboard API */
  
/**   
   @ingroup SVC_UX100_KEYBOARD_API

   @brief   Initialize display device


 * @return 0  upon successful request
 * @return  -1 unable to complete request (Check Errno) 
 * 
 *@note It should be called once (at least).  
*/
/*SVC_PROTOTYPE*/
int ux100_keybdInit(  );

/**   
   @ingroup SVC_UX100_KEYBOARD_API

   @brief   Get the next available key from the buffer.

 * @param[in] timeout Time (in milliseconds) to wait for a key strike if errno = 0.
 * @return  -1 unable to complete request (Check Errno) 
 * 
*/
/*SVC_PROTOTYPE*/
int ux100_keybdGetKey( unsigned int timeout );

/**   
   @ingroup SVC_UX100_KEYBOARD_API

   @brief   Activate/Deactivate the numeric input restriction.
 * @par     Detailled description:
            The numeric input restriction assumes inability to enter more 
            than 3 numeric keys without separation by functional key.

            NOTE: This method ONLY has affect on Ux300 when Ux110 connected.

 * @param[in] on_off_flag Flag to activate(!=0)/deactivate(==0) numeric input restriction.
 * @return  -1 unable to complete request (Check errno) 
 * 
*/
/*SVC_PROTOTYPE*/
int ux100_keybNumericInputRestrictionPolicy( int on_off_flag );

/**   
 * @ingroup SVC_UX100_KEYBOARD_API

 * @brief   Lock the keyboard numeric input.
 * @par     Detailed description:
            The function locks the numeric input which assumes inability to enter any 
            numeric keys. This restriction is not applicable to functional keys.

            NOTE: This method ONLY has effect on Ux300 when Ux110 connected.

 * @return  -1 unable to complete request (Check errno) 
 * On error return, errno:
 * @li EPERM  - Operation not applicable for existing system configuration.
*/
/*SVC_PROTOTYPE*/
int ux100_keybNumericInputLock( void );  

/**   
 * @ingroup SVC_UX100_KEYBOARD_API

 * @brief   Unlock the numeric input.
 * @par     Detailed description:
            The function unlocks the numeric input.

            NOTE: This method ONLY has effect on Ux300 when Ux110 connected.

 * @return  -1 unable to complete request (Check errno) 
 * On error return, errno:
 * @li EPERM  - Operation not applicable for existing system configuration.
 * @li EAGAIN - The reset timer is running, try again later.
*/
/*SVC_PROTOTYPE*/
int ux100_keybNumericInputUnlock( void );  


/**
 * @ingroup SVC_UX100_KEYBOARD_API
 *
 * @brief  Used to open keyboard device and redirect key presses to event for capturing
 *    See: ux100_getKeyEventKeybd(), ux100_closeKeybd()
 *
 * Note: while keyboard is open via ux100_openKeybd(), no ux100 keyboard output is fed to
 *       /dev/uinput (ie, no other processes will see any key presses via /dev/input/event)
 *
 * @return status - indicates if successful or failed
 *
 * @param[in] innerKeyTimeout - maximum timeout in milliseconds to wait for a key press (0 = infinite). This
 *                              affects when/what ux100_getEventKeybd() returns. Must be >= 0.
 *                              Between digits, timeout is between key release and the next key press. No
 *                              timeout occurs while a key is held/pressed.
 * @return If = 0 then Success; If = -1 then error, check errno:
 * @li EINVAL - invalid parameters
 * @li EBUSY  - when the keyboard device is already open.
 * @li EACCES - access denied, unable to open keyboard device.
 */
/*SVC_PROTOTYPE*/ int ux100_openKeybd(int innerKeyTimeout);

/*SVC_STRUCT*/
/**
 * Structure to store keycode event
 */
struct ux100_keyValueData {
    int keycode;	/**< Key code (standard linux codes) */
    char keyValue[4];	/**< KeyValue string null terminated */
};


/**
 * @ingroup SVC_UX100_KEYBOARD_API
 *
 * @brief  Get key press via events
 *    See: ux100_openKeybd(), ux100_closeKeybd()
 *
 * This method uses events and allows caller to get the last key press,
 *  get key with wait, get key w/o wait, or cancel key wait (used to unblock
 *  any other process waiting on key press).
 *
 *  SECURITY: @li Only 3 numeric keys may be pressed in a row (any additional numeric key presses are ignored
 *              and not processed.
 *            @li To clear (and start the count over), either:
 *                  press any non-numeric key
 *                        -OR-
 *                  close the keybd (ux100_closeKeybd()) for 2 seconds (minimum).
 *              Note: if the keybd is closed and reopened in < 2sec., then the counter is not reset.
 *
 * @param[in] flags = Get Event Flags - 0 = block, 1 = non-block, 2 = Last , 4 = cancel (options can be or'd)
 * @return keyValueData:
 * 	  check errno == 0, no errors, and valid keyValue returned. Note: keyValue = "" when errno != 0
 *    keyValue > 0 keyValue of key being pressed/activated.
 *    keyValue = empty string, indicates key released, this allows caller to implement repeat key if desired.
 *      values for keyValue are:
 *      @verbatim
 *      | key  | keycode | keyValue |
 *      ----------------------------
 *      |  1   |     2   |   "1"    |
 *      |  2   |     3   |   "2"    |
 *      |  3   |     4   |   "3"    |
 *      |  4   |     5   |   "4"    |
 *      |  5   |     6   |   "5"    |
 *      |  6   |     7   |   "6"    |
 *      |  7   |     8   |   "7"    |
 *      |  8   |     9   |   "8"    |
 *      |  9   |    10   |   "9"    |
 *      |  0   |    11   |   "0"    |
 *      | down |   108   |   "DN"   |
 *      | up   |   103   |   "UP"   |
 *      | STOP |     1   |   "ESC"  |
 *      | CORR |    14   |   "BS"   |
 *      | INFO |   138   |   "HLP"  |
 *      |  OK  |    28   |   "CR"   |
 *      @endverbatim
 *
 *    errno values:
 * @li ETIME - timeout occured waiting for keypress/release
 * @li EINVAL - invalid flags values
 * @li EACCES - when access is denied
 * @li EINTR  - get key event canceled
 * @li ENOENT  - when no key data is available
 */
/*SVC_PROTOTYPE*/ struct ux100_keyValueData ux100_getKeyEventKeybd(int flags /* 0 */);

/**
 * @ingroup SVC_UX100_KEYBOARD_API
 *
 * @brief  Used to disable redirecting of key presses to event.
 *    See: ux100_openKeybd(), ux100_getKeyEventKeybd()
 *
 * This method closes key board and event. Any pending key press events
 * will be released.
 *
 * @return If >= 0 then Success; If = -1 then error, errno:
 *   EBADF - invalid handle
 */
/*SVC_PROTOTYPE*/ int ux100_closeKeybd(void);



/************************************************************************
                            Security API Part
 ***********************************************************************/
/** @defgroup SVC_UX100_SECURITY_API Ux100 Security API */
 
/**   
   @ingroup SVC_UX100_SECURITY_API

   @brief   Get current SECIRQ value in pinpad
 *
 *
 * @return current SECCIRQ if errno =0 (successful request), or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int ux100_secGetCurrSECIRQ( void );

/**   
   @ingroup SVC_UX100_SECURITY_API

   @brief   Get saved SECIRQ value in pinpad
 *
 * @return saved SECCIRQ if errno =0 (successful request), or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int ux100_secGetSavedSECIRQ( void );

/**   
   @ingroup SVC_UX100_SECURITY_API

   @brief   Get saved UKSR value in pinpad
 *
 * @return saved SECCIRQ if errno =0 (successful request), or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int ux100_secGetSavedUKSR(  void );

/**   
   @ingroup SVC_UX100_SECURITY_API

   @brief   Get current UKSR value in pinpad
 *
 * @return saved SECCIRQ if errno =0 (successful request), or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int ux100_secGetCurrUKSR(  void );

/**   
   @ingroup SVC_UX100_SECURITY_API

   @brief   Generate a random from Ux100
 *  
 * @param[in] size Length of the random number
 * 
 * @return struct uxByteBuffer - Output data. Depending on data length, msg may have data returned if errno = 0.
 * @return  -1 unable to complete request (Check Errno)
 *  
 * @note IMPORTANT NOTES:
 * @li Application must free output buffer 'data' member when non-NULL.
 *     
*/
/*SVC_PROTOTYPE*/
struct uxByteBuffer ux100_secGenerateRandom( int size );

/**   
   @ingroup SVC_UX100_SECURITY_API

   @brief	Set a date, which ux100 can trust
 *
 * @param[in]  date         BDC coded date (yyyymmdd)
 *
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int ux100_secSetSecureDate( unsigned int date );

/**   
   @ingroup SVC_UX100_SECURITY_API

   @brief	Load a certificate
 *
 * @param[in] type Public key type used to authenticate certificate
 * @param[in] certif Certificate structure
 *
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int ux100_secLoadCertificate( int type, struct uxByteBuffer certif );

/**   
   @ingroup SVC_UX100_SECURITY_API

   @brief	Load a public key
 *
 * @param[in] type Public key type
 * @param[in] key Public key structure
 *
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
/*SVC_PROTOTYPE*/ int ux100_secLoadPublicKey( int type, struct uxByteBuffer key );

/**   
   @ingroup SVC_UX100_SECURITY_API

   @brief	Read a certificate.
 *
 * @param[in] type Certificate type
 *
 * @return struct uxByteBuffer - certificate. Depending on data length, msg may have data returned.
 * IMPORTANT NOTES:
 * @li Application must free 'data' member when non-NULL.
 */
/*SVC_PROTOTYPE*/ struct uxByteBuffer ux100_secReadCertificate( int type );

/**   
   @ingroup SVC_UX100_SECURITY_API

   @brief	Read a public key
 *
 * @param[in] type Public key type
 *
 * @return struct uxByteBuffer - public key. Depending on data length, msg may have data returned.
 * IMPORTANT NOTES:
 * @li Application must free 'data' member when non-NULL.
 */
/*SVC_PROTOTYPE*/ struct uxByteBuffer ux100_secReadPublicKey( int type );

/**   
   @ingroup SVC_UX100_SECURITY_API

   @brief	Perform the mutual authentication between the ux100 and controller
 *
 * @param[in]  forceRenewal 	Force the mutual authentication regardless of the keys existence
 *
 * @return 0 - function successfully executed, or -1 - an error occurs. Check the errno value.
 */
/*SVC_PROTOTYPE*/ int ux100_secPerformParing( int forceRenewal );

/**   
   @ingroup SVC_UX100_SECURITY_API

   @brief	Generate a public and private RSA key pair in the ux100
 *
 * @param[in]  exponent		Public Exponent
 * @param[in]  size			Key Size (in bytes)
 * @param[in]  forceRenewal	Force the mutual authentication regardless of the keys existence
 *
 * @return 0 - function successfully executed, or -1 - an error occurs. Check the errno value.
 */
/*SVC_PROTOTYPE*/ int ux100_secGenerateRSAKeyPair( unsigned int exponent, int size, int forceRenewal );

/**   
   @ingroup SVC_UX100_SECURITY_API

   @brief	Retrieve list of all symmetric/asymmetric keys loaded/generated in the ux100
 *
 * @param[in] NA
 *
 * @return struct ux100KeyList - list of keys. Depending on data length, message may have data returned.
 *
 * IMPORTANT NOTES:
 * @li Application must free 'info' member when non-NULL.
 */
/*SVC_PROTOTYPE*/ struct ux100KeyList ux100_secGetKeyList( void );

/**   
   @ingroup SVC_UX100_SECURITY_API

   @brief  Retrieves cryptographic challenge to reset the anti-removal switch.
 *      
 * @param[in] NA
 *   
 * // todo Add encryption/decryption functions instead memcpy().
 *  
 * @retval	struct uxByteBuffer		Encrypted Ux100 challenge, if errno = 0;
 * @otherwise check errno
 *
 * IMPORTANT NOTES:
 * @li Application must free 'data' member when non-NULL.
*/
/*SVC_PROTOTYPE*/
struct uxByteBuffer ux100_secGetRemovalSwitchChallenge( void );

/**   
   @ingroup SVC_UX100_SECURITY_API

   @brief  Resets the anti-removal switch of the ux100.
 *      
 * @param[in] f_pbChallenge Cryptographic challenge to confirm command authencity.
 *   
 * //todo Add encryption/decryption functions instead memcpy().
 *  
 * @retval	0	The command completed successfully.
 * @retval	-1	Unable to complete request (check errno).
*/
/*SVC_PROTOTYPE*/
int ux100_secRemovalSwitchReset( struct uxByteBuffer f_pbChallenge );


/**   
   @ingroup SVC_UX100_SECURITY_API

   @brief  Initializes the ciphered communication link between controller and PINPAD.
 *
 * @param[in]  NA
 *
 * @return 0 - function successfully executed, or -1 - an error occurs. Check the errno value.
 */
/*SVC_PROTOTYPE*/ int ux100_secInitCipheredLink( void );

/**   
   @ingroup SVC_UX100_SECURITY_API

   @brief  This function handles the numeric entry in tamper mode function in the PINPAD.
 *
 * @param[in]  function		= 0		Deactivate numeric entry in tamper mode
 * 							= 1		Activate numeric entry in tamper mode
 * 							= 2		Get status of numeric entry in tamper mode
 *
 * @return 0 - function successfully executed, numeric entry in tamper mode deactivated
 *         1 - function successfully executed, numeric entry in tamper mode activated		    
 *  	  -1 - an error occured. Check the errno value.
  */
/*SVC_PROTOTYPE*/ int ux100_secTamperNumEntHandl( unsigned char f_bType );


/************************************************************************
                             PIN API Part
 ***********************************************************************/
/** @defgroup SVC_UX100_PIN_API Ux100 PIN Session API */
 
/**   
   @ingroup SVC_UX100_PIN_API

   @brief  Start a PIN entry process by setting up minimum and maximum PIN length and timeout.
   
   @details PIN session must begins by this function to set up Ux100 under PIN mode
 *      
 * @param[in] pinLenMin minimum pin length
 * @param[in] pinLenMax maximum pin length
 * @param[in] timeout   timeout for pin entry
 * 
 * @retval	0	Command completed successfully.
 * @retval	-1	Unable to complete request (check errno).
*/
/*SVC_PROTOTYPE*/
int  ux100_pinStartPinEntry( unsigned char  pinLenMin, unsigned char pinLenMax, int timeout );    

/**   
   @ingroup SVC_UX100_PIN_API

   @brief  Get the number of entered PIN digits.
   
 * 
 * @retval	Number of entered PIN digits if errno = 0
 * @retval	-1	Unable to complete request (check errno).
*/
/*SVC_PROTOTYPE*/
int ux100_pinGetNbCurrDigitEntered( void );

/**   
   @ingroup SVC_UX100_PIN_API

   @brief  Delete all or n digits of already entered PIN digits.
   
 *      
 *  @param[in] nbDigit number of digits to be deleted. 0 - Delete all digits, n - delete n digits
 * 
 * @retval	0	Command completed successfully.
 * @retval	-1	Unable to complete request (check errno).
*/
/*SVC_PROTOTYPE*/
int  ux100_pinDelete ( int nbDigit );  

/**   
   @ingroup SVC_UX100_PIN_API

   @brief  Abort a PIN entry process.
 * 
 * @retval	0	Command completed successfully.
 * @retval	-1	Unable to complete request (check errno).
*/
/*SVC_PROTOTYPE*/
int  ux100_pinAbort( void );    
 
/**   
   @ingroup SVC_UX100_PIN_API

   @brief  Send a PIN block to Vault.
 * 
 *
 * @retval	0	Command completed successfully.
 * @retval	-1	Unable to complete request (check errno).
*/
/*SVC_PROTOTYPE*/
int  ux100_pinSendEncPinBlockToVault( void );  

/**   
   @ingroup SVC_UX100_PIN_API

   @brief  Retrieve PIN mode infromation from Ux100
   
   @details It gives information about Ux100's PIN entry mode.
 *      
 * @retval	0	No PIN Mode
 * @retval	1	PIN Mode 
 * @retval	-1	Unable to complete request (check errno).
*/
/*SVC_PROTOTYPE*/
int  ux100_pinEntryMode( void );
                                     
/************************************************************************
                             PW API Part
 ***********************************************************************/
/** @defgroup SVC_UX100_PW_API Ux100 PW Session API */
 
/**   
   @ingroup SVC_UX100_PW_API

   @brief  Start a PW entry process by setting up minimum and maximum PW length and timeout.
   
   @details PW session must begin by this function to set up Ux100 under PW mode
 *      
 * @param[in] pwLenMin minimum pw length
 * @param[in] pwLenMax maximum pw length
 * @param[in] timeout  timeout for pw entry
 * 
 * @retval	0	Command completed successfully.
 * @retval	-1	Unable to complete request (check errno).
*/
/*SVC_PROTOTYPE*/
int  ux100_pwStartPwEntry( unsigned char  pwLenMin, unsigned char pwLenMax, int timeout );    

/**   
   @ingroup SVC_UX100_PW_API

   @brief  Get the number of entered PW digits.
   
 * 
 * @retval	Number of entered PW digits if errno = 0
 * @retval	-1	Unable to complete request (check errno).
*/
/*SVC_PROTOTYPE*/
int ux100_pwGetNbCurrDigitEntered( void );

/**   
   @ingroup SVC_UX100_PW_API

   @brief  Delete all or n digits of already entered PW digits.
   
 *      
 *  @param[in] nbDigit number of digits to be deleted. 0 - Delete all digits, n - delete n digits
 * 
 * @retval	0	Command completed successfully.
 * @retval	-1	Unable to complete request (check errno).
*/
/*SVC_PROTOTYPE*/
int  ux100_pwDelete ( int nbDigit );  

/**   
   @ingroup SVC_UX100_PW_API

   @brief  Abort a PW entry process.
 * 
 * @retval	0	Command completed successfully.
 * @retval	-1	Unable to complete request (check errno).
*/
/*SVC_PROTOTYPE*/
int  ux100_pwAbort( void );    
 
/**   
   @ingroup SVC_UX100_PW_API

   @brief  Send a PW block to Vault.
 * 
 *
 * @retval	0	Command completed successfully.
 * @retval	-1	Unable to complete request (check errno).
*/
/*SVC_PROTOTYPE*/
int  ux100_pwSendEncPwBlockToVault( void );  

/**   
   @ingroup SVC_UX100_PW_API

   @brief  Retrieve PW mode information from Ux100
   
   @details It gives information about Ux100's PW entry mode.
 *      
 * @retval	0	No PW/PIN Mode
 * @retval	1	PIN Mode
 * @retval	2	PW Mode 
 * @retval	-1	Unable to complete request (check errno).
*/
/*SVC_PROTOTYPE*/
int  ux100_pwEntryMode( void );

/************************************************************************
                             Buzzer API Part
 ***********************************************************************/
/** @defgroup SVC_UX100_BUZZER_API Ux100 Buzzer API */

/**   
   @ingroup SVC_UX100_BUZZER_API

   @brief Set buzzer beep 
 * 
 * @param[in] frequency Frequency of buzzer beep. 
 * @param[in] time Time (in milliseconds) to keep buzzzer beep.
 * @param[in] volume Volume for buzzzer beep.
 *
 * @retval	0	Command completed successfully.
 * @retval	-1	Unable to complete request (check errno).
*/
/*SVC_PROTOTYPE*/
int ux100_buzzerSetBeep( unsigned int frequency, unsigned int time, unsigned int volume );

/**   
   @ingroup SVC_UX100_BUZZER_API

   @brief Get buzzer beep info
 * 
 * @retval	struct ux100BuzzerInfoStruct buzzer beep information if errno = 0
 * @return  otherwise check errno
 * 
*/
/*SVC_PROTOTYPE*/
struct ux100BuzzerInfoStruct ux100_buzzerGetInfo( );

/** Set keyboard buzzer beep 
 *

 *
 * @return 0 upon successful request, or -1 if unable to complete request - Check Errno
 */
 
/**   
   @ingroup SVC_UX100_BUZZER_API

   @brief Set keyboard buzzer beep 
 * 
 * @param[in] frequency Frequency of keyboard buzzer beep. 
 * @param[in] time Time (in milliseconds) to keep keyboard buzzzer beep.
 * @param[in] volume Volume for keyboard buzzzer beep.
 *   
 * @retval	0	Command completed successfully.
 * @retval	-1	Unable to complete request (check errno).
 * 
*/
/*SVC_PROTOTYPE*/
int ux100_buzzerSetKeyboardBeep(unsigned int frequency, unsigned int time, unsigned int volume);

/**   
   @ingroup SVC_UX100_BUZZER_API

   @brief Get buzzer beep info
 * 
 * @retval	struct ux100BuzzerInfoStruct keyboard buzzer beep information if errno = 0
 * @return  otherwise check errno
 * 
*/
/*SVC_PROTOTYPE*/
struct ux100BuzzerInfoStruct ux100_buzzerGetKeyboardBeepInfo( );

/************************************************************************
                             Manufacturer API Part
 ***********************************************************************/
/** @defgroup SVC_UX100_MFG_API Ux100 Manufacturer API */

/**   
 * @ingroup SVC_UX100_MFG_API
 *
 * @brief Reset Anti-Removal Switch (non-secure) 
 * 
 * @details The function resets the Anti-Removal Switch without cryptographic challenge.
 *          The 'PGBOOT' firmware version is required to complete operation.
 *
 * @retval	0	Command completed successfully.
 * @retval	-1	Unable to complete request (check errno).
*/
/*SVC_PROTOTYPE*/
int ux100_mfgRemovalSwitchReset( void );

/**   
 * @ingroup SVC_UX100_MFG_API
 *
 * @brief Clear Ux100 security alarm 
 * 
 * @details The function detampers the Ux100 and regenerate the TRK.
 *          The 'PGBOOT' firmware version is required to complete operation.
 *
 * @retval	0	Command completed successfully.
 * @retval	-1	Unable to complete request (check errno).
*/
/*SVC_PROTOTYPE*/
int ux100_mfgClearTamper( void );


#ifdef __cplusplus
}
#endif

#endif

