/** @addtogroup ui UI */
/** @{ */
/** @addtogroup ui_led UI - LED: Services */
/** @{ */
/**
 *  @file svc_led.h
 *
 *	@brief LED's service.
 *
 *
 */

#ifndef SVC_LED_H
#define SVC_LED_H

#ifdef __cplusplus
extern "C" {
#endif

#include "svcmgrSvcDef.h"


/*SVC_SERVICE:led*/

/**
 * @name Led state on/off
 */
/** \{ */
#define LED_STATE_ON   1     /**< Led state to ON */
#define LED_STATE_OFF  0    /**< Led state to OFF */
/** \} */

/**
 * @name Leds sections (keypad, smartcard, msr)
 *
 * Predefined Led names:
 *
 * msr1 - MSR top LED
 *
 * msr2 - MSR middle LED
 *
 * msr3  - MSR bottom LED
 *
 * keypad  - Keypad LED
 *
 * smartcard - Smartcard LED
 *
 * To set multiple parameters, use a pipe between parameters, for exampe: leds = LED_MSR1|LED_MSR2|LED_MSR3
 */
/** \{ */
#define LED_SELECT_MSR1			(1<<0)    /**< Selection for first/top msr led */
#define LED_SELECT_MSR2			(1<<1)    /**< Selection for second/middle msr led */
#define LED_SELECT_MSR3			(1<<2)    /**< Selection for third/bottom msr led */
#define LED_SELECT_KEYPAD		(1<<3)    /**< Selection for keypad led */
#define LED_SELECT_SMARTCARD	(1<<4)    /**< Selection for smartcard led */
#define LED_SELECT_LOGO			(1<<5)    /**< Selection for logo (contactless) led */
#define LED_SELECT_CTLS1		(1<<6)    /**< Selection for first contactless led */
#define LED_SELECT_CTLS2		(1<<7)    /**< Selection for second contactless led */
#define LED_SELECT_CTLS3		(1<<8)    /**< Selection for third contactless led */
#define LED_SELECT_CTLS4		(1<<9)    /**< Selection for forth contactless led */
#define LED_SELECT_SYSTEM		(1<<10)   /**< Selection for system led */
/** \} */


/** Obtain the version of the LED service
 *
 * @return
 * Version struct defined in svcmgrSvcDef.h
 */
/*SVC_PROTOTYPE*/ struct version led_getVersion(void);


/** Initialization for LED show, clears all states that may be loaded
 *
 * @return
 * @li 0 Success
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li EINVAL - If the noted above combinations are not met
 * @li EACCES - Unable to access led interface
 *
 */
/*SVC_PROTOTYPE*/ int led_showInit(void);


/** Add a state to the show for the msr led's
 *
 * @param[in] msrState Contains ON/OFF state of msr1,2,3 LED's
 * @param[in] duration Time in msec for state to hold true
 *
 * @return
 * @li 0 Success
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li EINVAL - If the noted above combinations are not met
 * @li EACCES - Unable to access led interface
 *
 * @note
 * Examples:
 *     led_showAddState("100", 100); // turns on msr1 led for 100ms.
 *     led_showAddState("000", 150); // turns off msr1, msr2, and msr3 leds for 150ms.
 *     led_showAddState("111", 100); // turns on msr1, msr2, and msr3 leds for 100ms.
 */
/*SVC_PROTOTYPE*/ int led_showAddState(unsigned char msrState[3], int duration /*100*/);


/** Run the msr LED show
 *
 * Note: No fading is supported through led_show() (kept for backwards compatibility)
 *
 * @param[in] repeat Number of times to repeat (0 == infinite (until led_cancel()))
 *
 * @return
 * @li 0 Success
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li EBUSY - Show already running
 * @li EPERM - No states entered for show (see led_showAddState())
 * @li ECHILD - Unable to start show process
 *
 */
/*SVC_PROTOTYPE*/ int led_show(int repeat /*0*/);


/** Run the msr LED runway show
 *
 * Note: No fading is supported through led_showRunway()
 *
 * @param[in] repeat Number of times to repeat (0 == infinite (until led_cancel()))
 *
 * @return
 * @li 0 Success
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li EINVAL - If the noted above combinations are not met
 * @li EACCES - Unable to access led interface
 *
 */
/*SVC_PROTOTYPE*/ int led_showRunway(int repeat/*0*/);


/** Cancel the msr LED show or showRunway
 *
 * @return
 * @li 0 Success
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li EINVAL - If the noted above combinations are not met
 * @li EACCES - Unable to access led interface
 *
 */
/*SVC_PROTOTYPE*/ int led_showCancel(void);



/** Turn any/all LED's on or off
 *
 * Notes:
 *   - If led specified in ledSelect is actively blinking/fading, it will be stopped and set to specified state.
 *   - This method works for all leds (see led_getLedInfo(0))
 *
 * @param[in] ledSelect Any combination of leds to turn on/off (see defines for LED_INDEX_xxxx)
 * @param[in] state Defines the state of the led @see LED_STATE_ON @see LED_STATE_OFF
 *
 * @return
 * @li 0 Success
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li EINVAL - If the noted above combinations are not met
 * @li EACCES - Unable to access led interface
 *
 * @note
 * Example: status = LED_INDEX(LED_INDEX_MSR1|LED_INDEX_MSR2|LED_INDEX_MSR3, LED_STATE_ON).
 *
 */
/*SVC_PROTOTYPE*/ int led_changeState(int ledSelect, unsigned char state/*0*/);


/** Return the process ID for the LED service
 *
 * @return
 * Process ID
 *
 */
/*SVC_PROTOTYPE*/ int led_getPID(void);


/* ----------------------------------------------------------------- */
/** Below structure and methods may not be available on Mx platforms */
/* ----------------------------------------------------------------- */


/** \{ */
#define LED_HUE_WHITE		(1<<0)    /**< white led */
#define LED_HUE_RED			(1<<1)    /**< red led */
#define LED_HUE_GREEN		(1<<2)    /**< green led */
#define LED_HUE_BLUE		(1<<3)    /**< blue led */
#define LED_HUE_YELLOW		(1<<4)    /**< yellow led */
/** \} */


/*SVC_STRUCT*/
/**
 * Define an led information
 */
struct ledInfo_s {
	unsigned long int ledIndex; /**< led index for this led */
	char name[16];				/**< name identifier of led, eg. "msr1", "msr2", "msr3", "keypad", ... */
	int hue;					/**< what color the led is (LED_HUE_WHITE, LED_HUE_RED, LED_HUE_GREEN, LED_HUE_BLUE, LED_HUE_YELLOW) */
	int isPWM;					/**< non-zero if led is PWM (Pulse Width Modulated), brightness adjustable (fade supported) */
	int brightness;				/**< brightness level (0-off, max_brightness-full brightness) */
	int max_brightness; 		/**< max brightness level (set by OS) */
	int led_trigger;			/**< current led trigger (see LED_TRIGGER_XXXX defines) */
	char device[32]; 			/**< device name for led, eg. "pwm-white-msr1" - used by service, provided for reference */
};


/*SVC_STRUCT*/
/**
 * Define an collection of led information
 */
struct ledCollection_s {
	struct ledInfo_s *leds;     /**< led's, this will need to be free()'d when finished */
	int leds_count;             /**< Number of led's */
};


/*SVC_STRUCT*/
/**
 * Define an led name and hue
 */
struct ledNameHue_s {
	char name[16];				/**< name identifier of led, eg. "msr1", "msr2", "msr3", "keypad", ... */
	int hue;					/**< what color the led is (LED_HUE_WHITE, LED_HUE_RED, LED_HUE_GREEN, LED_HUE_BLUE, LED_HUE_YELLOW) */
};


/** Return the number of led's supported by this service
 *
 * @return
 * number of led's supported
 *
 * <b>Errno values</b>:
 * @li EACCES - Unable to access led interface
 * @li ECONNREFUSED - not supported on this platform
 *
 */
/*SVC_PROTOTYPE*/ int led_getCount(void);


/** Return struct containing info on selected leds (or all)
 *
 * @param[in] ledSelect Any combination of leds to get Info (0 == all)
 *
 * @return
 * ledCollection_s  containing ledInfo_s for specified led's
 *
 * <b>Errno values</b>:
 * @li ENOENT - If the ledSelect has no matching led in collection
 * @li EACCES - Unable to access led interface
 * @li ENOMEM - Internal malloc failed
 * @li ECONNREFUSED - not supported on this platform
 *
 * @note
 * @li If all of ledSelect is valid (example, specified 4 leds), then the returned array will contain 4 entries, starting with LSB of ledSelect).
 * @li The returned structure pointed to in struct ledCollection_s leds needs to be free()'d when finished using it.
 */
/*SVC_PROTOTYPE*/ struct ledCollection_s led_getLedInfo(unsigned long int ledSelect);



/** Get the name of led referenced by ledSelect
 *
 * @param[in] ledSelect led of interest. If more than one is referenced in ledSelect, first found is used.
 *
 * @return
 * @li errno != 0 if error
 * @li struct ledNameHue_s containing name of led and hue value. This is a pointer to a malloc()'d space, so it must be free()'d.
 * 
 * @Note
 * If the class name of led is for example: pwm-white-msr1, name is: msr1
 *
 * <b>Errno values</b>:
 * @li ENOENT - If the ledSelect has no matching led in collection
 * @li EACCES - Unable to access led interface
 * @li ECONNREFUSED - not supported on this platform
 */
/*SVC_PROTOTYPE*/ struct ledNameHue_s *led_getLedName(unsigned long int ledSelect);



/** Get index of LED based on name and hue
 *
 * @param[in] nameHue.name of led to set brightness
 * @param[in] nameHue.hue defines which led named "name" that has the specified hue (LED_HUE_GREEN for green system led for example)
 *
 * @return
 * @li >= 0 Success - ledIndex for specified LED
 * @li = 0 Error, errno != 0 (see errno for error cause)
 *
 * <b>Errno values</b>:
 * @li EINVAL - If name or hue invalid
 * @li ENOENT - If the ledSelect has no matching led in collection
 */
/*SVC_PROTOTYPE*/ unsigned long int led_getLedIndexNameHue(struct ledNameHue_s nameHue);



/** \{ */
#define LED_MAX_BRIGHTNESS	255
#define LED_OFF_BRIGHTNESS	0
/** \} */

/** Set brightness of any/all LED's
 *
 * @param[in] ledSelect Any combination of leds to set brightness (see defines for LED_SELECT_xxxx)
 * @param[in] level Defines the brightness percent level of the led (0 (off) to max_brightness (see struct ledInfo_s))
 *
 * Notes:
 *   1. level > max_brightness will set brightness level to max_brightness (ie, will not return/cause error)
 *   2. brightness can be set (and led will light) without defining/setting a trigger. This produces a solid (non-pulsing) led
 *
 * @return
 * @li 0 Success
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li EINVAL - If level < 0
 * @li ENOENT - If the ledSelect has no matching led in collection
 * @li EACCES - Unable to access led interface
 * @li ECONNREFUSED - not supported on this platform
 */
/*SVC_PROTOTYPE*/ int led_setBrightness(unsigned long int ledSelect, int level/*0*/);



/** Get brightness of LED
 *
 * @param[in] ledSelect led of interest. If more than one is referenced in ledSelect, first found is used.
 *
 *
 * @return
 * @li >= 0 Brightness level
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li ENOENT - If the ledSelect has no matching led in collection
 * @li EACCES - Unable to access led interface
 * @li ECONNREFUSED - not supported on this platform
 */
/*SVC_PROTOTYPE*/ int led_getBrightness(unsigned long int ledSelect);



/** Get Max brightness of LED
 *
 * @param[in] ledSelect led of interest. If more than one is referenced in ledSelect, first found is used.
 *
 *
 * @return
 * @li >= 0 Max brightness level
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li ENOENT - If the ledSelect has no matching led in collection
 * @li EACCES - Unable to access led interface
 * @li ECONNREFUSED - not supported on this platform
 */
/*SVC_PROTOTYPE*/ int led_getMaxBrightness(unsigned long int ledSelect);



/** Set brightness of LED's based on name and hue
 *
 * @param[in] nameHue.name of led to set brightness
 * @param[in] nameHue.hue defines which led named "name" that has the specified hue (LED_HUE_GREEN for green system led for example)
 * @param[in] level Defines the brightness percent level of the led (0 (off) to max_brightness (see struct ledInfo_s))
 *
 * Notes:
 *   1. level > max_brightness will set brightness level to max_brightness (ie, will not return/cause error)
 *   2. brightness can be set (and led will light) without defining/setting a trigger. This produces a solid (non-pulsing) led
 *
 * @return
 * @li 0 Success
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li EINVAL - If level < 0 and/or name, hue invalid
 * @li ENOENT - If the ledSelect has no matching led in collection
 * @li EACCES - Unable to access led interface
 * @li ECONNREFUSED - not supported on this platform
 */
/*SVC_PROTOTYPE*/ int led_setBrightnessNameHue(struct ledNameHue_s nameHue, int level/*0*/);


/** \{ */
#define LED_TRIGGER_NONE		(0) 	/**< no trigger - led off (previous trigger disabled) */
#define LED_TRIGGER_PULSE		(1) 	/**< pulse - pulse led one or more times */
#define LED_TRIGGER_ONESHOT		(2) 	/**< oneshot - pulse led one time only at full brightness */
#define LED_TRIGGER_TIMER		(3) 	/**< timer - create a "clock" pulse sequence - run until stopped */
#define LED_TRIGGER_DEFAULT_ON	(4) 	/**< default-on - led on steady at power-up */
#define LED_TRIGGER_CPU			(5)		/**< cpu - shows cpu activity */
#define LED_TRIGGER_MAX   		LED_TRIGGER_CPU
/** \} */


/** \{ */
#define LED_FADE_NONE		(0)			/**< no fade */
#define LED_FADE_IN			(1<<0)		/**< fade in (gradual brightness) when going from LED_OFF to LED_FULL */
#define LED_FADE_OUT		(1<<1)		/**< fade out (gradual lightness) when going from LED_FULL to LED_OFF */
/** \} */


/*SVC_STRUCT*/
/**
 * Define an led trigger configuration
 */
struct ledTrigger_s {
  int led_trigger /* 0 */;		/**< type of trigger (see LED_TRIGGER_xxx defines above) */
  int delay_on /* 0 */;			/**< delay on in ms (<100, will be set to 500ms (default) */
  int delay_off /* 0 */;		/**< delay off in ms (<100, will be set to 500ms (default) */
  int brightness /* 255 */;		/**< how bright the led will be during operation (10 to 255) */
  int invert /* 0 */;			/**< invert led behavior (0= led normally off, !=0 led normally on) */
  int fade /* 0 */;				/**< fade select (may be OR'd together, eg. fade = LED_FADE_IN | LED_FADE_OUT; ) */
  int fade_speed /* 0 */;		/**< fade speed 0 to 9 (0= slowest, 9= fastest) */
  int option1 /* 0 */;			/**< future parameter, must be set to 0 */
  int option2 /* 0 */;			/**< future parameter, must be set to 0 */
  int option3 /* 0 */;			/**< future parameter, must be set to 0 */
};


/** Set the trigger for the specified led(s)
 *
 * @param[in] ledSelect Any combination of leds to set trigger
 * @param[in] ledTrigger structure containing type of trigger and addition parameters
 *
 * 	Notes:
 *     - If fade is specified for non-pwm controlled led, it will be ignored (fade not supported)
 *     - fade is only supported for oneshot and timer trigger types, will be ignored for all others
 *     - fade, fade_speed, fade_max_brightness are only used for trigger: LED_TRIGGER_PULSE
 *     - invert applies to default state of led (and what state led will be in when trigger completes)
 *
 * @return
 * @li 0 Success
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li EINVAL - If ledSelect == 0, or any of the parameters are invalid
 * @li ENOENT - If the ledSelect has no matching led in collection
 * @li EACCES - Unable to access led interface
 * @li ECONNREFUSED - not supported on this platform
 *
 */
/*SVC_PROTOTYPE*/ int led_setTrigger(unsigned long int ledSelect, struct ledTrigger_s ledTrigger);


/** Cause a pulse or sequence of pulses to occur for the specified led(s)
 *
 * @param[in] ledSelect - any combination of leds to set/trigger pulse
 * @param[in] count - how many pulses to generate (0=infinity) not used for trigger LED_TRIGGER_ONESHOT
 *
 * 	Notes:
 *     - Has affect on led(s) only if trigger previously set to LED_TRIGGER_PULSE or LED_TRIGGER_ONESHOT
 *     - For LED_TRIGGER_ONESHOT, count parameter is ignored, treated as value of 1 (thus the name oneshot)
 *
 * @return
 * @li 0 Success
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li EINVAL - If ledSelect == 0, or any of the parameters are invalid
 * @li ENOENT - If the ledSelect has no matching led in collection
 * @li EACCES - Unable to access led interface
 * @li ECONNREFUSED - not supported on this platform
 *
 */
/*SVC_PROTOTYPE*/ int led_pulse(unsigned long int ledSelect, int count);


#ifdef __cplusplus
}
#endif
#endif //SVC_LED_H

/// @}*/
/// @}*/
