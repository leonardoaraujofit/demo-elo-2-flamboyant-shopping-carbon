/** @addtogroup comms Comms */
/** @{ */
/** @addtogroup bluetoothservice Comms - Bluetooth: Services */
/** @{ */
/** 
 * @file svc_bluetooth.h 
 *	
 * @brief Bluetooth service
 *
 * This header file contains information about Bluetooth service.
 */


   /*
  * VeriFone, Inc.
  */


#ifndef SVC_BLUETOOTH_H
#define SVC_BLUETOOTH_H

#include "svcmgrSvcDef.h" /*This header file is defined and maintained by VeriFone to provide necessary
                            infrastructure components. The file must be #included in the Service Interface
                            header file as it contains the definition for struct version, which is returned by
                            the {service}_getVersion( ) function required by every Service.
                            svcmgrSvcDef.h also contains structures to support Pairlists.*/

/*SVC_SERVICE:bluetooth*/

#define BT_PWR_DOWN   	0     	/**< Bluetooth power down 	*/
#define BT_PWR_UP  		1    	/**< Bluetooth power up 	*/
#define BT_TRY_SUSPEND	2	/**< Bluetooth try to suspend controller 	*/
#define BT_TRY_RESUME	3	/**< Bluetooth try to resume controller 	*/


#define BT_MIN_CHANNEL 	1		/**< Minimum Bluetooth channel*/
#define BT_MAX_CHANNEL 	30		/**< Maximum Bluetooth channel*/
#define BT_MAX_DEVICE	99		/**< Maximum Bluetooth device*/
#define PAN_MIN_TIMEOUT 1       /**< Minimum Bluetooth PAN Ping timeout value*/
#define PAN_MAX_TIMEOUT 99999   /**< Maximum Bluetooth PAN Ping timeout value*/

#define BT_HOST_CONFIG_PAIRING_METHOD_JUST_WORKS			0
#define BT_HOST_CONFIG_PAIRING_METHOD_PASSKEY_ENTRY			1
#define BT_HOST_CONFIG_PAIRING_METHOD_NUMERIC_COMPARISON	2

#define BT_HOST_CONFIG_SERVICES_MASK_OBEX	1
#define BT_HOST_CONFIG_SERVICES_MASK_PAN	2
#define BT_HOST_CONFIG_SERVICES_MASK_DUN	4
#define BT_HOST_CONFIG_SERVICES_MASK_SPP	8

#ifndef TRUE
#define TRUE 			1
#endif
#ifndef FALSE
#define FALSE			0
#endif

#define DUN_BT_CHANNEL  1     	/**< Bluetooth Channel for DUN connections */

#define MAX_DATA_LENGTH 127		/**< Maximum length of packet for sending an obex file*/

/*SVC_STRUCT*/
/**
 * Bluetooth info structure
 */
struct btDevice{
	char 	name[256];			/**< Bluetooth device name */
	char 	address[18];		/**< Bluetooth device address. Format: xx:xx:xx:xx:xx:xx */
	char	btDeviceClass[3];	/**< Bluetooth device class */
};


/*SVC_STRUCT*/
/**
 * Return a list of bluetooth devices found during a scan
 */
struct btDeviceList{
	int deviceCount;			/**< Number of devices found */
	struct btDevice *device;	/**< Device detail */
};


/*SVC_STRUCT*/
/**
 *  Local Bluetooth configuration structure
 */
struct btConf{
	char name[128];			/**< Bluetooth device name */
    char address[18];		/**< Bluetooth device address. Format: xx:xx:xx:xx:xx:xx */
    unsigned char discoverable;		/**< Is Bluetooth discoverable ? */
};

/*SVC_STRUCT*/
/**
 * Bluetooth supported profiles
 */
struct btSupport {
	unsigned char available;	/**< Bluetooth HW availability */
	unsigned char ibeacon;		/**< iBeacon support */
	unsigned char classic;		/**< BT classic profiles support */
	unsigned char ble;			/**< BLE support */
};

/*SVC_STRUCT*/
/**
 *  Remote Bluetooth device configuration structure.
 */
struct btLinkDev{
    char address[18];				/**< Bluetooth device address. Format: xx:xx:xx:xx:xx:xx */
	unsigned char link_key[16];		/**< Link Key for the device. Obtained usually through pairing */	
	unsigned char key_type;			/**< Used by bluez to decide the auth procedure */
	unsigned char pin_len;			/**< Length of PIN used in case of legacy pairing */
};

/*SVC_STRUCT*/
/**
 * List of remote deivces data. Returned by get_link_key API
 */
struct btLinkDevList{
	struct btLinkDev *device;
	int device_count;
};

/*SVC_STRUCT*/
struct bluetoothHostConfig {
	char local_address[6];
	char local_name[249];
	char class_of_device[3];
	int pairing_method;
	int services_mask;
	int discoverable;
};

/*SVC_STRUCT*/
/**
 * Define beacon value that is going to be advertised by: int bluetooth_ibeaconStartAdvertise() function
 */
struct btIBeacon
{
	char uuid[16];  /**< Service UUID */
	char major[2];  /**< Service major ID */
	char minor[2];  /**< Service minor ID */
};

/** Obtain the version of the BLUETOOTH service
 *
 * @return Version struct defined in svcmgrSvcDef.h
 */
/*SVC_PROTOTYPE*/ struct version bluetooth_getVersion(void);

/** Switch Bluetooth power
 *
 * @param[in] operation Power switching operation: BT_PWR_UP | BT_PWR_DOWN | BT_TRY_SUSPEND | BT_TRY_RESUME
 *
 * @return 
 * @li 0 on success
 * @li  -1 on error
 * 
 * <b>Errno values</b>:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINVAL Invalid arg
 * @li EACCES Permission denied
 * @li EBUSY Unable to resume
 *
 * @note
 * @li BT_PWR_ON will do nothing in case that BT is already up (return value is still 0).
 */
/*SVC_PROTOTYPE*/ int bluetooth_power(int operation);

/** Bluetooth power status
 * 
 * @return 
 * @li 0 if Bluetooth is down
 * @li 1 if power is up and -1 on error
 * 
 * <b>Errno values</b>:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EBUSY In suspned mode
 */
/*SVC_PROTOTYPE*/ int bluetooth_isUp(void);

/** Get Bluetooth current configuration
 *
 * @return On success struct btConf - a bluetooth configuration structure. On failure, an empty structure + errno:
 *
 * <b>Errno values</b>:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EHOSTDOWN bluetooth is down
 */
/*SVC_PROTOTYPE*/ struct btConf bluetooth_getConfig(void);

/** Get Bluetooth supported profiles
 *
 * @return On success struct btSupport - supported bluetooth profiles. On failure, an empty structure + errno:
 * <b>Errno values</b>:
 * @li ENOENT Bluetooth SW not found
 */
/*SVC_PROTOTYPE*/struct btSupport bluetooth_getSupport(void);

/** Start scanning nearby discoverable Bluetooth devices
 *
 * @return 
 * @li 0 if scannig was stared successfully
 * @li -1 on error
 * 
 * <b>Errno values</b>:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not savailable
 * @li EHOSTDOWN bluetooth is down
 * @li EAGAIN The system lacked the necessary resources
 */
/*SVC_PROTOTYPE*/ int bluetooth_scan(void);

/** Get last scan results
 * 
 * @return 
 * On success struct btDeviceList for the nearby devices. On failure, an empty structure + errno:
 * 
 * <b>Errno values</b>:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINPROGRESS Scannig is still in progress
 * @li EIO Communication error with Bluetooth device
 */
/*SVC_PROTOTYPE*/ struct btDeviceList bluetooth_scanResults(void);

/** Connect to HID device
 * 
 * @return 
 * On success struct btDeviceList for the nearby devices. On failure, an empty structure + errno:
 * 
 * <b>Errno values</b>:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINVAL Invalid Bluetooth address
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with Bluetooth device
 */
/*SVC_PROTOTYPE*/ int bluetooth_hidConnect(char *remoteDeviceAddr);

/** Disonnect from HID device
 * 
 * @return 
 * On success struct btDeviceList for the nearby devices. On failure, an empty structure + errno:
 * 
 * <b>Errno values</b>:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINVAL Invalid Bluetooth address
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with Bluetooth device
 */
/*SVC_PROTOTYPE*/ int bluetooth_hidDisconnect(char *remoteDeviceAddr);

/** Get connected HIDs list
 * 
 * @return On success struct btDeviceList for the nearby devices. On failure, an empty structure + errno:
 * 
 * <b>Errno values</b>:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EIO Communication error with Bluetooth device
 * @li ENOMEM Out of memory
 * 
 * @note
 * @li There is no class information in btDevice struct
 */
/*SVC_PROTOTYPE*/ struct btDeviceList bluetooth_getConnectedHids(void);

/**
 * Get a list of linked devices.
 * 
 * param[in] raddr  BT addr of remote device we want the data of
 * 
 * @return 
 * btLinkDevList on success or empty structure plus errno on error
 *
 * <b>Errno values</b>:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINVAL Invalid arg
 * @li EACCES BT is up or user is not root
 * @li ENODATA Key storage file is corrupted
 *
 * @note
 * @li if argument is null a list of all the linked BT devices is reutrned
 */
/*SVC_PROTOTYPE*/ struct btLinkDevList bluetooth_get_link_devs(char* raddr /*NULL*/);

/**
 * Restore link data for devices received as argument
 * 
 * @return 
 * @li 0 on success, if all devices in the list have been succesfully restored
 * @li -1 plus errno on failure
 *
 * <b>Errno values</b>:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINVAL Invalid arg
 * @li EACCES BT is up or user is not root
 * @li ENODATA Key storage file is corrupted
 *
 * @note
 * @li Can be called only when BT is down
 * @li Can be called only by root
 */
/*SVC_PROTOTYPE*/ int bluetooth_restore_link_devs(struct btLinkDevList devs);

/**
 * Configure device as host
 *
 * @return
 * @li 0 on success
 * @li -1 plus errno on failure
 *
 * <b>Errno values</b>:
 * @li ENOENT Not supported
 * @li ENODEV Bluetooth not available
 * @li ENOEXEC Unable to configure device as host
 * @li EINVAL Invalid parameter
 *
 * @note
 * @li Bluetooth has to be UP
 */
/*SVC_PROTOTYPE*/ int bluetooth_set_host_config(struct bluetoothHostConfig bhc);

/**
 * Start iBeacon advertisement
 *
 * @return
 * @li 0 on success, if advertisement started successfully
 * @li -1 plus errno on failure
 *
 * <b>Errno values</b>:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINPROGRESS If advertising in progress
 * @li ENOEXEC Failed to start advertising
 * @li EINVAL Invalid arg
 *
 * @note
 * @li Bluetooth has to be UP
 */
/*SVC_PROTOTYPE*/ int bluetooth_ibeacon_start_advertise( struct btIBeacon );

/**
 * Return the current iBeacon value which is currently advertised if any
 *
 * @return
 * @li Advertisement data or empty structure if no advertisement
 *
 * <b>Errno values</b>:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINVAL Invalid arg
 *
 */
/*SVC_PROTOTYPE*/ struct btIBeacon bluetooth_ibeacon_get_info(void);

/** Stop IBeacon advertising */
/**
 * Stop iBeacon advertising
 *
 * @return
 * @li 0 on success,
 * @li -1 on failure plus errno
 *
 * <b>Errno values</b>:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINVAL Invalid arg
 *
 */
/*SVC_PROTOTYPE*/ int bluetooth_ibeacon_stop_advertise();

/** Switch Bluetooth radio on/off
 *
 * @param[in] operation Power switching operation: BT_PWR_UP | BT_PWR_DOWN
 *
 * @return 
 * @li 0 on success
 * @li  -1 on error
 * 
 * @note
 * @li BT_PWR_ON will do nothing in case that BT is already up (return value is still 0).
 */
/*SVC_PROTOTYPE*/ int bluetooth_setRadioPower(int operation );

/** Bluetooth radio power status
 * 
 * @return 
 * @li BT_PWR_DOWN if Bluetooth is down
 * @li BT_PWR_UP if power is up and -1 on error
 * 
 */
/*SVC_PROTOTYPE*/ int bluetooth_getRadioPower( void );

#include "svc_bluetooth_api.h"
#include "svc_bluetooth_errno.h"

#endif //SVC_BLUETOOTH_H
///@}
///@}
