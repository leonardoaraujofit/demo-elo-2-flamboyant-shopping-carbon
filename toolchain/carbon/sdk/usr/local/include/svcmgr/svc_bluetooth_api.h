#ifndef SVC_BLUETOOTH_API_H
#define SVC_BLUETOOTH_API_H

#define SVC_BT_SEARCH_OBEX     0x01 /* 0000 0001 OBEX Profile search mask */
#define SVC_BT_SEARCH_PAN      0x02 /* 0000 0010 PAN Profile search mask */
#define SVC_BT_SEARCH_DUN      0x04 /* 0000 0100 DUN Profile search mask */
#define SVC_BT_SEARCH_SP       0x08 /* 0000 1000 Serial Port Profile search mask */
#define SVC_BT_SMAX_RESP        10 /* maximum returned devices number */
#define SVC_BT_SEARCH_TIME      8
#define SVC_BT_INQUIRY_DURATION 8
#define SVC_BT_MAXTIMEOUT       6500

#define SVC_BT_ISCAN_CONFIRM          1
#define SVC_BT_ISCAN_REJECT           0
#define SVC_BT_ISCAN_DEFAULT_TIMEOUT  120

#include "svc_bluetooth_hciconfig.h"

#ifdef __cplusplus
extern "C" {
#endif

/** Pair with BT device
 * @return 0 on success if pairing was successful, or errno for failure. errno:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li ESHUTDOWN Bluetooth is down
 * @li EINVAL Invalid parameters
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with Bluetooth device
 * @li EALPAIR The Bluetooth device is already paired
 * @li ENOMEM Allocation failed
 */
/*SVC_PROTOTYPE*/ int bluetooth_pair(char *pin,char *remoteDeviceAddr);

/** Check if paired with BT device
 * @return 1 on success if devices are paired, or 0 with errno for failure/devices are not paired. errno:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li ESHUTDOWN Bluetooth is down
 * @li EINVAL Invalid parameters
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with Bluetooth device
 * @li ENOMEM Allocation failed
 */
/*SVC_PROTOTYPE*/ int bluetooth_ispaired(char *remoteDeviceAddr);

/** UnPair with BT device
 * @return 0 on success if unpairing was successful, or errno for failure. errno:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li ESHUTDOWN Bluetooth is down
 * @li EINVAL Invalid parameters
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with Bluetooth device
 * @li EUNPAIR The Bluetooth device is not paired with the remote device
 * @li ENOMEM Allocation failed
 */
/*SVC_PROTOTYPE*/ int bluetooth_unpair(char *remoteDeviceAddr);

/*SVC_STRUCT*/
/**
 * Bluetooth paired info structure
 */
struct btPaired{
	char 	name[256];        /**< Bluetooth device name */
	char 	address[18];		/**< Bluetooth device address. Format: xx:xx:xx:xx:xx:xx */
};

/*SVC_STRUCT*/
/**
 * Return a list of paired devices
 */
struct btPairedDeviceList{
	int device_count;				   /**< Number of devices found */
	struct btPaired *device;		/**< All devices */
};

/** Retrieve a list of paired devices
 * @return on success struct btPairedDeviceList for the requested devices. On failure, an empty structure + errno:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li ECOMM Communication error with Bluetooth daemon
 */
/*SVC_PROTOTYPE*/ struct btPairedDeviceList bluetooth_pairedList(void);

/*SVC_STRUCT*/
/**
 * Bluetooth hciconfig parameters structure
 */
struct btHciConf{
	char ** args;			/**< Array of string arguments */
	int args_count;		/**< Number of arguments  */
};

/** HCI device configuration 
 * @return struct btHciInfo on success if HCI device configuration was successful. On failure empty structure + errno:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINVAL Invalid Bluetooth address
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with Bluetooth device
 * @li ENOMEM Allocation failed
 * @li ENOSYS Function not implemented
 */
/*SVC_PROTOTYPE*/ struct btHciInfo bluetooth_hciconfig(struct btHciConf params);

/** Obex Send File Interface
 * @return 0 on success if pairing was successful, or errno for failure. errno:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINVAL Invalid Bluetooth address
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with Bluetooth device
 * @li ENOMEM Allocation failed
 * @li ENAMETOOLONG Absolute path to obex file to send is too long
 */
/*SVC_PROTOTYPE*/ int bluetooth_obex_send(char *remoteDeviceAddr,int channel,char *file);

/** PAN User Interface connection
 * @return struct btPannConn if PAN connection was successful, or empty structure plus errno for failure. errno:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINVAL Invalid Bluetooth address
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with Bluetooth device
 * @li ENOMEM Allocation failed
 * @li EDACON Device already connected
 * @li EPANCUN PAN connection unsucessfull
 * @li EUNPAIR Devices are not paired
 */
/*SVC_PROTOTYPE*/ struct btPanConn bluetooth_pan_connect(char *napBTAddr);

/** PAN User Interface disconnection
 * @return 0 on success if the disconnection was successful, or -1 for failure with errno:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINVAL Invalid Bluetooth address
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with Bluetooth device
 * @li ENOMEM Allocation failed
 * @li EPANCNK PAN connection not killed
 * @li EBNEP0NE bnep0 interface does not exist
 */
/*SVC_PROTOTYPE*/ int bluetooth_pan_disconnect(char *napBTAddr);

/** ICMP ECHO_REQUEST packets to a network hosts through the specific bnep interface
 * @returns 0 on success if at least one response has been received from the specified host;
 * @returns -1 for failure or host unreachable with errno:
 * @li ENODEV Bluetooth not available
 * @li EINVAL Invalid Bluetooth address
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with Bluetooth device
 * @li ENOMEM Allocation failed
 * @li ETIMEDOUT Ping has timed out
 */
/*SVC_PROTOTYPE*/ int bluetooth_pan_ping(char *napBTAdd, char *host, int timeout);

/** DUN User Interface connection
 * @return 0 on success if the connection was successful, or errno for failure. errno:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINVAL Invalid parameters
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with Bluetooth device
 * @li ENOMEM Allocation failed
 * @li EUNPAIR Devices are not paired
 * @li ESPPCON Error in SPP connection
 * @li EEXIST Modem device already exists
 * @li ETTYFAILURE Failure in creating /dev/ttyModem
 */
/*SVC_PROTOTYPE*/ int bluetooth_dun_connect(char *remoteDeviceAddr);

/** DUN User Interface disconnection
 * @return 0 on success if the disconnection was successful, or errno for failure. errno:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINVAL Iinvalid parameters
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with Bluetooth device
 * @li ENOMEM Allocation failed
 * @li ETTYUNLINK Error in unlinking /dev/ttyModem
 * @li ESPPDISCON Error in disconnecting SPP
 */
/*SVC_PROTOTYPE*/ int bluetooth_dun_disconnect(char *remoteDeviceAddr);

/** SPP User Interface connection
 * @return the minor of the rfcomm device created if the connection was successful, or errno for failure. errno:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINVAL Invalid parameters
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with Bluetooth device
 * @li ENOMEM Allocation failed
 * @li EACCES Permission denied
 * @li EBUSY Device or resource busy
 * @li EHOSTDOWN Host is down
 * @li ECONNREFUSED Connection refused
 * @li EPIPE Broken pipe
 * @li ECHILD Could not start connection in background
 * @li ETIMEDOUT Connection attempt has timed out
 * @li ENOEXEC Connection script call failed
 */
/*SVC_PROTOTYPE*/ int bluetooth_spp_connect(char *remoteDeviceAddr, int channel);

/** SPP User Interface disconnection
 * @return 0 on success if the disconnection was successful, or errno for failure. errno:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINVAL Invalid parameters
 * @li ENOMEM Memory management error
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with service manager server
 * @li ENOTCONN No spp connection for the given address-channel pair
 */
/*SVC_PROTOTYPE*/ int bluetooth_spp_disconnect(char *remoteDeviceAddr, int channel);

/** PAN User Interface connection status
 * @return struct btPanConn if PAN connection exists, or empty structure plus errno for failure. errno:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINVAL Invalid Bluetooth address
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with Bluetooth device
 * @li ENOMEM Allocation failed
 * @li ENCON Not connected 
 */
/*SVC_PROTOTYPE*/ struct btPanConn bluetooth_is_pan_connected(char *napBTAddr);

/** PAN User Interface connection status
 * @return struct rfcomm device minor if connection exists, -1 for not connected, plus errno on failure:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINVAL Invalid Bluetooth address
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with Bluetooth device
 * @li ENOMEM Allocation failed
 */
/*SVC_PROTOTYPE*/ int bluetooth_is_spp_connected(char *btAddr, int channel);

/** DUN User Interface connection status
 * @return 1 on success if the connection is up, 0 if the connection is down and -1 on failure with errno:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EINVAL Invalid Bluetooth address
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with Bluetooth device
 * @li ENOMEM Allocation failed
 * @li ENCON Not connected 
 */
/*SVC_PROTOTYPE*/ int bluetooth_is_dun_connected(char *remoteDeviceAddr);

/*SVC_STRUCT*/
/**
 * Bluetooth browse info structure
 */
 /* TODO: change int into char if needed */
 struct sdpBrowse{
	char 	name[256];			/**< Bluetooth device name */
	char 	address[18];		/**< Bluetooth device address. Format: xx:xx:xx:xx:xx:xx */
	unsigned char pan_profile;		/**< 1 if PAN profile, 0 otherwise */
	unsigned char dun_profile;		/**< 1 if DUN profile, 0 otherwise */
	unsigned char obex_profile;		/**< 1 if OBEX profile, 0 otherwise */
	unsigned char  sp_profile;		/**< 1 if SPP profile, 0 otherwise */
	unsigned char  dun_channel;		/**< RFCOMM Channel number for DUN profile */
	unsigned char  obex_channel;		/**< RFCOMM Channel number for OBEX profile */
	unsigned char  sp_channel;		/**< Channel number for SP profile */
};

/*SVC_STRUCT*/
/**
 * Return a list of bluetooth devices found during browse
 */
struct sdpBrowseDeviceList{
	int device_count;			/**< Number of devices found */
	struct sdpBrowse *device;		/**< All devices */
};

/*SVC_STRUCT*/
/**
 *  PAN connection 
 */
struct btPanConn{
	char bnepIface[16];	//**< bnep interface: bnep0, bnep1 etc.
	char address[18];		//**< Bluetooth address
	unsigned char minor;	//**< bnep ingerface index 
};

/*SVC_STRUCT*/
/**
 *  List of PAN connected devices
 */
struct btPanConnDeviceList{
	int device_count;
	struct btPanConn *device;
};


/*SVC_STRUCT*/
/**
 * structure containing Bluetooth information for inrange  Bluetooth devices
 */
struct btRemoteDeviceInfo{
	int  			rssi;		/**< rssi */
	struct sdpBrowse 	dev_info;	/**< name, address and profile information*/
};


/*SVC_STRUCT*/
/**
 *  list containing btRemoteDeviceInfo structs
 */
struct btRemoteDeviceInfoList{
	int device_count;				/**< Number of devices found */
	struct btRemoteDeviceInfo *device;	/**< Device detail */
};

/** SDP Start browsing process for Bluetooth devices
 * @return 0 on success if browsing was started successfully, or errno. errno:
 * @li EINVAL Invalid parameters
 * @li EAGAIN The system lacked the necessary resources
 * @li ENODEV Bluetooth not available
 * @li ENOENT Bluetooth SW not found
 * @li EHOSTDOWN Bluetooth is down
 * @li EINPROGRESS Browsing is still in progress
 * Notes:
 * @li If the device is not paired, the function might not return the available profiles.
 */
/*SVC_PROTOTYPE*/ int bluetooth_sdp_browse(char *deviceAddress , unsigned char profile, int max_devices, int inquiry_time);

/** SDP Retrieves the information obtained after the browsing mechanism is finished
 * @return on success struct sdpBrowseDeviceList for the requested devices. On failure, an empty structure + errno:
 * @li ENODEV Bluetooth not available
 * @li ENOENT Bluetooth SW not found
 * @li EHOSTDOWN Bluetooth is down
 * @li EAGAIN The system lacked the necessary resources
 * @li EINPROGRESS Browsing is still in progress
 * @li ECOMM SDP connection failed. Only when address was specified
 */
/*SVC_PROTOTYPE*/ struct sdpBrowseDeviceList bluetooth_sdp_browse_results(void);

/** Returns a list with the existing PAN connections
 * @return on success struct btPanConnDeviceList filled up with PAN connected devices. On failure, an empty structure + errno:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li ECOMM Communication error 
 */
/*SVC_PROTOTYPE*/ struct btPanConnDeviceList bluetooth_list_pan_connected(void);

/** Display connection RSSI. The higher the RSSI number, the stronger the signal.
 * @return RSSI value on success, or EFAI and errno. errno:
 * @li EINVAL Invalid parameters
 * @li ENOENT Bluetooth SW not found
 * @li ENCON Not connected
 * @li ENODEV Bluetooth not available
 * @li ENOMEM Allocation failed
 * @li EUNPAIR The Bluetooth device is not paired with the remote device
 * @li ENOTCONN Endpoint is not connected
 * @li ECONNREFUSED Connection refused
 * @li EHOSTDOWN No data available
 * Notes:
 * @li Prerequisite: Requires previous pairing.
 */
/*SVC_PROTOTYPE*/ int bluetooth_rssi(char *remoteDeviceAddr);

/** Display connection Link quality.
 * @return link quality value on success, or -1 and errno. errno:
 * @li EINVAL Invalid parameters
 * @li ENOENT Bluetooth SW not found
 * @li ENCON Not connected
 * @li ENODEV Bluetooth not available
 * @li ENOMEM Allocation failed
 * @li EUNPAIR The Bluetooth device is not paired with the remote device
 * @li ENOTCONN Endpoint is not connected
 * @li ECONNREFUSED Connection refused
 * @li EHOSTDOWN No data available
 * Notes:
 * @li Prerequisite: Requires previous pairing. 
 */
/*SVC_PROTOTYPE*/ int bluetooth_lq(char *remoteDeviceAddr);

/** Enable VFI profile.
 * @return 0 for success, or -1 and errno. errno:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EIO failed to enable VFI profile
 */
/*SVC_PROTOTYPE*/ int bluetooth_vfi_enable();

/** Disable VFI profile.
 * @return 0 for success, or -1 and errno. errno:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li EIO failed to disable VFI profile
 */
/*SVC_PROTOTYPE*/ int bluetooth_vfi_disable();

/** Check eth cable status for remote device
 * @return string containing Access Point status on success, NULL on error, check errno: 
 * errno:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li ENOMEM Allocation failed
 * @li EUNPAIR The Bluetooth device is not paired with the remote device
 * @li EIO failed to get VFI info
 * Notes:
 * @li Prerequisite: Requires pairing
 */
/*SVC_PROTOTYPE*/ char* bluetooth_vfi_apstatus(char *remoteDeviceAddr);

/** Enable inquiry scan (Trident only)
 * @param[in] timeout in second during which inquiry scan will be activated (betwen 30s and 600s), 
 * @return 0 if inquiry scan was successfully activated, -1 on error, check errno:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li ESHUTDOWN Bluetooth is down
 * @li EINVAL Invalid parameters
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with Bluetooth device
 * @li ENOMEM Allocation failed
 */
/*SVC_PROTOTYPE*/ int bluetooth_discoveryEnable(int timeout);

/** Disable inquiry scan (Trident only)
 * @return 0 if inquiry scan was successfully stopped, -1 on error, check errno:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li ESHUTDOWN Bluetooth is down
 * @li EINVAL Invalid parameters
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with Bluetooth device
 * @li ENOMEM Allocation failed
 */
/*SVC_PROTOTYPE*/ int bluetooth_discoveryDisable();

/*SVC_STRUCT*/
/**
 *   Pairing request information
 */
struct btPairingRequest{
	int type;				//**< Pairing type
	char address[18];		//**< Remote Bluetooth device address
	char name[256];			//**< Remote Bluetooth device name
	char pairing_code[16];	//**< Request code.
};

/** Get incoming pairing request (Trident only)
 * @return on success struct btPairingRequest for the pairing request, on failure an empty structure, check errno:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li ESHUTDOWN Bluetooth is down
 * @li EINVAL Invalid parameters
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with Bluetooth device
 * @li ENOMEM Allocation failed
 * @li ENOTCONN Discovery mode not enabled
 */
/*SVC_PROTOTYPE*/ struct btPairingRequest bluetooth_getPairingRequest();

/** Respond to incoming pairing request (Trident only)
 * @param[in] address address of Bluetooth device requesting the pairing
 * @param[in] code confirm or not pairing request 
 * @return 0 if response was successfully sent, -1 on error, check errno:
 * @li ENOENT Bluetooth SW not found
 * @li ENODEV Bluetooth not available
 * @li ESHUTDOWN Bluetooth is down
 * @li EINVAL Invalid parameters
 * @li ECOMM Communication error with Bluetooth daemon
 * @li EIO Communication error with Bluetooth device
 * @li ENOMEM Allocation failed
 * @li ENOTCONN Discovery mode not enabled
 */
/*SVC_PROTOTYPE*/ int bluetooth_confirmPairingRequest(char* address, char* code);


/** Get information on all in-range  Paired devices
 *
 * @return On success struct btRemoteDeviceInfoList for the nearby paired devices. On failure, an empty structure + errno:
 *
 */
/*SVC_PROTOTYPE*/ struct btRemoteDeviceInfoList bluetooth_getScanPairInfo( );

#ifdef RAPTOR
/** Enable Passthru mode
 *
 * @return On success 0, On error -1, errno set accordingly (based on fopen
 * errnos)
 *
 */
/*SVC_PROTOTYPE*/ int bluetooth_enablePassthruMode(void);

/** Disable Passthru mode
 *
 * @return On success 0, On error -1, errno set accordingly (based on unlink
 * errnos)
 *
 */
/*SVC_PROTOTYPE*/ int bluetooth_disablePassthruMode(void);
#endif

#ifdef __cplusplus
}
#endif

#endif // SVC_BLUETOOTH_API_H
