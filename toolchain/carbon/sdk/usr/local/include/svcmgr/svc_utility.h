/** @addtogroup utils Utils */
/** @{ */
/** @addtogroup utilyservice Utils - Utility: Services */
/** @{ */
/**
 * @file svc_utility.h
 *
 * @brief Utility service interface methods
 *
 * The utility service contains general purpose (non-category) methods
 * for doing such things as: get system time, reboot, etc.
 */

#ifndef SVC_SERVICE_UTILITY_H
#define SVC_SERVICE_UTILITY_H

#ifdef __cplusplus
extern "C" {
#endif

#include "svcmgrSvcDef.h"


/* Defines max size/buffer for environment variables obtained from ini file */
/* Important: These are copied and renamed from svc.h, these should stay in sync. */
/*            also, converted from hex, Services cannot have hex array constants. */
#define UTILITY_MAX_LABEL_LEN	(32)
#define UTILITY_MAX_VALUE_LEN	(512)


/*SVC_SERVICE:utility*/
/**
 * @name External Storage Types
*/
/** \{ */
#define EXTERNAL_STORAGE_USB            1    /**< USB Memory device */
#define EXTERNAL_STORAGE_MICRO_SD       2    /**< MicroSD Memory device */
/** \} */


/*SVC_STRUCT*/
/**
 * Return Install Status
 */
struct installStatus {
  int result;			/**< @li 0 = Install complete  No Reboot @li 1 = Install complete Reboot Required @li < 0 Error */
  char msg[256];		/**< Install Error Message (only valid when result < 0) */
};


/*SVC_STRUCT*/
/**
 * Return getEnvFile
 */
struct utilityGetEnvFile {
  int  count;		   	                   /**< @li >=0 - section/name exists in envFile, @li < 0 Error */
  char value[UTILITY_MAX_VALUE_LEN];       /**< Environment variable for section/name (only valid when count > 0) */
};

/*SVC_STRUCT*/
/**
 * The contents of a single section of a Config (ini) file
 */
struct utilityIniSection {
  char* section;		         /**< Section name */
  struct pairlist entries;      /**< Name/value pairs under section */
};

/*SVC_STRUCT*/
/**
 * The commplete contents of a Config (ini) file
 */
struct utilityIniContents {
  struct utilityIniSection* sections;      /**< Array of ini section data */
  int sections_count;		   		        /**< Number of ini sections (array len) */
};


/*SVC_STRUCT*/
/**
 * Return information on a single external storage device
 */
struct utilityAnExternalStorage {
  int type;                     /**< Type of storage: 1=USB, 2=microSD, */
  char mountPoint[64];         /**< Mount point */
};

/*SVC_STRUCT*/
/**
 * Return information on all available external storage devices
 */
struct utilityExternalStorage {
  int storage_count;
  struct utilityAnExternalStorage *storage;
};

/*SVC_STRUCT*/
/**
 * Bundle information needed to remove a bundle
 */
struct bundleInformationStorage {
	char name[64];
	char version[32];
	char user[16];
};

/*SVC_STRUCT*/
/**
 * List of all bundles to be removed
 */
struct removeBundlesStorage {
	struct bundleInformationStorage* bundles;
	int bundles_count;
};

/*SVC_STRUCT*/
/**
 * Specific bundle information to be returned. Contains bundle name and error associated with that bundle
 */
struct bundleReturnStorage {
	char name[64];
	int error;
};

/*SVC_STRUCT*/
/**
 * List of all bundle information to be returned from \ref utility_removeAppBundles
 */
struct removeBundlesReturnStorage {
	struct bundleReturnStorage* bundles;
	int bundles_count;
};

/*SVC_STRUCT*/
/**
 * Replica of the linux structure tm in time.h
 */
  struct utilityDateTime {
    int tm_sec;                 /**< Seconds */
    int tm_min;                 /**< Minutes */
    int tm_hour;                /**< Hours */
    int tm_mday;                /**< Day of the mont */
    int tm_mon;                 /**< Month */
    int tm_year;                /**< Year */
    int tm_wday;                /**< Day of the week */
    int tm_yday;                /**< Day in the year */
    int tm_isdst;               /**< Daylight saving time */
};



/*SVC_STRUCT*/
/**
 * Return dumplog results
 */
  struct utilityDumpLogStatus {
	int status;
	char file_msg[512];	            /**< Contains .tgz filename with pathing or Error message (errno != 0) */
};


/** Obtain service version
 *
 * @return
 * Struct version containing version
 *
 * @note
 * For XML interface, see xml:\ref utility_getVersion
 */
/*SVC_PROTOTYPE*/ struct version utility_getVersion(void);



/** Obtain system time
 *
 * @return
 * Malloced string containing system time or NULL on error
 *
 * <b>Errno values</b>:
 * @li ENOMEM - When unable to malloc space for return string.
 *
 * @note
 * For XML interface, see xml:\ref utility_gettime
 */
/*SVC_PROTOTYPE*/ char* utility_gettime(void);



/** Set system time and RTC
 *
 * @param[in] utildt Structure utilityDateTime
 *
 * @note
 * IMPORTANT! In the utilityDateTime structure, set the year to the actual year (e.g. 2013), and the month between 1 and 12
 *
 * @return
 * @li 0 Success
 * @li -1 Failure
 *
 * @note
 * For XML interface, see xml: \ref utility_settime
 */
/*SVC_PROTOTYPE*/ int utility_settime(struct utilityDateTime *utildt);


/** Copy Linux time to RTC
 *
 * @return
 * @li 0 Success
 * @li -1 Failure
 *
 */
/*SVC_PROTOTYPE*/ int utility_copyTime_linuxRTC(void);


/** Request system reboots
 *
 * @return
 * @li 0 Upon successful request
 * @li -1 If unable to complete request
 *
 * <b>Errno values</b>:
 * @li ECHILD - When unable to fork child process to handle reboot request
 *
 * @note
 * For XML interface, see xml:\ref utility_reboot
 */
/*SVC_PROTOTYPE*/ int utility_reboot(void);


/** Obtain value for name in section within user config ini.
 *
 * @param[in] section Of interest
 * @param[in] name To get value for
 *
 * @return Struct getEnvFile
 *
 * On getEnvFile.count < 0 return, errno:
 * <b>Errno values</b>:
 * @li ENOENT - When name entry not found in section specified in config ini file.
 *
 * @note
 * For XML interface, see xml:\ref utility_getenvfile
 */
/*SVC_PROTOTYPE*/ struct utilityGetEnvFile utility_getenvfile(char *section /*NULL*/, char *name /*NULL*/);


/** Obtain value for name in section within specified ini.
 *
 * @param[in] pathname Full pathname of ini file, reverts to _getenvfile() when NULL
 * @param[in] section Of interest
 * @param[in] name To get value for
 *
 * @return
 * Struct getEnvFile
 *
 * On getEnvFile.count < 0 return, errno:
 *
 * <b>Errno values</b>:
 * @li ENOENT - When name entry not found in section specified in config ini file.
 *
 * @note
 * For XML interface, see xml:\ref utility_getenvfile
 */
/*SVC_PROTOTYPE*/ struct utilityGetEnvFile utility_getenvfilename(char* pathname /*NULL*/, char *section /*NULL*/, char *name /*NULL*/);


/** Set value for name in section within user config ini.
 *
 * @param[in] section Of interest
 * @param[in] name To set value for
 * @param[in] value To be set
 *
 * @return
 * @li 0 Upon successful request
 * @li -1 If Unable to complete request
 *
 * @note
 * @li Section MUST be specified, if value specified, name MUST be specified
 * @li If name specified, and value == empty, this removes entry
 *
 * On error return, errno:\n
 * <b>Errno values</b>:
 * @li EINVAL - If the noted above combinations are not met
 * @li EACCES - Unable to set name in section in env file
 *
 * @note For XML interface, see xml:\ref utility_putenvfile
 */
/*SVC_PROTOTYPE*/ int utility_putenvfile(char *section /*NULL*/, char *name /*NULL*/, char *value /*NULL*/);


/** Set value for name in section within specified ini.
 *
 * @param[in] pathname Full pathname of ini file, reverts to _putenvfile() when NULL
 * @param[in] section Of interest
 * @param[in] name To set value for
 * @param[in] value To be set
 *
 * @return
 * @li 0 Upon successful request
 * @li -1 If unable to complete request
 *
 * @note
 * @li Section MUST be specified, if value specified, name MUST be specified
 * @li If name specified, and value == empty, this removes entry
 *
 * On error return, errno:\n
 * <b>Errno values</b>:
 * @li EINVAL - If the noted above combinations are not met
 * @li EACCES - Unable to set name in section in env file
 *
 * @note
 * For XML interface, see xml:\ref utility_putenvfile
 */
/*SVC_PROTOTYPE*/ int utility_putenvfilename(char* pathname /*NULL*/, char *section /*NULL*/, char *name /*NULL*/, char *value /*NULL*/);


/** Obtain value for name in section within user config ini.
 *
 * @param[in] pathname Full pathname of ini file to retrieve contents.
 *
 * @return
 * Struct utilityIniContents
 *
 * <b>Errno values</b>:
 * @li ENODATA - When filename is NULL or empty
 * @li ENOENT - If filename not found or could not be opened
 * @li ENOMEM - If out of memory
 * @li EFAULT - When an internal error prevents successful completion
 *
 */
/*SVC_PROTOTYPE*/ struct utilityIniContents utility_iniContents(char* pathname);


 /** Release memory allocated by utility_iniContents
 *
 * @note IMPORTANT! Always invoke this function after using utility_iniContents
 *
 */
/*SVC_PROTOTYPE*/void utility_iniContents_release(struct utilityIniContents ic);


/** Install file/package in to system.
 *
 * @note
 * For XML interface, see xml:\ref utility_installfile
 */
/*SVC_PROTOTYPE*/ struct installStatus utility_installfile(void);


/**
 * @name MIBmanager run option Types
*/
/** \{ */
#define MIB_OPTION_DOWNLOAD		1 	/**< -d option, Start download MIB session where port_name = COM1, COM2, COM3, USB0, /dev/ttyO1,..., param = port_name */
#define MIB_OPTION_STORE		2	/**< -w option, Store MIB from file, param = filepath */
#define MIB_OPTION_READ			3	/**< -r option, Read MIB to file, param = filepath */
#define MIB_OPTION_WRITEMAC 	4	/**< -m option, Write MIB MAC Address into file system, param not used */
#define MIB_OPTION_WRITEMIB		5	/**< -o option, Write MIB into NAND at /etc/mib, param not used */
#define MIB_OPTION_SETDETAMPER	6	/**< -k option, Set MIB detamper option to KLD detamper, param not used */
#define MIB_OPTION_CANCEL		9	/**< Cancel any currently running mibmanager, param not used */
/** \} */

/** run the MIB Manager with the given option and param
 * 
 * @param[in] option for what action to request the MIBmanager to run (see MIB_OPTION_xxxx)
 * @param[in] param string used to contain filepath, port_name depending on option (must be < 80 char)
 * 
 * @return 
 * @li 0 Upon successful request
 * @li -1 If unable to complete request
 *
 * On error return, errno:\n
 * <b>Errno values</b>:
 * @li EINVAL - If the noted above combinations are not met (bad option, or no param when option requires one)
 * @li EBUSY - MIB Manager/Loader already active (must wait tell its finished to run again
 * @li EACCES - Unable to run/start
 */
/*SVC_PROTOTYPE*/ int utility_runManagerMIB(int option, char *param);


/**
 * @name MIBmanager run option Types
*/
/** \{ */
#define MIB_STATUS_TYPE_INFO		1	/**< Information message in msg[], used to give feedback on progress */
#define MIB_STATUS_TYPE_ERROR		7	/**< Error message in msg[], indicates cause of error encountered */
#define MIB_STATUS_TYPE_FINISHED 	9	/**< Finished message, indicated MIBmanager process has finished/done */
/** \} */


#define MIBMSGBUFSIZE	140


/*SVC_STRUCT*/
/**
 * Return status results
 */
struct utilityRunMIBstatus {
	int status;		/**< Contains type of status message (see MIB_STATUS_TYPE_xxxx) */
	char msg[MIBMSGBUFSIZE];	/**< Contains message string (null terminated) */
};

/** get status messages from the MIBmanager process
 * 
 * @param[in] flags Specify blocking/non-blocking/cancel/purge/last (see defines for event_getEvent() in svc_event.h)
 * @param[in] timeout (0 = wait forever) - max time in seconds to wait for event.
 * 
 * @return 
 * Struct utilityRunMIBstatus
 * Errno = 0 success, on error:
 *
 * <b>Errno values</b>:
 *   ENODEV - Unable to cancel previous utility_getMIBloaderStatus() in progress.
 *   EAGAIN - No last status message (when flags requesting last).
 *   ENODATA - No status message available (when flags set to non-block and no event).
 *   EBUSY  - status message cancel already in progress, and attempting to cancel get status (flags == 4).
 *   EINTR  - utility_getMIBloaderStatus() canceled (by another thread/user).
 */
/*SVC_PROTOTYPE*/ struct utilityRunMIBstatus utility_getStatusManagerMIB(int flags, int timeout);


/** Run Application
 *
 * @param[in] app  Executable to start, set to NULL to exit System Mode and start default applications
 *
 * @return
 * InstallStatus - Install status code and error string (if appropriate)
 *
 * @return
 *  @li 0  Success
 *  @li <0 Error (see installStatus.msg for explanation).
 *
 * @note
 * For XML interface, see xml:\ref utility_installfile
 */
/*SVC_PROTOTYPE*/ struct installStatus utility_runApplication(char *app);


/** External Storage
 *
 * \warning Make sure to invoke utility_externalStorage_release to release any memory allocated by this function
 *
 * @return
 * Utility_externalStorage - Return a list of available external devices.
 *
 */
/*SVC_PROTOTYPE*/ struct utilityExternalStorage utility_externalStorage(void);


/** Release any memory allocated by utility_externalStorage
 *
 * @note IMPORTANT! Always call this function after using utility_externalStorage
 *
 * @return void
 *
 */
/*SVC_PROTOTYPE*/ void utility_externalStorage_release(struct utilityExternalStorage exstor);

#ifdef RAPTOR
#define UTILITY_USB_DEFAULT	1
#define UTILITY_USB_HOST	2
#define UTILITY_USB_DEVICE	3

/*SVC_STRUCT*/
/**
 * USB core configuration struct
 *
 * To find out which number is assigned to each specific USB port use lsusb, the
 * number corresponds to the BUS number.
 */
struct utility_usb_config {
	int usb1;
	int usb2;
};

/** Get USB core configuration
 *
 * @return
 * - If success, utilitiy_usb_config struct.
 *
 * - If error, utility_usb_config.usb1 will contain -1, errno will be updated
 *   accordignly.
 */
/*SVC_PROTOTYPE*/struct utility_usb_config utility_get_usb_config(void);


/** Set USB core configuration
 *
 * @return
 * - If success, 0
 *
 * - If error, -1, errno will be updated accordignly.
 */
/*SVC_PROTOTYPE*/int utility_set_usb_config(struct utility_usb_config config);
#endif

/** Gather log files and installed software versions to a .tgz
 *
 * @param[in] options Sets the options for where/what content to .tgz
 *            @li 0x00 - Create .tgz in /tmp/ directory
 *            @li 0x01 - Create .tgz in external storage device (typically usbstor1)
 *
 * @return
 * - If status == 0, return struct contains result .tgz filename including full path
 *
 * - If error, status is set and return struct contains error message (cause):
 *         @li EINVAL - Invalid options specified
 *         @li ENOENT - Unable to generate log dump .tgz file
 *         @li ENXIO - External storage device specified, but no external device found
 */
/*SVC_PROTOTYPE*/ struct utilityDumpLogStatus utility_dumplogs(int options/*0*/);


/** Set/get the display brightness (backlight) level (0 to 100)
*
 * @param[in] level Level of brightness @li 0 = off @li 100 = max @li 50 = typical @li if < 0, current level returned
 *
 * @return
 * @li If >= 0 current level returned (post level set);
 * @li If = -1 then error, check errno:
 *
 * <b>Errno values</b>:
 * @li EINVAL - When input level is > 100.
 * @li ENOENT - Unable to open display brightness control device for read
 * @li EPERM - Unable to open display brightness control device for write
 * @li EBADF - Write of new brightness level failed
 *
 * @note
 *   Due to translation of user levels to hardware levels, setting brightness
 *   to a level may result in another level value returned when successful.
 *   If the brightness is decreased, a translated value is used that will decrease
 *   the brightness (down to minimum allowed) that may result in a returned value
 *   different from what was requested. The same is true when increasing brightness.
 *
 */
/*SVC_PROTOTYPE*/ int utility_displayBrightness(int level /* 50 */);


/** Takes screenshots using the framebuffer
 *
 * @param[in] pngfile Full pathname of PNG image file to produce from grab of framebuffer content
 * @param[in] interlace Enables interlacing in PNG output file
 * @param[in] x X-origin to start screen capture from
 * @param[in] y Y-origin to start screen capture from
 * @param[in] w Width in pixels of screen capture (0 causes full width starting from x)
 * @param[in] h Height in pixels of screen capture (0 causes full height starting from y).
 *
 * @return
 * @li 0 - Upon successful request
 * @li -1 - If unable to complete request
 *
 * @note
 * @li If pngfile is NULL or empty, default "/tmp/screenshot.png" will be used.
 * @li All output capture files are of type png, regardless of name extension specified.
 * @li To get display pixel (w,h), see svc_sysinfo.h - sysinfo_platform();
 *
 * On error return,\n
 <b>Errno values</b>:
 * @li EINVAL - If the input params are invalid (negative or out of range).
 * @li EACCES - Unable to create/open for write, pngfile as specified.
 * @li EBADF - Unable to write to pngfile (no space?).
 */
/*SVC_PROTOTYPE*/ int utility_fbgrab(char *pngfile/*""*/, int interlace/* 0 */, int x /*0*/, int y/*0*/, int w/*0*/, int h/*0*/);


/** Return the amount of time in seconds before the OS will expire
 *
 * @param[in] days Number of days an expiring OS will run.  Pass 0 for default 30 days.
 *
 * @return
 * @li 0 = OS never expires,
 * @li > 0 = OS will expire in the returned number of seconds
 * @li < 0 = OS expired.
 *
 * errno == 0 always when returning from this method.
 */
/*SVC_PROTOTYPE*/ long int utility_OSexpireSeconds(int days);


/** Remove all of the bundles for a specific user
 *
 * @param[in] user The user to remove all bundles from
 * 
 * @return 
 * Bundles - if errno == 0, a list of bundles with an error code for each
 *
 * errno values:
 * @li 0 - no error
 * @li EINVAL - invalid arg (user not specified or empty)
 * @li ENOENT - no bundles installed for specified user (nothing to remove)
 * @li EACCES - permission denied (caller does not have permission to remove bundles belonging to user)
 *
 *
 * Error return for each bundle:
 * @li 0 - no error
 * @li EACCES - permission denied
 * @li Other - error from secure installer
 *
 * @note
 * @li when struct \ref removeBundlesReturnStorage is returned, you must free the pointer to bundles when done processing the data
 *
 * \warning  Make sure you invoke utility_removeAllBundles_release to release all the memory allocated by this function.
 *
 * @note For XML interface, see xml:\ref utility_removeAllBundles
 *
 */
/*SVC_PROTOTYPE*/ struct removeBundlesReturnStorage utility_removeAllBundles(char *user);


/**Release any memory allocated by utility_removeAllBundles() in struct removeBundlesReturnStorage
 *
 */
/*SVC_PROTOTYPE*/void utility_removeAllBundles_release(struct removeBundlesReturnStorage removeRetStorage);



/** Remove a list of bundles
 *
 * @param[in] bundles a list of bundles to be removed \ref removeBundlesStorage
 *
 * @return Bundles - a list of bundles with an error for each
 *
 * @note Input
 * @li if \ref removeBundlesStorage bundles.version is set to "0", function will get existing version of that bundle
 *
 * Error return for each bundle:\n
 * <b>Errno values</b>:
 * @li 0 - no error
 * @li EINVAL - specified user does not match the bundle
 * @li ENOPKG - invalid bundle name
 * @li EBADR - specified version does not match the existing bundle's version
 * @li EACCES - permission denied
 * @li Other - error from secure installer
 *
 * @note
 * @li when struct \ref removeBundlesReturnStorage is returned, you must free the pointer to bundles when done processing the data
 *
 * @note For XML interface, see xml:\ref utility_removeBundles
 *
 * errno == 0 always when returning from this method.
 */
/*SVC_PROTOTYPE*/ struct removeBundlesReturnStorage utility_removeBundles(struct removeBundlesStorage bundles);



/**
 * @name System Diagnostic Counters
*/
/** \{ */
#define SYSDIAGCNTR_NAND_FLASH_ECC_ERRORS 			     	 128  /**< Number of flash memory ECC errors detected */
#define SYSDIAGCNTR_NAND_FLASH_ECC_CORRECTED		     	 129  /**< Number of flash memory ECC errors corrected */
#define SYSDIAGCNTR_NAND_FLASH_BAD_BLOCKS 			     	 130  /**< Number of flash memory bad blocks detected */
#define SYSDIAGCNTR_HARD_KEYPAD_PRESSES   			     	 131  /**< Number of key presses on hard keypad in non-secure mode */
#define SYSDIAGCNTR_LAST_PIN_ENTRY_START_TIME_STAMP		     132  /**< Time (number of seconds since 1970/01/01 epoch) that last PIN entry was started */
#define SYSDIAGCNTR_LAST_PIN_ENTRY_END_TIME_STAMP		     133  /**< Time (number of seconds since 1970/01/01 epoch) that last PIN entry was completed */
#define SYSDIAGCNTR_PIN_ENTRY_ERRORS				     	 134  /**< Number of PIN entry errors */
#define SYSDIAGCNTR_POWER_CYCLES_OR_REBOOTS 		     	 135  /**< Number of power cycles plus software-initiated reboots */
#define SYSDIAGCNTR_SYSTEM_MODE_ENTERED	 		     	     136  /**< Number of times that system mode has been entered */
#define SYSDIAGCNTR_SYSTEM_MODE_ENTRY_FAILED 		     	 137  /**< Number of failed attempts to enter system mode */
#define SYSDIAGCNTR_SYSTEM_MODE_PASSWORD_CHANGE_TIME	     138  /**< Time (number of seconds since 1970/01/01 epoch) that any system mode password was last changed */
#define SYSDIAGCNTR_PROCESSED_DOWNLOAD_FILES_OR_PACKAGES	 139  /**< Number of downloaded files and packages processed */
#define SYSDIAGCNTR_MSR_READS_ATTEMPTED	 			         140  /**< Number of magnetic card reads attempted */
#define SYSDIAGCNTR_MSR_READS_WITH_ERRORS_TRACK_1	         141  /**< Number of magnetic card read errors on track one */
#define SYSDIAGCNTR_MSR_READS_WITH_ERRORS_TRACK_2	         142  /**< Number of magnetic card read errors on track two */
#define SYSDIAGCNTR_MSR_READS_WITH_ERRORS_TRACK_3	         143  /**< Number of magnetic card read errors on track three */
#define SYSDIAGCNTR_EMF_ENCOUNTERED					         144  /**< Number of times electromagnetic interference detected by magnetic card reader (hw+drivers don't support so actually counts aggregate track read counters instead) */
#define SYSDIAGCNTR_SMARTCARD_INSERTIONS     		 	     145  /**< Number of smart card insertions with no errors */
#define SYSDIAGCNTR_SMARTCARD_ERRORS        		 	     146  /**< Number of smart card insertions with errors */
#define SYSDIAGCNTR_CONTACTLESS_READS      			         147  /**< Number of contactless card reads */
#define SYSDIAGCNTR_CONTACTLESS_ERRORS     			         148  /**< Number of contactless card errors */
#define SYSDIAGCNTR_TOUCHPANEL_CALIBRATIONS	 		         149  /**< Number of times capacitive touch panel has been calibrated */
#define SYSDIAGCNTR_TOUCHPANEL_CALIBRATION_TIME_STAMP 	     150  /**< Time (number of seconds since 1970/01/01 epoch) that capacitive touch panel was last calibrated */
#define SYSDIAGCNTR_SIGNATURE_CAPTURES_PERFORMED      	     151  /**< Number of signature captures performed */
#define SYSDIAGCNTR_USB_SERIAL_DISCONNECTS	 		         152  /**< Number of times a USB cable disconnection has been detected on USB gadget */
#define SYSDIAGCNTR_TAILGATE_NAKS_SENT		 	 	         153  /**< Number of NAKs sent by terminal during ECR tailgate protocol */
#define SYSDIAGCNTR_TAILGATE_NAKS_RECEIVED	 		 	     154  /**< Number of NAKs received by terminal during ECR tailgate protocol */
#define SYSDIAGCNTR_TOUCHPANEL_DATA_SAMPLES			 	     155  /**< Number of individual capacitive or resistive touch panel data samples received */
#define SYSDIAGCNTR_UBI_MAX_ERASE_COUNT			 	         156  /**< Number of times the flash memory has been completely erased (same as # that shows up near bottom of screen during reboots) */
#define SYSDIAGCNTR_TOUCHPANEL_SCALING_CALC			 	     157  /**< Number of times touch panel scaling constants have been recalculated */
#define SYSDIAGCNTR_TOUCHPANEL_SCALING_CALC_TIME_STAMP 	     158  /**< Time (number of seconds since 1970/01/01 epoch) of last touch panel scaling constants recalculation */
#define SYSDIAGCNTR_TAILGATE_POLL_ADDRESS		 	 	     159  /**< Current poll address for ECR tailgate protocol */
#define SYSDIAGCNTR_CUMULATIVE_UPTIME		 	 	         160  /**< Cumulative up-time of the unit - 30second granularity */
#define SYSDIAGCNTR_HIGH_TEMP_SHUT_STATE_NORMAL	 	 	     161  /**< Number of times temperature returns to normal */
#define SYSDIAGCNTR_HIGH_TEMP_SHUT_STATE_WARN	 	 	     162  /**< Number of times temperature is high */
#define SYSDIAGCNTR_HIGH_TEMP_SHUT_STATE_DANGER	 	         163  /**< Number of times temperature reaches critical level */
/** \} */


/**
 * @name Counters Set Type
*/
/** \{ */
#define DIAGCNTR_SETYPE_ABS    0 /**< Counter value will be set to amount specified in value param */
#define DIAGCNTR_SETYPE_INC    1 /**< Counter value will be incremented by amount specified in value param */
#define DIAGCNTR_SETYPE_DEC    2 /**< Counter value will be decremented by amount specified in value param */
#define DIAGCNTR_SETYPE_TIME   3 /**< Counter value will be set to the current time (using time()) value param is ignored */
/** \} */



/** Get the current value of the specified diagnostic counter
 *
 * @param[in] index Counter index, 1-127 is a user defined counter, 128-255 are system counters
 *
 * @return
 * Counter value (when errno == 0)\n
 * If error, errno:
 * @li EINVAL - Invalid index specified
 *
 * @note
 * @li The defines SYSDIAGCNTR_xxxx define the currently available system diagnostic counters (see comments for purpose)
 */
/*SVC_PROTOTYPE*/ int utility_getDiagCounter(int index/*REQ*/);


/** Set the value of the specified diagnostic counter (only user counters are settable)
 *
 * @param[in] index Counter index, 1-127 is a user defined counter\n
 * @param[in] setType Indicates how the value will be set (see defines DIAGCNTR_SETYPE_xxx)
 * @param[in] value Value to set counter to
 *
 * @return 
 * @li 0 = success
 * @li -1 = error
 *
 * If error, errno:
 *  @li EINVAL - invalid index specified or invalid setType
 *
 */
/*SVC_PROTOTYPE*/ int utility_setDiagCounter(int index/*REQ*/, int setType /*0*/, int value/*1*/);


/*SVC_STRUCT*/
/**
 * structure for handle binary file data
 */
struct binaryFileData {
	char filename[256]; /**< name of file to be written into (may include pathing) */
	long int  mode; /**< mode of file (permissions), eg. S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH (see sys/stat.h */
	void *data; /**< malloc'd space holding file data */
	int   data_count; /**< size of malloc'd space and/or size of file (in bytes) */
};


/** Set the value of the specified diagnostic counter (only user counters are settable)
 *
 * @param[in] binFileData - a structure containing name of file (with pathing) to save, what mode/permissions
 *                          to give to file, and the file data (consists of malloc'd data space and size)
 * Notes:
 * @li mode is forced to give caller R/W permission (ie, >= (S_IRUSR|S_IWUSR)) and group R (S_IRGRP)
 * @li group for file is forced to group "share"
 * @li if binFileData.mode == 0, then the default of (S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH) will be applied
 *
 * @return
 * @li 0 = success
 * @li -1 = error
 *
 * If error, errno:
 *  @li ENOENT - invalid filename (empty?), no data, or invalid data_count
 *  @li EINVAL - The mode provided was invalid
 *  @li EACCES - Access denied, unable to open filename specified
 */
/*SVC_PROTOTYPE*/ int utility_saveToFile(struct binaryFileData binFileData);




#ifdef __cplusplus
}
#endif
#endif //SVC_SERVICE_UTILITY_H

/// @} */
/// @} */
