/** @addtogroup comms Comms */
/** @{ */
/** @addtogroup ethernetservice Comms - Ethernet: Services */
/** @{ */
/** 
 *  @file svc_ethernet.h 
 *
 *  @brief Ethernet service
 *
 *	This header file contains information about Ethernet service.  
 *
 */


   /*
  * VeriFone, Inc.
  */

#ifndef SVC_ETHERNET_H
#define SVC_ETHERNET_H

#include "svcmgrSvcDef.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef STRUCT_NETCONF
#define STRUCT_NETCONF


/** NetIfConfig structure for the Ethernet parameters
 * 
 * Network configuration structure - used by networkapps_getConfig
 * This is identical to the struct 'net_conf_t' in svcNetwork.h of libvfisvc.
 *
 * IMPORTANT! These must always stay identical as one is cast to the other.
 */
/*SVC_STRUCT*/
struct netConf
{
	short int	dhcp;		    /**< @li 1 = DHCP enabled, @li 0 = DHCP disabled */
	char		MAC[18];	    /**< Ethernet MAC address */
	char		ipaddr[16];	    /**< Current IP Address */
	char		netmask[16];	/**< Current Network Mask */
	char		gateway[16];	/**< Current Gateway, address of the gateway. */
	char		dns1[16];	    /**< DNS Server 1 Address */
	char		dns2[16];	    /**< DNS Server 2 Address */
};
#endif


/*SVC_SERVICE:ethernet*/

/** Obtain the version of the Ethernet service.
 *
 * @return 
 * Version struct defined in svcmgrSvcDef.h.\n
 * On success, errno = 0.
 */
/*SVC_PROTOTYPE*/ 
struct version ethernet_getVersion(void);


/** Read current saved network configuration. May differ from active network
 *   configuration if configuration changed after network was brought up.
 *
 * @return 
 * Struct netConf - a network configuration structure
 */
/*SVC_PROTOTYPE*/ 
struct netConf ethernet_getConfig(void) __attribute__ ((deprecated));


/** Set/write network configuration.
 * 
 * @param[in] conf  Network configuration structure (structure members that are empty will NOT be set/altered)
 *
 * @return 
 * @li 0 Success
 * @li -1 Error
 *
 * <b>Errno values</b>: 
 * @li EINVAL - Illegal value passed within conf (struct netConf)
 */
/*SVC_PROTOTYPE*/ 
int ethernet_setConfig(struct netConf conf) __attribute__ ((deprecated));




#ifdef __cplusplus
}
#endif

#endif //SVC_ETHERNET_H
///@}
///@}
