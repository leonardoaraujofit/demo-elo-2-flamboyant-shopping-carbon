/** @addtogroup utils Utils */
/** @{*/
/** @addtogroup eventservice Utils - Event: Services */
/** @{*/
/** 
 *  @file svc_event.h 
 *
 *  @brief Event service
 *
 *	This header file contains information about Event service.
 *
 *	  
 */
 
 

#ifndef SVC_SERVICE_EVENT_H
#define SVC_SERVICE_EVENT_H

#ifdef __cplusplus
extern "C" {
#endif

#include "svcmgrSvcDef.h"

/*SVC_SERVICE:event*/

/**
 * @name  event_getEvent flags values
*/
/** \{ */
#define GETEVENT_BLOCK      (0x00) /**< Get event, if no event block, waiting for event */
#define GETEVENT_NONBLOCK   (0x01) /**< Get event, don't block, return empty if no event */
#define GETEVENT_LAST       (0x02) /**< Get the last event that was received (and previously read) */
#define GETEVENT_CANCEL     (0x04) /**< Cancel/unblock any process waiting on this event key */
#define GETEVENT_PURGE      (0x08) /**< Purge/flush and pending events and return last event read */
/** \} */

#ifndef MAX_KEY_LEN
#define MAX_KEY_LEN         (99) /**< Max length for event key names */
#endif

/**
 * Event structure
 *
 * @note 
 * e.event can be used with or without e.data. If e.data is used, you must set e.size for sizeof e.data 
 *
 * @note
 * As seen by struct event an event is comprised of an event number (int) and
 * optional event data. Set event.size to zero and event.data to null when no
 * data is attached to the event. Otherwise, set the event.data pointer and
 * event.size. size is the number of data bytes to extract on getEvent().
 * Ensure that size includes null-termination bytes of strings, and so on. Event data
 * can be a maximum of 4000 bytes. All Service rules apply. Pointer data in both
 * input and output parameters are owned by, and must be freed by, the caller.
 *
 */
struct event {
    int            event;  /**< Event ID/tag, used in lieu of data when an integer is enough for event response */
    unsigned int   size;  /**< Size of data (in bytes) */
    void*          data;   /**< Malloc's space to hold event data */
};




/** Obtain the version of the event service
 *
 * @return 
 * Version struct defined in svcmgrSvcDef.h
 * 
 * @note
 * Every Service is required to support a set of core interface functions to provide
 * uniformity across Services. The Service precompiler checks for these functions
 * and outputs a compliance error when a function is missing.
 *
 * @note
 * Struct version {service}_getVersion(void).
 */
/*SVC_PROTOTYPE*/ struct version event_getVersion(void);


/*
 * This is a helper Service and typically called ONLY from other services!
 */

/** Open event queue.
 *
 * @param[in] key  Name/key of event to open
 *
 * @return 
 * @li  0 Success
 * @li -1 On error
 * 
 * <b>Errno Values:</b>
 * @li EINVAL - Invalid key format
 * @li EFAULT - Unable to open/create event
 * @li ENOSPC - No more events allowed (full)
 * @li EALREADY - event key already open
 *
 * @note
 * A Service can use multiple event channels, each requires a unique key. There is a
 * system-wide limit of 256 event channels. An error (errno=EFAULT) returns from
 * event_open() if this limit is reached.
 *
 * @note
 * An event channel is opened when event_open() is called and remains open
 * and active until closed.
 */
void event_open(char* key);


/** Close a previously opened event queue.
 *
 * @param[in] key Name/key of event as specified to event_open()
 *
 * @return
 * @li 0 Success
 * @li -1 On error
 *
 * <b>Errno Values:</b>
 * @li EINVAL - Invalid key format
 * @li EFAULT - Unable to close event
 * 
 * @note
 * When closed, the Event channel is cleaned from the
 * system and any unread events in the queue are lost. A channel can be closed and
 * reopened using the same key to flush the FIFO.
 */
void event_close(char* key);



/** Cancel a event_getEvent() in progress.
 *
 * @param[in] key Name/key of event as specified to event_open()
 *
 * @return 
 * @li 0 - If key open but no getEvent() in progress
 * @li >0 - If key open and getEvent() in progress, cancel sent
 * @li -1 - If key not open, or error
 *
 * <b>Errno Values:</b>
 * @li EINVAL - Invalid key format
 * @li ENODEV - No event for key (event_open() using passed key not active)
 * @li EFAULT - Internal error
 */
int event_cancel(char* key);


/** Cancel a event_getEvent() in progress specifying a cause.
 * 
 *   The cause value is passed back to event_getEvent in the event.event parameter.
 *
 * @param[in] key Name/key of event as specified to event_open()
 * @param[in] cause User assigned value for why cancel was made
 *
 * @return 
 * @li 0 if key open but no getEvent() in progress
 * @li >0 if key open and getEvent() in progress, cancel sent
 * @li -1 if key not open, or error
 *
 * <b>Errno Values:</b>
 * @li EINVAL - Invalid key format
 * @li ENODEV - No event for key (event_open() using passed key not active)
 * @li EFAULT - Internal error
 */
int event_cancelWithCause(char* key, int cause);


/** Returns next event.
 *
 * Setting flags=0 will perform the default action of retrieving the next non-previously
 * retrieved event, blocking if needed waiting for the occurrence of the next event.
 *
 * @param[in] key  Name/key of event as specified to event_open()
 * @param[in] flags Specify blocking/non-blocking/cancel/purge/last (see defines)
 *
 * @return 
 * Event  - upon success, event structure will contain event and possibly data.
 *
 * 
 * @note
 * @li If data != NULL, user/caller is responsible to free data (malloc'd memory).
 * @li Max data size is 4000 bytes
 * @li if flags & GETEVENT_PURGE, max data size is 2048 bytes
 * @li GETEVENT_PURGE will purge any pending events and return the last (newest) event
 * @li GETEVENT_PURGE|GETEVENT_NONBLOCK will return right away with errno=ENODATA if no events pending
 * @li GETEVENT_PURGE and GETEVENT_NONBLOCK not specified, will wait for event if no event pending
 *
 * <b>Errno Values:</b>
 *   EINVAL - Invalid key format
 *   ENOENT - key isn't active/open (event_open() not called first)
 *   ENODEV - Unable to cancel previous event_getEvent() in progress.
 *   EAGAIN - No last event (when flags requesting last).
 *   ENODATA - No event data available (when flags set to non-block and no event).
 *   EBUSY  - event cancel already in progress, and attempting to cancel event (flags == 4).
 *   EFAULT - event.size specified, but no data available to read.
 *   EINTR  - event_getEvent() canceled (by another thread/user). event field of returned
 *            structure may contain cause (see event_cancelWithCause())
 *   ENOMEM - Unable to allocate needed memory for event.size to read associated data
 */
struct event event_getEvent(char* key, int flags);


/** Returns next event.
 * 
 * Setting flags=0 will perform the default action of retrieving the next non-previously
 * retrieved event, blocking if needed waiting for the occurrence of the next event.
 *
 * @param[in] key Name/key of event as specified to event_open()
 * @param[in] flags Specify blocking/non-blocking/cancel/purge/last (see defines)
 * @param[in] timeout (0 = wait forever) - max time in seconds to wait for event.
 *
 * @return 
 * Event - Upon success, event structure will contain event and possibly data.
 *
 * @note
 * @li If data != NULL, user/caller is responsible to free data (malloc'd memory).
 * @li Max data size is 4000 bytes
 * @li if flags & GETEVENT_PURGE, max data size is 2048 bytes
 * @li GETEVENT_PURGE will purge any pending events and return the last (newest) event
 * @li GETEVENT_PURGE|GETEVENT_NONBLOCK will return right away with errno=ENODATA if no events pending
 * @li GETEVENT_PURGE and GETEVENT_NONBLOCK not specified, will wait for event if no event pending
 * @li timeout parameter only used if flags == GETEVENT_BLOCK
 * @li Upon timeout, errno == EINTR (same as cancel), and event.event == ENOENT
 *
 * <b>Errno Values:</b>
 *   EINVAL - Invalid key format
 *   ENOENT - key isn't active/open (event_open() not called first)
 *   ENODEV - Unable to cancel previous event_getEvent() in progress.
 *   EAGAIN - No last event (when flags requesting last).
 *   ENODATA - No event data available (when flags set to non-block and no event).
 *   EBUSY  - event cancel already in progress, and attempting to cancel event (flags == 4).
 *   EFAULT - event.size specified, but no data available to read.
 *   EINTR  - event_getEvent() canceled (by another thread/user). event field of returned
 *            structure may contain cause (see event_cancelWithCause())
 *   ENOMEM - Unable to allocate needed memory for event.size to read associated data
 */
struct event event_getEventTimeout(char* key, int flags, int timeout);


/** Place an event on the 'key' queue.
 *
 *
 * @param[in] key Name/key of event as specified to event_open()
 * @param[in] e Event (see struct event) to be posted
 *
 * @note
 * @li If data != NULL and it was malloc'd, user/caller is responsible to free.
 * @li Max data size is 4000 bytes
 *
 * @return 
 * Errno = 0 success, on error:
 * 
 * <b>Errno Values:</b>
 * @li  EINVAL - Invalid key format or data size too big (<4000 bytes)
 * @li  ENOENT - No event for key (event_open() using passed key not active)
 * @li  EFAULT - Internal error
 */
void event_setEvent(char* key, struct event e);


/*
 * Below group of methods is to provide a system wide event that can be used as a trigger mechanism
 */


/**
 * @name  Name-Value pair parameter max size values
*/
/** \{ */
#define NVP_NAME_MAX_LENGTH   32  /**< Max length of Name parameter (including NULL) */
#define NVP_VALUE_MAX_LENGTH  32  /**< Max length of Value parameter (including NULL) */
/** \} */


/*SVC_STRUCT*/
struct nameValuePair {
	char name[NVP_NAME_MAX_LENGTH];   /**< ASCII string NULL terminated for name component */
	char value[NVP_VALUE_MAX_LENGTH]; /**< ASCII string NULL terminated for value component */
};


/*SVC_STRUCT*/
struct eventNameValuePair {
	int event;                  /**< Event ID/tag, used alone or with name/value pair list */
	int list_count;             /**< Number of name value pair list */
	struct nameValuePair *list; /**< Malloc'd pairlist of name,value pairs to be passed in event */
};


/** Returns next Name-Value Pair event.
 * 
 * Setting flags=0 will perform the default action of retrieving the next non-previously
 * retrieved event, blocking if needed waiting for the occurrence of the next event.
 *
 * @param[in] key Name/key of event as specified to event_openNameValuePairEvent()
 * @param[in] flags Specify blocking/non-blocking/cancel/purge/last (see defines)
 * @param[in] timeout (0 = wait forever) - max time in seconds to wait for event.
 *
 * @return 
 * EventNameValuePair - Upon success, eventNameValuePair structure will contain event and possibly data.
 *
 * @note
 * @li If list != NULL, user/caller is responsible to free list (malloc'd memory).
 * @li Max name-value pair list size is 4000 bytes
 * @li if flags & GETEVENT_PURGE, max data size is 2048 bytes
 * @li GETEVENT_PURGE will purge any pending events and return the last (newest) event
 * @li GETEVENT_PURGE|GETEVENT_NONBLOCK will return right away with errno=ENODATA if no events pending
 * @li GETEVENT_PURGE and GETEVENT_NONBLOCK not specified, will wait for event if no event pending
 * @li timeout parameter only used if flags == GETEVENT_BLOCK
 * @li Upon timeout, errno == EINTR (same as cancel), and event.event == ENOENT
 *
 * <b>Errno Values:</b>
 *   EINVAL - Invalid key format
 *   ENODEV - Unable to cancel previous event_getEvent() in progress.
 *   EAGAIN - No last event (when flags requesting last).
 *   ENODATA - No event data available (when flags set to non-block and no event).
 *   EBUSY  - event cancel already in progress, and attempting to cancel event (flags == 4).
 *   EFAULT - event.size specified, but no data available to read.
 *   EINTR  - event_getEvent() canceled (by another thread/user). event field of returned
 *            structure may contain cause (see event_cancelWithCause())
 *   ENOMEM - Unable to allocate needed memory for event.size to read associated data
 */
/*SVC_PROTOTYPE*/ struct eventNameValuePair event_getNameValuePairEvent(char* key, int flags, int timeout);


 /** Place a eventNameValuePair event on the name-value queue (only one exists in service).
  *
  * @param[in] key Name/key of event as specified to event_openNameValuePairEvent()
  * @param[in] et Name-value list event (see struct eventNameValuePair) to be posted
  *
  * @note
  * @li If list != NULL and it was malloc'd, user/caller is responsible to free.
  * @li Max data size is 4000 bytes
  *
  * @return 
  * @li errno = 0 success
  * @li on error:
  * 
  * <b>Errno Values:</b>
  * @li EINVAL - Invalid key format or data size too big (<4000 bytes)
  * @li ENODEV - No event for key (event_open() using passed key not active)
  * @li EFAULT - Internal error
  */
/*SVC_PROTOTYPE*/ void event_setNameValuePairEvent(char* key, struct eventNameValuePair et);


/** Open Name-Value Pair event queue.
 *
 * @param[in] key Name/key of event as specified to event_openNameValuePairEvent()
 *
 * @return 
 * @li errno = 0 success
 * @li  on error:
 * 
 * <b>Errno Values:</b>
 * @li EINVAL - Invalid key format
 * @li EFAULT - Unable to open/create event
 */
/*SVC_PROTOTYPE*/ void event_openNameValuePairEvent(char* key);


/** Close a previously opened Name-Value Pair event queue.
 *
 * @param[in] key Name/key of event as specified to event_openNameValuePairEvent()
 *
 * @return 
 * @li errno = 0 success
 * @li  on error:
 * 
 * <b>Errno Values:</b>
 *  @li EINVAL - Invalid key format
 *  @li EFAULT - Unable to close event
 */
/*SVC_PROTOTYPE*/ void event_closeNameValuePairEvent(char* key);


/** Cancel a event_getNameValuePairEvent() in progress specifying a cause.\n
 *   The cause value is passed back to event_getNameValuePairEvent in the event.event parameter.
 *
 * @param[in] key Name/key of event as specified to event_openNameValuePairEvent()
 * @param[in] cause User assigned value for why cancel was made
 *
 * @return 
 * Status\n :  
 * @li 0 if key open but no event_getNameValuePairEvent() in progress
 * @li >0 if key open and event_getNameValuePairEvent() in progress, cancel sent
 * @li -1 if key not open, or error:
 *
 * <b>Errno Values:</b>
 *  @li EINVAL - Invalid key format
 *  @li ENODEV - No event queue (event_openNameValuePairEvent() not active)
 *  @li EFAULT - Internal error
 */
/*SVC_PROTOTYPE*/ int event_cancelNameValuePairEvent(char* key, int cause);


/** Manage multicast event channel. This creates or removes a multicast event channel.
 *
 * @param[in] channel  Name of multicast event channel to create/remove/cancel
 * @param[in] mode used to specify how this call executes
 * @li mode = 0 - create (if already created, no change)
 * @li mode = 1 - remove multicast event channel (if pending requests, cancel all) then remove multicast channel
 * @li mode = 2 - cancel all pending event_getEvent() or event_getEventTimeout() calls
 * @param[in] cause - if mode == 1 or 2, user assigned value for why remove or cancel was made (ignored by mode == 0)
 *
 * @return
 * @li  > 0 handle - upon success, the multicast event handle is returned.
 * @li -1 On error
 *
 * <b>Errno Values:</b>
 * @li EINVAL - Invalid key format
 * @li EFAULT - Unable to open/create multicast event channel
 * @li EAGAIN - Already created and mode == 0
 * @li ENOSPC - No more multicast channels allowed (max already created)
 *
 * @note
 * @li A Service can use multiple event channels (single or multicast), each requires a unique key. There is a
 * system-wide limit of 256 event channels. An error (errno=EFAULT) returns from
 * event_open() if this limit is reached.
 * @li maximum users of this event (up to 32 are currently allowed)
 *
 * @note
 * A multicast event channel once created remains open and active until removed.
 */
void event_multicast_channel(char *channel, int mode/*0*/, int cause);


/**  Returned mcast event add Structure */ 
struct event_mcastAddEvent {
	int status; 	/**< Used to indicate status of request */
	char eventkey[MAX_KEY_LEN+1];	    /**<  Event key used to get mcast events */
};

/** add event to multicast event channel.
 *  This is an event where a single event can be sent to multiple processes (fan out, or 1 to many)
 *
 * @param[in] channel  Name of multicast event channel (see event_multicast_channel())
 * @param[in] key Name/key of event as specified to event_open(), --OR--
 *  		  If key = NULL, or key[0] == 0, will create event key, open event, and attach to mcast channel
 *
 * @return 
 * @li  0 Success, the event was successfully added to the multicast event channel.
 * @li -1 On error
 * 
 * <b>Errno Values:</b>
 * @li EINVAL - Invalid channel or key format
 * @li ENOENT - Specified channel does not exist (must first create channel)
 * @li ENOSPC - No more events can be added to channel (full)
 *
 * @note
 * @li A Service can use multiple event channels, each requires a unique key. There is a
 * system-wide limit of 16 event channels. An error (errno=EFAULT) returns from
 * event_open() if this limit is reached.
 * @li maximum events attached to this event channel (up to 16 are currently allowed)
 *
 * @note
 * An event must be opened prior to adding to the multicast event channel and must 
 * remain open and active until removed from the channel (see event_multicast_removeEvent()).
 */
struct event_mcastAddEvent event_multicast_addEvent(char *channel, char *key);


/** remove event from multicast event channel. This does not close the channel, 
 *  it removes the specified event key from the multicast channel and no more multicast events
 *  will be received by the specified event key. If the flag is set, the removed event will also be closed.
 *
 * @param[in] channel  Name of multicast event channel (see event_multicast_channel())
 * @param[in] key Name/key of event connected to channel (see event_multicast_addEvent())
 * @param[in] flag that provides options
 * 		@li flag = 0 do not close event specified by key
 * 		@li flag = 1 will close the event specified by key
 *
 * @return 
 * @li  0 Success, the event was successfully removed from the multicast event channel.
 * @li -1 On error
 * 
 * <b>Errno Values:</b>
 * @li EINVAL - Invalid channel or key format
 * @li ENOENT - Specified channel does not exist (must first create channel)
 * @li ENODEV - Specified event does not exist in channel (previously removed?)
 */
int event_multicast_removeEvent(char *channel, char *key, int flag/*0*/);



/** Place an event in the multicast event queue.
 * 
 * This will send an event to all nodes of the channel
 *
 * @param[in] channel  Name of multicast event channel (see event_multicast_channel())
 * @param[in] e Event (see struct event) to be posted to multicast channel
 *
 * @note
 * @li If data != NULL and it was malloc'd, user/caller is responsible to free.
 * @li Max data size is 4000 bytes
 *
 * @return 
 * @li Count - number of events attached to this mcast channel
 * Errno = 0 success, on error:
 * 
 * <b>Errno Values:</b>
 * @li EINVAL - Invalid key or event (data size too big (<4000 bytes))
 * @li ENOENT - Specified channel does not exist (must first create channel)
 * @li ECHILD - One or more attached events (nodes) failed to see event
 */
int event_multicast_setEvent(char *channel, struct event e);


#ifdef __cplusplus
}
#endif
#endif //SVC_SERVICE_EVENT_H

/// @}*/
/// @}*/
