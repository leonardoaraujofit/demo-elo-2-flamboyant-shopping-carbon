
/**
 * @file svc_security.h
 *
 * @brief Security Services
 *
 * This header file contains information about Security Services.
 */


#ifndef SVC_SECURITY_H
#define SVC_SECURITY_H

#ifdef __cplusplus
extern "C" {
#endif

/*SVC_SERVICE:security*/
#include "svcmgrSvcDef.h"

#include <keyinfo_types.h>


// Defines

#define MAX_CERT_SN_ID		128


/**
 * @name Certificate status values
*/
/** \{ */
#define NO_MORE_CERTIFICATES	 0
#define VALID_CERTIFICATE	 1
/** \} */

/**
 * @name Key Spaces
*/
/** \{ */
#define MASTER_SESSION_KEYS	0
#define DUKPT_KEYS		    1
#define USER_KEYS		    2
#define RKL_KEYS            3
#define ADE_KEYS		    4
#define RSA_KEYS			5
/** \} */

/**
 * @name Key Status Values
*/
/** \{ */
#define NO_KEY_LOADED		0
#define KEY_PRESENT		1
#define GISKE_KEY_PRESENT	2
#define DUKPT_EOL		3

#define SECURITY_NO_KEYS_RKL        0       /**< RKL - Both files DO NOT exist */
#define SECURITY_PUBLIC_KEY_RKL     1       /**< RKL - Only /root/rkl_keys/rkl_cert.crt exist */
#define SECURITY_PRIVATE_KEY_RKL    2       /**< RKL - Only /root/rkl_keys/rkl_priv_key.der exist */
#define SECURITY_BOTH_KEYS_RKL      3       /**< RKL - Both files exist */
/** \} */

/**
 * @name Key Strength Values
*/
/** \{ */
#define UNKNOWN_STRENGTH	0
#define ONE_DES_STRENGTH	1
#define THREE_DES_STRENGTH	3
/** \} */

/**
 * @name Key Dump Errors
*/
/** \{ */
#define KEY_DUMP_SUCCESS                0
#define KEY_DUMP_USB_NOT_FOUND          1
#define KEY_DUMP_FILE_COPY_ERROR        2
#define KEY_DUMP_KEYS_NOT_FOUND         3
/** \} */

/**
 * @name Key Injection Modes
*/
/** \{ */
#define INJECT_BANK_KEYS	0
#define INJECT_RKL_KEY_PAIR	1
#define INJECT_PERSONALIZATION	2
/** \} */


/**
 * @name Service Switch Actions
*/
/** \{ */
#define SERVICE_SWITCH_READ             0
#define SERVICE_SWITCH_LATCH            1
#define SERVICE_SWITCH_ARM              2
#define SERVICE_SWITCH_DISABLE_REBOOT   3
#define SERVICE_SWITCH_ENABLE_REBOOT    4
#define SERVICE_SWITCH_REBOOT_STATE     5
/** \} */

/**
 * @name Service Switch Responses
*/
/** \{ */
#define SERVICE_SWITCH_OFF              0
#define SERVICE_SWITCH_ON               1
/** \} */

/**
 * @name Stunnel port range
*/
/** \{ */
#define STUNNEL_PORT_MIN 10000
#define STUNNEL_PORT_MAX 10200
/** \} */

/**
 * @name Stunnel modes
*/
/** \{ */
#define STUNNEL_MODE_SERVER 1
#define STUNNEL_MODE_CLIENT 2
/** \} */


/*SVC_STRUCT*/
/**
 * Vault component versions
 */
struct vaultVersion {
  unsigned char vault[32];	/**< String representing vault version */
};

/*SVC_STRUCT*/
/**
 * Attack Status
 */
struct attackStatus {
  int isAttacked;				/**< @li 0 = False @li != 0 True */
  int securityBarrierFlag;			/**< Security Barrier Flag value */
  int tamperStatusReg;				/**< Tamper Status Register */
};

/*SVC_STRUCT*/
/**
 * Single Certificate S/N + ID record
 */
struct certEntry {
  int status; 			        /**< @li 1 = Valid record returned @li \n 0 = No more records */
  int level;			        /**< Certificate level within tree (starts with 1) */
  char sn[MAX_CERT_SN_ID];  	/**< Serial number of certificate */
  char id[MAX_CERT_SN_ID];	    /**< ID number of the certificate */
};

/*SVC_STRUCT*/
/**
 * Single Certificate Extended: S/N + ID record + Certificate Name
 */
struct certEntryExtended {
  int status;                       /**< @li 1 = Valid record returned @li \n 0 = No more records */
  int level;                        /**< Certificate level within tree (starts with 1) */
  char sn[MAX_CERT_SN_ID];           /**< Serial number of certificate */
  char id[MAX_CERT_SN_ID];          /**< ID number of the certificate */
  char certName[MAX_CERT_SN_ID];    /**< Certificate Name */
};


#define MAX_ELEM_STRLEN		32	/**< Size of sn, id, and certName in certListElem struct to reduce size */

/*SVC_STRUCT*/
/**
 * Single Certificate Element: S/N + ID record + Certificate Name
 */
struct certListElem {
	int status;							/**< @li 1 = Valid record returned @li \n 0 = No more records */
	int level;							/**< Certificate level within tree (starts with 1) */
	char sn[MAX_ELEM_STRLEN];			/**< Serial number of certificate */
	char id[MAX_ELEM_STRLEN];			/**< ID number of the certificate */
	char certName[MAX_ELEM_STRLEN];		/**< Certificate Name */
};

/*SVC_STRUCT*/
/**
 * Array of Certificates Extended: S/N + ID record + Certificate Name
 */
struct certEntryList {
	int certs_count;				/**< Number of certs */
	struct certListElem *certs;		/**< certs available */
};


/*SVC_STRUCT*/
/**
 * Key status record
 */
struct keyRecord {
  int status; 			/**< Depends on key type */
  int strength;			/**< 1= 1 DES or 3 = 3 DES */
  char EPB[32];  		/**< Encrypted PIN Block */
  char KSN[32];			/**< Key Serial Number */
  char GISKE[32];		/**< GISKE flags */
};

/*SVC_STRUCT*/
/**
 * RSA Key list
 */
struct keyRSAlist {
  struct keyRSAinfo *key;   /**< RSA key list */
  int total_keys;           /**< total keys found */
};

/*SVC_STRUCT*/
/**
 * Collection of related keys - Important keys must be freed!
 */
struct keyRecords {
  int keys_count;		/**< Number of keyRecords */
  struct keyRecord *keys;	/**< Key records */
};

/*SVC_STRUCT*/
/**
 * Events related to IPP Passthrough operation
 */
struct securityStatusReturn {
  int status;			/**< Current Status */
  char *msg;			/**< Status message string */
};

/*SVC_STRUCT*/
/**
 * CPU Parameters
 */
struct securityCPUParameters {
  unsigned char Scfg;               /**< Reserved */
  unsigned short int BrcmRev;       /**< Reserved */
  unsigned int Dcfg;                /**< Reserved */
  char DevId[72];                   /**< Hash of device id */
  unsigned char SVID;                /**< @li 0=Manufacturing @li 1=Production @li 8=Development */
};


/*SVC_STRUCT*/
/**
 * Tamper Record
 */
struct tamperRecord {
  char SBF[8];                   /**< Security Barrier Flag */
  char encodedSrcReg[8];         /**< Encoded Tamper Source Register */
  char status[8];               /**< Tamper Register */
  char date[32];                /**< Date/Time of Tamper */
};

/*SVC_STRUCT*/
/**
 * Collection of related keys - Important keys must be freed!
 */
struct tamperRecords {
  int tamper_count;                 /**< Number of tamper entries */
  struct tamperRecord *tamper;      /**< Tamper records */
};

/*SVC_STRUCT*/
/**
 * Single certificate used to pair Ux devices
 */
struct uxCertRecord {
  int level;                        /**< Certificate level,\n 0 = Root/Primary,\n 1 = CA/Customer, \n 2 = Enc */
  unsigned int certified_key_id;    /**< Certiffied key id */
  unsigned int certifying_key_id;   /**< Certifying key id */
  int is_valid;                        /**< 1 = Valid certificate, \n 0 = Invalid certificate */
};

/*SVC_STRUCT*/
/**
 * Collection of certificates used to pair Ux devices - Important certs must be freed!
 */
struct uxCertRecords {
  int certs_count;                 /**< Number of certificates */
  struct uxCertRecord *certs;      /**< certificates */
};

/*SVC_STRUCT*/
/**
 *  cptest conversion status structure
 */
struct cptest_status_st
{
    int status;     /* conversion status */
    unsigned int msg_len;
    char msg[2056];  /* information regarding conversion status */
};

/*
 * svc events for CP test( for use in svc_event status field )
 */
typedef enum
{
    CPTEST_SVCEVENT_MSG = 1,
    CPTEST_SVCEVENT_DONE,
    CPTEST_SVCEVENT_ABORT_REQ
} CPTEST_SVCEVENT_STATUS;

/*
 * cptest status ( for use in cptest_status_st status field )
 * CPTEST_STATUS_SUCCESS is 0
 */
typedef enum
{
    CPTEST_STATUS_TAMPER = -5,
    CPTEST_STATUS_NON_PROD,
    CPTEST_STATUS_LOCKED,
    CPTEST_STATUS_ALREADY_CONVERTED,
    CPTEST_STATUS_ERROR,
    CPTEST_STATUS_SUCCESS,
    CPTEST_STATUS_USB_CONFIG,
    CPTEST_STATUS_DONE
} CPTEST_STATUS;

/*SVC_STRUCT*/
/**
 * Stunnel port redirection record
 */
struct stunnelPortRecord {
  int mode;			/**< Client/Server mode (1:Server, 2:Client) */
  char accept[32];	/**< Accept connections on specified address [HOST:]PORT */
  char connect[32];	/**< Connect to specified address [HOST:]PORT */
};

/*SVC_STRUCT*/
/**
 * Stunnel configuration - Important keys must be freed!
 */
struct stunnelConfig {
  int startup; 	/**< Activate on starup (0:No, 1:Yes) */
  int ports_count;		/**< Number of port records */
  struct stunnelPortRecord *ports;	/**< Port redirection list */
};

/*SVC_STRUCT*/
/**
 * Challenge buffer which contains the challenge data of 8 bytes and the working key data of 16 bytes.
 */
struct securityChallenge {
  char challenge[8];
  char workingKey[16];
};

/*SVC_STRUCT*/
/**
 * Challenge buffer which contains the challenge data of 8 bytes.
 */
struct securityResolvedChallenge {
  char challenge[8];
};

/** Obtain the version of the Security service
 *
 * @return
 * Version struct defined in svcmgrSvcDef.h
 */
/*SVC_PROTOTYPE*/ struct version security_getVersion(void);



/** Obtain the vault component versions
 *
 * @return
 * VaultVersion
 */
/*SVC_PROTOTYPE*/ struct vaultVersion security_getVaultVersion(void);


/** Obtain a certificate S/N and ID from the certificate tree
 *
 * @return
 * CertEntry structure containing a single certificate Serial Number and Id
 */
/*SVC_PROTOTYPE*/ struct certEntry security_getCertificate(void);


/** Obtain a certificate S/N, ID and Certificate Name from the certificate tree
 *
 * @return
 * CertEntryExtended structure containing a single certificate Serial Number, Id and Certificate Name.
 */
/*SVC_PROTOTYPE*/ struct certEntryExtended security_getCertificateExtended(void);


/** Obtain full list of: certificate S/N, ID and Certificate Name from the certificate tree
 *
 * @return
 * certEntryList structure containing a pointer to malloc'd memory containing array of
 *    certEntryExtended structs (this must be free()'d when finished (can use:
 *    security_getCertificateExtendedList_release()).
 * certEntryList also contains how many entries are in the array: certs_count
 */
/*SVC_PROTOTYPE*/ struct certEntryList security_getCertificateList(void);


/** Release memory allocated by security_getCertificateExtendedList
 *
 * @note IMPORTANT! ALWAYS call this function after using the struct returned by security_getCertificateExtendedList functions
 */
/*SVC_PROTOTYPE*/ void security_getCertificateList_release(struct certEntryList certList);


/** Obtain Attack Status
 *
 * @return
 * AttackStatus structure containing SBF and Tamper Register
 */

/*SVC_PROTOTYPE*/ struct attackStatus security_getAttackStatus(void);



/** Obtain key status
 *
 * @return
 * KeyRecord structure containing a single certificate Serial Number and Id.
 */
/*SVC_PROTOTYPE*/ struct keyRecords security_getKeys(int keySpace);



/** Dump public keys to USB  
 *
 * @return 
 * Error number.
 */
/*SVC_PROTOTYPE*/ int security_dumpPublicKeys( void );



/** Initiate Key Injection via serial port
 *
 *  @param[in] mode @li 0 - Inject Bank Keys  @li 1 - Inject RKL key pair
 * @param[in] port  Serial port to be used during key injection. 1 = COM1, 2 = COM2 (no other ports supported!)
 * @param[in] rate  Serial port baud rate (per termios.h)
*/
/*SVC_PROTOTYPE*/ int security_keyInjection(int mode, int port, int rate);



/** Cancel Key Injection Session
*/
/*SVC_PROTOTYPE*/ int security_cancelKeyInjection(void);



/** Set System Mode Password
 *
 * @param[in] inUserName Pointer User or Login string
 * @param[in] inShaPasswdStr Pointer to SHA password (may be a concatenation of old and new passwords for mode=1)
 * @param[in] mode  @li 0 - Set Password  @li 1 - Set pending password change
 */
/*SVC_PROTOTYPE*/ int security_passwordSet(char *inUserName, unsigned char inShaPasswdStr[32], int mode);



/** Compare System Mode Password
 *
 * @param[in] inUserName pointer User or Login string
 * @param[in] inShaPasswdStr1 Pointer to SHA password
 * @param[in] inShaPasswdStr2  Pointer to a second SHA password (to be concatenated with inShaPasswdStr1)
 * @param[in] mode  @li 0 - Check Password @li 1 - Check pending password
 *
 * @return
 * @li 0 Match
 * @li -1 No match
 * @li -2  No entry or Password change pending and no match
 *
 * @note
 * Set PasswdStr1 = "", PasswdStr2 = NULL, mode = 1 to test for a pending pwd, if rslt != -2 then have pending pwd.
 *
 * ex.
 *
 *    int pending = (security_passwordCompare(usrLogin, "", NULL, 1) != -2) ? 1 : 0;
*/
/*SVC_PROTOTYPE*/ int security_passwordCompare(char* inUserName, unsigned char inShaPasswdStr1[32], unsigned char inShaPasswdStr2[32], int mode);



/** Manage Password Expiration
 *
 * @param[in] inUserName  pointer User or Login string
 * @param[in] mode @li 0 Read Expired @li 1 Set Expired
 * @param[in] value  @li 1 - Set expired to 1 @li 0 - delete expired environment variable
*/
/*SVC_PROTOTYPE*/ int security_passwordExpired(char *inUserName, int mode, int value);


/** Read event from a Securityoperation - IPP Passthrough
 *
 * @param[in] flags Get Event Flags:
 *                      @li 1 = Non-block
 *                      @li 2 = Last
 *                      @li 4 = Cancel (options can be or'd)
 *
 * @return
 * Struct securityStatusReturn - status will reflect current IPP Passthrough status
 *
 * @li Status will be: @li -1 = fatal error @li 1 = Normal @li 2 = Non-fatal error
 * @li msg will be a null terminated status string.  NOTE: msg must be freed by caller
 */
/*SVC_PROTOTYPE*/ struct securityStatusReturn security_getEvent(int flags);



/** Securely Set Password(s) using Ux encrpytion
*
* @param[in] mode  Reserved, must be set to 0
* @param[in] pwdIndex  Which password to set: @li 0 = Service Switch/ARS 1, @li 1 = Service Switch/ARS 2
* @param[in] pwd  Encrypted password (Using Ux encryption), 8 bytes
*
* @return
* @li 0 = Success
* @li 1 = Bad input parameter,
* @li 2 = SPWEK not present
*/
/*SVC_PROTOTYPE*/int security_setUxPassword(int mode, int pwdIndex, char *pwd);


/** Securely present Service Switch/ARS 1 and Service Switch/ARS 2 passwords and attempt to reset an active ARS if passwords match.
*
* @param[in] mode  Reserved, must be set to 0
* @param[in] pwdIndex  Which password to set: @li 0 = Service Switch/ARS 1, @li 1 = Service Switch/ARS 2
* @param[in] pwd  Encrypted password (Using Ux encryption), 8 bytes
*
* @return
* @li 0 = Success
* @li 1 = Bad input parameter
* @li 2 = SPWEK not present
* @li 3 = ARS reset failed
* @li 4 = 60sec timeout exceeded
*/
/*SVC_PROTOTYPE*/int security_resetUxARS(int mode, int pwdIndex, char *pwd);


/** Read CPU NVM Parameters
 * @return
 * Struct securityCPUParameters
 */
/*SVC_PROTOTYPE*/ struct securityCPUParameters security_getCpuParameters(void);


/** Clear Tamper - For manufacturing and development use only!
*/
/*SVC_PROTOTYPE*/ void security_clearTamper(void);

/** Service Switch operations
*
* @param[in] action  Operation to perform - See: Service Switch Actions
*
*  @return
 * @li 0 Not Triggered
 * @li 1 Triggered
 * @li < 0  Error
 * @li -4  Feature not enabled
*/
/*SVC_PROTOTYPE*/ int security_serviceSwitch(int action);


/** Read Service Switch Status
 *
 * @return
 * @li 0  Not Triggered
 * @li 1 Triggered
 * @li < 0 = Error
*/
/*SVC_PROTOTYPE*/ int security_readServiceSwitch(void);



/** Read the Tamper Log
*
*
* @return Struct tamperRecords
*/
/*SVC_PROTOTYPE*/ struct tamperRecords security_getTamperLog(void);


/** Authenticate a File
*
* @param[in] filename String pointer to fullpath of file to authenticate
* @param[in] gid  string pointer to group id
*
* @return
* @li 1 = Authentication Passed
* @li Any other return value is a fail!
*/
/*SVC_PROTOTYPE*/ int security_authFile(char *filename, char *gid);



/*SVC_STRUCT*/
/**
 * Web source authentication status
 */
struct securityWebAuth {
  int cgiSource;		/**< @li 0 = Not from a cgi source @li 1 = cgi source */
  int authenticated;	/**< @li 0 = Not authenticated @li 1 = authenticated web page */
};

#ifndef DOXYGEN_OMIT
/** Indicates if the originator is from a cgi call via the web, and if so,
 *  was the source authenticated or not.
 *
 * @note
 * @li This call can be used in a service client-side call to
 *        know if the caller has been authenticated (from "signed" code).
 *        Knowing this can be used to determine how to handle sensitive data, etc.
 * @li Any calls from "c" or php in the terminal are by definition from
 *        a authenticated source ("signed" code).
 * @li /home/sys2/security_authpage - Executable that can be called from PHP via
 *     exec() command to return a 2 element array containing cgiSource and authenticated values.
 * @li If WDE is not installed, then this method returns 0 for both cgiSource and authenticated.
 * @li This call should NOT be called from php. To determine if request was made from a
 *      secure/authenticated page, php should use:  /home/sys2/security_authPage
 * @li If javascript calls a php module that in-turn calls security_authPage, the cgiSource
 *      parameter will be zero, since php made the request, and it is a seperate process from
 *      the javascript handler.
 *
 *
 * @return Struct securityWebAuth
 */
struct securityWebAuth security_webAuthSource(void);
#endif


/*SVC_STRUCT*/
struct cryptoFileDataBinary {
	void *data;                 /**< Buffer containing data read/written */
	int   data_count;           /**< Amount of data (in bytes) in buffer */
};



/**
 * @defgroup SDR Secure Data Stores
 *  These methods are reading/writing Secure Data Stores that are file based.
 */


/** Write data encrypted to filename. All data to be written must be contained in data.buffer
 * @ingroup SDR
 *
 * @param[in] filename Name of file with pathing to be written (encrypted)
 * @param[in] bdata Bdata.data contains data to be written, bdata.data_count is number of bytes in buffer to be encrypted.
 *
 * @return
 * @li 0 Success
 * @li -1 Error
 *
 * On error return, errno:
 * <b>Errno values</b>:
 * @li EINVAL - Invalid input parameters (filename must be defined, bdata.data_count > 0)
 * @li EACCES - Unable to open/access filename or not accessible from a non-secure WDE source.
 * @li EFAULT - Problem encrypting and/or writing to file.
 *
 */
/*SVC_PROTOTYPE*/ int security_cryptoWriteBinary(char *filename, struct cryptoFileDataBinary bdata);


/** Read encrypted data from filename placing result in returned data buffer and size
 * @ingroup SDR
 *
 * @param[in] filename Name of file with pathing to be read (and unencrypted)
 * @param[in] maxSize  Read upto maxSize from encrypted file (may be less)
 *
 * @return
 * Struct cryptoFileData - Upon success, data will contain data, data_count will contain amount of data.
 *
 * errno == 0 indicates successful and buffer contains data. Note: user must free() buffer when done processing.
 *
 * On error return, errno:
 * <b>Errno values</b>:
 * @li EACCES - No such file or not accessible from a non-secure WDE source.
 * @li ENOMEM - Unable to malloc space for data.
 * @li EFAULT - Problem reading file and/or unencrypting.
 *
 */
/*SVC_PROTOTYPE*/ struct cryptoFileDataBinary security_cryptoReadBinary(char *filename, int maxSize);


/** Write ASCII string encrypted to filename.
 * @ingroup SDR
 *
 * @param[in] filename  Name of file with pathing to be written (encrypted)
 * @param[in] buffer  ASCII null terminated string to be written encrypted.
 *
 * @return
 * @li 0 Success
 * @li -1 on error
 *
 * On error return, errno:
 * <b>Errno values</b>:
 * @li EACCES - Unable to open/access filename or not accessible from a non-secure WDE source.
 * @li EFAULT - Problem encrypting and/or writing to file.
 *
 */
/*SVC_PROTOTYPE*/ int security_cryptoWriteASCII(char *filename, char *buffer);


/** Read encrypted data string from filename placing result in returned buffer null terminated
 * @ingroup SDR
 *
 * @param[in] filename Name of file with pathing to be read (and unencrypted)
 * @param[in] maxSize Read upto maxSize from encrypted file (may be less)
 *
 * @return
 * buffer  Upon success, buffer will contain ASCII data, null terminated.
 *
 * errno == 0 indicates successful and buffer contains data.
 *
 * @note User must free() buffer when done processing.
 *
 * <b>Errno values</b>:
 * @li EACCES - No such file or not accessible from a non-secure WDE source.
 * @li EBADF - Bad return data format (uncrypted file, but data not ASCII).
 * @li ENOMEM - Unable to malloc space for data.
 * @li EFAULT - Problem reading file and/or unencrypting.
 *
 */
/*SVC_PROTOTYPE*/ char *security_cryptoReadASCII(char *filename, int maxSize);





/**
 * @defgroup IPP IPP PIN Entry
 *  These methods are for IPP based PIN Entry.
*/


/**
 * @name PIN Entry Parameter options (may be OR'd together)
 * @ingroup IPP
*/
/** \{ */
#define PE_ENABLE_AUTO_ENTER       1 /**< Turns Auto Enter feature ON */
#define PE_ACCEPT_NO_PIN           2 /**< Accepts NO PIN entry (allow pressing ENTER before any digit) */
#define PE_CLEAR_SAMEAS_BACKSPACE  8 /**< Makes the CLEAR key behave like a backspace key (only single digit is removed) */
#define PE_CLEAR_CANCELS_NO_PIN   16 /**< Cancels the PIN session when the CLEAR key is pressed when No PIN digits entered yet */
/** \} */


/*SVC_STRUCT*/
/**
 * PIN Entry parameters
 * @ingroup IPP
 */
struct securityPinParams {
	int keyType;    /**< 0 for MasterKey Management, 1 for DUKPT */
	int minPinSize; /**< Minimum number of PIN digits. Must be in range [4...12] inclusive */
	int maxPinSize; /**< Maximum number of PIN digits. Must be at least minPinSize, but not greater than 12 */

	int zeroPinAllowed;     /**< Zero PIN length allowed: 0 - not permitted, 1 - permitted */
	char pan[20];           /**< Personal Account Number (8...19 characters - null terminated) */
	char workingKey[128];   /**< 1DES - 16 characters, 3DES - 120 characters (null terminated) */
	int masterKey;          /**< 0...9 for MasterKey Management key, 0...2 for DUKPT to select DUKPT engine */

	int options;                /**< Options for PIN entry behavior (see defines) */
	int maxTimeout/*0*/;        /**< Maximum number of seconds to complete PIN Entry (0 - no timeout, max = 300) */
	int firstDigitTimeout/*0*/; /**< Maximum seconds to wait until first digit (0 - no timeout, max = 300) */
	int innerDigitTimeout/*0*/; /**< Maximum seconds to wait between digits (0 - no timeout, max = 300) */
};



/*SVC_STRUCT*/
struct securityPinEntryDiag {
	int testType;           /**< @li 0 - ROM checksum @li 1 - Serial Number @li 2 - ROM version @li 3 - Master/Session Encryption @li 4 - DUKPT Encryption */
	int masterKey;          /**< @li 0...9 for MasterKey Management key @li 0...2 for DUKPT to select DUKPT engine */
	char result[150];      /**< Contains result of diagostic when returned (ignored when passed) */
	int status;            /**< Status of diag result */
};

/*SVC_STRUCT*/
struct securityPinEntryResult {
	int status;                     /**< Status of this Pin Entry result (see Pin Entry Status defines below) */
	char encryptedPinBlock[32];     /**< Encrypted PIN block NULL terminated */
	char keySerialNum[32];          /**< Key serial number (DUKPT only) Null terminated */
};


/**
 * @name Pin Entry Status (via event)
 * @ingroup IPP
*/
/** \{ */
#define PINENTRY_STATUS_DIGIT_ENTERED		 1 /**< Digit entered by user */
#define PINENTRY_STATUS_BACKSPACE_ENTERED	 2 /**< Backspace key entered by user */
#define PINENTRY_STATUS_CLEAR_ENTERED		 3 /**< Clear key (<-) entered by user */
#define PINENTRY_STATUS_CANCEL_ENTERED		 4 /**< Cancel key (x) entered by user */
#define PINENTRY_STATUS_PINBLOCK_AVAILABLE	 5 /**< Return key entered by user */
#define PINENTRY_STATUS_END					10 /**< End requested before PIN entry complete */
#define PINENTRY_STATUS_CANCELED			11 /**< Canceled before PIN entry complete */
#define PINENTRY_STATUS_NULL_PIN			12 /**< NULL PIN entered */
#define PINENTRY_STATUS_IPP_ERROR           20 /**< IPP Command/Communication Error */
#define PINENTRY_STATUS_NO_DUKPT_KEY		21 /**< No DUKPT key(s) and requested PIN entry using DUKPT key */
#define PINENTRY_STATUS_NO_MASTER_KEY		22 /**< No Master session key and requested PIN entry using Master session */
#define PINENTRY_STATUS_MAXTIMEOUT			30 /**< Max timeout to enter PIN exceeded */
#define PINENTRY_STATUS_FIRSTDIGITTIMEOUT	31 /**< Timeout for user to enter first PIN digit exceeded */
#define PINENTRY_STATUS_INNERDIGITTIMEOUT	32 /**< Timeout for inner digit entry exceeded */
/** \} */




/** Perform a PIN entry diagnostic
 * @ingroup IPP
 *
 * @param[in] param Structure containing parameters (see struct securityPinEntryDiag)
 *
 * @return
 * Struct securityPinEntryDiag - Upon success, result will contain info.
 *
 * errno == 0 indicates successful and passed diag.
 *
 * In Returned securityPinEntryDiag structure:
 *    @li status = 0 - success @li  -1 - Error
 *
 * Result buffer contains:
 *   @li testType - 0 : result contains checksum
 *   @li testType - 1 : serial number if present
 *   @li testType - 2 : version number
 *   @li testType - 3-4 : encrypted PIN block
 *
 * On error, status = -1, errno:\n
 * <b>Errno values</b>:
 * @li EACCES - Unable to begin PIN session.
 * @li ENOENT - Unable to obtain PAN (when params.pan[0] == 0)
 * @li EBADF - Unable to create/open event required for pin Entry session
 * @li EAGAIN - Too many PIN entry requests in a short period - retry later
 * @li EINVAL - Invalid argument(s) passed or IPP communication error.
 */
/*SVC_PROTOTYPE*/ struct securityPinEntryDiag security_pinEntry_diag(struct securityPinEntryDiag param);



/** Begin a pin entry session
 * @ingroup IPP
 *
 * @param[in] params Structure containing parameters (see struct securityPinParams)
 *
 * @return
 * @li Status = 0 Success
 * @li -1 on error
 *
 * @note
 * If params.pan[] == "" (empty), then internally, last msr read is used to obtain PAN.
 *
 * <b>Errno values</b>:
 * @li EACCES - Unable to begin PIN session.
 * @li ENOENT - Unable to obtain PAN (when params.pan[0] == 0)
 * @li EBADF  - Unable to create/open event required for pin Entry session
 * @li EAGAIN - Too many PIN entry requests in a short period - retry later
 * @li EINVAL - Invalid argument(s) passed.
 * @li ENXIOL - IPP communication error.
 */
/*SVC_PROTOTYPE*/ int security_pinEntry_begin(struct securityPinParams params);


/** Read event from a security_pinEntry_begin() operation
 * @ingroup IPP
 *
 * @param[in] flags  Get Event Flags:
 *                      @li 0 = Blocking
 *                      @li 1 = Non-block
 *                      @li 2 = Last
 *                      @li 4 = Cancel (options can be or'd)
 *
 * @return
 * status >= 0 indicates which timeout expired or if operation cancelled.
 *
 * Status >= 0 (see defines above PINENTRY_STATUS_xxxx):
 * @li PINENTRY_STATUS_DIGIT_ENTERED  - Digit entered
 * @li PINENTRY_STATUS_BACKSPACE_ENTERED  - Backspace entered
 * @li PINENTRY_STATUS_CLEAR_ENTERED  - Clear entered
 * @li PINENTRY_STATUS_PINBLOCK_AVAILABLE  - Return entered (done) - encrypted PIN block is located in dataStore (see security_pinEntry_begin())
 * @li PINENTRY_STATUS_CANCELED - Canceled or commanded to end without completing PIN entry (see security_pinEntry_cancel())
 * @li PINENTRY_STATUS_MAXTIMEOUT - Max timeout exceeded
 * @li PINENTRY_STATUS_FIRSTDIGITTIMEOUT - Max timeout to first PIN digit exceeded
 * @li PINENTRY_STATUS_INNERDIGITTIMEOUT - Max timeout between PIN digits exceeded
 *
 *<b>Errno values</b>:
 * @li EFAULT - Invalid eventkey, or unable to cancel previous security_pinEntry_getEvent() in progress.
 * @li EAGAIN - No last event (when flags requesting last).
 * @li ENODATA - No event data available (when flags set to non-block and no event).
 * @li EBUSY - Event cancel already in progress, and attempting to cancel event (flags == 4).
 * @li ENODEV - Internal device error.
 * @li EINTR - getEvent canceled (by another thread/user).
 * @li ENOMEM - Unable to allocate needed memory to carryout request
 */
/*SVC_PROTOTYPE*/ int security_pinEntry_getEvent(int flags /* 0 */);


/** Close PIN Entry event
 *  This also closes the PIN entry device. Any unread data is lost.
 * @ingroup IPP
 *
 * @return
 * @li If >= 0 Success
 * @li If = -1 then error
 *
 * <b>Errno values</b>:
 * @li EINVAL - Invalid eventkey passed.
 * @li EFAULT - Error while closing event.
 */
/*SVC_PROTOTYPE*/ int security_pinEntry_closeEvent(void);


/** Used to obtain the encrypted PIN Block after successful PIN Entry
 * This method also closes the PIN entry device, data can NOT be read again.
 * @ingroup IPP
 *
 * @return
 * Struct securityPinEntryResult - Upon success, data will contain encrypted PIN block, data_count will contain size.
 *
 *  errno == 0 indicates successful and encrypted PIN block returned.
 *
 * @note
 *    @li User must free() data buffer when done processing.
 *    @li Upon success, referenced dataStore will be destroyed
 *
 * <b>Errno values</b>:
 * @li EBUSY - Still obtaining PIN block.
 * @li EBADF - No PIN block data available (perhaps already obtained).
 */
/*SVC_PROTOTYPE*/ struct securityPinEntryResult security_pinEntry_getEncryptedPinBlock(void);


/** Cancel a PIN entry in progress
 *
 *  This method is used to cancel PIN entry that was started
 *  from: security_pinEntry_begin()
 * @ingroup IPP
 *
 * @return 0
 */
/*SVC_PROTOTYPE*/ int security_pinEntry_cancel(void);




/**
 *  @defgroup SDB Secure Data Buffer methods
 *  These methods are reading/writing Secure Data Buffer.
 */


/**
 * @name Encryption Type
 * @ingroup SDB
 */
/** \{ */
#define ENCRYPTION_AES     1 /**< Encryption using AES computations on 128, 192, 256-bit key*/
#define ENCRYPTION_DEAX    2 /**< Encryption using DES DEAX  single, double, triple length key */
#define ENCRYPTION_DEA     3 /**< Encryption using DES DEA single length key */
#define ENCRYPTION_TDES    4 /**< Encryption using DES TDES double, triple length key */
/** \} */


/** Used to Encrypt or Decrypt a given data buffer using specified algorithm and key.
 * @ingroup SDB
 *
 * @param[in] type  Type of encryption (See: Encryption Type defines).
 * @param[in] encrypt Indicates if encrypting (>0), or decrypting (<=0)
 * @param[in] keylen  Length of key (in 8-bytes), values are: 1, 2, 3, or 4
 * @param[in] keyfile1  File containing encrypted key: 1st 8-bytes, or 1st and 2nd 8-bytes.
 *                       (this param may be NULL if the key is specified in the key param).
 * @param[in] keyfile2  File containing encrypted key: 3rd 8-bytes, or 3rd and 4th 8-bytes.
 *                       (this param may be NULL for a single or double length key).
 * @param[in] key  Contains encryption key (only used if keyfile1 == NULL).
 * @param[in] input  CryptoFileDataBinary structure containing input data to be encrypted/decrypted.
 * @param[in] padbyte  Contains a binary value that will be used as padding at the end
 *                        of the input data to make the data size a multiple of 8 for DES
 *                        or 16 for AES.
 *
 * @return
 * Struct cryptoFileDataBinary - Upon success, result will a malloc'd buffer containing
 *                                       encrypted/decrypted data, data_count will contain size.
 * errno == 0 indicates successful and encrypted/decrypted data returned.
 *
 * @note
 *    @li AES encryption for 128-bit key has keylen = 2, 192 keylen = 3, 256 keylen = 4.
 *    @li DES DEAX encryption has keylen: single keylen = 1, double keylen = 2, triple keylen = 3.
 *    @li DES DEA encryption has keylen: single keylen = 1.
 *    @li DES TDEA encryption has keylen: double keylen = 2, triple keylen = 3.
 *    @li AES encryption will be computed on a 16 byte input data block.
 *    @li DES encryption will be computed on a 8 byte input data block.
 *    @li If the input data size is not a multiple of 16 for AES or 8 for DES,
 *           the specified padding byte will be applied at the end of the input data.
 *    @li Upon success, return data contains a malloc'd data buffer, the caller is
 *           responsible to free this buffer.
 *
 *
 *<b>Errno values</b>:
 * @li EINVAL - Invalid parameter
 * @li EACCES - No such keyfile (1 or 2).
 * @li ENOMEM - Unable to malloc space for data.
 * @li EFAULT - Problem reading keyfile and/or unencrypting.
 * @li ERANGE - Keylen(*8) is greater than size of key (key is too small for requested encryption).
 * @li EBADR  - Problem encrypting using specified key, data, and type
 */
/*SVC_PROTOTYPE*/ struct cryptoFileDataBinary security_encryptDecryptData(int type, int encrypt, int keylen, char *keyfile1, char *keyfile2, struct cryptoFileDataBinary key, struct cryptoFileDataBinary input, unsigned char padbyte );


/** Obtain the list of certificates used to pair UX devices
 *
 * @return
 * UxCertRecords structure containing the list of certificates - .certs must be freed.
 */
/*SVC_PROTOTYPE*/ struct uxCertRecords security_getUxCertificates(void);


/** Obtain the list of installed VRK(V), System(R) and User(U) RSA keys
 *
 * @return
 * Struct keyRSAlist - Upon success, keyRSAlist.total_keys will be more then 0
 *
 * @note
 * @li User must free() keyRSAlist.key.
 */
/*SVC_PROTOTYPE*/ struct keyRSAlist security_getRSAkeyList();

/** Obtain information about VRK key
 *
 * @param[out] VRKkey  pointer where loaded info will be stored
 * @return
 * @li -1    - on error
 * @li 0     - success
 */
/*SVC_PROTOTYPE*/ int security_getVRKkeyInfo(struct keyRSAinfo *VRKkey);

/**
 * This function retrieves information about security keys.
 * @note After the retrieved pointer is no longer needed, it should
 * be passed to function free() to release memory.
 *
 * @return						Pointer to a JSON block containing information
 * 								about security keys. Its structure can be
 * 								verified against JSON schema located in
 * 								json/security_keys.json
 */
/*SVC_PROTOTYPE*/ char *security_getWarrantiedKeys();

/**
 * This function retrieves information about expected keys.
 * @note After the retrieved pointer is no longer needed, it should
 * be passed to function free() to release memory.
 *
 * @return						Pointer to a JSON block containing information
 * 								about expected keys. Its structure can be
 * 								verified against JSON schema located in
 * 								json/security_keys.json
 */
/*SVC_PROTOTYPE*/ char *security_getExpectedKeys();

/**
 * This function retrieves information about payment keys.
 * @note After the retrieved pointer is no longer needed, it should
 * be passed to function free() to release memory.
 *
 * @return						Pointer to a JSON block containing information
 * 								about expected keys. Its structure can be
 * 								verified against JSON schema located in
 * 								json/payment_keys.json
 */
/*SVC_PROTOTYPE*/ char *security_getPaymentKeys();

/**
 * Data exchange buffer
 */
/*SVC_STRUCT*/
struct securityDataBuffer {
	void* data;	/** Data buffer */
	int data_count;/** Data length */
};


/**
 *  @defgroup FSM Fiscal Security Module 
 *  These methods are implementing functionality required for managing the FSM.
 */

/** Used to obtain the X509 certificates from the Key Storage.
 * @ingroup FSM
 *
 * @param[in] pkiType	type of PKI (0 = VeriFone PKI)
 * @param[in] level		requested certificate level (0 = ROOT CA, 1 = DEVICE CA, ...)
 * 
 * @return struct securityDataBuffer - Upon success, data will contain certificate raw data (DER format), dataLen will contain certificate size.
 * errno == 0 indicates successful and certificate data returned.
 *
 * Notes:
 *    @li 1. User must free() data buffer when done processing.
 *    @li 2. The function returns certificates from the TLS certificate tree.
 *
 * On error return, errno:
 * @li EINVAL - Invalid argument.
 * @li EFAULT - Certificate tree is unavailable.
 * @li ENOENT - No requested certificate available (perhaps reach the certificate chain end).
 * @li ENOMEM - Out of memory.
 * @li EFAULT - Service Manager is unavailable.
 * @li EIO    - I/O error.
 */
/*SVC_PROTOTYPE*/ struct securityDataBuffer security_read_certificate( int pkiType, int level );


/** Used to initiates the Initial Key Exchange.
 * @ingroup FSM
 *
 * @return struct securityDataBuffer - Upon success, data will contain cryptogram #1, dataLen will contain cryptogram size.
 * errno == 0 indicates successful and cryptogram data returned.
 *
 * Notes:
 *    @li 1. User must free() data buffer when done processing.
 *    @li 2. The cryptogram length is 16 bytes.
 *
 * On error return, errno:
 * @li EINVAL - Invalid argument
 * @li ENOMEM - Out of memory
 */
/*SVC_PROTOTYPE*/ struct securityDataBuffer security_fsm_initiateXCHG( void );


/** This function finalizes the Initial Key Exchange.
 * @ingroup FSM
 *
 * @param[in] crypto2	structure containing cryptogram #2 to process
 * 
 * @return struct securityDataBuffer - Upon success, data will contain cryptogram #3, dataLen will contain cryptogram size.
 * errno == 0 indicates successful and cryptogram data returned.
 *
 * Notes:
 *    @li 1. User must free() data buffer when done processing.
 *    @li 2. The cryptogram length is 16 bytes.
 *
 * On error return, errno:
 * @li EINVAL   - Invalid argument
 * @li E2BIG    - Cryptogram is too large
 * @li ENOENT   - Host RSA key not found
 * @li EIO      - Could not decrypt cryptogram
 * @li EFAULT   - Decryption error
 * @li EMSGSIZE - Wrong decrypted data length
 * @li EBADE    - The controller nonce (Ra) mismatch
 * @li ENOKEY   - Could not store DLPK
 * @li ENOMEM   - Out of memory
 */
/*SVC_PROTOTYPE*/ struct securityDataBuffer security_fsm_finalizeXCHG( struct securityDataBuffer crypto2 );


/** Used to perform the Session Key Exchange.
 * @ingroup FSM
 *
 * @return struct securityDataBuffer - Upon success, data will contain datagram (S || IV || N || DPSK), 
 * dataLen will contain cryptogram size.
 * errno == 0 indicates successful and cryptogram data returned.
 *
 * Notes:
 *    @li 1. User must free() data buffer when done processing.
 *    @li 2. The cryptogram length is 80 bytes.
 *
 * On error return, errno:
 * @li EINVAL - Invalid argument
 * @li ENOMEM - Out of memory
 * @li ENOKEY - DLPK not exist
 * @li EIO    - Data processing error
 */
/*SVC_PROTOTYPE*/ struct securityDataBuffer security_fsm_setup_link( void );


/** Used to encrypt/decrypt data (AES-CCM, 256-bits).
 * @ingroup FSM
 *
 * @param[in] mode			operation type
 *    @li 0 - encrypt
 *    @li 1 - decrypt
 * @param[in] counter		message counter
 * @param[out] out			buffer to place data
 * @param[in/out] out_len	output buffer size/processed data size
 * @param[in] in			data to process
 * @param[in] in_len		data length
 * 
 * @return 
 *    @li -1    - on error
 *    @li 0     - success
 *
 * On error return, errno:
 * @li EINVAL - Invalid argument
 * @li E2BIG  - Input data is too large
 * @li ENOMEM - Out of memory
 * @li ENOKEY - DPSK not exist
 * @li EIO    - Data processing error
 * @li EBADF  - Message authentication failed
 * @li ENOSPC - Output buffer is too small
 */
/*SVC_PROTOTYPE*/ int security_fsm_process_data( int mode, int counter, unsigned char *out, int *out_len, unsigned char *in, int in_len );

/**
 *  @defgroup NSP Needham-Schroeder Protocol 
 *  These methods are implementing NS protocol based on X509 certificates.
 */

/** Used to obtain the X509 certificates from the Key Storage.
 * @ingroup NSP
 *
 * @param[in] pkiType	type of PKI (0 = VeriFone PKI)
 * @param[in] level		requested certificate level (0 = ROOT CA, 1 = DEVICE CA, ...)
 * 
 * @return struct securityDataBuffer - Upon success, data will contain certificate raw data, dataLen will contain certificate size.
 * errno == 0 indicates successful and certificate data returned.
 *
 * Notes:
 *    @li 1. User must free() data buffer when done processing.
 *    @li 2. The function returns certificates from the NS certificate tree.
 *
 * On error return, errno:
 * @li EINVAL - Invalid argument.
 * @li EFAULT - Certificate tree is unavailable.
 * @li ENOENT - No requested certificate available (perhaps reach the certificate chain end).
 * @li ENOMEM - Out of memory.
 * @li EIO    - I/O error.
 */
/*SVC_PROTOTYPE*/ struct securityDataBuffer security_ux_read_certificate( int pkiType, int level );


/** Used to build and verify the X509 certificates tree.
 * @ingroup NSP
 *
 * @param[in] pkiType	type of PKI (0 = VeriFone PKI, 1 = Gilbarco PKI)
 * @param[in] level		certificate level (0 = ROOT CA, 1 = DEVICE CA, ..., -1 = destroy certificate chain)
 * @param[in] cert		structure containing certificate to load
 * 
 * @return 0 - function successfully completed, or -1 - an error occurs. Check the errno value.
 *
 * Notes:
 *    @li 1. The self-signed certificate will be assumed as a ROOT CA and not accepted.
 *    @li 2. All certificated MUST be loaded in sequence.
 *    @li 3. The certificate size should not exceed the 2048 bytes.
 *    @li 4. The loading sequence MUST be initiated by the function call with level=0. 
 *           No certificate is required in this case. The function will retrieve the ROOT CA 
 *           from the device trusted storage and store it into the certificate chain.
 *
 * On error return, errno:
 * @li EINVAL  - Invalid argument.
 * @li EFBIG   - Certificate is too large.
 * @li EBADSLT - Certificate chain not initialized
 * @li EBADR   - Self-signed certificate.
 * @li EBADF   - Certificate verification failed.
 * @li EIO     - I/O error.
 */
/*SVC_PROTOTYPE*/ int security_ux_load_certificate( int pkiType, int level, struct securityDataBuffer cert );


/** Used to initiates Needham-Schroeder protocol.
 * @ingroup NSP
 *
 * @param[in] mode	bits 28-29: channel selection [0..3] other bits must be set to 0 (RFU)
 * 
 * @return struct securityDataBuffer - Upon success, data will contain cryptogram #1, dataLen will contain cryptogram size.
 * errno == 0 indicates successful and cryptogram data returned.
 *
 * Notes:
 *    @li 1. User must free() data buffer when done processing.
 *    @li 2. The cryptogram length is the same size as peripheral RSA key modulo length.
 *
 * On error return, errno:
 * @li EPERM  - Operation not permitted (unit is an active tamper)
 * @li EINVAL - Invalid argument.
 * @li ENOENT - Peripheral RSA key not found.
 * @li EBADF  - Could not decode RSA key
 * @li ENOMEM - Out of memory.
 * @li EACCES - Permission denied.
 * @li EIO    - I/O error.
 */
/*SVC_PROTOTYPE*/ struct securityDataBuffer security_ux_initiateNS( int mode );


/** This function implements the second step of the Needham-Schroeder protocol.
 * @ingroup NSP
 *
 * @param[in] mode		bits 28-29: channel selection [0..3] other bits must be set to 0 (RFU)
 * @param[in] crypto1	structure containing cryptogram #1 to process
 * 
 * @return struct securityDataBuffer - Upon success, data will contain cryptogram #2, dataLen will contain cryptogram size.
 * errno == 0 indicates successful and cryptogram data returned.
 *
 * Notes:
 *    @li 1. User must free() data buffer when done processing.
 *    @li 2. The cryptogram length is the same size as host RSA key modulo length.
 *
 * On error return, errno:
 * @li EPERM  - Operation not permitted (unit is an active tamper)
 * @li EINVAL - Invalid argument.
 * @li EFBIG  - Cryptogram is too large.
 * @li ENOENT - Host RSA key not found.
 * @li EBADF  - Could not decode RSA key
 * @li ENOMEM - Out of memory.
 * @li EACCES - Permission denied.
 * @li EIO    - I/O error.
 */
/*SVC_PROTOTYPE*/ struct securityDataBuffer security_ux_resolveNS( int mode, struct securityDataBuffer crypto1 );


/** This function finalizes the Needham-Schroeder protocol.
 * @ingroup NSP
 *
 * @param[in] mode		bits 28-29: channel selection [0..3] other bits must be set to 0 (RFU)
 * @param[in] crypto2	structure containing cryptogram #2 to process
 * 
 * @return struct securityDataBuffer - Upon success, data will contain cryptogram #3, dataLen will contain cryptogram size.
 * errno == 0 indicates successful and cryptogram data returned.
 *
 * Notes:
 *    @li 1. User must free() data buffer when done processing.
 *    @li 2. The cryptogram length is 16 bytes.
 *
 * On error return, errno:
 * @li EPERM  - Operation not permitted (unit is an active tamper)
 * @li EINVAL - Invalid argument.
 * @li EFBIG  - Cryptogram is too large.
 * @li ENOENT - Host RSA key not found.
 * @li ENOMEM - Out of memory.
 * @li EACCES - Permission denied.
 * @li EIO    - I/O error.
 */
/*SVC_PROTOTYPE*/ struct securityDataBuffer security_ux_finalizeNS( int mode, struct securityDataBuffer crypto2 );


/** This function finalizes the Needham-Schroeder protocol.
 * @ingroup NSP
 *
 * @param[in] mode		bits 28-29: channel selection [0..3] other bits must be set to 0 (RFU)
 * @param[in] crypto3	structure containing cryptogram #3 to process
 * 
 * @return 0 - function successfully completed, or -1 - an error occurs. Check the errno value.
 *
 * On error return, errno:
 * @li EPERM  - Operation not permitted (unit is an active tamper)
 * @li EINVAL - Invalid argument.
 * @li ENOENT - Peripheral RSA key not found.
 * @li EACCES - Permission denied.
 * @li EIO    - I/O error.
 */
/*SVC_PROTOTYPE*/ int security_ux_verifyNS( int mode, struct securityDataBuffer crypto3 );


/** Used to obtain the random session key for PIN or password encryption.
 * @ingroup NSP
 *
 * @param[in] mode		bits 28-29: channel selection [0..3]
 *                      bit 0: session key encryption mode (0 = DES ECB with key dual size, 1 = DES CBC with key dual size)
 * @param[in] keyType	requested key type (0 = session PIN key, 1 = session password key)
 * 
 * @return struct securityDataBuffer - Upon success, data will contain session key, dataLen will contain key size.
 * errno == 0 indicates successful and session key returned.
 *
 * Notes:
 *    @li 1. User must free() data buffer when done processing.
 *
 * On error return, errno:
 * @li EINVAL - Invalid argument.
 * @li ENOENT - PINKEK/PWDKEK not present.
 * @li ENOMEM - Out of memory.
 * @li EIO    - I/O error.
 */
 /*SVC_PROTOTYPE*/ struct securityDataBuffer security_ux_get_session_key( int mode, int keyType );


/** The function injects the PIN or password encrypted block to the Vault.
 * @ingroup NSP
 *
 * @param[in] mode		bits 28-29: channel selection [0..3]
 *                      	bit 0: block encryption mode (0 = DES ECB with key dual size, 1 = DES CBC with key dual size)
 * @param[in] blockType		ciphered block type (0 = PIN block, 1 = password block)
 * @param[in] cipherBlock	structure containing ciphered block to process
 * 
 * @return 0 - function successfully completed, or -1 - an error occurs. Check the errno value.
 *
 * On error return, errno:
 * @li EINVAL - Invalid argument.
 * @li EFBIG  - Ciphered block is too large.
 * @li ENOENT - SPINEK/SPWDEK not present.
 * @li ETIME  - Too many calls in a short period of time. 
 *              The function is limited to a call every 30 seconds on average to deter exhaustive PIN/PWD search.
 * @li EAGAIN - Vault is in wrong mode. Use iPS_SelectPINAlgo() to select corresponded PIN/PWD mode.
 * @li EIO    - I/O error.
 */
/*SVC_PROTOTYPE*/ int security_ux_load_cipher_block( int mode, int blockType, struct securityDataBuffer cipherBlock );


/** This function converts production terminal to CP test terminal.
 *
 * @return 0 - function successfully completed, or -1 - an error occurs. Check the errno value.
 *
 * On error return, errno:
 * @li EPERM  - Operation not permitted (unit already converted or invalid)
 * @li ENOMEM - Out of memory.
 * @li EACCES - Permission denied.
 */
/*SVC_PROTOTYPE*/ struct cptest_status_st* security_cptest_convert( void);


/** Read event from a Securityoperation - CP Test conversion
 *
 * @param[in] flags Get Event Flags:
 *                      @li 1 = Non-block
 *                      @li 2 = Last
 *                      @li 4 = Cancel (options can be or'd)
 *
 * @return
 * Struct cptest_status_st - status will reflect current CP test conversion status
 *
 */
/*SVC_PROTOTYPE*/ struct cptest_status_st security_cptest_get_event( int flags );


/** Check if network is correctly configured for CP Test conversion
 *
 * @return
 * int - 0 (CPTEST_SUCCESS) if network defaults to ethenet over USB, -1 (CPTEST_ERROR)
 * otherwise
 *
 */
/*SVC_PROTOTYPE*/ int security_cptest_check_network_config( void );


/** Request CPTest conversion cancel. Depending on how far advanced the conversion process is
 * this request  may be denied.
 * @return
 * struct cptest_status_st Status  contains status of request ( CPTEST_ERROR or CPTEST_SUCCESS
 *  and text message
 *
 */
/*SVC_PROTOTYPE*/ struct cptest_status_st * security_cptest_cancel_request( void );

/**  checks if conversion of terminal is permitted
 *  - i.e. is it an unlocked PROD terminal that hasn't already been converted
 * @return
 * int - 0 (CPTEST_STATUS_SUCCESS) if conrversion permitted,  else  the following errors
 * indicating that conversion is not permitted are returned:
 *    -5  - CPTEST_STATUS_TAMPER
 *    -4  - CPTEST_STATUS_NON_PROD
 *    -3  - CPTEST_STATUS_LOCKED
 *    -2  - CPTEST_STATUS_ALREADY_CONVERTED
 *    -1  - CPTEST_STATUS_ERROR 
*/

/*SVC_PROTOTYPE*/ int security_cptest_check_conversion_permission( void );


/** The function gets the PIN ciphered by the Pin working key from the Vault.
 * @ingroup NSP
 *
 * @param[in] mode		bits 28-29: channel selection [0..3]
 *                      	bit 0: block encryption mode (0 = DES ECB with key dual size, 1 = DES CBC with key dual size)
 * @param[in] workingKey       	structure containing PIN working key
 * 
 * @return struct securityPinBuffer - Upon success, dataWK will contain the Pin session key, dataLen will contain the PIN ciphered key size.
 * errno == 0 indicates successful and session key returned. 
 *
 * On error return, errno:
 * @li EINVAL - Invalid argument.
 * @li EFBIG  - Ciphered block is too large.
 * @li ENOENT - SPINEK/SPWDEK not present.
 * @li ETIME  - Too many calls in a short period of time. 
 *              The function is limited to a call every 30 seconds on average to deter exhaustive PIN/PWD search.
 * @li EAGAIN - Vault is in wrong mode. Use iPS_SelectPINAlgo() to select corresponded PIN/PWD mode.
 * @li EIO    - I/O error.
 */
/*SVC_PROTOTYPE*/ struct securityDataBuffer  security_ux_get_cipher_pinblock( int mode, struct securityDataBuffer workingKey );

/** The function sets stunnel configuration
 *
 * @return 0 - function successfully completed, or -1 - an error occurs. Check the errno value.
 *
 * On error return, errno:
 * @li EINVAL - Invalid configuration.
 * @li EACCES - Permission denied.
 */
/*SVC_PROTOTYPE*/ int security_stunnel_setConfig( struct stunnelConfig conf );

/** The function returned stunnel configuration
 *
 * @return 0 - function successfully completed, or -1 - an error occurs. Check the errno value.
 *
 * On error return, errno:
 * @li ENOENT - No config.
 */
/*SVC_PROTOTYPE*/ struct stunnelConfig security_stunnel_getConfig( );

/** The function starts stunnel
 * 
 * @return 0 - function successfully completed, or -1 - an error occurs. Check the errno value.
 *
 * On error return, errno:
 * @li EBADF - Invalid configuration.
 * @li EEXIST - Stunnel already started
 * @li ENOKEY - SSL certificate not found.
 * @li EAGAIN - Stunnel failed to start 
 */
/*SVC_PROTOTYPE*/ int security_stunnel_start( );

/** The function stops stunnel
 *
 * @return 0 - function successfully completed, or -1 - an error occurs. Check the errno value.
 *
 * On error return, errno:
 * @li ESRCH - Stunnel is not running
 */
/*SVC_PROTOTYPE*/ int security_stunnel_stop( );

/**
 * @brief         Generates Cryprographic Challenge
 *
 * @param[in]     mode 		Working key encryption mode
 *				bits 28-29: channel selection [0..3]
 *				bit 0: block encryption mode (0 = DES ECB with key dual size, 1 = DES CBC with key dual size)
 *
 * @return 	struct securityChallenge - Upon success, data will contain challenge (8 bytes) in clear and the encrypted working key(16 bytes).
 * errno == 0 indicates successful and cryptogram data returned.
 *
 *
 * @par           Detailled function description:
 *                       This function generates a challenge Ra to be resolved by PP.
 *                       It also generates and sends a working key Pa, encrypted by PAK,
 *                       which has to be used in order to resolve the challenge.
 *
 * @return 0 - function successfully completed, or -1 - an error occurs. Check the errno value.
 *
 * On error return, errno:
 * @li EPERM  - Operation not permitted (unit is an active tamper)
 * @li EINVAL - Invalid argument.
 * @li ENOENT - Peripheral RSA key not found.
 * @li EACCES - Permission denied.
 * @li EIO    - I/O error.
 */
/*SVC_PROTOTYPE*/ struct securityChallenge security_generateChallenge( int mode );

/**
 * @brief         Resolves Cryprographic Challenge
 *
 * @param[in]     mode			Working key encryption mode (0 - DES ECB with dual key size)
 * 					bits 28-29: channel selection [0..3]
 *					bit 0: block encryption mode (0 = DES ECB with key dual size, 1 = DES CBC with key dual size)
 * @param[in]     challengeIn		Challenge in clear to resolve (8 bytes)
 * @param[in]     workingKey		Working key encrypted (16 bytes)
 * @param[out]    resolved challenge 	Encrypted challenge ( 8 bytes)
 * 
 * @return      struct securityResolvedChallenge - Upon success, data will contain resolved challenge encrypted (8 bytes).
 * errno == 0 indicates successful and cryptogram data returned..
 *
 * @par           Detailled function description:
 *                       This function resolves the given challenge.
 *                       It decrypts Pb with CAK and uses it to encrypt the challenge Rb.
 *
 * @return 0 - function successfully completed, or -1 - an error occurs. Check the errno value.
 *
 * On error return, errno:
 * @li EPERM  - Operation not permitted (unit is an active tamper)
 * @li EINVAL - Invalid argument.
 * @li ENOENT - Peripheral RSA key not found.
 * @li EACCES - Permission denied.
 * @li EIO    - I/O error.
*/
/*SVC_PROTOTYPE*/ struct securityResolvedChallenge  security_resolveChallenge( int mode, struct securityChallenge StChallenge );

/**
 * @brief         Verifies Cryprographic Challenge
 *
 * @param[in]	mode		Working key encryption mode (0 - DES ECB with dual key size)
 *				bits 28-29: channel selection [0..3]
 *                              bit 0: block encryption mode (0 = DES ECB with key dual size, 1 = DES CBC with key dual size)
 * @param[in]	StResolvedChallenge	Resolved challenge (8 bytes)
 *
 * @par           Detailled function description:
 *                       This function verifies that given value match with the challenge 
 *			generated during generateChallenge.
 *
 * @return 0 - function successfully completed, or -1 - an error occurs. Check the errno value.
 * 
 * On error return, errno:
 * @li EPERM  - Operation not permitted (unit is an active tamper)
 * @li EINVAL - Invalid argument.
 * @li ENOENT - Peripheral RSA key not found.
 * @li EACCES - Permission denied.
 * @li EIO    - I/O error.
*/
/*SVC_PROTOTYPE*/ int security_verifyChallenge(int mode, struct securityResolvedChallenge StResolvedChallenge);



#ifdef __cplusplus
}
#endif
#endif //SVC_SECURITY_H


