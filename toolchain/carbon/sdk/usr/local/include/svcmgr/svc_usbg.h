#ifndef SVC_USBG_H
#define SVC_USBG_H

#ifdef __cplusplus
extern "C" {
#endif

#include "svcmgrSvcDef.h"

/*SVC_SERVICE:usbg*/
#define USBG_MAX_INST			8
#define USBG_MAX_CONFIG_FUNCTIONS	7
#define USBG_MAX_ACM			4
#define USBG_MAX_RNDIS			1

#define G_ACM				0x002
#define G_ECM				0x008
#define G_NCM				0x020
#define G_EEM				0x040
#define G_RNDIS				0x080

/*SVC_STRUCT*/ struct usbg_state {
	unsigned int default_config;
#ifdef RAPTOR
	unsigned int current_config;
#endif
};

/*
 * Obtain the version of the proximity service
 *
 * @return version struct defined in svcmgrSvcDef.h
 */
/*SVC_PROTOTYPE*/ struct version usbg_getVersion(void);

/*
 * usbg_get_default_config - gets the gadget subsystem state, filling the struct
 * usbg_state
 *
 * @return a struct usbg_state on success, or -1 as default config in case of
 * error, eventually updating errno.
 */
/*SVC_PROTOTYPE*/ struct usbg_state usbg_get_state(void);

/*
 * !!!!! DEPRECATED WILL DISAPEAR IN ADK5
 * usbg_set_default_config - set the pointed config as default, will be loaded
 * on startup, the config file has to exist in /flash/mnt/etc/config/usbg or
 * listed as one of the default configurations listed in this file.
 * @file_name: name of the file
 *
 * @return 0 on success, or < 0 in case of error, eventually updating errno (if
 * ret = -1).
 */
/*SVC_PROTOTYPE*/ int usbg_set_default_config(char *file_name);

/*
 * usbg_save_default_config - set the pointed config as default, will be loaded
 * on startup, the config file has to exist in /flash/mnt/etc/config/usbg or
 * listed as one of the default configurations listed in this file.
 * @mask: mask representing a configuration
 *
 * @return 0 on success, or < 0 in case of error, eventually updating errno (if
 * ret = -1).
 */
/*SVC_PROTOTYPE*/ int usbg_save_default_config(unsigned int mask);

/*
 * usbg_get_default_config - gets the default gadget config
 * @mask: describes the functions used in this device
 *
 * Gets the by default active functions mask from usbgd.
 *
 * @return 0 on success, or < 0 in case of error, eventually updating errno (if
 * ret = -1).
 */
/*SVC_PROTOTYPE*/ int usbg_get_default_config(unsigned int *mask);

/*
 * usbg_to_mask - gets mask for a given function and number of instances
 * @function one of the functions defined above
 * @n_inst number of instances of that function, never greater than
 *         USBG_MAX_INST
 *
 * On Raptor:
 * The linux kernel imposes a series of limitations on the number and
 * combination of functions:
 *	- Only 4 ACM functions per gadget allowed
 *	- Only 1 RNDIS function (as per protocol definition)
 *	- Only 7 functions in total
 *
 * On Trident:
 * The USB gadget subsystem is not using configfs so we're limited to any
 * combination of 1 ACM and 1 RNDIS function.
 *
 * Usbgd will check the configurations, yet it's good to validate any mask
 * before loading it into the system (usbg_validate_mask()).
 *
 * @return a mask or 0 in case arguments weren't good enough.
 *
 * !!!! Please consider using the macros defined below.
 */
/*SVC_PROTOTYPE*/ unsigned int usbg_to_mask(unsigned int function,
					    unsigned int n_inst);

/*
 * These APIs allow us to do mask operations on configurations, i.e. :
 *
 *	usbg_set_default_config(USBG_ACM(3) | USBG_ECM(1));
 *
 *		or
 *
 *	mask = USBG_ACM(3) | USBG_RNDIS(1);
 *
 *		or also
 *
 *	mask |= USBG_EEM(1);
 *
 *		or even (delete function from mask)
 *
 *	mask &= ~USBG_NCM(5);
 *
 * You are only alowed to use this defines and functions, the content of the
 * mask should never be altered manually.
 * !! RNDIS is only allowed to have one instance as per protocol definition !!
 */
#define USBG_ACM(n)		usbg_to_mask(G_ACM, n)
#define USBG_ECM(n)		usbg_to_mask(G_ECM, n)
#define USBG_NCM(n)		usbg_to_mask(G_NCM, n)
#define USBG_EEM(n)		usbg_to_mask(G_EEM, n)
#define USBG_RNDIS(n)		G_RNDIS

/*
 * usbg_is_function_enabled - determines whether a function is enabled
 * @mask mask to analyze
 * @function one of the functions defined above USBG_MAX_INST
 *
 * @return -1 on error, sets errno accordingly on success 0 or 1 depending on
 * the function state
 */
/*SVC_PROTOTYPE*/ int usbg_is_function_enabled(unsigned int mask,
					       unsigned int function);
/*
 * usbg_num_inst - gets number of instances for a given function on a given mask
 * @mask mask to analyze
 * @function one of the functions defined above USBG_MAX_INST
 *
 * @return -1 on error, sets errno accordingly on success 0 to 8 as the number
 * of function instances
 */
/*SVC_PROTOTYPE*/ int usbg_num_instances(unsigned int mask,
					 unsigned int function);

/*
 * Iterate over a given mask, "unsigned int f" and "int n" will be updated with
 * the function mask and number of instances respectively.
 *
 * Keep in mind that this will go through all the functions regardless if they
 * are defined in the target platform.
 */
#define usbg_for_each_function(f, n, mask) \
	for (f = 1, n = usbg_num_instances(mask, 1); \
	     usbg_function_is_valid(f); \
	     f <<= 1, n = usbg_num_instances(mask, f))

/*
 * usbg_validate_mask - validates the current mask for the current platform
 * @mask mask to validate
 *
 * @return -1 on error, sets errno accordingly on success 0
 */
/*SVC_PROTOTYPE*/ int usbg_validate_mask(unsigned int mask);

#ifdef RAPTOR
/*
 * usbg_function_inc - increase number instances on a given function
 * @mask mask to modify
 * @function one of the functions defined above USBG_MAX_INST
 *
 * @return 0 on error, sets errno accordingly, on success returns a mask
 */
/*SVC_PROTOTYPE*/ unsigned int usbg_function_inc(unsigned int mask,
						 unsigned int function);

/*
 * !!!!! DEPRECATED WILL DISAPEAR IN ADK5
 * usbg_load_gadget - load and enable configfs gadget
 * @file_name: name of the file
 *
 * Will load the configuration from "file_name" (located in /etc/usbg) and
 * enable the gadget driver.
 *
 * @return 0 on success, or < 0 in case of error, eventually updating errno (if
 * ret = -1).
 */
/*SVC_PROTOTYPE*/ int usbg_load_gadget(char *file_name);


/*
 * usbg_load_config - load and enable configfs gadget
 * @mask: mask representing usbg config
 *
 * Will load the configuration from maks and enable the gadget driver.
 *
 * @return 0 on success, or < 0 in case of error, eventually updating errno (if
 * ret = -1).
 */
/*SVC_PROTOTYPE*/ int usbg_load_config(unsigned int mask);


/*
 * usbg_get_current_config - gets the current gadget config
 * @mask: describes the functions used in this device
 *
 * Gets the currently active functions mask from usbgd.
 *
 * @return 0 on success, or < 0 in case of error, eventually updating errno (if
 * ret = -1).
 */
/*SVC_PROTOTYPE*/ int usbg_get_current_config(unsigned int *mask);

#endif /* RAPTOR */

#ifdef __cplusplus
}
#endif
#endif /* SVC_USBG_H */
