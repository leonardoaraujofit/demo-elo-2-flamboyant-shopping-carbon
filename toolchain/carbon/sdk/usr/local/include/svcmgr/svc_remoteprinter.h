/*
 *	COPYRIGHT (C) 2012 by VeriFone, Inc.  All rights reserved.
 *
 *	Filename:		        svc_remoteprinter.h
 *	Module Description:		Remote Printer Service Library
 *	Component of:			Printer
 *	Authors(s):			    Stephen_M5
 *	Function:			    User API library that interfaces to WDE XML application or C application.
 *  Revision 1.0            2012/Oct/15   File created
 *	Modification History: (use if not filled in by source control)
 * 
 *  2013.09.09  T_KonstantinK1	Fix CR1972, other fixes
 *  2013.09.16  T_KonstantinK1	Fix CR1972, added Events 
 *  2014.01.15  T_KonstantinK1	Fix VOSCOR-752, parameter of remoteprinter_SetDensity()
 *                              now has different meaning for TG2460* and M1470
 *
 *	BACKGROUND:
 *
 *	API Design:
 *
 *
 */

 /***************************************************************************
  *
  *	#include files required to compile this source file here
  *
  ***************************************************************************/
#ifndef SVC_REMOTE_PRINTER_H
#define SVC_REMOTE_PRINTER_H

#include "svcmgrSvcDef.h"

#ifdef __cplusplus
extern "C" {
#endif

/*SVC_SERVICE:remoteprinter*/

/**
 * @name Remote printer Size definitions:
*/
/** \{ */
#define SVC_RMPRINT_LIB_VERSION_SIZE 10
#define SVC_MAX_BIT_IMAGE_SIZE_PER_LINE  511
/** \} */

/**
 * @name remote printer Cut Type:
*/
/** \{ */
#define SVC_RMPRNT_PARTIAL_CUT 0
#define SVC_RMPRNT_FULL_CUT    1
/** \} */

/**
 * @name Remote Printer Types:
*/
/** \{ */
#define SVC_RMP_MODEL_NONE     0
#define SVC_RMP_MODEL_M1470    1
#define SVC_RMP_MODEL_TG2460   2
#define SVC_RMP_MODEL_TG2460H  3
/** \} */

/**
 * @name Remote Printer Bit Image Mode:
*/
/** \{ */
#define SVC_BIT_IMAGE_8DOTS_SINGLE_DENSITY  0
#define SVC_BIT_IMAGE_8DOTS_DOUBLE_DENSITY  1
#define SVC_BIT_IMAGE_24DOTS_SINGLE_DENSITY 32
#define SVC_BIT_IMAGE_24DOTS_DOUBLE_DENSITY 33
/** \} */

/**
 * @name Printer On/off Control:
*/
/** \{ */
#define SVC_RMPRNT_DISABLE_PRINTER   0
#define SVC_RMPRNT_ENABLE_PRINTER    1
/** \} */


/**
 * @name Printer Status Id:
*/
/** \{ */
#define SVC_ID_RMPRNT_PRINTER_STAT   1
#define SVC_ID_RMPRNT_OFFLINE_STAT   2
#define SVC_ID_RMPRNT_ERROR_STAT     3
#define SVC_ID_RMPRNT_SENSOR_STAT    4
#define SVC_ID_RMPRNT_PAPER_STAT     17
#define SVC_ID_RMPRNT_FULL_STAT      20
#define SVC_ID_RMPRNT_PRNTER_ID      21
#define SVC_ID_RMPRNT_POWER_STAT     22 // return power status: ON/OFF
/** \} */

/**
 * @name Printer Power Status Id:
*/
/** \{ */
#define RMPRNT_POWERRED_OFF 0
#define RMPRNT_POWERRED_ON	1
/** \} */

/**
 * @name Printer Barcode System Type ID:
*/
/** \{ */
#define SVC_ID_BARCODE_SYSTEM_UPC_A      0
#define SVC_ID_BARCODE_SYSTEM_UPC_E      1
#define SVC_ID_BARCODE_SYSTEM_EAN13      2
#define SVC_ID_BARCODE_SYSTEM_EAN8       3
#define SVC_ID_BARCODE_SYSTEM_CODE39     4
#define SVC_ID_BARCODE_SYSTEM_ITF        5
#define SVC_ID_BARCODE_SYSTEM_CODABAR    6
#define SVC_ID_BARCODE_SYSTEM_CODE93     7
#define SVC_ID_BARCODE_SYSTEM_CODE128    8
#define SVC_ID_BARCODE_SYSTEM_CODE32     20
	/**< Extension barcode systems  */
#define SVC_ID_BARCODE_SYSTEM_UPC_A_EX   65
#define SVC_ID_BARCODE_SYSTEM_UPC_E_EX   66
#define SVC_ID_BARCODE_SYSTEM_EAN13_EX   67
#define SVC_ID_BARCODE_SYSTEM_EAN8_EX    68
#define SVC_ID_BARCODE_SYSTEM_CODE39_EX  69
#define SVC_ID_BARCODE_SYSTEM_ITF_EX     70
#define SVC_ID_BARCODE_SYSTEM_CODABAR_EX 71
#define SVC_ID_BARCODE_SYSTEM_CODE93_EX  72
#define SVC_ID_BARCODE_SYSTEM_CODE128_EX 73
#define SVC_ID_BARCODE_SYSTEM_CODE32_EX  90
/** \} */

//Specific printer dependent commands:
/**
 * @name Remote Printer Command IDs:
*/
/** \{ */
#define SVC_RMPRINT_RAW_CMD            0 /**< //Raw commands, With this command ID, application will pass through total commands including command, parameters, data */

	/**< //The command IDs below are for specific printer dependent commands, the application will only need to passthrough variable command parameter(s) if necessary.*/

	/**< //TG2460 Printer dependent commands:    Command Description:                                                                                    Function rmpCommand pDataIn:                       Function rmpCommand pDataOut:                     */

	/**< //Generic TG Printer(TGP) commands:  */
#define SVC_TGP_HORIZONTAL_TAB         1  /**< // Horizontal tab                                                                                         NULL                                               NULL                                             */
#define SVC_TGP_LINE_FEED              2  /**< // Print and line feed                                                                                    NULL                                               NULL                                             */
#define SVC_TGP_CAR_RETURN             3  /**< // Print and carriage return                                                                              NULL                                               NULL                                             */
#define SVC_TGP_SET_RIGHT_SPACE        4  /**< // Set right-side char spacing                                                                            1 Byte:  n                                         NULL                                             */
#define SVC_TGP_SET_PRINT_MODE         5  /**< // Set print mode                                                                                         1 Byte:  n                                         NULL                                             */
#define SVC_TGP_SET_PRINT_POS          6  /**< // Set absolute print position                                                                            2 Bytes: nL nH                                     NULL                                             */
#define SVC_TGP_UNDER_LINE_MODE        7  /**< // Turn underline mode on/off                                                                             1 Byte:  n                                         NULL                                             */
#define SVC_TGP_INCH8_SPACE            8  /**< // Select 1/8-inch line spacing                                                                           NULL                                               NULL                                             */
#define SVC_TGP_INCH6_SPACE            9  /**< // Select 1/6-inch line spacing                                                                           NULL                                               NULL                                             */
#define SVC_TGP_MIN_LINE_SPACE         10 /**< // Set line spacing using min units                                                                       1 Byte:  n                                         NULL                                             */
#define SVC_TGP_SET_SCRIPT_MODE        11 /**< // Set/reset script mode                                                                                  1 Byte:  n                                         NULL                                             */
#define SVC_TGP_INIT_PRINTER           12 /**< // Initialize printer                                                                                     NULL                                               NULL                                             */
#define SVC_TGP_SET_HOR_TAB_POS        13 /**< // Set horizontal tab position (needs additional data)                                                    n Bytes: n1....nk NUL                              NULL                                             */
#define SVC_TGP_SET_BOLD_MODE          14 /**< // Turn bold mode on/off                                                                                  1 Byte:  n                                         NULL                                             */
#define SVC_TGP_SET_STRIKE_MODE        15 /**< // Turn double-strike mode on/off                                                                         1 Byte:  n                                         NULL                                             */
#define SVC_TGP_PRINT_AND_FEED         16 /**< // Print and paper feed                                                                                   1 Byte:  n                                         NULL                                             */
#define SVC_TGP_SET_INTERN_CHAR        17 /**< // Select international character set                                                                     1 Byte:  n                                         NULL                                             */
#define SVC_TGP_SET_ROTATE_MODE        18 /**< // Set print mode rotated by 90 degree                                                                    1 Byte:  n                                         NULL                                             */
#define SVC_TGP_SET_RELATIVE_POS       19 /**< // Set relative print position                                                                            2 Byte:  nL nH                                     NULL                                             */
#define SVC_TGP_SEL_JUSTIFICATION      20 /**< // Select justification                                                                                   1 Byte:  n                                         NULL                                             */
#define SVC_TGP_PANEL_KEYS             21 /**< // Enable/disable panel keys                                                                              1 Byte:  n                                         NULL                                             */
#define SVC_TGP_PRINT_FEED_LINES       22 /**< // Print and feed paper n lines                                                                           1 Byte:  n                                         NULL                                             */
#define SVC_TGP_SEL_CODE_TABLE         23 /**< // Select character code table                                                                            1 Byte:  n                                         NULL                                             */
#define SVC_TGP_GET_PAPER_STATUS       24 /**< // Transmit paper status                                                                                  NULL                                               1 Byte: Paper status: See printer spec           */
#define SVC_TGP_SET_SPEED_MODE         25 /**< // Select speed/quality mode                                                                              1 Byte:  n                                         NULL                                             */
#define SVC_TGP_SET_UP_SIDE_DOWN       26 /**< // Set/cancel upside-down printing                                                                        1 Byte:  n                                         NULL                                             */
#define SVC_TGP_CUT_MOVE_BACK          27 /**< // Total cut and paper moving back                                                                        NULL                                               NULL                                             */
#define SVC_TGP_SET_WHITE_BLACK        28 /**< // Turn white/black reverse printing on/off                                                               1 Byte:  n                                         NULL                                             */
#define SVC_TGP_SET_HRI_POS            29 /**< // Select printing position of HRI                                                                        1 Byte:  n                                         NULL                                             */
#define SVC_TGP_GET_MODE_ID 	        30 /**< // Transmit printer mode ID                                                                               1 Byte:  n                                         1 Byte: Printer Mode ID                          */
#define SVC_TGP_GET_TYPE_ID            31 /**< // Transmit printer type ID                                                                               1 Byte:  n                                         1 Byte: Printer Type ID                          */
#define SVC_TGP_GET_VERSION_ID         32 /**< // Transmit printer version ID                                                                            1 Byte:  n                                         4 Bytes: Version                                 */
#define SVC_TGP_SET_LEFT_MARGIN        33 /**< // Set left margin                                                                                        2 Bytes: nL nH                                     NULL                                             */
#define SVC_TGP_HORI_VERT_MOTION       34 /**< // Set horinzontal and vertical motion units                                                              2 Bytes: x y                                       NULL                                             */
#define SVC_TGP_PRINT_AREA_WIDTH       35 /**< // Set printing area width                                                                                2 Bytes: nL nH                                     NULL                                             */
#define SVC_TGP_PRINT_COUNTER          36 /**< // Print counter                                                                                          NULL                                               NULL                                             */
#define SVC_TGP_SEL_HRI_FONT           37 /**< // Select font for HRI characters                                                                         1 Byte:  n                                         NULL                                             */
#define SVC_TGP_BARCODE_HEIGHT         38 /**< // Set bar code height                                                                                    1 Byte:  n                                         NULL                                             */
#define SVC_TGP_GET_TRANSMIT_STATUS    39 /**< // Transmit status                                                                                        NULL                                               1 Byte: Paper sensor status                      */
#define SVC_TGP_SET_BARCODE_WIDTH      40 /**< // Set bar code width                                                                                     1 Byte:  n                                         NULL                                             */
	/**< //TG2460 Specific commands:*/
#define SVC_TG2460_MOVE_BACK   		    41 /**< // Moving back one character                                                                              NULL                                               NULL                                             */
#define SVC_TG2460_MACRO_DEFINITION    42 /**< // Start/end of macro definition                                                                          NULL                                               NULL                                             */
#define SVC_TG2460_EXECUTE_MACRO       43 /**< // Execute macro                                                                                          3 Bytes: r t m                                     NULL                                             */
#define SVC_TG2460_SEL_COUNTER_MODE    44 /**< // Select counter print mode                                                                              2 Bytes: n m                                       NULL                                             */
#define SVC_TG2460_SEL_COUNT_MODEA     45 /**< // Select count mode-A (needs additional data)                                                            6 Bytes: aL aH bL bH n r                           NULL                                             */
#define SVC_TG2460_SET_COUNTER         46 /**< // Set counter                                                                                            2 Bytes: nL nH                                     NULL                                             */
#define SVC_TG2460_SEL_COUNT_MODEB     47 /**< // Select count mode-B (needs additional data)                                                            10 Bytes: sa ; sb ; sn ; sr ; sc ;                 NULL                                             */
#define SVC_TG2460_SET_SCRIPT  		    48 /**< // Set superscript-subscript                                                                              1 Byte:  n                                         NULL                                             */
	/**< //TG2460H Specific Commands:*/
#define SVC_TG2460H_SET_VERT_POS       49 /**< // Set absolute vertical print position in page mode                                                      2 Bytes: nL nH                                     NULL                                             */
#define SVC_TG2460H_POWER_LED_BAR      50 /**< // Power on/off LED bar                                                                                   1 Byte:  n                                         NULL                                             */
#define SVC_TG2460H_SET_CPI_MODE       51 /**< // Set/Cancel CPI mode                                                                                    1 Byte:  n                                         NULL                                             */
#define SVC_TG2460H_SET_NOTCH_DISTANCE 52 /**< // Set notch distance                                                                                     2 Bytes: nH nL                                     NULL                                             */
#define SVC_TG2460H_ALIGN_HEAD         53 /**< // Align the print head with the notch                                                                    NULL                                               NULL                                             */
#define SVC_TG2460H_ALIGN_AUTO_CUTTER  54 /**< // Align the autocutter with the notch                                                                    NULL                                               NULL                                             */
	/**< //MCS Printer dependent commands:*/
#define SVC_MCSP_DELETE_FONTS          55 /**< // Delete fonts(instantly)                                                                                NULL                                               NULL                                             */
#define SVC_MCSP_GET_FONT_INFO         56 /**< // Query font info                                                                                        1 Byte:  n                                         8 Bytes: Font info: see MCS printer spec         */
#define SVC_MCSP_GET_LOGO_CRC          57 /**< // Query Logo CRC, logoIndex range[01-15]                                                                 1 Byte:  n                                         2 Bytes: Logo CRC                                */
#define SVC_MCSP_SET_FONT              58 /**< // Change font: fontIndex range[0-4].                                                                     1 Byte:  n                                         NULL                                             */
#define SVC_MCSP_SET_BIG_LETTERS       59 /**< // Set Big Letters                                                                                        NULL                                               NULL                                             */
#define SVC_MCSP_GET_PRINTER_ID        60 /**< // Get Printer ID                                                                                         NULL                                               2 Bytes: Printer ID                              */
#define SVC_MCSP_SET_SMALL_LETTERS     61 /**< // Change small letters.                                                                                  NULL                                               NULL                                             */
#define SVC_MCSP_FORWARD_NEXT_MARK     62 /**< // Go to the next printer mark, The printer forwards to next printer mark.                                NULL                                               NULL                                             */
#define SVC_MCSP_SET_NORMAL_LETTERS    63 /**< // Set normal letters(same as \H3).                                                                       NULL                                               NULL                                             */
#define SVC_MCSP_RECEIPT               64 /**< // Attendance test:                                                                                       NULL                                               NULL                                             */
#define SVC_MCSP_READ_SERIAL       	    65 /**< // Read Serial, The printer returns 12 digit serial number:                                               NULL                                               12 Bytes: Printer Serial number                  */
#define SVC_MCSP_SET_POWEROFF_TIMEOUT  66 /**< // Set power off timeout in seconds, valid value range[10-60], default 20:                                1 Byte:  n                                         NULL                                             */
#define SVC_MCSP_GET_POWEROFF_TIMEOUT  67 /**< // Query power off timeout                                                                                NULL                                               2 Bytes: Power off timer value                   */
#define SVC_MCSP_GET_SENSOR_INFO       68 /**< // Get temperature of print head sensor.                                                                  NULL                                               5 Bytes: x x , y y                               */
#define SVC_MCSP_DISPLAY_COUNTER       69 /**< // (Instantly) Display counter                                                                            NULL                                               8 Bytes: Printer counter number, little endian   */
#define SVC_RMPRINT_NUM_CMD            70 /**< //Total command number*/
/** \} */

#define SVC_RMP_APP_BUFF_SIZE  1024


#define REMOTEPRINTER_KEY ("RemoteprinterService")
#define REMOTEPRINTER_EVENT1 0x01


/*SVC_STRUCT*/
/**
 * remoteprinter binary data structure
 */
struct rmpBinData {
	void *data;	            /**< Binary data */
	int  data_count;		/**< Bytes of binary data */
};

/*SVC_STRUCT*/
/**
 * Returned data structure
 */
struct rmpDataReturn {
	int  len;		/**< Bytes of return data */
	char data[64];	/**< return data */
};

/*SVC_STRUCT*/
/**
 * Command Parameters Structure
 */
struct rmpCmdParams {
	char  id;            /**< Id*/
	int  cmdLen;	     /**< Length of command data */
	char cmdData[SVC_RMP_APP_BUFF_SIZE];
};

/** Obtain the version of the RMP service
 * @return version struct defined in svcmgrSvcDef.h
 */
/*SVC_PROTOTYPE*/ struct version remoteprinter_getVersion(void);

/** Obtain the status of printer
 * @return status of printer 1 - ON, 0 - OFF, -1 - Error
 */
/*SVC_PROTOTYPE*/ char remoteprinter_isOnReadStatus (void);

/** Obtain the status of printer
 * @return status of printer 1 - ON, 0 - OFF
 */
/*SVC_PROTOTYPE*/ char remoteprinter_isOnGetEvent (char bitmask);


/** Open the remoteprinter service
 * @param[comDev]: COM flag string
 * @return If = 0  then Success: Com device handle; If = -1 then error, check errno:
 * @li ENOMEM when unable to malloc space.
 * @li ENXIO when the COM device is not available.
 * @li EINVAL when input parameter is invalid.
 * 
 * IMPORTANT NOTES: 
 */
/*SVC_PROTOTYPE*/int remoteprinter_Open(char* comDev);

#if 0
/** Set power switch off time
 *
 * @param[time_s]: time set to power off remote printer
 * @return If = 0 then Success; If = -1 then error, check errno:
 * @li ENOMEM when unable to malloc space.
 * @li ENXIO when the COM device is not available.
 * @li EINVAL when input parameter is invalid.
 * @li ENODEV when no printer is connected.
 *
 * IMPORTANT NOTES:
 */
/*SVC_PROTOTYPE*/ int remoteprinter_SetSwitchOffTime(int time_s);

/** Get power switch off time
 *
 * @return If > 0 then Success; If = -1 then error, check errno:
 * @li ENXIO when the COM device is not available.
 * @li EINVAL when input parameter is invalid.
 * @li ENODEV when no printer is connected.
 *
 * IMPORTANT NOTES:
 */
/*SVC_PROTOTYPE*/ int remoteprinter_GetSwitchOffTime(void);
#endif

/** RMP Print text strings
 *
 * @param[binData]: Text data to be printed
 * @return If = 0 then Success; If = -1 then error, check errno:
 * @li ENOMEM when unable to malloc space.
 * @li ENXIO when the COM device is not available.
 * @li EINVAL when input parameter is invalid.
 * @li ENODEV when no printer is connected.
 * @li EPERM when printer paper is jamed or printer cover is up
 *
 * IMPORTANT NOTES:
 */
/*SVC_PROTOTYPE*/ int remoteprinter_Print (char* str, int len);

/** Turn on RMP printer
 *
 * @return If = 0 then Success; If = -1 then error, check errno:
 * @li ENOMEM when unable to malloc space.
 * @li ENXIO when the COM device is not available.
 * @li EINVAL when input parameter is invalid.
 * @li ENODEV when no printer is connected.
 *
 * IMPORTANT NOTES:
 */
/*SVC_PROTOTYPE*/ int remoteprinter_TurnOn(void);

/** Turn on RMP printer. Starts thread. Returns immediately. 
 *
 * @return If = 0 then Success; If = -1 then error, check errno:
 * @li ERROR when unable to start thread.
 *
 * IMPORTANT NOTES:
 */
/*SVC_PROTOTYPE*/ int remoteprinter_TurnOnThreaded(void);

/** Turn off RMP printer
 *
 * @return If = 0 then Success; If = -1 then error, check errno:
 * @li ENOMEM when unable to malloc space.
 * @li ENXIO when the COM device is not available.
 * @li EINVAL when input parameter is invalid.
 * @li ENODEV when no printer is connected.
 *
 * IMPORTANT NOTES:
 */
/*SVC_PROTOTYPE*/ int remoteprinter_TurnOff(void);

/** Flush printer buffer
 *
 * @return If = 0 then Success; If = -1 then error, check errno:
 * @li ENOMEM when unable to malloc space.
 * @li ENXIO when the COM device is not available.
 * @li EINVAL when input parameter is invalid.
 * @li ENODEV when no printer is connected.
 *
 * IMPORTANT NOTES:
 */
/*SVC_PROTOTYPE*/ int remoteprinter_Flush(void);

/** Get Remote printer status.
 *
 * @param[statusType]: type of status info which is supposed to get.
 * @return rmpBinData: if data_count >0 status info, if data_count =0 then error, check errno:
 * @li ENOMEM when unable to malloc space.
 * @li ENXIO when the COM device is not available.
 * @li EINVAL when input parameter is invalid.
 * @li ENODEV when no printer is connected.
 * @li EPERM when this feature is not supported by connected printer.
 *
 * IMPORTANT NOTES:
 */
/*SVC_PROTOTYPE*/ struct rmpBinData remoteprinter_GetStatus(int statusType);

/** Remote printer print Barcode.
 *
 * @param[barcodeType]: Which barcode system type to be printed.
 * @param[binData]: Barcode data including barcode check-digit bit.
 * @return If = 0 then Success; If = -1 then error, check errno:
 * @li ENOMEM when unable to malloc space.
 * @li ENXIO when the COM device is not available.
 * @li EINVAL when input parameter is invalid.
 * @li ENODEV when no printer is connected.
 * @li EPERM when printer paper is jamed or printer cover is up
 *
 * IMPORTANT NOTES:
 */
/*SVC_PROTOTYPE*/ int remoteprinter_PrintBarCode(int barcodeType,struct rmpBinData binData);

/** Update remote printer firmware.
 *
 * @param[pFileName]: firmware file to be downloaded into printer
 * @return If = 0 then Success; If = -1 then error, check errno:
 * @li ENOMEM when unable to malloc space.
 * @li ENXIO when the COM device is not available.
 * @li EINVAL when input parameter is invalid.
 * @li ENODEV when no printer is connected.
 *
 * IMPORTANT NOTES:
 */
/*SVC_PROTOTYPE*/ int remoteprinter_UpdateFirmware(char* pFileName);

/** Download logo(s) into remote printer.
 *
 * @param[sectorId]: logo sector into which the logo will be downloaded.
 * @param[logoFileName]: logo file to be downloaded into printer
 * @return If > 0(acutual sector id downloaded) then Success; If = -1 then error, check errno:
 * @li ENOMEM when unable to malloc space.
 * @li ENXIO when the COM device is not available.
 * @li EINVAL when input parameter is invalid.
 * @li ENODEV when no printer is connected.
 *
 * IMPORTANT NOTES:
 */
/*SVC_PROTOTYPE*/ int remoteprinter_DownloadLogo(int sectorId, char* logoFileName);

/** Download font(s) into remote printer.
 *
 * @param[fontId]: font sector into which the fontfile will be downloaded.
 * @param[fontFileName]: font file to be downloaded into printer
 * @return If > 0(acutual sector id downloaded) then Success; If = -1 then error, check errno:
 * @li ENOMEM when unable to malloc space.
 * @li ENXIO when the COM device is not available.
 * @li EINVAL when input parameter is invalid.
 * @li ENODEV when no printer is connected.
 *
 * IMPORTANT NOTES:
 */
/*SVC_PROTOTYPE*/ int remoteprinter_DownloadFont(int fontId, char* fontFileName);

/** Print logo on remote printer.
 *
 * @param[logoId]: which logo to be printed
 * @param[binData]: Additional data: for TG2460: xH xL yH yL, for TG2460H: m mode.
 * @return = 0 then Success; If = -1 then error, check errno:
 * @li ENOMEM when unable to malloc space.
 * @li ENXIO when the COM device is not available.
 * @li EINVAL when input parameter is invalid.
 * @li ENODEV when no printer is connected.
 * @li EPERM when font download feature is not supported by connected printer or printer paper is jamed or printer cover is up.
 *
 * IMPORTANT NOTES:
 */
/*SVC_PROTOTYPE*/ int remoteprinter_PrintLogo(int logoId,struct rmpBinData binData);

/** Remote printer prints BitImage.
 *
 * @param[modeId]: which mode to be printed with.
 * @param[binData]: BitImage binary data for one line graphic printing.
 * @return = 0 then Success; If = -1 then error, check errno:
 * @li ENOMEM when unable to malloc space.
 * @li ENXIO when the COM device is not available.
 * @li EINVAL when input parameter is invalid.
 * @li ENODEV when no printer is connected.
 * @li EPERM when bitimage printing feature is not supported by connected printer, or when printer paper is jamed or printer cover is up.
 *
 * IMPORTANT NOTES:
 */
/*SVC_PROTOTYPE*/ int remoteprinter_PrintBitImage(int modeId,struct rmpBinData binData);

/** Remote printer Sends command.
 *
 * @param[cmdID]: which command to be executed.
 * @param[binData]: command data to be sent down to printer.
 * @return = 0 then Success; If = -1 then error, check errno:
 * @li ENOMEM when unable to malloc space.
 * @li ENXIO when the COM device is not available.
 * @li EINVAL when input parameter is invalid.
 * @li ENODEV when no printer is connected.
 * @li EPERM when this command is not supported by connected printer.
 *
 * IMPORTANT NOTES:
 */
/*SVC_PROTOTYPE*/ struct rmpBinData remoteprinter_Command(int cmdID, struct rmpBinData binData);

/** Detect Remote printer.
 *
 * @return = 0 then Success; If = -1 then error, check errno:
 * @li ENOMEM when unable to malloc space.
 * @li ENXIO when the COM device is not available.
 * @li EINVAL when input parameter is invalid.
 * @li ENODEV when no printer is connected.
 * @li EPERM when printer paper is jamed or printer cover is up
 *
 * IMPORTANT NOTES:
 */
/*SVC_PROTOTYPE*/ int remoteprinter_Detect(void);

/** Get Remote printer Lib version.
 *
 * @return = 0 then Success; If = -1 then error, check errno:
 * @li ENOMEM when unable to malloc space.
 * @li ENXIO when the COM device is not available.
 * @li EINVAL when input parameter is invalid.
 * @li ENODEV when no printer is connected.
 *
 * IMPORTANT NOTES:
 */
/*SVC_PROTOTYPE*/ struct rmpBinData remoteprinter_GetLibVersion(void);

/** Set Remote printer density.
 *
 * @param[level]: which density level to be set.
 * @li for TG2460* printer the following values are used:
 *  0,48 to set printing density -50%
 *  1,49 to set printing density -37%
 *  2,50 to set printing density -25% 
 *  3,51 to set printing density -12%
 *  4,52 to set printing density 0%
 *  5,53 to set printing density +12%
 *  6,54 to set printing density +25%
 *  7,55 to set printing density +37%
 *  8,56 to set printing density +50%
 *  Printing density reverts to the default value when the printer is reset or turned off.
 *  Default value is 4.
 * @li for M1470 (MCS Artema) printer the following values are used:
 *  [-10, +10] range of values, where 
 *  -10 - brightest , 0 - normal, +10 - darkest
 *  Default value is 0.
 *  Printing density is stored in the printer's EEPROM and is NOT reset during power off.
 * @return = 0 then Success; If = -1 then error, check errno:
 * @li ENOMEM when unable to malloc space.
 * @li ENXIO when the COM device is not available.
 * @li EINVAL when input parameter is invalid.
 * @li ENODEV when no printer is connected.
 *
 * IMPORTANT NOTES:
 * The meaning of the parameter depends on the type of printer (TG2460* or M1470)
 */
/*SVC_PROTOTYPE*/ int remoteprinter_SetDensity(signed int level);

/** Set Remote printer character size(width, height).
 *
 * @param[width]: set character width.
 * @param[height]: set character height.
 * @return = 0 then Success; If = -1 then error, check errno:
 * @li ENOMEM when unable to malloc space.
 * @li ENXIO when the COM device is not available.
 * @li EINVAL when input parameter is invalid.
 * @li ENODEV when no printer is connected.
 *
 * IMPORTANT NOTES:
 */
/*SVC_PROTOTYPE*/ int remoteprinter_SetCharacterSize(int width,int height);

/** Remote printer cut paper
 *
 * @param[cutType]: partial cut or full cut.
 * @return = 0 then Success; If = -1 then error, check errno:
 * @li ENOMEM when unable to malloc space.
 * @li ENXIO when the COM device is not available.
 * @li EINVAL when input parameter is invalid.
 * @li ENODEV when no printer is connected.
 * @li EPERM when this feature is not supported by connected printer.
 *
 * IMPORTANT NOTES:
 */
/*SVC_PROTOTYPE*/ int remoteprinter_Cut(int cutType);

/** Close Remote printer.
 *
 * @return = 0 then Success; If = -1 then error, check errno:
 * @li ENOMEM when unable to malloc space.
 * @li ENXIO when the COM device is not available.
 * @li EINVAL when input parameter is invalid.
 * @li ENODEV when no printer is connected.
 *
 * IMPORTANT NOTES:
 */
/*SVC_PROTOTYPE*/ int remoteprinter_Close(void);


#ifdef __cplusplus
}
#endif

#endif //SVC_REMOTE_PRINTER_H
