
/** 
 *  @file svc_netloader.h 
 *
 *	@brief Netloader service.
 * 
 * Install applications over Ethernet (only with the Mx800Downloader application)
 *   
 * 
 */


 /*
 * All Rights Reserved. No part of this software may be reproduced,
 *  transmitted, transcribed, stored in a retrieval system, or
 *  translated into any language or computer language, in any form
 *  or by any means electronic, mechanical, magnetic, optical,
 *  chemical, manual or otherwise, without the prior permission of
 *  VeriFone Inc.
 *
 *   Copyright, 2011 VeriFone Inc.
 *   2099 Gateway Place, Suite 600
 *   San Jose, CA.  95110
 */



#ifndef SVC_SERVICE_NETLOADER_H
#define SVC_SERVICE_NETLOADER_H
#ifdef __cplusplus
extern "C" {
#endif

#include "svcmgrSvcDef.h"

/*SVC_SERVICE:netloader*/

/**
 * @name Return 'status' codes from netloader_getEvent
*/
/** \{ */
#define STATUS_NETLOADER_ERR    (0)     /**< Error return (errno will be non-zero), 'msg' member is NULL */
#define STATUS_NETLOADER_MSG    (1)     /**< Status message available - 'msg' member is non-NULL */
/** \} */

/**
 * @name Extended type 1 messages: return 'status' codes from netloader_getEvent
 *
 * Returned when \ref netloader_setExtendedMsg >= 1.
 * 
 * @note
 * You will always receive a STATUS_NETLOADER_DNLD_FILE status first. STATUS_NETLOADER_INSTL_FAIL
 * status does not mean the file has been removed, but rather that files such as images and text files 
 * cannot be extracted for installation. Unless STATUS_NETLOADER_DNLD_ERR status is received, STATUS_NETLOADER_FINISH_FILE
 * status will always be called last, indicating that netloader is done processing that file.
 *\n
 *
*/
/** \{ */
#define STATUS_NETLOADER_DNLD_FILE     (2)    /**< 'msg' member will return the filename of downloading file */
#define STATUS_NETLOADER_DNLD_DONE     (3)    /**< 'msg' member returns "DNLD DONE" */
#define STATUS_NETLOADER_DNLD_ERR      (4)    /**< 'msg' member returns "DNLD ERROR" */
#define STATUS_NETLOADER_INSTL_FILE    (5)    /**< 'msg' member will return the filename of the file being installed*/
#define STATUS_NETLOADER_INSTL_DONE    (6)    /**< 'msg' member returns "INSTL DONE" */
#define STATUS_NETLOADER_INSTL_FAIL	   (7)    /**< 'msg' member returns "INSTL FAIL" */
#define STATUS_NETLOADER_REBOOT	       (8)    /**< 'msg' member returns "REBOOT" */
#define STATUS_NETLOADER_FINISH_FILE   (9)    /**< 'msg' member will return the filename of the finished file */
#define STATUS_NETLOADER_REBOOT_REQUIRED	(10)	/**< 'msg' member returns "REBOOT REQUIRED" */
/** \} */


/**
 * @name Extended type 2 messages: return 'status' codes from netloader_getEvent
 *
 * Returned when \ref netloader_setExtendedMsg >= 2.
 *
 * @note
 * You will always receive a STATUS_NETLOADER_DNLD_FILE status first. STATUS_NETLOADER_INSTL_FAIL
 * status does not mean the file has been removed, but rather that files such as images and text files
 * cannot be extracted for installation. Unless STATUS_NETLOADER_DNLD_ERR status is received, STATUS_NETLOADER_FINISH_FILE
 * status will always be called last, indicating that netloader is done processing that file.
 *
*/
/** \{ */
#define STATUS_NETLOADER_READY         (100)    /**< 'msg' member returns "", netloader ready to connect on socket I/O */
#define STATUS_NETLOADER_EXIT          (200)    /**< 'msg' member returns "", netloader stopping and closing socket I/O */
/** \} */

/** 
 * Netloader event 
 */
/*SVC_STRUCT*/
struct netloader_event{
    int status;             /**< Return state value */
    char* msg;              /**< Pointer to return message */
};

/** Set extended messages
 *
 * @param[in] type  Enable extended type 
 *                  @li 1 = Messages
 *                  @li 2 = Enable extended type 1 and 2 messages
 *                  @li 9 = Enable extended type 1 and don't auto-reboot after install
 *                  @li 0 = Disable
 *
 * @return 
 * @li 0 Upon successful request
 * @li -1 if Unable to complete request
 *
 * @note
 * For XML interface, see xml:\ref netloader_setExtendedMsg.
 */
/*SVC_PROTOTYPE*/ int netloader_setExtendedMsg(int type/*1*/);


/** Get version of netloader service
 *
 * @return 
 * Version struct containing version
 *
 * @note 
 * For XML interface, see xml:\ref netloader_getVersion.
 */
/*SVC_PROTOTYPE*/ struct version netloader_getVersion(void);


/** get Active Status of netloader daemon server
 *
 * @return
 * @li 0 Netloader daemon not active (ie, not waiting for download)
 * @li 1 Netloader daemon active (broadcasting and waiting for download)
 *
 */
/*SVC_PROTOTYPE*/ int netloader_getActiveStatus(void);


/** Start netloader (listen on socket for download request)
 *
 * @param[in] delay Seconds before broadcasting presence and waiting for incoming download request(s).
 *
 * @return 
 * @li 0 Upon successful request
 * @li -1 If unable to complete request
 *
 * <b>Errno values</b>:
 * @li EINVAL - When passed delay value in invalid (less than zero)
 *
 * @note 
 * For XML interface, see xml:\ref netloader_start.
 */
/*SVC_PROTOTYPE*/ int netloader_start(int delay/*0*/);


/** Start netloader extended(listen on socket for download request)
 *
 * @param[in] delay Seconds before broadcasting presence and waiting for incoming download request(s).
 * @param[in] noReboot non-zero to not automatically reboot after install
 *
 * Note:
 * @li if noReboot is specified, extended messages are enforced (as if you did netloader_setExtendedMsg(2))
 *
 * @return
 * @li 0 Upon successful request
 * @li -1 If unable to complete request
 *
 * <b>Errno values</b>:
 * @li EINVAL - When passed delay value in invalid (less than zero)
 *
 * @note
 * For XML interface, see xml:\ref netloader_start.
 */
/*SVC_PROTOTYPE*/ int netloader_startExt(int delay, int noReboot);


/** Stop netloader (stop listening on socket for download request).
 *
 * @return 
 * @li 0 Upon successful request
 * @li -1 If unable to complete request
 *
 * @note 
 * For XML interface, see xml:\ref netloader_stop.
 */
/*SVC_PROTOTYPE*/ int netloader_stop(void);


/** Restart netloader (listen on socket for download request) stop is performed, then start after delay seconds.
 *
 * @param[in] delay Seconds before broadcasting presence and waiting for incoming download request(s).
 *
 * @return 
 * @li 0 Upon successful request
 * @li -1 If unable to complete request
 *
 *  <b>Errno values</b>:
 * @li EINVAL - When passed delay value in invalid (less than zero).
 *
 * @note 
 * For XML interface, see xml:\ref netloader_restart.
 */
/*SVC_PROTOTYPE*/ int netloader_restart(int delay/*0*/);


/** Read a netloader event.
 *
 * @param[in] flags  Get Event Flags:
 *                      @li 0 = blocking
 *                      @li 1 = Non-block
 *                      @li 2 = Return Last event
 *                      @li 4 = Cancel _getEvent() in progress
 *
 * @return 
 * Struct netloader_event - Event type is defined by 'status' member. Depending on status, msg may have an ASCII string.
 * 
 * @note IMPORTANT!
 * @li Application must free netloader_event 'msg' member when non-NULL
 * @li Flags are bit values and may be OR'd together
 * @li Setting flags=0 will perform the default action of retrieving the next non-previously retrieved event, blocking if needed waiting for the occurrence of the next event
 */
/*SVC_PROTOTYPE*/ struct netloader_event netloader_getEvent(int flags);


/** Read a netloader event
 *
 * @param[in] flags  Get Event Flags: 
 *                      @li -1 = Non-block
 *                      @li 2 = Return Last event
 *                      @li 4 = Cancel _getEvent() in progress
 *
 * @param[in] timeout (0 = wait forever) - max time in seconds to wait for event
 *
 * @return 
 * Struct netloader_event - Event type is defined by 'status' member. Depending on status, msg may have an ASCII string
 * 
 * @note IMPORTANT!
 * @li Application must free netloader_event 'msg' member when non-NULL
 * @li Flags are bit values and may be OR'd together
 * @li Setting flags=0 will perform the default action of retrieving the next non-previously retrieved event, blocking if needed waiting for the occurrence of the next event
 *
 * 
 * <b>Errno values</b>:
 *  @li ENOENT - Timeout occured before any event received
 *  @li EINVAL - Invalid key format
 *  @li ENODEV - Unable to cancel previous event_getEvent() in progress
 *  @li EAGAIN - No last event (when flags requesting last)
 *  @li ENODATA - No event data available (when flags set to non-block and no event).
 *  @li EBUSY - Event cancel already in progress, and attempting to cancel event (flags == 4)
 *  @li EFAULT - Event.size specified, but no data available to read
 *  @li EINTR  - Event_getEvent() canceled (by another thread/user). event field of returned structure may contain cause (see event_cancelWithCause())
 *  @li ENOMEM - Unable to allocate needed memory for event.size to read associated data
 */
/*SVC_PROTOTYPE*/ struct netloader_event netloader_getEventTimeout(int flags, int timeout);



/****************************************************************************************************/
/* Netloader client methods - those used to command a netloader server daemon to transfer file etc. */


/*SVC_STRUCT*/
struct netloader_broadcastTableEntry {
   char version[16];                     /**< Version of netloader server (eg. 1.3.10) */
   char serialNumber[16];                /**< Serial Number (eg. 169-000-796) */
   char model[8];                        /**< Model number (eg. Mx925) */
   char releaseVersion[32];              /**< Release version (eg. release-30110101) */
   char discretionaryData[132];          /**< Discretionary data (null terminated string) */
   char ip[16];                          /**< IP address of netloader server */
   int  port;                            /**< Port number of netloader server */
};


/*SVC_STRUCT*/
struct netloader_broadcastTable {
    int broadcastTableEntry_count;                               /**< Number of entries in broadcastTableEntry[] */
    struct netloader_broadcastTableEntry *broadcastTableEntry;   /**< Pointer to array of structs */
};


/**
 * @name Return 'status' codes from netloader_broadcastGetEvent()
*/
/** \{ */
#define NETLOADER_SCAN_CLIENT_ERR   (0)     /**< Error enabling the broadcast client daemon (see /var/log/messages) */
#define NETLOADER_SCAN_RCVD         (1)     /**< Broadcast message from a netloader server received */
#define NETLOADER_SCAN_FULL         (2)     /**< Broadcast table full and received another broadcast */
/** \} */



/*SVC_STRUCT*/
struct netloader_sendFileHandle {
    int entryID;                     /**< Entry ID used by netloader_sendFileGetEvent(), netloader_sendFileCancel(),... */
    char key[32];                   /**< Event key associated with the sendFile request */
};


/**
 * @name Return 'status' codes from netloader_sendFileGetEvent()
*/
/** \{ */
#define NETLOADER_SNDFILE_ERR        (0)    /**< Error connecting to netloader server or connection lost (see /var/log/messages) */
#define NETLOADER_SNDFILE_PROGRESS   (1)    /**< Progress event on file transfer to netloader server */
#define NETLOADER_SNDFILE_STATE      (2)    /**< Indicates state of transfer (see netloader_sendFile()) */
/** \} */


/**
 * @name Return 'state' data values for status == NETLOADER_SNDFILE_STATE from netloader_sendFileGetEvent()
*/
/** \{ */
#define NETLOADER_SNDFILE_STATE_SENDING     (1)  /**< Sending file in progress */
#define NETLOADER_SNDFILE_STATE_PROCESSING  (3)  /**< Transfer complete, other end processing */
#define NETLOADER_SNDFILE_STATE_BADINSTALL  (5)  /**< Other end failed installing bundle/package */
#define NETLOADER_SNDFILE_STATE_REBOOTING   (7)  /**< Other end has completed processing and is rebooting */
#define NETLOADER_SNDFILE_STATE_DONE        (9)  /**< Other end has completed processing (reboot not performed) */
#define NETLOADER_SNDFILE_STATE_CANCELLED   (11) /**< Transfer cancelled */
/** \} */


/*SVC_STRUCT*/
struct netloader_sendFileEvent{
    int status;                     /**< Status of netloader_sendFile() (see defines above for value and meaning) */
    int data;                       /**< Data associated with status, if status = NETLOADER_SNDFILE_PROGRESS, data is % of file sent (0 - 100) */
};


/** Get a netloader broadcast get event
 * 
 * @param[in] flags  Get Event Flags:
                        @li -1 = Non-block
 *                      @li 2 = Return Last event
 *                      @li 4 = Cancel _getEvent() in progress
 *
 * @return 
 * Indicates if a broadcast event occured (another device broadcasted netloader server available)
 * @li Flags are bit values and may be OR'd together.
 * @li Setting flags=0 will perform the default action of retrieving the next non-previously retrieved event, blocking if needed waiting for the occurrence of the next event.
 * @li Use netload_getBroadcastTable() to obtain info on who broadcasted a netloader service.
 */
/*SVC_PROTOTYPE*/ int netloader_broadcastGetEvent(int flags);



/** Used to obtain currently available broadcast netloader servers.
 *
 * This method returns a struct containing a array of structures with
 * information regarding what netloader servers have sent broadcast messages.
 *
 * @note
 *   1. The returned broadcastTableEntry in the struct MUST be free'd by caller (free())
 *
 *   2. Entries are available after netloader_disableBroadcastScan() called but may be "old"
 *
 * @return 
 * @li If >= 0  Amount of data available
 * @li If = -1  Error
 *
 * <b>Errno values</b>:
 * @li ENOENT - When no entries in broadcast table
 */
/*SVC_PROTOTYPE*/ struct netloader_broadcastTable netloader_getBroadcastTable(void);



/** Used to clear the broadcast table(s).
 * 
 *    See: netloader_getBroadcastTable(), netloader_enableBroadcastScan()
 *
 * This method does not alter the state of the broadcast watch daemon.
 * That is, if enabled, will remain enabled.
 *
 * @return 
 * @li If >= 0 Success
 * @li If = -1 Error
 *
 * @note
 * No error states are defined at this time.
 */
/*SVC_PROTOTYPE*/ int netloader_clearBroadcastTable(void);



/** Used to enable the broadcast watch daemon.
 *
 *    See: netloader_disableBroadcastScan()
 *
 * This method does not alter the broadcast tables, but will
 * cause the service to start looking for broadcasts.
 *
 * @param[in] port Port number to look for broadcasts. If 0, will used default port+1 (5142+1)
 *
 * @return 
 * @li If >= 0 Success
 * @li If = -1 Error
 *
 * <b>Errno values</b>:
 * @li  EBUSY - Already active
 * @li  EBADF - Unable to start broadcast scanner thread
 */
/*SVC_PROTOTYPE*/ int netloader_enableBroadcastScan(int port/*0*/);



/** Used to disable the broadcast watch daemon.
 *
 *    See: netloader_enableBroadcastScan()
 *
 * This method does not alter the broadcast tables, but will
 * cause the service to stop looking for broadcasts.
 *
 * @return 
 * @li If >= 0 Success
 * @li If = -1 Error
 * 
 * <b>Errno values</b>:
 * @li  EACCES - Already disabled
 */
/*SVC_PROTOTYPE*/ int netloader_disableBroadcastScan(void);



/** Used to send a file using the netloader protocol to a netloader server running at ip=ip on port=port.
 *
 *    This method schedules for the file to be sent and returns immediately.
 *
 *    The caller should use netloader_sendFileGetEvent() to get status/success/failure, etc.
 *
 * @param[in] sourcefile File with pathing to be downloaded/sent
 * @param[in] destfile  File w/pathing for remote end (where to place and what to name at download side).
 *
 * Rules for destfile:
 *
 *	1. The default destination directory is set to: /mnt/flash/install/dl/ 
 *     (regardless of any pathing specified in destfile).
 *
 *	2. /tmp/ if specified in destfile, this overrides Rule 1. No install operation
 *     is performed for this destination.
 *
 *	3. If on Ux200, and sourcefile starts with "4media", destination directory 
 *     is set to: /mnt/mediaSOC/dnld/
 *
 *	4. If environment variable *LOAD2HOME set, destination directory is set 
 *     to: /home/sys2/flash
 *
 * @param[in] server  Server IP address or name of netloader server to receive file (xxx.xxx.xxx.xxx)
 * @param[in] port  Port number where netloader server is waiting for connection (if 0, default netloader port used)
 * @param[in] user  Username used when full set (for remove of user packages), default when NULL or empty = "usr1"
 * @param[in] fileMask  Mask to set file (chmod), if <= 0, default to 644
 * @param[in] full  When set, user wishes to remove all previous user (non-os) bundles.
 * @param[in] progressEventInterval  When non-zero during the file transfer, events will be sent indicating progress.
 *                                    Value indicates % interval for event (example, value = 1, send event every 1%)
 *                                    currently, only 1% (value < 10) and 10% (value >= 10) are used.
 *                                    See netloader_sendFileGetEvent() for how to receive the events.
 * @param[in] maxDoneDelay  Max number of seconds to wait once download completes for install process
 *                                    to complete and send response back. (if 0, default to 90 seconds)
 *
 * @return
 * Struct netloader_sendFileHandle. 
  * @li If errno != 0, error
 * 
 * <b>Errno values</b>:
 * @li  ENOMEM -Not enough memory to make request/send file
 * @li  EINVAL - Unable to stat filename (not found or no permissions)
 * @li  EBADF - Filename empty or not a regular file (directory,...)
 * @li  EMFILE - Too many sendFile threads active (can only run up to 16 at a given time), use netloader_sendFileCancelAll() to cleanup
 * @li  EFBIG - File too large for remote terminal (not enough space)
 * @li  ENXIO - Unable to connect to remote server
 * @li  EPIPE - Connection closed by server (before send completed)
 * @li  EIO - Read/write to server failed (connection broken, ...)
 */
/*SVC_PROTOTYPE*/ struct netloader_sendFileHandle netloader_sendFileExt(char *sourcefile, char *destfile, char *server, int port/*0*/, char *user/*usr1*/, int fileMask/*0*/, int full/*0*/, int progressEventInterval/*0*/, int maxDoneDelay/*0*/);



/** Used to send a file using the netloader protocol to a netloader server running at ip=ip on port=port.
 *
 *    This method schedules for the file to be sent and returns immediately.
 *    The caller should use netloader_sendFileGetEvent() to get status/success/failure, etc.
 *
 * @param[in] filename File with pathing to be downloaded/sent
 * @param[in] server  Server IP address or name of netloader server to receive file (xxx.xxx.xxx.xxx)
 * @param[in] port  Port number where netloader server is waiting for connection (if 0, default netloader port used)
 * @param[in] user  Username used when full set (for remove of user packages), default when NULL or empty = "usr1"
 * @param[in] full  When set, user wishes to remove all previous user (non-os) bundles.
 * @param[in] enableProgressEvent  a
 *
 * @param[in] maxDoneDelay  Max number of seconds to wait once download completes for install process
 *                                    to complete and send response back. (if 0, default to 90 seconds)
 *
 * @return 
 * struct netloader_sendFileHandle. 
 * @li If errno != 0, Error
 *
 * <b>Errno values</b>:
 * @li  ENOMEM - Not enough memory to make request/send file
 * @li  EINVAL - Unable to stat filename (not found or no permissions)
 * @li  EBADF - Filename empty or not a regular file (directory,...)
 * @li  EMFILE - Too many sendFile threads active (can only run up to 16 at a given time), use netloader_sendFileCancelAll() to cleanup
 * @li  EFBIG - File too large for remote terminal (not enough space)
 * @li  ENXIO - Unable to connect to remote server
 * @li  EPIPE - Connection closed by server (before send completed)
 * @li  EIO - Read/write to server failed (connection broken, ...)
 */
/*SVC_PROTOTYPE*/ struct netloader_sendFileHandle netloader_sendFile(char *filename, char *server, int port/*0*/, char *user/*usr1*/, int full/*0*/, int enableProgressEvent/*0*/, int maxDoneDelay/*0*/);


/** Read a netloader sendFile event.
 *
 *   This is used to get status and associated data during the netloader_sendFile() processing
 *
 * @param[in] handle Sruct as returned by netloader_sendFile() used to get appropriate event
 * @param[in] flags  Get Event Flags:
 *                   @li 0 = Block
 *                   @li 1 = Non-block
 *                   @li 2 = Return Last event
 *                   @li 4 = Cancel _getEvent() in progress
 *
 * @return 
 * Status,data 
 * @li If errno == 0, return struct is valid and indicates success or failure of transfer and progress if enabled.
 * @li If error (errno != 0), called failed for this reasons:
 * 
 * <b>Errno values</b>:
 * @li EINVAL - Invalid key in handle\n
 *  
 * <b>For status: NETLOADER_SNDFILE_CLIENT_ERR:</b>\n
 * <I> data: contains errno indicating type of error encountered (see /var/log/messages for additional info) </I>
 *  @li EBADF - Problem encountered while reading from filename
 *  @li EPIPE - Problem encountered while writing file data to server (via tcp/ip socket)
 * 
 * 
 * <b>For progress:  NETLOADER_SNDFILE_PROGRESS ( this event only occurs when progressEventInterval != 0 in netloader_sendFile()): </b>\n
 *  <I>data: contains % of file transfered to connected netloader server (0 - 100)</I>\n
 *
 * 
 * <b>For state: NETLOADER_SNDFILE_STATE:</b>\n
 * <I>data: contains indication of what state the file transfer is at. Current values are:</I>
 *  @li NETLOADER_SNDFILE_STATE_SENDING  Sending file in progress
 *  @li NETLOADER_SNDFILE_STATE_PROCESSING  Transfer complete, other end processing
 *  @li NETLOADER_SNDFILE_STATE_REBOOTING  Other end of transfer is rebooting
 *  @li NETLOADER_SNDFILE_STATE_DONE  Other end has completed processing (reboot not performed)
 *
 * @note 
 * For small files, not all intervals for progress may be sent to the event due to minimum data packet size.
 */
/*SVC_PROTOTYPE*/ struct netloader_sendFileEvent netloader_sendFileGetEvent(struct netloader_sendFileHandle handle, int flags/*0*/);


/** Used to cancel a specific file transfer
 *    See: netloader_sendFile()
 *
 * This method will cause a transfer in progress to terminate.
 *
 * @param[in] handle Handle struct as returned from netloader_sendFile().
 *
 * @return 
 * @li If >= 0 Success
 * @li If = -1 Error
 * 
 * <b>Errno values</b>:
 * @li  EINVAL - Invalid handle content (entryID or handle references an old (finished) transfer)
 * @li  EBADF - Problem ending transfer
 */
/*SVC_PROTOTYPE*/ int netloader_sendFileCancel(struct netloader_sendFileHandle handle);


/** Used to cancel all currently active file transfers
 *
 *    See: netloader_sendFile()
 *
 * This method will cause all netloader_sendFile() transfers in progress to
 *  terminate. If the file has completed the transfer and is being installed,
 *  this will NOT be interrupted.
 *
 * @return 
 * @li If >= 0 Success
 * @li If = -1 Error
 * 
 * <b>Errno values</b>:
 * @li  EBADF - One (or more) transfer processes could not be ended
 */
/*SVC_PROTOTYPE*/ int netloader_sendFileCancelAll(void);




#ifdef __cplusplus
}
#endif
#endif //SVC_SERVICE_NETLOADER_H


