#ifndef SVC_SCANNER_H
#define SVC_SCANNER_H

#ifdef __cplusplus
extern "C" {
#endif

/*SVC_SERVICE:scanner*/
#include "svcmgrSvcDef.h"


/** Obtain the version of the scanner service
 *
 * @return version struct defined in svcmgrSvcDef.h
 */
/*SVC_PROTOTYPE*/ struct version scanner_getVersion(void);



/** Used to open scanner device and enable capturing of scans
 *    See: kbd_getScannerEvent(), scanner_disable()
 *
 *
 * @param[in] notifyOnDataCount = when/if to be notified via event
 *      <0 = no notification/events,
 * 		0  = will get notified when first character is received to empty input buffer.
 * 		>0 = event when notifyOnDataCount received/available (see scanner_getScanPrintableData())
 * @return If = 0 then Success; If = -1 then error, check errno:
 * @li EBUSY  - when the scanner device is already enabled.
 * @li EACCES - access denied, unable to open scanner device.
 * @li ENOENT - no scanner device found.
 */
/*SVC_PROTOTYPE*/ int scanner_enable(int notifyOnDataCount);


/*SVC_STRUCT*/
/**
 * Structure to store scan data
 */
struct scanData {
  char *data;	/**< scan data malloc'd string null terminated */
  int   count;  /**< amount of data placed in data[] */
};

/** get scan via events
 *    See: scanner_enable(), scanner_disable()
 *
 * This method uses events and allows caller to get the last scan,
 *  get scan with wait, get scan w/o wait, or cancel scan wait (used to unblock
 *  any other process waiting on a scan).
 *
 * @param[in] flags = Get Event Flags - 0 = block, 1 = non-block, 2 = Last , 4 = cancel (options can be or'd)
 * @return scan info (NOT the scan data, see scanner_getScanData():
 * 	  check errno == 0, no errors, and return value indicates minimum amount of data available
 *
 *    errno values:
 * @li EINVAL - invalid flags values
 * @li EACCES - when access is denied (scanner disabled)
 * @li EINTR  - get scan event canceled
 */
/*SVC_PROTOTYPE*/ int scanner_getScanEvent(int flags /* 0 */);


/** Used to obtain amount of currently available scan data.
 *
 * This method returns the amount (count) of available scan data characters
 * All characters are represented in a single byte.
 * Any non-printable characters in the incoming scan data is encoded as ".".
 *
 * Note: The returned buffer MUST be free'd by caller (free())
 *
 * @return If >= 0 then amount of data available; If = -1 then error, errno:
 * @li EACCES - when access is denied (scanner disabled)
 */
/*SVC_PROTOTYPE*/ int scanner_getScanDataCount(void);



/** Used to obtain available scan data (last successful scan).
 *    See: scanner_open(), scanner_getScanEvent()
 *
 * This method returns scan data that is printable, including space and carriage return.
 * The shiftkey modify is used internally to create upper/lower case alpha and other
 * printable characters (eg, !@#$%()+ etc.)
 * All characters are represented in a single byte.
 * Any non-printable characters in the incoming scan data is omitted.
 *
 * @param[in] maxCount = Maximum amount of scan characters to return (excluding NULL terminator).
 *                       If 0, return all buffered data.
 *
 * Return data string is a Null terminated character array.
 *
 * Note: The returned buffer MUST be free'd by caller (free())
 *
 * @return data and count in struct when Success; If error, data = NULL, count = 0, and errno:
 * @li EINVAL - when maxCount is < 0 (invalid)
 * @li EACCES - when access is denied (scanner disabled)
 * @li ENOENT - when no scan data is available
 * @li ENOMEM - when no data space available to malloc.
 */
/*SVC_PROTOTYPE*/ struct scanData scanner_getScanData(int maxCount /* 0 */);



/** Used to disable capturing of scans.
 *    See: scanner_open(), scanner_getScanEvent()
 *
 * This method closes scanner device and event. Any pending scan events
 * will be released.
 *
 * @return If >= 0 then Success; If = -1 then error, errno:
 * @li  EBADF - invalid handle
 */
/*SVC_PROTOTYPE*/ int scanner_disable(void);








#ifdef __cplusplus
}
#endif
#endif //SVC_SCANNER_H
