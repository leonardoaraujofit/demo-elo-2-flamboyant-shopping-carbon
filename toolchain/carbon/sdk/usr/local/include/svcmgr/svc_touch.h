/*
 *      Copyright, 2015 VeriFone Inc.
 *      2099 Gateway Place, Suite 600
 *      San Jose, CA.  95110
 *
 *  All Rights Reserved. No part of this software may be reproduced,
 *  transmitted, transcribed, stored in a retrieval system, or
 *  translated into any language or computer language, in any form
 *  or by any means electronic, mechanical, magnetic, optical,
 *  chemical, manual or otherwise, without the prior permission of
 *  VeriFone Inc.
 */
#ifndef SVC_TOUCH_H
#define SVC_TOUCH_H

#ifdef __cplusplus
extern "C" {
#endif

#include "svcmgrSvcDef.h"

/*SVC_SERVICE:touch*/


/** get version of touch service
 *
 * @return version struct containing version.
 *
 */
/*SVC_PROTOTYPE*/ struct version touch_getVersion(void);


/*SVC_STRUCT*/
/**
 * Define an on-screen secure keypad key location
 */
struct secureHotspot_s {
	short int x_upperLeft;	/**< Upper Left Horizontal pixel location > */
	short int y_upperLeft;	/**< Upper Left Vertical pixel location > */
	short int x_lowerRight;	/**< Lower Right Horizontal pixel location > */
	short int y_lowerRight;	/**< Lower Right Vertical pixel location > */
	char value; 			/**< ASCII value associated with this hotspot key location > */
};

/*SVC_STRUCT*/
/**
 * Define an collection of secure key locations
 * Typically this list contains keys: 0-9 (0x30-0x39), Escape (0x1B), Backspace (0x08), Return (0x0D)
 */
struct secureKeyCollection_s {
	struct secureHotspot_s *keys;	/**< keys */
	int keys_count;					/**< Number of keys */
};

/*SVC_STRUCT*/
/**
 * Define touch dither for x,y
 */
struct dither_s {
	int x /*2*/; /**< amount of screen pixels for dither in x-axis > */
	int y /*2*/; /**< amount of screen pixels for dither in y-axis > */
};


/** Set the secure key hotspots
 *
 * @param[in] secureKeyCollection_s structure containing hotspot locations for secure keypad (on-screen)
 *
 * @return
 * @li 0 Success
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li ENOENT - If keyHotspots.keys == 0, or if keys_count <= 0
 * @li EINVAL - If too many keys are defined (keys_count > 20)
 * @li EBADF - Failure when setting secure key hotspots via secure touch interface
 * @li ECONNREFUSED - not supported on this platform
 * @li ESRCH - touch panel is shutdown, must be awaken first (see touch_wakeup())
 *
 */
/*SVC_PROTOTYPE*/ int touch_setSecureHotspots(struct secureKeyCollection_s keyHotspots);


/** Shutdown the touch controller chip and interface
 *
 * @return
 * @li 0 Success
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li EBADF - Failure when requesting touch driver to shutdown
 * @li ECONNREFUSED - not supported on this platform
 *
 */
/*SVC_PROTOTYPE*/ int touch_shutdown(void);


/** Wakeup the touch controller chip and interface
 *
 * @return
 * @li 0 Success
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li EBADF - Failure when requesting touch driver to wakeup
 * @li ECONNREFUSED - not supported on this platform
 *
 */
/*SVC_PROTOTYPE*/ int touch_wakeup(void);


/** Calibrate the touch panel
 *
 * @param[in] type 
 * 		0 - soft calibration, 
 * 		1 - factory calibration
 * 
 * Notes: 
 *   1. factory calibration: 
 *      Resets the touch controller and overwrites all calibration values.
 *      With this cal, we can't tell if something is on the panel (object,
 *      hand, etc).
 *   2. soft calibration (normal cal):
 *      Does a soft calibration w/o affecting the factory cal values. This
 *      type of calibration can detect objects on screen since this is really
 *      for fine tuning the panel (temperature, etc. can affect calibration
 *      to vary from factory settings).
 *
 * @return
 * @li 0 Success
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li ENOENT - Touch calibration using this method not supported on this platform.
 * @li ENODEV - No touch device present on this platform.
 * @li EINVAL - Invalid type passed (see above)
 * @li EOVERFLOW - Calibration failed, suggest (re)performing a factory cal or panel issues.
 * @li EBADMSG - Soft cal is out of range (possible object on panel or factory cal required).
 * @li EBADSLT - Delta between factory cal and soft cal out of range (factory cal required and/or object on panel).
 * @li ECONNREFUSED - This call not supported on this platform.
 *
 */
/*SVC_PROTOTYPE*/ int touch_calibrate(int type);


/** Get/Set the touch x,y dither
 *
 * @param[in] dither
 * 	 A dither_s structure containing x,y dither values
 *   
 *   Notes:
 *   1. If dither x or y is < 0, than the current dither value for that param (x or y) is returned.
 *   2. Valid dither values for setting are 0 to 15 (inclusive) for x and/or y (default is 0,0)
 *   3. You can set 1 or both of the dither values (any you don't want changed, pass a negative value for them).
 *   4. The dither values are in pixels (display resolution) and determine how much movement
 *      in a touch to the panel is needed before reporting movement to an application. This
 *   	is used to de-sensitize the panel so for example, menu items are easier to select w/o
 *   	the software (fltk for example) thinking it is a scroll movement (because the x,y 
 *   	touch values are moving due to the finger on the panel).
 *   5. The default is 0,0 (x,y) and we have had good results up to ~10, but speed slows the
 *      bigger the dither value. Above this and you may find the panel appearing non-responsive.
 *   6. The dither values are cleared (including the saved values when a factory calibration
 *      is performed (<B>touch_calibrate(1)</B>)
 *
 * @return current dither values (or values after setting dither)
 * @li 0 Success
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li ENOMEM - Unable to get required memory to complete requrest
 * @li EINVAL - Invalid x and/or y dither values (must be less than max (<0 current dither returned w/o error))
 * @li EBADF - Failure when setting dither or unable to get dither values (internal error)
 * @li ECONNREFUSED - not supported on this platform
 *
 */
/*SVC_PROTOTYPE*/ struct dither_s touch_dither(struct dither_s dither);


/** Get/Set the touch grounded configuration
 *
 * @param[in] grounded
 * 		0 - set current grounded status to not grounded
 * 		1 - set current grounded status to grounded
 *   	       -1 - return current grounded status
 * 
 *   Notes:
 *   1. This call informs the touch driver when the terminal connection changes between
 *	grounded and ungrounded.  This allows the driver in turn to inform the K2 touch controller
 *	so that it can select the correct sensitivity levels and internal parameters for 
 *	for each configuration (<B>touch_grounded(1)</B>)
 *
 * @return Current grounded status (after changing if that was requested)
 * @li 0/1 Success
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li EBADF - Call to set/get grounded state failed
 * @li ECONNREFUSED - not supported on this platform
 *
 */
/*SVC_PROTOTYPE*/ int touch_grounded(int grounded);


/** Get/Set the touch mode
 *
 * @param[in] mode
 * 		0 - 3 set current mode
 * 			@li 0 - Normal/menu - GUI usage, this is used for menus, scrolling, etc with a GUI
 * 			@li 1 - Signature capture - this mode is typically selected when capturing a signature where you want finer resolution and performance, yet some filtering is applied to reduce noise
 * 			@li 2 - Hardware Test - this mode removes all filtering, jitter reduction, and algorithms. Typically used for panel testing.
 * 		    @li 3 - Secure sign-on - typically used for secure sign-on screens
 *   	-1 - return current mode
 * 
 *   Notes:
 *   1. This call informs the touch driver when the terminal operational mode changes between
 *	normal (menu), signature capture, sign-on and HW DQ test.  This allows the driver to select
 *	the best set of operational parameters for each mode (<B>touch_grounded(1)</B>)
 *
 * @return Current mode (after changing if that was requested)
 * @li 0,1,2 Success
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li EINVAL - Invalid mode parameter
 * @li ECONNREFUSED - not supported on this platform
 *
 */
/*SVC_PROTOTYPE*/ int touch_mode(int mode);


/** Disable/Enable Atmel touch panels
 *
 * @param[in] enable
 * 		0 - 1 set current enable/disable
 * 			@li 0 - Disable Atmel panel (used during Contactless transactions)
 * 			@li 1 - Enable Atmel panel 
 *   	-1 - return current enable status (0 = disabled, 1 = enabled)
 * 
 *   Notes:
 *   1. This call allows contactless code to turn Atmel panels off to cut electrical noise
 * 	that otherwise interferes with contactless operation.
 *
 * @return Current enable status (after changing if that was requested)
 * @li 0,1 Success
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li EINVAL - Invalid parameter
 * @li ECONNREFUSED - not supported on this platform
 *
 */
/*SVC_PROTOTYPE*/ int touch_enable(int enable);


/** Return Atmel touch chip config version number
 *
 * @return
 * @li Version number 2 bytes MMmm on Success (MM is major version byte, mm is minor version byte)
 * @li -1 Error
 *
 * <b>Errno values</b>:
 * @li ECONNREFUSED - not supported on this platform
 *
 */
/*SVC_PROTOTYPE*/ int touch_getConfigVersion(void);


/** Generic Atmel mxt object access function
 *
 * @param[in] action
 * 		Enum defined in K2lib.h
 * 			@li 0 - Get size of object
 * 			@li 1 - Get number of instances of object
 * 			@li 2 - Get object data offset
 * 			@li 3 - Read data from object
 * 			@li 4 - Write data to object
 * @param[in] type
 * 		Object type number as defined in Atmel protocol guides
 * @param[in] instance
 * 		Instance of the object (0 is first instance)
 * @param[in] start
 * 		Offset into the object instance (0 means start at beginning of object)
 * @param[in] count
 * 		Number of bytes of object if read or write
 * 
 * @return Result of the operation
 * @li -1 Error
 * @li Size, Number of Instances or Offset for action 0,1,2
 * @li Number of bytes Read or Written for actions 3,4
 *
 * <b>Errno values</b>:
 * @li EINVAL - Invalid parameter
 * @li ENOENT - Null parameter when non-null is expected
 * @li ENOMSG - Invalid object number
 * @li EOVERFLOW - Invalid object instance specified
 * @li ENODATA - Null data pointer
 * @li EROFS - Write operation requested on a read-only object
 * @li ENOMEM - Unable to allocate memory for object data or sysfs object access path string
 * @li ECONNREFUSED - not supported on this platform
 *
 */
/*SVC_PROTOTYPE*/ int touch_atmelMxtOperation(int action, int type, int instance, int start, int count, unsigned char *data);


#ifdef __cplusplus
}
#endif
#endif //SVC_TOUCH_H
