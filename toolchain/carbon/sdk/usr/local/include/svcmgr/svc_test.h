#ifndef SVC_SERVICE_TEST_H
#define SVC_SERVICE_TEST_H

#include "svcmgrSvcDef.h"

#ifdef __cplusplus
extern "C" {
#endif

/*SVC_SERVICE:test*/

/*SVC_STRUCT*/
struct ary_char {
	int    m_sz_count;
	char** m_sz;
};

/*SVC_STRUCT*/
struct quad_sol {
	float sol1;
	float sol2;
};

/*SVC_STRUCT*/
struct arys {
	int*   i1;
	int*   ia;
	char*  sz;
	char** sz1;
	char** sza;
	int    ia_count;
	int    sza_count;
};

/*SVC_STRUCT*/
struct arysc {
	char* sz;
	char  szac[10];
	char* szaca[5];
	int   i;
	int*  pi;
	int   pic[9];
};

/*SVC_STRUCT*/
// note: 'QQ==' is base64binary for 'A'
struct testBin {
  void* data /*QQ==*/;
  int data_count;
};

/*SVC_STRUCT*/
struct testMultBin {
  struct testBin  bin;
  struct testBin* bin1;
  struct testBin* bina;
  int bina_count;
};

/*SVC_PROTOTYPE*/ struct version test_getVersion(void);
/*SVC_PROTOTYPE*/ void test_vv(void);
/*SVC_PROTOTYPE*/ void test_vv_err(void);
/*SVC_PROTOTYPE*/ int test_square(int x);
/*SVC_PROTOTYPE*/ int test_square_def(int x /*5*/);
/*SVC_PROTOTYPE*/ int test_square_req(int x /*REQ*/);
/*SVC_PROTOTYPE*/ char* test_prop_lang(void);
/*SVC_PROTOTYPE*/ struct ary_char test_prop_names(void);
/*SVC_PROTOTYPE*/ struct pairlist test_prop_list(void);
/*SVC_PROTOTYPE*/ struct quad_sol test_quad_equ(float a /*REQ*/, float b /*-1.0*/, float c);
/*SVC_PROTOTYPE*/ char* test_cat(char* sz1 /*hello*/, char* sz2 /*world*/);
/*SVC_PROTOTYPE*/ char* test_append(char* sz1);
/*SVC_PROTOTYPE*/ int test_clnt_doubler(int x /*REQ*/);
/*SVC_PROTOTYPE*/ int test_clnt_errno(void);
/*SVC_PROTOTYPE*/ int* test_multp(int* pi, int i);
/*SVC_PROTOTYPE*/ int test_mult(int* pi, int i);
/*SVC_PROTOTYPE*/ char* test_arys(struct arys* a);
/*SVC_PROTOTYPE*/ struct arys* test_arys_rtn(struct arysc* ac);
/*SVC_PROTOTYPE*/ char* test_arysc(struct arysc a);
/*SVC_PROTOTYPE*/ struct arysc test_arysc_rtn(char* sz, char szac[6] /*test*/, char* szaca[5], int i1 /*22*/, int* pi, int pic[8]);
/*SVC_PROTOTYPE*/ struct testBin test_bincp(struct testBin b);
/*SVC_PROTOTYPE*/ char* test_binToString(struct testBin b);
/*SVC_PROTOTYPE*/ struct testBin test_stringToBin(char* sz /*ABCD*/);
/*SVC_PROTOTYPE*/ char* test_servercat(char* passedStr);

/*SVC_PROTOTYPE*/ int test_startWebSocket(void);


#ifdef __cplusplus
}
#endif

#endif //SVC_SERVICE_TEST_H
