/** @addtogroup */
/** @{ */
/** @addtogroup */
/** @{ */
/** 
 *  @file platforminfo_api.h 
 *
 *	@brief Platform info library user header file provide platform and device specific information such as: Components version, device configuration, physical characteristics, etc.
 * 
 */

#ifndef __PLATFORMINFO_API_H__
#define __PLATFORMINFO_API_H__


#include <stdint.h>
#include <verifone/vfsys_defs.h>

/* Needed for MIB and CIB structures */
#include <verifone/cib.h>
#include <verifone/mib.h>

/**
 * @name Platform info version
*/
/** \{ */
#define PI_PLATFORMINFO_VER        "1.3.5"
/** \} */

/* Allow for C++ users */

#ifdef __cplusplus
extern "C" {
#endif


/* Legacy Info types */
/* NOTE: Usage of legacy InfoTypes is NOT RECOMMENDED for new applications */
/* Legacy InfoTypes list will not be updated to not break the number of the standard InfoTypes */

/**
 * Legacy Info types
 */
enum
{
    PI_LEGACY_UNIT_CONFIG = 0,        /**< buffer = pointer to 32bit unsigned int */
    PI_LEGACY_BBL_BAT_STATUS,         /**< buffer = pointer to 32bit unsigned int */
    PI_LEGACY_BBL_BAT_VOLTAGE,        /**< buffer = pointer to 32bit unsigned int */
    PI_LEGACY_MULTIPORT_CABLE_STATUS, /**< buffer = pointer to 32bit unsigned int */
    PI_LEGACY_SMARTCARD_VER,          /**< buffer = pointer to 32bit unsigned int */
    PI_LEGACY_PROCESSOR_MHZ,          /**< buffer = pointer to 32bit unsigned int */
    PI_LEGACY_TOUCHPANEL_PINNACLE,    /**< buffer = pointer to 32bit unsigned int */
    PI_LEGACY_INFO_CARD_MSR,          /**< buffer = pointer to 32bit unsigned int */
    PI_LEGACY_INFO_DSP,               /**< buffer = pointer to struct PI_legacy_info_display_st */
    PI_LEGACY_INFO_EPROM,             /**< buffer = pointer to string with size PI_LEGACY_INFO_EPROM_SIZE_NULL_TERM */
    PI_LEGACY_INFO_KERNEL,            /**< buffer = pointer to string with size PI_LEGACY_INFO_KERNEL_SIZE_NULL_TERM */
    PI_LEGACY_INFO_RFS,               /**< buffer = pointer to string with size PI_LEGACY_INFO_RFS_SIZE_NULL_TERM */
    PI_LEGACY_INFO_TYPE,              /**< buffer = pointer to struct PI_legacy_info_type_st */
    PI_LEGACY_INFO_KEYPAD,            /**< buffer = pointer to struct PI_legacy_info_keypad_st */
    PI_LEGACY_IO_MODULE_CONFIG,       /**< buffer = pointer to 32bit unsigned int */
    PI_LEGACY_FLASH_INFO,             /**< Please use PI_FLASH_INFO instead */
    PI_LEGACY_MODEL_MX,               /**< Please use PI_MODEL_MX instead */
    PI_LEGACY_SDRAM_SIZE,             /**< Please use PI_RAM_SIZE instead */
    PI_LEGACY_INFO_SN,                /**< Please use PI_SERIAL_NUM instead */
    PI_LEGACY_INFO_PTID,              /**< Please use PI_UNIT_ID instead */
    PI_LEGACY_INFO_MAC,               /**< Please use PI_MAC_ADDRESS instead */

    PI_MID_LAST_INFO_TYPE
};


/**
 * Info types
 */
enum
{
    PI_VERSION_KERNEL = PI_MID_LAST_INFO_TYPE +1, /**< buffer = pointer to string with size PI_VERSION_KERNEL_SIZE */
    PI_VERSION_RFS,                 /**< buffer = pointer to string with size PI_VERSION_RFS_SIZE */
    PI_VERSION_SECURITY_RFS,        /**< buffer = pointer to string with size PI_VERSION_SECURITY_RFS_SIZE */
    PI_VERSION_VAULT,               /**< buffer = pointer to string with size PI_VERSION_VAULT_SIZE */
    PI_VERSION_VAULT_PEDGUARD,      /**< buffer = pointer to string with size PI_VERSION_VAULT_PEDGAURD_SIZE */
    PI_VERSION_SBI,                 /**< buffer = pointer to string with size PI_VERSION_SBI_SIZE_NULL_TERM */
    PI_VERSION_CIB,                 /**< buffer = pointer to string with size PI_VERSION_CIB_SIZE_NULL_TERM */
    PI_VERSION_MIB,                 /**< buffer = pointer to string with size PI_VERSION_MIB_SIZE_NULL_TERM */
    PI_VERSION_UBOOT,               /**< buffer = pointer to string with size PI_VERSION_UBOOT_SIZE_NULL_TERM */
    PI_VERSION_RELEASE,             /**< buffer = pointer to string with size PI_VERSION_RELEASE_SIZE */
    PI_VERSION_PLATFORMINFO,        /**< buffer = pointer to string with size PI_VERSION_PLATFORMINFO_SIZE */
    PI_VERSION_CONTACTLESS,         /**< buffer = pointer to string with size PI_VERSION_CTLS_SIZE */
    PI_MAC_ADDRESS,                 /**< buffer = pointer to string with size PI_MAC_ADDRESS_SIZE_NULL_TERM */
    PI_SERIAL_NUM,                  /**< buffer = pointer to string with size PI_SERIAL_NUMBER_SIZE_NULL_TERM */
    PI_MODEL_MX,                    /**< buffer = pointer to 32bit unsigned int */
    PI_UNIT_ID,                     /**< buffer = pointer to string with size PI_UNIT_ID_SIZE_NULL_TERM */
    PI_RAM_SIZE,                    /**< buffer = pointer to 32bit unsigned int */
    PI_FLASH_INFO,                  /**< buffer = pointer to struct PI_flash_info_st */
    PI_CIB,                         /**< buffer = pointer to struct CIB_Struct */
    PI_MIB,                         /**< buffer = pointer to struct Manufacturing_Information_Block_t */
    PI_IO_MODULE_CONFIG,            /**< buffer = pointer to 32bit unsigned int */
    PI_PACKAGES_LIST_ENTRY,         /**< buffer = pointer to struct PI_pkginfo_st */
    PI_KERNEL_MODULES_LIST_ENTRY,   /**< Disabled due to security */
    PI_UART_MATRIX,                 /**< buffer = pointer to struct PI_uart_matrix_st */
    PI_DISPLAY_INFO,                /**< buffer = pointer to struct PI_display_info_st */
    PI_EXTERNAL_BATTERY_INFO,       /**< buffer = pointer to struct PI_external_battery_info_st */
    PI_VERSION_SRED,                /**< buffer = pointer to string with size PI_VERSION_SRED_SIZE */
    PI_VERSION_OPENPROTOCOL,        /**< buffer = pointer to string with size PI_VERSION_OPENPROTOCOL_SIZE */
    PI_WIFI_MAC_ADDRESS,            /**< buffer = pointer to string with size PI_MAC_ADDRESS_SIZE_NULL_TERM */
    PI_BT_MAC_ADDRESS,              /**< buffer = pointer to string with size PI_MAC_ADDRESS_SIZE_NULL_TERM */
    PI_DEVICE_SVID,                 /**< buffer = pointer to 32bit unsigned int */
    PI_VERSION_VHQ,                 /**< buffer = pointer to string with size PI_VERSION_VHQ_SIZE */
    PI_CIB_PRODUCT_NUMBER_STR,      /**< buffer = pointer to string with size PI_CIB_PRODUCT_NUMBER_SIZE */
    PI_MIB_PART_NUMBER,             /**< buffer = pointer to string with size PI_MIB_PART_NUMBER_SIZE */
    PI_MIB_MODEL_NUM,               /**< buffer = pointer to string with size PI_MIB_MODEL_NUM_SIZE */
    PI_MIB_COUNTRY,                 /**< buffer = pointer to string with size PI_MIB_COUNTRY_SIZE */
    PI_ETHERNET_INFO,               /**< buffer = pointer to struct PI_ethernet_info_st */
    PI_PRINTER_INFO,                /**< buffer = pointer to struct PI_printer_info_st */
    PI_MODEM_INFO,                  /**< buffer = pointer to struct PI_modem_info_st */
    PI_MSR_INFO,                    /**< buffer = pointer to struct PI_msr_info_st */
    PI_TOUCH_SCREEN_INFO,           /**< buffer = pointer to struct PI_touch_screen_info_st */
    PI_CTLS_INFO,                   /**< buffer = pointer to struct PI_ctls_info_st */
    PI_SMART_CARD_INFO,             /**< buffer = pointer to struct PI_smart_card_info_st */
    PI_WRENCHMAN_INFO,              /**< buffer = pointer to struct PI_wrenchman_info_st */
    PI_DEVICE_CHIP_ID,              /**< buffer = pointer to string with size PI_DEVICE_CHIP_ID_SIZE */
    PI_IO_MODULE_COM_STATE,         /**< buffer = pointer to struct PI_io_module_com_state_st */
    PI_CIB_MODEL_ID_STR,            /**< buffer = pointer to string with size PI_CIB_MODEL_ID_SIZE */
    PI_EEPROMS_INFO,                /**< buffer = pointer to struct PI_eeproms_info_st */
    PI_IO_MODULE_INFO,              /**< buffer = pointer to struct PI_io_module_info_st */
    PI_SDIO_INFO,                   /**< buffer = pointer to struct PI_sdio_info_st */
    PI_KEYPAD_INFO,                 /**< buffer = pointer to struct PI_keypad_info_st */
    PI_GPRS_INFO,                   /**< buffer = pointer to struct PI_gprs_info_st */
    PI_ETH2_MAC_ADDRESS,            /**< buffer = pointer to string with size PI_MAC_ADDRESS_SIZE_NULL_TERM */
    PI_UX400_PRESENT,               /**< buffer = pointer to 32bit unsigned int */
    PI_BASE_STATION_INFO,           /**< buffer = pointer to struct PI_base_station_info_st */
    PI_BLUETOOTH_INFO,              /**< buffer = pointer to struct PI_bluetooth_info_st */
    PI_SECURITY_DETAMPER_OPTION,    /**< buffer = pointer to 32bit unsigned int */
    PI_RAM_INFO,                    /**< buffer = pointer to struct PI_ram_info_st */
    PI_WIFI_INFO,                   /**< buffer = pointer to struct PI_wifi_info_st */
    PI_GPS_INFO,                    /**< buffer = pointer to struct PI_gps_info_st */
    PI_ADC0_HARDWARE_ID,            /**< buffer = pointer to 32bit unsigned int */
    PI_MIB_MFG_DATE_TIME,           /**< buffer = pointer to struct PI_mib_mfg_date_time_st */
    PI_NUM_OF_LAST_INFO_TYPE,       /**< buffer = pointer to 32bit unsigned int */
    PI_REMOTE_PRINTER_SUPPORT,      /**< buffer = pointer to 32bit unsigned int; Note: UX30x supported only! */
    PI_MDB_SUPPORT,                 /**< buffer = pointer to 32bit unsigned int; Note: UX30x supported only! */
    PI_FSM_INFO,                    /**< buffer = pointer to struct PI_fsm_info_st */
    PI_CUSTOMER_DISPLAY_INFO,       /**< buffer = pointer to struct PI_customer_display_info_st */
    PI_SOUND_INFO,                  /**< buffer = pointer to struct PI_sound_info_st */
    PI_SYSFS_DEVICE_INFO,           /**< buffer = pointer to struct PI_sysfs_device_path_st */
    PI_SAM_INFO,                    /**< buffer = pointer to struct PI_sam_info */
    PI_APPDEV_OTP_INFO,             /**< buffer = pointer to struct PI_nv_device_mode_info */
    PI_VATS_OTP_INFO,			    /**< buffer = pointer to struct PI_nv_device_mode_info */
    PI_NONPAYMENT_OTP_INFO,         /**< buffer = pointer to struct PI_nv_device_mode_info */
    PI_PCI_OTP_INFO,	            /**< buffer = pointer to struct PI_nv_device_mode_info */
#ifdef RAPTOR
	PI_DL_TMPFS_INFO,				/**< buffer = pointer to struct PI_flash_info_st */
#endif
    PI_CTLS_ENV,                    /**< buffer = pointer to 32bit unsigned int */
    PI_CTLS_ANT_STAT,               /**< buffer = pointer to 32bit unsigned int */
#ifdef RAPTOR
    PI_SECURE_APPLET_INFO,          /**< buffer = pointer to struct PI_applet_name_st */
    PI_GENERIC_DEVICE_TREE_INFO,    /**< buffer = pointer to struct PI_generic_devicetree_info */
#endif
    PI_GET_STANDBY_MCU_INFO,
    PI_CIB_DATE_TIME,
#ifdef RAPTOR
    PI_IS_MANUF_TESTING_ENABLED,    /**< buffer = pointer to int */
#endif
    // Add more InfoTypes above this comment
    PI_LAST_INFO_TYPE
};


/**
 * Error codes
 */
enum PI_error_codes
{
    PI_OK = 0,
    PI_OK_LAST_ENTRY            =  1, /**< OK, Last entry in a list (Not an error) */

    PI_ERROR_UNKNOWN_INFO_TYPE  = -1, /**< Unknown info type */
    PI_ERROR_FATAL              = -2, /**< Bad pointer */
    PI_ERROR_SIZE               = -3, /**< Insufficient buffer length */
    PI_ERROR_INTERNAL           = -4, /**< Library internal error */
    PI_ERROR_MISMATCH           = -5, /**< Mismatch error */
    PI_ERROR_NOT_EXIST          = -6, /**< Requested data not exist */
    PI_ERROR_CIB_ERROR          = -7, /**< Required data demand CIB which is missing/corrupted */
    PI_ERROR_MIB_ERROR          = -8, /**< Required data demand MIB which is missing/corrupted */
    PI_ERROR_PERMISSION         = -9, /**< Required data can't be accessed */
    PI_ERROR_NOT_SUPPORTED      =-10, /**< Not supported */

    PI_ERROR_LAST
};

/**
 * @name Convert ERROR Codes into string, use this MACRO to help identify errors
*/
/** \{ */
#define PI_ERR_TO_TXT(err_code)         ((err_code) == PI_OK) ?                    "PI_OK":\
                                         ((err_code) == PI_OK_LAST_ENTRY)?          "PI_OK_LAST_ENTRY":\
                                         ((err_code) == PI_ERROR_UNKNOWN_INFO_TYPE)?"PI_ERROR_UNKNOWN_INFO_TYPE":\
                                         ((err_code) == PI_ERROR_FATAL) ?           "PI_ERROR_FATAL":\
                                         ((err_code) == PI_ERROR_SIZE) ?            "PI_ERROR_SIZE":\
                                         ((err_code) == PI_ERROR_INTERNAL) ?        "PI_ERROR_INTERNAL":\
                                         ((err_code) == PI_ERROR_MISMATCH) ?        "PI_ERROR_MISMATCH":\
                                         ((err_code) == PI_ERROR_NOT_EXIST) ?       "PI_ERROR_NOT_EXIST":\
                                         ((err_code) == PI_ERROR_CIB_ERROR) ?       "PI_ERROR_CIB_ERROR":\
                                         ((err_code) == PI_ERROR_MIB_ERROR) ?       "PI_ERROR_MIB_ERROR":\
                                         ((err_code) == PI_ERROR_PERMISSION) ?      "PI_ERROR_PERMISSION":\
                                         ((err_code) == PI_ERROR_NOT_SUPPORTED) ?   "PI_ERROR_NOT_SUPPORTED":\
                                                 "PI_ERROR_UNKNOWN_ERR_CODE"  // shall be last
/** \} */


/**
 * @name Platform info size defines, user applications shall use buffer size including NULL termination
*/
/** \{ */
#define PI_LEGACY_INFO_KERNEL_SIZE              8
#define PI_LEGACY_INFO_KERNEL_SIZE_NULL_TERM    PI_LEGACY_INFO_KERNEL_SIZE+1
#define PI_LEGACY_INFO_RFS_SIZE                 8
#define PI_LEGACY_INFO_RFS_SIZE_NULL_TERM       PI_LEGACY_INFO_RFS_SIZE+1
#define PI_LEGACY_INFO_EPROM_SIZE               8
#define PI_LEGACY_INFO_EPROM_SIZE_NULL_TERM     PI_LEGACY_INFO_EPROM_SIZE+1

#define PI_VERSION_KERNEL_SIZE          30
#define PI_VERSION_RFS_SIZE             20
#define PI_VERSION_SECURITY_RFS_SIZE    20
#define PI_VERSION_VAULT_SIZE           20
#define PI_VERSION_VAULT_PEDGUARD_SIZE  20
#define PI_VERSION_SBI_SIZE             25
#define PI_VERSION_SBI_SIZE_NULL_TERM   PI_VERSION_SBI_SIZE+1
#define PI_VERSION_UBOOT_SIZE           25
#define PI_VERSION_UBOOT_SIZE_NULL_TERM PI_VERSION_UBOOT_SIZE+1
#define PI_VERSION_CIB_SIZE             5
#define PI_VERSION_CIB_SIZE_NULL_TERM   PI_VERSION_CIB_SIZE+1
#define PI_VERSION_MIB_SIZE             4                     /**< Use PI_DEVICE_REV_SIZE instead */
#define PI_VERSION_MIB_SIZE_NULL_TERM   PI_VERSION_MIB_SIZE+1 /**< Use PI_DEVICE_REV_SIZE_NULL_TERM instead */
#define PI_VERSION_PLATFORMINFO_SIZE    20
#define PI_VERSION_WRENCHMAN_FW_SIZE    16
#define PI_VERSION_WRENCHMAN_FW_SIZE_NULL_TERM  PI_VERSION_WRENCHMAN_FW_SIZE + 1
#define PI_VERSION_RELEASE_SIZE         30
#define PI_VERSION_CTLS_SIZE            30
#define PI_VERSION_SRED_SIZE            20
#define PI_VERSION_OPENPROTOCOL_SIZE    20
#define PI_VERSION_VHQ_SIZE             20
#define PI_DEVICE_REV_SIZE              4
#define PI_DEVICE_REV_SIZE_NULL_TERM    PI_DEVICE_REV_SIZE+1
#define PI_MAC_ADDRESS_SIZE             12
#define PI_MAC_ADDRESS_SIZE_NULL_TERM   PI_MAC_ADDRESS_SIZE+1
#define PI_SERIAL_NUMBER_SIZE           16
#define PI_SERIAL_NUMBER_SIZE_NULL_TERM PI_SERIAL_NUMBER_SIZE+1
#define PI_UNIT_ID_SIZE                 8
#define PI_UNIT_ID_SIZE_NULL_TERM       PI_UNIT_ID_SIZE+1
#define PI_CIB_PRODUCT_NUMBER_SIZE      12 /**< 11+1 NULL terminated */
#define PI_MIB_PART_NUMBER_SIZE         33 /**< 32+1 NULL terminated */
#define PI_MIB_MODEL_NUM_SIZE           13 /**< 12+1 NULL terminated */
#define PI_MIB_COUNTRY_SIZE             13 /**< 12+1 NULL terminated */
#define PI_DEVICE_CHIP_ID_SIZE          16
#define PI_CIB_MODEL_ID_SIZE            13 /**< 12+1 NULL terminated */
#define PI_CIB_DATE_TIME_SIZE			40
#define PI_CIB_DATE_TIME_SIZE_NULL_TERM 41

/** \} */


/**
 * @name Battery defines
*/
/** \{ */
#define PI_BATTERY_OK                   1
#define PI_BATTERY_BAD                  0
#define PI_BATTERY_EXIST                1
#define PI_BATTERY_REQUIRED             1
#define PI_BATTERY_CHARGING             1
#define PI_BATTERY_TYPE_SMART_BATT      1
#define PI_BATTERY_TYPE_NO_GAUGE_BATT   2
/** \} */


/**
 * @name I/O modules bit-wise
*/
/** \{ */
//#define PI_IO_MODULE_RFID               (1 << 0)   /* The RFID at MxTrident product is in the device, not in I/O module */
#define PI_IO_MODULE_WIFI               (1 << 1)
#define PI_IO_MODULE_COM1               (1 << 2)
#define PI_IO_MODULE_COM2               (1 << 3)
#define PI_IO_MODULE_AUDIO              (1 << 4)
#define PI_IO_MODULE_USB_OTG            (1 << 5)
#define PI_IO_MODULE_USB_HOST           (1 << 6)
#define PI_IO_MODULE_POE                (1 << 7)
#define PI_IO_MODULE_ETHERNET           (1 << 8)
#define PI_IO_MODULE_BLUETOOTH          (1 << 9)
#define PI_IO_MODULE_GPRS               (1 << 10)
#define PI_IO_MODULE_TAILGATE           (1 << 11)
#define PI_IO_MODULE_MULTI              (1 << 12)
#define PI_IO_MODULE_POWER              (1 << 13)
#define PI_IO_MODULE_MDB                (1 << 14)
#define PI_IO_MODULE_RS485              (1 << 15)
#define PI_IO_MODULE_WAN                (1 << 16)
#define PI_IO_MODULE_USB_DEVICE         (1 << 17)
#define PI_IO_MODULE_COM3               (1 << 18)
#define PI_IO_MODULE_FSM                (1 << 19)
#define PI_IO_MODULE_MODEM              (1 << 20)
#define PI_IO_MODULE_CASH_DRAWER        (1 << 21)
#define PI_IO_MODULE_PRINTER            (1 << 22)
#define PI_IO_MODULE_LCD                (1 << 23)


#define PI_IO_MODULE_HWRV_SIZE          (3)
#define PI_IO_MODULE_INFO_EMPTY         (0x01)
/** \} */


/**
 * @name Kernel module struct fields size
*/
/** \{ */
#define PI_MODULE_NAME_MAX_LEN          36
#define PI_MODULE_VERSION_MAX_LEN       16
#define PI_MODULE_TYPE_MAX_LEN          16
#define PI_MODULE_DESC_MAX_LEN          50
/** \} */


/**
 * @name UART Matrix COM path string size
*/
/** \{ */
#define PI_COM_SIZE                     16
/** \} */


/**
 * Device modes
 */
enum PI_device_modes
{
    PI_DEVICE_MODE_MANU = 0,
    PI_DEVICE_MODE_PROD = 1,
    PI_DEVICE_MODE_TEST = 7,
    PI_DEVICE_MODE_DEV  = 8
};

/**
 * @name Display defines
*/
/** \{ */
#define PI_DISPLAY_BLACK_WHITE          0
#define PI_DISPLAY_COLOR                1
#define PI_DISPLAY_NAME_MAX_LEN         30
#define PI_DISPLAY_IF_TYPE_SPI          4
#define PI_DISPLAY_IF_TYPE_UX_BRIDGE    7
#define PI_DISPLAY_IF_TYPE_PARALLEL     8
#define PI_DISPLAY_IF_TYPE_CONTROLLER   9
/** \} */


/**
 * @name Ethernet defines
*/
/** \{ */
#define PI_ETHERNET_NOT_EXIST           0
#define PI_ETHERNET_EXIST               1
#define PI_ETHERNET_TYPE_PRSNT_W_O_LEDS 1
#define PI_ETHERNET_TYPE_PRSNT_WITH_LEDS 2
#define PI_ETHERNET_DUPLEX_HALF         0
#define PI_ETHERNET_DUPLEX_FULL         1
#define PI_ETHERNET_SPEED_10M           10
#define PI_ETHERNET_SPEED_100M          100
#define PI_ETHERNET_SPEED_1000M         1000
/** \} */


/**
 * @name Printer defines
*/
/** \{ */
#define PI_PRINTER_NOT_EXIST           0
#define PI_PRINTER_EXIST               1
#define PI_PRINTER_TYPE_THERMAL_1      1
#define PI_PRINTER_TYPE_THERMAL_2      2
#define PI_PRINTER_TYPE_USB            4
/** \} */


/**
 * @name Modem defines
*/
/** \{ */
#define PI_MODEM_NOT_EXIST             0
#define PI_MODEM_EXIST                 1
#define PI_MODEM_TYPE_UART             1
#define PI_MODEM_TYPE_USB_HOST         3
#define PI_MODEM_TYPE_BLUETOOTH        4
/** \} */


/**
 * @name MSR defines
*/
/** \{ */
#define PI_MSR_NOT_EXIST               0
#define PI_MSR_EXIST                   1
#define PI_MSR_TYPE_I2C                0x05
#define PI_MSR_TYPE_SCANNING_ADC       0x0B
/** \} */


/**
 * @name Touch screen defines
*/
/** \{ */
#define PI_TOUCH_SCREEN_NOT_EXIST      0
#define PI_TOUCH_SCREEN_EXIST          1
#define PI_TOUCH_SCREEN_TYPE_SPI       0x04 /**< SPI Capacitive touch screen */
#define PI_TOUCH_SCREEN_TYPE_I2C       0x05 /**< I2C Capacitive touch screen */
#define PI_TOUCH_SCREEN_TYPE_K2        0x06 /**< Cirque K2 Capacitive touch screen */
#define PI_TOUCH_SCREEN_TYPE_ADC       0x0A /**< Resistive touch screen */
/** \} */


/**
 * @name Contactless defines
*/
/** \{ */
#define PI_CTLS_NOT_EXIST              0
#define PI_CTLS_EXIST                  1
#define PI_CTLS_TYPE_SPI               0x04
/** \} */


/**
 * @name Smart card defines
*/
/** \{ */
#define PI_SMART_CARD_NOT_EXIST        0
#define PI_SMART_CARD_EXIST            1
#define PI_SMART_CARD_TYPE_I2C         0x05
#define PI_SMART_CARD_MOTOR_NOT_EXIST  0
#define PI_SMART_CARD_MOTOR_DDS855     1
/** \} */


/**
 * @name Wrenchman defines
*/
/** \{ */
#define PI_WRENCHMAN_NOT_EXIST         0
#define PI_WRENCHMAN_EXIST             1
#define PI_WRENCHMAN_TYPE_SPI          0x04
/** \} */


/**
 * @name EEPROMs defines
*/
/** \{ */
#define PI_EEPROM_MAX_DEVICES          3
#define PI_EEPROM_MAIN_BOARD           0
#define PI_EEPROM_IO_MODULE            1
#define PI_EEPROM_NOT_EXIST            0
#define PI_EEPROM_EXIST                1
#define PI_EEPROM_TYPE_SPI             0x04
#define PI_EEPROM_TYPE_I2C             0x05
/** \} */


/**
 * @name SDIO defines
*/
/** \{ */
#define PI_SDIO_SOCKET_NOT_EXIST       0
#define PI_SDIO_SOCKET_EXIST           1
/** \} */


/**
 * @name Keypad defines
*/
/** \{ */
#define PI_KEYPAD_NOT_EXIST            0
#define PI_KEYPAD_EXIST                1
#define PI_KEYPAD_TYPE_SPI             0x04
#define PI_KEYPAD_TYPE_I2C             0x05
#define PI_KEYPAD_TYPE_GPIO            0x06
#define PI_KEYPAD_TYPE_USB             0x07
#define PI_KEYPAD_FUNC_NOT_EXIST       0
#define PI_KEYPAD_FUNC_EXIST           1
/** \} */


/**
 * @name GPRS defines
*/
/** \{ */
#define PI_GPRS_NOT_EXIST              0
#define PI_GPRS_EXIST                  1
#define PI_GPRS_TYPE_UART              1
#define PI_GPRS_TYPE_USB_HOST          2

#define PI_GPRS_RADIO_TYPE_2G          0
#define PI_GPRS_RADIO_TYPE_3G          1
/** \} */


/**
 * @name Base station defines
*/
/** \{ */
#define PI_BASE_STATION_USB_TYPE       1
#define PI_BASE_STATION_TYPE           2 /**< GEN / BT&WIFI */
/** \} */


/**
 * @name Bluetooth defines
*/
/** \{ */
#define PI_BLUETOOTH_NOT_EXIST         0
#define PI_BLUETOOTH_EXIST             1
#define PI_BLUETOOTH_TYPE_UART         1
#define PI_BLUETOOTH_TYPE_USB_HOST     2
/** \} */


/**
 * @name WiFi defines
*/
/** \{ */
#define PI_WIFI_NOT_EXIST              0
#define PI_WIFI_EXIST                  1
#define PI_WIFI_TYPE_UART              1
#define PI_WIFI_TYPE_USB_HOST          2
#define PI_WIFI_TYPE_SDIO              3
/** \} */


/**
 * @name GPS defines
*/
/** \{ */
#define PI_GPS_NOT_EXIST               0
#define PI_GPS_EXIST                   1
#define PI_GPS_TYPE_USB                1
/** \} */


/**
 * @name FSM (Fiscal Module) defines
*/
/** \{ */
#define PI_FSM_NOT_EXIST               0
#define PI_FSM_EXIST                   1
#define PI_FSM_TYPE_UART               1
#define PI_FSM_TYPE_USB                2
/** \} */


/**
 * @name Customer display (2nd display) defines
*/
/** \{ */
#define PI_CUSTOMER_DISPLAY_NOT_EXIST  0
#define PI_CUSTOMER_DISPLAY_EXIST      1
#define PI_CUSTOMER_DISPLAY_TYPE_SPI   4
/** \} */


/**
 * @name Sound defines
*/
/** \{ */
#define PI_SOUND_NOT_EXIST               0
#define PI_SOUND_EXIST                   1
/** \} */


/**
 * @name Security defines
*/
/** \{ */
#define PI_SECURITY_STANDARD_DETAMPER  0
#define PI_SECURITY_KLD_DETAMPER       1
/** \} */


/**
 * @name ADC0 Hardware ID
*/
/** \{ */
#define PI_ADC0_HARDWARE_ID_PCI4       (1 << 31) /**< PCI4 unit */
/** \} */


/**
 * @name Remote printer support defines
*/
/** \{ */
#define PI_REMOTE_PRINTER_NOT_SUPPORTED 0
#define PI_REMOTE_PRINTER_SUPPORTED     1
/** \} */


/**
 * @name MDB support defines
*/
/** \{ */
#define PI_MDB_NOT_SUPPORTED            0
#define PI_MDB_SUPPORTED                1
/** \} */


/**
 * @name Backlight defines
*/
/** \{ */
#define PI_SYSFS_DEVICE_MAX_LEN	512
#define PI_SYSFS_ATTR_VALUE_MAX_LEN	50
/** \} */


/**
 * @name Legacy Unit Config bit-wise
*/
/** \{ */
#define PI_LEGACY_UNIT_CONFIG_ETHERNET   (1 << 0)
#define PI_LEGACY_UNIT_CONFIG_SMARTCARD  (1 << 1)
#define PI_LEGACY_UNIT_CONFIG_AUDIO      (1 << 2)
#define PI_LEGACY_UNIT_CONFIG_BEEPER     (1 << 3)
#define PI_LEGACY_UNIT_CONFIG_TOUCHPANEL (1 << 4)
/** \} */


/**
 * @name Legacy Multiport cable bit-wise
*/
/** \{ */
#define PI_LEGACY_MPCABLE_SPECTRUM       (1 << 0)  /**< Everest = 0, Spectrum = 1 */
#define PI_LEGACY_MPCABLE_USB_DEVICE     (1 << 1)  /**< Host = 0, Device (Client) = 1 */
#define PI_LEGACY_MPCABLE_COM1_RS485     (1 << 2)  /**< RS232 = 0, RS485 LAN = 1 */
#define PI_LEGACY_MPCABLE_COM3_TAILGATE  (1 << 3)  /**< RS232 = 0, RS485 Tailgate = 1 */
#define PI_LEGACY_MPCABLE_COM1_ATTACHED  (1 << 4)  /**< COM1 Attached = 1 */
#define PI_LEGACY_MPCABLE_COM2_ATTACHED  (1 << 5)  /**< COM2 Attached = 1 */
#define PI_LEGACY_MPCABLE_COM3_ATTACHED  (1 << 6)  /**< COM3 Attached = 1 */
#define PI_LEGACY_MPCABLE_AUDIO          (1 << 7)  /**< Audio headphone connector exist = 1 */
#define PI_LEGACY_MPCABLE_NO_ETHERNET    (1 << 8)  /**< No Ethernet = 1 */
#define PI_LEGACY_MPCABLE_NO_USB         (1 << 9)  /**< No USB = 1 */
#define PI_LEGACY_MPCABLE_NO_COM1        (1 << 10) /**< No COM1 = 1 */
#define PI_LEGACY_MPCABLE_NO_COM2        (1 << 11) /**< No COM2 = 1 */
#define PI_LEGACY_MPCABLE_NO_COM3        (1 << 12) /**< No COM3 = 1 */
/** \} */


/**
 * @name Legacy I/O module bit-wise
*/
/** \{ */
#define PI_LEGACY_IO_MODULE_CONTACTLESS  0x1 /**< I/O module have Contactless */
#define PI_LEGACY_IO_MODULE_WIFI         0x2 /**< I/O module have WiFi */
/** \} */


/**
 * @name Legacy keypad type
*/
/** \{ */
#define PI_LEGACY_KEYPAD_TYPE_HARD_REAL    1
#define PI_LEGACY_KEYPAD_TYPE_TOUCH_SCREEN 2
/** \} */

/**
 * @name CTLS Reader Environment
*/
/** \{ */
#define PI_CTLS_ENV_NON_METAL  0
#define PI_CTLS_ENV_METAL      1
/** \} */


/**
 * @name CTLS Antenna Status
*/
/** \{ */
#define PI_CTLS_ANT_FAULT   0
#define PI_CTLS_ANT_PRESENT 1
/** \} */

#ifdef RAPTOR
/**
 * @name Secure Applet Name / Version information
*/
#define PI_MAX_APPLET_NAME_SIZE             32
#define PI_MAX_APPLET_VER_SIZE              32
#define PI_MAX_APPLETS                      20

/** \} */
#endif


/**
 * Buzzer info
 */
typedef struct
{
    int exist;
} PI_buzzer_info_st;

/**
 * Speaker info
 */
typedef struct
{
    int exist;
} PI_speaker_info_st;

/**
 * SAM info
 */
typedef struct
{
    int exist;
    int isPrimarySocket;
    int numOfSecSockets;
} PI_sam_info_st;

/**
 * Flash info
 */
typedef struct
{
    unsigned long total;
    unsigned long used;
    unsigned long free;
    unsigned long percent_used;
} PI_flash_info_st;

/**
 * RAM info
 */
typedef struct
{
    unsigned long total;
    unsigned long used;
    unsigned long free;
    unsigned long percent_used;
} PI_ram_info_st;

/**
 * UART Matrix structure, fields value is tty path
 */
typedef struct
{
    char com1[PI_COM_SIZE];
    char com2[PI_COM_SIZE];
    char com3[PI_COM_SIZE];
    char com4[PI_COM_SIZE];
    char com5[PI_COM_SIZE];
    char com6[PI_COM_SIZE];
} PI_uart_matrix_st;

/**
 * External battery info
 */
typedef struct
{
    unsigned long voltage;  /**< In units of 0.01v */
    unsigned long status;   /**< 1 = OK,  0 = bad */
    char capacity;          /**< Percentage (e.g 81 = 81%) */
    char exist;             /**< 1 = yes, 0 = no */
    char charging;          /**< 1 = yes, 0 = no */
    char battery_required;  /**< 1 = yes, 0 = no */
    char ctrl_id;           /**< External controller ID */
    char power_connected;   /**< 1 = yes, 0 = no */
    signed char temperature;/**< -10 --> 60 [C] */
    char type;              /**< 1 = Smart battery, 2 = Simple battery (No fuel gauge) */
    unsigned short max_charge; /**< Units [mAh], e.g: 2200mAh, 2450mAh */
    char reserved[10];
} PI_external_battery_info_st;

/**
 * Package info
 */
typedef SecinsPkgInfo PI_pkginfo_st;

/**
 * Kernel module info
 */
typedef struct
{
    char name        [PI_MODULE_NAME_MAX_LEN + 1];
    char version     [PI_MODULE_VERSION_MAX_LEN + 1];
    char type        [PI_MODULE_TYPE_MAX_LEN + 1];
    char description [PI_MODULE_DESC_MAX_LEN + 1];
} PI_kernel_module_info_st;

/**
 * Display info
 */
typedef struct
{
    int color;
    int pixels_per_row;
    int pixels_per_col;
    int type; /**< Model type */
    char name[PI_DISPLAY_NAME_MAX_LEN];
    int interface_type;
    char reserved[20];
} PI_display_info_st; /**< Total size = 70 bytes */

/**
 * Ethernet info
 */
typedef struct
{
    int  exist;
    int  type;
    int  link_up;
    char mac[PI_MAC_ADDRESS_SIZE];
    char reserved[20];
} PI_ethernet_info_st;

/**
 * Printer info
 */
typedef struct
{
    int exist;
    int type;
    char reserved[20];
} PI_printer_info_st;

/**
 * Modem info
 */
typedef struct
{
    int exist;
    int type;
    int port;
    char ctrl_id;
    char dev[15];
    char reserved[4];
} PI_modem_info_st;

/**
 * MSR info
 */
typedef struct
{
    int exist;
    int type;
    int port;
    char reserved[20];
} PI_msr_info_st;

/**
 * Touch screen info
 */
typedef struct
{
    int exist;
    int type;
    int port;
    int ctrl_id;
    int address;
    char reserved[12];
} PI_touch_screen_info_st;

/**
 * Contactless info
 */
typedef struct
{
    int exist;
    int type;
    int port;
    int config_id;
    char reserved[20];
} PI_ctls_info_st;

/**
 * Smart card info
 */
typedef struct
{
    int exist;
    int type;
    int port;
    int motor;
    char reserved[16];
} PI_smart_card_info_st;

/**
 * Wrenchman info
 */
typedef struct
{
    int exist;
    int type;
    int port;
    char reserved[20];
} PI_wrenchman_info_st;

/**
 * EEPROM data structure
 */
typedef struct
{
    int exist;
    int type;
    int port;
    int address;
    int size;
    char reserved[10];
} PI_eeprom_data_st;

/**
 * EEPROMs info
 */
typedef struct
{
   /** eeprom 0 - Main board
    * eeprom 1 - IO Module
    * eeprom 2 - Not used
    */
    PI_eeprom_data_st eeprom[PI_EEPROM_MAX_DEVICES];
} PI_eeproms_info_st;

/**
 * IO Module COM state structure
 */
typedef struct
{
    int com1_attached;
    int com2_attached;
    int com3_attached;
}PI_io_module_com_state_st;

/**
 * IO Module info
 */
typedef struct
{
    int type;
    char hwrv[PI_IO_MODULE_HWRV_SIZE+1];
    char reserved[20];
}PI_io_module_info_st;

/**
 * SDIO  info
 */
typedef struct
{
    int sdio_a_exist;
    int sdio_b_exist;
    int sdio_a_detected;
    int sdio_b_detected;
    char reserved[20];
}PI_sdio_info_st;

/**
 * Keypad info
 */
typedef struct
{
    int exist;
    int type;
    int port;
    int num_standard_keys;
    int func_keypad_exist;
    char reserved[30];
}PI_keypad_info_st;

/**
 * GPRS info
 */
typedef struct
{
    int exist;
    int type;
    int port;
    int ctrl_id;
    char radio_type;  /**< 0 = 2G GPRS, 1 = 3G GSM */
    char dual_sim;
    char reserved[18];
}PI_gprs_info_st;

/**
 * Base station info
 */
typedef struct
{
    int exist;
    int interface_type;
    int ethernet_exist;
    int modem_exist;
    int uart_exist;
    int usb_host_exist;
    char reserved[20];
}PI_base_station_info_st;

/**
 * Bluetooth info
 */
typedef struct
{
    char exist;
    char type;
    char port;
    char ctrl_id;
    char bt_address[PI_MAC_ADDRESS_SIZE];
    char reserved[18];
}PI_bluetooth_info_st;

/**
 * WiFi info
 */
typedef struct
{
    char exist;
    char type;
    char port;
    char ctrl_id;
    char mac_address[PI_MAC_ADDRESS_SIZE];
    char reserved[18];
}PI_wifi_info_st;

/**
 * GPS info
 */
typedef struct
{
    char exist;
    char type;
    char port;
    char ctrl_id;
    char reserved[20];
}PI_gps_info_st;

/**
 * MIB Date/Time structure
 */
typedef struct
{
    int year;
    int month;
    int day;
    int hour;
    int minute;
    int second;
    char reserved[20];
}PI_mib_mfg_date_time_st;

/**
 * FSM info
 */
typedef struct
{
    char exist;
    char type;
    char port;
    char ctrl_id;
    char reserved[30];
}PI_fsm_info_st;

/**
 * Customer display info
 */
typedef struct
{
    char exist;
    char type;
    char port;
    char ctrl_id;
    unsigned long pixels_per_row;
    unsigned long pixels_per_col;
    char backlight_exist;
    char reserved[20];
}PI_customer_display_info_st;

/**
 * Sound info
 */
typedef struct
{
    char audio_exist;
    char buzzer_exist;
    char reserved[20];
}PI_sound_info_st;

/**
 * Sysfs info
 */
typedef struct
{
    char subsystem[PI_SYSFS_ATTR_VALUE_MAX_LEN];
    char device[PI_SYSFS_ATTR_VALUE_MAX_LEN];
    char sysattr[PI_SYSFS_ATTR_VALUE_MAX_LEN];
    char path[PI_SYSFS_DEVICE_MAX_LEN];
    char value[PI_SYSFS_ATTR_VALUE_MAX_LEN];
}PI_sysfs_device_info_st;

/**
 * Legacy type info
 */
typedef struct
{
    unsigned long processor;  /**< 7 = vf210x chip */
    unsigned long platform;   /**< 2000 = V/OS */
} PI_legacy_info_type_st;

/**
 * Legacy keypad info
 */
typedef struct
{
    unsigned long num_standard_keys;
    unsigned long keypad_type;           /**< 1 = Telco (Hard real keypad), 2 = Touch screen keypad */
    unsigned long touch_panel_installed; /**< 1 = Yes, 0 = No */
} PI_legacy_info_keypad_st;

/**
 * Legacy display info
 */
typedef struct
{
    unsigned long type;                  /**< 0 = segmented, 1 = pixel (default) */
    unsigned long pixels_per_row;
    unsigned long pixels_per_col;
    unsigned long contrast_adjustable;   /**< 0 = No (default), 1 = Yes */
} PI_legacy_info_display_st;

#ifdef RAPTOR
/**
 * Secure Applet Name / Version information
 */
typedef struct
{
    char pi_applet_name [PI_MAX_APPLET_NAME_SIZE];
    char pi_applet_version [PI_MAX_APPLET_VER_SIZE];
} PI_applet_st;

typedef struct
{
    int pi_num_applets;

    PI_applet_st  pi_applet_name_ver [PI_MAX_APPLETS ];

} PI_applet_name_ver_st;

#endif

/** 
 * Get required platform information into user buffer
 *
 * @note This is the only API of this library
 *
 * @param[in] InfoType Required information type
 * @param[in] buffer Pointer to user buffer / data structure
 * @param[in] len Size of user buffer in bytes
 * @param[in] rsize Returned size of buffer in bytes
 *
 * @return 
 * @li PI_OK = OK
 * @li PI_OK_LAST_ENTRY = No more entries on list (Not error)
 * @li PI_ERROR_UNKNOWN_INFO_TYPE = Unknown info type
 * @li PI_ERROR_FATAL = Fatal error / bad pointer
 * @li PI_ERROR_SIZE = Insufficient buffer length
 * @li PI_ERROR_INTERNAL = Library internal error
 * @li PI_ERROR_MISMATCH = Mismatch error
 * @li PI_ERROR_NOT_EXIST = Data not exist (e.g: Version not found)
 * @li PI_ERROR_CIB_ERROR = CIB missing/corrupted
 * @li PI_ERROR_MIB_ERROR = MIB missing/corrupted
 * @li PI_ERROR_PERMISSION = Permission error
 * @li PI_ERROR_NOT_SUPPORTED = Operation not supported
 *
 */
int platforminfo_get(unsigned long InfoType, void* buffer, unsigned long len, unsigned long* rsize);



#ifdef __cplusplus
}   /* extern "C" */
#endif



#endif /* __PLATFORMINFO_API_H__ */

/// @} */
/// @} */

/* Example how to use this library:

#include <platforminfo_api.h>

// Print Serial number
void print_serial_number()
{
    unsigned long rsize = 0;
    char serial_number[PI_SERIAL_NUMBER_SIZE_NULL_TERM];
    int result;
    result = platforminfo_get(PI_SERIAL_NUM ,serial_number, PI_SERIAL_NUMBER_SIZE_NULL_TERM, &rsize);
    if (result != PI_OK)
    {
        printf("ERROR, %s() failed at platforminfo_get(), return: %s \n", __func__, PI_ERR_TO_TXT(result));
        return;
    }

    printf("Serial number: %s \n", serial_number);
}

*/
