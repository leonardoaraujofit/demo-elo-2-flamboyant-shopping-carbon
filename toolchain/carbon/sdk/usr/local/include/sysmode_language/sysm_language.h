#ifndef __SYSMODE_LANGUAGE__
#define __SYSMODE_LANGUAGE__

namespace SYSM_LANG {

    typedef struct {
        unsigned int    id;
        char            name[50];
        char            dictionary[50];
        char            locale[50];
        char            full_locale[50];
        char            font[100];
        char            bold_font[100];
    } lang_struct;

    typedef struct {
        unsigned int    font_id;
        unsigned int    bold_font_id;
    } font_struct;

    extern bool LANG_AVAILABLE;
    extern const char * LANG_CONFIG_PATH;
    extern const char * LANG_LOCALE_PATH;
    extern const char * LANG_SAVED_PATH;
    extern const char * LANG_ENV;
    extern const char * LANGUAGE_ENV;
    extern const char * LOCALE_END;
    extern lang_struct user_lang;
    extern lang_struct default_lang;
    extern font_struct current_font;

    void init_lang();
    bool set_lang(int id);
    void save_fonts(unsigned int font_id, unsigned int bold_font_id);
    void get_current_fonts(unsigned int * f_id, unsigned int * d_f_id);
};

#endif //__SYSMODE_LANGUAGE__
