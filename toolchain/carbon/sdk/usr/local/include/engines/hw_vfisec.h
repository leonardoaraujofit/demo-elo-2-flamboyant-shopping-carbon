/******************************************************************************
 *
 *  Copyright 2000
 *  Broadcom Corporation
 *  16215 Alton Parkway
 *  PO Box 57013
 *  Irvine CA 92619-7013
 *
 *****************************************************************************/
/* 
 * Broadcom Corporation uBSec SDK 
 */
/*
 * Character device header file.
 */
/*
 * Revision History:
 *
 * October 2000 JTT Created.
 */

#define MAX_PUBLIC_KEY_BITS (4096)
#define MAX_PUBLIC_KEY_unsigned char S (MAX_PUBLIC_KEY_BITS/8)
#define SHA_BIT_SIZE  (160)
#define MAX_CRYPTO_KEY_LENGTH 24
#define MAX_MAC_KEY_LENGTH 64
#define t_VFISEC_KEY_DEVICE_NAME "VaultCrypto"

/* Math command types. */
#define t_VFISEC_MATH_MODADD 0x0001
#define t_VFISEC_MATH_MODSUB 0x0002
#define t_VFISEC_MATH_MODMUL 0x0004
#define t_VFISEC_MATH_MODEXP 0x0008
#define t_VFISEC_MATH_MODREM 0x0010
#define t_VFISEC_MATH_MODINV 0x0020

typedef long vfi210x_MathCommand_t;
typedef long vfi210x_RNGCommand_t;

typedef struct vfi210x_crypto_context_s {
	unsigned int	flags;
	unsigned char	crypto[MAX_CRYPTO_KEY_LENGTH];
	unsigned char 	auth[MAX_MAC_KEY_LENGTH];
} vfi210x_crypto_context_t, *vfi210x_crypto_context_p;

#include <vficrypt.h>

#ifdef __cplusplus
extern "C" {
#endif

/* 
 * Predeclare the function pointer types that we dynamically load from the DSO.
 */
#define SHA1INIT    (1<<0)   
#define SHA1TERM    (1<<1)
#define SHA1BUFF    (0)
#define SHA1ALL     (SHA1INIT | SHA1TERM)

typedef unsigned char vfi210x_des_vector_t[8];
typedef unsigned char vfi210x_des_key_single_t[8];
typedef struct {
	vfi210x_des_key_single_t key1;
	vfi210x_des_key_single_t key2;
	vfi210x_des_key_single_t key3;
} vfi210x_des_key_triple_t;

typedef unsigned char vfi210x_key_t[8];





#ifndef FALSE
  #define FALSE 0
#endif
#ifndef TRUE
  #define TRUE 0
#endif

/************************************/
/* D E S / A E S  F U N C T I O N   */
/************************************/
/************************************************************************************************************/
/* FUNCTION NAME:   vfiSec_SYM_CRYPT                                                                        */
/* DESCRIPTION:     Performs general DES/AES operation                                                      */
/* INPUT:           cryptHandle:    Cryptographic handler - Not in use yet                                  */
/*                  encrypt:        VFI_ENCRYPT / VFI_ENCRYPT                                               */
/*                  Mode:           Can be one of the following: VFI_MODE_ECB / VFI_MODE_CBC                */
/*                  keyData:        Pointer to the key                                                      */
/*                  ucKeySize:      Key size in bytes, can be VFI_DES_56/VFI_DES_112/VFI_DES_168            */
/*                                                            VFI_AES_128/VFI_AES_192/VFI_AES_256           */
/*                  auth_key:       Not in use yet                                                          */
/*                  iv:             Pointer to 8/16 bytes initial vector. Used for VFI_MODE_CBC             */
/*                  srcData:        Pointer to the plaintext buffer                                         */
/* OUTPUT:          dstData:        Pointer to the encrypted/decrypted buffer                               */
/* RETURN:          0:              The operation was performed successfully                                */
/*                  <0:             The operation failed                                                    */
/* NOTES:           None                                                                                    */
/************************************************************************************************************/
typedef int t_VFISEC_SYM_CRYPT( int         cryptHandle,    uint8_t     cryptType,
                                uint8_t     encrypt,        uint8_t     Mode,
                                uint8_t    *keyData,        uint8_t     ucKeySize,
                                uint8_t    *auth_key,       uint8_t    *iv, 
                                uint8_t    *srcData,        uint8_t    *dstData,   unsigned int size);


typedef int t_VFISEC_LoadKey (  int  cryptHandle,
                                KEYTAG *KeyTag ,  GENKEY * Key);

typedef void t_VFISEC_FreeGENKEY(GENKEY * Key);

/*****************************/
/* R S A   F U N C T I O N S */
/*****************************/

typedef int t_VFISEC_rsa_key_generate_mod_expo(int cryptHandle,
                                               unsigned int modulus_bit_length,
                                               unsigned long e_value,
                                               RSA_KEY *rsa);
/*---------------------------------------------------------------
 * Name    : t_VFISEC_rsa_mod_exp
 * Purpose : Perform an RSA mod exponentiation
 * Input   : pointers to the data parameters and crypto context
 			 x: the plain text message
 			 m: the modulus
 			 e: the exponent
 * Output  : y: = x**e mod m, y length is find_mod_size(m_bitlen)
 * Return  : Appropriate status
 * Remark  :
 			 If the callback parameter is NULL, the operation is synchronous
 			 The modulus will be round up according to a table
 			 The exponent will be round up to the nearest 32 bits
 			 The exponent and modulus parameter sizes may not be the same
 			 x_len must <= m_len (right?)
 *--------------------------------------------------------------*/
typedef int t_VFISEC_rsa_mod_exp (  int cryptHandle,
                                    uint8_t *x, uint32_t x_bitlen,
                                    uint8_t *m, uint32_t m_bitlen,
                                    uint8_t *e, uint32_t e_bitlen,
                                    uint8_t *y, uint32_t *y_uint8_tlen    );

/*---------------------------------------------------------------
 * Name    : t_VFISEC_rsa_mod_exp_crt
 * Purpose : Perfrom an RSA Chinese Remainder Theorem operation
 * Input   : pointers to the data parameters and crypto context
 			 x: the encrypted message
 			 p
 			 q
 			 edp, pinv: actual bit length must <= p bit length, and padded to p bit length
 			 edq: actual bit length must <= q bit length, and padded to q bit length
 * Output  : y, length is find_mod_size(q_bitlen*2)
 * Return  : Appropriate status
 * Remark  :
 			 Hardware uses pinv, so upper layer code will swap p and q, dp and dq, if they use qinv.
 *--------------------------------------------------------------*/
typedef int t_VFISEC_rsa_mod_exp_crt (
                    int cryptHandle,
                    uint8_t *x,    uint32_t x_bitlen,
                    uint8_t *edq,
                    uint8_t *q,    uint32_t q_bitlen,
                    uint8_t *edp,
                    uint8_t *p,    uint32_t p_bitlen,
                    uint8_t *pinv,
                    uint8_t *y,    uint32_t *y_uint8_tlen  );
typedef int t_VFISEC_RSA_public_encrypt (
                    int cryptHandle,
                    int flen,
                    const uint8_t *from,
                    uint8_t *to,
                    RSA_KEY *rsa,
                    int padding) ;

typedef int t_VFISEC_RSA_public_decrypt (
                    int cryptHandle,
                    int flen,
                    const uint8_t *from,
                    uint8_t *to,
                    RSA_KEY *rsa,
                    int padding) ;
typedef int t_VFISEC_RSA_private_encrypt(
                    int cryptHandle,
                    int flen,
                    const uint8_t *from,
                    uint8_t *to,
                    RSA_KEY *rsa,
                    int padding) ;
typedef int t_VFISEC_RSA_private_decrypt(
                    int cryptHandle,
                    int flen,
                    const uint8_t *from,
                    uint8_t *to,
                    RSA_KEY *rsa,
                    int padding) ;

typedef int t_VFISEC_RSA_sign(
                    int cryptHandle,
                    int type,
                    const unsigned char *m,
                    unsigned int m_length,
                    unsigned char *sigret,
                    unsigned int *siglen,
                    const RSA_KEY *rsa);
typedef int t_VFISEC_RSA_verify(
                    int cryptHandle,
                    int dtype,
                    const unsigned char *m,
                    unsigned int m_length,
                    const unsigned char *sigbuf,
                    unsigned int siglen,
                    const RSA_KEY *rsa);

/*****************************/
/* D S A   F U N C T I O N S */
/*****************************/
/*-------------------------------------------------------------------------------
 * Name    : t_VFISEC_dsa_sign
 * Purpose : Perform a DSA signature
 * Input   : pointers to the data parameters
 *			 hash: SHA1 hash of the message, length is 20 uint8_ts
 * 			 p: 512 bit to 1024 bit prime, length must be exact in 64 bit increments
 * 			 q: 160 bit prime
 * 			 g: = h**(p-1)/q mod p, where h is a generator. g length is same as p length
 * 			 random: random number, length is 20 uint8_ts, 
                                 generated internally if specified NULL
 * 			 x: private key, length is 20 uint8_ts
 * Output  : rs: the signature, first part is r, followed by s, each is q length
 * Return  : Appropriate status
 * Remark  : If the callback parameter is NULL, the operation is synchronous
 *------------------------------------------------------------------------------*/
typedef int t_VFISEC_dsa_sign (
				  int cryptHandle,
                  uint8_t *hash,
                  uint8_t *random,
                  uint8_t *p,      uint32_t p_bitlen,
                  uint8_t *q,
                  uint8_t *g,
                  uint8_t *x,
                  uint8_t *s,     uint32_t *s_uint8_tlen,
                  uint8_t *r,     uint32_t *r_uint8_tlen);

/*---------------------------------------------------------------
 * Name    : t_VFISEC_dsa_verify
 * Purpose : Verify a DSA signature (private key: x, public key: p, q, g, y.
 * Input   : pointers to the data parameters
 *			 p: 512 bit to 1024 bit prime, in 64 bits increment
 *			 q: 160 bit prime
 *			 g: = h**(p-1)/q mod p, where h is a generator, g length is same as p length
 *           y: = g**x mod p, length is same as p length
 *			 r,s: signature
 *           For parameters q, r, s, they don't have a length field, since it's fixed 20 uint8_ts long.
 * Return  : v. If v == r then verify success
 * Remark  : If the callback parameter is NULL, the operation is synchronous
 *--------------------------------------------------------------*/
typedef int t_VFISEC_dsa_verify (
                    int cryptHandle,
                    uint8_t *hash,
                    uint8_t *p,    uint32_t p_bitlen,
                    uint8_t *q,
                    uint8_t *g,
                    uint8_t *y,
                    uint8_t *r,    uint8_t  *s,
                    uint8_t *v,    uint32_t *v_uint8_tlen  );

/***********************************************/
/* D E F F I   H E L M A N   F U N C T I O N S */
/***********************************************/
/*---------------------------------------------------------------
 * Name    : t_VFISEC_diffie_hellman_generate
 * Purpose : Generate a Diffie-Hellman key pair
 * Input   : pointers to the data parameters
 * 			 x: the secret value, has actual bit length (IN/OUT)
 *			 xvalid: 1 - manual input of private key pointed by x, 
 *			         0 - generate private key internally.
 *			 g: generator, has actual bit length
 *			 m: modulus, has actual bit length
 * Output  : y: the public value. y = g**x mod m, y length is find_mod_size(m_bitlen)
 * Return  : Appropriate status
 * Remark  : If the callback parameter is NULL, the operation is synchronous
 			 g and x are rounded up to 32 bits
 *--------------------------------------------------------------*/
typedef int t_VFISEC_diffie_hellman_generate (
                    int cryptHandle,
                    uint8_t *x,      uint32_t x_bitlen,
                    uint32_t xvalid,
                    uint8_t *y,      uint32_t *y_uint8_tlen,
                    uint8_t *g,      uint32_t g_bitlen,
                    uint8_t *m,      uint32_t m_bitlen      );

/*---------------------------------------------------------------------------
 * Name    : t_VFISEC_diffie_hellman_shared_secret
 * Purpose : Generate a Diffie-Hellman shared secret
 * Input   : pointers to the data parameters
 			 x: the secret value, has actual bit length
 			 y: the other end public value, has actual bit length
 			 m: modulus, has actual bit length
 * Output  : k: the shared secret. k = y**x mod m, k length is find_mod_size(m_bitlen)
 * Return  : Appropriate status
 * Remark  : If the callback parameter is NULL, the operation is synchronous
 *--------------------------------------------------------------------------*/
typedef int t_VFISEC_diffie_hellman_shared_secret (
 					 int cryptHandle,
                     uint8_t *x, uint32_t x_bitlen,
                     uint8_t *y, uint32_t y_bitlen,
                     uint8_t *m, uint32_t m_bitlen,
                     uint8_t *k, uint32_t *k_uint8_tlen    );

/*****************************/
/* S H A   F U N C T I O N S */
/*****************************/
typedef int t_VFISEC_SHA1_Init(int cryptHandle, SHAcontext *c);
typedef int t_VFISEC_SHA1_Update(int cryptHandle, SHAcontext *c, const void *data, unsigned int len);
typedef int t_VFISEC_SHA1_Final(int cryptHandle, unsigned char *md, SHAcontext *c);
typedef unsigned char *t_VFISEC_SHA1(int cryptHandle, const unsigned char *d, unsigned int n, unsigned char *md);

typedef int t_VFISEC_SHA256_Init(int cryptHandle, SHAcontext *c);
typedef int t_VFISEC_SHA256_Update(int cryptHandle, SHAcontext *c, const void *data, unsigned int len);
typedef int t_VFISEC_SHA256_Final(int cryptHandle, unsigned char *md, SHAcontext *c);
typedef unsigned char *t_VFISEC_SHA256(int cryptHandle, const unsigned char *d, unsigned int n,unsigned char *md);

typedef int t_VFISEC_SHA_File(int cryptHandle, const char *pFileName, unsigned char *pDigest,unsigned int is256);

/***************************************************************/
/* B A C K W A R D S   C O M P A T I B L E   F U N C T I O N S */
/***************************************************************/
/******************************************************************************
* Function Name:    t_VFISEC_GetRnd
* Description:      Get a random number pull
* Parameters:       RandomBuf - pointer to output random buffer
*                   length    - length of buffer
* Return Value:     Number of random uint8_ts created
* NOTES:            None
******************************************************************************/
typedef int t_VFISEC_GetRnd (int cryptHandle, unsigned int *pRandomBuf, unsigned int length);

typedef int t_VFISEC_GetVaultVersion(unsigned char * pVersion);

typedef int t_VFISEC_SHA1_Old(int action, unsigned char *buffer, unsigned long nb, unsigned char * sha20);

typedef int t_VFISEC_SHA256_Old(int action, unsigned char *buffer, unsigned long nb, unsigned char * sha32);

typedef int t_VFISEC_pkaRSA(unsigned char *msg, unsigned char *mod, unsigned int mlen, unsigned char *exp, unsigned int elen, unsigned char *result);

/***********************************************/
/* S P E C I F I C   F U N C T I O N A L I T Y */
/***********************************************/
typedef int t_VFISEC_Open(int mode);
typedef int t_VFISEC_Close(int handle);

#ifdef __cplusplus
}
#endif
