
#ifndef _E_PKEY_H__
#define _E_PKEY_H__

#include <openssl/engine.h>

#ifdef __cplusplus
extern "C" {
#endif

int vfi210x_register_ec_pmeth(ENGINE *e);

#ifdef __cplusplus
}
#endif

#endif
