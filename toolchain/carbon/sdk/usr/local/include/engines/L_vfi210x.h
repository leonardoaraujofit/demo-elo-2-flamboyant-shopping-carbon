/*
 *FILE NAME:   L_vfi210x.h
 *
 * MODULE NAME: @file L_vfi210x.h
 *
 * DESCRIPTION: vault engine library help functions
 *
 * Copied/modified by Ishaiahu Paz(ishai_p1@verifone.com) from e_vfi210x.c,
 * written initially for Broadcom Corporation uBSec SDK project 2000 for the
 * vault engine project 2010.
 *
 *
 *REVISION:   1.0  October 28 2010  -  Ishaiahu Paz
 *HISTORY:
 *
 */

#ifndef L_vfi210xH
#define L_vfi210xH

#ifdef __cplusplus
extern "C" {
#endif


           /*=========================================*
            *         D E F I N I T I O N S           *
            *=========================================*/
//typedef struct VE_key_st  VE_KEY;



typedef struct VE_key_st {
  char *label;
  u8 isPrivate;  /**< private key present? */
  EVP_PKEY *evp_key;    /**< initially NULL, need to call PKCS11_load_key */
  //void *_private;
  VS_OBJECT_HANDLE object;
  u8 id[32];
  size_t id_len;
  //VE_KEY_ops *ops;
  int type;               /* EVP_PKEY_xxx */
  int (*get_public) (struct VE_key_st *, EVP_PKEY *);
  int (*get_private) (struct VE_key_st *, EVP_PKEY *);

} VE_KEY;



typedef struct ve_ctx
{
  //ctx
  VS_FUNCTION_LIST_PTR method;

  VS_SESSION_HANDLE session;
  char *init_args;
  //slot
  u8 haveSession;
  //token
  u8 initialized;
  int nkeys, nprkeys;
  VE_KEY *key;

} VE_CTX;







           /*=========================================*
            *   F U N C T I O N  P R O T O T Y P E S  *
            *=========================================*/
/*
 * Functions to operate with string constants for the DSO file name and the
 * function symbol names to bind to.
 */

const char *get_VFISEC_LIBNAME(void);
void free_VFISEC_LIBNAME(void);
long set_VFISEC_LIBNAME(const char *name);

/*Get DSO*/
//DSO *get_vfi210x_dso(void)


/* --------------------------------------------------------------------------
 * FUNCTION NAME: L_LoadModule
 * DESCRIPTION:   loads VFICRYPT dll and receives function list from it
 * ARGS:          char *modulename               (in) - module name to load
                  VS_FUNCTION_LIST_PTR_PTR funcs (out)- function list from dll
 * RETURN:        1- ok
                  0 - err
 * NOTES:
 * ------------------------------------------------------------------------ */
int  L_LoadModule(VS_FUNCTION_LIST_PTR_PTR funcs);

/* --------------------------------------------------------------------------
 * FUNCTION NAME: L_UnloadModule
 * DESCRIPTION:   unloads VFICRYPT dll
 * ARGS:
 * RETURN:        1- ok
                  0 - err
 * NOTES:
 * ------------------------------------------------------------------------ */
int L_UnloadModule(void);


/* --------------------------------------------------------------------------
 * FUNCTION NAME: L_CTX_Unload
 * DESCRIPTION:   unloads VFICRYPT driver and vficrypt.dll
 * ARGS:
 * RETURN:        1- ok
                  0 - err
 * NOTES:
 * ------------------------------------------------------------------------ */
void L_CTX_Unload(VE_CTX *ctx);



#ifdef __cplusplus
}
#endif

#endif /*L_vfi210xH*/


