#ifndef _LIBEPP_H_
#define _LIBEPP_H_

#ifdef __cplusplus
extern "C" {
#endif

// NS mode
#define PP_NS_DEFAULT           0
// Encryption/Decryption modes
#define PP_MODE_2DES_ECB        0
#define PP_MODE_2DES_CBC        1

// Channel selection
#define PP_CHANNEL_0            (0<<28)
#define PP_CHANNEL_1            (1<<28)
#define PP_CHANNEL_2            (2<<28)
#define PP_CHANNEL_3            (3<<28)

#define PP_SLAVE_ID         (1<<30)
#define PP_GETSK_ID         (1<<27)
#define PP_ROOT_PK_ID       (1<<26)
#define PP_PWD_WK_ID        (1<<25)

#define PP_RSA_KEY_SIZE_MAX   256
#define PP_RSA_KEY_SIZE_MIN    64
#define PP_CRYPTOGRAM3_SIZE    16
#define PP_CHALLENGE_SIZE       8
#define PP_SYMM_KEY_SIZE       16
#define PP_SYMM_BLOCK_SIZE      8
#define PP_DLP_BLOCK_SIZE      16
#define PP_KCV_SIZE             4
#define PP_MAC_SIZE             8

/**
 * @name RSA Pairing Key Selector
*/
/** \{ */
#define PP_USE_MAGIC_KEY        (0<<24)
#define PP_USE_VSHLD_KEY        (1<<24)
/** \} */

/* Secure peripheral support (MagIC3 P-Series & UX100/110 PINpads, UX300 SCR/MSR */
int pp_generateRSAKeyPair(int size, int expValue, int forceRenew);
int pp_readPublicKey(int maxModLen, unsigned char *modValue, int *modLen, int *expValue);

/** Read public key from vault
 * @param[in]  mode  - bit 24 = RSA key to read: 0 = MagIC pairing key, 1 = VeriShield pairing key
 * @param[in]  maxModLen = Maximum modulus length (size of modValue buffer)
 * @param[out] modValue = Modulus value � MSB first
 * @param[out] modLen = Modulus Length
 * @param[out] expValue = Public exponent value
 * @return 0 = Success, 1 = Bad input parameter, 2 = key not present, 3 = other error
 */
int pp_readPublicKey_Ext(int mode, int maxModLen, unsigned char *modValue, int *modLen, int *expValue);
int pp_initiateNS(int mode, int modLen, unsigned char *modValue, int expValue, unsigned char *crypto1); /* Used on the master */
int pp_resolveNS (int mode, int modLen, unsigned char *modValue, int expValue,
                  int cryptoLen, unsigned char *crypto1, unsigned char *crypto2); /* Used on the slave */
int pp_finalizeNS(int mode, int cryptoLen, unsigned char *crypto2, unsigned char *crypto3); /* Used on the master */
int pp_verifyNS(int mode, unsigned char *crypto3); /* Used on the slave */
int pp_generateChallenge(int mode, unsigned char *challenge, unsigned char *workingKey);
int pp_resolveChallenge(int mode, unsigned char *challenge, unsigned char *workingKey, unsigned char *result);
int pp_verifyChallenge(int mode, unsigned char *challenge);
int pp_encryptData(int mode, unsigned char *inData, int inDataLen, unsigned char *outData, int *outDataLen);
int pp_decryptData(int mode, unsigned char *inData, int inDataLen, unsigned char *outData);
int pp_initCipheredlink(int mode, unsigned char *outData);
int pp_setupCipheredlink(int mode, unsigned char *inData);
int pp_cipherData(int mode, int counter, unsigned char *inData, int inDataLen, unsigned char *outData);
int pp_macData(int mode, int counter, unsigned char *inData, int inDataLen, unsigned char *outMAC);
int pp_getPinKey(int mode,  unsigned char *workingKey);
int pp_encryptPin(int mode, unsigned char *workingKey, unsigned char *pin);
int pp_loadPin(int mode, int pinLen, unsigned char *pin);
int pp_isPinInjected(int *injected);
int pp_getPWsessionKey(int mode, unsigned char *workingKey);
int pp_loadPWblock(int mode, int pwdLen, unsigned char *pwd);
int pp_createARSVchallenge(int mode, unsigned char *challenge);
int pp_resolveARSVchallenge(int mode, unsigned char *challenge, unsigned char *outputdata);
int pp_returnARSVchallenge(int mode, unsigned char *challenge, unsigned char *outputdata);
int pp_verifyARSVchallenge(int mode, unsigned char *challenge);

#ifdef __cplusplus
}
#endif

#endif /* _LIBEPP_H_ */
