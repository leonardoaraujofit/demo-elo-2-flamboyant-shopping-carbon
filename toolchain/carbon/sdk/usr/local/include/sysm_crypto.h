#include <sysmode_ui/sysm_lang.h>

#ifndef __CRYPTOFLEX_H__
#define __CRYPTOFLEX_H__

/* definition of the file ID on the smart card*/

#define MASTER_FILE_ID      0x3f00
#define RESET_A_ID          0x3f22
#define RESET_B_ID          0x3f30
#define UNLOCK_ID           0x3f33
#define AUTH_INJ_ID         0x3f31
#define AUTH_PP_A_ID        0x3f23
#define AUTH_PP_B_ID        0x3f34

#define RSA_PUB_ID          0x1012
#define RSA_PRIV_ID         0x0012
#define COUNTER_ID          0x4000

int crypt_readPublicKey(unsigned char* f_pubKey, int * f_sc_status, unsigned short* f_sw1sw2);
int crypt_decryptRSA(unsigned char *data, int lengthIn, int * f_sc_status, unsigned short* f_sw1sw2 );
int crypt_readCertificate(unsigned char* f_pbCertData, int* f_pwDataLength,
                             int * f_sc_status, unsigned short* f_sw1sw2 );
int crypt_selectFileID( int f_wFileID, int * f_sc_status, unsigned short* f_sw1sw2);
int crypt_verifyPin( char* pin1, char* pin2, int* f_sc_status, unsigned short* f_sw1sw2);
int crypt_decreaseCounter(unsigned int dec, unsigned int *pCounter, int * f_sc_status, unsigned short* f_sw1sw2);
int crypt_readCounter(unsigned int *pCounter, int * f_sc_status, unsigned short* f_sw1sw2 );

#endif
