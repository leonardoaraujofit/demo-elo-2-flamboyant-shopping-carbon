/** @addtogroup utils Utils */
/** @{ */
/**
 *  @file svcmgrSvcInfConsumer.h
 *
 *  @brief Service interface public property API
 *
 *  Public property API.
 *
 */

/*
*  Copyright, 2015 VeriFone Inc.
*  2099 Gateway Place, Suite 600
*  San Jose, CA.  95110
*
* All Rights Reserved. No part of this software may be reproduced,
* transmitted, transcribed, stored in a retrieval system, or
* translated into any language or computer language, in any form
* or by any means electronic, mechanical, magnetic, optical,
* chemical, manual or otherwise, without the prior permission of
* VeriFone Inc.
*
*/
#ifndef _SVCMGR_SVCINF_CONSUMER_H_
#define _SVCMGR_SVCINF_CONSUMER_H_

#ifdef __cplusplus
extern "C" {
#endif


/**
* @name svc_rtn <callcode> values
*/
/** \{ */
#define		SVC_CALLCODE_FATAL			(0)		/*!< error occurred before meaningful data extracted from svc_cmd, svc_rtn built with service = svcmgr and method = error. */
#define		SVC_CALLCODE_SVCMGR			(1)		/*!< Internal error prevented \<svc_rtn\> xml to be built.This is severe and suggests memory or DOM errors. */
#define		SVC_CALLCODE_SERVICE		(2)		/*!< svc_cmd service does not exist or could not be loaded. */
#define		SVC_CALLCODE_METHOD			(3)		/*!< svc_cmd method does not exist. */
#define		SVC_CALLCODE_CMDPARSE		(4)		/*!< svc_cmd parse error. */
#define		SVC_CALLCODE_MISSINGREQ		(5)		/*!< svc_cmd is missing a required parameter. */
#define		SVC_CALLCODE_WDEINF			(6)		/*!< XML interface unavailable (interface fn {service}_xml_cmd() doesn't exist) */
#define		SVC_CALLCODE_RTNBUILD		(7)		/*!< svc_rtn could not be built due to internal error */
#define		SVC_CALLCODE_BLOCKED		(8)		/*!< the cmd not processed due to svcmgr security policy settings. */
#define		SVC_CALLCODE_CFGPATHNAME	(9)		/*!< Interface lib built without defining SVC_CONFIG_PATHNAME, internal error, service = svcmgr and method = error.*/
#define		SVC_CALLCODE_BINCONV		(10)	/*!< Binary type conversion failed (void*), is binary string in base64 format? */
#define		SVC_CALLCODE_ACCESS			(11)	/*!< access to service method not allowed */
/** \} */

/**
* @brief Callback prototype for use with smi_GetPropertyNames()
*
* @note
* 'name' is owned by the library, do not free.
*/
typedef void (*cbPropName)(char* name, void* data);


/**
* Returns each property name sequentially to callback.
*
* @param[in] callback Callback property name.
* @param[in] data Allows user defined data to be passed into callback, set to NULL if unused.
*/
void smi_GetPropertyNames(cbPropName callback, void* data);


/**
* Gets property value.
*
* @param[in] name The string, property key id.
*
* @return string pointer to property value, or NULL if property 'name' does not exit.
*
* @note
* The returned char * pointer is owned by the library, do not free.
*/
char* smi_GetPropertyValue(const char* name);


#ifdef __cplusplus
}
#endif

#endif // _SVCMGR_SVCINF_CONSUMER_H_
///@}
