/**
 * @copyright
 * COPYRIGHT (C) 2012 by VeriFone, Inc.  All rights reserved.
 * @file                    remoteprinter.h
 * Module Description:      Remote printer Library
 * Component of:            Printer
 * @author                  Stephen_M5
 * Function:                User API library that interfaces to application.  Performs printer protocol.
 * Revision 1.0            2012/Oct/1   File created
 * Modification History: (use if not filled in by source control)
 *
 * 2013.09.06  T_KonstantinK1	Fix CR1972: new function rmpTurnOnThreaded()
 */

 /***************************************************************************
  *
  *	#include files required to compile this source file here
  *
  ***************************************************************************/
#ifndef _REMOTE_PRINTER_H_
#define _REMOTE_PRINTER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <errno.h>
#include <stdio.h>
#include <stdbool.h>

/**
 * @name Error return codes
 */
/** \{ */
#define E_PRNT_COM_NOT_AVAIL    -101
#define E_PRNT_NOT_SUPPORT      -102
#define E_PRNT_COM_IN_USE       -103
#define E_PRNT_PARAM_INVAL      -104
#define E_PRNT_NOT_CONNECTED    -105
#define E_PRNT_NO_PAPER         -106
#define E_PRNT_POWERRED_OFF     -107
#define E_PRNT_PAPER_JAM        -108
#define E_PRNT_COVER_UP         -109
#define E_PRNT_PAPER_JAM_COVER_UP         -110
/** \} */

#define RMPRINT_LIB_VERSION_SIZE 10

#define MAX_BIT_IMAGE_SIZE_PER_LINE  511

typedef enum{
    RMPRNT_PARTIAL_CUT=0,
    RMPRNT_FULL_CUT
}PMP_CUT_TYPE;

typedef enum{
	RMP_MODEL_NONE=0,
	RMP_MODEL_M1470,
	RMP_MODEL_TG2460,
	RMP_MODEL_TG2460H
}PRINTER_TYPE;

typedef enum
{
	BIT_IMAGE_8DOTS_SINGLE_DENSITY=0,
	BIT_IMAGE_8DOTS_DOUBLE_DENSITY,
	BIT_IMAGE_24DOTS_SINGLE_DENSITY=32,
	BIT_IMAGE_24DOTS_DOUBLE_DENSITY

}RMPRNT_BIT_IMAGE_MODE;

typedef enum
{
	RMPRNT_DISABLE_PRINTER=0,
	RMPRNT_ENABLE_PRINTER,
}RMPRNT_ON_OFF;

typedef enum
{
	RMPRNT_DENSITY_LIGHTER=0,
	RMPRNT_DENSITY_LIGHT,
	RMPRNT_DENSITY_NORMAL,
	RMPRNT_DENSITY_DARK,
	RMPRNT_DENSITY_DARKER,
}RMPRNT_DENSITY_LEVEL;

typedef enum
{
	ID_RMPRNT_PRINTER_STAT=1,
	ID_RMPRNT_OFFLINE_STAT,
	ID_RMPRNT_ERROR_STAT,
	ID_RMPRNT_SENSOR_STAT,
	ID_RMPRNT_PAPER_STAT=17,
	ID_RMPRNT_FULL_STAT=20,
	ID_RMPRNT_PRNTER_ID=21,
	ID_RMPRNT_POWER_STAT=22 /**< return power status: ON/OFF */
}RMPRNT_STAT_ID;

typedef enum
{
	ID_BARCODE_SYSTEM_UPC_A=0,
	ID_BARCODE_SYSTEM_UPC_E,
	ID_BARCODE_SYSTEM_EAN13,
	ID_BARCODE_SYSTEM_EAN8,
	ID_BARCODE_SYSTEM_CODE39,
	ID_BARCODE_SYSTEM_ITF,
	ID_BARCODE_SYSTEM_CODABAR,
	ID_BARCODE_SYSTEM_CODE93,
	ID_BARCODE_SYSTEM_CODE128,
	ID_BARCODE_SYSTEM_CODE32=20,

	/**
     * @name Extension barcode systems
     */
    /** \{ */
	ID_BARCODE_SYSTEM_UPC_A_EX=65,
	ID_BARCODE_SYSTEM_UPC_E_EX,
	ID_BARCODE_SYSTEM_EAN13_EX,
	ID_BARCODE_SYSTEM_EAN8_EX,
	ID_BARCODE_SYSTEM_CODE39_EX,
	ID_BARCODE_SYSTEM_ITF_EX,
	ID_BARCODE_SYSTEM_CODABAR_EX,
	ID_BARCODE_SYSTEM_CODE93_EX,
	ID_BARCODE_SYSTEM_CODE128_EX,
	ID_BARCODE_SYSTEM_CODE32_EX=90,
    /** \} */
}BARCODE_SYSTEM_TYPE_ID;

/**
 * Specific printer dependent commands.
 *
 * @li TGP_     - Generic TG Printer(TGP) commands
 * @li TG2460_  - TG2460 Specific commands
 * @li TG2460H_ - TG2460H Specific Commands
 * @li MCSP_    - MCS Printer dependent commands
 */
typedef enum
{
	RMPRINT_RAW_CMD=0,          /**< Raw commands, With this command ID, application will pass through total commands including command, parameters, data */

	// The command IDs below are for specific printer dependent commands, the application will only need to passthrough variable command parameter(s) if necessary.

	//TG2460 Printer dependent commands:    Command Description:                                                    Function rmpCommand pDataIn:                                       Function rmpCommand pDataOut:

	//Generic TG Printer(TGP) commands
    /** \{ */
    TGP_HORIZONTAL_TAB,			/**< Horizontal tab \n                                                          Function rmpCommand pDataIn: NULL \n                                 Function rmpCommand pDataOut: NULL */
    TGP_LINE_FEED, 				/**< Print and line feed \n                                                     Function rmpCommand pDataIn: NULL \n                                 Function rmpCommand pDataOut: NULL */
    TGP_CAR_RETURN,				/**< Print and carriage return \n                                               Function rmpCommand pDataIn: NULL \n                                 Function rmpCommand pDataOut: NULL */
	TGP_SET_RIGHT_SPACE,  	    /**< Set right-side char spacing \n                                             Function rmpCommand pDataIn: 1 Byte: n \n                            Function rmpCommand pDataOut: NULL */
    TGP_SET_PRINT_MODE, 		/**< Set print mode \n                                                          Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: NULL */
	TGP_SET_PRINT_POS,          /**< Set absolute print position \n                                             Function rmpCommand pDataIn: 2 Bytes: nL nH \n                       Function rmpCommand pDataOut: NULL */
	TGP_UNDER_LINE_MODE,     	/**< Turn underline mode on/off \n                                              Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: NULL */
	TGP_INCH8_SPACE,			/**< Select 1/8-inch line spacing \n                                            Function rmpCommand pDataIn: NULL,                                   Function rmpCommand pDataOut: NULL */
	TGP_INCH6_SPACE,			/**< Select 1/6-inch line spacing \n                                            Function rmpCommand pDataIn: NULL \n                                 Function rmpCommand pDataOut: NULL */
	TGP_MIN_LINE_SPACE,     	/**< Set line spacing using min units \n                                        Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: NULL */
	TGP_SET_SCRIPT_MODE,		/**< Set/reset script mode \n                                                   Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: NULL */
	TGP_INIT_PRINTER,			/**< Initialize printer \n                                                      Function rmpCommand pDataIn: NULL \n                                 Function rmpCommand pDataOut: NULL */
	TGP_SET_HOR_TAB_POS,        /**< Set horizontal tab position (needs additional data) \n                     Function rmpCommand pDataIn: n Bytes: n1....nk NUL \n                Function rmpCommand pDataOut: NULL */
	TGP_SET_BOLD_MODE,   		/**< Turn bold mode on/off \n                                                   Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: NULL */
	TGP_SET_STRIKE_MODE,		/**< Turn double-strike mode on/off \n                                          Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: NULL */
	TGP_PRINT_AND_FEED,  		/**< Print and paper feed \n                                                    Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: NULL */
	TGP_SET_INTERN_CHAR,		/**< Select international character set \n                                      Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: NULL */
	TGP_SET_ROTATE_MODE,		/**< Set print mode rotated by 90 degree \n                                     Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: NULL */
	TGP_SET_RELATIVE_POS,   	/**< Set relative print position \n                                             Function rmpCommand pDataIn: 2 Byte:  nL nH \n                       Function rmpCommand pDataOut: NULL */
	TGP_SEL_JUSTIFICATION,  	/**< Select justification \n                                                    Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: NULL */
	TGP_PANEL_KEYS, 			/**< Enable/disable panel keys \n                                               Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: NULL */
	TGP_PRINT_FEED_LINES,   	/**< Print and feed paper n lines \n                                            Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: NULL */
	TGP_SEL_CODE_TABLE, 		/**< Select character code table, \n                                            Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: NULL */
	TGP_GET_PAPER_STATUS,		/**< Transmit paper status, \n                                                  Function rmpCommand pDataIn: NULL \n                                 Function rmpCommand pDataOut: 1 Byte: Paper status: See printer spec */
	TGP_SET_SPEED_MODE, 		/**< Select speed/quality mode \n                                               Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: NULL */
	TGP_SET_UP_SIDE_DOWN,   	/**< Set/cancel upside-down printing \n                                         Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: NULL */
	TGP_CUT_MOVE_BACK,			/**< Total cut and paper moving back \n                                         Function rmpCommand pDataIn: NULL \n                                 Function rmpCommand pDataOut: NULL */
	TGP_SET_WHITE_BLACK,    	/**< Turn white/black reverse printing on/off \n                                Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: NULL */
	TGP_SET_HRI_POS,        	/**< Select printing position of HRI \n                                         Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: NULL */
	TGP_GET_MODE_ID,			/**< Transmit printer mode ID \n                                                Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: 1 Byte: Printer Mode ID */
	TGP_GET_TYPE_ID,			/**< Transmit printer type ID \n                                                Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: 1 Byte: Printer Type ID */
	TGP_GET_VERSION_ID,			/**< Transmit printer version ID \n                                             Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: 4 Bytes: Version */
	TGP_SET_LEFT_MARGIN,    	/**< Set left margin \n                                                         Function rmpCommand pDataIn: 2 Bytes: nL nH \n                       Function rmpCommand pDataOut: NULL */
	TGP_HORI_VERT_MOTION,   	/**< Set horinzontal and vertical motion units \n                               Function rmpCommand pDataIn: 2 Bytes: x y \n                         Function rmpCommand pDataOut: NULL */
	TGP_PRINT_AREA_WIDTH,   	/**< Set printing area width \n                                                 Function rmpCommand pDataIn: 2 Bytes: nL nH \n                       Function rmpCommand pDataOut: NULL */
	TGP_PRINT_COUNTER, 			/**< Print counter \n                                                           Function rmpCommand pDataIn: NULL \n                                 Function rmpCommand pDataOut: NULL */
	TGP_SEL_HRI_FONT,   		/**< Select font for HRI characters \n                                          Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: NULL */
	TGP_BARCODE_HEIGHT, 		/**< Set bar code height \n                                                     Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: NULL */
	TGP_GET_TRANSMIT_STATUS,	/**< Transmit status \n                                                         Function rmpCommand pDataIn: NULL \n                                 Function rmpCommand pDataOut: 1 Byte: Paper sensor status */
	TGP_SET_BARCODE_WIDTH,   	/**< Set bar code width \n                                                      Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: NULL */
    /** \} */

	//TG2460 Specific commands:
	TG2460_MOVE_BACK, 			/**< Moving back one character \n                                               Function rmpCommand pDataIn: NULL \n                                 Function rmpCommand pDataOut: NULL */
	TG2460_MACRO_DEFINITION, 	/**< Start/end of macro definition \n                                           Function rmpCommand pDataIn: NULL \n                                 Function rmpCommand pDataOut: NULL */
	TG2460_EXECUTE_MACRO,      	/**< Execute macro \n                                                           Function rmpCommand pDataIn: 3 Bytes: r t m \n                       Function rmpCommand pDataOut: NULL */
	TG2460_SEL_COUNTER_MODE,   	/**< Select counter print mode \n                                               Function rmpCommand pDataIn: 2 Bytes: n m \n                         Function rmpCommand pDataOut: NULL */
	TG2460_SEL_COUNT_MODEA,    	/**< Select count mode-A (needs additional data) \n                             Function rmpCommand pDataIn: 6 Bytes: aL aH bL bH n r \n             Function rmpCommand pDataOut: NULL */
	TG2460_SET_COUNTER,    		/**< Set counter \n                                                             Function rmpCommand pDataIn: 2 Bytes: nL nH \n                       Function rmpCommand pDataOut: NULL */
	TG2460_SEL_COUNT_MODEB,    	/**< Select count mode-B (needs additional data) \n                             Function rmpCommand pDataIn: 10 Bytes: sa ; sb ; sn ; sr ; sc ; \n   Function rmpCommand pDataOut: NULL */
	TG2460_SET_SCRIPT,			/**< Set superscript-subscript \n                                               Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: NULL */

	//TG2460H Specific Commands:
	TG2460H_SET_VERT_POS,       /**< Set absolute vertical print position in page mode \n                       Function rmpCommand pDataIn: 2 Bytes: nL nH \n                       Function rmpCommand pDataOut: NULL */
	TG2460H_POWER_LED_BAR,      /**< Power on/off LED bar \n                                                    Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: NULL */
    TG2460H_SET_CPI_MODE,       /**< Set/Cancel CPI mode \n                                                     Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: NULL */
    TG2460H_SET_NOTCH_DISTANCE, /**< Set notch distance \n                                                      Function rmpCommand pDataIn: 2 Bytes: nH nL \n                       Function rmpCommand pDataOut: NULL */
    TG2460H_ALIGN_HEAD,         /**< Align the print head with the notch \n                                     Function rmpCommand pDataIn: NULL \n                                 Function rmpCommand pDataOut: NULL */
    TG2460H_ALIGN_AUTO_CUTTER,  /**< Align the autocutter with the notch \n                                     Function rmpCommand pDataIn: NULL \n                                 Function rmpCommand pDataOut: NULL */

	//MCS Printer dependent commands:
    MCSP_DELETE_FONTS,          /**< Delete fonts(instantly) \n                                                 Function rmpCommand pDataIn: NULL \n                                 Function rmpCommand pDataOut: NULL */
    MCSP_GET_FONT_INFO,         /**< Query font info \n                                                         Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: 8 Bytes: Font info: see MCS printer spec */
    MCSP_GET_LOGO_CRC,          /**< Query Logo CRC, logoIndex range[01-15] \n                                  Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: 2 Bytes: Logo CRC */
    MCSP_SET_FONT,              /**< Change font: fontIndex range[0-4]. \n                                      Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: NULL */
    MCSP_SET_BIG_LETTERS,       /**< Set Big Letters \n                                                         Function rmpCommand pDataIn: NULL \n                                 Function rmpCommand pDataOut: NULL */
                                // Clear flash bank:
    MCSP_GET_PRINTER_ID,        /**< Get Printer ID \n                                                          Function rmpCommand pDataIn: NULL \n                                 Function rmpCommand pDataOut: 2 Bytes: Printer ID */
    MCSP_SET_SMALL_LETTERS,     /**< Change small letters. \n                                                   Function rmpCommand pDataIn: NULL \n                                 Function rmpCommand pDataOut: NULL */
    MCSP_FORWARD_NEXT_MARK,     /**< Go to the next printer mark, The printer forwards to next printer mark. \n Function rmpCommand pDataIn: NULL \n                                 Function rmpCommand pDataOut:NULL */
    MCSP_SET_NORMAL_LETTERS,    /**< Set normal letters(same as \H3). \n                                        Function rmpCommand pDataIn: NULL \n                                 Function rmpCommand pDataOut: NULL */
    MCSP_RECEIPT,            	/**< Attendance test: \n                                                        Function rmpCommand pDataIn: NULL \n                                 Function rmpCommand pDataOut: NULL */
    MCSP_READ_SERIAL,       	/**< Read Serial, The printer returns 12 digit serial number: \n                Function rmpCommand pDataIn: NULL \n                                 Function rmpCommand pDataOut: 12 Bytes: Printer Serial number */
    MCSP_SET_POWEROFF_TIMEOUT,  /**< Set power off timeout in seconds, valid value range[10-60], default 20: \n Function rmpCommand pDataIn: 1 Byte:  n \n                           Function rmpCommand pDataOut: NULL */
    MCSP_GET_POWEROFF_TIMEOUT,  /**< Query power off timeout \n                                                 Function rmpCommand pDataIn: NULL \n                                 Function rmpCommand pDataOut: 2 Bytes: Power off timer value */
    MCSP_GET_SENSOR_INFO,       /**< Get temperature of print head sensor. \n                                   Function rmpCommand pDataIn: NULL \n                                 Function rmpCommand pDataOut: 5 Bytes: x x , y y */
    MCSP_DISPLAY_COUNTER,       /**< (Instantly) Display counter \n                                             Function rmpCommand pDataIn: NULL \n                                 Function rmpCommand pDataOut: 8 Bytes: Printer counter number, little endian */

    RMPRINT_NUM_CMD             /**< Total command number */

}RMPRNT_COMMAND_ID;

/**
 * Open remote printer
 *
 * @param comDev     = Pointer to com device name
 *
 * @return
 * @li >0                        = Remote printer handle
 * @li -1                        = OS Error, check errno
 * @li E_PRNT_COM_NOT_AVAIL      = COM device not available
 * @li E_PRNT_PARAM_INVAL        = Invalid input parameter
 */
int rmpOpen(char* comDev);

//int rmpSetSwitchOffTime(int time_s);
//int rmpGetSwitchOffTime(void);

/**
 * Remote printer print text
 *
 * @param paToPrint     = Pointer text buffer
 * @param iSizeToPrint  = Size of text to be printed out
 *
 * @return
 * @li 0                         = Success
 * @li -1                        = OS Error, check errno
 * @li E_PRNT_COM_NOT_AVAIL      = COM device not available
 * @li E_PRNT_NOT_CONNECTED      = No or unknown printer is connected
 * @li E_PRNT_PARAM_INVAL        = Invalid input parameter
 * @li E_PRNT_PAPER_JAM_COVER_UP = Paper jamed and printer Cover is up
 * @li E_PRNT_PAPER_JAM          = Paper Jamed
 * @li E_PRNT_COVER_UP           = Printer cover is up
 */
int rmpPrint (char *paToPrint, int iSizeToPrint);

/**
 * Turn on remote printer
 *
 * @return
 * @li 0                         = Success
 * @li -1                        = OS Error, check errno
 * @li E_PRNT_COM_NOT_AVAIL      = COM device not available
 * @li E_PRNT_NOT_CONNECTED      = No or unknown printer is connected
 */
int rmpTurnOn(void);


/**
 * Turn on remote printer.
 *
 * The function returns immediately after starting a new thread
 * for the slow operation rmpTurnOn().
 *
 * Will call the passed callback function when printer is On.
 *
 * @param callback_func Callback function which is called when the printer is On.
 */
int rmpTurnOnThreaded( void (*callback_func)(int) );


/**
 * Turn off remote printer
 *
 * @return
 * @li SUCCESS(0)                = Success, started new thread
 * @li ERROR (-1)                = Error, could not start thread
 * @li E_PRNT_COM_NOT_AVAIL      = COM device not available
 * @li E_PRNT_NOT_CONNECTED      = No or unknown printer is connected
 */
int rmpTurnOff(void);

/**
 * Flush remote printer buffer
 *
 * @return
 * @li 0                         = Success
 * @li -1                        = OS Error, check errno
 * @li E_PRNT_COM_NOT_AVAIL      = COM device not available
 * @li E_PRNT_NOT_CONNECTED      = No or unknown printer is connected
 * @li E_PRNT_NOT_SUPPORT        = This feature is not supported by connected printer
 */
int rmpFlush(void);

/**
 * Get remote printer print status.
 *
 * @param statusType     = Set status type
 * @param statusInfo     = Pointer to buffer to store status info.
 * @param len            = Size of the buffer above
 *
 * @return
 * @li >0                        = Success, Bytes of status data
 * @li -1                        = OS Error, check errno
 * @li E_PRNT_COM_NOT_AVAIL      = COM device not available
 * @li E_PRNT_NOT_CONNECTED      = No or unknown printer is connected
 * @li E_PRNT_PARAM_INVAL        = Invalid input parameter
 */
int rmpGetStatus(char statusType, char* statusInfo, int len);

/**
 * Remote printer prints barcode
 *
 * @param barcodeType    = Set barcode system type
 * @param pBuf           = Pointer to buffer stored barcode data.
 * @param len            = Size of the barcode data
 *
 * @return
 * @li 0                         = Success
 * @li -1                        = OS Error, check errno
 * @li E_PRNT_COM_NOT_AVAIL      = COM device not available
 * @li E_PRNT_NOT_CONNECTED      = No or unknown printer is connected
 * @li E_PRNT_PARAM_INVAL        = Invalid input parameter
 * @li E_PRNT_PAPER_JAM_COVER_UP = Paper jamed and printer Cover is up
 * @li E_PRNT_PAPER_JAM          = Paper Jamed
 * @li E_PRNT_COVER_UP           = Printer cover is up
 */
int rmpPrintBarCode(char barcodeType, char* pBuf, int len);

/**
 * Update Remote printer firmware
 *
 * @param pFileName      = Pointer to firmware name string
 *
 * @return
 * @li 0                         = Success
 * @li -1                        = OS Error, check errno
 * @li E_PRNT_COM_NOT_AVAIL      = COM device not available
 * @li E_PRNT_NOT_CONNECTED      = No or unknown printer is connected
 * @li E_PRNT_PARAM_INVAL        = Invalid input parameter
 */
int rmpUpdateFirmware(char* pFileName);

/**
 * Download logo into remote printer
 *
 * @param sectorId       = Sector in which the logo is downloaded.
 * @param logoFileName   = Pointer to logo name string
 *
 * @return
 * @li >0                        = Success, Sector ID in which the logo is actually downloaded.
 * @li -1                        = OS Error, check errno
 * @li E_PRNT_COM_NOT_AVAIL      = COM device not available
 * @li E_PRNT_NOT_CONNECTED      = No or unknown printer is connected
 * @li E_PRNT_PARAM_INVAL        = Invalid input parameter
 */
int rmpDownloadLogo(char sectorId, char* logoFileName);

/**
 * Download font into remote printer
 *
 * @param fontId         = Sector in which the font is downloaded.
 * @param fontFileName   = Pointer to font name string
 *
 * @return
 * @li >0                        = Success, Sector ID in which the font is actually downloaded.
 * @li -1                        = OS Error, check errno
 * @li E_PRNT_COM_NOT_AVAIL      = COM device not available
 * @li E_PRNT_NOT_CONNECTED      = No or unknown printer is connected
 * @li E_PRNT_PARAM_INVAL        = Invalid input parameter
 * @li E_PRNT_NOT_SUPPORT        = This feature is not supported by connected printer
 */
int rmpDownloadFont(char fontId, char* fontFileName);

/**
 * Remote printer prints logo
 *
 * @param logoId         = logo to be printed out.
 * @param pBuf           = Pointer to additional command if needed.
 * @param len            = Size of the additional command
 *
 * @return
 * @li 0                         = Success
 * @li -1                        = OS Error, check errno
 * @li E_PRNT_COM_NOT_AVAIL      = COM device not available
 * @li E_PRNT_NOT_CONNECTED      = No or unknown printer is connected
 * @li E_PRNT_PARAM_INVAL        = Invalid input parameter
 * @li E_PRNT_PAPER_JAM_COVER_UP = Paper jamed and printer Cover is up
 * @li E_PRNT_PAPER_JAM          = Paper Jamed
 * @li E_PRNT_COVER_UP           = Printer cover is up
 */
int rmpPrintLogo(char logoId,char* pBuf, int len);

/**
 * Remote printer prints Bit Image in line
 *
 * @param mode           = Set bitImage printing mode.
 * @param pBuf           = Pointer to bitImage data buffer.
 * @param len            = Size of the bitImage data
 *
 * @return
 * @li 0                         = Success
 * @li -1                        = OS Error, check errno
 * @li E_PRNT_COM_NOT_AVAIL      = COM device not available
 * @li E_PRNT_NOT_CONNECTED      = No or unknown printer is connected
 * @li E_PRNT_PARAM_INVAL        = Invalid input parameter
 * @li E_PRNT_NOT_SUPPORT        = This feature is not supported by connected printer
 * @li E_PRNT_PAPER_JAM_COVER_UP = Paper jamed and printer Cover is up
 * @li E_PRNT_PAPER_JAM          = Paper Jamed
 * @li E_PRNT_COVER_UP           = Printer cover is up
 */
int rmpPrintBitImage(char mode, char* pBuf, int len);

/**
 * Execute specific printer dependent command
 *
 * @param      cmdID          = Set command ID
 * @param      pDataIn        = Pointer to command buffer.
 * @param      inlen          = Size of the command data
 * @param[out] pDataOut       = Pointer to output buffer
 * @param      maxOutlen      = Size of the output buffer
 *
 * @return
 * @li >=0                       = Success or bytes of returned data
 * @li -1                        = OS Error, check errno
 * @li E_PRNT_COM_NOT_AVAIL      = COM device not available
 * @li E_PRNT_NOT_CONNECTED      = No or unknown printer is connected
 * @li E_PRNT_PARAM_INVAL        = Invalid input parameter
 * @li E_PRNT_NOT_SUPPORT        = This feature is not supported by connected printer
 */
int rmpCommand(char cmdID, char* pDataIn, int inlen, char* pDataOut, int maxOutlen);

/**
 * Detect remote printer type
 *
 * @return
 * @li 3                         = TG2460H printer detected
 * @li 2                         = TG2460 printer detected
 * @li 1                         = MCS(M1470) printer detected
 * @li 0                         = No or unknown printer detected
 * @li -1                        = OS Error, check errno
 * @li E_PRNT_COM_NOT_AVAIL      = COM device not available
 * @li E_PRNT_PAPER_JAM_COVER_UP = Paper jamed and printer Cover is up
 * @li E_PRNT_PAPER_JAM          = Paper Jamed
 * @li E_PRNT_COVER_UP           = Printer cover is up
 */
int rmpDetect(void);

/**
 * Get remote printer lib version
 *
 * @param pVersion       = Pointer to version buffer.
 * @param len            = Size of the version buffer
 *
 * @return
 * @li >0                        = Success,Bytes of returned version data
 * @li -1                        = OS Error, check errno
 * @li E_PRNT_COM_NOT_AVAIL      = COM device not available
 * @li E_PRNT_PARAM_INVAL        = Invalid input parameter
 */
int rmpGetVersion(char* pVersion, int len);

/**
 * Set remote printer printing density
 *
 * @param level          = Density level:
 * @li					a) for TG* printer values [0..8], [48..56]
 * @li					b) for M1470/Artema printer values [-10..10], -10 brightest, +10 darkest.
 *
 * @return
 * @li 0                         = Success
 * @li -1                        = OS Error, check errno
 * @li E_PRNT_COM_NOT_AVAIL      = COM device not available
 * @li E_PRNT_PARAM_INVAL        = Invalid input parameter
 * @li E_PRNT_NOT_CONNECTED      = No or unknown printer is connected
 */
int rmpSetDensity(signed int level);

/**
 * Set remote printer character size
 *
 * @param width          = Character width.
 * @param height         = Character height.
 *
 * @return
 * @li 0                         = Success
 * @li -1                        = OS Error, check errno
 * @li E_PRNT_COM_NOT_AVAIL      = COM device not available
 * @li E_PRNT_PARAM_INVAL        = Invalid input parameter
 * @li E_PRNT_NOT_CONNECTED      = No or unknown printer is connected
 */
int rmpSetCharacterSize(char width,char height);

/**
 * Set remote printer cut type
 *
 * @param cutType        = full cut or partial cut.
 *
 * @return
 * @li 0                         = Success
 * @li -1                        = OS Error, check errno
 * @li E_PRNT_COM_NOT_AVAIL      = COM device not available
 * @li E_PRNT_PARAM_INVAL        = Invalid input parameter
 * @li E_PRNT_NOT_CONNECTED      = No or unknown printer is connected
 * @li E_PRNT_NOT_SUPPORT        = This feature is not supported by connected printer
 */
int rmpCut(char cutType);

/**
 * Close remote printer
 *
 * @return
 * @li 0                         = Success
 * @li -1                        = OS Error, check errno
 * @li E_PRNT_COM_NOT_AVAIL      = COM device not available
 * @li E_PRNT_PARAM_INVAL        = Invalid input parameter
 * @li E_PRNT_NOT_CONNECTED      = No or unknown printer is connected
 */
int rmpClose(void);



#ifdef __cplusplus
}
#endif


#endif /* _REMOTE_PRINTER_H_ */
