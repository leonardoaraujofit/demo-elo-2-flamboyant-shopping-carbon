
/**************************************************************************
 * FILE NAME:   cap_touch.h                                               *
 * MODULE NAME: CAP_TOUCH                                                 *
 * PROGRAMMER:  Don Ward                                                  *
 * DESCRIPTION: Capacitive Touchpanel Driver header                       *
 **************************************************************************/
#ifndef __CAP_TOUCH_H__
#define __CAP_TOUCH_H__

	/*==========================================*
	*         I N T R O D U C T I O N          *
	*==========================================*/

	/*==========================================*
	*           D E F I N I T I O N S          *
	*==========================================*/

#define STM_READ_WRITE_SIZE_MAX 	128 // Max size of a single read or write when under control of STM bootloader
#define	K2_MAX_SPI_MSG_LEN	        (STM_READ_WRITE_SIZE_MAX+10) // Sensible limit for max size of one SPI buffer

#ifndef RAPTOR

// SPI Modes
#define CPOL0_CPHA0	0
#define CPOL0_CPHA1	1
#define CPOL1_CPHA0	2
#define CPOL1_CPHA1	3
#else
#include <linux/sigtiff.h>
#endif


// X and Y max values

#define stringify(val) #val

// 2 levels are needed for these macros to work properly
#define _paste_(a,b) a##b
#define paste(a,b) _paste_(a,b)

#define Firmware_ID ((Firmware_Info >> 16) & 0xff)
#define Firmware_Version ((Firmware_Info >> 8) & 0xff)
#define Firmware_Sub_Version ((Firmware_Info >> 0) & 0xff)

#ifdef __KERNEL__
#define MSG(fmt,args...) { struct timeval tv; do_gettimeofday(&tv); printk(KERN_INFO "%ld.%06ld %s %d: "fmt,tv.tv_sec,tv.tv_usec,__func__,__LINE__,##args);}
#else
#define MSG(fmt,args...) { struct timeval tv; gettimeofday(&tv,0); printf("%ld.%06ld %s %d: "fmt,tv.tv_sec,tv.tv_usec,__func__,__LINE__,##args); }
#endif

#define RELATIVE 0
#define ABSOLUTE 1
#define INPUT_MODE ABSOLUTE
struct write_read
{
    const void *tx_buff;
    void *rx_buff;
    int len;
};

struct cal_pair
{
    xy_t disp;
    xy_t raw;
};

struct diagnostics
{
    struct diagData
    {
	int tag; // 24 lsb are actual tag, 8 msb are qualifier
	int seq;
	struct timeval tv;
	union
	{
	    long info;
	    xyz_t xyz;
	};
    } __attribute__((packed)) *data;
    int count;
    long flags;
};

struct user_input
{
    int typ;
    int code;
    int value;
};

enum touch_param_mode
{    
    TOUCH_PARAM_MODE_NORMAL = 0,
    TOUCH_PARAM_MODE_SIGCAP,
    TOUCH_PARAM_MODE_TEST,
    TOUCH_PARAM_MODE_SIGNON,
    
    TOUCH_PARAMS_MODE_COUNT // This must stay at end of list
};

enum touch_param_action
{
    TOUCH_PARAM_ACTION_READ_CURRENT,
    TOUCH_PARAM_ACTION_USE_DEFAULT,
    TOUCH_PARAM_ACTION_READ,
    TOUCH_PARAM_ACTION_READ_DEFAULT,
    TOUCH_PARAM_ACTION_WRITE,
    TOUCH_PARAM_ACTION_SET_MODE,
    TOUCH_PARAM_ACTION_GET_MODE,
};

enum touch_param_item
{
    TOUCH_PARAM_Z_THRESHOLD_PEN_ON,
    TOUCH_PARAM_Z_THRESHOLD_PEN_OFF,
    TOUCH_PARAM_Z_THRESHOLD_FINGER_ON,
    TOUCH_PARAM_Z_THRESHOLD_FINGER_OFF,
    TOUCH_PARAM_Z_ON_COUNT,
    TOUCH_PARAM_SMOOTHING_FILTER_LENGTH,
    TOUCH_PARAM_EXPONENTIAL_SMOOTHING_RATIO,
    TOUCH_PARAM_JITTER_MAX_X,
    TOUCH_PARAM_JITTER_MAX_Y,
    TOUCH_PARAM_FUZZ_X,
    TOUCH_PARAM_FUZZ_Y,
    TOUCH_PARAM_FINGER_SIGNATURE_ENABLE,
    
    TOUCH_PARAMS_ITEM_COUNT  // This must stay at end of list
};

struct touch_param_rw
{
    enum touch_param_mode mode;
    enum touch_param_action action;
    enum touch_param_item item;
    int *value;
};

#ifndef RAPTOR

struct gpio
{
    enum gpio_command
    {
	GPIO_GET,
	GPIO_SET_PIN,
	GPIO_SET_MODE,
	GPIO_SET_INTR,
	GPIO_WRITE,
	GPIO_READ,
	GPIO_DIAG,
    } command;
    u_char group;
    u_char sub;
    u_char number;
    u_char open;
    u_char pinsetup;
    u_char mode;
    u_char intrpin;
    u_char intr;
    u_char unmask;
    u_char mask;
    u_char read;
    u_char write;
};

struct i2c_params
{
    u_short addr;		// slave address
    u_short flags;		// flags including r/w bit
    u_short len;		// msg length
    void *buf;			// pointer to msg data
    u_char port;		// port (I2C channel) number 0 or 1
};
#define I2C_M_WR 0		// for documentation only

struct egx_i2c_params
{
    void *cmdData;
    enum egx_cmdTweaks cmdTweaks;
    int respTimeout;
    void *respData;
};

#define egx_i2c_pkt(dataLen) \
  struct { \
      u_char id; \
      u_char dlen; \
      u_char data[dataLen]; \
} __attribute__((packed))

#define EGX_I2C_DATA_LEN 8
#define EGX_I2C_PKT_LEN sizeof(egx_i2c_pkt(EGX_I2C_DATA_LEN))
#define EGX_I2C_DATA_OFFSET offsetof(egx_i2c_pkt(0),data)

#define EGX_FN_VER_ID_STR "75E2v"
#define EGX_FN_TEST_ID_STR "Test"
#define EGX_SUBVER_SIZE_BITS 8
#define EGX_RELEASE_SUBVER 0x10

typedef egx_i2c_pkt(EGX_I2C_DATA_LEN) egxCmdPkt_t;

struct special_i2c_params
{
    u_short reg;		// audio chip register address
    u_short flags;		// flags including r/w bit
    u_short len;		// data length
    void *buf;			// pointer to message data
};

#endif

// Hex rather than (1<<...) values shown so that one can quickly see which values to set for diagnostics
// The 6 ms nibbles only are used here so that the 2 ms nibbles can be used by driver as a qualifier if needed
#define DIAGFLAG_PRINT_PKT_DATA		0x0000002
#define DIAGFLAG_PRINT_XYZ_DATA		0x0000004
#define DIAGFLAG_PRINT_DRDY_IRQ		0x0000008
#define DIAGFLAG_PRINT_ADC_VALUES	0x0000010
#define DIAGFLAG_PRINT_SIG_POINT	0x0000020
#define DIAGFLAG_PRINT_SIG_DUMP		0x0000040
#define DIAGFLAG_PRINT_SPI_CS		0x0000080
#define DIAGFLAG_PRINT_LINE_NUMBER	0x0000100
#define DIAGFLAG_PRINT_SCALED_DATA	0x0000200
#define DIAGFLAG_PRINT_EDGE_FILTER	0x0000400
#define DIAGFLAG_PRINT_PKT_TIMING	0x0000800
#define DIAGFLAG_PRINT_DRDY_MONITOR	0x0001000
#define DIAGFLAG_PRINT_SIG_PARAMS	0x0002000
#define DIAGFLAG_PRINT_SIG_RESULT	0x0004000
#define DIAGFLAG_DBG_FIFO_OVERFLOW	0x0008000
#define DIAGFLAG_PRINT_ALPF_DATA	0x0010000
#ifndef RAPTOR
#define DIAGFLAG_PRINT_EGX_DATA		0x0020000
#endif
#define DIAGFLAG_PRINT_DEBUG_DATA	0x0040000
#define DIAGFLAG_PRINT_INCORRECT_FEED	0x0080000

#define DIAGQUAL(q) ((q) << 24)

enum miscDbg_command
{
    MISCDBG_RESET,
    MISCDBG_COUNT,
    MISCDBG_END
};

enum dataCtrl_command
{
    DATA_DISABLE_INIT,
    DATA_DISABLE,
    DATA_RESTORE
};

#ifdef RAPTOR
#define touch_dev_cirque "/dev/input/cirque_k2"
#else
#define touch_dev_cirque "/dev/captouch"
#endif
#define touch_dev touch_dev_cirque

#define CAP_TOUCH_MAGIC 0xEF // Seemed to be free according to Documentation/ioctl-number.txt

#define CAP_TOUCH_GET_FIRMWARE_INFO	_IO(CAP_TOUCH_MAGIC,1)
#define CAP_TOUCH_WRITE_READ		_IOWR(CAP_TOUCH_MAGIC,2,struct write_read)
#define CAP_TOUCH_SELECT_SPI_MODE	_IO(CAP_TOUCH_MAGIC,3)
#define CAP_TOUCH_HARD_RESET		_IO(CAP_TOUCH_MAGIC,4)
#define CAP_TOUCH_DATA_CONTROL		_IO(CAP_TOUCH_MAGIC,5)
#define CAP_TOUCH_SIGCAP                _IOWR(CAP_TOUCH_MAGIC,6,struct sigcap_params)
#define CAP_TOUCH_GET_PANEL_INFO	_IOR(CAP_TOUCH_MAGIC,7,struct touchpanel_info)
#define CAP_TOUCH_GET_MODEL_INFO	_IO(CAP_TOUCH_MAGIC,8)
#define CAP_TOUCH_GET_STYLUS_ID		_IO(CAP_TOUCH_MAGIC,9)
#define CAP_TOUCH_GET_DATA_READY	_IO(CAP_TOUCH_MAGIC,10)
#define CAP_TOUCH_SET_PANEL_INFO	_IOW(CAP_TOUCH_MAGIC,11,struct touchpanel_info)
#define CAP_TOUCH_DIAGNOSTICS		_IOWR(CAP_TOUCH_MAGIC,12,struct diagnostics)
#define CAP_TOUCH_PRINTK		_IOW(CAP_TOUCH_MAGIC,13,char)
#define CAP_TOUCH_GET_XYZ		_IOR(CAP_TOUCH_MAGIC,14,xyz_t)
#define CAP_TOUCH_STYLUS_ONLY		_IO(CAP_TOUCH_MAGIC,15)
#define CAP_TOUCH_GPIO			_IOWR(CAP_TOUCH_MAGIC,16,struct gpio)
#define CAP_TOUCH_PREAMBLE_STATE	_IO(CAP_TOUCH_MAGIC,17)
#define CAP_TOUCH_GET_PANEL_MISSING	_IO(CAP_TOUCH_MAGIC,18)
#define CAP_TOUCH_GET_LIB_DIAGFLAGS	_IOR(CAP_TOUCH_MAGIC,19,long)
#define CAP_TOUCH_SET_LIB_DIAGFLAGS	_IOW(CAP_TOUCH_MAGIC,20,long)
#define CAP_TOUCH_PALM_REJECT		_IO(CAP_TOUCH_MAGIC,21)

#define CAP_TOUCH_K2_PARAMS_CHANGED	_IO(CAP_TOUCH_MAGIC,23)
#define CAP_TOUCH_MANUAL_CAL_NEEDED	_IO(CAP_TOUCH_MAGIC,24)
#define CAP_TOUCH_WAIT_DATA_READY	_IO(CAP_TOUCH_MAGIC,25)
#define CAP_TOUCH_SYNC_CC_WITH_HW_DR	_IO(CAP_TOUCH_MAGIC,26)
#define CAP_TOUCH_CIB_PANEL_INFO	_IOW(CAP_TOUCH_MAGIC,27,struct touchpanel_info)
#ifndef RAPTOR
#define CAP_TOUCH_I2C_XFER		_IOWR(CAP_TOUCH_MAGIC,28,struct i2c_params)
#endif
#define CAP_TOUCH_GET_FW_INFO_STR	_IOR(CAP_TOUCH_MAGIC,29,char)
#define CAP_TOUCH_SEND_DEBUG_DATA	_IO(CAP_TOUCH_MAGIC,30)
#define CAP_TOUCH_MISC_DEBUG		_IO(CAP_TOUCH_MAGIC,31)
#ifndef RAPTOR
#define CAP_TOUCH_EGX_GET_DATA_READY	_IO(CAP_TOUCH_MAGIC,32)
#define CAP_TOUCH_EGX_WAIT_DATA_READY	_IO(CAP_TOUCH_MAGIC,33)
#define CAP_TOUCH_EGX_I2C_COMMAND	_IOWR(CAP_TOUCH_MAGIC,34,struct egx_i2c_params)
#define CAP_TOUCH_EGX_I2C_WAKEUP	_IOR(CAP_TOUCH_MAGIC,35,char)
#endif
#define CAP_TOUCH_CONTROL_HOTSPOTS	_IO(CAP_TOUCH_MAGIC,36)
#define CAP_TOUCH_GET_HOTSPOTS		_IOR(CAP_TOUCH_MAGIC,37,hotspot_table_t)
#define CAP_TOUCH_SET_HOTSPOTS		_IOW(CAP_TOUCH_MAGIC,38,hotspot_table_t)
#define CAP_TOUCH_RW_DEVMEM		_IOWR(CAP_TOUCH_MAGIC,39,struct devmem_access)
#define CAP_TOUCH_READ_HOTSPOT_DATA	_IOR(CAP_TOUCH_MAGIC,40,hotspot_data_t)
#ifndef RAPTOR
#define CAP_TOUCH_PREAMBLE_MODE		_IO(CAP_TOUCH_MAGIC,41)
#endif
#ifdef RAPTOR
#define CAP_TOUCH_SET_KP_HOTSPOTS 	_IOW(CAP_TOUCH_MAGIC,42,struct touch_hs_s)
#endif
#define CAP_TOUCH_USER_INPUT		_IOW(CAP_TOUCH_MAGIC, 43, struct user_input)
#define CAP_TOUCH_USER_XYZ		_IOW(CAP_TOUCH_MAGIC, 44, xyz_t)
#define CAP_TOUCH_RW_TOUCH_PARAM	_IOWR(CAP_TOUCH_MAGIC, 45, struct touch_param_rw)
#define CAP_TOUCH_GROUNDED		_IO(CAP_TOUCH_MAGIC,46)
#define CAP_TOUCH_SUSPENDED		_IO(CAP_TOUCH_MAGIC,47)
#define CAP_TOUCH_COPY_PARAMS_FROM_K2	_IO(CAP_TOUCH_MAGIC,48)
#ifdef RAPTOR
#define CAP_TOUCH_DOUBLETAP_TIMEOUT 	_IO(CAP_TOUCH_MAGIC,49)
#endif

#ifndef RAPTOR
#define EGX_RESP_TIMEOUT_MS_DEFAULT	1000

#define EGX_I2C_SLAVE_ADDR 		0x04
#define EGX_I2C_PORT 			0
#define EGX_HID_REPORT_LEN		64

#define SPECIAL_I2C_XFER		_IOWR(CAP_TOUCH_MAGIC,99,struct special_i2c_params)
#define SPECIAL_I2C_SLAVE_ADDR 0x14
#define SPECIAL_I2C_PORT 1

#define CIRQUE_DATA_READY() 	(GPIO_Read(GPIO_DATA_READY) ? 1 : 0)
#define GPIO_DATA_READY		GPIO_B,2,0 // or GPIO_B,BITNUM,2

#endif

#endif /*#ifndef __CAP_TOUCH_H__*/
