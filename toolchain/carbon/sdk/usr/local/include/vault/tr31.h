
#ifndef __TR31_H__
#define __TR31_H__


#define ASCII_DES_KEY_LEN			16
#define ASCII_TRIPLE_DES_KEY_LEN	(ASCII_DES_KEY_LEN * 2)

// KSN10 is 10 bytes (binary), ASCII_KSN requires 2 hex digits per byte
#define ASCII_KSN10_LEN                                20
#define ASCII_KSN12_LEN                                24

#define KMM_STR_LEN                 3
#define GID_STR_LEN                 3
#define SLOT_STR_LEN                5
#define LABEL_STR_LEN               128
#define ENGINE_STR_LEN              1
#define USER_STR_LEN                2
#define DELETE_KEYS_COMMAND_STR_LEN 1
#define FRIENDLY_LABEL_STR_LEN      32
// delete keys string length equeals its longest possible argument
#define DELETE_KEYS_STR_LEN         (DELETE_KEYS_COMMAND_STR_LEN + FRIENDLY_LABEL_STR_LEN)


#define TR31_KEY_BLOCK_VER_ID_SIZE	1
#define TR31_KEY_BLOCK_LEN_SIZE		4
#define TR31_KEY_USAGE_SIZE			2
#define TR31_ALGORITHM_SIZE			1
#define TR31_MODE_OF_USE_SIZE		1
#define TR31_KEY_VER_NUM_SIZE		2
#define TR31_EXPORTABILITY_SIZE		1
#define TR31_NUM_OPT_BLOCKS_SIZE	2
#define TR31_RESERVED_SIZE			2


// ASCII character for the delete keys block.
#define DELETE_ALL_KEYS_NOT_RKL_RSA	'y'
#define DO_NOT_DELETE_ANY_KEYS		'n'
#define DELETE_IPP_NON_RKL_KEYS		'p'
#define DELETE_KEYS_ENGINE			'e'
#define DELETE_KEYS_USER			'u'
#define DELETE_KEYS_SLOT			's'
#define DELETE_FRIENDLY_LABEL		'f'

// ASCII character describing the key engine.
#define KEY_ENGINE_IPP_DUKPT		'd'
#define KEY_ENGINE_IPP_MS			'm'
#define KEY_ENGINE_VSS				'v'
#define KEY_ENGINE_VSS_KLK			'k'
#define KEY_ENGINE_RSA_KEY			'r'
#define KEY_ENGINE_ECC_KEY			'e'
#define KEY_ENGINE_VSP				'p'
#define KEY_ENGINE_ADE_DUKPT        'a'
#define KEY_ENGINE_USR				'u'
#define KEY_ENGINE_ATOS				'z'
#define KEY_ENGINE_OTHER			'o'
#define KEY_ENGINE_POSEIDON			'z'

#define RSA_TYPE_KRD_CRED           'k'
#define RSA_TYPE_SSL_SERVER_CERT    's'
#define RSA_TYPE_SSL_CLIENT_CERT    'c'
#define RSA_TYPE_SSL_BOTH_CERT      'b'
// TODO: "SCR key type" is not defined in Verifone KBH specification
#define RSA_TYPE_SCR_KEY            'r'
#define RSA_TYPE_PAIRING            'p'
#define RSA_TYPE_TERMINAL_SIGN      't'
#define RSA_TYPE_KEY_EXCHANGE       'e'


typedef struct
{
	// Data from the Header.
	char key_block_ver_id;
	char key_block_len[TR31_KEY_BLOCK_LEN_SIZE + 1];
	char key_usage[TR31_KEY_USAGE_SIZE + 1];
	char algorithm;
	char mode_of_use;
	char key_ver_num[TR31_KEY_VER_NUM_SIZE + 1];
	char exportability;
	char num_opt_blocks[TR31_NUM_OPT_BLOCKS_SIZE + 1];
	char reserved[TR31_RESERVED_SIZE + 1];

	// Data from the Optional blocks.
	char ksn[ASCII_KSN12_LEN + 1]; // key serial number
	char kmm_mask[KMM_STR_LEN + 1];
	char kmm[KMM_STR_LEN + 1];
	char gid[GID_STR_LEN + 1];
	char slot[SLOT_STR_LEN + 1];
	char label[LABEL_STR_LEN + 1];
	char engine;
	char delete_keys[DELETE_KEYS_STR_LEN + 1];
	char rsa_type;

	// The key.
	char *key;
	int keylen;

} TR31DATA;


#endif
