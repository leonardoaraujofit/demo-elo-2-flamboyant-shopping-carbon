#ifndef _PEDGUARD_H_
#define _PEDGUARD_H_

/*

    ;*************************************************************
    ;*************************************************************
    ;*************************************************************
    ;*   VeriFone.Inc  - SMF            This source code     *
    ;*   is confidential proprietary information of VeriFone     *
    ;*   Inc. and is an unpublished work or authorship protected *
    ;*   by the copyright laws of the United States.         *
    ;*   Unauthorized copying or use of this document or the     *
    ;*   program contained herein, in original or modified form, *
    ;*   is a violation of Federal and State law.            *
    ;*                               *
    ;*************************************************************
    ;*************************************************************
    ;*************************************************************

*/

/*
;|===========================================================================+
;| FILE NAME:  | PEDGUARD.H
;|-------------+-------------------------------------------------------------+
;| DEVICE:     | PEDGUARD
;|             |
;|-------------+-------------------------------------------------------------+
;| DESCRIPTION:| PEDGUARD FA DEFINITIONS
;|             | OSTYPE
;|             | PUBLIC FUNCTIONS PROTOTYPES
;|             | ERROR CODE
;|             |
;|-------------+-------------------------------------------------------------+
;| SOFTWARE    | PEDGUARD FA
;| TYPE        |
;|-------------+-------------------------------------------------------------+
;| OS          |
;|-------------+-------------------------------------------------------------+
;| ENVIRONNMENT| None
;| VARIABLES   |
;|-------------+-------------------------------------------------------------+
;| LANGUAGE    | C
;|-------------+-------------------------------------------------------------+
;| VERSION:    | 0.9
;|             |
;|             |
;|-------------+-------------------------------------------------------------+
;| DATE:       | October 1999
;|-------------+-------------------------------------------------------------+
;| REVISION    | 0.1     : Initial draft     / Dominique Gougeon
;| HISTORY:    |
;|             |
;|-------------+-------------------------------------------------------------+
;| COMMENTS:   |
;|             |
;|===========================================================================+
*/


/*
;|===========================================================================+
;| NOTE: | OSTYPE
;|   |
;|===========================================================================+
*/

#define PC          0
#define DAYTONA     0
#define K2          0
#define PULSAR      0
#define SPECTRUM    1
#define TRIDENT     1

#define DEBUG       0      /* just use to debug the module */


#if TRIDENT
  #define _TRIDENT_ 
#endif
/*
;|===========================================================================+
;|===========================================================================+
*/

                                

#if DAYTONA
#define  _DAYTONA_
#define  _RSAPACIFIC_       /*Rsa module */
#define  RANDOM      1      /* Random generator turned on/off */
#define CERT_FILE "CertFile.CRT"  /* Name of the file where the certicate tree is store */
#define MAX_CERTIFICATES    12
#define O_BINARY 0
#endif

#if K2
#define  _K2_
#define  _RSAHNL_       /*Rsa module */
//define  _RSAPACIFIC_       /*Rsa module */
#define  RANDOM       0     /* Random generator turned on/off */
#define CERT_FILE "/CertFile.sys"
#define MAX_CERTIFICATES    22
#endif


#if PC
#define  _PC_
#define  _RSASMF_           /*Rsa module */
#define  RANDOM       1     /* Random generator turned on/off */
#define CERT_FILE "CertFile.CRT"  /* Name of the file where the certicate tree is store */
#define MAX_CERTIFICATES    12
#endif


#if PULSAR
#include "error.h"
#define  _PULSAR_       1
#define  _RSAPULSAR_    1           /*Rsa module */
#define  _RSASMF_        0           /*Rsa module */
#define  RANDOM         0           /* Random generator turned on/off */
#define CERT_FILE       "0/CertFile.sys"    /* Name of the file where the certicate tree is store */
#define O_BINARY        0x8000
#define malloc    PulsarMalloc
#define free    PulsarFree
#define MAX_CERTIFICATES    12
#endif


#if SPECTRUM
#ifndef _SPECTRUM_
#define  _SPECTRUM_     1
#endif
#define  _RSAHNL_       1
#define  RANDOM         0           /* Random generator turned on/off */
#define CERT_FILE       "/root/crt/certfile.sys"
#define MAX_CERTIFICATES    66
//#define dbprintf printf
#endif


/*
;|===========================================================================+
;| NOTE: | General define
;|   |
;|===========================================================================+
*/



#define CLEARFILETYPE    "CLEAR"  /* Name of the "fileType" for the certificate tree    */
                                  /* deletion mechanism based on the random             */


/*
;|===========================================================================+
;| NOTE: | PUBLIC FUNCTIONS
;|   |
;|===========================================================================+
*/


/****************************************************************************************/
/****************************************************************************************/
/* Function                                            */
/****************************************************************************************/
/****************************************************************************************/
/*
;|==========================================================================+
;| FUNCTION   | int getPedguardVersion( char *  version );
;|            |
;|------------+-------------------------------------------------------------
;| DESCRIPTION|copy the Pedguard version string in the buffer pointed
;|            |by the parameter char * version
;|            |If version = NULL, then only the lenght is returned
;|            |
;|            |
;|------------+-------------------------------------------------------------
;| PARAMETERS |char *   version :   buffer where the string is copied
;|            |
;|------------+-------------------------------------------------------------
;| RETURN     |int : length of the string including the string ending '/0x00'
;|        |      ( Limited to 16 characters )
;|        |
;|        |
;|            |
;|===========================================================================+
*/

int getPedguardVersion( char *  version );




/****************************************************************************************/
/****************************************************************************************/
/* Function                                        */
/****************************************************************************************/
/****************************************************************************************/

int checkPKCS7signature( const char * signatureFile,
                            const char * fileToBeChecked,
                            unsigned char * shaBuff,
                            const char * fileTypeInfo,
                            unsigned int mode,
                            const char *CRLfile	);



/*
-parameter in:

const char *   signatureFile : The path and the name of the file
                            that contains the  PKCS7 signature

const char *  fileToBeChecked: The path and the name of the file
                                   to be checked. This parameter is used
                                  in the mode =1

byte  * shaBuff     : The sha string of the file to be checked
                            this  parameter is used in the mode = 2

const char * fileTypeInfo : This string parameter indicates the type
                            of the file to be checked. This type needs
                            to be present in the FileType extension of the
                            partition certificates to which the signer
                            certficate used to check this file, is attached to.
                            Otherwise,this file cannot be authenticate by this
                            certificate
                            In the mode == 3, then this parameter is ignored
                            since the fileType shall be "CLEAR"

unsigned int mode        : mode = 0 : The name of file to be checked
                                          is retrieved from the signature file


                           mode=1  : The name of the file to be checked
                                         is indicated by the parameter
                                         fileToBeChecked of thid function

                           mode=2  : The sha string of the file to be
                                         checked is given by the parameter
                                         shaBuff

                           mode=3  :  The signature is checked and compare if the
                                      random value signed is this one previously stored.
                                      The partition used needs to have the fileType "CLEAR"
                                      In this mode, the parameter 'fileTypeInfo'
                                      is ignored

-returm:  0 : Bad Signature
         or
         1 : Good Signature
         or
         < 0 Error  - File does not exist or bad PKCS7 signature format
                      or The certificate cannot use for this type of file



*/



/****************************************************************************************/
/****************************************************************************************/
/* Function                                        */
/****************************************************************************************/
/****************************************************************************************/

short CMMInitialize (char *rootfilename);
/* -----------------3/3/99 4:29PM--------------------
 this routine validates the FA certificate heirarchy
 and optionally re-initializes the PEDguard FA storage and
 heirarchy mechanisms.  this routine must be called
 before any other routine in the FA storage system.

 Inputs:    none
 Outputs:   none.

 Result:    2
            1                           new CM file created
            0               initialization successful.

        ERR_CERTIFICATES_CORRUPT    CRC mismatch, certificates corrupted.
                    Tree rebuilt.

        ERR_CANNOT_READ_ROOT    Root file open failed.  Initialization
                    failed.

            ERR_FILE_READ               read error on tree file.  Initialization
                                        failed.

 Side Effects: the Certificate Storage file is opened,
           thus initializing the file handle "certFile".
 --------------------------------------------------*/


/****************************************************************************************/
/****************************************************************************************/
/* Function                                        */
/****************************************************************************************/
/****************************************************************************************/
short addCertificate (char *filename);
/* -----------------3/12/99 1:46PM-------------------
 Using the Authority Identifier and the Subject
 Identifier fields of the new Certificate place
 the new Certificate in the Certificate heirarchy.

 Inputs:    filename                    name of file containing
                                        certificate to add.
 Outputs:   none

 Result:    0   successful
        ERR_CERTIFICATES_CORRUPT    certificates are corrupted
        ERR_INVALID_CERT        invalid new certificate
        ERR_CERT_FILE_FULL      out of certificate space
        ERR_CERT_REJECTED_WIDE  attempted to add peer to level
                                    occupied by a wide certificate
 --------------------------------------------------*/


/****************************************************************************************/
/****************************************************************************************/
/* Function                                        */
/****************************************************************************************/
/****************************************************************************************/
short CMDiag (short diagOption, char *data, short *dataLength);
/* --------------------------------------------------
 Perform diagnostic routines on the file and records.

 Inputs:    diagOption      0 - check CRC of each record
                            1 - check tree structure for
                                valid links and lost/
                                unlinked records.
                            2 - retrieve records serially,
                                starting with the first
                                record below the root.
                3 - retrieve serial number and
                unique ID of each record
                serially starting with the
                first record below the root.

            data            pointer to data area allocated
                            by caller.  If zero(0) only
                            length will be returned.

 Outputs:   data            pointer to caller's buffer
                            used to receive tree representation

            dataLength      amount of data placed in the
                            buffer.

 Results:   option      result meaning
        ------      --------------
          0         0 - all CRCs match.  Data okay.
               <0 - data is corrupt

          1         0 - all record links are valid,
                and all records are linked
                together.
               -1 - there are invalid links.
               -2 - there are unlinked records.

          2,3       0 - success.
               <0 - End-of-data or error code.
                See list of errors in ErrorPED.h
 --------------------------------------------------*/



#ifdef RANDOM


/****************************************************************************************/
/****************************************************************************************/
/* Function                                        */
/****************************************************************************************/
/****************************************************************************************/
 /*
;|===========================================================================+
;| FUNCTION   | void getRand(unsigned char * pRandom )
;|------------+--------------------------------------------------------------+
;| DESCRIPTION| This function generate a random based on SHA and keep record
;|        | of the last calling to generate the next random
;|------------+--------------------------------------------------------------+
;| PARAMETERS | unsigned char * : the buffer where the 8 hex-ascii
;|        |           byte random is stored
;|------------+--------------------------------------------------------------+
;| RETURN     | void
;|------------+--------------------------------------------------------------+
;| GLOBAL     |
;| VARIABLES  |
;|        |
;|------------+--------------------------------------------------------------+
;| LOCAL      |
;| VARIABLES  |
;|        |
;|        |
;|        |
;|------------+--------------------------------------------------------------+
;| COMMENTS   |
;|        |
;|===========================================================================+
*/
void getRandom(unsigned char * pRandom );




/****************************************************************************************/
/****************************************************************************************/
/* Function                                        */
/****************************************************************************************/
/****************************************************************************************/
/*
;|===========================================================================+
;| FUNCTION   | void initRandom( unsigned char * seed, unsigned int seedLen )
;|------------+--------------------------------------------------------------+
;| DESCRIPTION| This function initialize the random generator,
;|        | The initialisation is based on the USN and the time when the
;|        | function is called
;|------------+--------------------------------------------------------------+
;| PARAMETERS |unsigned char * seed: seed
;|        |unsigned int seedLen: the length of the seed
;|------------+--------------------------------------------------------------+
;| RETURN     | void
;|------------+--------------------------------------------------------------+
;| GLOBAL     | randomPool
;| VARIABLES  |
;|        |
;|------------+--------------------------------------------------------------+
;| LOCAL      | ptrSHAcontext
;| VARIABLES  | i
;|        |
;|------------+--------------------------------------------------------------+
;| COMMENTS   |
;|        |
;|===========================================================================+
*/
void initRandom( unsigned char * pSeed, unsigned int seedLen );




#endif




/*
;|===========================================================================+
;| NOTE: | ERROR
;|   |
;|===========================================================================+
*/

/*General errors */

#define PEDERROR                -1

#define ERRORSYSTEM                             PEDERROR
#define ERRORX509                               PEDERROR-100
#define ERRORPKCS7                              PEDERROR-200
#define ERRORPKCS1                              PEDERROR-300
#define ERRORASN1BER                            PEDERROR-400
#define ERRORCMM                                PEDERROR-500
#define ERRORCRL                                PEDERROR-600
#define ERRORRKL                                -700



/*system and miscelleaneous */
#define MALLOC_ERROR                            ERRORSYSTEM-1
#define FILE_OPEN_ERROR                         ERRORSYSTEM-2
#define RSA_PARAMETER_ERROR                     ERRORSYSTEM-3
#define INVALID_ARGS_ERROR                      ERRORSYSTEM-4
#define BUFFER_SIZE_ERROR                       ERRORSYSTEM-5
#define UNKNOWN_SERV_ID		                    ERRORSYSTEM-6
#define INVALID_RESP_ERROR						ERRORSYSTEM-7
#define INVALID_USAGE_ERROR						ERRORSYSTEM-8


/* File     : PKCS7RD.H */
/* Function : getPKCS7Info  */
/* File     : FILESIG.H */
/* Function : checkPKCS7signature() */
#define FILE_SIG_OPEN_ERROR                     ERRORPKCS7-1
#define DATAOPENFILE_ERROR                      ERRORPKCS7-2
#define PKCS7SHA_ERROR                          ERRORPKCS7-3
#define CERTIFICATE_ERROR                       ERRORPKCS7-4
#define KEYUSAGECERTIFICATE_ERROR               ERRORPKCS7-5
#define FILETYPECERTIFICATE_ERROR               ERRORPKCS7-6
#define KEYUSECNT_READ_ERROR                    ERRORPKCS7-7
#define KEYUSECNT_EXPIRED_ERROR                 ERRORPKCS7-8
#define KEYUSECNT_DECREMENT_ERROR               ERRORPKCS7-9
#define SIGCERTIFICATE_ERROR                    ERRORPKCS7-10
#define READCERTIFICATE_ERROR                   ERRORPKCS7-11
#define CERTIFICATENOTFOUND_ERROR               ERRORPKCS7-12
#define PKCS7_FORMAT_ERROR                      ERRORPKCS7-13
#define PKCS7_FORMAT_SIGNDATAOID_ERROR          ERRORPKCS7-14
#define PKCS7_FORMAT_VERSION_ERROR              ERRORPKCS7-15
#define PKCS7_FORMAT_SHA_ERROR                  ERRORPKCS7-16
#define PKCS7_FORMAT_FILEDATA_ERROR             ERRORPKCS7-17
#define PKCS7_FORMAT_ATTRIBUTES_ERROR           ERRORPKCS7-18
#define PKCS7_FORMAT_ENCRYPTED_ERROR            ERRORPKCS7-19
#define PKCS7_FORMAT_OID_ERROR                  ERRORPKCS7-20
#define PKCS7_FORMAT_KEYID_ERROR                ERRORPKCS7-21



/* File     : X509READ.C    */
/* Function : getX509Info   */
/* File     : X509SIG.C     */
/* Function : checkX509signature()  */
#define X509_VERSION_ERROR                      ERRORX509-1
#define X509_KEY_ERROR                          ERRORX509-2
#define X509_EXTENSION_MISSING_ERROR            ERRORX509-3
#define CRITICAL_EXTENSIONS_UNKOWN_ERROR        ERRORX509-4
#define X509_SUBJECT_KEYIDENTIFIER_ERROR        ERRORX509-5
#define X509_INHERITANCE_ERROR                  ERRORX509-6
#define X509_AUTHORITY_KEYIDENTIFIER_ERROR      ERRORX509-7
#define X509_FILETYPE_ERROR                     ERRORX509-8
#define X509_CACONSTRAINT_ERROR                 ERRORX509-9
#define X509_EXTENSION_TWICE_ERROR              ERRORX509-10
#define X509_EXTENSION_ERROR                    ERRORX509-11
#define OID_STRING_TOO_LONG_ERROR               ERRORX509-12
#define X509_KEYWIDTH_ERROR                     ERRORX509-13
#define X509_KEYUSECOUNTER_ERROR                ERRORX509-14
#define X509_KEYUSAGE_ERROR                     ERRORX509-15
#define X509_SIGNATURE_ERROR                    ERRORX509-16
#define X509_TBS_ERROR                          ERRORX509-17
#define X509_SERIALNUMBER_ERROR                 ERRORX509-18
#define X509_UNIQUEID_ERROR                     ERRORX509-19
#define X509_FORMAT_SHA_ERROR                   ERRORPKCS7-20






/* File     : BER.C             */
/* File     : ASN1.C            */
#define COMPONENTNOTFOUND                       ERRORASN1BER-1
#define ERRORBERDECODING                        ERRORASN1BER-2
#define OUTOFDECODINGLIMIT                      ERRORASN1BER-3
#define BERBADFORMAT                            ERRORASN1BER-4
#define BUFFERTOOSMALL                          ERRORASN1BER-5
#define INTEGERBADFORMAT                        ERRORASN1BER-6
#define BOOLEANBADFORMAT                        ERRORASN1BER-7
#define BADCOMPONENT                            ERRORASN1BER-8
#define STRINGBADFORMAT                         ERRORASN1BER-9
#define ERRORMODE                               ERRORASN1BER-10
#define BADBERSTRING                            ERRORASN1BER-11


/* File     : PKCS1SIG.C        */
/* Function : checkPKCS1Sig     */
/* File     : PKCS1ENC.C        */
/* Function : makePKCS1packet   */
#define PKCS1_FORMAT_ERROR                      ERRORPKCS1-1



#define ERR_CERTIFICATES_CORRUPT                ERRORCMM-1
#define ERR_CANNOT_READ_ROOT                    ERRORCMM-2
#define ERR_FILE_READ                           ERRORCMM-3
#define ERR_CANNOT_CREATE_CERT_FILE             ERRORCMM-4
#define ERR_INVALID_CERT                        ERRORCMM-5
#define ERR_CERT_FILE_FULL                      ERRORCMM-6
#define ERR_CERT_REJECTED_WIDE                  ERRORCMM-7
#define ERR_DUPLICATE_CERT                      ERRORCMM-8
#define ERR_DIAG_NO_MORE_RECORDS                ERRORCMM-9
#define ERR_INVALID_CERT_HANDLE         ERRORCMM-10



#define CRL_READ_ERROR                          ERRORCRL-1
#define CRL_READCERTIFICATE_ERROR               ERRORCRL-2
#define CRL_CERTIFICATE_ERROR                   ERRORCRL-3
#define CRL_FILETYPECERTIFICATE_ERROR           ERRORCRL-4
#define CRL_KEYUSAGECERTIFICATE_ERROR           ERRORCRL-5
#define CRL_KEYUSECNT_READ_ERROR                ERRORCRL-6
#define CRL_KEYUSECNT_EXPIRED_ERROR             ERRORCRL-7
#define CRL_KEYUSECNT_DECREMENT_ERROR           ERRORCRL-8
#define CRL_AUTHENTICATE_ERROR                  ERRORCRL-9
#define CRL_INVALID_TIME_ERROR                  ERRORCRL-10
#define CRL_INVALID_SIG_TIME_ERROR              ERRORCRL-11
#define CRL_INVALID_SIG_ATTR_ERROR              ERRORCRL-12
#define CRL_INVALID_SER_NUM_ERROR               ERRORCRL-13
#define CRL_CERT_REVOKED_ERROR                  ERRORCRL-14


#define ERR_RKL_NO_MEMORY                       ERRORRKL-1
#define ERR_RKL_NO_KRD_PRIV_KEY                 ERRORRKL-2
#define ERR_RKL_NO_KRD_CERT                     ERRORRKL-3
#define ERR_RKL_BLOB_DECRYPT                    ERRORRKL-4
#define ERR_RKL_INVALID_KEY_SEQ                 ERRORRKL-5
#define ERR_RKL_INVALID_KEY                     ERRORRKL-6
#define ERR_RKL_SURREP_FORWARD_CHECK            ERRORRKL-7
#define ERR_RKL_READ_TR34_FILE                  ERRORRKL-8
#define ERR_RKL_INVALID_TR34_DATA               ERRORRKL-9
#define ERR_RKL_PARSE_TR31_KBH                  ERRORRKL-10
#define ERR_RKL_INVALID_SIG                     ERRORRKL-11
#define ERR_RKL_AUTH_FAIL                       ERRORRKL-12
#define ERR_RKL_NO_CRL                          ERRORRKL-13
#define ERR_RKL_EXTRA_CRL                       ERRORRKL-14
#define ERR_RKL_CRL_READ                        ERRORRKL-15
#define ERR_RKL_CRL_CERT                        ERRORRKL-16
#define ERR_RKL_CRL_AUTH                        ERRORRKL-17
#define ERR_RKL_CRL_INVALID                     ERRORRKL-18
#define ERR_RKL_CERT_REVOKED                    ERRORRKL-19
#define ERR_RKL_NO_SIG_FILES                    ERRORRKL-20
#define ERR_RKL_INVALID_DUKPT_KBH               ERRORRKL-21
#define ERR_RKL_INVALID_DUKPT_KEY               ERRORRKL-22
#define ERR_RKL_STORE_IPP_DUKPT_KEY             ERRORRKL-23
#define ERR_RKL_INVALID_MS_KEY                  ERRORRKL-24
#define ERR_RKL_STORE_IPP_MS_KEY                ERRORRKL-25
#define ERROR_VSS_SCRIPT_NUM                    ERRORRKL-26
#define ERROR_VSS_SCRIPT_NOT_LOADED             ERRORRKL-27
#define ERROR_VSS_KEY_SLOT                      ERRORRKL-28
#define ERROR_VSS_KEY_SLOT_NOT_LOADABLE         ERRORRKL-29
#define ERROR_VSS_DUKPT_KEY_AREA                ERRORRKL-30
#define ERROR_VSS_DUKPT_ALLOCATE_KEY_AREA       ERRORRKL-31
#define ERROR_VSS_DUKPT_KEY_LENGTH              ERRORRKL-32
#define ERROR_VSS_KEY_LENGTH                    ERRORRKL-33
#define ERROR_VSS_DUKPT_KSN_LENGTH              ERRORRKL-34
#define ERROR_VSP_FILE_OPEN                     ERRORRKL-35
#define ERROR_VSP_FILE_WRITE                    ERRORRKL-36
#define ERROR_USR_USR_NUM                       ERRORRKL-37
#define ERROR_USR_SLOT_NUM                      ERRORRKL-38
#define ERROR_USR_FILE_OPEN                     ERRORRKL-39
#define ERROR_USR_FILE_WRITE                    ERRORRKL-40
#define ERROR_USR_NOT_IMPLEMENTED               ERRORRKL-41
#define ERROR_TS_OPEN_CREATE                    ERRORRKL-42
#define ERROR_TS_OLD                            ERRORRKL-43
#define ERROR_TS_KEY_ENGINE                     ERRORRKL-44
#define ERROR_TS_READ                           ERRORRKL-45
#define ERROR_TS_WRITE                          ERRORRKL-46
#define ERROR_TS_SCRIPT_NUM                     ERRORRKL-47
#define ERROR_TS_VSS_KEY_SLOT                   ERRORRKL-48
#define ERROR_TS_MS_KEY_SLOT                    ERRORRKL-49
#define ERROR_TS_DUKPT_KEY_SLOT                 ERRORRKL-50
#define ERROR_TS_USR                            ERRORRKL-51
#define ERROR_TS_USR_SLOT                       ERRORRKL-52
#define ERROR_DELETE_OPTION                     ERRORRKL-53
#define ERROR_TS_ADE_DUKPT_KEY_SLOT             ERRORRKL-54
#define ERROR_ADE_DUKPT_KEY_LENGTH              ERRORRKL-55
#define ERROR_ADE_DUKPT_KEY_TR31_DATA           ERRORRKL-56
#define ERR_POSEIDON_INVALID_KBH                ERRORRKL-57
#define ERR_RKL_NO_TR31_FILES                   ERRORRKL-58


/*
;|===========================================================================+
;| NOTE: | MISCELLANEOUS DEFINITIONS
;|   |
;|===========================================================================+
*/

#ifndef OK
#define OK               ZERO
#endif

#ifndef FALSE
#define FALSE            0x00
#endif

#ifndef TRUE
#define TRUE            !FALSE
#endif

#ifndef pNULL
#define pNULL           (void*)NULL
#endif

#define BUFFER_SIZE      512

#ifndef ZERO
#define ZERO     0
#endif

#ifndef ONE
#define ONE      1
#endif

#ifndef TWO
#define TWO      0x02
#endif

#ifndef THREE
#define THREE    0x03
#endif

#define BIT0             0x01
#define BIT1             0x02
#define BIT2             0x04
#define BIT3             0x08
#define BIT4             0x10
#define BIT5             0x20
#define BIT6             0x40
#define BIT7             0x80


/*
;|===========================================================================+
;| NOTE: | TYPEDEF
;|   |
;|===========================================================================+
*/


#ifndef TYPE
#define TYPE
    typedef unsigned char   byte;   /*  8 bit */
    typedef unsigned short  word;   /* 16 bit */
#endif




/*
;|===========================================================================+
;|       END   END   END   END     END    END   END   END  END  END
;|===========================================================================+
*/
#endif

