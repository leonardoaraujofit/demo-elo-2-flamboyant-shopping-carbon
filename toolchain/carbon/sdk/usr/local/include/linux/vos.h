#ifndef _VOS_H_
#define _VOS_H_

/** @file Vos.h
  * @brief This file contains generic printer definitions for adaptering printer daemon
  *
  */
/********************************************************
Change log:
	05/05/2014: initial draft
********************************************************/

/* ======================================= *
 *         D E F I N I T I O N S           *
 * ======================================= */

//#define dbg(x...) printk(KERN_ERR x)
#define dbg(x...) do {} while (0)

//#define MAX_COPY_SIZE   128 // Nothing to do with MAXY now
#define MAX_COPY_SIZE   64 // Nothing to do with MAXY now
#define LTPDRV_OK              	0
#define LTPDRV_BUF_FULL      	-1

#define MAX_IN_BUF		64*4
#define MAX_LINE_BUF	64*4
#define MAX_KMALLOC_SIZE 0x400000

#define PIXEL_LINE_LEN       	48 /*in bytes*/
#define  MAX_DST_PIXELS       48/*64*/ /* max. pixels in one DST pulse 64*/
////#define  MAX_DST_PIXELS       24
//#define  MAX_DST_PIXELS       32
#define  MAX_DIVIDE_NUM       (PIXEL_LINE_LEN/(MAX_DST_PIXELS/8)) //Edward change to 64

/* IOCTL code*/
#define LTPIO_GET_STATUS	0x6b01
#define LTPIO_CANCEL_PRINT	0x6b02
#define LTPIO_DAEMON_PID	0x6b03
#define LTPIO_GET_BUSY_STATUS	0x6b04 //Edward add

#define PRINTER_OK		       0	/* Successful result */
#define PRINTER_OUT_OF_PAPER	      -1	/* No Paper */

/* Step Motor states*/
#define  MOTOR_NOT_ACTIVE     	0
#define  MOTOR_START          	1
#define  MOTOR_STEP1          	2
#define  MOTOR_STEP2          	3
#define  MOTOR_STEP3		4
#define  MOTOR_STEP4          	5
#define  MOTOR_STEP5          	6
#define  MOTOR_STEP6          	7
#define  MOTOR_STEP7          	8
#define  MOTOR_STEP8          	9

/*==========================================*
 *         P U B L I C    T Y P E S         *
 *==========================================*/

typedef struct
{
    unsigned long SeqNum;		/* 0-255*/
    unsigned long LineLen;    	/* in bytes in one pixel line, if =0, Line is empty*/
    unsigned long LinesNum;  	/* number of pixel lines in data array */
    unsigned char *Data;		/* data array */
} PrintDataStr;

typedef struct
{
    unsigned long LastPrintedSeqNum;	/* 0-255, after Init = 255*/
    unsigned long NumDataInQueue;   	/* 0 if empty*/
    unsigned long CurrentSeqNum;		/* if printing now*/
    unsigned long CurrentLine;			/* pixel line index from current Data, which is printing now*/
    unsigned long OutOfPaper;			/* 0 if paper is OK*/
    unsigned long Temperature;			/*Temperature*/
    unsigned long Vp;					/* 0, if off, what to do, if not Vp A2D????*/
    unsigned long Motor_State;
    unsigned long Job_id;
    unsigned long LowBattery;
    unsigned long Cutter_State;
} PrintStatusStr;

typedef struct
{
    unsigned long Signature;
    unsigned char  Padding;
    unsigned char MotorTimeIndx;    /*Motor time as index in StepTime table*/
    /*Pixel line can be devided to some parts, if the number of pixels is big*/
    /*If Pixel line is empty, DivideNum=1 and Data is zero filled array*/
    unsigned char MaxDivideIndx;
    unsigned char BlockIndx;   /* Index of input block for this line in InBuf*/
    unsigned int DstTime;    /* DST (STROBE) time in timer units*/
    unsigned int LineIndx;  /* indx of pixel lines in input data array */
    unsigned char LineData[MAX_DIVIDE_NUM][PIXEL_LINE_LEN]; /* pixel lines data */
    //unsigned char LineData[48][PIXEL_LINE_LEN]; /* pixel lines data */
} PrintLineStr;

/*==========================================*
 *    P U B L I C    M E T H O D S          *
 *==========================================*/

//---------------------------------------------------
// Receive print data method
//---------------------------------------------------
int LTPDrv_GetStatus(PrintStatusStr *Status);
//int LTPDrv_Print(PrintDataStr *Data, int job_id);
//int LTPDrv_Print(const char __user *Data, int job_id);
int LTPDrv_Print(const char __attribute__((noderef, address_space(1))) *Data, unsigned long int job_id);
void LTPDrv_Reset (void);
void LTPFifo_Reset (void);
void LTPFill_Reset (void);
void fillstrlineDoWork (void);


/*==========================================*
 *    P U B L I C    V A R I A B L E S      *
 *==========================================*/
//Edward mark
//extern unsigned char InBufBusy;
/*
extern unsigned char	LineBufRdIndx;
extern unsigned char	NextLineRdIndx;
*/

#endif /* _PRINTER_H_ */
