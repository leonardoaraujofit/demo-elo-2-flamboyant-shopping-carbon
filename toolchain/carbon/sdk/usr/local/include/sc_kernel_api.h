/****************************************************************************
* This source code is confidential proprietary information of VeriFone Inc. *
* and is an unpublished work or authorship protected by the copyright laws  *
* of the United States. Unauthorized copying or use of this document or the *
* program contained herein, in original or modified form, is a violation of *
* Federal and State law.                                                    *
*                                                                           *
*                           Verifone                                        *
*                      Rocklin, CA 95765                                    *
*                        Copyright 2003                                     *
****************************************************************************/

/*
;|===========================================================================+
;| FILE NAME:  | sc_kernel_api.h
;|-------------+-------------------------------------------------------------+
;| DESCRIPTION:| kernel part of the smart card driver
;|-------------+-------------------------------------------------------------+
;| TYPE:       | 
;|-------------+-------------------------------------------------------------+
;| COMMENTS:   | definitions for User and Kernel parts of smart card 
;|               driver based on LINUX.
;|===========================================================================+
*/
/*
$Archive:   $
$Author:    Thierry Payet
$Date:      04/08/14
$Modtime:   $
$Revision:  0.1
$Log:       $
*/

#ifndef __SC_KERNEL_API_H__
#define __SC_KERNEL_API_H__
#include "sc_kernel.h"

          /*==========================================*
           *           D E F I N I T I O N S          *
           *==========================================*/

#define SC_FUNC_TYPE_MASK		0x0000ff00
#define SC_FUNC_INDX_MASK		0x000000ff
#define SC_FUNC_VOID_VOID		0x00000100
#define SC_FUNC_VOID_DATA		0x00000200
#define SC_FUNC_VOID_WR_PNTR	0x00000400
#define SC_FUNC_VOID_RD_PNTR	0x00000800
#define SC_FUNC_DATA_DATA		0x00001000
#define SC_FUNC_DATA_STRCT   	0x00002000
#define SC_FUNC_IO_COMND_DATA 	0x00004000

/*---------------- Functions: void F(void)----------*/  

/*---------------- Functions: void F(data)----------*/  

/*---------------- Functions: void F( *wr_data|)----------*/

/*---------------- Functions: void F( *rd_data)----------*/  
#define SC_KERNEL_GetVersion           (SC_FUNC_VOID_RD_PNTR|1)
#define SC_KERNEL_GetMaxSlot           (SC_FUNC_VOID_RD_PNTR|2)

/*---------------- Functions: data F(data)----------*/  
#define SC_KERNEL_PowerDown            (SC_FUNC_DATA_DATA|1)
#define SC_KERNEL_ShutDown             (SC_FUNC_DATA_DATA|2)
#define SC_KERNEL_GpioWrite            (SC_FUNC_DATA_DATA|3)
#define SC_KERNEL_GpioRead             (SC_FUNC_DATA_DATA|4)
#define SC_KERNEL_Register_PID			(SC_FUNC_DATA_DATA|5)

/*---------------- Functions: data F( *struct)----------*/  
#define SC_KERNEL_PowerUpCold          (SC_FUNC_DATA_STRCT|1)
#define SC_KERNEL_PowerUpWarm          (SC_FUNC_DATA_STRCT|2)
#define SC_KERNEL_SlotStatus           (SC_FUNC_DATA_STRCT|3)  
#define SC_KERNEL_IsoTransceive        (SC_FUNC_DATA_STRCT|4)  
#define SC_KERNEL_AtrResult            (SC_FUNC_DATA_STRCT|5)
#define SC_KERNEL_RxResult             (SC_FUNC_DATA_STRCT|6)  
#define SC_KERNEL_RunScript            (SC_FUNC_DATA_STRCT|7)
/*-------------------------------------(SC_FUNC_DATA_STRCT|8) is reserved*/

typedef struct
{
	int   Return;
	int   Param;
	IccSynScriptStruct   *Script;
	void  *pCommand;
}SC_KernelCommandStr;


void wakeUpApduProcess(void);
void wakeUpAtrProcess(void);
void wakeUpAtrSyncProcess(void);

#endif
 /*of__SC_KERNEL_API_H__ */


