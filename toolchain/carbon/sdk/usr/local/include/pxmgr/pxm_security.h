/*
 *      Copyright, 2013 VeriFone Inc.
 *      2099 Gateway Place, Suite 600
 *      San Jose, CA.  95110
 *
 *  All Rights Reserved. No part of this software may be reproduced,
 *  transmitted, transcribed, stored in a retrieval system, or
 *  translated into any language or computer language, in any form
 *  or by any means electronic, mechanical, magnetic, optical,
 *  chemical, manual or otherwise, without the prior permission of
 *  VeriFone Inc.
 */

#ifndef PXM_SECURITY_H_
#define PXM_SECURITY_H_

#include <pxmgr.h>

#ifdef __cplusplus
extern "C" {
#endif

#define PUB_KEY_LEN(k) (sizeof(MAGIC_PUBKEY_TYPE)-1+(k)->wModuloLength)
#define CERT_LEN(c) (sizeof(MAGIC_PUBCERT_TYPE)-1+(c)->wSignatureSize)

#define UX300_RS_CHALLENGE_SIZE		8

enum {
    MG_ROOT_KEY,
    MG_CA_KEY,
    MG_CA_CRT,
    MG_DSK_KEY,
    MG_DSK_CRT
} MG_KEY_TYPE;

typedef struct
{
   int type;           /* Key type */
   unsigned int id;    /* unique identifier */
}  KEY_LIST;

typedef struct
{
   unsigned int uKeyID;               /* Key unique identifier     */
   unsigned int uExponent;            /* Public key exponent       */
   unsigned short wModuloLength;        /* Public key modulo         */
   unsigned char tbModulus[1];          /* Public key modulus        */
} __attribute__ ((packed)) MAGIC_PUBKEY_TYPE;

typedef struct
{
   unsigned int uCertifiedID;         /* Certified key ID         */
   unsigned int uCertifyingID;        /* Certifying key ID        */
   unsigned int uValidFrom;           /* Valid from ddmmyyyy      */
   unsigned int uValidTo;             /* Valid to ddmmyyyy        */
   unsigned short wSignatureSize;       /* Signature size in bytes  */
   unsigned char tbSignature[1];        /* Certificate data         */
} __attribute__ ((packed)) MAGIC_PUBCERT_TYPE;

typedef struct
{
   unsigned char tbSha256[32];          /* SHA-256 hash*/												
   unsigned char bHashType;    			/* Type of additional Hash   */	 	
   unsigned char bKeySlot; 		        /* Key usage                 */	
   unsigned char bByte0;                /* Zero byte  				 */
   unsigned char bRDT1;                 /* Recover data trailer ABh  */
   unsigned int uCertifiedID;           /* Certified key ID          */
   unsigned int uCertifyingID;          /* Certifying key ID         */
   unsigned int uValidFrom;             /* Valid from ddmmyyyy       */
   unsigned int uValidTo;               /* Valid to ddmmyyyy         */
   unsigned int uExponent;              /* Public key exponent       */
   unsigned char bRDT2;                 /* Recover data trailer CDh  */
   unsigned char tbSha1[20];            /* SHA-1 hash  (Deprecated, replaced by SHA-256) */
} __attribute__ ( ( packed ) ) MAGIC_SIGNATURE_TYPE;

int pxsec_getAttackStatus( int* isAttacked, int* barrierFlag, int* tamperReg );
int pxsec_readCertificate( int type, unsigned int id, MAGIC_PUBKEY_TYPE * pPubKey, MAGIC_PUBCERT_TYPE* pCert );
int pxsec_authCertificate( int type, MAGIC_PUBKEY_TYPE * pPubKey, MAGIC_PUBCERT_TYPE* pCert );
int pxsec_loadCertificate( int type, MAGIC_PUBKEY_TYPE * pPubKey, MAGIC_PUBCERT_TYPE* pCert );
int pxsec_initiateNS( int mode, int max_len, int *crypto_len, unsigned char *crypto1 );
int pxsec_resolveNS( int mode, int crypto1_len, unsigned char *crypto1, int crypto2_max_len, int *crypto2_len, unsigned char *crypto2 );
int pxsec_finalizeNS( int mode, int crypto2_Len, unsigned char *crypto2, unsigned char *crypto3 );
int pxsec_verifyNS( int mode, unsigned char *crypto3 );
int pxsec_generateChallenge(int mode, unsigned char *challenge, unsigned char *workingKey );
int pxsec_resolveChallenge( int mode, unsigned char *challenge, unsigned char *workingKey, unsigned char *result );
int pxsec_verifyChallenge(int mode, unsigned char *challenge );
int pxsec_setupCipheredlink(int mode, unsigned char *seed);
int pxsec_loadPin( int mode, int len, unsigned char* value );
int pxsec_getPinKey( int mode, unsigned char* key );
int pxsec_resolveARSChallenge( int mode, unsigned char* challenge, unsigned char* result );
int pxsec_verifyARSChallenge( int mode, unsigned char* challenge );
int pxsec_ServiceSwitchControl( int action );


#ifdef __cplusplus
}
#endif

#endif // PXM_SECURITY_H_
