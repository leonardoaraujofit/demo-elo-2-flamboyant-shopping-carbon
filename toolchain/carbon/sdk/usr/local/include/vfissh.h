/******************************************************************************
 *
 *		Copyright, 2007 VeriFone Inc.
 *		2099 Gateway Place, Suite 600
 *		San Jose, CA.  95110
 *
 *  All Rights Reserved. No part of this software may be reproduced,
 *  transmitted, transcribed, stored in a retrieval system, or
 *  translated into any language or computer language, in any form
 *  or by any means electronic, mechanical, magnetic, optical,
 *  chemical, manual or otherwise, without the prior permission of
 *  VeriFone Inc.
 *
 *****************************************************************************/
/**
 * @file vfissh.h
 */

#ifndef _VFISSH_H_
#define _VFISSH_H_

#ifdef __cplusplus
extern "C" {
#endif


#define SCP_ERROR_FILE	"/tmp/.scperror"
/**
 * usage: rtnVal = scpGetFile( "/home/usr1/toFile.dat", "data/fromFile.dat",
 *                             "10.64.89.16", "myname", "mypasswd", &status );
 *
 * @param[in] toFile     - contains name of to file (including any pathing) LOCAL
 *                         eg. "/home/usr1/toFile.dat"
 * @param[in] fromFile   - contains name of from file (including any pathing) REMOTE
 *                         eg. "data/fromFile.dat"
 * @param[in] remoteHost - IP address of remote host getting file from
 *                         eg. "10.64.89.16"
 * @param[in] user       - user name to be used for authentication 
 *                         (NULL if no user name required) eg. "myname"
 * @param[in] passwd     - password to be used for authentication 
 *                         (NULL if no password required) eg. "mypasswd"
 * @param[in] status     - location to place status of operation
 *
 *  @return:
 *  @li    0  = success
 *	@li -1,1  = failure
 */
int scpGetFile( char *toFile, char *fromFile, char *remoteHost, char *user, char *passwd, int *status );

/**
 *
 */
int scpGetFile2( ftp_parm_t *pFtp, int *status );

/**
 * usage: rtnVal = scpPutFile( "/home/usr1/fromFile.dat", "/home/usr1/toFile.dat",
 *                             "10.64.89.16", "myname", "mypasswd", &status );
 *
 * @param[in] fromFile   - contains name of from file (including any pathing) LOCAL
 *                         eg. "/home/usr1/fromFile.dat"
 * @param[in] toFile     - contains name of to file (including any pathing) REMOTE
 *                         eg. "/home/usr1/toFile.dat"
 * @param[in] remoteHost - IP address of remote host sending file to
 *                         eg. "10.64.89.16"
 * @param[in] user       - user name to be used for authentication 
 *                         (NULL if no user name required) eg. "myname"
 * @param[in] passwd     - password to be used for authentication 
                           (NULL if no password required) eg. "mypasswd"
 * @param[in] status     - location to place status of operation
 *
 * @return
 * @li    0 = success
 * @li -1,1 = failure
 */
int scpPutFile( char *fromFile, char *toFile, char *remoteHost, char *user, char *passwd, int *status );

/**
 *
 */
int scpPutFile2( ftp_parm_t *pFtp, int *status );



#ifdef __cplusplus
}
#endif

#endif
