/*
 *	Copyright, 2013 VeriFone Inc.
 *	2099 Gateway Place, Suite 600
 *	San Jose, CA.  95110
 *
 *  All Rights Reserved. No part of this software may be reproduced,
 *  transmitted, transcribed, stored in a retrieval system, or
 *  translated into any language or computer language, in any form
 *  or by any means electronic, mechanical, magnetic, optical,
 *  chemical, manual or otherwise, without the prior permission of
 *  VeriFone Inc.
 */

/*!
    @file   dymenu.h
    \brief  Dynamic Menu Library, which processes extension configs
    \author Vladimirs Ambrosovs
    \date   18/09/2013
    \version 1.0
*/

#pragma once

/*!
    \section Brief
    The library provides the user with the capabilities to obtain
    the list of the extensions, which are configured in the JSON
    format in the configuration files stored in /etc/config. 
    Thise configuration files can be uploaded to the device
    using special "config" type package. (For more information
    consult the Secure Installer documentation).

    \section HowTo
    The JSON configuration file follows several rules.

    \subsection filename Filename
    filename - should be of the format: \<user\>.sysmenu.json. 
       \<user\> should correspond to the owner of the config file
    \note: the name of the file and the owner of the file has hard
           binding, so if, the owner is usr1, but the name of the file
           is usr2.sysmenu.json, the configuration will NOT be loaded

    \subsection format Format
    The simple configuration JSON file is as follows:
    \verbatim

    {
        "parent"    : "1"
        "id"        : "14"
        "name"      : "Some agent"
        "minlevel"  : "supervisor"
        "items"     : 
        {
            "status" : "agntstat"
            "config" : "agntconf"
        }
    }
    \endverbatim
    
    parent    - the parent menu option of the extension in sysmode. must be one of the standard
    id        - the ID of the extension in menu, must be standard or 0, if 0, the system will give it a dynamic ID
    name      - the extension name
    minlevel  - the sysmode user, which is allowed to see this item
    items     - the array of items

    status  & config
     the names of the items (submenu names)
    
    agntstat & agntconf 
     the name of the applications, which should exist in /home/\<user\>/  
     directory. In other words, the value of the item is a relative to home 
     directory path to the binary

*/

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>

/*! \def DM_MAX_NAME_LEN 
    \brief the maximum length of the extension name */
#define DM_MAX_NAME_LEN     23 

/*!
    \def SOCK_FILE 
    \brief definition of the name of the socket to be used as address
*/
#define SOCK_FILE "/tmp/.sysm_ext_socket"


/*!
    \struct dymenu_iid
    \brief Structure which stores the identificator for the item
    
    The extension item is the minimal unit. Every extension may define
    several items. The user needs only the name of the item to be 
    displayed in the menu list. So this structure contains the name 
    of the item and it's identifier number, which will be then
    used by the library mechanism to load the correct extension
    without exposing the additional data to the user
*/
/*! \typedef dymenu_iid_t 
    \brief The type which wraps the dymenu_iid struct */
typedef struct dymenu_iid
{
    unsigned int   id;            /*!< id the identificator of the item */
    char name[DM_MAX_NAME_LEN+1]; /*!< name the name of the item */
} dymenu_iid_t;  


enum {
    E_DM_NOCONF = 1, /*!< No configuration was loaded */
    E_DM_INVAL,      /*!< Item does not exist in the configuration */
    E_DM_WAIT,       /*!< Could not wait on launched extension */
    E_DM_SOCKINI,    /*!< Could not initialize socket with launched extension */
    E_DM_ARG,        /*!< Wrong argument provided to the function */
    E_DM_DIR,        /*!< Could not read dir */
};


/*!
    \struct dymenu_ext
    \brief Structure which stores the data about one extension

    The structure contains the name of the structure, as well as
    the list and number of items
*/
/*! \typedef dymenu_ext_t 
    \brief The type which wraps the dymenu_ext struct */
typedef struct dymenu_ext
{
    int parent_id; /*!< extension parent ID for sysmode FLTK (must be taken from policy) */
    int id; /*!< extension ID for sysmode FLTK (must be taken from policy if standard or 0) */
    char name[DM_MAX_NAME_LEN+1]; /*!< name the name of the extension */
    dymenu_iid_t * items; /*!< items the pointer to the list of the items */
    size_t         items_len; /*!< items_len the length of list of items */
}  dymenu_ext_t; 


/*!
    \brief Function to retrieve the list of extensions

    The functions retrieves the list of extensions. It will
    be equal to the number of configuration files found. 
    As a result of the function the configurationd directory 
    (/etc/config, ), will be scanned for usr1.sysmenu.json
    (or other user name may be specified depending on which user,
    owns extension. NOTE: prefix should be equal to the username)
    file.As a result, a structure will be created on heap, and the
    pointer to the list will be returned by the function, together
    with length. 

    To dereference the structure, the dymenu_unref_exts
    should be used. When that function is called, the memory 
    gets released 

    If the length value was lost, it can be retrieved second
    time by calling \sa dymenu_get_len() 

    \note: when exts are no more required, use dymenu_unref_exts

    \note: if dymenu_unref_exts has not been called, and the
    function was called the second time, then already allocated
    list will be returned, so no double allocation happens.
    (i.e if the resources are no longer needed, the dymenu_unref_exts()
     has to be called)

    \param[in]  sysm_usr only those required by this access level,
                if NULL, list for all the users will be returned
    \param[out] exts pointer to an entry list of extension names
    \param[out] len number of extension names in the list
    \return Function returns 0 on Success, and -1 on Failure
            The actual error can be traced switching on the
            debug flag during compilation. This will allow to 
            see the error output in the syslog
    \sa dymenu_ref_exts() dymenu_unref_exts()
*/
int
dymenu_get_exts(const char * sysm_usr, dymenu_ext_t * exts[], size_t * len);

/*!
    \brief Function to execute the extension item

    The function will use the name to call the secure installer
    and execute the application. The function will use the name
    to find the appropriate extension
    
    \note Calling the function if successfull, will block until the
    executed process returns

    \param[in] item The iid structure of the item
    \return Return code of the launch operation:\n
        [ 0x0] Success\n\n
        [-0x1] Failure. No configuration has been loaded
               the call to dymenu_get_entries() required\n\n
        [-0x2] Failure. Unknown item strucutre has been passed
               Possibly created by the library user. Only items
               returned by dymenu_get_entries() are valid\n\n
        [-0x3] Application launched successfully, but library
               could not wait on the application. It means that
               launched application did not define __SYSM_EXTENSION
               or did not include <dyinit.h>. Possible fallback
               just exit the caller application, so the launched
               application could use the framebuffer\n\n
        [-0x4] Failed to initialize the listening socket,
               The reason may be different. debug should be enabled
               to see the actual reason (check errno) \n\n
        [>0x0] The Secure Installer error
*/
int 
dymenu_exec(dymenu_iid_t item);

/*!
    \brief Function to get the size of entries

    The function retrieves the length of currently allocated extensions
    The valid pointer will be considered the one, which was returned by 
    dymenu_get_exts(). If the user tries providing NULL or random pointer,
    the return value will be 0, as it will be considered an expired list.
    This ensures the safety of the function.

    \param[in] exts pointer to the list of extensions

    \note: if the function was called before dymenu_get_exts(), then
    the return value will be 0

    \note: if reference count value of the internal structure will
    reach 0, the extension entries will be dealocated and 0 will be returned

    \return number of the elements in the list, 0 on Failure
*/
size_t
dymenu_get_size(dymenu_ext_t * exts);

/*!
    \brief Function to release the memory allocated by the dymenu_get_exts()
           call

    \note Calling dymenu_get_exts() several times before dymenu_unref_exts(),
          will not cause double memory allocation, as already cached list
          will be returned, unles the dymenu_unref_exts() has been called

    \return 0 on Success, -1 on error
*/

void 
dymenu_unref_exts();

/*!
    \brief Function to get the information if any extensions are installed

    This function simply checks, whether any JSON files exist, without 
    loading them. This is useful for sysmode, especially, during the
    initialization process to determine, whether the "extensions" menu
    should be enabled or no. 

    \return 0 if no configurations exist, 1 if at least one was found

    \note If the JSON file is wrong, this function will still return
          success. The menu will be enabled, but when the user will try
          to get the list of extensions, the DEBUG will show, that 
          that some of the JSON file format is wrong
*/
int
dymenu_exts_installed();

#ifdef __cplusplus
}
#endif

