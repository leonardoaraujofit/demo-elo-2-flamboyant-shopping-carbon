#ifndef __SYSM_SMARTCARD__
#define __SYSM_SMARTCARD__

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sysmode_ui/sysm_lang.h>

extern const char * RSA_GEN_MSG;
extern const char * ERR_RSA_GEN_FAILED;
extern const char * REMOVE_CARD;
extern const char * PIN_OK_MSG;

typedef struct{
    char* name;
    unsigned short file_id;
    unsigned char key_slot;
    unsigned char cert_slot;
    unsigned char slave_slot;
    unsigned int key_id;
} key_struct ;

int sc_init(const char *, key_struct *, key_struct *);
int sc_authenticate(const char *, key_struct *, int);
int sc_build_cert(const char *, key_struct *);
int sc_verify_PIN(const char *, char *);
int sc_end();

#endif // __SYSM_SMARTCARD__
