/*****************************************************************************
* Copyright (c) 2012 VeriFone Inc.
* 2099 Gateway Place, Suite 600
* San Jose, CA. 95110
*
* All Rights Reserved. No part of this software may be reproduced,
* transmitted, transcribed, stored in a retrieval system, or
* translated into any language or computer language, in any form
* or by any means electronic, mechanical, magnetic, optical,
* chemical, manual or otherwise, without the prior permission of
* VeriFone Inc.
*
******************************************************************************/

#ifndef CTLSDUTYCYCLE_H
#define CTLSDUTYCYCLE_H

#ifdef __cplusplus
extern "C" {
#endif


#define CYCLE_DAEMON_OK 0
#define CYCLE_DAEMON_ERROR 1
#define CYCLE_DAEMON_BUSY 2
#define CYCLE_DAEMON_TEMP_CRIT 3

/*Does nothing, always returns CYCLE_DAEMON_OK*/
int ctls_cycle_daemon_field_on(void);

/*
 * 
 * Returns PICC_IsCardPresent() if ret is CYCLE_DAEMON_OK
 * If ret is not CYCLE_DAEMON_OK, then the returned value
 * has no meaning and should not be used
 * 
 * ret can also be:
 * CYCLE_DAEMON_ERROR: Basic communication failure, try again
 * CYCLE_DAEMON_BUSY: Try again in a while, something is not set up yet.
 * CYCLE_DAEMON_TEMP_CRIT: The temperature is too high, system
 * 		is in cooldown mode.
 * 
 * 
 * 
 * Ret needs to be a valid pointer to an integer
 * If ret is NULL the function returns 0xFF
 * */
unsigned char ctls_cycle_daemon_poll(int *ret);
/*
 * Turns the field off.
 * When the process holding the connection of the daemon 
 * quits the field will automatically be shut off.
 * returns CYCLE_DAEMON_OK on success
 * returns CYCLE_DAEMON_ERROR on failure
 * */

int ctls_cycle_daemon_field_off(void);


#ifdef __cplusplus
}
#endif

#endif
