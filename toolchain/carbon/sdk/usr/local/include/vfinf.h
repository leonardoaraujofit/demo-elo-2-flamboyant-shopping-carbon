//---------------------------------------------------------------------------

#ifndef vfintH
#define vfintH

//---------------------------------------------------------------------------

//#include "general.h"
//#include "types.def"


//#include "vendor_defns/hwe_vfisec.h"
#ifdef NEED_HW_VFISEC
 #include "hw_vfisec.h"
#endif

#if defined(__cplusplus)
extern "C" {
#endif

typedef unsigned char   u8;
typedef u8 *u8ptr;
typedef unsigned int    u32;
typedef unsigned int  	ulint;
typedef unsigned char   byte;

#define vs_rv_t VS_RV

typedef ulint VS_OBJECT_HANDLE;
typedef ulint VS_SESSION_HANDLE;

#define VS_INVALID_HANDLE  (0)

typedef int vs_rv_t;

typedef struct vs_version_t
{
  u8 major;
  u8 minor;
}vs_version_t;


/* Forward reference.  */
struct vs_function_list;

#ifdef WIN32
 #define DLL_CALL __declspec(dllexport)
#else
  #define DLL_CALL
#endif
//  VS_FUNCTION_LIST_PTR method;

#define _VS_DECLARE_FUNCTION(name, args)	\
typedef vs_rv_t (*VS_ ## name) args;		\
vs_rv_t DLL_CALL name args


#define _VSC_DECLARE_FUNCTION(name, args)	\
typedef u8ptr (*VS_ ## name) args;		\
u8ptr DLL_CALL name args


_VS_DECLARE_FUNCTION (V_Initialize, (void *init_args));
_VS_DECLARE_FUNCTION (V_Finalize, (void *reserved));
_VS_DECLARE_FUNCTION (V_GetFunctionList, (struct vs_function_list **function_list));

/*****************************/
/* D E S   F U N C T I O N S */
/*****************************/
_VS_DECLARE_FUNCTION (V_SYM_CRYPT, ( int         cryptHandle,    uint8_t     cryptType,
                                    uint8_t     encrypt,        uint8_t     Mode,
                                    uint8_t    *keyData,        uint8_t     ucKeySize,
                                    uint8_t    *auth_key,       uint8_t    *iv, 
                                    uint8_t    *srcData,        uint8_t    *dstData,   unsigned int size));


_VS_DECLARE_FUNCTION (V_GenerateKey, (  int  cryptHandle,
                                KEYTAG *KeyTag ,  GENKEY * Key ,int bits, unsigned long e_value));

_VS_DECLARE_FUNCTION (V_LoadKey, (  int  cryptHandle,
                                KEYTAG *KeyTag ,  GENKEY * Key));

_VS_DECLARE_FUNCTION (V_FreeGENKEY,(GENKEY * Key));

/*****************************/
/* R S A   F U N C T I O N S */
/*****************************/
_VS_DECLARE_FUNCTION (V_rsa_mod_exp, (  int cryptHandle,
                                    u8 *x, u32 x_bitlen,
                                    u8 *m, u32 m_bitlen,
                                    u8 *e, u32 e_bitlen,
                                    u8 *y, u32 *y_u8len    ));

_VS_DECLARE_FUNCTION (V_rsa_mod_exp_crt, (
                    int cryptHandle,
                    u8 *x,    u32 x_bitlen,
                    u8 *edq,
                    u8 *q,    u32 q_bitlen,
                    u8 *edp,
                    u8 *p,    u32 p_bitlen,
                    u8 *pinv,
                    u8 *y,    u32 *y_u8len  ));


_VS_DECLARE_FUNCTION (V_rsa_public_encrypt, (
                    int cryptHandle,
                    int flen,
                    const u8 *from,
                    u8 *to,
                    RSA_KEY *rsa,
                    int padding));

_VS_DECLARE_FUNCTION (V_rsa_public_decrypt, (
                    int cryptHandle,
                    int flen,
                    const u8 *from,
                    u8 *to,
                    RSA_KEY *rsa,
                    int padding));
_VS_DECLARE_FUNCTION (V_rsa_private_encrypt,(
                    int cryptHandle,
                    int flen,
                    const u8 *from,
                    u8 *to,
                    RSA_KEY *rsa,
                    int padding));
_VS_DECLARE_FUNCTION (V_rsa_private_decrypt,(
                    int cryptHandle,
                    int flen,
                    const u8 *from,
                    u8 *to,
                    RSA_KEY *rsa,
                    int padding));

_VS_DECLARE_FUNCTION (V_rsa_sign,(
                    int cryptHandle,
                    int type,
                    const u8 *m,
                    u32 m_length,
                    u8 *sigret,
                    u32 *siglen,
                    const RSA_KEY *rsa));
_VS_DECLARE_FUNCTION (V_rsa_verify,(
                    int cryptHandle,
                    int dtype,
                    const u8 *m,
                    u32 m_length,
                    const u8 *sigbuf,
                    u32 siglen,
                    const RSA_KEY *rsa));



/*****************************/
/* D S A   F U N C T I O N S */
/*****************************/
_VS_DECLARE_FUNCTION (V_dsa_sign, (
          int cryptHandle,
                  u8 *hash,
                  u8 *random,
                  u8 *p,      u32 p_bitlen,
                  u8 *q,
                  u8 *g,
                  u8 *x,
                  u8 *s,     u32 *s_u8len,
                  u8 *r,     u32 *r_u8len));

_VS_DECLARE_FUNCTION (V_dsa_verify, (
                    int cryptHandle,
                    u8 *hash,
                    u8 *p,    u32 p_bitlen,
                    u8 *q,
                    u8 *g,
                    u8 *y,
                    u8 *r,    u8  *s,
                    u8 *v,    u32 *v_u8len  ));

/***********************************************/
/* D E F F I   H E L M A N   F U N C T I O N S */
/***********************************************/
_VS_DECLARE_FUNCTION (V_diffie_hellman_generate, (
                    int cryptHandle,
                    u8 *x,      u32 x_bitlen,
                    u32 xvalid,
                    u8 *y,      u32 *y_u8len,
                    u8 *g,      u32 g_bitlen,
                    u8 *m,      u32 m_bitlen      ));

_VS_DECLARE_FUNCTION (V_diffie_hellman_shared_secret, (
            int cryptHandle,
                     u8 *x, u32 x_bitlen,
                     u8 *y, u32 y_bitlen,
                     u8 *m, u32 m_bitlen,
                     u8 *k, u32 *k_u8len    ));

/*****************************/
/* S H A   F U N C T I O N S */
/*****************************/
_VS_DECLARE_FUNCTION (V_SHA1_Init,(int cryptHandle, SHAcontext *c));
_VS_DECLARE_FUNCTION (V_SHA1_Update,(int cryptHandle, SHAcontext *c, const void *data, u32 len));
_VS_DECLARE_FUNCTION (V_SHA1_Final,(int cryptHandle, u8 *md, SHAcontext *c));
//typedef u8 *VS_V_SHA1(int cryptHandle, const u8 *d, u32 n, u8 *md)
_VSC_DECLARE_FUNCTION (V_SHA1,(int cryptHandle, const u8 *d, u32 n, u8 *md));

_VS_DECLARE_FUNCTION (V_SHA256_Init,(int cryptHandle, SHAcontext *c));
_VS_DECLARE_FUNCTION (V_SHA256_Update,(int cryptHandle, SHAcontext *c, const void *data, u32 len));
_VS_DECLARE_FUNCTION (V_SHA256_Final,(int cryptHandle, u8 *md, SHAcontext *c));
_VSC_DECLARE_FUNCTION (V_SHA256, (int cryptHandle, const u8 *d, u32 n,u8 *md));

_VS_DECLARE_FUNCTION (V_SHA_File,(int cryptHandle, const char *pFileName, u8 *pDigest,u32 is256));

/***************************************************************/
/* B A C K W A R D S   C O M P A T I B L E   F U N C T I O N S */
/***************************************************************/
_VS_DECLARE_FUNCTION (V_GetRnd, (int cryptHandle, u32 *pRandomBuf, u32 length));

_VS_DECLARE_FUNCTION (V_GetVaultVersion,(u8 * pVersion));

_VS_DECLARE_FUNCTION (V_pkaRSA,(u8 *msg, u8 *mod, u32 mlen, u8 *exp, u32 elen, u8 *result));

_VS_DECLARE_FUNCTION (V_ecdh_compute_key, (unsigned char *out,
										   unsigned int outlen,
										   unsigned char *eckey,
										   unsigned int eclen,
										   unsigned char *pub,
										   unsigned int publen,
										   unsigned char *kdf_ukm,
										   unsigned int kdf_ukm_len));

_VS_DECLARE_FUNCTION (V_wrap_cipher, (unsigned char *out,
									  unsigned char *in,
									  int inlen,
									  unsigned char *iv,
									  unsigned char *key,
									  int keylen,
									  int decrypt));

/***********************************************/
/* S P E C I F I C   F U N C T I O N A L I T Y */
/***********************************************/
_VS_DECLARE_FUNCTION (V_VaultCrypto_Open,(int mode));
_VS_DECLARE_FUNCTION (V_VaultCrypto_Close,(int handle));



struct vs_function_list
{
  vs_version_t version                                            ;
  VS_V_Initialize                   V_Initialize                  ;
  VS_V_Finalize                     V_Finalize                    ;
  VS_V_GetFunctionList              V_GetFunctionList             ;
  VS_V_SYM_CRYPT                    V_SYM_CRYPT                   ;
  VS_V_LoadKey                      V_LoadKey                     ;
  VS_V_GenerateKey                  V_GenerateKey                 ; 
  VS_V_FreeGENKEY                   V_FreeGENKEY                  ;
  VS_V_rsa_mod_exp                  V_rsa_mod_exp                 ;
  VS_V_rsa_mod_exp_crt              V_rsa_mod_exp_crt             ;
  VS_V_rsa_public_encrypt           V_rsa_public_encrypt          ;
  VS_V_rsa_public_decrypt           V_rsa_public_decrypt          ;
  VS_V_rsa_private_encrypt          V_rsa_private_encrypt         ;
  VS_V_rsa_private_decrypt          V_rsa_private_decrypt         ;
  VS_V_rsa_sign                     V_rsa_sign                    ;
  VS_V_rsa_verify                   V_rsa_verify                  ;
  VS_V_dsa_sign                     V_dsa_sign                    ;
  VS_V_dsa_verify                   V_dsa_verify                  ;
  VS_V_diffie_hellman_generate      V_diffie_hellman_generate     ;
  VS_V_diffie_hellman_shared_secret V_diffie_hellman_shared_secret;
  VS_V_SHA1_Init                    V_SHA1_Init                   ;
  VS_V_SHA1_Update                  V_SHA1_Update                 ;
  VS_V_SHA1_Final                   V_SHA1_Final                  ;
  VS_V_SHA1                         V_SHA1                        ;
  VS_V_SHA256_Init                  V_SHA256_Init                 ;
  VS_V_SHA256_Update                V_SHA256_Update               ;
  VS_V_SHA256_Final                 V_SHA256_Final                ;
  VS_V_SHA256                       V_SHA256                      ;
  VS_V_SHA_File                     V_SHA_File                    ;
  VS_V_GetRnd                       V_GetRnd                      ;
  VS_V_GetVaultVersion              V_GetVaultVersion             ;
  VS_V_pkaRSA                       V_pkaRSA                      ;
  VS_V_VaultCrypto_Open             V_VaultCrypto_Open            ;
  VS_V_VaultCrypto_Close            V_VaultCrypto_Close           ;
  VS_V_ecdh_compute_key             V_ecdh_compute_key            ;
  VS_V_wrap_cipher                  V_wrap_cipher                 ;
};


typedef struct vs_function_list   VS_FUNCTION_LIST;
typedef struct vs_function_list  *VS_FUNCTION_LIST_PTR;
typedef struct vs_function_list **VS_FUNCTION_LIST_PTR_PTR;



typedef vs_rv_t (*vs_createmutex_t) (void **mutex);
typedef vs_rv_t (*vs_destroymutex_t) (void *mutex);
typedef vs_rv_t (*vs_lockmutex_t) (void *mutex);
typedef vs_rv_t (*vs_unlockmutex_t) (void *mutex);


typedef struct vs_initialize_args
{
  vs_createmutex_t create_mutex;
  vs_destroymutex_t destroy_mutex;
  vs_lockmutex_t lock_mutex;
  vs_unlockmutex_t unlock_mutex;
  ulint flags;
  void *reserved;
}vs_initialize_args_t;

typedef struct vs_initialize_args  VS_INITIALIZE_ARGS;
typedef struct vs_initialize_args *VS_INITIALIZE_ARGS_PTR;


/*error codes*/
#define VSR_OK                            (0UL)
#define VSR_GENERAL_ERROR                 (5UL)
#define VSR_OBJECT_HANDLE_INVALID         (0x82UL)

#define VSR_SESSION_HANDLE_INVALID        (0xb3UL)

#define VSR_VFISEC_NOT_INITIALIZED        (0x190UL)
#define VSR_VFISEC_ALREADY_INITIALIZED    (0x191UL)
#define VSR_MUTEX_BAD                     (0x1a0UL)
#define VSR_MUTEX_NOT_LOCKED              (0x1a1UL)

#if defined(__cplusplus)
}
#endif

#endif /*vfintH*/

