/** @addtogroup utils Utils */
/** @{ */
/**
 *  @file svc.h
 *
 *  @brief Service interface private property API
 *
 *  Property API (private), public API's are in svcmgrSvcInf.h
 *
 */

/*
*  Copyright, 2015 VeriFone Inc.
*  2099 Gateway Place, Suite 600
*  San Jose, CA.  95110
*
* All Rights Reserved. No part of this software may be reproduced,
* transmitted, transcribed, stored in a retrieval system, or
* translated into any language or computer language, in any form
* or by any means electronic, mechanical, magnetic, optical,
* chemical, manual or otherwise, without the prior permission of
* VeriFone Inc.
*
*/
 
#ifndef _SVC_H_
#define _SVC_H_

#ifdef __cplusplus
#define VFI_NO_MINMAX
extern "C" {
#endif

#include "vficore.h"
 
#include "svcInfoAPI.h"
#include "svcNetwork.h"

/*
 * Keys for IPC resources
 *
 * IPC resource keys will start at 0x46410000
 * with semaphores from 0x46410000 to 0x46410FFF
 *      shared mem from 0x46411000 to 0x46411FFF
 *      msg queues from 0x46412000 to 0x46412FFF
 *
 * Services which use any resources should define them here.
 */

/**
* @name Semaphore Keys
*/
#define PUTENV_SEMAPHORE_KEY    0x46410000
#define SVCEVENT_SEM_KHTAB    	0x46410001
#define SVCEVENT_SEM_PID    	0x46410002
#define FE_SEMAPHORE_KEY        0x46410010

/**
* @name Shared memory keys
*
* @note
* The Event Service reserves 1E00 thru 1EFF
*
*/
#define INITABLE_SHM_KEY        0x46411000
#define SVCEVENT_SHM_KHTAB		0x46411E00 
#define SVCEVENT_SHM_BASEKEY    0x46411E01 


#define SVC_MAX_FILENAME_LEN 0x200 /*!< Maximum length of a filename including path */

#define MAX_SECTION_LEN 	0x20 /*!< Data size for Section:label = value */
#define MAX_LABEL_LEN  		0x20
#define MAX_VALUE_LEN  		0x200

#define RFS_VERSION_FILENAME  "/etc/rfs-version"

/**
* @name LED control defines
*/
#define LED_KEYPAD	(1<<1)
#define LED1		(1<<4)
#define LED2		(1<<5)
#define LED3		(1<<6)
#ifndef LED_SC
#define LED_SC     (1<<7)
#endif

/**
* @name Log flags
*/
#define LOGFILE_NOFLAGS    0x000000
#define LOGFILE_SECURITY   0x000001
#define LOGFILE_DATETIME   0x000002
#define LOGFILE_UNLIMITED  0x800000

	/* SVC DEFINES */
#define clrscr()		svcSystem("/usr/bin/clear")

#define MIN_SVCEXPAND_FREE_FLASH_SPACE   (250 * 1024)   /*!< 250KBytes */

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

#define MAX_SIGIO_TABLE_ENTRIES		10
#define MAX_SIGALRM_TABLE_ENTRIES	10

/**
* @name Compressed file extensions
*/
#define VFI_TAR					".tar"
#define VFI_TAR_GNU_ZIP			".tgz"
#define VFI_GNU_ZIP				".gz"
#define VFI_TAR_BIN_ZIP2		".bz2"
#define VFI_PACKAGE             ".ipk"

/**
* @name svcRunAPP variable argument definitions
*/
#define SVC_RUN_APP_KILLALL		0
#define SVC_RUN_APP_NO_KILLALL		1

/**
* @name Feature Enablement Token Definitions
*/
#define MAX_TOKEN_ISSUER_SIZE       64
#define MAX_TOKEN_CUSTOMER_SIZE     64
#define ENABLEMENT_VALID_SZ     18
#define ENABLEMENT_STARTEND_SZ      5
#define ENABLEMENT_TAG_SZ       5
#define ENABLEMENT_UNIT_DAYS    1
#define ENABLEMENT_UNIT_USES    2

/**
* @name Used with enablementTokenDetail()
*/
#define ENABLEMENT_ISSUER       "ISSUER"
#define ENABLEMENT_CUSTOMER     "CUSTOMER"
#define ENABLEMENT_CREATION     "CREATION"
#define TOKEN_DESCRIPTION       "DESCRIPTION"
#define TOKEN_VALID         "VALID"
#define TOKEN_START         "START"
#define TOKEN_END           "END"
#define TOKEN_COUNT         "COUNT"


/**
* @brief Define feature enablement token
*/
typedef struct {
  char major[ENABLEMENT_TAG_SZ];
  char minor[ENABLEMENT_TAG_SZ]; 
} enablementTokenTag;

/**
*
*/
struct vfiSigIOElement
{
	int fdesc;
	void (*pSigIOfunc) (void);
};

/**
*
*/
struct vfiSigIO
{
	int vfiSigIOInitFlag;
	struct vfiSigIOElement vfiSigIOEntries[MAX_SIGIO_TABLE_ENTRIES];
};

/**
*
*/
struct vfiSigAlarmElement
{
	int procId;
	void (*pSigAlarmfunc) (void);
};
 
/**
* 
*/
struct vfiSigAlarm
{
	int vfiSigAlarmInitFlag;
	struct vfiSigAlarmElement vfiSigAlarmEntries[MAX_SIGALRM_TABLE_ENTRIES];
};

/**
* @brief FTP parameter
*/
typedef struct {
	char	ftpHost[96];
	char	port[8];
	char	userID[32];
	char	password[32];
	char	localFile[96];
	char	remoteFile[96];
	char	errorMsg[128];
} ftp_parm_t;


/**
* @note
* Implementation for the following part of the API is moved to vficore package/library.
*/
/** \{ */

/**
* See vficore library functions _vficore_putEnvFile(section, label, value)
*/
int putEnvFile(char *section, char *label, char *value);

/**
* See vficore library functions _vficore_putEnvFilename(section, label, value, filename)
*/
int putEnvFilename(char *section, char *label, char *value, char* filename);

/**
* See vficore library functions _vficore_putEnvFileUsr(section, label, value, user, f_redirectSupport)
*/
int putEnvFileUsr(char *section, char *label, char *value, char *user, int f_redirectSupport);

/**
* See vficore library functions _vficore_getEnvFile(section, label, value, vlen)
*/
int getEnvFile(char *section, char *label, char *value, int vlen);

/**
* See vficore library functions _vficore_getEnvFilename(section, label, value, vlen, filename)
*/
int getEnvFilename(char *section, char *label, char *value, int vlen, char* filename);

/**
* See vficore library functions _vficore_getEnvFileUsr(section, label, value, vlen, user, f_redirectSupport)
*/
int getEnvFileUsr(char *section, char *label, char *value, int vlen, char *user, int f_redirectSupport);

/**
* See vficore library functions _vficore_delEnvFile(user, mode)
*/
int delEnvFile(char *user,int mode);

/**
* See vficore library functions _vficore_delEnvFile(user, mode)
*/
void *loadEnvFileUsr(char *user);
/** \} */

/**
* Sleep for a given time in milliseconds.
*
* @param[in] mstm Time in milliseconds.
*
*/
void svcWait(int mstm);

/**
* Gives microseconds elapsed since the Epoch (currently Jan 1, 1970)
*
* @return microseconds elapsed since the Epoch (currently Jan 1, 1970)
*
*/
unsigned long long svcGetSysMicrosec(void);

/**
* Gives milliseconds elapsed since the Epoch (currently Jan 1, 1970)
*
* @return milliseconds elapsed since the Epoch (currently Jan 1, 1970)
*
*/
unsigned long long svcGetSysMillisec(void);

/**
* Calculates CRC checksum.
*
* @param[in] type CRC type (CRC16LSB, CRC16MSB, CCITTLSB,
* 							CCITTMSB, CRC32LSB, CRC32LSB,
* 							CRC16MSB, CCITTMSB)
* @param[in] msg  Message to do CRC on.
* @param[in] size Message size.
*
* @return calculated CRC checksum given valid type, or 0 on unknown type.
*
*/
unsigned long svcCrcCalc(int type,unsigned char *msg,int size);

/**
* Reverse bits in long integer variable.
*
* @param[in] x Variable to be reversed.
*
* @return reversed long integer
*
*/
unsigned long bitreverse32(unsigned long x);

/**
* Reverse bits in short integer variable.
*
* @param[in] x Variable to be reversed.
*
* @return reversed short integer
*
*/
unsigned short bitreverse16(unsigned short x);

/**
* Reverse bits in char variable.
*
* @param[in] x Variable to be reversed.
*
* @return reversed char variable
*
*/
unsigned char bitreverse8(unsigned char x);

/**
* Read key data from file descriptor.
*
* @param[in] fd File descriptor from where to read.
* @param[out] buff Output buffer.
* @param[in] min Minimum number of characters for noncanonical read.
* @param[in] max Maximum number of bytes to read from file descriptor.
* @param[in] ms Timeout in milliseconds for noncanonical read.
* @param[in] clrFlagsMask Termios c_lflag clear mask.
* @param[in] setFlagsMask Termios c_lflag set mask.
*
* @return on success returns bytes read from descriptor, on failure -1 (check
* errno for error details)
*/
int svcKeys(int fd,char *buff,int min,int max,int ms,int clrFlagsMask,int setFlagsMask);

/**
* Read single character from stdio.
*/
int svcKeyRd(void);

/**
* Read single character from stdio.
*/
int svcKeyRead(void);

/**
* Wait for key from stdin.
*
* @param[in] msec Milliseconds to wait for input.
*/
int svcKeyWait(unsigned int msec);

/**
* Read text string from stdio.
*
* @param[out] dest Destination buffer.
* @param[in] type unused
* @param[in] max Max keys to read.
* @param[in] min Min keys to be read.
*
* @return number of bytes read (see svcKeys() return values for details)
*/
int svcKeyTxt(char *dest,int type,int max,int min);

/**
* Convert string to hex.
*/
void svcDsp2Hex(char *, long *, unsigned int);

/**
* Turn on one or more LEDs
*
* @param[in] mask Mask which leds to turn on.
*/
void ledOn(int mask);

/**
* Turn off one or more LEDs
*
* @param[in] mask Mask wich leds to turn off.
*/
void ledOff(int mask);

/**
* Enable/Disable MSR runway LEDs
*
* @param[in] enable Weather to enable or disable the leds.
*/
void ledRunway( int enable );

/**
* Mute/unmute speaker.
*
* @param[in] mode Mute/enable speaker (0 - mute, 1 - enable)
*/
void speaker(int mode);

/**
* Copy Linux clock to RTC.
*/
void setRTC(void);

/**
* Not implemented.
*/
void getRTC(void);

/**
* Set display brightness.
*
* @param[in] direction Wheather to make ir brighter(1) or darker(0)
*
* @note:
* Click Brightness 1 level up=1 or down=0
* NEW!!! - If direction = -1 then return current brightness value/2
*
* @note:
* Legacy Mx scale was 0-31, Trident is 0-63
*/
int dspSetBrightness(short direction);

/**
* Sound control
*
* @param[in] vol Sound volume
* @param[in] bass Bass level
* @param[in] treble Treble level
*
* @return 0 if ok, any other value on failure
*/
int soundCtrl(int vol, int bass, int treble);

/**
* Get ALSA master volume value
*
* @param[out] val Volume as integer value
*
* @return 0 on success, other value on failure.
*/
int GetAlsaMasterVolume(long *val);

/**
* Set alarm callback for pid
*
* @param[in] pid Pid to monitor.
* @param[in] rxfp Callback function
*
* @return 0 on success, -1 in case of failure
*/
int svcSetAlarmCallback(int pid, void (*rxfp)(void));

/**
* Release callbacks for pid.
*
* @param[in] pid Pid that is freed of alarm callbacks.
*/
short svcReleaseAlarmCallback(int pid);


/**
* Signal IO handler
*
* @param[in] inSigNo Signal number
* @param[in] inSigInfo Extra information for signal
* @param[in] thirdArg Unused
*/
void svcSigIOhandler(int inSigNo, siginfo_t *inSigInfo, void *thirdArg);

/**
* Signal alarm handler
*
* @param[in] inSigNo Signal number
* @param[in] inSigInfo Extra information for signal
* @param[in] thirdArg Unused
*/
void svcSigAlarmhandler(int inSigNo, siginfo_t *inSigInfo, void *thirdArg);


/**
* Kill current process after given time and signal with SIGALRM
*
* @param[in] secs Second to wait before sending SIGALRM signal.
*/
void svcAlarm(unsigned int secs);

/**
* Undefined behaviour.
*/
void IOMod(int subCmd, int parm1, char *fileName);

/**
* Undefined behaviour.
*/
long getFreeFlashSpace( void );

/**
* Undefined behaviour.
*/
long getPackageContentSize( char *packageName );

/**
* Get tarball content size
*
* @param[in] tarballName Name of the file
*
* @return size of the given tarball in bytes
*/
long getTarballContentSize( char *tarballName );

/**
* Extract files in use accounts home directory.
*
* @param[in] inFile Pointer to file name to expand.
* @param[in] keepinFile Reserved. Must pass 0 in this parameter.
* @param[in] uowner Reserved. Must pass null in this parameter.
*
* @return 0 on success (file expanded without error), -1 error
* with errno set to one of the following:
* 	EINVAL - Invalid parameter
* 	EPERM - Permission error
* 	ENOSPC - No space to extract the file
*/
int svcExpand(char *inFile, int keepinFile, char *uowner);

/**
* Undefined behaviour.
*/
int svcExpandWorkDir(char *inFile, int keepinFile, char *uowner, char *workingDirectory);

/**
* Undefined behaviour.
*/
int svcExpandWorkDirExtended(char *inF, int keepinFile, char *uowner, char *workingDirectory, int authAllowed);

/**
* Install files found in secure installer download directory
* /mnt/flash/install/dl.
*
* @note
* This call replaces svcExpand and loadOSFiles
* retMsg will return a string that describes an error code (if the call fails).
* A null string is returned on success length should be set to the size of
* retMsg and will limit the return string so there is no overflow.
*
* @return 0 - Success no reboot needed, 1 = Success reboot needed,
* 	< 0 (negative) secure installer error code.
*/
int svcSecureInstall(char *retMsg, int length);

/**
* Causes the Secure Installer to install all files found in secure installer
* download directory, /mnt/flash/install/dl.
*
* @param[out] retMsg In case of error, contains error string.
* @param[in] length Size of retMsg
* @param[out] outFlags
* @pramm[in] inFlags
*
* @return 0 on success, < 0 on failure (retMsg contains the error string)
* 
*/
int svcSecureInstallExtended(char *retMsg, int length,int *outFlags,int inFlags);

/**
* Disable svc install.
*
* @param[in] value Value to assign to disableInstall
*
*/
void svcDisableInstall(int value);    

/**
* Returns the state of the reboot required flag.
*
* @return 0 - no reboot required, any other value than 0 - reboot required
*/
int isRebootRequired(void);

/**
* Returns the state of the application reboot required flag.
*
* @return 0 - no restart required, any other value than 0 - reboot required
*/
int isAppRestartRequired(void);	

/**
* Runs the application located in /home/usr1 of the filename referenced by
* *file.
*
* @param[in] file Filename to run.
* @param[in] search4exe Must be set to 0.
*/
void svcRunApp(char *file, int search4Exe);

/**
* Undefined behaviour.
*/
int setPassword(char *reqUsr, char *pwd);

/**
* Undefined behaviour.
*/
int svcPassword(char *pwd);

/**
* Undefined behaviour.
*/
int readEEprom(unsigned char key, char *buf);

/**
* Undefined behaviour.
*/
int writeEEprom(unsigned char key, char *buf, unsigned char len);

/**
* Undefined behaviour.
*/
int svcIsRunning(char *psStr, char *inpids);

/**
* Reboot the terminal
*/
void svcRestart(void);

/**
* Undefined behaviour.
*/
int svcRun(char *fn,int mode);

/**
* Set Linux RTC
*
* @param[in] dateTime New date & time according to format - MMDDhhmmYYYY.ss
*
* @return 0 on success, < 0 on error
*/
int setDateTime(char *dateTime);

/**
* Undefined behaviour.
*/
int lockRoot(void);

/**
* Undefined behaviour.
*/
int colorizeSysmode(void);

/**
* Performs a touch panel calibration.
*
* @return 0 on success, -1 on error
*/
int touchCompNSave(void);

/**
* Undefined behaviour.
*/
int touchSigCap(void *data,long size);

/**
* Undefined behaviour.
*/
int getTarInfo(char *tarRes, char *permFile);

/**
* Sets and retrieves touch panel functionality.
*/
int touchCmd(int cmd, int value);

/**
* Determines how many USB storage/memory devices are currently plugged into
* the USB host port, mounted, and ready for access.
*
* @param[out] usbStor Pointer to char of at least 4 allocated bytes.
* If devices ready, 1 is set to according device byte in
* array.
*
* @return 0 on success.
*/
int svcUsbStorPresent(char *usbStor);

/**
* Transfers the file from the FTP server to the terminal.
*
* @param[in] ftp Pointer to the FTP parameter structure, with required request
* parameters.
*
* @return 0 on success, < 0 on error.
*/
int ftpGet(ftp_parm_t *ftp);

/**
* Transfers a file from terminal to the FTP server.
*
* @param[in] ftp Pointer to the FTP parameter structure, with required request
* parameters.
*
* @return 0 on success, < 0 on error.
*/
int ftpPut(ftp_parm_t *ftp);

/**
* Transfer a file from the SFTP host
*
* @param[in] ftp Pointer to the FTP parameter structure, with required request
* parameters.
*
* @return 0 on success, < 0 on error.
*/
int scpGet(ftp_parm_t *ftp);

/**
* Transfer a file to the SFTP host
*
* @param[in] ftp Pointer to the FTP parameter structure, with required request
* parameters.
*
* @return 0 on success, < 0 on error.
*/
int scpPut(ftp_parm_t *ftp);

/**
* Undefined behaviour.
*/
int processConfig(char *fn, char *usr);

/**
* Undefined behaviour.
*/
int processConfigExtended(char *fn, char *usr, int mode, int authAllowed);

/**
* Undefined behaviour.
*/
void enableProcessMonitor(void);

/**
* Undefined behaviour.
*/
void disableProcessMonitor(void);

/**
* Undefined behaviour.
*/
void clrProcessMonitor(void);

/**
* Undefined behaviour.
*/
void enableButtonSig(void);

/**
* Undefined behaviour.
*/
void disableButtonSig(void);

/**
* Undefined behaviour.
*/
int putSysctl(char *varSysctl);

/**
* Undefined behaviour.
*/
int getSysctl(char *varSysctl, char *valSysctl, int len);

/**
* Undefined behaviour.
*/
int rtc3610Read(int opt, char *buf);

/**
* Undefined behaviour.
*/
int rtc3610Write(int opt, char *buf);

/**
* Undefined behaviour.
*/
int keypadMode(int mode);

/**
* Runs the application located in /home/usr1
*
* @param[in] file File name of the application to run.
* @param[in] search4Exe Parameter has no effect, must be set to 0
*/
void svcCustomRunApp(char *file, int search4Exe, ...);

/**
* Allows an application to start another application across user accounts.
*
* @param[in] user Must point to a string containing usr1-usr16
* @param[in] file Must point to a string containing the file name of the
* executable.
*
* @return 0 on success, < 0 on error.
*/
int svcRunUsr(char *usr,char *file);

/**
* Executes "system" command and returns the result.
*
* @param[in] command The command to execute.
*
* @note
* Unsafe call, avoid to use.
*
* @return return status from the executed command.
*/
int svcSystem(char *command);

/**
* Undefined behaviour.
*/
int safeSystem(char *command, int scan);

/**
* Undefined behaviour.
*/
int svcPackage( char *command, char *fileName );

/**
* Return the status of the following keys:
* 	Master Session
* 	DUKPT
* 	RKL
* 	User
*
* @param[in] keyStat Pointer to an array of integers
* @param[in] len  Number of integers in the array
*
* @note
* A maximum of 23 integers are returned.
*
* @return call status
*/
int getKeyStatus(int *keyStat,int len);

/**
* Undefined behaviour.
*/
void generalLog(int pri, const char *tag, const char *format, ...);

/**
* Undefined behaviour.
*/
void secureLog(int pri, const char *format, ...);

/**
* Get status of specific token.
*
* @param[in] tokenTag Token to get status for.
*
* @return number of days until token expires
*/
int enablementTokenStatus(enablementTokenTag *tokenTag);

/**
* Enable an on demand token and return it's status.
*
* @param[in] tokenTag Token to get status for.
*
* @return number of days until token expires
*/
int enablementTokenDemandStatus(enablementTokenTag *tokenTag);

/**
* Get count of enablement tokens.
*
* @return count of enablement tokens
*/
int enablementTokenCount(void);

/**
* Get enablement token index
*
* @param[in] index index associated with the token
* @param[in] tokenTag Token to get index.
*
* @return token structure associated with the given index
*/
int enablementTokenIndex(int index, enablementTokenTag *tokenTag);

/**
* Get value associated with the specified token.
*
* @param[in] tokenTag Token to get details for.
* @param[in] name Key name
* @param[out] value valur returned from the key
* @param[in] length lengh of the returned value
*
* @return 0 on success, -1 on error
*/
int enablementTokenDetail(enablementTokenTag *tokenTag, char *name, char *value, int length);
// new Enablement API to get status, including unit (for DAYS and USES)

/**
* Get token expiration.
*
* @param[in] tokenTag Token to get details for.
* @param[in] pUnit Unit in which to return expiraton
*
* @return token expiration value
*/
int enablementTokenExpiration(enablementTokenTag *tokenTag, int *pUnit);


	// Derived Functions
#define MSEL0_set_dir()					IOMod(1,0, NULL)
#define MSEL0_set_bit(sense)			IOMod(2,sense, NULL)
#define MSEL3_set_dir()					IOMod(3,0, NULL)
#define MSEL3_set_bit(sense)			IOMod(4,sense, NULL)
#define nRESET_MOD_set_bit(sense)		IOMod(5,sense, NULL)
#define usbDeviceCtrl(state)			IOMod(100,state, NULL)

	// Defined Functions for RF Contactless Reader
#define RFCR_ISP_SEL_cfgpin()			MSEL0_set_dir()
#define RFCR_ISP_SEL_bit(sense)			MSEL0_set_bit(sense)
#define RFCR_RST_cfgpin()				MSEL3_set_dir()
#define RFCR_RST_bit(sense)				MSEL3_set_bit(sense)
#define RFCR_Power_bit(sense)			nRESET_MOD_set_bit(sense)

	// Defined Functions for Signature Capture
#ifndef SigCap
#define SigCap(data,size)               touchSigCap(data,size)
#endif

#define SigCapBegin(size)   		SigCap(0,size)
#define SigCapEnd()      		SigCap(0,0)
#define SigCapCount()    		SigCap(0,-1)
#define SigCapResume()                  SigCap(0,-2)
#define SigCapSuspend()			SigCap(0,-3)
#define SigCapGet(data,size)        	SigCap(data,size)
#define palmReject(mode)		touchCmd(11,mode)
#define palmRejectState()		touchCmd(12,0)
#define SetNormalSpeed(pps)		touchCmd(14,pps)
#define SetSigCapSpeed(pps)		touchCmd(16,pps)

#ifndef VFI_NO_MINMAX
//***************************************************************************
// Macros lifted from kernel.h
/**
 * min()/max() macros that also do
 * strict type-checking.. See the
 * "unnecessary" pointer comparison.
 */
#define min(x,y) ({ \
	typeof(x) _x = (x);	\
	typeof(y) _y = (y);	\
	(void) (&_x == &_y);		\
	_x < _y ? _x : _y; })

#define max(x,y) ({ \
	typeof(x) _x = (x);	\
	typeof(y) _y = (y);	\
	(void) (&_x == &_y);		\
	_x > _y ? _x : _y; })

#endif /* VFI_NO_MINMAX -- don't define min/max if compiling for C++ */

/**
 * ..and if you can't take the strict
 * types, you can specify one yourself.
 *
 * Or not use min/max at all, of course.
 */
#define min_t(type,x,y) \
	({ type __x = (x); type __y = (y); __x < __y ? __x: __y; })
#define max_t(type,x,y) \
	({ type __x = (x); type __y = (y); __x > __y ? __x: __y; })

#ifdef __cplusplus
}
#endif
#endif // svc.h
