/* IPP legacy library */
#ifndef _IPPLEG_
#define _IPPLEG_

#ifdef __cplusplus
extern "C" {
#endif

/* Function prototype */
short get_key_mgmnt(char *kmm, char *demf);
short select_key_mgmnt(unsigned char kmm, unsigned char demf);
short ipp_diag(short test_type, char *result, short master_key);
short ipp_abort(void);
short ipp_mac(char mk, char *wk, char sk, char *message, unsigned short len, char *result);
short ipp_read(char *buffer, unsigned short size);
short ipp_getpin(short key_type, char dsp_line, char min_pin_len, char max_pin_len, char zero_pin_ok, short max_time, char *pan, char *working_key, short master_key);
int _getKeyStatus(int *keyStatus, int length);
int getKSN(int index, char *KSN);
int MSKeyStatus(int keyIndex, char *details);
#ifdef __cplusplus
}
#endif
#endif // ippleg.h
