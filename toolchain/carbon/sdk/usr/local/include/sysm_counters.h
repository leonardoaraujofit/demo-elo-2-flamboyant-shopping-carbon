#ifndef __SYSM_COUNTERS__
#define __SYSM_COUNTERS__

#include <libintl.h>

#define gettext_noop(String) String

typedef struct
{
    char * name;
    char * value;
} t_cntr;


typedef struct 
{
    char    *  name;
    t_cntr  ** cntrs;
    unsigned int len;
} t_cntr_group;

void free_groups(t_cntr_group ** groups, unsigned int len);
t_cntr_group **  get_counters_info(unsigned int * len);

#endif // __SYSM_COUNTERS__
