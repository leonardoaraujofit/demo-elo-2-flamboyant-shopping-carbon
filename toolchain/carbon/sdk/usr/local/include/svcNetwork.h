/*
 *	Copyright, 2008 VeriFone Inc.
 *	2099 Gateway Place, Suite 600
 *	San Jose, CA.  95110
 *
 *  All Rights Reserved. No part of this software may be reproduced,
 *  transmitted, transcribed, stored in a retrieval system, or
 *  translated into any language or computer language, in any form
 *  or by any means electronic, mechanical, magnetic, optical,
 *  chemical, manual or otherwise, without the prior permission of
 *  VeriFone Inc.
 */
 
/** @addtogroup vfisvc Services */
/** @{ */
/** @addtogroup svcnetwork Services : Networking */
/** @{ */
/** 
 *  @file svcNetwork.h 
 *
 *	@brief Networking related services
 *
 * 
 */




#ifndef _SVCNETWORK_H_
#define _SVCNETWORK_H_

#ifdef __cplusplus
extern "C" {
#endif



/**
 Network interface information
*/

typedef struct
{
	short	dhcp; /**< 1 - uses DHCP; other - does not uses DHCP */
	char	MAC[18]; /**< MAC address of the interface */
	char	ipaddr[16]; /**< Current interface IP address (IPv4) */
	char	netmask[16]; /** Netmask */
	char	gateway[16]; /** Gateway IP address */
	char	dns1[16]; /** Primary DNS address */
	char	dns2[16]; /** Secondary DNS address */
} net_conf_t;

/**
 * @name Type of interface
*/
/** \{ */
typedef enum {
	NET_INTERFACE_DISABLED, /**< Interface is disabled */
	NET_INTERFACE_ETHERNET /**< Ethernet interface  */
} net_interface_t;
/** \} */


/**
 * @name Network interface for which data must be returned
*/
/** \{ */
typedef enum {
	NGI_CURRENT = 0, /**< Current interface */
	NGI_REQUESTED = 1, /**< Requested (via config.usr1) */
	NGI_ACTIVE = 2 /**< Active interface */
} netGetInterface_t;
/** \} */


/* Part of libvfisvc.so */

/** 
 * Return network link status
 *
 * @return <0 = Error, 1 = Link up, 0 = Link or interface down
 *
 */
int netLinkStatus(void);

/** 
 * Mark the eth0 interface down
 *
 * @return <0 = Error, 0 = Success
 *
 */
int netDown(void);

/** 
 * Mark the eth0 interface up
 *
 * @return <0 = Error, 0 = Success
 *
 */
int netUp(void);

/** 
 * Ping the specified host
 *
 * @param[in] host Either string containing IP address or fully qualified domain name (ASCII)
 *
 * @return <0 = Error, 0 = Success
 *
 */
int netPing(char *host);

/** 
 * Return current network configuration
 *
 * @param[out] pNetConf Structure into which network configuration will be returned
 *
 * @return <0 = Error, 0 = Success
 *
 */
int netGetConfig(net_conf_t *net);

/** 
 * Return the enum value representing network interface
 *
 * @param[in] whichOne Indicates current, requested (via config.usr1) or active interface.
 *
 * @return Requested network interface
 *
 */
net_interface_t netGetInterface( netGetInterface_t whichOne );

/** 
 * Set ntp protocol to hostname
 *
 * @param[in] server Host name
 *
 * @return <0 = Error, 0 = Success
 *
 */
 int ntp(char *server);


// The netUp(...) macro below is to assist with application compatibility
//    between different opsys versions.
//
// May be called with no parameter or with a single parameter.  Calls
//    netUp(void) function prototyped above.
//
// The macro works as follows:
// First ARM register r0 (_arg_) is set to the value of the input argument
// or to 0 if no input argument. Then a separate (as far as the COMPILER is
// concerned) register variable _ret_ is declared for the result. Next the
// original netUp() function is called, whose result (as with all function
// calls) appears in r0.
//
// The code after the colons in the __asm__ volatile statement tells the
// ASSEMBLER that we are using the same register for input and output which
// helps with optimization.
//
// The result in r0 still needs to be returned to the caller of the macro;
// this is done by moving the contents of _arg_ (ie. r0) to _ret_ and then
// returning the value of _ret_.  Both steps are necessary to cause the compiler
// generate the corect code.  The compiler knows that the macro result has to
// be returned in r0 and recognizes that the result is ALREADY in r0 hence it
// can optimize as much as possible.  Please note that just about any attempt
// to simplify the final line of the macro code results in an omptimzation
// error -- I have tried.
//
#define netUp(...) ({ \
	register int _arg_ asm("r0") = __VA_ARGS__+0, _ret_; \
	__asm__ volatile ("bl netUp" : "=r" (_arg_) : "0" (_arg_)); \
	_ret_ = _arg_, _ret_; \
})


#ifdef __cplusplus
}
#endif
#endif // svcNetwork.h


/// @} */
/// @} */

