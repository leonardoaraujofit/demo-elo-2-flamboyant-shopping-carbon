#ifndef SYSM_MANUFACTURING_H_
#define SYSM_MANUFACTURING_H_

#include "sysm_util.h"
#include <sysmode_ui/sysm_ui.h>
#include "sysm_login.h"
#include "sysm_menu.h"
#include <sysmode_ui/sysm_ui.h>

#ifdef LIBINTL
    #include <libintl.h>
#else
    #define gettext(a)  (a)
#endif

#define CORRECT_MODE 1

#define ERROR_MANUF_FILE_OPEN	1
#define ERROR_MANUF_FILE_WRITE	2

extern "C" {
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <pthread.h>
#include <vficrypt.h>
#include <svcmgr/svc_security.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
}



void man_to_prod_cb(const SYSM_UI::t_ui_menu_entry *e);

#endif /* SYSM_MANUFACTURING_H_ */
