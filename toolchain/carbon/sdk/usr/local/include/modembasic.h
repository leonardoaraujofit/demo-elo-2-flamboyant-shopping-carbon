/*------------------------------------------------------------------------
 * Basic modem helper library API (libvfimodembasic)
 *
 *
 * Created on : 07/2015
 * Author     : Jean-Luc Vermote <jean_v2@verifone.com>
 *
 *-----------------------------------------------------------------------*/

#ifndef _LIBMODEMBASIC_H_
#define _LIBMODEMBASIC_H_

/* 
 * Function result codes 
 */
#define MODEMBASIC_OK                    0  // Operation successfull
#define MODEMBASIC_FAILED               -1  // Operation failed (system error, generic error, ...)
#define MODEMBASIC_INVAL                -2  // Invalid parameter
#define MODEMBASIC_COUNTRY_FAILED       -3  // Country setup failed
#define MODEMBASIC_SDLC_FAILED          -4  // SDLC failure
#define MODEMBASIC_NOTEXIST             -5  // Modem not found 


#ifdef __cplusplus
extern "C" {
#endif

/*
 *  Modem type available on the current platform
 */
enum modemBasicType {
    MODEMBASICTYPE_INTERNAL      =   (1 << 0),   /* Internal modem */
    MODEMBASICTYPE_BASE_USB      =   (1 << 1),   /* Modem in VFI USB base */
    MODEMBASICTYPE_BT            =   (1 << 2),   /* Modem available through Bluetooth, including VFI BT base */
    MODEMBASICTYPE_EXTERNAL_USB  =   (1 << 3),   /* External USB Modem, excluding VFI USB base */
};


/*
 *  Modem Hardware/chip model
 */
enum modemBasicHardware { 
    MODEMBASICHW_UNKNOWN = 0,            /* Unknown */
    MODEMBASICHW_CX930XX,                /* Conexant CX930xx family */
    MODEMBASICHW_UX300_INSYS_PSTN,       /* UX300 PSTN */
    MODEMBASICHW_UX300_INSYS_ISDN,       /* UX300 ISDN */
};


/*
 *  Modem information structure
 */
struct ModemBasicInfo {
    char                        path[256];              /* modem device path */
    enum modemBasicHardware     hardware;               /* Modem chip model */
    char                        firmwareVersion[32];    /* firmware version */
    char                        hardwareRevision[32];   /* modem hw revision */
};

/*------------------------------------------------------------------------ 
 *                          Library API
 *------------------------------------------------------------------------
 */

/*------------------------------------------------------------------------
 *                  modembasic_activateSDLC
 *
 * Start SDLC protocol. 
 * The modem must be connected in synchronous/V80 mode.
 *
 * params:
 *  in : fd          File descriptor on the modem TTY
 *  in : address     The local SDLC address
 *
 * return:
 *  MODEMBASIC_OK           : Success
 *  MODEMBASIC_INVAL        : Invalid file descriptor
 *  MODEMBASIC_SDLC_FAILED  : Unable to start SDLC, or SDLC not supported 
 *  MODEMBASIC_FAILED       : Unexpected system error
 *
 *------------------------------------------------------------------------
 */ 
int modembasic_activateSDLC(int fd, unsigned int address);


/*------------------------------------------------------------------------
 *                  modembasic_deactivateSDLC
 *
 * Stop SDLC protocol.
 *
 * params:
 *  in :    fd      File descriptor on the modem TTY
 *
 * return:
 *  MODEMBASIC_OK           : Success
 *  MODEMBASIC_INVAL        : Invalid file descriptor
 *  MODEMBASIC_SDLC_FAILED  : Unable to stop SDLC, or SDLC not supported 
 *  MODEMBASIC_FAILED       : Unexpected system error
 *
 *------------------------------------------------------------------------
 */ 
int modembasic_deactivateSDLC(int fd);


/*------------------------------------------------------------------------
 *                  modembasic_hwReset
 *
 * Performs a hardware reset on the internal modem. 
 *
 * params:
 *  none
 *
 * return:
 *  MODEMBASIC_OK       Success
 *  MODEMBASIC_FAILED   Reset failed
 *
 *------------------------------------------------------------------------
 */ 
int modembasic_hwReset(void);


/*------------------------------------------------------------------------
 *                  modembasic_getType
 *
 * Returns all the currently available modems as a bitwise OR of values from 
 * enum modemBasicType.
 *
 * params:
 *  none
 *
 * return:
 *  a bitwise OR of values from enum modemBasicType. 
 *  0 if no modem available.
 *
 *------------------------------------------------------------------------
 */ 
unsigned int modembasic_getType(void);


/*------------------------------------------------------------------------
 *                  modembasic_init
 *
 * Performs these operations:
 *  - open and setup the modem TTY
 *  - setup modem country settings. 
 *    If the country is not defined in system configuration then 
 *    the Universal/Europe CTR-21 settings are used.
 *  - fill the ModemBasicInfo structure given as parameter
 *  - return a file desriptor on the modem TTY. The caller is in charge of 
 *    managing the life of this descriptor e.g. closing, etc ...   
 *
 * If the modem chip is unknown then the file descriptor on the TTY and
 * the TTY device path are returned but the country settings are not set.
 *
 * params:
 *  in : type        the modem type to initialize
 *  out: outInfo     the ModemBasicInfo structure to be filled during init
 *
 * return:
 *  >=0                       : Init successfull.The value is a valid descriptor 
 *                              on the modem TTY. It is up to the caller to 
 *                              manage the life of this descriptor.
 *  MODEMBASIC_INVAL          : Invalid parameter.
 *  MODEMBASIC_NOTEXIST       : Requested modem type is not available or
 *                              has been removed.
 *  MODEMBASIC_COUNTRY_FAILED : Country setup failed.
 *  MODEMBASIC_FAILED         : Init failed e.g. command or system errors, ...
 *
 *------------------------------------------------------------------------
 */ 
int modembasic_init(enum modemBasicType type, struct ModemBasicInfo *outInfo);


/*------------------------------------------------------------------------
 *                  modembasic_hangup
 *
 * Disconnect the modem using DTR
 *
 * params:
 *  in : fd     File descriptor on the modem TTY
 *
 * return:
 *  MODEMBASIC_OK       Success
 *  MODEMBASIC_FAILED   Failed e.g. system error, bad descriptor, ...
 *
 *------------------------------------------------------------------------
 */ 
int modembasic_hangup(int fd);


#ifdef __cplusplus
}
#endif

#endif
