// Verifone includes
#include <svc.h>

// C++ includes
#include <string>
#include <thread>
#include <chrono>

// linux/posix includes
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <syslog.h>
#include <errno.h>

// ADK includes
#include "msr/msr.h"
#include "html/gui.h"

// rapidjson includes
#include "document.h"
#include "writer.h"
#include "stringbuffer.h"

// { "success": true, "name": "LUCAS MONARI", "cardPan": "0800777755558888", serialNumer: "123-456-789" }
std::string GetJsonMessage(const bool success = false,
                           const std::string& cardholder_name = "",
                           const std::string& pan = "",
                           const std::string& expire_date = "") {
  syslog(LOG_DEBUG, "[GetJsonMessage] success: %d", success);
  syslog(LOG_DEBUG, "[GetJsonMessage] cardholder_name: %s",
         cardholder_name.c_str());
  syslog(LOG_DEBUG, "[GetJsonMessage] pan: %s", pan.c_str());

  // Getting POS's serial number
  std::string serialNumber(20, '\0');
  svcInfoSerialNum(&serialNumber[0]);
  serialNumber.resize(::strlen(serialNumber.c_str()));

  // Building the JSON
  rapidjson::Document doc;
  doc.SetObject();
  doc.AddMember("success", success, doc.GetAllocator());
  doc.AddMember("name", cardholder_name, doc.GetAllocator());
  doc.AddMember("cardPan", pan, doc.GetAllocator());
  doc.AddMember("serialNumber", serialNumber, doc.GetAllocator());
  doc.AddMember("expireDate", expire_date, doc.GetAllocator());

  rapidjson::StringBuffer buffer;
  rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
  doc.Accept(writer);

  syslog(LOG_DEBUG, "[GetJsonMessage] JSON built: %s",
         buffer.GetString());
  return buffer.GetString();
}

void SendMessageToTablet(const std::string& json) {
  const int kPort = 7777;
  const std::string kTabletIP = "192.168.50.1";
  struct sockaddr_in serv_addr = { };
  int sockfd = socket(AF_INET, SOCK_STREAM, 0);

  syslog(LOG_DEBUG, "[SendMessageToTablet] Trying to send message to Tablet"
                    " in address: %s:%d", kTabletIP.c_str(),
                    kPort);

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(kPort);
  serv_addr.sin_addr.s_addr = inet_addr(kTabletIP.c_str());

  if (!connect(sockfd, reinterpret_cast<struct sockaddr *>(&serv_addr),
              sizeof(serv_addr))) {
    syslog(LOG_DEBUG, "[SendMessageToTablet] Socket connected!");
    ssize_t bytes_sent = send(sockfd, json.c_str(), json.size(), 0);
    syslog(LOG_DEBUG, "[SendMessageToTablet] Bytes sent [json]: %d!", bytes_sent);

    if (bytes_sent > 0) {
      bytes_sent = send(sockfd, "\n", 1, 0);
      syslog(LOG_DEBUG, "[SendMessageToTablet] Bytes sent [\\n]: %d!", bytes_sent);
    }

    close(sockfd);
    syslog(LOG_DEBUG, "[SendMessageToTablet] Socket closed!");
  } else {
    syslog(LOG_ERR, "[SendMessageToTablet] Error on connecting socket: %s",
           strerror(errno));
  }
}

typedef enum {
  LED_NONE = 0,
  LED_1 = 1,
  LED_1_2 = 2,
  LED_1_2_3 = 3,
  LEDS_COUNT = 4,
} LEDS;

void HandleLeds(const LEDS leds) {
  switch (leds) {
    case LED_1:
      MSR_SwitchLeds(MSR_LED_OFF, MSR_LED_WHITE, MSR_LED_OFF, 0);
      break;
    case LED_1_2:
      MSR_SwitchLeds(MSR_LED_WHITE, MSR_LED_WHITE, MSR_LED_OFF, 0);
      break;
    case LED_1_2_3:
      MSR_SwitchLeds(MSR_LED_WHITE, MSR_LED_WHITE, MSR_LED_WHITE, 0);
      break;
    default:
      MSR_SwitchLeds(MSR_LED_OFF, MSR_LED_OFF, MSR_LED_OFF, 0);
  }
}

int main(int argc, char* argv[]) {
  syslog(LOG_DEBUG, "[main] Application initialized!");

  std::thread thread_leds([]() {
    syslog(LOG_DEBUG, "[thread_leds] Handling leds...");
    LEDS leds = LED_NONE;
    const int kTimeBetweenLedChanges = 300;

    while (true) {
      leds = static_cast<LEDS>((leds + 1) % LEDS_COUNT);
      HandleLeds(leds);
      std::this_thread::sleep_for(std::chrono::milliseconds(kTimeBetweenLedChanges));
    }
  });

  const int INFINITE = -1;
  vfigui::uiInvokeURL("index.html");

  while (true) {
    syslog(LOG_DEBUG, "[main] Trying to activate magstripe reader...");
    int return_code = MSR_Activate(NULL, NULL);
    if (return_code == MSR_OK) {
      syslog(LOG_DEBUG, "[main] Magstripe reader successfully activated!");
      MSR_TrackData tracks = { };
      MSR_DecodedData data = { };
      bool valid = true;
        std::string json;

      syslog(LOG_DEBUG, "[main] Waiting for card...");
      return_code = MSR_GetData(INFINITE, &tracks, &data);
      if (return_code == MSR_OK) {
        syslog(LOG_DEBUG, "[main] Card data done...");
        if (data.t1.valid) {
          syslog(LOG_DEBUG, "[main] Track1 is valid and it'll be used!");
          json = GetJsonMessage(true, data.t1.name, data.t1.pan, data.t1.exp_date);
        } else if (data.t2.valid) {
          syslog(LOG_DEBUG, "[main] Track2 is valid and it'll be used!");
          json = GetJsonMessage(true, "", data.t2.pan);
        } else {
          syslog(LOG_DEBUG, "[main] Neither Track1 nor Track2 is valid!");
          valid = false;
        }
      } else {
        syslog(LOG_ERR, "[main] Error on reading magstripe data: %d!", return_code);
      }

      MSR_Deactivate();
      syslog(LOG_DEBUG, "[main] Magstripe reader deactivated!");

      if (!valid) {
        syslog(LOG_DEBUG, "[main] Generating message to report error on reading card data!");
        json = GetJsonMessage();
      }

      SendMessageToTablet(json);
    } else {
      syslog(LOG_ERR, "[main] Error on activating magstripe reader: %d!", return_code);
    }
  }

  return 0;
}
