package br.com.fit.capturacadastro.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.support.annotation.NonNull;

import java.util.Date;

@Entity(tableName = "visits",
        foreignKeys =
            @ForeignKey(entity = CardHolderData.class,
                    parentColumns = "id", childColumns = "customer_id"),
        primaryKeys = { "customer_id", "timestamp" })
public class CardHolderVisit {

    @ColumnInfo(name = "customer_id")
    private int customerId;

    @NonNull
    private Date timestamp = new Date();

    @ColumnInfo(name = "guest")
    private boolean hasGuest;

    public CardHolderVisit() {
    }

    public CardHolderVisit(CardHolderData cardHolder) {
        customerId = cardHolder.getId();
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public boolean hasGuest() {
        return hasGuest;
    }

    public void setHasGuest(boolean hasGuest) {
        this.hasGuest = hasGuest;
    }
}
