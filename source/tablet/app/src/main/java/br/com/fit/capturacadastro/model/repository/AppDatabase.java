package br.com.fit.capturacadastro.model.repository;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import br.com.fit.capturacadastro.model.CardHolderData;
import br.com.fit.capturacadastro.model.CardHolderVisit;

@Database(entities = {CardHolderData.class, CardHolderVisit.class},
        version = 1, exportSchema = false)
@TypeConverters({DateTypeConverter.class})
public abstract class AppDatabase extends RoomDatabase {

    private static final String DATABASE_NAME = "customers";

    private static AppDatabase INSTANCE = null;

    public abstract CardHolderDAO CardHolderDAO();

    public static AppDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room
                    .databaseBuilder(context.getApplicationContext(), AppDatabase.class, DATABASE_NAME)
                    .build();
        }

        return INSTANCE;
    }
}
