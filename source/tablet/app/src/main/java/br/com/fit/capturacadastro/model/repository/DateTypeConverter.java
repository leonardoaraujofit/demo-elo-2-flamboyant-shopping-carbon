package br.com.fit.capturacadastro.model.repository;

import android.arch.persistence.room.TypeConverter;

import java.util.Date;

public class DateTypeConverter {

    @TypeConverter
    public static Long fromDate(Date date) {
        return date == null ? null : date.getTime();
    }

    @TypeConverter
    public static Date toDate(Long millisSinceEpoch) {
        return millisSinceEpoch == null ? null : new Date(millisSinceEpoch);
    }
}
