package br.com.fit.capturacadastro.model.repository;

import android.content.Context;
import android.util.Log;

import java.util.Date;

import br.com.fit.capturacadastro.model.CardHolderData;
import br.com.fit.capturacadastro.model.CardHolderVisit;
import br.com.fit.capturacadastro.util.Hash;

public class CardHolderPersistence {

    private static final String TAG = "PERSISTENCE";

    private static final String PUBLIC_KEY =  "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjiND9d0R" +
            "GSxrjZrx/jCrYZykJzr1BLbJvnroXB4YhRVh+iuVe0IItJ34YNDTHo4F2LbM1XfURP2GyJnY0Of/" +
            "NBxmVpWfX+UbUrJ5u2NBcIjtAXBm3ewObrIKaTehzeaULYh49X9CFc3ha23tNjIPv3rxX5wJqex7" +
            "VN55o9PjKpT/iZYdsOinKuPTO2kL4C5HEFrygSNRh/WFoV/rE7J1+sAaGg3WJ32rmiAZrffEstHh" +
            "mvH1ub8bk0TuuDiSHiEQdcgqXD5lLCel/wgbA5AexBMvyvXekh5LqY+OArt6m0+Eu8B7Pr21IMj9" +
            "yPtjuN3Lf5x/fpyqxXQPfSXNE0o1zQIDAQAB";

    private Context context;

    public CardHolderPersistence(Context context) {
        this.context = context;
    }

    public boolean cardHolderExists(CardHolderData cardHolder) {
        cardHolder.setHash(Hash.SHA256(cardHolder.getName() + cardHolder.getCardPan()));
        Log.d(TAG, "CardHolder hash: " + cardHolder.getHash());

        cardHolder.setCardPanHash(Hash.SHA256(cardHolder.getCardPan()));
        Log.d(TAG, "CardHolder pan-hash: " + cardHolder.getCardPanHash());

        String firstDigitsPan = cardHolder.getCardPan()
                .substring(0, Math.min(cardHolder.getCardPan().length(), 6));
        cardHolder.setCardPan(firstDigitsPan);
        Log.d(TAG, "CardHolder pan first digits: " + cardHolder.getCardPan());

        Integer id = AppDatabase.getInstance(context)
                        .CardHolderDAO()
                        .getCardHolderID(cardHolder.getHash());

        cardHolder.setId(id == null ? 0 : id);

        Log.d(TAG, "CardHolder exists: " + (cardHolder.getId() > 0));

        return cardHolder.getId() > 0;
    }

    public void insertCardHolder(CardHolderData cardHolderData) {
/*
        This code snippet was commented out because we won't encrypt the data in this 1st deliverable
        ---------------------------------------------------------------------------------------------

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("nome", cardHolderData.getName());
        jsonObject.addProperty("cartao", cardHolderData.getCardPan());
        jsonObject.addProperty("cpf", cardHolderData.getCPF());
        jsonObject.addProperty("telefone", cardHolderData.getPhone());
        jsonObject.addProperty("email", cardHolderData.getEmail());

        Gson gson = new Gson();
        String jsonToEncrypt = gson.toJson(jsonObject);

        String encryptedData;

        try {
            encryptedData = RSACipher.encrypt(jsonToEncrypt, RSACipher.stringToPublicKey(PUBLIC_KEY));
            Log.d(TAG, "Enprypted data: " + encryptedData);
        } catch (Exception ex) {
            ex.printStackTrace();
            Log.e(TAG, "ERRO AO ENCRIPTAR DADOS", ex);
            encryptedData = "";
        }

        cardHolderData.setEncryptedData(encryptedData);
*/

        // Update Timestamp
        cardHolderData.setTimestamp(new Date());

        Long insertedID = AppDatabase.getInstance(context)
                            .CardHolderDAO().insertCardHolder(cardHolderData);

        if (insertedID != null) {
            cardHolderData.setId(insertedID.intValue());
        }
    }

    public void insertVisitToCardHolder(CardHolderVisit cardHolderVisit) {
        AppDatabase.getInstance(context)
                .CardHolderDAO()
                .insertVisit(cardHolderVisit);
    }
}
