package br.com.fit.capturacadastro;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import br.com.fit.capturacadastro.view.MainActivity;

public class BootCompleteReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        context.startActivity(new Intent(context, MainActivity.class));
    }
}
