package br.com.fit.capturacadastro.view;

import android.text.Editable;
import android.text.TextWatcher;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MaskWatcher implements TextWatcher {
    private boolean isRunning = false;
    private boolean isDeleting = false;
    private final String mask;

    private Map<Character, List<Character>> validChars = new HashMap<>();

    public MaskWatcher(String mask) {
        this.mask = mask;
        validChars.put('9', Arrays.asList('0', '1', '2', '3', '4', '5', '6', '7', '8', '9'));
        validChars.put('.', Arrays.asList('.'));
        validChars.put('-', Arrays.asList('-'));
        validChars.put(' ', Arrays.asList(' '));
        validChars.put('(', Arrays.asList('('));
        validChars.put(')', Arrays.asList(')'));
    }

    public static MaskWatcher buildCpf() {
        return new MaskWatcher("999.999.999-99");
    }

    public static MaskWatcher buildPhone() {
        return new MaskWatcher("(99) 99999-9999");
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
        isDeleting = count > after;
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (isRunning || isDeleting) {
            return;
        }

        isRunning = true;

        int editableLength = editable.length();

        if (editableLength < mask.length()) {
            char currentCharInMask = mask.charAt(editableLength -1);
            char currentCharInEdit = editable.charAt(editableLength -1);

            // Get valid chars list according with validChars map
            List<Character> currentValidChars = validChars.get(currentCharInMask);

            // Fix current char
            if ((currentValidChars.size() == 1) && (currentCharInEdit != currentCharInMask)) {
                editable.insert(editableLength-1, mask, editableLength-1, editableLength);
                editableLength++;

                currentCharInMask = mask.charAt(editableLength -1);
                currentValidChars = validChars.get(currentCharInMask);
            }

            // If currentValidChars doesn't contains the pressed char, erase that char of edit
            if (!currentValidChars.contains(currentCharInEdit)) {
                editable.delete(editableLength -1, editableLength);
                editableLength--;
            }

            char nextCharInMask = mask.charAt(editableLength);

            // Get valid chars list according with validChars map
            List<Character> nextValidChars = validChars.get(nextCharInMask);

            if (nextValidChars.size() == 1) {
                editable.append(nextCharInMask);
            }
        }

        isRunning = false;
    }
}
