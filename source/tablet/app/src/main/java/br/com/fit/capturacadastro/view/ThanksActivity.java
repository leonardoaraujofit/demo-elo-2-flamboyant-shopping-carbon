package br.com.fit.capturacadastro.view;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.WindowManager;

import br.com.fit.capturacadastro.R;
import br.com.fit.capturacadastro.model.CardHolderData;
import br.com.fit.capturacadastro.model.repository.CardHolderService;

public class ThanksActivity extends Activity {

    private static final String TAG = "THANKS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thanks);

        Log.d(TAG, "onCreate");
    }

    @Override
    protected void onStart() {
        super.onStart();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "Finish");
                ThanksActivity.this.finish();
            }
        }, 6000);
    }
}
