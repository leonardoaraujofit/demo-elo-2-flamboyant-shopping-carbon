package br.com.fit.capturacadastro.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.provider.DocumentFile;
import android.text.InputFilter;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import br.com.fit.capturacadastro.R;
import br.com.fit.capturacadastro.model.CardHolderData;
import br.com.fit.capturacadastro.model.CardHolderVisit;
import br.com.fit.capturacadastro.model.ExportDataTask;
import br.com.fit.capturacadastro.model.repository.CardHolderContext;
import br.com.fit.capturacadastro.model.repository.CardHolderService;
import br.com.fit.capturacadastro.util.CPFValidator;

public class CustomerActivity extends Activity implements View.OnClickListener, ExportDataTask.OnExportResultListener {

    // Contants
    private static final String TAG = "CUSTOMER";
    private static final int REQUEST_CODE_STORAGE_ACCESS = 100;
    private static final List<String> EXPORTERS_CPF =
            Arrays.asList("12345678909", "98765432100", "14789632555", "52369874155");

    // Receivers
    private CardHolderSaveReceiver saveReceiver = new CardHolderSaveReceiver();

    // Logical Fields
    private boolean cardHolderExist = false;
    private boolean clearDataAfterExport = false;

    // Layout
    private TextInputLayout layoutEmail;
    private TextInputLayout layoutPhone;
    private TextInputLayout layoutCPF;

    private TextView txtMessage;

    private EditText edtEmail;
    private EditText edtPhone;
    private EditText edtCPF;

    private RadioGroup rdgGuest;
    private RadioGroup rdgAdvert;

    private Button btnSave;

    private ImageView imgLogo;

    // Model of view
    private CardHolderData cardHolder = null;

    public static float dpToPx(Context context, float valueInDp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);

        cardHolder = CardHolderContext.getInstance().getCardHolder();

        cardHolderExist = cardHolder.getId() > 0;

        LocalBroadcastManager
                .getInstance(this)
                .registerReceiver(saveReceiver, new IntentFilter(CardHolderSaveReceiver.ACTION));

        imgLogo = (ImageView) findViewById(R.id.img_logo_elo_customer);
        layoutCPF = (TextInputLayout) findViewById(R.id.input_layout_cpf);
        layoutPhone = (TextInputLayout) findViewById(R.id.input_layout_phone);
        layoutEmail = (TextInputLayout) findViewById(R.id.input_layout_email);
        txtMessage = (TextView) findViewById(R.id.txt_message);

        rdgGuest = (RadioGroup) findViewById(R.id.rdg_guest);
        rdgAdvert = (RadioGroup) findViewById(R.id.rdg_advert);

        if (!cardHolderExist) {
            edtCPF = (EditText) layoutCPF.findViewById(R.id.edt_cpf);
            edtCPF.addTextChangedListener(MaskWatcher.buildCpf());
            edtCPF.setFilters(new InputFilter[]{new InputFilter.LengthFilter(14)});

            edtPhone = (EditText) layoutPhone.findViewById(R.id.edt_phone);
            edtPhone.addTextChangedListener(MaskWatcher.buildPhone());
            edtPhone.setFilters(new InputFilter[]{new InputFilter.LengthFilter(15)});

            edtEmail = (EditText) layoutEmail.findViewById(R.id.edt_email);
        } else {
            ((RelativeLayout) findViewById(R.id.relative_img_logo_customer))
                    .setGravity(Gravity.CENTER_VERTICAL);

            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) imgLogo.getLayoutParams();
            layoutParams.setMargins(0, 0, 0, 0);
            imgLogo.setLayoutParams(layoutParams);

            layoutCPF.setVisibility(View.GONE);
            layoutEmail.setVisibility(View.GONE);
            layoutPhone.setVisibility(View.GONE);
            rdgAdvert.setVisibility(View.GONE);

            txtMessage.setText(getString(R.string.msg_bem_vindo_cliente, cardHolder.getFirstName()));
        }

        btnSave = (Button) findViewById(R.id.btn_save);
        btnSave.setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager
                .getInstance(this)
                .unregisterReceiver(saveReceiver);
        super.onDestroy();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }


    public void onExportResult(boolean result) {
        txtMessage.setText(result ? R.string.msg_export_success : R.string.msg_export_error);

        // startActivity(new Intent(android.provider.Settings.ACTION_MEMORY_CARD_SETTINGS));

        if (result) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            }, 5000);
        } else {
            btnSave.setEnabled(true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != REQUEST_CODE_STORAGE_ACCESS) {
            super.onActivityResult(requestCode, resultCode, data);
            return;
        }

        Uri treeUri;
        // Get Uri from Storage Access Framework.
        if ((resultCode != Activity.RESULT_OK) || ((treeUri = data.getData()) == null)) {
            onExportResult(false);
            return;
        }

        // Persist access permissions.
        final int takeFlags = data.getFlags()
                & (Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        getContentResolver().takePersistableUriPermission(treeUri, takeFlags);

        DocumentFile pickedDir = DocumentFile.fromTreeUri(this, treeUri);

        new ExportDataTask(this, this, pickedDir, clearDataAfterExport).execute();
    }

    private boolean isEmailValid(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    private boolean isPhoneValid(String phone) {
        String phone_digits = phone.replaceAll("\\D+", "");

        Pattern pattern = Patterns.PHONE;
        return phone.isEmpty() ||
                pattern.matcher(phone).matches() &&
                        phone_digits.length() >= 10 && phone_digits.length() <= 15;
    }

    private boolean validateCPF() {
        String cpf = edtCPF.getText().toString();

        if (!CPFValidator.isFormatValid(cpf)) {
            layoutCPF.setError(getString(R.string.cpf_invalido));
            return false;
        }

        if (!CPFValidator.isCPFValid(cpf)) {
            Log.d(TAG, "Digito verificador");
            layoutCPF.setError(getString(R.string.cpf_invalido));
            return false;
        }

        layoutCPF.setError(null);

        return true;
    }

    private boolean validateFields() {
        boolean success = true;

        int checkedRadio = rdgGuest.getCheckedRadioButtonId();
        if (checkedRadio != R.id.guest_yes && checkedRadio != R.id.guest_no) {
            rdgGuest.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_shake));
            success = false;
        }

        if (cardHolderExist) {
            return success;
        }
        
        success = success && validateCPF();

        if (!isEmailValid(edtEmail.getText().toString())) {
            layoutEmail.setError(getString(R.string.email_invalido));
            success = false;
        } else {
            layoutEmail.setError(null);
        }

        if (!isPhoneValid(edtPhone.getText().toString())) {
            layoutPhone.setError(getString(R.string.phone_invalido));
            success = false;
        } else {
            layoutPhone.setError(null);
        }

        return success;
    }

    private String getCPFClean() {
        return edtCPF.getText().toString()
                .replace("-", "")
                .replace(".", "");
    }

    private void fieldsToCardHolder() {
        if (!cardHolderExist) {
            cardHolder.setEmail(edtEmail.getText().toString());
            cardHolder.setPhone(edtPhone.getText().toString());
            cardHolder.setCPF(getCPFClean());
            cardHolder.setAcceptAdvertising(rdgAdvert.getCheckedRadioButtonId() == R.id.advert_yes);
        }

        CardHolderVisit visit = new CardHolderVisit(cardHolder);
        visit.setHasGuest(rdgGuest.getCheckedRadioButtonId() == R.id.guest_yes);

        CardHolderContext.getInstance().setCardHolderVisit(visit);
    }

    private boolean isExporterCPF() {
        return edtCPF != null && edtCPF.getVisibility() == View.VISIBLE &&
                EXPORTERS_CPF.contains(edtCPF.getText().toString()
                        .replaceAll("\\D", ""));
    }

    private void showExportDialog() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.title_export_dialog)
                .setCancelable(true)
                .setMessage(R.string.msg_export_dialog)
                .setIcon(android.R.drawable.ic_menu_help)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        txtMessage.setText(R.string.msg_exportando);
                        clearDataAfterExport = true;
                        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                        startActivityForResult(intent, REQUEST_CODE_STORAGE_ACCESS);
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        txtMessage.setText(R.string.msg_exportando);
                        clearDataAfterExport = false;
                        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                        startActivityForResult(intent, REQUEST_CODE_STORAGE_ACCESS);

                    }
                }).setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                btnSave.setEnabled(true);
            }
        }).show();

    }

    // btnSave click
    @Override
    public void onClick(View v) {
        Log.d(TAG, "SAVE CLICK");

        // Avoid multiple clicks
        btnSave.setEnabled(false);

        if (isExporterCPF()) {
            showExportDialog();
            // btnSave.setEnabled(true);
            return;
        }

        if (!validateFields()) {
            btnSave.setEnabled(true);
            return;
        }

        fieldsToCardHolder();

        startService(new Intent(CardHolderService.ACTION_CARDHOLDER_SAVE)
                .setPackage(getPackageName()));
    }

    public class CardHolderSaveReceiver extends BroadcastReceiver {
        public static final String ACTION = "br.com.fit.capturacadastro.action.CARDHOLDERSAVE";

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "START THANKS");
            startActivity(new Intent(context, ThanksActivity.class));
            // Close current activity
            finish();
        }
    }
}
