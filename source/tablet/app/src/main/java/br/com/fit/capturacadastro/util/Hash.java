package br.com.fit.capturacadastro.util;

import android.util.Log;

import java.security.MessageDigest;

public class Hash {
    private static final String TAG = "HASH";

    private static String convertToHex(byte[] data) {
        StringBuilder buf = new StringBuilder();
        for (byte b : data) {
            int halfbyte = (b >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte) : (char) ('a' + (halfbyte - 10)));
                halfbyte = b & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString().toUpperCase();
    }

    public static String SHA256(String text) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] textBytes = text.getBytes("UTF-8");
            md.update(textBytes, 0, textBytes.length);
            byte[] hash = md.digest();
            return convertToHex(hash);
        } catch (Exception ex) {
            ex.printStackTrace();
            Log.e(TAG, "ERRO AO GERAR HASH [" + ex.getMessage()+ "]");
        }

        return "";
    }
}
