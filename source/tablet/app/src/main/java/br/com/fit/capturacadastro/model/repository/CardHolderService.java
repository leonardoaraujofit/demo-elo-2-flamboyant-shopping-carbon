package br.com.fit.capturacadastro.model.repository;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import br.com.fit.capturacadastro.model.CardHolderData;
import br.com.fit.capturacadastro.model.CardHolderVisit;
import br.com.fit.capturacadastro.view.CustomerActivity;
import br.com.fit.capturacadastro.view.MainActivity;

public class CardHolderService extends IntentService {

    private static final String TAG = "SERVICE";

    public static final String ACTION_CARDHOLDER_EXISTS = "br.com.fit.capturacadastro.action.CARDHOLDER_EXISTS";
    public static final String ACTION_CARDHOLDER_SAVE = "br.com.fit.capturacadastro.action.CARDHOLDER_SAVE";

    private CardHolderPersistence cardHolderPersistence = new CardHolderPersistence(this);

    public CardHolderService() {
        super("CardHolderService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String action = intent.getAction();
        CardHolderData cardHolder = CardHolderContext.getInstance().getCardHolder();

        if (action == null || cardHolder == null) {
            return;
        }

        switch (action) {
            case ACTION_CARDHOLDER_EXISTS: {
                Log.d(TAG, "CARDHOLDER EXISTS");
                boolean exists = cardHolderPersistence.cardHolderExists(cardHolder);

                Intent resultIntent = new Intent(MainActivity.CardSwipeReceiver.ACTION_CARDHOLEDER_EXISTS)
                        .putExtra(MainActivity.CardSwipeReceiver.EXTRA_CARDHOLDER_EXISTS, exists);

                LocalBroadcastManager.getInstance(this)
                        .sendBroadcast(resultIntent);
                break;
            }

            case ACTION_CARDHOLDER_SAVE: {
                Log.d(TAG, "CARDHOLDER SAVE");
                CardHolderVisit cardHolderVisit = CardHolderContext.getInstance().getCardHolderVisit();

                if (cardHolder.getId() == 0) {
                    cardHolderPersistence.insertCardHolder(cardHolder);

                    cardHolderVisit.setCustomerId(cardHolder.getId());
                    cardHolderVisit.setTimestamp(cardHolder.getTimestamp());
                }

                cardHolderPersistence.insertVisitToCardHolder(cardHolderVisit);

                LocalBroadcastManager.getInstance(this)
                        .sendBroadcast(new Intent(CustomerActivity.CardHolderSaveReceiver.ACTION));

                break;
            }
        }
    }

}
