package br.com.fit.capturacadastro.model.repository;

import br.com.fit.capturacadastro.model.CardHolderData;
import br.com.fit.capturacadastro.model.CardHolderVisit;

public class CardHolderContext {

    private static CardHolderContext INSTANCE = null;

    private CardHolderData cardHolder = null;
    private CardHolderVisit cardHolderVisit = null;
    private String serialNumber = null;

    private CardHolderContext() {
    }

    public static CardHolderContext getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CardHolderContext();
        }

        return INSTANCE;
    }

    public CardHolderData getCardHolder() {
        return cardHolder;
    }

    public void setCardHolder(CardHolderData cardHolder) {
        this.cardHolder = cardHolder;
    }

    public void setCardHolderVisit(CardHolderVisit cardHolderVisit) {
        this.cardHolderVisit = cardHolderVisit;
    }

    public CardHolderVisit getCardHolderVisit() {
        return cardHolderVisit;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
}
