package br.com.fit.capturacadastro.view;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;

import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import br.com.fit.capturacadastro.R;
import br.com.fit.capturacadastro.SocketService;
import br.com.fit.capturacadastro.model.CardHolderData;
import br.com.fit.capturacadastro.model.repository.CardHolderContext;
import br.com.fit.capturacadastro.model.repository.CardHolderService;

public class MainActivity extends Activity implements View.OnClickListener {

    private static final String TAG = "MAIN";

    private CardSwipeReceiver swipeReceiver = new CardSwipeReceiver();
    private TextView txtMensagem;
    private Button enterButton;
    private LinearLayout layoutCentral;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        layoutCentral = (LinearLayout) findViewById(R.id.layout_central);
        txtMensagem = (TextView) findViewById(R.id.txt_message);

        IntentFilter filter = new IntentFilter();
        filter.addAction(CardSwipeReceiver.ACTION_CARDSWIPE);
        filter.addAction(CardSwipeReceiver.ACTION_CARDHOLEDER_EXISTS);

        LocalBroadcastManager.getInstance(this)
                .registerReceiver(swipeReceiver, filter);

        startService(new Intent(this, SocketService.class));
    }

    @Override
    protected void onStart() {
        txtMensagem.setText(R.string.msg_passe_cartao);
        deflateEnterButton();
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "Destroy MAIN");
        LocalBroadcastManager.getInstance(this)
                .unregisterReceiver(swipeReceiver);

        stopService(new Intent(this, SocketService.class));

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (deflateEnterButton()) {
            txtMensagem.setText(R.string.msg_passe_cartao);
        } // else
            // super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        startActivity(new Intent(this, CustomerActivity.class));
    }

    private void inflateEnterButton() {
        enterButton = (Button) findViewById(R.id.btn_enter);

        if (enterButton == null) {
            enterButton  = (Button) getLayoutInflater()
                    .inflate(R.layout.btn_enter, layoutCentral,false);
            enterButton.setOnClickListener(this);
            layoutCentral.addView(enterButton);
        }
    }

    private boolean deflateEnterButton() {
        enterButton = (Button) findViewById(R.id.btn_enter);

        if (enterButton != null) {
            layoutCentral.removeView(enterButton);
            enterButton.setOnClickListener(null);
            return true;
        }

        return false;
    }

    public class CardSwipeReceiver extends BroadcastReceiver {

        public static final String TAG = "SWIPE";

        public static final String ACTION_CARDSWIPE = "br.com.fit.capturacadastro.view.action.CARDSWIPE";
        public static final String ACTION_CARDHOLEDER_EXISTS = "br.com.fit.capturacadastro.view.action.CARDHOLEDER_EXISTS";

        public static final String EXTRA_CARDHOLDER = "br.com.fit.capturacadastro.view.extra.CARDHOLDER";
        public static final String EXTRA_CARDHOLDER_EXISTS = "br.com.fit.capturacadastro.view.extra.CARDHOLDER_EXISTS";
        public static final String EXTRA_SUCCESS = "br.com.fit.capturacadastro.view.extra.SUCCESS";
        public static final String EXTRA_SERIAL_NUMBER = "br.com.fit.capturacadastro.view.extra.SERIAL_NUMBER";


        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Bundle extras = intent.getExtras();

            if (action == null || extras == null)  {
                return;
            }

            switch (action) {
                case ACTION_CARDSWIPE: {
                    CardHolderData cardHolderData = (CardHolderData) extras.getSerializable(EXTRA_CARDHOLDER);
                    boolean success = extras.getBoolean(EXTRA_SUCCESS, false);
                    String serialNumber = extras.getString(EXTRA_SERIAL_NUMBER, "");

                    if (cardHolderData != null && success && !serialNumber.isEmpty()) {
                        CardHolderContext.getInstance().setSerialNumber(serialNumber);
                        CardHolderContext.getInstance().setCardHolder(cardHolderData);

                        context.startService(new Intent(CardHolderService.ACTION_CARDHOLDER_EXISTS)
                                .setPackage(context.getPackageName()));
                    } else {
                        deflateEnterButton();
                        txtMensagem.setText(R.string.msg_erro_cartao);
                    }
                    break;
                }

                case ACTION_CARDHOLEDER_EXISTS: {
                    boolean exists = extras.getBoolean(EXTRA_CARDHOLDER_EXISTS, false);
                    Log.d(TAG, "EXISTS: " + exists);

                    inflateEnterButton();
                    String firstName = CardHolderContext.getInstance()
                            .getCardHolder().getFirstName();

                    txtMensagem.setText(getString(R.string.msg_bem_vindo_main, firstName));
                    break;
                }
            }
        }
    }
}
