package br.com.fit.capturacadastro.util;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CPFValidator {

    public static boolean isFormatValid(String cpf) {
        Pattern pattern = Pattern.compile(
                "([0-9]{3})\\.([0-9]{3})\\.([0-9]{3})-([0-9]{2})");

        Matcher matcher = pattern.matcher(cpf);

        return matcher.matches();
    }

    public static boolean isCPFValid(String cpf) {
        cpf = cpf.replace(".", "").replace("-", "");

        if (cpf.length() != 11) {
            return false;
        }

        char[] array = new char[11];
        Arrays.fill(array, cpf.charAt(0));
        String cpf_digitos_iguais =  new String(array);

        if (cpf_digitos_iguais.equals(cpf)) {
            return false;
        }

        String digit = cpf.substring(0, 9);
        String dvs = cpf.substring(9, 11);

        String dv1 = generateDigits(digit);
        String dv2 = generateDigits(digit + dv1);

        return dvs.equals(dv1 + dv2);
    }

    private static String generateDigits(String digitos) {
        int peso = digitos.length() + 1;
        int dv = 0;
        for (int i = 0; i < digitos.length(); i++) {
            dv += Integer.parseInt(digitos.substring(i, i + 1)) * peso;
            peso--;
        }

        dv = 11 - (dv % 11);

        if (dv > 9) {
            return "0";
        }

        return String.valueOf(dv);
    }
}
