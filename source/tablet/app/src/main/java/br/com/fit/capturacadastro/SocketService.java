package br.com.fit.capturacadastro;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import br.com.fit.capturacadastro.model.CardHolderData;
import br.com.fit.capturacadastro.view.MainActivity;

public class SocketService extends IntentService {

    private static final String TAG = "SOCKET_SERVICE";

    public SocketService() {
        super("SocketService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        ServerSocket server = null;
        try {
            server = new ServerSocket(7777);

            Socket client;
            String socketData;
            JsonParser parser = new JsonParser();
            JsonObject jsonObject;
            CardHolderData cardHolderData;
            Intent notifier;
            JsonElement element;

            do {
                client = server.accept();

                BufferedReader reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
                socketData = reader.readLine();

                Log.d(TAG, "Received from socket: " + socketData);

                // cardHolderData = gson.fromJson(socketData, CardHolderData.class);
                notifier = new Intent(MainActivity.CardSwipeReceiver.ACTION_CARDSWIPE);

                jsonObject = parser.parse(socketData)
                                .getAsJsonObject();

                element = jsonObject.get("success");
                if (element != null) {
                    notifier.putExtra(MainActivity.CardSwipeReceiver.EXTRA_SUCCESS, element.getAsBoolean());
                }

                element = jsonObject.get("serialNumber");
                if (element != null) {
                    notifier.putExtra(MainActivity.CardSwipeReceiver.EXTRA_SERIAL_NUMBER, element.getAsString());
                }

                if (jsonObject.get("name") != null &&
                        jsonObject.get("cardPan") != null &&
                        jsonObject.get("expireDate") != null) {
                    CardHolderData cardHolder = new CardHolderData(jsonObject.get("name").getAsString(),
                            jsonObject.get("cardPan").getAsString(),
                            jsonObject.get("expireDate").getAsString());

                    notifier.putExtra(MainActivity.CardSwipeReceiver.EXTRA_CARDHOLDER, cardHolder);
                }

                LocalBroadcastManager.getInstance(this)
                        .sendBroadcast(notifier);
            } while (true);

        } catch (IOException ex) {
            ex.printStackTrace();
            Log.e(TAG, "ERRO AO RECEBER DADOS");
            Log.e(TAG, ex.getMessage());
        } finally {
            if (server != null) {
                try {
                    server.close();
                } catch (IOException ex) {
                    Log.e(TAG, "ERRO AO FECHAR O SOCKET");
                    Log.e(TAG, ex.getMessage());
                }
            }
        }
    }
}
