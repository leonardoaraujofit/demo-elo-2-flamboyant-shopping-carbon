package br.com.fit.capturacadastro.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.regex.Pattern;

// { "success": true, "name": "LUCAS MONARI", "cardPan": "0800777755558888"}

@Entity(tableName = "customers", indices={@Index(value="hash", unique=true)})
public class CardHolderData implements Serializable {

    @PrimaryKey(autoGenerate = true) private int id;
    @ColumnInfo(name = "hash") private String hash; // SHA256(CARD PAN + NAME)

    @ColumnInfo(name = "card_pan") private String cardPan; // CARD PAN SUBSTRING (0, 6)
    @ColumnInfo(name = "card_pan_hash") private String cardPanHash; // SHA256(CARD PAN)
    @ColumnInfo(name = "cpf") private String CPF;

    private String name = "";
    private String phone;
    private String email;

    @ColumnInfo(name = "advertising") private boolean acceptAdvertising;
    @NonNull private Date timestamp = new Date();
    @ColumnInfo(name = "expire_date") private String expireDate = "";

    public CardHolderData() {
    }

    public CardHolderData(String name, String cardPan, String expireDate) {
        this.name = name;
        this.cardPan = cardPan;
        this.expireDate = expireDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCardPan() {
        return cardPan;
    }

    public void setCardPan(String cardPan) {
         this.cardPan = cardPan;
    }

    public String getCardPanHash() {
        return cardPanHash;
    }

    public void setCardPanHash(String cardPanHash) {
        this.cardPanHash = cardPanHash;
    }

    @NonNull
    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(@NonNull Date timestamp) {
        this.timestamp = timestamp;
    }

    public boolean getAcceptAdvertising() {
        return acceptAdvertising;
    }

    public void setAcceptAdvertising(boolean advertising) {
        this.acceptAdvertising = advertising;
    }

    public String getFirstName() {
        // Name could be
        // FIRST LAST
        // LAST/FIRST
        // LAST\FIRST

        String firstName = name.trim();
        String[] names = firstName.split(" ");

        if (names.length > 0) {
            // name.split(" ")[0] returns
            // FIRST
            // LAST/FIRST
            // LAST\FIRST
            firstName = names[0];
        }

        // .split("/|\\\\") returns
        // ["FIRST"]
        // ["LAST", "FIRST"]
        // ["LAST", "FIRST"]
        names = firstName.split("/|\\\\");

        // names[names.length-1];
        // ["FIRST"] -> "FIRST"
        // ["LAST", "FIRST"] -> "FIRST"
        // ["LAST", "FIRST"] -> "FIRST"
        firstName = names[names.length-1];
        firstName = firstName.toLowerCase();

        if (firstName.isEmpty()) {
            firstName = "Visitante";
        } else {
            firstName = Character.toUpperCase(firstName.charAt(0)) +
                    firstName.substring(1);
        }

        return firstName;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }
}
