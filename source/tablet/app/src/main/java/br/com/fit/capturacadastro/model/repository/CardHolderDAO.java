package br.com.fit.capturacadastro.model.repository;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.database.Cursor;

import br.com.fit.capturacadastro.model.CardHolderData;
import br.com.fit.capturacadastro.model.CardHolderVisit;

@Dao
public interface CardHolderDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insertCardHolder(CardHolderData cardHolder);

    @Query("SELECT id FROM customers WHERE hash = :hash")
    Integer getCardHolderID(String hash);

    @Query("SELECT " +
                ":terminalId as terminal_id, id, name, card_pan, card_pan_hash, expire_date, cpf, phone, " +
                "email, date(timestamp/1000, 'unixepoch', 'localtime') as date, " +
                "time(timestamp/1000, 'unixepoch', 'localtime') as time, advertising " +
            "FROM customers")
    Cursor getAllCardHoldersWithTerminalId(String terminalId);

    @Insert
    void insertVisit(CardHolderVisit cardHolderVisit);

    @Query("SELECT " +
                ":terminalId as terminal_id, v.customer_id, c.cpf, v.guest, " +
                "date(v.timestamp/1000, 'unixepoch', 'localtime') as date, " +
                "time(v.timestamp/1000, 'unixepoch', 'localtime') as time " +
            "FROM visits v " +
            "JOIN customers c ON c.id = v.customer_id")
    Cursor getAllVisitsWithTerminalId(String terminalId);

    @Query("DELETE FROM visits")
    void clearAllVisits();
}
