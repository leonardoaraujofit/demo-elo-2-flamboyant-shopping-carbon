package br.com.fit.capturacadastro.model;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.provider.DocumentFile;
import android.util.Log;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import br.com.fit.capturacadastro.model.repository.AppDatabase;
import br.com.fit.capturacadastro.model.repository.CardHolderContext;

public class ExportDataTask extends AsyncTask<Void, Void, Void> {

    public interface OnExportResultListener {
        void onExportResult(boolean result);
    }

    private static final String TAG = "EXPORT";

    private boolean exportSuccessfully = false;
    private boolean clearVisits = false;
    private WeakReference<Context> weakContext;
    private WeakReference<OnExportResultListener> weakCallback;
    private DocumentFile exportPath;

    public ExportDataTask(Context context, OnExportResultListener callback, DocumentFile exportPath, boolean clearVisits) {
        this.weakContext = new WeakReference<>(context);
        this.weakCallback = new WeakReference<>(callback);
        this.clearVisits = clearVisits;
        this.exportPath = exportPath;
    }

    private List<String> cursorValueToStringArray(Cursor cursor) {
        if (cursor == null || cursor.isClosed() || cursor.isAfterLast()) {
            Log.d(TAG, "Cursor is either null, closed or after last record! We're done.");
            return null;
        }

        int count = cursor.getColumnCount();
        List<String> values = new ArrayList<>(count);
        for (int i = 0; i < count; ++i) {
            values.add(cursor.getString(i));
        }

        return values;
    }

    private boolean exportData(Cursor cursor, String fileName, ContentResolver resolver) {
        Log.d(TAG, String.format("Exporting data to %s", fileName));
        boolean success = false;

        DocumentFile file = exportPath.createFile("text/csv", fileName);

        Log.d(TAG, "Exporting in " + file.getUri().toString());

        List<String> columnNames = Arrays.asList(cursor.getColumnNames());
        CSVPrinter printer = null;

        try {
            OutputStream outputStream = resolver.openOutputStream(file.getUri());
            printer = new CSVPrinter(new OutputStreamWriter(outputStream),
                    CSVFormat.EXCEL.withDelimiter(';'));

            Log.d(TAG, "Writing field names");

            printer.printRecord(columnNames);
            cursor.moveToFirst();

            Log.d(TAG, "Writing records ...");

            while (!cursor.isAfterLast()) {
                List<String> values = cursorValueToStringArray(cursor);

                if (values == null) {
                    Log.d(TAG, "No values were returned! We're done.");
                    break;
                }

                printer.printRecord(values);
                cursor.moveToNext();
                Log.d(TAG, "Record has been successfully wrote!");
            }

            success = true;
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Exception: " + e.getMessage(), e);
            success = false;
        } finally {
            if (!cursor.isClosed()) {
                cursor.close();
            }

            if (printer != null) {
                try {
                    printer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e(TAG, "Exception: " + e.getMessage(), e);
                }
            }
        }

        return success;
    }

    private void createExportDirInPath() {
        String serialNumber = CardHolderContext.getInstance().getSerialNumber();
        String dateTime = new SimpleDateFormat("yyMMdd_HHmmss")
                .format(Calendar.getInstance().getTime());

        String exportDirName = serialNumber + "_" + dateTime;

        DocumentFile exportDir = exportPath.findFile(exportDirName);

        if (exportDir == null) {
            exportDir = exportPath.createDirectory(exportDirName);
        }

        exportPath = exportDir;
    }

    @Override
    protected Void doInBackground(Void... eraseTableContentsAtEnd) {
        Log.d(TAG, "Starting background data export...");

        Context context = weakContext.get();

        if (context != null) {
            try {
                String serial = CardHolderContext.getInstance().getSerialNumber();
                createExportDirInPath();

                final Cursor cursorCustomers = AppDatabase.getInstance(context)
                        .CardHolderDAO().getAllCardHoldersWithTerminalId(serial);

                final Cursor cursorVisits = AppDatabase.getInstance(context)
                        .CardHolderDAO().getAllVisitsWithTerminalId(serial);

                exportSuccessfully =
                        exportData(cursorCustomers, "customers.csv", context.getContentResolver()) &&
                        exportData(cursorVisits, "visits.csv", context.getContentResolver());

                if (exportSuccessfully && clearVisits) {
                    AppDatabase.getInstance(context).CardHolderDAO().clearAllVisits();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
                Log.e(TAG, "Error exporting data", ex);
                exportSuccessfully = false;
            }
        }

        Log.d(TAG, "Background data export done!");
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        OnExportResultListener listener = weakCallback.get();
        if (listener != null) {
            listener.onExportResult(exportSuccessfully);
        }

        super.onPostExecute(aVoid);
    }
}
