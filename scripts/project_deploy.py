#!/usr/bin/env python

import os, shutil, argparse, subprocess, traceback, sys, stat

def get_args():
  parser = argparse.ArgumentParser(description='Deploy StarterKit')
  parser.add_argument('platform', nargs='?', default='all', choices=['all', 'verix', 'engage'])
  parser.add_argument('-t', '--toolchain', action='store_true')
  args = parser.parse_args()
  return args

def create_deploy_folder(platform):
  folder_path = os.path.join('..', 'deploy', platform)
  print 'Creating folder at: ' + os.path.abspath(folder_path)
  
  if not os.path.exists(folder_path):
    os.makedirs(folder_path)
  else:
    def on_error_callback(func, path, exc_info):
      os.chmod(path, stat.S_IWRITE)
      if os.path.isfile(path):
        os.remove(path)
    for filename in os.listdir(folder_path):
      shutil.rmtree(os.path.join(folder_path, filename), onerror=on_error_callback)

def copy_folder(src, dst):
  if not os.path.exists(src):
    raise Exception("Can't find files at " + src)
  shutil.copytree(src, dst)

def copy_toolchain(platform):
  print 'Moving toolchain...'
  base_src = os.path.join('..', 'toolchain')
  base_dst = os.path.join('..', 'deploy', platform, 'toolchain')
  folders_to_copy = ['signer', 'nmake', platform]

  for folder in folders_to_copy:
    print 'Copying ' + folder + ' folder...'
    copy_folder(os.path.join(base_src, folder), os.path.join(base_dst, folder))

def generate_docs(platform):
  print "Generating docs..."
  os.chdir('doxygen')
  subprocess.call(['python', 'doc_deploy.py', platform])
  os.chdir('..')

def copy_samples(platform):
  base_src = os.path.join('..', 'samples')
  base_dst = os.path.join('..', 'deploy', platform, 'samples')

  print "Copying samples..."
  folders_to_copy = []
  if platform == 'verix':
    folders_to_copy = ['course_verix', 'sdk_verix', 'vlib_verix', 'adk']
  elif platform == 'engage':
    folders_to_copy = ['course_engage', 'adk']
  for folder in folders_to_copy:
    print 'Copying ' + folder + ' folder...'
    copy_folder(os.path.join(base_src, folder), os.path.join(base_dst, folder))

  print "Removing and renaming folders..."
  for root, dirs, files in os.walk(base_dst, topdown=False):
    for dir in dirs:
      fullpath = os.path.join(root, dir)
      if dir == 'build':
        shutil.rmtree(fullpath)
      elif (platform == 'verix' and 'engage' in dir) or (platform == 'engage' and 'verix' in dir):
        shutil.rmtree(fullpath)
      elif '_verix' in dir:
        os.rename(fullpath, os.path.join(root, dir.replace('_verix', '')))
      elif '_engage' in dir:
        os.rename(fullpath, os.path.join(root, dir.replace('_engage', '')))

def deploy_starterkit(platform):
  create_deploy_folder(platform)
  copy_toolchain(platform)
  generate_docs(platform)
  copy_samples(platform)

if __name__=='__main__':
  args = get_args()

  print 'Deploying StarterKit: ' + args.platform
  print "Target platform: " + args.platform
  print "Check toolchain? " + str(args.toolchain)
  
  try:
    if args.toolchain:
      subprocess.check_call(['python', 'toolchain_download.py'])
    
    if args.platform == 'all':
      deploy_starterkit('verix')
      deploy_starterkit('engage')
    else:
      deploy_starterkit(args.platform)
    sys.exit(0)
  except:
    traceback.print_exc()
    sys.exit(1)