#!/usr/bin/env python

from shutil import copytree, rmtree, move, copy2
from distutils.dir_util import copy_tree
from distutils.file_util import copy_file
import traceback
import os
import sys
import stat

class Updater:
	def __init__(self):
		self.dependencies = []
		self.remotePath = ''
		self.dependenciesFile = ''
		self.aliasDict = {}
		self.versionsFile = 'ver'
		self.versionsDict = {}
		self.mappingsFile = ''
		self.mappingsDict = {}
		self.downloaded = {}
		self.tempPath = '__temp\\'
	
	def update(self):
		self.loadDependencies()
		if self.loadVersions():
			self.filterDependencies()
		self.loadMappings()
		self.downloadDependencies()
		self.updateVersionsFile()
		self.performMappings()
	
	def loadDependencies(self):
		file = open(self.dependenciesFile)
		lines = file.readlines()
		for l in lines:
			if l not in ['','\n'] and l[0] is not '#':
				tokens = l.replace('\n','').split(':')
				folder = tokens[0].strip()
				alias = tokens[1].strip()
				self.aliasDict[folder] = alias
				self.dependencies.append(folder)
		file.close()
		
	def filterDependencies(self):
		newdependencies = []
		print 'Checking for new dependencies needed:'
		for d in self.dependencies:
			key = d[:d.rfind('\\')]
			version = d[d.rfind('\\')+1:]
			if key in self.versionsDict:
				lastversion = self.versionsDict[key]
				if lastversion != version:
					newdependencies.append(d)
					print ' - New version: {}'.format(d)
			else:
				newdependencies.append(d)
				print ' - New version: {}'.format(d)
		self.dependencies = newdependencies
		print 'Finished checking for new dependencies.\n\n'
	
	def loadDictionary(self, fileName, dict):
		if (os.path.exists(fileName)):
			file = open(fileName)
			lines = file.readlines()
			for l in lines:
				if l not in ['','\n'] and l[0] is not '#':
					tokens = l.replace('\n','').split(':')
					key = tokens[0].strip()
					value = tokens[1].strip()
					dict[key] = value
			file.close()
			return True
		else:
			return False
			
	def loadVersions(self):
		if self.loadDictionary(self.versionsFile, self.versionsDict):
			#print 'Versions: {}'.format(self.versionsDict)
			return True
		else:
			print 'No versions file found.\n'
			return False
			
	def loadMappings(self):
		auxMappingDict = {}
		if self.loadDictionary(self.mappingsFile, auxMappingDict):
			for from_,to_ in auxMappingDict.iteritems():
				key = from_[:from_.find('\\')]
				if key not in self.mappingsDict:
					self.mappingsDict[key] = {}
				self.mappingsDict[key][from_] = to_;
			#print 'Mappings: {}'.format(self.mappingsDict)
		else:
			print 'No mappings file found.\n'
			
	def updateVersionsFile(self):
		ver = open(self.versionsFile, 'w')
		ver.seek(0)
		ver.truncate()
		for key,value in self.versionsDict.iteritems():
			ver.write('{}:{}\n'.format(key,value))
		ver.close()
		
	def downloadDependencies(self):
		try:
			print 'Downloading new dependencies:'
			if len(self.dependencies) > 0:
				for d in self.dependencies:
					a = os.path.join(self.remotePath, d)
					if os.path.exists(a):
						b = os.path.join(self.tempPath, self.aliasDict[d])
						print ' - Downloading {}'.format(d)
						copy_tree(a, b, preserve_mode=0)
						key = d[:d.rfind('\\')]
						newversion = d[d.rfind('\\')+1:]
						self.versionsDict[key] = newversion
						self.downloaded[d] = True
					else:
						self.downloaded[d] = False
						print ' - ERROR: Invalid dependency or version: {}'.format(d)
				print 'Finished downloading new dependencies.\n\n'
			else:
				print 'Toolchain is already up-to-date.\n\n'
		except:
			print 'Errors during download, see below:'
		
	def checkConnectivity(self):
		try:
			return os.path.exists(self.remotePath)
		except:
			return False

	def performMappings(self):
		try:
			print 'Mapping downloaded dependencies:'
			for path,alias in self.aliasDict.iteritems():
				localpath = os.path.join(self.tempPath, alias)
				#print path
				#print self.downloaded
				if path not in self.downloaded or not self.downloaded[path]:
					print ' - Mapping {}: \tNOT DOWNLOADED'.format(path)
				else:
					print ' - Mapping {}'.format(path)
					if os.path.exists(localpath):
						if alias in self.mappingsDict:
							for from_,to_ in self.mappingsDict[alias].iteritems():
								if from_.find('*') != -1:
									folder = os.path.join(self.tempPath,from_[:-1])
									names = os.listdir(folder)
									for name in names:
										filename = os.path.join(folder, name)
										if os.path.isfile(filename):
											destination = os.path.join(to_,name)
											if not os.path.exists(to_):
												os.makedirs(to_)
											copy_file(filename, to_)
								else:
									copy_tree(os.path.join(self.tempPath,from_), to_)
						else:
							copy_tree(localpath, alias)
			print 'Finished mapping downloaded dependencies.\n'
		except:
			print 'Errors during mapping, see below:\n\t'
			raise
				
	def removeTempPath(self):
		if os.path.exists(self.tempPath):
			rmtree(self.tempPath)
		
if __name__=='__main__':

	#Instantiates updater for toolchain
	u = Updater()
	u.remotePath = r'\\SAONTE264\toolchain'
	u.dependenciesFile = '../toolchain/toolchain.conf'
	u.mappingsFile = '../toolchain/mapping.conf'
	u.versionsFile = '../toolchain/ver'

	print '=========================\nToolchain updater 1.0\n=========================\n'
	print 'Remote path: {}\n'.format(u.remotePath)
	print 'Checking connectivity...'
	print ''

	if not u.checkConnectivity():
		print 'Could not reach %s' % u.remotePath
		sys.exit(1)
	
	try:
		u.update()
	except:
		print sys.exc_info()
		print traceback.format_exc()
		print '\nERROR: Toolchain update failed, see above errors.\n', sys.exc_info()[0]
	
	try:
		u.removeTempPath()
	except:
		print traceback.format_exc()
		print '\nERROR: Temp folder removal failed, see above errors.\n'
	
	print '\nFINISHED: Press <Enter> to exit.\n'
	raw_input()
	
	
