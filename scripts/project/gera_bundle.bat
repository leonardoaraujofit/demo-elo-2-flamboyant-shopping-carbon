@echo off

SETLOCAL

set WinPrgFile=%ProgramFiles(x86)%
if not "%WinPrgFile%"=="" goto win64
set WinPrgFile=%ProgramFiles%
:win64
For %%x in ("%WinPrgFile%") do set PROGRAM_FILES=%%~sx

:: Static variables (Don't change anything here!)
set TEMP_FOLDER=__temp__
set PACKAGE_MAN_PATH=%PROGRAM_FILES%\Verifone\PackageManagerProduction
set CYGWIN_DIR=%PACKAGE_MAN_PATH%\Cygwin
set SIGNER="%PROGRAM_FILES%\Verifone\FST\FileSignature.exe"
set PACKAGE_MAN="%PACKAGE_MAN_PATH%\PackageManager.exe"

:: Defining needed variables (You can change them according your needs)
set PROJECT_BASE_DIR=..\..
set BUILD_DIR=%PROJECT_BASE_DIR%\build
set ASSETS_DIR=%PROJECT_BASE_DIR%\assets\pos
set APP_PATH=%PROJECT_BASE_DIR%\build\demo_elo
set VERSION=1.0.0

call :ExpandPath %PROJECT_BASE_DIR%
set PROJECT_BASE_DIR=%EXPANDED_PATH%

call :ExpandPath %BUILD_DIR%
set BUILD_DIR=%EXPANDED_PATH%

call :ExpandPath %ASSETS_DIR%
set ASSETS_DIR=%EXPANDED_PATH%

:: Using a "batch trick" to extract filename and change APP_PATH to absolute path
for %%x in (%APP_PATH%) do (
  set APP_NAME=%%~nx
  set APP_PATH=%%~fx
)

:: Temporarily including Cygwin into the PATH
:: P.S.: This could fail if the Windows PATH contains an empty entry (;;)
::       Change the next line as following:
:: set PATH=%CYGWIN_DIR%
set PATH=%PATH%;%CYGWIN_DIR%

:: Cleaning existing directory, if needs
if exist %TEMP_FOLDER%\nul (
  rmdir /S /Q %TEMP_FOLDER%
)

:: Creating needed directories
mkdir %TEMP_FOLDER%\CONTROL
pushd %TEMP_FOLDER%

:: Copying app and needed assets
copy %APP_PATH% .
xcopy %ASSETS_DIR%\* . /S /E

:: Creating config files (app)
(echo Package: %APP_NAME% &echo Version: %VERSION% &echo User: usr1) > CONTROL\control
echo %APP_NAME% > CONTROL\start

:: Generating app .tar file
tar.exe --mode="u=rwx,go=rx" -pcf %APP_NAME%-%VERSION%.tar %APP_NAME% CONTROL
del /Q CONTROL\start

:: Creating config files (resources)
(echo Package: resources &echo Version: %VERSION% &echo User: usr1 &echo Group: system) > CONTROL\control

:: Generating resources .tar file
tar.exe --mode="u=rwx,go=rx" -pcf resources-%VERSION%.tar www CONTROL

:: Creating config files (fonts)
(echo Package: fonts &echo Version: %VERSION% &echo User: usr1 &echo Group: share &echo Type: userfont) > CONTROL\control

:: Generating fonts .tar file
tar.exe --mode="u=rwx,go=rx" -pcf fonts-%VERSION%.tar CONTROL
pushd fonts
tar.exe --mode="u=rwx,go=rx" -prf ../fonts-%VERSION%.tar *
popd

:: Generating bundle config files
(echo Name: %APP_NAME% &echo Version: %VERSION% &echo User:usr1) > CONTROL\control

:: Generating the 1st .tgz file
tar.exe --mode="u=rwx,go=rx" -pczf %APP_NAME%-%VERSION%.tgz %APP_NAME%-%VERSION%.tar resources-%VERSION%.tar fonts-%VERSION%.tar CONTROL

:: Generating the 2nd .tgz file
tar.exe --mode="u=rwx,go=rx" -pczf %APP_NAME%.tgz %APP_NAME%-%VERSION%.tgz

:: Fixing permissions and signing the just generated bundle
%PACKAGE_MAN% -i %APP_NAME%.tgz -fap -d -q -c

:: Moving generated bundle to parent directory
move %APP_NAME%.tgz %BUILD_DIR%

:: Cleaning the temp directory
popd
rmdir /S /Q %TEMP_FOLDER%

:: End of script
ENDLOCAL
exit /B

:: Helper subroutines
:ExtractFileName
  set APP_NAME=%~n1
  exit /B

:ExpandPath
  set EXPANDED_PATH=%~dpn1
  exit /B

:ListFiles
  setlocal enabledelayedexpansion enableextensions
  set FILE_LIST=
  for /F %%f in ('dir /b %~dpn1') do set FILE_LIST=!FILE_LIST! %%f
  endlocal & set FILE_LIST=%FILE_LIST:~1%
  exit /B
